/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.DocAsplTcont;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocAsplTcontMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 11:15:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface DocAsplTcontMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    DocAsplTcont selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<DocAsplTcont> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(DocAsplTcont record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(DocAsplTcont record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(DocAsplTcont record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(DocAsplTcont record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: updateContNoByTcontNo
     * @方法描述: 根据贸易合同编号更新贸易合同表中的合同编号
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateContNoByTcontNo(HashMap map);
    /**
     * @方法名称: xdzc0016
     * @方法描述: 根据贸易背景收集任务表去查询贸易合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdzc0016.resp.List> xdzc0016(QueryModel model);

    /**
     * @方法名称: updateEmptyByTContNo
     * @方法描述: 根据贸易合同编号置空
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateEmptyByTContNo(@Param("tcontNo")String tContNo);

    /**
     * @方法名称: selectByTcontNo
     * @方法描述: 根据贸易合同编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    DocAsplTcont selectByTcontSerno(@Param("tcontImgId") String tcontImgId);
}