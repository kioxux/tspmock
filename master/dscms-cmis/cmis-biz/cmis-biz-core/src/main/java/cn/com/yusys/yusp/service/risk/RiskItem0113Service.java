package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author quwen
 * @version 1.0.0
 * @date 2021年9月7日13:41:23
 * @desc 小企业无还本续贷业务限额校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0113Service {

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private AccLoanService accLoanService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author quwen
     * @date 2021年9月8日13:41:23
     * @version 1.0.0
     * @desc    小企业无还本续贷业务限额10亿
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0113(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String pvpSerno = (String)queryModel.getCondition().get("bizId");
        if (StringUtils.isBlank(pvpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 通过申请流水号获取申请信息
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPvpLoanSernoKey(pvpSerno);
        if(Objects.isNull(pvpLoanApp)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017); //通过放款流水号未获取到贷款出账申请信息
            return riskResultDto;
        }
        // 判断该笔业务是不是小企业无还本续贷业务
        if(!CmisCommonConstants.STD_LOAN_MODAL_8.equals(pvpLoanApp.getLoanModal())){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_LEVEL_1); //不校验
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); //不使用此项检查
            return riskResultDto;
        }else{
            // 获取所有小企业无还本续贷业务历史金额+本笔
            QueryModel queryModelData = new QueryModel();
            queryModelData.addCondition("loanModal", CmisCommonConstants.STD_LOAN_MODAL_8);
            List<AccLoan> accLoanList = accLoanService.selectByModel(queryModelData);
            // 所有小企业无还本续贷业务历史金额
            BigDecimal exchangeRmbAmt = BigDecimal.ZERO;
            // 所有小企业无还本续贷业务金额（包含本笔）
            BigDecimal totalRepayLoanAmt = BigDecimal.ZERO;

            if(CollectionUtils.nonEmpty(accLoanList)){
                for (AccLoan accLoan : accLoanList) {
                    exchangeRmbAmt = exchangeRmbAmt.add(accLoan.getExchangeRmbAmt());
                }
            }else{
                exchangeRmbAmt = BigDecimal.ZERO;
            }

            // 小企业无还本续贷业务限额10亿
            totalRepayLoanAmt = exchangeRmbAmt.add(pvpLoanApp.getCvtCnyAmt());
            if(totalRepayLoanAmt.compareTo(new BigDecimal(1000000000))>0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11202); //小企业无还本续贷业务限额10亿
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
