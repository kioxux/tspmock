package cn.com.yusys.yusp.service.server.xdxw0064;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0064.req.Xdxw0064DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0064.resp.List;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 接口处理类:优企贷、优农贷授信调查信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Service
public class Xdxw0064Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0064Service.class);

    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;

    /**
     * 优企贷、优农贷授信调查信息查询
     * @param xdxw0064DataReqDto
     * @return
     */
    @Transactional
    public java.util.List<List> getSurveyList(Xdxw0064DataReqDto xdxw0064DataReqDto) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, JSON.toJSONString(xdxw0064DataReqDto));
        java.util.List<List> result = null;
        try {
            Map queryMap = new HashMap();
            String query_type = xdxw0064DataReqDto.getQuery_type();
            String cert_code = xdxw0064DataReqDto.getCert_code();//证件号
            String survey_serno = xdxw0064DataReqDto.getSurvey_serno();//流水号
            if (Objects.equals(CmisBizConstants.CX_LSH, query_type)) {
                queryMap.put("survey_serno", survey_serno);
            }
            if (Objects.equals(CmisBizConstants.CX_ZJH, query_type)) {
                queryMap.put("cert_code", cert_code);
            }
            // 查询调查主表
            result = lmtSurveyReportMainInfoMapper.getSurveyList(queryMap);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, JSON.toJSONString(result));
        return result;
    }
}
