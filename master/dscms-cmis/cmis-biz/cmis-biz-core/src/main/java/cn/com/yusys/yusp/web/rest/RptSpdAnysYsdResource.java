/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.RptBasicInfoPersFamilyAssets;
import cn.com.yusys.yusp.domain.RptSpdAnysJxd;
import cn.com.yusys.yusp.dto.PfkInfoDto;
import cn.com.yusys.yusp.dto.PfkInfosDto;
import cn.com.yusys.yusp.service.RptBasicInfoPersFamilyAssetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysYsd;
import cn.com.yusys.yusp.service.RptSpdAnysYsdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysYsdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-24 15:11:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanysysd")
public class RptSpdAnysYsdResource {
    @Autowired
    private RptSpdAnysYsdService rptSpdAnysYsdService;


	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysYsd>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysYsd> list = rptSpdAnysYsdService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysYsd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysYsd>> index(QueryModel queryModel) {
        List<RptSpdAnysYsd> list = rptSpdAnysYsdService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysYsd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysYsd> show(@PathVariable("serno") String serno) {
        RptSpdAnysYsd rptSpdAnysYsd = rptSpdAnysYsdService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysYsd>(rptSpdAnysYsd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysYsd> create(@RequestBody RptSpdAnysYsd rptSpdAnysYsd) throws URISyntaxException {
        rptSpdAnysYsdService.insert(rptSpdAnysYsd);
        return new ResultDto<RptSpdAnysYsd>(rptSpdAnysYsd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysYsd rptSpdAnysYsd) throws URISyntaxException {
        int result = rptSpdAnysYsdService.update(rptSpdAnysYsd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysYsdService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysYsdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据申请流水号查询数据
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysYsd> selectBySerno(@RequestBody String serno) {
        RptSpdAnysYsd rptSpdAnysYsd = rptSpdAnysYsdService.selectByPrimaryKey(serno);
        return  ResultDto.success(rptSpdAnysYsd);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<RptSpdAnysYsd> save(@RequestBody RptSpdAnysYsd rptSpdAnysYsd) throws URISyntaxException {
        RptSpdAnysYsd rptSpdAnysYsd1 = rptSpdAnysYsdService.selectByPrimaryKey(rptSpdAnysYsd.getSerno());
        if(rptSpdAnysYsd1!=null){
            rptSpdAnysYsdService.update(rptSpdAnysYsd);
        }else{
            rptSpdAnysYsdService.insert(rptSpdAnysYsd);
        }
        return ResultDto.success(rptSpdAnysYsd);
    }
    @PostMapping("/initYsd")
    protected ResultDto<Integer> initYsd(@RequestBody RptSpdAnysYsd rptSpdAnysYsd){
        return new ResultDto<Integer>(rptSpdAnysYsdService.insertSelective(rptSpdAnysYsd));
    }

    @PostMapping("/autoValue")
    protected ResultDto<Integer> autoValue(@RequestBody  Map params){
        return new ResultDto<Integer>(rptSpdAnysYsdService.autoValue(params));
    }
}
