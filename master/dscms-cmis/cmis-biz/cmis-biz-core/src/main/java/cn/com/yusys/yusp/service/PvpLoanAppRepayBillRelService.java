/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.dto.RepayBillRelDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PvpLoanAppRepayBillRel;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppRepayBillRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppRepayBillRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 72908
 * @创建时间: 2021-05-06 21:42:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpLoanAppRepayBillRelService {

    @Autowired
    private PvpLoanAppRepayBillRelMapper pvpLoanAppRepayBillRelMapper;

    private static final Logger log = LoggerFactory.getLogger(PvpLoanAppRepayBillRelService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PvpLoanAppRepayBillRel selectByPrimaryKey(String pkId) {
        return pvpLoanAppRepayBillRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PvpLoanAppRepayBillRel> selectAll(QueryModel model) {
        List<PvpLoanAppRepayBillRel> records = (List<PvpLoanAppRepayBillRel>) pvpLoanAppRepayBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PvpLoanAppRepayBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpLoanAppRepayBillRel> list = pvpLoanAppRepayBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PvpLoanAppRepayBillRel record) {
        return pvpLoanAppRepayBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PvpLoanAppRepayBillRel record) {
        return pvpLoanAppRepayBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PvpLoanAppRepayBillRel record) {
        return pvpLoanAppRepayBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PvpLoanAppRepayBillRel record) {
        return pvpLoanAppRepayBillRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pvpLoanAppRepayBillRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByBillNo(String billNo) {
        return pvpLoanAppRepayBillRelMapper.deleteByBillNo(billNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pvpLoanAppRepayBillRelMapper.deleteByIds(ids);
    }

//    /**
//     * @return
//     * @方法名称: selectCtrCont
//     * @方法描述: 偿还借据分页查询
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//    public List<RepayBillRelDto> queryRepayBillRel(HashMap<String, String> queryMap) {
//        //PageHelper.startPage(model.getPage(), model.getSize());
//        List<RepayBillRelDto> ctrList = pvpLoanAppRepayBillRelMapper.selectRepayBillRelByParams(queryMap);
//        //PageHelper.clearPage();
//        return ctrList;
//    }

    public List<RepayBillRelDto> queryRepayBillRel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RepayBillRelDto> ctrList = pvpLoanAppRepayBillRelMapper.selectRepayBillRelByParams(model);
        PageHelper.clearPage();
        return ctrList;
    }

    public List<RepayBillRelDto> queryAllRepayBillRel(QueryModel model) {
        List<RepayBillRelDto> ctrList = pvpLoanAppRepayBillRelMapper.selectRepayBillRelByParams(model);
        return ctrList;
    }

/*    *//**
     * @创建人 zhanyb
     * @创建时间 2021-05-05 15:35
     * @注释 还本计划分页查询
     *//*
    public List<RepayCapPlan> queryRepaycapplan(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        //HashMap<String, String > queyParam = new HashMap<String, String>();
        //model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_000);
        //model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        List<RepayCapPlan> list = pvpLoanAppRepayBillRelMapper.selectRepayBillRelByParams(model);
        PageHelper.clearPage();
        return list;
    }*/

    /**
     *偿还借据,引入台账
     * @param queryMap
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map leadInAccLoan(HashMap queryMap) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            PvpLoanAppRepayBillRel pvpLoanAppRepayBillRel = new PvpLoanAppRepayBillRel();
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                pvpLoanAppRepayBillRel.setInputId(userInfo.getLoginCode());
                pvpLoanAppRepayBillRel.setInputBrId(userInfo.getOrg().getCode());
                pvpLoanAppRepayBillRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                pvpLoanAppRepayBillRel.setUpdId(userInfo.getLoginCode());
                pvpLoanAppRepayBillRel.setUpdBrId(userInfo.getOrg().getCode());
                pvpLoanAppRepayBillRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                pvpLoanAppRepayBillRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                pvpLoanAppRepayBillRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            String pkId = StringUtils.uuid(true);
            pvpLoanAppRepayBillRel.setPkId(pkId);
            pvpLoanAppRepayBillRel.setBillNo((String) queryMap.get("billNo"));//将贷款台账表acc_loan中的“借据编号”赋值给偿还借据关联表中的“借据编号”
            pvpLoanAppRepayBillRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            pvpLoanAppRepayBillRel.setSerno((String)queryMap.get("pvpSerno"));//将贷款台账表acc_loan中的“放款流水号”赋值给偿还借据关联表中的“流水号”，queryMap.get("pvpSerno")是Object类型
            //pvpLoanAppRepayBillRel.setSerno();//流水号
            int insertCount = pvpLoanAppRepayBillRelMapper.insertSelective(pvpLoanAppRepayBillRel);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("pkId",pkId);
            log.info("偿还借据引入台账"+pkId+"-保存成功！");
        }catch(YuspException e){
            log.error("偿还借据引入台账异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("偿还借据引入台账异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: selectByPvpSerno
     * @方法描述: 根据出账申请流水号查询关联的原借据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PvpLoanAppRepayBillRel selectByPvpSerno(String pvpSerno) {
        return pvpLoanAppRepayBillRelMapper.selectByPvpSerno(pvpSerno);
    }
}