package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanProInfo
 * @类描述: coop_plan_pro_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-14 15:31:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CoopPlanProInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键ID **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 项目编号 **/
	private String proNo;

	/** 合作方类型 **/
	private String partnerType;

	/** 合作方编号 **/
	private String partnerNo;

	/** 合作方名称 **/
	private String partnerName;
	
	/** 合作方案编号 **/
	private String coopPlanNo;
	
	/** 项目名称 **/
	private String proName;
	
	/** 项目类型 **/
	private String proType;
	
	/** 项目额度（元） **/
	private java.math.BigDecimal proLmt;
	
	/** 项目状态 **/
	private String proStatus;
	
	/** 是否通过销售公司销售 **/
	private String isSaleCprtSale;
	
	/** 销售公司名称 **/
	private String saleCprtName;
	
	/** 销售公司电话 **/
	private String saleCprtPhone;
	
	/** 负责人名称 **/
	private String chiefName;
	
	/** 负责人电话 **/
	private String chiefPhone;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 项目总投资（元） **/
	private java.math.BigDecimal proTotalInvest;
	
	/** 项目权利人名称 **/
	private String proWrrPersonName;
	
	/** 项目总销售收入(元) **/
	private java.math.BigDecimal proTotalSaleIncome;
	
	/** 其他部分销售总价(元) **/
	private java.math.BigDecimal otherPartSaleTotal;
	
	/** 项目开工时间 **/
	private String proStartDate;
	
	/** 项目竣工时间 **/
	private String proEndDate;
	
	/** 交付日期 **/
	private String deliverDate;
	
	/** 项目地址位置A **/
	private String proAddrPlaceA;
	
	/** 项目地址位置B **/
	private String proAddrPlaceB;
	
	/** 项目地址位置C **/
	private String proAddrPlaceC;
	
	/** 项目具体位置 **/
	private String proDetailPlace;
	
	/** 占地面积（㎡） **/
	private java.math.BigDecimal occupSqu;
	
	/** 住宅建筑面积（㎡） **/
	private java.math.BigDecimal resiArchSqu;
	
	/** 工业建筑面积（㎡） **/
	private java.math.BigDecimal indtArchSqu;
	
	/** 住宅销售均价（元/㎡） **/
	private java.math.BigDecimal resiSaleAvgPrice;
	
	/** 工业销售均价（元/㎡） **/
	private java.math.BigDecimal indtSaleAvgPrice;
	
	/** 总建筑面积（㎡） **/
	private java.math.BigDecimal totlArchSqu;
	
	/** 商业建筑面积（㎡） **/
	private java.math.BigDecimal commArchSqu;
	
	/** 其他建筑面积（㎡） **/
	private java.math.BigDecimal otherArchSqu;
	
	/** 商业销售均价（元/㎡） **/
	private java.math.BigDecimal commSaleAvgPrice;
	
	/** 其他销售均价（元/㎡） **/
	private java.math.BigDecimal otherSaleAvgPrice;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}
	
    /**
     * @return ProNo
     */	
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo == null ? null : coopPlanNo.trim();
	}
	
    /**
     * @return CoopPlanNo
     */	
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName == null ? null : proName.trim();
	}
	
    /**
     * @return ProName
     */	
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param proType
	 */
	public void setProType(String proType) {
		this.proType = proType == null ? null : proType.trim();
	}
	
    /**
     * @return ProType
     */	
	public String getProType() {
		return this.proType;
	}
	
	/**
	 * @param proLmt
	 */
	public void setProLmt(java.math.BigDecimal proLmt) {
		this.proLmt = proLmt;
	}
	
    /**
     * @return ProLmt
     */	
	public java.math.BigDecimal getProLmt() {
		return this.proLmt;
	}
	
	/**
	 * @param proStatus
	 */
	public void setProStatus(String proStatus) {
		this.proStatus = proStatus == null ? null : proStatus.trim();
	}
	
    /**
     * @return ProStatus
     */	
	public String getProStatus() {
		return this.proStatus;
	}
	
	/**
	 * @param isSaleCprtSale
	 */
	public void setIsSaleCprtSale(String isSaleCprtSale) {
		this.isSaleCprtSale = isSaleCprtSale == null ? null : isSaleCprtSale.trim();
	}
	
    /**
     * @return IsSaleCprtSale
     */	
	public String getIsSaleCprtSale() {
		return this.isSaleCprtSale;
	}
	
	/**
	 * @param saleCprtName
	 */
	public void setSaleCprtName(String saleCprtName) {
		this.saleCprtName = saleCprtName == null ? null : saleCprtName.trim();
	}
	
    /**
     * @return SaleCprtName
     */	
	public String getSaleCprtName() {
		return this.saleCprtName;
	}
	
	/**
	 * @param saleCprtPhone
	 */
	public void setSaleCprtPhone(String saleCprtPhone) {
		this.saleCprtPhone = saleCprtPhone == null ? null : saleCprtPhone.trim();
	}
	
    /**
     * @return SaleCprtPhone
     */	
	public String getSaleCprtPhone() {
		return this.saleCprtPhone;
	}
	
	/**
	 * @param chiefName
	 */
	public void setChiefName(String chiefName) {
		this.chiefName = chiefName == null ? null : chiefName.trim();
	}
	
    /**
     * @return ChiefName
     */	
	public String getChiefName() {
		return this.chiefName;
	}
	
	/**
	 * @param chiefPhone
	 */
	public void setChiefPhone(String chiefPhone) {
		this.chiefPhone = chiefPhone == null ? null : chiefPhone.trim();
	}
	
    /**
     * @return ChiefPhone
     */	
	public String getChiefPhone() {
		return this.chiefPhone;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param proTotalInvest
	 */
	public void setProTotalInvest(java.math.BigDecimal proTotalInvest) {
		this.proTotalInvest = proTotalInvest;
	}
	
    /**
     * @return ProTotalInvest
     */	
	public java.math.BigDecimal getProTotalInvest() {
		return this.proTotalInvest;
	}
	
	/**
	 * @param proWrrPersonName
	 */
	public void setProWrrPersonName(String proWrrPersonName) {
		this.proWrrPersonName = proWrrPersonName == null ? null : proWrrPersonName.trim();
	}
	
    /**
     * @return ProWrrPersonName
     */	
	public String getProWrrPersonName() {
		return this.proWrrPersonName;
	}
	
	/**
	 * @param proTotalSaleIncome
	 */
	public void setProTotalSaleIncome(java.math.BigDecimal proTotalSaleIncome) {
		this.proTotalSaleIncome = proTotalSaleIncome;
	}
	
    /**
     * @return ProTotalSaleIncome
     */	
	public java.math.BigDecimal getProTotalSaleIncome() {
		return this.proTotalSaleIncome;
	}
	
	/**
	 * @param otherPartSaleTotal
	 */
	public void setOtherPartSaleTotal(java.math.BigDecimal otherPartSaleTotal) {
		this.otherPartSaleTotal = otherPartSaleTotal;
	}
	
    /**
     * @return OtherPartSaleTotal
     */	
	public java.math.BigDecimal getOtherPartSaleTotal() {
		return this.otherPartSaleTotal;
	}
	
	/**
	 * @param proStartDate
	 */
	public void setProStartDate(String proStartDate) {
		this.proStartDate = proStartDate == null ? null : proStartDate.trim();
	}
	
    /**
     * @return ProStartDate
     */	
	public String getProStartDate() {
		return this.proStartDate;
	}
	
	/**
	 * @param proEndDate
	 */
	public void setProEndDate(String proEndDate) {
		this.proEndDate = proEndDate == null ? null : proEndDate.trim();
	}
	
    /**
     * @return ProEndDate
     */	
	public String getProEndDate() {
		return this.proEndDate;
	}
	
	/**
	 * @param deliverDate
	 */
	public void setDeliverDate(String deliverDate) {
		this.deliverDate = deliverDate == null ? null : deliverDate.trim();
	}
	
    /**
     * @return DeliverDate
     */	
	public String getDeliverDate() {
		return this.deliverDate;
	}
	
	/**
	 * @param proAddrPlaceA
	 */
	public void setProAddrPlaceA(String proAddrPlaceA) {
		this.proAddrPlaceA = proAddrPlaceA == null ? null : proAddrPlaceA.trim();
	}
	
    /**
     * @return ProAddrPlaceA
     */	
	public String getProAddrPlaceA() {
		return this.proAddrPlaceA;
	}
	
	/**
	 * @param proAddrPlaceB
	 */
	public void setProAddrPlaceB(String proAddrPlaceB) {
		this.proAddrPlaceB = proAddrPlaceB == null ? null : proAddrPlaceB.trim();
	}
	
    /**
     * @return ProAddrPlaceB
     */	
	public String getProAddrPlaceB() {
		return this.proAddrPlaceB;
	}
	
	/**
	 * @param proAddrPlaceC
	 */
	public void setProAddrPlaceC(String proAddrPlaceC) {
		this.proAddrPlaceC = proAddrPlaceC == null ? null : proAddrPlaceC.trim();
	}
	
    /**
     * @return ProAddrPlaceC
     */	
	public String getProAddrPlaceC() {
		return this.proAddrPlaceC;
	}
	
	/**
	 * @param proDetailPlace
	 */
	public void setProDetailPlace(String proDetailPlace) {
		this.proDetailPlace = proDetailPlace == null ? null : proDetailPlace.trim();
	}
	
    /**
     * @return ProDetailPlace
     */	
	public String getProDetailPlace() {
		return this.proDetailPlace;
	}
	
	/**
	 * @param occupSqu
	 */
	public void setOccupSqu(java.math.BigDecimal occupSqu) {
		this.occupSqu = occupSqu;
	}
	
    /**
     * @return OccupSqu
     */	
	public java.math.BigDecimal getOccupSqu() {
		return this.occupSqu;
	}
	
	/**
	 * @param resiArchSqu
	 */
	public void setResiArchSqu(java.math.BigDecimal resiArchSqu) {
		this.resiArchSqu = resiArchSqu;
	}
	
    /**
     * @return ResiArchSqu
     */	
	public java.math.BigDecimal getResiArchSqu() {
		return this.resiArchSqu;
	}
	
	/**
	 * @param indtArchSqu
	 */
	public void setIndtArchSqu(java.math.BigDecimal indtArchSqu) {
		this.indtArchSqu = indtArchSqu;
	}
	
    /**
     * @return IndtArchSqu
     */	
	public java.math.BigDecimal getIndtArchSqu() {
		return this.indtArchSqu;
	}
	
	/**
	 * @param resiSaleAvgPrice
	 */
	public void setResiSaleAvgPrice(java.math.BigDecimal resiSaleAvgPrice) {
		this.resiSaleAvgPrice = resiSaleAvgPrice;
	}
	
    /**
     * @return ResiSaleAvgPrice
     */	
	public java.math.BigDecimal getResiSaleAvgPrice() {
		return this.resiSaleAvgPrice;
	}
	
	/**
	 * @param indtSaleAvgPrice
	 */
	public void setIndtSaleAvgPrice(java.math.BigDecimal indtSaleAvgPrice) {
		this.indtSaleAvgPrice = indtSaleAvgPrice;
	}
	
    /**
     * @return IndtSaleAvgPrice
     */	
	public java.math.BigDecimal getIndtSaleAvgPrice() {
		return this.indtSaleAvgPrice;
	}
	
	/**
	 * @param totlArchSqu
	 */
	public void setTotlArchSqu(java.math.BigDecimal totlArchSqu) {
		this.totlArchSqu = totlArchSqu;
	}
	
    /**
     * @return TotlArchSqu
     */	
	public java.math.BigDecimal getTotlArchSqu() {
		return this.totlArchSqu;
	}
	
	/**
	 * @param commArchSqu
	 */
	public void setCommArchSqu(java.math.BigDecimal commArchSqu) {
		this.commArchSqu = commArchSqu;
	}
	
    /**
     * @return CommArchSqu
     */	
	public java.math.BigDecimal getCommArchSqu() {
		return this.commArchSqu;
	}
	
	/**
	 * @param otherArchSqu
	 */
	public void setOtherArchSqu(java.math.BigDecimal otherArchSqu) {
		this.otherArchSqu = otherArchSqu;
	}
	
    /**
     * @return OtherArchSqu
     */	
	public java.math.BigDecimal getOtherArchSqu() {
		return this.otherArchSqu;
	}
	
	/**
	 * @param commSaleAvgPrice
	 */
	public void setCommSaleAvgPrice(java.math.BigDecimal commSaleAvgPrice) {
		this.commSaleAvgPrice = commSaleAvgPrice;
	}
	
    /**
     * @return CommSaleAvgPrice
     */	
	public java.math.BigDecimal getCommSaleAvgPrice() {
		return this.commSaleAvgPrice;
	}
	
	/**
	 * @param otherSaleAvgPrice
	 */
	public void setOtherSaleAvgPrice(java.math.BigDecimal otherSaleAvgPrice) {
		this.otherSaleAvgPrice = otherSaleAvgPrice;
	}
	
    /**
     * @return OtherSaleAvgPrice
     */	
	public java.math.BigDecimal getOtherSaleAvgPrice() {
		return this.otherSaleAvgPrice;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getPartnerNo() {
		return partnerNo;
	}

	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
}