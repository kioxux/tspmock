package cn.com.yusys.yusp.vo;

import javax.persistence.Column;

import cn.com.yusys.yusp.domain.OtherCnyRateApprSub;

/**
 * OtherCnyRateApprSub扩展实体类
 * @author qiantj
 *
 */
public class OtherCnyRateApprSubVo extends OtherCnyRateApprSub{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** 额度分项编号 **/
	private String subAccNo;
	
	/** 贷款品种 **/
	private String loanType;
	
	/** 贷款期限 **/
	private Integer loanTerm;
	
	/** 贷款金额 **/
	private java.math.BigDecimal loanAmt;
	
	/** 担保方式 **/
	private String guarType;

	public String getSubAccNo() {
		return subAccNo;
	}

	public void setSubAccNo(String subAccNo) {
		this.subAccNo = subAccNo;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public Integer getLoanTerm() {
		return loanTerm;
	}

	public void setLoanTerm(Integer loanTerm) {
		this.loanTerm = loanTerm;
	}

	public java.math.BigDecimal getLoanAmt() {
		return loanAmt;
	}

	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}

	public String getGuarType() {
		return guarType;
	}

	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
}
