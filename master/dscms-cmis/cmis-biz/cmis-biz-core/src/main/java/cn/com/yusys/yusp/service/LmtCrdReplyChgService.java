/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.CusUpdateInitLoanDateDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.GrtGuarBizRstRelMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyChgMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCrdReplyChgService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-08-24 11:20:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCrdReplyChgService {
    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppService.class);
    @Autowired
    private LmtCrdReplyChgMapper lmtCrdReplyChgMapper;
    @Autowired
    private  ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;
    @Autowired
    private  CtrLoanContService ctrLoanContService;
    @Autowired
    private GrtGuarContMapper grtGuarContMapper;
    @Autowired
    private GuarBusinessRelService guarBusinessRelService;
    @Resource
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private  CmisLmtClientService cmisLmtClientService;
    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private  IqpHouseService iqpHouseService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private GrtGuarContRelService grtGuarContRelService;
    @Autowired
    private GrtGuarContService grtGuarContService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtCrdReplyChg selectByPrimaryKey(String pkId, String replyNo) {
        return lmtCrdReplyChgMapper.selectByPrimaryKey(pkId, replyNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtCrdReplyChg> selectAll(QueryModel model) {
        List<LmtCrdReplyChg> records = (List<LmtCrdReplyChg>) lmtCrdReplyChgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtCrdReplyChg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtCrdReplyChg> list = lmtCrdReplyChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtCrdReplyChg record) {
        return lmtCrdReplyChgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtCrdReplyChg record) {
        return lmtCrdReplyChgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtCrdReplyChg record) {
        return lmtCrdReplyChgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtCrdReplyChg record) {
        return lmtCrdReplyChgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String replyNo) {
        return lmtCrdReplyChgMapper.deleteByPrimaryKey(pkId, replyNo);
    }
    /**
     * @param  lmtCrdReplyChg
     * @return
     * @author wzy
     * @date 2021/8/24 14:09
     * @version 1.0.0
     * @desc  新增
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public LmtCrdReplyChg saveOrUpdate(LmtCrdReplyChg lmtCrdReplyChg) {
        String pkId = lmtCrdReplyChg.getPkId();
        LmtCrdReplyChg record = selectByPrimaryKey(lmtCrdReplyChg.getPkId(),lmtCrdReplyChg.getReplyNo());
        if(record == null){
            lmtCrdReplyChg.setApproveStatus("000");
            lmtCrdReplyChg.setPkId(UUID.randomUUID().toString());
            int result = insertSelective(lmtCrdReplyChg);
            if(result !=1){
                throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, "审批信息新增异常！");
            }
        }else{
            int result = updateSelective(lmtCrdReplyChg);
            if(result !=1){
                throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, "审批信息新增异常！");
            }
        }
        return  lmtCrdReplyChg;
    }
    /**
     * @param  replyNo
     * @return
     * @author wzy
     * @date 2021/8/24 14:09
     * @version 1.0.0
     * @desc  查询最新待发起变更
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public LmtCrdReplyChg selectNew(String replyNo) {
        Map map = new HashMap();
        map.put("replyNo",replyNo);
        return lmtCrdReplyChgMapper.selectNew(map);
    }
    /**
     * @param  replyNo
     * @return
     * @author wzy
     * @date 2021/8/24 14:09
     * @version 1.0.0
     * @desc  查询最新待发起变更
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public LmtCrdReplyChg selectWaitSubmit(String replyNo) {
        Map map = new HashMap();
        map.put("replyNo",replyNo);
        return lmtCrdReplyChgMapper.selectWaitSubmit(map);
    }
    /**
     * @param  replyNo
     * @return
     * @author wzy
     * @date 2021/8/24 14:09
     * @version 1.0.0
     * @desc  查询是否存在在途的流程
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public LmtCrdReplyChg selectRun(String replyNo) {
        Map map = new HashMap();
        map.put("replyNo",replyNo);
        return lmtCrdReplyChgMapper.selectRun(map);
    }

    @Transactional(rollbackFor = Exception.class)
    public void handleBizEnd(String pkId, String replyNo) throws Exception {
        log.info("零售批复变更业务申请审批结束后业务处理逻辑开始【{}】", pkId);
        // 批复信息实体类
        LmtCrdReplyInfo lmtCrdReplyInfo = null;
        // 合同主表实体类
        CtrLoanCont ctrLoanCont = null;
        // 合同流水号
        String contNo = null;
        // 临时变量
        int nums;
        try {
            Map map = new HashMap();
            map.put("replyNo",replyNo);
            LmtCrdReplyChg lmtCrdReplyChg = lmtCrdReplyChgMapper.selectNew(map);
            String iqpSerno = lmtCrdReplyChg.getIqpSerno();
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
            iqpLoanApp.setAppAmt(lmtCrdReplyChg.getReplyAmtChg());
            iqpLoanApp.setContAmt(lmtCrdReplyChg.getReplyAmtChg());
            iqpLoanApp.setAppTerm(new BigDecimal(lmtCrdReplyChg.getReplyTermChg()));
            iqpLoanApp.setExecRateYear(lmtCrdReplyChg.getReplyRateChg());
            iqpLoanApp.setGuarWay(lmtCrdReplyChg.getGuarModeChg());
            iqpLoanApp.setRepayMode(lmtCrdReplyChg.getRepayModeChg());
            iqpLoanApp.setEndDate(DateUtils.addMonth(iqpLoanApp.getStartDate(), "yyyy-MM-dd", iqpLoanApp.getAppTerm().intValue()));
            // 1、生成批复信息
            lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(replyNo);
            lmtCrdReplyInfo.setAppTerm(lmtCrdReplyChg.getReplyTermChg());
            lmtCrdReplyInfo.setReplyAmt(lmtCrdReplyChg.getReplyAmtChg());
            lmtCrdReplyInfo.setExecRateYear(lmtCrdReplyChg.getReplyRateChg());
            lmtCrdReplyInfo.setGuarMode(lmtCrdReplyChg.getGuarModeChg());
            lmtCrdReplyInfo.setLoanCond(lmtCrdReplyChg.getLoanCondChg());
            lmtCrdReplyInfo.setRiskAdvice(lmtCrdReplyChg.getRiskAdviceChg());
            nums = lmtCrdReplyInfoService.updateSelective(lmtCrdReplyInfo);
            Asserts.isTrue(nums > 0, "修改批复信息出错！");
            // 2、前往额度系统建立额度
            // 组装额度系统报文
            CmisLmt0001ReqDto cmisLmt0001ReqDto = new CmisLmt0001ReqDto();
            cmisLmt0001ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);
            cmisLmt0001ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
            cmisLmt0001ReqDto.setAccNo(iqpLoanApp.getIqpSerno());
            cmisLmt0001ReqDto.setCurType(lmtCrdReplyInfo.getCurType());
            cmisLmt0001ReqDto.setCusId(lmtCrdReplyInfo.getCusId());
            cmisLmt0001ReqDto.setCusName(lmtCrdReplyInfo.getCusName());
            cmisLmt0001ReqDto.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG1); // 个人客户
            cmisLmt0001ReqDto.setIsCreateAcc("0");
            cmisLmt0001ReqDto.setLmtAmt(lmtCrdReplyInfo.getReplyAmt());
            cmisLmt0001ReqDto.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_02);
            cmisLmt0001ReqDto.setTerm(lmtCrdReplyInfo.getAppTerm());
            cmisLmt0001ReqDto.setStartDate(lmtCrdReplyInfo.getReplyStartDate());
            cmisLmt0001ReqDto.setEndDate(lmtCrdReplyInfo.getReplyEndDate());
            cmisLmt0001ReqDto.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            cmisLmt0001ReqDto.setManagerId(iqpLoanApp.getManagerId());
            cmisLmt0001ReqDto.setManagerBrId(iqpLoanApp.getManagerBrId());
            cmisLmt0001ReqDto.setInputId(iqpLoanApp.getInputId());
            cmisLmt0001ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
            cmisLmt0001ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));

            List<CmisLmt0001LmtSubListReqDto> CmisLmt0001LmtSubListReqDtoList = new ArrayList<CmisLmt0001LmtSubListReqDto>();
            CmisLmt0001LmtSubListReqDto cmisLmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
            cmisLmt0001LmtSubListReqDto.setAccSubNo(lmtCrdReplyInfo.getReplySerno());
            cmisLmt0001LmtSubListReqDto.setOrigiAccSubNo("");
            cmisLmt0001LmtSubListReqDto.setParentId(iqpLoanApp.getIqpSerno());
            cmisLmt0001LmtSubListReqDto.setLimitSubNo(lmtCrdReplyInfo.getPrdId());
            cmisLmt0001LmtSubListReqDto.setLimitSubName(lmtCrdReplyInfo.getPrdName());
            cmisLmt0001LmtSubListReqDto.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02);
            cmisLmt0001LmtSubListReqDto.setAvlamt(cmisLmt0001ReqDto.getLmtAmt());
            cmisLmt0001LmtSubListReqDto.setCurType(cmisLmt0001ReqDto.getCurType());
            cmisLmt0001LmtSubListReqDto.setTerm(cmisLmt0001ReqDto.getTerm());
            cmisLmt0001LmtSubListReqDto.setStartDate(cmisLmt0001ReqDto.getStartDate());
            cmisLmt0001LmtSubListReqDto.setEndDate(cmisLmt0001ReqDto.getEndDate());
            cmisLmt0001LmtSubListReqDto.setBailPreRate(iqpLoanApp.getBailPerc());
            cmisLmt0001LmtSubListReqDto.setRateYear(lmtCrdReplyInfo.getExecRateYear());
            cmisLmt0001LmtSubListReqDto.setSuitGuarWay(lmtCrdReplyInfo.getGuarMode());
            String graper = iqpLoanApp.getGraper();
            cmisLmt0001LmtSubListReqDto.setLmtGraper((graper == null || "".equals(graper)) ? 0 : Integer.parseInt(graper));
            cmisLmt0001LmtSubListReqDto.setIsRevolv("0");
            cmisLmt0001LmtSubListReqDto.setIsPreCrd("0");
            cmisLmt0001LmtSubListReqDto.setIsLriskLmt("0");
            cmisLmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            cmisLmt0001LmtSubListReqDto.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            CmisLmt0001LmtSubListReqDtoList.add(cmisLmt0001LmtSubListReqDto);
            cmisLmt0001ReqDto.setLmtSubList(CmisLmt0001LmtSubListReqDtoList);

            // 调用额度接口
            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-建立额度请求报文：" + cmisLmt0001ReqDto.toString());
            ResultDto<CmisLmt0001RespDto> cmisLmt0001RespDto = cmisLmtClientService.cmisLmt0001(cmisLmt0001ReqDto);
            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-建立额度返回报文：" + cmisLmt0001RespDto.toString());


            if (cmisLmt0001RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0001RespDto.getData().getErrorCode())) {
                log.info("根据业务申请编号【{}】,前往额度系统建立额度成功！", iqpLoanApp.getIqpSerno());
            } else {
                log.info("根据业务申请编号【{}】,前往额度系统建立额度失败！", iqpLoanApp.getIqpSerno());
                throw new Exception("额度系统-前往额度系统建立额度失败:" + cmisLmt0001RespDto.getData().getErrorMsg());
            }
            //3. 授信申请更新批复编号
            iqpLoanApp.setReplyNo(lmtCrdReplyInfo.getReplySerno());
            // 4、生成合同主表信息
            ctrLoanCont =ctrLoanContService.selectContByIqpSerno(iqpSerno);
            contNo = ctrLoanCont.getContNo();
            // 生成合同流水号
            HashMap<String, String> param = new HashMap<>();
            param.put("bizType", iqpLoanApp.getBelgLine());
            ctrLoanCont.setContAmt(lmtCrdReplyChg.getReplyAmtChg());
            ctrLoanCont.setTeam(lmtCrdReplyChg.getReplyTermChg().toString());
            ctrLoanCont.setExecRateYear(lmtCrdReplyChg.getReplyRateChg());
            ctrLoanCont.setGuarWay(lmtCrdReplyChg.getGuarModeChg());
            ctrLoanCont.setRepayMode(lmtCrdReplyChg.getRepayModeChg());
            nums = ctrLoanContService.updateSelective(ctrLoanCont);
            Asserts.isTrue(nums > 0, "修改合同主表信息出错！");
            //根据合同编号查询担保合同
            List<GrtGuarCont> grtGuarContList = grtGuarContService.selectDataBySerno(iqpSerno);
            if (grtGuarContList == null || grtGuarContList.size() == 0) {
                grtGuarContList = grtGuarContMapper.selectDataByContNo(ctrLoanCont.getContNo());
            }
            for (int i = 0; i < grtGuarContList.size(); i++) {
                GrtGuarCont grtguarcont = grtGuarContList.get(i);
                if ("0".equals(grtguarcont.getIsSuperaddGuar())) {//担保合同类型
                    grtguarcont.setGuarAmt(lmtCrdReplyChg.getReplyAmtChg());
                    if(StringUtils.isEmpty(grtguarcont.getBizLine())){
                        log.error("担保合同【"+grtguarcont.getGuarContNo()+"】的业务条线为空");
                    }
                    grtGuarContMapper.updateByPrimaryKey(grtguarcont);
                }
            }
            ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(iqpLoanApp.getPrdId());
            String prdType = CfgPrdBasicinfoDto.getData().getPrdType();
            if("10".equals(prdType)){//零售按揭
                if ("1".equals(iqpLoanApp.getIsOutstndTrdLmtAmt())) {
                    //这里是对提交时第三方占用做覆盖
                    String proSerno = iqpLoanApp.getTdpAgrNo();//项目流水号
                    //1. 向额度系统发送接口：校验额度
                    CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
                    cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0009ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);//金融机构代码
                    cmisLmt0009ReqDto.setDealBizNo(iqpLoanApp.getIqpSerno());//合同编号
                    cmisLmt0009ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
                    cmisLmt0009ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
                    cmisLmt0009ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
                    cmisLmt0009ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
                    cmisLmt0009ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
                    cmisLmt0009ReqDto.setIsLriskBiz("0");//是否低风险
                    cmisLmt0009ReqDto.setIsFollowBiz("0");//是否无缝衔接
                    cmisLmt0009ReqDto.setIsBizRev("0");//是否合同重签
                    cmisLmt0009ReqDto.setBizAttr("1");//交易属性
                    cmisLmt0009ReqDto.setDealBizAmt(iqpLoanApp.getAppAmt());//交易业务金额
                    cmisLmt0009ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
                    cmisLmt0009ReqDto.setStartDate(iqpLoanApp.getStartDate());//合同起始日
                    cmisLmt0009ReqDto.setEndDate(iqpLoanApp.getEndDate());//合同到期日
                    List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
                    CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
                    cmisLmt0009OccRelListReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_03);//额度类型
                    cmisLmt0009OccRelListReqDto.setLmtSubNo(proSerno);//额度分项编号
                    cmisLmt0009OccRelListReqDto.setBizTotalAmt(iqpLoanApp.getAppAmt());//占用总额(折人民币)
                    cmisLmt0009OccRelListReqDto.setBizSpacAmt(iqpLoanApp.getAppAmt());//占用敞口(折人民币)
                    cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
                    cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);

                    log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度校验请求报文："+ cmisLmt0009ReqDto.toString());
                    ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
                    log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度校验返回报文："+ cmisLmt0009RespDto.toString());

                    String code =  cmisLmt0009RespDto.getData().getErrorCode();

                    if("0000".equals(code)){
                        log.info("根据业务申请编号【{}】,前往额度系统-第三方额度校验成功！", iqpLoanApp.getIqpSerno());
                        //2. 向额度系统发送接口：占用额度
                        CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                        cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                        cmisLmt0011ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);//金融机构代码
                        cmisLmt0011ReqDto.setDealBizNo(iqpLoanApp.getIqpSerno());//业务流水号
                        cmisLmt0011ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
                        cmisLmt0011ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
                        cmisLmt0011ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
                        cmisLmt0011ReqDto.setBizAttr("1");//交易属性
                        cmisLmt0011ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
                        cmisLmt0011ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
                        cmisLmt0011ReqDto.setIsLriskBiz("0");//是否低风险
                        cmisLmt0011ReqDto.setIsFollowBiz("0");//是否无缝衔接
                        cmisLmt0011ReqDto.setIsBizRev("0");//是否合同重签
                        cmisLmt0011ReqDto.setDealBizAmt(iqpLoanApp.getAppAmt());//交易业务金额
                        cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
                        cmisLmt0011ReqDto.setStartDate(iqpLoanApp.getStartDate());//合同起始日
                        cmisLmt0011ReqDto.setEndDate(iqpLoanApp.getEndDate());//合同到期日
                        cmisLmt0011ReqDto.setDealBizStatus("200");//合同状态
                        cmisLmt0011ReqDto.setInputId(iqpLoanApp.getInputId());//登记人
                        cmisLmt0011ReqDto.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
                        cmisLmt0011ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期

                        List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
                        CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                        cmisLmt0011OccRelListDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_03);//额度类型
                        cmisLmt0011OccRelListDto.setLmtSubNo(proSerno);//额度分项编号
                        cmisLmt0011OccRelListDto.setBizTotalAmt(iqpLoanApp.getAppAmt());//占用总额(折人民币)
                        cmisLmt0011OccRelListDto.setBizSpacAmt(iqpLoanApp.getAppAmt());//占用敞口(折人民币)
                        cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpLoanApp.getAppAmt());//占用敞口(折人民币)
                        cmisLmt0011OccRelListDto.setBizSpacAmtCny(iqpLoanApp.getAppAmt());//占用敞口(折人民币)
                        cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
                        cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                        log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度占用请求报文："+ cmisLmt0011ReqDto.toString());
                        ResultDto<CmisLmt0011RespDto> cmisLmt0011RespDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                        log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度占用返回报文："+ cmisLmt0011RespDto.toString());
                        code =  cmisLmt0011RespDto.getData().getErrorCode();

                        if("0000".equals(code)){
                            log.info("根据业务申请编号【{}】,前往额度系统-第三方额度占用成功！", iqpLoanApp.getIqpSerno());
                            //调额度接口：更新第三方起始到期日
                            CmisLmt0029ReqDto cmisLmt0029ReqDto = new CmisLmt0029ReqDto();
                            cmisLmt0029ReqDto.setDealBizNo(iqpLoanApp.getIqpSerno());
                            cmisLmt0029ReqDto.setOriginAccNo(iqpLoanApp.getIqpSerno());
                            cmisLmt0029ReqDto.setNewAccNo(contNo);
                            cmisLmt0029ReqDto.setStartDate(iqpLoanApp.getStartDate());
                            cmisLmt0029ReqDto.setEndDate(iqpLoanApp.getEndDate());
                            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-更新第三方起始到期日请求报文：" + cmisLmt0029ReqDto.toString());
                            ResultDto<CmisLmt0029RespDto> cmisLmt0029RespDto = cmisLmtClientService.cmislmt0029(cmisLmt0029ReqDto);
                            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-更新第三方起始到期日返回报文：" + cmisLmt0029RespDto.toString());

                            if (cmisLmt0029RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0029RespDto.getData().getErrorCode())) {
                                log.info("根据业务申请编号【{}】,前往额度系统更新第三方起始到期日成功！", iqpLoanApp.getIqpSerno());
                            } else {
                                log.info("根据业务申请编号【{}】,前往额度系统更新第三方起始到期日失败！", iqpLoanApp.getIqpSerno());
                            }
                        } else {
                            log.info("根据业务申请编号【{}】,前往额度系统-第三方额度占用异常！", iqpLoanApp.getIqpSerno());
                            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "额度系统-第三方额度占用异常:"+cmisLmt0011RespDto.getData().getErrorMsg());
                        }
                    } else if(!"0000".equals(code) && !"70125".equals(code)){
                        log.info("根据业务申请编号【{}】,前往额度系统-第三方额度校验失败！", iqpLoanApp.getIqpSerno());
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "额度系统-第三方额度校验失败:"+cmisLmt0009RespDto.getData().getErrorMsg());
                    }
                }
            }
            //调外部接口同步第一次授信时间
            CusUpdateInitLoanDateDto cusUpdateInitLoanDateDto = new CusUpdateInitLoanDateDto();
            cusUpdateInitLoanDateDto.setCusId(iqpLoanApp.getCusId());
            cusUpdateInitLoanDateDto.setInitLoanDate(LocalDate.now().toString());
            try {
                log.info("调用客户同步首次建立信贷关系时间开始，请求信息"+cusUpdateInitLoanDateDto.toString());
                ResultDto<Integer> integerResultDto = iCusClientService.updateInitLoanDate(cusUpdateInitLoanDateDto);
                log.info("调用客户同步首次建立信贷关系时间结束，响应信息"+integerResultDto);
            }catch (Exception e){
                log.info("调用客户同步首次建立信贷关系时间失败"+ e);
            }
            if(!"00".equals(iqpLoanApp.getGuarWay())){
                try {
                    log.info("调用信贷授信协议信息同步开始");
                    guarBusinessRelService.sendLmtinf("02",iqpLoanApp.getIqpSerno());
                    log.info("调用信贷授信协议信息同步结束");
                }catch (Exception e){
                    log.info("调用信贷授信协议信息同步失败"+ e);
                }
                try {
                    log.info("调用根据业务流水号同步押品与业务关联信息开始");
                    guarBusinessRelService.sendBuscon(iqpLoanApp.getIqpSerno(),"01");
                    log.info("调用根据业务流水号同步押品与业务关联信息结束");
                }catch (Exception e){
                    log.info("调用根据业务流水号同步押品与业务关联信息失败"+ e);
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
        log.info("零售批复变更业务申请审批结束后业务处理逻辑结束【{}】", pkId);
    }
}
