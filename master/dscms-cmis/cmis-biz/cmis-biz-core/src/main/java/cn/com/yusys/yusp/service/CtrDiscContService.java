/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14.Xdpj14ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14.Xdpj14RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccDiscMapper;
import cn.com.yusys.yusp.repository.mapper.CtrDiscContMapper;
import cn.com.yusys.yusp.repository.mapper.IqpDiscAppMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrDiscContService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-13 09:06:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CtrDiscContService {
    private static final Logger log = LoggerFactory.getLogger(CtrDiscContService.class);
    @Autowired
    private CtrDiscContMapper ctrDiscContMapper;
    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;
    @Autowired
    private AccDiscMapper accDiscMapper;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private GuarBusinessRelService guarBusinessRelService;

    @Autowired
    private IqpDiscAppMapper iqpDiscAppMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;

    @Autowired
    private AccDiscService accDiscService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CtrDiscCont selectByPrimaryKey(String pkId) {
        return ctrDiscContMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CtrDiscCont> selectAll(QueryModel model) {
        List<CtrDiscCont> records = (List<CtrDiscCont>) ctrDiscContMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CtrDiscCont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrDiscCont> list = ctrDiscContMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CtrDiscCont record) {
        return ctrDiscContMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public int insertSelective(CtrDiscCont record) {
        return ctrDiscContMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CtrDiscCont record) {
        return ctrDiscContMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public int  updateSelective(CtrDiscCont record) {
        return ctrDiscContMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateLmtAccNoByContNo
     * @方法描述: 根据合同号更新lmtAccNo
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateLmtAccNoByContNo(CtrDiscCont record) {
        return ctrDiscContMapper.updateLmtAccNoByContNo(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return ctrDiscContMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return ctrDiscContMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: onSign
     * @方法描述: 贴现协议签订
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int onSign(CtrDiscCont ctrDiscCont) {
        ctrDiscCont.setContStatus(CmisCommonConstants.CONT_STATUS_200);
        int count = ctrDiscContMapper.updateByPrimaryKey(ctrDiscCont);
        // 押品系统
        // TODO guarBusinessRelService.sendBuscon(ctrDiscCont.getSerno(), CmisCommonConstants.STD_BUSI_TYPE_02);
        // 发送票据系统，一般贴现协议不需要发票据系统
        if(!CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(ctrDiscCont.getDiscContType())){
            Xdpj14ReqDto reqDto = new Xdpj14ReqDto();
            reqDto.setManageBrId(ctrDiscCont.getManagerId());//管理机构号
            reqDto.setCustName(ctrDiscCont.getCusName());//客户名称
            reqDto.setDiscountNo(ctrDiscCont.getContNo());//协议编号
            reqDto.setPhoneNo(ctrDiscCont.getPhone());//电话
            if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(ctrDiscCont.getDiscContType())){
                reqDto.setSecondDisBalance(ctrDiscCont.getDrftTotalAmt());//秒贴额度
                reqDto.setTotalDisBalance(ctrDiscCont.getDrftTotalAmt());//总额度
                reqDto.setTotalOnlineBalance(ctrDiscCont.getDrftTotalAmt());//总线上额度
            }else {
                reqDto.setSecondDisBalance(ctrDiscCont.getContAmt());//秒贴额度
                reqDto.setTotalDisBalance(ctrDiscCont.getContAmt());//总额度
                reqDto.setTotalOnlineBalance(ctrDiscCont.getContAmt());//总线上额度
            }
            reqDto.setCustNo(ctrDiscCont.getCusId());//客户号
            reqDto.setStartDate(ctrDiscCont.getStartDate().replace("-", ""));//起始日期
            reqDto.setEndDate(ctrDiscCont.getEndDate().replace("-", ""));//终止日期
            reqDto.setType("0");//0:新增 1:修改 2:注销 3:校验
            log.info("贴现协议【{}】信贷签约通知，发送票据系统签约通知开始,请求报文为:【{}】", ctrDiscCont.getContNo(), reqDto.toString());
            ResultDto<Xdpj14RespDto> resultDto =  dscms2PjxtClientService.xdpj14(reqDto);
            log.info("贴现协议【{}】信贷签约通知，发送票据系统签约通知结束,响应报文为:【{}】", ctrDiscCont.getContNo(), resultDto.toString());
            if (!SuccessEnum.CMIS_SUCCSESS.key.equals(resultDto.getCode())) {
                log.error("贴现协议【{}】信贷签约通知，发送票据系统签约通知异常",ctrDiscCont.getContNo());
                throw BizException.error(null, resultDto.getCode(), resultDto.getMessage());
            }
        }
        // 判断是否为合同续签,若为续签合同则恢复原合同的占额
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(ctrDiscCont.getIsRenew())){
            CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
            cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrDiscCont.getManagerBrId()));//金融机构代码
            cmisLmt0012ReqDto.setBizNo(ctrDiscCont.getOrigiContNo());//合同编号
            cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_08);//恢复类型
            cmisLmt0012ReqDto.setRecoverAmtCny(BigDecimal.ZERO);//恢复金额
            cmisLmt0012ReqDto.setRecoverSpacAmtCny(BigDecimal.ZERO);//恢复敞口金额
            cmisLmt0012ReqDto.setInputId(ctrDiscCont.getInputId());//登记人
            cmisLmt0012ReqDto.setInputBrId(ctrDiscCont.getInputBrId());//登记机构
            cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
            log.info("贴现协议【{}】前往额度系统恢复原合同【{}】额度开始,请求报文为：【{}】",ctrDiscCont.getContNo(), ctrDiscCont.getOrigiContNo(), JSON.toJSONString(cmisLmt0012ReqDto));
            ResultDto<CmisLmt0012RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
            log.info("贴现协议【{}】前往额度系统恢复原合同【{}】额度结束,响应报文为：【{}】",ctrDiscCont.getContNo(), ctrDiscCont.getOrigiContNo(),JSON.toJSONString(resultDtoDto));
            if (!"0".equals(resultDtoDto.getCode())) {
                log.error("贴现协议【{}】签订异常",ctrDiscCont.getContNo());
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if (!"0000".equals(code)) {
                log.error("恢复原合同额度异常！" + resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null, code, resultDtoDto.getData().getErrorMsg());
            }
        }

        // 生成归档任务 只有最高额贴现协议生成
        if(count>0 && CmisCommonConstants.STD_DISC_CONT_TYPE_02.equals(ctrDiscCont.getDiscContType())){
            log.info("开始系统生成最高额贴现协议档案归档信息");
            String cusId = ctrDiscCont.getCusId();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
            docArchiveClientDto.setArchiveMode("02");
            docArchiveClientDto.setDocClass("03");
            docArchiveClientDto.setDocType("09");
            docArchiveClientDto.setBizSerno(ctrDiscCont.getSerno());
            docArchiveClientDto.setCusId(cusId);
            docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
            docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
            docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
            docArchiveClientDto.setManagerId(ctrDiscCont.getManagerId());
            docArchiveClientDto.setManagerBrId(ctrDiscCont.getManagerBrId());
            docArchiveClientDto.setInputId(ctrDiscCont.getInputId());
            docArchiveClientDto.setInputBrId(ctrDiscCont.getInputBrId());
            docArchiveClientDto.setContNo(ctrDiscCont.getContNo());
            docArchiveClientDto.setLoanAmt(ctrDiscCont.getDrftTotalAmt());
            docArchiveClientDto.setStartDate(ctrDiscCont.getStartDate());
            docArchiveClientDto.setEndDate(ctrDiscCont.getEndDate());
            docArchiveClientDto.setPrdId(ctrDiscCont.getPrdId());
            docArchiveClientDto.setPrdName(ctrDiscCont.getPrdName());
            int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
            if(num < 1){
                log.error("系统生成最高额贴现协议档案归档信息失败");
            }
        }
        return count;
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询未生效数据(列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrDiscCont> toSignlist(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_100);
        return ctrDiscContMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询除了合同状态为未生效数据(历史列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrDiscCont> doneSignlist(QueryModel model) {
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contStatusOther",CmisCommonConstants.CONT_STATUS_OTHER);
        return ctrDiscContMapper.selectByModel(model);
    }

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrDiscCont> selectByLmtAccNo(String lmtAccNo) {
        return ctrDiscContMapper.selectByLmtAccNo(lmtAccNo);
    }


    /**
     * @方法名称：selectByIqpSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 14:21
     * @修改记录：修改时间 修改人员 修改时间
    */
    public CtrDiscCont selectByIqpSerno(String serno){
        return ctrDiscContMapper.selectByIqpSerno(serno);
    }

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 14:21
     * @修改记录：修改时间 修改人员 修改时间
     */
    public CtrDiscCont selectByContNo(String contNo){
        return ctrDiscContMapper.selectByContNo(contNo);
    }

    /**
     * @方法名称: 贴现协议注销
     * @方法描述: 根据合同号更新签订状态和时间, 包含合同项下的担保合同签订
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map onLogOut(@RequestBody Map params) throws Exception {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        String contNo = "";
        String contStatus = "";
        int pvpLoanAppCount = 0;
        int accLoanCount = 0;
        try {
            contNo = (String) params.get("contNo");//主键合同编号
            contStatus = (String) params.get("contStatus");//合同状态
            CtrDiscCont ctrDiscCont = ctrDiscContMapper.selectByContNo(contNo);
            // 判断合同下是否存在未结清的业务
            String responseResult = ctrLoanContService.isContUncleared(contNo);
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(responseResult)){
                rtnCode = EcbEnum.ECB020055.key;
                rtnMsg = EcbEnum.ECB020055.value;
                return result;
            }

            // TODO: 2021/10/4 贴现协议会出现协议到期日为空的情况，如何获取注销后的合同状态需要确认，暂时先置为注销状态
            // 获取合同注销后的合同状态
            String finalContStatus = "";
            if(!StringUtils.isBlank(ctrDiscCont.getEndDate())){
                finalContStatus = ctrLoanContService.getContStatusAfterLogout(ctrDiscCont.getEndDate(),contStatus);
            }else {
                finalContStatus = CmisCommonConstants.CONT_STATUS_600;
            }
            
            // 更新当前登录信息
            User userInfo = SessionUtils.getUserInformation();
            String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            if(Objects.nonNull(userInfo)){
                ctrDiscCont.setUpdId(userInfo.getLoginCode());
                ctrDiscCont.setUpdBrId(userInfo.getOrg().getCode());
                ctrDiscCont.setUpdDate(nowDate);
            }

            if("100".equals(contStatus)){
                // 更新 合同信息
                ctrDiscCont.setContStatus(finalContStatus);
                this.updateSelective(ctrDiscCont);
            }else if("200".equals(contStatus)){
                ctrDiscCont.setContStatus(finalContStatus);
                this.updateSelective(ctrDiscCont);
            }

            //合同注销后，释放额度
            //恢复敞口
            BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
            //恢复总额
            BigDecimal recoverAmtCny = BigDecimal.ZERO;
            if(CmisCommonConstants.STD_DRFT_TYPE_1.equals(ctrDiscCont.getDrftType())){
                recoverSpacAmtCny = BigDecimal.ZERO;
                if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(ctrDiscCont.getDiscContType())){
                    recoverAmtCny = ctrDiscCont.getDrftTotalAmt();
                }else {
                    recoverAmtCny = ctrDiscCont.getContAmt();
                }
            }else {
                if(CmisCommonConstants.STD_DISC_CONT_TYPE_01.equals(ctrDiscCont.getDiscContType())){
                    recoverSpacAmtCny = ctrDiscCont.getDrftTotalAmt();
                    recoverAmtCny = ctrDiscCont.getDrftTotalAmt();
                }else {
                    recoverSpacAmtCny = ctrDiscCont.getContAmt();
                    recoverAmtCny = ctrDiscCont.getContAmt();
                }
            }
            // 根据合是否存在业务，得到恢复类型
            String recoverType = "";
            List<AccDisc> accDiscList = accDiscService.selectByContNo(ctrDiscCont.getContNo());
            if(CollectionUtils.nonEmpty(accDiscList)){
                //结清注销
                for (AccDisc accDisc : accDiscList) {
                    if(CmisCommonConstants.ACC_STATUS_7.equals(accDisc.getAccStatus())){
                        recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                    }else{
                        recoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
                        break;
                    }
                }
            }else {
                //未用注销
                recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
            }
            CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
            cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrDiscCont.getManagerBrId()));//金融机构代码
            cmisLmt0012ReqDto.setBizNo(ctrDiscCont.getContNo());//合同编号
            cmisLmt0012ReqDto.setRecoverType(recoverType);//恢复类型
            cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
            cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
            cmisLmt0012ReqDto.setInputId(ctrDiscCont.getInputId());
            cmisLmt0012ReqDto.setInputBrId(ctrDiscCont.getInputBrId());
            cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            log.info("贴现协议【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", contNo, cmisLmt0012ReqDto.toString());
            ResultDto<CmisLmt0012RespDto> resultDto=  cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
            log.info("贴现协议【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", contNo, resultDto.toString());
            if(!"0".equals(resultDto.getCode())){
                log.error("业务申请恢复额度异常！");
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请恢复额度异常！");
                throw BizException.error(null,code, resultDto.getData().getErrorMsg());
            }

            //不存在电子用印的情况下，需要作废档案归档任务
            if (!"1".equals(ctrDiscCont.getIsESeal())) {
                log.info("根据业务申请编号【" + ctrDiscCont.getSerno() + "】需要作废档案归档任务开始");
                try {
                    docArchiveInfoService.invalidByBizSerno(ctrDiscCont.getSerno(),"");
                } catch (Exception e) {
                    log.info("根据业务申请编号【" + ctrDiscCont.getSerno() + "】需要作废档案归档任务异常：" + e.getMessage());
                }
                log.info("根据业务申请编号【" + ctrDiscCont.getSerno() + "】需要作废档案归档任务结束");
            }
        } catch (Exception e) {
            log.error("贴现协议注销异常", e);
            rtnCode = EcbEnum.CTR_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.CTR_EXCEPTION_DEF.value;
            throw new Exception(e.getMessage());
        } finally {
            //成功则无需处理
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: 贴现协议重签
     * @方法描述: 根据合同号更新签订状态和时间, 包含合同项下的担保合同签订
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map contReSign(@RequestBody Map params) throws Exception {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        String contNo = "";
        int pvpLoanAppCount = 0;
        int accLoanCount = 0;
        try {
            CtrDiscCont ctrDiscCont = ctrDiscContMapper.selectByContNo(contNo);
            String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            // 更新当前登录信息
            User userInfo = SessionUtils.getUserInformation();
            if(Objects.nonNull(userInfo)){
                ctrDiscCont.setUpdId(userInfo.getLoginCode());
                ctrDiscCont.setUpdBrId(userInfo.getOrg().getCode());
                ctrDiscCont.setUpdDate(nowDate);
            }
            contNo = (String) params.get("contNo");//主键合同编号
            Map pvpQueryMap = new HashMap();
            pvpQueryMap.put("contNo", contNo);
            pvpLoanAppCount = pvpLoanAppMapper.countPvpLoanAppCountByCtrDiscContNo(pvpQueryMap);
            accLoanCount = accDiscMapper.countAccLoanCountByCtrDiscContNo(contNo);
            if (pvpLoanAppCount > 0) {
                rtnCode = EcbEnum.ON_RESIGN_EXCEPTION_PVP.key;
                rtnMsg = EcbEnum.ON_RESIGN_EXCEPTION_PVP.value;
            }else if(accLoanCount > 0){
                rtnCode = EcbEnum.ON_RESIGN_EXCEPTION_ACC.key;
                rtnMsg = EcbEnum.ON_RESIGN_EXCEPTION_ACC.value;
            }else{
                ctrDiscCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);
                updateSelective(ctrDiscCont);
            }
        } catch (Exception e) {
            log.error("贴现协议重签异常", e);
            rtnCode = EcbEnum.CTR_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.CTR_EXCEPTION_DEF.value;
            throw new Exception(e.getMessage());
        } finally {
            //成功则无需处理
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: selectByQuerymodel
     * @方法描述: 根据入参查询合同数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrDiscCont> selectByQuerymodel(QueryModel model) {
        return ctrDiscContMapper.selectByModel(model);
    }

    /**
     * @方法名称: setCtrDiscContStatus100
     * @方法描述: 根据合同编号将合同状态设置为未生效
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void setCtrDiscContStatus100(String contNo){
        CtrDiscCont ctrDiscCont = this.selectByContNo(contNo);
        ctrDiscCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);
        this.update(ctrDiscCont);
    }
    // TODO: 2021/8/20  根据合同号获取合同可出账金额
    /**
     * @param contNo
     * @return BigDecimal
     * @author qw
     * @date 2021/8/20 18:09
     * @version 1.0.0
     * @desc 根据合同号获取合同可出账金额
     * @修改历史:
     */
//    public BigDecimal getCanOutAccountAmt(String contNo) {
//        //可出账金额
//        BigDecimal canOutAccountAmt = BigDecimal.ZERO;
//        CtrDiscCont ctrDiscCont = ctrDiscContMapper.selectByContNo(contNo);
//        if(Objects.isNull(ctrDiscCont)){
//            throw BizException.error(null, EcbEnum.ECB020022.key, EcbEnum.ECB020022.value);
//        }
//        //合同最高可用信金额折算人民币金额
//        BigDecimal contHighAmt = ctrDiscCont.getContHighAvlAmt();
//
//        //业务敞口余额（未扣减保证金）
//        BigDecimal totalSpacBalance = BigDecimal.ZERO;
//        //业务敞口初始总金额（未扣减保证金）
//        BigDecimal totalOrigiSpacAmt = BigDecimal.ZERO;
//
//        //业务敞口余额（扣减保证金）
//        BigDecimal totalSpacBalanceSubtractBailAmt = BigDecimal.ZERO;
//        //业务敞口初始总金额（扣减保证金）
//        BigDecimal totalOrigiSpacSubtractBailAmt = BigDecimal.ZERO;
//
//        //保证金币种
//        String bailCurType = "";
//        //保证金折算人民币金额
//        BigDecimal bailAmtCny = BigDecimal.ZERO;
//        List<AccDisc> accDiscList = accDiscService.selectByContNo(ctrDiscCont.getContNo());
//        if(CollectionUtils.nonEmpty(accDiscList)){
//            for (AccDisc accDisc : accDiscList) {
//                totalSpacBalance = totalSpacBalance.add(accDisc.getExchangeRmbSpac());//业务敞口余额（未扣减保证金）
//                totalOrigiSpacAmt = totalOrigiSpacAmt.add(accDisc.getOrigiOpenAmt().multiply(accCvrs.getExchangeRate()));//业务敞口初始总金额（未扣减保证金）
//
//                totalSpacBalanceSubtractBailAmt = totalSpacBalanceSubtractBailAmt.add(accCvrs.getExchangeRmbSpac()).subtract(accCvrs.getBailCvtCnyAmt());//业务敞口余额（扣减保证金）
//                totalOrigiSpacSubtractBailAmt = (totalOrigiSpacSubtractBailAmt.add(accCvrs.getOrigiOpenAmt().multiply(accCvrs.getExchangeRate()).subtract(accCvrs.getBailCvtCnyAmt())));//业务敞口初始总金额（扣减保证金）
//            }
//            //低风险
//            if(CmisCommonConstants.GUAR_MODE_21.equals(ctrCvrgCont.getGuarMode())||
//                    CmisCommonConstants.GUAR_MODE_40.equals(ctrCvrgCont.getGuarMode())||
//                    CmisCommonConstants.GUAR_MODE_60.equals(ctrCvrgCont.getGuarMode())){
//                //一般合同
//                if(CmisCommonConstants.STD_CONT_TYPE_1.equals(ctrCvrgCont.getContType())){
//                    canOutAccountAmt = contHighAmt.subtract(totalSpacBalance);
//                }else {
//                    //最高额合同
//                    canOutAccountAmt = contHighAmt.subtract(totalOrigiSpacAmt);
//                }
//            }else{
//                //一般合同
//                if(CmisCommonConstants.STD_CONT_TYPE_1.equals(ctrCvrgCont.getContType())){
//                    canOutAccountAmt = contHighAmt.subtract(totalSpacBalanceSubtractBailAmt);
//                }else {
//                    //最高额合同
//                    canOutAccountAmt = contHighAmt.subtract(totalOrigiSpacSubtractBailAmt);
//                }
//            }
//        }else{
//            canOutAccountAmt = contHighAmt;
//        }
//        return canOutAccountAmt;
//    }

    /**
     * @方法名称: getSumContAmt
     * @方法描述: 根据额度编号查询合同金额总和
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BigDecimal getSumContAmt(String lmtAccNo) {
        return ctrDiscContMapper.getSumContAmt(lmtAccNo);
    }
}
