/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppRelGrpLimit
 * @类描述: lmt_app_rel_grp_limit数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-24 20:34:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_app_rel_grp_limit")
public class LmtAppRelGrpLimit extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 集团客户编号 **/
	@Column(name = "GRP_CUS_ID", unique = false, nullable = true, length = 20)
	private String grpCusId;
	
	/** 集团客户名称 **/
	@Column(name = "GRP_CUS_NAME", unique = false, nullable = true, length = 80)
	private String grpCusName;
	
	/** 集团授信金额 **/
	@Column(name = "GRP_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal grpLmtAmt;
	
	/** 已用额度 **/
	@Column(name = "OUTSTND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outstndAmt;
	
	/** 可用额度 **/
	@Column(name = "AVL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal avlLmtAmt;
	
	/** 集团授信余额 **/
	@Column(name = "GRP_LMT_BALANCT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal grpLmtBalanctAmt;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param grpCusId
	 */
	public void setGrpCusId(String grpCusId) {
		this.grpCusId = grpCusId;
	}
	
    /**
     * @return grpCusId
     */
	public String getGrpCusId() {
		return this.grpCusId;
	}
	
	/**
	 * @param grpCusName
	 */
	public void setGrpCusName(String grpCusName) {
		this.grpCusName = grpCusName;
	}
	
    /**
     * @return grpCusName
     */
	public String getGrpCusName() {
		return this.grpCusName;
	}
	
	/**
	 * @param grpLmtAmt
	 */
	public void setGrpLmtAmt(java.math.BigDecimal grpLmtAmt) {
		this.grpLmtAmt = grpLmtAmt;
	}
	
    /**
     * @return grpLmtAmt
     */
	public java.math.BigDecimal getGrpLmtAmt() {
		return this.grpLmtAmt;
	}
	
	/**
	 * @param outstndAmt
	 */
	public void setOutstndAmt(java.math.BigDecimal outstndAmt) {
		this.outstndAmt = outstndAmt;
	}
	
    /**
     * @return outstndAmt
     */
	public java.math.BigDecimal getOutstndAmt() {
		return this.outstndAmt;
	}
	
	/**
	 * @param avlLmtAmt
	 */
	public void setAvlLmtAmt(java.math.BigDecimal avlLmtAmt) {
		this.avlLmtAmt = avlLmtAmt;
	}
	
    /**
     * @return avlLmtAmt
     */
	public java.math.BigDecimal getAvlLmtAmt() {
		return this.avlLmtAmt;
	}
	
	/**
	 * @param grpLmtBalanctAmt
	 */
	public void setGrpLmtBalanctAmt(java.math.BigDecimal grpLmtBalanctAmt) {
		this.grpLmtBalanctAmt = grpLmtBalanctAmt;
	}
	
    /**
     * @return grpLmtBalanctAmt
     */
	public java.math.BigDecimal getGrpLmtBalanctAmt() {
		return this.grpLmtBalanctAmt;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}