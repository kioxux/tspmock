package cn.com.yusys.yusp.web.server.xdzc0018;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0018.req.Xdzc0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0018.resp.Xdzc0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0018.Xdzc0018Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:我的资产统计查询接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0018:我的资产统计查询接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0018Resource.class);

    @Autowired
    private Xdzc0018Service xdzc0018Service;
    /**
     * 交易码：xdzc0018
     * 交易描述：我的资产统计查询接口
     *
     * @param xdzc0018DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("我的资产统计查询接口")
    @PostMapping("/xdzc0018")
    protected @ResponseBody
    ResultDto<Xdzc0018DataRespDto> xdzc0018(@Validated @RequestBody Xdzc0018DataReqDto xdzc0018DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value, JSON.toJSONString(xdzc0018DataReqDto));
        Xdzc0018DataRespDto xdzc0018DataRespDto = new Xdzc0018DataRespDto();// 响应Dto:我的资产统计查询接口
        ResultDto<Xdzc0018DataRespDto> xdzc0018DataResultDto = new ResultDto<>();
        try {
            xdzc0018DataRespDto = xdzc0018Service.xdzc0018Service(xdzc0018DataReqDto);
            xdzc0018DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0018DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value, e.getMessage());
            // 封装xdzc0018DataResultDto中异常返回码和返回信息
            xdzc0018DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0018DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0018DataRespDto到xdzc0018DataResultDto中
        xdzc0018DataResultDto.setData(xdzc0018DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value, JSON.toJSONString(xdzc0018DataResultDto));
        return xdzc0018DataResultDto;
    }
}
