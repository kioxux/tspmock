/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarCorp;
import cn.com.yusys.yusp.service.RptLmtRepayAnysGuarCorpService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarCorpResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-05 23:04:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptlmtrepayanysguarcorp")
public class RptLmtRepayAnysGuarCorpResource {
    @Autowired
    private RptLmtRepayAnysGuarCorpService rptLmtRepayAnysGuarCorpService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptLmtRepayAnysGuarCorp>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptLmtRepayAnysGuarCorp> list = rptLmtRepayAnysGuarCorpService.selectAll(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarCorp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptLmtRepayAnysGuarCorp>> index(QueryModel queryModel) {
        List<RptLmtRepayAnysGuarCorp> list = rptLmtRepayAnysGuarCorpService.selectByModel(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarCorp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptLmtRepayAnysGuarCorp> show(@PathVariable("pkId") String pkId) {
        RptLmtRepayAnysGuarCorp rptLmtRepayAnysGuarCorp = rptLmtRepayAnysGuarCorpService.selectByPrimaryKey(pkId);
        return new ResultDto<RptLmtRepayAnysGuarCorp>(rptLmtRepayAnysGuarCorp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptLmtRepayAnysGuarCorp> create(@RequestBody RptLmtRepayAnysGuarCorp rptLmtRepayAnysGuarCorp) throws URISyntaxException {
        rptLmtRepayAnysGuarCorpService.insert(rptLmtRepayAnysGuarCorp);
        return new ResultDto<RptLmtRepayAnysGuarCorp>(rptLmtRepayAnysGuarCorp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptLmtRepayAnysGuarCorp rptLmtRepayAnysGuarCorp) throws URISyntaxException {
        int result = rptLmtRepayAnysGuarCorpService.update(rptLmtRepayAnysGuarCorp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptLmtRepayAnysGuarCorpService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptLmtRepayAnysGuarCorpService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据流水号查询
     *
     * @param map RptLmtRepayAnysGuarCorp rptLmtRepayAnysGuarCorp
     * @return
     */
    @ApiOperation("根据流水号查看担保人为公司简易版的信息")
    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptLmtRepayAnysGuarCorp>> selectBySerno(@RequestBody Map<String, Object> map) {
        String serno = map.get("serno").toString();
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        return new ResultDto<List<RptLmtRepayAnysGuarCorp>>(rptLmtRepayAnysGuarCorpService.selectByModel(model));
    }

    /**
     * 保存信息
     *
     * @param rptLmtRepayAnysGuarCorp
     * @return
     */
    @PostMapping("/saveGuarCorp")
    protected ResultDto<Integer> saveGuarCorp(@RequestBody RptLmtRepayAnysGuarCorp rptLmtRepayAnysGuarCorp) {
        return new ResultDto<Integer>(rptLmtRepayAnysGuarCorpService.saveGuarCorp(rptLmtRepayAnysGuarCorp));
    }

    @PostMapping("/insertCorp")
    protected  ResultDto<String> insertCorp(@RequestBody RptLmtRepayAnysGuarCorp rptLmtRepayAnysGuarCorp){
        return new ResultDto<String>(rptLmtRepayAnysGuarCorpService.insertCorp(rptLmtRepayAnysGuarCorp));
    }

    @PostMapping("/selectByPkId")
    protected ResultDto<RptLmtRepayAnysGuarCorp> selectByPkId(@RequestBody String pkId){
        return new ResultDto<RptLmtRepayAnysGuarCorp>(rptLmtRepayAnysGuarCorpService.selectByPrimaryKey(pkId));
    }
    @PostMapping("/initGuarCorp")
    protected ResultDto<RptLmtRepayAnysGuarCorp> initGuarCorp(@RequestBody Map map){
        return new ResultDto<RptLmtRepayAnysGuarCorp>(rptLmtRepayAnysGuarCorpService.initGuarCorp(map));
    }
}
