package cn.com.yusys.yusp.web.server.xdcz0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0003.req.Xdcz0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0003.resp.Xdcz0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0003.Xdcz0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:电子银行承兑汇票出账申请
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0003:电子银行承兑汇票出账申请")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0003.BizXdcz0003Resource.class);
    @Autowired
    private Xdcz0003Service xdcz0003Service;

    /**
     * 交易码：xdcz0003
     * 交易描述：电子保函开立
     *
     * @param xdcz0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("电子银行承兑汇票出账申请")
    @PostMapping("/xdcz0003")
    protected @ResponseBody
    ResultDto<Xdcz0003DataRespDto> xdcz0003(@Validated @RequestBody Xdcz0003DataReqDto xdcz0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, JSON.toJSONString(xdcz0003DataReqDto));
        Xdcz0003DataRespDto xdcz0003DataRespDto = new Xdcz0003DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0003DataRespDto> xdcz0003DataResultDto = new ResultDto<>();
        try {
            // 从xdcz0003DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, JSON.toJSONString(xdcz0003DataReqDto));
            xdcz0003DataRespDto = xdcz0003Service.xdcz0003(xdcz0003DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, JSON.toJSONString(xdcz0003DataRespDto));

            // 封装xdcz0003DataResultDto中正确的返回码和返回信息
            xdcz0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, e.getMessage());
            // 封装xdcz0003DataResultDto中异常返回码和返回信息
            xdcz0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0003DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdcz0003DataRespDto到xdcz0003DataResultDto中
        xdcz0003DataResultDto.setData(xdcz0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, JSON.toJSONString(xdcz0003DataRespDto));
        return xdcz0003DataResultDto;
    }
}
