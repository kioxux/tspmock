package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocScanUserInfo
 * @类描述: doc_scan_user_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-19 13:53:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class DocScanUserInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 扫描信息流水号 **/
	private String dsuiSerno;
	
	/** 档案流水号 **/
	private String docSerno;
	
	/** 扫描人编号 **/
	private String scanUserId;
	
	/** 扫描人姓名 **/
	private String scanUserName;
	
	/** 扫描时间 **/
	private String scanTime;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param dsuiSerno
	 */
	public void setDsuiSerno(String dsuiSerno) {
		this.dsuiSerno = dsuiSerno == null ? null : dsuiSerno.trim();
	}
	
    /**
     * @return DsuiSerno
     */	
	public String getDsuiSerno() {
		return this.dsuiSerno;
	}
	
	/**
	 * @param docSerno
	 */
	public void setDocSerno(String docSerno) {
		this.docSerno = docSerno == null ? null : docSerno.trim();
	}
	
    /**
     * @return DocSerno
     */	
	public String getDocSerno() {
		return this.docSerno;
	}
	
	/**
	 * @param scanUserId
	 */
	public void setScanUserId(String scanUserId) {
		this.scanUserId = scanUserId == null ? null : scanUserId.trim();
	}
	
    /**
     * @return ScanUserId
     */	
	public String getScanUserId() {
		return this.scanUserId;
	}
	
	/**
	 * @param scanUserName
	 */
	public void setScanUserName(String scanUserName) {
		this.scanUserName = scanUserName == null ? null : scanUserName.trim();
	}
	
    /**
     * @return ScanUserName
     */	
	public String getScanUserName() {
		return this.scanUserName;
	}
	
	/**
	 * @param scanTime
	 */
	public void setScanTime(String scanTime) {
		this.scanTime = scanTime == null ? null : scanTime.trim();
	}
	
    /**
     * @return ScanTime
     */	
	public String getScanTime() {
		return this.scanTime;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}