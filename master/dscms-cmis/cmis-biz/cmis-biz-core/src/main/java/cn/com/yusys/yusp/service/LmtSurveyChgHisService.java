/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.UUID;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSurveyChgHis;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyChgHisMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyChgHisService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-26 10:40:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSurveyChgHisService {

    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyChgHisService.class);

    @Autowired
    private LmtSurveyChgHisMapper lmtSurveyChgHisMapper;

    @Autowired
    private CtrLoanContService ctrLoanContService; //合同主表

    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService; // 小贷批复表

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSurveyChgHis selectByPrimaryKey(String pkId, String contNo) {
        return lmtSurveyChgHisMapper.selectByPrimaryKey(pkId, contNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSurveyChgHis> selectAll(QueryModel model) {
        List<LmtSurveyChgHis> records = (List<LmtSurveyChgHis>) lmtSurveyChgHisMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSurveyChgHis> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSurveyChgHis> list = lmtSurveyChgHisMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSurveyChgHis record) {
        return lmtSurveyChgHisMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSurveyChgHis record) {
        return lmtSurveyChgHisMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSurveyChgHis record) {
        return lmtSurveyChgHisMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSurveyChgHis record) {
        return lmtSurveyChgHisMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String contNo) {
        return lmtSurveyChgHisMapper.deleteByPrimaryKey(pkId, contNo);
    }

    /**
     * @param lmtSurveyChgHis
     * @return cn.com.yusys.yusp.commons.data.model.ResultDto
     * @author hubp
     * @date 2021/8/26 10:44
     * @version 1.0.0
     * @desc  调查报告替换
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto surveyReplace(LmtSurveyChgHis lmtSurveyChgHis) {
        logger.info("*****************调查报告替换开始*****************");
        int result = -1;
        try {
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(lmtSurveyChgHis.getContNo()); //合同信息
            LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(lmtSurveyChgHis.getReplyNo());
            if (!lmtCrdReplyInfo.getCusId().equals(ctrLoanCont.getCusId())) {
                return new ResultDto(9999).message("调查报告替换必须为同一客户！");
            }
            if (lmtSurveyChgHis.getReplyNo().equals(ctrLoanCont.getReplyNo())) {
                return new ResultDto(9999).message("调查报告替换不能为同一批复！");
            }
            // 开始放入历史数据
            lmtSurveyChgHis.setPkId(UUID.randomUUID().toString());
            lmtSurveyChgHis.setSurveySerno(lmtCrdReplyInfo.getSurveySerno());
            lmtSurveyChgHis.setOldSurveySerno(ctrLoanCont.getSurveySerno());
            lmtSurveyChgHis.setOldReplyNo(ctrLoanCont.getReplyNo());
            // 开始更新合同数据
            ctrLoanCont.setSurveySerno(lmtCrdReplyInfo.getSurveySerno());
            ctrLoanCont.setReplyNo(lmtCrdReplyInfo.getReplySerno());
            result = ctrLoanContService.updateSelective(ctrLoanCont);
            if (result != 1) {
                throw BizException.error(null, "9999", "合同数据更新失败！");
            }
            result = lmtSurveyChgHisMapper.insertSelective(lmtSurveyChgHis);
            if (result != 1) {
                throw BizException.error(null, "9999", "批复历史数据插入失败！");
            }

            logger.info("*********************************前往额度系统替换合同与额度信息开始 替换合同编号【{}】***********************************", lmtSurveyChgHis.getContNo());
            cmisBizXwCommonService.sendCmisLmt0001ForReplace(lmtCrdReplyInfo, lmtSurveyChgHis.getOldSurveySerno(), lmtSurveyChgHis.getOldReplyNo());
            logger.info("*********************************前往额度系统替换合同与额度信息结束 替换合同编号【{}】***********************************", lmtSurveyChgHis.getContNo());
        } catch (Exception e) {
            logger.error("*****************调查报告替换异常*****************【{}】", JSON.toJSON(e));
            throw e;
        } finally {
            logger.info("*****************调查报告替换结束*****************");
        }
        return new ResultDto(result);
    }
}
