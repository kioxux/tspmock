/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.LmtFinGuarApp;
import cn.com.yusys.yusp.domain.LmtFinGuarList;
import cn.com.yusys.yusp.domain.LmtThrShrsAppRel;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtFinGuarAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFinGuarAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-02-04 11:57:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtFinGuarAppService {
    private static final Logger log = LoggerFactory.getLogger(LmtFinGuarAppService.class);

    @Autowired
    private LmtFinGuarAppMapper lmtFinGuarAppMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtFinGuarListService lmtFinGuarListService;

    @Autowired
    private LmtThrShrsAppRelService lmtThrShrsAppRelService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtFinGuarApp selectByPrimaryKey(String serno) {
        return lmtFinGuarAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtFinGuarApp> selectAll(QueryModel model) {
        List<LmtFinGuarApp> records = (List<LmtFinGuarApp>) lmtFinGuarAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtFinGuarApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtFinGuarApp> list = lmtFinGuarAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtFinGuarApp record) {
        return lmtFinGuarAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtFinGuarApp record) {
        return lmtFinGuarAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtFinGuarApp record) {
        return lmtFinGuarAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtFinGuarApp record) {
        return lmtFinGuarAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return lmtFinGuarAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtFinGuarAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectCountByOldSerno
     * @方法描述: 根据原业务流水号查询复议申请
     * @参数: serno
     * */
    public LmtFinGuarApp selectCountByOldSerno(String serno){
        return lmtFinGuarAppMapper.selectCountByOldSerno(serno);
    }

    /***
     * 审批通过业务处理
     * lmtFinGuarApp 业务申请信息
     * */
    public void lmtFinGuarAppAgree (LmtFinGuarApp lmtFinGuarApp){

    }

    /**
     * 通过起始日，期限类型，期限计算到期日
     * @param dateStr 起始日
     * @param termType 期限类型
     * @param term 期限
     */
    private String calDateByTermTypeAndTerm(String dateStr,String termType,int term) {
        String calDate = "";
        if (CmisBizConstants.LMT_TERM_TYPE_001.equals(termType)) {//期限类型为年
            calDate = DateUtils.addYear(dateStr, DateFormatEnum.DEFAULT.getValue(), term);
        } else if (CmisBizConstants.LMT_TERM_TYPE_002.equals(termType)) {//期限类型为月
            calDate = DateUtils.addMonth(dateStr, DateFormatEnum.DEFAULT.getValue(), term);
        } else if (CmisBizConstants.LMT_TERM_TYPE_003.equals(termType)) {//期限类型为日
            calDate = DateUtils.addDay(dateStr, DateFormatEnum.DEFAULT.getValue(), term);
        }
        return calDate;
    }
}
