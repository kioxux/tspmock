/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.AsplAssetsList;
import cn.com.yusys.yusp.domain.IqpAppAspl;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AsplIoPool;
import cn.com.yusys.yusp.service.AsplIoPoolService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplIoPoolResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 21:04:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "资产出入池")
@RequestMapping("/api/aspliopool")
public class AsplIoPoolResource {
    @Autowired
    private AsplIoPoolService asplIoPoolService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AsplIoPool>> query() {
        QueryModel queryModel = new QueryModel();
        List<AsplIoPool> list = asplIoPoolService.selectAll(queryModel);
        return new ResultDto<List<AsplIoPool>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AsplIoPool>> index(QueryModel queryModel) {
        List<AsplIoPool> list = asplIoPoolService.selectByModel(queryModel);
        return new ResultDto<List<AsplIoPool>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AsplIoPool> show(@PathVariable("pkId") String pkId) {
        AsplIoPool asplIoPool = asplIoPoolService.selectByPrimaryKey(pkId);
        return new ResultDto<AsplIoPool>(asplIoPool);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AsplIoPool> create(@RequestBody AsplIoPool asplIoPool) throws URISyntaxException {
        asplIoPoolService.insert(asplIoPool);
        return new ResultDto<AsplIoPool>(asplIoPool);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AsplIoPool asplIoPool) throws URISyntaxException {
        int result = asplIoPoolService.update(asplIoPool);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveaspliopool")
    protected ResultDto<Integer> saveAsplIoPool(@RequestBody Map map) throws URISyntaxException {
        return asplIoPoolService.saveAsplIoPool(map);
    }
    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = asplIoPoolService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = asplIoPoolService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:inPoolList
     * @函数描述:资产入池列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("资产入池列表")
    @PostMapping("/inpoollist")
    protected ResultDto<List<AsplIoPool>> inPoolList(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.getCondition().put("inoutType", CmisCommonConstants.INOUT_TYPE_1);
        queryModel.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<AsplIoPool> list = asplIoPoolService.inoutPoolList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<AsplIoPool>>(list);
    }

    /**
     * @方法名称: queryAsplIoPoolByParams
     * @方法描述: 根据入参查询资产池入池申请数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("根据入参查询数据")
    @PostMapping("/queryaspliopoolbyparams")
    protected ResultDto<AsplIoPool> queryAsplIoPoolByParams(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("serno",(String)map.get("serno"));
        queryData.put("oprType", CommonConstance.OPR_TYPE_ADD);
        AsplIoPool asplIoPool = asplIoPoolService.queryAsplIoPoolByParams(queryData);

        return new ResultDto<AsplIoPool>(asplIoPool);
    }

    /**
     * @函数名称:outPoolList
     * @函数描述:资产出池列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("资产出池列表")
    @PostMapping("/outpoollist")
    protected ResultDto<List<AsplIoPool>> outPoolList(@RequestBody QueryModel queryModel) {
        queryModel.setSort("input_date desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.getCondition().put("inoutType", CmisCommonConstants.INOUT_TYPE_0);
        queryModel.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<AsplIoPool> list = asplIoPoolService.inoutPoolList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<AsplIoPool>>(list);
    }

    /**
     * @函数名称:asplList
     * @函数描述:资产池查询列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("资产池查询列表")
    @PostMapping("/aspllist")
    protected ResultDto<List<AsplIoPool>> asplList(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<AsplIoPool> list = asplIoPoolService.inoutPoolList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<AsplIoPool>>(list);
    }
    /**
     * @函数名称:asplList
     * @函数描述:新增一笔出池
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("新增一笔出池记录")
    @PostMapping("/addoutpoollist")
    protected ResultDto<AsplIoPool> addOutPoollist(@RequestBody Map map) {
        ResultDto<AsplIoPool> resultDto = asplIoPoolService.addOutPoollist(map);
        return resultDto;
    }

    /**
     * @函数名称:delteoutpoollist
     * @函数描述:删除一笔出池记录
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("删除一笔出池记录")
    @PostMapping("/delteoutpoollist")
    protected ResultDto<AsplIoPool> deleteOutpoollist(@RequestBody Map map) {
        ResultDto<AsplIoPool> resultDto = asplIoPoolService.deleteOutpoollist(map);
        return resultDto;
    }
}
