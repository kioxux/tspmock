package cn.com.yusys.yusp.web.server.xdsx0013;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdsx0013.Xdsx0013Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdsx0013.req.Xdsx0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0013.resp.Xdsx0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:风控发信贷根据客户号查询授信信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0013:风控发信贷根据客户号查询授信信息")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0013Resource.class);

    @Autowired
    private Xdsx0013Service xdsx0013Service;

    /**
     * 交易码：xdsx0013
     * 交易描述：风控发信贷根据客户号查询授信信息
     *
     * @param xdsx0013DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控发信贷根据客户号查询授信信息")
    @PostMapping("/xdsx0013")
    protected @ResponseBody
    ResultDto<Xdsx0013DataRespDto> xdsx0013(@Validated @RequestBody Xdsx0013DataReqDto xdsx0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, JSON.toJSONString(xdsx0013DataReqDto));
        Xdsx0013DataRespDto xdsx0013DataRespDto = new Xdsx0013DataRespDto();// 响应Dto:风控发信贷根据客户号查询授信信息
        ResultDto<Xdsx0013DataRespDto> xdsx0013DataResultDto = new ResultDto<>();
        try {
            // 从xdsx0013DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, JSON.toJSONString(xdsx0013DataReqDto));
            xdsx0013DataRespDto = xdsx0013Service.xdsx0013(xdsx0013DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, JSON.toJSONString(xdsx0013DataRespDto));
            // 封装xdsx0013DataResultDto中正确的返回码和返回信息
            xdsx0013DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0013DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, e.getMessage());
            // 封装xdsx0013DataResultDto中异常返回码和返回信息
            xdsx0013DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0013DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0013DataRespDto到xdsx0013DataResultDto中
        xdsx0013DataResultDto.setData(xdsx0013DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, JSON.toJSONString(xdsx0013DataResultDto));
        return xdsx0013DataResultDto;
    }
}
