/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.PvpLoanApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PvpLoanAppXqywhbxdResult;
import cn.com.yusys.yusp.service.PvpLoanAppXqywhbxdResultService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppXqywhbxdResultResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xuchao
 * @创建时间: 2021-06-18 13:41:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pvploanappxqywhbxdresult")
public class PvpLoanAppXqywhbxdResultResource {
    @Autowired
    private PvpLoanAppXqywhbxdResultService pvpLoanAppXqywhbxdResultService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpLoanAppXqywhbxdResult>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpLoanAppXqywhbxdResult> list = pvpLoanAppXqywhbxdResultService.selectAll(queryModel);
        return new ResultDto<List<PvpLoanAppXqywhbxdResult>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpLoanAppXqywhbxdResult>> index(QueryModel queryModel) {
        List<PvpLoanAppXqywhbxdResult> list = pvpLoanAppXqywhbxdResultService.selectByModel(queryModel);
        return new ResultDto<List<PvpLoanAppXqywhbxdResult>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PvpLoanAppXqywhbxdResult> show(@PathVariable("pkId") String pkId) {
        PvpLoanAppXqywhbxdResult pvpLoanAppXqywhbxdResult = pvpLoanAppXqywhbxdResultService.selectByPrimaryKey(pkId);
        return new ResultDto<PvpLoanAppXqywhbxdResult>(pvpLoanAppXqywhbxdResult);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpLoanAppXqywhbxdResult> create(@RequestBody PvpLoanAppXqywhbxdResult pvpLoanAppXqywhbxdResult) throws URISyntaxException {
        pvpLoanAppXqywhbxdResultService.insert(pvpLoanAppXqywhbxdResult);
        return new ResultDto<PvpLoanAppXqywhbxdResult>(pvpLoanAppXqywhbxdResult);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpLoanAppXqywhbxdResult pvpLoanAppXqywhbxdResult) throws URISyntaxException {
        int result = pvpLoanAppXqywhbxdResultService.update(pvpLoanAppXqywhbxdResult);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pvpLoanAppXqywhbxdResultService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpLoanAppXqywhbxdResultService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryPvpLoanAppXqywhbxdResultDataByParams
     * @函数描述:根据入参查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<PvpLoanAppXqywhbxdResult>> selectByModel(@RequestBody QueryModel queryModel) {
        List<PvpLoanAppXqywhbxdResult> pvpLoanAppXqywhbxdResultList = pvpLoanAppXqywhbxdResultService.selectByModel(queryModel);
        return new ResultDto<List<PvpLoanAppXqywhbxdResult>>(pvpLoanAppXqywhbxdResultList);
    }
}
