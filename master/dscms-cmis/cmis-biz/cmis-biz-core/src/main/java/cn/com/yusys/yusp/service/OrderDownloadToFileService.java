package cn.com.yusys.yusp.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.File;
import java.util.Map;

/**
 * 预约下载传到文件服务 。。。。就这样发吧。。。。
 * @author Administrator
 *
 */
@FeignClient(fallback= OrderDownloadToFileServiceHystrixImpl.class,name = "yusp-file", path = "/api/file/provider")
public interface OrderDownloadToFileService {
 
	@PostMapping("/uploadfile")
	Map uploadfile(@RequestParam("file") File file);
	
}
