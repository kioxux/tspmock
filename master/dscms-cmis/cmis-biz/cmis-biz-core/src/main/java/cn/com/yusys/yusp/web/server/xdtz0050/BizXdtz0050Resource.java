package cn.com.yusys.yusp.web.server.xdtz0050;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdtz0050.Xdtz0050Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0050.req.Xdtz0050DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0050.resp.Xdtz0050DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:对私客户关联业务检查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0050:对私客户关联业务检查")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0050Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0050Resource.class);

    @Autowired
    private Xdtz0050Service xdtz0050Service;

    /**
     * 交易码：xdtz0050
     * 交易描述：对私客户关联业务检查
     *
     * @param xdtz0050DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("对私客户关联业务检查")
    @PostMapping("/xdtz0050")
    protected @ResponseBody
    ResultDto<Xdtz0050DataRespDto> xdtz0050(@Validated @RequestBody Xdtz0050DataReqDto xdtz0050DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, JSON.toJSONString(xdtz0050DataReqDto));
        Xdtz0050DataRespDto xdtz0050DataRespDto = new Xdtz0050DataRespDto();// 响应Dto:对私客户关联业务检查
        ResultDto<Xdtz0050DataRespDto> xdtz0050DataResultDto = new ResultDto<>();
        String cusId = xdtz0050DataReqDto.getCusId();//客户号
        try {
            //业务逻辑
            xdtz0050DataRespDto = xdtz0050Service.xdtz0050(cusId);
            // 封装xdtz0050DataResultDto中正确的返回码和返回信息
            xdtz0050DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0050DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, e.getMessage());
            // 封装xdtz0050DataResultDto中异常返回码和返回信息
            xdtz0050DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0050DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0050DataRespDto到xdtz0050DataResultDto中
        xdtz0050DataResultDto.setData(xdtz0050DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, JSON.toJSONString(xdtz0050DataResultDto));
        return xdtz0050DataResultDto;
    }
}
