/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.BizMustCheckDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.BizMustCheckDetails;
import cn.com.yusys.yusp.service.BizMustCheckDetailsService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BizMustCheckDetailsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-12 15:37:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/bizmustcheckdetails")
public class BizMustCheckDetailsResource {
    @Autowired
    private BizMustCheckDetailsService bizMustCheckDetailsService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BizMustCheckDetails>> query() {
        QueryModel queryModel = new QueryModel();
        List<BizMustCheckDetails> list = bizMustCheckDetailsService.selectAll(queryModel);
        return new ResultDto<List<BizMustCheckDetails>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BizMustCheckDetails>> index(QueryModel queryModel) {
        List<BizMustCheckDetails> list = bizMustCheckDetailsService.selectByModel(queryModel);
        return new ResultDto<List<BizMustCheckDetails>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<BizMustCheckDetails> show(@PathVariable("pkId") String pkId) {
        BizMustCheckDetails bizMustCheckDetails = bizMustCheckDetailsService.selectByPrimaryKey(pkId);
        return new ResultDto<BizMustCheckDetails>(bizMustCheckDetails);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BizMustCheckDetails> create(@RequestBody BizMustCheckDetails bizMustCheckDetails) throws URISyntaxException {
        bizMustCheckDetailsService.insert(bizMustCheckDetails);
        return new ResultDto<BizMustCheckDetails>(bizMustCheckDetails);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BizMustCheckDetails bizMustCheckDetails) throws URISyntaxException {
        int result = bizMustCheckDetailsService.update(bizMustCheckDetails);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = bizMustCheckDetailsService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = bizMustCheckDetailsService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据流水号查询必输页签
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<List<BizMustCheckDetails>> selectBySerno(@RequestBody String serno){
        return new ResultDto<List<BizMustCheckDetails>>(bizMustCheckDetailsService.selectBySerno(serno));
    }

    /**
     * 新增必输页签
     * @param bizMustCheckDetailsDto
     * @return
     */
    @PostMapping("/insertMustCheck")
    protected ResultDto<Integer> insertMustCheck(@RequestBody BizMustCheckDetailsDto bizMustCheckDetailsDto){
        return new ResultDto<Integer>(bizMustCheckDetailsService.insertMustCheck(bizMustCheckDetailsDto));
    }
    /**
     * 页签保存完成
     */
    @PostMapping("doMustCheck")
    protected ResultDto<Integer> doMustCheck(@RequestBody BizMustCheckDetails bizMustCheckDetails){
        return new ResultDto<Integer>(bizMustCheckDetailsService.doMustCheck(bizMustCheckDetails));
    }

    /**
     * 单一授信调查报告完整性检验
     * @param bizMustCheckDetails
     * @return
     */
    @PostMapping("/rptCheck")
    protected ResultDto<Integer> rptCheck(@RequestBody BizMustCheckDetails bizMustCheckDetails){
        return new ResultDto<Integer>(bizMustCheckDetailsService.rptCheck(bizMustCheckDetails));
    }
}
