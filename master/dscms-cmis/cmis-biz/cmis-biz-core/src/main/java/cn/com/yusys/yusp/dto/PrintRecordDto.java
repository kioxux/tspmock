package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PrintRecord
 * @类描述: print_record数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-13 13:50:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PrintRecordDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 批次号 **/
	private String batchNo;
	/** 业务编号 **/
	private String bizNo;
	
	/** 打印人 **/
	private String printId;
	
	/** 打印时间 **/
	private String printTime;
	
	/** 打印机构 **/
	private String printBrId;
	
	/** 创建时间 **/
	private java.util.Date creatTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param batchNo
	 */
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo == null ? null : batchNo.trim();
	}
	
    /**
     * @return BatchNo
     */	
	public String getBatchNo() {
		return this.batchNo;
	}
	
	/**
	 * @param bizNo
	 */
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo == null ? null : bizNo.trim();
	}
	
    /**
     * @return BizNo
     */	
	public String getBizNo() {
		return this.bizNo;
	}
	
	/**
	 * @param printId
	 */
	public void setPrintId(String printId) {
		this.printId = printId == null ? null : printId.trim();
	}
	
    /**
     * @return PrintId
     */	
	public String getPrintId() {
		return this.printId;
	}
	
	/**
	 * @param printTime
	 */
	public void setPrintTime(String printTime) {
		this.printTime = printTime == null ? null : printTime.trim();
	}
	
    /**
     * @return PrintTime
     */	
	public String getPrintTime() {
		return this.printTime;
	}
	
	/**
	 * @param printBrId
	 */
	public void setPrintBrId(String printBrId) {
		this.printBrId = printBrId == null ? null : printBrId.trim();
	}
	
    /**
     * @return PrintBrId
     */	
	public String getPrintBrId() {
		return this.printBrId;
	}
	
	/**
	 * @param creatTime
	 */
	public void setCreatTime(java.util.Date creatTime) {
		this.creatTime = creatTime;
	}
	
    /**
     * @return CreatTime
     */	
	public java.util.Date getCreatTime() {
		return this.creatTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}