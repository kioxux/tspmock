/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: XbdInfo
 * @类描述: xbd_info数据实体类
 * @功能描述:
 * @创建人: wq
 * @创建时间: 2021-08-04 15:55:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "xbd_info")
public class XbdInfo extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 流水号 **/
	@Id
	@Column(name = "PVP_SERNO", unique = false, nullable = true, length = 40)
	private String pvpSerno;

	/** 保单号 **/
	@Column(name = "BD_NO", unique = false, nullable = true, length = 40)
	private String bdNo;

	/** 担保合同号 **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
	private String guarContNo;

	/** 确认函编号 **/
	@Column(name = "QRH_NO", unique = false, nullable = true, length = 40)
	private String qrhNo;

	/** 投保人名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 保险人名称 **/
	@Column(name = "INSURANCE_NAME", unique = false, nullable = true, length = 80)
	private String insuranceName;

	/** 被保险人名称 **/
	@Column(name = "INSURED_NAME", unique = false, nullable = true, length = 80)
	private String insuredName;

	/** 承保借款本金 **/
	@Column(name = "CB_LOAN_AMT", unique = false, nullable = true, length = 80)
	private java.math.BigDecimal cbLoanAmt;

	/** 保险起始日期 **/
	@Column(name = "BX_START_DATE", unique = false, nullable = true, length = 10)
	private String bxStartDate;

	/** 保险截止日期 **/
	@Column(name = "BX_END_DATE", unique = false, nullable = true, length = 10)
	private String bxEndDate;

	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}

	/**
	 * @return pvpSerno
	 */
	public String getPvpSerno() {
		return this.pvpSerno;
	}

	/**
	 * @param bdNo
	 */
	public void setBdNo(String bdNo) {
		this.bdNo = bdNo;
	}

	/**
	 * @return bdNo
	 */
	public String getBdNo() {
		return this.bdNo;
	}

	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}

	/**
	 * @return guarContNo
	 */
	public String getGuarContNo() {
		return this.guarContNo;
	}

	/**
	 * @param qrhNo
	 */
	public void setQrhNo(String qrhNo) {
		this.qrhNo = qrhNo;
	}

	/**
	 * @return qrhNo
	 */
	public String getQrhNo() {
		return this.qrhNo;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param insuranceName
	 */
	public void setInsuranceName(String insuranceName) {
		this.insuranceName = insuranceName;
	}

	/**
	 * @return insuranceName
	 */
	public String getInsuranceName() {
		return this.insuranceName;
	}

	/**
	 * @param insuredName
	 */
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	/**
	 * @return insuredName
	 */
	public String getInsuredName() {
		return this.insuredName;
	}

	/**
	 * @param cbLoanAmt
	 */
	public void setCbLoanAmt(java.math.BigDecimal cbLoanAmt) {
		this.cbLoanAmt = cbLoanAmt;
	}

	/**
	 * @return cbLoanAmt
	 */
	public java.math.BigDecimal getCbLoanAmt() {
		return this.cbLoanAmt;
	}

	/**
	 * @param bxStartDate
	 */
	public void setBxStartDate(String bxStartDate) {
		this.bxStartDate = bxStartDate;
	}

	/**
	 * @return bxStartDate
	 */
	public String getBxStartDate() {
		return this.bxStartDate;
	}

	/**
	 * @param bxEndDate
	 */
	public void setBxEndDate(String bxEndDate) {
		this.bxEndDate = bxEndDate;
	}

	/**
	 * @return bxEndDate
	 */
	public String getBxEndDate() {
		return this.bxEndDate;
	}

}