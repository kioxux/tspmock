/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanApp
 * @类描述: iqp_loan_app数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-07 16:31:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_loan_app")
public class IqpLoanApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 业务申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;

	/** 原申请流水号 **/
	@Column(name = "OLD_IQP_SERNO", unique = false, nullable = true, length = 40)
	private String oldIqpSerno;

	/** 是否复议 **/
	@Column(name = "IS_RECONSID", unique = false, nullable = true, length = 5)
	private String isReconsid;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;

	/** 手机号码 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 11)
	private String phone;

	/** 业务类型 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;

	/** 特殊业务类型 **/
	@Column(name = "ESPEC_BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String especBizType;

	/** 申请日期 **/
	@Column(name = "APP_DATE", unique = false, nullable = true, length = 10)
	private String appDate;

	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;

	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;

	/** 贷款用途 **/
	@Column(name = "LOAN_PURP", unique = false, nullable = true, length = 5)
	private String loanPurp;

	/** 贷款形式 **/
	@Column(name = "LOAN_MODAL", unique = false, nullable = true, length = 5)
	private String loanModal;

	/** 贷款性质 **/
	@Column(name = "LOAN_CHA", unique = false, nullable = true, length = 5)
	private String loanCha;

	/** 是否申请优惠利率 **/
	@Column(name = "PREFER_RATE_FLAG", unique = false, nullable = true, length = 1)
	private String preferRateFlag;

	/** 其他消费贷款月还款额 **/
	@Column(name = "OTHER_COMSUME_REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherComsumeRepayAmt;

	/** 本笔公积金贷款月还款额 **/
	@Column(name = "PUND_LOAN_MON_REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pundLoanMonRepayAmt;

	/** 本笔月还款额 **/
	@Column(name = "MONTH_REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal monthRepayAmt;

	/** 是否在线抵押 **/
	@Column(name = "IS_OL_PLD", unique = false, nullable = true, length = 1)
	private String isOlPld;

	/** 合同模式 **/
	@Column(name = "CONT_MODE", unique = false, nullable = true, length = 5)
	private String contMode;

	/** 项目编号 **/
	@Column(name = "PRO_NO", unique = false, nullable = true, length = 40)
	private String proNo;

	/** 调查人意见 **/
	@Column(name = "INVE_ADVICE", unique = false, nullable = true, length = 500)
	private String inveAdvice;

	/** 个人信用情况其他说明 **/
	@Column(name = "INDIV_CDT_EXPL", unique = false, nullable = true, length = 500)
	private String indivCdtExpl;

	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;
	
	/** 调查编号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 合同类型 **/
	@Column(name = "CONT_TYPE", unique = false, nullable = true, length = 5)
	private String contType;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;
	
	/** 合同期限 **/
	@Column(name = "CONT_TERM", unique = false, nullable = true, length = 10)
	private Integer contTerm;
	
	/** 起始日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 签约方式 **/
	@Column(name = "SIGN_MODE", unique = false, nullable = true, length = 5)
	private String signMode;
	
	/** 贷款投向 **/
	@Column(name = "LOAN_TER", unique = false, nullable = true, length = 40)
	private String loanTer;
	
	/** 其他约定 **/
	@Column(name = "OTHER_AGREED", unique = false, nullable = true, length = 500)
	private String otherAgreed;
	
	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;
	
	/** 贷款类别 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 5)
	private String loanType;
	
	/** 浮动点数 **/
	@Column(name = "RATE_FLOAT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rateFloatPoint;
	
	/** 结息方式 **/
	@Column(name = "EI_MODE", unique = false, nullable = true, length = 5)
	private String eiMode;
	
	/** 贷款发放账号 **/
	@Column(name = "LOAN_PAYOUT_ACCNO", unique = false, nullable = true, length = 40)
	private String loanPayoutAccno;
	
	/** 贷款发放账号名称 **/
	@Column(name = "LOAN_PAYOUT_ACC_NAME", unique = false, nullable = true, length = 40)
	private String loanPayoutAccName;
	
	/** 是否曾被拒绝 **/
	@Column(name = "IS_HAS_REFUSED", unique = false, nullable = true, length = 5)
	private String isHasRefused;
	
	/** 主担保方式 **/
	@Column(name = "GUAR_WAY", unique = false, nullable = true, length = 5)
	private String guarWay;
	
	/** 是否共同申请人 **/
	@Column(name = "IS_COMMON_RQSTR", unique = false, nullable = true, length = 5)
	private String isCommonRqstr;
	
	/** 是否确认支付方式 **/
	@Column(name = "IS_CFIRM_PAY_WAY", unique = false, nullable = true, length = 5)
	private String isCfirmPayWay;
	
	/** 支付方式 **/
	@Column(name = "PAY_MODE", unique = false, nullable = true, length = 5)
	private String payMode;
	
	/** 申请币种 **/
	@Column(name = "APP_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String appCurType;
	
	/** 申请金额 **/
	@Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appAmt;
	
	/** 申请汇率 **/
	@Column(name = "APP_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appRate;
	
	/** 保证金来源 **/
	@Column(name = "BAIL_SOUR", unique = false, nullable = true, length = 5)
	private String bailSour;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 保证金币种 **/
	@Column(name = "BAIL_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String bailCurType;
	
	/** 保证金金额 **/
	@Column(name = "BAIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAmt;
	
	/** 保证金汇率 **/
	@Column(name = "BAIL_EXCHANGE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailExchangeRate;
	
	/** 风险敞口金额 **/
	@Column(name = "RISK_OPEN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal riskOpenAmt;
	
	/** 期限类型 **/
	@Column(name = "TERM_TYPE", unique = false, nullable = true, length = 5)
	private String termType;
	
	/** 申请期限 **/
	@Column(name = "APP_TERM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appTerm;
	
	/** 利率依据方式 **/
	@Column(name = "IR_ACCORD_TYPE", unique = false, nullable = true, length = 5)
	private String irAccordType;
	
	/** 利率种类 **/
	@Column(name = "IR_TYPE", unique = false, nullable = true, length = 6)
	private String irType;
	
	/** 基准利率（年） **/
	@Column(name = "RULING_IR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rulingIr;
	
	/** 对应基准利率(月) **/
	@Column(name = "RULING_IR_M", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rulingIrM;
	
	/** 计息方式 **/
	@Column(name = "LOAN_RAT_TYPE", unique = false, nullable = true, length = 5)
	private String loanRatType;
	
	/** 利率调整类型 **/
	@Column(name = "IR_ADJUST_TYPE", unique = false, nullable = true, length = 5)
	private String irAdjustType;
	
	/** 利率调整周期(月) **/
	@Column(name = "IR_ADJUST_TERM", unique = false, nullable = true, length = 10)
	private Integer irAdjustTerm;
	
	/** 调息方式 **/
	@Column(name = "PRA_TYPE", unique = false, nullable = true, length = 5)
	private String praType;
	
	/** 利率形式 **/
	@Column(name = "RATE_TYPE", unique = false, nullable = true, length = 5)
	private String rateType;
	
	/** 利率浮动方式 **/
	@Column(name = "IR_FLOAT_TYPE", unique = false, nullable = true, length = 5)
	private String irFloatType;
	
	/** 利率浮动百分比 **/
	@Column(name = "IR_FLOAT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal irFloatRate;
	
	/** 执行年利率 **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;
	
	/** 执行利率(月) **/
	@Column(name = "REALITY_IR_M", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realityIrM;


	/** 逾期利率浮动百分比 **/
	@Column(name = "OVERDUE_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueRatePefloat;

	/** 逾期利率（年） **/
	@Column(name = "OVERDUE_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueExecRate;

	/** 复息利率浮动比 **/
	@Column(name = "CI_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ciRatePefloat;

	/** 复息执行利率(年利率) **/
	@Column(name = "CI_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ciExecRate;

	
	/** 申请状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 审批通过日期（精确到秒） **/
	@Column(name = "APPROVE_PASS_DATE", unique = false, nullable = true, length = 20)
	private String approvePassDate;
	
	/** 本合同项下最高可用信金额 **/
	@Column(name = "CONT_HIGH_AVL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contHighAvlAmt;
	
	/** 是否续签 **/
	@Column(name = "IS_RENEW", unique = false, nullable = true, length = 5)
	private String isRenew;
	
	/** 原合同编号 **/
	@Column(name = "ORIGI_CONT_NO", unique = false, nullable = true, length = 40)
	private String origiContNo;
	
	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;
	
	/** 授信额度编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;
	
	/** 是否电子用印 **/
	@Column(name = "IS_E_SEAL", unique = false, nullable = true, length = 5)
	private String isESeal;
	
	/** 渠道来源 **/
	@Column(name = "CHNL_SOUR", unique = false, nullable = true, length = 5)
	private String chnlSour;

	/** 第三方渠道流水号 **/
	@Column(name = "THIRD_CHNL_SERNO", unique = false, nullable = true, length = 100)
	private String thirdChnlSerno;

	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 4)
	private String belgLine;
	
	/** 债项等级 **/
	@Column(name = "DEBT_LEVEL", unique = false, nullable = true, length = 5)
	private String debtLevel;
	
	/** 违约风险暴露EAD **/
	@Column(name = "EAD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ead;
	
	/** 违约损失率LGD **/
	@Column(name = "LGD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lgd;
	
	/** 联系人 **/
	@Column(name = "LINKMAN", unique = false, nullable = true, length = 80)
	private String linkman;
	
	/** 传真 **/
	@Column(name = "FAX", unique = false, nullable = true, length = 20)
	private String fax;
	
	/** 邮箱 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 80)
	private String email;
	
	/** QQ **/
	@Column(name = "QQ", unique = false, nullable = true, length = 20)
	private String qq;
	
	/** 微信 **/
	@Column(name = "WECHAT", unique = false, nullable = true, length = 40)
	private String wechat;
	
	/** 送达地址 **/
	@Column(name = "DELIVERY_ADDR", unique = false, nullable = true, length = 500)
	private String deliveryAddr;
	
	/** 本行角色 **/
	@Column(name = "BANK_ROLE", unique = false, nullable = true, length = 40)
	private String bankRole;
	
	/** 银团总金额 **/
	@Column(name = "BKSYNDIC_TOTL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bksyndicTotlAmt;
	
	/** 还款顺序 **/
	@Column(name = "REPAY_SEQ", unique = false, nullable = true, length = 40)
	private String repaySeq;
	
	/** 银团纸质合同编号 **/
	@Column(name = "BKSYNDIC_PAPER_CONT_NO", unique = false, nullable = true, length = 40)
	private String bksyndicPaperContNo;
	
	/** 借款利率调整日 **/
	@Column(name = "LOAN_RATE_ADJ_DAY", unique = false, nullable = true, length = 5)
	private String loanRateAdjDay;
	
	/** LPR利率区间 **/
	@Column(name = "LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
	private String lprRateIntval;
	
	/** 当前LPR利率 **/
	@Column(name = "CURT_LPR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLprRate;
	
	/** 提款方式  **/
	@Column(name = "DRAW_MODE", unique = false, nullable = true, length = 5)
	private String drawMode;
	
	/** 提款期限 **/
	@Column(name = "DRAW_TERM", unique = false, nullable = true, length = 40)
	private String drawTerm;
	
	/** 开户行名称 **/
	@Column(name = "ACCTSVCR_NAME", unique = false, nullable = true, length = 40)
	private String acctsvcrName;
	
	/** 还款具体说明 **/
	@Column(name = "REPAY_DETAIL", unique = false, nullable = true, length = 500)
	private String repayDetail;
	
	/** 目标企业 **/
	@Column(name = "TARGET_CORP", unique = false, nullable = true, length = 500)
	private String targetCorp;
	
	/** 并购协议 **/
	@Column(name = "MERGER_AGR", unique = false, nullable = true, length = 500)
	private String mergerAgr;
	
	/** 并购交易价款 **/
	@Column(name = "MERGER_TRAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mergerTranAmt;
	
	/** 是否占用第三方额度 **/
	@Column(name = "IS_OUTSTND_TRD_LMT_AMT", unique = false, nullable = true, length = 5)
	private String isOutstndTrdLmtAmt;
	
	/** 第三方合同协议编号 **/
	@Column(name = "TDP_AGR_NO", unique = false, nullable = true, length = 40)
	private String tdpAgrNo;
	
	/** 合作方客户编号 **/
	@Column(name = "COOP_CUS_ID", unique = false, nullable = true, length = 20)
	private String coopCusId;
	
	/** 合作方客户名称 **/
	@Column(name = "COOP_CUS_NAME", unique = false, nullable = true, length = 80)
	private String coopCusName;
	
	/** 是否无缝对接 **/
	@Column(name = "IS_SEAJNT", unique = false, nullable = true, length = 5)
	private String isSeajnt;
	
	/** 折算人民币金额 **/
	@Column(name = "CVT_CNY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cvtCnyAmt;
	
	/** 用途分析 **/
	@Column(name = "PURP_ANALY", unique = false, nullable = true, length = 500)
	private String purpAnaly;
	
	/** 交叉核验详细分析 **/
	@Column(name = "CROSS_CHK_DETAIL_ANALY", unique = false, nullable = true, length = 500)
	private String crossChkDetailAnaly;
	
	/** 还款来源 **/
	@Column(name = "REPAY_SOUR", unique = false, nullable = true, length = 2000)
	private String repaySour;
	
	/** 调查人结论 **/
	@Column(name = "INVE_CONCLU", unique = false, nullable = true, length = 500)
	private String inveConclu;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 首付/定金 **/
	@Column(name = "FIRSTPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal firstpayAmt;
	
	/** 合同总价 **/
	@Column(name = "CONT_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contTotalAmt;
	
	/** 宽限期 **/
	@Column(name = "GRAPER", unique = false, nullable = true, length = 20)
	private String graper;
	
	/** 是否公积金组合贷款 **/
	@Column(name = "FUND_UNION_FLAG", unique = false, nullable = true, length = 5)
	private String fundUnionFlag;
	
	/** 是否线上提款 **/
	@Column(name = "IS_ONLINE_DRAW", unique = false, nullable = true, length = 10)
	private String isOnlineDraw;
	
	/** 还款日 **/
	@Column(name = "REPAY_DATE", unique = false, nullable = true, length = 5)
	private String repayDate;
	
	/** 是否先放款后抵押 **/
	@Column(name = "BEFOREHAND_IND", unique = false, nullable = true, length = 5)
	private String beforehandInd;
	
	/** 纸质合同签订日期 **/
	@Column(name = "PAPER_CONT_SIGN_DATE", unique = false, nullable = true, length = 10)
	private String paperContSignDate;
	
	/** 其他借款用途 **/
	@Column(name = "OTHER_LOAN_PURP", unique = false, nullable = true, length = 200)
	private String otherLoanPurp;

	/** 违约利率浮动百分比 **/
	@Column(name = "DEFAULT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal defaultRate;

	/** 违约利率（年） **/
	@Column(name = "DEFAULT_RATE_Y", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal defaultRateY;

	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param oldIqpSerno
	 */
	public void setOldIqpSerno(String oldIqpSerno) {
		this.oldIqpSerno = oldIqpSerno;
	}
	
    /**
     * @return oldIqpSerno
     */
	public String getOldIqpSerno() {
		return this.oldIqpSerno;
	}
	
	/**
	 * @param isReconsid
	 */
	public void setIsReconsid(String isReconsid) {
		this.isReconsid = isReconsid;
	}
	
    /**
     * @return isReconsid
     */
	public String getIsReconsid() {
		return this.isReconsid;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
    /**
     * @return bizType
     */
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param especBizType
	 */
	public void setEspecBizType(String especBizType) {
		this.especBizType = especBizType;
	}
	
    /**
     * @return especBizType
     */
	public String getEspecBizType() {
		return this.especBizType;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}
	
    /**
     * @return appDate
     */
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	
    /**
     * @return prdTypeProp
     */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp;
	}
	
    /**
     * @return loanPurp
     */
	public String getLoanPurp() {
		return this.loanPurp;
	}
	
	/**
	 * @param loanModal
	 */
	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal;
	}
	
    /**
     * @return loanModal
     */
	public String getLoanModal() {
		return this.loanModal;
	}
	
	/**
	 * @param loanCha
	 */
	public void setLoanCha(String loanCha) {
		this.loanCha = loanCha;
	}
	
    /**
     * @return loanCha
     */
	public String getLoanCha() {
		return this.loanCha;
	}
	
	/**
	 * @param preferRateFlag
	 */
	public void setPreferRateFlag(String preferRateFlag) {
		this.preferRateFlag = preferRateFlag;
	}
	
    /**
     * @return preferRateFlag
     */
	public String getPreferRateFlag() {
		return this.preferRateFlag;
	}
	
	/**
	 * @param otherComsumeRepayAmt
	 */
	public void setOtherComsumeRepayAmt(java.math.BigDecimal otherComsumeRepayAmt) {
		this.otherComsumeRepayAmt = otherComsumeRepayAmt;
	}
	
    /**
     * @return otherComsumeRepayAmt
     */
	public java.math.BigDecimal getOtherComsumeRepayAmt() {
		return this.otherComsumeRepayAmt;
	}
	
	/**
	 * @param pundLoanMonRepayAmt
	 */
	public void setPundLoanMonRepayAmt(java.math.BigDecimal pundLoanMonRepayAmt) {
		this.pundLoanMonRepayAmt = pundLoanMonRepayAmt;
	}
	
    /**
     * @return pundLoanMonRepayAmt
     */
	public java.math.BigDecimal getPundLoanMonRepayAmt() {
		return this.pundLoanMonRepayAmt;
	}
	
	/**
	 * @param monthRepayAmt
	 */
	public void setMonthRepayAmt(java.math.BigDecimal monthRepayAmt) {
		this.monthRepayAmt = monthRepayAmt;
	}
	
    /**
     * @return monthRepayAmt
     */
	public java.math.BigDecimal getMonthRepayAmt() {
		return this.monthRepayAmt;
	}
	
	/**
	 * @param isOlPld
	 */
	public void setIsOlPld(String isOlPld) {
		this.isOlPld = isOlPld;
	}
	
    /**
     * @return isOlPld
     */
	public String getIsOlPld() {
		return this.isOlPld;
	}
	
	/**
	 * @param contMode
	 */
	public void setContMode(String contMode) {
		this.contMode = contMode;
	}
	
    /**
     * @return contMode
     */
	public String getContMode() {
		return this.contMode;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
	/**
	 * @param inveAdvice
	 */
	public void setInveAdvice(String inveAdvice) {
		this.inveAdvice = inveAdvice;
	}

	/**
	 * @return proNo
	 */
	public String getProNo() {
		return this.proNo;
	}

    /**
     * @return inveAdvice
     */
	public String getInveAdvice() {
		return this.inveAdvice;
	}
	
	/**
	 * @param indivCdtExpl
	 */
	public void setIndivCdtExpl(String indivCdtExpl) {
		this.indivCdtExpl = indivCdtExpl;
	}
	
    /**
     * @return indivCdtExpl
     */
	public String getIndivCdtExpl() {
		return this.indivCdtExpl;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param contType
	 */
	public void setContType(String contType) {
		this.contType = contType;
	}
	
    /**
     * @return contType
     */
	public String getContType() {
		return this.contType;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param contTerm
	 */
	public void setContTerm(Integer contTerm) {
		this.contTerm = contTerm;
	}
	
    /**
     * @return contTerm
     */
	public Integer getContTerm() {
		return this.contTerm;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param signMode
	 */
	public void setSignMode(String signMode) {
		this.signMode = signMode;
	}
	
    /**
     * @return signMode
     */
	public String getSignMode() {
		return this.signMode;
	}
	
	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}
	
    /**
     * @return loanTer
     */
	public String getLoanTer() {
		return this.loanTer;
	}
	
	/**
	 * @param otherAgreed
	 */
	public void setOtherAgreed(String otherAgreed) {
		this.otherAgreed = otherAgreed;
	}
	
    /**
     * @return otherAgreed
     */
	public String getOtherAgreed() {
		return this.otherAgreed;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
    /**
     * @return loanType
     */
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}
	
    /**
     * @return rateFloatPoint
     */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}
	
	/**
	 * @param eiMode
	 */
	public void setEiMode(String eiMode) {
		this.eiMode = eiMode;
	}
	
    /**
     * @return eiMode
     */
	public String getEiMode() {
		return this.eiMode;
	}
	
	/**
	 * @param loanPayoutAccno
	 */
	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno;
	}
	
    /**
     * @return loanPayoutAccno
     */
	public String getLoanPayoutAccno() {
		return this.loanPayoutAccno;
	}
	
	/**
	 * @param loanPayoutAccName
	 */
	public void setLoanPayoutAccName(String loanPayoutAccName) {
		this.loanPayoutAccName = loanPayoutAccName;
	}
	
    /**
     * @return loanPayoutAccName
     */
	public String getLoanPayoutAccName() {
		return this.loanPayoutAccName;
	}
	
	/**
	 * @param isHasRefused
	 */
	public void setIsHasRefused(String isHasRefused) {
		this.isHasRefused = isHasRefused;
	}
	
    /**
     * @return isHasRefused
     */
	public String getIsHasRefused() {
		return this.isHasRefused;
	}
	
	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay;
	}
	
    /**
     * @return guarWay
     */
	public String getGuarWay() {
		return this.guarWay;
	}
	
	/**
	 * @param isCommonRqstr
	 */
	public void setIsCommonRqstr(String isCommonRqstr) {
		this.isCommonRqstr = isCommonRqstr;
	}
	
    /**
     * @return isCommonRqstr
     */
	public String getIsCommonRqstr() {
		return this.isCommonRqstr;
	}
	
	/**
	 * @param isCfirmPayWay
	 */
	public void setIsCfirmPayWay(String isCfirmPayWay) {
		this.isCfirmPayWay = isCfirmPayWay;
	}
	
    /**
     * @return isCfirmPayWay
     */
	public String getIsCfirmPayWay() {
		return this.isCfirmPayWay;
	}
	
	/**
	 * @param payMode
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	
    /**
     * @return payMode
     */
	public String getPayMode() {
		return this.payMode;
	}
	
	/**
	 * @param appCurType
	 */
	public void setAppCurType(String appCurType) {
		this.appCurType = appCurType;
	}
	
    /**
     * @return appCurType
     */
	public String getAppCurType() {
		return this.appCurType;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return appAmt
     */
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param appRate
	 */
	public void setAppRate(java.math.BigDecimal appRate) {
		this.appRate = appRate;
	}
	
    /**
     * @return appRate
     */
	public java.math.BigDecimal getAppRate() {
		return this.appRate;
	}
	
	/**
	 * @param bailSour
	 */
	public void setBailSour(String bailSour) {
		this.bailSour = bailSour;
	}
	
    /**
     * @return bailSour
     */
	public String getBailSour() {
		return this.bailSour;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param bailCurType
	 */
	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType;
	}
	
    /**
     * @return bailCurType
     */
	public String getBailCurType() {
		return this.bailCurType;
	}
	
	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}
	
    /**
     * @return bailAmt
     */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}
	
	/**
	 * @param bailExchangeRate
	 */
	public void setBailExchangeRate(java.math.BigDecimal bailExchangeRate) {
		this.bailExchangeRate = bailExchangeRate;
	}
	
    /**
     * @return bailExchangeRate
     */
	public java.math.BigDecimal getBailExchangeRate() {
		return this.bailExchangeRate;
	}
	
	/**
	 * @param riskOpenAmt
	 */
	public void setRiskOpenAmt(java.math.BigDecimal riskOpenAmt) {
		this.riskOpenAmt = riskOpenAmt;
	}
	
    /**
     * @return riskOpenAmt
     */
	public java.math.BigDecimal getRiskOpenAmt() {
		return this.riskOpenAmt;
	}
	
	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType;
	}
	
    /**
     * @return termType
     */
	public String getTermType() {
		return this.termType;
	}
	
	/**
	 * @param appTerm
	 */
	public void setAppTerm(java.math.BigDecimal appTerm) {
		this.appTerm = appTerm;
	}
	
    /**
     * @return appTerm
     */
	public java.math.BigDecimal getAppTerm() {
		return this.appTerm;
	}
	
	/**
	 * @param irAccordType
	 */
	public void setIrAccordType(String irAccordType) {
		this.irAccordType = irAccordType;
	}
	
    /**
     * @return irAccordType
     */
	public String getIrAccordType() {
		return this.irAccordType;
	}
	
	/**
	 * @param irType
	 */
	public void setIrType(String irType) {
		this.irType = irType;
	}
	
    /**
     * @return irType
     */
	public String getIrType() {
		return this.irType;
	}
	
	/**
	 * @param rulingIr
	 */
	public void setRulingIr(java.math.BigDecimal rulingIr) {
		this.rulingIr = rulingIr;
	}
	
    /**
     * @return rulingIr
     */
	public java.math.BigDecimal getRulingIr() {
		return this.rulingIr;
	}
	
	/**
	 * @param rulingIrM
	 */
	public void setRulingIrM(java.math.BigDecimal rulingIrM) {
		this.rulingIrM = rulingIrM;
	}
	
    /**
     * @return rulingIrM
     */
	public java.math.BigDecimal getRulingIrM() {
		return this.rulingIrM;
	}
	
	/**
	 * @param loanRatType
	 */
	public void setLoanRatType(String loanRatType) {
		this.loanRatType = loanRatType;
	}
	
    /**
     * @return loanRatType
     */
	public String getLoanRatType() {
		return this.loanRatType;
	}
	
	/**
	 * @param irAdjustType
	 */
	public void setIrAdjustType(String irAdjustType) {
		this.irAdjustType = irAdjustType;
	}
	
    /**
     * @return irAdjustType
     */
	public String getIrAdjustType() {
		return this.irAdjustType;
	}
	
	/**
	 * @param irAdjustTerm
	 */
	public void setIrAdjustTerm(Integer irAdjustTerm) {
		this.irAdjustTerm = irAdjustTerm;
	}
	
    /**
     * @return irAdjustTerm
     */
	public Integer getIrAdjustTerm() {
		return this.irAdjustTerm;
	}
	
	/**
	 * @param praType
	 */
	public void setPraType(String praType) {
		this.praType = praType;
	}
	
    /**
     * @return praType
     */
	public String getPraType() {
		return this.praType;
	}
	
	/**
	 * @param rateType
	 */
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	
    /**
     * @return rateType
     */
	public String getRateType() {
		return this.rateType;
	}
	
	/**
	 * @param irFloatType
	 */
	public void setIrFloatType(String irFloatType) {
		this.irFloatType = irFloatType;
	}
	
    /**
     * @return irFloatType
     */
	public String getIrFloatType() {
		return this.irFloatType;
	}
	
	/**
	 * @param irFloatRate
	 */
	public void setIrFloatRate(java.math.BigDecimal irFloatRate) {
		this.irFloatRate = irFloatRate;
	}
	
    /**
     * @return irFloatRate
     */
	public java.math.BigDecimal getIrFloatRate() {
		return this.irFloatRate;
	}

	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return execRateYear
     */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param realityIrM
	 */
	public void setRealityIrM(java.math.BigDecimal realityIrM) {
		this.realityIrM = realityIrM;
	}
	
    /**
     * @return realityIrM
     */
	public java.math.BigDecimal getRealityIrM() {
		return this.realityIrM;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param approvePassDate
	 */
	public void setApprovePassDate(String approvePassDate) {
		this.approvePassDate = approvePassDate;
	}
	
    /**
     * @return approvePassDate
     */
	public String getApprovePassDate() {
		return this.approvePassDate;
	}
	
	/**
	 * @param contHighAvlAmt
	 */
	public void setContHighAvlAmt(java.math.BigDecimal contHighAvlAmt) {
		this.contHighAvlAmt = contHighAvlAmt;
	}
	
    /**
     * @return contHighAvlAmt
     */
	public java.math.BigDecimal getContHighAvlAmt() {
		return this.contHighAvlAmt;
	}
	
	/**
	 * @param isRenew
	 */
	public void setIsRenew(String isRenew) {
		this.isRenew = isRenew;
	}
	
    /**
     * @return isRenew
     */
	public String getIsRenew() {
		return this.isRenew;
	}
	
	/**
	 * @param origiContNo
	 */
	public void setOrigiContNo(String origiContNo) {
		this.origiContNo = origiContNo;
	}
	
    /**
     * @return origiContNo
     */
	public String getOrigiContNo() {
		return this.origiContNo;
	}
	
	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}
	
    /**
     * @return isUtilLmt
     */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param isESeal
	 */
	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal;
	}
	
    /**
     * @return isESeal
     */
	public String getIsESeal() {
		return this.isESeal;
	}
	
	/**
	 * @param chnlSour
	 */
	public void setChnlSour(String chnlSour) {
		this.chnlSour = chnlSour;
	}
	
    /**
     * @return chnlSour
     */
	public String getChnlSour() {
		return this.chnlSour;
	}

	/**
	 * @param thirdChnlSerno
	 */
	public void setThirdChnlSerno(String thirdChnlSerno) {
		this.thirdChnlSerno = thirdChnlSerno;
	}

	/**
	 * @return thirdChnlSerno
	 */
	public String getThirdChnlSerno() {
		return this.thirdChnlSerno;
	}

	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}
	
    /**
     * @return belgLine
     */
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param debtLevel
	 */
	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel;
	}
	
    /**
     * @return debtLevel
     */
	public String getDebtLevel() {
		return this.debtLevel;
	}
	
	/**
	 * @param ead
	 */
	public void setEad(java.math.BigDecimal ead) {
		this.ead = ead;
	}
	
    /**
     * @return ead
     */
	public java.math.BigDecimal getEad() {
		return this.ead;
	}
	
	/**
	 * @param lgd
	 */
	public void setLgd(java.math.BigDecimal lgd) {
		this.lgd = lgd;
	}
	
    /**
     * @return lgd
     */
	public java.math.BigDecimal getLgd() {
		return this.lgd;
	}
	
	/**
	 * @param linkman
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}
	
    /**
     * @return linkman
     */
	public String getLinkman() {
		return this.linkman;
	}
	
	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	
    /**
     * @return fax
     */
	public String getFax() {
		return this.fax;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
    /**
     * @return email
     */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}
	
    /**
     * @return qq
     */
	public String getQq() {
		return this.qq;
	}
	
	/**
	 * @param wechat
	 */
	public void setWechat(String wechat) {
		this.wechat = wechat;
	}
	
    /**
     * @return wechat
     */
	public String getWechat() {
		return this.wechat;
	}
	
	/**
	 * @param deliveryAddr
	 */
	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr;
	}
	
    /**
     * @return deliveryAddr
     */
	public String getDeliveryAddr() {
		return this.deliveryAddr;
	}
	
	/**
	 * @param bankRole
	 */
	public void setBankRole(String bankRole) {
		this.bankRole = bankRole;
	}
	
    /**
     * @return bankRole
     */
	public String getBankRole() {
		return this.bankRole;
	}
	
	/**
	 * @param bksyndicTotlAmt
	 */
	public void setBksyndicTotlAmt(java.math.BigDecimal bksyndicTotlAmt) {
		this.bksyndicTotlAmt = bksyndicTotlAmt;
	}
	
    /**
     * @return bksyndicTotlAmt
     */
	public java.math.BigDecimal getBksyndicTotlAmt() {
		return this.bksyndicTotlAmt;
	}
	
	/**
	 * @param repaySeq
	 */
	public void setRepaySeq(String repaySeq) {
		this.repaySeq = repaySeq;
	}
	
    /**
     * @return repaySeq
     */
	public String getRepaySeq() {
		return this.repaySeq;
	}
	
	/**
	 * @param bksyndicPaperContNo
	 */
	public void setBksyndicPaperContNo(String bksyndicPaperContNo) {
		this.bksyndicPaperContNo = bksyndicPaperContNo;
	}
	
    /**
     * @return bksyndicPaperContNo
     */
	public String getBksyndicPaperContNo() {
		return this.bksyndicPaperContNo;
	}
	
	/**
	 * @param loanRateAdjDay
	 */
	public void setLoanRateAdjDay(String loanRateAdjDay) {
		this.loanRateAdjDay = loanRateAdjDay;
	}
	
    /**
     * @return loanRateAdjDay
     */
	public String getLoanRateAdjDay() {
		return this.loanRateAdjDay;
	}
	
	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval;
	}
	
    /**
     * @return lprRateIntval
     */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}
	
	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}
	
    /**
     * @return curtLprRate
     */
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}
	
	/**
	 * @param drawMode
	 */
	public void setDrawMode(String drawMode) {
		this.drawMode = drawMode;
	}
	
    /**
     * @return drawMode
     */
	public String getDrawMode() {
		return this.drawMode;
	}
	
	/**
	 * @param drawTerm
	 */
	public void setDrawTerm(String drawTerm) {
		this.drawTerm = drawTerm;
	}
	
    /**
     * @return drawTerm
     */
	public String getDrawTerm() {
		return this.drawTerm;
	}
	
	/**
	 * @param acctsvcrName
	 */
	public void setAcctsvcrName(String acctsvcrName) {
		this.acctsvcrName = acctsvcrName;
	}
	
    /**
     * @return acctsvcrName
     */
	public String getAcctsvcrName() {
		return this.acctsvcrName;
	}
	
	/**
	 * @param repayDetail
	 */
	public void setRepayDetail(String repayDetail) {
		this.repayDetail = repayDetail;
	}
	
    /**
     * @return repayDetail
     */
	public String getRepayDetail() {
		return this.repayDetail;
	}
	
	/**
	 * @param targetCorp
	 */
	public void setTargetCorp(String targetCorp) {
		this.targetCorp = targetCorp;
	}
	
    /**
     * @return targetCorp
     */
	public String getTargetCorp() {
		return this.targetCorp;
	}
	
	/**
	 * @param mergerAgr
	 */
	public void setMergerAgr(String mergerAgr) {
		this.mergerAgr = mergerAgr;
	}
	
    /**
     * @return mergerAgr
     */
	public String getMergerAgr() {
		return this.mergerAgr;
	}
	
	/**
	 * @param mergerTranAmt
	 */
	public void setMergerTranAmt(java.math.BigDecimal mergerTranAmt) {
		this.mergerTranAmt = mergerTranAmt;
	}
	
    /**
     * @return mergerTranAmt
     */
	public java.math.BigDecimal getMergerTranAmt() {
		return this.mergerTranAmt;
	}
	
	/**
	 * @param isOutstndTrdLmtAmt
	 */
	public void setIsOutstndTrdLmtAmt(String isOutstndTrdLmtAmt) {
		this.isOutstndTrdLmtAmt = isOutstndTrdLmtAmt;
	}
	
    /**
     * @return isOutstndTrdLmtAmt
     */
	public String getIsOutstndTrdLmtAmt() {
		return this.isOutstndTrdLmtAmt;
	}
	
	/**
	 * @param tdpAgrNo
	 */
	public void setTdpAgrNo(String tdpAgrNo) {
		this.tdpAgrNo = tdpAgrNo;
	}
	
    /**
     * @return tdpAgrNo
     */
	public String getTdpAgrNo() {
		return this.tdpAgrNo;
	}
	
	/**
	 * @param coopCusId
	 */
	public void setCoopCusId(String coopCusId) {
		this.coopCusId = coopCusId;
	}
	
    /**
     * @return coopCusId
     */
	public String getCoopCusId() {
		return this.coopCusId;
	}
	
	/**
	 * @param coopCusName
	 */
	public void setCoopCusName(String coopCusName) {
		this.coopCusName = coopCusName;
	}
	
    /**
     * @return coopCusName
     */
	public String getCoopCusName() {
		return this.coopCusName;
	}
	
	/**
	 * @param isSeajnt
	 */
	public void setIsSeajnt(String isSeajnt) {
		this.isSeajnt = isSeajnt;
	}
	
    /**
     * @return isSeajnt
     */
	public String getIsSeajnt() {
		return this.isSeajnt;
	}
	
	/**
	 * @param cvtCnyAmt
	 */
	public void setCvtCnyAmt(java.math.BigDecimal cvtCnyAmt) {
		this.cvtCnyAmt = cvtCnyAmt;
	}
	
    /**
     * @return cvtCnyAmt
     */
	public java.math.BigDecimal getCvtCnyAmt() {
		return this.cvtCnyAmt;
	}
	
	/**
	 * @param purpAnaly
	 */
	public void setPurpAnaly(String purpAnaly) {
		this.purpAnaly = purpAnaly;
	}
	
    /**
     * @return purpAnaly
     */
	public String getPurpAnaly() {
		return this.purpAnaly;
	}
	
	/**
	 * @param crossChkDetailAnaly
	 */
	public void setCrossChkDetailAnaly(String crossChkDetailAnaly) {
		this.crossChkDetailAnaly = crossChkDetailAnaly;
	}
	
    /**
     * @return crossChkDetailAnaly
     */
	public String getCrossChkDetailAnaly() {
		return this.crossChkDetailAnaly;
	}
	
	/**
	 * @param repaySour
	 */
	public void setRepaySour(String repaySour) {
		this.repaySour = repaySour;
	}
	
    /**
     * @return repaySour
     */
	public String getRepaySour() {
		return this.repaySour;
	}
	
	/**
	 * @param inveConclu
	 */
	public void setInveConclu(String inveConclu) {
		this.inveConclu = inveConclu;
	}
	
    /**
     * @return inveConclu
     */
	public String getInveConclu() {
		return this.inveConclu;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param firstpayAmt
	 */
	public void setFirstpayAmt(java.math.BigDecimal firstpayAmt) {
		this.firstpayAmt = firstpayAmt;
	}
	
    /**
     * @return firstpayAmt
     */
	public java.math.BigDecimal getFirstpayAmt() {
		return this.firstpayAmt;
	}
	
	/**
	 * @param contTotalAmt
	 */
	public void setContTotalAmt(java.math.BigDecimal contTotalAmt) {
		this.contTotalAmt = contTotalAmt;
	}
	
    /**
     * @return contTotalAmt
     */
	public java.math.BigDecimal getContTotalAmt() {
		return this.contTotalAmt;
	}
	
	/**
	 * @param graper
	 */
	public void setGraper(String graper) {
		this.graper = graper;
	}
	
    /**
     * @return graper
     */
	public String getGraper() {
		return this.graper;
	}
	
	/**
	 * @param fundUnionFlag
	 */
	public void setFundUnionFlag(String fundUnionFlag) {
		this.fundUnionFlag = fundUnionFlag;
	}
	
    /**
     * @return fundUnionFlag
     */
	public String getFundUnionFlag() {
		return this.fundUnionFlag;
	}
	
	/**
	 * @param isOnlineDraw
	 */
	public void setIsOnlineDraw(String isOnlineDraw) {
		this.isOnlineDraw = isOnlineDraw;
	}
	
    /**
     * @return isOnlineDraw
     */
	public String getIsOnlineDraw() {
		return this.isOnlineDraw;
	}
	
	/**
	 * @param repayDate
	 */
	public void setRepayDate(String repayDate) {
		this.repayDate = repayDate;
	}
	
    /**
     * @return repayDate
     */
	public String getRepayDate() {
		return this.repayDate;
	}
	
	/**
	 * @param beforehandInd
	 */
	public void setBeforehandInd(String beforehandInd) {
		this.beforehandInd = beforehandInd;
	}
	
    /**
     * @return beforehandInd
     */
	public String getBeforehandInd() {
		return this.beforehandInd;
	}
	
	/**
	 * @param paperContSignDate
	 */
	public void setPaperContSignDate(String paperContSignDate) {
		this.paperContSignDate = paperContSignDate;
	}
	
    /**
     * @return paperContSignDate
     */
	public String getPaperContSignDate() {
		return this.paperContSignDate;
	}
	
	/**
	 * @param otherLoanPurp
	 */
	public void setOtherLoanPurp(String otherLoanPurp) {
		this.otherLoanPurp = otherLoanPurp;
	}
	
    /**
     * @return otherLoanPurp
     */
	public String getOtherLoanPurp() {
		return this.otherLoanPurp;
	}

	public BigDecimal getOverdueRatePefloat() {
		return overdueRatePefloat;
	}

	public void setOverdueRatePefloat(BigDecimal overdueRatePefloat) {
		this.overdueRatePefloat = overdueRatePefloat;
	}

	public BigDecimal getOverdueExecRate() {
		return overdueExecRate;
	}

	public void setOverdueExecRate(BigDecimal overdueExecRate) {
		this.overdueExecRate = overdueExecRate;
	}

	public BigDecimal getCiRatePefloat() {
		return ciRatePefloat;
	}

	public void setCiRatePefloat(BigDecimal ciRatePefloat) {
		this.ciRatePefloat = ciRatePefloat;
	}

	public BigDecimal getCiExecRate() {
		return ciExecRate;
	}

	public void setCiExecRate(BigDecimal ciExecRate) {
		this.ciExecRate = ciExecRate;
	}

	public BigDecimal getDefaultRate() {
		return defaultRate;
	}

	public void setDefaultRate(BigDecimal defaultRate) {
		this.defaultRate = defaultRate;
	}

	public BigDecimal getDefaultRateY() {
		return defaultRateY;
	}

	public void setDefaultRateY(BigDecimal defaultRateY) {
		this.defaultRateY = defaultRateY;
	}


}