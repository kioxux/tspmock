package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CreditReportQryLstAndRealDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.CtrEntrustLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.PvpEntrustLoanAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.server.cmiscfg0001.req.CmisCfg0001ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0001.resp.CmisCfg0001RespDto;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpEntrustLoanAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zxz
 * @创建时间: 2021-04-19 16:56:29
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpEntrustLoanAppService {

    private static final Logger log = LoggerFactory.getLogger(PvpEntrustLoanAppService.class);

    @Autowired
    private PvpEntrustLoanAppMapper pvpEntrustLoanAppMapper;

    @Autowired
    private ToppAcctSubService toppAcctSubService;

    @Autowired
    private CtrEntrustLoanContMapper ctrEntrustLoanContMapper;

    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;

    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;

    @Autowired
    private AccEntrustLoanService accEntrustLoanService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public PvpEntrustLoanApp selectByPrimaryKey(String serno) {
        return pvpEntrustLoanAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<PvpEntrustLoanApp> selectAll(QueryModel model) {
        List<PvpEntrustLoanApp> records = (List<PvpEntrustLoanApp>) pvpEntrustLoanAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PvpEntrustLoanApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpEntrustLoanApp> list = pvpEntrustLoanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PvpEntrustLoanApp record) {
        return pvpEntrustLoanAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PvpEntrustLoanApp record) {
        return pvpEntrustLoanAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PvpEntrustLoanApp record) {
        return pvpEntrustLoanAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PvpEntrustLoanApp record) {
        return pvpEntrustLoanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return pvpEntrustLoanAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pvpEntrustLoanAppMapper.deleteByIds(ids);
    }

    /**
     * 委托贷款
     *
     * @param pvpEntrustLoanApp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map savepvpEntrustLoanApp(PvpEntrustLoanApp pvpEntrustLoanApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (pvpEntrustLoanApp == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                String contNo = pvpEntrustLoanApp.getContNo();
                HashMap<String, String> queryMap = new HashMap<>();
                queryMap.put("contNo", contNo);
                CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.queryCtrEntrustLoanByDataParams(queryMap);
                IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectByIqpEntrustLoanSernoKey(ctrEntrustLoanCont.getSerno());
                //BeanUtils.copyProperties(lmtAppr, newLmtAppr);
                Map seqMap = new HashMap();
                seqMap.put("contNo",contNo);
                String billNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO, seqMap);//借据编号
                pvpEntrustLoanApp.setBillNo(billNo);//借据编号
                pvpEntrustLoanApp.setPrdId(ctrEntrustLoanCont.getPrdId());//产品编号
                pvpEntrustLoanApp.setPrdName(ctrEntrustLoanCont.getPrdName());//产品名称
                pvpEntrustLoanApp.setGuarMode(ctrEntrustLoanCont.getGuarMode());//担保方式
                pvpEntrustLoanApp.setContAmt(ctrEntrustLoanCont.getContAmt());//合同金额
                pvpEntrustLoanApp.setCurType(ctrEntrustLoanCont.getCurType());//贷款发放币
                pvpEntrustLoanApp.setContHighDisb(ctrEntrustLoanCont.getContHighAvlAmt());//本合同项下最高可用金额
                pvpEntrustLoanApp.setStartDate(ctrEntrustLoanCont.getStartDate());//合同起始日
                pvpEntrustLoanApp.setEndDate(ctrEntrustLoanCont.getEndDate());//合同到期日
                pvpEntrustLoanApp.setIsUtilLmt(ctrEntrustLoanCont.getIsUtilLmt());//是否使用授信额度
                pvpEntrustLoanApp.setLoanStartDate(ctrEntrustLoanCont.getStartDate());//贷款起始日期
                pvpEntrustLoanApp.setLoanEndDate(ctrEntrustLoanCont.getEndDate());//贷款到期日期
                pvpEntrustLoanApp.setRateAdjMode(ctrEntrustLoanCont.getRateAdjMode());//利率调整方式
                pvpEntrustLoanApp.setLprRateIntval(ctrEntrustLoanCont.getLprRateIntval());//LPR授信利率区间
                pvpEntrustLoanApp.setCurtLprRate(ctrEntrustLoanCont.getCurtLprRate());//当前LPR利率
                pvpEntrustLoanApp.setLoanTermUnit("M");//贷款期限单位
                if (null == ctrEntrustLoanCont.getRateFloatPoint() || "".equals(ctrEntrustLoanCont.getRateFloatPoint())) {
                    pvpEntrustLoanApp.setRateFloatPoint(BigDecimal.ZERO);//浮动点数
                } else {
                    pvpEntrustLoanApp.setRateFloatPoint(new BigDecimal(ctrEntrustLoanCont.getRateFloatPoint()));//浮动点数
                }
                pvpEntrustLoanApp.setExecRateYear(ctrEntrustLoanCont.getExecRateYear());//执行利率(年)
                pvpEntrustLoanApp.setRepayMode(ctrEntrustLoanCont.getRepayMode());//还款方式
                pvpEntrustLoanApp.setLoanPayoutAccno(ctrEntrustLoanCont.getLoanPayoutAccno());//贷款发放账号
                pvpEntrustLoanApp.setPayoutAcctName(ctrEntrustLoanCont.getLoanPayoutAcctName());//发放账号名称
                pvpEntrustLoanApp.setLoanTer(ctrEntrustLoanCont.getLoanTer());//贷款投向
                pvpEntrustLoanApp.setLmtAccNo(ctrEntrustLoanCont.getLmtAccNo());//授信额度编号
                pvpEntrustLoanApp.setReplyNo(iqpEntrustLoanApp.getReplyNo());//批复编号
                pvpEntrustLoanApp.setConsignorCusId(ctrEntrustLoanCont.getConsignorCusId());//委托人客户编号
                pvpEntrustLoanApp.setConsignorCusName(ctrEntrustLoanCont.getConsignorCusName());//委托人名称
                pvpEntrustLoanApp.setCsgnLoanChrgCollectType(ctrEntrustLoanCont.getCsgnLoanChrgCollectType());//委托贷款手续费收取方式
                pvpEntrustLoanApp.setCsgnLoanChrgRate(ctrEntrustLoanCont.getCsgnLoanChrgCollectRate());//委托贷款手续费比例

                pvpEntrustLoanApp.setIsHolidayDelay(CmisCommonConstants.YES_NO_1);//节假日是否顺延
                pvpEntrustLoanApp.setInputId(userInfo.getLoginCode());
                pvpEntrustLoanApp.setInputBrId(userInfo.getOrg().getCode());
                pvpEntrustLoanApp.setManagerId(userInfo.getLoginCode());
                pvpEntrustLoanApp.setManagerBrId(userInfo.getOrg().getCode());
                pvpEntrustLoanApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                pvpEntrustLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            }

            Map seqMap = new HashMap();

            String pvpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, seqMap);

            pvpEntrustLoanApp.setPvpSerno(pvpSerno);
            pvpEntrustLoanApp.setPkId(StringUtils.uuid(true));

            int insertCount = pvpEntrustLoanAppMapper.insertSelective(pvpEntrustLoanApp);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("pvpSerno", pvpSerno);
            log.info("委托贷款" + pvpSerno + "-保存成功！");
            //引入合同中的还本计划
            this.getRepayPlanFromCont(pvpSerno);
            //引入合同中的交易对手信息
            this.getToppAcctInfoFromCont(pvpSerno);
            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(pvpEntrustLoanApp.getCusId());
            creditReportQryLstAndRealDto.setCusId(pvpEntrustLoanApp.getCusId());
            creditReportQryLstAndRealDto.setCusName(pvpEntrustLoanApp.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(pvpSerno);
            creditReportQryLstAndRealDto.setScene("03");
            if ("1".equals(qryCls)) {
                creditReportQryLstAndRealDto.setQryResn("17");
            }
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        } catch (YuspException e) {
            log.error("委托贷款新增保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("委托贷款异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 引入合同中的还本计划
     *
     * @param pvpSerno
     * @return
     */
    public void getRepayPlanFromCont(String pvpSerno) {
        PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);
        if (Objects.isNull(pvpEntrustLoanApp)) {
            log.info("根据出账流水号【{}】，未查询到对应的出账申请信息", pvpSerno);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        // 还款方式（按期付息按计划还本时引入合同中的还本计划）
        String repayMode = pvpEntrustLoanApp.getRepayMode();
        if (repayMode != null && !"".equals(repayMode) && CmisCommonConstants.STD_REPAY_MODE_A040.equals(repayMode)) {
            CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByPrimaryKey(pvpEntrustLoanApp.getContNo());
            if (Objects.isNull(ctrEntrustLoanCont)) {
                log.error("根据合同号【{}】，未查询到对应的出账申请信息", pvpEntrustLoanApp.getContNo());
                throw BizException.error(null, EcbEnum.ECB020025.key, EcbEnum.ECB020025.value);
            }
            // 合同总金额（折合人民币）
            BigDecimal contTotalRepayAmt = ctrEntrustLoanCont.getContHighAvlAmt();
            // 出账总金额（折合人民币）
            BigDecimal pvpTotalRepayAmt = pvpEntrustLoanApp.getPvpAmt();
            // 得到合同中的还本计划
            RepayCapPlan repayCapPlan = new RepayCapPlan();
            List<RepayCapPlan> repayCapPlanList = repayCapPlanService.selectBySerno(ctrEntrustLoanCont.getSerno());

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                log.error("获取当前登录人信息异常！");
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            }
            for (int i = 0; i < repayCapPlanList.size(); i++) {
                // 每一期的还本计划
                RepayCapPlan repayCapPlanData = repayCapPlanList.get(i);
                if (i == repayCapPlanList.size() - 1) {
                    // 除去最后一期的所有期数金额之和
                    BigDecimal excludeLastAmt = BigDecimal.ZERO;
                    // 最后一期金额
                    BigDecimal lastRepayAmt = BigDecimal.ZERO;
                    List<RepayCapPlan> repayCapPlans = repayCapPlanService.selectBySerno(pvpEntrustLoanApp.getPvpSerno());
                    for (RepayCapPlan capPlan : repayCapPlans) {
                        excludeLastAmt = excludeLastAmt.add(capPlan.getRepayAmt());
                        lastRepayAmt = pvpEntrustLoanApp.getPvpAmt().subtract(excludeLastAmt);
                    }
                    repayCapPlan.setPkId(StringUtils.uuid(true));
                    repayCapPlan.setBillNo(pvpEntrustLoanApp.getBillNo());
                    repayCapPlan.setSerno(pvpEntrustLoanApp.getPvpSerno());
                    repayCapPlan.setTerms(repayCapPlanData.getTerms());
                    repayCapPlan.setRepayDate(repayCapPlanData.getRepayDate());
                    repayCapPlan.setRepayAmt(lastRepayAmt);
                    repayCapPlan.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    repayCapPlan.setInputId(userInfo.getLoginCode());
                    repayCapPlan.setInputBrId(userInfo.getOrg().getCode());
                    repayCapPlan.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                    int insertCount = repayCapPlanService.insert(repayCapPlan);
                    if (insertCount <= 0) {
                        log.error("引入还本计划【{}】异常", pvpEntrustLoanApp.getPvpSerno());
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                } else {
                    repayCapPlan.setPkId(StringUtils.uuid(true));
                    repayCapPlan.setBillNo(pvpEntrustLoanApp.getBillNo());
                    repayCapPlan.setSerno(pvpEntrustLoanApp.getPvpSerno());
                    repayCapPlan.setTerms(repayCapPlanData.getTerms());
                    repayCapPlan.setRepayDate(repayCapPlanData.getRepayDate());
                    BigDecimal everyRepayAmt = (pvpTotalRepayAmt.multiply(repayCapPlanData.getRepayAmt())).divide(contTotalRepayAmt, 2, BigDecimal.ROUND_UP);
                    repayCapPlan.setRepayAmt(everyRepayAmt);
                    repayCapPlan.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    repayCapPlan.setInputId(userInfo.getLoginCode());
                    repayCapPlan.setInputBrId(userInfo.getOrg().getCode());
                    repayCapPlan.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                    int insertCount = repayCapPlanService.insert(repayCapPlan);
                    if (insertCount <= 0) {
                        log.error("引入还本计划【{}】异常", pvpEntrustLoanApp.getPvpSerno());
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                }
            }
        }
    }

    /**
     * 引入合同中的还本计划
     *
     * @param pvpSerno
     * @return
     */
    public void getToppAcctInfoFromCont(String pvpSerno) {
        PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);
        if (Objects.isNull(pvpEntrustLoanApp)) {
            log.info("根据出账流水号【{}】，未查询到对应的出账申请信息", pvpSerno);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        // 支付方式（受托支付时引入合同中的交易对手信息）
        String isBeEntrustedPay = pvpEntrustLoanApp.getIsBeEntrustedPay();
        if (isBeEntrustedPay != null && !"".equals(isBeEntrustedPay) && CmisCommonConstants.STD_RAY_MODE_1.equals(isBeEntrustedPay)) {
            CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByPrimaryKey(pvpEntrustLoanApp.getContNo());
            if (Objects.isNull(ctrEntrustLoanCont)) {
                log.error("根据合同号【{}】，未查询到对应的出账申请信息", ctrEntrustLoanCont.getContNo());
                throw BizException.error(null, EcbEnum.ECB020025.key, EcbEnum.ECB020025.value);
            }
            // 得到合同中的交易对手信息
            ToppAcctSub toppAcctSub = new ToppAcctSub();
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", ctrEntrustLoanCont.getSerno());
            List<ToppAcctSub> toppAcctSubList = toppAcctSubService.selectBySerno(queryModel);

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                log.error("获取当前登录人信息异常！");
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            }
            for (ToppAcctSub acctSub : toppAcctSubList) {
                BeanUtils.copyProperties(acctSub, toppAcctSub);
                toppAcctSub.setBizSerno(pvpSerno);
                toppAcctSub.setPkId(StringUtils.uuid(true));
                toppAcctSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                toppAcctSub.setInputId(userInfo.getLoginCode());
                toppAcctSub.setInputBrId(userInfo.getOrg().getCode());
                toppAcctSub.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                int insertCount = toppAcctSubService.insertSelective(toppAcctSub);
                if (insertCount <= 0) {
                    log.error("引入交易对手【{}】异常", pvpEntrustLoanApp.getPvpSerno());
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            }
        }
    }

    /**
     * 委托贷款
     *
     * @param pvpEntrustLoanApp
     * @returnf
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonUpdatePvpEntrustLoanApp(PvpEntrustLoanApp pvpEntrustLoanApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (pvpEntrustLoanApp == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                pvpEntrustLoanApp.setUpdId(userInfo.getLoginCode());
                pvpEntrustLoanApp.setUpdBrId(userInfo.getOrg().getCode());
                pvpEntrustLoanApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                pvpEntrustLoanApp.setOprType("01");
            }

            Map seqMap = new HashMap();


            int updCount = pvpEntrustLoanAppMapper.updateByPrimaryKeySelective(pvpEntrustLoanApp);
            if (updCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }


        } catch (YuspException e) {
            log.error("委托贷款新增保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("委托贷款异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param pvpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取委托贷款申请" + pvpSerno + "申请主表信息");
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);
            if (pvpEntrustLoanApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新委托贷款申请" + pvpSerno + "流程审批状态为【111】-审批中");
            int updateCount = this.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_111);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            // 发起流程状态之前，发起占额
            if(Objects.equals(CmisCommonConstants.WF_STATUS_000,pvpEntrustLoanApp.getApproveStatus())) {
                //获取贸易融资合同数据，并根据合同中的业务类型进行数据判断
                log.info("贸易融资放款申请发起流程-获取放款申请" + pvpSerno + "获取业务合同数据");
                //判断是否低风险占额
                String guarMode = pvpEntrustLoanApp.getGuarMode();
                BigDecimal BizAmtCny = BigDecimal.ZERO;
                BigDecimal bizSpacAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    BizAmtCny = pvpEntrustLoanApp.getPvpAmt();
                    bizSpacAmtCny = BigDecimal.ZERO;
                } else {
                    BizAmtCny = pvpEntrustLoanApp.getPvpAmt();
                    bizSpacAmtCny = pvpEntrustLoanApp.getPvpAmt();
                }
                //发台账占用接口占合同额度
                CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
                cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0013ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpEntrustLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0013ReqDto.setSerno(pvpEntrustLoanApp.getBillNo());//交易流水号
                cmisLmt0013ReqDto.setBizNo(pvpEntrustLoanApp.getContNo());//合同编号
                cmisLmt0013ReqDto.setInputId(pvpEntrustLoanApp.getInputId());//登记人
                cmisLmt0013ReqDto.setInputBrId(pvpEntrustLoanApp.getInputBrId());//登记机构
                cmisLmt0013ReqDto.setInputDate(pvpEntrustLoanApp.getInputDate());//登记日期
                List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDto = new ArrayList<CmisLmt0013ReqDealBizListDto>();
                CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizDto = new CmisLmt0013ReqDealBizListDto();
                cmisLmt0013ReqDealBizDto.setDealBizNo(pvpEntrustLoanApp.getBillNo());//台账编号
                cmisLmt0013ReqDealBizDto.setIsFollowBiz("0");//是否无缝衔接
                cmisLmt0013ReqDealBizDto.setOrigiDealBizNo("");//原交易业务编号
                cmisLmt0013ReqDealBizDto.setOrigiDealBizStatus("");//原交易业务状态
                cmisLmt0013ReqDealBizDto.setOrigiRecoverType("");//原交易业务恢复类型
                cmisLmt0013ReqDealBizDto.setOrigiBizAttr("");//原交易属性
                cmisLmt0013ReqDealBizDto.setCusId(pvpEntrustLoanApp.getCusId());//客户编号
                cmisLmt0013ReqDealBizDto.setCusName(pvpEntrustLoanApp.getCusName());//客户名称
                cmisLmt0013ReqDealBizDto.setPrdId(pvpEntrustLoanApp.getPrdId());//产品编号
                cmisLmt0013ReqDealBizDto.setPrdName(pvpEntrustLoanApp.getPrdName());//产品名称
                cmisLmt0013ReqDealBizDto.setDealBizAmtCny(BizAmtCny);//台账占用总额(人民币)
                cmisLmt0013ReqDealBizDto.setDealBizSpacAmtCny(bizSpacAmtCny);//台账占用敞口(人民币)
                cmisLmt0013ReqDealBizDto.setDealBizBailPreRate(BigDecimal.ZERO);//保证金比例
                cmisLmt0013ReqDealBizDto.setDealBizBailPreAmt(BigDecimal.ZERO);//保证金金额
                cmisLmt0013ReqDealBizDto.setStartDate(pvpEntrustLoanApp.getLoanStartDate());//台账起始日
                cmisLmt0013ReqDealBizDto.setEndDate(pvpEntrustLoanApp.getLoanStartDate());//台账到期日
                cmisLmt0013ReqDealBizDto.setDealBizStatus("100");//台账状态
                cmisLmt0013ReqDealBizListDto.add(cmisLmt0013ReqDealBizDto);
                cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDto);
                log.info("放款申请发起流程-更新放款申请" + pvpSerno + "，前往额度系统进行资金业务额度同步开始");
                ResultDto<CmisLmt0013RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0013(cmisLmt0013ReqDto);
                log.info("放款申请发起流程-更新放款申请" + pvpSerno + "，前往额度系统进行资金业务额度同步结束");
                if (Objects.isNull(resultDtoDto.getData()) || !SuccessEnum.SUCCESS.key.equals(resultDtoDto.getData().getErrorCode())) {
                    String code = resultDtoDto.getData().getErrorCode();
                    log.error("出账申请占用额度异常！");
                    throw new YuspException("额度占用接口返回码：" + code + "，贷款出账申请流水号：" + pvpSerno + "，额度占用接口返回信息:", resultDtoDto.getData().getErrorMsg());
                }
            }

        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("贴现业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 通过放款申请流水号更新放款申请状态
     * 释放额度
     *
     * @param pvpSerno
     */
    public void handleBusinessAfterCallBack(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请打回流程-获取放款申请" + pvpSerno + "申请信息");
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);

            if (pvpEntrustLoanApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }

            int updCount = 0;
            log.info("放款申请发起流程-更新放款申请" + pvpSerno + "流程状态-退回");
            updCount = updateApprStatus(pvpSerno,CmisCommonConstants.WF_STATUS_992);
            if (updCount <= 0) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + pvpSerno + "流程打回业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【解除】，业务与担保合同关系结果表数据更新为【解除】
     * 20210112 该需求待确认
     * 20210113 放款申请拒绝后，仅将当前申请状态变更为“否决”，该笔业务的贷款合同及合同与担保合同关系结果不变，若需作废合同，则人工在合同管理模块将该合同作废。
     * 2、更新申请主表的审批状态为998 拒绝
     *
     * @param pvpSerno
     */
    public void handleBusinessAfterRefuse(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请拒绝流程-获取放款申请" + pvpSerno + "申请信息");
            PvpEntrustLoanApp pvpEntrustLoanApp = this.selectByEntrustSernoKey(pvpSerno);
            if (pvpEntrustLoanApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }

            //获取合同数据，并根据合同中的业务类型进行数据判断
            log.info("放款申请拒绝流程-获取放款申请" + pvpSerno + "获取业务合同数据");
            CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo(pvpEntrustLoanApp.getContNo());
            if (ctrEntrustLoanCont == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_CONTNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_CONTNULL_EXCEPTION.value);
            }


            log.info("放款申请拒绝流程-更新放款申请" + pvpSerno + "流程状态-拒绝");
            pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
            int updCount = this.updateSelective(pvpEntrustLoanApp);
            if (updCount <= 0) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.value);
            }

            String inputId = "";
            String inputBrId = "";
            String inputDate = "";
            // 获取当前的登录人信息
            User userInfo = SessionUtils.getUserInformation();
            if (Objects.nonNull(userInfo)) {
                inputId = userInfo.getLoginCode();
                inputBrId = userInfo.getOrg().getCode();
                inputDate = stringRedisTemplate.opsForValue().get("openDay");
            }
            // 根据担保方式判断是否低风险
            String guarMode = pvpEntrustLoanApp.getGuarMode();
            // 台账总余额
            BigDecimal balanceAmtCny = pvpEntrustLoanApp.getPvpAmt();
            // 恢复敞口余额
            BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
            // 恢复总额
            BigDecimal recoverAmtCny = BigDecimal.ZERO;

            if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                recoverAmtCny = pvpEntrustLoanApp.getPvpAmt();
                recoverSpacAmtCny = BigDecimal.ZERO;
            } else {
                recoverAmtCny = pvpEntrustLoanApp.getPvpAmt();
                recoverSpacAmtCny = pvpEntrustLoanApp.getPvpAmt();
            }
            // 组装报文进行台账恢复
            CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
            cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpEntrustLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0014ReqDto.setSerno(pvpEntrustLoanApp.getPvpSerno());//交易流水号
            cmisLmt0014ReqDto.setInputId(inputId);//登记人
            cmisLmt0014ReqDto.setInputBrId(inputBrId);//登记机构
            cmisLmt0014ReqDto.setInputDate(inputDate);//登记日期

            List<CmisLmt0014ReqdealBizListDto> dealBizList = new ArrayList<>();

            CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
            cmisLmt0014ReqdealBizListDto.setDealBizNo(pvpEntrustLoanApp.getBillNo());//台账编号
            cmisLmt0014ReqdealBizListDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
            cmisLmt0014ReqdealBizListDto.setRecoverStd(CmisLmtConstants.STD_ZB_RECOVER_STD_02);//恢复标准值
            cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(BigDecimal.ZERO);//台账总余额（人民币）
            cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(BigDecimal.ZERO);//台账敞口余额（人民币）
            cmisLmt0014ReqdealBizListDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口余额(人民币)
            cmisLmt0014ReqdealBizListDto.setRecoverAmtCny(recoverAmtCny);//恢复总额(人民币)
            cmisLmt0014ReqdealBizListDto.setDealBizBailPreRate(BigDecimal.ZERO);//追加后保证金比例
            cmisLmt0014ReqdealBizListDto.setSecurityAmt(BigDecimal.ZERO);//追加后保证金金额
            dealBizList.add(cmisLmt0014ReqdealBizListDto);
            cmisLmt0014ReqDto.setDealBizList(dealBizList);
            log.info("台账【{}】恢复，前往额度系统进行额度恢复开始,请求报文为:【{}】", pvpEntrustLoanApp.getBillNo(), cmisLmt0014ReqDto.toString());
            ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
            log.info("台账【{}】恢复，前往额度系统进行额度恢复结束,响应报文为:【{}】", pvpEntrustLoanApp.getBillNo(), cmisLmt0014RespDtoResultDto.toString());
            if (!"0".equals(cmisLmt0014RespDtoResultDto.getCode())) {
                log.info("台账恢复，前往额度系统进行额度恢复异常");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (!"0000".equals(cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
                log.info("台账【{}】恢复，前往额度系统进行额度恢复异常", pvpEntrustLoanApp.getBillNo());
                throw BizException.error(null, cmisLmt0014RespDtoResultDto.getData().getErrorCode(), cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + pvpSerno + "流程拒绝业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    @Transactional
    public int updateApprStatus(String iqpSerno, String ApprStatus) {
        return pvpEntrustLoanAppMapper.updateApprStatus(iqpSerno, ApprStatus);
    }

    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     *
     * @param pvpSerno
     */
    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
    public ResultDto handleBusinessDataAfterEnd(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("审批通过-获取委托贷款申请" + pvpSerno + "申请主表信息");
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);
            if (pvpEntrustLoanApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            int updateCount = 0;
//            Map seqMap = new HashMap();
//            seqMap.put("contNo", pvpEntrustLoanApp.getContNo());
//            String bill_no = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ, seqMap);
            log.info("放款审批通过-放款申请" + pvpSerno + "生成授权信息");
            PvpAuthorize pvpAuthorize = getPvpAuthorize(pvpSerno, pvpEntrustLoanApp.getBillNo());

            log.info("放款审批通过-放款申请" + pvpSerno + "生成借据信息");
            AccEntrustLoan accEntrustLoan = getAccEntrustLoanInfo(pvpSerno, pvpEntrustLoanApp.getBillNo());

            int resultAu = pvpAuthorizeService.insertSelective(pvpAuthorize);
            int resultAc = accEntrustLoanService.insertSelective(accEntrustLoan);

            log.info("流程发起-更新委托贷款申请" + pvpSerno + "流程审批状态为【997】-审批结束");
            updateCount = pvpEntrustLoanAppMapper.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_997);
            // 系统通知
            CmisCfg0001ReqDto cmisCfg0001ReqDto = new CmisCfg0001ReqDto();
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.PK_VALUE, new HashMap<>());
            cmisCfg0001ReqDto.setSerno(serno);
            cmisCfg0001ReqDto.setMessageType(CmisBizConstants.STD_WB_NOTICE_TYPE_2);//审批通过
            cmisCfg0001ReqDto.setPubTime(DateUtils.getCurrTime());
            cmisCfg0001ReqDto.setReceiverType(null);
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            cmisCfg0001ReqDto.setInputDate(openday);
            cmisCfg0001ReqDto.setInputBrId(pvpEntrustLoanApp.getInputBrId());
            cmisCfg0001ReqDto.setInputId(pvpEntrustLoanApp.getInputId());
            cmisCfg0001ReqDto.setContent(pvpEntrustLoanApp.getCusName() + "(" + pvpEntrustLoanApp.getCusId() + ")的" + pvpEntrustLoanApp.getPrdName() + "放款申请已完成，审批结果为【通过】");
            log.info("根据放款申请编号【{}】，审批操作生成首页提醒！", pvpSerno);
            ResultDto<CmisCfg0001RespDto> cmisCfg0001RespDto = dscmsCfgQtClientService.cmisCfg0001(cmisCfg0001ReqDto);
            log.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.key,
                    DscmsCfgEnum.TRADE_CODE_CMISCFG0001.value, JSON.toJSONString(cmisCfg0001RespDto));
            if (cmisCfg0001RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisCfg0001RespDto.getData().getErrorCode())) {
                log.info("根据放款申请编号【{}】,生成首页提醒成功！", pvpSerno);
            } else {
                log.info("根据放款申请编号【{}】,生成首页提醒成功失败！", pvpSerno);
            }
            // 委托贷款出账申请审批状态修改
            pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_997);
            // 判断是否自动出账 STD_PVP_MODE
            try{
                if("1".equals(pvpEntrustLoanApp.getPvpMode())){
                    log.info("委托贷款自动出账" + pvpSerno);
                    ResultDto pvpResultDto = pvpAuthorizeService.sendAuthToCoreForXdWTDK(pvpAuthorize);
                    if(Objects.nonNull(pvpResultDto)){
                        if(Objects.equals("0",pvpResultDto.getCode())){
                            log.info("委托贷款自动出账成功" + pvpSerno);
                            return new ResultDto(null).message("自动出账成功").code("0");
                        }else{
                            return pvpResultDto;
                        }
                    }
                }
            }catch(BizException e){
                log.info("委托贷款自动出账" + pvpSerno+"失败");
                return new ResultDto(null).message(e.getMessage()).code("9999");
            }
            catch(Exception e){
                log.info("委托贷款自动出账" + pvpSerno+"失败");
                return new ResultDto(null).message(e.getMessage()).code("9999");
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("贴现业务申请流程审批通过业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
        return new ResultDto(null).message("出账申请成功").code("0");
    }

    private AccEntrustLoan getAccEntrustLoanInfo(String pvpSerno, String bill_no) {

        AccEntrustLoan accEntrustLoan = new AccEntrustLoan();
        PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);
        String cont_no = pvpEntrustLoanApp.getContNo();
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("contNo", cont_no);
        CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContMapper.queryCtrEntrustLoanByDataParams(queryMap);

        String pkid = StringUtils.uuid(true);
        accEntrustLoan.setPkId(pkid);//主键
        accEntrustLoan.setBillNo(bill_no);//借据编号
        accEntrustLoan.setGuarMode(pvpEntrustLoanApp.getGuarMode());//担保方式
        accEntrustLoan.setLmtAccNo(pvpEntrustLoanApp.getLmtAccNo());//授信台账编号
        accEntrustLoan.setReplyNo(pvpEntrustLoanApp.getReplyNo());//批复编号
        accEntrustLoan.setIsUtilLmt(pvpEntrustLoanApp.getIsUtilLmt());//是否使用授信额度
        accEntrustLoan.setSubjectNo("WT002");//科目号
        accEntrustLoan.setRepaySubAccno(pvpEntrustLoanApp.getRepaySubAccno());//贷款还款账户子序号
        accEntrustLoan.setPayoutAcctName(pvpEntrustLoanApp.getPayoutAcctName());//发放账号名称
        accEntrustLoan.setIsBeEntrustedPay(pvpEntrustLoanApp.getIsBeEntrustedPay());//是否受托支付
        accEntrustLoan.setLoanModal(ctrEntrustLoanCont.getLoanModal());//贷款形式
        accEntrustLoan.setPvpSerno(pvpEntrustLoanApp.getPvpSerno());//出账流水号
        accEntrustLoan.setContNo(pvpEntrustLoanApp.getContNo());//合同编号
        accEntrustLoan.setCusId(pvpEntrustLoanApp.getCusId());//客户编号
        accEntrustLoan.setCusName(pvpEntrustLoanApp.getCusName());//客户名称
        accEntrustLoan.setPrdId(pvpEntrustLoanApp.getPrdId());//产品编号
        accEntrustLoan.setPrdName(pvpEntrustLoanApp.getPrdName());//产品名称
        accEntrustLoan.setPrdTypeProp(pvpEntrustLoanApp.getPrdTypeProp());//产品类型属性
        accEntrustLoan.setContCurType(pvpEntrustLoanApp.getCurType());//贷款发放币种
        accEntrustLoan.setCurType(ctrEntrustLoanCont.getCurType());//币种
        accEntrustLoan.setCurtLprRate(ctrEntrustLoanCont.getCurtLprRate());//当前LPR利率
        accEntrustLoan.setExchangeRate(new BigDecimal("1"));//汇率
        accEntrustLoan.setZcbjAmt(pvpEntrustLoanApp.getPvpAmt());//正常本金
        accEntrustLoan.setOverdueCapAmt(BigDecimal.ZERO);//逾期本金
        accEntrustLoan.setDebitInt(BigDecimal.ZERO);//欠息
        accEntrustLoan.setPenalInt(BigDecimal.ZERO);//罚息
        accEntrustLoan.setCompoundInt(BigDecimal.ZERO);//复息
        accEntrustLoan.setLoanAmt(pvpEntrustLoanApp.getPvpAmt());//出账金额
        accEntrustLoan.setLoanBalance(BigDecimal.ZERO);//贷款余额
        accEntrustLoan.setExchangeRmbAmt(pvpEntrustLoanApp.getPvpAmt());//折合人民币金额
        accEntrustLoan.setExchangeRmbBal(BigDecimal.ZERO);//折合人民币余额
        accEntrustLoan.setInnerDebitInterest(BigDecimal.ZERO);//表内欠息
        accEntrustLoan.setOffDebitInterest(BigDecimal.ZERO);//表外欠息
        accEntrustLoan.setLoanStartDate(pvpEntrustLoanApp.getLoanStartDate());//贷款起始日
        accEntrustLoan.setLoanEndDate(pvpEntrustLoanApp.getLoanEndDate());//贷款到期日
        accEntrustLoan.setLoanTerm(pvpEntrustLoanApp.getLoanTerm());//贷款期限
        accEntrustLoan.setLoanTermUnit(pvpEntrustLoanApp.getLoanTermUnit());//贷款期限单位
        accEntrustLoan.setExtTimes("0");//展期次数
        accEntrustLoan.setOverdueDay("0");//逾期天数
        accEntrustLoan.setOverdueTimes("0");//逾期次数
        accEntrustLoan.setSettlDate("");//结清日期
        accEntrustLoan.setRateAdjMode(pvpEntrustLoanApp.getRateAdjMode());//利率调整方式
        accEntrustLoan.setIsSegInterest(pvpEntrustLoanApp.getIsSegInterest());//是否分段计息
        accEntrustLoan.setLprRateIntval(pvpEntrustLoanApp.getLprRateIntval());//LPR授信利率区间
        accEntrustLoan.setCurtLprRate(pvpEntrustLoanApp.getCurtLprRate());//当前LPR利率
        accEntrustLoan.setRateFloatPoint(pvpEntrustLoanApp.getRateFloatPoint());//浮动点数
        accEntrustLoan.setExecRateYear(pvpEntrustLoanApp.getExecRateYear());//执行利率(年)
        accEntrustLoan.setOverdueRatePefloat(pvpEntrustLoanApp.getOverdueRatePefloat());//逾期利率浮动比
        accEntrustLoan.setOverdueExecRate(pvpEntrustLoanApp.getOverdueExecRate());//逾期执行利率(年利率)
        accEntrustLoan.setCiRatePefloat(pvpEntrustLoanApp.getCiRatePefloat());//复息利率浮动比
        accEntrustLoan.setCiExecRate(pvpEntrustLoanApp.getCiExecRate());//复息执行利率(年利率)
        accEntrustLoan.setRateAdjType(pvpEntrustLoanApp.getRateAdjType());//利率调整选项
        accEntrustLoan.setNextRateAdjInterval(pvpEntrustLoanApp.getNextRateAdjInterval());//下一次利率调整间隔
        accEntrustLoan.setNextRateAdjUnit(pvpEntrustLoanApp.getNextRateAdjUnit());//下一次利率调整间隔单位
        accEntrustLoan.setFirstAdjDate(pvpEntrustLoanApp.getFirstAdjDate());//第一次调整日
        accEntrustLoan.setRepayMode(pvpEntrustLoanApp.getRepayMode());//还款方式
        accEntrustLoan.setEiIntervalCycle(pvpEntrustLoanApp.getEiIntervalCycle());//结息间隔周期
        accEntrustLoan.setEiIntervalUnit(pvpEntrustLoanApp.getEiIntervalUnit());//结息间隔周期单位
        accEntrustLoan.setDeductType(pvpEntrustLoanApp.getDeductType());//扣款方式
        accEntrustLoan.setDeductDay(pvpEntrustLoanApp.getDeductDay());//扣款日
        accEntrustLoan.setLoanPayoutAccno(pvpEntrustLoanApp.getLoanPayoutAccno());//贷款发放账号
        accEntrustLoan.setLoanPayoutSubNo(pvpEntrustLoanApp.getLoanPayoutSubNo());//贷款发放账号子序号
        accEntrustLoan.setRepayAccno(pvpEntrustLoanApp.getRepayAccno());//还款计划账号
        accEntrustLoan.setRepayAcctName(pvpEntrustLoanApp.getRepayAcctName());//还款账户名称
        accEntrustLoan.setLoanTer(pvpEntrustLoanApp.getLoanTer());//贷款投向
        accEntrustLoan.setAgriLoanTer(pvpEntrustLoanApp.getAgriLoanTer());//涉农贷款投向 委托贷款都为对公客户，应该无值
        accEntrustLoan.setLoanPurpType(pvpEntrustLoanApp.getLoanPurpType());//借款用途类型
        accEntrustLoan.setLoanPromiseFlag(pvpEntrustLoanApp.getLoanPromiseFlag());//贷款承诺标志
        accEntrustLoan.setLoanPromiseType(pvpEntrustLoanApp.getLoanPromiseType());//贷款承诺类型
        accEntrustLoan.setCsgnLoanChrgCollectType(pvpEntrustLoanApp.getCsgnLoanChrgCollectType());//委托贷款手续费收取方式
        accEntrustLoan.setCsgnLoanChrgRate(pvpEntrustLoanApp.getCsgnLoanChrgRate());//委托贷款手续费比例
        accEntrustLoan.setCsgnLoanChrgAmt(pvpEntrustLoanApp.getCsgnLoanChrgAmt());//委托贷款手续费金额
        accEntrustLoan.setConsignorIdSettlAccno(pvpEntrustLoanApp.getConsignorIdSettlAccno());   //委托人存款账号
        accEntrustLoan.setConsignorCusId(pvpEntrustLoanApp.getConsignorCusId());    //委托人
        accEntrustLoan.setConsignorCusName(pvpEntrustLoanApp.getConsignorCusName());    //委托人名称
        accEntrustLoan.setFinaBrId(pvpEntrustLoanApp.getFinaBrId());//账务机构编号
        accEntrustLoan.setFinaBrIdName(pvpEntrustLoanApp.getFinaBrIdName());//账务机构名称
        accEntrustLoan.setDisbOrgNo(pvpEntrustLoanApp.getDisbOrgNo());//放款机构编号
        accEntrustLoan.setDisbOrgName(pvpEntrustLoanApp.getDisbOrgName());//放款机构名称
        accEntrustLoan.setIsSbsy("0");//是否贴息
        accEntrustLoan.setSbsyPerc(BigDecimal.ZERO);//贴息比例
        accEntrustLoan.setFiveClass("10");//五级分类
        accEntrustLoan.setTenClass("11");//十级分类
        accEntrustLoan.setClassDate(LocalDate.now().toString());//分类日期
        accEntrustLoan.setAccStatus(CmisCommonConstants.ACC_STATUS_6);//台账状态
        accEntrustLoan.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型

        User userInfo = SessionUtils.getUserInformation();
        accEntrustLoan.setInputId(pvpEntrustLoanApp.getInputId());
        accEntrustLoan.setInputBrId(pvpEntrustLoanApp.getInputBrId());
        accEntrustLoan.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        accEntrustLoan.setUpdId(userInfo.getLoginCode());
        accEntrustLoan.setUpdBrId(userInfo.getOrg().getCode());
        accEntrustLoan.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        accEntrustLoan.setManagerId(pvpEntrustLoanApp.getManagerId());
        accEntrustLoan.setManagerBrId(pvpEntrustLoanApp.getManagerBrId());
        accEntrustLoan.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return accEntrustLoan;
    }

    private PvpAuthorize getPvpAuthorize(String pvpSerno, String bill_no) {
        PvpAuthorize pvpAuthorize = new PvpAuthorize();
        PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);
        if (pvpEntrustLoanApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }
        //出账号
        String loanNo = "";
        String loanStartDate = pvpEntrustLoanApp.getLoanStartDate();            //贷款起始日
        String loanEndDate = pvpEntrustLoanApp.getLoanEndDate();                //贷款到期日
        String monthDiffer = pvpEntrustLoanApp.getLoanTerm();            //返回贷款到期日和起始日相差的月数
        //获取当前日期
        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);

        Map seqMap = new HashMap();
        String tran_serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PA_TRAN_SERNO, seqMap);
        pvpAuthorize.setTranSerno(tran_serno);
        pvpAuthorize.setPvpSerno(pvpSerno);                                    // 出账流水号
        pvpAuthorize.setAuthorizeNo("");
        pvpAuthorize.setAuthorizeType("");
        pvpAuthorize.setContNo(pvpEntrustLoanApp.getContNo());                        //合同号
        pvpAuthorize.setBillNo(bill_no);                                        //借据号
        pvpAuthorize.setUntruPayType("");
        pvpAuthorize.setCusId(pvpEntrustLoanApp.getCusId());                        //借款人客户号
        pvpAuthorize.setCusName(pvpEntrustLoanApp.getCusName());                   //借款人客户名称
        pvpAuthorize.setPrdId(pvpEntrustLoanApp.getPrdId());//产品编号
        pvpAuthorize.setPrdName(pvpEntrustLoanApp.getPrdName());//产品名称
        pvpAuthorize.setTranId(CmisCommonConstants.WTDK); //交易编号
        pvpAuthorize.setAcctNo(pvpEntrustLoanApp.getRepaySubAccno()); //账号ln3123
        pvpAuthorize.setDisbOrgNo(pvpEntrustLoanApp.getDisbOrgNo());//放款机构
        pvpAuthorize.setAcctName(pvpEntrustLoanApp.getPayoutAcctName());//账号名称
        pvpAuthorize.setCurType(pvpEntrustLoanApp.getCurType());//币种
        pvpAuthorize.setTranAmt(pvpEntrustLoanApp.getPvpAmt());            //放款金额
        pvpAuthorize.setTranDate(stringRedisTemplate.opsForValue().get("openDay"));                        //交易日期
        pvpAuthorize.setSendTimes(new BigDecimal(0));   //发送次数
        pvpAuthorize.setReturnCode(""); //返回编码
        pvpAuthorize.setReturnDesc(""); //返回说明
        pvpAuthorize.setManagerId(pvpEntrustLoanApp.getManagerId()); //主办人
        pvpAuthorize.setManagerBrId(pvpEntrustLoanApp.getManagerBrId()); //主办机构
        pvpAuthorize.setFinaBrId(pvpEntrustLoanApp.getFinaBrId());//账务机构
        pvpAuthorize.setAuthStatus(CmisCommonConstants.STD_AUTH_STATUS_0); //授权状态
        pvpAuthorize.setTradeStatus(CmisCommonConstants.TRADE_STATUS_1);                //核心状态未出账
        pvpAuthorize.setFldvalue01(pvpEntrustLoanApp.getLoanSubjectNo());            //贷款科目代码
        pvpAuthorize.setFldvalue02("bizType");    //***贷款类型
        pvpAuthorize.setFldvalue03(pvpEntrustLoanApp.getLoanPayoutAccno());         //收款人账号
        pvpAuthorize.setFldvalue04(pvpEntrustLoanApp.getLoanPayoutAccno());        //借款人结算账号
        pvpAuthorize.setFldvalue05(pvpEntrustLoanApp.getSbsyDepAccno());    //贴息人存款账号
        pvpAuthorize.setFldvalue07(pvpEntrustLoanApp.getConsignorIdSettlAccno());            //委托人存款账号
        pvpAuthorize.setFldvalue08(pvpEntrustLoanApp.getLoanEndDate());             //到期日
        pvpAuthorize.setFldvalue09(String.valueOf(monthDiffer));            //期限代码
        pvpAuthorize.setFldvalue10(pvpEntrustLoanApp.getCurType());                                //币种
        pvpAuthorize.setFldvalue11("");        //本金自动归还标志    0否1是
        pvpAuthorize.setFldvalue12("");         //利息自动归还标志     0否1是
        pvpAuthorize.setFldvalue13("");
        //重定价代码
        String LprRate = pvpEntrustLoanApp.getCurtLprRate().toString();
        pvpAuthorize.setFldvalue14(LprRate);           //基准利率代码
        pvpAuthorize.setFldvalue15(String.valueOf(pvpEntrustLoanApp.getRateFloatPoint()));    //正常贷款利率浮动比例
        pvpAuthorize.setFldvalue16(String.valueOf(pvpEntrustLoanApp.getOverdueRatePefloat()));    //逾期贷款利率浮动比例
        pvpAuthorize.setFldvalue17(String.valueOf(pvpEntrustLoanApp.getOverdueExecRate()));    //逾期贷款执行利率
        pvpAuthorize.setFldvalue18(String.valueOf(pvpEntrustLoanApp.getExecRateYear()));    //正常贷款执行利率
        pvpAuthorize.setFldvalue19(pvpEntrustLoanApp.getDeductType());                                        //还款方式       1-普通贷款 2-分期贷款
        pvpAuthorize.setFldvalue20(pvpEntrustLoanApp.getRepayMode());                //普通贷款结息方式代码
        pvpAuthorize.setFldvalue21("");                //分期贷款还款方式代码
        pvpAuthorize.setFldvalue22("");                //分期贷款总期数
        pvpAuthorize.setFldvalue23(String.valueOf(pvpEntrustLoanApp.getSbsyPerc()));   //贴息比例
        pvpAuthorize.setFldvalue24(String.valueOf(pvpEntrustLoanApp.getSbysEnddate()));                //贴息到期日
        pvpAuthorize.setFldvalue25(pvpEntrustLoanApp.getLoanPromiseFlag());                //贷款承诺标志    0无 1有
        pvpAuthorize.setFldvalue26(pvpEntrustLoanApp.getLoanPromiseType());                //承诺类型     贷款承诺标志=1时，本项目为必填项目
//        //1大中型企业贷款承诺 2	小企业贷款承诺3	个人贷款承诺
        pvpAuthorize.setFldvalue27(pvpEntrustLoanApp.getRepayAcctName());                //放款账户名
        // 老信贷前台 默认00001
        pvpAuthorize.setFldvalue28("00001");            //委托存款子户号
        pvpAuthorize.setFldvalue29("1");                                            //--合同类型    1单笔发放、2限定最高余额
        pvpAuthorize.setFldvalue30("1");                        //是否计复息   0不计算、1计算
        pvpAuthorize.setFldvalue31(pvpEntrustLoanApp.getLoanPurpType());        //用途代码     00  流动资金   01  固定资金  02  技术改造  04  流资转期
//        //05  固资转期 06  财政性直贷 07  财政性自营贷款 08  政府指令性贷款   09  其他
//        //分期贷款：10  住房  11  汽车 12  商铺  13  工程机械       19  其他
        pvpAuthorize.setFldvalue32(pvpEntrustLoanApp.getCsgnLoanChrgCollectType());    //委托贷款手续费收取方式
        pvpAuthorize.setFldvalue33(pvpEntrustLoanApp.getCsgnLoanChrgRate().toString());    //委托贷款手续费收取比例
        pvpAuthorize.setFldvalue34(pvpEntrustLoanApp.getCsgnLoanChrgAmt().toString());   //委托贷款手续费
        pvpAuthorize.setFldvalue35(pvpEntrustLoanApp.getDeductDay());        //按揭还款日（缺省为放款日）
        pvpAuthorize.setFldvalue36("2");    //贷款利率浮动方式 1  加百分比 2 加点 3正常加点逾期加百分比
        pvpAuthorize.setFldvalue37(pvpEntrustLoanApp.getRateFloatPoint().toString());    //正常利率浮动点数(年利率)
        pvpAuthorize.setFldvalue38(pvpEntrustLoanApp.getOverdueRatePefloat().toString());    //逾期利率浮动点数(年利率)
        pvpAuthorize.setFldvalue39(pvpEntrustLoanApp.getManagerId());           //客户经理
        pvpAuthorize.setFldvalue40("【委托贷款】");
        pvpAuthorize.setFldvalue41(pvpEntrustLoanApp.getConsignorCusName().trim()); //委托人名称
        pvpAuthorize.setFldvalue42(pvpEntrustLoanApp.getConsignorCusId()); //委托人名称
//        pvpAuthorize.setHoliday_lx_delay(pvpLoanApp.getHoliday_lx_delay());//节假日顺延
//        pvpAuthorize.setFldvalue45(pvpLoanApp.getBizType());            //业务品种

        return pvpAuthorize;
    }

    /**
     * 获取基本信息
     *
     * @param pvpSerno
     * @return
     */
    public PvpEntrustLoanApp selectByEntrustSernoKey(String pvpSerno) {
        return pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询待发起数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<PvpEntrustLoanApp> toSignlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return pvpEntrustLoanAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询已处理数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<PvpEntrustLoanApp> doneSignlist(QueryModel model) {
        model.addCondition("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return pvpEntrustLoanAppMapper.selectByModel(model);
    }

    /**
     * 委托贷款
     *
     * @param pvpSerno
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map invalidentrustloanapp(String pvpSerno) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            log.info("获取委托贷款出账申请" + pvpSerno + "申请主表信息");
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);
            if (pvpEntrustLoanApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(pvpSerno);
            // 更改状态为 作废
            pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
            log.info("修改开始,如果结果为1则修改成功 否则修改失败");
            int i = pvpEntrustLoanAppMapper.updateByPrimaryKeySelective(pvpEntrustLoanApp);
            log.info("修改结束,修改条数=" + i);

            String inputId = "";
            String inputBrId = "";
            String inputDate = "";
            // 获取当前的登录人信息
            User userInfo = SessionUtils.getUserInformation();
            if (Objects.nonNull(userInfo)) {
                inputId = userInfo.getLoginCode();
                inputBrId = userInfo.getOrg().getCode();
                inputDate = stringRedisTemplate.opsForValue().get("openDay");
            }
            // 根据担保方式判断是否低风险
            String guarMode = pvpEntrustLoanApp.getGuarMode();
            // 台账总余额
            BigDecimal balanceAmtCny = pvpEntrustLoanApp.getPvpAmt();
            // 恢复敞口余额
            BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
            // 恢复总额
            BigDecimal recoverAmtCny = BigDecimal.ZERO;

            if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                recoverAmtCny = pvpEntrustLoanApp.getPvpAmt();
                recoverSpacAmtCny = BigDecimal.ZERO;
            } else {
                recoverAmtCny = pvpEntrustLoanApp.getPvpAmt();
                recoverSpacAmtCny = pvpEntrustLoanApp.getPvpAmt();
            }
            // 组装报文进行台账恢复
            CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
            cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpEntrustLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0014ReqDto.setSerno(pvpEntrustLoanApp.getPvpSerno());//交易流水号
            cmisLmt0014ReqDto.setInputId(inputId);//登记人
            cmisLmt0014ReqDto.setInputBrId(inputBrId);//登记机构
            cmisLmt0014ReqDto.setInputDate(inputDate);//登记日期

            List<CmisLmt0014ReqdealBizListDto> dealBizList = new ArrayList<>();

            CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
            cmisLmt0014ReqdealBizListDto.setDealBizNo(pvpEntrustLoanApp.getBillNo());//台账编号
            cmisLmt0014ReqdealBizListDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
            cmisLmt0014ReqdealBizListDto.setRecoverStd(CmisLmtConstants.STD_ZB_RECOVER_STD_02);//恢复标准值
            cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(BigDecimal.ZERO);//台账总余额（人民币）
            cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(BigDecimal.ZERO);//台账敞口余额（人民币）
            cmisLmt0014ReqdealBizListDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口余额(人民币)
            cmisLmt0014ReqdealBizListDto.setRecoverAmtCny(recoverAmtCny);//恢复总额(人民币)
            cmisLmt0014ReqdealBizListDto.setDealBizBailPreRate(BigDecimal.ZERO);//追加后保证金比例
            cmisLmt0014ReqdealBizListDto.setSecurityAmt(BigDecimal.ZERO);//追加后保证金金额
            dealBizList.add(cmisLmt0014ReqdealBizListDto);
            cmisLmt0014ReqDto.setDealBizList(dealBizList);
            log.info("台账【{}】恢复，前往额度系统进行额度恢复开始,请求报文为:【{}】", pvpEntrustLoanApp.getBillNo(), cmisLmt0014ReqDto.toString());
            ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
            log.info("台账【{}】恢复，前往额度系统进行额度恢复结束,响应报文为:【{}】", pvpEntrustLoanApp.getBillNo(), cmisLmt0014RespDtoResultDto.toString());
            if (!"0".equals(cmisLmt0014RespDtoResultDto.getCode())) {
                log.info("台账恢复，前往额度系统进行额度恢复异常");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (!"0000".equals(cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
                log.info("台账【{}】恢复，前往额度系统进行额度恢复异常", pvpEntrustLoanApp.getBillNo());
                throw BizException.error(null, cmisLmt0014RespDtoResultDto.getData().getErrorCode(), cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
            }

            result.put("serno", pvpSerno);
            log.info("委托贷款出账" + pvpSerno + "-作废成功！");
        } catch (YuspException e) {
            log.error("委托贷款出账作废异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("委托贷款异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }


    /**
     * @创建人 ZXZ
     * @创建时间 2021-04-24 15:34
     * @注释 借据打印
     */
    public Map entrustprint(String pvpSerno) {
        // 参与者：在【数据列表区】界面，选择借据信息已创建，但未发起放款审批的单条记录，点击【借据打印】按钮；
        // 根据主键取获取对象
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        result.put("rtnCode", rtnCode);
        result.put("rtnMsg", rtnMsg);
        return result;
    }

    /**
     * @方法名称: countPvpEntrustCountByContNo
     * @方法描述: 根据委托合同编号查询出账申请数量
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int countPvpEntrustCountByContNo(Map pvpQueryMap) {
        return pvpEntrustLoanAppMapper.countPvpEntrustCountByContNo(pvpQueryMap);
    }

    /**
     * 获取当前合同编号下的所有在途申请
     *
     * @param contNo
     * @return
     */
    public List<PvpEntrustLoanApp> selectByContNo(String contNo) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("contNo", contNo);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        return selectAll(queryModel);
    }

    /**
     * @param pvpSerno
     * @return
     * @author qw
     * @date 2021-7-2 16:42:50
     * @version 1.0.0
     * @desc 委托贷款出账通知作废-台账恢复
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void retailRestore(String pvpSerno) {

        PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);

        try {
            String inputId = "";
            String inputBrId = "";
            String inputDate = "";
            // 获取当前的登录人信息
            User userInfo = SessionUtils.getUserInformation();
            if (Objects.nonNull(userInfo)) {
                inputId = userInfo.getLoginCode();
                inputBrId = userInfo.getOrg().getCode();
                inputDate = stringRedisTemplate.opsForValue().get("openDay");
            }
            // 根据担保方式判断是否低风险
            String guarMode = pvpEntrustLoanApp.getGuarMode();
            // 台账总余额
            BigDecimal balanceAmtCny = pvpEntrustLoanApp.getPvpAmt();
            // 恢复敞口余额
            BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
            // 恢复总额
            BigDecimal recoverAmtCny = BigDecimal.ZERO;

            if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                recoverAmtCny = pvpEntrustLoanApp.getPvpAmt();
                recoverSpacAmtCny = BigDecimal.ZERO;
            } else {
                recoverAmtCny = pvpEntrustLoanApp.getPvpAmt();
                recoverSpacAmtCny = pvpEntrustLoanApp.getPvpAmt();
            }
            // 组装报文进行台账恢复
            CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
            cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpEntrustLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0014ReqDto.setSerno(pvpEntrustLoanApp.getPvpSerno());//交易流水号
            cmisLmt0014ReqDto.setInputId(inputId);//登记人
            cmisLmt0014ReqDto.setInputBrId(inputBrId);//登记机构
            cmisLmt0014ReqDto.setInputDate(inputDate);//登记日期

            List<CmisLmt0014ReqdealBizListDto> dealBizList = new ArrayList<>();

            CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
            cmisLmt0014ReqdealBizListDto.setDealBizNo(pvpEntrustLoanApp.getBillNo());//台账编号
            cmisLmt0014ReqdealBizListDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
            cmisLmt0014ReqdealBizListDto.setRecoverStd(CmisLmtConstants.STD_ZB_RECOVER_STD_02);//恢复标准值
            cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(BigDecimal.ZERO);//台账总余额（人民币）
            cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(BigDecimal.ZERO);//台账敞口余额（人民币）
            cmisLmt0014ReqdealBizListDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口余额(人民币)
            cmisLmt0014ReqdealBizListDto.setRecoverAmtCny(recoverAmtCny);//恢复总额(人民币)
            cmisLmt0014ReqdealBizListDto.setDealBizBailPreRate(BigDecimal.ZERO);//追加后保证金比例
            cmisLmt0014ReqdealBizListDto.setSecurityAmt(BigDecimal.ZERO);//追加后保证金金额
            dealBizList.add(cmisLmt0014ReqdealBizListDto);
            cmisLmt0014ReqDto.setDealBizList(dealBizList);
            log.info("台账【{}】恢复，前往额度系统进行额度恢复开始,请求报文为:【{}】", pvpEntrustLoanApp.getBillNo(), cmisLmt0014ReqDto.toString());
            ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
            log.info("台账【{}】恢复，前往额度系统进行额度恢复结束,响应报文为:【{}】", pvpEntrustLoanApp.getBillNo(), cmisLmt0014RespDtoResultDto.toString());
            if (!"0".equals(cmisLmt0014RespDtoResultDto.getCode())) {
                log.info("台账恢复，前往额度系统进行额度恢复异常");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (!"0000".equals(cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
                log.info("台账【{}】恢复，前往额度系统进行额度恢复异常", pvpEntrustLoanApp.getBillNo());
                throw BizException.error(null, cmisLmt0014RespDtoResultDto.getData().getErrorCode(), cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
            }

        } catch (Exception e) {
            log.info("台账恢复异常" + e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
    }

    /**
     * @param contNo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author shenli
     * @date 2021-10-11 16:14:27
     * @version 1.0.0
     * @desc 生成借据编号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String getBillNo(String contNo) {
        Map pvpQueryMap = new HashMap();
        pvpQueryMap.put("contNo", contNo);
        int pvpEntrustLoanAppCount = pvpEntrustLoanAppMapper.selectPvpEntrustLoanAppCountByContNo(pvpQueryMap);
        String billCount = String.format("%04d",pvpEntrustLoanAppCount + 1);
        return contNo + billCount;
    }
}
