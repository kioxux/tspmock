/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CreditCardAdjustmentAmtInfo;
import cn.com.yusys.yusp.domain.CreditCardAdjustmentAppInfo;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import com.mysql.cj.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditCardAdjustmentJudgInifo;
import cn.com.yusys.yusp.repository.mapper.CreditCardAdjustmentJudgInifoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardAdjustmentJudgInifoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-25 10:00:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardAdjustmentJudgInifoService {

    @Autowired
    private CreditCardAdjustmentJudgInifoMapper creditCardAdjustmentJudgInifoMapper;
    @Autowired
    private CreditCardAdjustmentAppInfoService creditCardAdjustmentAppInfoService;
    @Autowired
    private CreditCardAdjustmentJudgInifoService creditCardAdjustmentJudgInifoService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditCardAdjustmentJudgInifo selectByPrimaryKey(String pkId) {
        return creditCardAdjustmentJudgInifoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditCardAdjustmentJudgInifo> selectAll(QueryModel model) {
        List<CreditCardAdjustmentJudgInifo> records = (List<CreditCardAdjustmentJudgInifo>) creditCardAdjustmentJudgInifoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditCardAdjustmentJudgInifo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardAdjustmentJudgInifo> list = creditCardAdjustmentJudgInifoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditCardAdjustmentJudgInifo record) {
        return creditCardAdjustmentJudgInifoMapper.insert(record);
    }

    /**
     * @方法名称: save
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int save(CreditCardAdjustmentJudgInifo record) {
        CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo = creditCardAdjustmentAppInfoService.selectByPrimaryKey(record.getSerno());
        if(creditCardAdjustmentAppInfo == null){
            throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, "未找到该调额申请！");
        }
        if(StringUtils.isNullOrEmpty(record.getApproveConclusion())){
            throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, "请传入审批结论！");
        }
        if(StringUtils.isNullOrEmpty(record.getApprovePostFlag())){
            throw BizException.error(null, EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key, "请传入审批岗位标识！");
        }else if("01".equals(record.getApprovePostFlag())){//如果审批岗位为二岗
            CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifo = creditCardAdjustmentJudgInifoService.selectBySernoAndApprovePostFlag(record.getSerno(),record.getApprovePostFlag());
            if(creditCardAdjustmentJudgInifo != null){
                return creditCardAdjustmentJudgInifoMapper.updateBySernoAndApprovePostFlagSelective(record);
            }else {
                return creditCardAdjustmentJudgInifoMapper.insert(record);
            }

        }else {
            CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifo = creditCardAdjustmentJudgInifoService.selectBySernoAndApprovePostFlag(record.getSerno(),record.getApprovePostFlag());
            if(creditCardAdjustmentJudgInifo != null){
                return creditCardAdjustmentJudgInifoMapper.updateBySernoAndApprovePostFlagSelective(record);
            }else {
                return creditCardAdjustmentJudgInifoMapper.insert(record);
            }
        }
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditCardAdjustmentJudgInifo record) {
        return creditCardAdjustmentJudgInifoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditCardAdjustmentJudgInifo record) {
        return creditCardAdjustmentJudgInifoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditCardAdjustmentJudgInifo record) {
        return creditCardAdjustmentJudgInifoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return creditCardAdjustmentJudgInifoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditCardAdjustmentJudgInifoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据流水号和审批人标识查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CreditCardAdjustmentJudgInifo selectBySernoAndApprovePostFlag(String serno,String approvePostFlag) {
        return creditCardAdjustmentJudgInifoMapper.selectBySernoAndApprovePostFlag(serno,approvePostFlag);
    }
    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/9/6 15:22
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CreditCardAdjustmentJudgInifo selectBySerno(String serno) {
        return creditCardAdjustmentJudgInifoMapper.selectBySerno(serno);
    }

    public List<CreditCardAdjustmentJudgInifo> selectAllBySerno(String serno) {
        return  creditCardAdjustmentJudgInifoMapper.selectAllBySerno(serno);
    }
}
