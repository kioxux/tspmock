/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.PvpAccpApp;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.VisaXdRisk;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: VisaXdRiskMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:50:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface VisaXdRiskMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    VisaXdRisk selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByCrpserno
     * @方法描述: 根据客户信息流水查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    VisaXdRisk selectByCrpserno(@Param("crpSerno") String crpSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<VisaXdRisk> selectByModel(QueryModel model);

    /**
     * @方法名称: querySfVisaList
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<VisaXdRisk> querySfVisaList(QueryModel model);
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(VisaXdRisk record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(VisaXdRisk record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(VisaXdRisk record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(VisaXdRisk record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据客户编号查询该客户是否存在面签信息
     * @param cusId
     * @return
     */
    int selectCountByCusId(@Param("cusId")String cusId);

    /**
     * @方法名称: crpSerno
     * @方法描述: 根据客户信息流水号删除
     * @参数与返回说明: crpSerno - 客户信息流水号
     * @算法描述: 无
     */

    int deleteByCrpSerno(String crpSerno);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PvpAccpApp selectBySerno(String serno);
}