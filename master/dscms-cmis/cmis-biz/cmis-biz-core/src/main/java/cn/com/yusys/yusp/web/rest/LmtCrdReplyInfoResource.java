/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.service.LmtCrdReplyInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCrdReplyInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: sl
 * @创建时间: 2021-04-20 14:05:42
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "授信批复")
@RequestMapping("/api/lmtcrdreplyinfo")
public class LmtCrdReplyInfoResource {
    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtCrdReplyInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtCrdReplyInfo> list = lmtCrdReplyInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtCrdReplyInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtCrdReplyInfo>> index(QueryModel queryModel) {
        List<LmtCrdReplyInfo> list = lmtCrdReplyInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtCrdReplyInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{replyNo}")
    protected ResultDto<LmtCrdReplyInfo> show(@PathVariable("replyNo") String replyNo) {
        LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(replyNo);
        return new ResultDto<LmtCrdReplyInfo>(lmtCrdReplyInfo);
    }

    /**
     * @函数名称:selectByReplySerno
     * @函数描述:查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbyreplyserno")
    protected ResultDto<LmtCrdReplyInfo> selectByReplySerno(@RequestBody LmtCrdReplyInfo info) {
        LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(info.getReplySerno());
        return new ResultDto<LmtCrdReplyInfo>(lmtCrdReplyInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtCrdReplyInfo> create(@RequestBody LmtCrdReplyInfo lmtCrdReplyInfo) throws URISyntaxException {
        lmtCrdReplyInfoService.insert(lmtCrdReplyInfo);
        return new ResultDto<LmtCrdReplyInfo>(lmtCrdReplyInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtCrdReplyInfo lmtCrdReplyInfo) throws URISyntaxException {
        int result = lmtCrdReplyInfoService.update(lmtCrdReplyInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{replyNo}")
    protected ResultDto<Integer> delete(@PathVariable("replyNo") String replyNo) {
        int result = lmtCrdReplyInfoService.deleteByPrimaryKey(replyNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtCrdReplyInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/26 22:29
     * @注释 分页条件查询
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<LmtCrdReplyInfo>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<LmtCrdReplyInfo> list = lmtCrdReplyInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtCrdReplyInfo>>(list);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/26 22:29
     * @注释 分页条件查询
     */
    @PostMapping("/selectbymodelxw")
    protected ResultDto<List<LmtCrdReplyInfo>> selectbymodelxw(@RequestBody QueryModel queryModel) {
        List<LmtCrdReplyInfo> list = lmtCrdReplyInfoService.selectByModelxw(queryModel);
        return new ResultDto<List<LmtCrdReplyInfo>>(list);
    }

    /**
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<java.util.Map<java.lang.String,java.lang.String>>>
     * @author shenli
     * @date 2021/4/20 0020 14:11
     * @version 1.0.0
     * @desc 批复信息列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("批复信息列表")
    @PostMapping("/selectreplylist")
    protected ResultDto<List<Map<String, String>>> selectReplyList() {
        List<Map<String, String>>  list = lmtCrdReplyInfoService.selectReplyList();
        return new ResultDto<List<Map<String, String>>>(list);
    }

    /**
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<java.util.Map<java.lang.String,java.lang.String>>>
     * @author wzy
     * @date 2021/4/20 0020 14:11
     * @version 1.0.0
     * @desc 批复信息详情
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("批复信息详情")
    @PostMapping("/selectreplybypk")
    protected ResultDto<LmtCrdReplyInfo> selectreplyByPk(@RequestBody LmtCrdReplyInfo lmtCrdReplyInfo) {
        LmtCrdReplyInfo  record = lmtCrdReplyInfoService.selectByPrimaryKey(lmtCrdReplyInfo.getReplySerno());
        return new ResultDto<>(record);
    }
}
