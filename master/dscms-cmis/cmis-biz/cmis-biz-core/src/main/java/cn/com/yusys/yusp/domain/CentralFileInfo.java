/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralFileInfo
 * @类描述: central_file_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-08 23:33:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "central_file_info")
public class CentralFileInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 档案编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "FILE_NO")
	private String fileNo;
	
	/** 临时库位号 **/
	@Column(name = "TEMP_LOCATION_NO", unique = false, nullable = true, length = 40)
	private String tempLocationNo;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;

	/** 全局流水号 **/
	@Column(name = "TRACE_ID", unique = false, nullable = true, length = 40)
	private String traceId;

	/** 业务类型 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 接收人 **/
	@Column(name = "RECEIVER_ID", unique = false, nullable = true, length = 100)
	private String receiverId;
	
	/** 接收机构 **/
	@Column(name = "RECEIVER_ORG", unique = false, nullable = true, length = 40)
	private String receiverOrg;
	
	/** 档案接收日期 **/
	@Column(name = "FILE_RECEIVER_DATE", unique = false, nullable = true, length = 20)
	private String fileReceiverDate;
	
	/** 档案出库人 **/
	@Column(name = "OUT_ID", unique = false, nullable = true, length = 100)
	private String outId;
	
	/** 档案出库机构 **/
	@Column(name = "OUT_ORG", unique = false, nullable = true, length = 40)
	private String outOrg;
	
	/** 档案出库日期 **/
	@Column(name = "FILE_OUT_DATE", unique = false, nullable = true, length = 20)
	private String fileOutDate;
	
	/** 台账状态 **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
	private String accStatus;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param fileNo
	 */
	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}
	
    /**
     * @return fileNo
     */
	public String getFileNo() {
		return this.fileNo;
	}
	
	/**
	 * @param tempLocationNo
	 */
	public void setTempLocationNo(String tempLocationNo) {
		this.tempLocationNo = tempLocationNo;
	}
	
    /**
     * @return tempLocationNo
     */
	public String getTempLocationNo() {
		return this.tempLocationNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
    /**
     * @return bizType
     */
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param receiverId
	 */
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	
    /**
     * @return receiverId
     */
	public String getReceiverId() {
		return this.receiverId;
	}
	
	/**
	 * @param receiverOrg
	 */
	public void setReceiverOrg(String receiverOrg) {
		this.receiverOrg = receiverOrg;
	}
	
    /**
     * @return receiverOrg
     */
	public String getReceiverOrg() {
		return this.receiverOrg;
	}
	
	/**
	 * @param fileReceiverDate
	 */
	public void setFileReceiverDate(String fileReceiverDate) {
		this.fileReceiverDate = fileReceiverDate;
	}
	
    /**
     * @return fileReceiverDate
     */
	public String getFileReceiverDate() {
		return this.fileReceiverDate;
	}
	
	/**
	 * @param outId
	 */
	public void setOutId(String outId) {
		this.outId = outId;
	}
	
    /**
     * @return outId
     */
	public String getOutId() {
		return this.outId;
	}
	
	/**
	 * @param outOrg
	 */
	public void setOutOrg(String outOrg) {
		this.outOrg = outOrg;
	}
	
    /**
     * @return outOrg
     */
	public String getOutOrg() {
		return this.outOrg;
	}
	
	/**
	 * @param fileOutDate
	 */
	public void setFileOutDate(String fileOutDate) {
		this.fileOutDate = fileOutDate;
	}
	
    /**
     * @return fileOutDate
     */
	public String getFileOutDate() {
		return this.fileOutDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	
    /**
     * @return accStatus
     */
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}