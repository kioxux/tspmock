package cn.com.yusys.yusp.mock;

import java.util.List;

public class SubTree {

    private String id;
    private String data;
    private String address;
    private String name;
    private List<SubTree> children;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<SubTree> getChildren() {
        return children;
    }

    public void setChildren(List<SubTree> children) {
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
