/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constant.CommonConstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtRepayCapPlan;
import cn.com.yusys.yusp.service.LmtRepayCapPlanService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRepayCapPlanResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-13 14:10:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtrepaycapplan")
public class LmtRepayCapPlanResource {
    @Autowired
    private LmtRepayCapPlanService lmtRepayCapPlanService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtRepayCapPlan>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtRepayCapPlan> list = lmtRepayCapPlanService.selectAll(queryModel);
        return new ResultDto<List<LmtRepayCapPlan>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtRepayCapPlan>> index(QueryModel queryModel) {
        List<LmtRepayCapPlan> list = lmtRepayCapPlanService.selectByModel(queryModel);
        return new ResultDto<List<LmtRepayCapPlan>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtRepayCapPlan> show(@PathVariable("pkId") String pkId) {
        LmtRepayCapPlan lmtRepayCapPlan = lmtRepayCapPlanService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtRepayCapPlan>(lmtRepayCapPlan);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtRepayCapPlan> create(@RequestBody LmtRepayCapPlan lmtRepayCapPlan) throws URISyntaxException {
        lmtRepayCapPlanService.insert(lmtRepayCapPlan);
        return new ResultDto<LmtRepayCapPlan>(lmtRepayCapPlan);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtRepayCapPlan lmtRepayCapPlan) throws URISyntaxException {
        int result = lmtRepayCapPlanService.update(lmtRepayCapPlan);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtRepayCapPlanService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtRepayCapPlanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:saveRepayPlan
     * @函数描述:新增还款计划
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saverepayplan")
    protected ResultDto<Map> saveRepayPlan(@RequestBody LmtRepayCapPlan lmtRepayCapPlan) {
        Map result = lmtRepayCapPlanService.saveRepayPlan(lmtRepayCapPlan);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:saveRepayPlan
     * @函数描述:新增还款计划
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbyserno")
    protected ResultDto<List> selectBySerno(@RequestBody String serno) {
        List result = lmtRepayCapPlanService.selectBySerno(serno.replaceAll("\"", ""));
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:deletelmtRepayCapPlan
     * @函数描述:逻辑删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */

    @PostMapping("/deletelmtrepaycapplan")
    protected ResultDto<Map> deletelmtRepayCapPlan(@RequestBody LmtRepayCapPlan lmtRepayCapPlan) {
        Map rtnData = new HashMap();
        rtnData = lmtRepayCapPlanService.deleteByPkId(lmtRepayCapPlan);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:selectByIqpSerno
     * @函数描述:根据流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByIqpSerno")
    protected ResultDto<List<LmtRepayCapPlan>> selectByIqpSerno(@RequestBody QueryModel queryModel) {
        List<LmtRepayCapPlan> result = lmtRepayCapPlanService.selectByIqpSerno(queryModel);
        return new ResultDto<List<LmtRepayCapPlan>>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPkId")
    protected ResultDto<Integer> deleteByPkId(@RequestBody Map map) {
        int result = 0;
        LmtRepayCapPlan lmtRepayCapPlan = lmtRepayCapPlanService.selectByPrimaryKey((String) map.get("pkId"));
        if(lmtRepayCapPlan != null){
            lmtRepayCapPlan.setOprType(CommonConstance.OPR_TYPE_DELETE);
            result = lmtRepayCapPlanService.updateSelective(lmtRepayCapPlan);
        }else{
            // 不做操作  直接返回
        }
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:addOrUpdateAllTable
     * @函数描述:新增或保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addOrUpdateAllTable")
    protected ResultDto<Boolean> addOrUpdateAllTable(@RequestBody List<LmtRepayCapPlan> list) {
        boolean result = lmtRepayCapPlanService.addOrUpdateAllTable(list);
        return new ResultDto<Boolean>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtRepayCapPlan>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<LmtRepayCapPlan>>(lmtRepayCapPlanService.selectByModel(model));
    }
}
