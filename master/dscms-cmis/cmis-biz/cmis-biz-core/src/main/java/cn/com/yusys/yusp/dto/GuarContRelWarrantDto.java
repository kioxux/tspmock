package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarContRelWarrant
 * @类描述: guar_cont_rel_warrant数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-13 15:05:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarContRelWarrantDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 担保合同编号 **/
	private String guarContNo;
	
	/** 核心担保编号 **/
	private String coreGuarantyNo;
	
	/** 权证编号 **/
	private String warrantNo;
	
	/** 押品编号 **/
	private String guarNo;
	
	/** 押品名称 **/
	private String pldimnMemo;
	
	/** 押品类型 **/
	private String guarType;
	
	/** 押品所有人编号 **/
	private String guarCusId;
	
	/** 押品所有人名称 **/
	private String guarCusName;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}
	
    /**
     * @return GuarContNo
     */	
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param coreGuarantyNo
	 */
	public void setCoreGuarantyNo(String coreGuarantyNo) {
		this.coreGuarantyNo = coreGuarantyNo == null ? null : coreGuarantyNo.trim();
	}
	
    /**
     * @return CoreGuarantyNo
     */	
	public String getCoreGuarantyNo() {
		return this.coreGuarantyNo;
	}
	
	/**
	 * @param warrantNo
	 */
	public void setWarrantNo(String warrantNo) {
		this.warrantNo = warrantNo == null ? null : warrantNo.trim();
	}
	
    /**
     * @return WarrantNo
     */	
	public String getWarrantNo() {
		return this.warrantNo;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo == null ? null : pldimnMemo.trim();
	}
	
    /**
     * @return PldimnMemo
     */	
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType == null ? null : guarType.trim();
	}
	
    /**
     * @return GuarType
     */	
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId == null ? null : guarCusId.trim();
	}
	
    /**
     * @return GuarCusId
     */	
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName == null ? null : guarCusName.trim();
	}
	
    /**
     * @return GuarCusName
     */	
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}