package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.PvpExtLoan;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.PvpExtLoanService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @className PvpExtLoanBiz
 * @Author xianglei
 * @Description 展期放款申请 流程后处理
 * @Date 2021/1/25 : 20:28
 */
@Service
public class PvpExtLoanBiz implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(PvpExtLoanBiz.class);
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private PvpExtLoanService pvpExtLoanService;
    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();
        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                updateStatus(serno, CmisCommonConstants.WF_STATUS_111);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                pvpExtLoanService.handleBusinessDataAfterEnd(serno);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(instanceInfo);
                if(isFirstNode){
                    updateStatus(serno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + instanceInfo);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(instanceInfo);
                if(isFirstNode){
                    updateStatus(serno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + instanceInfo);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(instanceInfo);
                if(isFirstNode){
                    updateStatus(serno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                updateStatus(serno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatus(serno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto instanceInfo) {
        String flowCode = instanceInfo.getFlowCode();
        return CmisFlowConstants.BIZ_TYPE_PVP_EXT_LOAN_APP.equals(flowCode);
    }
    //更新状态
    public void updateStatus(String serno,String state){
        PvpExtLoan pvpExtLoan = pvpExtLoanService.selectByPrimaryKey(serno);
        pvpExtLoan.setApproveStatus(state);
        pvpExtLoanService.updateSelective(pvpExtLoan);
    }
}
