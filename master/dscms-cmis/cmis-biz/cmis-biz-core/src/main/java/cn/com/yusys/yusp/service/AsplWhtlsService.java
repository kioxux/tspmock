/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.AsplWhtls;
import cn.com.yusys.yusp.domain.AsplWhtlsOutPortModel;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.AsplWhtlsMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: AsplWhtlsService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 09:24:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AsplWhtlsService {

    @Resource
    private AsplWhtlsMapper asplWhtlsMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AsplWhtls selectByPrimaryKey(String pkId) {
        return asplWhtlsMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AsplWhtls> selectAll(QueryModel model) {
        List<AsplWhtls> records = (List<AsplWhtls>) asplWhtlsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AsplWhtls> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AsplWhtls> list = asplWhtlsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AsplWhtls record) {
        return asplWhtlsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AsplWhtls record) {
        return asplWhtlsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AsplWhtls record) {
        return asplWhtlsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AsplWhtls record) {
        return asplWhtlsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return asplWhtlsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return asplWhtlsMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: exportAsplAorgList
     * @方法描述: 导出
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ProgressDto exportAsplWhtls(AsplWhtls asplWhtls) {
        try {
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("cusId", asplWhtls.getCusId());
            queryModel.getCondition().put("cusName", asplWhtls.getCusName());
            queryModel.getCondition().put("contNo", asplWhtls.getContNo());
            queryModel.getCondition().put("managerId", asplWhtls.getManagerId());
            queryModel.getCondition().put("inureStatus", asplWhtls.getInureStatus());
            queryModel.getCondition().put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
            DataAcquisition dataAcquisition = (page, size, object) -> {
                QueryModel queryModeTemp = (QueryModel)object;
                queryModeTemp.setPage(page);
                queryModeTemp.setSize(size);
                return selectByModel(queryModeTemp);
            };
            ExportContext exportContext = ExportContext.of(AsplWhtlsOutPortModel.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, queryModel);
            return ExcelUtils.asyncExport(exportContext);
        } catch (Exception e) {
            throw new YuspException(EcbEnum.E_OUTPORTEXCEL_EXCEPTION_02.key,EcbEnum.E_OUTPORTEXCEL_EXCEPTION_02.value);
        }

    }

    /**
     * 批量插入承兑行白名单
     *
     * @param asplWhtlsOutPortModel 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int importAsplWhtls(List<AsplWhtlsOutPortModel> asplWhtlsOutPortModel) {
        List<AsplWhtls> asplWhtlsList = (List<AsplWhtls>) BeanUtils.beansCopy(asplWhtlsOutPortModel, AsplWhtls.class);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            for (AsplWhtls asplWhtlsExcel : asplWhtlsList) {
                QueryModel query = new QueryModel();
                query.getCondition().put("serno", asplWhtlsExcel.getSerno());
                query.getCondition().put("oprType", CmisCommonConstants.ADD_OPR);
                List<AsplWhtls> asplWhtlsListData = asplWhtlsMapper.selectByModel(query);
                //数据库中已存在，更新信息
                if (CollectionUtils.isNotEmpty(asplWhtlsListData)){
                    AsplWhtls asplAorgListUpdate = asplWhtlsListData.get(0);
                    // 客户编号
                    asplAorgListUpdate.setInureStatus(asplWhtlsExcel.getCusId());
                    // 客户名称
                    asplAorgListUpdate.setInureStatus(asplWhtlsExcel.getCusName());
                    // 资产池协议编号
                    asplAorgListUpdate.setInureStatus(asplWhtlsExcel.getContNo());
                    // 资产池协议额度
                    asplAorgListUpdate.setContAmt(asplWhtlsExcel.getContAmt());
                    //  客户经理
                    asplAorgListUpdate.setManagerId(asplWhtlsExcel.getManagerId());
                    // 导入模式
                    asplAorgListUpdate.setInureStatus(CommonConstance.IMPORT_MODE_02);
                    // 生效状态
                    asplAorgListUpdate.setInureStatus(CommonConstance.STATUS_1);
                    // 原因描述
                    asplAorgListUpdate.setImportResn(asplWhtlsExcel.getImportResn());
                    //登记日期
                    asplAorgListUpdate.setInputDate(sf.format(new Date()));
                    //最近修改人
                    asplAorgListUpdate.setUpdId(userInfo.getLoginCode());
                    //最近修改机构
                    asplAorgListUpdate.setUpdBrId(userInfo.getOrg().getCode());
                    //最近修改日期
                    asplAorgListUpdate.setUpdDate(sf.format(new Date()));
                    //修改时间
                    asplAorgListUpdate.setUpdateTime(new Date());
                    asplWhtlsMapper.updateByPrimaryKeySelective(asplAorgListUpdate);

                    //新增白名单额度信息日志记录

                }else{//数据库中不存在，新增信息
                    //生成主键
                    Map paramMap= new HashMap<>();
                    String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
                    asplWhtlsExcel.setPkId(pkValue);
                    // 审批状态
                    asplWhtlsExcel.setInureStatus(CommonConstance.APPROVE_STATUS_000);
                    //登记日期
                    asplWhtlsExcel.setInputDate(sf.format(new Date()));
                    //操作类型
                    asplWhtlsExcel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                    //最近修改人
                    asplWhtlsExcel.setUpdId(userInfo.getLoginCode());
                    //最近修改机构
                    asplWhtlsExcel.setUpdBrId(userInfo.getOrg().getCode());
                    //最近修改日期
                    asplWhtlsExcel.setUpdDate(sf.format(new Date()));
                    //创建时间
                    asplWhtlsExcel.setCreateTime(new Date());
                    asplWhtlsMapper.insertSelective(asplWhtlsExcel);

                    //新增白名单额度信息日志记录
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return asplWhtlsOutPortModel.size();
    }

    /**
     * 异步下载资产池承兑行名单模板
     * @return 导出进度信息
     */
    public ProgressDto exportAsplWhtlsModel() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {

                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(AsplWhtlsOutPortModel.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }
    /**
     * 根据资产池协议编号查询是否是资产池白名单
     */
    public boolean isWhtlsByconNo(String contNo){
        return asplWhtlsMapper.isWhtlsByconNo(contNo)>0?true:false;
    }

    /**
     * 根据 业务流水号 查询白名单数据
     * @return
     */

    public AsplWhtls selectByParam(Map map) {
        return asplWhtlsMapper.selectByParam(map);
    }
}
