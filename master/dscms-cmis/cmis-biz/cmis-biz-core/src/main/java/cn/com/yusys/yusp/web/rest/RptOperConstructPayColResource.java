/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOperConstructPayCol;
import cn.com.yusys.yusp.service.RptOperConstructPayColService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperConstructPayColResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-15 21:15:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptoperconstructpaycol")
public class RptOperConstructPayColResource {
    @Autowired
    private RptOperConstructPayColService rptOperConstructPayColService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOperConstructPayCol>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOperConstructPayCol> list = rptOperConstructPayColService.selectAll(queryModel);
        return new ResultDto<List<RptOperConstructPayCol>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOperConstructPayCol>> index(QueryModel queryModel) {
        List<RptOperConstructPayCol> list = rptOperConstructPayColService.selectByModel(queryModel);
        return new ResultDto<List<RptOperConstructPayCol>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptOperConstructPayCol> show(@PathVariable("pkId") String pkId) {
        RptOperConstructPayCol rptOperConstructPayCol = rptOperConstructPayColService.selectByPrimaryKey(pkId);
        return new ResultDto<RptOperConstructPayCol>(rptOperConstructPayCol);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOperConstructPayCol> create(@RequestBody RptOperConstructPayCol rptOperConstructPayCol) throws URISyntaxException {
        rptOperConstructPayColService.insert(rptOperConstructPayCol);
        return new ResultDto<RptOperConstructPayCol>(rptOperConstructPayCol);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOperConstructPayCol rptOperConstructPayCol) throws URISyntaxException {
        int result = rptOperConstructPayColService.update(rptOperConstructPayCol);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptOperConstructPayColService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOperConstructPayColService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptOperConstructPayCol>> selectByModel(@RequestBody QueryModel model) {
        return new ResultDto<List<RptOperConstructPayCol>>(rptOperConstructPayColService.selectByModel(model));
    }

    @PostMapping("/deletePayCol")
    protected ResultDto<Integer> deletePayCol(@RequestBody RptOperConstructPayCol rptOperConstructPayCol) {
        String pkId = rptOperConstructPayCol.getPkId();
        return new ResultDto<Integer>(rptOperConstructPayColService.deleteByPrimaryKey(pkId));
    }

    @PostMapping("/insert")
    protected ResultDto<Integer> insert(@RequestBody RptOperConstructPayCol rptOperConstructPayCol) {
        rptOperConstructPayCol.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptOperConstructPayColService.insertSelective(rptOperConstructPayCol));
    }

    @PostMapping("/updatePayCol")
    protected ResultDto<Integer> updatePayCol(@RequestBody RptOperConstructPayCol rptOperConstructPayCol) {
        return new ResultDto<Integer>(rptOperConstructPayColService.updateSelective(rptOperConstructPayCol));
    }
}
