package cn.com.yusys.yusp.web.server.xdxw0058;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0058.req.Xdxw0058DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0058.resp.Xdxw0058DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0058.Xdxw0058Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据流水号查询借据号
 *
 * @author zhangpeng
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDXW0058:根据流水号查询借据号")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0058Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0058Resource.class);
    @Autowired
    private Xdxw0058Service xdxw0058Service;

    /**
     * 交易码：xdxw0058
     * 交易描述：根据流水号查询借据号
     *
     * @param xdxw0058DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("申请人在本行当前逾期贷款数量")
    @PostMapping("/xdxw0058")
    protected @ResponseBody
    ResultDto<Xdxw0058DataRespDto> xdxw0058(@Validated @RequestBody Xdxw0058DataReqDto xdxw0058DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0058.key, DscmsEnum.TRADE_CODE_XDXW0058.value, JSON.toJSONString(xdxw0058DataReqDto));
        Xdxw0058DataRespDto xdxw0058DataRespDto = new Xdxw0058DataRespDto();// 响应Dto:根据流水号查询借据号
        ResultDto<Xdxw0058DataRespDto> xdxw0058DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0058.key, DscmsEnum.TRADE_CODE_XDXW0058.value, JSON.toJSONString(xdxw0058DataReqDto));
            xdxw0058DataRespDto = xdxw0058Service.xdxw0058(xdxw0058DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0058.key, DscmsEnum.TRADE_CODE_XDXW0058.value, JSON.toJSONString(xdxw0058DataRespDto));
            // 封装xdxw0058DataResultDto中正确的返回码和返回信息
            xdxw0058DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0058DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0058.key, DscmsEnum.TRADE_CODE_XDXW0058.value, e.getMessage());
            // 封装xdxw0058DataResultDto中异常返回码和返回信息
            xdxw0058DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0058DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0058DataRespDto到xdxw0058DataResultDto中
        xdxw0058DataResultDto.setData(xdxw0058DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0058.key, DscmsEnum.TRADE_CODE_XDXW0058.value, JSON.toJSONString(xdxw0058DataResultDto));
        return xdxw0058DataResultDto;
    }
}
