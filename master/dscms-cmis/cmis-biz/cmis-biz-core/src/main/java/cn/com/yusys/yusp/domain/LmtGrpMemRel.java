/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-dto模块
 * @类名称: LmtGrpMemRel
 * @类描述: lmt_grp_mem_rel数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-18 14:43:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_grp_mem_rel")
public class LmtGrpMemRel extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 集团申请流水号 **/
	@Column(name = "GRP_SERNO", unique = false, nullable = true, length = 40)
	private String grpSerno;

	/** 单一客户申请流水号 **/
	@Column(name = "SINGLE_SERNO", unique = false, nullable = true, length = 40)
	private String singleSerno;

	/** 集团客户编号 **/
	@Column(name = "GRP_CUS_ID", unique = false, nullable = true, length = 40)
	private String grpCusId;

	/** 集团客户名称 **/
	@Column(name = "GRP_CUS_NAME", unique = false, nullable = true, length = 80)
	private String grpCusName;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;

	/** 敞口额度 **/
	@Column(name = "OPEN_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal openLmtAmt;

	/** 低风险额度 **/
	@Column(name = "LOW_RISK_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lowRiskLmtAmt;

	/** 原敞口额度 **/
	@Column(name = "ORIGI_OPEN_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiOpenLmtAmt;

	/** 原低风险额度 **/
	@Column(name = "ORIGI_LOW_RISK_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiLowRiskLmtAmt;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 是否参与本次申报 **/
	@Column(name = "IS_PRTCPT_CURT_DECLARE", unique = false, nullable = true, length = 5)
	private String isPrtcptCurtDeclare;

	/** 客户经理提交状态 **/
	@Column(name = "MANAGER_ID_SUBMIT_STATUS", unique = false, nullable = true, length = 5)
	private String managerIdSubmitStatus;

	/** 是否可调剂 **/
	@Column(name = "IS_ADJUST_FLAG", unique = false, nullable = true, length = 5)
	private String isAdjustFlag;

	/** 本次是否调剂 **/
	@Column(name = "IS_CURT_ADJUST", unique = false, nullable = true, length = 5)
	private String isCurtAdjust;

	/** 是否申请变更 **/
	@Column(name = "IS_CURT_CHG", unique = false, nullable = true, length = 5)
	private String isCurtChg;

	/** 是否包含预授信额度 **/
	@Column(name = "IS_CONTAIN_PRE_LMT", unique = false, nullable = true, length = 5)
	private String isContainPreLmt;

	/** 本次是否细化 **/
	@Column(name = "IS_CURT_REFINE", unique = false, nullable = true, length = 5)
	private String isCurtRefine;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param grpSerno
	 */
	public void setGrpSerno(String grpSerno) {
		this.grpSerno = grpSerno;
	}

	/**
	 * @return grpSerno
	 */
	public String getGrpSerno() {
		return this.grpSerno;
	}

	/**
	 * @param singleSerno
	 */
	public void setSingleSerno(String singleSerno) {
		this.singleSerno = singleSerno;
	}

	/**
	 * @return singleSerno
	 */
	public String getSingleSerno() {
		return this.singleSerno;
	}

	/**
	 * @param grpCusId
	 */
	public void setGrpCusId(String grpCusId) {
		this.grpCusId = grpCusId;
	}

	/**
	 * @return grpCusId
	 */
	public String getGrpCusId() {
		return this.grpCusId;
	}

	/**
	 * @param grpCusName
	 */
	public void setGrpCusName(String grpCusName) {
		this.grpCusName = grpCusName;
	}

	/**
	 * @return grpCusName
	 */
	public String getGrpCusName() {
		return this.grpCusName;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	/**
	 * @return cusType
	 */
	public String getCusType() {
		return this.cusType;
	}

	/**
	 * @param openLmtAmt
	 */
	public void setOpenLmtAmt(java.math.BigDecimal openLmtAmt) {
		this.openLmtAmt = openLmtAmt;
	}

	/**
	 * @return openLmtAmt
	 */
	public java.math.BigDecimal getOpenLmtAmt() {
		return this.openLmtAmt;
	}

	/**
	 * @param lowRiskLmtAmt
	 */
	public void setLowRiskLmtAmt(java.math.BigDecimal lowRiskLmtAmt) {
		this.lowRiskLmtAmt = lowRiskLmtAmt;
	}

	/**
	 * @return lowRiskLmtAmt
	 */
	public java.math.BigDecimal getLowRiskLmtAmt() {
		return this.lowRiskLmtAmt;
	}

	/**
	 * @param origiOpenLmtAmt
	 */
	public void setOrigiOpenLmtAmt(java.math.BigDecimal origiOpenLmtAmt) {
		this.origiOpenLmtAmt = origiOpenLmtAmt;
	}

	/**
	 * @return origiOpenLmtAmt
	 */
	public java.math.BigDecimal getOrigiOpenLmtAmt() {
		return this.origiOpenLmtAmt;
	}

	/**
	 * @param origiLowRiskLmtAmt
	 */
	public void setOrigiLowRiskLmtAmt(java.math.BigDecimal origiLowRiskLmtAmt) {
		this.origiLowRiskLmtAmt = origiLowRiskLmtAmt;
	}

	/**
	 * @return origiLowRiskLmtAmt
	 */
	public java.math.BigDecimal getOrigiLowRiskLmtAmt() {
		return this.origiLowRiskLmtAmt;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param isPrtcptCurtDeclare
	 */
	public void setIsPrtcptCurtDeclare(String isPrtcptCurtDeclare) {
		this.isPrtcptCurtDeclare = isPrtcptCurtDeclare;
	}

	/**
	 * @return isPrtcptCurtDeclare
	 */
	public String getIsPrtcptCurtDeclare() {
		return this.isPrtcptCurtDeclare;
	}

	/**
	 * @param managerIdSubmitStatus
	 */
	public void setManagerIdSubmitStatus(String managerIdSubmitStatus) {
		this.managerIdSubmitStatus = managerIdSubmitStatus;
	}

	/**
	 * @return managerIdSubmitStatus
	 */
	public String getManagerIdSubmitStatus() {
		return this.managerIdSubmitStatus;
	}

	/**
	 * @param isAdjustFlag
	 */
	public void setIsAdjustFlag(String isAdjustFlag) {
		this.isAdjustFlag = isAdjustFlag;
	}

	/**
	 * @return isAdjustFlag
	 */
	public String getIsAdjustFlag() {
		return this.isAdjustFlag;
	}

	/**
	 * @param isCurtAdjust
	 */
	public void setIsCurtAdjust(String isCurtAdjust) {
		this.isCurtAdjust = isCurtAdjust;
	}

	/**
	 * @return isCurtAdjust
	 */
	public String getIsCurtAdjust() {
		return this.isCurtAdjust;
	}

	/**
	 * @param isCurtChg
	 */
	public void setIsCurtChg(String isCurtChg) {
		this.isCurtChg = isCurtChg;
	}

	/**
	 * @return isCurtChg
	 */
	public String getIsCurtChg() {
		return this.isCurtChg;
	}

	/**
	 * @param isContainPreLmt
	 */
	public void setIsContainPreLmt(String isContainPreLmt) {
		this.isContainPreLmt = isContainPreLmt;
	}

	/**
	 * @return isContainPreLmt
	 */
	public String getIsContainPreLmt() {
		return this.isContainPreLmt;
	}

	/**
	 * @param isCurtRefine
	 */
	public void setIsCurtRefine(String isCurtRefine) {
		this.isCurtRefine = isCurtRefine;
	}

	/**
	 * @return isCurtRefine
	 */
	public String getIsCurtRefine() {
		return this.isCurtRefine;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}