package cn.com.yusys.yusp.service.client.bsp.pjxt.xdpj14;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14.Xdpj14ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14.Xdpj14RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2IrsClientService;
import cn.com.yusys.yusp.service.Dscms2PjxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：信贷签约通知
 *
 * @author quwen
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Xdpj14Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdpj14Service.class);

    // 1）注入：BSP封装调用票据系统系统的接口
    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;

    /**
     * 业务逻辑处理方法：信贷签约通知
     *
     * @param xdpj14ReqDto
     * @return
     */
    @Transactional
    public Xdpj14RespDto xdpj14(Xdpj14ReqDto xdpj14ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ14.key, EsbEnum.TRADE_CODE_XDPJ14.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ14.key, EsbEnum.TRADE_CODE_XDPJ14.value, JSON.toJSONString(xdpj14ReqDto));
        ResultDto<Xdpj14RespDto> xdpj14ResultDto = dscms2PjxtClientService.xdpj14(xdpj14ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ14.key, EsbEnum.TRADE_CODE_XDPJ14.value, JSON.toJSONString(xdpj14ResultDto));

        String xdpj14Code = Optional.ofNullable(xdpj14ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String xdpj14Meesage = Optional.ofNullable(xdpj14ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Xdpj14RespDto xdpj14RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdpj14ResultDto.getCode())) {
            //  获取相关的值并解析
            xdpj14RespDto = xdpj14ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, xdpj14Code, xdpj14Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ14.key, EsbEnum.TRADE_CODE_XDPJ14.value);
        return xdpj14RespDto;
    }
}
