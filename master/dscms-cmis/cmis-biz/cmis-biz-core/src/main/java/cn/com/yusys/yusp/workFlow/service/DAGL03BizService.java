package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.DocReadAppInfo;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.DocReadAppInfoService;
import cn.com.yusys.yusp.service.DocReadDetailInfoService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hujun
 * @className DocumentAccessApplyAppBiz
 * @Description 档案调阅申请后处理
 * @Date 2021/06/12
 */
@Service
public class DAGL03BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(DAGL03BizService.class);

    @Autowired
    private DocReadAppInfoService docReadAppInfoService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private DocReadDetailInfoService docReadDetailInfoService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("档案调阅审批后业务处理类型" + currentOpType);
        try {
            // 更新条件参数
            WFBizParamDto param = new WFBizParamDto();
            Map<String, Object> params = new HashMap<>();
            param.setBizId(resultInstanceDto.getBizId());
            param.setInstanceId(resultInstanceDto.getInstanceId());
            DocReadAppInfo info = docReadAppInfoService.selectByPrimaryKey(serno);
            params.put("readType", info.getReadType());
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);

            if (OpType.STRAT.equals(currentOpType)) {
                log.info("档案调阅申请" + serno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("档案调阅申请" + serno + "流程提交操作，流程参数" + resultInstanceDto);
                info.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                docReadAppInfoService.updateSelective(info);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("档案调阅申请" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("档案调阅申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理-修改业务数据状态为通过
                info.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                docReadAppInfoService.updateSelective(info);
                // 审批通过后需要修改档案台账表中对应的数据的状态，归还日期等信息；-2021-06-18 hujun
                docReadAppInfoService.updateDocStsApply(info.getDraiSerno(), info.getBackDate());
                // 档案申请出库
                docReadAppInfoService.applyDelivery(info.getDraiSerno());
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    info.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    docReadAppInfoService.updateSelective(info);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("档案调阅申请" + serno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    info.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    docReadAppInfoService.updateSelective(info);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("档案调阅申请" + serno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    info.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    docReadAppInfoService.updateSelective(info);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("档案调阅申请" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                info.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                docReadAppInfoService.updateSelective(info);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("档案调阅申请" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                info.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                docReadAppInfoService.updateSelective(info);
                // 调阅申请审否决后还原引入的台账状态为 03:"已入库"
                docReadDetailInfoService.returnDocAccStatus(info);
            } else {
                log.warn("档案调阅申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DAGL03.equals(flowCode);
    }
}
