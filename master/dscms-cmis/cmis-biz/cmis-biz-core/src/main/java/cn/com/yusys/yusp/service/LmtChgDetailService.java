/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtChgDetail;
import cn.com.yusys.yusp.domain.LmtIntbankApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtChgDetailMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtChgDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:18:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtChgDetailService extends BizInvestCommonService{
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtChgDetailService.class);

    @Resource
    private LmtChgDetailMapper lmtChgDetailMapper;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtIntbankAppService lmtIntbankAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtChgDetail selectByPrimaryKey(String pkId) {
        return lmtChgDetailMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtChgDetail> selectAll(QueryModel model) {
        List<LmtChgDetail> records = (List<LmtChgDetail>) lmtChgDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtChgDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtChgDetail> list = lmtChgDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtChgDetail record) {
        return lmtChgDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtChgDetail record) {
        return lmtChgDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public int update(LmtChgDetail record) {
        record.setUpdId(getCurrentUser().getLoginCode());
        record.setUpdBrId(getCurrentUser().getOrg().getCode());
        record.setUpdDate(getCurrrentDateStr());
        record.setUpdateTime(new Date());
        return lmtChgDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtChgDetail record) {
        return lmtChgDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtChgDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtChgDetailMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: saveLmtChgDetail
     * @方法描述: 保存授信变更从表信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map saveLmtChgDetail(LmtChgDetail lmtChgDetail) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            lmtChgDetail.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            User userInfo = SessionUtils.getUserInformation();
            lmtChgDetail.setInputId(userInfo.getLoginCode());
            lmtChgDetail.setInputBrId(userInfo.getOrg().getCode());
            lmtChgDetail.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtChgDetail.setUpdId(userInfo.getLoginCode());
            lmtChgDetail.setUpdBrId(userInfo.getOrg().getCode());
            lmtChgDetail.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtChgDetail.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtChgDetail.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

            int count = lmtChgDetailMapper.insert(lmtChgDetail);

            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("新增授信变更从表信息情况！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: updateBySerno
     * @方法描述: 授信变更删除后，将对应授信变更从表信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public boolean updateBySerno(String lmtSerno) {
        Map map = new HashMap();
        map.put("lmtSerno",lmtSerno);
        map.put("oprType","01");
        List<LmtChgDetail> list = lmtChgDetailMapper.selectByParams(map);
        if(list == null || list.size() == 0 ){
            return true;
        }
        int count = lmtChgDetailMapper.updateBySerno(lmtSerno);
        if (count != 1) {
            //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
            throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
        }
        return true;
    }

    /**
     * 主体授信-授信变更-变更申请书
     * @param serno
     * @return
     */
    public int insertLmtChgDetail(String serno) {
        LmtChgDetail lmtChgDetail = new LmtChgDetail();
        lmtChgDetail.setPkId(generatePkId());
        lmtChgDetail.setLmtSerno(serno);
        lmtChgDetail.setOprType(CmisBizConstants.OPR_TYPE_01);
        lmtChgDetail.setInputBrId(getCurrentUser().getOrg().getCode());
        lmtChgDetail.setInputId(getCurrentUser().getLoginCode());
        lmtChgDetail.setCreateTime(getCurrrentDate());
        lmtChgDetail.setUpdateTime(getCurrrentDate());
        lmtChgDetail.setUpdDate(getCurrrentDateStr());
        lmtChgDetail.setUpdBrId(getCurrentUser().getOrg().getCode());
        lmtChgDetail.setUpdId(getCurrentUser().getLoginCode());
        return insert(lmtChgDetail);
    }

    /**
     * 根据流水号获取对象
     * @param lmtSerno
     * @return
     */
    public LmtChgDetail selectByLmtSerno(String lmtSerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("lmtSerno",lmtSerno);
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<LmtChgDetail> lmtChgDetails = selectByModel(queryModel);
        if (lmtChgDetails!=null && lmtChgDetails.size()>0){
            return lmtChgDetails.get(0);
        }
        return null;
    }

    /**
     * 根据流水号获取对象
     * @param lmtSerno
     * @return
     */
    public LmtChgDetail selectByLmtSerno1(String lmtSerno) {
        return lmtChgDetailMapper.selectByLmtSerno1(lmtSerno);
    }

    /**
     * 根据授信申请流水号查询分项
     * @param lmtSerno
     * @return
     */
    public List<Map<String,String>> queryDetailByLmtSerno(String lmtSerno) {
        List<Map<String,String>> list = new ArrayList<>();
        List<LmtChgDetail> lmtChgDetails = lmtChgDetailMapper.queryDetailByLmtSerno(lmtSerno);
        if(lmtChgDetails.isEmpty() || lmtChgDetails.size() == 0){
            return list;
        }else{
            for(LmtChgDetail lmtChgDetail : lmtChgDetails){
                HashMap<String , String> map = new HashMap();
                // 处理成员客户
                // 获取成员客户号
                LmtApp lmtApp = lmtAppService.selectBySerno(lmtChgDetail.getLmtSerno());
                if(lmtApp != null ){
                    map.put("cusId",lmtApp.getCusId());
                    map.put("cusName",lmtApp.getCusName());
                }
                map.put("pkId",lmtChgDetail.getPkId());
                map.put("lmtSerno",lmtChgDetail.getLmtSerno());
                map.put("origiLmtSurvey",lmtChgDetail.getOrigiLmtSurvey());
                map.put("lmtChgContent",lmtChgDetail.getLmtChgContent());
                map.put("lmtChgResn",lmtChgDetail.getLmtChgResn());
                map.put("inputId",lmtChgDetail.getInputId());
                map.put("inputBrId",lmtChgDetail.getInputBrId());
                map.put("inputDate",lmtChgDetail.getInputDate());
                list.add(map);
            }
        }

        return list;
    }

    /**
     * 保存变更申请表
     * @param lmtChgDetail
     * @return
     */
    public int updateBgsqb(LmtChgDetail lmtChgDetail) {
        String lmtSerno = lmtChgDetail.getLmtSerno();
        updateMustCheckStatus(lmtSerno,"bgsqb");
        return update(lmtChgDetail);
    }
}
