/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.RptOperEnergyCons;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.RptOperationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptOperation;
import cn.com.yusys.yusp.repository.mapper.RptOperationMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperationService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 09:33:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptOperationService {

    @Autowired
    private RptOperationMapper rptOperationMapper;

    @Autowired
    private RptOperEnergyConsService rptOperEnergyConsService;

    @Autowired
    private RptOperEnergySalesService rptOperEnergySalesService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private LmtAppService lmtAppService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptOperation selectByPrimaryKey(String serno) {
        return rptOperationMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptOperation> selectAll(QueryModel model) {
        List<RptOperation> records = (List<RptOperation>) rptOperationMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptOperation> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptOperation> list = rptOperationMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptOperation record) {
        return rptOperationMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptOperation record) {
        return rptOperationMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptOperation record) {
        return rptOperationMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptOperation record) {
        return rptOperationMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptOperationMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptOperationMapper.deleteByIds(ids);
    }

    /**
     * 修改经营情况
     * @param rptOperation
     * @return
     */
    public int updateRptOperation(RptOperation rptOperation){
        int count = 0;
        String serno = rptOperation.getSerno();
        RptOperation tem = rptOperationMapper.selectByPrimaryKey(serno);
        if(tem!=null){
            count = rptOperationMapper.updateByPrimaryKeySelective(rptOperation);
        }else{
            count = rptOperationMapper.insertSelective(rptOperation);
        }
        return count;
    }

    /**
     * 引入行业分类
     * @param rptOperation
     * @return
     */
    public int insertRptOperation(RptOperation rptOperation){
        int count = 0;
        String serno = rptOperation.getSerno();
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        ResultDto<CusCorpDto> cusCorpDtoResultDto = iCusClientService.queryCusCropDtoByCusId(lmtApp.getCusId());
        if(cusCorpDtoResultDto !=null&&cusCorpDtoResultDto.getData()!=null){
            String tradeClass = cusCorpDtoResultDto.getData().getTradeClass();
            rptOperation.setTradeClass(tradeClass);
        }
        count = rptOperationMapper.insertSelective(rptOperation);
        return count;

    }
}
