package cn.com.yusys.yusp.web.server.xdxw0068;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0068.req.Xdxw0068DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0068.resp.Xdxw0068DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0068.Xdxw0068Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:授信调查结论信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0068:授信调查结论信息查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0068Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0068Resource.class);

    @Autowired
    private Xdxw0068Service xdxw0068Service;
    /**
     * 交易码：xdxw0068
     * 交易描述：授信调查结论信息查询
     *
     * @param xdxw0068DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("授信调查结论信息查询")
    @PostMapping("/xdxw0068")
    protected @ResponseBody
    ResultDto<Xdxw0068DataRespDto> xdxw0068(@Validated @RequestBody Xdxw0068DataReqDto xdxw0068DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, JSON.toJSONString(xdxw0068DataReqDto));
        Xdxw0068DataRespDto xdxw0068DataRespDto = new Xdxw0068DataRespDto();// 响应Dto:授信调查结论信息查询
        ResultDto<Xdxw0068DataRespDto> xdxw0068DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0068DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, JSON.toJSONString(xdxw0068DataReqDto));
            xdxw0068DataRespDto = xdxw0068Service.getComSurveyReportList(xdxw0068DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, JSON.toJSONString(xdxw0068DataRespDto));
            // 封装xdxw0068DataResultDto中正确的返回码和返回信息
            xdxw0068DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0068DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, e.getMessage());
            // 封装xdxw0068DataResultDto中异常返回码和返回信息
            xdxw0068DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0068DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }
        // 封装xdxw0068DataRespDto到xdxw0068DataResultDto中
        xdxw0068DataResultDto.setData(xdxw0068DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, JSON.toJSONString(xdxw0068DataResultDto));
        return xdxw0068DataResultDto;
    }
}
