package cn.com.yusys.yusp.service.server.xdxw0078;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtGuareCloestInfo;
import cn.com.yusys.yusp.dto.server.xdxw0078.req.Xdxw0078DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0078.resp.Xdxw0078DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtInspectInfoMapper;
import cn.com.yusys.yusp.service.LmtGuareCloestInfoService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class Xdxw0078Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0078Service.class);

    @Autowired
    private LmtGuareCloestInfoService lmtGuareCloestInfoService;

    /**
     * 推送云评估信息
     *
     * @param xdxw0078DataReqDto
     * @return
     */
    public Xdxw0078DataRespDto xdxw0078(Xdxw0078DataReqDto xdxw0078DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0078DataReqDto));
        Xdxw0078DataRespDto xdxw0078DataRespDto = new Xdxw0078DataRespDto();
        try {
            //楼盘
            String housename = xdxw0078DataReqDto.getHousename();
            //楼栋
            String hbuild = xdxw0078DataReqDto.getHbuild();
            //楼层
            String hfloor = xdxw0078DataReqDto.getHfloor();
            //房间号
            String roomid = xdxw0078DataReqDto.getRoomid();
            //估价
            String judge = xdxw0078DataReqDto.getJudge();
            //总价
            String total = xdxw0078DataReqDto.getTotal();
            //面积
            String area = xdxw0078DataReqDto.getArea();
            //地址
            String hregion = xdxw0078DataReqDto.getHregion();
            //查询人员
            String speople = xdxw0078DataReqDto.getSpeople();
            //查询日期
            String sdate = xdxw0078DataReqDto.getSdate();
            //查询人ID
            String managerid = xdxw0078DataReqDto.getManagerid();
            //电话号码
            String tel = xdxw0078DataReqDto.getTel();
            // 抵押物云评估信息
            LmtGuareCloestInfo  lmtGuareCloestInfo = new LmtGuareCloestInfo();
            lmtGuareCloestInfo.setBuildingName(housename);
            lmtGuareCloestInfo.setBuilding(hbuild);
            lmtGuareCloestInfo.setFloor(hfloor);
            lmtGuareCloestInfo.setRoomNo(roomid);
            lmtGuareCloestInfo.setAssEvaAmt(new BigDecimal(judge));
            lmtGuareCloestInfo.setTotalAmt(new BigDecimal(total));
            lmtGuareCloestInfo.setSqu(area);
            lmtGuareCloestInfo.setQryUser(speople);
            lmtGuareCloestInfo.setQryDate(sdate);
            lmtGuareCloestInfo.setQryId(managerid);
            lmtGuareCloestInfo.setPhone(tel);
            lmtGuareCloestInfo.setAddr(hregion);
            int result = lmtGuareCloestInfoService.insertSelective(lmtGuareCloestInfo);
            if (result ==1) {
                xdxw0078DataRespDto.setOpFlag("S");
                xdxw0078DataRespDto.setOpMsg("数据新增成功！");
            }else{
                xdxw0078DataRespDto.setOpFlag("F");
                xdxw0078DataRespDto.setOpMsg("数据库操作失败！");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0078DataRespDto));
        return xdxw0078DataRespDto;
    }
}
