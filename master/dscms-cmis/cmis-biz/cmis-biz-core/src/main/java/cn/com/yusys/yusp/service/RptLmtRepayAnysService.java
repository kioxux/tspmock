/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptLmtRepayAnys;
import cn.com.yusys.yusp.repository.mapper.RptLmtRepayAnysMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-06-23 18:37:15
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptLmtRepayAnysService {

    @Autowired
    private RptLmtRepayAnysMapper rptLmtRepayAnysMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptLmtRepayAnys selectByPrimaryKey(String serno) {
        return rptLmtRepayAnysMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptLmtRepayAnys> selectAll(QueryModel model) {
        List<RptLmtRepayAnys> records = (List<RptLmtRepayAnys>) rptLmtRepayAnysMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptLmtRepayAnys> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptLmtRepayAnys> list = rptLmtRepayAnysMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptLmtRepayAnys record) {
        return rptLmtRepayAnysMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptLmtRepayAnys record) {
        return rptLmtRepayAnysMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptLmtRepayAnys record) {
        return rptLmtRepayAnysMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptLmtRepayAnys record) {
        return rptLmtRepayAnysMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptLmtRepayAnysMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptLmtRepayAnysMapper.deleteByIds(ids);
    }

    /**
     * 保存信息
     *
     * @param rptLmtRepayAnys
     * @return
     */
    public int saveAnys(RptLmtRepayAnys rptLmtRepayAnys) {
        String serno = rptLmtRepayAnys.getSerno();
        RptLmtRepayAnys temp = rptLmtRepayAnysMapper.selectByPrimaryKey(serno);
        int count = 0;
        if (temp != null) {
            count = rptLmtRepayAnysMapper.updateByPrimaryKeySelective(rptLmtRepayAnys);
        } else {
            count = rptLmtRepayAnysMapper.insertSelective(rptLmtRepayAnys);
        }
        return count;
    }
}
