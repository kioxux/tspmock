package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtAppSub;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import cn.com.yusys.yusp.domain.ZxdPreLmtApply;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.repository.mapper.ZxdPreLmtApplyMapper;
import cn.com.yusys.yusp.service.LmtAppService;
import cn.com.yusys.yusp.service.LmtAppSubPrdService;
import cn.com.yusys.yusp.service.LmtAppSubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class RiskItem0128Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0128Service.class);

    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private ZxdPreLmtApplyMapper zxdPreLmtApplyMapper;

    /**
     * @方法名称: riskItem0128
     * @方法描述: 征信贷授信额度校验
     * @参数与返回说明:
     * @算法描述:
     * 征信评分卡中通过的借款人预期借款额度>= 征信贷授信申请中授信额度(元)：
     * @创建人: zhangliang15
     * @创建时间: 2021-10-04 09:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0128(QueryModel queryModel) {
        // 获取申请流水
        String serno = queryModel.getCondition().get("bizId").toString();
        String applyRecordId = "";//征信贷评分卡对接记录ID

        log.info("征信贷授信额度校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();

        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 查询授信申请信息
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        if (Objects.isNull(lmtApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }
        // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
        Map paramsSub = new HashMap();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtAppSub> subList = lmtAppSubService.selectByParams(paramsSub);
        if (Objects.isNull(subList) || subList.isEmpty()) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        // 获取授信分项产品明细
        for(LmtAppSub lmtAppSub : subList){
            Map paramsSubPrd = new HashMap();
            paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
            paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
            if (Objects.isNull(subListPrd) || subListPrd.isEmpty()) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0005);
                return riskResultDto;
            }
            for(LmtAppSubPrd lmtAppSubPrd : subListPrd){
                // P009:征信贷
                if (Objects.equals("P009", lmtAppSubPrd.getLmtBizTypeProp())) {
                    //根据对接记录Id获取征信贷评分卡信息
                    ZxdPreLmtApply zxdPreLmtApply = zxdPreLmtApplyMapper.selectByPrimaryKey(lmtAppSubPrd.getApplyRecordId());
                    if(Objects.isNull(zxdPreLmtApply) || Objects.isNull(zxdPreLmtApply.getBorrowerLossLmt())){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00093);
                        return riskResultDto;
                    }
                    // 如果为征信贷，征信评分卡中通过的借款人预期借款额度>= 征信贷授信申请中授信额度(元)：
                    if((zxdPreLmtApply.getBorrowerLossLmt()).compareTo(lmtAppSubPrd.getLmtAmt()) < 0){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00094);
                        return riskResultDto;
                    }
                }
            }
        }

        log.info("征信贷授信额度校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}