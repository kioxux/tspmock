/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtAppRelCusDetailsLimit;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022LmtListRespDto;
import cn.com.yusys.yusp.repository.mapper.LmtAppRelCusDetailsLimitMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppRelCusDetailsLimitService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-19 10:09:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtAppRelCusDetailsLimitService extends BizInvestCommonService{

    @Autowired
    private LmtAppRelCusDetailsLimitMapper lmtAppRelCusDetailsLimitMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtAppRelCusDetailsLimit selectByPrimaryKey(String pkId) {
        return lmtAppRelCusDetailsLimitMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtAppRelCusDetailsLimit> selectAll(QueryModel model) {
        List<LmtAppRelCusDetailsLimit> records = (List<LmtAppRelCusDetailsLimit>) lmtAppRelCusDetailsLimitMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtAppRelCusDetailsLimit> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtAppRelCusDetailsLimit> list = lmtAppRelCusDetailsLimitMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtAppRelCusDetailsLimit record) {
        return lmtAppRelCusDetailsLimitMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtAppRelCusDetailsLimit record) {
        return lmtAppRelCusDetailsLimitMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtAppRelCusDetailsLimit record) {
        return lmtAppRelCusDetailsLimitMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtAppRelCusDetailsLimit record) {
        return lmtAppRelCusDetailsLimitMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtAppRelCusDetailsLimitMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtAppRelCusDetailsLimitMapper.deleteByIds(ids);
    }

    /**
     * 根据流水号获取资金业务存量授信情况
     * @param serno
     * @return
     */
    public LmtAppRelCusDetailsLimit selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<LmtAppRelCusDetailsLimit> lmtAppRelCusDetailsLimits = selectByModel(queryModel);
        if (lmtAppRelCusDetailsLimits!=null && lmtAppRelCusDetailsLimits.size()>0){
            return lmtAppRelCusDetailsLimits.get(0);
        }
        return null;
    }

    /**
     * 添加申请关联客户额度明细信息
     * @param serno
     * @param cmisLmt0022LmtListRespDto
     */
    public int insertRelCusDetails(String serno, CmisLmt0022LmtListRespDto cmisLmt0022LmtListRespDto) {
        LmtAppRelCusDetailsLimit lmtAppRelCusDetailsLimit = new LmtAppRelCusDetailsLimit();
        BeanUtils.copyProperties(cmisLmt0022LmtListRespDto,lmtAppRelCusDetailsLimit);
        lmtAppRelCusDetailsLimit.setPkId(generatePkId());
        lmtAppRelCusDetailsLimit.setSerno(serno);
        //额度分项流水号
        lmtAppRelCusDetailsLimit.setSubSerno(cmisLmt0022LmtListRespDto.getApprSubSerno());
//        lmtAppRelCusDetailsLimit.setSubLmtNo();
//        lmtAppRelCusDetailsLimit.setCurType();
        BigDecimal outstandAmt = changeValueToBigDecimal(cmisLmt0022LmtListRespDto.getOutstandAmt());
        BigDecimal lmtAmt = changeValueToBigDecimal(cmisLmt0022LmtListRespDto.getLmtAmt());
        //可用额度
        lmtAppRelCusDetailsLimit.setAvlLmtAmt(lmtAmt.subtract(outstandAmt));
        //已用额度
        lmtAppRelCusDetailsLimit.setOutstndAmt(changeValueToBigDecimal(cmisLmt0022LmtListRespDto.getOutstandAmt()));
        lmtAppRelCusDetailsLimit.setLmtEndDate(cmisLmt0022LmtListRespDto.getEndDate());
        //产品名称 采用 项目名称
        lmtAppRelCusDetailsLimit.setPrdName(cmisLmt0022LmtListRespDto.getProName());
//        lmtAppRelCusDetailsLimit.setTranDate();
//        lmtAppRelCusDetailsLimit.setAssetEndDate(cmisLmt0022LmtListRespDto.getEndDate());
        //初始化(新增)通用domain信息
        initInsertDomainPropertiesForWF(lmtAppRelCusDetailsLimit);
        lmtAppRelCusDetailsLimit.setInputId(cmisLmt0022LmtListRespDto.getInputId());
        lmtAppRelCusDetailsLimit.setInputBrId(cmisLmt0022LmtListRespDto.getInputBrId());
        lmtAppRelCusDetailsLimit.setTranDate(cmisLmt0022LmtListRespDto.getInputDate());
        lmtAppRelCusDetailsLimit.setInputDate(cmisLmt0022LmtListRespDto.getInputDate());
        return insert(lmtAppRelCusDetailsLimit);
    }
}
