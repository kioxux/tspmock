package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.BailExtractApp;
import cn.com.yusys.yusp.domain.DocAsplTcont;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.BailExtractAppService;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import cn.com.yusys.yusp.service.DocAsplTcontService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @className BailExtractApp
 * @Description 购销合同业务后处理
 * @Date 2020/12/21 : 10:43
 */
@Service
public class ZCYW03BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(ZCYW03BizService.class);

    @Autowired
    private  DocAsplTcontService docAsplTcontService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;


    @Autowired
    private WorkflowCoreClient workflowCoreClient;


    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String tcontImgId = resultInstanceDto.getBizId();
        try {

            DocAsplTcont docAsplTcont = docAsplTcontService.selectByTcontSerno(tcontImgId);
            // 获取当前业务申请路程类型
            String flowCode = resultInstanceDto.getBizType();
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("购销合同业务申请" + tcontImgId + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("购销合同业务申请" + tcontImgId + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                // 正常提交下一步处理   审批中 111
                docAsplTcontService.updateApproveStatusAfterFlow(docAsplTcont,CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("购销合同业务申请" + tcontImgId + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("购销合同业务申请" + tcontImgId + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.更新贸易合同配置表的审批状态 由审批中111 -> 审批通过 997

                docAsplTcontService.updateApproveStatusAfterFlow(docAsplTcont,CmisCommonConstants.WF_STATUS_997);
                sendImage(resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("购销合同业务申请" + tcontImgId + "流程退回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    docAsplTcontService.updateApproveStatusAfterFlow(docAsplTcont,CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("购销合同业务申请" + tcontImgId + "流程打回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    docAsplTcontService.updateApproveStatusAfterFlow(docAsplTcont,CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("购销合同业务申请" + tcontImgId + "流程拿回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    docAsplTcontService.updateApproveStatusAfterFlow(docAsplTcont,CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("购销合同业务申请" + tcontImgId + "拿回初始节点操作，流程参数" + resultInstanceDto);
                //流程拿回到第一个节点，申请主表的业务
                docAsplTcontService.updateApproveStatusAfterFlow(docAsplTcont,CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("购销合同业务申请" + tcontImgId + "否决操作，流程参数" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                docAsplTcontService.updateApproveStatusAfterFlow(docAsplTcont,CmisCommonConstants.WF_STATUS_998);
            } else {
                log.info("购销合同业务申请" + tcontImgId + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
            // 贸易合同业务 审批流程
        return "ZCYW03".equals(flowCode);
    }

    /**
     * 推送影像审批信息
     * @author xs
     * @date 2021-10-04 19:52:42
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {
        //根据流程实例获取所有审批意见
        ResultDto<List<ResultCommentDto>> resultCommentDtos = workflowCoreClient.getAllComments(resultInstanceDto.getInstanceId());
        List<ResultCommentDto> data = resultCommentDtos.getData();
        //审批人
        String approveUserId = "";
        for (ResultCommentDto resultCommentDto : data) {
            String nodeId = resultCommentDto.getNodeId();
            if ("357_6".equals(nodeId)){//集中作业放款复审岗
                String userId = resultCommentDto.getUserId();
                if(!approveUserId.contains(userId)){
                    //审批人不能重复
                    approveUserId = approveUserId+","+userId;
                }
            }
        }
        if(approveUserId.length() > 0){
            // 一般情况下，直接获取流程实列中的参数。
            Map<String, Object> map = (Map) JSON.parse(resultInstanceDto.getFlowParam());
            String topOutsystemCode = (String) map.get("topOutsystemCode");
            if(StringUtils.isEmpty(topOutsystemCode)){
                throw BizException.error(null, "9999", "影像审核参数【topOutsystemCode】为空");
            }
            Map<String, String> imageParams = (Map<String, String>) map.get("imageParams");
            String docid = imageParams.get("businessid");
            if(StringUtils.isEmpty(docid)){
                throw BizException.error(null, "9999", "影像审核参数【businessid】为空");
            }
            approveUserId = approveUserId.substring(1);
            String[] arr = topOutsystemCode.split(";");
            for (int i = 0; i < arr.length; i++) {
                ImageApprDto imageApprDto = new ImageApprDto();
                imageApprDto.setDocId(docid);//任务编号
                imageApprDto.setApproval("同意");//审批意见
                imageApprDto.setIsApproved("1");//审批状态1通过-1不通过3作废
                imageApprDto.setOutcode(arr[i]);//文件类型根节点
                imageApprDto.setOpercode(approveUserId);//审批人员
                cmisBizXwCommonService.sendImage(imageApprDto);
            }
        }
//        ResultDto<ResultInstanceDto> resultDto = workflowCoreClient.myinstanceInfoHis(resultInstanceDto.getInstanceId(),"357_6");
//        ImageApprDto imageApprDto = new ImageApprDto();
//        imageApprDto.setDocId(resultInstanceDto.getBizId());//任务编号
//        imageApprDto.setApproval("1");//审批状态1通过-1不通过3作废
//        imageApprDto.setOutcode("XXD_ZCC;XXD_ZCC02");//文件类型根节点
//        imageApprDto.setOpercode(resultDto.getData().getBizUserId());//审批人员
//        cmisBizXwCommonService.sendImage(imageApprDto);
    }
}
