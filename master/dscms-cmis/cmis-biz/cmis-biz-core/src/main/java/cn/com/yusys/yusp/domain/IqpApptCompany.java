/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApptCompany
 * @类描述: iqp_appt_company数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-10 10:15:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_appt_company")
public class IqpApptCompany extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 贷款申请人主键 **/
	@Column(name = "APPT_CODE", unique = false, nullable = false, length = 32)
	private String apptCode;
	
	/** 职业 **/
	@Column(name = "PFN", unique = false, nullable = true, length = 5)
	private String pfn;
	
	/** 职务 **/
	@Column(name = "DUT", unique = false, nullable = true, length = 5)
	private String dut;
	
	/** 职称 **/
	@Column(name = "PRO_TITLE", unique = false, nullable = true, length = 10)
	private String proTitle;
	
	/** 单位性质 **/
	@Column(name = "TYPE", unique = false, nullable = true, length = 2)
	private String type;
	
	/** 单位名称 **/
	@Column(name = "WORK_PLACE_NAME", unique = false, nullable = true, length = 128)
	private String workPlaceName;
	
	/** 统一社会信用代码 **/
	@Column(name = "UNIFY_CREDIT_CODE", unique = false, nullable = true, length = 40)
	private String unifyCreditCode;
	
	/** 单位地址省/直辖市 **/
	@Column(name = "ADDR_PROVIENCE", unique = false, nullable = true, length = 12)
	private String addrProvience;
	
	/** 单位地址-市 **/
	@Column(name = "ADDR_CITY", unique = false, nullable = true, length = 12)
	private String addrCity;
	
	/** 单位地址-区 **/
	@Column(name = "ADDR_AREAR", unique = false, nullable = true, length = 12)
	private String addrArear;
	
	/** 单位地址 **/
	@Column(name = "ADDR_DETAIL", unique = false, nullable = true, length = 100)
	private String addrDetail;
	
	/** 单位所属行业 **/
	@Column(name = "KIND", unique = false, nullable = true, length = 5)
	private String kind;
	
	/** 单位电话 **/
	@Column(name = "TEL", unique = false, nullable = true, length = 30)
	private String tel;
	
	/** 进入现单位时间 **/
	@Column(name = "COMEIN_DT", unique = false, nullable = true, length = 10)
	private String comeinDt;
	
	/** 所属部门 **/
	@Column(name = "DEP", unique = false, nullable = true, length = 20)
	private String dep;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode;
	}
	
    /**
     * @return apptCode
     */
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param pfn
	 */
	public void setPfn(String pfn) {
		this.pfn = pfn;
	}
	
    /**
     * @return pfn
     */
	public String getPfn() {
		return this.pfn;
	}
	
	/**
	 * @param dut
	 */
	public void setDut(String dut) {
		this.dut = dut;
	}
	
    /**
     * @return dut
     */
	public String getDut() {
		return this.dut;
	}
	
	/**
	 * @param proTitle
	 */
	public void setProTitle(String proTitle) {
		this.proTitle = proTitle;
	}
	
    /**
     * @return proTitle
     */
	public String getProTitle() {
		return this.proTitle;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
    /**
     * @return type
     */
	public String getType() {
		return this.type;
	}
	
	/**
	 * @param workPlaceName
	 */
	public void setWorkPlaceName(String workPlaceName) {
		this.workPlaceName = workPlaceName;
	}
	
    /**
     * @return workPlaceName
     */
	public String getWorkPlaceName() {
		return this.workPlaceName;
	}
	
	/**
	 * @param unifyCreditCode
	 */
	public void setUnifyCreditCode(String unifyCreditCode) {
		this.unifyCreditCode = unifyCreditCode;
	}
	
    /**
     * @return unifyCreditCode
     */
	public String getUnifyCreditCode() {
		return this.unifyCreditCode;
	}
	
	/**
	 * @param addrProvience
	 */
	public void setAddrProvience(String addrProvience) {
		this.addrProvience = addrProvience;
	}
	
    /**
     * @return addrProvience
     */
	public String getAddrProvience() {
		return this.addrProvience;
	}
	
	/**
	 * @param addrCity
	 */
	public void setAddrCity(String addrCity) {
		this.addrCity = addrCity;
	}
	
    /**
     * @return addrCity
     */
	public String getAddrCity() {
		return this.addrCity;
	}
	
	/**
	 * @param addrArear
	 */
	public void setAddrArear(String addrArear) {
		this.addrArear = addrArear;
	}
	
    /**
     * @return addrArear
     */
	public String getAddrArear() {
		return this.addrArear;
	}
	
	/**
	 * @param addrDetail
	 */
	public void setAddrDetail(String addrDetail) {
		this.addrDetail = addrDetail;
	}
	
    /**
     * @return addrDetail
     */
	public String getAddrDetail() {
		return this.addrDetail;
	}
	
	/**
	 * @param kind
	 */
	public void setKind(String kind) {
		this.kind = kind;
	}
	
    /**
     * @return kind
     */
	public String getKind() {
		return this.kind;
	}
	
	/**
	 * @param tel
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}
	
    /**
     * @return tel
     */
	public String getTel() {
		return this.tel;
	}
	
	/**
	 * @param comeinDt
	 */
	public void setComeinDt(String comeinDt) {
		this.comeinDt = comeinDt;
	}
	
    /**
     * @return comeinDt
     */
	public String getComeinDt() {
		return this.comeinDt;
	}
	
	/**
	 * @param dep
	 */
	public void setDep(String dep) {
		this.dep = dep;
	}
	
    /**
     * @return dep
     */
	public String getDep() {
		return this.dep;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}