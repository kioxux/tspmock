/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

/**
 * @项目名称: cmis-biz-coreModule
 * @类名称: OtherDiscPerferRateApp
 * @类描述: other_disc_perfer_rate_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-03 16:12:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_disc_perfer_rate_app")
public class OtherDiscPerferRateApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 银行存款余额 **/
	@Column(name = "BANK_DEP_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bankDepBal;
	
	/** 银行贷款余额 **/
	@Column(name = "BANK_LOAN_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bankLoanBal;
	
	/** 存款日均 **/
	@Column(name = "DEP_AVERDAY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal depAverday;
	
	/** 贷款日均 **/
	@Column(name = "LOAN_AVERDAY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAverday;
	
	/** 本行年度贴现总额 **/
	@Column(name = "YEAR_DISC_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearDiscAmt;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;

	/** 是否上调权限 **/
	@Column(name = "IS_UPPER_APPR_AUTH", unique = false, nullable = false, length = 5)
	private String isUpperApprAuth;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;
	
	/** 优惠内容 **/
	@Column(name = "FAVOUR_CONTENT", unique = false, nullable = true, length = 2000)
	private String favourContent;

	/** 业务类型（特殊需要） **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param bankDepBal
	 */
	public void setBankDepBal(java.math.BigDecimal bankDepBal) {
		this.bankDepBal = bankDepBal;
	}
	
    /**
     * @return bankDepBal
     */
	public java.math.BigDecimal getBankDepBal() {
		return this.bankDepBal;
	}
	
	/**
	 * @param bankLoanBal
	 */
	public void setBankLoanBal(java.math.BigDecimal bankLoanBal) {
		this.bankLoanBal = bankLoanBal;
	}
	
    /**
     * @return bankLoanBal
     */
	public java.math.BigDecimal getBankLoanBal() {
		return this.bankLoanBal;
	}
	
	/**
	 * @param depAverday
	 */
	public void setDepAverday(java.math.BigDecimal depAverday) {
		this.depAverday = depAverday;
	}
	
    /**
     * @return depAverday
     */
	public java.math.BigDecimal getDepAverday() {
		return this.depAverday;
	}
	
	/**
	 * @param loanAverday
	 */
	public void setLoanAverday(java.math.BigDecimal loanAverday) {
		this.loanAverday = loanAverday;
	}
	
    /**
     * @return loanAverday
     */
	public java.math.BigDecimal getLoanAverday() {
		return this.loanAverday;
	}
	
	/**
	 * @param yearDiscAmt
	 */
	public void setYearDiscAmt(java.math.BigDecimal yearDiscAmt) {
		this.yearDiscAmt = yearDiscAmt;
	}
	
    /**
     * @return yearDiscAmt
     */
	public java.math.BigDecimal getYearDiscAmt() {
		return this.yearDiscAmt;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	/**
	 * @param isUpperApprAuth
	 */
	public void setIsUpperApprAuth(String isUpperApprAuth) { this.isUpperApprAuth = isUpperApprAuth; }

    /**
     * @return isUpperApprAuth
     */
	public String getIsUpperApprAuth() { return this.isUpperApprAuth; }

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param favourContent
	 */
	public void setFavourContent(String favourContent) {
		this.favourContent = favourContent;
	}
	
    /**
     * @return favourContent
     */
	public String getFavourContent() {
		return this.favourContent;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	/**
	 * @return bizType
	 */
	public String getBizType() {
		return this.bizType;
	}
}