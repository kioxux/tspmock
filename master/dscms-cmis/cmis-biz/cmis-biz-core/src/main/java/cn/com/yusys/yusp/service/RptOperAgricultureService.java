/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.domain.RptOperProductionOper;
import cn.com.yusys.yusp.dto.RptOperAgricultureDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptOperAgriculture;
import cn.com.yusys.yusp.repository.mapper.RptOperAgricultureMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperAgricultureService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 14:15:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptOperAgricultureService {

    @Autowired
    private RptOperAgricultureMapper rptOperAgricultureMapper;

    @Autowired
    private RptOperProductionOperService rptOperProductionOperService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptOperAgriculture selectByPrimaryKey(String serno) {
        return rptOperAgricultureMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptOperAgriculture> selectAll(QueryModel model) {
        List<RptOperAgriculture> records = (List<RptOperAgriculture>) rptOperAgricultureMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptOperAgriculture> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptOperAgriculture> list = rptOperAgricultureMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptOperAgriculture record) {
        return rptOperAgricultureMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptOperAgriculture record) {
        return rptOperAgricultureMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptOperAgriculture record) {
        return rptOperAgricultureMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptOperAgriculture record) {
        return rptOperAgricultureMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptOperAgricultureMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptOperAgricultureMapper.deleteByIds(ids);
    }

    public int updateOper(RptOperAgriculture rptOperAgriculture){
        RptOperAgriculture temp = selectByPrimaryKey(rptOperAgriculture.getSerno());
        if(Objects.nonNull(temp)){
            return update(rptOperAgriculture);
        }else{
            return insert(rptOperAgriculture);
        }
    }
}
