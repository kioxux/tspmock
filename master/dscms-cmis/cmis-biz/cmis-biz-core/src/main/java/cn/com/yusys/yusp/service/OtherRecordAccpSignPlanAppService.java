/*
 * Automatically generated by code generator
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.OtherAppDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.OtherRecordAccpSignPlanApp;
import cn.com.yusys.yusp.repository.mapper.OtherRecordAccpSignPlanAppMapper;

/**
 * @Project Name: cmis-biz-coreModule
 * @Class Name: OtherRecordAccpSignPlanAppService
 * @Class Description: #Service Class
 * @Function Description: 
 * @Creator: hhj123456
 * @Create Time: 2021-06-09 20:24:59
 * @Modification Note: 
 * @Modification Records: Modified Time    Modified By    Modified Reason
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) Yusys Technologies Co., Ltd. All Rights Reserved.
 */
@Service
@Transactional
public class OtherRecordAccpSignPlanAppService {

    @Autowired
    private OtherRecordAccpSignPlanAppMapper otherRecordAccpSignPlanAppMapper;
	
    /**
     * @Method Name: selectByPrimaryKey
     * @Method Description: Query by primary key
     * @Parameter and Return Description: 
     * @Algorithm Description: 
     */
    public OtherRecordAccpSignPlanApp selectByPrimaryKey(String serno) {
        return otherRecordAccpSignPlanAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @Method Name: selectAll
     * @Method Description: Query all data
     * @Parameter and Return Description: 
     * @Algorithm Description: 
     */
    @Transactional(readOnly=true)
    public List<OtherRecordAccpSignPlanApp> selectAll(QueryModel model) {
        List<OtherRecordAccpSignPlanApp> records = (List<OtherRecordAccpSignPlanApp>) otherRecordAccpSignPlanAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @Method Name: selectByModel
     * @Method Description: Query by condition - query paging
     * @Parameter and Return Description: 
     * @Algorithm Description: 
     */
    
    public List<OtherRecordAccpSignPlanApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherRecordAccpSignPlanApp> list = otherRecordAccpSignPlanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @Method Name: insert
     * @Method Description: Insert
     * @Parameter and Return Description: 
     * @Algorithm Description: 
     */
    public int insert(OtherRecordAccpSignPlanApp record) {
        return otherRecordAccpSignPlanAppMapper.insert(record);
    }

    /**
     * @Method Name: insertSelective
     * @Method Description: Insert - Insert only non-empty fields
     * @Parameter and Return Description: 
     * @Algorithm Description: 
     */
    public int insertSelective(OtherRecordAccpSignPlanApp record) {
        return otherRecordAccpSignPlanAppMapper.insertSelective(record);
    }

    /**
     * @Method Name: update
     * @Method Description: Update by primary key 
     * @Parameter and Return Description: 
     * @Algorithm Description: 
     */
    public int update(OtherRecordAccpSignPlanApp record) {
        return otherRecordAccpSignPlanAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @Method Name: updateSelective
     * @Method Description: Update by primary key - update only non-empty fields
     * @Parameter and Return Description: 
     * @Algorithm Description: 
     */
    public int updateSelective(OtherRecordAccpSignPlanApp record) {
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return otherRecordAccpSignPlanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @Method Name: deleteByPrimaryKey
     * @Method Description: Delete by primary key
     * @Parameter and Return Description: 
     * @Algorithm Description: 
     */
    public int deleteByPrimaryKey(String serno) {
        return otherRecordAccpSignPlanAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @Method Name: deleteByIds
     * @Method Description: Delete by primary keys
     * @Parameter and Return Description: 
     * @Algorithm Description: 
     */
    public int deleteByIds(String ids) {
        return otherRecordAccpSignPlanAppMapper.deleteByIds(ids);
    }

    /**
     * 更新审批状态
     *
     * @author lyh
     */
    public int updateApproveStatus(String serno, String approveStatus) {
        OtherRecordAccpSignPlanApp otherRecordAccpSignPlanApp = new OtherRecordAccpSignPlanApp();
        otherRecordAccpSignPlanApp.setSerno(serno);
        otherRecordAccpSignPlanApp.setApproveStatus(approveStatus);
        return updateSelective(otherRecordAccpSignPlanApp);
    }

    /**
     * 根据流水号查询其他事项申报数据
     *
     * @author macm
     */

    public OtherAppDto selectOtherAppDtoDataByParam(Map map) {
        return otherRecordAccpSignPlanAppMapper.selectOtherAppDtoDataByParam(map);
    }

}
