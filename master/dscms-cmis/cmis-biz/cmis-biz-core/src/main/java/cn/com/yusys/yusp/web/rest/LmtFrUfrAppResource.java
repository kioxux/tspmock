/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.LmtFrUfrAccRelDto;
import cn.com.yusys.yusp.dto.LmtFrUfrAppDto;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtFrUfrApp;
import cn.com.yusys.yusp.service.LmtFrUfrAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFrUfrAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-30 16:38:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtfrufrapp")
public class LmtFrUfrAppResource {
    private static final Logger log = LoggerFactory.getLogger(LmtFrUfrAppResource.class);
    @Autowired
    private LmtFrUfrAppService lmtFrUfrAppService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtFrUfrApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtFrUfrApp> list = lmtFrUfrAppService.selectAll(queryModel);
        return new ResultDto<List<LmtFrUfrApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtFrUfrApp>> index(QueryModel queryModel) {
        List<LmtFrUfrApp> list = lmtFrUfrAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtFrUfrApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtFrUfrApp> show(@PathVariable("serno") String serno) {
        LmtFrUfrApp lmtFrUfrApp = lmtFrUfrAppService.selectByPrimaryKey(serno);
        return new ResultDto<LmtFrUfrApp>(lmtFrUfrApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtFrUfrApp> create(@RequestBody LmtFrUfrApp lmtFrUfrApp) throws URISyntaxException {
        lmtFrUfrAppService.insert(lmtFrUfrApp);
        return new ResultDto<LmtFrUfrApp>(lmtFrUfrApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtFrUfrApp lmtFrUfrApp) throws URISyntaxException {
        int result = lmtFrUfrAppService.update(lmtFrUfrApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:saveLmtFrUrfApp
     * @函数描述:保存额度冻结信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveLmtFrUrfApp")
    protected ResultDto<Integer> saveLmtFrUrfApp(@RequestBody Map params)throws Exception {
        int rtData = lmtFrUfrAppService.saveLmtFrUrfApp(params);
        return new ResultDto<>(rtData);
    }
    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtFrUfrAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtFrUfrAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    
    /**
     * 额度初始化本次冻结解冻金额信息
     * @param
     * @return
     */
    @GetMapping ("/getLmtFrozeAmt/{frUfrSerno}")
    public ResultDto<LmtFrUfrAppDto> getLmtFrozeAmt(@PathVariable("frUfrSerno") String frUfrSerno){
        LmtFrUfrAppDto lmtFrUfrAppDto = lmtFrUfrAppService.getLmtFrozeAmt(frUfrSerno);
        return new ResultDto<>(lmtFrUfrAppDto);
    }
}
