/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.cmis.commons.enums.SystemCacheEnum;
import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.SpringContextUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import com.jcraft.jsch.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtApprBackItem;
import cn.com.yusys.yusp.repository.mapper.LmtApprBackItemMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprBackItemService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-24 11:03:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtApprBackItemService {

    private static StringRedisTemplate instance;
    /**
     * 处理缓存默认使用的RedisTemplate
     */
    private static final String REDIS_TEMPLATE_NAME = "stringRedisTemplate";

    @Resource
    private LmtApprBackItemMapper lmtApprBackItemMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtApprBackItem selectByPrimaryKey(String pkId, String serno) {
        return lmtApprBackItemMapper.selectByPrimaryKey(pkId, serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtApprBackItem> selectAll(QueryModel model) {
        List<LmtApprBackItem> records = (List<LmtApprBackItem>) lmtApprBackItemMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtApprBackItem> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtApprBackItem> list = lmtApprBackItemMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtApprBackItem record) {
        return lmtApprBackItemMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtApprBackItem record) {
        return lmtApprBackItemMapper.insertSelective(record);
    }



    /**
     * @方法名称: insertSelfOutInputData
     * @方法描述: 插入 - 不处理登记人相关字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelfOutInputData(LmtApprBackItem record) {
        return lmtApprBackItemMapper.insertSelfOutInputData(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtApprBackItem record) {
        return lmtApprBackItemMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtApprBackItem record) {
        return lmtApprBackItemMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String serno) {
        return lmtApprBackItemMapper.deleteByPrimaryKey(pkId, serno);
    }

    /**
     * @方法名称: selectListByModel
     * @方法描述: 查询流水号项下数据,有则返回,无则新增
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtApprBackItem> selectListByModel(QueryModel queryModel) {
        queryModel.setPage(1);
        queryModel.setSize(100);
        List<LmtApprBackItem> list = new ArrayList<>();
        queryModel.setSort("backItemOrder asc");
        if(StringUtils.isBlank((String) queryModel.getCondition().get("serno"))){
            // 传入参数为空时  不做处理
            // System.out.println("111111111111111111111");
        }else {
            List<LmtApprBackItem> lmtApprBackItems = new ArrayList<>();
            User userInfo = SessionUtils.getUserInformation();
            // 先更新传入参数查询数据
            lmtApprBackItems = selectByModel(queryModel);
            // 判断数据是否为空 有则返回,无则新增
            if (lmtApprBackItems.isEmpty() || lmtApprBackItems.size() == 0) {
                // 根据 字典项 插入数据
                BoundHashOperations<String, Object, Object> dataDict = instance().opsForHash().getOperations().boundHashOps(SystemCacheEnum.CACHE_KEY_DATADICT.key);
                Object object = dataDict.get("STD_ZB_BACK_ITEM");
                if (object instanceof String) {
                    List<Map<String, String>> listMap = (List<Map<String, String>>) ObjectMapperUtils.toObject(StringUtils.replaceObjNull(object), List.class);
                    for(Map<String, String> map : listMap){
                        if(!StringUtils.isBlank(map.get("key"))){
                            LmtApprBackItem lmtApprBackItem = new LmtApprBackItem();
                            lmtApprBackItem.setPkId(UUID.randomUUID().toString());
                            lmtApprBackItem.setSerno((String) queryModel.getCondition().get("serno"));
                            lmtApprBackItem.setBackItemId(map.get("key"));
                            lmtApprBackItem.setBackItemName(map.get("value"));
                            lmtApprBackItem.setBackItemOrder(Integer.valueOf(map.get("key")));
                            lmtApprBackItem.setBackItemPoint(0);
                            insertSelfOutInputData(lmtApprBackItem);
                            lmtApprBackItems.add(lmtApprBackItem);
                        }
                    }
                }
            }
            // 返回查询数据
            for(LmtApprBackItem lmtApprBackItem : lmtApprBackItems){
                if(StringUtils.isBlank(lmtApprBackItem.getUpdId())){
                    lmtApprBackItem.setUpdId("");// 用于去除展示的null
                }
                list.add(lmtApprBackItem);
            }
        }
        return list;
    }

    private static synchronized void init() {
        if (instance == null) {
            instance = SpringContextUtils.getBean(REDIS_TEMPLATE_NAME);
        }
    }

    private static StringRedisTemplate instance() {
        if (instance == null) {
            init();
        }
        return instance;
    }

    /**
     * @函数名称:savebackitempointdata
     * @函数描述:保存当前列表数据
     * @参数与返回说明:
     * @算法描述:
     */

    public boolean saveBackItemPointData(List<LmtApprBackItem> list) {
        boolean result = false;
        if(!list.isEmpty() && list.size()>0){
            User userInfo = SessionUtils.getUserInformation();
            int updateNum = 0;
            for(LmtApprBackItem lmtApprBackItem : list){
                LmtApprBackItem oldLmtApprBackItem = this.selectByPrimaryKey(lmtApprBackItem.getPkId(),lmtApprBackItem.getSerno());
                // 校验是否分值有变化
                if(oldLmtApprBackItem.getBackItemPoint() != lmtApprBackItem.getBackItemPoint()){
                    lmtApprBackItem.setInputId(userInfo.getLoginCode());
                    lmtApprBackItem.setInputBrId(userInfo.getOrg().getCode());
                    lmtApprBackItem.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtApprBackItem.setUpdId(userInfo.getLoginCode());
                    lmtApprBackItem.setUpdBrId(userInfo.getOrg().getCode());
                    lmtApprBackItem.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    updateSelective(lmtApprBackItem);
                    updateNum++;
                }
            }
            if(list.size() >= updateNum){
                result = true;
            }
        }
        return result;
    }
}
