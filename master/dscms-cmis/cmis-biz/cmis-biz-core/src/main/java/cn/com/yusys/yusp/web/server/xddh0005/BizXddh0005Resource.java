package cn.com.yusys.yusp.web.server.xddh0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0005.req.Xddh0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0005.resp.Xddh0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:提前还款
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0005:提前还款")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0005Resource.class);
    @Autowired
    private cn.com.yusys.yusp.service.server.xddh0005.Xddh0005Service xddh0005Service;

    /**
     * 交易码：xddh0005
     * 交易描述：提前还款
     *
     * @param xddh0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提前还款")
    @PostMapping("/xddh0005")
    protected @ResponseBody
    ResultDto<Xddh0005DataRespDto> xddh0005(@Validated @RequestBody Xddh0005DataReqDto xddh0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0005.key, DscmsEnum.TRADE_CODE_XDDH0005.value, JSON.toJSONString(xddh0005DataReqDto));
        Xddh0005DataRespDto xddh0005DataRespDto = new Xddh0005DataRespDto();// 响应Dto:提前还款
        ResultDto<Xddh0005DataRespDto> xddh0005DataResultDto = new ResultDto<>();
        try {
            // 从xddh0005DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddh0005DataReqDto));
            xddh0005DataRespDto = xddh0005Service.xddh0005(xddh0005DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddh0005DataRespDto));
            // 封装xddh0005DataResultDto中正确的返回码和返回信息
            xddh0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0005.key, DscmsEnum.TRADE_CODE_XDDH0005.value, e.getMessage());
            // 封装xddh0005DataResultDto中异常返回码和返回信息
            xddh0005DataResultDto.setCode(e.getErrorCode());
            xddh0005DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0005.key, DscmsEnum.TRADE_CODE_XDDH0005.value, e.getMessage());
            // 封装xddh0005DataResultDto中异常返回码和返回信息
            xddh0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0005DataRespDto到xddh0005DataResultDto中
        xddh0005DataResultDto.setData(xddh0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0005.key, DscmsEnum.TRADE_CODE_XDDH0005.value, JSON.toJSONString(xddh0005DataResultDto));
        return xddh0005DataResultDto;
    }
}
