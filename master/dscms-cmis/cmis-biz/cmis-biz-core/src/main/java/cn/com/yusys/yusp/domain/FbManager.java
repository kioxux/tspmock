/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FbManager
 * @类描述: fb_manager数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-08 23:05:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "fb_manager")
public class FbManager extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 分部编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "FB_CODE")
	private String fbCode;
	
	/** 分部名称 **/
	@Column(name = "FB_NAME", unique = false, nullable = true, length = 80)
	private String fbName;
	
	/** 部长 **/
	@Column(name = "MINISTER_ID", unique = false, nullable = true, length = 20)
	private String ministerId;
	
	/** 分管中层 **/
	@Column(name = "ZC_MANAGER_ID", unique = false, nullable = true, length = 20)
	private String zcManagerId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param fbCode
	 */
	public void setFbCode(String fbCode) {
		this.fbCode = fbCode;
	}
	
    /**
     * @return fbCode
     */
	public String getFbCode() {
		return this.fbCode;
	}
	
	/**
	 * @param fbName
	 */
	public void setFbName(String fbName) {
		this.fbName = fbName;
	}
	
    /**
     * @return fbName
     */
	public String getFbName() {
		return this.fbName;
	}
	
	/**
	 * @param ministerId
	 */
	public void setMinisterId(String ministerId) {
		this.ministerId = ministerId;
	}
	
    /**
     * @return ministerId
     */
	public String getMinisterId() {
		return this.ministerId;
	}
	
	/**
	 * @param zcManagerId
	 */
	public void setZcManagerId(String zcManagerId) {
		this.zcManagerId = zcManagerId;
	}
	
    /**
     * @return zcManagerId
     */
	public String getZcManagerId() {
		return this.zcManagerId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}