package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.IqpLoanAppPrtcptBankSub;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.RepayCapPlan;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @author quwen
 * @version 1.0.0
 * @date 2021年10月22日16:04:21
 * @desc 银团贷款参与行信息校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0133Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0133Service.class);
    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpLoanAppPrtcptBankSubService iqpLoanAppPrtcptBankSubService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author quwen
     * @date 2021年10月22日16:06:37
     * @version 1.0.0
     * @desc   银团贷款参与行信息校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0133(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String iqpSerno = (String)queryModel.getCondition().get("bizId");
        if (StringUtils.isBlank(iqpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 通过申请流水号获取申请信息
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
        if(Objects.isNull(iqpLoanApp)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007); //通过合同申请流水号未获取到对应的合同申请信息
            return riskResultDto;
        }
        //判断产品是否为银团贷款
        String prdId = iqpLoanApp.getPrdId();
        if("012020".equals(prdId)){
            QueryModel queryModelData = new QueryModel();
            queryModelData.addCondition("serno",iqpSerno);
            queryModelData.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<IqpLoanAppPrtcptBankSub> iqpLoanAppPrtcptBankSubList = iqpLoanAppPrtcptBankSubService.selectByModel(queryModelData);
            if(CollectionUtils.isEmpty(iqpLoanAppPrtcptBankSubList)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0013301); //未填写参与行信息
                return riskResultDto;
            }else{
                // 银团总金额
                BigDecimal bksyndicTotlAmt = iqpLoanApp.getBksyndicTotlAmt();
                // 参与行合同总金额
                BigDecimal joinBankTotalAmt = BigDecimal.ZERO;
                // 参与比例
                BigDecimal joinBankRate = BigDecimal.ZERO;
                for (IqpLoanAppPrtcptBankSub iqpLoanAppPrtcptBankSub : iqpLoanAppPrtcptBankSubList) {
                    log.info("参与比例为【{}】",iqpLoanAppPrtcptBankSub.getPrtcptRate());
                    if(BigDecimal.ONE.compareTo(iqpLoanAppPrtcptBankSub.getPrtcptRate())<0){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0013302); //参与比例必须小于等于100%
                        return riskResultDto;
                    }
                    joinBankTotalAmt = joinBankTotalAmt.add(iqpLoanAppPrtcptBankSub.getContAmt());
                    joinBankRate = joinBankRate.add(iqpLoanAppPrtcptBankSub.getPrtcptRate());
                }
                log.info("银团总金额为【{}】",bksyndicTotlAmt);
                log.info("所有参与行合同总金额为【{}】",joinBankTotalAmt);
                if(bksyndicTotlAmt.compareTo(joinBankTotalAmt)!=0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0013303); //所有参与行合同总金额必须等于银团总金额
                    return riskResultDto;
                }
                log.info("所有参与行总参与比例为【{}】",joinBankRate);
                if(joinBankRate.compareTo(BigDecimal.ONE)!=0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0013304); //所有参与行总参与比例必须等于100%
                    return riskResultDto;
                }
            }
        }else{
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); //校验通过
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
