/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpEntrustLoanApp
 * @类描述: iqp_entrust_loan_app数据实体类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-05-11 18:58:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_entrust_loan_app")
public class IqpEntrustLoanApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 10)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;
	
	/** 申请业务类型 **/
	@Column(name = "BUSI_TYPE", unique = false, nullable = true, length = 5)
	private String busiType;
	
	/** 合同类型 **/
	@Column(name = "CONT_TYPE", unique = false, nullable = true, length = 5)
	private String contType;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;
	
	/** 本合同项下最高可用信金额 **/
	@Column(name = "CONT_HIGH_AVL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contHighAvlAmt;
	
	/** 合同期限 **/
	@Column(name = "CONT_TERM", unique = false, nullable = true, length = 10)
	private Integer contTerm;
	
	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 纸质合同签订日期 **/
	@Column(name = "PAPER_CONT_SIGN_DATE", unique = false, nullable = true, length = 10)
	private String paperContSignDate;
	
	/** 是否续签 **/
	@Column(name = "IS_RENEW", unique = false, nullable = true, length = 5)
	private String isRenew;
	
	/** 原合同编号 **/
	@Column(name = "ORIGI_CONT_NO", unique = false, nullable = true, length = 40)
	private String origiContNo;
	
	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;
	
	/** 授信台账编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;
	
	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;
	
	/** 是否电子用印 **/
	@Column(name = "IS_E_SEAL", unique = false, nullable = true, length = 5)
	private String isESeal;
	
	/** 是否在线抵押 **/
	@Column(name = "IS_OL_PLD", unique = false, nullable = true, length = 5)
	private String isOlPld;
	
	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 4)
	private String belgLine;
	
	/** 债项等级 **/
	@Column(name = "DEBT_LEVEL", unique = false, nullable = true, length = 5)
	private String debtLevel;
	
	/** 违约风险暴露EAD **/
	@Column(name = "EAD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ead;
	
	/** 违约损失率LGD **/
	@Column(name = "LGD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lgd;
	
	/** 联系人 **/
	@Column(name = "LINKMAN", unique = false, nullable = true, length = 80)
	private String linkman;
	
	/** 电话 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;
	
	/** 传真 **/
	@Column(name = "FAX", unique = false, nullable = true, length = 20)
	private String fax;
	
	/** 邮箱 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 80)
	private String email;
	
	/** QQ **/
	@Column(name = "QQ", unique = false, nullable = true, length = 20)
	private String qq;
	
	/** 微信 **/
	@Column(name = "WECHAT", unique = false, nullable = true, length = 40)
	private String wechat;
	
	/** 送达地址 **/
	@Column(name = "DELIVERY_ADDR", unique = false, nullable = true, length = 500)
	private String deliveryAddr;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 贷款形式 **/
	@Column(name = "LOAN_MODAL", unique = false, nullable = true, length = 5)
	private String loanModal;
	
	/** 借款种类 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 5)
	private String loanType;
	
	/** 贷款用途 **/
	@Column(name = "LOAN_PURP", unique = false, nullable = true, length = 200)
	private String loanPurp;
	
	/** 委托人类型 **/
	@Column(name = "CONSIGNOR_TYPE", unique = false, nullable = true, length = 40)
	private String consignorType;
	
	/** 委托贷款类型 **/
	@Column(name = "CONSIGNOR_LOAN_TYPE", unique = false, nullable = true, length = 40)
	private String consignorLoanType;
	
	/** 委托人客户编号 **/
	@Column(name = "CONSIGNOR_CUS_ID", unique = false, nullable = true, length = 40)
	private String consignorCusId;
	
	/** 委托人客户名称 **/
	@Column(name = "CONSIGNOR_CUS_NAME", unique = false, nullable = true, length = 40)
	private String consignorCusName;
	
	/** 委托人证件类型 **/
	@Column(name = "CONSIGNOR_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String consignorCertType;
	
	/** 委托人证件号码 **/
	@Column(name = "CONSIGNOR_CERT_CODE", unique = false, nullable = true, length = 40)
	private String consignorCertCode;
	
	/** 委托贷款手续费收取方式 **/
	@Column(name = "CSGN_LOAN_CHRG_COLLECT_TYPE", unique = false, nullable = true, length = 40)
	private String csgnLoanChrgCollectType;
	
	/** 委托贷款手续费率 **/
	@Column(name = "CSGN_LOAN_CHRG_COLLECT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal csgnLoanChrgCollectRate;
	
	/** 委托条件 **/
	@Column(name = "CSGN_COND", unique = false, nullable = true, length = 200)
	private String csgnCond;
	
	/** 利率调整方式 **/
	@Column(name = "RATE_ADJ_MODE", unique = false, nullable = true, length = 5)
	private String rateAdjMode;
	
	/** 借款利率调整日 **/
	@Column(name = "LOAN_RATE_ADJ_DAY", unique = false, nullable = true, length = 40)
	private String loanRateAdjDay;
	
	/** LPR利率区间 **/
	@Column(name = "LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
	private String lprRateIntval;
	
	/** 当前LPR利率 **/
	@Column(name = "CURT_LPR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLprRate;
	
	/** 执行年利率 **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;
	
	/** 浮动点数 **/
	@Column(name = "RATE_FLOAT_POINT", unique = false, nullable = true, length = 40)
	private String rateFloatPoint;
	
	/** 结息方式 **/
	@Column(name = "EI_MODE", unique = false, nullable = true, length = 5)
	private String eiMode;
	
	/** 提款方式 **/
	@Column(name = "DRAW_MODE", unique = false, nullable = true, length = 5)
	private String drawMode;
	
	/** 提款期限 **/
	@Column(name = "DRAW_TERM", unique = false, nullable = true, length = 40)
	private String drawTerm;
	
	/** 贷款发放账号 **/
	@Column(name = "LOAN_PAYOUT_ACCNO", unique = false, nullable = true, length = 40)
	private String loanPayoutAccno;
	
	/** 贷款发放账户名称 **/
	@Column(name = "LOAN_PAYOUT_ACCT_NAME", unique = false, nullable = true, length = 40)
	private String loanPayoutAcctName;
	
	/** 开户行名称 **/
	@Column(name = "ACCTSVCR_NAME", unique = false, nullable = true, length = 40)
	private String acctsvcrName;
	
	/** 支付方式 **/
	@Column(name = "PAY_MODE", unique = false, nullable = true, length = 5)
	private String payMode;
	
	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;
	
	/** 还款具体说明 **/
	@Column(name = "REPAY_DETAIL", unique = false, nullable = true, length = 400)
	private String repayDetail;
	
	/** 贷款投向 **/
	@Column(name = "LOAN_TER", unique = false, nullable = true, length = 40)
	private String loanTer;
	
	/** 本笔信贷投入的必要性及详细用途分析 **/
	@Column(name = "CRET_USE_ANALY", unique = false, nullable = true, length = 1000)
	private String cretUseAnaly;
	
	/** 还款来源 **/
	@Column(name = "REPAY_SOUR", unique = false, nullable = true, length = 100)
	private String repaySour;
	
	/** 调查人结论 **/
	@Column(name = "INVE_CONCLU", unique = false, nullable = true, length = 1000)
	private String inveConclu;
	
	/** 其他约定 **/
	@Column(name = "OTHER_AGREED", unique = false, nullable = true, length = 1000)
	private String otherAgreed;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param busiType
	 */
	public void setBusiType(String busiType) {
		this.busiType = busiType;
	}
	
    /**
     * @return busiType
     */
	public String getBusiType() {
		return this.busiType;
	}
	
	/**
	 * @param contType
	 */
	public void setContType(String contType) {
		this.contType = contType;
	}
	
    /**
     * @return contType
     */
	public String getContType() {
		return this.contType;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param contHighAvlAmt
	 */
	public void setContHighAvlAmt(java.math.BigDecimal contHighAvlAmt) {
		this.contHighAvlAmt = contHighAvlAmt;
	}
	
    /**
     * @return contHighAvlAmt
     */
	public java.math.BigDecimal getContHighAvlAmt() {
		return this.contHighAvlAmt;
	}
	
	/**
	 * @param contTerm
	 */
	public void setContTerm(Integer contTerm) {
		this.contTerm = contTerm;
	}
	
    /**
     * @return contTerm
     */
	public Integer getContTerm() {
		return this.contTerm;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param paperContSignDate
	 */
	public void setPaperContSignDate(String paperContSignDate) {
		this.paperContSignDate = paperContSignDate;
	}
	
    /**
     * @return paperContSignDate
     */
	public String getPaperContSignDate() {
		return this.paperContSignDate;
	}
	
	/**
	 * @param isRenew
	 */
	public void setIsRenew(String isRenew) {
		this.isRenew = isRenew;
	}
	
    /**
     * @return isRenew
     */
	public String getIsRenew() {
		return this.isRenew;
	}
	
	/**
	 * @param origiContNo
	 */
	public void setOrigiContNo(String origiContNo) {
		this.origiContNo = origiContNo;
	}
	
    /**
     * @return origiContNo
     */
	public String getOrigiContNo() {
		return this.origiContNo;
	}
	
	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}
	
    /**
     * @return isUtilLmt
     */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param isESeal
	 */
	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal;
	}
	
    /**
     * @return isESeal
     */
	public String getIsESeal() {
		return this.isESeal;
	}
	
	/**
	 * @param isOlPld
	 */
	public void setIsOlPld(String isOlPld) {
		this.isOlPld = isOlPld;
	}
	
    /**
     * @return isOlPld
     */
	public String getIsOlPld() {
		return this.isOlPld;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}
	
    /**
     * @return belgLine
     */
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param debtLevel
	 */
	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel;
	}
	
    /**
     * @return debtLevel
     */
	public String getDebtLevel() {
		return this.debtLevel;
	}
	
	/**
	 * @param ead
	 */
	public void setEad(java.math.BigDecimal ead) {
		this.ead = ead;
	}
	
    /**
     * @return ead
     */
	public java.math.BigDecimal getEad() {
		return this.ead;
	}
	
	/**
	 * @param lgd
	 */
	public void setLgd(java.math.BigDecimal lgd) {
		this.lgd = lgd;
	}
	
    /**
     * @return lgd
     */
	public java.math.BigDecimal getLgd() {
		return this.lgd;
	}
	
	/**
	 * @param linkman
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}
	
    /**
     * @return linkman
     */
	public String getLinkman() {
		return this.linkman;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	
    /**
     * @return fax
     */
	public String getFax() {
		return this.fax;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
    /**
     * @return email
     */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}
	
    /**
     * @return qq
     */
	public String getQq() {
		return this.qq;
	}
	
	/**
	 * @param wechat
	 */
	public void setWechat(String wechat) {
		this.wechat = wechat;
	}
	
    /**
     * @return wechat
     */
	public String getWechat() {
		return this.wechat;
	}
	
	/**
	 * @param deliveryAddr
	 */
	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr;
	}
	
    /**
     * @return deliveryAddr
     */
	public String getDeliveryAddr() {
		return this.deliveryAddr;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param loanModal
	 */
	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal;
	}
	
    /**
     * @return loanModal
     */
	public String getLoanModal() {
		return this.loanModal;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
    /**
     * @return loanType
     */
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp;
	}
	
    /**
     * @return loanPurp
     */
	public String getLoanPurp() {
		return this.loanPurp;
	}
	
	/**
	 * @param consignorType
	 */
	public void setConsignorType(String consignorType) {
		this.consignorType = consignorType;
	}
	
    /**
     * @return consignorType
     */
	public String getConsignorType() {
		return this.consignorType;
	}
	
	/**
	 * @param consignorLoanType
	 */
	public void setConsignorLoanType(String consignorLoanType) {
		this.consignorLoanType = consignorLoanType;
	}
	
    /**
     * @return consignorLoanType
     */
	public String getConsignorLoanType() {
		return this.consignorLoanType;
	}
	
	/**
	 * @param consignorCusId
	 */
	public void setConsignorCusId(String consignorCusId) {
		this.consignorCusId = consignorCusId;
	}
	
    /**
     * @return consignorCusId
     */
	public String getConsignorCusId() {
		return this.consignorCusId;
	}
	
	/**
	 * @param consignorCusName
	 */
	public void setConsignorCusName(String consignorCusName) {
		this.consignorCusName = consignorCusName;
	}
	
    /**
     * @return consignorCusName
     */
	public String getConsignorCusName() {
		return this.consignorCusName;
	}
	
	/**
	 * @param consignorCertType
	 */
	public void setConsignorCertType(String consignorCertType) {
		this.consignorCertType = consignorCertType;
	}
	
    /**
     * @return consignorCertType
     */
	public String getConsignorCertType() {
		return this.consignorCertType;
	}
	
	/**
	 * @param consignorCertCode
	 */
	public void setConsignorCertCode(String consignorCertCode) {
		this.consignorCertCode = consignorCertCode;
	}
	
    /**
     * @return consignorCertCode
     */
	public String getConsignorCertCode() {
		return this.consignorCertCode;
	}
	
	/**
	 * @param csgnLoanChrgCollectType
	 */
	public void setCsgnLoanChrgCollectType(String csgnLoanChrgCollectType) {
		this.csgnLoanChrgCollectType = csgnLoanChrgCollectType;
	}
	
    /**
     * @return csgnLoanChrgCollectType
     */
	public String getCsgnLoanChrgCollectType() {
		return this.csgnLoanChrgCollectType;
	}
	
	/**
	 * @param csgnLoanChrgCollectRate
	 */
	public void setCsgnLoanChrgCollectRate(java.math.BigDecimal csgnLoanChrgCollectRate) {
		this.csgnLoanChrgCollectRate = csgnLoanChrgCollectRate;
	}
	
    /**
     * @return csgnLoanChrgCollectRate
     */
	public java.math.BigDecimal getCsgnLoanChrgCollectRate() {
		return this.csgnLoanChrgCollectRate;
	}
	
	/**
	 * @param csgnCond
	 */
	public void setCsgnCond(String csgnCond) {
		this.csgnCond = csgnCond;
	}
	
    /**
     * @return csgnCond
     */
	public String getCsgnCond() {
		return this.csgnCond;
	}
	
	/**
	 * @param rateAdjMode
	 */
	public void setRateAdjMode(String rateAdjMode) {
		this.rateAdjMode = rateAdjMode;
	}
	
    /**
     * @return rateAdjMode
     */
	public String getRateAdjMode() {
		return this.rateAdjMode;
	}
	
	/**
	 * @param loanRateAdjDay
	 */
	public void setLoanRateAdjDay(String loanRateAdjDay) {
		this.loanRateAdjDay = loanRateAdjDay;
	}
	
    /**
     * @return loanRateAdjDay
     */
	public String getLoanRateAdjDay() {
		return this.loanRateAdjDay;
	}
	
	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval;
	}
	
    /**
     * @return lprRateIntval
     */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}
	
	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}
	
    /**
     * @return curtLprRate
     */
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return execRateYear
     */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(String rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}
	
    /**
     * @return rateFloatPoint
     */
	public String getRateFloatPoint() {
		return this.rateFloatPoint;
	}
	
	/**
	 * @param eiMode
	 */
	public void setEiMode(String eiMode) {
		this.eiMode = eiMode;
	}
	
    /**
     * @return eiMode
     */
	public String getEiMode() {
		return this.eiMode;
	}
	
	/**
	 * @param drawMode
	 */
	public void setDrawMode(String drawMode) {
		this.drawMode = drawMode;
	}
	
    /**
     * @return drawMode
     */
	public String getDrawMode() {
		return this.drawMode;
	}
	
	/**
	 * @param drawTerm
	 */
	public void setDrawTerm(String drawTerm) {
		this.drawTerm = drawTerm;
	}
	
    /**
     * @return drawTerm
     */
	public String getDrawTerm() {
		return this.drawTerm;
	}
	
	/**
	 * @param loanPayoutAccno
	 */
	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno;
	}
	
    /**
     * @return loanPayoutAccno
     */
	public String getLoanPayoutAccno() {
		return this.loanPayoutAccno;
	}
	
	/**
	 * @param loanPayoutAcctName
	 */
	public void setLoanPayoutAcctName(String loanPayoutAcctName) {
		this.loanPayoutAcctName = loanPayoutAcctName;
	}
	
    /**
     * @return loanPayoutAcctName
     */
	public String getLoanPayoutAcctName() {
		return this.loanPayoutAcctName;
	}
	
	/**
	 * @param acctsvcrName
	 */
	public void setAcctsvcrName(String acctsvcrName) {
		this.acctsvcrName = acctsvcrName;
	}
	
    /**
     * @return acctsvcrName
     */
	public String getAcctsvcrName() {
		return this.acctsvcrName;
	}
	
	/**
	 * @param payMode
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	
    /**
     * @return payMode
     */
	public String getPayMode() {
		return this.payMode;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param repayDetail
	 */
	public void setRepayDetail(String repayDetail) {
		this.repayDetail = repayDetail;
	}
	
    /**
     * @return repayDetail
     */
	public String getRepayDetail() {
		return this.repayDetail;
	}
	
	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}
	
    /**
     * @return loanTer
     */
	public String getLoanTer() {
		return this.loanTer;
	}
	
	/**
	 * @param cretUseAnaly
	 */
	public void setCretUseAnaly(String cretUseAnaly) {
		this.cretUseAnaly = cretUseAnaly;
	}
	
    /**
     * @return cretUseAnaly
     */
	public String getCretUseAnaly() {
		return this.cretUseAnaly;
	}
	
	/**
	 * @param repaySour
	 */
	public void setRepaySour(String repaySour) {
		this.repaySour = repaySour;
	}
	
    /**
     * @return repaySour
     */
	public String getRepaySour() {
		return this.repaySour;
	}
	
	/**
	 * @param inveConclu
	 */
	public void setInveConclu(String inveConclu) {
		this.inveConclu = inveConclu;
	}
	
    /**
     * @return inveConclu
     */
	public String getInveConclu() {
		return this.inveConclu;
	}
	
	/**
	 * @param otherAgreed
	 */
	public void setOtherAgreed(String otherAgreed) {
		this.otherAgreed = otherAgreed;
	}
	
    /**
     * @return otherAgreed
     */
	public String getOtherAgreed() {
		return this.otherAgreed;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}