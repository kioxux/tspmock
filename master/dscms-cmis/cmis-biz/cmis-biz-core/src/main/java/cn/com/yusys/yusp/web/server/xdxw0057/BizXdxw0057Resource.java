package cn.com.yusys.yusp.web.server.xdxw0057;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0057.req.Xdxw0057DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0057.resp.Xdxw0057DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0057.Xdxw0057Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:根据核心客户号查询经营性贷款批复额度
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0057:根据核心客户号查询经营性贷款批复额度")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0057Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0057Resource.class);

    @Autowired
    private Xdxw0057Service xdxw0057Service;

    /**
     * 交易码：xdxw0057
     * 交易描述：根据核心客户号查询经营性贷款批复额度
     *
     * @param xdxw0057DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据核心客户号查询经营性贷款批复额度")
    @PostMapping("/xdxw0057")
    protected @ResponseBody
    ResultDto<Xdxw0057DataRespDto> xdxw0057(@Validated @RequestBody Xdxw0057DataReqDto xdxw0057DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, JSON.toJSONString(xdxw0057DataReqDto));
        Xdxw0057DataRespDto xdxw0057DataRespDto = new Xdxw0057DataRespDto();// 响应Dto:根据核心客户号查询经营性贷款批复额度
        ResultDto<Xdxw0057DataRespDto> xdxw0057DataResultDto = new ResultDto<>();
        String queryType = xdxw0057DataReqDto.getQueryType();//查询类型
        String cusId = xdxw0057DataReqDto.getCusId();//客户号
        // 从xdxw0057DataReqDto获取业务值进行业务逻辑处理
        try {
            // 调用xdxw0057Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, JSON.toJSONString(xdxw0057DataReqDto));
            xdxw0057DataRespDto = xdxw0057Service.getLmtByQueryType(xdxw0057DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, JSON.toJSONString(xdxw0057DataRespDto));
            // 封装xdxw0057DataResultDto中正确的返回码和返回信息
            xdxw0057DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0057DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, e.getMessage());
            // 封装xdxw0057DataResultDto中异常返回码和返回信息
            xdxw0057DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0057DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0057DataRespDto到xdxw0057DataResultDto中
        xdxw0057DataResultDto.setData(xdxw0057DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, JSON.toJSONString(xdxw0057DataResultDto));
        return xdxw0057DataResultDto;
    }
}
