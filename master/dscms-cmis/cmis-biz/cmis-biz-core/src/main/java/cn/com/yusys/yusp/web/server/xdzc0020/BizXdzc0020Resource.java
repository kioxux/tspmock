package cn.com.yusys.yusp.web.server.xdzc0020;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0020.req.Xdzc0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0020.resp.Xdzc0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0020.Xdzc0020Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:出入池详情查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0020:出入池详情查询")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0020Resource.class);

    @Autowired
    private Xdzc0020Service xdzc0020Service;
    /**
     * 交易码：xdzc0020
     * 交易描述：出入池详情查询
     *
     * @param xdzc0020DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("出入池详情查询")
    @PostMapping("/xdzc0020")
    protected @ResponseBody
    ResultDto<Xdzc0020DataRespDto> xdzc0020(@Validated @RequestBody Xdzc0020DataReqDto xdzc0020DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value, JSON.toJSONString(xdzc0020DataReqDto));
        Xdzc0020DataRespDto xdzc0020DataRespDto = new Xdzc0020DataRespDto();// 响应Dto:出入池详情查询
        ResultDto<Xdzc0020DataRespDto> xdzc0020DataResultDto = new ResultDto<>();
        try {
            xdzc0020DataRespDto = xdzc0020Service.xdzc0020Service(xdzc0020DataReqDto);
            xdzc0020DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0020DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value, e.getMessage());
            // 封装xdzc0020DataResultDto中异常返回码和返回信息
            xdzc0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0020DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0020DataRespDto到xdzc0020DataResultDto中
        xdzc0020DataResultDto.setData(xdzc0020DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value, JSON.toJSONString(xdzc0020DataResultDto));
        return xdzc0020DataResultDto;
    }
}
