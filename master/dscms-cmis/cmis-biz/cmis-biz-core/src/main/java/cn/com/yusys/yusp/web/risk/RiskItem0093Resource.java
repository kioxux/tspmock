package cn.com.yusys.yusp.web.risk;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.OtherRecordAccpSignOrAllPldAppService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "riskItem0093银票签发及全资质押类业务备案申请校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0093")
public class RiskItem0093Resource {

    @Autowired
    OtherRecordAccpSignOrAllPldAppService otherRecordAccpSignOrAllPldAppService;

    @ApiOperation(value = "银票签发及全资质押类业务备案申请校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0093(@RequestBody QueryModel queryModel) {
        if(queryModel.getCondition().size()==0){
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return  ResultDto.success(riskResultDto);
        }
        return ResultDto.success(otherRecordAccpSignOrAllPldAppService.riskItem0093(queryModel));
    }
}
