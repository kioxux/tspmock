package cn.com.yusys.yusp.web.server.xddh0016;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0016.req.Xddh0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0016.resp.Xddh0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;

/**
 * 接口处理类:提前还款申请
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0016:提前还款申请")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXddh0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0016Resource.class);

    /**
     * 交易码：xddh0016
     * 交易描述：提前还款申请
     *
     * @param xddh0016DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提前还款申请")
    @PostMapping("/xddh0016")
    protected @ResponseBody
    ResultDto<Xddh0016DataRespDto> xddh0016(@Validated @RequestBody Xddh0016DataReqDto xddh0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0016.key, DscmsEnum.TRADE_CODE_XDDH0016.value, JSON.toJSONString(xddh0016DataReqDto));
        Xddh0016DataRespDto xddh0016DataRespDto = new Xddh0016DataRespDto();// 响应Dto:提前还款申请
        ResultDto<Xddh0016DataRespDto> xddh0016DataResultDto = new ResultDto<>();
        String billNo = xddh0016DataReqDto.getBillNo();//借据号
        String contNo = xddh0016DataReqDto.getContNo();//合同号
        String cusId = xddh0016DataReqDto.getCusId();//客户编号
        String cusName = xddh0016DataReqDto.getCusName();//客户名称
        String repayMode = xddh0016DataReqDto.getRepayMode();//还款方式
        String repayModeDesc = xddh0016DataReqDto.getRepayModeDesc();//还款模式明细
        BigDecimal repayAmt = xddh0016DataReqDto.getRepayAmt();//还款金额
        String isPeriod = xddh0016DataReqDto.getIsPeriod();//是否缩期
        String periodTimes = xddh0016DataReqDto.getPeriodTimes();//缩期期数
        String appResn = xddh0016DataReqDto.getAppResn();//申请理由
        try {
            // 从xddh0016DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xddh0016DataRespDto对象开始
            xddh0016DataRespDto.setOpFLag(StringUtils.EMPTY);// 登记标志
            xddh0016DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xddh0016DataRespDto对象结束
            // 封装xddh0016DataResultDto中正确的返回码和返回信息
            xddh0016DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0016DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0016.key, DscmsEnum.TRADE_CODE_XDDH0016.value, e.getMessage());
            // 封装xddh0016DataResultDto中异常返回码和返回信息
            xddh0016DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0016DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0016DataRespDto到xddh0016DataResultDto中
        xddh0016DataResultDto.setData(xddh0016DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0016.key, DscmsEnum.TRADE_CODE_XDDH0016.value, JSON.toJSONString(xddh0016DataResultDto));
        return xddh0016DataResultDto;
    }
}
