package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuareCommenOwnerInfo
 * @类描述: guare_commen_owner_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-17 10:32:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuareCommenOwnerInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 调查编号 **/
	private String surveyNo;
	
	/** 共有人客户编号 **/
	private String commenOwnerCusId;
	
	/** 共有人姓名 **/
	private String commenOwnerName;
	
	/** 共有人证件号码 **/
	private String commenOwnerCertCode;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveyNo
	 */
	public void setSurveyNo(String surveyNo) {
		this.surveyNo = surveyNo == null ? null : surveyNo.trim();
	}
	
    /**
     * @return SurveyNo
     */	
	public String getSurveyNo() {
		return this.surveyNo;
	}
	
	/**
	 * @param commenOwnerCusId
	 */
	public void setCommenOwnerCusId(String commenOwnerCusId) {
		this.commenOwnerCusId = commenOwnerCusId == null ? null : commenOwnerCusId.trim();
	}
	
    /**
     * @return CommenOwnerCusId
     */	
	public String getCommenOwnerCusId() {
		return this.commenOwnerCusId;
	}
	
	/**
	 * @param commenOwnerName
	 */
	public void setCommenOwnerName(String commenOwnerName) {
		this.commenOwnerName = commenOwnerName == null ? null : commenOwnerName.trim();
	}
	
    /**
     * @return CommenOwnerName
     */	
	public String getCommenOwnerName() {
		return this.commenOwnerName;
	}
	
	/**
	 * @param commenOwnerCertCode
	 */
	public void setCommenOwnerCertCode(String commenOwnerCertCode) {
		this.commenOwnerCertCode = commenOwnerCertCode == null ? null : commenOwnerCertCode.trim();
	}
	
    /**
     * @return CommenOwnerCertCode
     */	
	public String getCommenOwnerCertCode() {
		return this.commenOwnerCertCode;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}