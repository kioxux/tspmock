package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CtrLoanContDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class RiskItem0103Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0103Service.class);

    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;

    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private AccCvrsService accCvrsService;

    @Autowired
    private AccTfLocService accTfLocService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    /**
     * @方法名称: riskItem0103
     * @方法描述: 权证出库押品状态出库校验
     * @参数与返回说明:
     * @算法描述:
     * （1）、权证部分出库时
     *       1、押品与授信关联关系需解除
     *      2、授信分项下用信敞口余额小于等于授信分项下授信金额
     * （2）、权证更新出库时
     *       1、权证入库时，属预告登记时，出库时，权证出库原因细类选择权证更新出库时，才允许出库，否则拦截。
     * @创建人: yfs
     * @创建时间: 2021-08-26 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0103(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("权证出库押品状态出库校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppService.selectByPrimaryKey(serno);
        if(Objects.isNull(guarWarrantManageApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10301);
            return riskResultDto;
        }
        // 权证部分出库时
        if(Objects.equals(CmisBizConstants.STD_WARRANT_OUT_TYPE_01,guarWarrantManageApp.getWarrantOutTypeSub())) {
            //核心担保编号
            String guarContNo = guarWarrantManageApp.getGuarContNo();

            if (StringUtils.isEmpty(guarContNo)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                return riskResultDto;
            }

            //押品部分出库,检查权证关联的押品是否与授信分项解除关系以及部分出库的权证对应的抵押合同项下剩余的已入库权证所对应的融资敞口余额之和小于等于所对应担保合同所担保的债务敞口金额之和
            String result = checkGuarBizRelIsRemove(guarContNo, guarWarrantManageApp.getCoreGuarantyNo());

            if(StringUtils.nonEmpty(result)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(result);
                return riskResultDto;
            }
        }
        // 权证更新出库时
        if(Objects.equals(CmisBizConstants.STD_WARRANT_OUT_TYPE_03,guarWarrantManageApp.getWarrantOutTypeSub())) {
            // 根据担保合同编号查询担保合同和业务的关系表
            QueryModel model1 = new QueryModel();
            model1.addCondition("warrantNo", guarWarrantManageApp.getWarrantNo());
            model1.addCondition("coreGuarantyNo", guarWarrantManageApp.getCoreGuarantyNo());
            // STD_ZB_PRO_CERTI_ITEM  增加字典项  证明权利或事项
            // 01 抵押权
            // 02 预告登记
            // 03 居住权
            // 04 其他
            model1.addCondition("proCertiItem", "02");
            List<GuarWarrantInfo>  list = guarWarrantInfoService.selectAll(model1);
            if(CollectionUtils.isEmpty(list)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10310);
                return riskResultDto;
            }
            // 根据担保合同编号查询担保合同和业务的关系表
            QueryModel model3 = new QueryModel();
            model3.addCondition("guarContNo", guarWarrantManageApp.getGuarContNo());
            //主合同
            String mainContNo = grtGuarBizRstRelService.selectContNoByGuarContNo(model3);

            if(StringUtils.isBlank(mainContNo)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10304);
                return riskResultDto;
            }
            // 通过合同编号去查询合同信息
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(mainContNo);
            if(Objects.isNull(ctrLoanCont)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10311);
                return riskResultDto;
            }
            //022001	个人一手住房按揭贷款
            //022002	个人二手住房按揭贷款
            //022020	个人一手商用房按揭贷款
            //022021	个人二手商用房按揭贷款
            //022024	接力贷
            //022031	拍卖贷
            //022040	个人二手住房按揭贷款（资金托管）
            //022051	个人二手商用房按揭贷款（资金托管）
            //022052	个人一手住房按揭贷款（常熟资金监管）
            //022053	个人二手住房按揭贷款（连云港资金托管）
            //022054	个人二手商用房按揭贷款（连云港资金托管）
            //022055	个人一手住房按揭贷款（宿迁资金监管）
            //022056	个人一手商用房按揭贷款（宿迁资金监管）
            if(Objects.equals("022001",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022002",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022020",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022021",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022024",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022031",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022040",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022051",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022052",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022053",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022054",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022055",ctrLoanCont.getPrdId()) ||
                    Objects.equals("022056",ctrLoanCont.getPrdId()))

             {
                 riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                 riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                 return riskResultDto;
            }else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10304);
                return riskResultDto;
            }
        }
        log.info("权证出库押品状态出库校验结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 检查权证关联的押品是否与授信分项解除关系
     * 部分出库的权证对应的抵押合同项下剩余的已入库权证所对应的融资敞口余额之和小于等于所对应担保合同所担保的债务敞口金额之和
     * @param guarContNo
     * @param coreGuarantyNo
     * @return
     */
    public String checkGuarBizRelIsRemove(String guarContNo,String coreGuarantyNo){
        GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);

        //是否授信项下
        String isUnderLmt = grtGuarCont.getIsUnderLmt();

        log.info("是否授信项下："+isUnderLmt);

        if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(isUnderLmt)){
            //授信额度编号
            String lmtAccNo = grtGuarCont.getLmtAccNo();
            log.info("担保合同【"+guarContNo+"】的授信额度编号为【"+lmtAccNo+"】");

            if (StringUtils.isEmpty(lmtAccNo)){
                return "";
            }

            //根据授信额度编号查询授信分项流水号
            String subSerno = lmtReplyAccSubPrdService.getLmtReplyAccSubDataByAccSubPrdNo(lmtAccNo);
            log.info("[1]授信分项流水号:"+subSerno);

            if (StringUtils.isEmpty(subSerno)){
                Map map = new HashMap();
                map.put("accSubNo",lmtAccNo);
                LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
                subSerno = lmtReplyAccSub.getSubSerno();
                log.info("[2]授信分项流水号:"+subSerno);
            }

            //根据核心担保编号查询押品编号
            String guarNos = guarContRelWarrantService.selectGuarNosByCoreGuarantyNo(coreGuarantyNo);
            log.info("[1]押品编号:"+guarNos);

            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno",subSerno);
            queryModel.addCondition("guarNos",guarNos);
            guarNos = guarBizRelService.selectGuarNosByModel(queryModel);
            log.info("[2]押品编号:"+guarNos);

            if (StringUtils.nonEmpty(guarNos)){
                return "押品"+guarNos+"与授信分项"+subSerno+"的关系没有解除";
            }

            //根据担保合同编号查询关联的合同编号
            String contNos = grtGuarBizRstRelService.selectContNosByGuarContNo(guarContNo);
            log.info("根据担保合同编号【"+guarContNo+"】查询关联的合同编号【"+contNos+"】");

            if (StringUtils.isEmpty(contNos)){
                return "";
            }

            //贷款台账--用信敞口余额
            BigDecimal accLoanBalance = accLoanService.selectTotalSpacAmtByContNos(contNos);
            log.info("贷款台账--用信敞口余额【"+accLoanBalance+"】");

            //银承台账--用信敞口余额
            BigDecimal accAccpBalance = accAccpService.selectTotalSpacAmtByContNos(contNos);
            log.info("银承台账--用信敞口余额【"+accAccpBalance+"】");

            //保函台账--用信敞口余额
            BigDecimal accCvrsBalance = accCvrsService.selectTotalSpacAmtByContNos(contNos);
            log.info("保函台账--用信敞口余额【"+accCvrsBalance+"】");

            //开证台账--用信敞口余额
            BigDecimal accTfLocBalance = accTfLocService.selectTotalSpacAmtByContNos(contNos);
            log.info("开证台账--用信敞口余额【"+accTfLocBalance+"】");

            //部分出库的权证对应的抵押合同项下剩余的已入库权证所对应的融资敞口余额之和 = 贷款台账--用信敞口余额 + 银承台账--用信敞口余额 + 保函台账--用信敞口余额 + 开证台账--用信敞口余额
            BigDecimal totalBalance = accLoanBalance.add(accAccpBalance).add(accCvrsBalance).add(accTfLocBalance);
            log.info("部分出库的权证对应的抵押合同项下剩余的已入库权证所对应的融资敞口余额之和为【"+totalBalance+"】");

            if (totalBalance.compareTo(BigDecimal.ZERO)==0){
                //部分出库的权证对应的抵押合同项下剩余的已入库权证所对应的融资敞口余额之和为0，则校验通过
                return "";
            }

            //根据流水号和押品编号查询对应融资金额总和
            BigDecimal totalCorreFinAmt = guarBizRelService.selectTotalCorreFinAmtByModel(queryModel);

            if (totalBalance.compareTo(totalCorreFinAmt)==1){
                return "部分出库的权证对应的抵押合同项下剩余的已入库权证所对应的融资敞口余额之和【"+totalBalance+"】不能超过对应担保合同所担保的债务敞口金额之和【"+totalCorreFinAmt+"】";
            }
        }

        return "";
    }
}
