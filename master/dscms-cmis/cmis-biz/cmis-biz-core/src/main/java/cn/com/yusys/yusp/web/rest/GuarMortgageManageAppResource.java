/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.GuarMortgageManageAppDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarMortgageManageApp;
import cn.com.yusys.yusp.service.GuarMortgageManageAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageManageAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:23:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarmortgagemanageapp")
public class GuarMortgageManageAppResource {
    @Autowired
    private GuarMortgageManageAppService guarMortgageManageAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarMortgageManageApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarMortgageManageApp> list = guarMortgageManageAppService.selectAll(queryModel);
        return new ResultDto<List<GuarMortgageManageApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<GuarMortgageManageApp>> index(@RequestBody QueryModel queryModel) {
        List<GuarMortgageManageApp> list = guarMortgageManageAppService.selectByModel(queryModel);
        return new ResultDto<List<GuarMortgageManageApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarMortgageManageApp> show(@PathVariable("serno") String serno) {
        GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppService.selectByPrimaryKey(serno);
        return new ResultDto<GuarMortgageManageApp>(guarMortgageManageApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarMortgageManageApp> create(@RequestBody GuarMortgageManageApp guarMortgageManageApp) throws URISyntaxException {
        guarMortgageManageAppService.insert(guarMortgageManageApp);
        return new ResultDto<GuarMortgageManageApp>(guarMortgageManageApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarMortgageManageApp guarMortgageManageApp) throws URISyntaxException {
        int result = guarMortgageManageAppService.update(guarMortgageManageApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarMortgageManageAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarMortgageManageAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteOnlogic
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据流水号逻辑删除")
    @PostMapping("/deleteOnlogic")
    protected ResultDto<Integer> deleteOnlogic(@RequestBody GuarMortgageManageApp guarMortgageManageApp) {
        //int result = guarMortgageManageAppService.deleteOnlogic(serno);
        int result = guarMortgageManageAppService.updateSelective(guarMortgageManageApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryGuarMBySerno
     * @函数描述:根据流水号查询抵押登记申请对象
     * @param serno
     * @return
     */
    @ApiOperation("根据流水号查询抵押登记申请对象")
    @PostMapping("/queryGuarMBySerno")
    protected ResultDto<GuarMortgageManageAppDto> queryGuarMortgageManageAppDtoBySerno(@RequestBody String serno){
        GuarMortgageManageAppDto guarMortgageManageAppDto = guarMortgageManageAppService.queryGuarMortgageManageAppDtoBySerno(serno);
        return new ResultDto<GuarMortgageManageAppDto>(guarMortgageManageAppDto);
    }

    /**
     * @函数名称:tempSave
     * @函数描述:根据抵押登记申请对象暂存(新增/修改)数据
     * @param guarMortgageManageAppDto
     * @return
     */
    @ApiOperation("根据抵押登记申请对象暂存(新增/修改)数据")
    @PostMapping("/tempSave")
    protected ResultDto<Integer> tempSave(@RequestBody GuarMortgageManageAppDto guarMortgageManageAppDto){
        int result  = guarMortgageManageAppService.tempSave(guarMortgageManageAppDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据担保合同编号查询是否有在途的抵押登记申请
     * @函数名称:isExistByGuarContNo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/isExistByGuarContNo")
    protected ResultDto isExistByGuarContNo(@RequestBody QueryModel queryModel){
        return guarMortgageManageAppService.isExistByGuarContNo(queryModel);
    }

    /**
     * 查询押品对应的未审批通过的抵押登记流水号
     * @函数名称:selectByGuarNo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/selectByGuarNo/{guarNo}")
    protected ResultDto selectByGuarNo(@PathVariable("guarNo") String guarNo){
        String sernos = guarMortgageManageAppService.selectOnTheWaySernosByGuarNo(guarNo);
        return new ResultDto<String>(sernos);
    }

    /**
     * 查询押品对应的未审批通过的抵押登记流水号
     * @函数名称:selectByGuarNo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/selectApprovedRecordsByGuarNo/{guarNo}")
    protected ResultDto selectApprovedRecordsByGuarNo(@PathVariable("guarNo") String guarNo){
        int count = guarMortgageManageAppService.selectApprovedRecordsByGuarNo(guarNo);
        return new ResultDto<>(count);
    }
}
