package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 保证金存款特惠利率申请业务处理类 --寿光
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class SGCZ11BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(QTSX04BizService.class);

    @Autowired
    private OtherBailDepPreferRateAppService otherBailDepPreferRateAppService;

    @Autowired
    private OtherAccpPerferFeeAppService otherAccpPerferFeeAppService;

    @Autowired
    private OtherDiscPerferRateAppService otherDiscPerferRateAppService;

    @Autowired
    private OtherBailDepPreferRateAppCusListService otherBailDepPreferRateAppCusListService;

    @Autowired
    private OtherAccpPerferFeeAppCusListService OtherAccpPerferFeeAppCusListService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();
        String bizType = instanceInfo.getBizType();
        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
                //保证金存款特惠利率申请
                if (BizFlowConstant.SGF05.equals(bizType) || BizFlowConstant.DHF05.equals(bizType)){
                    otherBailDepPreferRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_111);
                    OtherBailDepPreferRateApp otherBailDepPreferRateApp =  otherBailDepPreferRateAppService.selectByPrimaryKey(serno);
                    WFBizParamDto param = new WFBizParamDto();
                    param.setBizId(serno);
                    param.setInstanceId(instanceInfo.getInstanceId());
                    Map<String, Object> params = new HashMap<>();
                    params.put("isUpperApprAuth",otherBailDepPreferRateApp.getIsUpperApprAuth());
                    param.setParam(params);
                    workflowCoreClient.updateFlowParam(param);
                }
                //银票手续费率优惠申请
                if (BizFlowConstant.SGF06.equals(bizType)||BizFlowConstant.DHF06.equals(bizType)){
                    otherAccpPerferFeeAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_111);
                    OtherAccpPerferFeeApp otherAccpPerferFeeApp =  otherAccpPerferFeeAppService.selectByPrimaryKey(serno);
                    WFBizParamDto param = new WFBizParamDto();
                    param.setBizId(serno);
                    param.setInstanceId(instanceInfo.getInstanceId());
                    Map<String, Object> params = new HashMap<>();
                    params.put("isUpperApprAuth",otherAccpPerferFeeApp.getIsUpperApprAuth());
                    param.setParam(params);
                    workflowCoreClient.updateFlowParam(param);
                }
                //贴现优惠利率申请
                if (BizFlowConstant.SGF07.equals(bizType)||BizFlowConstant.DHF07.equals(bizType)){
                    otherDiscPerferRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_111);
                    OtherDiscPerferRateApp otherDiscPerferRateApp =  otherDiscPerferRateAppService.selectByPrimaryKey(serno);
                    WFBizParamDto param = new WFBizParamDto();
                    param.setBizId(serno);
                    param.setInstanceId(instanceInfo.getInstanceId());
                    Map<String, Object> params = new HashMap<>();
                    params.put("isUpperApprAuth",otherDiscPerferRateApp.getIsUpperApprAuth());
                    param.setParam(params);
                    workflowCoreClient.updateFlowParam(param);
                }

            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                // 结束后处理

                //保证金存款特惠利率申请
                if (BizFlowConstant.SGF05.equals(bizType)||BizFlowConstant.DHF05.equals(bizType)){
                    otherBailDepPreferRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_997);
                    //时间 登记人 等级机构相关
                    //转换日期 ASSURE_CERT_CODE
                    SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                    User user = SessionUtils.getUserInformation();
                    QueryModel model = new QueryModel();
                    model.addCondition("serno",serno);
                    List<OtherBailDepPreferRateAppCusList> list =  otherBailDepPreferRateAppCusListService.selectByModel(model);
                    list.stream().forEach(e -> {
                        e.setCusListStatus(CmisBizConstants.STD_CUS_LIST_STATUS_01);
                        e.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                        e.setUpdateTime(DateUtils.getCurrDate());
                        e.setUpdDate(dateFormat.format(new Date()) );
                        e.setUpdId(user.getLoginCode());
                        e.setUpdBrId(user.getOrg().getCode());
                        otherBailDepPreferRateAppCusListService.updateSelective(e);
                    });

                }
                //银票手续费率优惠申请
                if (BizFlowConstant.SGF06.equals(bizType)||BizFlowConstant.DHF06.equals(bizType)){
                    otherAccpPerferFeeAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_997);
                    //时间 登记人 等级机构相关
                    //转换日期 ASSURE_CERT_CODE
                    SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                    User user = SessionUtils.getUserInformation();
                    QueryModel model = new QueryModel();
                    model.addCondition("serno",serno);
                    List<OtherAccpPerferFeeAppCusList> list =  OtherAccpPerferFeeAppCusListService.selectAll(model);
                    List<String> cusIdList = new ArrayList<>();
                    list.stream().forEach(e -> {
                        e.setCusListStatus(CmisBizConstants.STD_CUS_LIST_STATUS_01);
                        e.setUpdateTime(DateUtils.getCurrDate());
                        e.setUpdDate(dateFormat.format(new Date()) );
                        e.setUpdId(user.getLoginCode());
                        e.setUpdBrId(user.getOrg().getCode());
                        OtherAccpPerferFeeAppCusListService.updateSelective(e);
                        cusIdList.add(e.getCusId());
                    });
                    //列表不为空  更新关联客户的其他有效汇率优惠申请为失效状态
                    if (cusIdList.size() > 0) {
                        model = new QueryModel();
                        model.addCondition("cusIds", cusIdList);
                        model.addCondition("cusListStatus", CmisBizConstants.STD_CUS_LIST_STATUS_01);
                        List<OtherAccpPerferFeeAppCusList> otherAccpPerferFeeAppCusLists = OtherAccpPerferFeeAppCusListService.selectAll(model);
                        if (otherAccpPerferFeeAppCusLists != null && otherAccpPerferFeeAppCusLists.size() > 0) {
                            for (OtherAccpPerferFeeAppCusList otherAccpPerferFeeAppCusList : otherAccpPerferFeeAppCusLists) {
                                //判断是否为当前申请
                                if (!otherAccpPerferFeeAppCusList.getSerno().equals(serno)) {
                                    otherAccpPerferFeeAppCusList.setCusListStatus(CmisBizConstants.STD_CUS_LIST_STATUS_02);
                                    OtherAccpPerferFeeAppCusListService.updateSelective(otherAccpPerferFeeAppCusList);
                                }
                            }
                        }
                    }
                }
                //贴现优惠利率申请
                if (BizFlowConstant.SGF07.equals(bizType)||BizFlowConstant.DHF07.equals(bizType)){
                    otherDiscPerferRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_997);
                }

                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992

                //保证金存款特惠利率申请
                if (BizFlowConstant.SGF05.equals(bizType)||BizFlowConstant.DHF05.equals(bizType)){
                    otherBailDepPreferRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                }
                //银票手续费率优惠申请
                if (BizFlowConstant.SGF06.equals(bizType)||BizFlowConstant.DHF06.equals(bizType)){
                    otherAccpPerferFeeAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                }
                //贴现优惠利率申请
                if (BizFlowConstant.SGF07.equals(bizType)||BizFlowConstant.DHF07.equals(bizType)){
                    otherDiscPerferRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                }

            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);

                //保证金存款特惠利率申请
                if (BizFlowConstant.SGF05.equals(bizType)||BizFlowConstant.DHF05.equals(bizType)){
                    otherBailDepPreferRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                }
                //银票手续费率优惠申请
                if (BizFlowConstant.SGF06.equals(bizType)||BizFlowConstant.DHF06.equals(bizType)){
                    otherAccpPerferFeeAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                }
                //贴现优惠利率申请
                if (BizFlowConstant.SGF07.equals(bizType)||BizFlowConstant.DHF07.equals(bizType)){
                    otherDiscPerferRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                }


            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);

                //保证金存款特惠利率申请
                if (BizFlowConstant.SGF05.equals(bizType)||BizFlowConstant.DHF05.equals(bizType)){
                    otherBailDepPreferRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
                }
                //银票手续费率优惠申请
                if (BizFlowConstant.SGF06.equals(bizType)||BizFlowConstant.DHF06.equals(bizType)){
                    otherAccpPerferFeeAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
                }
                //贴现优惠利率申请
                if (BizFlowConstant.SGF07.equals(bizType)||BizFlowConstant.DHF07.equals(bizType)){
                    otherDiscPerferRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
                }

            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.SGCZ11.equals(flowCode);
    }
}
