package cn.com.yusys.yusp.service.server.xdtz0014;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.req.Xdpj22ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.Xdpj22RespDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0014.req.Xdtz0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0014.req.Xdtz0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0014.resp.Xdtz0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class Xdtz0014Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0014Service.class);

    @Autowired
    private PvpAuthorizeMapper pvpAuthorizeMapper;
    @Autowired
    private AccAccpMapper accAccpMapper;
    @Autowired
    private PvpAccpAppMapper pvpAccpAppMapper;
    @Autowired
    private BailDepositInfoMapper bailDepositInfoMapper;
    @Autowired
    private BailDepositInfoService bailDepositInfoService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;
    @Autowired
    private AccAccpDrftSubMapper accAccpDrftSubMapper;


    /**
     * 保证金补交/冲补交结果通知服务
     *
     * @param xdtz0014DataReqDto
     * @return
     */
    @Transactional
    public Xdtz0014DataRespDto bzjbjInfo(Xdtz0014DataReqDto xdtz0014DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdtz0014DataReqDto));
        Map map = new HashMap<>();
        Xdtz0014DataRespDto xdtz0014DataRespDto = new Xdtz0014DataRespDto();
        try {
            //批次号
            String pcSerno = xdtz0014DataReqDto.getSeqNo();
            // 操作码
            // 字典项  1、补交 2、冲正 3、未用退回/ 4、老电票补交保证金 5、老电票冲补交保证金 6、冲补交保证金前额度校验 8、冲补交保证金前额度校验（老电票） 9、纸票交保证金
            xdtz0014DataReqDto.getOprId();
            //核心保证金缴纳流水号
            String hxSecurityHandNo = xdtz0014DataReqDto.getCoreBailPaySerNo();
            //银承合同编号
            String contNo = xdtz0014DataReqDto.getContNo();
            //保证金类型	字典项 1 银承
            String bailType = xdtz0014DataReqDto.getBailType();
            //保证金账号
            String bailAcctNo = xdtz0014DataReqDto.getBailAcctNo();
            //保证金币种	字典项 同金额币种
            String bailCurType = xdtz0014DataReqDto.getBailCurType();
            //保证金金额
            String bzjje = xdtz0014DataReqDto.getBailAmt();
            //计息方式	字典项000 活期 203 三个月 206 六个月 999 协议 301 一年
            String intType = xdtz0014DataReqDto.getIntType();
            //保证金利息存入账号
            String bailIntAcctNo = xdtz0014DataReqDto.getBailIntAcctNo();
            //申请人户名
            String rqstrAcctName = xdtz0014DataReqDto.getRqstrAcctName();
            //申请人账号
            String rqstrAcctNo = xdtz0014DataReqDto.getRqstrAcctNo();
            //到期日期
            String end_Date = xdtz0014DataReqDto.getEndDate();
            //票据池出票标记
            String drfpoIsseMkDate = xdtz0014DataReqDto.getDrfpoIsseMk();
            //票号
            String sBillNo = xdtz0014DataReqDto.getDrftNo();
            //票面金额
            BigDecimal drftAmt = xdtz0014DataReqDto.getDrftAmt();

            Date curDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
            String strCurDate = sdf.format(curDate);
            String strCurDate1 = sdf1.format(curDate);
            //到期日期格式转换
            String due_Date = "";
            if (!"".equals(end_Date) && end_Date != null) {
                due_Date = sdf.format(sdf1.parse(end_Date));
            }
            if (xdtz0014DataReqDto.getSeqNo() == null || "".equals(xdtz0014DataReqDto.getSeqNo())) {
                //返回信息
                xdtz0014DataRespDto.setOpFlag("F");
                xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,批次号为空!");
            } else if (xdtz0014DataReqDto.getOprId() == null || "".equals(xdtz0014DataReqDto.getOprId())) {
                //返回信息
                xdtz0014DataRespDto.setOpFlag("F");
                xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,操作码为空!");
            } else {
                //操作码判断
                if ("1".equals(xdtz0014DataReqDto.getOprId())) {//补交保证金（新票+老纸）
                    //缴存登记人
                    AccAccp accAccp = accAccpMapper.selectByCoreBillNo(pcSerno);
                    if (xdtz0014DataReqDto.getSeqNo().indexOf("YCHX") != -1) {
                        hxSecurityHandNo = strCurDate1 + hxSecurityHandNo;//核心发送的保证金补交记录,流水号= 日期+核心流水号
                    }
                    BailDepositInfo bailDepositInfo = new BailDepositInfo();
                    bailDepositInfo.setSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>()));
                    bailDepositInfo.setContNo(contNo);
                    bailDepositInfo.setBailType(bailType);
                    bailDepositInfo.setBailAccNo(bailAcctNo);
                    bailDepositInfo.setBailAmt(new BigDecimal(bzjje));
                    bailDepositInfo.setInterestMode(intType);
                    bailDepositInfo.setRqstrAccNo(rqstrAcctNo);
                    bailDepositInfo.setRqstrAccName(rqstrAcctName);
                    bailDepositInfo.setEndDate(end_Date);
                    bailDepositInfo.setStatus("0");
                    bailDepositInfo.setBailInterestDepAcct(bailIntAcctNo);
                    bailDepositInfo.setDepositInputDate(strCurDate);
                    bailDepositInfo.setDepositInputId(accAccp.getInputId());
                    bailDepositInfo.setDepositInputBrId(accAccp.getInputBrId());
                    bailDepositInfo.setInputAccpBailSerno(pcSerno);
                    bailDepositInfoMapper.insertSelective(bailDepositInfo);

                    CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
                    CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
                    cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(accAccp.getManagerBrId()));//金融机构代码
                    cmisLmt0014ReqDto.setSerno(pcSerno);//交易流水号
                    cmisLmt0014ReqDto.setInputId(accAccp.getInputId());//登记人
                    cmisLmt0014ReqDto.setInputBrId(accAccp.getInputBrId());//登记机构
                    cmisLmt0014ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期
                    cmisLmt0014ReqdealBizListDto.setDealBizNo(pcSerno);
                    cmisLmt0014ReqdealBizListDto.setRecoverType("04");//恢复类型
                    cmisLmt0014ReqdealBizListDto.setRecoverStd("02");//差额
                    cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(BigDecimal.ZERO);//台账金额
                    cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(new BigDecimal(xdtz0014DataReqDto.getBailAmt()));//敞口金额
                    cmisLmt0014ReqdealBizListDto.setSecurityAmt(new BigDecimal(xdtz0014DataReqDto.getBailAmt()));//保证金金额
                    cmisLmt0014ReqDto.setDealBizList(Arrays.asList(cmisLmt0014ReqdealBizListDto));
                    logger.info("发送cmisLmt0014开始，参数：{}", JSON.toJSONString(cmisLmt0014ReqDto));
                    ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
                    logger.info("发送cmisLmt0014结束，返回：{}", JSON.toJSONString(cmisLmt0014RespDtoResultDto));
                    if (Objects.nonNull(cmisLmt0014RespDtoResultDto)
                            && Objects.equals(cmisLmt0014RespDtoResultDto.getCode(), SuccessEnum.CMIS_SUCCSESS.key)
                            && Objects.equals(cmisLmt0014RespDtoResultDto.getData().getErrorCode(), SuccessEnum.SUCCESS.key)) {
                        xdtz0014DataRespDto.setOpFlag("S");
                        xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,保证金补交信息插入成功!");
                    } else {
                        xdtz0014DataRespDto.setOpFlag("F");
                        xdtz0014DataRespDto.setOpMsg(cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
                    }


                } else if ("2".equals(xdtz0014DataReqDto.getOprId())) {//冲补交保证金（新票+老纸）
                    // 保证金总额查询
                    String bzj = bailDepositInfoMapper.selectBzjInfo(hxSecurityHandNo);
                    if (bzj != null && bzj.length() != 0) {
                        //汇率
                        String rate = "1";
                        double securityAmt = Double.parseDouble(bzjje) * Double.parseDouble(rate);
                        String security_money_amt = bzj;
                        double SMA = Double.parseDouble(security_money_amt) - securityAmt;
                        SMA = SMA <= 0.0 ? 0.0 : SMA;
                        map.put("serno", hxSecurityHandNo);
                        map.put("newBzj", BigDecimal.valueOf(SMA));
                        //更新保证金
                        bailDepositInfoMapper.updateBailamt(map);
                        map.clear();
                        xdtz0014DataRespDto.setOpFlag("S");
                        xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,保证金冲正信息修改成功!");
                    } else {
                        xdtz0014DataRespDto.setOpFlag("F");
                        xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,未查询到该核心保证金交易流水号的信息!");
                    }
                } else if ("3".equals(xdtz0014DataReqDto.getOprId())) {//未用退回
                    //未用退回查看
                    AccAccp accAccp = accAccpMapper.queryDomainByCoreBillNo(xdtz0014DataReqDto.getSeqNo());
                    if (Objects.isNull(accAccp)) {
                        xdtz0014DataRespDto.setOpFlag("F");
                        xdtz0014DataRespDto.setOpMsg("银承台账不存在！！");
                    } else {
                        this.updateAccAndLmt(accAccp, xdtz0014DataReqDto);
                        xdtz0014DataRespDto.setOpFlag("S");
                        xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,退回成功!");
                    }
                } else if ("4".equals(xdtz0014DataReqDto.getOprId())) {//老电票补交保证金
                    List<AccAccp> list = accAccpMapper.getBzjjeInfo(sBillNo);
                    if (list != null && list.size() > 0) {
                        AccAccp accp = list.get(0);
                        String bail_amt = accp.getBailAmt().toString();
                        double SMA = Double.parseDouble(bail_amt) + Double.parseDouble(bzjje);
                        SMA = SMA <= 0.0 ? 0.0 : SMA;
                        BigDecimal newSma = BigDecimal.valueOf(SMA);
                        map.put("sBillNo", sBillNo);
                        map.put("sma", SMA);
                        accAccpMapper.updateBzjjeInfo(map);
                        xdtz0014DataRespDto.setOpFlag("S");
                        xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,保证金补交信息插入成功!");

                    } else {
                        xdtz0014DataRespDto.setOpFlag("F");
                        xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,未查询到该票号的借据信息!");

                    }
                } else if ("5".equals(xdtz0014DataReqDto.getOprId())) {//老电票冲补交保证金
                    List<AccAccp> list = accAccpMapper.getBzjjeInfo(sBillNo);
                    if (list != null && list.size() > 0) {
                        AccAccp accp = list.get(0);
                        String bail_amt = accp.getBailAmt().toString();
                        double SMA = Double.parseDouble(bail_amt) - Double.parseDouble(bzjje);
                        SMA = SMA <= 0.0 ? 0.0 : SMA;
                        map.put("sBillNo", sBillNo);
                        map.put("sma", BigDecimal.valueOf(SMA));
                        accAccpMapper.updateBzjjeInfo(map);

                        xdtz0014DataRespDto.setOpFlag("S");
                        xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,保证金冲补交成功!");

                    } else {
                        xdtz0014DataRespDto.setOpFlag("F");
                        xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,未查询到该票号的借据信息,无法冲补交!");

                    }
                } else if ("6".equals(xdtz0014DataReqDto.getOprId())) {//冲补交保证金前额度校验(新票+老纸票)
                    String isflag = bailDepositInfoMapper.selectSfcz(hxSecurityHandNo);
                    String rate = "1";
                    double securityAmt = Double.parseDouble(bzjje) * Double.parseDouble(rate);
                    if (isflag != null && isflag.length() > 0) {
                        String cont_no = pvpAuthorizeMapper.getcontNo(pcSerno);// 取合同号
                        double contAmt = bailDepositInfoService.difAmt(cont_no);//合同可出账金额
                        if (contAmt - securityAmt < 0) {
                            xdtz0014DataRespDto.setOpFlag("F");
                            xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,保证金金额不足，不允许冲正!");
                        } else {
                            xdtz0014DataRespDto.setOpFlag("S");
                            xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,允许冲正!");
                        }
                    } else {
                        xdtz0014DataRespDto.setOpFlag("F");
                        xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,未查询到该核心保证金交易流水号的信息!");
                    }
                } else if ("8".equals(xdtz0014DataReqDto.getOprId())) {//冲补交保证金前额度校验(老电票)
                    List<AccAccp> list = accAccpMapper.getBzjjeInfo(sBillNo);

                    if (list != null && list.size() > 0) {
                        AccAccp accp = list.get(0);
                        Double bail_amt = accp.getBailAmt().doubleValue();//该电票下保证金
                        double contAmt = bailDepositInfoService.difAmt(contNo);//合同可出账金额
                        if (contAmt - Double.parseDouble(bzjje) < 0 || bail_amt - Double.parseDouble(bzjje) < 0) {
                            xdtz0014DataRespDto.setOpFlag("F");
                            xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,保证金金额不足，不允许冲正!");
                        } else {
                            xdtz0014DataRespDto.setOpFlag("S");
                            xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,允许冲正!");
                        }
                    } else {
                        xdtz0014DataRespDto.setOpFlag("F");
                        xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,未查询到该票号的借据信息,无法冲补交!");
                    }
                } else if ("9".equals(xdtz0014DataReqDto.getOprId())) {//票据纸票交保证金、外币换算成人民币
                    //查询客户经理部分信息
                    PvpAccpApp pvpAccpApp = pvpAccpAppMapper.selectBySerno(xdtz0014DataReqDto.getSeqNo());
                    if (pvpAccpApp != null) {
                        String REGISTER_BR_ID = pvpAccpApp.getManagerBrId();
                        String fina_br_id = pvpAccpApp.getFinaBrId();
                        String manager_id = pvpAccpApp.getManagerId();
                        String rate = "1";
                        double securityAmt = Double.parseDouble(bzjje) * Double.parseDouble(rate);
//                             插入数据
//                            (SERNO, CONT_NO, SECURITY_MONEY_TYPE, SECURITY_MONEY_AC_NO,
//                               SECURITY_MONEY_AMT, TERMCD_TYPE, CUS_ACCT_NAME, CUS_ACCT,
//                               END_DATE, CONT_STATE, LXACNO,register_date,
//                               servno,REGISTER_BR_ID,REGISTER_ID,CONT_DEP,CUR_TYPE)
//                                values ( Hx_Security_Hand_No , pcSerno , '1',securityMoneyAcNo ,
//                                securityAmt, rftermType, applyAcName, applyAcNo,
//                                due_Date, '0', securityAmoRateAcNo , strCurDate,
//                                accpContNo,REGISTER_BR_ID,manager_id,fina_br_id,'CNY');

                        BailDepositInfo bailDepositInfo = new BailDepositInfo();

                        bailDepositInfo.setSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>()));
                        bailDepositInfo.setContNo(contNo);
                        bailDepositInfo.setBailType(bailType);
                        bailDepositInfo.setBailAccNo(bailAcctNo);

                        bailDepositInfo.setBailAmt(new BigDecimal(bzjje));
                        bailDepositInfo.setInterestMode(intType);
                        bailDepositInfo.setRqstrAccNo(rqstrAcctNo);
                        bailDepositInfo.setRqstrAccName(rqstrAcctName);

                        bailDepositInfo.setEndDate(end_Date);
                        bailDepositInfo.setStatus("0");
                        bailDepositInfo.setBailInterestDepAcct(bailIntAcctNo);
                        bailDepositInfo.setDepositInputDate(strCurDate);

                        bailDepositInfo.setInputAccpBailSerno(pcSerno);
                        bailDepositInfo.setDepositInputBrId(REGISTER_BR_ID);//缴存登记机构
                        bailDepositInfo.setDepositInputId(manager_id);//缴存登记人
                        bailDepositInfo.setContDep(fina_br_id);//合同部门
                        bailDepositInfo.setCurType("CNY");//币种
                        bailDepositInfoMapper.insertSelective(bailDepositInfo);
                    }
                    xdtz0014DataRespDto.setOpFlag("S");
                    xdtz0014DataRespDto.setOpMsg("接口XDTZ0014,交保证金信息插入成功!");
                }
            }
        } catch (BizException e) {
            logger.info("报错堆栈信息1", e);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info("报错堆栈信息2", e);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdtz0014DataRespDto));
        return xdtz0014DataRespDto;
    }


    public void updateAccAndLmt(AccAccp accaccp, Xdtz0014DataReqDto xdtz0014DataReqDto) {

        //add by zhangjw 20211121 逻辑修改
        BigDecimal drftAmt = xdtz0014DataReqDto.getDrftAmt();//票面金额
        String bailAmt = xdtz0014DataReqDto.getBailAmt();//保证金金额
        if (StringUtils.isBlank(bailAmt)) bailAmt = "0";
        BigDecimal bailAmtAmt = bailAmt == null || bailAmt.equals("") ? BigDecimal.ZERO : new BigDecimal(bailAmt);
        String drftNo = xdtz0014DataReqDto.getDrftNo();//票号
        logger.info("银票未用退回：批次【" + accaccp.getCoreBillNo() + "】,退票【" + drftNo + "】--------------------开始");
        logger.info("银票未用退回：,退票【" + drftNo + "】--------------------票面金额 【" + bailAmtAmt.toString() + "】,保证金金额【" + bailAmtAmt.toString() + "】");
        //将未用退回的票 状态修改为作废
        AccAccpDrftSub accAccpDrftSub = new AccAccpDrftSub();
        accAccpDrftSub.setBillNo(xdtz0014DataReqDto.getDrftNo());
        accAccpDrftSub.setCoreBillNo(xdtz0014DataReqDto.getSeqNo());
        accAccpDrftSub.setAccStatus("7");
        accAccpDrftSubMapper.updateAccStatusByCoreBillNoAndBillNo(accAccpDrftSub);

        BigDecimal wythCk = BigDecimal.ZERO;
        //判断是否低风险业务，若是，则原始敞口金额不变
        if (!Objects.equals("21", accaccp.getGuarMode()) && !Objects.equals("40", accaccp.getGuarMode())) {
            wythCk = drftAmt.subtract(bailAmtAmt); //未用退回的敞口部分
        }
        logger.info("银票未用退回：,退票【" + drftNo + "】--------------------未用退回票的敞口金额 wythCk【" + wythCk + "】");

        //如果未用退回的金额>0，则更新原始敞口金额 = 原始敞口金额 - 未用退回敞口部分金额
        BigDecimal origiOpenAmt = accaccp.getOrigiOpenAmt();
        BigDecimal openAmt = accaccp.getSpacAmt();
        BigDecimal accDrftBalanceAmt = accaccp.getDrftBalanceAmt();

        BigDecimal accBailAmt = accaccp.getBailAmt();
        if (wythCk.compareTo(BigDecimal.ZERO) > 0) {
            origiOpenAmt = origiOpenAmt.subtract(wythCk);
            openAmt = openAmt.subtract(wythCk);
        }
        logger.info("银票未用退回：,退票【" + drftNo + "】--------------------原始敞口金额 origiOpenAmt【" + origiOpenAmt + "】");
        logger.info("银票未用退回：,退票【" + drftNo + "】--------------------敞口余额 openAmt【" + openAmt + "】");
        accaccp.setOrigiOpenAmt(origiOpenAmt);
        accaccp.setSpacAmt(openAmt);

        if (bailAmtAmt.compareTo(BigDecimal.ZERO) > 0) {
            accBailAmt = accBailAmt.subtract(bailAmtAmt);
            accaccp.setBailAmt(accBailAmt);
        }
        logger.info("银票未用退回：,退票【" + drftNo + "】--------------------保证金金额 accBailAmt【" + accBailAmt + "】");

        accDrftBalanceAmt = accDrftBalanceAmt.subtract(drftAmt);
        accaccp.setDrftBalanceAmt(accDrftBalanceAmt);
        logger.info("银票未用退回：,退票【" + drftNo + "】--------------------银票总余额 accDrftBalanceAmt【" + accDrftBalanceAmt + "】");
        accaccp.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));
        accAccpMapper.updateByPrimaryKeySelective(accaccp);


        String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
        ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = null;
        //若作废的银票票面大于0，则计算更新原始敞口金额
        if (drftAmt.compareTo(BigDecimal.ZERO) > 0) {
            //原始占用总额 = 批次金额 - 项下作废的票面金额 = 原始敞口金额 /（1-保证金比例）PS:因为开票当天无票据明细，故无法使用票据明细汇总计算
            logger.info("银票未用退回：批次【" + accaccp.getCoreBillNo() + "】,退票【" + drftNo + "】，计算原始占用总金额开始 ");
            BigDecimal zfBillAmtTotal = getZfBillAmtTotal(accaccp);
            logger.info("银票未用退回：批次【" + accaccp.getCoreBillNo() + "】,退票【" + drftNo + "】，获取票据系统计算作废总票面金额 zfBillAmtTotal【"+zfBillAmtTotal+"】 ");
            BigDecimal yszje = accaccp.getDrftTotalAmt().subtract(zfBillAmtTotal);
            logger.info("银票未用退回：批次【" + accaccp.getCoreBillNo() + "】,退票【" + drftNo + "】，原始占用总金额 = 批次总金额【"+accaccp.getDrftTotalAmt()+"】 - 票据作废金额【"+zfBillAmtTotal+"】 - 本笔票作废金额【"+drftAmt+"】 ");
            yszje = yszje.subtract(drftAmt);
            logger.info("银票未用退回：批次【" + accaccp.getCoreBillNo() + "】,退票【" + drftNo + "】，计算原始占用总金额 yszje =" + yszje + " ");
            try {
                cmisLmt0014RespDtoResultDto = sendCmisLmt0014WYTH(serno, accaccp, "08", "01", yszje, origiOpenAmt);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        logger.info("银票未用退回：批次【" + accaccp.getCoreBillNo() + "】,退票【" + drftNo + "】--------------------结束");
    }


    public BigDecimal getZfBillAmtTotal(AccAccp accaccp) {

        // 作废的票面总金额
        BigDecimal zfBillAmtTotal = BigDecimal.ZERO;

        // 发送xdpj22接口查询
        Xdpj22ReqDto reqDto = new Xdpj22ReqDto();
        reqDto.setSBatchNo(accaccp.getCoreBillNo());
        logger.info("发送xdpj22接口查询,请求参数:" + Objects.toString(reqDto));
        ResultDto<Xdpj22RespDto> Xdpj22ResultDto = dscms2PjxtClientService.xdpj22(reqDto);
        logger.info("获取xdpj22接口查询,响应报文:" + Objects.toString(Xdpj22ResultDto));
        if (SuccessEnum.CMIS_SUCCSESS.key.equals(Xdpj22ResultDto.getCode())) {
            // 更新银承台账信息
            if (Objects.nonNull(Xdpj22ResultDto.getData())) {
                Xdpj22RespDto xdpj22RespDto = Xdpj22ResultDto.getData();
                java.util.List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.List> lists = xdpj22RespDto.getList();
                if (CollectionUtils.nonEmpty(lists)) {
                    for (cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.List e : lists) {
                        if (!Objects.equals("1", e.getChargeFlag())) {
                            // 只要有一张票没结清, 台账状态就是未结清
                            if (Objects.equals("CD_114", e.getStatusFlag()) || Objects.equals("CD_115", e.getStatusFlag())
                                    || Objects.equals("TY_002", e.getStatusFlag()) || Objects.equals("TY_001", e.getStatusFlag())) {
                                //作废的票的 票面金额  票面敞口金额   累加
                                BigDecimal zfBillAmt = new BigDecimal(e.getfBillAmount());
                                BigDecimal zfBzjAmt = new BigDecimal(e.getBailAmt());
                                zfBillAmtTotal = zfBillAmtTotal.add(zfBillAmt);
                                logger.info("银票批次【" + accaccp.getCoreBillNo() + "】同步：计算作废票据明细 zfBillAmtTotal 【" + zfBillAmtTotal + "】 累加 【" + zfBillAmt + "】 ");
                            }
                        }
                    }
                }

            } else {
                throw BizException.error(null, "9999", "票据系统获取数据失败");
            }
        } else {
            throw BizException.error(null, "9999", "票据系统获取数据失败");
        }
        return zfBillAmtTotal;
    }



    /**
     * 发送台账恢复接口(未用退回)
     * @param serno
     * @param accAccp
     */
    private ResultDto<CmisLmt0014RespDto> sendCmisLmt0014WYTH(String serno, AccAccp accAccp, String recoverType,String recoverStd,BigDecimal yszje,BigDecimal ysckje) {
        CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
        cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(accAccp.getManagerBrId()));//金融机构代码
        cmisLmt0014ReqDto.setSerno(serno+"WYTH");//交易流水号
        cmisLmt0014ReqDto.setInputId(accAccp.getInputId());//登记人
        cmisLmt0014ReqDto.setInputBrId(accAccp.getInputBrId());//登记机构
        cmisLmt0014ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//登记日期

        CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
        cmisLmt0014ReqdealBizListDto.setDealBizNo(accAccp.getCoreBillNo());//台账编号
        cmisLmt0014ReqdealBizListDto.setRecoverType(recoverType);
        cmisLmt0014ReqdealBizListDto.setRecoverStd(recoverStd);// 恢复标准值
        cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(accAccp.getDrftBalanceAmt());// 银承台账余额
        cmisLmt0014ReqdealBizListDto.setCreditTotal(yszje);//原始占用金额
        // 判断有无敞口金额 21低风险质押 40全额保证金
        if(!Objects.equals("21",accAccp.getGuarMode()) && !Objects.equals("40",accAccp.getGuarMode())){
            cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(accAccp.getSpacAmt());// 台账敞口余额（人民币）
            cmisLmt0014ReqdealBizListDto.setCreditSpacTotal(ysckje);//原始敞口金额
        }else{
            cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(BigDecimal.ZERO);// 台账敞口余额（人民币）
            cmisLmt0014ReqdealBizListDto.setCreditSpacTotal(BigDecimal.ZERO);//原始敞口金额
        }
        cmisLmt0014ReqDto.setDealBizList(Arrays.asList(cmisLmt0014ReqdealBizListDto));
        logger.info("银票（未用退回）" + serno + "，前往额度系统恢复占用额度开始,参数" + JSON.toJSONString(cmisLmt0014ReqDto));
        ResultDto<CmisLmt0014RespDto> resultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
        logger.info("银票（未用退回）" + serno + "，前往额度系统恢复占用额度结束,参数" + JSON.toJSONString(resultDto));
        return resultDto;
    }

}
