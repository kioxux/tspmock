package cn.com.yusys.yusp.web.server.xdsx0006;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0006.req.Xdsx0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0006.resp.Xdsx0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdsx0006.Xdsx0006Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:专业贷款评级结果同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDSX0006:专业贷款评级结果同步")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0006Resource.class);

    @Autowired
    private Xdsx0006Service xdsx0006Service;
    /**
     * 交易码：xdsx0006
     * 交易描述：专业贷款评级结果同步
     *
     * @param xdsx0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdsx0006:专业贷款评级结果同步")
    @PostMapping("/xdsx0006")
    protected @ResponseBody
    ResultDto<Xdsx0006DataRespDto> xdsx0006(@Validated @RequestBody Xdsx0006DataReqDto xdsx0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, JSON.toJSONString(xdsx0006DataReqDto));
        Xdsx0006DataRespDto xdsx0006DataRespDto = new Xdsx0006DataRespDto();// 响应Dto:专业贷款评级结果同步
        ResultDto<Xdsx0006DataRespDto> xdsx0006DataResultDto = new ResultDto<>();

        // 从xdsx0006DataReqDto获取业务值进行业务逻辑处理
        String evalAppSerno = xdsx0006DataReqDto.getEvalAppSerno();//评级申请流水号
        String lmtAppSerno = xdsx0006DataReqDto.getLmtAppSerno();//授信申请流水号
        String cusId = xdsx0006DataReqDto.getCusId();//客户编号
        String cusName = xdsx0006DataReqDto.getCusName();//客户名称
        String certType = xdsx0006DataReqDto.getCertType();//证件类型
        String certNo = xdsx0006DataReqDto.getCertNo();//证件号码
        String evalYear = xdsx0006DataReqDto.getEvalYear();//评级年度
        String evalHppType = xdsx0006DataReqDto.getEvalHppType();//评级发生类型
        String evalModel = xdsx0006DataReqDto.getEvalModel();//评级模型
        String majorLoanStartLvl = xdsx0006DataReqDto.getMajorLoanStartLvl();//专业贷款系统初始等级
        String majorLoanAdjdLvl = xdsx0006DataReqDto.getMajorLoanAdjdLvl();//专业贷款调整后等级
        String majorLoanAdviceLvl = xdsx0006DataReqDto.getMajorLoanAdviceLvl();//专业贷款建议等级
        String majorLoanFinalLvl = xdsx0006DataReqDto.getMajorLoanFinalLvl();//专业贷款最终认定等级
        BigDecimal majorLoanExpectLossRate = xdsx0006DataReqDto.getMajorLoanExpectLossRate();//专业贷款预期损失率
        String evalStartDate = xdsx0006DataReqDto.getEvalStartDate();//评级生效日
        String evalEndDate = xdsx0006DataReqDto.getEvalEndDate();//评级到期日
        String riskTypeMax = xdsx0006DataReqDto.getRiskTypeMax();//风险大类
        String riskType = xdsx0006DataReqDto.getRiskType();//风险种类
        String riskTypeMin = xdsx0006DataReqDto.getRiskTypeMin();//风险小类
        String riskDtghDate = xdsx0006DataReqDto.getRiskDtghDate();//风险划分日期
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, JSON.toJSONString(xdsx0006DataReqDto));
            xdsx0006DataRespDto = xdsx0006Service.updateMajorGradeInfoDetail(xdsx0006DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, JSON.toJSONString(xdsx0006DataRespDto));
            // 封装xdsx0006DataResultDto中正确的返回码和返回信息
            xdsx0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, e.getMessage());
            // 封装xdsx0006DataResultDto中异常返回码和返回信息
            xdsx0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0006DataRespDto到xdsx0006DataResultDto中
        xdsx0006DataResultDto.setData(xdsx0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, JSON.toJSONString(xdsx0006DataResultDto));
        return xdsx0006DataResultDto;
    }
}
