/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardFinalJudgInifo
 * @类描述: credit_card_final_judg_inifo数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-27 14:33:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_card_final_judg_inifo")
public class CreditCardFinalJudgInifo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 终审岗位标识 **/
	@Column(name = "FINAL_POST_FLAG", unique = false, nullable = true, length = 5)
	private String finalPostFlag;
	
	/** 申请卡产品 **/
	@Column(name = "APPLY_CARD_PRD", unique = false, nullable = true, length = 6)
	private String applyCardPrd;
	
	/** 是否同时申请普通信用卡 **/
	@Column(name = "IS_APPLY_COMMON_CARD", unique = false, nullable = true, length = 5)
	private String isApplyCommonCard;
	
	/** 审批额度 **/
	@Column(name = "APPROVE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal approveAmt;
	
	/** 日费率 **/
	@Column(name = "DAILY_FEE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal dailyFeeRate;
	
	/** 审批结果 **/
	@Column(name = "APPROVE_RESULT", unique = false, nullable = true, length = 5)
	private String approveResult;
	
	/** 拒绝原因 **/
	@Column(name = "REFUSE_REASON", unique = false, nullable = true, length = 5)
	private String refuseReason;
	
	/** 普通信用卡申请卡产品 **/
	@Column(name = "COMMON_CARD_PRD", unique = false, nullable = true, length = 5)
	private String commonCardPrd;
	
	/** 普卡核准额度 **/
	@Column(name = "COMMON_CARD_APPROVE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal commonCardApproveAmt;
	
	/** 普卡审批结果 **/
	@Column(name = "COMMON_CARD_APPROVE_RESULT", unique = false, nullable = true, length = 5)
	private String commonCardApproveResult;
	
	/** 普卡拒绝原因 **/
	@Column(name = "COMMON_CARD_REFUSE_REASON", unique = false, nullable = true, length = 5)
	private String commonCardRefuseReason;
	
	/** 审批结论 **/
	@Column(name = "APPROVE_CONCLUSION", unique = false, nullable = true, length = 5)
	private String approveConclusion;
	
	/** 退回原因 **/
	@Column(name = "RETURN_REASON", unique = false, nullable = true, length = 5)
	private String returnReason;
	
	/** 终审备注 **/
	@Column(name = "FINAL_JUDG_REMARK", unique = false, nullable = true, length = 500)
	private String finalJudgRemark;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param finalPostFlag
	 */
	public void setFinalPostFlag(String finalPostFlag) {
		this.finalPostFlag = finalPostFlag;
	}
	
    /**
     * @return finalPostFlag
     */
	public String getFinalPostFlag() {
		return this.finalPostFlag;
	}
	
	/**
	 * @param applyCardPrd
	 */
	public void setApplyCardPrd(String applyCardPrd) {
		this.applyCardPrd = applyCardPrd;
	}
	
    /**
     * @return applyCardPrd
     */
	public String getApplyCardPrd() {
		return this.applyCardPrd;
	}
	
	/**
	 * @param isApplyCommonCard
	 */
	public void setIsApplyCommonCard(String isApplyCommonCard) {
		this.isApplyCommonCard = isApplyCommonCard;
	}
	
    /**
     * @return isApplyCommonCard
     */
	public String getIsApplyCommonCard() {
		return this.isApplyCommonCard;
	}
	
	/**
	 * @param approveAmt
	 */
	public void setApproveAmt(java.math.BigDecimal approveAmt) {
		this.approveAmt = approveAmt;
	}
	
    /**
     * @return approveAmt
     */
	public java.math.BigDecimal getApproveAmt() {
		return this.approveAmt;
	}
	
	/**
	 * @param dailyFeeRate
	 */
	public void setDailyFeeRate(java.math.BigDecimal dailyFeeRate) {
		this.dailyFeeRate = dailyFeeRate;
	}
	
    /**
     * @return dailyFeeRate
     */
	public java.math.BigDecimal getDailyFeeRate() {
		return this.dailyFeeRate;
	}
	
	/**
	 * @param approveResult
	 */
	public void setApproveResult(String approveResult) {
		this.approveResult = approveResult;
	}
	
    /**
     * @return approveResult
     */
	public String getApproveResult() {
		return this.approveResult;
	}
	
	/**
	 * @param refuseReason
	 */
	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}
	
    /**
     * @return refuseReason
     */
	public String getRefuseReason() {
		return this.refuseReason;
	}
	
	/**
	 * @param commonCardPrd
	 */
	public void setCommonCardPrd(String commonCardPrd) {
		this.commonCardPrd = commonCardPrd;
	}
	
    /**
     * @return commonCardPrd
     */
	public String getCommonCardPrd() {
		return this.commonCardPrd;
	}
	
	/**
	 * @param commonCardApproveAmt
	 */
	public void setCommonCardApproveAmt(java.math.BigDecimal commonCardApproveAmt) {
		this.commonCardApproveAmt = commonCardApproveAmt;
	}
	
    /**
     * @return commonCardApproveAmt
     */
	public java.math.BigDecimal getCommonCardApproveAmt() {
		return this.commonCardApproveAmt;
	}
	
	/**
	 * @param commonCardApproveResult
	 */
	public void setCommonCardApproveResult(String commonCardApproveResult) {
		this.commonCardApproveResult = commonCardApproveResult;
	}
	
    /**
     * @return commonCardApproveResult
     */
	public String getCommonCardApproveResult() {
		return this.commonCardApproveResult;
	}
	
	/**
	 * @param commonCardRefuseReason
	 */
	public void setCommonCardRefuseReason(String commonCardRefuseReason) {
		this.commonCardRefuseReason = commonCardRefuseReason;
	}
	
    /**
     * @return commonCardRefuseReason
     */
	public String getCommonCardRefuseReason() {
		return this.commonCardRefuseReason;
	}
	
	/**
	 * @param approveConclusion
	 */
	public void setApproveConclusion(String approveConclusion) {
		this.approveConclusion = approveConclusion;
	}
	
    /**
     * @return approveConclusion
     */
	public String getApproveConclusion() {
		return this.approveConclusion;
	}
	
	/**
	 * @param returnReason
	 */
	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}
	
    /**
     * @return returnReason
     */
	public String getReturnReason() {
		return this.returnReason;
	}
	
	/**
	 * @param finalJudgRemark
	 */
	public void setFinalJudgRemark(String finalJudgRemark) {
		this.finalJudgRemark = finalJudgRemark;
	}
	
    /**
     * @return finalJudgRemark
     */
	public String getFinalJudgRemark() {
		return this.finalJudgRemark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}