package cn.com.yusys.yusp.web.server.xdtz0034;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdtz0034.Xdtz0034Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0034.req.Xdtz0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0034.resp.Xdtz0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据客户号查询申请人是否有行内信用记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0034:根据客户号查询申请人是否有行内信用记录")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0034Resource{
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0034Resource.class);
    @Autowired
    private Xdtz0034Service xdtz0034Service;
    /**
     * 交易码：xdtz0034iz
     * 交易描述：根据客户号查询申请人是否有行内信用记录
     *
     * @param xdtz0034DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号查询申请人是否有行内信用记录")
    @PostMapping("/xdtz0034")
    protected @ResponseBody
    ResultDto<Xdtz0034DataRespDto> xdtz0034(@Validated @RequestBody Xdtz0034DataReqDto xdtz0034DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, JSON.toJSONString(xdtz0034DataReqDto));
        Xdtz0034DataRespDto xdtz0034DataRespDto = new Xdtz0034DataRespDto();// 响应Dto:根据客户号查询申请人是否有行内信用记录
        ResultDto<Xdtz0034DataRespDto> xdtz0034DataResultDto = new ResultDto<>();
        String cusId = xdtz0034DataReqDto.getCusId();//客户号
        try {
            // 从xdtz0034DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, JSON.toJSONString(xdtz0034DataReqDto));
            xdtz0034DataRespDto = xdtz0034Service.xdtz0034(xdtz0034DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, JSON.toJSONString(xdtz0034DataRespDto));
            // 封装xdtz0034DataResultDto中正确的返回码和返回信息
            xdtz0034DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0034DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, e.getMessage());
            // 封装xdtz0034DataResultDto中异常返回码和返回信息
            xdtz0034DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0034DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0034DataRespDto到xdtz0034DataResultDto中
        xdtz0034DataResultDto.setData(xdtz0034DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, JSON.toJSONString(xdtz0034DataResultDto));
        return xdtz0034DataResultDto;
    }
}
