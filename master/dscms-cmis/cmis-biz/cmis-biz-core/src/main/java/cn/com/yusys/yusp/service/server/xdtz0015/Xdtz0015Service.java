package cn.com.yusys.yusp.service.server.xdtz0015;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccDisc;
import cn.com.yusys.yusp.domain.CtrDiscCont;
import cn.com.yusys.yusp.domain.CtrHighAmtAgrCont;
import cn.com.yusys.yusp.domain.IqpDiscApp;
import cn.com.yusys.yusp.dto.CfgSorgFinaDto;
import cn.com.yusys.yusp.dto.QueryAreaInfoDto;
import cn.com.yusys.yusp.dto.server.xdtz0015.req.Xdtz0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0015.resp.Xdtz0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccDiscMapper;
import cn.com.yusys.yusp.repository.mapper.CtrHighAmtAgrContMapper;
import cn.com.yusys.yusp.repository.mapper.IqpDiscAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.CtrDiscContService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import net.bytebuddy.asm.Advice;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdtz0015Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-08 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdtz0015Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0015Service.class);

    @Resource
    private AccDiscMapper accDiscMapper;

    @Autowired
    private CtrDiscContService ctrDiscContService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private IqpDiscAppMapper iqpDiscAppMapper;

    @Autowired
    private CtrHighAmtAgrContMapper ctrHighAmtAgrContMapper;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * 贴现记账结果通知
     *
     * @param xdtz0015DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0015DataRespDto getXdtz0015(Xdtz0015DataReqDto xdtz0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, JSON.toJSONString(xdtz0015DataReqDto));
        Xdtz0015DataRespDto xdtz0015DataRespDto = new Xdtz0015DataRespDto();

        try {
            //根据票号查询是否存在该贴现借据
            String drftNo = xdtz0015DataReqDto.getPorderNo();
            Map param = new HashMap<>();
            param.put("drftNo", drftNo);
            int num = accDiscMapper.selectCountFromAccDiscBybillNo(param);
            if (num > 0) {
                throw BizException.error(null, EcbEnum.ECB010016.key, EcbEnum.ECB010016.value);
            }
            AccDisc accDisc = new AccDisc();
            String pkId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());
            accDisc.setPkId(pkId);
            // 出账流水号
            String pvpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
            accDisc.setPvpSerno(pvpSerno);
            accDisc.setIsSxet(xdtz0015DataReqDto.getIsSxet());//是否省心E帖
            //贴现协议编号
            accDisc.setContNo(xdtz0015DataReqDto.getDiscAgrNo());
            // 如果 通过贴现协议编号查询贴现协议信息
            if(!StringUtils.isBlank(xdtz0015DataReqDto.getDiscAgrNo())){
                CtrDiscCont ctrDiscCont = ctrDiscContService.selectByContNo(xdtz0015DataReqDto.getDiscAgrNo());
                if(ctrDiscCont != null){
                    accDisc.setCusId(ctrDiscCont.getCusId());
                    accDisc.setCusName(ctrDiscCont.getCusName());
                    accDisc.setPrdTypeProp(ctrDiscCont.getPrdTypeProp());
                    accDisc.setIsUtilLmt(ctrDiscCont.getIsUtilLmt());
                    accDisc.setLmtAccNo(ctrDiscCont.getLmtAccNo());
                    accDisc.setReplyNo(ctrDiscCont.getAcptCrpReplyNo());
                    accDisc.setInputId(ctrDiscCont.getInputId());//登记人
                    accDisc.setInputBrId(ctrDiscCont.getInputBrId());//登记机构
                    accDisc.setUpdId(ctrDiscCont.getUpdId());//最近修改人
                    accDisc.setUpdBrId(ctrDiscCont.getUpdBrId());//最近修改机构
                    accDisc.setManagerBrId(ctrDiscCont.getManagerBrId());//管户机构
                    accDisc.setManagerId(ctrDiscCont.getManagerId());//客户经理
                    accDisc.setReplyNo(ctrDiscCont.getReplyNo());//批复编号
                    accDisc.setGuarMode(ctrDiscCont.getGuarMode());//担保方式
                } else {
                    CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContMapper.selectDataByContNo(xdtz0015DataReqDto.getDiscAgrNo());
                    if (Objects.nonNull(ctrHighAmtAgrCont)) {
                        accDisc.setCusId(ctrHighAmtAgrCont.getCusId());
                        accDisc.setCusName(ctrHighAmtAgrCont.getCusName());
                        accDisc.setIsUtilLmt(ctrHighAmtAgrCont.getIsUtilLmt());
                        accDisc.setLmtAccNo(ctrHighAmtAgrCont.getLmtAccNo());
                        accDisc.setReplyNo(ctrHighAmtAgrCont.getReplyNo());
                        accDisc.setInputId(ctrHighAmtAgrCont.getInputId());//登记人
                        accDisc.setInputBrId(ctrHighAmtAgrCont.getInputBrId());//登记机构
                        accDisc.setUpdId(ctrHighAmtAgrCont.getUpdId());//最近修改人
                        accDisc.setUpdBrId(ctrHighAmtAgrCont.getUpdBrId());//最近修改机构
                        accDisc.setManagerBrId(ctrHighAmtAgrCont.getManagerBrId());//管户机构
                        accDisc.setManagerId(ctrHighAmtAgrCont.getManagerId());//客户经理
                        accDisc.setReplyNo(ctrHighAmtAgrCont.getReplyNo());//批复编号
                        accDisc.setGuarMode(ctrHighAmtAgrCont.getGuarMode());//担保方式
                    }
                }
            }

            accDisc.setUpdDate(LocalDate.now().toString());//最近修改日期

            //持票人结算账号
            accDisc.setDrfthldSettlAccno(xdtz0015DataReqDto.getDrftHldSettlAcctNo());
            //持票人结算账户户名
            accDisc.setDrfthldSettlAcctName(xdtz0015DataReqDto.getDrftHldSettlAcctName());
            //贴现日期
            accDisc.setDscntDate(xdtz0015DataReqDto.getDiscDate());
            //票据类型映射新老信贷迁移
            String drftType = xdtz0015DataReqDto.getDrftType();
            //产品编号
            String prdId = "";
            //产品名称
            String prdName = "";
            //是否电子票据
            String isEDrft = "";
            if (Objects.equals(drftType, CommonConstance.DRFT_TYPE_1)) {
                prdId = CommonConstance.STD_PJ_PRD_ID_052198;
                prdName = CommonConstance.STD_PJ_PRD_NAME_052198;
                isEDrft = "0";
                accDisc.setDrftType(CommonConstance.DRFT_TYPE_1);
            } else if (Objects.equals(drftType, CommonConstance.DRFT_TYPE_2)) {
                prdId = CommonConstance.STD_PJ_PRD_ID_052199;
                prdName = CommonConstance.STD_PJ_PRD_NAME_052199;
                isEDrft = "0";
                accDisc.setDrftType(CommonConstance.DRFT_TYPE_2);
            } else if (Objects.equals(drftType, CommonConstance.DRFT_TYPE_3)) {
                prdId = CommonConstance.STD_PJ_PRD_ID_052198;
                prdName = CommonConstance.STD_PJ_PRD_NAME_052198;
                isEDrft = "1";
                accDisc.setDrftType(CommonConstance.DRFT_TYPE_1);
            } else if (Objects.equals(drftType, CommonConstance.DRFT_TYPE_4)) {
                prdId = CommonConstance.STD_PJ_PRD_ID_052199;
                prdName = CommonConstance.STD_PJ_PRD_NAME_052199;
                isEDrft = "1";
                accDisc.setDrftType(CommonConstance.DRFT_TYPE_2);
            }
            accDisc.setPrdId(prdId);
            accDisc.setPrdName(prdName);
            accDisc.setIsEDrft(isEDrft);
            //贴现类型 默认直贴
            accDisc.setDscntType("1");
            //币种
            accDisc.setCurType(xdtz0015DataReqDto.getCurType());
            //票面总金额
            accDisc.setDrftAmt(xdtz0015DataReqDto.getDrftTotlAmt());
            //贴现总利息
            accDisc.setDscntInt(xdtz0015DataReqDto.getDiscTotlInt());
            //贴现金额
            if (Objects.nonNull(xdtz0015DataReqDto.getDrftTotlAmt()) && Objects.nonNull(xdtz0015DataReqDto.getDiscTotlInt())) {
                accDisc.setRpayDscntAmt(xdtz0015DataReqDto.getDrftTotlAmt().subtract(xdtz0015DataReqDto.getDiscTotlInt()));
            }
            //机构号
            String orgNo = xdtz0015DataReqDto.getOrgNo();
            if (StringUtils.isNotBlank(orgNo)) {
                StringBuffer sf = new StringBuffer(orgNo);
                accDisc.setFinaBrId(sf.substring(0, 2) + "9801");//账务机构编号
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("finaBrNo", accDisc.getFinaBrId());
                logger.info("根据机构编号获取机构名称开始，参数为：{}", accDisc.getFinaBrId());
                ResultDto<List<CfgSorgFinaDto>> listResultDto = iCmisCfgClientService.selecSorgFina(queryModel);
                logger.info("根据机构编号获取机构名称结束，返回为：{}", JSON.toJSONString(listResultDto));
                if (Objects.nonNull(listResultDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, listResultDto.getCode())
                        && CollectionUtils.nonEmpty(listResultDto.getData())) {
                    CfgSorgFinaDto cfgSorgFinaDto = JSONObject.parseObject(JSON.toJSONString(listResultDto.getData().get(0)), CfgSorgFinaDto.class);
                    accDisc.setFinaBrIdName(cfgSorgFinaDto.getFinaBrName());
                }
            }
            //管户客户经理
            accDisc.setManagerId(xdtz0015DataReqDto.getManagerId());
            accDisc.setAccStatus("1");//台账状态 默认正常
            //借据号
            accDisc.setBillNo(xdtz0015DataReqDto.getPorderNo());
            //汇票号码
            accDisc.setPorderNo(xdtz0015DataReqDto.getPorderNo());
            //票据签发日期
            accDisc.setDraftStartDate(xdtz0015DataReqDto.getDrftSignDate());
            //票据签发地（本地、异地）
            accDisc.setDraftSignAddr(xdtz0015DataReqDto.getDrftSignArea());
            //贴现日期
            accDisc.setDscntDay(xdtz0015DataReqDto.getDiscDay());
            //票据到期日
            accDisc.setDraftEndDate(xdtz0015DataReqDto.getDiscEndDate());
            //执行年利率
            if (Objects.nonNull(xdtz0015DataReqDto.getRealityIrY())) {
                accDisc.setDscntRateYear(xdtz0015DataReqDto.getRealityIrY().divide(new BigDecimal(100)));
            }
            //出票人姓名
            accDisc.setDrwrName(xdtz0015DataReqDto.getDrwrName());
            //出票人开户行账号
            accDisc.setDrwrAccno(xdtz0015DataReqDto.getDrwrAcctbAcctNo());
            //出票人开户行行名
            accDisc.setDrwrAcctsvcrnm(xdtz0015DataReqDto.getDrwrAcctbName());
            //出票人开户行行号
            accDisc.setDrwrAcctsvcrNo(xdtz0015DataReqDto.getDrwrAcctbNo());
            //承兑人名称
            accDisc.setAorgNo(xdtz0015DataReqDto.getAccptrAcctbAcctNo());
            // TODO  承兑人开户行账号accptrAcctbAcctNo 插入逻辑
            //收款人名称
            accDisc.setPyeeName(xdtz0015DataReqDto.getPyeeName());
            //收款人开户行行号
            accDisc.setPyeeAcctsvcrNo(xdtz0015DataReqDto.getPyeeAcctbNo());
            //收款人开户行行名
            accDisc.setPyeeAcctsvcrName(xdtz0015DataReqDto.getPyeeAcctbName());
            //收款人开户行账号
            accDisc.setPyeeAccno(xdtz0015DataReqDto.getPyeeAcctbAcctNo());
            //承兑行类型
            accDisc.setAorgType(xdtz0015DataReqDto.getAorgType());
            //承兑行行号
            accDisc.setAorgNo(xdtz0015DataReqDto.getAorgTypeNo());
            //承兑行名称
            accDisc.setAorgName(xdtz0015DataReqDto.getAccptrName());
            accDisc.setFiveClass("10");//五级分类
            accDisc.setTenClass("11");//十级分类
            accDisc.setClassDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//分类日期
            //操作类型
            accDisc.setOprType(CommonConstance.OPR_TYPE_ADD);
            int flag = accDiscMapper.insertSelective(accDisc);
            if (flag < 0) {
                xdtz0015DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
                xdtz0015DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            } else {
                xdtz0015DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);
                xdtz0015DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, JSON.toJSONString(xdtz0015DataRespDto));
        return xdtz0015DataRespDto;
    }

}
