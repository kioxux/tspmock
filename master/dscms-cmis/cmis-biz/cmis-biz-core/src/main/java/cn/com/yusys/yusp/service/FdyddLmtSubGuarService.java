/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.SfResultInfo;
import cn.com.yusys.yusp.domain.VisaXdRisk;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.FdyddLmtSubGuar;
import cn.com.yusys.yusp.repository.mapper.FdyddLmtSubGuarMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FdyddLmtSubGuarService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-17 11:02:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class FdyddLmtSubGuarService {
    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppService.class);
    @Autowired
    private FdyddLmtSubGuarMapper fdyddLmtSubGuarMapper;
    @Resource
    private SequenceTemplateClient sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public FdyddLmtSubGuar selectByPrimaryKey(String pkId) {
        return fdyddLmtSubGuarMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectBySubSerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public FdyddLmtSubGuar selectBySubSerno(String serno) {
        return fdyddLmtSubGuarMapper.selectBySubSerno(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<FdyddLmtSubGuar> selectAll(QueryModel model) {
        List<FdyddLmtSubGuar> records = (List<FdyddLmtSubGuar>) fdyddLmtSubGuarMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<FdyddLmtSubGuar> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<FdyddLmtSubGuar> list = fdyddLmtSubGuarMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(FdyddLmtSubGuar record) {
        return fdyddLmtSubGuarMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(FdyddLmtSubGuar record) {
        return fdyddLmtSubGuarMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(FdyddLmtSubGuar record) {
        return fdyddLmtSubGuarMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(FdyddLmtSubGuar record) {
        return fdyddLmtSubGuarMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelectiveBySubSerno
     * @方法描述: 根据分享流水更新 - 只更新非空字段
     * @创建者 ：zhangliang15
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelectiveBySubSerno(FdyddLmtSubGuar record) {
        return fdyddLmtSubGuarMapper.updateSelectiveBySubSerno(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return fdyddLmtSubGuarMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return fdyddLmtSubGuarMapper.deleteByIds(ids);
    }

    /**

     * @方法名称: fdyddLmtSubGuarlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<FdyddLmtSubGuar> fdyddLmtSubGuarlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        return fdyddLmtSubGuarMapper.selectByModel(model);
    }

    /**

     * @方法名称: fdyddLmtSubGuarlistHislist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<FdyddLmtSubGuar> fdyddLmtSubGuarHislist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        return fdyddLmtSubGuarMapper.selectByModel(model);
    }

    /**
     * @方法名称: insertFdyddLmtSubGuar
     * @方法描述: 房抵e点贷授信押品关联新增
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang
     * @创建时间: 2021-08-05 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map insertFdyddLmtSubGuar(FdyddLmtSubGuar record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String pkId = "";
        String cusId = "";
        try {
            // 获取创建，修改日期
            record.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            // 生成新流水
            pkId = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            record.setPkId(pkId);
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            int count = fdyddLmtSubGuarMapper.insert(record);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",房抵e点贷授信押品关联新增失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存房抵e点贷授信押品关联数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("pkId", pkId);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }
}
