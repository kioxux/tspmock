/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constant.CoopPlanAppConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestChgReplyMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.Comm4CalFormulaUtils;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestChgReplyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 14:20:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestChgReplyService extends BizInvestCommonService{

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtSigInvestChgReplyService.class);

    @Autowired
    private LmtSigInvestChgReplyMapper lmtSigInvestChgReplyMapper;
    @Autowired
    private LmtSigInvestAccService lmtSigInvestAccService;
    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtSigInvestRstService lmtSigInvestRstService;

    @Autowired
    private LmtSigInvestBasicInfoSubAccService lmtSigInvestBasicInfoSubAccService;

    @Autowired
    private LmtSigInvestBasicInfoSubService lmtSigInvestBasicInfoSubService;

    @Autowired
    private LmtSigInvestBasicLmtAccService lmtSigInvestBasicLmtAccService;

    @Autowired
    private LmtSigInvestBasicInfoAccService lmtSigInvestBasicInfoAccService ;

    @Autowired
    private LmtSigInvestBasicInfoService lmtSigInvestBasicInfoService;

    @Autowired
    private LmtSigInvestBasicLimitAppService lmtSigInvestBasicLimitAppService;

    @Autowired
    private LmtSigInvestBasicInfoRstService lmtSigInvestBasicInfoRstService;

    @Autowired
    private LmtSigInvestBasicLmtRstService lmtSigInvestBasicLmtRstService;

    @Autowired
    private LmtSigInvestBasicInfoSubRstService lmtSigInvestBasicInfoSubRstService;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate ;

    @Autowired
    private Comm4CalFormulaUtils comm4CalFormulaUtils ;
    @Autowired
    private MessageSendService  messageSendService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    MessageCommonService sendMessage;

    @Autowired
    private LmtSigInvestSubAppService lmtSigInvestSubAppService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestChgReply selectByPrimaryKey(String serno) {
        return lmtSigInvestChgReplyMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestChgReply> selectAll(QueryModel model) {
        List<LmtSigInvestChgReply> records = (List<LmtSigInvestChgReply>) lmtSigInvestChgReplyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestChgReply> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestChgReply> list = lmtSigInvestChgReplyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestChgReply record) {
        return lmtSigInvestChgReplyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestChgReply record) {
        return lmtSigInvestChgReplyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestChgReply record) {
        return lmtSigInvestChgReplyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestChgReply record) {
        return lmtSigInvestChgReplyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return lmtSigInvestChgReplyMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestChgReplyMapper.deleteByIds(ids);
    }

    /**
     * 查询变更信息
     * @param model
     * @return
     */
    public List<LmtSigInvestChgReply> selectByCondition(QueryModel model){
        return  lmtSigInvestChgReplyMapper.selectByCondition(model);
    }

    /**
     * 查询历史变更信息
     * @param model
     * @return
     */
    public List<LmtSigInvestChgReply> selectHisByCondition(QueryModel model){
        return  lmtSigInvestChgReplyMapper.selectHisByCondition(model);
    }

    /**
     * 新增批复变更
     * @param map
     * @return
     */
    public Map<String,Object> insertReplyChg(Map<String,Object> map){
        String replySerno = map.get("replySerno").toString();

        //存在在途的授信业务，不允许发起
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno",replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestChgReply> lmtSigInvestChgReplyList = selectByModel(queryModel);
        if (!CollectionUtils.isEmpty(lmtSigInvestChgReplyList)){
            boolean present = lmtSigInvestChgReplyList.stream().filter(a ->
                    CmisBizConstants.APPLY_STATE_APP.equals(a.getApproveStatus())
                            || CmisBizConstants.APPLY_STATE_TODO.equals(a.getApproveStatus())
                            || CmisBizConstants.APPLY_STATE_CALL_BACK.equals(a.getApproveStatus()))
                    .findFirst().isPresent();
            if (present){
                throw BizException.error(null,EclEnum.LMT_SIG_INVESTAPP_EORROR000027.key,EclEnum.LMT_SIG_INVESTAPP_EORROR000027.value);
            }
        }

        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.selectByReplySerno(replySerno);

        if (lmtSigInvestAcc == null){
            throw BizException.error(null,EclEnum.ECL070077.key,EclEnum.ECL070077.value);
        }

        LmtSigInvestChgReply lmtSigInvestChgReply = BeanUtils.beanCopy(lmtSigInvestAcc,LmtSigInvestChgReply.class);
        String serno = generateSerno(CmisCommonConstants.LMT_CHG_SERNO);
        lmtSigInvestChgReply.setSerno(serno);
        lmtSigInvestChgReply.setReplySerno(replySerno);
        lmtSigInvestChgReply.setReplyStatus(lmtSigInvestAcc.getAccStatus());
        lmtSigInvestChgReply.setApproveStatus(CoopPlanAppConstant.APPR_STATUS_START);
        if (StringUtils.nonBlank(lmtSigInvestAcc.getApprResult())){
            lmtSigInvestChgReply.setApprResult(lmtSigInvestAcc.getApprResult());
        }else{
            lmtSigInvestChgReply.setApprResult(CmisCommonConstants.commonSignMap.get("O-12"));
        }
        lmtSigInvestChgReply.setCreateTime(getCurrrentDate());
        //获取债项评级信息
        lmtSigInvestChgReply.setDebtEvalDate(lmtSigInvestAcc.getDebtEvalDate());
        lmtSigInvestChgReply.setDebtEvalResult(lmtSigInvestAcc.getDebtEvalResult());
        lmtSigInvestChgReply.setDebtEvalOrgName(lmtSigInvestAcc.getDebtEvalOrgName());
        lmtSigInvestChgReply.setCurType(CmisBizConstants.STD_ZB_CUR_TYP_CNY);
        //产品类型
        lmtSigInvestChgReply.setPrdType(lmtSigInvestAcc.getLmtBizType());
        //标准化投资类型
        lmtSigInvestChgReply.setNormalInvestType(lmtSigInvestAcc.getInvestType());

        //初始化（更新）通用domain信息
        initInsertDomainProperties(lmtSigInvestChgReply);
        lmtSigInvestChgReplyMapper.insertSelective(lmtSigInvestChgReply);

        Map<String, Object> result =new HashMap<>();
        result.put("serno",serno);
        return result;
    }

    /**
     * 逻辑删除
     * 1.判断当前申请状态是否为未发起 逻辑删除
     * 2.判断当前申请状态是否为退回，修改为自行退出
     */
    @Transactional
    public  int logicalDelete(LmtSigInvestChgReply lmtSigInvestChgReply){
        log.info("批复变更申请记录删除开始。。。。");
        //1.判断当前申请状态是否为未发起 逻辑删除
        if (CmisBizConstants.APPLY_STATE_TODO.equals(lmtSigInvestChgReply.getApproveStatus())){
            lmtSigInvestChgReply.setOprType(CommonConstance.OPR_TYPE_DELETE);
            return lmtSigInvestChgReplyMapper.logicalDelete(lmtSigInvestChgReply);
        }
        //2.判断当前申请状态是否为退回，修改为自行退出
        if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(lmtSigInvestChgReply.getApproveStatus())){
            //流程退回删除
            log.info("批复变更申请流程删除=》BizId：{}",lmtSigInvestChgReply.getSerno());
            workflowCoreClient.deleteByBizId(lmtSigInvestChgReply.getSerno());
            lmtSigInvestChgReply.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            return lmtSigInvestChgReplyMapper.updateByPrimaryKey(lmtSigInvestChgReply);
        }
        log.info("批复变更申请记录删除结束。。。。");
        return 0;
    }

    /**
     * 获取单笔投资授信批复变更记录
     * @param serno
     * @return
     */
    public LmtSigInvestChgReply selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestChgReply> lmtSigInvestChgReplies = selectAll(queryModel);
        if (lmtSigInvestChgReplies!=null && lmtSigInvestChgReplies.size()>0){
            return lmtSigInvestChgReplies.get(0);
        }
        return null;
    }

    /**
     * 更新批复审批状态
     * @param lmtSigInvestChgReply
     * @param approveStatus
     * @param currentUserId
     * @param currentOrgId
     */
    public void updateReplyChgStatus(LmtSigInvestChgReply lmtSigInvestChgReply, String approveStatus, String currentUserId, String currentOrgId) {
        //初始化（更新）通用domain信息
        initUpdateDomainProperties(lmtSigInvestChgReply,currentUserId,currentOrgId);
        //TODO 是否更换approveStatus
        lmtSigInvestChgReply.setApproveStatus(approveStatus);
        update(lmtSigInvestChgReply);
    }

    /**
     * 资金业务授信批复变更审批流程(通过、否决 录入批复表中)
     * @param lmtSigInvestChgReply
     * @param currentUserId
     * @param currentOrgId
     * @param approveStatus
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleAfterEnd(LmtSigInvestChgReply lmtSigInvestChgReply, String currentUserId, String currentOrgId, String approveStatus) throws Exception  {
        log.info("批复变更流程审批处理结束开始.....");
        //保存批复变更状态
        lmtSigInvestChgReply.setApproveStatus(approveStatus);
        //初始化更新属性信息
        update(lmtSigInvestChgReply);

        if(CmisBizConstants.APPLY_STATE_PASS.equals(approveStatus)){
            this.flowPassHandleAfterEnd(lmtSigInvestChgReply, currentUserId, currentOrgId) ;
        }
        log.info("批复变更流程审批处理结束开始.....");
    }

    /**
     * 审批否决生成批复数据不更新台账
     * @param lmtSigInvestChgReply
     * @param currentUserId
     * @param currentOrgId
     */
    private void flowRefuseHandleAfterEnd(LmtSigInvestChgReply lmtSigInvestChgReply, String currentUserId, String currentOrgId) throws Exception{
        //生成批复变更批复信息
        LmtSigInvestRst lmtSigInvestRst = lmtSigInvestRstService.initLmtSigInvestRstInfoByChgReply(lmtSigInvestChgReply);
        lmtSigInvestRstService.insert(lmtSigInvestRst);

        String serno = lmtSigInvestChgReply.getSerno();
        //旧的批复信息-批复流水号
        String oldReplySerno = lmtSigInvestChgReply.getReplySerno();
        //新的批复信息-批复流水号
        String newReplySerno = lmtSigInvestRst.getReplySerno() ;
        //获取原台账信息
        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.selectByReplySerno(oldReplySerno);
        if (lmtSigInvestAcc == null ){
            throw new Exception(EclEnum.ECL070072.value) ;
        }
        //原台帐关联申请流水号
        String oldSerno = lmtSigInvestAcc.getSerno();

        //更新
        /**生成批复单笔投资对应底层资产基本情况批复表	lmt_sig_invest_basic_info_sub_rst 单笔投资关联底层信息明细台账表	lmt_sig_invest_basic_info_sub_acc **/
        //单笔投资关联底层信息明细申请表	lmt_sig_invest_basic_info_sub
        List<LmtSigInvestBasicInfoSub> lmtSigInvestBasicInfoSubList = lmtSigInvestBasicInfoSubService.selectBySerno(serno);
        if(lmtSigInvestBasicInfoSubList!=null && lmtSigInvestBasicInfoSubList.size()>0){
            for (LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub : lmtSigInvestBasicInfoSubList) {
                //添加批复表数据
                lmtSigInvestBasicInfoSubRstService.insertBasicInfoSubRst(lmtSigInvestBasicInfoSub,oldSerno,newReplySerno,currentOrgId,currentUserId);
            }
        }

        /**生成批复单笔投资对应底层资产基本情况批复表	lmt_sig_invest_basic_info_rst  **/
        //获取审批或者申请中的 单笔投资对应底层资产基本情况 信息 LmtSigInvestBasicInfoAppr
        LmtSigInvestBasicInfo lmtSigInvestBasicInfo = lmtSigInvestBasicInfoService.selectBySerno(serno);
        if(lmtSigInvestBasicInfo != null){
            //添加批复表数据
            lmtSigInvestBasicInfoRstService.insertBasicInfoRst(lmtSigInvestBasicInfo,oldSerno,newReplySerno,currentOrgId,currentUserId);
        }

        /**底层授信额度批复表	lmt_sig_invest_basic_lmt_rst **/
        //获取审批或申请中 底层授信额度批复表 信息 初始化批复底层授信额度批复表
        List<LmtSigInvestBasicLimitApp> lmtSigInvestBasicLimitApps = lmtSigInvestBasicLimitAppService.selectBySerno(serno) ;
        if(lmtSigInvestBasicLimitApps!=null && lmtSigInvestBasicLimitApps.size()>0){
            for (LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp : lmtSigInvestBasicLimitApps) {
                //添加批复表数据
                lmtSigInvestBasicLmtRstService.insertBasicLmtRst(lmtSigInvestBasicLimitApp,oldSerno,newReplySerno,currentOrgId,currentUserId);
            }
        }

    }

    /**
     * 审批通过生成批复和台账表
     * 批复变更 （台账表、批复表）
     * 1.serno使用原来申请表的serno不采用批复变更表中的serno
     * 2.replySerno 采用最新的批复变更表中的replySerno
     * @param lmtSigInvestChgReply
     * @param currentUserId
     * @param currentOrgId
     */
    private void flowPassHandleAfterEnd(LmtSigInvestChgReply lmtSigInvestChgReply, String currentUserId, String currentOrgId) throws Exception{
        String replySerno = lmtSigInvestChgReply.getReplySerno();
        if(StringUtils.isBlank(replySerno)){
            throw new Exception("批复流水号获取异常！");
        }

        BigDecimal lmtAmt = lmtSigInvestChgReply.getLmtAmt();

        //更新批复结果表
        LmtSigInvestRst lmtSigInvestRst = lmtSigInvestRstService.selectByReplySerno(replySerno);
        if(lmtSigInvestRst==null){
            throw new Exception("批复信息获取异常！");
        }
        lmtSigInvestRst.setLmtAmt(lmtSigInvestChgReply.getLmtAmt());
        lmtSigInvestRst.setApprResult(CmisCommonConstants.commonSignMap.get("0-12"));
        lmtSigInvestRst.setUpdDate(DateUtils.getCurrDateStr());
        lmtSigInvestRst.setUpdateTime(DateUtils.getCurrTimestamp());
        lmtSigInvestRstService.update(lmtSigInvestRst);

        //更新台账
        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.selectByReplySerno(replySerno);
        lmtSigInvestAcc.setLmtAmt(lmtAmt);
        lmtSigInvestAcc.setUpdDate(DateUtils.getCurrDateStr());
        lmtSigInvestAcc.setUpdateTime(DateUtils.getCurrTimestamp());
        lmtSigInvestRst.setApprResult(CmisCommonConstants.commonSignMap.get("0-12"));
        lmtSigInvestAccService.update(lmtSigInvestAcc);


        LmtSigInvestSubApp lmtSigInvestSubApp = lmtSigInvestSubAppService.selectBySerno(lmtSigInvestRst.getSerno());
        if (lmtSigInvestSubApp != null &&
                (lmtSigInvestSubApp.getPrdTotalAmt() == null || BigDecimal.ZERO.compareTo(lmtSigInvestSubApp.getPrdTotalAmt()) == 0 )){
            throw BizException.error(null,"9999","项目总金额获取失败");
        }

        //判断是否有占用底层信息，并更新底层授信金额
        List<LmtSigInvestBasicLmtRst> LmtSigInvestBasicLmtRsts = lmtSigInvestBasicLmtRstService.selectByReplySerno(replySerno);
        if(LmtSigInvestBasicLmtRsts!=null && LmtSigInvestBasicLmtRsts.size()>0){
            for(LmtSigInvestBasicLmtRst basicRst : LmtSigInvestBasicLmtRsts){

                if(StringUtils.isBlank(basicRst.getBasicReplySerno())){
                    throw new Exception("底层批复流水号获取异常！");
                }
                QueryModel model = new QueryModel();
                model.addCondition("basicReplySerno",basicRst.getBasicReplySerno());
                BizInvestCommonService.checkParamsIsNull("basicReplySerno",basicRst.getBasicReplySerno());
                model.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
                List<LmtSigInvestBasicInfoSubRst> subRst = lmtSigInvestBasicInfoSubRstService.selectByModel(model);
                LmtSigInvestBasicInfoSubRst lmtSigInvestBasicInfoSubRst = null;
                if(subRst!=null && subRst.size()>0){
                    lmtSigInvestBasicInfoSubRst = subRst.get(0);
                }else{
                    throw new Exception("底层批复结果获取异常！");
                }

                //BigDecimal a = lmtSigInvestBasicInfoSubRst.getBasicAssetBalanceAmt().multiply(lmtAmt);
                //basicRst.setLmtAmt(a.divide(lmtSigInvestRst.getIntendActualIssuedScale()));
                BigDecimal useAmt = comm4CalFormulaUtils.getPrdLmtUseAmt(lmtSigInvestBasicInfoSubRst.getBasicAssetBalanceAmt(),
                        lmtAmt, lmtSigInvestSubApp.getPrdTotalAmt());
                basicRst.setLmtAmt(useAmt);
                basicRst.setUpdDate(DateUtils.getCurrDateStr());
                basicRst.setUpdateTime(DateUtils.getCurrTimestamp());
                lmtSigInvestBasicLmtRstService.update(basicRst);

                //更新底层台账
                List<LmtSigInvestBasicLmtAcc> basicAccs =  lmtSigInvestBasicLmtAccService.selectByModel(model);
                LmtSigInvestBasicLmtAcc basicAcc = null;
                if(basicAccs!=null && basicAccs.size()>0){
                    basicAcc = basicAccs.get(0);
                }else{
                    throw new Exception("底层批复台账获取异常！");
                }

                basicAcc.setLmtAmt(basicRst.getLmtAmt());

                basicAcc.setUpdDate(DateUtils.getCurrDateStr());
                basicAcc.setUpdateTime(DateUtils.getCurrTimestamp());
                lmtSigInvestBasicLmtAccService.update(basicAcc);
            }
        }

        //发送额度
        lmtSigInvestAccService.lmtSigInvestSendCmisLmt0005(lmtSigInvestAcc.getSerno(),CmisBizConstants.STD_SX_LMT_TYPE_02,lmtSigInvestAcc.getAccNo());
    }

    /**
     * @作者:lizx
     * @方法名称: sendWbMsgNotice
     * @方法描述:  推送首页提醒事项
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/1 15:36
     * @param lmtSigInvestChgReply:
     * @param messageType:
     * @param comment:
     * @param inputId:
     * @param inputBrId:
     * @return: void
     * @算法描述: 无
    */
    @Transactional(rollbackFor=Exception.class)
    public void sendWbMsgNotice( LmtSigInvestChgReply lmtSigInvestChgReply,String messageType,String comment,String inputId,String inputBrId,String result) throws Exception {
        log.info("资金同业批复变更【{}】处理通知首页提醒事项处理...", lmtSigInvestChgReply.getSerno());

        Map<String, String> map = new HashMap<>();
        map.put("cusName", lmtSigInvestChgReply.getCusName());
        map.put("prdName", "主体授信/产品授信批复变更");
        map.put("result", result);
        ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(lmtSigInvestChgReply.getManagerId());
        sendMessage.sendMessage("MSG_ZJ_M_0001",map,"1",lmtSigInvestChgReply.getManagerId(),byLoginCode.getData().getUserMobilephone());
    }
}
