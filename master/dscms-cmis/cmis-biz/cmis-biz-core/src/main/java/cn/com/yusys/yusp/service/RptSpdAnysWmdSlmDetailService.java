/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptSpdAnysWmdSlmDetail;
import cn.com.yusys.yusp.repository.mapper.RptSpdAnysWmdSlmDetailMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysWmdSlmDetailService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-13 21:54:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptSpdAnysWmdSlmDetailService {

    @Autowired
    private RptSpdAnysWmdSlmDetailMapper rptSpdAnysWmdSlmDetailMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptSpdAnysWmdSlmDetail selectByPrimaryKey(String pkId) {
        return rptSpdAnysWmdSlmDetailMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptSpdAnysWmdSlmDetail> selectAll(QueryModel model) {
        List<RptSpdAnysWmdSlmDetail> records = (List<RptSpdAnysWmdSlmDetail>) rptSpdAnysWmdSlmDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptSpdAnysWmdSlmDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptSpdAnysWmdSlmDetail> list = rptSpdAnysWmdSlmDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptSpdAnysWmdSlmDetail record) {
        return rptSpdAnysWmdSlmDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptSpdAnysWmdSlmDetail record) {
        return rptSpdAnysWmdSlmDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptSpdAnysWmdSlmDetail record) {
        return rptSpdAnysWmdSlmDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptSpdAnysWmdSlmDetail record) {
        return rptSpdAnysWmdSlmDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptSpdAnysWmdSlmDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * 保存
     * @param rptSpdAnysWmdSlmDetail
     * @return
     */
    public int save(RptSpdAnysWmdSlmDetail rptSpdAnysWmdSlmDetail){
        String pkId = rptSpdAnysWmdSlmDetail.getPkId();
        if(StringUtils.nonBlank(pkId)){
            return update(rptSpdAnysWmdSlmDetail);
        }else {
            rptSpdAnysWmdSlmDetail.setPkId(StringUtils.getUUID());
            return insert(rptSpdAnysWmdSlmDetail);
        }
    }
}
