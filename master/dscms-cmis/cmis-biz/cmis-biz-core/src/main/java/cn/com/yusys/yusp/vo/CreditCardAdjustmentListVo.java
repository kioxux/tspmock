package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

/**
 * 批量额度申请导入模板对象
 *
 * @author zhangsm
 * @since 2021-06-06
 */
@ExcelCsv(namePrefix = "批量额度申请导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class CreditCardAdjustmentListVo {

	/** 卡号 **/
	@ExcelField(title = "卡号", viewLength = 40)
	private String cardNo;

	/** 卡产品 **/
	@ExcelField(title = "卡产品", dictCode = "STD_CARD_APPLY_CARD_PRD", viewLength = 20)
	private String cardPrd;

	/** 客户姓名 **/
	@ExcelField(title = "客户姓名", viewLength = 40)
	private String cusName;

	/** 证件类型 **/
	@ExcelField(title = "证件类型", dictCode = "STD_ZB_CERT_TYP",viewLength = 30)
	private String certType;

	/** 证件号码 **/
	@ExcelField(title = "证件号码", viewLength = 40)
	private String certCode;

	/** 原始信用额度 **/
	@ExcelField(title = "原始信用额度", viewLength = 25)
	private BigDecimal origCreditCardLmt;

	/** 新信用额度 **/
	@ExcelField(title = "新信用额度", viewLength = 16)
	private BigDecimal newCreditCardLmt;

	/** 是否提供增信证明 **/
	@ExcelField(title = "是否提供增信证明", viewLength = 30)
	private String isIncrease;

	/** 备注 **/
	@ExcelField(title = "备注", viewLength = 50)
	private String remarks;

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardPrd() {
		return cardPrd;
	}

	public void setCardPrd(String cardPrd) {
		this.cardPrd = cardPrd;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertCode() {
		return certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public BigDecimal getOrigCreditCardLmt() {
		return origCreditCardLmt;
	}

	public void setOrigCreditCardLmt(BigDecimal origCreditCardLmt) {
		this.origCreditCardLmt = origCreditCardLmt;
	}

	public BigDecimal getNewCreditCardLmt() {
		return newCreditCardLmt;
	}

	public void setNewCreditCardLmt(BigDecimal newCreditCardLmt) {
		this.newCreditCardLmt = newCreditCardLmt;
	}

	public String getIsIncrease() {
		return isIncrease;
	}

	public void setIsIncrease(String isIncrease) {
		this.isIncrease = isIncrease;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}