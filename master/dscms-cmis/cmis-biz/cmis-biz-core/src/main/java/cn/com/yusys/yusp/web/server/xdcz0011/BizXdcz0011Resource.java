package cn.com.yusys.yusp.web.server.xdcz0011;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0011.req.Xdcz0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0011.resp.List;
import cn.com.yusys.yusp.dto.server.xdcz0011.resp.Xdcz0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:出账记录详情查看
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0011:出账记录详情查看")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0011.BizXdcz0011Resource.class);

    /**
     * 交易码：xdcz0011
     * 交易描述：出账记录详情查看
     *
     * @param xdcz0011DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("出账记录详情查看")
    @PostMapping("/xdcz0011")
    protected @ResponseBody
    ResultDto<Xdcz0011DataRespDto> xdcz0011(@Validated @RequestBody Xdcz0011DataReqDto xdcz0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0011.key, DscmsEnum.TRADE_CODE_XDCZ0011.value, JSON.toJSONString(xdcz0011DataReqDto));
        Xdcz0011DataRespDto xdcz0011DataRespDto = new Xdcz0011DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0011DataRespDto> xdcz0011DataResultDto = new ResultDto<>();
        String cusId = xdcz0011DataReqDto.getCusId();
        String startPageNum = xdcz0011DataReqDto.getStartPageNum();
        String pageSize = xdcz0011DataReqDto.getPageSize();
        try {
            // 从xdcz0011DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdcz0011DataRespDto对象开始
            List list = new List();
//             list.setContNo();//合同号
//             list.setCusId();//客户号
//             list.setCusName();//客户名
//             list.setBillNo();//借据名
//             list.setLoanEndDate();//借款到期日
//             list.setLoanUseType();//借款用途
//             list.setContAmt();//合同金额
//             list.setPreferSurplusTimes();//优会剩余次数
//             list.setPreferPoints();//优惠点数
//             list.setLoanAmt();//借据金额
//             list.setRepayAcctNo();//还款账号
//             list.setAcctName();//账户名称
//             list.setReplacePayoutMon();//代发月份
//             list.setIndivSalAMt();//工资总金额
//             list.setRealityIrY();//执行利率
//             list.setAmtUcase();//金额大写
//             list.setLoan();//放款时间

            xdcz0011DataRespDto.setList(null);
            xdcz0011DataRespDto.setTotalQnt(StringUtils.EMPTY);
            // TODO 封装xdcz0011DataRespDto对象结束
            // 封装xdcz0011DataResultDto中正确的返回码和返回信息
            xdcz0011DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0011DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0011.key, DscmsEnum.TRADE_CODE_XDCZ0011.value, e.getMessage());
            // 封装xdcz0011DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdcz0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0011DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdcz0011DataRespDto到xdcz0011DataResultDto中
        xdcz0011DataResultDto.setData(xdcz0011DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0011.key, DscmsEnum.TRADE_CODE_XDCZ0011.value, JSON.toJSONString(xdcz0011DataRespDto));
        return xdcz0011DataResultDto;
    }
}
