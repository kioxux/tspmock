/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LcApplSellInfo
 * @类描述: lc_appl_sell_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:19:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lc_appl_sell_info")
public class LcApplSellInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 人员类型 STD_ZB_MGR_USR_KIND **/
	@Column(name = "USR_KIND", unique = false, nullable = true, length = 5)
	private String usrKind;
	
	/** 营销（管户）人员姓名 **/
	@Column(name = "USR_NAME", unique = false, nullable = true, length = 100)
	private String usrName;
	
	/** 营销（管户）人员编号 **/
	@Column(name = "USR_CODE", unique = false, nullable = true, length = 32)
	private String usrCode;
	
	/** 业务系数 **/
	@Column(name = "BS_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bsRate;
	
	/** 责任认定系数 **/
	@Column(name = "RES_BS_RATE", unique = false, nullable = true, length = 9)
	private java.math.BigDecimal resBsRate;
	
	/** 登记时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 20)
	private String createTime;
	
	/** 登记人 **/
	@Column(name = "CREATE_USR", unique = false, nullable = true, length = 20)
	private String createUsr;
	
	/** 最后修改时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 状态  STD_ZB_DATA_STS **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 5)
	private String status;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 10)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param usrKind
	 */
	public void setUsrKind(String usrKind) {
		this.usrKind = usrKind;
	}
	
    /**
     * @return usrKind
     */
	public String getUsrKind() {
		return this.usrKind;
	}
	
	/**
	 * @param usrName
	 */
	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}
	
    /**
     * @return usrName
     */
	public String getUsrName() {
		return this.usrName;
	}
	
	/**
	 * @param usrCode
	 */
	public void setUsrCode(String usrCode) {
		this.usrCode = usrCode;
	}
	
    /**
     * @return usrCode
     */
	public String getUsrCode() {
		return this.usrCode;
	}
	
	/**
	 * @param bsRate
	 */
	public void setBsRate(java.math.BigDecimal bsRate) {
		this.bsRate = bsRate;
	}
	
    /**
     * @return bsRate
     */
	public java.math.BigDecimal getBsRate() {
		return this.bsRate;
	}
	
	/**
	 * @param resBsRate
	 */
	public void setResBsRate(java.math.BigDecimal resBsRate) {
		this.resBsRate = resBsRate;
	}
	
    /**
     * @return resBsRate
     */
	public java.math.BigDecimal getResBsRate() {
		return this.resBsRate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public String getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param createUsr
	 */
	public void setCreateUsr(String createUsr) {
		this.createUsr = createUsr;
	}
	
    /**
     * @return createUsr
     */
	public String getCreateUsr() {
		return this.createUsr;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}