/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IndustryBsinsIncomeTop;
import cn.com.yusys.yusp.repository.mapper.IndustryBsinsIncomeTopMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IndustryBsinsIncomeTopService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-31 19:31:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IndustryBsinsIncomeTopService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(IndustryBsinsIncomeTopService.class);

    @Resource
    private IndustryBsinsIncomeTopMapper industryBsinsIncomeTopMapper;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private ICusClientService iCusClientService;// 客户服务

    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;// 参数服务
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IndustryBsinsIncomeTop selectByPrimaryKey(String pkId) {
        return industryBsinsIncomeTopMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IndustryBsinsIncomeTop> selectAll(QueryModel model) {
        List<IndustryBsinsIncomeTop> records = (List<IndustryBsinsIncomeTop>) industryBsinsIncomeTopMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IndustryBsinsIncomeTop> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IndustryBsinsIncomeTop> list = industryBsinsIncomeTopMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IndustryBsinsIncomeTop record) {
        return industryBsinsIncomeTopMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IndustryBsinsIncomeTop record) {
        return industryBsinsIncomeTopMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IndustryBsinsIncomeTop record) {
        return industryBsinsIncomeTopMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IndustryBsinsIncomeTop record) {
        return industryBsinsIncomeTopMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return industryBsinsIncomeTopMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return industryBsinsIncomeTopMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:selectIndustryBsinsIncomeTopDataBySerno
     * @函数描述:通用列表查询
     * @参数与返回说明:
     * @param map 分页查询类
     * @算法描述:
     */

    public List<Map> selectIndustryBsinsIncomeTopDataBySerno(Map map) {
        List<Map> oldList = new ArrayList<>();
        List<Map> returnList = new ArrayList<>();
        try{
            if((String)map.get("serno") != null && !"".equals((String)map.get("serno"))){
                log.info("获取当前授信申请对应的客户信息>>>>>>>");// TRADE_CLASS
                LmtApp lmtApp = lmtAppService.selectBySerno((String)map.get("serno"));
                ResultDto<CusCorpDto> cusCorpDto = iCusClientService.queryCusCropDtoByCusId(lmtApp.getCusId());
                log.info("客户信息:"+JSON.toJSONString(cusCorpDto));// TRADE_CLASS
                if(cusCorpDto.getData() != null && cusCorpDto.getData().getTradeClass() != null && !"".equals(cusCorpDto.getData().getTradeClass())) {
                    map.put("tradeClass",cusCorpDto.getData().getTradeClass());
                    oldList = industryBsinsIncomeTopMapper.selectIndustryBsinsIncomeTopDataBySerno(map);
                }else{
                    log.info("该客户行业信息获取失败!map:", JSON.toJSONString(cusCorpDto));
                    throw BizException.error(null, EcbEnum.ECB010060.key,EcbEnum.ECB010060.value);
                }
                log.info("客户同行业信息（不包含行业名称）:"+JSON.toJSONString(oldList));
                // 获取行业名称
                for(Map map1 : oldList ){
                    AdminSmTreeDicDto adminSmTreeDicDto = new AdminSmTreeDicDto();
                    adminSmTreeDicDto.setOptType("STD_ZB_TRADE_CLASS");
                    adminSmTreeDicDto.setEnName((String) map1.get("tradeClass"));
                    AdminSmTreeDicDto adminSmTreeDicDtoResultDto = dscmsCfgClientService.queryCfgTreeByCode(adminSmTreeDicDto).getData();
                    map1.put("tradeClassName",adminSmTreeDicDtoResultDto.getCnName());
                    returnList.add(map1);
                }
                log.info("客户同行业信息（包含行业名称）:"+JSON.toJSONString(oldList));
            }else{
                log.info("获取map中授信申请流水号异常!map:", JSON.toJSONString(map));
                throw BizException.error(null, EcbEnum.ECB010001.key,EcbEnum.ECB010001.value);
            }
        }catch(Exception e){
            log.info("系统异常!", JSON.toJSONString(e));
            throw BizException.error(null, EcbEnum.ECB019999.key,e.getMessage());
        }
        return returnList;
    }
}
