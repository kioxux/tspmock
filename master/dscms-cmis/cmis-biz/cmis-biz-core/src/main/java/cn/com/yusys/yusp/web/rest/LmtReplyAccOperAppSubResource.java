/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplyAccOperAppSub;
import cn.com.yusys.yusp.service.LmtReplyAccOperAppSubService;

import javax.management.Query;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccOperAppSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:27:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplyaccoperappsub")
public class LmtReplyAccOperAppSubResource {
    @Autowired
    private LmtReplyAccOperAppSubService lmtReplyAccOperAppSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyAccOperAppSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyAccOperAppSub> list = lmtReplyAccOperAppSubService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyAccOperAppSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyAccOperAppSub>> index(QueryModel queryModel) {
        List<LmtReplyAccOperAppSub> list = lmtReplyAccOperAppSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyAccOperAppSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyAccOperAppSub> show(@PathVariable("pkId") String pkId) {
        LmtReplyAccOperAppSub lmtReplyAccOperAppSub = lmtReplyAccOperAppSubService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyAccOperAppSub>(lmtReplyAccOperAppSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplyAccOperAppSub> create(@RequestBody LmtReplyAccOperAppSub lmtReplyAccOperAppSub) throws URISyntaxException {
        lmtReplyAccOperAppSubService.insert(lmtReplyAccOperAppSub);
        return new ResultDto<LmtReplyAccOperAppSub>(lmtReplyAccOperAppSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyAccOperAppSub lmtReplyAccOperAppSub) throws URISyntaxException {
        int result = lmtReplyAccOperAppSubService.update(lmtReplyAccOperAppSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyAccOperAppSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyAccOperAppSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getSubAndPrd
     * @函数描述:单一客户额度冻结解冻获取当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @创建人: yangwl
     * @创建时间: 2021-5-20
     * @算法描述:
     */
    @PostMapping("/getsubandprd")
    protected ResultDto<Map> getSubAndPrd(@RequestBody String serno) {
        Map map = lmtReplyAccOperAppSubService.getSubAndPrd(serno);
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:getsubandprdbygrpSerno
     * @函数描述:集团客户额度冻结解冻获取当前授信申请对应的授信分项及授信分项下的适用授信品种
     * @参数与返回说明:
     * @创建人: yangwl
     * @创建时间: 2021-5-20
     * @算法描述:
     */
    @PostMapping("/getsubandprdbygrpserno")
    protected ResultDto<Map> getSubandPrdByGrpSerno(@RequestBody String grpSerno) {
        Map map = lmtReplyAccOperAppSubService.getSubandPrdByGrpSerno(grpSerno);
        return new ResultDto<>(map);
    }
}
