package cn.com.yusys.yusp.web.server.xdsx0026;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0026.req.ListEntl;
import cn.com.yusys.yusp.dto.server.xdsx0026.req.ListHouse;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdsx0026.Xdsx0026Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdsx0026.req.Xdsx0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0026.resp.Xdsx0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 接口处理类:风控推送面签信息至信贷系统
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0026:风控推送面签信息至信贷系统")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0026Resource.class);

    @Autowired
    private Xdsx0026Service xdsx0026Service;
    /**
     * 交易码：xdsx0026
     * 交易描述：风控推送面签信息至信贷系统
     *
     * @param xdsx0026DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控推送面签信息至信贷系统")
    @PostMapping("/xdsx0026")
    protected @ResponseBody
    ResultDto<Xdsx0026DataRespDto> xdsx0026(@Validated @RequestBody Xdsx0026DataReqDto xdsx0026DataReqDto) throws BizException, Exception  {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, JSON.toJSONString(xdsx0026DataReqDto));
        Xdsx0026DataRespDto xdsx0026DataRespDto = new Xdsx0026DataRespDto();// 响应Dto:风控推送面签信息至信贷系统
        ResultDto<Xdsx0026DataRespDto> xdsx0026DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, JSON.toJSONString(xdsx0026DataReqDto));
            xdsx0026DataRespDto = xdsx0026Service.getXdsx0026(xdsx0026DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, JSON.toJSONString(xdsx0026DataRespDto));
            // 封装xdsx0026DataResultDto中正确的返回码和返回信息
            xdsx0026DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0026DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, e.getMessage());
            xdsx0026DataResultDto.setCode(e.getErrorCode());
            xdsx0026DataResultDto.setMessage(e.getMessage());
        }  catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, e.getMessage());
            // 封装xdsx0026DataResultDto中异常返回码和返回信息
            xdsx0026DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0026DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0026DataRespDto到xdsx0026DataResultDto中
        xdsx0026DataResultDto.setData(xdsx0026DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, JSON.toJSONString(xdsx0026DataResultDto));
        return xdsx0026DataResultDto;
    }
}
