/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GrtGuarContRel;
import cn.com.yusys.yusp.dto.GrtGuarContClientDto;
import cn.com.yusys.yusp.dto.GrtGuarContRelDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarContRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-19 14:00:54
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface GrtGuarContRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    GrtGuarContRel selectByPrimaryKey(@Param("pkId") String pkId);


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GrtGuarContRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(GrtGuarContRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(GrtGuarContRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(GrtGuarContRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(GrtGuarContRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */


    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 校验担保合同与押品关系数据是否存在
     * @param grtGuarContRel
     * @return
     */
    GrtGuarContRel selectByGrtGuarContRel(GrtGuarContRel grtGuarContRel);

    /**
     * 通过担保合同编号查询担保合同与押品关系数据
     * @param guarMap
     * @return
     */
    List<GrtGuarContRel> selectGGCRList(Map guarMap);


    /**
     * 通过借款人编号查询担保合同与押品关系数据
     * @param
     * @return
     */
    List<GrtGuarContRel> selectByBorrowerId(Map params);

    /**
     * 查询所有引用担保的押品
     * @param
     * @return
     */
    List<GrtGuarContRel> getGrtGuarContRel(@Param("oprType") String oprType);

    /**
     * 根据担保合同编号查询数量
     * @param guarContNo
     * @return
     */
    int getCountByGuarNo(@Param("guarContNo")String guarContNo);

    /**
     * 根据担保合同编号查询合同编号
     * @param guarContNo
     * @return
     */
    String getContNoByGuarContNo(String guarContNo);

    /**
     * @方法名称：getByGuarContNo
     * @方法描述：根据担保合同编号查询担保合同和担保人、物的关系
     * @创建人：zhangming12
     * @创建时间：2021/5/18 13:46
     * @修改记录：修改时间 修改人员 修改时间
    */
    List<GrtGuarContRel> getByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * @函数名称:selectGrtGuarContRelLinkGuarBaseInfo
     * @函数描述:查询担保合同和押品基本信息列表通过关系表关联
     * @参数与返回说明:
     * @算法描述:
     */
    List<GrtGuarContRelDto> selectGrtGuarContRelLinkGuarBaseInfo(QueryModel model);
    /**
     * @函数名称:insertGrtGuarContRel
     * @函数描述:批量新增
     * @参数与返回说明:
     * @算法描述:
     */
    int insertGrtGuarContRelList(@Param("list")List<GrtGuarContRel> grtGuarContRelList);

    /**
     * @函数名称:deleteGrtGuarContRelByGuarContNo
     * @函数描述:根据担保合同编号关联删除担保合同下押品关联
     * @参数与返回说明:guarContNo 担保合同编号
     * @算法描述:
     */
    int deleteGrtGuarContRelByGuarContNo(String guarContNo);


    /**
     * @函数名称:deleteGrtGuarContRelByGuarNo
     * @函数描述：逻辑删除grtGuarContRel
     * @参数与返回说明:
     * @算法描述:
     */
    int deleteGrtGuarContRelByGuarNo(@Param("guarNo") String guarNo);

    /**
     * 根据担保合同编号查询押品统一编号
     * @param guarContNo 担保合同号
     * @return 保证人编号
     */
    List<GrtGuarContRel> getGuarNoByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * @函数名称:selectDetailByGuarContNo
     * @函数描述:通过担保合同编号查询担保合同和押品基本信息列表通过关系表关联
     * @参数与返回说明:
     * @算法描述:
     */
    GrtGuarContRel selectDetailByGuarContNo(String guarContNo);
    /**
     * @创建人 WH
     * @创建时间 2021/7/12 19:01
     * @注释 小微授信查询押品关系
     */
    List<GrtGuarContRel> selectForXw(String serno);
    /**
     * @创建人 XS
     * @创建时间 2021/7/12 19:01
     * @注释 查询是否已经质押
     */
    List<String> selectByGuarCopyNoList(@Param("list") List<String> guarCopyNoList);

    /**
     * 根据押品编号查询关联的担保合同不是解除状态的记录数
     * @param guarNo
     * @return
     */
    int selectNoRelieveRecordsByGuarNo(@Param("guarNo") String guarNo);

    int deleteByAssetNo(@Param("assetNo")String assetNo);

    /**
     * 根据担保合同编号把关联的押品的记录的状态改为失效
     * @param guarContNo
     * @return
     */
    int updateStatusInvalidByGuarContNo(@Param("guarContNo") String guarContNo);

    String selectGuarNosByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * 更新权利价值字段
     * @param queryModel
     * @return
     */
    int updateCertiAmt(QueryModel queryModel);

    /**
     * 根据合同编号查询关联的押品
     * @param contNos
     * @return
     */
    String selectGuarNosByContNos(@Param("contNos") String contNos);

    /**
     * 根据保证人id查询关联的担保合同编号
     * @param guarantyId
     * @return
     */
    String selectGuarContNosByGuarantyId(@Param("guarantyId") String guarantyId);

    /**
     * 根据担保合同号查询押品号
     * @param guarContNo
     * @return
     */
    List<String> selectGuarNoByGuarContNo(@Param("guarContNo") String guarContNo);

    String selectAorgInfoByGuarContNo(@Param("guarContNo") String guarContNo);
}
