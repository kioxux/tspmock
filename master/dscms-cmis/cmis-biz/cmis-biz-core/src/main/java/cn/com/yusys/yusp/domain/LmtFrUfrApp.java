/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFrUfrApp
 * @类描述: lmt_fr_ufr_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-30 16:38:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_fr_ufr_app")
public class LmtFrUfrApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 全局流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;
	
	/** 授信申请流水号 **/
	@Column(name = "lmt_serno", unique = false, nullable = false, length = 40)
	private String lmtSerno;
	
	/** 冻结/解冻流水号 **/
	@Column(name = "fr_ufr_serno", unique = false, nullable = false, length = 40)
	private String frUfrSerno;
	
	/** 客户编号 **/
	@Column(name = "cus_id", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 授信协议编号 **/
	@Column(name = "lmt_ctr_no", unique = false, nullable = false, length = 40)
	private String lmtCtrNo;
	
	/** 申请类型 STD_ZB_OPT_MODE **/
	@Column(name = "app_type", unique = false, nullable = true, length = 5)
	private String appType;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "cur_type", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 授信总额度 **/
	@Column(name = "lmt_totl_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtTotlAmt;
	
	/** 循环额度 **/
	@Column(name = "revolv_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal revolvAmt;
	
	/** 一次性额度 **/
	@Column(name = "one_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oneAmt;
	
	/** 低风险业务类型  STD_ZB_LOW_RISK_TYP **/
	@Column(name = "low_risk_type", unique = false, nullable = true, length = 5)
	private String lowRiskType;
	
	/** 授信起始日期 **/
	@Column(name = "lmt_star_date", unique = false, nullable = true, length = 10)
	private String lmtStarDate;
	
	/** 授信到期日期 **/
	@Column(name = "lmt_end_date", unique = false, nullable = true, length = 10)
	private String lmtEndDate;
	
	/** 授信已冻结额度 **/
	@Column(name = "lmt_outstnd_froze_amt", unique = false, nullable = true, length = 8)
	private BigDecimal lmtOutstndFrozeAmt;
	
	/** 本次冻结额度 **/
	@Column(name = "lmt_froze_amt", unique = false, nullable = true, length = 8)
	private BigDecimal lmtFrozeAmt;
	
	/** 本次解冻额度 **/
	@Column(name = "lmt_unfroze_amt", unique = false, nullable = true, length = 8)
	private BigDecimal lmtUnfrozeAmt;
	
	/** 冻结原因 **/
	@Column(name = "froze_resn", unique = false, nullable = true, length = 2000)
	private String frozeResn;
	
	/** 解冻原因 **/
	@Column(name = "unfroze_resn", unique = false, nullable = true, length = 2000)
	private String unfrozeResn;
	
	/** 主办人 **/
	@Column(name = "manager_id", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "manager_br_id", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 申请状态 STD_ZB_APP_STATUS **/
	@Column(name = "approve_status", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 最后修改人 **/
	@Column(name = "upd_id", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "upd_br_id", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改时间 **/
	@Column(name = "upd_date", unique = false, nullable = true, length = 20)
	private String updDate;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno;
	}
	
    /**
     * @return lmtSerno
     */
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param frUfrSerno
	 */
	public void setFrUfrSerno(String frUfrSerno) {
		this.frUfrSerno = frUfrSerno;
	}
	
    /**
     * @return frUfrSerno
     */
	public String getFrUfrSerno() {
		return this.frUfrSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo;
	}
	
    /**
     * @return lmtCtrNo
     */
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}
	
	/**
	 * @param appType
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}
	
    /**
     * @return appType
     */
	public String getAppType() {
		return this.appType;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param lmtTotlAmt
	 */
	public void setLmtTotlAmt(java.math.BigDecimal lmtTotlAmt) {
		this.lmtTotlAmt = lmtTotlAmt;
	}
	
    /**
     * @return lmtTotlAmt
     */
	public java.math.BigDecimal getLmtTotlAmt() {
		return this.lmtTotlAmt;
	}
	
	/**
	 * @param revolvAmt
	 */
	public void setRevolvAmt(java.math.BigDecimal revolvAmt) {
		this.revolvAmt = revolvAmt;
	}
	
    /**
     * @return revolvAmt
     */
	public java.math.BigDecimal getRevolvAmt() {
		return this.revolvAmt;
	}
	
	/**
	 * @param oneAmt
	 */
	public void setOneAmt(java.math.BigDecimal oneAmt) {
		this.oneAmt = oneAmt;
	}
	
    /**
     * @return oneAmt
     */
	public java.math.BigDecimal getOneAmt() {
		return this.oneAmt;
	}
	
	/**
	 * @param lowRiskType
	 */
	public void setLowRiskType(String lowRiskType) {
		this.lowRiskType = lowRiskType;
	}
	
    /**
     * @return lowRiskType
     */
	public String getLowRiskType() {
		return this.lowRiskType;
	}
	
	/**
	 * @param lmtStarDate
	 */
	public void setLmtStarDate(String lmtStarDate) {
		this.lmtStarDate = lmtStarDate;
	}
	
    /**
     * @return lmtStarDate
     */
	public String getLmtStarDate() {
		return this.lmtStarDate;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate;
	}
	
    /**
     * @return lmtEndDate
     */
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param lmtOutstndFrozeAmt
	 */
	public void setLmtOutstndFrozeAmt(BigDecimal lmtOutstndFrozeAmt) {
		this.lmtOutstndFrozeAmt = lmtOutstndFrozeAmt;
	}
	
    /**
     * @return lmtOutstndFrozeAmt
     */
	public BigDecimal getLmtOutstndFrozeAmt() {
		return this.lmtOutstndFrozeAmt;
	}
	
	/**
	 * @param lmtFrozeAmt
	 */
	public void setLmtFrozeAmt(BigDecimal lmtFrozeAmt) {
		this.lmtFrozeAmt = lmtFrozeAmt;
	}
	
    /**
     * @return lmtFrozeAmt
     */
	public BigDecimal getLmtFrozeAmt() {
		return this.lmtFrozeAmt;
	}
	
	/**
	 * @param lmtUnfrozeAmt
	 */
	public void setLmtUnfrozeAmt(BigDecimal lmtUnfrozeAmt) {
		this.lmtUnfrozeAmt = lmtUnfrozeAmt;
	}
	
    /**
     * @return lmtUnfrozeAmt
     */
	public BigDecimal getLmtUnfrozeAmt() {
		return this.lmtUnfrozeAmt;
	}
	
	/**
	 * @param frozeResn
	 */
	public void setFrozeResn(String frozeResn) {
		this.frozeResn = frozeResn;
	}
	
    /**
     * @return frozeResn
     */
	public String getFrozeResn() {
		return this.frozeResn;
	}
	
	/**
	 * @param unfrozeResn
	 */
	public void setUnfrozeResn(String unfrozeResn) {
		this.unfrozeResn = unfrozeResn;
	}
	
    /**
     * @return unfrozeResn
     */
	public String getUnfrozeResn() {
		return this.unfrozeResn;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}


}