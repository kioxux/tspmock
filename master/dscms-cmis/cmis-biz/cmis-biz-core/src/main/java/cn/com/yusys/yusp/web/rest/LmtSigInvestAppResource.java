/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.service.LmtSigInvestAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-17 16:02:36
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestapp")
public class LmtSigInvestAppResource {
    private static final Logger log = LoggerFactory.getLogger(LmtSigInvestAppResource.class);
    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;

    @Value("${yusp.file-server.home-path}")
    private String serverPath;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestApp> list = lmtSigInvestAppService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtSigInvestApp>> index(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort(" serno desc");
        }
        List<LmtSigInvestApp> list = lmtSigInvestAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByModelSubject
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByModelSubject")
    protected ResultDto<List<LmtSigInvestApp>> selectByModelSubject(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort(" serno desc");
        }
        List<LmtSigInvestApp> list = lmtSigInvestAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByModelSubjectHis
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByModelSubjectHis")
    protected ResultDto<List<LmtSigInvestApp>> selectByModelSubjectHis(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort(" serno desc");
        }
        List<LmtSigInvestApp> list = lmtSigInvestAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByModelProduct
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByModelProduct")
    protected ResultDto<List<LmtSigInvestApp>> selectByModelProduct(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort("serno desc");
        }
        List<LmtSigInvestApp> list = lmtSigInvestAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByModelProductHis
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByModelProductHis")
    protected ResultDto<List<LmtSigInvestApp>> selectByModelProductHis(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort(" serno desc");
        }
        List<LmtSigInvestApp> list = lmtSigInvestAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByModelSubject
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByModelSubjectCha")
    protected ResultDto<List<LmtSigInvestApp>> selectByModelSubjectCha(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort(" serno desc");
        }
        List<LmtSigInvestApp> list = lmtSigInvestAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByModelSubjectHis
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByModelSubjectChaHis")
    protected ResultDto<List<LmtSigInvestApp>> selectByModelSubjectChaHis(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort(" serno desc");
        }
        List<LmtSigInvestApp> list = lmtSigInvestAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByModelProduct
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByModelProductCha")
    protected ResultDto<List<LmtSigInvestApp>> selectByModelProductCha(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort("serno desc");
        }
        List<LmtSigInvestApp> list = lmtSigInvestAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByModelProductHis
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByModelProductChaHis")
    protected ResultDto<List<LmtSigInvestApp>> selectByModelProductChaHis(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort(" serno desc");
        }
        List<LmtSigInvestApp> list = lmtSigInvestAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestApp> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestApp lmtSigInvestApp = lmtSigInvestAppService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestApp>(lmtSigInvestApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestApp> create(@RequestBody LmtSigInvestApp lmtSigInvestApp) throws URISyntaxException {
        lmtSigInvestAppService.insert(lmtSigInvestApp);
        return new ResultDto<LmtSigInvestApp>(lmtSigInvestApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestApp lmtSigInvestApp) throws URISyntaxException {
        int result = lmtSigInvestAppService.update(lmtSigInvestApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtSigInvestApp> selectBySerno(@RequestBody Map condition) {
        String serno = (String) condition.get("serno");
        LmtSigInvestApp lmtSigInvestApp = lmtSigInvestAppService.selectBySerno(serno);
        return new ResultDto<LmtSigInvestApp>(lmtSigInvestApp);
    }

    /**
     * 逻辑删除
     *
     * @return
     */
    @PostMapping("/deleteLogicBySerno")
    protected ResultDto<Integer> deleteLogicBySerno(@RequestBody Map condition) {
        String serno = (String) condition.get("serno");
        int result = lmtSigInvestAppService.deleteLogicBySerno(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:addProjectBasicInfo
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addProjectBasicInfo")
    protected ResultDto<Map<String, Object>> addProjectBasicInfo(@RequestBody Map info) throws URISyntaxException, ParseException {
        Map result = lmtSigInvestAppService.addProjectBasicInfo(info);
        return new ResultDto<Map<String, Object>>(result);
    }

    /**
     * 更新修改
     *
     * @param info
     * @return
     */
    @PostMapping("/updateProjectBasicInfo")
    protected ResultDto<Integer> updateProjectBasicInfo(@RequestBody Map info) {
        Integer result = lmtSigInvestAppService.updateProjectBasicInfo(info);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据pkid 更新非空字段
     *
     * @param lmtSigInvestApp
     * @return
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody LmtSigInvestApp lmtSigInvestApp) {
        int result = lmtSigInvestAppService.updateByKeyAndVal(lmtSigInvestApp);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:onReconside
     * @函数描述:单笔投资授信复议申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/onreconside")
    protected ResultDto<LmtSigInvestApp> onReconside(@RequestBody LmtSigInvestApp lmtSigInvestApp) throws ParseException {
        ResultDto<LmtSigInvestApp> resultDto = lmtSigInvestAppService.onReconside(lmtSigInvestApp);
        return resultDto;
    }

    /**
     * 授信续作
     *
     * @param lmtSigInvestAcc
     * @return
     */
    @PostMapping("/onxuzuo")
    protected ResultDto<LmtSigInvestApp> onXuzuo(@RequestBody Map lmtSigInvestAcc) throws ParseException {
        //待完善
        LmtSigInvestApp newApp = lmtSigInvestAppService.onXuzuo(lmtSigInvestAcc);
        return new ResultDto<LmtSigInvestApp>(newApp);
    }

    /**
     * 除法保留4位小数
     *
     * @param condition
     * @return
     */
    @PostMapping("/calculate")
    protected ResultDto<BigDecimal> onDivide(@RequestBody Map condition) {
        BigDecimal result = null;
        try {
            String type = (String) condition.get("type");
            BigDecimal num1 = new BigDecimal(String.valueOf(condition.get("num1")));
            BigDecimal num2 = new BigDecimal(String.valueOf(condition.get("num2")));
            result = null;
            if ("divide".equals(type)) {
                result = num1.divide(num2, 4, BigDecimal.ROUND_CEILING);
            }
            if ("add".equals(type)) {
                result = num1.add(num2);
            }
            if ("multi".equals(type)) {
                result = num1.multiply(num2);
            }
            if ("sub".equals(type)) {
                result = num1.subtract(num2);
            }
            System.out.println("result===>" + result);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            //计算错误
            throw BizException.error(null, EclEnum.LMT_CALCULATE_PARAMS_ERROR.key, EclEnum.LMT_CALCULATE_PARAMS_ERROR.value);
        }
        return new ResultDto<BigDecimal>(result);
    }

    /**
     * 存量业务tabs显示状态
     * 1.法人-集团成员 显示（机构归属集团存量授信）页签
     *
     * @return
     */
    @PostMapping("/cunLiangYeWuTabsStatus")
    protected ResultDto<Map> cunLiangYeWuTabsStatus(@RequestBody Map condition) {
        Map result = lmtSigInvestAppService.cunLiangYeWuTabsStatus(condition);
        return new ResultDto<>(result);
    }

    /**
     * 授信变更
     *
     * @return
     */
    @PostMapping("/oninvestchange")
    protected ResultDto<LmtSigInvestApp> onInvestChange(@RequestBody Map lmtSigInvestAcc) throws ParseException {
        LmtSigInvestApp lmtSigInvestApp = lmtSigInvestAppService.onInvestChange(lmtSigInvestAcc);
        return new ResultDto<LmtSigInvestApp>(lmtSigInvestApp);
    }

    /**
     * 图片
     *
     * @param condition
     * @return
     */
    @PostMapping("/updatePicAbsoultPath")
    protected ResultDto<String> updateFilePath(@RequestBody Map condition) {
        condition.put("serverPath", serverPath);
        String result = lmtSigInvestAppService.updatePicAbsoultPath(condition);
        return new ResultDto<>(result);
    }

    /**
     * 获取大额授信相关信息
     *
     * @param lmtSigInvestApp
     * @return
     */
    @PostMapping("/getLargeLmtInfo")
    protected ResultDto<Map> getLargeLmtInfo(@RequestBody LmtSigInvestApp lmtSigInvestApp) {
        Map result = lmtSigInvestAppService.getLargeLmtInfo(lmtSigInvestApp);
        return new ResultDto<>(result);
    }

    /**
     * 更新总体调查结论
     * @param lmtSigInvestApp
     * @return
     */
    @PostMapping("/updateZtdcjl")
    protected ResultDto<Integer> updateZtdcjl(@RequestBody LmtSigInvestApp lmtSigInvestApp){
        int result = lmtSigInvestAppService.updateZtdcjl(lmtSigInvestApp);
        return new ResultDto<>(result);
    }


    /**
     * 补录资产编号
     * @param condition
     * @return
     */
    @PostMapping("/changeAssetNo")
    protected ResultDto<Integer> changeAssetNo(@RequestBody Map condition){
        int result = lmtSigInvestAppService.changeAssetNo(condition);
        return new ResultDto<>(result);
    }
}
