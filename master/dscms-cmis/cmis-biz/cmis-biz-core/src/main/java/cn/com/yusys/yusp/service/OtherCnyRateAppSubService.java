/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.cmis.commons.exception.CmisContextException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.OtherCnyRateAppSub;
import cn.com.yusys.yusp.domain.OtherCnyRateLoanAppSub;
import cn.com.yusys.yusp.repository.mapper.OtherCnyRateAppSubMapper;
import cn.com.yusys.yusp.repository.mapper.OtherCnyRateLoanAppSubMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateAppSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-03 17:22:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherCnyRateAppSubService {

    @Autowired
    private OtherCnyRateAppSubMapper otherCnyRateAppSubMapper;
    
    @Autowired
    private OtherCnyRateLoanAppSubMapper otherCnyRateLoanAppSubMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OtherCnyRateAppSub selectByPrimaryKey(String subSerno) {
        return otherCnyRateAppSubMapper.selectByPrimaryKey(subSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherCnyRateAppSub> selectAll(QueryModel model) {
        List<OtherCnyRateAppSub> records = (List<OtherCnyRateAppSub>) otherCnyRateAppSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OtherCnyRateAppSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherCnyRateAppSub> list = otherCnyRateAppSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OtherCnyRateAppSub record) {
        return otherCnyRateAppSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OtherCnyRateAppSub record) {
        return otherCnyRateAppSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OtherCnyRateAppSub record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return otherCnyRateAppSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OtherCnyRateAppSub record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return otherCnyRateAppSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String subSerno) {
        return otherCnyRateAppSubMapper.deleteByPrimaryKey(subSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherCnyRateAppSubMapper.deleteByIds(ids);
    }
    
    /**
     * 逻辑删除人民币权限表及关联表
     * @param subSerno
     * @return
     */
    public int updateAll(String subSerno) {
    	//一年期流贷信息表
    	QueryModel queryModel = new QueryModel();
    	queryModel.addCondition("subSerno", subSerno);
    	List<OtherCnyRateLoanAppSub> loanAppSubList = otherCnyRateLoanAppSubMapper.selectByModel(queryModel);
    	if (!CollectionUtils.isEmpty(loanAppSubList)) {
    		loanAppSubList.stream().forEach(loanAppSub -> {
    			loanAppSub.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
    			otherCnyRateLoanAppSubMapper.updateByPrimaryKeySelective(loanAppSub);
    		});
    	}
    	OtherCnyRateAppSub otherCnyRateAppSub = otherCnyRateAppSubMapper.selectByPrimaryKey(subSerno);
    	otherCnyRateAppSub.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
    	return otherCnyRateAppSubMapper.updateByPrimaryKeySelective(otherCnyRateAppSub);
    }
}
