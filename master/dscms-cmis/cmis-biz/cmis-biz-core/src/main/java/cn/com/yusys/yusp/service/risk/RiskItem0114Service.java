package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.req.CmisCus0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.resp.CmisCus0012RespDto;
import cn.com.yusys.yusp.repository.mapper.LmtReplyAccMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.CtrCvrgContService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Service
public class RiskItem0114Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0114Service.class);

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Resource
    private LmtReplyAccMapper lmtReplyAccMapper;

    @Resource
    private CtrCvrgContService ctrCvrgContService;

    @Resource
    private ICmisCfgClientService iCmisCfgClientService;
    /**
     * @方法名称: riskItem0114
     * @方法描述: 统一授信管控校验
     * @参数与返回说明:
     * @算法描述:
     * 1、借款人为个人时，校验其本人、配偶、成年子女及本人、配偶、成年子女作为高管（法人代表、实际控制人、出资人）的企业，在我行是否有存量授信，如有则提示。
     * 2、借款人为企业时，校验其法人代表及其配偶、实际控制人及其配偶及成年子女、出资人在我行是否有存量经营性业务，同时校验上述人员担任高管的企业在我行是否有存量经营性业务，如有则提示
     * @创建人: yfs
     * @创建时间: 2021-09-07 09:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0114(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        log.info("统一授信管控校验开始*******************业务流水号：【{}】，客户号：【{}】",serno,cusId);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0021); //获取客户信息失败
            return riskResultDto;
        }
        // 获取客户模块中的客户基本信息
        ResultDto<CusBaseDto> resultDto = cmisCusClientService.cusBaseInfo(cusId);
        if (Objects.isNull(resultDto) || Objects.isNull(resultDto.getData())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
            return riskResultDto;
        }
        CusBaseDto cusBaseDtoDo = resultDto.getData();
        String cusCatalog = cusBaseDtoDo.getCusCatalog();
        // 个人客户
        if (Objects.equals("1", cusCatalog)) {
            riskResultDto = isExitsOperationalBus(cusId);
            if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                return riskResultDto;
            }
        } else {
            QueryModel model = new QueryModel();
            model.getCondition().put("mrgTypes", "200400,201200,201700");
            model.getCondition().put("cusIdRel", cusId);
            model.getCondition().put("oprType", "01");
            ResultDto<List<CusCorpMgrDto>> cusCorpMgrDtoResult = cmisCusClientService.queryCusCorpMgr(model);
            if(Objects.isNull(cusCorpMgrDtoResult) || CollectionUtils.isEmpty(cusCorpMgrDtoResult.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11404);
                return riskResultDto;
            }
            for(Object map : cusCorpMgrDtoResult.getData()) {
                CusCorpMgrDto cusCorpMgrDto = ObjectMapperUtils.instance().convertValue(map, CusCorpMgrDto.class);
                riskResultDto = isExitsOperationalBusCrop(cusCorpMgrDto);
                if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                    return riskResultDto;
                }
            }
        }

        log.info("统一授信管控校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }


    /**
     * （不能改）
     * 借款人为个人时，校验其本人、配偶、成年子女及本人、配偶、成年子女作为高管（法人代表、实际控制人、出资人）的企业，在我行是否有存量授信
     * @param cusId
     * @return
     */
    public RiskResultDto isExitsStockCreditInCompany(String cusId) {
        RiskResultDto riskResultDto = new RiskResultDto();
        //通过证件号查询到借款人作为高管的关联企业客户号
        log.info("通过证件号查询到借款人作为高管的关联企业客户号,查询参数为:{}", JSON.toJSONString(cusId));
        CmisCus0012ReqDto cmisCus0012ReqDto = new CmisCus0012ReqDto();
        cmisCus0012ReqDto.setCusId(cusId);
        ResultDto<CmisCus0012RespDto> magResultDto = cmisCusClientService.cmiscus0012(cmisCus0012ReqDto);
        if(Objects.nonNull(magResultDto) && Objects.nonNull(magResultDto.getData())) {
            java.util.List<cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List> respList = magResultDto.getData().getList();
            if(CollectionUtils.nonEmpty(respList)) {
                for(cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List list: respList) {
                    // （法人代表 200400、实际控制人 201200、出资人 201700）
                    if(Objects.equals("200400",list.getMrgType()) || Objects.equals("201200",list.getMrgType()) || Objects.equals("201700",list.getMrgType()) ) {
                        //查找出该客户是否存在有效的授信协议
                        HashMap<String, String> paramMap = new HashMap<String, String>();
                        paramMap.put("cusId", list.getCusIdRel());
                        paramMap.put("openDay", DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                        log.info("根据客户号查找出该客户是否存在有效的授信协议开始,查询参数为:{}", JSON.toJSONString(paramMap));
                        int count = lmtReplyAccMapper.selectCountLmtReplayAccBycusId(paramMap);
                        log.info("根据客户号查找出该客户是否存在有效的授信协议结束,返回结果为:{}", JSON.toJSONString(count));
                        if(count > 0) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11402);
                            return riskResultDto;
                        }
                    }
                }
            }
        }
        return riskResultDto;
    }

    /**
     * 校验该客户是否有存量授信
     * @param cusId
     * @return
     */
    public RiskResultDto isExitsStockCredit(String cusId) {
        RiskResultDto riskResultDto = new RiskResultDto();
        //查找出该客户是否存在有效的授信协议
        HashMap<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("cusId", cusId);
        paramMap.put("openDay", DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        log.info("根据客户号查找出该客户是否存在有效的授信协议开始,查询参数为:{}", JSON.toJSONString(paramMap));
        int count = lmtReplyAccMapper.selectCountLmtReplayAccBycusId(paramMap);
        log.info("根据客户号查找出该客户是否存在有效的授信协议结束,返回结果为:{}", JSON.toJSONString(count));
        if(count > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11402);
            return riskResultDto;
        }
        return riskResultDto;
    }

    /**
     * 查询该客户在我行是否有经营性业务
     * @param cusId
     * @return
     */
    public RiskResultDto isExitsStockBusCont(String cusId) {
        RiskResultDto riskResultDto = new RiskResultDto();
        //查询该客户在我行是否有经营性业务
        List<BusContInfoDto> allBusContInfo = ctrCvrgContService.getAllBusContInfo(cusId);
        CfgPrdBasicinfoDto cfgPrdBasicinfoDto = new CfgPrdBasicinfoDto();
        boolean exit = false;
        for(BusContInfoDto busContInfoDto : allBusContInfo){
            // 查询产品信息
            ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoResultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(busContInfoDto.getPrdId());
            if (Objects.isNull(cfgPrdBasicinfoDtoResultDto) || Objects.isNull(cfgPrdBasicinfoDtoResultDto.getData())) {// 未查询到产品信息
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02510);
                return riskResultDto;
            } else {
                cfgPrdBasicinfoDto = cfgPrdBasicinfoDtoResultDto.getData();
                if("08".equals(cfgPrdBasicinfoDto.getPrdType())){
                    exit = true;
                    break;
                }
            }
        }
        if(exit) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11406);
            return riskResultDto;
        }
        return riskResultDto;
    }

    /**
     * 校验本人、配偶、成年子女作为高管（法人代表、实际控制人、出资人）的企业，在我行是否有经营性业务
     * @param cusId
     * @return
     */
    public RiskResultDto isExitsStockBusContForCompany(String cusId) {
        RiskResultDto riskResultDto = new RiskResultDto();
        //通过证件号查询到借款人作为高管的关联企业客户号
        log.info("通过证件号查询到借款人作为高管的关联企业客户号,查询参数为:{}", JSON.toJSONString(cusId));
        CmisCus0012ReqDto cmisCus0012ReqDto = new CmisCus0012ReqDto();
        cmisCus0012ReqDto.setCusId(cusId);
        ResultDto<CmisCus0012RespDto> magResultDto = cmisCusClientService.cmiscus0012(cmisCus0012ReqDto);
        if(Objects.nonNull(magResultDto) && Objects.nonNull(magResultDto.getData())) {
            java.util.List<cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List> respList = magResultDto.getData().getList();
            if(CollectionUtils.nonEmpty(respList)) {
                boolean exit = false;
                for(cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List list: respList) {
                    // （法人代表 200400、实际控制人 201200、出资人 201700）
                    if(Objects.equals("200400",list.getMrgType()) || Objects.equals("201200",list.getMrgType()) || Objects.equals("201700",list.getMrgType()) ) {
                        //查询该企业客户在我行是否有经营性业务
                        List<BusContInfoDto> allBusContInfo = ctrCvrgContService.getAllBusContInfo(list.getCusIdRel());
                        CfgPrdBasicinfoDto cfgPrdBasicinfoDto = new CfgPrdBasicinfoDto();
                        for(BusContInfoDto busContInfoDto : allBusContInfo){
                            // 查询产品信息
                            ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoResultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(busContInfoDto.getPrdId());
                            if (Objects.isNull(cfgPrdBasicinfoDtoResultDto) || Objects.isNull(cfgPrdBasicinfoDtoResultDto.getData())) {// 未查询到产品信息
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02510);
                                return riskResultDto;
                            } else {
                                cfgPrdBasicinfoDto = cfgPrdBasicinfoDtoResultDto.getData();
                                if("08".equals(cfgPrdBasicinfoDto.getPrdType())){
                                    exit = true;
                                    break;
                                }
                            }
                        }
                        if(exit) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11407);
                            return riskResultDto;
                        }
                    }
                }
            }
        }
        return riskResultDto;
    }

    /**
     *
     * 借款人为个人时，校验其本人、配偶、成年子女及本人、配偶、成年子女作为高管（法人代表、实际控制人、出资人）的企业，在我行是否有存量授信，如有则提示。
     * @param cusId
     * @return
     */
    public RiskResultDto isExitsOperationalBus(String cusId) {
        RiskResultDto riskResultDto = new RiskResultDto();
        riskResultDto = isExitsStockCredit(cusId);
        if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        riskResultDto = isExitsStockCreditInCompany(cusId);
        if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        // 获取个人社会关系信息
        ResultDto<List<CusIndivSocialResp>> cusIndivSocialDto = cmisCusClientService.selectCusIndivSocialDtoList(cusId);
        if (Objects.isNull(cusIndivSocialDto) || CollectionUtils.isEmpty(cusIndivSocialDto.getData())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11401);
            return riskResultDto;
        }
        for(Object map : cusIndivSocialDto.getData()) {
            CusIndivSocialResp cusIndivSocialResp = ObjectMapperUtils.instance().convertValue(map, CusIndivSocialResp.class);
            // 102000 配偶 103000 子女
            if(Objects.equals("102000",cusIndivSocialResp.getIndivCusRel()) || Objects.equals("103000",cusIndivSocialResp.getIndivCusRel())) {
                riskResultDto = isExitsStockCredit(cusIndivSocialResp.getCusId());
                if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                    return riskResultDto;
                }
                riskResultDto = isExitsStockCreditInCompany(cusIndivSocialResp.getCusId());
                if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                    return riskResultDto;
                }
            }
        }
        return riskResultDto;
    }

    /**
     * 借款人为企业时，校验其法人代表及其配偶、实际控制人及其配偶及成年子女、出资人在我行是否有存量经营性业务，同时校验上述人员担任高管的企业在我行是否有存量经营性业务，如有则提示
     * @param cusCorpMgrDto
     * @return
     */
    public RiskResultDto isExitsOperationalBusCrop(CusCorpMgrDto cusCorpMgrDto) {
        RiskResultDto riskResultDto = new RiskResultDto();
        // 法人代表 200400、实际控制人 201200、出资人 201700
        riskResultDto = isExitsStockBusCont(cusCorpMgrDto.getCusId());
        if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        riskResultDto = isExitsStockBusContForCompany(cusCorpMgrDto.getCusId());
        if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        // 校验其法人代表及其配偶、实际控制人及其配偶及成年子女在我行是否有存量经营性业务
        if(Objects.equals("200400",cusCorpMgrDto.getMrgType()) || Objects.equals("201200",cusCorpMgrDto.getMrgType())) {
            // 获取个人社会关系信息
            ResultDto<List<CusIndivSocialResp>> cusIndivSocialDto = cmisCusClientService.selectCusIndivSocialDtoList(cusCorpMgrDto.getCusId());
            if (Objects.isNull(cusIndivSocialDto)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11405);
                return riskResultDto;
            }
            if(CollectionUtils.nonEmpty(cusIndivSocialDto.getData())) {
                for(Object map : cusIndivSocialDto.getData()) {
                    CusIndivSocialResp cusIndivSocialResp = ObjectMapperUtils.instance().convertValue(map, CusIndivSocialResp.class);
                    // 校验其法人代表及其配偶、实际控制人及其配偶及成年子女
                    // 102000 配偶 103000 子女
                    if((Objects.equals("200400",cusCorpMgrDto.getMrgType()) && Objects.equals("102000",cusIndivSocialResp.getIndivCusRel()))
                            || ((Objects.equals("201200",cusCorpMgrDto.getMrgType()) && Objects.equals("102000",cusIndivSocialResp.getIndivCusRel()))
                            || (Objects.equals("201200",cusCorpMgrDto.getMrgType()) && Objects.equals("103000",cusIndivSocialResp.getIndivCusRel())))) {
                        riskResultDto = isExitsStockBusCont(cusCorpMgrDto.getCusId());
                        if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                            return riskResultDto;
                        }
                        // 担任高管的企业在我行是否有存量经营性业务
                        riskResultDto = isExitsStockBusContForCompany(cusCorpMgrDto.getCusId());
                        if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                            return riskResultDto;
                        }
                    }
                }
            }
        }
        return riskResultDto;
    }
}