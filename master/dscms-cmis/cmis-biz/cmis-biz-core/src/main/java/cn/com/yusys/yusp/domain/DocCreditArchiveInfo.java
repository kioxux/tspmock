/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocCreditArchiveInfo
 * @类描述: doc_credit_archive_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 21:13:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_credit_archive_info")
public class DocCreditArchiveInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 档案流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "DCAI_SERNO")
	private String dcaiSerno;
	
	/** 关联业务流水号 **/
	@Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 40)
	private String bizSerno;
	
	/** 主借款人名称 **/
	@Column(name = "BORROWER_CUS_NAME", unique = false, nullable = true, length = 80)
	private String borrowerCusName;
	
	/** 主借款人证件号码 **/
	@Column(name = "BORROWER_CERT_CODE", unique = false, nullable = true, length = 40)
	private String borrowerCertCode;
	
	/** 征信查询对象名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 征信查询对象证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;
	
	/** 授权书日期 **/
	@Column(name = "AUTHBOOK_DATE", unique = false, nullable = true, length = 20)
	private String authbookDate;
	
	/** 查询人 **/
	@Column(name = "QRY_USER", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="qryUserName")
	private String qryUser;
	
	/** 查询机构 **/
	@Column(name = "QRY_ORG", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName" ,refFieldName="qryOrgName")
	private String qryOrg;
	
	/** 生成日期 **/
	@Column(name = "CREATE_DATE", unique = false, nullable = true, length = 10)
	private String createDate;
	
	/** 接收人 **/
	@Column(name = "RECEIVER_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="receiverIdName")
	private String receiverId;
	
	/** 接收日期 **/
	@Column(name = "RECEIVER_DATE", unique = false, nullable = true, length = 10)
	private String receiverDate;
	
	/** 核对日期 **/
	@Column(name = "CHECK_DATE", unique = false, nullable = true, length = 10)
	private String checkDate;
	
	/** 征信档案状态 **/
	@Column(name = "DOC_STAUTS", unique = false, nullable = true, length = 5)
	private String docStauts;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 核对人 **/
	@Column(name = "CHECK_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="checkIdName")
	private String checkId;
	
	
	/**
	 * @param dcaiSerno
	 */
	public void setDcaiSerno(String dcaiSerno) {
		this.dcaiSerno = dcaiSerno;
	}
	
    /**
     * @return dcaiSerno
     */
	public String getDcaiSerno() {
		return this.dcaiSerno;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}
	
    /**
     * @return bizSerno
     */
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param borrowerCusName
	 */
	public void setBorrowerCusName(String borrowerCusName) {
		this.borrowerCusName = borrowerCusName;
	}
	
    /**
     * @return borrowerCusName
     */
	public String getBorrowerCusName() {
		return this.borrowerCusName;
	}
	
	/**
	 * @param borrowerCertCode
	 */
	public void setBorrowerCertCode(String borrowerCertCode) {
		this.borrowerCertCode = borrowerCertCode;
	}
	
    /**
     * @return borrowerCertCode
     */
	public String getBorrowerCertCode() {
		return this.borrowerCertCode;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param authbookDate
	 */
	public void setAuthbookDate(String authbookDate) {
		this.authbookDate = authbookDate;
	}
	
    /**
     * @return authbookDate
     */
	public String getAuthbookDate() {
		return this.authbookDate;
	}
	
	/**
	 * @param qryUser
	 */
	public void setQryUser(String qryUser) {
		this.qryUser = qryUser;
	}
	
    /**
     * @return qryUser
     */
	public String getQryUser() {
		return this.qryUser;
	}
	
	/**
	 * @param qryOrg
	 */
	public void setQryOrg(String qryOrg) {
		this.qryOrg = qryOrg;
	}
	
    /**
     * @return qryOrg
     */
	public String getQryOrg() {
		return this.qryOrg;
	}
	
	/**
	 * @param createDate
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
    /**
     * @return createDate
     */
	public String getCreateDate() {
		return this.createDate;
	}
	
	/**
	 * @param receiverId
	 */
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	
    /**
     * @return receiverId
     */
	public String getReceiverId() {
		return this.receiverId;
	}
	
	/**
	 * @param receiverDate
	 */
	public void setReceiverDate(String receiverDate) {
		this.receiverDate = receiverDate;
	}
	
    /**
     * @return receiverDate
     */
	public String getReceiverDate() {
		return this.receiverDate;
	}
	
	/**
	 * @param checkDate
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	
    /**
     * @return checkDate
     */
	public String getCheckDate() {
		return this.checkDate;
	}
	
	/**
	 * @param docStauts
	 */
	public void setDocStauts(String docStauts) {
		this.docStauts = docStauts;
	}
	
    /**
     * @return docStauts
     */
	public String getDocStauts() {
		return this.docStauts;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param checkId
	 */
	public void setCheckId(String checkId) {
		this.checkId = checkId;
	}
	
    /**
     * @return checkId
     */
	public String getCheckId() {
		return this.checkId;
	}


}