/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PvpDemandRepayAppInfo;
import cn.com.yusys.yusp.service.PvpDemandRepayAppInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpDemandRepayAppInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-27 10:56:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pvpdemandrepayappinfo")
public class PvpDemandRepayAppInfoResource {
    @Autowired
    private PvpDemandRepayAppInfoService pvpDemandRepayAppInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpDemandRepayAppInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpDemandRepayAppInfo> list = pvpDemandRepayAppInfoService.selectAll(queryModel);
        return new ResultDto<List<PvpDemandRepayAppInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpDemandRepayAppInfo>> index(QueryModel queryModel) {
        List<PvpDemandRepayAppInfo> list = pvpDemandRepayAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PvpDemandRepayAppInfo>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpDemandRepayAppInfo> create(@RequestBody PvpDemandRepayAppInfo pvpDemandRepayAppInfo) throws URISyntaxException {
        pvpDemandRepayAppInfoService.insert(pvpDemandRepayAppInfo);
        return new ResultDto<PvpDemandRepayAppInfo>(pvpDemandRepayAppInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpDemandRepayAppInfo pvpDemandRepayAppInfo) throws URISyntaxException {
        int result = pvpDemandRepayAppInfoService.update(pvpDemandRepayAppInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String billNo) {
        int result = pvpDemandRepayAppInfoService.deleteByPrimaryKey(pkId, billNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.PvpDemandRepayAppInfo>>
     * @author hubp
     * @date 2021/8/27 13:51
     * @version 1.0.0
     * @desc   分页查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<PvpDemandRepayAppInfo>> selectByModel(@RequestBody QueryModel queryModel) {
        List<PvpDemandRepayAppInfo> list = pvpDemandRepayAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<PvpDemandRepayAppInfo>>(list);
    }

    /**
     * @param pvpDemandRepayAppInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.PvpDemandRepayAppInfo>
     * @author hubp
     * @date 2021/8/27 15:16
     * @version 1.0.0
     * @desc  新增一条数据，并将数据返回
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addpvpdemandrepayapp")
    protected ResultDto<PvpDemandRepayAppInfo> addPvpDemandRepayApp(@RequestBody PvpDemandRepayAppInfo pvpDemandRepayAppInfo) {
        return pvpDemandRepayAppInfoService.addPvpDemandRepayApp(pvpDemandRepayAppInfo);
    }

    /**
     * @param pvpDemandRepayAppInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.PvpDemandRepayAppInfo>>
     * @author hubp
     * @date 2021/8/27 22:44
     * @version 1.0.0
     * @desc    根据主键，借据编号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbypkid")
    protected ResultDto<PvpDemandRepayAppInfo> selectByPkId(@RequestBody PvpDemandRepayAppInfo pvpDemandRepayAppInfo) {
        return new ResultDto<PvpDemandRepayAppInfo>(pvpDemandRepayAppInfoService.selectByPkId(pvpDemandRepayAppInfo.getPkId()));
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.PvpDemandRepayAppInfo>>
     * @author hubp
     * @date 2021/8/27 13:51
     * @version 1.0.0
     * @desc   通过借据编号获取核心交易信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectcorebymodel")
    protected ResultDto selectCoreByModel(@RequestBody QueryModel queryModel) {
        return pvpDemandRepayAppInfoService.selectCoreData(queryModel);
    }

    /**
     * @param pvpDemandRepayAppInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/8/28 10:09
     * @version 1.0.0
     * @desc    送核心进行冲正
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/sendcoredata")
    protected ResultDto sendCoreData(@RequestBody PvpDemandRepayAppInfo pvpDemandRepayAppInfo) {
        return pvpDemandRepayAppInfoService.sendCoreData(pvpDemandRepayAppInfo);
    }

    /**
     * @param pvpDemandRepayAppInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/8/28 10:55
     * @version 1.0.0
     * @desc  保存基本数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/updatepvpdemandrepayapp")
    protected ResultDto<Integer> updatePvpDemandRepayApp(@RequestBody PvpDemandRepayAppInfo pvpDemandRepayAppInfo) {
        int result = pvpDemandRepayAppInfoService.updateSelective(pvpDemandRepayAppInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @param pvpDemandRepayAppInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/8/28 11:01
     * @version 1.0.0
     * @desc  保存第三方数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/updatepvpdemandrepayappinfo")
    protected ResultDto updatePvpDemandRepayAppInfo(@RequestBody PvpDemandRepayAppInfo pvpDemandRepayAppInfo) {
        return pvpDemandRepayAppInfoService.updatePvpDemandRepayAppInfo(pvpDemandRepayAppInfo);
    }

    /**
     * @param pvpDemandRepayAppInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/8/28 14:17
     * @version 1.0.0
     * @desc   删除单条数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/deletedata")
    protected ResultDto<Integer> deleteData(@RequestBody PvpDemandRepayAppInfo pvpDemandRepayAppInfo) {
        int result = pvpDemandRepayAppInfoService.deleteByTowKey(pvpDemandRepayAppInfo.getPkId(), pvpDemandRepayAppInfo.getBillNo());
        return new ResultDto<Integer>(result);
    }
}
