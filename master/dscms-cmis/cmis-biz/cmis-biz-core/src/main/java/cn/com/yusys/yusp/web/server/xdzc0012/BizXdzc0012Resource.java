package cn.com.yusys.yusp.web.server.xdzc0012;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0012.req.Xdzc0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0012.resp.Xdzc0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0012.Xdzc0012Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:贸易背景资料收集通知接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0012:贸易背景资料收集通知接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0012Resource.class);

    @Autowired
    private Xdzc0012Service xdzc0012Service;
    /**
     * 交易码：xdzc0012
     * 交易描述：贸易背景资料收集通知接口
     *
     * @param xdzc0012DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贸易背景资料收集通知接口")
    @PostMapping("/xdzc0012")
    protected @ResponseBody
    ResultDto<Xdzc0012DataRespDto> xdzc0012(@Validated @RequestBody Xdzc0012DataReqDto xdzc0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value, JSON.toJSONString(xdzc0012DataReqDto));
        Xdzc0012DataRespDto xdzc0012DataRespDto = new Xdzc0012DataRespDto();// 响应Dto:贸易背景资料收集通知接口
        ResultDto<Xdzc0012DataRespDto> xdzc0012DataResultDto = new ResultDto<>();

        try {
            xdzc0012DataRespDto = xdzc0012Service.xdzc0012Service(xdzc0012DataReqDto);
            xdzc0012DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0012DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value, e.getMessage());
            // 封装xdzc0012DataResultDto中异常返回码和返回信息
            xdzc0012DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0012DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0012DataRespDto到xdzc0012DataResultDto中
        xdzc0012DataResultDto.setData(xdzc0012DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value, JSON.toJSONString(xdzc0012DataResultDto));
        return xdzc0012DataResultDto;
    }
}
