/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpRiskChkDebtCfrm;
import cn.com.yusys.yusp.service.IqpRiskChkDebtCfrmService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRiskChkDebtCfrmResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-25 17:33:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpriskchkdebtcfrm")
public class IqpRiskChkDebtCfrmResource {
    private static final Logger log = LoggerFactory.getLogger(IqpRiskChkDebtCfrmResource.class);
    @Autowired
    private IqpRiskChkDebtCfrmService iqpRiskChkDebtCfrmService;

	/**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpRiskChkDebtCfrm>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpRiskChkDebtCfrm> list = iqpRiskChkDebtCfrmService.selectAll(queryModel);
        return new ResultDto<List<IqpRiskChkDebtCfrm>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpRiskChkDebtCfrm>> index(QueryModel queryModel) {
        List<IqpRiskChkDebtCfrm> list = iqpRiskChkDebtCfrmService.selectByModel(queryModel);
        return new ResultDto<List<IqpRiskChkDebtCfrm>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<IqpRiskChkDebtCfrm> show(@PathVariable("serno") String serno) {
        IqpRiskChkDebtCfrm iqpRiskChkDebtCfrm = iqpRiskChkDebtCfrmService.selectByPrimaryKey(serno);
        return new ResultDto<IqpRiskChkDebtCfrm>(iqpRiskChkDebtCfrm);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpRiskChkDebtCfrm> create(@RequestBody IqpRiskChkDebtCfrm iqpRiskChkDebtCfrm) throws URISyntaxException {
        iqpRiskChkDebtCfrmService.insert(iqpRiskChkDebtCfrm);
        return new ResultDto<IqpRiskChkDebtCfrm>(iqpRiskChkDebtCfrm);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpRiskChkDebtCfrm iqpRiskChkDebtCfrm) throws URISyntaxException {
        int result = iqpRiskChkDebtCfrmService.update(iqpRiskChkDebtCfrm);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = iqpRiskChkDebtCfrmService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpRiskChkDebtCfrmService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 通过流水号获取负债汇总信息
     * @param iqpSerno
     * @return
     */
    @GetMapping("getRiskCheckInfo/{iqpSerno}")
    protected ResultDto<Map> getRiskCheckInfo(@PathVariable("iqpSerno")String iqpSerno){
        log.info("通过流水号获取负债汇总信息入参{}"+iqpSerno);
        return new ResultDto<Map>(iqpRiskChkDebtCfrmService.getRiskCheckInfo(iqpSerno));
    }
}
