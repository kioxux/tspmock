/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.IqpLoanAppDramPlanSub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestRelFinaDetails;
import cn.com.yusys.yusp.service.LmtSigInvestRelFinaDetailsService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRelFinaDetailsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-20 15:09:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestrelfinadetails")
public class LmtSigInvestRelFinaDetailsResource {
    @Autowired
    private LmtSigInvestRelFinaDetailsService lmtSigInvestRelFinaDetailsService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestRelFinaDetails>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestRelFinaDetails> list = lmtSigInvestRelFinaDetailsService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestRelFinaDetails>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtSigInvestRelFinaDetails>> index(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" createTime asc ");
        }
        List<LmtSigInvestRelFinaDetails> list = lmtSigInvestRelFinaDetailsService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestRelFinaDetails>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestRelFinaDetails> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestRelFinaDetails lmtSigInvestRelFinaDetails = lmtSigInvestRelFinaDetailsService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestRelFinaDetails>(lmtSigInvestRelFinaDetails);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestRelFinaDetails> create(@RequestBody LmtSigInvestRelFinaDetails lmtSigInvestRelFinaDetails) throws URISyntaxException {
        lmtSigInvestRelFinaDetailsService.insert(lmtSigInvestRelFinaDetails);
        return new ResultDto<LmtSigInvestRelFinaDetails>(lmtSigInvestRelFinaDetails);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestRelFinaDetails lmtSigInvestRelFinaDetails) throws URISyntaxException {
        int result = lmtSigInvestRelFinaDetailsService.update(lmtSigInvestRelFinaDetails);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:addOrUpdateAllTable
     * @函数描述:新增或保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addOrUpdateAllTable")
    protected ResultDto<Boolean> addOrUpdateAllTable(@RequestBody List<LmtSigInvestRelFinaDetails> list) {
        boolean result = lmtSigInvestRelFinaDetailsService.addOrUpdateAllTable(list);
        return new ResultDto<Boolean>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteLogicByPkId")
    protected ResultDto<Integer> delete(@RequestBody Map condition) {
        String pkId = (String) condition.get("pkId");
        int result = lmtSigInvestRelFinaDetailsService.deleteLogicByPkId(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestRelFinaDetailsService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
