/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtGrpMemRel;
import cn.com.yusys.yusp.domain.LmtSurveyReportComInfo;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.*;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.client.bsp.outerdata.zsnew.ZsnewService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptCptlSitu;
import cn.com.yusys.yusp.repository.mapper.RptCptlSituMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-18 09:34:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptCptlSituService {
    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyReportComInfo.class);
    @Autowired
    private RptCptlSituMapper rptCptlSituMapper;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private ZsnewService zsnewService;
    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptCptlSitu selectByPrimaryKey(String pkId) {
        return rptCptlSituMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptCptlSitu> selectAll(QueryModel model) {
        List<RptCptlSitu> records = (List<RptCptlSitu>) rptCptlSituMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptCptlSitu> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptCptlSitu> list = rptCptlSituMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptCptlSitu record) {
        return rptCptlSituMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptCptlSitu record) {
        return rptCptlSituMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptCptlSitu record) {
        return rptCptlSituMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptCptlSitu record) {
        return rptCptlSituMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptCptlSituMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptCptlSituMapper.deleteByIds(ids);
    }

    /**
     * 查询集团成员融资信息
     *
     * @param model
     * @return
     */
    public List<Map<String, Object>> selectGrpSitu(QueryModel model) {
        String grpSerno = model.getCondition().get("serno").toString();
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            String serno = lmtGrpMemRel.getSingleSerno();
            String cusName = lmtGrpMemRel.getCusName();
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("serno", serno);
            List<RptCptlSitu> rptCptlSituList = rptCptlSituMapper.selectByModel(queryModel);
            if (rptCptlSituList != null && rptCptlSituList.size() > 0) {
                RptCptlSitu rptCptlSitu = rptCptlSituList.get(0);
                //行政处罚
                if (rptCptlSitu.getAdministrativePenalty() != null && rptCptlSitu.getAdministrativePenalty() > 0) {
                    Map<String, Object> administrativePenaltyMap = new HashMap<>();
                    administrativePenaltyMap.put("cusName", cusName);
                    administrativePenaltyMap.put("riskType", "行政处罚");
                    administrativePenaltyMap.put("count", rptCptlSitu.getAdministrativePenalty());
                    administrativePenaltyMap.put("desc", rptCptlSitu.getAdministrativePenaltyDesc());
                    result.add(administrativePenaltyMap);
                }
                //经营异常
                if (rptCptlSitu.getAbnormalOperation() != null && rptCptlSitu.getAbnormalOperation() > 0) {
                    Map<String, Object> getAbnormalOperationMap = new HashMap<>();
                    getAbnormalOperationMap.put("cusName", cusName);
                    getAbnormalOperationMap.put("riskType", "经营异常");
                    getAbnormalOperationMap.put("count", rptCptlSitu.getAbnormalOperation());
                    getAbnormalOperationMap.put("desc", rptCptlSitu.getAbnormalOperationDesc());
                    result.add(getAbnormalOperationMap);
                }
                //环保处罚
                if (rptCptlSitu.getEnvironmentalPunishment() != null && rptCptlSitu.getEnvironmentalPunishment() > 0) {
                    Map<String, Object> environmentalPunishmentMap = new HashMap<>();
                    environmentalPunishmentMap.put("cusName", cusName);
                    environmentalPunishmentMap.put("riskType", "环保处罚");
                    environmentalPunishmentMap.put("count", rptCptlSitu.getEnvironmentalPunishment());
                    environmentalPunishmentMap.put("desc", rptCptlSitu.getEnvironmentalPunishmentDesc());
                    result.add(environmentalPunishmentMap);
                }
                //欠税公告
                if (rptCptlSitu.getTaxArrearsNotice() != null && rptCptlSitu.getTaxArrearsNotice() > 0) {
                    Map<String, Object> taxArrearsNoticeMap = new HashMap<>();
                    taxArrearsNoticeMap.put("cusName", cusName);
                    taxArrearsNoticeMap.put("riskType", "欠税公告");
                    taxArrearsNoticeMap.put("count", rptCptlSitu.getTaxArrearsNotice());
                    taxArrearsNoticeMap.put("desc", rptCptlSitu.getTaxArrearsNoticeDesc());
                    result.add(taxArrearsNoticeMap);
                }
                //税收违法
                if (rptCptlSitu.getIllegalTaxation() != null && rptCptlSitu.getIllegalTaxation() > 0) {
                    Map<String, Object> illegalTaxationMap = new HashMap<>();
                    illegalTaxationMap.put("cusName", cusName);
                    illegalTaxationMap.put("riskType", "税收违法");
                    illegalTaxationMap.put("count", rptCptlSitu.getIllegalTaxation());
                    illegalTaxationMap.put("desc", rptCptlSitu.getIllegalTaxationDesc());
                    result.add(illegalTaxationMap);
                }
                //股权出质
                if (rptCptlSitu.getEquityOutPledge() != null && rptCptlSitu.getEquityOutPledge() > 0) {
                    Map<String, Object> equityOutPledgeMap = new HashMap<>();
                    equityOutPledgeMap.put("cusName", cusName);
                    equityOutPledgeMap.put("riskType", "股权出质");
                    equityOutPledgeMap.put("count", rptCptlSitu.getEquityOutPledge());
                    equityOutPledgeMap.put("desc", rptCptlSitu.getEquityOutPledgeDesc());
                    result.add(equityOutPledgeMap);
                }
                //股权质押
                if (rptCptlSitu.getEquityPledge() != null && rptCptlSitu.getEquityPledge() > 0) {
                    Map<String, Object> equityPledgeMap = new HashMap<>();
                    equityPledgeMap.put("cusName", cusName);
                    equityPledgeMap.put("riskType", "股权质押");
                    equityPledgeMap.put("count", rptCptlSitu.getEquityPledge());
                    equityPledgeMap.put("desc", rptCptlSitu.getEquityPledgeDesc());
                    result.add(equityPledgeMap);
                }
                //股权冻结
                if (rptCptlSitu.getEquityFreeze() != null && rptCptlSitu.getEquityFreeze() > 0) {
                    Map<String, Object> equityFreezeMap = new HashMap<>();
                    equityFreezeMap.put("cusName", cusName);
                    equityFreezeMap.put("riskType", "股权冻结");
                    equityFreezeMap.put("count", rptCptlSitu.getEquityFreeze());
                    equityFreezeMap.put("desc", rptCptlSitu.getEquityFreezeDesc());
                    result.add(equityFreezeMap);
                }
                //动产抵押
                if (rptCptlSitu.getChattelMortgage() != null && rptCptlSitu.getChattelMortgage() > 0) {
                    Map<String, Object> chattelMortgageMap = new HashMap<>();
                    chattelMortgageMap.put("cusName", cusName);
                    chattelMortgageMap.put("riskType", "动产抵押");
                    chattelMortgageMap.put("count", rptCptlSitu.getChattelMortgage());
                    chattelMortgageMap.put("desc", rptCptlSitu.getChattelMortgageDesc());
                    result.add(chattelMortgageMap);
                }
                //知识产权出质
                if (rptCptlSitu.getIntellectualPropertyPledge() != null && rptCptlSitu.getIntellectualPropertyPledge() > 0) {
                    Map<String, Object> intellectualPropertyPledgeMap = new HashMap<>();
                    intellectualPropertyPledgeMap.put("cusName", cusName);
                    intellectualPropertyPledgeMap.put("riskType", "知识产权出质");
                    intellectualPropertyPledgeMap.put("count", rptCptlSitu.getIntellectualPropertyPledge());
                    intellectualPropertyPledgeMap.put("desc", rptCptlSitu.getIntellectualPropertyPledgeDesc());
                    result.add(intellectualPropertyPledgeMap);
                }
                //失信被执行人
                if (rptCptlSitu.getDishonestExecutedPerson() != null && rptCptlSitu.getDishonestExecutedPerson() > 0) {
                    Map<String, Object> dishonestExecutedPersonMap = new HashMap<>();
                    dishonestExecutedPersonMap.put("cusName", cusName);
                    dishonestExecutedPersonMap.put("riskType", "知识产权出质");
                    dishonestExecutedPersonMap.put("count", rptCptlSitu.getDishonestExecutedPerson());
                    dishonestExecutedPersonMap.put("desc", rptCptlSitu.getDishonestExecutedPersonDesc());
                    result.add(dishonestExecutedPersonMap);
                }
            }
        }
        return result;
    }

    /**
     * 融资情况初始化
     *
     * @param rptCptlSitu
     * @return
     */
    public RptCptlSitu initCptlSitu(RptCptlSitu rptCptlSitu) throws YuspException {
        RptCptlSitu result = rptCptlSituMapper.selectBySerno(rptCptlSitu.getSerno());
        if (Objects.nonNull(result)) {
            return result;
        } else {
            result = new RptCptlSitu();
            result.setPkId(StringUtils.getUUID());
            result.setSerno(rptCptlSitu.getSerno());
            result.setCusId(rptCptlSitu.getCusId());
            result.setCusName(rptCptlSitu.getCusName());
            //调用中数平台接口
            logger.info("查询工商信息开始..................");
            ZsnewReqDto zsnewReqDto = new ZsnewReqDto();
            zsnewReqDto.setName(rptCptlSitu.getCusName());
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, JSON.toJSONString(zsnewReqDto));
            ResultDto<ZsnewRespDto> zsnewResultDto = dscms2OuterdataClientService.zsnew(zsnewReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, JSON.toJSONString(zsnewResultDto));
            String zsnewCode = Optional.ofNullable(zsnewResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String zsnewMeesage = Optional.ofNullable(zsnewResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            ZsnewRespDto zsnewRespDto = null;
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, zsnewResultDto.getCode())) {
                //  获取相关的值并解析
                zsnewRespDto = zsnewResultDto.getData();
            }
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value);
            if (null != zsnewRespDto && null != zsnewRespDto.getData()) {
                Data data = zsnewRespDto.getData();
                if (null != data.getCODE() && "200".equals(data.getCODE())) {
                    ENT_INFO entInfo = (ENT_INFO) data.getENT_INFO();
                    //行政处罚信息
                    List<ENTCASEBASEINFO> entcasebaseinfo = entInfo.getENTCASEBASEINFO();
                    if (CollectionUtils.nonEmpty(entcasebaseinfo)) {
                        result.setAdministrativePenalty(entcasebaseinfo.size());
                    }
                    //经营异常
                    List<EXCEPTIONLIST> exceptionlist = entInfo.getEXCEPTIONLIST();
                    if (CollectionUtils.nonEmpty(exceptionlist)) {
                        result.setAbnormalOperation(exceptionlist.size());
                    }
                    //环保处罚 TODO
                    //欠税公告 TODO
                    //税收违法 TODO
                    //股权出质
                    List<STOCKPAWN> stockpawn = entInfo.getSTOCKPAWN();
                    if (CollectionUtils.nonEmpty(stockpawn)) {
                        result.setEquityOutPledge(stockpawn.size());
                    }
                    //股权质押 TODO
                    //股权冻结 TODO
                    //动产抵押
                    List<MORTGAGEBASIC> mortgagebasic = entInfo.getMORTGAGEBASIC();
                    if (CollectionUtils.nonEmpty(mortgagebasic)) {
                        result.setChattelMortgage(mortgagebasic.size());
                    }
                    //知识产权出质 TODO
                    //失信被执行人
                    List<PUNISHBREAK> punishbreak = entInfo.getPUNISHBREAK();
                    if (CollectionUtils.nonEmpty(punishbreak)) {
                        result.setDishonestExecutedPerson(punishbreak.size());
                    }
                }
            }
            int count = rptCptlSituMapper.insertSelective(result);
            if (count > 0) {
                return result;
            }
        }
        return null;
    }
    public RptCptlSitu selectBySerno(String serno){
        return rptCptlSituMapper.selectBySerno(serno);
    }

    public int save(RptCptlSitu rptCptlSitu){
        String serno = rptCptlSitu.getSerno();
        RptCptlSitu temp = rptCptlSituMapper.selectBySerno(serno);
        if(Objects.nonNull(temp)){
            return update(rptCptlSitu);
        }else {
            rptCptlSitu.setPkId(StringUtils.getUUID());
            return insert(rptCptlSitu);
        }

    }
}
