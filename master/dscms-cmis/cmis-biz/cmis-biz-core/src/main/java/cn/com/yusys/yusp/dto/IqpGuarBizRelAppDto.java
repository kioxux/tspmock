package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpGuarBizRelApp
 * @类描述: iqp_guar_biz_rel_app数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 10:55:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpGuarBizRelAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务流水号 **/
	private String iqpSerno;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 担保合同流水号  **/
	private String guarPkId;
	
	/** 担保合同编号 **/
	private String guarContNo;
	
	/** 本次担保金额 **/
	private java.math.BigDecimal thisGuarAmt;
	
	/** 是否追加担保 STD_ZB_YES_NO **/
	private String isAddGuar;
	
	/** 是否阶段性担保 STD_ZB_YES_NO **/
	private String isPerGur;
	
	/** 关联关系 STD_ZB_CORRE_REL **/
	private String correRel;
	
	/** 关联类型 STD_ZB_REL_TYPE **/
	private String rType;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param guarPkId
	 */
	public void setGuarPkId(String guarPkId) {
		this.guarPkId = guarPkId == null ? null : guarPkId.trim();
	}
	
    /**
     * @return GuarPkId
     */	
	public String getGuarPkId() {
		return this.guarPkId;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}
	
    /**
     * @return GuarContNo
     */	
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param thisGuarAmt
	 */
	public void setThisGuarAmt(java.math.BigDecimal thisGuarAmt) {
		this.thisGuarAmt = thisGuarAmt;
	}
	
    /**
     * @return ThisGuarAmt
     */	
	public java.math.BigDecimal getThisGuarAmt() {
		return this.thisGuarAmt;
	}
	
	/**
	 * @param isAddGuar
	 */
	public void setIsAddGuar(String isAddGuar) {
		this.isAddGuar = isAddGuar == null ? null : isAddGuar.trim();
	}
	
    /**
     * @return IsAddGuar
     */	
	public String getIsAddGuar() {
		return this.isAddGuar;
	}
	
	/**
	 * @param isPerGur
	 */
	public void setIsPerGur(String isPerGur) {
		this.isPerGur = isPerGur == null ? null : isPerGur.trim();
	}
	
    /**
     * @return IsPerGur
     */	
	public String getIsPerGur() {
		return this.isPerGur;
	}
	
	/**
	 * @param correRel
	 */
	public void setCorreRel(String correRel) {
		this.correRel = correRel == null ? null : correRel.trim();
	}
	
    /**
     * @return CorreRel
     */	
	public String getCorreRel() {
		return this.correRel;
	}
	
	/**
	 * @param rType
	 */
	public void setRType(String rType) {
		this.rType = rType == null ? null : rType.trim();
	}
	
    /**
     * @return RType
     */	
	public String getRType() {
		return this.rType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}