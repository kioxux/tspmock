/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarPerson
 * @类描述: rpt_lmt_repay_anys_guar_person数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-05 23:35:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_lmt_repay_anys_guar_person")
public class RptLmtRepayAnysGuarPerson extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 总体概述 **/
	@Column(name = "GENERAL_OVERVIEW", unique = false, nullable = true, length = 65535)
	private String generalOverview;
	
	/** 本次拟担保额度 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 担保测算额度 **/
	@Column(name = "GUAR_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarLmtAmt;
	
	/** 担保人名称 **/
	@Column(name = "GUAR_NAME", unique = false, nullable = true, length = 40)
	private String guarName;
	
	/** 担保人担保能力资产分析 **/
	@Column(name = "GUAR_ABL_ASSET_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String guarAblAssetAnalysis;
	
	/** 担保人担保能力负债分析 **/
	@Column(name = "GUAR_ABL_LIBIL_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String guarAblLibilAnalysis;
	
	/** 担保人担保能力收入分析 **/
	@Column(name = "GUAR_ABL_INCOM_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String guarAblIncomAnalysis;
	
	/** 担保人担保能力对外担保分析 **/
	@Column(name = "GUAR_ABL_EXT_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String guarAblExtAnalysis;
	
	/** 担保能力总体评价 **/
	@Column(name = "GUAR_ABL_EVAL", unique = false, nullable = true, length = 65535)
	private String guarAblEval;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param generalOverview
	 */
	public void setGeneralOverview(String generalOverview) {
		this.generalOverview = generalOverview;
	}
	
    /**
     * @return generalOverview
     */
	public String getGeneralOverview() {
		return this.generalOverview;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param guarLmtAmt
	 */
	public void setGuarLmtAmt(java.math.BigDecimal guarLmtAmt) {
		this.guarLmtAmt = guarLmtAmt;
	}
	
    /**
     * @return guarLmtAmt
     */
	public java.math.BigDecimal getGuarLmtAmt() {
		return this.guarLmtAmt;
	}
	
	/**
	 * @param guarName
	 */
	public void setGuarName(String guarName) {
		this.guarName = guarName;
	}
	
    /**
     * @return guarName
     */
	public String getGuarName() {
		return this.guarName;
	}
	
	/**
	 * @param guarAblAssetAnalysis
	 */
	public void setGuarAblAssetAnalysis(String guarAblAssetAnalysis) {
		this.guarAblAssetAnalysis = guarAblAssetAnalysis;
	}
	
    /**
     * @return guarAblAssetAnalysis
     */
	public String getGuarAblAssetAnalysis() {
		return this.guarAblAssetAnalysis;
	}
	
	/**
	 * @param guarAblLibilAnalysis
	 */
	public void setGuarAblLibilAnalysis(String guarAblLibilAnalysis) {
		this.guarAblLibilAnalysis = guarAblLibilAnalysis;
	}
	
    /**
     * @return guarAblLibilAnalysis
     */
	public String getGuarAblLibilAnalysis() {
		return this.guarAblLibilAnalysis;
	}
	
	/**
	 * @param guarAblIncomAnalysis
	 */
	public void setGuarAblIncomAnalysis(String guarAblIncomAnalysis) {
		this.guarAblIncomAnalysis = guarAblIncomAnalysis;
	}
	
    /**
     * @return guarAblIncomAnalysis
     */
	public String getGuarAblIncomAnalysis() {
		return this.guarAblIncomAnalysis;
	}
	
	/**
	 * @param guarAblExtAnalysis
	 */
	public void setGuarAblExtAnalysis(String guarAblExtAnalysis) {
		this.guarAblExtAnalysis = guarAblExtAnalysis;
	}
	
    /**
     * @return guarAblExtAnalysis
     */
	public String getGuarAblExtAnalysis() {
		return this.guarAblExtAnalysis;
	}
	
	/**
	 * @param guarAblEval
	 */
	public void setGuarAblEval(String guarAblEval) {
		this.guarAblEval = guarAblEval;
	}
	
    /**
     * @return guarAblEval
     */
	public String getGuarAblEval() {
		return this.guarAblEval;
	}


}