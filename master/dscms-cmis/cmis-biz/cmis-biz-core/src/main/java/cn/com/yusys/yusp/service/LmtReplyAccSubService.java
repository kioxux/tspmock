/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtReplyAccSubDto;
import cn.com.yusys.yusp.dto.LmtReplyAccSubPrdDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtReplyAccSubMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-09 16:13:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyAccSubService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtReplyAccSubService.class);

    @Autowired
    private LmtReplyAccSubMapper lmtReplyAccSubMapper;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtReplyAccSub selectByPrimaryKey(String pkId) {
        return lmtReplyAccSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtReplyAccSub> selectAll(QueryModel model) {
        List<LmtReplyAccSub> records = (List<LmtReplyAccSub>) lmtReplyAccSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtReplyAccSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyAccSub> list = lmtReplyAccSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtReplyAccSub record) {
        return lmtReplyAccSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtReplyAccSub record) {
        return lmtReplyAccSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtReplyAccSub record) {
        return lmtReplyAccSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtReplyAccSub record) {
        return lmtReplyAccSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyAccSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyAccSubMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryLmtReplyAccSubByParams
     * @方法描述: 通过查询条件查询授批复台账分项信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReplyAccSub> queryLmtReplyAccSubByParams(HashMap<String, String> queryMap){
        return lmtReplyAccSubMapper.queryLmtReplyAccSubByParams(queryMap);
    }

    /**
     * @方法名称: selectByAccNo
     * @方法描述: 根据授信台帐号获取对应授信分项台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReplyAccSub> selectByAccNo(String accNo) {
        Map params = new HashMap();
        params.put("accNo", accNo);
        params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return lmtReplyAccSubMapper.queryLmtReplyAccSubByParams(params);
    }

    /**
     * @方法名称: selectByAccNo
     * @方法描述: 根据授信台帐号获取对应授信分项台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReplyAccSub> selectBySubserno(String subSerno) {
        Map params = new HashMap();
        params.put("subSerno", subSerno);
        params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return lmtReplyAccSubMapper.selectBySubserno(params);
    }

    /**
     * @方法名称: copyLmtAppSub
     * @方法描述: 发起授信申请变更时，将台账对应的授信分项挂载到新的授信变更申请下
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean copyToLmtAppSub(String accNo,String newSerno) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        Map params = new HashMap();
        params.put("accNo", accNo);
        params.put("oprType", CmisCommonConstants.ADD_OPR);
        String subSerno = "";
        String originAccSubSerno = "";
        LmtAppSub lmtAppSub = new LmtAppSub();
        LmtReplyAccSub lmtReplyAccSub = new LmtReplyAccSub();
            List<LmtReplyAccSub> lmtReplyAccSubList = lmtReplyAccSubMapper.queryLmtReplyAccSubByParams(params);
            if(lmtReplyAccSubList.size() > 0){
                for (int i = 0; i < lmtReplyAccSubList.size(); i++) {
                    lmtReplyAccSub = lmtReplyAccSubList.get(i);
                    originAccSubSerno = lmtReplyAccSub.getAccSubNo();
                    subSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
                    if (StringUtils.isEmpty(subSerno)) {
                        return false;
                    }
                    BeanUtils.beanCopy(lmtReplyAccSub, lmtAppSub);
                    lmtAppSub.setPkId(UUID.randomUUID().toString());
                    lmtAppSub.setSerno(newSerno);
                    lmtAppSub.setSubSerno(subSerno);
                    lmtAppSub.setSubName(lmtReplyAccSub.getAccSubName());
                    User userInfo = SessionUtils.getUserInformation();
                    if (userInfo == null) {
                        return false;
                    } else {
                        lmtAppSub.setInputId(userInfo.getLoginCode());
                        lmtAppSub.setInputBrId(userInfo.getOrg().getCode());
                        lmtAppSub.setInputDate(openday);
                        lmtAppSub.setUpdId(userInfo.getLoginCode());
                        lmtAppSub.setUpdBrId(userInfo.getOrg().getCode());
                        lmtAppSub.setUpdDate(openday);
                        lmtAppSub.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
                        lmtAppSub.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
                    }
                    int count = lmtAppSubService.insert(lmtAppSub);
                    boolean result = lmtReplyAccSubPrdService.copyToLmtAppSubPrd(originAccSubSerno,subSerno);
                    // TODO 判断授信适用产品 待补充
                    if (count != 1 || !result) {
                        //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                        throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",分项挂载失败！");
                    }
                }
            }
        return true;
    }

    /**
     * @方法名称: generateNewLmtReplyAccSub
     * @方法描述: 生成全新的授信批复分项台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public LmtReplyAccSub generateLmtReplyAccSubByLmtReplySub(LmtReplySub lmtReplySub, String accNo) {
        LmtReplyAccSub lmtReplyAccSub = new LmtReplyAccSub();
        BeanUtils.beanCopy(lmtReplySub, lmtReplyAccSub);
        lmtReplyAccSub.setPkId(UUID.randomUUID().toString());
        String accSubNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_DETAILS, new HashMap<>());
        log.info("生成新的授信台账分项流水号:" + accSubNo);
        lmtReplyAccSub.setAccSubNo(accSubNo);
        lmtReplyAccSub.setAccSubName(lmtReplySub.getSubName());
        lmtReplyAccSub.setAccNo(accNo);
        lmtReplyAccSub.setReplySerno(lmtReplySub.getReplySerno());
        lmtReplyAccSub.setReplySubSerno(lmtReplySub.getReplySubSerno());
        lmtReplyAccSub.setSerno(lmtReplySub.getSerno());
        lmtReplyAccSub.setSubSerno(lmtReplySub.getSubSerno());
        lmtReplyAccSubService.insert(lmtReplyAccSub);
        LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLmtReplyAccByAccNo(accNo);
        if(CmisCommonConstants.LMT_TYPE_03.equals(lmtReplyAcc.getLmtType())){
            if(StringUtils.nonEmpty(lmtReplySub.getOrigiLmtAccSubNo())){
                ctrLoanContService.updateLmtAccNoByOldLmtAccNo(lmtReplySub.getOrigiLmtAccSubNo(),lmtReplyAccSub.getAccSubNo(),lmtReplySub.getReplySerno());
            }
        }
        log.info("生成新的授信台账分项:" + accSubNo);
        return lmtReplyAccSub;
    }

    /**
     * @方法名称: generateLmtReplyAccSubHandle
     * @方法描述: 新生成批复台账业务处理方法
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void generateLmtReplyAccSubHandle(String replySerno, String accNo) throws Exception {
        // 通过查询授信批复分项，将查出的数据插入至批复分项台账中
        HashMap<String, String> queryLmtReplySubMap = new HashMap<String, String>();
        queryLmtReplySubMap.put("replySerno", replySerno);
        queryLmtReplySubMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplySub> lmtReplySubList = lmtReplySubService.queryLmtReplySubByParams(queryLmtReplySubMap);

        if(lmtReplySubList != null && lmtReplySubList.size() > 0){
            for (LmtReplySub lmtReplySub : lmtReplySubList) {
                LmtReplyAccSub lmtReplyAccSub = this.generateLmtReplyAccSubByLmtReplySub(lmtReplySub, accNo);
                // 通过查询授信批复分项适用品种，将查出的数据插入至批复分项台账适用品种中
                lmtReplyAccSubPrdService.generateNewLmtReplyAccSubPrdHandle(lmtReplySub.getReplySubSerno(), lmtReplyAccSub.getAccSubNo());
            }
        }else {
            throw new Exception("授信批复分项查询异常!");
        }
    }

    /**
     * @方法名称: updateLmtReplyAccSub
     * @方法描述: 更新全新的授信批复分项台账
     * @参数与返回说明:
     * @算法描述: 根据授信批复分项更新原批复台账数据，如下情况：
     *  1.授信批复分项数据存在，对应分项台账数据也存在，则直接一一对应更新分项台账数据
     *  2.授信批复分项数据存在，对应台账分项数据不存在，则新增授信分项台账数据
     *  3.授信批复分项数据不存在，对应台账分项数据存在，经过与李成金讨论，申请时不存在此类数据，将分项金额改为0即可。
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplyAccSubByLmtReplySub(LmtReply lmtReply, String accNo) throws Exception {
        // 通过查询授信批复分项，将查出的数据插入至批复分项台账中
        HashMap<String, String> queryLmtReplySubMap = new HashMap<String, String>();
        queryLmtReplySubMap.put("replySerno", lmtReply.getReplySerno());
        queryLmtReplySubMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 新生成的批复分项数据
        List<LmtReplySub> lmtReplySubList = lmtReplySubService.queryLmtReplySubByParams(queryLmtReplySubMap);
        // 根据原批复流水查询对应的台账分项数据
        queryLmtReplySubMap.replace("replySerno", lmtReply.getOrigiLmtReplySerno());
        List<LmtReplyAccSub> lmtReplyAccSubList =lmtReplyAccSubService.queryLmtReplyAccSubByParams(queryLmtReplySubMap);

        if(lmtReplySubList != null && lmtReplySubList.size() > 0 && lmtReplyAccSubList != null && lmtReplyAccSubList.size() > 0){
            log.info("循环最新的批复分项信息,批复编号:"+lmtReplySubList.get(0).getReplySerno());
            for (LmtReplySub lmtReplySub : lmtReplySubList) {
                LmtReplyAccSub lmtReplyAccSub = new LmtReplyAccSub();
                // 当前批复分项是否新增
                boolean isNew = true;
                for(LmtReplyAccSub item : lmtReplyAccSubList){
                    // 如果授信批复台账分项号与批复中的原批复台账分项号一致，则将批复分项内容复制至批复台账分项中
                    if(item.getAccSubNo().equals(lmtReplySub.getOrigiLmtAccSubNo())){
                        log.info("如果授信批复台账分项号与批复中的原批复台账分项号一致，则将批复分项内容复制至批复台账分项中."+lmtReplySub.getOrigiLmtAccSubNo());
                        BeanUtils.beanCopy(lmtReplySub, lmtReplyAccSub);
                        lmtReplyAccSub.setPkId(item.getPkId());
                        lmtReplyAccSub.setAccNo(item.getAccNo());
                        lmtReplyAccSub.setAccSubNo(item.getAccSubNo());
                        lmtReplyAccSub.setAccSubName(item.getAccSubName());
                        lmtReplyAccSub.setReplySerno(lmtReplySub.getReplySerno());
                        lmtReplyAccSub.setReplySubSerno(lmtReplySub.getReplySubSerno());
                        lmtReplyAccSub.setSerno(lmtReplySub.getSerno());
                        lmtReplyAccSub.setSubSerno(lmtReplySub.getSubSerno());
                        lmtReplyAccSub.setInputBrId(item.getInputBrId());
                        lmtReplyAccSub.setInputId(item.getInputId());
                        lmtReplyAccSub.setInputDate(item.getInputDate());
                        // 台账默认值
                        lmtReplyAccSub.setAccStatus(CmisCommonConstants.ACC_STATUS_1);
                        this.update(lmtReplyAccSub);
                        isNew = false;
                        break;
                    }
                }
                if(isNew){
                    // 如果是新增 则根据分项内容生成新的授信台账分项
                    lmtReplyAccSub = generateLmtReplyAccSubByLmtReplySub(lmtReplySub, accNo);
                    // 如果是新增 则根据批复分项适用品种内容生成新的授信台账分项适用品种
                    lmtReplyAccSubPrdService.generateNewLmtReplyAccSubPrdHandle(lmtReplySub.getReplySubSerno(), lmtReplyAccSub.getAccSubNo());
                }else{
                    // 更新授信台账批复适用品种
                    lmtReplyAccSubPrdService.updateLmtReplyAccSubPrdByLmtReplySubPrd(lmtReplySub, lmtReplyAccSub.getAccSubNo());
                }
            }
            log.info("循环原批复台账分项信息,批复台账编号:"+lmtReplyAccSubList.get(0).getAccNo());
            for(LmtReplyAccSub lmtReplyAccSub : lmtReplyAccSubList){
                boolean isExist = false;
                for(LmtReplySub lmtReplySub : lmtReplySubList){
                    if(lmtReplySub.getOrigiLmtAccSubNo().equals(lmtReplyAccSub.getAccSubNo())){
                        isExist = true;
                        break;
                    }
                }
                if(isExist){
                    continue;
                }else{
                    log.info("当前批复台账分项信息已不存在,对当前分项以及分项下品种信息执行逻辑删除操作:"+lmtReplyAccSub.getAccSubNo());
                    lmtReplyAccSub.setOprType(CmisCommonConstants.OP_TYPE_02);
                    lmtReplyAccSub.setLmtAmt(new BigDecimal(0));
                    lmtReplyAccSubService.updateSelective(lmtReplyAccSub);
                    List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.selectByAccSubNo(lmtReplyAccSub.getAccSubNo());
                    for(LmtReplyAccSubPrd lmtReplyAccSubPrd : lmtReplyAccSubPrdList){
                        lmtReplyAccSubPrd.setLmtAmt(new BigDecimal(0));
                        lmtReplyAccSubPrdService.updateSelective(lmtReplyAccSubPrd);
                    }
                }
            }
        }else {
            throw new Exception("授信批复分项或授信批复台账分项查询异常!");
        }
    }

    /**
     * @方法名称: getReplySubInfoByGrpSerno
     * @方法描述: 通过授信审批数据生成批复数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: yangwl
     * @创建时间: 2021-05-24 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public List<LmtReplyAccSubDto> getLmtReplyAccSubByGrpSerno(String grpAccNo) throws Exception {
        List<LmtReplyAccSubDto> lmtReplyAccSubDtoList = new ArrayList<>();
        List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpAccNo);
        if (lmtGrpMemRels != null && lmtGrpMemRels.size() != 0) {
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels) {
                String accNo = lmtGrpMemRel.getSingleSerno();
                if (StringUtils.isBlank(accNo)) {
                    throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
                }
                HashMap<String, String> queryMap = new HashMap<>();
                queryMap.put("accNo", accNo);
                List<LmtReplyAccSub> replyAccSubList = this.queryLmtReplyAccSubByParams(queryMap);
                if (replyAccSubList != null && replyAccSubList.size() != 0) {
                    for (LmtReplyAccSub lmtReplyAccSub : replyAccSubList) {
                        LmtReplyAccSubDto lmtReplyAccSubDto = new LmtReplyAccSubDto();
                        BeanUtils.beanCopy(lmtReplyAccSub, lmtReplyAccSubDto);
                        lmtReplyAccSubDto.setLmtDrawNo(lmtReplyAccSub.getAccSubNo());
                        lmtReplyAccSubDto.setLmtDrawType(lmtReplyAccSub.getAccSubName());
                        HashMap<String, String> queryPrdMap = new HashMap<>();
                        queryPrdMap.put("accSubNo", lmtReplyAccSub.getAccSubNo());
                        List<LmtReplyAccSubPrd> replySubPrdList = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByParams(queryPrdMap);
                        List<LmtReplyAccSubPrdDto> lmtReplyAccSubPrdDtoList = new ArrayList<>();
                        for (LmtReplyAccSubPrd lmtReplyAccSubPrd : replySubPrdList) {
                            LmtReplyAccSubPrdDto lmtReplyAccSubPrdDto = new LmtReplyAccSubPrdDto();
                            BeanUtils.beanCopy(lmtReplyAccSubPrd, lmtReplyAccSubPrdDto);
                            lmtReplyAccSubPrdDto.setLmtDrawNo(lmtReplyAccSubPrd.getAccSubPrdNo());
                            lmtReplyAccSubPrdDto.setLmtDrawType(lmtReplyAccSubPrd.getLmtBizTypeName());
                            lmtReplyAccSubPrdDtoList.add(lmtReplyAccSubPrdDto);
                        }
                        lmtReplyAccSubDto.setChildren(lmtReplyAccSubPrdDtoList);
                        lmtReplyAccSubDtoList.add(lmtReplyAccSubDto);
                    }
                }
            }
        }
        return lmtReplyAccSubDtoList;
    }

    /**
     * @方法名称: getSubandPrdBySingleSerno
     * @方法描述: 根据申请流水号获取批复分项信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: yangwl
     * @创建时间: 2021-06-26 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public List<LmtReplyAccSubDto> getSubandPrdBySingleSerno(String singleSerno) throws Exception {
        List<LmtReplyAccSubDto> lmtReplyAccSubDtoList = new ArrayList<>();
                HashMap<String, String> queryMap = new HashMap<>();
                queryMap.put("serno", singleSerno);
                List<LmtReplyAccSub> replyAccSubList = this.queryLmtReplyAccSubByParams(queryMap);
                if (replyAccSubList != null && replyAccSubList.size() != 0) {
                    lmtReplyAccSubDtoList = (List<LmtReplyAccSubDto>) BeanUtils.beansCopy(replyAccSubList, LmtReplyAccSubDto.class);
                    lmtReplyAccSubDtoList.forEach(lmtReplyAccSubDto -> {
                        String replySubSerno = lmtReplyAccSubDto.getReplySubSerno();
                        String accSubName = lmtReplyAccSubDto.getAccSubName();
                        if (StringUtils.nonBlank(accSubName)) {
                            lmtReplyAccSubDto.setLmtDrawType(accSubName);
                        }
                        if (!StringUtils.isBlank(replySubSerno)) {
                            HashMap<String, String> queryPrdMap = new HashMap<>();
                            queryPrdMap.put("replySubSerno", replySubSerno);
                            List<LmtReplyAccSubPrd> replySubPrdList = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByParams(queryPrdMap);
                            List<LmtReplyAccSubPrdDto> lmtReplyAccSubPrdDtoList = (List<LmtReplyAccSubPrdDto>) BeanUtils.beansCopy(replySubPrdList, LmtReplyAccSubPrdDto.class);
                            lmtReplyAccSubDto.setChildren(lmtReplyAccSubPrdDtoList);
                        }
                    });
                }
        return lmtReplyAccSubDtoList;
    }

    /**
     * @方法名称: getLmtReplyAccSubByAccNo
     * @方法描述: 根据台账编号获取授信批复台账分项信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtReplyAccSub getLmtReplyAccSubByAccSubNo(Map map) {
        LmtReplyAccSub lmtReplyAccSub = new LmtReplyAccSub();
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        if(map.get("accSubNo")==null || "".equals(map.get("accSubNo"))) {
            return lmtReplyAccSub;
        }
        lmtReplyAccSub = lmtReplyAccSubMapper.getLmtReplyAccSubByAccSubNo(map);
        return lmtReplyAccSub;
    }

    /**
     * @方法名称: getLmtReplyAccSubByAccNo
     * @方法描述: 根据台账编号获取授信批复台账分项信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtReplyAccSub selectLmtReplyAccSubByAccSubNo(Map map) {
        LmtReplyAccSub lmtReplyAccSub = new LmtReplyAccSub();
        if(map.get("accSubNo")==null || "".equals(map.get("accSubNo"))) {
            return lmtReplyAccSub;
        }
        lmtReplyAccSub = lmtReplyAccSubMapper.getLmtReplyAccSubByAccSubNo(map);
        return lmtReplyAccSub;
    }

    /**
     * @方法名称: getfdyddLmtReplyAccSubByAccSubNo
     * @方法描述: 获取房抵e抵贷授信批复台账分项信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     */
    public List<LmtReplyAccSub> getfdyddLmtReplyAccSubByAccSubNo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyAccSub> list = lmtReplyAccSubMapper.getfdyddLmtReplyAccSubByAccSubNo(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectLmtReplyAccSubDataByGrpAccNo
     * @方法描述: 根据集团台账流水号查询成员客户授信台账分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     */

    public List<LmtReplyAccSub> selectLmtReplyAccSubDataByGrpAccNo(String grpAccNo) {
        return lmtReplyAccSubMapper.selectLmtReplyAccSubDataByGrpAccNo(grpAccNo);
    }
}
