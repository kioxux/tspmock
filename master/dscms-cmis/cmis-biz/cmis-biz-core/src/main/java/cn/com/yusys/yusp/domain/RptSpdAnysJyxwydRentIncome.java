/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJyxwydRentIncome
 * @类描述: rpt_spd_anys_jyxwyd_rent_income数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 10:36:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_jyxwyd_rent_income")
public class RptSpdAnysJyxwydRentIncome extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 承租人 **/
	@Column(name = "LESSEE", unique = false, nullable = true, length = 80)
	private String lessee;
	
	/** 现有租赁合同期限 **/
	@Column(name = "TERM_OF_EXISTING_LEASE_CONTRACT", unique = false, nullable = true, length = 10)
	private Integer termOfExistingLeaseContract;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lessee
	 */
	public void setLessee(String lessee) {
		this.lessee = lessee;
	}
	
    /**
     * @return lessee
     */
	public String getLessee() {
		return this.lessee;
	}
	
	/**
	 * @param termOfExistingLeaseContract
	 */
	public void setTermOfExistingLeaseContract(Integer termOfExistingLeaseContract) {
		this.termOfExistingLeaseContract = termOfExistingLeaseContract;
	}
	
    /**
     * @return termOfExistingLeaseContract
     */
	public Integer getTermOfExistingLeaseContract() {
		return this.termOfExistingLeaseContract;
	}


}