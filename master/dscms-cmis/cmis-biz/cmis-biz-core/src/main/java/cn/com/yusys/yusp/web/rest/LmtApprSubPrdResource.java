/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtApprSubPrd;
import cn.com.yusys.yusp.service.LmtApprSubPrdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprSubPrdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:35:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtapprsubprd")
public class LmtApprSubPrdResource {
    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtApprSubPrd>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtApprSubPrd> list = lmtApprSubPrdService.selectAll(queryModel);
        return new ResultDto<List<LmtApprSubPrd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtApprSubPrd>> index(QueryModel queryModel) {
        List<LmtApprSubPrd> list = lmtApprSubPrdService.selectByModel(queryModel);
        return new ResultDto<List<LmtApprSubPrd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtApprSubPrd> show(@PathVariable("pkId") String pkId) {
        LmtApprSubPrd lmtApprSubPrd = lmtApprSubPrdService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtApprSubPrd>(lmtApprSubPrd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtApprSubPrd> create(@RequestBody LmtApprSubPrd lmtApprSubPrd) throws URISyntaxException {
        lmtApprSubPrdService.insert(lmtApprSubPrd);
        return new ResultDto<LmtApprSubPrd>(lmtApprSubPrd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtApprSubPrd lmtApprSubPrd) throws URISyntaxException {
        int result = lmtApprSubPrdService.updateSelective(lmtApprSubPrd);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchUpdate
     * @函数描述:批量对象修改
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchupdate")
    protected ResultDto<Integer> batchUpdate(@RequestBody List<LmtApprSubPrd> lmtApprSubPrdList) throws URISyntaxException {
        int result = lmtApprSubPrdService.batchUpdate(lmtApprSubPrdList);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtApprSubPrdService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtApprSubPrdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectBySubSerno
     * @函数描述:根据入参查询对象列表，公共API接口
     * @参数与返回说明:
     * @param map
     * 分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbysubserno")
    protected ResultDto<List<LmtApprSubPrd>> selectBySubSerno(@RequestBody  Map map) {
        String subSerno = (String) map.get("subSerno");
        List<LmtApprSubPrd> list = lmtApprSubPrdService.selectBySubSerno(subSerno.replaceAll("\"", ""));
        return new ResultDto<List<LmtApprSubPrd>>(list);
    }

    /**
     * @函数名称:deleteLmtAppSubPrd
     * @函数描述:删除适用授信产品
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletelmtapprsubprd")
    protected ResultDto<Map> deleteLmtApprSubPrd(@RequestBody LmtApprSubPrd lmtApprSubPrd) throws URISyntaxException{
        Map result = lmtApprSubPrdService.deleteByPkId(lmtApprSubPrd);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:addlmtapprsubprd
     * @函数描述:新增适用授信产品
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addlmtapprsubprd")
    protected ResultDto<Map> saveLmtApprSubPrd(@RequestBody LmtApprSubPrd lmtApprSubPrd) throws URISyntaxException {
        Map result = lmtApprSubPrdService.saveLmtApprSubPrd(lmtApprSubPrd);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:selectBySubPrdSerno
     * @函数描述:根据授信适用产品流水号获取对象
     * @参数与返回说明:
     * @param map
     * 分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbysubprdserno")
    protected ResultDto<LmtApprSubPrd> selectBySubPrdSerno(@RequestBody Map map) {
        String apprSubPrdSerno = (String) map.get("subPrdSerno");
        LmtApprSubPrd lmtApprSubPrd = lmtApprSubPrdService.selectBySubPrdSerno(apprSubPrdSerno);
        return new ResultDto<LmtApprSubPrd>(lmtApprSubPrd);
    }

    /**
     * @函数名称:updateLmtApprSubPrd
     * @函数描述:更新适用授信产品
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatelmtapprsubprd")
    protected ResultDto<Map> updateLmtApprSubPrd(@RequestBody LmtApprSubPrd lmtApprSubPrd) throws URISyntaxException{
        Map result = lmtApprSubPrdService.updateLmtApprSubPrd(lmtApprSubPrd);
        return new ResultDto<>(result);
    }
}
