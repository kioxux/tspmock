package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.PvpJxhjRepayLoan;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.PvpLoanAppRepayBillRell;
import cn.com.yusys.yusp.domain.RepayCapPlan;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * @author quwen
 * @version 1.0.0
 * @date 2021年8月3日16:04:21
 * @desc 偿还借据测算结果校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0119Service {

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private PvpJxhjRepayLoanService pvpJxhjRepayLoanService;

    @Autowired
    private PvpLoanAppRepayBillRellService pvpLoanAppRepayBillRellService;


    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author quwen
     * @date 2021年7月30日16:06:37
     * @version 1.0.0
     * @desc 借新还旧、无还本续贷、小企业无还本续贷，偿还借据测算结果校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0119(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String pvpSerno = (String)queryModel.getCondition().get("bizId");
        if (StringUtils.isBlank(pvpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 通过申请流水号获取申请信息
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPvpLoanSernoKey(pvpSerno);
        if(Objects.isNull(pvpLoanApp)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017); //通过合同申请流水号未获取到对应的合同申请信息
            return riskResultDto;
        }
        //判断贷款形式是否为借新还旧、无还本续贷、小企业无还本续贷业务
        String loanModal = pvpLoanApp.getLoanModal();
        if (CmisCommonConstants.STD_LOAN_MODAL_3.equals(loanModal) ||
                CmisCommonConstants.STD_LOAN_MODAL_6.equals(loanModal) ||
                CmisCommonConstants.STD_LOAN_MODAL_8.equals(loanModal)) {
            QueryModel queryModel1 = new QueryModel();
            queryModel1.addCondition("serno",pvpSerno);
            List<PvpLoanAppRepayBillRell> pvpLoanAppRepayBillRellList = pvpLoanAppRepayBillRellService.selectByModel(queryModel1);
            if(CollectionUtils.isEmpty(pvpLoanAppRepayBillRellList) || pvpLoanAppRepayBillRellList.size()>1){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_01190); //通过出账流水号未找到或找到多笔关联的借据
                return riskResultDto;
            }
            HashMap queryMap = new HashMap();
            queryMap.put("serno",pvpSerno);
            PvpJxhjRepayLoan pvpJxhjRepayLoan = pvpJxhjRepayLoanService.queryPvpJxhjRepayLoanDataByParams(queryMap);
            if(Objects.isNull(pvpJxhjRepayLoan)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_01191); //通过申请流水号未查询到关联的测算信息
                return riskResultDto;
            }else{
                BigDecimal newLoanAmt = new BigDecimal(pvpJxhjRepayLoan.getNewLoanAmt());//借新本金
                BigDecimal selfPrcp = new BigDecimal(pvpJxhjRepayLoan.getSelfPrcp());//自还本金
                BigDecimal nextPrcp = new BigDecimal(pvpJxhjRepayLoan.getNextPrcp());//未到期本金
                // 2021年11月17日23:11:46 hubp 问题编号：20211117-00125
                BigDecimal prcp = new BigDecimal(StringUtils.isBlank(pvpJxhjRepayLoan.getPrcp()) ? "0" : pvpJxhjRepayLoan.getPrcp());//拖欠本金
                if(newLoanAmt.compareTo(pvpLoanApp.getCvtCnyAmt()) != 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_01192); //借新本金必须等于本次出账金额
                    return riskResultDto;
                }
                if(nextPrcp.compareTo((newLoanAmt.add(selfPrcp)).subtract(prcp)) < 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_01193); //借新本金+自还本金必须等于未到期本金+拖欠本金
                    return riskResultDto;
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
