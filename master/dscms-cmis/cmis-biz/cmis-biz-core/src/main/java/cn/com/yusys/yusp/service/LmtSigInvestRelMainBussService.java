/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestRelMainBuss;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestRelMainBussMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRelMainBussService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 11:18:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestRelMainBussService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestRelMainBussMapper lmtSigInvestRelMainBussMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestRelMainBuss selectByPrimaryKey(String pkId) {
        return lmtSigInvestRelMainBussMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestRelMainBuss> selectAll(QueryModel model) {
        List<LmtSigInvestRelMainBuss> records = (List<LmtSigInvestRelMainBuss>) lmtSigInvestRelMainBussMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestRelMainBuss> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestRelMainBuss> list = lmtSigInvestRelMainBussMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * 前三大主营业务情况占比 只能添加3条限制  总百分比不超过100%(与客户沟通后不做该限制)
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public int insert(LmtSigInvestRelMainBuss record) {
        //checkCanInsert(record);
        record.setPkId(generatePkId());
        record.setOprType(CmisBizConstants.OPR_TYPE_01);
        record.setCreateTime(getCurrrentDate());
        record.setUpdateTime(getCurrrentDate());
        record.setInputDate(getCurrrentDateStr());
        record.setInputId(getCurrentUser().getLoginCode());
        record.setInputBrId(getCurrentUser().getOrg().getCode());
        record.setUpdBrId(getCurrentUser().getOrg().getCode());
        record.setUpdId(getCurrentUser().getLoginCode());
        record.setUpdDate(getCurrrentDateStr());
        return lmtSigInvestRelMainBussMapper.insert(record);
    }

    /**
     * 1)列表数据应小于等于三条。
     * 2)占比之和不能超过100%
     * @param record
     */
    private void checkCanInsert(LmtSigInvestRelMainBuss record) {
        String serno = record.getSerno();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestRelMainBuss> lmtSigInvestRelMainBusses = selectAll(queryModel);
        if (lmtSigInvestRelMainBusses != null && lmtSigInvestRelMainBusses.size() > 0) {
            //1)列表数据应小于等于三条。
            if (lmtSigInvestRelMainBusses.size() == 3){
                throw BizException.error(null, EclEnum.LMT_SIG_INVEST_MAINBUSS_UP_3.key,EclEnum.LMT_SIG_INVEST_MAINBUSS_UP_3.value);
            }
            //2)占比之和不能超过100%
            BigDecimal scale = BigDecimal.ZERO;
            for (LmtSigInvestRelMainBuss lmtSigInvestRelMainBuss : lmtSigInvestRelMainBusses) {
                scale = scale.add(lmtSigInvestRelMainBuss.getBusiRate());
            }
            if (scale.compareTo(new BigDecimal(1)) > 0){
                throw BizException.error(null,EclEnum.LMT_SIG_INVEST_MAINBUSS_SUM_MOVE_100.key,EclEnum.LMT_SIG_INVEST_MAINBUSS_SUM_MOVE_100.value);
            }
        }
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestRelMainBuss record) {
        return lmtSigInvestRelMainBussMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestRelMainBuss record) {
        return lmtSigInvestRelMainBussMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestRelMainBuss record) {
        return lmtSigInvestRelMainBussMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestRelMainBussMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestRelMainBussMapper.deleteByIds(ids);
    }

    /**
     * 单个对象逻辑删除
     * @param pkId
     * @return
     */
    public int deleteLogicByPkId(String pkId) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("pkId",pkId);
        BizInvestCommonService.checkParamsIsNull("pkId",pkId);
        List<LmtSigInvestRelMainBuss> lmtSigInvestRelMainBusses = selectAll(queryModel);
        if (lmtSigInvestRelMainBusses!=null && lmtSigInvestRelMainBusses.size()>0){
            LmtSigInvestRelMainBuss lmtSigInvestRelMainBuss = lmtSigInvestRelMainBusses.get(0);
            lmtSigInvestRelMainBuss.setOprType(CmisBizConstants.OPR_TYPE_02);
            return update(lmtSigInvestRelMainBuss);
        }
        return 0;
    }
}
