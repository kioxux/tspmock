/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.AccEntrustLoanTxnDetails;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3112.Ln3112RespDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CtrLoanExt;
import cn.com.yusys.yusp.service.CtrLoanExtService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLoanExtResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-12 11:01:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/ctrloanext")
public class CtrLoanExtResource {
    @Autowired
    private CtrLoanExtService ctrLoanExtService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrLoanExt>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrLoanExt> list = ctrLoanExtService.selectAll(queryModel);
        return new ResultDto<List<CtrLoanExt>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrLoanExt>> index(QueryModel queryModel) {
        List<CtrLoanExt> list = ctrLoanExtService.selectByModel(queryModel);
        return new ResultDto<List<CtrLoanExt>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{extCtrNo}")
    protected ResultDto<CtrLoanExt> show(@PathVariable("extCtrNo") String extCtrNo) {
        CtrLoanExt ctrLoanExt = ctrLoanExtService.selectByPrimaryKey(extCtrNo);
        return new ResultDto<CtrLoanExt>(ctrLoanExt);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrLoanExt> create(@RequestBody CtrLoanExt ctrLoanExt) throws URISyntaxException {
        ctrLoanExtService.insert(ctrLoanExt);
        return new ResultDto<CtrLoanExt>(ctrLoanExt);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrLoanExt ctrLoanExt) throws URISyntaxException {
        int result = ctrLoanExtService.update(ctrLoanExt);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{extCtrNo}")
    protected ResultDto<Integer> delete(@PathVariable("extCtrNo") String extCtrNo) {
        int result = ctrLoanExtService.deleteByPrimaryKey(extCtrNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrLoanExtService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<CtrLoanExt>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<CtrLoanExt>>(ctrLoanExtService.selectByModel(model));
    }
}
