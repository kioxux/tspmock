package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanApp
 * @类描述: iqp_loan_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-06 14:13:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpLoanAppDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 业务申请流水号 **/
	private String iqpSerno;

	/** 全局流水号 **/
	private String serno;

	/** 原申请流水号 **/
	private String oldIqpSerno;

	/** 是否复议 **/
	private String isReconsid;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 证件号码 **/
	private String certCode;

	/** 手机号码 **/
	private String phone;

	/** 业务类型 **/
	private String bizType;

	/** 特殊业务类型 **/
	private String especBizType;

	/** 申请日期 **/
	private String appDate;

	/** 产品编号 **/
	private String prdId;

	/** 产品名称 **/
	private String prdName;

	/** 贷款用途 **/
	private String loanPurp;

	/** 贷款形式 **/
	private String loanModal;

	/** 贷款性质 **/
	private String loanCha;

	/** 是否申请优惠利率 **/
	private String preferRateFlag;

	/** 利率调整日 **/
	private String rateAdjDate;

	/** 其他消费贷款月还款额 **/
	private java.math.BigDecimal otherComsumeRepayAmt;

	/** 本笔公积金贷款月还款额 **/
	private java.math.BigDecimal pundLoanMonRepayAmt;

	/** 本笔月还款额 **/
	private java.math.BigDecimal monthRepayAmt;

	/** 是否在线抵押 **/
	private String isOlPld;

	/** 合同模式 **/
	private String contMode;

	/** 第三方标识 **/
	private String thirdPartyFlag;

	/** 项目编号 **/
	private String proNo;

	/** 项目流水号 **/
	private String proSerno;

	/** 调查人意见 **/
	private String inveAdvice;

	/** 个人信用情况其他说明 **/
	private String indivCdtExpl;

	/** 批复编号 **/
	private String replyNo;

	/** 调查编号 **/
	private String surveySerno;

	/** 合同类型 **/
	private String contType;

	/** 币种 **/
	private String curType;

	/** 合同金额 **/
	private java.math.BigDecimal contAmt;

	/** 合同期限 **/
	private Integer contTerm;

	/** 起始日 **/
	private String startDate;

	/** 到期日 **/
	private String endDate;

	/** 签约方式 **/
	private String signMode;

	/** 贷款投向 **/
	private String loanTer;

	/** 其他约定 **/
	private String otherAgreed;

	/** 还款方式 **/
	private String repayMode;

	/** 借款种类 **/
	private String loanType;

	/** 浮动点数 **/
	private java.math.BigDecimal rateFloatPoint;

	/** 结息方式 **/
	private String eiMode;

	/** 贷款发放账号 **/
	private String loanPayoutAccno;

	/** 贷款发放账号名称 **/
	private String loanPayoutAccName;

	/** 是否曾被拒绝 **/
	private String isHasRefused;

	/** 主担保方式 **/
	private String guarWay;

	/** 是否共同申请人 **/
	private String isCommonRqstr;

	/** 是否确认支付方式 **/
	private String isCfirmPayWay;

	/** 支付方式 **/
	private String payMode;

	/** 申请币种 **/
	private String appCurType;

	/** 申请金额 **/
	private java.math.BigDecimal appAmt;

	/** 申请汇率 **/
	private java.math.BigDecimal appRate;

	/** 保证金来源 **/
	private String bailSour;

	/** 保证金比例 **/
	private java.math.BigDecimal bailPerc;

	/** 保证金币种 **/
	private String bailCurType;

	/** 保证金金额 **/
	private java.math.BigDecimal bailAmt;

	/** 保证金汇率 **/
	private java.math.BigDecimal bailExchangeRate;

	/** 风险敞口金额 **/
	private java.math.BigDecimal riskOpenAmt;

	/** 期限类型 **/
	private String termType;

	/** 申请期限 **/
	private java.math.BigDecimal appTerm;

	/** 利率依据方式 **/
	private String irAccordType;

	/** 利率种类 **/
	private String irType;

	/** 基准利率（年） **/
	private java.math.BigDecimal rulingIr;

	/** 对应基准利率(月) **/
	private java.math.BigDecimal rulingIrM;

	/** 计息方式 **/
	private String loanRatType;

	/** 利率调整类型 **/
	private String irAdjustType;

	/** 利率调整周期(月) **/
	private Integer irAdjustTerm;

	/** 调息方式 **/
	private String praType;

	/** 利率形式 **/
	private String rateType;

	/** 利率浮动方式 **/
	private String irFloatType;

	/** 利率浮动百分比 **/
	private java.math.BigDecimal irFloatRate;

	/** 固定加点值 **/
	private java.math.BigDecimal irFloatPoint;

	/** 执行年利率 **/
	private java.math.BigDecimal execRateYear;

	/** 执行利率(月) **/
	private java.math.BigDecimal realityIrM;

	/** 逾期利率浮动方式 **/
	private String overdueFloatType;

	/** 逾期利率加点值 **/
	private java.math.BigDecimal overduePoint;

	/** 逾期利率浮动百分比 **/
	private java.math.BigDecimal overdueRate;

	/** 逾期利率（年） **/
	private java.math.BigDecimal overdueRateY;

	/** 违约利率浮动方式 **/
	private String defaultFloatType;

	/** 违约利率浮动加点值 **/
	private java.math.BigDecimal defaultPoint;

	/** 违约利率浮动百分比 **/
	private java.math.BigDecimal defaultRate;

	/** 违约利率（年） **/
	private java.math.BigDecimal defaultRateY;

	/** 申请状态 **/
	private String approveStatus;

	/** 审批通过日期（精确到秒） **/
	private String approvePassDate;

	/** 本合同项下最高可用信金额 **/
	private java.math.BigDecimal contHighAvlAmt;

	/** 是否续签 **/
	private String isRenew;

	/** 原合同编号 **/
	private String origiContNo;

	/** 是否使用授信额度 **/
	private String isUtilLmt;

	/** 授信额度编号 **/
	private String lmtAccNo;

	/** 是否电子用印 **/
	private String isESeal;

	/** 所属条线 **/
	private String belgLine;

	/** 债项等级 **/
	private String debtLevel;

	/** 违约风险暴露EAD **/
	private java.math.BigDecimal ead;

	/** 违约损失率LGD **/
	private java.math.BigDecimal lgd;

	/** 联系人 **/
	private String linkman;

	/** 传真 **/
	private String fax;

	/** 邮箱 **/
	private String email;

	/** QQ **/
	private String qq;

	/** 微信 **/
	private String wechat;

	/** 送达地址 **/
	private String deliveryAddr;

	/** 本行角色 **/
	private String bankRole;

	/** 银团总金额 **/
	private java.math.BigDecimal bksyndicTotlAmt;

	/** 还款顺序 **/
	private String repaySeq;

	/** 银团纸质合同编号 **/
	private String bksyndicPaperContNo;

	/** 借款利率调整日 **/
	private String loanRateAdjDay;

	/** LPR利率区间 **/
	private String lprRateIntval;

	/** 当前LPR利率 **/
	private java.math.BigDecimal curtLprRate;

	/** LPR浮动点(BP) **/
	private java.math.BigDecimal lprBp;

	/** 提款方式  **/
	private String drawMode;

	/** 提款期限 **/
	private String drawTerm;

	/** 开户行名称 **/
	private String acctsvcrName;

	/** 还款具体说明 **/
	private String repayDetail;

	/** 目标企业 **/
	private String targetCorp;

	/** 并购协议 **/
	private String mergerAgr;

	/** 并购交易价款 **/
	private java.math.BigDecimal mergerTranAmt;

	/** 是否占用第三方额度 **/
	private String isOutstndTrdLmtAmt;

	/** 第三方合同协议编号 **/
	private String tdpAgrNo;

	/** 合作方客户编号 **/
	private String coopCusId;

	/** 合作方客户名称 **/
	private String coopCusName;

	/** 是否无缝对接 **/
	private String isSeajnt;

	/** 折算人民币金额 **/
	private java.math.BigDecimal cvtCnyAmt;

	/** 用途分析 **/
	private String purpAnaly;

	/** 交叉核验详细分析 **/
	private String crossChkDetailAnaly;

	/** 还款来源 **/
	private String repaySour;

	/** 调查人结论 **/
	private String inveConclu;

	/** 操作类型 **/
	private String oprType;

	/** 主管机构 **/
	private String managerBrId;

	/** 主管客户经理 **/
	private String managerId;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最后修改人 **/
	private String updId;

	/** 最后修改机构 **/
	private String updBrId;

	/** 最后修改日期 **/
	private String updDate;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;

	/** 首付/定金 **/
	private java.math.BigDecimal firstpayAmt;

	/** 合同总价 **/
	private java.math.BigDecimal contTotalAmt;

	/** 宽限期 **/
	private String graper;

	/** 是否公积金组合贷款 **/
	private String fundUnionFlag;

	/** 是否线上提款 **/
	private String isOnlineDraw;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}

	/**
	 * @return Serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}

	/**
	 * @return IqpSerno
	 */
	public String getIqpSerno() {
		return this.iqpSerno;
	}

	/**
	 * @param oldIqpSerno
	 */
	public void setOldIqpSerno(String oldIqpSerno) {
		this.oldIqpSerno = oldIqpSerno == null ? null : oldIqpSerno.trim();
	}

	/**
	 * @return OldIqpSerno
	 */
	public String getOldIqpSerno() {
		return this.oldIqpSerno;
	}

	/**
	 * @param isReconsid
	 */
	public void setIsReconsid(String isReconsid) {
		this.isReconsid = isReconsid == null ? null : isReconsid.trim();
	}

	/**
	 * @return IsReconsid
	 */
	public String getIsReconsid() {
		return this.isReconsid;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}

	/**
	 * @return CusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}

	/**
	 * @return CusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}

	/**
	 * @return CertCode
	 */
	public String getCertCode() {
		return this.certCode;
	}

	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}

	/**
	 * @return Phone
	 */
	public String getPhone() {
		return this.phone;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType == null ? null : bizType.trim();
	}

	/**
	 * @return BizType
	 */
	public String getBizType() {
		return this.bizType;
	}

	/**
	 * @param especBizType
	 */
	public void setEspecBizType(String especBizType) {
		this.especBizType = especBizType == null ? null : especBizType.trim();
	}

	/**
	 * @return EspecBizType
	 */
	public String getEspecBizType() {
		return this.especBizType;
	}

	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate == null ? null : appDate.trim();
	}

	/**
	 * @return AppDate
	 */
	public String getAppDate() {
		return this.appDate;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}

	/**
	 * @return PrdId
	 */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}

	/**
	 * @return PrdName
	 */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp == null ? null : loanPurp.trim();
	}

	/**
	 * @return LoanPurp
	 */
	public String getLoanPurp() {
		return this.loanPurp;
	}

	/**
	 * @param loanModal
	 */
	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal == null ? null : loanModal.trim();
	}

	/**
	 * @return LoanModal
	 */
	public String getLoanModal() {
		return this.loanModal;
	}

	/**
	 * @param loanCha
	 */
	public void setLoanCha(String loanCha) {
		this.loanCha = loanCha == null ? null : loanCha.trim();
	}

	/**
	 * @return LoanCha
	 */
	public String getLoanCha() {
		return this.loanCha;
	}

	/**
	 * @param preferRateFlag
	 */
	public void setPreferRateFlag(String preferRateFlag) {
		this.preferRateFlag = preferRateFlag == null ? null : preferRateFlag.trim();
	}

	/**
	 * @return PreferRateFlag
	 */
	public String getPreferRateFlag() {
		return this.preferRateFlag;
	}

	/**
	 * @param rateAdjDate
	 */
	public void setRateAdjDate(String rateAdjDate) {
		this.rateAdjDate = rateAdjDate == null ? null : rateAdjDate.trim();
	}

	/**
	 * @return RateAdjDate
	 */
	public String getRateAdjDate() {
		return this.rateAdjDate;
	}

	/**
	 * @param otherComsumeRepayAmt
	 */
	public void setOtherComsumeRepayAmt(java.math.BigDecimal otherComsumeRepayAmt) {
		this.otherComsumeRepayAmt = otherComsumeRepayAmt;
	}

	/**
	 * @return OtherComsumeRepayAmt
	 */
	public java.math.BigDecimal getOtherComsumeRepayAmt() {
		return this.otherComsumeRepayAmt;
	}

	/**
	 * @param pundLoanMonRepayAmt
	 */
	public void setPundLoanMonRepayAmt(java.math.BigDecimal pundLoanMonRepayAmt) {
		this.pundLoanMonRepayAmt = pundLoanMonRepayAmt;
	}

	/**
	 * @return PundLoanMonRepayAmt
	 */
	public java.math.BigDecimal getPundLoanMonRepayAmt() {
		return this.pundLoanMonRepayAmt;
	}

	/**
	 * @param monthRepayAmt
	 */
	public void setMonthRepayAmt(java.math.BigDecimal monthRepayAmt) {
		this.monthRepayAmt = monthRepayAmt;
	}

	/**
	 * @return MonthRepayAmt
	 */
	public java.math.BigDecimal getMonthRepayAmt() {
		return this.monthRepayAmt;
	}

	/**
	 * @param isOlPld
	 */
	public void setIsOlPld(String isOlPld) {
		this.isOlPld = isOlPld == null ? null : isOlPld.trim();
	}

	/**
	 * @return IsOlPld
	 */
	public String getIsOlPld() {
		return this.isOlPld;
	}

	/**
	 * @param contMode
	 */
	public void setContMode(String contMode) {
		this.contMode = contMode == null ? null : contMode.trim();
	}

	/**
	 * @return ContMode
	 */
	public String getContMode() {
		return this.contMode;
	}

	/**
	 * @param thirdPartyFlag
	 */
	public void setThirdPartyFlag(String thirdPartyFlag) {
		this.thirdPartyFlag = thirdPartyFlag == null ? null : thirdPartyFlag.trim();
	}

	/**
	 * @return ThirdPartyFlag
	 */
	public String getThirdPartyFlag() {
		return this.thirdPartyFlag;
	}

	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}

	/**
	 * @return ProNo
	 */
	public String getProNo() {
		return this.proNo;
	}

	/**
	 * @param proSerno
	 */
	public void setProSerno(String proSerno) {
		this.proSerno = proSerno == null ? null : proSerno.trim();
	}

	/**
	 * @return ProSerno
	 */
	public String getProSerno() {
		return this.proSerno;
	}

	/**
	 * @param inveAdvice
	 */
	public void setInveAdvice(String inveAdvice) {
		this.inveAdvice = inveAdvice == null ? null : inveAdvice.trim();
	}

	/**
	 * @return InveAdvice
	 */
	public String getInveAdvice() {
		return this.inveAdvice;
	}

	/**
	 * @param indivCdtExpl
	 */
	public void setIndivCdtExpl(String indivCdtExpl) {
		this.indivCdtExpl = indivCdtExpl == null ? null : indivCdtExpl.trim();
	}

	/**
	 * @return IndivCdtExpl
	 */
	public String getIndivCdtExpl() {
		return this.indivCdtExpl;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo == null ? null : replyNo.trim();
	}

	/**
	 * @return ReplyNo
	 */
	public String getReplyNo() {
		return this.replyNo;
	}

	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}

	/**
	 * @return SurveySerno
	 */
	public String getSurveySerno() {
		return this.surveySerno;
	}

	/**
	 * @param contType
	 */
	public void setContType(String contType) {
		this.contType = contType == null ? null : contType.trim();
	}

	/**
	 * @return ContType
	 */
	public String getContType() {
		return this.contType;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}

	/**
	 * @return CurType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	/**
	 * @return ContAmt
	 */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}

	/**
	 * @param contTerm
	 */
	public void setContTerm(Integer contTerm) {
		this.contTerm = contTerm;
	}

	/**
	 * @return ContTerm
	 */
	public Integer getContTerm() {
		return this.contTerm;
	}

	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}

	/**
	 * @return StartDate
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}

	/**
	 * @return EndDate
	 */
	public String getEndDate() {
		return this.endDate;
	}

	/**
	 * @param signMode
	 */
	public void setSignMode(String signMode) {
		this.signMode = signMode == null ? null : signMode.trim();
	}

	/**
	 * @return SignMode
	 */
	public String getSignMode() {
		return this.signMode;
	}

	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer == null ? null : loanTer.trim();
	}

	/**
	 * @return LoanTer
	 */
	public String getLoanTer() {
		return this.loanTer;
	}

	/**
	 * @param otherAgreed
	 */
	public void setOtherAgreed(String otherAgreed) {
		this.otherAgreed = otherAgreed == null ? null : otherAgreed.trim();
	}

	/**
	 * @return OtherAgreed
	 */
	public String getOtherAgreed() {
		return this.otherAgreed;
	}

	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode == null ? null : repayMode.trim();
	}

	/**
	 * @return RepayMode
	 */
	public String getRepayMode() {
		return this.repayMode;
	}

	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType == null ? null : loanType.trim();
	}

	/**
	 * @return LoanType
	 */
	public String getLoanType() {
		return this.loanType;
	}

	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}

	/**
	 * @return RateFloatPoint
	 */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}

	/**
	 * @param eiMode
	 */
	public void setEiMode(String eiMode) {
		this.eiMode = eiMode == null ? null : eiMode.trim();
	}

	/**
	 * @return EiMode
	 */
	public String getEiMode() {
		return this.eiMode;
	}

	/**
	 * @param loanPayoutAccno
	 */
	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno == null ? null : loanPayoutAccno.trim();
	}

	/**
	 * @return LoanPayoutAccno
	 */
	public String getLoanPayoutAccno() {
		return this.loanPayoutAccno;
	}

	/**
	 * @param loanPayoutAccName
	 */
	public void setLoanPayoutAccName(String loanPayoutAccName) {
		this.loanPayoutAccName = loanPayoutAccName == null ? null : loanPayoutAccName.trim();
	}

	/**
	 * @return LoanPayoutAccName
	 */
	public String getLoanPayoutAccName() {
		return this.loanPayoutAccName;
	}

	/**
	 * @param isHasRefused
	 */
	public void setIsHasRefused(String isHasRefused) {
		this.isHasRefused = isHasRefused == null ? null : isHasRefused.trim();
	}

	/**
	 * @return IsHasRefused
	 */
	public String getIsHasRefused() {
		return this.isHasRefused;
	}

	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay == null ? null : guarWay.trim();
	}

	/**
	 * @return GuarWay
	 */
	public String getGuarWay() {
		return this.guarWay;
	}

	/**
	 * @param isCommonRqstr
	 */
	public void setIsCommonRqstr(String isCommonRqstr) {
		this.isCommonRqstr = isCommonRqstr == null ? null : isCommonRqstr.trim();
	}

	/**
	 * @return IsCommonRqstr
	 */
	public String getIsCommonRqstr() {
		return this.isCommonRqstr;
	}

	/**
	 * @param isCfirmPayWay
	 */
	public void setIsCfirmPayWay(String isCfirmPayWay) {
		this.isCfirmPayWay = isCfirmPayWay == null ? null : isCfirmPayWay.trim();
	}

	/**
	 * @return IsCfirmPayWay
	 */
	public String getIsCfirmPayWay() {
		return this.isCfirmPayWay;
	}

	/**
	 * @param payMode
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode == null ? null : payMode.trim();
	}

	/**
	 * @return PayMode
	 */
	public String getPayMode() {
		return this.payMode;
	}

	/**
	 * @param appCurType
	 */
	public void setAppCurType(String appCurType) {
		this.appCurType = appCurType == null ? null : appCurType.trim();
	}

	/**
	 * @return AppCurType
	 */
	public String getAppCurType() {
		return this.appCurType;
	}

	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}

	/**
	 * @return AppAmt
	 */
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}

	/**
	 * @param appRate
	 */
	public void setAppRate(java.math.BigDecimal appRate) {
		this.appRate = appRate;
	}

	/**
	 * @return AppRate
	 */
	public java.math.BigDecimal getAppRate() {
		return this.appRate;
	}

	/**
	 * @param bailSour
	 */
	public void setBailSour(String bailSour) {
		this.bailSour = bailSour == null ? null : bailSour.trim();
	}

	/**
	 * @return BailSour
	 */
	public String getBailSour() {
		return this.bailSour;
	}

	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}

	/**
	 * @return BailPerc
	 */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}

	/**
	 * @param bailCurType
	 */
	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType == null ? null : bailCurType.trim();
	}

	/**
	 * @return BailCurType
	 */
	public String getBailCurType() {
		return this.bailCurType;
	}

	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}

	/**
	 * @return BailAmt
	 */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}

	/**
	 * @param bailExchangeRate
	 */
	public void setBailExchangeRate(java.math.BigDecimal bailExchangeRate) {
		this.bailExchangeRate = bailExchangeRate;
	}

	/**
	 * @return BailExchangeRate
	 */
	public java.math.BigDecimal getBailExchangeRate() {
		return this.bailExchangeRate;
	}

	/**
	 * @param riskOpenAmt
	 */
	public void setRiskOpenAmt(java.math.BigDecimal riskOpenAmt) {
		this.riskOpenAmt = riskOpenAmt;
	}

	/**
	 * @return RiskOpenAmt
	 */
	public java.math.BigDecimal getRiskOpenAmt() {
		return this.riskOpenAmt;
	}

	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType == null ? null : termType.trim();
	}

	/**
	 * @return TermType
	 */
	public String getTermType() {
		return this.termType;
	}

	/**
	 * @param appTerm
	 */
	public void setAppTerm(java.math.BigDecimal appTerm) {
		this.appTerm = appTerm;
	}

	/**
	 * @return AppTerm
	 */
	public java.math.BigDecimal getAppTerm() {
		return this.appTerm;
	}

	/**
	 * @param irAccordType
	 */
	public void setIrAccordType(String irAccordType) {
		this.irAccordType = irAccordType == null ? null : irAccordType.trim();
	}

	/**
	 * @return IrAccordType
	 */
	public String getIrAccordType() {
		return this.irAccordType;
	}

	/**
	 * @param irType
	 */
	public void setIrType(String irType) {
		this.irType = irType == null ? null : irType.trim();
	}

	/**
	 * @return IrType
	 */
	public String getIrType() {
		return this.irType;
	}

	/**
	 * @param rulingIr
	 */
	public void setRulingIr(java.math.BigDecimal rulingIr) {
		this.rulingIr = rulingIr;
	}

	/**
	 * @return RulingIr
	 */
	public java.math.BigDecimal getRulingIr() {
		return this.rulingIr;
	}

	/**
	 * @param rulingIrM
	 */
	public void setRulingIrM(java.math.BigDecimal rulingIrM) {
		this.rulingIrM = rulingIrM;
	}

	/**
	 * @return RulingIrM
	 */
	public java.math.BigDecimal getRulingIrM() {
		return this.rulingIrM;
	}

	/**
	 * @param loanRatType
	 */
	public void setLoanRatType(String loanRatType) {
		this.loanRatType = loanRatType == null ? null : loanRatType.trim();
	}

	/**
	 * @return LoanRatType
	 */
	public String getLoanRatType() {
		return this.loanRatType;
	}

	/**
	 * @param irAdjustType
	 */
	public void setIrAdjustType(String irAdjustType) {
		this.irAdjustType = irAdjustType == null ? null : irAdjustType.trim();
	}

	/**
	 * @return IrAdjustType
	 */
	public String getIrAdjustType() {
		return this.irAdjustType;
	}

	/**
	 * @param irAdjustTerm
	 */
	public void setIrAdjustTerm(Integer irAdjustTerm) {
		this.irAdjustTerm = irAdjustTerm;
	}

	/**
	 * @return IrAdjustTerm
	 */
	public Integer getIrAdjustTerm() {
		return this.irAdjustTerm;
	}

	/**
	 * @param praType
	 */
	public void setPraType(String praType) {
		this.praType = praType == null ? null : praType.trim();
	}

	/**
	 * @return PraType
	 */
	public String getPraType() {
		return this.praType;
	}

	/**
	 * @param rateType
	 */
	public void setRateType(String rateType) {
		this.rateType = rateType == null ? null : rateType.trim();
	}

	/**
	 * @return RateType
	 */
	public String getRateType() {
		return this.rateType;
	}

	/**
	 * @param irFloatType
	 */
	public void setIrFloatType(String irFloatType) {
		this.irFloatType = irFloatType == null ? null : irFloatType.trim();
	}

	/**
	 * @return IrFloatType
	 */
	public String getIrFloatType() {
		return this.irFloatType;
	}

	/**
	 * @param irFloatRate
	 */
	public void setIrFloatRate(java.math.BigDecimal irFloatRate) {
		this.irFloatRate = irFloatRate;
	}

	/**
	 * @return IrFloatRate
	 */
	public java.math.BigDecimal getIrFloatRate() {
		return this.irFloatRate;
	}

	/**
	 * @param irFloatPoint
	 */
	public void setIrFloatPoint(java.math.BigDecimal irFloatPoint) {
		this.irFloatPoint = irFloatPoint;
	}

	/**
	 * @return IrFloatPoint
	 */
	public java.math.BigDecimal getIrFloatPoint() {
		return this.irFloatPoint;
	}

	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}

	/**
	 * @return ExecRateYear
	 */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}

	/**
	 * @param realityIrM
	 */
	public void setRealityIrM(java.math.BigDecimal realityIrM) {
		this.realityIrM = realityIrM;
	}

	/**
	 * @return RealityIrM
	 */
	public java.math.BigDecimal getRealityIrM() {
		return this.realityIrM;
	}

	/**
	 * @param overdueFloatType
	 */
	public void setOverdueFloatType(String overdueFloatType) {
		this.overdueFloatType = overdueFloatType == null ? null : overdueFloatType.trim();
	}

	/**
	 * @return OverdueFloatType
	 */
	public String getOverdueFloatType() {
		return this.overdueFloatType;
	}

	/**
	 * @param overduePoint
	 */
	public void setOverduePoint(java.math.BigDecimal overduePoint) {
		this.overduePoint = overduePoint;
	}

	/**
	 * @return OverduePoint
	 */
	public java.math.BigDecimal getOverduePoint() {
		return this.overduePoint;
	}

	/**
	 * @param overdueRate
	 */
	public void setOverdueRate(java.math.BigDecimal overdueRate) {
		this.overdueRate = overdueRate;
	}

	/**
	 * @return OverdueRate
	 */
	public java.math.BigDecimal getOverdueRate() {
		return this.overdueRate;
	}

	/**
	 * @param overdueRateY
	 */
	public void setOverdueRateY(java.math.BigDecimal overdueRateY) {
		this.overdueRateY = overdueRateY;
	}

	/**
	 * @return OverdueRateY
	 */
	public java.math.BigDecimal getOverdueRateY() {
		return this.overdueRateY;
	}

	/**
	 * @param defaultFloatType
	 */
	public void setDefaultFloatType(String defaultFloatType) {
		this.defaultFloatType = defaultFloatType == null ? null : defaultFloatType.trim();
	}

	/**
	 * @return DefaultFloatType
	 */
	public String getDefaultFloatType() {
		return this.defaultFloatType;
	}

	/**
	 * @param defaultPoint
	 */
	public void setDefaultPoint(java.math.BigDecimal defaultPoint) {
		this.defaultPoint = defaultPoint;
	}

	/**
	 * @return DefaultPoint
	 */
	public java.math.BigDecimal getDefaultPoint() {
		return this.defaultPoint;
	}

	/**
	 * @param defaultRate
	 */
	public void setDefaultRate(java.math.BigDecimal defaultRate) {
		this.defaultRate = defaultRate;
	}

	/**
	 * @return DefaultRate
	 */
	public java.math.BigDecimal getDefaultRate() {
		return this.defaultRate;
	}

	/**
	 * @param defaultRateY
	 */
	public void setDefaultRateY(java.math.BigDecimal defaultRateY) {
		this.defaultRateY = defaultRateY;
	}

	/**
	 * @return DefaultRateY
	 */
	public java.math.BigDecimal getDefaultRateY() {
		return this.defaultRateY;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}

	/**
	 * @return ApproveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param approvePassDate
	 */
	public void setApprovePassDate(String approvePassDate) {
		this.approvePassDate = approvePassDate == null ? null : approvePassDate.trim();
	}

	/**
	 * @return ApprovePassDate
	 */
	public String getApprovePassDate() {
		return this.approvePassDate;
	}

	/**
	 * @param contHighAvlAmt
	 */
	public void setContHighAvlAmt(java.math.BigDecimal contHighAvlAmt) {
		this.contHighAvlAmt = contHighAvlAmt;
	}

	/**
	 * @return ContHighAvlAmt
	 */
	public java.math.BigDecimal getContHighAvlAmt() {
		return this.contHighAvlAmt;
	}

	/**
	 * @param isRenew
	 */
	public void setIsRenew(String isRenew) {
		this.isRenew = isRenew == null ? null : isRenew.trim();
	}

	/**
	 * @return IsRenew
	 */
	public String getIsRenew() {
		return this.isRenew;
	}

	/**
	 * @param origiContNo
	 */
	public void setOrigiContNo(String origiContNo) {
		this.origiContNo = origiContNo == null ? null : origiContNo.trim();
	}

	/**
	 * @return OrigiContNo
	 */
	public String getOrigiContNo() {
		return this.origiContNo;
	}

	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt == null ? null : isUtilLmt.trim();
	}

	/**
	 * @return IsUtilLmt
	 */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo == null ? null : lmtAccNo.trim();
	}

	/**
	 * @return LmtAccNo
	 */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param isESeal
	 */
	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal == null ? null : isESeal.trim();
	}

	/**
	 * @return IsESeal
	 */
	public String getIsESeal() {
		return this.isESeal;
	}

	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine == null ? null : belgLine.trim();
	}

	/**
	 * @return BelgLine
	 */
	public String getBelgLine() {
		return this.belgLine;
	}

	/**
	 * @param debtLevel
	 */
	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel == null ? null : debtLevel.trim();
	}

	/**
	 * @return DebtLevel
	 */
	public String getDebtLevel() {
		return this.debtLevel;
	}

	/**
	 * @param ead
	 */
	public void setEad(java.math.BigDecimal ead) {
		this.ead = ead;
	}

	/**
	 * @return Ead
	 */
	public java.math.BigDecimal getEad() {
		return this.ead;
	}

	/**
	 * @param lgd
	 */
	public void setLgd(java.math.BigDecimal lgd) {
		this.lgd = lgd;
	}

	/**
	 * @return Lgd
	 */
	public java.math.BigDecimal getLgd() {
		return this.lgd;
	}

	/**
	 * @param linkman
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman == null ? null : linkman.trim();
	}

	/**
	 * @return Linkman
	 */
	public String getLinkman() {
		return this.linkman;
	}

	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax == null ? null : fax.trim();
	}

	/**
	 * @return Fax
	 */
	public String getFax() {
		return this.fax;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	/**
	 * @return Email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq == null ? null : qq.trim();
	}

	/**
	 * @return Qq
	 */
	public String getQq() {
		return this.qq;
	}

	/**
	 * @param wechat
	 */
	public void setWechat(String wechat) {
		this.wechat = wechat == null ? null : wechat.trim();
	}

	/**
	 * @return Wechat
	 */
	public String getWechat() {
		return this.wechat;
	}

	/**
	 * @param deliveryAddr
	 */
	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr == null ? null : deliveryAddr.trim();
	}

	/**
	 * @return DeliveryAddr
	 */
	public String getDeliveryAddr() {
		return this.deliveryAddr;
	}

	/**
	 * @param bankRole
	 */
	public void setBankRole(String bankRole) {
		this.bankRole = bankRole == null ? null : bankRole.trim();
	}

	/**
	 * @return BankRole
	 */
	public String getBankRole() {
		return this.bankRole;
	}

	/**
	 * @param bksyndicTotlAmt
	 */
	public void setBksyndicTotlAmt(java.math.BigDecimal bksyndicTotlAmt) {
		this.bksyndicTotlAmt = bksyndicTotlAmt;
	}

	/**
	 * @return BksyndicTotlAmt
	 */
	public java.math.BigDecimal getBksyndicTotlAmt() {
		return this.bksyndicTotlAmt;
	}

	/**
	 * @param repaySeq
	 */
	public void setRepaySeq(String repaySeq) {
		this.repaySeq = repaySeq == null ? null : repaySeq.trim();
	}

	/**
	 * @return RepaySeq
	 */
	public String getRepaySeq() {
		return this.repaySeq;
	}

	/**
	 * @param bksyndicPaperContNo
	 */
	public void setBksyndicPaperContNo(String bksyndicPaperContNo) {
		this.bksyndicPaperContNo = bksyndicPaperContNo == null ? null : bksyndicPaperContNo.trim();
	}

	/**
	 * @return BksyndicPaperContNo
	 */
	public String getBksyndicPaperContNo() {
		return this.bksyndicPaperContNo;
	}

	/**
	 * @param loanRateAdjDay
	 */
	public void setLoanRateAdjDay(String loanRateAdjDay) {
		this.loanRateAdjDay = loanRateAdjDay == null ? null : loanRateAdjDay.trim();
	}

	/**
	 * @return LoanRateAdjDay
	 */
	public String getLoanRateAdjDay() {
		return this.loanRateAdjDay;
	}

	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval == null ? null : lprRateIntval.trim();
	}

	/**
	 * @return LprRateIntval
	 */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}

	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}

	/**
	 * @return CurtLprRate
	 */
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}

	/**
	 * @param lprBp
	 */
	public void setLprBp(java.math.BigDecimal lprBp) {
		this.lprBp = lprBp;
	}

	/**
	 * @return LprBp
	 */
	public java.math.BigDecimal getLprBp() {
		return this.lprBp;
	}

	/**
	 * @param drawMode
	 */
	public void setDrawMode(String drawMode) {
		this.drawMode = drawMode == null ? null : drawMode.trim();
	}

	/**
	 * @return DrawMode
	 */
	public String getDrawMode() {
		return this.drawMode;
	}

	/**
	 * @param drawTerm
	 */
	public void setDrawTerm(String drawTerm) {
		this.drawTerm = drawTerm == null ? null : drawTerm.trim();
	}

	/**
	 * @return DrawTerm
	 */
	public String getDrawTerm() {
		return this.drawTerm;
	}

	/**
	 * @param acctsvcrName
	 */
	public void setAcctsvcrName(String acctsvcrName) {
		this.acctsvcrName = acctsvcrName == null ? null : acctsvcrName.trim();
	}

	/**
	 * @return AcctsvcrName
	 */
	public String getAcctsvcrName() {
		return this.acctsvcrName;
	}

	/**
	 * @param repayDetail
	 */
	public void setRepayDetail(String repayDetail) {
		this.repayDetail = repayDetail == null ? null : repayDetail.trim();
	}

	/**
	 * @return RepayDetail
	 */
	public String getRepayDetail() {
		return this.repayDetail;
	}

	/**
	 * @param targetCorp
	 */
	public void setTargetCorp(String targetCorp) {
		this.targetCorp = targetCorp == null ? null : targetCorp.trim();
	}

	/**
	 * @return TargetCorp
	 */
	public String getTargetCorp() {
		return this.targetCorp;
	}

	/**
	 * @param mergerAgr
	 */
	public void setMergerAgr(String mergerAgr) {
		this.mergerAgr = mergerAgr == null ? null : mergerAgr.trim();
	}

	/**
	 * @return MergerAgr
	 */
	public String getMergerAgr() {
		return this.mergerAgr;
	}

	/**
	 * @param mergerTranAmt
	 */
	public void setMergerTranAmt(java.math.BigDecimal mergerTranAmt) {
		this.mergerTranAmt = mergerTranAmt;
	}

	/**
	 * @return MergerTranAmt
	 */
	public java.math.BigDecimal getMergerTranAmt() {
		return this.mergerTranAmt;
	}

	/**
	 * @param isOutstndTrdLmtAmt
	 */
	public void setIsOutstndTrdLmtAmt(String isOutstndTrdLmtAmt) {
		this.isOutstndTrdLmtAmt = isOutstndTrdLmtAmt == null ? null : isOutstndTrdLmtAmt.trim();
	}

	/**
	 * @return IsOutstndTrdLmtAmt
	 */
	public String getIsOutstndTrdLmtAmt() {
		return this.isOutstndTrdLmtAmt;
	}

	/**
	 * @param tdpAgrNo
	 */
	public void setTdpAgrNo(String tdpAgrNo) {
		this.tdpAgrNo = tdpAgrNo == null ? null : tdpAgrNo.trim();
	}

	/**
	 * @return TdpAgrNo
	 */
	public String getTdpAgrNo() {
		return this.tdpAgrNo;
	}

	/**
	 * @param coopCusId
	 */
	public void setCoopCusId(String coopCusId) {
		this.coopCusId = coopCusId == null ? null : coopCusId.trim();
	}

	/**
	 * @return CoopCusId
	 */
	public String getCoopCusId() {
		return this.coopCusId;
	}

	/**
	 * @param coopCusName
	 */
	public void setCoopCusName(String coopCusName) {
		this.coopCusName = coopCusName == null ? null : coopCusName.trim();
	}

	/**
	 * @return CoopCusName
	 */
	public String getCoopCusName() {
		return this.coopCusName;
	}

	/**
	 * @param isSeajnt
	 */
	public void setIsSeajnt(String isSeajnt) {
		this.isSeajnt = isSeajnt == null ? null : isSeajnt.trim();
	}

	/**
	 * @return IsSeajnt
	 */
	public String getIsSeajnt() {
		return this.isSeajnt;
	}

	/**
	 * @param cvtCnyAmt
	 */
	public void setCvtCnyAmt(java.math.BigDecimal cvtCnyAmt) {
		this.cvtCnyAmt = cvtCnyAmt;
	}

	/**
	 * @return CvtCnyAmt
	 */
	public java.math.BigDecimal getCvtCnyAmt() {
		return this.cvtCnyAmt;
	}

	/**
	 * @param purpAnaly
	 */
	public void setPurpAnaly(String purpAnaly) {
		this.purpAnaly = purpAnaly == null ? null : purpAnaly.trim();
	}

	/**
	 * @return PurpAnaly
	 */
	public String getPurpAnaly() {
		return this.purpAnaly;
	}

	/**
	 * @param crossChkDetailAnaly
	 */
	public void setCrossChkDetailAnaly(String crossChkDetailAnaly) {
		this.crossChkDetailAnaly = crossChkDetailAnaly == null ? null : crossChkDetailAnaly.trim();
	}

	/**
	 * @return CrossChkDetailAnaly
	 */
	public String getCrossChkDetailAnaly() {
		return this.crossChkDetailAnaly;
	}

	/**
	 * @param repaySour
	 */
	public void setRepaySour(String repaySour) {
		this.repaySour = repaySour == null ? null : repaySour.trim();
	}

	/**
	 * @return RepaySour
	 */
	public String getRepaySour() {
		return this.repaySour;
	}

	/**
	 * @param inveConclu
	 */
	public void setInveConclu(String inveConclu) {
		this.inveConclu = inveConclu == null ? null : inveConclu.trim();
	}

	/**
	 * @return InveConclu
	 */
	public String getInveConclu() {
		return this.inveConclu;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

	/**
	 * @return OprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}

	/**
	 * @return ManagerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}

	/**
	 * @return ManagerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

	/**
	 * @return InputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

	/**
	 * @return InputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

	/**
	 * @return InputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

	/**
	 * @return UpdId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

	/**
	 * @return UpdBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

	/**
	 * @return UpdDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return CreateTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return UpdateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param firstpayAmt
	 */
	public void setFirstpayAmt(java.math.BigDecimal firstpayAmt) {
		this.firstpayAmt = firstpayAmt;
	}

	/**
	 * @return FirstpayAmt
	 */
	public java.math.BigDecimal getFirstpayAmt() {
		return this.firstpayAmt;
	}

	/**
	 * @param contTotalAmt
	 */
	public void setContTotalAmt(java.math.BigDecimal contTotalAmt) {
		this.contTotalAmt = contTotalAmt;
	}

	/**
	 * @return ContTotalAmt
	 */
	public java.math.BigDecimal getContTotalAmt() {
		return this.contTotalAmt;
	}

	/**
	 * @param graper
	 */
	public void setGraper(String graper) {
		this.graper = graper == null ? null : graper.trim();
	}

	/**
	 * @return Graper
	 */
	public String getGraper() {
		return this.graper;
	}

	/**
	 * @param fundUnionFlag
	 */
	public void setFundUnionFlag(String fundUnionFlag) {
		this.fundUnionFlag = fundUnionFlag == null ? null : fundUnionFlag.trim();
	}

	/**
	 * @return FundUnionFlag
	 */
	public String getFundUnionFlag() {
		return this.fundUnionFlag;
	}

	/**
	 * @param isOnlineDraw
	 */
	public void setIsOnlineDraw(String isOnlineDraw) {
		this.isOnlineDraw = isOnlineDraw == null ? null : isOnlineDraw.trim();
	}

	/**
	 * @return IsOnlineDraw
	 */
	public String getIsOnlineDraw() {
		return this.isOnlineDraw;
	}


}