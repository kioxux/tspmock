/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.IqpHouseDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpHouseMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpHouseService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-12 10:31:57
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpHouseService {
    private static final Logger log = LoggerFactory.getLogger(IqpHouseService.class);

    @Autowired
    private IqpHouseMapper iqpHouseMapper;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpHouse selectByPrimaryKey(String pkId) {
        return iqpHouseMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpHouse> selectAll(QueryModel model) {
        List<IqpHouse> records = (List<IqpHouse>) iqpHouseMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpHouse> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpHouse> list = iqpHouseMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpHouse record) {
        return iqpHouseMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpHouse record) {
        return iqpHouseMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpHouse record) {
        return iqpHouseMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpHouse record) {
        return iqpHouseMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpHouseMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpHouseMapper.deleteByIds(ids);
    }

    /**
     * 通过入参更新数据
     * @param delMap
     * @return
     */
    public int updateByParams(Map delMap) {
        return iqpHouseMapper.updateByParams(delMap);
    }

    public List<IqpHouse> selectByIqpSerno(String iqpSernoOld) {
        return iqpHouseMapper.selectByIqpSerno(iqpSernoOld);
    }


    public IqpHouse selectByIqpSernos(String iqpSernoOld) {
        return iqpHouseMapper.selectByIqpSernos(iqpSernoOld);
    }

    /**
     * 业务申请获取商贷月还款额(暂时设置商贷月还款额为固定值)
     *
     * @param map
     * @return
     */
    public IqpHouseDto getMonthRepayAmt(Map map) {
        IqpHouseDto iqpHouseDto = new IqpHouseDto();
        BigDecimal appAmt = new BigDecimal(0);
        String iqpSerno = (String) map.get("iqpSerno");//申请流水号
        String getAppAmt = (String) map.get("appAmt");//申请金额
        if(StringUtils.isBlank(getAppAmt)) {
            log.info("入参的申请金额为空，查询业务申请" + iqpSerno + "主表信息获取申请金额");
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
            if (iqpLoanApp == null) {
                throw new YuspException(EcbEnum.APPAMT_CHECK_APPNOTEXISTS_EXCEPTION.key, EcbEnum.APPAMT_CHECK_APPNOTEXISTS_EXCEPTION.value);
            }
            appAmt = iqpLoanApp.getAppAmt();
            if (appAmt == null || appAmt.compareTo(new BigDecimal(0)) <= 0) {
                log.info("申请金额为空" + iqpSerno);
                return iqpHouseDto;
            }
        }
        //因为前端入参的金额，存在，分隔符，因此在转化数值前需要将，替换
        appAmt = new BigDecimal(getAppAmt.replace(",",""));

        BigDecimal monthRepayAmt = new BigDecimal("0");
        //商贷月还款额 = 申请金额/申请期限
        monthRepayAmt = appAmt.divide(BigDecimal.valueOf(12), 2, BigDecimal.ROUND_HALF_UP);
        iqpHouseDto.setMonthRepayAmt(monthRepayAmt);
        return iqpHouseDto;
    }
}
