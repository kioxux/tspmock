/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LoanEdCheck
 * @类描述: loan_ed_check数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-04 21:31:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "loan_ed_check")
public class LoanEdCheck extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 100)
	private String contNo;
	
	/** 中文合同编号 **/
	@Column(name = "CONT_CN_NO", unique = false, nullable = true, length = 100)
	private String contCnNo;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 借据金额 **/
	@Column(name = "BILL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billAmt;
	
	/** 受托支付账户户名 **/
	@Column(name = "TRADE_PARTNER_ACC", unique = false, nullable = true, length = 80)
	private String tradePartnerAcc;
	
	/** 受托支付账户账号 **/
	@Column(name = "TRADE_PARTNER_NAME", unique = false, nullable = true, length = 80)
	private String tradePartnerName;
	
	/** 受托支付开户行号 **/
	@Column(name = "ACCTB_NO", unique = false, nullable = true, length = 80)
	private String acctbNo;
	
	/** 受托支付开户行名 **/
	@Column(name = "ACCTB_NM", unique = false, nullable = true, length = 200)
	private String acctbNm;
	
	/** 审核结果 **/
	@Column(name = "AUDIT_RST", unique = false, nullable = true, length = 10)
	private String auditRst;
	
	/** 审核时间 **/
	@Column(name = "AUDIT_DATE", unique = false, nullable = true, length = 10)
	private String auditDate;
	
	/** 借据期限（月份） **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 10)
	private String loanTerm;
	
	/** 审批利率 **/
	@Column(name = "APPR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal apprRate;
	
	/** 放款/还款卡号 **/
	@Column(name = "DISB_REPAY_CARD", unique = false, nullable = true, length = 40)
	private String disbRepayCard;
	
	/** 是否需提高利率 **/
	@Column(name = "IS_ADJ_RATE", unique = false, nullable = true, length = 2)
	private String isAdjRate;
	
	/** 调整后执行利率 **/
	@Column(name = "ADJ_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adjExecRate;
	
	/** 客户经理号 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 40)
	private String managerId;
	
	/** 客户经理名 **/
	@Column(name = "MANAGER_NAME", unique = false, nullable = true, length = 40)
	private String managerName;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param contCnNo
	 */
	public void setContCnNo(String contCnNo) {
		this.contCnNo = contCnNo;
	}
	
    /**
     * @return contCnNo
     */
	public String getContCnNo() {
		return this.contCnNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param billAmt
	 */
	public void setBillAmt(java.math.BigDecimal billAmt) {
		this.billAmt = billAmt;
	}
	
    /**
     * @return billAmt
     */
	public java.math.BigDecimal getBillAmt() {
		return this.billAmt;
	}
	
	/**
	 * @param tradePartnerAcc
	 */
	public void setTradePartnerAcc(String tradePartnerAcc) {
		this.tradePartnerAcc = tradePartnerAcc;
	}
	
    /**
     * @return tradePartnerAcc
     */
	public String getTradePartnerAcc() {
		return this.tradePartnerAcc;
	}
	
	/**
	 * @param tradePartnerName
	 */
	public void setTradePartnerName(String tradePartnerName) {
		this.tradePartnerName = tradePartnerName;
	}
	
    /**
     * @return tradePartnerName
     */
	public String getTradePartnerName() {
		return this.tradePartnerName;
	}
	
	/**
	 * @param acctbNo
	 */
	public void setAcctbNo(String acctbNo) {
		this.acctbNo = acctbNo;
	}
	
    /**
     * @return acctbNo
     */
	public String getAcctbNo() {
		return this.acctbNo;
	}
	
	/**
	 * @param acctbNm
	 */
	public void setAcctbNm(String acctbNm) {
		this.acctbNm = acctbNm;
	}
	
    /**
     * @return acctbNm
     */
	public String getAcctbNm() {
		return this.acctbNm;
	}
	
	/**
	 * @param auditRst
	 */
	public void setAuditRst(String auditRst) {
		this.auditRst = auditRst;
	}
	
    /**
     * @return auditRst
     */
	public String getAuditRst() {
		return this.auditRst;
	}
	
	/**
	 * @param auditDate
	 */
	public void setAuditDate(String auditDate) {
		this.auditDate = auditDate;
	}
	
    /**
     * @return auditDate
     */
	public String getAuditDate() {
		return this.auditDate;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return loanTerm
     */
	public String getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param apprRate
	 */
	public void setApprRate(java.math.BigDecimal apprRate) {
		this.apprRate = apprRate;
	}
	
    /**
     * @return apprRate
     */
	public java.math.BigDecimal getApprRate() {
		return this.apprRate;
	}
	
	/**
	 * @param disbRepayCard
	 */
	public void setDisbRepayCard(String disbRepayCard) {
		this.disbRepayCard = disbRepayCard;
	}
	
    /**
     * @return disbRepayCard
     */
	public String getDisbRepayCard() {
		return this.disbRepayCard;
	}
	
	/**
	 * @param isAdjRate
	 */
	public void setIsAdjRate(String isAdjRate) {
		this.isAdjRate = isAdjRate;
	}
	
    /**
     * @return isAdjRate
     */
	public String getIsAdjRate() {
		return this.isAdjRate;
	}
	
	/**
	 * @param adjExecRate
	 */
	public void setAdjExecRate(java.math.BigDecimal adjExecRate) {
		this.adjExecRate = adjExecRate;
	}
	
    /**
     * @return adjExecRate
     */
	public  java.math.BigDecimal getAdjExecRate() {
		return this.adjExecRate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	
    /**
     * @return managerName
     */
	public String getManagerName() {
		return this.managerName;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}