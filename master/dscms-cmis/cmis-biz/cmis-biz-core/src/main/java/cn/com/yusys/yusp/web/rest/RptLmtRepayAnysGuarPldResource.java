/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarPld;
import cn.com.yusys.yusp.service.RptLmtRepayAnysGuarPldService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarPldResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-02 22:52:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptlmtrepayanysguarpld")
public class RptLmtRepayAnysGuarPldResource {
    @Autowired
    private RptLmtRepayAnysGuarPldService rptLmtRepayAnysGuarPldService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptLmtRepayAnysGuarPld>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptLmtRepayAnysGuarPld> list = rptLmtRepayAnysGuarPldService.selectAll(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarPld>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptLmtRepayAnysGuarPld>> index(QueryModel queryModel) {
        List<RptLmtRepayAnysGuarPld> list = rptLmtRepayAnysGuarPldService.selectByModel(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarPld>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptLmtRepayAnysGuarPld> create(@RequestBody RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld) throws URISyntaxException {
        rptLmtRepayAnysGuarPldService.insert(rptLmtRepayAnysGuarPld);
        return new ResultDto<RptLmtRepayAnysGuarPld>(rptLmtRepayAnysGuarPld);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld) throws URISyntaxException {
        int result = rptLmtRepayAnysGuarPldService.update(rptLmtRepayAnysGuarPld);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String serno) {
        int result = rptLmtRepayAnysGuarPldService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }
    /**
     * 根据流水号查询
     * @param map RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld
     * @return
     */
    @ApiOperation("根据流水号查看抵质押担保的信息")
    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptLmtRepayAnysGuarPld>> selectBySerno(@RequestBody Map<String, Object> map){
        String serno = map.get("serno").toString();
        QueryModel model = new QueryModel();
        model.addCondition("serno",serno);
        return new ResultDto<List<RptLmtRepayAnysGuarPld>>(rptLmtRepayAnysGuarPldService.selectByModel(model));
    }

    /**
     * RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld
     * @param model
     * @return
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<RptLmtRepayAnysGuarPld>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptLmtRepayAnysGuarPld>>(rptLmtRepayAnysGuarPldService.selectByModel(model));
    }

    /**
     * 删除
     * @param rptLmtRepayAnysGuarPld
     * @return
     */
    @PostMapping("/deletePld")
    protected ResultDto<Integer> deletePld(@RequestBody RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld){
        String pkId = rptLmtRepayAnysGuarPld.getPkId();
        return new ResultDto<Integer>(rptLmtRepayAnysGuarPldService.deleteByPrimaryKey(pkId));
    }

    /**
     * 修改
     * @param rptLmtRepayAnysGuarPld
     * @return
     */
    @PostMapping("/updatePld")
    protected ResultDto<Integer> updatePld(@RequestBody RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld){
        return new ResultDto<Integer>(rptLmtRepayAnysGuarPldService.updateSelective(rptLmtRepayAnysGuarPld));
    }

    /**
     * 新增
     * @param rptLmtRepayAnysGuarPld
     * @return
     */
    @PostMapping("/insertPld")
    protected ResultDto<Integer> insertPld(@RequestBody RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld){
        return new ResultDto<Integer>(rptLmtRepayAnysGuarPldService.insertSelective(rptLmtRepayAnysGuarPld));
    }

    /**
     * 保存信息
     * @param rptLmtRepayAnysGuarPld
     * @return
     */
    @PostMapping("/savePld")
    protected ResultDto<Integer> savePld(@RequestBody RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld){
        return new ResultDto<Integer>(rptLmtRepayAnysGuarPldService.savePld(rptLmtRepayAnysGuarPld));
    }

    /**
     * 根据流水号查询
     * @param serno
     * @return
     */
    @PostMapping("/selectPldBySerno")
    protected ResultDto<RptLmtRepayAnysGuarPld> selectBySerno(@RequestBody String serno){
        return new ResultDto<RptLmtRepayAnysGuarPld>(rptLmtRepayAnysGuarPldService.selectBySerno(serno));
    }
}
