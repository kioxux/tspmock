package cn.com.yusys.yusp.service.server.xdxw0066;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0025.req.CmisCus0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0025.resp.CmisCus0025RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0066.req.Xdxw0066DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0066.resp.List;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:调查基本信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0066Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0066Service.class);

    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * 调查基本信息查询
     * @param xdxw0066DataReqDto
     * @return
     */
    @Transactional
    public java.util.List<List> getSurveyReportList(Xdxw0066DataReqDto xdxw0066DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, JSON.toJSONString(xdxw0066DataReqDto));
        java.util.List<List> result = lmtSurveyReportMainInfoMapper.getSurveyReportList(xdxw0066DataReqDto);

        //查询优农贷名单信息
        CmisCus0025ReqDto cmisCus0025ReqDto = new CmisCus0025ReqDto();
        cmisCus0025ReqDto.setCertCode(xdxw0066DataReqDto.getCert_code());
        cmisCus0025ReqDto.setCusName(xdxw0066DataReqDto.getPrd_name());
        cmisCus0025ReqDto.setSerno(xdxw0066DataReqDto.getSurvey_serno());
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0025.key, DscmsEnum.TRADE_CODE_CMISCUS0025.value, JSON.toJSONString(cmisCus0025ReqDto));
        ResultDto<CmisCus0025RespDto> resultDto = cmisCusClientService.cmiscus0025(cmisCus0025ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0025.key, DscmsEnum.TRADE_CODE_CMISCUS0025.value, JSON.toJSONString(resultDto));
        CmisCus0025RespDto cmisCus0025RespDto = new CmisCus0025RespDto();
        if (resultDto != null && CmisBizConstants.NUM_ZERO.equals(resultDto.getCode())) {
            cmisCus0025RespDto = Optional.ofNullable(resultDto.getData()).orElse(new CmisCus0025RespDto());
        }
        String addr = cmisCus0025RespDto.getOperAddr();
        String live_year = cmisCus0025RespDto.getLocalResiLmt();
        String app_amt = cmisCus0025RespDto.getAgriflag();
        String cus_list_serno = cmisCus0025RespDto.getSerno();
        String marry_status = cmisCus0025RespDto.getMarStatus();
        String house_prop = cmisCus0025RespDto.getResiType();
        String local_resident = cmisCus0025RespDto.getLocalRegist();
        String tel = cmisCus0025RespDto.getMobileNo();
        String spouse_name = cmisCus0025RespDto.getSpouseName();
        String spouse_cert_code = cmisCus0025RespDto.getSpouseIdcardNo();
        String spouse_tel = cmisCus0025RespDto.getSpouseMobileNo();
        String years_operations = cmisCus0025RespDto.getOperLmt();
        String business_addres = cmisCus0025RespDto.getOperAddr();
        String education = cmisCus0025RespDto.getEdu();
        String sex = cmisCus0025RespDto.getSex();

        //名单信息
        result = result.parallelStream().map(ret -> {
            cn.com.yusys.yusp.dto.server.xdxw0066.resp.List temp = new cn.com.yusys.yusp.dto.server.xdxw0066.resp.List();
            BeanUtils.copyProperties(ret, temp);
            temp.setAddr(addr);//是否为农户
            temp.setLive_year(live_year);//是否正常经营
            temp.setCus_list_serno(cus_list_serno);//名单流水
            temp.setMarry_status(marry_status);//婚姻状态
            temp.setHouse_prop(house_prop);//居住类型
            temp.setLocal_resident(local_resident);//本地户口
            temp.setTel(tel);//手机
            temp.setSpouse_name(spouse_name);
            temp.setSpouse_cert_code(spouse_cert_code);
            temp.setSpouse_tel(spouse_tel);
            temp.setYears_operations(years_operations);
            temp.setBusiness_addres(business_addres);
            temp.setEducation(education);
            temp.setSex(sex);
            //返回列表
            return temp;
        }).collect(Collectors.toList());

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, JSON.toJSONString(result));
        return result;
    }
}
