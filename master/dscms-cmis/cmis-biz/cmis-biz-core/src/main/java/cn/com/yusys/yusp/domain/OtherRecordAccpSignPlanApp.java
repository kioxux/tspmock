/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherRecordAccpSignPlanApp
 * @类描述: other_record_accp_sign_plan_app数据实体类
 * @功能描述: 
 * @创建人: hhj123456
 * @创建时间: 2021-06-09 20:28:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_record_accp_sign_plan_app")
public class OtherRecordAccpSignPlanApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 上报前一日银票余额 **/
	@Column(name = "REP_BEF_DAY_REC_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repBefDayRecBal;
	
	/** 下周计划签发银票金额 **/
	@Column(name = "NEXT_WEEK_PLAN_SIGN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nextWeekPlanSignAmt;
	
	/** 敞口银票计划签发金额 **/
	@Column(name = "OPEN_REC_PLAN_ISS_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal openRecPlanIssAmt;
	
	/** 敞口银票计划签发金额(纸票) **/
	@Column(name = "OPEN_REC_PLAN_ISS_AMT_PAPER", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal openRecPlanIssAmtPaper;
	
	/** 全资质押类计划签发金额 **/
	@Column(name = "WHO_IMN_PLAN_ISS_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal whoImnPlanIssAmt;
	
	/** 全资质押类计划签发金额(纸票) **/
	@Column(name = "WHO_IMN_PLAN_ISS_AMT_PAPER", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal whoImnPlanIssAmtPaper;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;

	/** 是否上调审批权限 **/
	@Column(name = "IS_UPPER_APPR_AUTH", unique = false, nullable = false, length = 5)
	private String isUpperApprAuth;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param repBefDayRecBal
	 */
	public void setRepBefDayRecBal(java.math.BigDecimal repBefDayRecBal) {
		this.repBefDayRecBal = repBefDayRecBal;
	}
	
    /**
     * @return repBefDayRecBal
     */
	public java.math.BigDecimal getRepBefDayRecBal() {
		return this.repBefDayRecBal;
	}
	
	/**
	 * @param nextWeekPlanSignAmt
	 */
	public void setNextWeekPlanSignAmt(java.math.BigDecimal nextWeekPlanSignAmt) {
		this.nextWeekPlanSignAmt = nextWeekPlanSignAmt;
	}
	
    /**
     * @return nextWeekPlanSignAmt
     */
	public java.math.BigDecimal getNextWeekPlanSignAmt() {
		return this.nextWeekPlanSignAmt;
	}
	
	/**
	 * @param openRecPlanIssAmt
	 */
	public void setOpenRecPlanIssAmt(java.math.BigDecimal openRecPlanIssAmt) {
		this.openRecPlanIssAmt = openRecPlanIssAmt;
	}
	
    /**
     * @return openRecPlanIssAmt
     */
	public java.math.BigDecimal getOpenRecPlanIssAmt() {
		return this.openRecPlanIssAmt;
	}
	
	/**
	 * @param openRecPlanIssAmtPaper
	 */
	public void setOpenRecPlanIssAmtPaper(java.math.BigDecimal openRecPlanIssAmtPaper) {
		this.openRecPlanIssAmtPaper = openRecPlanIssAmtPaper;
	}
	
    /**
     * @return openRecPlanIssAmtPaper
     */
	public java.math.BigDecimal getOpenRecPlanIssAmtPaper() {
		return this.openRecPlanIssAmtPaper;
	}
	
	/**
	 * @param whoImnPlanIssAmt
	 */
	public void setWhoImnPlanIssAmt(java.math.BigDecimal whoImnPlanIssAmt) {
		this.whoImnPlanIssAmt = whoImnPlanIssAmt;
	}
	
    /**
     * @return whoImnPlanIssAmt
     */
	public java.math.BigDecimal getWhoImnPlanIssAmt() {
		return this.whoImnPlanIssAmt;
	}
	
	/**
	 * @param whoImnPlanIssAmtPaper
	 */
	public void setWhoImnPlanIssAmtPaper(java.math.BigDecimal whoImnPlanIssAmtPaper) {
		this.whoImnPlanIssAmtPaper = whoImnPlanIssAmtPaper;
	}
	
    /**
     * @return whoImnPlanIssAmtPaper
     */
	public java.math.BigDecimal getWhoImnPlanIssAmtPaper() {
		return this.whoImnPlanIssAmtPaper;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param isUpperApprAuth
	 */
	public void setIsUpperApprAuth(String isUpperApprAuth) {
		this.isUpperApprAuth = isUpperApprAuth;
	}

	/**
	 * @return isUpperApprAuth
	 */
	public String getIsUpperApprAuth() {
		return this.isUpperApprAuth;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}