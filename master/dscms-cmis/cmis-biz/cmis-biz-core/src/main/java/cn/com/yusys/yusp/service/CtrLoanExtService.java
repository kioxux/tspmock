/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccEntrustLoanTxnDetails;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3112.Ln3112ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3112.Ln3112RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CtrLoanExt;
import cn.com.yusys.yusp.repository.mapper.CtrLoanExtMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLoanExtService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-12 11:01:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CtrLoanExtService {

    @Autowired
    private CtrLoanExtMapper ctrLoanExtMapper;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CtrLoanExt selectByPrimaryKey(String extCtrNo) {
        return ctrLoanExtMapper.selectByPrimaryKey(extCtrNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CtrLoanExt> selectAll(QueryModel model) {
        List<CtrLoanExt> records = (List<CtrLoanExt>) ctrLoanExtMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CtrLoanExt> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrLoanExt> list = ctrLoanExtMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CtrLoanExt record) {
        return ctrLoanExtMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CtrLoanExt record) {
        return ctrLoanExtMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CtrLoanExt record) {
        return ctrLoanExtMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CtrLoanExt record) {
        return ctrLoanExtMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String extCtrNo) {
        return ctrLoanExtMapper.deleteByPrimaryKey(extCtrNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return ctrLoanExtMapper.deleteByIds(ids);
    }

    /**
     * 展期信息
     *
     * @author 刘权
     **/
    public ResultDto<Ln3112RespDto> sendToZQXX(CtrLoanExt ctrLoanExt) {

        Ln3112ReqDto ln3112ReqDto = new Ln3112ReqDto();
        //借据编号
        ln3112ReqDto.setDkjiejuh(ctrLoanExt.getOldBillNo());

        ResultDto<Ln3112RespDto> resultDto = dscms2CoreLnClientService.ln3112(ln3112ReqDto);
        String ln3112Code = Optional.ofNullable(resultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3112Meesage = Optional.ofNullable(resultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(ln3112Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            return resultDto;
        } else {
            resultDto.setMessage(ln3112Meesage);
            return resultDto;
        }
    }
}
