package cn.com.yusys.yusp.service.client.bsp.core.ib1241;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreIbClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author wangyk
 * @version 1.0.0
 * @date 2021/6/29 10:33
 * @desc 冲正交易
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Ib1241Service {
    private static final Logger logger = LoggerFactory.getLogger(Ib1241Service.class);
    // 1）注入：BSP封装调用核心系统的接口
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;

    /**
     * @param ib1241ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto
     * @author 王玉坤
     * @date 2021/6/22 20:13
     * @version 1.0.0
     * @desc 调用子账户信息新查询接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Ib1241RespDto ib1241(Ib1241ReqDto ib1241ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1241.key, EsbEnum.TRADE_CODE_IB1241.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1241.key, EsbEnum.TRADE_CODE_IB1241.value, JSON.toJSONString(ib1241ReqDto));
        ResultDto<Ib1241RespDto> ib1241RespDtoResultDto = dscms2CoreIbClientService.ib1241(ib1241ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1241.key, EsbEnum.TRADE_CODE_IB1241.value, JSON.toJSONString(ib1241RespDtoResultDto));
        String ib1241Code = Optional.ofNullable(ib1241RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ib1241Meesage = Optional.ofNullable(ib1241RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ib1241RespDto ib1241RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ib1241RespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            ib1241RespDto = ib1241RespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(ib1241Code, ib1241Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1241.key, EsbEnum.TRADE_CODE_IB1241.value);
        return ib1241RespDto;
    }
}
