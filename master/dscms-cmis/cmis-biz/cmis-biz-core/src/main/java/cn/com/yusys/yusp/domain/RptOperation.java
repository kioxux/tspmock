/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperation
 * @类描述: rpt_operation数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 09:33:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_operation")
public class RptOperation extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 行业分类 **/
	@Column(name = "TRADE_CLASS", unique = false, nullable = true, length = 5)
	private String tradeClass;
	
	/** 行业情况 **/
	@Column(name = "TRADE_CASE", unique = false, nullable = true, length = 65535)
	private String tradeCase;
	
	/** 主要产品 **/
	@Column(name = "MAIN_PRD", unique = false, nullable = true, length = 40)
	private String mainPrd;
	
	/** 加弹车（台） **/
	@Column(name = "LOAD_CAR_NUM", unique = false, nullable = true, length = 10)
	private Integer loadCarNum;
	
	/** 年产量（吨） **/
	@Column(name = "ANNUAL_OUTPUT", unique = false, nullable = true, length = 10)
	private Integer annualOutput;
	
	/** 开工天数 **/
	@Column(name = "START_DAY_NUM", unique = false, nullable = true, length = 10)
	private Integer startDayNum;
	
	/** 加弹车（总节数） **/
	@Column(name = "LOAD_CAR_NODE_NUM", unique = false, nullable = true, length = 10)
	private Integer loadCarNodeNum;
	
	/** 电费单价（元/度） **/
	@Column(name = "POWER_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal powerRate;
	
	/** 注册资本 **/
	@Column(name = "REGI_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regiCapAmt;
	
	/** 年电费（万元） **/
	@Column(name = "YEAR_POWER_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearPowerAmt;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String otherDesc;
	
	/** 近年销售波动原因 **/
	@Column(name = "RECENT_SALE_WAVE_REASON", unique = false, nullable = true, length = 65535)
	private String recentSaleWaveReason;
	
	/** 所得税率 **/
	@Column(name = "INCOME_TAX_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal incomeTaxRate;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass;
	}
	
    /**
     * @return tradeClass
     */
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param tradeCase
	 */
	public void setTradeCase(String tradeCase) {
		this.tradeCase = tradeCase;
	}
	
    /**
     * @return tradeCase
     */
	public String getTradeCase() {
		return this.tradeCase;
	}
	
	/**
	 * @param mainPrd
	 */
	public void setMainPrd(String mainPrd) {
		this.mainPrd = mainPrd;
	}
	
    /**
     * @return mainPrd
     */
	public String getMainPrd() {
		return this.mainPrd;
	}
	
	/**
	 * @param loadCarNum
	 */
	public void setLoadCarNum(Integer loadCarNum) {
		this.loadCarNum = loadCarNum;
	}
	
    /**
     * @return loadCarNum
     */
	public Integer getLoadCarNum() {
		return this.loadCarNum;
	}
	
	/**
	 * @param annualOutput
	 */
	public void setAnnualOutput(Integer annualOutput) {
		this.annualOutput = annualOutput;
	}
	
    /**
     * @return annualOutput
     */
	public Integer getAnnualOutput() {
		return this.annualOutput;
	}
	
	/**
	 * @param startDayNum
	 */
	public void setStartDayNum(Integer startDayNum) {
		this.startDayNum = startDayNum;
	}
	
    /**
     * @return startDayNum
     */
	public Integer getStartDayNum() {
		return this.startDayNum;
	}
	
	/**
	 * @param loadCarNodeNum
	 */
	public void setLoadCarNodeNum(Integer loadCarNodeNum) {
		this.loadCarNodeNum = loadCarNodeNum;
	}
	
    /**
     * @return loadCarNodeNum
     */
	public Integer getLoadCarNodeNum() {
		return this.loadCarNodeNum;
	}
	
	/**
	 * @param powerRate
	 */
	public void setPowerRate(java.math.BigDecimal powerRate) {
		this.powerRate = powerRate;
	}
	
    /**
     * @return powerRate
     */
	public java.math.BigDecimal getPowerRate() {
		return this.powerRate;
	}
	
	/**
	 * @param regiCapAmt
	 */
	public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
		this.regiCapAmt = regiCapAmt;
	}
	
    /**
     * @return regiCapAmt
     */
	public java.math.BigDecimal getRegiCapAmt() {
		return this.regiCapAmt;
	}
	
	/**
	 * @param yearPowerAmt
	 */
	public void setYearPowerAmt(java.math.BigDecimal yearPowerAmt) {
		this.yearPowerAmt = yearPowerAmt;
	}
	
    /**
     * @return yearPowerAmt
     */
	public java.math.BigDecimal getYearPowerAmt() {
		return this.yearPowerAmt;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param recentSaleWaveReason
	 */
	public void setRecentSaleWaveReason(String recentSaleWaveReason) {
		this.recentSaleWaveReason = recentSaleWaveReason;
	}
	
    /**
     * @return recentSaleWaveReason
     */
	public String getRecentSaleWaveReason() {
		return this.recentSaleWaveReason;
	}
	
	/**
	 * @param incomeTaxRate
	 */
	public void setIncomeTaxRate(java.math.BigDecimal incomeTaxRate) {
		this.incomeTaxRate = incomeTaxRate;
	}
	
    /**
     * @return incomeTaxRate
     */
	public java.math.BigDecimal getIncomeTaxRate() {
		return this.incomeTaxRate;
	}


}