/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.GuarWarrantManageApp;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm007.Cwm007ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm007.Cwm007RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.GuarContRelWarrantMapper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GuarWarrantInfo;
import cn.com.yusys.yusp.repository.mapper.GuarWarrantInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarWarrantInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 17:13:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarWarrantInfoService {
    private final Logger log = LoggerFactory.getLogger(GuarWarrantManageAppService.class);

    @Autowired
    private GuarWarrantInfoMapper guarWarrantInfoMapper;

    @Autowired
    private Dscms2YpqzxtClientService dscms2YpqzxtClientService;

    @Autowired
    private GuarContRelWarrantMapper guarContRelWarrantMapper;

    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public GuarWarrantInfo selectByPrimaryKey(String pkId) {
        return guarWarrantInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GuarWarrantInfo> selectAll(QueryModel model) {
        List<GuarWarrantInfo> records = (List<GuarWarrantInfo>) guarWarrantInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<GuarWarrantInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarWarrantInfo> list = guarWarrantInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectWarrantInInfoByModel
     * @方法描述: 条件查询 - 查询进行分页（适用于权证出库选择权证）
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GuarWarrantInfo> selectWarrantInInfoByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarWarrantInfo> list = guarWarrantInfoMapper.selectWarrantInInfoByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(GuarWarrantInfo record) {
        return guarWarrantInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(GuarWarrantInfo record) {
        return guarWarrantInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(GuarWarrantInfo record) {
        return guarWarrantInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(GuarWarrantInfo record) {
        return guarWarrantInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return guarWarrantInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarWarrantInfoMapper.deleteByIds(ids);
    }

    /**
     * 根据权证编号查询权证信息
     * @param warrantNo
     * @return
     */
    public GuarWarrantInfo queryByWarrantNo(String warrantNo){
        return guarWarrantInfoMapper.queryByWarrantNo(warrantNo);
    }

    /**
     * 根据权证编号查询权证信息
     * @param warrantNo
     * @return
     */
    public GuarWarrantInfo selectByWarrantNo(String warrantNo){
        return guarWarrantInfoMapper.selectByWarrantNo(warrantNo);
    }

    /**
     * 根据核心担保编号查询
     * @param coreGuarantyNo
     * @return
     */
    public GuarWarrantInfo selectByCoreGuarantyNo(String coreGuarantyNo){
        return guarWarrantInfoMapper.selectByCoreGuarantyNo(coreGuarantyNo);
    }



    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GuarWarrantInfo> selectInfoList(QueryModel model) {
        List<GuarWarrantInfo> list = guarWarrantInfoMapper.selectByModel(model);
        return list;
    }

    /**
     * @param contNo
     * @return int
     * @author hubp
     * @date 2021/8/14 17:26
     * @version 1.0.0
     * @desc    根据合同编号查询合同项下押品是否入库
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int getInfoByContNo(String contNo){
        return  guarWarrantInfoMapper.getInfoByContNo(contNo);
    }

    public int countWarrantByContNo(String contNo){
        return  guarWarrantInfoMapper.countWarrantByContNo(contNo);
    }

    /**
     * 根据合同编号查询合同项下入库已记账的权证数
     * @param queryModel
     * @return
     */
    public int countWarrantInRecordsByGuarNoAndGuarContNo(QueryModel queryModel){
        return  guarWarrantInfoMapper.countWarrantInRecordsByGuarNoAndGuarContNo(queryModel);
    }

    /**
     * 根据核心担保编号查询权证是否电子权证
     * @param coreGuarantyNo
     * @return
     */
    public String selectIsEWarrantByCoreGuarantyNo(String coreGuarantyNo){
        return guarWarrantInfoMapper.selectIsEWarrantByCoreGuarantyNo(coreGuarantyNo);
    }

    /**
     * 根据担保合同编号和押品编号查询最新的权证信息
     * @param queryModel
     * @return
     */
    public GuarWarrantInfo selectByGuarContNoAndGuarNo(QueryModel queryModel){
        GuarWarrantInfo guarWarrantInfo = null;
        List<String> coreGuarNos = guarContRelWarrantMapper.getCoreGuarNosByGuarContNoAndGuarNo(queryModel);
        if(CollectionUtils.nonEmpty(coreGuarNos)){
            guarWarrantInfo = guarWarrantInfoMapper.selectByCoreGuarNos(coreGuarNos);
        }
        return guarWarrantInfo;
        //return guarWarrantInfoMapper.selectByGuarContNoAndGuarNo(queryModel);
    }

    /**
     * 根据核心担保编号查询实时权证状态
     * @param coreGuarantyNo
     * @return
     */
    public String selectCurCertiStateByCoreGuarantyNo(String coreGuarantyNo){
        //调权证系统接口查询该核心担保编号的权证状态
        Cwm007ReqDto cwm007ReqDto = new Cwm007ReqDto();
        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoMapper.selectByCoreGuarantyNo(coreGuarantyNo);

        if (guarWarrantInfo==null || StringUtils.isEmpty(guarWarrantInfo.getCoreGuarantyNo())){
            throw new BizException(null,"",null,"根据核心担保编号【"+coreGuarantyNo+"】查不到权证信息！");
        }

        String managerId = guarWarrantInfo.getManagerId();

        if (StringUtils.isEmpty(managerId)){
            GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppService.selectWarrantOutInfoByCoreGuarantyNo(coreGuarantyNo);
            managerId = guarWarrantManageApp.getManagerId();
        }
        String userName = OcaTranslatorUtils.getUserName(managerId);
        //柜员名称
        cwm007ReqDto.setUserName(userName);
        //核心担保品编号
        cwm007ReqDto.setGageId(coreGuarantyNo);

        log.info("核心担保编号【"+coreGuarantyNo+"】，押品状态查询接口开始，请求报文："+ cwm007ReqDto);
        ResultDto<Cwm007RespDto> cwm007RespResultDto = dscms2YpqzxtClientService.cwm007(cwm007ReqDto);
        log.info("核心担保编号【"+coreGuarantyNo+"】，押品状态查询接口结束，响应报文："+ cwm007RespResultDto);

        String code = Optional.ofNullable(cwm007RespResultDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(cwm007RespResultDto.getMessage()).orElse(StringUtils.EMPTY);

        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            Cwm007RespDto cwm007RespDto1 = cwm007RespResultDto.getData();
            log.info("押品状态查询接口 成功"+ cwm007RespDto1);
            return cwm007RespDto1.getReturnType();
        } else {
            log.error("押品状态查询接口 失败，"+meesage);
            throw new BizException(null, "", null, meesage);
        }
    }

    /**
     * 查询是否存在住房按揭抵押
     * @param coreGrtNo
     * @return
     */
    public int selectAccommoDation(String coreGrtNo){
        return guarWarrantInfoMapper.selectAccommoDation(coreGrtNo);
    }

    /**
     * @方法名称: updateWarrantStateByCoreGrtNo
     * @方法描述: 根据押品编号修改权证出入库状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateWarrantStateByCoreGrtNo(Map queryMap){
        return guarWarrantInfoMapper.updateWarrantStateByCoreGrtNo(queryMap);
    }

    /**
     * @方法名称: selectIsExistByCoreGuarantyNo
     * @方法描述: 查询是否存在指定的核心担保编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int selectIsExistByCoreGuarantyNo(@Param("coreGrtNo") String coreGrtNo){
        return guarWarrantInfoMapper.selectIsExistByCoreGuarantyNo(coreGrtNo);
    }

    /**
     * 查询入库在途、入库未记账、入库已记账的权证记录数
     * @param coreGrtNo
     * @return
     */
    public int countWarrantInStateByCoreGuarantyNo(@Param("coreGrtNo") String coreGrtNo) {
        return guarWarrantInfoMapper.countWarrantInStateByCoreGuarantyNo(coreGrtNo);
    }

    /**
     * 根据核心担保编号列表查询权证信息
     * @param ypbhs
     * @return
     */
    public List<GuarWarrantInfo> selectByCoreGuarantyNos(List<String> ypbhs){
        return guarWarrantInfoMapper.selectByCoreGuarantyNos(ypbhs);
    }
}
