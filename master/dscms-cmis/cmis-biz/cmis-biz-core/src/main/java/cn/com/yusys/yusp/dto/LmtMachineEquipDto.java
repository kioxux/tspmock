package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtMachineEquip
 * @类描述: lmt_machine_equip数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-13 14:20:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtMachineEquipDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 授信协议编号 **/
	private String lmtCtrNo;
	
	/** 项目编号 **/
	private String proNo;
	
	/** 项目名称 **/
	private String proName;
	
	/** 责任人姓名 **/
	private String managerNamen;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 联系方式 **/
	private String linkMode;
	
	/** 回购担保总额 **/
	private java.math.BigDecimal guarAmtRebuy;
	
	/** 保证金比例 **/
	private java.math.BigDecimal bailPerc;
	
	/** 单户按揭限额 **/
	private java.math.BigDecimal singleQuotaSched;
	
	/** 单户按揭期限（月） **/
	private String singleTermSched;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo == null ? null : lmtCtrNo.trim();
	}
	
    /**
     * @return LmtCtrNo
     */	
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}
	
    /**
     * @return ProNo
     */	
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName == null ? null : proName.trim();
	}
	
    /**
     * @return ProName
     */	
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param managerNamen
	 */
	public void setManagerNamen(String managerNamen) {
		this.managerNamen = managerNamen == null ? null : managerNamen.trim();
	}
	
    /**
     * @return ManagerNamen
     */	
	public String getManagerNamen() {
		return this.managerNamen;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param linkMode
	 */
	public void setLinkMode(String linkMode) {
		this.linkMode = linkMode == null ? null : linkMode.trim();
	}
	
    /**
     * @return LinkMode
     */	
	public String getLinkMode() {
		return this.linkMode;
	}
	
	/**
	 * @param guarAmtRebuy
	 */
	public void setGuarAmtRebuy(java.math.BigDecimal guarAmtRebuy) {
		this.guarAmtRebuy = guarAmtRebuy;
	}
	
    /**
     * @return GuarAmtRebuy
     */	
	public java.math.BigDecimal getGuarAmtRebuy() {
		return this.guarAmtRebuy;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return BailPerc
     */	
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param singleQuotaSched
	 */
	public void setSingleQuotaSched(java.math.BigDecimal singleQuotaSched) {
		this.singleQuotaSched = singleQuotaSched;
	}
	
    /**
     * @return SingleQuotaSched
     */	
	public java.math.BigDecimal getSingleQuotaSched() {
		return this.singleQuotaSched;
	}
	
	/**
	 * @param singleTermSched
	 */
	public void setSingleTermSched(String singleTermSched) {
		this.singleTermSched = singleTermSched == null ? null : singleTermSched.trim();
	}
	
    /**
     * @return SingleTermSched
     */	
	public String getSingleTermSched() {
		return this.singleTermSched;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}