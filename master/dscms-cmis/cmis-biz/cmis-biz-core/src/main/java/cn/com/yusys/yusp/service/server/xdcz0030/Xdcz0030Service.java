package cn.com.yusys.yusp.service.server.xdcz0030;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033DealBizListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.resp.CmisLmt0033RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0030.req.DealBizList;
import cn.com.yusys.yusp.dto.server.xdcz0030.req.OccRelList;
import cn.com.yusys.yusp.dto.server.xdcz0030.req.Xdcz0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0030.resp.Xdcz0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.web.server.xdcz0030.BizXdcz0030Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class Xdcz0030Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0030Service.class);

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0030DataRespDto xdcz0030(Xdcz0030DataReqDto xdcz0030DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, JSON.toJSONString(xdcz0030DataReqDto));
        Xdcz0030DataRespDto xdcz0030DataRespDto = new Xdcz0030DataRespDto();
        CmisLmt0033ReqDto cmisLmt0033ReqDto = new CmisLmt0033ReqDto();
        try {
            BeanUtils.copyProperties(xdcz0030DataReqDto, cmisLmt0033ReqDto);
            List<OccRelList> occRelList = xdcz0030DataReqDto.getOccRelList();
            List<DealBizList> dealBizList = xdcz0030DataReqDto.getDealBizList();
            if (CollectionUtils.nonEmpty(occRelList)) {
                List<CmisLmt0033OccRelListReqDto> collect = occRelList.parallelStream().map(occRel -> {
                    CmisLmt0033OccRelListReqDto cmisLmt0033OccRelListReqDto = new CmisLmt0033OccRelListReqDto();
                    BeanUtils.copyProperties(occRel, cmisLmt0033OccRelListReqDto);
                    return cmisLmt0033OccRelListReqDto;
                }).collect(Collectors.toList());
                cmisLmt0033ReqDto.setOccRelList(collect);
            }
            if (CollectionUtils.nonEmpty(dealBizList)) {
                List<CmisLmt0033DealBizListReqDto> collect = dealBizList.parallelStream().map(dealBiz -> {
                    CmisLmt0033DealBizListReqDto cmisLmt0033DealBizListReqDto = new CmisLmt0033DealBizListReqDto();
                    BeanUtils.copyProperties(dealBiz, cmisLmt0033DealBizListReqDto);
                    return cmisLmt0033DealBizListReqDto;
                }).collect(Collectors.toList());
                cmisLmt0033ReqDto.setDealBizList(collect);
            }
            ResultDto<CmisLmt0033RespDto> cmisLmt0033RespDtoResultDto = cmisLmtClientService.cmislmt0033(cmisLmt0033ReqDto);
            if (Objects.nonNull(cmisLmt0033RespDtoResultDto) && Objects.equals(cmisLmt0033RespDtoResultDto.getCode(), SuccessEnum.CMIS_SUCCSESS.key)) {
                CmisLmt0033RespDto data = cmisLmt0033RespDtoResultDto.getData();
                BeanUtils.copyProperties(data, xdcz0030DataRespDto);
            }
            // TODO 合同可出账金额校验 待马顺提供
        } catch (BeansException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, JSON.toJSONString(xdcz0030DataRespDto));
        return xdcz0030DataRespDto;
    }
}
