/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain.bat;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BatBizAssetsAnalyse
 * @类描述: bat_biz_assets_analyse数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 11:11:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_biz_assets_analyse")
public class BatBizAssetsAnalyse extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = false, length = 30)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 30)
	private String managerBrId;
	
	/** 管理资产户数 **/
	@Column(name = "NORMAL_CUS_COUNT", unique = false, nullable = true, length = 10)
	private Integer normalCusCount;
	
	/** 管理资产笔数 **/
	@Column(name = "NORMAL_ACC_COUNT", unique = false, nullable = true, length = 10)
	private Integer normalAccCount;
	
	/** 管理资产余额 **/
	@Column(name = "NORMAL_TOTAL_BALANCE", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal normalTotalBalance;
	
	/** 瑕疵资产户数 **/
	@Column(name = "FLAW_CUS_COUNT", unique = false, nullable = true, length = 10)
	private Integer flawCusCount;
	
	/** 瑕疵资产笔数 **/
	@Column(name = "FLAW_ACC_COUNT", unique = false, nullable = true, length = 10)
	private Integer flawAccCount;
	
	/** 瑕疵资产余额 **/
	@Column(name = "FLAW_TOTAL_BALANCE", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal flawTotalBalance;
	
	/** 不良资产户数 **/
	@Column(name = "BAD_CUS_COUNT", unique = false, nullable = true, length = 10)
	private Integer badCusCount;
	
	/** 不良资产笔数 **/
	@Column(name = "BAD_ACC_COUNT", unique = false, nullable = true, length = 10)
	private Integer badAccCount;
	
	/** 不良资产余额 **/
	@Column(name = "BAD_TOTAL_BALANCE", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal badTotalBalance;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param normalCusCount
	 */
	public void setNormalCusCount(Integer normalCusCount) {
		this.normalCusCount = normalCusCount;
	}
	
    /**
     * @return normalCusCount
     */
	public Integer getNormalCusCount() {
		return this.normalCusCount;
	}
	
	/**
	 * @param normalAccCount
	 */
	public void setNormalAccCount(Integer normalAccCount) {
		this.normalAccCount = normalAccCount;
	}
	
    /**
     * @return normalAccCount
     */
	public Integer getNormalAccCount() {
		return this.normalAccCount;
	}
	
	/**
	 * @param normalTotalBalance
	 */
	public void setNormalTotalBalance(java.math.BigDecimal normalTotalBalance) {
		this.normalTotalBalance = normalTotalBalance;
	}
	
    /**
     * @return normalTotalBalance
     */
	public java.math.BigDecimal getNormalTotalBalance() {
		return this.normalTotalBalance;
	}
	
	/**
	 * @param flawCusCount
	 */
	public void setFlawCusCount(Integer flawCusCount) {
		this.flawCusCount = flawCusCount;
	}
	
    /**
     * @return flawCusCount
     */
	public Integer getFlawCusCount() {
		return this.flawCusCount;
	}
	
	/**
	 * @param flawAccCount
	 */
	public void setFlawAccCount(Integer flawAccCount) {
		this.flawAccCount = flawAccCount;
	}
	
    /**
     * @return flawAccCount
     */
	public Integer getFlawAccCount() {
		return this.flawAccCount;
	}
	
	/**
	 * @param flawTotalBalance
	 */
	public void setFlawTotalBalance(java.math.BigDecimal flawTotalBalance) {
		this.flawTotalBalance = flawTotalBalance;
	}
	
    /**
     * @return flawTotalBalance
     */
	public java.math.BigDecimal getFlawTotalBalance() {
		return this.flawTotalBalance;
	}
	
	/**
	 * @param badCusCount
	 */
	public void setBadCusCount(Integer badCusCount) {
		this.badCusCount = badCusCount;
	}
	
    /**
     * @return badCusCount
     */
	public Integer getBadCusCount() {
		return this.badCusCount;
	}
	
	/**
	 * @param badAccCount
	 */
	public void setBadAccCount(Integer badAccCount) {
		this.badAccCount = badAccCount;
	}
	
    /**
     * @return badAccCount
     */
	public Integer getBadAccCount() {
		return this.badAccCount;
	}
	
	/**
	 * @param badTotalBalance
	 */
	public void setBadTotalBalance(java.math.BigDecimal badTotalBalance) {
		this.badTotalBalance = badTotalBalance;
	}
	
    /**
     * @return badTotalBalance
     */
	public java.math.BigDecimal getBadTotalBalance() {
		return this.badTotalBalance;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}