package cn.com.yusys.yusp.web.server.xdzc0026;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0026.req.Xdzc0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0026.resp.Xdzc0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0026.Xdzc0026Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池主动还款接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0026:资产池主动还款接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0026Resource.class);

    @Autowired
    private Xdzc0026Service xdzc0026Service;
    /**
     * 交易码：xdzc0026
     * 交易描述：资产池主动还款接口
     *
     * @param xdzc0026DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池主动还款接口")
    @PostMapping("/xdzc0026")
    protected @ResponseBody
    ResultDto<Xdzc0026DataRespDto> xdzc0026(@Validated @RequestBody Xdzc0026DataReqDto xdzc0026DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value, JSON.toJSONString(xdzc0026DataReqDto));
        Xdzc0026DataRespDto xdzc0026DataRespDto = new Xdzc0026DataRespDto();// 响应Dto:资产池主动还款接口
        ResultDto<Xdzc0026DataRespDto> xdzc0026DataResultDto = new ResultDto<>();
        try {
            xdzc0026DataRespDto = xdzc0026Service.xdzc0026Service(xdzc0026DataReqDto);
            xdzc0026DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0026DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value, e.getMessage());
            // 封装xdzc0026DataResultDto中异常返回码和返回信息
            xdzc0026DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0026DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0026DataRespDto到xdzc0026DataResultDto中
        xdzc0026DataResultDto.setData(xdzc0026DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value, JSON.toJSONString(xdzc0026DataResultDto));
        return xdzc0026DataResultDto;
    }
}
