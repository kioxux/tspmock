/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.PvpRruUpdAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpRruUpdAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-28 14:37:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pvprruupdapp")
public class PvpRruUpdAppResource {
    @Autowired
    private PvpRruUpdAppService pvpRruUpdAppService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpRruUpdApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpRruUpdApp> list = pvpRruUpdAppService.selectAll(queryModel);
        return new ResultDto<List<PvpRruUpdApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpRruUpdApp>> index(QueryModel queryModel) {
        List<PvpRruUpdApp> list = pvpRruUpdAppService.selectByModel(queryModel);
        return new ResultDto<List<PvpRruUpdApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{rruSerno}")
    protected ResultDto<PvpRruUpdApp> show(@PathVariable("rruSerno") String rruSerno) {
        PvpRruUpdApp pvpRruUpdApp = pvpRruUpdAppService.selectByPrimaryKey(rruSerno);
        return new ResultDto<PvpRruUpdApp>(pvpRruUpdApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpRruUpdApp> create(@RequestBody PvpRruUpdApp pvpRruUpdApp) throws URISyntaxException {
        pvpRruUpdAppService.insert(pvpRruUpdApp);
        return new ResultDto<PvpRruUpdApp>(pvpRruUpdApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpRruUpdApp pvpRruUpdApp) throws URISyntaxException {
        int result = pvpRruUpdAppService.updateSelective(pvpRruUpdApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{rruSerno}")
    protected ResultDto<Integer> delete(@PathVariable("rruSerno") String rruSerno) {
        int result = pvpRruUpdAppService.deleteByPrimaryKey(rruSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpRruUpdAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }



    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody PvpRruUpdApp pvpRruUpdApp) {
        Integer  result = pvpRruUpdAppService.insertSelective(pvpRruUpdApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * 校验该借据是否已经存在一条在途的退汇申请
     * @param pvpRruUpdApp
     * @return
     */
    @PostMapping("/checkIsExistPvpRruUpdAppBizByBillNo")
    protected ResultDto<Integer> checkIsExistPvpRruUpdAppBizByBillNo(@RequestBody PvpRruUpdApp pvpRruUpdApp) {
        Integer  result = pvpRruUpdAppService.checkIsExistPvpRruUpdAppBizByBillNo(pvpRruUpdApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据借据标号查询借据信息
     * @param pvpTruPayInfo
     * @return
     */
    @PostMapping("/getAccloanByPrimary")
    protected ResultDto<AccLoan> getAccloanByPrimary(@RequestBody PvpTruPayInfo pvpTruPayInfo) {
        AccLoan  accLoan = pvpRruUpdAppService.getAccloanByPrimary(pvpTruPayInfo);
        return new ResultDto<AccLoan>(accLoan);
    }
    /**
     * 根据借据标号查询借据信息
     * @param pvpTruPayInfo
     * @return
     */
    @PostMapping("/getIqpAcct")
    protected ResultDto<IqpAcct> getIqpAcct(@RequestBody PvpTruPayInfo pvpTruPayInfo) {
        IqpAcct iqpAcct = pvpRruUpdAppService.getIqpAcct(pvpTruPayInfo.getAcctNo());
        return new ResultDto<IqpAcct>(iqpAcct);
    }


}
