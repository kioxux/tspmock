package cn.com.yusys.yusp.service.server.xdtz0051;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.server.xdtz0051.resp.Xdtz0051DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccAccpMapper;
import cn.com.yusys.yusp.repository.mapper.AccCvrsMapper;
import cn.com.yusys.yusp.repository.mapper.AccDiscMapper;
import cn.com.yusys.yusp.repository.mapper.AccEntrustLoanMapper;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.AccTfLocMapper;
import cn.com.yusys.yusp.repository.mapper.CtrAccpContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrContExtMapper;
import cn.com.yusys.yusp.repository.mapper.CtrCvrgContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrDiscContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrEntrustLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrTfLocContMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import cn.com.yusys.yusp.repository.mapper.GuarGuaranteeMapper;
import cn.com.yusys.yusp.repository.mapper.IqpContExtMapper;
import cn.com.yusys.yusp.repository.mapper.LmtAppMapper;
import cn.com.yusys.yusp.service.ICusClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version 1.0.0
 * @项目名称：
 * @类名称：
 * @类描述： #Dao类
 * @功能描述：
 * @创建人：YX-WJ
 * @创建时间：2021/6/9 22:27
 * @修改备注
 * @修改记录： 修改时间 修改人员 修改原因
 * --------------------------------------------------
 * @Copyrigth(c) 宇信科技-版权所有
 */
@Service
public class Xdtz0051Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0051Service.class);

    @Autowired
    private LmtAppMapper lmtAppMapper;

    @Autowired
    private CtrCvrgContMapper ctrCvrgContMapper;

    @Autowired
    private CtrDiscContMapper ctrDiscContMapper;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private CtrEntrustLoanContMapper ctrEntrustLoanContMapper;

    @Autowired
    private CtrTfLocContMapper ctrTfLocContMapper;

    @Autowired
    private CtrContExtMapper ctrContExtMapper;

    @Autowired
    private GrtGuarContMapper grtGuarContMapper;

    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;

    @Autowired
    private GuarGuaranteeMapper guarGuaranteeMapper;

    @Autowired
    private AccAccpMapper accAccpMapper;

    @Autowired
    private AccCvrsMapper accCvrsMapper;

    @Autowired
    private AccDiscMapper accDiscMapper;

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private AccEntrustLoanMapper accEntrustLoanMapper;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CtrAccpContMapper ctrAccpContMapper;

    @Autowired
    private IqpContExtMapper iqpContExtMapper;

    @Autowired
    private AccTfLocMapper accTfLocMapper;

    //客户存在业务信息的数量默认是0，存在是1
    private int flagInt = 0;

    public Xdtz0051DataRespDto xdtz0051(String cusId) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value);
        Xdtz0051DataRespDto xdtz0051DataRespDto = new Xdtz0051DataRespDto();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key,
                    DscmsEnum.TRADE_CODE_XDTZ0051.value, cusId);
            // 检查客户号对应胡客户是否存在
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            ResultDto<CusCorpDto> cusCorpDtoResultDto = iCusClientService.queryCusCropDtoByCusId(cusId);
            //如果不存在，返回 N
            if (cusBaseClientDto == null || cusCorpDtoResultDto.getData() == null) {
                xdtz0051DataRespDto.setOpFlag("N");
                xdtz0051DataRespDto.setOpMsg("不存在该客户，请检查客户号！");
                return xdtz0051DataRespDto;
            }
            //检查该客户是否存在业务信息，存在，则flagInt+1，结束方法
            checkIfHasBusiness(xdtz0051DataRespDto, cusId);
            //如果该客户不存在业务信息，则可以执行删除操作
            if (flagInt == 0) {
                // 根据客户号删除客户基础信息 CUS_BASE(逻辑删)
                ResultDto<Boolean> deleteCusBaseResult = iCusClientService.deleteCusBaseByCusId(cusId);
                if (deleteCusBaseResult.getData()) {
                    // 根据客户号删除对公客户基础信息 CUS_CORP  todo 可能涉及分布式事务 ，在特殊情况下第二步更新cus_corp表时发生异常，需要回滚对cus_bas表的操作
                    ResultDto<Boolean> deleteCusCorpResult = iCusClientService.deleteCusCorpByCusId(cusId);
                    if (deleteCusCorpResult.getData()) {
                        // 删除成功 返回 是，并提示 关闭成功! 未删除成功 返回 否  并提示;该客户存在业务信息，禁止关闭！
                        xdtz0051DataRespDto.setOpFlag("Y");
                        xdtz0051DataRespDto.setOpMsg("关闭成功!");
                    }
                }
            }
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value, cusId);
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value);
        return xdtz0051DataRespDto;
    }

    /**
     * @Description:存在业务信息则直接返回
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:34
     * @return: void
     **/
    private void returnErrorInfo(Xdtz0051DataRespDto xdtz0051DataRespDto) {
        xdtz0051DataRespDto.setOpFlag("N");
        xdtz0051DataRespDto.setOpMsg("该客户存在业务信息，禁止关闭！");
    }

    /**
     * @param cusId:客户号
     * @Description:检查客户是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/10 18:40
     * @return: void
     **/
    private void checkIfHasBusiness(Xdtz0051DataRespDto xdtz0051DataRespDto, String cusId) {
        // 根据客户号校验是否存在授信申请表LMT_APP信息
        flagInt += lmtAppMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在银票协议详情 CTR_ACCP_CONT
        flagInt += ctrAccpContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在保函协议详情 CTR_CVRG_CONT
        flagInt += ctrCvrgContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在贴现协议详情 CTR_DISC_CONT
        flagInt += ctrDiscContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在贷款合同详情 CTR_LOAN_CONT
        flagInt += ctrLoanContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在委托贷款合同详情 CTR_ENTRUST_LOAN_CONT
        flagInt += ctrEntrustLoanContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在开证合同详情 CTR_TF_LOC_CONT
        flagInt += ctrTfLocContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }

        // 根据客户号校验是否存在提货担保协议 CTR_TF_PGAS 老信贷无数据表 todo

        //根据客户号校验是否存在 展期协议  iqp_cont_ext
        flagInt += iqpContExtMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }

        // 根据客户号校验是否存在展期协议主表 ctr_cont_ext
        flagInt += ctrContExtMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }

        // 根据客户号校验是否存在CTR_TF_LOC_EXT 进口开证修改合同  新信贷未找到此表 todo

        // 根据客户号校验是否存在担保合同表 GRT_GUAR_CONT
        flagInt += grtGuarContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在 GUAR_BASE_INFO 抵质押信息
        flagInt += guarBaseInfoMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在保证人 GUAR_GUARANTEE 信息
        flagInt += guarGuaranteeMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在银承台账 ACC_ACCP
        flagInt += accAccpMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在保函台账 ACC_CVRS
        flagInt += accCvrsMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在贴现台账 ACC_DISC
        flagInt += accDiscMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在贷款台账 ACC_LOAN
        flagInt += accLoanMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在委托贷款台账 ACC_ENTRUST_LOAN
        flagInt += accEntrustLoanMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在信用证台帐 ACC_TF_LOC  todo
        flagInt += accTfLocMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            returnErrorInfo(xdtz0051DataRespDto);
            return;
        }
        // 根据客户号校验是否存在 ACC_TF_PGAS 提货担保台帐 信贷新信贷无此表 todo
    }
}
