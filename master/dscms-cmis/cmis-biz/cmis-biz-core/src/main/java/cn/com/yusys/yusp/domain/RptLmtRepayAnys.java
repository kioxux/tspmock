/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnys
 * @类描述: rpt_lmt_repay_anys数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-23 18:37:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_lmt_repay_anys")
public class RptLmtRepayAnys extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 系统新增流动资金测算额度 **/
	@Column(name = "NEW_WORKING_CAPITAL_CAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal newWorkingCapitalCal;
	
	/** 其他说明 **/
	@Column(name = "OTHER_INSTRUCTIONS", unique = false, nullable = true, length = 65535)
	private String otherInstructions;
	
	/** 第一还款能力分析 **/
	@Column(name = "FIRST_REPAYMENT_ABILITY_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String firstRepaymentAbilityAnalysis;
	
	/** 第二还款能力分析 **/
	@Column(name = "SECOND_REPAYMENT_ABILITY_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String secondRepaymentAbilityAnalysis;
	
	/** 其他担保方式列明相关情况 **/
	@Column(name = "OTHER_GUAR_MODE_DESC", unique = false, nullable = true, length = 65535)
	private String otherGuarModeDesc;
	
	/** 风控措施及评价 **/
	@Column(name = "RISK_CONTROL_MEASURES_EVALUATION", unique = false, nullable = true, length = 65535)
	private String riskControlMeasuresEvaluation;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param newWorkingCapitalCal
	 */
	public void setNewWorkingCapitalCal(java.math.BigDecimal newWorkingCapitalCal) {
		this.newWorkingCapitalCal = newWorkingCapitalCal;
	}
	
    /**
     * @return newWorkingCapitalCal
     */
	public java.math.BigDecimal getNewWorkingCapitalCal() {
		return this.newWorkingCapitalCal;
	}
	
	/**
	 * @param otherInstructions
	 */
	public void setOtherInstructions(String otherInstructions) {
		this.otherInstructions = otherInstructions;
	}
	
    /**
     * @return otherInstructions
     */
	public String getOtherInstructions() {
		return this.otherInstructions;
	}
	
	/**
	 * @param firstRepaymentAbilityAnalysis
	 */
	public void setFirstRepaymentAbilityAnalysis(String firstRepaymentAbilityAnalysis) {
		this.firstRepaymentAbilityAnalysis = firstRepaymentAbilityAnalysis;
	}
	
    /**
     * @return firstRepaymentAbilityAnalysis
     */
	public String getFirstRepaymentAbilityAnalysis() {
		return this.firstRepaymentAbilityAnalysis;
	}
	
	/**
	 * @param secondRepaymentAbilityAnalysis
	 */
	public void setSecondRepaymentAbilityAnalysis(String secondRepaymentAbilityAnalysis) {
		this.secondRepaymentAbilityAnalysis = secondRepaymentAbilityAnalysis;
	}
	
    /**
     * @return secondRepaymentAbilityAnalysis
     */
	public String getSecondRepaymentAbilityAnalysis() {
		return this.secondRepaymentAbilityAnalysis;
	}
	
	/**
	 * @param otherGuarModeDesc
	 */
	public void setOtherGuarModeDesc(String otherGuarModeDesc) {
		this.otherGuarModeDesc = otherGuarModeDesc;
	}
	
    /**
     * @return otherGuarModeDesc
     */
	public String getOtherGuarModeDesc() {
		return this.otherGuarModeDesc;
	}
	
	/**
	 * @param riskControlMeasuresEvaluation
	 */
	public void setRiskControlMeasuresEvaluation(String riskControlMeasuresEvaluation) {
		this.riskControlMeasuresEvaluation = riskControlMeasuresEvaluation;
	}
	
    /**
     * @return riskControlMeasuresEvaluation
     */
	public String getRiskControlMeasuresEvaluation() {
		return this.riskControlMeasuresEvaluation;
	}


}