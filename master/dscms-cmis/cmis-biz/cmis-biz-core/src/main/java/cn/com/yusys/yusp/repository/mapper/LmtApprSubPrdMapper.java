/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import cn.com.yusys.yusp.domain.LmtApprSub;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtApprSubPrd;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprSubPrdMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:35:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtApprSubPrdMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtApprSubPrd selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtApprSubPrd> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtApprSubPrd record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtApprSubPrd record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtApprSubPrd record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtApprSubPrd record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectLmtApprSubPrdByParams
     * @方法描述: 根据条件查询授信审批分项适用品种数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtApprSubPrd> selectLmtApprSubPrdByParams(HashMap paramsMap);


    /**
     * @方法名称: queryLmtApproveSubPrdByParams
     * @方法描述: 根据条件查询授
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtApprSubPrd> queryLmtApproveSubPrdByParams(HashMap<String, String> queryMaps);

    /**
     * 通过入参查询授信申请信息
     *
     * @param queryMap
     * @return
     */
    List<LmtApprSubPrd> selectByParams(Map queryMap);

    /**
     * @方法名称: updateByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPkId(LmtApprSubPrd record);

    /**
     * @方法名称: calLmtAmt
     * @方法描述: 根据授信分项编号（sub_serno）计算授信额度总和
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal calLmtAmt(String subSerno);

    /**
     * 根据适用分项产品编号获取适用产品对象
     *
     * @param subPrdSerno
     * @return
     */
    LmtApprSubPrd selectBySubPrdSerno(String subPrdSerno);

    /**
     * 根据申请流水号与
     * @param serno
     * @param lmtBizType
     * @return
     */
    ArrayList<LmtApprSubPrd> queryLmtApprSubBySerno(@Param("serno") String serno, @Param("lmtBizType") String lmtBizType);

    /**
     * @函数名称: queryLmtApprSubDataBySerno
     * @函数描述: 根据申请流水号查询授信审批过程中分项品种数据
     * @创建人: css
     */

    ArrayList<LmtApprSubPrd> queryLmtApprSubDataBySerno(Map<String, String> params);

    /**
     * @函数名称: queryLmtApprSubDataByApprSerno
     * @函数描述: 根据审批流水号查询授信审批过程中分项品种数据
     * @创建人: css
     */

    ArrayList<LmtApprSubPrd> queryLmtApprSubDataByApprSerno(Map<String, String> params);

    /**
     * @方法名称: updateSxkdPrdAmtBySubSerno
     * @方法描述: 根据分项流水号更新省心快贷金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateSxkdPrdAmtBySubSerno(LmtApprSubPrd record);

}