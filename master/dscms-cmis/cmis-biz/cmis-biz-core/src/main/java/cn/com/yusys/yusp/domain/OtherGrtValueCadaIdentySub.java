/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherGrtValueCadaIdentySub
 * @类描述: other_grt_value_cada_identy_sub数据实体类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-11 16:29:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_grt_value_cada_identy_sub")
public class OtherGrtValueCadaIdentySub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = false, length = 40)
	private String guarNo;
	
	/** 房产评估价值 **/
	@Column(name = "REALPRO_EVAL_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realproEvalValue;
	
	/** 土地评估价值 **/
	@Column(name = "LAND_EVAL_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal landEvalValue;
	
	/** 房地产评估总价 **/
	@Column(name = "REALPRO_EVAL_TOTAL_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realproEvalTotalValue;
	
	/** 评估机构名称 **/
	@Column(name = "EVAL_ORG_NAME", unique = false, nullable = true, length = 100)
	private String evalOrgName;
	
	/** 评估报告出具时间 **/
	@Column(name = "EVAL_REPORT_TIME", unique = false, nullable = true, length = 20)
	private String evalReportTime;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param realproEvalValue
	 */
	public void setRealproEvalValue(java.math.BigDecimal realproEvalValue) {
		this.realproEvalValue = realproEvalValue;
	}
	
    /**
     * @return realproEvalValue
     */
	public java.math.BigDecimal getRealproEvalValue() {
		return this.realproEvalValue;
	}
	
	/**
	 * @param landEvalValue
	 */
	public void setLandEvalValue(java.math.BigDecimal landEvalValue) {
		this.landEvalValue = landEvalValue;
	}
	
    /**
     * @return landEvalValue
     */
	public java.math.BigDecimal getLandEvalValue() {
		return this.landEvalValue;
	}
	
	/**
	 * @param realproEvalTotalValue
	 */
	public void setRealproEvalTotalValue(java.math.BigDecimal realproEvalTotalValue) {
		this.realproEvalTotalValue = realproEvalTotalValue;
	}
	
    /**
     * @return realproEvalTotalValue
     */
	public java.math.BigDecimal getRealproEvalTotalValue() {
		return this.realproEvalTotalValue;
	}
	
	/**
	 * @param evalOrgName
	 */
	public void setEvalOrgName(String evalOrgName) {
		this.evalOrgName = evalOrgName;
	}
	
    /**
     * @return evalOrgName
     */
	public String getEvalOrgName() {
		return this.evalOrgName;
	}
	
	/**
	 * @param evalReportTime
	 */
	public void setEvalReportTime(String evalReportTime) {
		this.evalReportTime = evalReportTime;
	}
	
    /**
     * @return evalReportTime
     */
	public String getEvalReportTime() {
		return this.evalReportTime;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}