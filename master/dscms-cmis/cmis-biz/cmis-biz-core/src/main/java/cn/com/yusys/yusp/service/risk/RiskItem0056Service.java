package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CtrLoanContService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021年7月28日16:04:21
 * @desc 优惠利率校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0056Service {

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021年7月30日16:06:37
     * @version 1.0.0
     * @desc    零售合同申请时，合同的利率<=业务申请时的利率
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0056(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String contNo = queryModel.getCondition().get("bizId").toString();
        String prdType = StringUtils.EMPTY;
        if (StringUtils.isBlank(contNo)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 通过申请流水号获取申请信息
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByContno(contNo);
        if(Objects.isNull(ctrLoanCont) || StringUtils.isBlank(ctrLoanCont.getIqpSerno()) || Objects.isNull(ctrLoanCont.getExecRateYear())){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007); //通过合同申请流水号未获取到对应的合同申请信息
            return riskResultDto;
        }
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(ctrLoanCont.getIqpSerno());
        if(Objects.isNull(iqpLoanApp) || Objects.isNull(iqpLoanApp.getExecRateYear())){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003); //通过授信申请流水号未获取到对应的授信申请信息
            return riskResultDto;
        }else{
            if(ctrLoanCont.getExecRateYear().compareTo(iqpLoanApp.getExecRateYear()) < 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("修改利率不能低于业务申请时的默认利率！");
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
