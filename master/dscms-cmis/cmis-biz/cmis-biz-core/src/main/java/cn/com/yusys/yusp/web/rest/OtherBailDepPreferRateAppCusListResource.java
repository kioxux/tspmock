/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherBailDepPreferRateAppCusList;
import cn.com.yusys.yusp.service.OtherBailDepPreferRateAppCusListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherBailDepPreferRateAppCusListResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-11 09:52:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otherbaildeppreferrateappcuslist")
public class OtherBailDepPreferRateAppCusListResource {
    @Autowired
    private OtherBailDepPreferRateAppCusListService otherBailDepPreferRateAppCusListService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherBailDepPreferRateAppCusList>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherBailDepPreferRateAppCusList> list = otherBailDepPreferRateAppCusListService.selectAll(queryModel);
        return new ResultDto<List<OtherBailDepPreferRateAppCusList>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherBailDepPreferRateAppCusList>> index(QueryModel queryModel) {
        List<OtherBailDepPreferRateAppCusList> list = otherBailDepPreferRateAppCusListService.selectByModel(queryModel);
        return new ResultDto<List<OtherBailDepPreferRateAppCusList>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherBailDepPreferRateAppCusList> create(@RequestBody OtherBailDepPreferRateAppCusList otherBailDepPreferRateAppCusList) throws URISyntaxException {
        otherBailDepPreferRateAppCusListService.insert(otherBailDepPreferRateAppCusList);
        return new ResultDto<OtherBailDepPreferRateAppCusList>(otherBailDepPreferRateAppCusList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherBailDepPreferRateAppCusList otherBailDepPreferRateAppCusList) throws URISyntaxException {
        int result = otherBailDepPreferRateAppCusListService.update(otherBailDepPreferRateAppCusList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody Map condition) {
        int result = otherBailDepPreferRateAppCusListService.deleteByPrimaryKey(
                condition.get("serno").toString(), condition.get("cusId").toString());
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: addotherbaildeppreferrateappcuslist
     * @方法描述: 新增保证金存款特惠利率登记
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/addotherbaildeppreferrateappcuslist")
    protected ResultDto addotherbaildeppreferrateappcuslist(@RequestBody OtherBailDepPreferRateAppCusList otherBailDepPreferRateAppCusList) {
        return otherBailDepPreferRateAppCusListService.addotherbaildeppreferrateappcuslist(otherBailDepPreferRateAppCusList);
    }

    /**
     * @方法名称: updateotherbaildeppreferrateappcuslist
     * @方法描述: 修改保证金存款特惠利率登记
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/updateotherbaildeppreferrateappcuslist")
    protected ResultDto updateotherbaildeppreferrateappcuslist(@RequestBody OtherBailDepPreferRateAppCusList otherBailDepPreferRateAppCusList) {
        return otherBailDepPreferRateAppCusListService.updateotherbaildeppreferrateappcuslist(otherBailDepPreferRateAppCusList);
    }

    /**
     * @函数名称:deleteInfo
     * @函数描述:单个对象删除，将操作类型置为删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteInfo/{cusInfo}")
    protected ResultDto<Integer> deleteInfo(@PathVariable("cusInfo") String cusInfo) {
        int result = otherBailDepPreferRateAppCusListService.deleteInfo(cusInfo);
        return new ResultDto<Integer>(result);
    }

}
