package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @className IqpLoanAppSpecialDto
 * @Description 特殊业务申请dto
 * @Date 2020/12/3 : 17:11
 */

public class IqpLoanAppSpecialDto implements Serializable {
    private BigDecimal rulingIrM;

    public BigDecimal getRulingIrM() { return rulingIrM; }
    public void setRulingIrM(BigDecimal rulingIrM) { this.rulingIrM = rulingIrM; }

    private BigDecimal irAdjustTerm;

    public BigDecimal getIrAdjustTerm() { return irAdjustTerm; }
    public void setIrAdjustTerm(BigDecimal irAdjustTerm) { this.irAdjustTerm = irAdjustTerm; }

    private BigDecimal riskOpenAmt;

    public BigDecimal getRiskOpenAmt() { return riskOpenAmt; }
    public void setRiskOpenAmt(BigDecimal riskOpenAmt) { this.riskOpenAmt = riskOpenAmt; }

    private BigDecimal repayTermOne;

    public BigDecimal getRepayTermOne() { return repayTermOne; }
    public void setRepayTermOne(BigDecimal repayTermOne) { this.repayTermOne = repayTermOne; }

    private BigDecimal realityIrM;

    public BigDecimal getRealityIrM() { return realityIrM; }
    public void setRealityIrM(BigDecimal realityIrM) { this.realityIrM = realityIrM; }

    private BigDecimal rulingIr;

    public BigDecimal getRulingIr() { return rulingIr; }
    public void setRulingIr(BigDecimal rulingIr) { this.rulingIr = rulingIr; }

    private BigDecimal repayDate;

    public BigDecimal getRepayDate() { return repayDate; }
    public void setRepayDate(BigDecimal repayDate) { this.repayDate = repayDate; }

    private BigDecimal reserveAmt;

    public BigDecimal getReserveAmt() { return reserveAmt; }
    public void setReserveAmt(BigDecimal reserveAmt) { this.reserveAmt = reserveAmt; }

    private BigDecimal intGraperDay;

    public BigDecimal getIntGraperDay() { return intGraperDay; }
    public void setIntGraperDay(BigDecimal intGraperDay) { this.intGraperDay = intGraperDay; }

    private BigDecimal exchangeRate;

    public BigDecimal getExchangeRate() { return exchangeRate; }
    public void setExchangeRate(BigDecimal exchangeRate) { this.exchangeRate = exchangeRate; }

    private BigDecimal lmtAmt;

    public BigDecimal getLmtAmt() { return lmtAmt; }
    public void setLmtAmt(BigDecimal lmtAmt) { this.lmtAmt = lmtAmt; }

    private BigDecimal reserveTerm;

    public BigDecimal getReserveTerm() { return reserveTerm; }
    public void setReserveTerm(BigDecimal reserveTerm) { this.reserveTerm = reserveTerm; }

    private BigDecimal capGraperDay;

    public BigDecimal getCapGraperDay() { return capGraperDay; }
    public void setCapGraperDay(BigDecimal capGraperDay) { this.capGraperDay = capGraperDay; }

    private BigDecimal irFloatPoint;

    public BigDecimal getIrFloatPoint() { return irFloatPoint; }
    public void setIrFloatPoint(BigDecimal irFloatPoint) { this.irFloatPoint = irFloatPoint; }

    private BigDecimal repayTermTwo;

    public BigDecimal getRepayTermTwo() { return repayTermTwo; }
    public void setRepayTermTwo(BigDecimal repayTermTwo) { this.repayTermTwo = repayTermTwo; }

    private BigDecimal liquFreeTime;

    public BigDecimal getLiquFreeTime() { return liquFreeTime; }
    public void setLiquFreeTime(BigDecimal liquFreeTime) { this.liquFreeTime = liquFreeTime; }

    private BigDecimal appAmt;

    public BigDecimal getAppAmt() { return appAmt; }
    public void setAppAmt(BigDecimal appAmt) { this.appAmt = appAmt; }

    private BigDecimal repayFre;

    public BigDecimal getRepayFre() { return repayFre; }
    public void setRepayFre(BigDecimal repayFre) { this.repayFre = repayFre; }

    private BigDecimal defaultPoint;

    public BigDecimal getDefaultPoint() { return defaultPoint; }
    public void setDefaultPoint(BigDecimal defaultPoint) { this.defaultPoint = defaultPoint; }

    private BigDecimal irFloatRate;

    public BigDecimal getIrFloatRate() { return irFloatRate; }
    public void setIrFloatRate(BigDecimal irFloatRate) { this.irFloatRate = irFloatRate; }

    private BigDecimal overduePoint;

    public BigDecimal getOverduePoint() { return overduePoint; }
    public void setOverduePoint(BigDecimal overduePoint) { this.overduePoint = overduePoint; }

    private BigDecimal sbsyPerc;

    public BigDecimal getSbsyPerc() { return sbsyPerc; }
    public void setSbsyPerc(BigDecimal sbsyPerc) { this.sbsyPerc = sbsyPerc; }

    private BigDecimal exchangeRateAmt;

    public BigDecimal getExchangeRateAmt() { return exchangeRateAmt; }
    public void setExchangeRateAmt(BigDecimal exchangeRateAmt) { this.exchangeRateAmt = exchangeRateAmt; }

    private BigDecimal appTerm;

    public BigDecimal getAppTerm() { return appTerm; }
    public void setAppTerm(BigDecimal appTerm) { this.appTerm = appTerm; }

    private BigDecimal bailRate;

    public BigDecimal getBailRate() { return bailRate; }
    public void setBailRate(BigDecimal bailRate) { this.bailRate = bailRate; }

    private BigDecimal availableAmt;

    public BigDecimal getAvailableAmt() { return availableAmt; }
    public void setAvailableAmt(BigDecimal availableAmt) { this.availableAmt = availableAmt; }

    private BigDecimal overdueRateY;

    public BigDecimal getOverdueRateY() { return overdueRateY; }
    public void setOverdueRateY(BigDecimal overdueRateY) { this.overdueRateY = overdueRateY; }

    private BigDecimal repayAmtTwo;

    public BigDecimal getRepayAmtTwo() { return repayAmtTwo; }
    public void setRepayAmtTwo(BigDecimal repayAmtTwo) { this.repayAmtTwo = repayAmtTwo; }

    private BigDecimal defaultRateY;

    public BigDecimal getDefaultRateY() { return defaultRateY; }
    public void setDefaultRateY(BigDecimal defaultRateY) { this.defaultRateY = defaultRateY; }

    private BigDecimal irFloatPointTemp;

    public BigDecimal getIrFloatPointTemp() { return irFloatPointTemp; }
    public void setIrFloatPointTemp(BigDecimal irFloatPointTemp) { this.irFloatPointTemp = irFloatPointTemp; }

    private BigDecimal appRate;

    public BigDecimal getAppRate() { return appRate; }
    public void setAppRate(BigDecimal appRate) { this.appRate = appRate; }

    private BigDecimal securityAmt;

    public BigDecimal getSecurityAmt() { return securityAmt; }
    public void setSecurityAmt(BigDecimal securityAmt) { this.securityAmt = securityAmt; }

    private BigDecimal appRateAmt;

    public BigDecimal getAppRateAmt() { return appRateAmt; }
    public void setAppRateAmt(BigDecimal appRateAmt) { this.appRateAmt = appRateAmt; }

    private BigDecimal calTerm;

    public BigDecimal getCalTerm() { return calTerm; }
    public void setCalTerm(BigDecimal calTerm) { this.calTerm = calTerm; }

    private BigDecimal overdueRate;

    public BigDecimal getOverdueRate() { return overdueRate; }
    public void setOverdueRate(BigDecimal overdueRate) { this.overdueRate = overdueRate; }

    private BigDecimal realityIrY;

    public BigDecimal getRealityIrY() { return realityIrY; }
    public void setRealityIrY(BigDecimal realityIrY) { this.realityIrY = realityIrY; }

    private BigDecimal sbsyAmt;

    public BigDecimal getSbsyAmt() { return sbsyAmt; }
    public void setSbsyAmt(BigDecimal sbsyAmt) { this.sbsyAmt = sbsyAmt; }

    private BigDecimal defaultRate;

    public BigDecimal getDefaultRate() { return defaultRate; }
    public void setDefaultRate(BigDecimal defaultRate) { this.defaultRate = defaultRate; }

    private String irFloatType;

    public String getIrFloatType() { return irFloatType; }
    public void setIrFloatType(String irFloatType) { this.irFloatType = irFloatType; }

    private String repaySrcDes;

    public String getRepaySrcDes() { return repaySrcDes; }
    public void setRepaySrcDes(String repaySrcDes) { this.repaySrcDes = repaySrcDes; }

    private String lmtCtrNo;

    public String getLmtCtrNo() { return lmtCtrNo; }
    public void setLmtCtrNo(String lmtCtrNo) { this.lmtCtrNo = lmtCtrNo; }

    private String irAdjustType;

    public String getIrAdjustType() { return irAdjustType; }
    public void setIrAdjustType(String irAdjustType) { this.irAdjustType = irAdjustType; }

    private String loanTypeName;

    public String getLoanTypeName() { return loanTypeName; }
    public void setLoanTypeName(String loanTypeName) { this.loanTypeName = loanTypeName; }

    private String billNo;

    public String getBillNo() { return billNo; }
    public void setBillNo(String billNo) { this.billNo = billNo; }

    private String repayType;

    public String getRepayType() { return repayType; }
    public void setRepayType(String repayType) { this.repayType = repayType; }

    private String updBrId;

    public String getUpdBrId() { return updBrId; }
    public void setUpdBrId(String updBrId) { this.updBrId = updBrId; }

    private String termType;

    public String getTermType() { return termType; }
    public void setTermType(String termType) { this.termType = termType; }

    private String appDate;

    public String getAppDate() { return appDate; }
    public void setAppDate(String appDate) { this.appDate = appDate; }

    private String sbsyMode;

    public String getSbsyMode() { return sbsyMode; }
    public void setSbsyMode(String sbsyMode) { this.sbsyMode = sbsyMode; }

    private String intGraperType;

    public String getIntGraperType() { return intGraperType; }
    public void setIntGraperType(String intGraperType) { this.intGraperType = intGraperType; }

    private String loanType;

    public String getLoanType() { return loanType; }
    public void setLoanType(String loanType) { this.loanType = loanType; }

    private String equipNo;

    public String getEquipNo() { return equipNo; }
    public void setEquipNo(String equipNo) { this.equipNo = equipNo; }

    private String bailSour;

    public String getBailSour() { return bailSour; }
    public void setBailSour(String bailSour) { this.bailSour = bailSour; }

    private String stopPintTerm;

    public String getStopPintTerm() { return stopPintTerm; }
    public void setStopPintTerm(String stopPintTerm) { this.stopPintTerm = stopPintTerm; }

    private String inputBrIdName;

    public String getInputBrIdName() { return inputBrIdName; }
    public void setInputBrIdName(String inputBrIdName) { this.inputBrIdName = inputBrIdName; }

    private String updDate;

    public String getUpdDate() { return updDate; }
    public void setUpdDate(String updDate) { this.updDate = updDate; }

    private String defrayMode;

    public String getDefrayMode() { return defrayMode; }
    public void setDefrayMode(String defrayMode) { this.defrayMode = defrayMode; }

    private String praType;

    public String getPraType() { return praType; }
    public void setPraType(String praType) { this.praType = praType; }

    private String sbsyUnitName;

    public String getSbsyUnitName() { return sbsyUnitName; }
    public void setSbsyUnitName(String sbsyUnitName) { this.sbsyUnitName = sbsyUnitName; }

    private String inputDate;

    public String getInputDate() { return inputDate; }
    public void setInputDate(String inputDate) { this.inputDate = inputDate; }

    private String repayRule;

    public String getRepayRule() { return repayRule; }
    public void setRepayRule(String repayRule) { this.repayRule = repayRule; }

    private String updBrIdName;

    public String getUpdBrIdName() { return updBrIdName; }
    public void setUpdBrIdName(String updBrIdName) { this.updBrIdName = updBrIdName; }

    private String irAccordType;

    public String getIrAccordType() { return irAccordType; }
    public void setIrAccordType(String irAccordType) { this.irAccordType = irAccordType; }

    private String cusId;

    public String getCusId() { return cusId; }
    public void setCusId(String cusId) { this.cusId = cusId; }

    private String prdId;

    public String getPrdId() { return prdId; }
    public void setPrdId(String prdId) { this.prdId = prdId; }

    private String repayTerm;

    public String getRepayTerm() { return repayTerm; }
    public void setRepayTerm(String repayTerm) { this.repayTerm = repayTerm; }

    private String lmtEndDate;

    public String getLmtEndDate() { return lmtEndDate; }
    public void setLmtEndDate(String lmtEndDate) { this.lmtEndDate = lmtEndDate; }

    private String chnlSour;

    public String getChnlSour() { return chnlSour; }
    public void setChnlSour(String chnlSour) { this.chnlSour = chnlSour; }

    private String prdName;

    public String getPrdName() { return prdName; }
    public void setPrdName(String prdName) { this.prdName = prdName; }

    private String loanRatType;

    public String getLoanRatType() { return loanRatType; }
    public void setLoanRatType(String loanRatType) { this.loanRatType = loanRatType; }

    private String deductDeduType;

    public String getDeductDeduType() { return deductDeduType; }
    public void setDeductDeduType(String deductDeduType) { this.deductDeduType = deductDeduType; }

    private String repayAmtOne;

    public String getRepayAmtOne() { return repayAmtOne; }
    public void setRepayAmtOne(String repayAmtOne) { this.repayAmtOne = repayAmtOne; }

    private String isCulEstate;

    public String getIsCulEstate() { return isCulEstate; }
    public void setIsCulEstate(String isCulEstate) { this.isCulEstate = isCulEstate; }

    private String strategyNewLoanName;

    public String getStrategyNewLoanName() { return strategyNewLoanName; }
    public void setStrategyNewLoanName(String strategyNewLoanName) { this.strategyNewLoanName = strategyNewLoanName; }

    private String updId;

    public String getUpdId() { return updId; }
    public void setUpdId(String updId) { this.updId = updId; }

    private String lmtStarDate;

    public String getLmtStarDate() { return lmtStarDate; }
    public void setLmtStarDate(String lmtStarDate) { this.lmtStarDate = lmtStarDate; }

    private String capGraperType;

    public String getCapGraperType() { return capGraperType; }
    public void setCapGraperType(String capGraperType) { this.capGraperType = capGraperType; }

    private String managerBrId;

    public String getManagerBrId() { return managerBrId; }
    public void setManagerBrId(String managerBrId) { this.managerBrId = managerBrId; }

    private String thirdLimitId;

    public String getThirdLimitId() { return thirdLimitId; }
    public void setThirdLimitId(String thirdLimitId) { this.thirdLimitId = thirdLimitId; }

    private String cusName;

    public String getCusName() { return cusName; }
    public void setCusName(String cusName) { this.cusName = cusName; }

    private String appCurType;

    public String getAppCurType() { return appCurType; }
    public void setAppCurType(String appCurType) { this.appCurType = appCurType; }

    private String fiveClass;

    public String getFiveClass() { return fiveClass; }
    public void setFiveClass(String fiveClass) { this.fiveClass = fiveClass; }

    private String inputBrId;

    public String getInputBrId() { return inputBrId; }
    public void setInputBrId(String inputBrId) { this.inputBrId = inputBrId; }

    private String guarWay;

    public String getGuarWay() { return guarWay; }
    public void setGuarWay(String guarWay) { this.guarWay = guarWay; }

    private String rateSelType;

    public String getRateSelType() { return rateSelType; }
    public void setRateSelType(String rateSelType) { this.rateSelType = rateSelType; }

    private String thirdLimitType;

    public String getThirdLimitType() { return thirdLimitType; }
    public void setThirdLimitType(String thirdLimitType) { this.thirdLimitType = thirdLimitType; }

    private String repayDtType;

    public String getRepayDtType() { return repayDtType; }
    public void setRepayDtType(String repayDtType) { this.repayDtType = repayDtType; }

    private String iqpSerno;

    public String getIqpSerno() { return iqpSerno; }
    public void setIqpSerno(String iqpSerno) { this.iqpSerno = iqpSerno; }

    private String sbsyAcctName;

    public String getSbsyAcctName() { return sbsyAcctName; }
    public void setSbsyAcctName(String sbsyAcctName) { this.sbsyAcctName = sbsyAcctName; }

    private String inputId;

    public String getInputId() { return inputId; }
    public void setInputId(String inputId) { this.inputId = inputId; }

    private String overdueFloatType;

    public String getOverdueFloatType() { return overdueFloatType; }
    public void setOverdueFloatType(String overdueFloatType) { this.overdueFloatType = overdueFloatType; }

    private String loanCha;

    public String getLoanCha() { return loanCha; }
    public void setLoanCha(String loanCha) { this.loanCha = loanCha; }

    private String rateType;

    public String getRateType() { return rateType; }
    public void setRateType(String rateType) { this.rateType = rateType; }

    private String inputIdName;

    public String getInputIdName() { return inputIdName; }
    public void setInputIdName(String inputIdName) { this.inputIdName = inputIdName; }

    private String irType;

    public String getIrType() { return irType; }
    public void setIrType(String irType) { this.irType = irType; }

    private String loanModal;

    public String getLoanModal() { return loanModal; }
    public void setLoanModal(String loanModal) { this.loanModal = loanModal; }

    private String defaultFloatType;

    public String getDefaultFloatType() { return defaultFloatType; }
    public void setDefaultFloatType(String defaultFloatType) { this.defaultFloatType = defaultFloatType; }

    private String repayFreType;

    public String getRepayFreType() { return repayFreType; }
    public void setRepayFreType(String repayFreType) { this.repayFreType = repayFreType; }

    private String isCommonRqstr;

    public String getIsCommonRqstr() { return isCommonRqstr; }
    public void setIsCommonRqstr(String isCommonRqstr) { this.isCommonRqstr = isCommonRqstr; }

    private String iqpChnlSour;

    public String getIqpChnlSour() { return iqpChnlSour; }
    public void setIqpChnlSour(String iqpChnlSour) { this.iqpChnlSour = iqpChnlSour; }

    private String serno;

    public String getSerno() { return serno; }
    public void setSerno(String serno) { this.serno = serno; }

    private String comUpIndtify;

    public String getComUpIndtify() { return comUpIndtify; }
    public void setComUpIndtify(String comUpIndtify) { this.comUpIndtify = comUpIndtify; }

    private String bizType;

    public String getBizType() { return bizType; }
    public void setBizType(String bizType) { this.bizType = bizType; }

    private String managerBrIdName;

    public String getManagerBrIdName() { return managerBrIdName; }
    public void setManagerBrIdName(String managerBrIdName) { this.managerBrIdName = managerBrIdName; }

    private String isCfirmPayWay;

    public String getIsCfirmPayWay() { return isCfirmPayWay; }
    public void setIsCfirmPayWay(String isCfirmPayWay) { this.isCfirmPayWay = isCfirmPayWay; }

    private String newPrdLoan;

    public String getNewPrdLoan() { return newPrdLoan; }
    public void setNewPrdLoan(String newPrdLoan) { this.newPrdLoan = newPrdLoan; }

    private String bailCurType;

    public String getBailCurType() { return bailCurType; }
    public void setBailCurType(String bailCurType) { this.bailCurType = bailCurType; }

    private String updIdName;

    public String getUpdIdName() { return updIdName; }
    public void setUpdIdName(String updIdName) { this.updIdName = updIdName; }

    private String strategyNewLoan;

    public String getStrategyNewLoan() { return strategyNewLoan; }
    public void setStrategyNewLoan(String strategyNewLoan) { this.strategyNewLoan = strategyNewLoan; }

    private String thirdLimitName;

    public String getThirdLimitName() { return thirdLimitName; }
    public void setThirdLimitName(String thirdLimitName) { this.thirdLimitName = thirdLimitName; }

    private String loanDirection;

    public String getLoanDirection() { return loanDirection; }
    public void setLoanDirection(String loanDirection) { this.loanDirection = loanDirection; }

    private String sbsyAcct;

    public String getSbsyAcct() { return sbsyAcct; }
    public void setSbsyAcct(String sbsyAcct) { this.sbsyAcct = sbsyAcct; }

    private String subType;

    public String getSubType() { return subType; }
    public void setSubType(String subType) { this.subType = subType; }

    private String estateAdjustType;

    public String getEstateAdjustType() { return estateAdjustType; }
    public void setEstateAdjustType(String estateAdjustType) { this.estateAdjustType = estateAdjustType; }

    private String repaySpace;

    public String getRepaySpace() { return repaySpace; }
    public void setRepaySpace(String repaySpace) { this.repaySpace = repaySpace; }

    private String loanDirectionName;

    public String getLoanDirectionName() { return loanDirectionName; }
    public void setLoanDirectionName(String loanDirectionName) { this.loanDirectionName = loanDirectionName; }

    private String especBizType;//特殊业务类型

    public void setEspecBizType(String especBizType) {
        this.especBizType = especBizType;
    }

    public String getEspecBizType() {
        return especBizType;
    }

    //授信申请业务流水号
    private String lmtSerno;

    public void setLmtSerno(String lmtSerno) {
        this.lmtSerno = lmtSerno;
    }

    public String getLmtSerno() {
        return lmtSerno;
    }
}
