/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydCusInvoice
 * @类描述: tmp_wyd_cus_invoice数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_cus_invoice_tmp")
public class TmpWydCusInvoiceTmp {
	
	/** 借据号 **/
	@Column(name = "LENDING_REF")
	private String lendingRef;
	
	/** 交易流水号 **/
	@Column(name = "REF_NO")
	private String refNo;
	
	/** 交易日期 **/
	@Column(name = "TRANS_DATE")
	private java.util.Date transDate;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 申请日期 **/
	@Column(name = "APPLY_DATE", unique = false, nullable = true)
	private java.util.Date applyDate;
	
	/** 单位名称 **/
	@Column(name = "ENP_NAME", unique = false, nullable = true, length = 255)
	private String enpName;
	
	/** 纳税人识别号 **/
	@Column(name = "TAX_PAYER_ID", unique = false, nullable = true, length = 64)
	private String taxPayerId;
	
	/** 单位地址 **/
	@Column(name = "ENP_ADDRESS", unique = false, nullable = true, length = 1024)
	private String enpAddress;
	
	/** 单位电话 **/
	@Column(name = "ENP_PHONE", unique = false, nullable = true, length = 30)
	private String enpPhone;
	
	/** 开户银行 **/
	@Column(name = "OPEN_ACCT_BANK_NAME", unique = false, nullable = true, length = 32)
	private String openAcctBankName;
	
	/** 银行账号 **/
	@Column(name = "OPEN_ACCT_BANK_NUM", unique = false, nullable = true, length = 32)
	private String openAcctBankNum;
	
	/** 开票金额（元） **/
	@Column(name = "INV_AMOUNT", unique = false, nullable = true, length = 16)
	private String invAmount;
	
	/** 借款合同号 **/
	@Column(name = "CONTRACT_NO", unique = false, nullable = true, length = 64)
	private String contractNo;
	
	/** 还利息金额（元） **/
	@Column(name = "I_PAY", unique = false, nullable = true, length = 16)
	private String iPay;
	
	/** 还罚息金额（元） **/
	@Column(name = "PP_PAY", unique = false, nullable = true, length = 16)
	private String ppPay;
	
	/** 归属于资金端信托还利息金额（元） **/
	@Column(name = "FUND_INTEREST", unique = false, nullable = true, length = 16)
	private String fundInterest;
	
	/** 归属于资金端信托还罚息（元） **/
	@Column(name = "FUND_PENALTY", unique = false, nullable = true, length = 16)
	private String fundPenalty;
	
	/** 归属于票据信托还利息金额（元） **/
	@Column(name = "BILL_INTEREST", unique = false, nullable = true, length = 16)
	private String billInterest;
	
	/** 归属于票据信托还罚息（元） **/
	@Column(name = "BILL_PENALTY", unique = false, nullable = true, length = 16)
	private String billPenalty;
	
	/** 开票方 **/
	@Column(name = "INV_PARTY", unique = false, nullable = true, length = 255)
	private String invParty;
	
	/** 开票方式 **/
	@Column(name = "INV_TYPE", unique = false, nullable = true, length = 4)
	private String invType;
	
	/** 收件人 **/
	@Column(name = "RECIPIENT_NAME", unique = false, nullable = true, length = 128)
	private String recipientName;
	
	/** 联系电话 **/
	@Column(name = "CONTACT_NUM", unique = false, nullable = true, length = 18)
	private String contactNum;
	
	/** 邮寄地址 **/
	@Column(name = "ADDRESS", unique = false, nullable = true, length = 1024)
	private String address;
	
	/** 电子邮箱 **/
	@Column(name = "EMAIL_ADDR", unique = false, nullable = true, length = 64)
	private String emailAddr;
	
	/** 快递单号 **/
	@Column(name = "EXPRESS_NUM", unique = false, nullable = true, length = 64)
	private String expressNum;
	
	/** 发票状态 **/
	@Column(name = "INV_STATUS", unique = false, nullable = true, length = 4)
	private String invStatus;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param applyDate
	 */
	public void setApplyDate(java.util.Date applyDate) {
		this.applyDate = applyDate;
	}
	
    /**
     * @return applyDate
     */
	public java.util.Date getApplyDate() {
		return this.applyDate;
	}
	
	/**
	 * @param enpName
	 */
	public void setEnpName(String enpName) {
		this.enpName = enpName;
	}
	
    /**
     * @return enpName
     */
	public String getEnpName() {
		return this.enpName;
	}
	
	/**
	 * @param taxPayerId
	 */
	public void setTaxPayerId(String taxPayerId) {
		this.taxPayerId = taxPayerId;
	}
	
    /**
     * @return taxPayerId
     */
	public String getTaxPayerId() {
		return this.taxPayerId;
	}
	
	/**
	 * @param enpAddress
	 */
	public void setEnpAddress(String enpAddress) {
		this.enpAddress = enpAddress;
	}
	
    /**
     * @return enpAddress
     */
	public String getEnpAddress() {
		return this.enpAddress;
	}
	
	/**
	 * @param enpPhone
	 */
	public void setEnpPhone(String enpPhone) {
		this.enpPhone = enpPhone;
	}
	
    /**
     * @return enpPhone
     */
	public String getEnpPhone() {
		return this.enpPhone;
	}
	
	/**
	 * @param openAcctBankName
	 */
	public void setOpenAcctBankName(String openAcctBankName) {
		this.openAcctBankName = openAcctBankName;
	}
	
    /**
     * @return openAcctBankName
     */
	public String getOpenAcctBankName() {
		return this.openAcctBankName;
	}
	
	/**
	 * @param openAcctBankNum
	 */
	public void setOpenAcctBankNum(String openAcctBankNum) {
		this.openAcctBankNum = openAcctBankNum;
	}
	
    /**
     * @return openAcctBankNum
     */
	public String getOpenAcctBankNum() {
		return this.openAcctBankNum;
	}
	
	/**
	 * @param invAmount
	 */
	public void setInvAmount(String invAmount) {
		this.invAmount = invAmount;
	}
	
    /**
     * @return invAmount
     */
	public String getInvAmount() {
		return this.invAmount;
	}
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param lendingRef
	 */
	public void setLendingRef(String lendingRef) {
		this.lendingRef = lendingRef;
	}
	
    /**
     * @return lendingRef
     */
	public String getLendingRef() {
		return this.lendingRef;
	}
	
	/**
	 * @param refNo
	 */
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
    /**
     * @return refNo
     */
	public String getRefNo() {
		return this.refNo;
	}
	
	/**
	 * @param transDate
	 */
	public void setTransDate(java.util.Date transDate) {
		this.transDate = transDate;
	}
	
    /**
     * @return transDate
     */
	public java.util.Date getTransDate() {
		return this.transDate;
	}
	
	/**
	 * @param iPay
	 */
	public void setIPay(String iPay) {
		this.iPay = iPay;
	}
	
    /**
     * @return iPay
     */
	public String getIPay() {
		return this.iPay;
	}
	
	/**
	 * @param ppPay
	 */
	public void setPpPay(String ppPay) {
		this.ppPay = ppPay;
	}
	
    /**
     * @return ppPay
     */
	public String getPpPay() {
		return this.ppPay;
	}
	
	/**
	 * @param fundInterest
	 */
	public void setFundInterest(String fundInterest) {
		this.fundInterest = fundInterest;
	}
	
    /**
     * @return fundInterest
     */
	public String getFundInterest() {
		return this.fundInterest;
	}
	
	/**
	 * @param fundPenalty
	 */
	public void setFundPenalty(String fundPenalty) {
		this.fundPenalty = fundPenalty;
	}
	
    /**
     * @return fundPenalty
     */
	public String getFundPenalty() {
		return this.fundPenalty;
	}
	
	/**
	 * @param billInterest
	 */
	public void setBillInterest(String billInterest) {
		this.billInterest = billInterest;
	}
	
    /**
     * @return billInterest
     */
	public String getBillInterest() {
		return this.billInterest;
	}
	
	/**
	 * @param billPenalty
	 */
	public void setBillPenalty(String billPenalty) {
		this.billPenalty = billPenalty;
	}
	
    /**
     * @return billPenalty
     */

	/**
	 * @param invParty
	 */
	public void setInvParty(String invParty) {
		this.invParty = invParty;
	}
	
    /**
     * @return invParty
     */
	public String getInvParty() {
		return this.invParty;
	}
	
	/**
	 * @param invType
	 */
	public void setInvType(String invType) {
		this.invType = invType;
	}
	
    /**
     * @return invType
     */
	public String getInvType() {
		return this.invType;
	}
	
	/**
	 * @param recipientName
	 */
	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}
	
    /**
     * @return recipientName
     */
	public String getRecipientName() {
		return this.recipientName;
	}
	
	/**
	 * @param contactNum
	 */
	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}
	
    /**
     * @return contactNum
     */
	public String getContactNum() {
		return this.contactNum;
	}
	
	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
    /**
     * @return address
     */
	public String getAddress() {
		return this.address;
	}
	
	/**
	 * @param emailAddr
	 */
	public void setEmailAddr(String emailAddr) {
		this.emailAddr = emailAddr;
	}
	
    /**
     * @return emailAddr
     */
	public String getEmailAddr() {
		return this.emailAddr;
	}
	
	/**
	 * @param expressNum
	 */
	public void setExpressNum(String expressNum) {
		this.expressNum = expressNum;
	}
	
    /**
     * @return expressNum
     */
	public String getExpressNum() {
		return this.expressNum;
	}
	
	/**
	 * @param invStatus
	 */
	public void setInvStatus(String invStatus) {
		this.invStatus = invStatus;
	}
	
    /**
     * @return invStatus
     */
	public String getInvStatus() {
		return this.invStatus;
	}


}