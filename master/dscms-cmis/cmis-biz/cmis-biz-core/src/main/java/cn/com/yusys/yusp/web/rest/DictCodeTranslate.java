package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.standard.DictItem;
// import cn.com.yusys.yusp.commons.module.standard.Translate;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class DictCodeTranslate  /*implements Translate*/ {
    /**
     * 指数类型字典项翻译
     * @param
     * @return
     */
    private static final String STD_ZB_HOUSE_INDEX_TYPE_DICT = "STD_ZB_HOUSE_INDEX_TYPE";
    private static final String STD_ZB_YES_NO_DICT = "STD_ZB_YES_NO";
    private static final String STD_ZB_ORIGIN_DICT = "STD_ZB_ORIGIN";
    private static final String STD_ZB_GOLD_TYPE_DICT = "STD_ZB_GOLD_TYPE";
    private static final String STD_ZB_METAL_SPECIF_DICT = "STD_ZB_METAL_SPECIF";
    private static final String STD_ZB_QUALITY_UNIT_DICT = "STD_ZB_QUALITY_UNIT";
    private static final String STD_ZB_BOND_TYPE_DICT = "STD_ZB_BOND_TYPE";
    private static final String STD_ZB_LAND_LOCATE_AREA_DICT = "STD_ZB_LAND_LOCATE_AREA";
    private static final String STD_ZB_LAND_USE_TYPE_DICT = "STD_ZB_LAND_USE_TYPE";
    private static final String STD_ZB_DZW_TYPE_DICT = "STD_ZB_DZW_TYPE";
    private static final String STD_ZB_LAND_USE_DICT = "STD_ZB_LAND_USE";
    //外部评估机构准入状态字典项
    private static final String STD_ZB_ADMIT_STATE = "STD_ZB_ADMIT_STATE";
    //外部评估机构资质类型字典项
    private static final String OUT_ORG_ASS_TYPE = "OUT_ORG_ASS_TYPE";
    //评估机构对应业务区域字典项
    private static final String STD_ZB_PLD_AREA = "STD_ZB_PLD_AREA";
    private static final String STD_ZB_HOUSE_DIRECTION_DICT = "STD_ZB_HOUSE_DIRECTION";
    private static final String STD_ZB_F_STY_DICT = "STD_ZB_F_STY";
    private static final String STD_ZB_ROOF_STRUCTURE_DICT = "STD_ZB_ROOF_STRUCTURE";
    private static final String STD_ZB_HOUSE_STA_DICT = "STD_ZB_HOUSE_STA";
    private static final String STD_ZB_HOUSE_PLACE_INFO_DICT = "STD_ZB_HOUSE_PLACE_INFO";
    private static final String STD_ZB_AROUND_ENVIRONMENT_DICT = "STD_ZB_AROUND_ENVIRONMENT";
    private static final String STD_ZB_PUBLIC_RESOURCE_DICT = "STD_ZB_PUBLIC_RESOURCE";
    private static final String STD_ZB_DECORATION_DICT = "STD_ZB_DECORATION";
    private static final String STD_ZB_EQ_FA_DICT = "STD_ZB_EQ_FA";
    private static final String STD_ZB_EATATE_MANAGE_DICT = "STD_ZB_EATATE_MANAGE";
    private static final String STD_ZB_STREET_SITUATION_DICT = "STD_ZB_STREET_SITUATION";
    private static final String STD_ZB_AND_LIGHTING_DICT = "STD_ZB_AND_LIGHTING";

    // @Override
    public List<DictItem> translate(String s) {
        if(Objects.equals(s,STD_ZB_HOUSE_INDEX_TYPE_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","居住用房（新房指数）","CODE1","01","指数类型",STD_ZB_HOUSE_INDEX_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","居住用房（二手房指数）","CODE2","02","指数类型",STD_ZB_HOUSE_INDEX_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","商业用房（新房指数）","CODE3","03","指数类型",STD_ZB_HOUSE_INDEX_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","商业用房（二手房指数）","CODE4","04","指数类型",STD_ZB_HOUSE_INDEX_TYPE_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_YES_NO_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","是","CODE1","Y","是否推荐使用",STD_ZB_YES_NO_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","否","CODE2","N","是否推荐使用",STD_ZB_YES_NO_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_ORIGIN_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","外部数据","CODE1","01","样本来源",STD_ZB_ORIGIN_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","内部数据","CODE2","02","样本来源",STD_ZB_ORIGIN_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_GOLD_TYPE_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","铂金","CODE1","01","贵金属类别",STD_ZB_GOLD_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","黄金","CODE2","02","贵金属类别",STD_ZB_GOLD_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","白银","CODE3","03","贵金属类别",STD_ZB_GOLD_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","其他贵金属","CODE4","04","贵金属类别",STD_ZB_GOLD_TYPE_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_METAL_SPECIF_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","Au99.99","CODE1","01","品种/等级",STD_ZB_METAL_SPECIF_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","Au99.95","CODE2","02","品种/等级",STD_ZB_METAL_SPECIF_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","Ag99.9","CODE3","03","品种/等级",STD_ZB_METAL_SPECIF_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","Pt99.95","CODE4","05","品种/等级",STD_ZB_METAL_SPECIF_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE5","Ag99.99","CODE5","04","品种/等级",STD_ZB_METAL_SPECIF_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE6","其他","CODE6","99","品种/等级",STD_ZB_METAL_SPECIF_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_QUALITY_UNIT_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","克","CODE1","01","质量单位",STD_ZB_QUALITY_UNIT_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_BOND_TYPE_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","国债","CODE1","01","债券种类",STD_ZB_BOND_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","地方政府债券","CODE2","02","债券种类",STD_ZB_BOND_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","金融债券","CODE3","03","债券种类",STD_ZB_BOND_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","公司或企业债券","CODE4","04","债券种类",STD_ZB_BOND_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE5","可转换债券","CODE5","05","债券种类",STD_ZB_BOND_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE6","其他债券","CODE6","06","债券种类",STD_ZB_BOND_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE7","央行票据","CODE7","07","债券种类",STD_ZB_BOND_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE8","政策性银行发行的金融债券","CODE8","08","债券种类",STD_ZB_BOND_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE9","多边开发银行国际清算银行或国际货币基金组织发行的债券","CODE9","09","债券种类",STD_ZB_BOND_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE10","公共部门实体发行的债券","CODE10","10","债券种类",STD_ZB_BOND_TYPE_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_LAND_LOCATE_AREA_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","城市建成区","CODE1","01","土地所在地段情况",STD_ZB_LAND_LOCATE_AREA_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","规划区","CODE2","02","土地所在地段情况",STD_ZB_LAND_LOCATE_AREA_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","郊区农村","CODE3","03","土地所在地段情况",STD_ZB_LAND_LOCATE_AREA_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_LAND_USE_TYPE_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","商业","CODE1","01","土地使用类型",STD_ZB_LAND_USE_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","住宅","CODE2","02","土地使用类型",STD_ZB_LAND_USE_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","工业","CODE3","03","土地使用类型",STD_ZB_LAND_USE_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","酒店","CODE4","04","土地使用类型",STD_ZB_LAND_USE_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE5","写字楼","CODE5","05","土地使用类型",STD_ZB_LAND_USE_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE6","商住综合","CODE6","06","土地使用类型",STD_ZB_LAND_USE_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE7","其它","CODE7","07","土地使用类型",STD_ZB_LAND_USE_TYPE_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_DZW_TYPE_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","住宅","CODE1","01","定着物种类",STD_ZB_DZW_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","棚户","CODE2","02","定着物种类",STD_ZB_DZW_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","商用建筑","CODE3","03","定着物种类",STD_ZB_DZW_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","工业建筑","CODE4","04","定着物种类",STD_ZB_DZW_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE5","道路桥梁","CODE5","05","定着物种类",STD_ZB_DZW_TYPE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE6","其他","CODE6","06","定着物种类",STD_ZB_DZW_TYPE_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_LAND_USE_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","耕地","CODE1","0100","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","园地","CODE2","0200","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","林地","CODE3","0300","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","草地","CODE4","0400","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE5","商服用地","CODE5","0500","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE6","工矿仓储用地","CODE6","0600","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE7","住宅用地","CODE7","0700","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE8","公共管理与公共服务用地","CODE8","0800","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE9","特殊用地","CODE9","0900","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE10","交通运输用地","CODE10","1000","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE11","水域及水利设施用地","CODE11","1100","土地用途",STD_ZB_LAND_USE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE12","其他土地","CODE12","9900","土地用途",STD_ZB_LAND_USE_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_ADMIT_STATE)){
            //外部评估机构准入状态
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","已准入","CODE1","01","准入状态",STD_ZB_ADMIT_STATE));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","已退出","CODE2","02","准入状态",STD_ZB_ADMIT_STATE));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","黑名单","CODE3","03","准入状态",STD_ZB_ADMIT_STATE));
            return DictItemList;
        }else if(Objects.equals(s,OUT_ORG_ASS_TYPE)){
            //外部评估机构资质类型
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","房地产","CODE1","01","外部评估机构评估类型",OUT_ORG_ASS_TYPE));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","土地","CODE2","02","外部评估机构评估类型",OUT_ORG_ASS_TYPE));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","设备","CODE3","03","外部评估机构评估类型",OUT_ORG_ASS_TYPE));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","其他","CODE3","04","外部评估机构评估类型",OUT_ORG_ASS_TYPE));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_PLD_AREA)){
            //评估机构对应业务区域
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","分行辖内","CODE1","01","评估机构对应业务区域",STD_ZB_PLD_AREA));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","全行","CODE2","02","评估机构对应业务区域",STD_ZB_PLD_AREA));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","其他","CODE3","99","评估机构对应业务区域",STD_ZB_PLD_AREA));
            return DictItemList;
        }else if (Objects.equals(s,STD_ZB_HOUSE_DIRECTION_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","南北","CODE1","0301","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","东南","CODE2","0302","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","南","CODE3","0303","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","东西","CODE4","0304","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE5","东","CODE5","0305","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE6","西南","CODE6","0306","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE7","西","CODE7","0307","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE8","北","CODE8","0308","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE9","东北","CODE9","0309","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE10","西北","CODE10","0310","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE11","东","CODE11","0399","朝向",STD_ZB_HOUSE_DIRECTION_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_F_STY_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","一室一厅","CODE1","01","套型",STD_ZB_F_STY_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","两室一厅","CODE2","02","套型",STD_ZB_F_STY_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","两室两厅","CODE3","03","套型",STD_ZB_F_STY_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","三室一厅","CODE4","04","套型",STD_ZB_F_STY_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE5","三室两厅","CODE5","05","套型",STD_ZB_F_STY_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE6","其他","CODE6","99","套型",STD_ZB_F_STY_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_ROOF_STRUCTURE_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","钢结构","CODE1","01","屋顶构造",STD_ZB_ROOF_STRUCTURE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","钢筋混凝土结构","CODE2","02","屋顶构造",STD_ZB_ROOF_STRUCTURE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","砖混结构","CODE3","03","屋顶构造",STD_ZB_ROOF_STRUCTURE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","砖木结构","CODE4","04","屋顶构造",STD_ZB_ROOF_STRUCTURE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE5","简易结构","CODE5","05","屋顶构造",STD_ZB_ROOF_STRUCTURE_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_HOUSE_STA_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","主体结构封顶","CODE1","01","房屋状态",STD_ZB_HOUSE_STA_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","已竣工验收","CODE2","02","房屋状态",STD_ZB_HOUSE_STA_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","已入住","CODE3","03","房屋状态",STD_ZB_HOUSE_STA_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_HOUSE_PLACE_INFO_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","核心城区","CODE1","01","房地产所在地段情况",STD_ZB_HOUSE_PLACE_INFO_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","非核心城区或城市周边郊区","CODE2","02","房地产所在地段情况",STD_ZB_HOUSE_PLACE_INFO_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","距城区较远的郊区","CODE3","03","房地产所在地段情况",STD_ZB_HOUSE_PLACE_INFO_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_AROUND_ENVIRONMENT_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","一般","CODE1","01","周边环境",STD_ZB_AROUND_ENVIRONMENT_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","较好","CODE2","02","周边环境",STD_ZB_AROUND_ENVIRONMENT_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_PUBLIC_RESOURCE_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","齐备","CODE1","01","公共配置",STD_ZB_PUBLIC_RESOURCE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","较齐备","CODE2","02","公共配置",STD_ZB_PUBLIC_RESOURCE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","一般","CODE3","03","公共配置",STD_ZB_PUBLIC_RESOURCE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","不齐备","CODE4","04","公共配置",STD_ZB_PUBLIC_RESOURCE_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_DECORATION_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","精装修","CODE1","01","装修状况",STD_ZB_DECORATION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","简装修","CODE2","02","装修状况",STD_ZB_DECORATION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","毛坯","CODE3","03","装修状况",STD_ZB_DECORATION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","豪装","CODE4","04","装修状况",STD_ZB_DECORATION_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_EQ_FA_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","好","CODE1","01","设施设备",STD_ZB_EQ_FA_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","较好","CODE2","02","设施设备",STD_ZB_EQ_FA_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","一般","CODE3","03","设施设备",STD_ZB_EQ_FA_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","较差","CODE4","04","设施设备",STD_ZB_EQ_FA_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_EATATE_MANAGE_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","好","CODE1","01","物业管理",STD_ZB_EATATE_MANAGE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","较好","CODE2","02","物业管理",STD_ZB_EATATE_MANAGE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","一般","CODE3","03","物业管理",STD_ZB_EATATE_MANAGE_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","较差","CODE4","04","物业管理",STD_ZB_EATATE_MANAGE_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_STREET_SITUATION_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","不临街","CODE1","01","临街状况",STD_ZB_STREET_SITUATION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","一面临街","CODE2","02","临街状况",STD_ZB_STREET_SITUATION_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","两面临街","CODE3","03","临街状况",STD_ZB_STREET_SITUATION_DICT));
            return DictItemList;
        }else if(Objects.equals(s,STD_ZB_AND_LIGHTING_DICT)){
            List<DictItem> DictItemList = new ArrayList<DictItem>();
            DictItemList.add(buildSubjectItem("ITEM_CODE1","优","CODE1","01","通风采光",STD_ZB_AND_LIGHTING_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE2","良","CODE2","02","通风采光",STD_ZB_AND_LIGHTING_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE3","一般","CODE3","03","通风采光",STD_ZB_AND_LIGHTING_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE4","较差","CODE4","04","通风采光",STD_ZB_AND_LIGHTING_DICT));
            DictItemList.add(buildSubjectItem("ITEM_CODE5","差","CODE5","05","通风采光",STD_ZB_AND_LIGHTING_DICT));
            return DictItemList;
        }
        return null;
    }

    private DictItem buildSubjectItem(String name, String itemName, String itemNameEn, String value, String chnNmae,String DictCode) {
        return  null;
                // DictItem.builder().dictCode(DictCode)
                // .dictName(chnNmae)
                // .dictNameEn(DictCode)
                // .name(name)
                // .itemName(itemName)
                // .itemNameEn(itemNameEn)
                // .value(value)
                // .build();
    }
}
