/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FdyddWhbxdApproval
 * @类描述: FDYDD_WHBXD_APPROVAL数据实体类
 * @功能描述:
 * @创建人: zy
 * @创建时间: 2021-06-04 22:23:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "FDYDD_WHBXD_APPROVAL")
public class FdyddWhbxdApproval extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;

	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 50)
	private String cusName;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 30)
	private String contNo;

	/** 中文合同编号 **/
	@Column(name = "CN_CONT_NO", unique = false, nullable = true, length = 50)
	private String cnContNo;

	/** 客户经理号 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 30)
	private String managerId;

	/** 客户经理名称 **/
	@Column(name = "MANAGER_NAME", unique = false, nullable = true, length = 50)
	private String managerName;

	/** 旧借据编号 **/
	@Column(name = "OLD_BILL_NO", unique = false, nullable = true, length = 30)
	private String oldBillNo;

	/** 新借据编号 **/
	@Column(name = "NEW_BILL_NO", unique = false, nullable = true, length = 30)
	private String newBillNo;

	/** 新借据金额 **/
	@Column(name = "NEW_LOAN_AMOUNT", unique = false, nullable = true, length = 30)
	private String newLoanAmount;

	/** 新借据期限 **/
	@Column(name = "NEW_BILL_TIME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal newBillTime;

	/** 新借据起始日 **/
	@Column(name = "NEW_START_DATE", unique = false, nullable = true, length = 10)
	private String newStartDate;

	/** 新借据到期日 **/
	@Column(name = "NEW_END_DATE", unique = false, nullable = true, length = 10)
	private String newEndDate;

	/** 新借据审批利率 **/
	@Column(name = "NEW_BILL_CHACK_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal newBillChackRate;

	/** 新借据放款/还款卡号 **/
	@Column(name = "NEW_BILL_CARD_NUM", unique = false, nullable = true, length = 50)
	private String newBillCardNum;

	/** 是否调整利率 **/
	@Column(name = "ADJUST_OR_NOT", unique = false, nullable = true, length = 1)
	private String adjustOrNot;

	/** 调整后利率 **/
	@Column(name = "ADJUST_INTEREST_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adjustInterestRate;

	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 30)
	private String approveStatus;

	/** 审批时间 **/
	@Column(name = "APPROVE_DATE", unique = false, nullable = true, length = 10)
	private String approveDate;

	/** 审核结果 **/
	@Column(name = "APPROVE_RESULT", unique = false, nullable = true, length = 1)
	private String approveResult;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param cnContNo
	 */
	public void setCnContNo(String cnContNo) {
		this.cnContNo = cnContNo;
	}

	/**
	 * @return cnContNo
	 */
	public String getCnContNo() {
		return this.cnContNo;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	/**
	 * @return managerName
	 */
	public String getManagerName() {
		return this.managerName;
	}

	/**
	 * @param oldBillNo
	 */
	public void setOldBillNo(String oldBillNo) {
		this.oldBillNo = oldBillNo;
	}

	/**
	 * @return oldBillNo
	 */
	public String getOldBillNo() {
		return this.oldBillNo;
	}

	/**
	 * @param newBillNo
	 */
	public void setNewBillNo(String newBillNo) {
		this.newBillNo = newBillNo;
	}

	/**
	 * @return newBillNo
	 */
	public String getNewBillNo() {
		return this.newBillNo;
	}

	/**
	 * @param newLoanAmount
	 */
	public void setNewLoanAmount(String newLoanAmount) {
		this.newLoanAmount = newLoanAmount;
	}

	/**
	 * @return newLoanAmount
	 */
	public String getNewLoanAmount() {
		return this.newLoanAmount;
	}

	/**
	 * @param newBillTime
	 */
	public void setNewBillTime(java.math.BigDecimal newBillTime) {
		this.newBillTime = newBillTime;
	}

	/**
	 * @return newBillTime
	 */
	public java.math.BigDecimal getNewBillTime() {
		return this.newBillTime;
	}

	/**
	 * @param newStartDate
	 */
	public void setNewStartDate(String newStartDate) {
		this.newStartDate = newStartDate;
	}

	/**
	 * @return newStartDate
	 */
	public String getNewStartDate() {
		return this.newStartDate;
	}

	/**
	 * @param newEndDate
	 */
	public void setNewEndDate(String newEndDate) {
		this.newEndDate = newEndDate;
	}

	/**
	 * @return newEndDate
	 */
	public String getNewEndDate() {
		return this.newEndDate;
	}

	/**
	 * @param newBillChackRate
	 */
	public void setNewBillChackRate(java.math.BigDecimal newBillChackRate) {
		this.newBillChackRate = newBillChackRate;
	}

	/**
	 * @return newBillChackRate
	 */
	public java.math.BigDecimal getNewBillChackRate() {
		return this.newBillChackRate;
	}

	/**
	 * @param newBillCardNum
	 */
	public void setNewBillCardNum(String newBillCardNum) {
		this.newBillCardNum = newBillCardNum;
	}

	/**
	 * @return newBillCardNum
	 */
	public String getNewBillCardNum() {
		return this.newBillCardNum;
	}

	/**
	 * @param adjustOrNot
	 */
	public void setAdjustOrNot(String adjustOrNot) {
		this.adjustOrNot = adjustOrNot;
	}

	/**
	 * @return adjustOrNot
	 */
	public String getAdjustOrNot() {
		return this.adjustOrNot;
	}

	/**
	 * @param adjustInterestRate
	 */
	public void setAdjustInterestRate(java.math.BigDecimal adjustInterestRate) {
		this.adjustInterestRate = adjustInterestRate;
	}

	/**
	 * @return adjustInterestRate
	 */
	public java.math.BigDecimal getAdjustInterestRate() {
		return this.adjustInterestRate;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param approveDate
	 */
	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}

	/**
	 * @return approveDate
	 */
	public String getApproveDate() {
		return this.approveDate;
	}

	/**
	 * @param approveResult
	 */
	public void setApproveResult(String approveResult) {
		this.approveResult = approveResult;
	}

	/**
	 * @return approveResult
	 */
	public String getApproveResult() {
		return this.approveResult;
	}


}