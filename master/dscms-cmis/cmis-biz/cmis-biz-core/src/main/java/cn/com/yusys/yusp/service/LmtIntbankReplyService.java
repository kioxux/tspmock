/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.LmtIntbankAppr;
import cn.com.yusys.yusp.domain.LmtIntbankReply;
import cn.com.yusys.yusp.domain.LmtIntbankReplyChg;
import cn.com.yusys.yusp.repository.mapper.LmtIntbankReplyMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankReplyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-21 20:32:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtIntbankReplyService extends BizInvestCommonService{

    @Autowired
    private LmtIntbankReplyMapper lmtIntbankReplyMapper;


    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtIntbankReply selectByPrimaryKey(String pkId) {
        return lmtIntbankReplyMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtIntbankReply> selectAll(QueryModel model) {
        List<LmtIntbankReply> records = (List<LmtIntbankReply>) lmtIntbankReplyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtIntbankReply> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankReply> list = lmtIntbankReplyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtIntbankReply record) {
        return lmtIntbankReplyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtIntbankReply record) {
        return lmtIntbankReplyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtIntbankReply record) {
        return lmtIntbankReplyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtIntbankReply record) {
        return lmtIntbankReplyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtIntbankReplyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtIntbankReplyMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: initLmtIntBankRelyInfo
     * @方法描述: 初始化同业授信批复信息
     * @参数与返回说明: LmtIntbankAcc 返回同业授信批复信息
     * @算法描述: 无
     */
    public LmtIntbankReply initLmtIntBankRelyInfo(LmtIntbankAppr entityBean){
        //01 授信新增 02 授信变更 03 授信续作 05 授信复议
        //从审批表中获取数据
        String lmtType = entityBean.getLmtType() ;
        //同业授信申请表，期限，起始日，到期日等字段需要重新赋值
        Integer lmtTerm = entityBean.getLmtTerm() ;
        if(lmtTerm == null) lmtTerm = 0 ;

        //如果是授信变更
        User user = SessionUtils.getUserInformation();
        //生成主键
        Map paramMap= new HashMap<>() ;
        String pkValue =generateSerno(SeqConstant.PK_ID);
        String replySerno = generateSerno(SeqConstant.INTBANK_LMT_REPLY_SEQ);
        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期
        //初始化同业授信台账表信息
        LmtIntbankReply lmtIntBankRely = new LmtIntbankReply() ;
        //将 申请或者审批表中的数据拷贝到台账表中
        BeanUtils.copyProperties(entityBean, lmtIntBankRely);
        //设置主键
        lmtIntBankRely.setPkId(pkValue);
        //批复编号
        lmtIntBankRely.setReplySerno(replySerno);
        //登记日期
        lmtIntBankRely.setInputDate(getCurrrentDateStr());
        //起始日期  TODO 待改造
        String startDate = openDay ;
        //到期日
        String endDate = DateUtils.addMonth(startDate, "yyyy-MM-dd",lmtTerm);
        //期限
        lmtIntBankRely.setLmtTerm(lmtTerm);
        //起始日期
        lmtIntBankRely.setStartDate(startDate);
        //到日期
        lmtIntBankRely.setEndDate(endDate);
        //终审机构
        lmtIntBankRely.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        lmtIntBankRely.setApprMode(entityBean.getApprMode());
        lmtIntBankRely.setApprResult(entityBean.getReviewResult());
        //批复状态  REPLY_STATUS
        lmtIntBankRely.setReplyStatus(CmisBizConstants.STD_REPLY_STATUS_01) ;
        //最新更新日期
        lmtIntBankRely.setInputDate(openDay);
        //最新更新日期
        lmtIntBankRely.setUpdDate(openDay);
        //创建日期
        lmtIntBankRely.setCreateTime(getCurrrentDate());
        //更新日期
        lmtIntBankRely.setUpdateTime(getCurrrentDate());
        return lmtIntBankRely ;
    }
    /**
     * @方法名称：queryLmtReplyDataByParams
     * @方法描述：通过参数查询数据
     * @参数与返回说明：
     * @算法描述：
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtIntbankReply> queryLmtReplyDataByParams(HashMap<String, String> paramMap) {
        return lmtIntbankReplyMapper.queryLmtReplyDataByParams(paramMap);
    }

    /**
     * @方法名称：queryLmtReplyBySerno
     * @方法描述 通过授信流水号查询最新的授信审批数据
     * @参数与返回说明：
     * @算法描述：根据批复生效日期倒序查询最新一笔数据
     */
    public LmtIntbankReply queryLmtReplyBySerno(String serno) {
        HashMap<String, String> lmtReplyQueryMap = new HashMap<String, String>();
        lmtReplyQueryMap.put("replySerno", serno);
        BizInvestCommonService.checkParamsIsNull("replySerno",serno);
        lmtReplyQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtIntbankReply> lmtIntbankReplyList = queryLmtReplyDataByParams(lmtReplyQueryMap);
        LmtIntbankReply LmtIntbankReply = null;
        if (lmtIntbankReplyList != null && lmtIntbankReplyList.size() == 1) {
            LmtIntbankReply = lmtIntbankReplyList.get(0);
        }
        return LmtIntbankReply;
    }

    /**
     * 根据申请流水号查询同业授信批复信息
     * @param serno
     * @return
     */
    public LmtIntbankReply selectBySerno(String serno){
        return lmtIntbankReplyMapper.selectBySerno(serno);
    }

    /**
     * 根据批复流水号查询同业授信批复信息
     * @param replySerno
     * @return
     */
    public LmtIntbankReply selectByReplySerno(String replySerno){
        return lmtIntbankReplyMapper.selectByReplySerno(replySerno);
    }

    /**
     * @方法名称: updateRestByPkId
     * @方法描述: 根据Serno更新审批结论、审批意见
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateRestByPkId(QueryModel model) {
        String serno = (String) model.getCondition().get("serno");
        //根据流水号获取申请流水号对应审批表最新的数据
        QueryModel selectModel = new QueryModel();
        selectModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        selectModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //按照创建时间倒序排序
        selectModel.setSort(" createTime desc ");
        List<LmtIntbankReply> list = selectByModel(selectModel);
        //如果流水号对应审批表最新的数据有值，则更新审批表对应的审批结论和审批意见
        if(list!=null && list.size()>0){
            LmtIntbankReply lmtIntbankReply = list.get(0);
            //获取前端传入参数 apprResult-审批结论
            lmtIntbankReply.setApprResult((String)model.getCondition().get("reviewResult"));
            //更新 更新数据时间
            lmtIntbankReply.setUpdateTime(getCurrrentDate());
            return lmtIntbankReplyMapper.updateByPrimaryKey(lmtIntbankReply);
        }
        return 0;
    }


    public LmtIntbankReply initLmtIntBankRelyInfoByReplyChg(LmtIntbankReplyChg lmtIntbankReplyChg) {
        //01 授信新增 02 授信变更 03 授信续作 05 授信复议
        //从审批表中获取数据
//        String lmtType = entityBean.getLmtType() ;
        //同业授信申请表，期限，起始日，到期日等字段需要重新赋值
        Integer lmtTerm = lmtIntbankReplyChg.getLmtTerm() ;
        if(lmtTerm == null) lmtTerm = 0 ;

        //如果是授信变更
        User user = SessionUtils.getUserInformation();
        //生成主键
        Map paramMap= new HashMap<>() ;
        String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
        String replySerno = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.LMT_SERNO, paramMap);
        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期
        //初始化同业授信台账表信息
        LmtIntbankReply lmtIntBankRely = new LmtIntbankReply() ;
        //将 申请或者审批表中的数据拷贝到台账表中
        BeanUtils.copyProperties(lmtIntbankReplyChg, lmtIntBankRely);
        //设置主键
        lmtIntBankRely.setPkId(pkValue);
        //批复编号
        lmtIntBankRely.setReplySerno(replySerno);
        //登记日期
        lmtIntBankRely.setInputDate(openDay);
        //起始日期
        String startDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT) ;
        //到期日
        String endDate = DateUtils.addMonth(startDate, "yyyy-MM-dd",lmtTerm);
        //期限
        lmtIntBankRely.setLmtTerm(lmtTerm);
        //起始日期
        lmtIntBankRely.setStartDate(startDate);
        //到日期
        lmtIntBankRely.setEndDate(endDate);
        //终审机构
        lmtIntBankRely.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        //TODO:APPR_MODE	审批模式  APPR_RESULT	审批结论  暂定
        lmtIntBankRely.setApprMode("");
        lmtIntBankRely.setApprResult("");
        //批复状态  REPLY_STATUS
        lmtIntBankRely.setReplyStatus(CmisLmtConstants.STD_ZB_APPR_ST_01) ;
        //最新更新日期
        lmtIntBankRely.setUpdDate(getCurrrentDateStr());
        //创建日期
        lmtIntBankRely.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtIntBankRely.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtIntBankRely ;
    }
}
