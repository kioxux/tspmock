package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.CommonUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.req.Xwd009ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.resp.Xwd009RespDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd010.req.Xwd010ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd010.resp.Xwd010RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtPerferRateApplyInfoMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtPerferRateApplyInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-05-11 19:31:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtPerferRateApplyInfoService {

    private static final Logger logger = LoggerFactory.getLogger(LmtPerferRateApplyInfoService.class);

    @Autowired
    private LmtPerferRateApplyInfoMapper lmtPerferRateApplyInfoMapper;

    @Autowired
    private Dscms2XwywglptClientService dscms2XwywglptClientService;

    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;

    @Autowired
    private LmtSurveyConInfoService lmtSurveyConInfoService;

    @Autowired
    private AreaOrgService areaOrgService;

    @Autowired
    private AreaManagerService areaManagerService;

    @Autowired
    private FbManagerService fbManagerService;

    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private Dscms2Ciis2ndClientService dscms2Ciis2ndClientService;

    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtPerferRateApplyInfo selectByPrimaryKey(String pkId) {
        return lmtPerferRateApplyInfoMapper.selectByPrimaryKey(pkId);
    }


    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtPerferRateApplyInfo> selectAll(QueryModel model) {
        List<LmtPerferRateApplyInfo> records = (List<LmtPerferRateApplyInfo>) lmtPerferRateApplyInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtPerferRateApplyInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtPerferRateApplyInfo> list = lmtPerferRateApplyInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtPerferRateApplyInfo record) {
        return lmtPerferRateApplyInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtPerferRateApplyInfo record) {
        return lmtPerferRateApplyInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtPerferRateApplyInfo record) {
        return lmtPerferRateApplyInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtPerferRateApplyInfo record) {
        return lmtPerferRateApplyInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtPerferRateApplyInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtPerferRateApplyInfoMapper.deleteByIds(ids);
    }

    /**
     * @param lmtPerferRateApplyInfo
     * @return cn.com.yusys.yusp.domain.PerferRateApplyInfo
     * @author 王玉坤
     * @date 2021/4/16 17:18
     * @version 1.0.0
     * @desc 根据调查流水号插入或更新优惠利率信息
     * @修改历史:
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto saveBySurveyNo(LmtPerferRateApplyInfo lmtPerferRateApplyInfo) {
        // 更新、插入标识
        int nums = -1;
        // 返回码
        int code = 0;
        // 返回信息
        String msg = "";

        // 临时变量
        LmtPerferRateApplyInfo lmt = null;
        try {
            // 根据流水号查询优惠利率申请信息，有则更新、无则插入

            lmt = lmtPerferRateApplyInfoMapper.selectBySurveySerNo(lmtPerferRateApplyInfo.getSurveySerno());
            // 根据调查流水号查询优惠利率申请信息
            if (lmt != null) {
                // 处于000-待发起或者996-自行退出状态的才可以发起审批，如果不等于两者，则无法进行到下一步
                if (!CmisCommonConstants.WF_STATUS_000.equals(lmt.getApproveStatus()) && !CmisCommonConstants.WF_STATUS_996.equals(lmt.getApproveStatus()) ) {
                    throw new YuspException(EcbEnum.ECB010005.key, EcbEnum.ECB010005.value);
                }
                nums = lmtPerferRateApplyInfoMapper.updateByPrimaryKeySelective(lmtPerferRateApplyInfo);
                logger.info("根据调查流水号{}更新成功，更新条数{}", lmtPerferRateApplyInfo.getSurveySerno(), nums);
            } else {
                lmtPerferRateApplyInfo.setApproveStatus(BizFlowConstant.WF_STATUS_000);
                lmtPerferRateApplyInfo.setUpdateTime(DateUtils.getCurrDate());
                lmtPerferRateApplyInfo.setCreateTime(DateUtils.getCurrDate());
                nums = insert(lmtPerferRateApplyInfo);
                logger.info("根据调查流水号{}插入成功，插入条数{}", lmtPerferRateApplyInfo.getSurveySerno(), nums);
            }
            code = nums > 0 ? code : -1;
            msg = nums > 0 ? "处理成功" : "系统异常";
        } catch (YuspException e) {
            code = -1;
            msg = e.getMsg();
            logger.error(e.getMessage(), e);
        } catch (Exception e) {
            code = -1;
            msg = "系统异常";
            logger.error(e.getMessage(), e);
        }
        return new ResultDto(lmt).code(code).message(msg);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-11 21:32
     * @注释 查询单挑数据
     */
    public ResultDto selectBySurveySerNo(String SurveySerno) {
        LmtPerferRateApplyInfo lmt = lmtPerferRateApplyInfoMapper.selectBySurveySerNo(SurveySerno);
        return new ResultDto(lmt);
    }


    /**
     * 优惠利率申请流程
     * @param lmtPerferRateApplyInfo
     * @return
     */
    public ResultDto rateApp(LmtPerferRateApplyInfo lmtPerferRateApplyInfo) {
        // 保存操作
        this.saveBySurveyNo(lmtPerferRateApplyInfo);
        logger.info("根据调查流水号【{}】查询调查基本信息开始！", lmtPerferRateApplyInfo.getSurveySerno());
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(lmtPerferRateApplyInfo.getSurveySerno());
        logger.info("根据调查流水号【{}】查询调查基本信息结束！", lmtPerferRateApplyInfo.getSurveySerno());
        // 判断是否已完善调查基本信息
        if (Objects.isNull(lmtSurveyReportBasicInfo)) {
            return new ResultDto<String>(null).code("9999").message("请完善调查报告基本信息！");
        }

        logger.info("根据调查流水号【{}】查询调查主表开始！", lmtPerferRateApplyInfo.getSurveySerno());
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(lmtPerferRateApplyInfo.getSurveySerno());
        logger.info("根据调查流水号【{}】查询调查主表结束！", lmtPerferRateApplyInfo.getSurveySerno());
        // 判断是否已完善调查基本信息
        if (Objects.isNull(lmtSurveyReportMainInfo)) {
            return new ResultDto<String>(null).code("9999").message("请完善调查报告主表信息！");
        }

        logger.info("根据调查流水号【{}】查询调查结论信息开始！", lmtPerferRateApplyInfo.getSurveySerno());
        LmtSurveyConInfo lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(lmtPerferRateApplyInfo.getSurveySerno());
        logger.info("根据调查流水号【{}】查询调查结论信息结束！", lmtPerferRateApplyInfo.getSurveySerno());
        if (Objects.isNull(lmtSurveyConInfo)) {
            return new ResultDto<String>(null).code("9999").message("请完善调查结论信息！");
        }
        // 1、获取分部信息
        String orgId = lmtPerferRateApplyInfo.getManagerBrId();
        // 1、1、根据客户经理获取机构信息,根据机构信息查询区域信息
        logger.info("根据机构号【{}】查询区域信息开始", orgId);
        List<AreaOrg> areaOrgDtos = areaOrgService.selectByOrgId(orgId);
        if (CollectionUtils.isEmpty(areaOrgDtos)) {
            return new ResultDto<String>(null).code("9999").message("未查询到用户机构所属区域信息！");
        }
        logger.info("根据机构号【{}】查询区域信息结束,区域编号【{}】", orgId, areaOrgDtos.get(0).getAreaNo());

        // 1、2、根据区域编号查询区域信息，获取区域分部长编号
        logger.info("根据区域编号【{}】查询区域信息开始", areaOrgDtos.get(0).getAreaNo());
        AreaManager areaManager = areaManagerService.selectByPrimaryKey(areaOrgDtos.get(0).getAreaNo());
        if (Objects.isNull(areaManager)) {
            return new ResultDto<String>(null).code("9999").message("未查询到区域信息！");
        }
        logger.info("根据区域编号【{}】查询区域信息结束,区域分部编号", areaOrgDtos.get(0).getAreaNo(), areaManager.getSubBranch());

        // 2、获取贷款授权码 -- 征信报告编号
        logger.info("调用接口查询征信报告信息开始，业务流水号【{}】", lmtPerferRateApplyInfo.getSurveySerno());
        // 查询30天内是否有征信报告
        String reportId = "";
        // 获取系统时间
        logger.info("调用征信查询接口开始，业务流水号【{}】", lmtPerferRateApplyInfo.getSurveySerno());
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        Date nowDay = DateUtils.parseDate(openDay,"yyyy-MM-dd");
        // 30天前
        Date oldDay = DateUtils.addDay(nowDay, -30);
        // 调用通用接口
        CredzbReqDto credzbReqDto = new CredzbReqDto();
        credzbReqDto.setRuleCode("R003");
        credzbReqDto.setReqId(lmtPerferRateApplyInfo.getSurveySerno());
        // 2021年11月16日10:38:25 hubp 问题编号：20211116-0037 修改机构号传值方式
        if(StringUtils.nonBlank(lmtSurveyReportMainInfo.getManagerBrId())){
            credzbReqDto.setBrchno(lmtSurveyReportMainInfo.getManagerBrId());
        } else {
            credzbReqDto.setBrchno(lmtSurveyReportMainInfo.getInputBrId());
        }
        credzbReqDto.setCertificateNum(lmtSurveyReportBasicInfo.getCertCode());
        credzbReqDto.setCustomName(lmtSurveyReportBasicInfo.getCusName());
        credzbReqDto.setStartDate(DateUtils.formatDate(oldDay,"yyyy-MM-dd"));
        credzbReqDto.setEndDate(openDay);
        // 调查征信报告
        ResultDto<CredzbRespDto> credzbRespDto = dscms2Ciis2ndClientService.credzb(credzbReqDto);
        if (credzbRespDto != null && SuccessEnum.CMIS_SUCCSESS.key.equals(credzbRespDto.getCode())) {
            logger.info("调用征信查询接口成功，业务流水号【{}】", lmtPerferRateApplyInfo.getSurveySerno());
            if (CommonUtils.nonNull(credzbRespDto.getData()) && CommonUtils.nonNull(credzbRespDto.getData().getR003()) && StringUtils.nonBlank(credzbRespDto.getData().getR003().getREPORTID())) {
               logger.info("30天内征信报告编号【{}】", credzbRespDto.getData().getR003().getREPORTID());
                reportId = credzbRespDto.getData().getR003().getREPORTID();
            } else {
                logger.info("不存在30天内有效的征信报告");
                return new ResultDto<String>(null).code("9999").message("未查询到近30天内有效的征信报告!");
            }
        } else {
            String errorMsg = "";
            if(credzbRespDto!=null){
                errorMsg = credzbRespDto.getMessage();
            }
            logger.info("征信查询接口调用失败");
            return new ResultDto<String>(null).code("9999").message("未查询到近30天内有效的征信报告!");
        }
        logger.info("调用征信查询接口结束，业务流水号【{}】", lmtPerferRateApplyInfo.getSurveySerno());

        Xwd009ReqDto xwd009ReqDto = new Xwd009ReqDto();
        // 行内业务流水号
        xwd009ReqDto.setBusinessId(lmtSurveyReportBasicInfo.getSurveySerno());
        // 申请人身份证号码
        xwd009ReqDto.setIdentNo(lmtSurveyReportBasicInfo.getCertCode());
        // 申请人名称
        xwd009ReqDto.setApplName(lmtSurveyReportBasicInfo.getCusName());
        // 贷款期限
        xwd009ReqDto.setApplLimit(lmtSurveyConInfo.getAdviceTerm());
        // 业务来源 01-主动营销 02-客户上门 03-客户推荐 04-员工推荐
        xwd009ReqDto.setBusinessSource("01");
        // 申请利率
        xwd009ReqDto.setApplInterest(lmtPerferRateApplyInfo.getAppRate().toString());
        // 客户经理意见 1、客户还款记录良好，经营情况良好 2、高净值客户 3、借款人为某个特色贷推广的关键人或某个单位主要领导 4、单笔单议
        xwd009ReqDto.setCustomerManagerOpinion(lmtPerferRateApplyInfo.getManagerIdAdvice());
        // 担保类型
        xwd009ReqDto.setGuaranteeType(lmtSurveyConInfo.getGuarMode());
        // 申请原因
        xwd009ReqDto.setApplReason(lmtPerferRateApplyInfo.getOtherResn());
        // 申请人工号
        xwd009ReqDto.setCreator(lmtSurveyReportMainInfo.getManagerId());
        // 分部 01-张家港分部 02-南通分部 03-无锡分部 04-苏州分部 05-其它分部
        Map<String, String> map = new HashMap<>();
        map.put("zjgfb", "01"); // 张家港分部
        map.put("ntfb", "02"); // 南通分布
        map.put("yxfb", "03"); // 宜兴分部 -- 无锡分部
        map.put("szfb", "04"); // 苏州分部
        map.put("qbfb", "05"); // 青北分部 -- 其他分部
        xwd009ReqDto.setRateBranch(map.get(areaManager.getSubBranch()));
        // TODO 新老客户 1-新客户 2-老客户
        xwd009ReqDto.setNewCustomers("1");
        //  贷款授权码 -- 征信报告编号
        xwd009ReqDto.setCreditAuthCodeList(reportId);
        // 还款方式
        xwd009ReqDto.setRepayWay(lmtSurveyConInfo.getRepayMode());

        String isApproval = null;
        logger.info("*************优惠利率申请接口Xwd009调用开始，传入参数：【{}】*********" , JSON.toJSONString(xwd009ReqDto));
        ResultDto<Xwd009RespDto> xwd009ResultDto = dscms2XwywglptClientService.xwd009(xwd009ReqDto);
        logger.info("*************优惠利率申请接口Xwd009调用结束，响应参数：【{}】*********" , JSON.toJSONString(xwd009ResultDto));
        Xwd009RespDto xwd009RespDto = xwd009ResultDto.getData();
        if (xwd009RespDto != null && SuccessEnum.SUCCESS.key.equals(xwd009RespDto.getCode())) {
            logger.info("利率申请接口调用成功");
            isApproval = xwd009ResultDto.getData().getIsApproval();
            // 更新审批状态
            if (CommonConstance.STD_YES_OR_NO_Y.equals(isApproval)) {
                // 更新状态审批中
                lmtPerferRateApplyInfo.setApproveStatus(BizFlowConstant.WF_STATUS_111);
                // 更新新微贷业务流水号
                lmtPerferRateApplyInfo.setBizSerno(xwd009ResultDto.getData().getReqId());
            } else if (CommonConstance.STD_YES_OR_NO_N.equals(isApproval)) {
                // 更新状态审批成功
                // todo 这个是有问题的，新微贷未通过利率数值校验也会返回 'N'
                lmtPerferRateApplyInfo.setApproveStatus(BizFlowConstant.WF_STATUS_997);
                // 更新新微贷业务流水号
                lmtPerferRateApplyInfo.setBizSerno(xwd009ResultDto.getData().getReqId());
            }
            this.saveBySurveyNo(lmtPerferRateApplyInfo);
        } else {
            String errorMsg = "";
            if(xwd009RespDto != null){
                errorMsg = xwd009ResultDto.getMessage();
            }
            logger.info("利率申请接口调用失败");
            return new ResultDto<String>(null).code("9999").message("利率申请接口调用失败:【" + errorMsg + "】");
        }
        return new ResultDto<String>(isApproval);
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据第三方业务流水号查询优惠利率申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtPerferRateApplyInfo selectByBizSerno(String bizSerno) {
        return lmtPerferRateApplyInfoMapper.selectByBizSerno(bizSerno);
    }

    /**
     * @param lmtPerferRateApplyInfo
     * @return ResultDto
     * @author 尚智勇
     * @version 1.0.0
     * @desc 优惠利率申请撤销
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto<String> rateRev(LmtPerferRateApplyInfo lmtPerferRateApplyInfo) {
        logger.info("根据业务流水号【{}】查询优惠利率申请信息开始", lmtPerferRateApplyInfo.getSurveySerno());
        LmtPerferRateApplyInfo lmt = lmtPerferRateApplyInfoMapper.selectBySurveySerNo(lmtPerferRateApplyInfo.getSurveySerno());
        if (Objects.isNull(lmt)) {
            return new ResultDto<String>(null).code("9999").message("无对应优惠利率申请信息！");
        }
        logger.info("根据业务流水号【{}】查询优惠利率申请信息结束", lmtPerferRateApplyInfo.getSurveySerno());

        logger.info("发起优惠利率撤销开始，业务流水号【{}】", lmtPerferRateApplyInfo.getSurveySerno());
        Xwd010ReqDto xwd010ReqDto = new Xwd010ReqDto();
        xwd010ReqDto.setReqId(lmt.getBizSerno());
        ResultDto<Xwd010RespDto> xwd010ResultDto = dscms2XwywglptClientService.xwd010(xwd010ReqDto);
        logger.info("发起优惠利率撤销成功，业务流水号【{}】", lmtPerferRateApplyInfo.getSurveySerno());
        Xwd010RespDto respDto = xwd010ResultDto.getData();
        if (respDto != null && SuccessEnum.SUCCESS.key.equals(respDto.getCode())) {
            logger.info("优惠利率撤销接口调用成功");
            lmtPerferRateApplyInfo.setApproveStatus(BizFlowConstant.WF_STATUS_996);
            //保存处理结果
            logger.info("优惠利率申请状态更新开始");
            int i = lmtPerferRateApplyInfoMapper.updateByPrimaryKeySelective(lmtPerferRateApplyInfo);
            logger.info("优惠利率申请状态更新结束，更新条数【{}】", i);

        } else {
            String errorMsg = "";
            if (respDto != null) {
                errorMsg = xwd010ResultDto.getMessage();
            }
            logger.info("优惠利率撤销接口调用失败:【{}】", errorMsg);
            return new ResultDto<String>(null).code("9999").message("优惠利率撤销接口调用失败:【" + errorMsg + "】");
        }
        return new ResultDto<>(lmtPerferRateApplyInfo.getApproveStatus()).code(0);
    }

    public LmtPerferRateApplyInfo selectBySurveySerno(String surveySerno) {
        return lmtPerferRateApplyInfoMapper.selectBySurveySerNo(surveySerno);
    }
}
