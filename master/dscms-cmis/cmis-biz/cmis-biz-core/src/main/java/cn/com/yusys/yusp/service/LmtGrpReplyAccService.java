/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseDto;
import cn.com.yusys.yusp.dto.CusBaseQueryRespDto;
import cn.com.yusys.yusp.dto.CusGrpMemberRelDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.req.CmisLmt0002LmtListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.req.CmisLmt0002LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.req.CmisLmt0002ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.resp.CmisLmt0002RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtGrpReplyAccMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyAccService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:40:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGrpReplyAccService {

    private Logger log = Logger.getLogger(LmtGrpReplyAccService.class);

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtGrpReplyService lmtGrpReplyService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private LmtReplyService lmtReplyService;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Resource
    private LmtGrpReplyAccMapper lmtGrpReplyAccMapper;

    @Resource
    private CmisLmtClientService cmisLmtClientService;

    @Resource
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private ICusClientService iCusClientService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtGrpReplyAcc selectByPrimaryKey(String pkId) {
        return lmtGrpReplyAccMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtGrpReplyAcc> selectAll(QueryModel model) {
        List<LmtGrpReplyAcc> records = (List<LmtGrpReplyAcc>) lmtGrpReplyAccMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtGrpReplyAcc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrpReplyAcc> list = lmtGrpReplyAccMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtGrpReplyAcc record) {
        return lmtGrpReplyAccMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtGrpReplyAcc record) {
        return lmtGrpReplyAccMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtGrpReplyAcc record) {
        return lmtGrpReplyAccMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtGrpReplyAcc record) {
        return lmtGrpReplyAccMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtGrpReplyAccMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtGrpReplyAccMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtGrpReplyAcc> queryAll(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        model.setSort("reply_inure_date desc");
        //model.getCondition().put("replyStatus",CmisCommonConstants.STD_XD_REPLY_STATUS_01);
        List<LmtGrpReplyAcc> list = lmtGrpReplyAccMapper.queryAll(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryDetailByGrpAccNo
     * @方法描述: 根据集团授信台账编号查询集团台账信息
     * @参数与返回说明:
     * @算法描述: 根据集团授信台账编号查询集团台账信息
     * @创建人: mashun
     * @创建时间: 2021-07-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpReplyAcc queryDetailByGrpAccNo(String grpAccNo) {
        return lmtGrpReplyAccMapper.selectByGrpAccNo(grpAccNo);
    }


    /**
     * @方法名称: getLastLmtAcc
     * @方法描述: 根据集团号查询客户最新的一笔授信台账，
     * @参数与返回说明:
     * @算法描述: 通过集团号查询授信台账生效日最近的一笔授信台账
     * @创建人: xiamc
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpReplyAcc getLastLmtReplyAcc(String grpCusId){
        LmtGrpReplyAcc lmtGrpReplyAcc = new  LmtGrpReplyAcc();
        try{
            HashMap paramMap = new HashMap<String, Object>();
            paramMap.put("grpCusId", grpCusId);
            paramMap.put("replyStatus", CmisCommonConstants.STD_XD_REPLY_STATUS_01);
            paramMap.put("sort", "UPDATE_TIME desc");
            List<LmtGrpReplyAcc> lmtReplyAccList = lmtGrpReplyAccMapper.selectByParams(paramMap);
            if(lmtReplyAccList != null && lmtReplyAccList.size() > 0){
                lmtGrpReplyAcc = lmtReplyAccList.get(0);
            }
        } finally{
            return lmtGrpReplyAcc;
        }
    }

    /**
     * @方法名称: toSignlist
     * @方法描述:  该方法不做过滤直接全量插查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<LmtGrpReplyAcc> toSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return lmtGrpReplyAccMapper.selectByModel(model);
    }

    /**
     * @方法名称: generateLmtGrpReplyAccBySerno
     * @方法描述: 根据集团申请流水号遍历所有成员的申请流水号
     * @参数与返回说明:
     * @算法描述:
     * 1.生成集团授信批复台账
     * 2.生成单一客户批复台账
     * 3.插入集团授信申请成员关系表数据
     * @创建人: mashun
     * @创建时间: 2021-05-07 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     *
     */
    @Transactional
    public void generateLmtGrpReplyAccBySerno(String serno) throws Exception {
        LmtGrpReply lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByGrpSerno(serno);
        // 获取真正的授信类型
        String realLmtType = findRealLmtType(lmtGrpReply);
        if(CmisCommonConstants.LMT_TYPE_01.equals(realLmtType)
                || CmisCommonConstants.LMT_TYPE_03.equals(realLmtType)
                || CmisCommonConstants.LMT_TYPE_06.equals(realLmtType)
                || "998_05".equals(realLmtType)
                ){
            // 998_05  否决发起复议情况:生成新的批复 以及批复台账
            // 授信新增 获取授信再议时 生成新的批复台账
            this.generateNewLmtGrpReplyAcc(lmtGrpReply);
        }else if(CmisCommonConstants.LMT_TYPE_02.equals(realLmtType)
                || CmisCommonConstants.LMT_TYPE_04.equals(realLmtType)
                || CmisCommonConstants.LMT_TYPE_07.equals(realLmtType)
                || CmisCommonConstants.LMT_TYPE_08.equals(realLmtType)
                || "997_05".equals(realLmtType)
        ){
            // 997_05  通过发起复议 :新增批复 更新批复台账
            // 授信变更、授信复审、授信 更新原批复台账
            this.updateLmtGrpReplyAcc(lmtGrpReply, CmisCommonConstants.YES_NO_0);
        } else {
            throw new Exception("授信申请类型不符合，暂不支持" + lmtGrpReply.getLmtType() + "类型");
        }
    }

    /**
     * @方法名称: findRealLmtType
     * @方法描述: 根据授信批复查询原始批复的授信类型
     * @参数与返回说明:
     * @算法描述: 递归查询最近一笔授信类型不为复议或者再议的数据
     * @创建人: mashun
     * @创建时间: 2021-04-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     *
     */
    public String findRealLmtType(LmtGrpReply lmtGrpReply) throws Exception {
        HashMap<String, String> queryMap = new HashMap<String, String>();
        if (CmisCommonConstants.LMT_TYPE_05.equals(lmtGrpReply.getLmtType())) {
            if (!StringUtils.isBlank(lmtGrpReply.getOrigiGrpReplySerno())) {
                queryMap.put("grpReplySerno", lmtGrpReply.getOrigiGrpReplySerno());
                queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                List<LmtGrpReply> lastLmtGrpReplyList = lmtGrpReplyService.queryLmtGrpReplyByParams(queryMap);
                if (lastLmtGrpReplyList != null && lastLmtGrpReplyList.size() == 1) {
                    // 如果复议的上一笔为否决 那么生成新的批复 以及批复台账
                    // 如果复议上一笔为通过的数据 那么新增的批复 更新现有批复台账
                    LmtGrpApp lmtGrpApp = lmtGrpAppService.queryInfoByGrpSerno(lastLmtGrpReplyList.get(0).getGrpSerno());
                    if (lmtGrpApp == null) {
                        throw new Exception("根据原集团授信批复查询授信申请信息失败!原授信批复流水号:" + lmtGrpReply.getOrigiGrpReplySerno());
                    }
                    if (lmtGrpApp.getApproveStatus().equals(CmisCommonConstants.WF_STATUS_997)) {// 通过
                        return "997_05";
                    } else if (lmtGrpApp.getApproveStatus().equals(CmisCommonConstants.WF_STATUS_998)) {// 否决
                        return "998_05";
                    } else {
                        return findRealLmtType(lastLmtGrpReplyList.get(0));
                    }
                } else {
                    throw new Exception("原集团授信批复查询异常!原授信批复流水号:" + lmtGrpReply.getOrigiGrpReplySerno());
                }
            } else {
                return lmtGrpReply.getLmtType();
            }
        } else if (CmisCommonConstants.LMT_TYPE_06.equals(lmtGrpReply.getLmtType())) {
            return lmtGrpReply.getLmtType();
        } else {
            return lmtGrpReply.getLmtType();
        }
    }

    /**
     * @方法名称: generateNewLmtReplyAcc
     * @方法描述: 新增集团授信批复台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-07 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     *
     */
    @Transactional
    public void generateNewLmtGrpReplyAcc(LmtGrpReply lmtGrpReply) throws Exception {
        // 续作时，生成新的批复之前将之前的台账作废
        if(!StringUtils.isBlank(lmtGrpReply.getOrigiGrpReplySerno())){
            HashMap map = new HashMap();
            map.put("replySerno",lmtGrpReply.getOrigiGrpReplySerno());
            map.put("oprType",CmisCommonConstants.OP_TYPE_01);
            LmtGrpReplyAcc oldLmtGrpReplyAcc = this.queryDetailByGrpReplySerno(map);
            if(oldLmtGrpReplyAcc!=null){
                oldLmtGrpReplyAcc.setReplyStatus(CmisLmtConstants.STD_REPLY_STATUS_02);
                updateSelective(oldLmtGrpReplyAcc);
                log.info("根据原批复编号将原批复台账作废:" + oldLmtGrpReplyAcc.getGrpReplySerno());
            }
        }
        // 1.通过查询授信批复，将查出的数据插入至批复台账中
        LmtGrpReplyAcc lmtGrpReplyAcc = new LmtGrpReplyAcc();
        BeanUtils.copyProperties(lmtGrpReply, lmtGrpReplyAcc);
        String accNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_DETAILS, new HashMap<>());
        log.info("生成新的集团授信台账流水号:" + accNo);
        lmtGrpReplyAcc.setPkId(UUID.randomUUID().toString());
        lmtGrpReplyAcc.setGrpAccNo(accNo);
        lmtGrpReplyAcc.setReplyStatus(CmisLmtConstants.STD_ZB_LMT_STATE_01);
        this.insert(lmtGrpReplyAcc);
        log.info("生成新的集团授信台账:" + accNo);

        // 获取到授信申请的数据
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpReply.getGrpReplySerno());
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            String singleAccNo = lmtReplyAccService.generateLmtReplyAccForGrpByReplySerno(lmtGrpMemRel.getSingleSerno());
            lmtGrpMemRelService.generateLmtGrpMemRelForLmtGrpApprForword(lmtGrpReply.getInputId(), lmtGrpReply.getInputBrId(), accNo, lmtGrpMemRel, singleAccNo);
        }
    }

    /**
     * @方法名称: updateLmtGrpReplyAcc
     * @方法描述: 修改集团授信批复台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-07 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     *
     */
    @Transactional
    public void updateLmtGrpReplyAcc(LmtGrpReply lmtGrpReply, String isReplyChg) throws Exception {

        // 获取到授信申请的数据
        log.info("获取原台账关系表数据!");
        HashMap map = new HashMap();
        map.put("replySerno",lmtGrpReply.getOrigiGrpReplySerno());
        LmtGrpReplyAcc oldLmtGrpReplyAcc = this.queryDetailByGrpReplySerno(map);
        List<LmtGrpMemRel> lmtGrpMemRelAccList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(oldLmtGrpReplyAcc.getGrpAccNo());

        // 1.通过查询批复台账，将批复数据更新至批复台账中
        LmtGrpReplyAcc updateLmtGrpReplyAcc = new LmtGrpReplyAcc();
        LmtGrpReplyAcc lmtGrpReplyAcc = this.getLastLmtReplyAcc(lmtGrpReply.getGrpCusId());
        String oldPkId = lmtGrpReplyAcc.getPkId();
        BeanUtils.copyProperties(lmtGrpReply, updateLmtGrpReplyAcc);
        updateLmtGrpReplyAcc.setGrpAccNo(lmtGrpReplyAcc.getGrpAccNo());
        updateLmtGrpReplyAcc.setPkId(lmtGrpReplyAcc.getPkId());
        updateLmtGrpReplyAcc.setReplyStatus(CmisCommonConstants.STD_XD_REPLY_STATUS_01);
        log.info("更新集团授信台账:" + updateLmtGrpReplyAcc.getGrpAccNo());
        this.update(updateLmtGrpReplyAcc);

        List<String> cusList = new ArrayList<>();// 集团客户成员数据
        List<String> memList = new ArrayList<>();// 集团成员授信关系数据
        for(LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelAccList){
            memList.add(lmtGrpMemRel.getCusId());
        }
        log.info("查询当前集团授信项下成员关系,成员:"+memList.toString());
        CusGrpMemberRelDto cusGrpMemberRelDto = new CusGrpMemberRelDto();
        cusGrpMemberRelDto.setGrpNo(oldLmtGrpReplyAcc.getGrpCusId());
        log.info("查询集团项下成员关系,集团编号:"+oldLmtGrpReplyAcc.getGrpCusId());
        List<CusGrpMemberRelDto> cusGrpMemberRelDtoList = iCusClientService.queryCusGrpMemberRelDtoList(cusGrpMemberRelDto).getData();
        if (cusGrpMemberRelDtoList != null && cusGrpMemberRelDtoList.size() > 0) {
            for (Object o : cusGrpMemberRelDtoList) {
                CusGrpMemberRelDto item = ObjectMapperUtils.instance().convertValue(o, CusGrpMemberRelDto.class);
                cusList.add(item.getCusId());
                if(memList.toString().contains(item.getCusId())){
                    log.info("当前最新的集团成员表中成员数据存在于授信成员表中,当前成员:"+item.getCusId());
                    // 根据当前的集团申报批复信息 进行正常的更新操作
                    // 根据当前客户号去查询最新的批复信息,根据批复信息去进行正常的更新台账操作
                    HashMap lmtReplyMap = new HashMap();
                    lmtReplyMap.put("cusId",item.getCusId());
                    lmtReplyMap.put("sort","update_time desc");
                    List<LmtReply> lmtReplyList = lmtReplyService.queryLmtReplyDataByParams(lmtReplyMap);
                    if(!lmtReplyList.isEmpty()&&lmtReplyList.size()>0){
                        LmtGrpMemRel lmtGrpMemRel1 = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSernoAndCusId(lmtGrpReply.getGrpSerno(),item.getCusId());
                        if(lmtGrpMemRel1 != null && StringUtils.nonBlank(lmtGrpMemRel1.getSingleSerno())){
                            if (CmisCommonConstants.LMT_TYPE_02.equals(lmtGrpReply.getLmtType())) { // 变更
                                log.info("集团授信变更判断");
                                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel1.getIsCurtChg())) {
                                    log.info("本次成员客户不填报"+lmtGrpMemRel1.getCusId());
                                    // 不填报时,不校验当前成员客户
                                    continue;
                                }else{
                                    lmtReplyAccService.updateLmtReplyAccForGrpBySerno(lmtReplyList.get(0).getSerno(), isReplyChg);
                                }
                            } else if (CmisCommonConstants.LMT_TYPE_07.equals(lmtGrpReply.getLmtType())) { // 预授信细化
                                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel1.getIsCurtRefine())) {
                                    log.info("本次成员客户不填报"+lmtGrpMemRel1.getCusId());
                                    // 不填报时,不校验当前成员客户
                                    continue;
                                }else{
                                    lmtReplyAccService.updateLmtReplyAccForGrpBySerno(lmtReplyList.get(0).getSerno(), isReplyChg);
                                }
                            } else if (CmisCommonConstants.LMT_TYPE_08.equals(lmtGrpReply.getLmtType())) { // 额度调剂
                                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel1.getIsCurtAdjust())) {
                                    log.info("本次成员客户不填报"+lmtGrpMemRel1.getCusId());
                                    // 不填报时,不校验当前成员客户
                                    continue;
                                }else{
                                    lmtReplyAccService.updateLmtReplyAccForGrpBySerno(lmtReplyList.get(0).getSerno(), isReplyChg);
                                }
                            } else {  // 除去变更 预授信细化 调剂 外 统一以 是否参与本次申报 字段判断
                                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel1.getIsPrtcptCurtDeclare())) {
                                    log.info("本次成员客户不填报"+lmtGrpMemRel1.getCusId());
                                    // 不填报时,不校验当前成员客户
                                    continue;
                                }else{
                                    lmtReplyAccService.updateLmtReplyAccForGrpBySerno(lmtReplyList.get(0).getSerno(), isReplyChg);
                                }
                            }
                        }
                    }
                    LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(item.getCusId());
                    log.info("查询当前客户["+item.getCusId()+"]对应的生效的台账信息[{"+JSON.toJSONString(lmtReplyAcc)+"}]");
                    LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(lmtReplyAcc.getAccNo());
                    log.info("查询当前客户批复台账编号["+lmtReplyAcc.getAccNo()+"]查询对应的关系表数据[{"+JSON.toJSONString(lmtGrpMemRel)+"}]");
                    lmtGrpMemRel.setOpenLmtAmt(lmtReplyAcc.getOpenTotalLmtAmt());
                    lmtGrpMemRel.setLowRiskLmtAmt(lmtReplyAcc.getLowRiskTotalLmtAmt());
                    log.info("更新关系表敞口额度:"+lmtReplyAcc.getOpenTotalLmtAmt()+";更新关系表低风险额度:"+lmtReplyAcc.getLowRiskTotalLmtAmt());
                    lmtGrpMemRelService.update(lmtGrpMemRel);
                }else{
                    log.info("当前最新的集团成员表中成员数据不存在于授信成员表中,当前成员:"+item.getCusId());
                    // 继续判断当前客户是否存在台账,如果有那么更新当前台账信息(isGrp=1),并且新增台账关系数据
                                             //如果没有那么新增正常台账信息,并且新增台账关系数据
                    HashMap lmtReplyAccMap = new HashMap();
                    lmtReplyAccMap.put("cusId",item.getCusId());
                    lmtReplyAccMap.put("sort","update_time desc");
                    List<LmtReplyAcc> lmtReplyAccList = lmtReplyAccService.selectByParams(lmtReplyAccMap);
                    if(!lmtReplyAccList.isEmpty() && lmtReplyAccList.size()>0){
                        LmtReplyAcc lmtReplyAcc = lmtReplyAccList.get(0);
                        lmtReplyAcc.setIsGrp(CmisCommonConstants.YES_NO_1);
                        lmtReplyAccService.update(lmtReplyAcc);
                        LmtGrpMemRel lmtGrpMemRel = new LmtGrpMemRel();
                        lmtGrpMemRel.setPkId(UUID.randomUUID().toString());
                        lmtGrpMemRel.setGrpSerno(lmtGrpReply.getGrpSerno());
                        lmtGrpMemRel.setSingleSerno(lmtReplyAcc.getSerno());
                        lmtGrpMemRel.setGrpCusId(lmtGrpReply.getGrpCusId());
                        lmtGrpMemRel.setGrpCusName(lmtGrpReply.getGrpCusName());
                        lmtGrpMemRel.setCusId(item.getCusId());
                        lmtGrpMemRel.setCusName(item.getCusName());
                        lmtGrpMemRel.setCusType(item.getCusType());
                        lmtGrpMemRel.setManagerId(item.getManagerId());
                        lmtGrpMemRel.setManagerBrId(item.getManagerBrId());
                        lmtGrpMemRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                        lmtGrpMemRel.setInputId(lmtGrpReply.getInputId());
                        lmtGrpMemRel.setInputBrId(lmtGrpReply.getInputBrId());
                        lmtGrpMemRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtGrpMemRel.setUpdId(lmtGrpReply.getInputId());
                        lmtGrpMemRel.setUpdBrId(lmtGrpReply.getInputBrId());
                        lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtGrpMemRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        lmtGrpMemRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        lmtGrpMemRelService.insert(lmtGrpMemRel);
                    }else{
                        HashMap lmtReplyMap = new HashMap();
                        lmtReplyMap.put("cusId",item.getCusId());
                        lmtReplyMap.put("sort","update_time desc");
                        List<LmtReply> lmtReplyList = lmtReplyService.queryLmtReplyDataByParams(lmtReplyMap);

                        if(!lmtReplyList.isEmpty()&&lmtReplyList.size()>0){
                            // 新增 单一授信台账
                            String singleAccNo = lmtReplyAccService.generateLmtReplyAccForGrpBySerno(lmtReplyList.get(0).getSerno());
                            LmtGrpMemRel lmtGrpMemRel = new LmtGrpMemRel();
                            lmtGrpMemRel.setPkId(UUID.randomUUID().toString());
                            lmtGrpMemRel.setGrpSerno(lmtGrpReply.getGrpSerno());
                            lmtGrpMemRel.setSingleSerno(singleAccNo);
                            lmtGrpMemRel.setGrpCusId(lmtGrpReply.getGrpCusId());
                            lmtGrpMemRel.setGrpCusName(lmtGrpReply.getGrpCusName());
                            lmtGrpMemRel.setCusId(item.getCusId());
                            lmtGrpMemRel.setCusName(item.getCusName());
                            lmtGrpMemRel.setCusType(item.getCusType());
                            lmtGrpMemRel.setManagerId(item.getManagerId());
                            lmtGrpMemRel.setManagerBrId(item.getManagerBrId());
                            lmtGrpMemRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                            lmtGrpMemRel.setInputId(lmtGrpReply.getInputId());
                            lmtGrpMemRel.setInputBrId(lmtGrpReply.getInputBrId());
                            lmtGrpMemRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                            lmtGrpMemRel.setUpdId(lmtGrpReply.getInputId());
                            lmtGrpMemRel.setUpdBrId(lmtGrpReply.getInputBrId());
                            lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                            lmtGrpMemRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                            lmtGrpMemRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                            // 新增 台账关系信息
                            lmtGrpMemRelService.insert(lmtGrpMemRel);
                        }

                    }
                }
            }
            for(LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelAccList) {
                log.info("判断当前客户是否还存在与关系表中,成员:" + lmtGrpMemRel.getCusId());
                if (cusList.toString().contains(lmtGrpMemRel.getCusId())) {
                    /*log.info("当前客户存在于关系表中,成员:"+lmtGrpMemRel.getCusId());
                    LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLmtReplyAccByAccNo(lmtGrpMemRel.getSingleSerno());
                    if(lmtReplyAcc == null){
                        throw BizException.error(null, EcbEnum.ECB010065.key, EcbEnum.ECB010065.value );
                    }
                    // 根据当前的集团申报批复信息 进行正常的更新操作
                    String singleAccNo = lmtReplyAccService.generateLmtReplyAccForGrpBySerno(lmtReplyAcc.getSerno());
                    // 更新操作 先删再增
                    lmtGrpMemRelService.deleteByPrimaryKey(lmtGrpMemRel.getPkId());
                    lmtGrpMemRel.setSingleSerno(singleAccNo);
                    lmtGrpMemRelService.insert(lmtGrpMemRel);*/
                    // lmtGrpMemRelService.generateLmtGrpMemRelForLmtGrpApprForword(lmtGrpReply.getInputId(), lmtGrpReply.getInputBrId(),  updateLmtGrpReplyAcc.getGrpAccNo(), lmtGrpMemRel, singleAccNo);
                }else{
                    log.info("当前客户不存在于关系表中,成员:"+lmtGrpMemRel.getCusId());
                    // 解除原台账关系表数据 病区修改原团成员授信台账信息(isGrp=0)
                    HashMap lmtReplyAccMap = new HashMap();
                        lmtReplyAccMap.put("cusId",lmtGrpMemRel.getCusId());
                        lmtReplyAccMap.put("sort","update_time desc");
                    List<LmtReplyAcc> lmtReplyAccList = lmtReplyAccService.selectByParams(lmtReplyAccMap);
                    if (!lmtReplyAccList.isEmpty() && lmtGrpMemRelAccList.size() > 0) {
                        LmtReplyAcc lmtReplyAcc = lmtReplyAccList.get(0);
                        lmtReplyAcc.setIsGrp(CmisCommonConstants.YES_NO_0);
                        lmtReplyAccService.update(lmtReplyAcc);
                        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtReplyAcc.getAccNo());
                        if (!lmtGrpMemRelList.isEmpty() && lmtGrpMemRelList.size() > 0) {
                            lmtGrpMemRelList.get(0).setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                            lmtGrpMemRelService.updateSelective(lmtGrpMemRelList.get(0));
                        }
                    }
                }
            }
        }

//        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpReply.getGrpSerno());
//        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
//            lmtReplyAccService.updateLmtReplyAccForGrpBySerno(lmtGrpMemRel.getSingleSerno(), isReplyChg);
//        }
    }

    /**
     * @方法名称: getLastLmtAcc
     * @方法描述: 根据申请流水号查询授信批复生成授信批复台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-07 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void synLmtReplyAccToLmtSys(String cusId) throws Exception {
        CmisLmt0002ReqDto cmisLmt0002ReqDto = new CmisLmt0002ReqDto();

        // 查询客户生效的授信批复台账
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("replyStatus", CmisCommonConstants.STD_XD_REPLY_STATUS_01);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryMap.put("grpCusId", cusId);
        queryMap.put("sort", "UPDATE_TIME desc");
        List<LmtGrpReplyAcc> lmtGrpReplyAccList = lmtGrpReplyAccMapper.selectByParams(queryMap);
        log.info("当前集团客户的授信台账信息:"+JSON.toJSONString(lmtGrpReplyAccList));
        if (lmtGrpReplyAccList != null && lmtGrpReplyAccList.size() == 1) {
            LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccList.get(0);

            HashMap<String, String> queryLmtGrpReplyAccMap = new HashMap<String, String>();
            queryLmtGrpReplyAccMap.put("grpReplySerno", lmtGrpReplyAcc.getGrpReplySerno());
            queryLmtGrpReplyAccMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtGrpReply> lmtGrpReplyList = lmtGrpReplyService.queryLmtGrpReplyByParams(queryLmtGrpReplyAccMap);
            log.info("当前集团客户的授信台账信息[{"+lmtGrpReplyAcc.getGrpReplySerno()+"}]查询对应的批复信息[{"+JSON.toJSONString(lmtGrpReplyList)+"}]");
            LmtGrpReply lmtGrpReply;
            if (lmtGrpReplyList != null && lmtGrpReplyList.size() == 1) {
                lmtGrpReply = lmtGrpReplyList.get(0);
            } else {
                throw new Exception("集团授信批复数据种查询异常");
            }
            // 集团原批复台账
            LmtGrpReplyAcc oldLmtGrpReplyAcc = new LmtGrpReplyAcc();
            if(!StringUtils.isBlank(lmtGrpReply.getOrigiGrpReplySerno())){
                HashMap map = new HashMap();
                map.put("replySerno",lmtGrpReply.getOrigiGrpReplySerno());
                map.put("oprType",CmisCommonConstants.OP_TYPE_01);
                oldLmtGrpReplyAcc = this.queryDetailByGrpReplySerno(map);
            }

            //批复台账编号
            String grpAccNo = lmtGrpReplyAcc.getGrpAccNo();
            // 组装额度系统报文
            cmisLmt0002ReqDto.setSysId(CmisCommonConstants.SYS_SHORT_NAME);
            cmisLmt0002ReqDto.setInstuCde(CmisCommonUtils.getInstucde(lmtGrpReplyAcc.getManagerBrId()));
            cmisLmt0002ReqDto.setGrpNo(lmtGrpReplyAcc.getGrpCusId());
            cmisLmt0002ReqDto.setGrpName(lmtGrpReplyAcc.getGrpCusName());
            cmisLmt0002ReqDto.setGrpAccNo(grpAccNo);
            if((CmisCommonConstants.LMT_TYPE_01.equals(lmtGrpReplyAcc.getLmtType())
                    || CmisCommonConstants.LMT_TYPE_04.equals(lmtGrpReplyAcc.getLmtType())
                    || CmisCommonConstants.LMT_TYPE_07.equals(lmtGrpReplyAcc.getLmtType())
                    || CmisCommonConstants.LMT_TYPE_08.equals(lmtGrpReplyAcc.getLmtType())
                    || CmisCommonConstants.LMT_TYPE_05.equals(lmtGrpReplyAcc.getLmtType())
                    || CmisCommonConstants.LMT_TYPE_02.equals(lmtGrpReplyAcc.getLmtType()))){
                cmisLmt0002ReqDto.setIsCreateAcc(CmisCommonConstants.STD_ZB_YES_NO_0);
            }else if(CmisCommonConstants.LMT_TYPE_03.equals(lmtGrpReplyAcc.getLmtType())){
                if( oldLmtGrpReplyAcc != null && !"".equals(oldLmtGrpReplyAcc.getGrpAccNo())){
                    cmisLmt0002ReqDto.setOrigiGrpAccNo(oldLmtGrpReplyAcc.getGrpAccNo());
                }
                cmisLmt0002ReqDto.setIsCreateAcc(CmisCommonConstants.STD_ZB_YES_NO_1);
            }else if(CmisCommonConstants.LMT_TYPE_06.equals(lmtGrpReplyAcc.getLmtType())){
                // 查询当前申请对应批复数据  如果 原批复编号有值则为续作 没有为新增
                if(StringUtils.isBlank(lmtGrpReply.getOrigiGrpReplySerno())){
                    cmisLmt0002ReqDto.setIsCreateAcc(CmisCommonConstants.STD_ZB_YES_NO_0);
                }else{
                    cmisLmt0002ReqDto.setIsCreateAcc(CmisCommonConstants.STD_ZB_YES_NO_1);
                    if( oldLmtGrpReplyAcc != null && !"".equals(oldLmtGrpReplyAcc.getGrpAccNo())){
                        cmisLmt0002ReqDto.setOrigiGrpAccNo(oldLmtGrpReplyAcc.getGrpAccNo());
                    }
                }
            }
            cmisLmt0002ReqDto.setTerm(lmtGrpReplyAcc.getLmtTerm());
            String startDate = lmtGrpReply.getReplyInureDate();
            String grpStartDate = lmtReplyAccSubPrdService.getEarlyDateByGrpAccNo(lmtGrpReplyAcc.getGrpAccNo());
            String grpLastDate = lmtReplyAccSubPrdService.getLastDateByGrpAccNo(lmtGrpReplyAcc.getGrpAccNo());
            cmisLmt0002ReqDto.setStartDate(grpStartDate);
            cmisLmt0002ReqDto.setEndDate(grpLastDate);
            cmisLmt0002ReqDto.setInputId(lmtGrpReplyAcc.getInputId());
            cmisLmt0002ReqDto.setInputBrId(lmtGrpReplyAcc.getInputBrId());
            cmisLmt0002ReqDto.setInputDate(lmtGrpReplyAcc.getInputDate());

            List<CmisLmt0002LmtListReqDto> cmisLmt0002LmtListReqDtoList = new ArrayList<CmisLmt0002LmtListReqDto>();
            List<CmisLmt0002LmtSubListReqDto> cmisLmt0002LmtSubListReqDtoList = new ArrayList<CmisLmt0002LmtSubListReqDto>();
            List<LmtReplyAcc> lmtReplyAccList = lmtReplyAccService.selectLmtReplyAccByGrpAccNo(grpAccNo);
            for (LmtReplyAcc lmtReplyAcc : lmtReplyAccList) {
                CmisLmt0002LmtListReqDto replyAcc = new CmisLmt0002LmtListReqDto();
                HashMap<String, String> queryLmtReplyMap = new HashMap<String, String>();
                queryLmtReplyMap.put("replySerno", lmtReplyAcc.getReplySerno());
                queryLmtReplyMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                List<LmtReply> lmtReplyList = lmtReplyService.queryLmtReplyDataByParams(queryLmtReplyMap);
                LmtReply lmtReply;
                if (lmtReplyList != null && lmtReplyList.size() == 1) {
                    lmtReply = lmtReplyList.get(0);
                } else {
                    throw new Exception("单一客户授信批复数据种查询异常");
                }

                // 查询客户类型
                CusBaseDto cusBaseDto = cmisCusClientService.cusBaseInfo(lmtReplyAcc.getCusId()).getData();
                // 取批复表中原批复编号  查询对应的台账数据
                if(!StringUtils.isBlank(lmtReply.getOrigiLmtReplySerno())){
                    LmtReplyAcc oldLmtReplyAcc = new LmtReplyAcc();
                    HashMap map = new HashMap();
                    map.put("replySerno",lmtReply.getOrigiLmtReplySerno());
                    map.put("oprType",CmisCommonConstants.OP_TYPE_01);
                    oldLmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(map);
                    // 组装额度系统报文
                    if(oldLmtReplyAcc != null){
                        replyAcc.setOrigiAccNo(oldLmtReplyAcc.getAccNo());
                        replyAcc.setIsCreateAcc(CmisCommonConstants.YES_NO_1);
                    }else{
                        log.info("原批复查询不到："+JSON.toJSONString(oldLmtReplyAcc));
                        replyAcc.setIsCreateAcc(CmisCommonConstants.YES_NO_0);
                    }
                }else{
                    log.info("原批复流水号不存在+"+lmtReply.getOrigiLmtReplySerno());
                    replyAcc.setIsCreateAcc(CmisCommonConstants.YES_NO_0);
                }
                // 单一授信的处理不适用于集团
//                if (CmisCommonConstants.LMT_TYPE_03.equals(lmtReplyAcc.getLmtType())) {
//                    replyAcc.setIsCreateAcc(CmisCommonConstants.STD_ZB_YES_NO_1);
//                    Map<String, String> map = new HashMap<>();
//                    map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
//                    map.put("replySerno", lmtReply.getOrigiLmtReplySerno());
//                    LmtReplyAcc lmtReplyAccOrigi = lmtReplyAccService.selectAccNoDataByParams(map);
//                    replyAcc.setOrigiAccNo(lmtReplyAccOrigi.getAccNo());
//                } else {
//                    replyAcc.setIsCreateAcc(CmisCommonConstants.STD_ZB_YES_NO_0);
//                }
                replyAcc.setAccNo(lmtReplyAcc.getAccNo());
                replyAcc.setCurType(lmtReplyAcc.getCurType());
                replyAcc.setCusId(lmtReplyAcc.getCusId());
                replyAcc.setCusName(lmtReplyAcc.getCusName());
                replyAcc.setCusType(cusBaseDto.getCusCatalog());

                replyAcc.setLmtAmt(lmtReplyAcc.getOpenTotalLmtAmt().add(lmtReplyAcc.getLowRiskTotalLmtAmt()));
                replyAcc.setTerm(lmtReplyAcc.getLmtTerm());
                replyAcc.setStartDate(lmtReplyAccSubPrdService.getEarlyDateByAccNo(lmtReplyAcc.getAccNo()));
                replyAcc.setEndDate(lmtReplyAccSubPrdService.getLastDateByAccNo(lmtReplyAcc.getAccNo()));
                replyAcc.setAccStatus(lmtReplyAcc.getAccStatus());
                replyAcc.setManagerId(lmtReplyAcc.getManagerId());
                replyAcc.setManagerBrId(lmtReplyAcc.getManagerBrId());
                replyAcc.setInputId(lmtReplyAcc.getInputId());
                replyAcc.setInputBrId(lmtReplyAcc.getInputBrId());
                replyAcc.setInputDate(lmtReplyAcc.getInputDate());
                //授信模式 默认01--综合授信
                replyAcc.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_01);
                cmisLmt0002LmtListReqDtoList.add(replyAcc);
                HashMap<String, String> querySubMap = new HashMap<String, String>();
                querySubMap.put("accNo", lmtReplyAcc.getAccNo());
                //querySubMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);// TODO 删除的分项也要给
                List<LmtReplyAccSub> lmtReplyAccSubList = lmtReplyAccSubService.queryLmtReplyAccSubByParams(querySubMap);
                if (lmtReplyAccSubList != null) {
                    for (LmtReplyAccSub lmtReplyAccSub : lmtReplyAccSubList) {
                        // 获取对应批复中的原台账分项编号
                        LmtReplySub lmtReplySub = lmtReplySubService.queryLmtReplySubByReplySubSerno(lmtReplyAccSub.getReplySubSerno());
                        if(StringUtils.isBlank(lmtReplySub.getOrigiLmtAccSubNo()) && lmtReplyAccSub.getOprType().equals(CmisCommonConstants.OPR_TYPE_DELETE)){
                            log.info("本次新增的并且删除的分项不发送额度");
                            continue;
                        }
                        CmisLmt0002LmtSubListReqDto sub = new CmisLmt0002LmtSubListReqDto();
                        sub.setParentId(replyAcc.getAccNo());
                        sub.setLimitSubName(lmtReplyAccSub.getAccSubName());
                        sub.setAccSubNo(lmtReplyAccSub.getAccSubNo());
                        sub.setAvlamt(lmtReplyAccSub.getLmtAmt());
                        sub.setCurType(lmtReplyAccSub.getCurType());
                        sub.setIsLriskLmt(CmisCommonConstants.GUAR_MODE_60.equals(lmtReplyAccSub.getGuarMode()) ? CmisCommonConstants.STD_ZB_YES_NO_1 : CmisCommonConstants.STD_ZB_YES_NO_0);
                        sub.setSuitGuarWay(lmtReplyAccSub.getGuarMode());
                        // 是否循环授信
                        sub.setIsRevolv(lmtReplyAccSub.getIsRevolvLimit());
                        // 预授信字段
                        sub.setIsPreCrd(lmtReplyAccSub.getIsPreLmt());
                        // 新增字段 lmtBizTypeProp-授信品种类型 lmtSubType-授信分项类型
                        sub.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_01);
                        // 原授信分项编号
                        sub.setOrigiAccSubNo(Optional.ofNullable(lmtReplySub.getOrigiLmtAccSubNo()).orElse(""));
                        sub.setAccSubStatus(CmisLmtConstants.STD_ZB_LMT_STATE_01);
                        sub.setOprType(lmtReplyAccSub.getOprType());
                        Map<String,String> mapQuery = new HashMap<>();
                        mapQuery.put("accSubNo",lmtReplyAccSub.getAccSubNo());
                        mapQuery.put("isRevolvLimit",lmtReplyAccSub.getIsRevolvLimit());
                        String lastDate = lmtReplyAccSubPrdService.getLastEndDate(mapQuery);
                        String earlyDate = lmtReplyAccSubPrdService.getEarlyStartDate(mapQuery);
                        sub.setStartDate(earlyDate);
                        sub.setEndDate(lastDate);
                        cmisLmt0002LmtSubListReqDtoList.add(sub);

                        HashMap<String, String> querySubPrdMap = new HashMap<String, String>();
                        querySubPrdMap.put("accSubNo", lmtReplyAccSub.getAccSubNo());
                        //querySubPrdMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);// TODO 删除的分项也要给
                        List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByParams(querySubPrdMap);
                        if (lmtReplyAccSubPrdList != null) {
                            for (LmtReplyAccSubPrd lmtReplyAccSubPrd : lmtReplyAccSubPrdList) {
                                CmisLmt0002LmtSubListReqDto subPrd = new CmisLmt0002LmtSubListReqDto();
                                LmtReplySubPrd lmtReplySubPrd = lmtReplySubPrdService.queryLmtReplySubPrdByReplySubPrdSerno(lmtReplyAccSubPrd.getReplySubPrdSerno());
                                if(StringUtils.isBlank(lmtReplySubPrd.getOrigiLmtAccSubPrdNo()) && lmtReplyAccSubPrd.getOprType().equals(CmisCommonConstants.OPR_TYPE_DELETE)){
                                    log.info("本次新增的并且删除的分项品种不发送额度");
                                    continue;
                                }
                                subPrd.setAccSubNo(lmtReplyAccSubPrd.getAccSubPrdNo());
                                subPrd.setParentId(sub.getAccSubNo());
                                // 原授信分项编号
                                subPrd.setOrigiAccSubNo(Optional.ofNullable(lmtReplySubPrd.getOrigiLmtAccSubPrdNo()).orElse(""));
                                subPrd.setLimitSubName(lmtReplyAccSubPrd.getLmtBizTypeName());
                                subPrd.setLimitSubNo(lmtReplyAccSubPrd.getLmtBizType());
                                subPrd.setAvlamt(lmtReplyAccSubPrd.getLmtAmt());
                                subPrd.setCurType(lmtReplyAccSubPrd.getCurType());
                                subPrd.setTerm(lmtReplyAccSubPrd.getLmtTerm());
                                subPrd.setStartDate(lmtReplyAccSubPrd.getStartDate());
                                subPrd.setEndDate(lmtReplyAccSubPrd.getEndDate());
                                subPrd.setBailPreRate(lmtReplyAccSubPrd.getBailPreRate());
                                subPrd.setRateYear(lmtReplyAccSubPrd.getRateYear());
                                subPrd.setSuitGuarWay(lmtReplyAccSubPrd.getGuarMode());
                                subPrd.setIsPreCrd(lmtReplyAccSubPrd.getIsPreLmt());
                                subPrd.setLmtGraper(lmtReplyAccSubPrd.getLmtGraperTerm() == null ? "0" : lmtReplyAccSubPrd.getLmtGraperTerm().toString());
                                subPrd.setIsRevolv(lmtReplyAccSubPrd.getIsRevolvLimit());
                                subPrd.setAccSubStatus(CmisLmtConstants.STD_ZB_LMT_STATE_01);
                                subPrd.setIsLriskLmt(CmisCommonConstants.GUAR_MODE_60.equals(lmtReplyAccSubPrd.getGuarMode()) ? CmisCommonConstants.STD_ZB_YES_NO_1 : CmisCommonConstants.STD_ZB_YES_NO_0);
                                // 新增字段 lmtBizTypeProp-授信品种类型 lmtSubType-授信分项类型
                                subPrd.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02);
                                subPrd.setLmtBizTypeProp(lmtReplyAccSubPrd.getLmtBizTypeProp());
                                subPrd.setOprType(lmtReplyAccSubPrd.getOprType());
                                cmisLmt0002LmtSubListReqDtoList.add(subPrd);
                            }
                        } else {
                            throw new Exception("授信批复台账分项适用品种查询异常");
                        }
                    }
                } else {
                    throw new Exception("授信批复台账分项查询异常");
                }
            }
            // 组装报文
            cmisLmt0002ReqDto.setLmtList(cmisLmt0002LmtListReqDtoList);
            cmisLmt0002ReqDto.setLmSubList(cmisLmt0002LmtSubListReqDtoList);
            // 调用额度接口
            log.info("根据批复台账编号【"+grpAccNo+"】前往额度系统-建立额度请求报文：" + cmisLmt0002ReqDto);
            ResultDto<CmisLmt0002RespDto> cmisLmt0002RespDto = cmisLmtClientService.cmisLmt0002(cmisLmt0002ReqDto);
            log.info("根据批复台账编号【"+grpAccNo+"】前往额度系统-建立额度返回报文：" + cmisLmt0002RespDto);

            if (SuccessEnum.SUCCESS.key.equals(cmisLmt0002RespDto.getData().getErrorCode())) {
                log.info("根据批复台账编号【"+grpAccNo+"】,前往额度系统同步集团授信台账成功！");
            } else {
                log.info("根据批复台账编号【"+grpAccNo+"】,前往额度系统同步集团授信台账失败！");
                throw new Exception("额度系统-前往额度系统建立额度失败:" + cmisLmt0002RespDto.getData().getErrorMsg());
            }
        } else {
            throw new Exception("授信批复台账查询异常");
        }
    }

    /**
     * @方法名称: queryDetailByGrpReplySerno
     * @方法描述: 通过批复号 获取批复台账信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-07-15 10:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */

    private LmtGrpReplyAcc queryDetailByGrpReplySerno(Map map) {
        LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccMapper.queryDetailByGrpReplySerno(map);
        return lmtGrpReplyAcc;
    }

    /**
     * @方法名称: getReplyAccForRefine
     * @方法描述: 过滤得到可进行预授信细化的授信批复台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-05 10:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtGrpReplyAcc> getReplyAccForRefine(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrpReplyAcc> list = lmtGrpReplyAccMapper.getReplyAccForChg(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称：queryByManagerId
     * @方法描述：获取当前登录人下的批复台账信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-05-22 上午 10:04
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtGrpReplyAcc> queryByManagerId() {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        String managerId = userInfo.getLoginCode();
        if (StringUtils.isBlank(managerId)) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        HashMap<String, String> paramMap = new HashMap<>();
        paramMap.put("managerId", managerId);
        return lmtGrpReplyAccMapper.queryByManagerId(paramMap);
    }

    /**
     * @方法名称：queryByGrpCusId
     * @方法描述：根据集团客户号查询申请信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-06-22 上午 10:04
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtGrpReplyAcc> queryByGrpCusId(QueryModel queryModel) {
        return lmtGrpReplyAccMapper.queryByGrpCusId(queryModel);
    }

    /**
     * @方法名称：updateLmtGrpReplyAccUnderReplyChg
     * @方法描述：集团批复变更更新批复台账处理
     * @参数与返回说明：
     * @算法描述：
     * @创建人：css
     * @创建时间：2021-07-22 20:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    public void updateLmtGrpReplyAccUnderReplyChg(LmtGrpReply lmtGrpReply, String yesNo1) {
        // 1.通过查询批复台账，将批复数据更新至批复台账中
        LmtGrpReplyAcc updateLmtGrpReplyAcc = new LmtGrpReplyAcc();
        LmtGrpReplyAcc lmtGrpReplyAcc = this.getLastLmtReplyAcc(lmtGrpReply.getGrpCusId());
        String oldPkId = lmtGrpReplyAcc.getPkId();
        BeanUtils.copyProperties(lmtGrpReply, updateLmtGrpReplyAcc);
        updateLmtGrpReplyAcc.setGrpAccNo(lmtGrpReplyAcc.getGrpAccNo());
        updateLmtGrpReplyAcc.setPkId(lmtGrpReplyAcc.getPkId());
        updateLmtGrpReplyAcc.setReplyStatus(CmisCommonConstants.STD_XD_REPLY_STATUS_01);
        this.update(updateLmtGrpReplyAcc);
        log.info("更新授信台账:" + updateLmtGrpReplyAcc.getGrpAccNo());
    }

    /**
     * @函数名称:queryLmtGrpReplyAccByGrpReplySerno
     * @函数描述:根据批复编号查询批复台账信息
     * @参数与返回说明:
     * @算法描述:
     */

    public LmtGrpReplyAcc queryLmtGrpReplyAccByGrpReplySerno(String grpReplySerno) {
        HashMap<String,String> map = new HashMap();
        map.put("replySerno",grpReplySerno);
        map.put("oprType",CmisCommonConstants.OP_TYPE_01);
        LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccMapper.queryDetailByGrpReplySerno(map);
        return lmtGrpReplyAcc;
    }

        /**
         * @方法名称：selectForManager
         * @方法描述：当前登录人下的批复台账信息
         * @参数与返回说明：
         * @算法描述：
         * @创建人：css
         * @创建时间：2021-08-10 下午 1:56
         * @修改记录：修改时间 修改人员  修改原因
         */
    public List<LmtGrpReplyAcc> selectForManager(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("reply_inure_date desc");
        List<LmtGrpReplyAcc> list = lmtGrpReplyAccMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据集团流水查集团批复
     * @param grpSerno
     * @return
     */
    public LmtGrpReplyAcc queryDetailByGrpSerno(String grpSerno) {
        return lmtGrpReplyAccMapper.queryDetailByGrpSerno(grpSerno);
    }

    /**
     * 根据单一台账编号查集团批复
     * @param singleSerno
     * @return
     */
    public LmtGrpReplyAcc getBySingleSerno(String singleSerno) {
        LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(singleSerno);
        return lmtGrpReplyAccMapper.selectByGrpAccNo(lmtGrpMemRel.getGrpSerno());
    }
}
