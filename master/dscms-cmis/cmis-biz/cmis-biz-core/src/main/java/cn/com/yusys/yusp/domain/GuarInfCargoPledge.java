/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfCargoPledge
 * @类描述: guar_inf_cargo_pledge数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:44:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_inf_cargo_pledge")
public class GuarInfCargoPledge extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 货品名称 **/
	@Column(name = "CARGO_NAME", unique = false, nullable = true, length = 100)
	private String cargoName;
	
	/** 品牌/厂家/产地 **/
	@Column(name = "VEHICLE_BRAND", unique = false, nullable = true, length = 100)
	private String vehicleBrand;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 详细地址 **/
	@Column(name = "DETAILS_ADDRESS", unique = false, nullable = true, length = 500)
	private String detailsAddress;
	
	/** 货物计量单位 STD_ZB_CARGO_MEASURE_UNIT **/
	@Column(name = "CARGO_MEASURE_UNIT", unique = false, nullable = true, length = 10)
	private String cargoMeasureUnit;
	
	/** 货物数量 **/
	@Column(name = "CARGO_AMOUNT", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal cargoAmount;
	
	/** 最新核定单价 **/
	@Column(name = "LATEST_APPROVED_PRICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal latestApprovedPrice;
	
	/** 是否有监管公司 **/
	@Column(name = "HAS_SUPERVISION", unique = false, nullable = true, length = 10)
	private String hasSupervision;
	
	/** 监管公司名称 **/
	@Column(name = "SUPERVISION_COMPANY_NAME", unique = false, nullable = true, length = 100)
	private String supervisionCompanyName;
	
	/** 监管公司组织机构代码 **/
	@Column(name = "SUPERVISION_ORG_CODE", unique = false, nullable = true, length = 40)
	private String supervisionOrgCode;
	
	/** 协议生效日 **/
	@Column(name = "AGREEMENT_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String agreementBeginDate;
	
	/** 协议到期日 **/
	@Column(name = "AGREEMENT_END_DATE", unique = false, nullable = true, length = 10)
	private String agreementEndDate;
	
	/** 供应链质押价值 **/
	@Column(name = "GYL_VAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal gylVal;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 10)
	private String curType;
	
	/** 货物其他计量单位文本输入 **/
	@Column(name = "CAR_MEASURE_UNIT_OTHER", unique = false, nullable = true, length = 200)
	private String carMeasureUnitOther;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 金额 **/
	@Column(name = "AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal amt;
	
	/** 保管人 **/
	@Column(name = "KEEP_ID", unique = false, nullable = true, length = 20)
	private String keepId;
	
	/** 货物详细类型 **/
	@Column(name = "COODS_DETAIL_TYPE", unique = false, nullable = true, length = 20)
	private String coodsDetailType;
	
	/** 货物型号 **/
	@Column(name = "GOODS_MODEL", unique = false, nullable = true, length = 40)
	private String goodsModel;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 操作类型   **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param cargoName
	 */
	public void setCargoName(String cargoName) {
		this.cargoName = cargoName;
	}
	
    /**
     * @return cargoName
     */
	public String getCargoName() {
		return this.cargoName;
	}
	
	/**
	 * @param vehicleBrand
	 */
	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand;
	}
	
    /**
     * @return vehicleBrand
     */
	public String getVehicleBrand() {
		return this.vehicleBrand;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param detailsAddress
	 */
	public void setDetailsAddress(String detailsAddress) {
		this.detailsAddress = detailsAddress;
	}
	
    /**
     * @return detailsAddress
     */
	public String getDetailsAddress() {
		return this.detailsAddress;
	}
	
	/**
	 * @param cargoMeasureUnit
	 */
	public void setCargoMeasureUnit(String cargoMeasureUnit) {
		this.cargoMeasureUnit = cargoMeasureUnit;
	}
	
    /**
     * @return cargoMeasureUnit
     */
	public String getCargoMeasureUnit() {
		return this.cargoMeasureUnit;
	}
	
	/**
	 * @param cargoAmount
	 */
	public void setCargoAmount(java.math.BigDecimal cargoAmount) {
		this.cargoAmount = cargoAmount;
	}
	
    /**
     * @return cargoAmount
     */
	public java.math.BigDecimal getCargoAmount() {
		return this.cargoAmount;
	}
	
	/**
	 * @param latestApprovedPrice
	 */
	public void setLatestApprovedPrice(java.math.BigDecimal latestApprovedPrice) {
		this.latestApprovedPrice = latestApprovedPrice;
	}
	
    /**
     * @return latestApprovedPrice
     */
	public java.math.BigDecimal getLatestApprovedPrice() {
		return this.latestApprovedPrice;
	}
	
	/**
	 * @param hasSupervision
	 */
	public void setHasSupervision(String hasSupervision) {
		this.hasSupervision = hasSupervision;
	}
	
    /**
     * @return hasSupervision
     */
	public String getHasSupervision() {
		return this.hasSupervision;
	}
	
	/**
	 * @param supervisionCompanyName
	 */
	public void setSupervisionCompanyName(String supervisionCompanyName) {
		this.supervisionCompanyName = supervisionCompanyName;
	}
	
    /**
     * @return supervisionCompanyName
     */
	public String getSupervisionCompanyName() {
		return this.supervisionCompanyName;
	}
	
	/**
	 * @param supervisionOrgCode
	 */
	public void setSupervisionOrgCode(String supervisionOrgCode) {
		this.supervisionOrgCode = supervisionOrgCode;
	}
	
    /**
     * @return supervisionOrgCode
     */
	public String getSupervisionOrgCode() {
		return this.supervisionOrgCode;
	}
	
	/**
	 * @param agreementBeginDate
	 */
	public void setAgreementBeginDate(String agreementBeginDate) {
		this.agreementBeginDate = agreementBeginDate;
	}
	
    /**
     * @return agreementBeginDate
     */
	public String getAgreementBeginDate() {
		return this.agreementBeginDate;
	}
	
	/**
	 * @param agreementEndDate
	 */
	public void setAgreementEndDate(String agreementEndDate) {
		this.agreementEndDate = agreementEndDate;
	}
	
    /**
     * @return agreementEndDate
     */
	public String getAgreementEndDate() {
		return this.agreementEndDate;
	}
	
	/**
	 * @param gylVal
	 */
	public void setGylVal(java.math.BigDecimal gylVal) {
		this.gylVal = gylVal;
	}
	
    /**
     * @return gylVal
     */
	public java.math.BigDecimal getGylVal() {
		return this.gylVal;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param carMeasureUnitOther
	 */
	public void setCarMeasureUnitOther(String carMeasureUnitOther) {
		this.carMeasureUnitOther = carMeasureUnitOther;
	}
	
    /**
     * @return carMeasureUnitOther
     */
	public String getCarMeasureUnitOther() {
		return this.carMeasureUnitOther;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param amt
	 */
	public void setAmt(java.math.BigDecimal amt) {
		this.amt = amt;
	}
	
    /**
     * @return amt
     */
	public java.math.BigDecimal getAmt() {
		return this.amt;
	}
	
	/**
	 * @param keepId
	 */
	public void setKeepId(String keepId) {
		this.keepId = keepId;
	}
	
    /**
     * @return keepId
     */
	public String getKeepId() {
		return this.keepId;
	}
	
	/**
	 * @param coodsDetailType
	 */
	public void setCoodsDetailType(String coodsDetailType) {
		this.coodsDetailType = coodsDetailType;
	}
	
    /**
     * @return coodsDetailType
     */
	public String getCoodsDetailType() {
		return this.coodsDetailType;
	}
	
	/**
	 * @param goodsModel
	 */
	public void setGoodsModel(String goodsModel) {
		this.goodsModel = goodsModel;
	}
	
    /**
     * @return goodsModel
     */
	public String getGoodsModel() {
		return this.goodsModel;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}


}