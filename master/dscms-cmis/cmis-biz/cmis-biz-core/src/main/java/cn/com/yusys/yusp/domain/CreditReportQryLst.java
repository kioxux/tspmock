/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-doc模块
 * @类名称: CreditReportQryLst
 * @类描述: credit_report_qry_lst数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-06 17:07:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_report_qry_lst")
public class CreditReportQryLst extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 征信查询流水号 **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "CRQL_SERNO")
    private String crqlSerno;

    /** 查询对象证件类型 **/
    @Column(name = "CERT_TYPE", unique = false, nullable = false, length = 5)
    private String certType;

    /** 查询对象证件号码 **/
    @Column(name = "CERT_CODE", unique = false, nullable = false, length = 30)
    private String certCode;

    /** 征信查询对象号 **/
    @Column(name = "CUS_ID", unique = false, nullable = false, length = 30)
    private String cusId;

    /** 征信查询对象名称 **/
    @Column(name = "CUS_NAME", unique = false, nullable = false, length = 300)
    private String cusName;

    /** 主借款人客户号 **/
    @Column(name = "BORROWER_CUS_ID", unique = false, nullable = true, length = 30)
    private String borrowerCusId;

    /** 主借款人证件号码 **/
    @Column(name = "BORROWER_CERT_CODE", unique = false, nullable = true, length = 30)
    private String borrowerCertCode;

    /** 主借款人名称 **/
    @Column(name = "BORROWER_CUS_NAME", unique = false, nullable = true, length = 300)
    private String borrowerCusName;

    /** 与主借款人关系 **/
    @Column(name = "BORROW_REL", unique = false, nullable = true, length = 5)
    private String borrowRel;

    /** 征信查询类别 **/
    @Column(name = "QRY_CLS", unique = false, nullable = true, length = 5)
    private String qryCls;

    /** 征信查询原因 **/
    @Column(name = "QRY_RESN", unique = false, nullable = true, length = 5)
    private String qryResn;

    /** 查询原因描述 **/
    @Column(name = "QRY_RESN_DEC", unique = false, nullable = true, length = 2000)
    private String qryResnDec;

    /** 授权书编号 **/
    @Column(name = "AUTHBOOK_NO", unique = false, nullable = true, length = 40)
    private String authbookNo;

    /** 授权书内容 **/
    @Column(name = "AUTHBOOK_CONTENT", unique = false, nullable = true, length = 40)
    private String authbookContent;

    /** 其他授权书内容 **/
    @Column(name = "OTHER_AUTHBOOK_EXT", unique = false, nullable = true, length = 256)
    private String otherAuthbookExt;

    /** 授权书日期 **/
    @Column(name = "AUTHBOOK_DATE", unique = false, nullable = true, length = 10)
    private String authbookDate;

    /** 主管客户经理 **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /** 主管机构 **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /** 成功发起时间 **/
    @Column(name = "SEND_TIME", unique = false, nullable = true, length = 20)
    private String sendTime;

    /** 审批状态 **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /** 是否成功发起 **/
    @Column(name = "IS_SUCCSS_INIT", unique = false, nullable = true, length = 5)
    private String isSuccssInit;

    /** 征信报告编号 **/
    @Column(name = "REPORT_NO", unique = false, nullable = true, length = 40)
    private String reportNo;

    /** 征信查询状态 **/
    @Column(name = "QRY_STATUS", unique = false, nullable = true, length = 5)
    private String qryStatus;

    /** 征信返回地址 **/
    @Column(name = "CREDIT_URL", unique = false, nullable = true, length = 200)
    private String creditUrl;

    /** 报告生成时间 **/
    @Column(name = "REPORT_CREATE_TIME", unique = false, nullable = true, length = 10)
    private String reportCreateTime;

    /** 征信查询标识 **/
    @Column(name = "QRY_FLAG", unique = false, nullable = true, length = 5)
    private String qryFlag;

    /** 影像编号 **/
    @Column(name = "IMAGE_NO", unique = false, nullable = true, length = 40)
    private String imageNo;

    /** 创建时间 **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private Date createTime;

    /** 修改时间 **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private Date updateTime;

    /** 授权模式 */
    @Column(name = "AUTH_WAY", unique = false, nullable = true, length = 10)
    private String authWay;

    /**
     * @return authWay
     */
    public String getAuthWay() {
        return authWay;
    }

    /**
     * @param authWay
     */
    public void setAuthWay(String authWay) {
        this.authWay = authWay;
    }

    /**
     * @return crqlSerno
     */
    public String getCrqlSerno() {
        return this.crqlSerno;
    }

    /**
     * @param crqlSerno
     */
    public void setCrqlSerno(String crqlSerno) {
        this.crqlSerno = crqlSerno;
    }

    /**
     * @return certType
     */
    public String getCertType() {
        return this.certType;
    }

    /**
     * @param certType
     */
    public void setCertType(String certType) {
        this.certType = certType;
    }

    /**
     * @return certCode
     */
    public String getCertCode() {
        return this.certCode;
    }

    /**
     * @param certCode
     */
    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return borrowerCusId
     */
    public String getBorrowerCusId() {
        return this.borrowerCusId;
    }

    /**
     * @param borrowerCusId
     */
    public void setBorrowerCusId(String borrowerCusId) {
        this.borrowerCusId = borrowerCusId;
    }

    /**
     * @return borrowerCertCode
     */
    public String getBorrowerCertCode() {
        return this.borrowerCertCode;
    }

    /**
     * @param borrowerCertCode
     */
    public void setBorrowerCertCode(String borrowerCertCode) {
        this.borrowerCertCode = borrowerCertCode;
    }

    /**
     * @return borrowerCusName
     */
    public String getBorrowerCusName() {
        return this.borrowerCusName;
    }

    /**
     * @param borrowerCusName
     */
    public void setBorrowerCusName(String borrowerCusName) {
        this.borrowerCusName = borrowerCusName;
    }

    /**
     * @return borrowRel
     */
    public String getBorrowRel() {
        return this.borrowRel;
    }

    /**
     * @param borrowRel
     */
    public void setBorrowRel(String borrowRel) {
        this.borrowRel = borrowRel;
    }

    /**
     * @return qryCls
     */
    public String getQryCls() {
        return this.qryCls;
    }

    /**
     * @param qryCls
     */
    public void setQryCls(String qryCls) {
        this.qryCls = qryCls;
    }

    /**
     * @return qryResn
     */
    public String getQryResn() {
        return this.qryResn;
    }

    /**
     * @param qryResn
     */
    public void setQryResn(String qryResn) {
        this.qryResn = qryResn;
    }

    /**
     * @return qryResnDec
     */
    public String getQryResnDec() {
        return this.qryResnDec;
    }

    /**
     * @param qryResnDec
     */
    public void setQryResnDec(String qryResnDec) {
        this.qryResnDec = qryResnDec;
    }

    /**
     * @return authbookNo
     */
    public String getAuthbookNo() {
        return this.authbookNo;
    }

    /**
     * @param authbookNo
     */
    public void setAuthbookNo(String authbookNo) {
        this.authbookNo = authbookNo;
    }

    /**
     * @return authbookContent
     */
    public String getAuthbookContent() {
        return this.authbookContent;
    }

    /**
     * @param authbookContent
     */
    public void setAuthbookContent(String authbookContent) {
        this.authbookContent = authbookContent;
    }

    /**
     * @return otherAuthbookExt
     */
    public String getOtherAuthbookExt() {
        return this.otherAuthbookExt;
    }

    /**
     * @param otherAuthbookExt
     */
    public void setOtherAuthbookExt(String otherAuthbookExt) {
        this.otherAuthbookExt = otherAuthbookExt;
    }

    /**
     * @return authbookDate
     */
    public String getAuthbookDate() {
        return this.authbookDate;
    }

    /**
     * @param authbookDate
     */
    public void setAuthbookDate(String authbookDate) {
        this.authbookDate = authbookDate;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return sendTime
     */
    public String getSendTime() {
        return this.sendTime;
    }

    /**
     * @param sendTime
     */
    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    /**
     * @return approveStatus
     */
    public String getApproveStatus() {
        return this.approveStatus;
    }

    /**
     * @param approveStatus
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    /**
     * @return isSuccssInit
     */
    public String getIsSuccssInit() {
        return this.isSuccssInit;
    }

    /**
     * @param isSuccssInit
     */
    public void setIsSuccssInit(String isSuccssInit) {
        this.isSuccssInit = isSuccssInit;
    }

    /**
     * @return reportNo
     */
    public String getReportNo() {
        return this.reportNo;
    }

    /**
     * @param reportNo
     */
    public void setReportNo(String reportNo) {
        this.reportNo = reportNo;
    }

    /**
     * @return qryStatus
     */
    public String getQryStatus() {
        return this.qryStatus;
    }

    /**
     * @param qryStatus
     */
    public void setQryStatus(String qryStatus) {
        this.qryStatus = qryStatus;
    }

    /**
     * @return creditUrl
     */
    public String getCreditUrl() {
        return this.creditUrl;
    }

    /**
     * @param creditUrl
     */
    public void setCreditUrl(String creditUrl) {
        this.creditUrl = creditUrl;
    }

    /**
     * @return reportCreateTime
     */
    public String getReportCreateTime() {
        return this.reportCreateTime;
    }

    /**
     * @param reportCreateTime
     */
    public void setReportCreateTime(String reportCreateTime) {
        this.reportCreateTime = reportCreateTime;
    }

    /**
     * @return qryFlag
     */
    public String getQryFlag() {
        return this.qryFlag;
    }

    /**
     * @param qryFlag
     */
    public void setQryFlag(String qryFlag) {
        this.qryFlag = qryFlag;
    }

    /**
     * @return imageNo
     */
    public String getImageNo() {
        return this.imageNo;
    }

    /**
     * @param imageNo
     */
    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    /**
     * @return createTime
     */
    public Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }


}