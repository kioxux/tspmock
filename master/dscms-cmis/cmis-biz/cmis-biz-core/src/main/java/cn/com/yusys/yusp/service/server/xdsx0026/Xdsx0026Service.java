package cn.com.yusys.yusp.service.server.xdsx0026;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusHouseInfo;
import cn.com.yusys.yusp.domain.EntlQyInfo;
import cn.com.yusys.yusp.domain.VisaXdRisk;
import cn.com.yusys.yusp.dto.server.xdsx0026.req.ListEntl;
import cn.com.yusys.yusp.dto.server.xdsx0026.req.ListHouse;
import cn.com.yusys.yusp.dto.server.xdsx0026.req.Xdsx0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0026.resp.Xdsx0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CusHouseInfoMapper;
import cn.com.yusys.yusp.repository.mapper.EntlQyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.VisaXdRiskMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdsx0026Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-19 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdsx0026Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdsx0026Service.class);

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号服务

    @Resource
    private VisaXdRiskMapper visaXdRiskMapper;//房抵e点贷面签信息
    @Resource
    private EntlQyInfoMapper entlQyInfoMapper;//房抵e点贷企业经营信息
    @Resource
    private CusHouseInfoMapper cusHouseInfoMapper;//房抵e点贷房产信息

    /**
     * 风控推送面签信息至信贷系统
     *
     * @param xdsx0026DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0026DataRespDto getXdsx0026(Xdsx0026DataReqDto xdsx0026DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, JSON.toJSONString(xdsx0026DataReqDto));
        Xdsx0026DataRespDto xdsx0026DataRespDto = new Xdsx0026DataRespDto();
        try {
            //客户编号
            String cusId = xdsx0026DataReqDto.getCus_id();
            //根据客户编号查询该客户是否存在面签信息
            int num = visaXdRiskMapper.selectCountByCusId(cusId);
            if (num > 0) {
                throw BizException.error(null, EcbEnum.ECB010024.key, EcbEnum.ECB010024.value);
            }
            //TODO 是否存在流水号生成规范 合同面签流水号
//            String serno = UUID.randomUUID().toString().replace("-", "");
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            VisaXdRisk visaXdRisk = new VisaXdRisk();
            visaXdRisk.setSerno(serno);
            visaXdRisk.setCusId(cusId);
            visaXdRisk.setCusName(xdsx0026DataReqDto.getCus_name());
            visaXdRisk.setCertType(toConverCurrency(xdsx0026DataReqDto.getCredit_type()));//证件类型
            visaXdRisk.setCertCode(xdsx0026DataReqDto.getCredit_id());
            visaXdRisk.setMobileNo(xdsx0026DataReqDto.getPhone_no());
            visaXdRisk.setManagerId(xdsx0026DataReqDto.getManager_id());
            visaXdRisk.setManagerName(xdsx0026DataReqDto.getManager_name());
            visaXdRiskMapper.insertSelective(visaXdRisk);

            List<ListEntl> listEntls = xdsx0026DataReqDto.getListEntl();
            if (CollectionUtils.nonEmpty(listEntls)) {
                for (int i = 0; i < listEntls.size(); i++) {
                    ListEntl listEntl = listEntls.get(i);
                    //字段映射
                    EntlQyInfo entlQyInfo = new EntlQyInfo();
                    entlQyInfo.setSerno(listEntl.getApp_no());//流水号
                    entlQyInfo.setCusId(cusId);//客户编号
                    entlQyInfo.setCusName(listEntl.getEnt_name());//客户名称
                    entlQyInfo.setCusArea(listEntl.getEnt_region());//客户所在区域
                    entlQyInfo.setRegCde(listEntl.getEnt_buz_lic_no());//营业执照号
                    entlQyInfo.setOperRange(listEntl.getEnt_buz_scope());//经营范围
                    entlQyInfo.setYearSaleIncome(listEntl.getEnt_annual_sales());//年销售收入
                    entlQyInfo.setCrpStatus("1");//企业状态
                    //查询客户是否存在房抵e点贷企业经营信息
                    Map param = new HashMap<>();
                    param.put("cusId", cusId);
                    param.put("regCde", listEntl.getEnt_buz_lic_no());
                    int sum = entlQyInfoMapper.selectCountByCusId(param);
                    if(sum>0){
                        entlQyInfoMapper.updateByPrimaryKeySelective(entlQyInfo);
                    }else{
                        entlQyInfoMapper.insertSelective(entlQyInfo);
                    }
                    VisaXdRisk visaXdRisk1 = new VisaXdRisk();
                    visaXdRisk1.setSerno(serno);
                    visaXdRisk1.setCrpSerno(listEntl.getApp_no());
                    visaXdRiskMapper.updateByPrimaryKeySelective(visaXdRisk1);
                }
            }

            List<ListHouse> listHouses = xdsx0026DataReqDto.getListHouse();
            List<CusHouseInfo> cusHouseInfos = null;
            if (CollectionUtils.nonEmpty(listHouses)) {
                cusHouseInfos = listHouses.stream().map(e -> {
                    CusHouseInfo cusHouseInfo = new CusHouseInfo();
                    cusHouseInfo.setSignatureSerno(serno);//面签流水号
                    //TODO 流水号生成规范
                    String uuId = UUID.randomUUID().toString().replace("-", "");
                    cusHouseInfo.setSerno(uuId);//流水号
                    cusHouseInfo.setProvinceId(e.getProvince_id());
                    cusHouseInfo.setProvinceName(e.getProvince_name());
                    cusHouseInfo.setProvinceAlias(e.getProvince_alias());
                    cusHouseInfo.setProvinceInterCode(e.getProvince_zip_code());
                    cusHouseInfo.setCityId(e.getCity_id());
                    cusHouseInfo.setCityName(e.getCity_name());
                    cusHouseInfo.setCityAlias(e.getCity_alias());
                    cusHouseInfo.setCityCode(e.getCity_zip_code());
                    cusHouseInfo.setCountyId(e.getArea_id());
                    cusHouseInfo.setCountyName(e.getArea_name());
                    cusHouseInfo.setCountyCode(e.getArea_zip_code());
                    cusHouseInfo.setCommunityId(e.getProject_id());
                    cusHouseInfo.setCommunityName(e.getProject_name());
                    cusHouseInfo.setBuildingId(e.getBuilding_id());
                    cusHouseInfo.setBuildingName(e.getBuilding_name());
                    cusHouseInfo.setFloor(Integer.valueOf(e.getFloor()));
                    cusHouseInfo.setGeneralFloor(Integer.valueOf(e.getTotal_floor()));
                    cusHouseInfo.setRoomNum(e.getHouse_id());
                    cusHouseInfo.setRoomName(e.getHouse_name());
                    cusHouseInfo.setHouseType(e.getHouse_type());
                    cusHouseInfo.setHouseSqu(e.getHouse_area());
                    cusHouseInfo.setHouseLandNo(e.getHouse_right_no());
                    cusHouseInfo.setLocationInfo(e.getHouse_region_info());
                    cusHouseInfo.setLandSqu(e.getAcreage());
                    cusHouseInfo.setIsLease(e.getIs_rent());
                    cusHouseInfo.setEvalType(e.getEval_method());
                    cusHouseInfo.setEvalAmt(e.getEval_value());
                    cusHouseInfo.setPldimnOwner(e.getMortgagee());
                    cusHouseInfo.setBicycleParkingSqu(new BigDecimal(e.getBicycle_garage()));
                    cusHouseInfo.setCarportSqu(new BigDecimal(e.getParking_area()));
                    cusHouseInfo.setAtticSqu(new BigDecimal(e.getLoft_area()));
                    cusHouseInfo.setGuarNo(e.getGuaranty_id());
                    cusHouseInfo.setBicycleParkingAmt(new BigDecimal(e.getBicycle_garage_eval()));
                    cusHouseInfo.setCarportAmt(new BigDecimal(e.getPark_space_eval()));
                    cusHouseInfo.setAtticAmt(new BigDecimal(e.getLoft_eval()));
                    cusHouseInfo.setEvalTotalAmt(new BigDecimal(e.getTotal_value()));
                    return cusHouseInfo;
                }).collect(Collectors.toList());
            }
            //批量插入房抵e点贷房产信息
            cusHouseInfoMapper.insertCusHouseInfos(cusHouseInfos);
            xdsx0026DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);
            xdsx0026DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);
        } catch (BizException e) {
            xdsx0026DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdsx0026DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdsx0026DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdsx0026DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, e.getMessage());
            throw new Exception(EcbEnum.ECB019999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, JSON.toJSONString(xdsx0026DataRespDto));
        return xdsx0026DataRespDto;

    }


    /*
     * 证件映射 风控--->信贷
     * @param  风控证件码值
     * @return  信贷证件码值
     *
     */
    public static String toConverCurrency(String fkbz) {
        String xdbz = "";
        if ("10".equals(fkbz)) { //身份证
            xdbz = "A";
        } else if ("11".equals(fkbz)) { //户口簿
            xdbz = "C";
        } else if ("12".equals(fkbz)) {//护照
            xdbz = "B";
        } else if ("15".equals(fkbz)) {//港澳居民来往内地通行证
            xdbz = "D";
        } else if ("16".equals(fkbz)) {//台湾同胞来往内地通行证
            xdbz = "E";
        } else if ("18".equals(fkbz)) { // 外国人居留证
            xdbz = "S";
        } else if ("19".equals(fkbz)) {//警官证
            xdbz = "Y";
        } else if ("21".equals(fkbz)) {//港、澳、台身份证
            xdbz = "W";
        } else if ("1X".equals(fkbz)) {//其他证件
            xdbz = "P";
        } else if ("17".equals(fkbz)) {//临时身份证
            xdbz = "X";
        } else if ("13".equals(fkbz)) {//军官证
            xdbz = "G";
        } else if ("14".equals(fkbz)) {//士兵证
            xdbz = "H";
        } else if ("20".equals(fkbz)) {//组织机构代码
            xdbz = "Q";
        } else if ("22".equals(fkbz)) {//境外企业代码
            xdbz = "V";
        } else if ("24".equals(fkbz)) {//营业执照
            xdbz = "M";
        } else {
            xdbz = xdbz;//未匹配到的证件类型
        }
        return xdbz;
    }
}
