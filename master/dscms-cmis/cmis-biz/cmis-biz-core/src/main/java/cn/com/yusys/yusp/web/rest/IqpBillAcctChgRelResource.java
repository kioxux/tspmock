/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpBillAcctChgRel;
import cn.com.yusys.yusp.service.IqpBillAcctChgRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpBillAcctChgRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-01-25 09:09:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpbillacctchgrel")
public class IqpBillAcctChgRelResource {
    @Autowired
    private IqpBillAcctChgRelService iqpBillAcctChgRelService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpBillAcctChgRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpBillAcctChgRel> list = iqpBillAcctChgRelService.selectAll(queryModel);
        return new ResultDto<List<IqpBillAcctChgRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpBillAcctChgRel>> index(QueryModel queryModel) {
        List<IqpBillAcctChgRel> list = iqpBillAcctChgRelService.selectByModel(queryModel);
        return new ResultDto<List<IqpBillAcctChgRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpBillAcctChgRel> show(@PathVariable("pkId") String pkId) {
        IqpBillAcctChgRel iqpBillAcctChgRel = iqpBillAcctChgRelService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpBillAcctChgRel>(iqpBillAcctChgRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpBillAcctChgRel> create(@RequestBody IqpBillAcctChgRel iqpBillAcctChgRel) throws URISyntaxException {
        iqpBillAcctChgRelService.insert(iqpBillAcctChgRel);
        return new ResultDto<IqpBillAcctChgRel>(iqpBillAcctChgRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpBillAcctChgRel iqpBillAcctChgRel) throws URISyntaxException {
        int result = iqpBillAcctChgRelService.update(iqpBillAcctChgRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpBillAcctChgRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpBillAcctChgRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
