/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.req.CmisLmt0056ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.resp.CmisLmt0056RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.IqpTfLocAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizUtils;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpTfLocAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-12 15:07:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpTfLocAppService {

    private static final Logger log = LoggerFactory.getLogger(IqpTfLocAppService.class);

    @Autowired
    private IqpTfLocAppMapper iqpTfLocAppMapper;

    @Autowired
    private CtrTfLocContService ctrTfLocContService;//开证合同主表
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private GrtGuarContService grtGuarContService;//担保合同表

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private BailAccInfoService bailAccInfoService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private ImgCondDetailsService imgCondDetailsService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private ICusClientService icusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpTfLocApp selectByPrimaryKey(String pkId) {
        return iqpTfLocAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpTfLocApp> selectAll(QueryModel model) {
        List<IqpTfLocApp> records = (List<IqpTfLocApp>) iqpTfLocAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpTfLocApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpTfLocApp> list = iqpTfLocAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpTfLocApp record) {
        return iqpTfLocAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpTfLocApp record) {
        return iqpTfLocAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpTfLocApp record) {
        return iqpTfLocAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional
    public int updateSelective(IqpTfLocApp record) {
        return iqpTfLocAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpTfLocAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpTfLocAppMapper.deleteByIds(ids);
    }

    /**
     * 开证合同申请新增页面点击下一步
     * @param iqpTfLocApp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIqpTfLocAppInfo(IqpTfLocApp iqpTfLocApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (iqpTfLocApp == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            String cusId = iqpTfLocApp.getCusId();
            // 将 地址与联系方式 信息更新值贷款申请表中
            // 调用 对公客户基本信息查询接口，
            // 将 地址与联系方式 信息更新值贷款申请表中
            log.info("通过客户编号：【{}】，查询客户信息开始",cusId);
            CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(cusId);
            log.info("通过客户编号：【{}】，查询客户信息结束，响应报文为：【{}】",cusId, cusBaseDtoResultDto.toString());
            // 个人客户
            if(CmisCusConstants.STD_ZB_CUS_CATALOG_1.equals(cusBaseDtoResultDto.getCusCatalog())){
                CusIndivContactDto CusIndivAllDto = icusClientService.queryCusIndivByCusId(cusId);
                if (CusIndivAllDto != null && !"".equals(CusIndivAllDto.getCusId()) && CusIndivAllDto.getCusId() != null) {
                    iqpTfLocApp.setPhone(CusIndivAllDto.getMobile());
                    iqpTfLocApp.setFax(CusIndivAllDto.getFaxCode());
                    iqpTfLocApp.setEmail(CusIndivAllDto.getEmail());
                    iqpTfLocApp.setQq(CusIndivAllDto.getQq());
                    iqpTfLocApp.setWechat(CusIndivAllDto.getWechatNo());
                    iqpTfLocApp.setDeliveryAddr(CusIndivAllDto.getDeliveryStreet());
                }
            }else{
                CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cusId).getData();
                if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
                    iqpTfLocApp.setLinkman(cusCorpDto.getFreqLinkman());
                    iqpTfLocApp.setPhone(cusCorpDto.getFreqLinkmanTel());
                    iqpTfLocApp.setFax(cusCorpDto.getFax());
                    iqpTfLocApp.setEmail(cusCorpDto.getLinkmanEmail());
                    iqpTfLocApp.setQq(cusCorpDto.getQq());
                    iqpTfLocApp.setWechat(cusCorpDto.getWechatNo());
                    iqpTfLocApp.setDeliveryAddr(cusCorpDto.getSendAddr());
                }
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpTfLocApp.setInputId(userInfo.getLoginCode());
                iqpTfLocApp.setInputBrId(userInfo.getOrg().getCode());
                iqpTfLocApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            }

            Map seqMap = new HashMap();
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, seqMap);
            HashMap<String, String> param = new HashMap<>();
            String dkSeq = iqpHighAmtAgrAppService.getSuitableContNo(userInfo.getOrg().getCode(),CmisCommonConstants.STD_BUSI_TYPE_06);
            String contNo = sequenceTemplateClient.getSequenceTemplate(dkSeq, param);

            iqpTfLocApp.setSerno(serno);
            iqpTfLocApp.setContNo(contNo);
            iqpTfLocApp.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);
            iqpTfLocApp.setPkId(StringUtils.uuid(true));
            iqpTfLocApp.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_06);
            iqpTfLocApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            iqpTfLocApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 通过授信额度编号查询批复流水号
            if(StringUtils.nonBlank(iqpTfLocApp.getLmtAccNo())){
                LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(iqpTfLocApp.getLmtAccNo());
                if(Objects.isNull(lmtReplyAccSubPrd)){
                    log.error("通过产品额度编号【{}】未查询到产品额度信息",iqpTfLocApp.getLmtAccNo());
                    throw BizException.error(null, EcbEnum.ECB020068.key, EcbEnum.ECB020068.value);
                }
                log.info("分项额度编号为【{}】",lmtReplyAccSubPrd.getAccSubNo());
                Map map = new HashMap();
                map.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
                if(StringUtils.isBlank(lmtReplyAccSub.getReplySerno())){
                    log.error("通过分项额度编号【{}】未查询到分项额度信息",lmtReplyAccSubPrd.getAccSubNo());
                    throw BizException.error(null, EcbEnum.ECB020069.key, EcbEnum.ECB020069.value);
                }
                log.info("批复流水号为【{}】",lmtReplyAccSub.getReplySerno());
                iqpTfLocApp.setReplyNo(lmtReplyAccSub.getReplySerno());
            }
            int insertCount = iqpTfLocAppMapper.insertSelective(iqpTfLocApp);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("serno",serno);
            result.put("contNo",contNo);
            log.info("开证合同申请"+serno+"-保存成功！");

            //根据选择的额度分项自动生成担保合同
            IqpTfLocApp iqpTfLocAppData = iqpTfLocAppMapper.selectByIqpSerno(serno);
            if (!CmisCommonConstants.GUAR_MODE_00.equals(iqpTfLocAppData.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_21.equals(iqpTfLocAppData.getGuarMode()) &&
                    !CmisCommonConstants.GUAR_MODE_40.equals(iqpTfLocAppData.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_60.equals(iqpTfLocAppData.getGuarMode())) {
                String subSerno = "";
                if(iqpTfLocAppData.getLmtAccNo()==null||"".equals(iqpTfLocAppData.getLmtAccNo())){
                    subSerno = "";
                }else{
                    subSerno = lmtReplyAccSubPrdService.getLmtReplyAccSubDataByAccSubPrdNo(iqpTfLocAppData.getLmtAccNo());//GUAR_BIZ_REL
                }
                GrtGuarContDto grtGuarContDto = new GrtGuarContDto();
                grtGuarContDto.setSerno(iqpTfLocAppData.getSerno());//业务流水号
                grtGuarContDto.setCusId(iqpTfLocAppData.getCusId());//借款人编号
                grtGuarContDto.setBizLine(iqpTfLocAppData.getBelgLine());//业务条线
                grtGuarContDto.setGuarWay(iqpTfLocAppData.getGuarMode());//担保方式   //10抵押 20 质押 30保证
                grtGuarContDto.setIsUnderLmt(CmisCommonConstants.STD_ZB_YES_NO_1);
                grtGuarContDto.setGuarAmt(iqpTfLocAppData.getContAmt());//担保金额
                grtGuarContDto.setGuarTerm(iqpTfLocAppData.getContTerm());//担保期限
                grtGuarContDto.setGuarStartDate(iqpTfLocAppData.getStartDate());//担保起始日
                grtGuarContDto.setGuarEndDate(iqpTfLocAppData.getEndDate());//担保终止日
                grtGuarContDto.setReplyNo(iqpTfLocAppData.getReplyNo());//批复编号
                grtGuarContDto.setLmtAccNo(iqpTfLocAppData.getLmtAccNo());//授信额度编号
                grtGuarContDto.setSubSerno(subSerno);//授信分项流水号
                grtGuarContDto.setCusName(iqpTfLocAppData.getCusName());//借款人名称
                grtGuarContDto.setGuarContType(iqpTfLocAppData.getContType());//担保合同类型
                grtGuarContDto.setInputId(iqpTfLocAppData.getInputId());//登记人
                grtGuarContDto.setInputBrId(iqpTfLocAppData.getInputBrId());//登记机构
                grtGuarContDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                grtGuarContService.lmtAutoCreateGrtGuarCont(grtGuarContDto, contNo);
            }

            // 生成用信条件落实情况数据
            Map queryMap = new HashMap();
            queryMap.put("replySerno",iqpTfLocAppData.getReplyNo());
            queryMap.put("contNo",iqpTfLocAppData.getContNo());
            int insertCountData = imgCondDetailsService.generateImgCondDetailsData(queryMap);
//            if(insertCountData<=0){
//                throw BizException.error(null, EcbEnum.ECB020029.key, EcbEnum.ECB020029.value);
//            }
            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            creditReportQryLstAndRealDto.setCusId(cusId);
            creditReportQryLstAndRealDto.setCusName(iqpTfLocApp.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(serno);
            creditReportQryLstAndRealDto.setScene("02");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        }catch(YuspException e){
            log.error("开证合同申请新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("保存开证合同申请异常！",e.getMessage());
            throw new YuspException(EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key, EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value);
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }


    /**
     * 开证申请提交保存方法
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveIqpTfLocAppInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "开证申请" + serno;

            log.info(logPrefix + "获取申请数据");
            IqpTfLocApp iqpTfLocApp = JSONObject.parseObject(JSON.toJSONString(params), IqpTfLocApp.class);
            if (iqpTfLocApp == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存开证申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpTfLocApp.setUpdId(userInfo.getLoginCode());
                iqpTfLocApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpTfLocApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(logPrefix + "保存开证申请数据");
            int updCount = iqpTfLocAppMapper.updateByPrimaryKeySelective(iqpTfLocApp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存开证申请" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取开证合同申请" + iqpSerno + "申请主表信息");
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppMapper.selectByTfLocSernoKey(iqpSerno);
            if (iqpTfLocApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            int updateCount = 0;
            log.info("流程发起-更新开证合同申请" + iqpSerno + "流程审批状态为【111】-审批中");
            updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_111);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            //向额度系统发送接口,占用额度
            this.sendToLmt(iqpTfLocApp);
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("开证业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    public void sendToLmt(IqpTfLocApp iqpTfLocApp) {
        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpTfLocApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpTfLocApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }

        //向额度系统发送接口,占用额度
        String guarMode = iqpTfLocApp.getGuarMode();
        // 折算金额和保证金比例
        BigDecimal contHighAmt = iqpTfLocApp.getContHighAvlAmt();
        BigDecimal cvtCnyAmt = iqpTfLocApp.getCvtCnyAmt();
        BigDecimal bailPerc = iqpTfLocApp.getBailPerc();
        // 保证金金额和实际占额金额
        BigDecimal bailAmt = BigDecimal.ZERO;
        BigDecimal lmtAmt = BigDecimal.ZERO;
        BigDecimal lmtAmtCny = BigDecimal.ZERO;
        //不是低风险业务
        if (!CmisCommonConstants.GUAR_MODE_60.equals(guarMode) && !CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                && !CmisCommonConstants.GUAR_MODE_40.equals(guarMode)){
            bailAmt = bailPerc.multiply(contHighAmt.divide(BigDecimal.ONE.subtract(bailPerc),2,BigDecimal.ROUND_HALF_UP));
            lmtAmtCny = cvtCnyAmt.add(bailAmt);// 折算人民币
            lmtAmt = contHighAmt.add(bailAmt);// 原币种
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpTfLocApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpTfLocApp.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpTfLocApp.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpTfLocApp.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpTfLocApp.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpTfLocApp.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpTfLocApp.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpTfLocApp.getCvtCnyAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(iqpTfLocApp.getBailPerc());//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(bailAmt);//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpTfLocApp.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpTfLocApp.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpTfLocApp.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpTfLocApp.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpTfLocApp.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpTfLocApp.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(lmtAmt);//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(contHighAmt);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(lmtAmtCny);//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(cvtCnyAmt);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("开证合同业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpTfLocApp.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("开证合同业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpTfLocApp.getSerno(), JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }

        //是低风险且授信不足额的占额
        if ((CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode) ||
        CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) && iqpTfLocApp.getLmtAccNo()!=null){
            bailAmt = bailPerc.multiply(contHighAmt);
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpTfLocApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpTfLocApp.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpTfLocApp.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpTfLocApp.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpTfLocApp.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpTfLocApp.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpTfLocApp.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpTfLocApp.getCvtCnyAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(iqpTfLocApp.getBailPerc());//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(bailAmt);//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpTfLocApp.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpTfLocApp.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpTfLocApp.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpTfLocApp.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpTfLocApp.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpTfLocApp.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(contHighAmt);//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(cvtCnyAmt);//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("开证合同业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpTfLocApp.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("开证合同业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpTfLocApp.getSerno(), JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }
    }

    @Transactional
    public int updateApproveStatus(String iqpSerno, String approveStatus) {
        return iqpTfLocAppMapper.updateApproveStatus(iqpSerno, approveStatus);
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【打回】，业务与担保合同关系结果表数据更新为【打回】
     *
     * 放款申请打回后，仅将当前申请状态变更为“打回”，该笔业务的贷款合同及合同与担保合同关系结果不变，若需作废合同，则人工在合同管理模块将该合同作废。
     * 2、更新申请主表的审批状态为998 打回
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterCallBack(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请打回流程-获取放款申请" + iqpSerno + "申请信息");
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppMapper.selectByTfLocSernoKey(iqpSerno);

            if (iqpTfLocApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = iqpTfLocAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_992);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + iqpSerno + "流程打回业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【拒绝】，业务与担保合同关系结果表数据更新为【拒绝】
     * 2、更新申请主表的审批状态为998 【拒绝】
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterRefuse(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("开证合同申请否决流程-获取开证合同申请" + iqpSerno + "申请信息");
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppMapper.selectByTfLocSernoKey(iqpSerno);
            if (iqpTfLocApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_998);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            // 折算金额和保证金比例
            BigDecimal contHighAmt = iqpTfLocApp.getContHighAvlAmt();
            BigDecimal cvtCnyAmt = iqpTfLocApp.getCvtCnyAmt();
            BigDecimal bailPerc = iqpTfLocApp.getBailPerc();
            // 保证金金额和实际占额金额
            BigDecimal bailAmt = BigDecimal.ZERO;
            if (iqpTfLocApp.getLmtAccNo() != null && !"".equals(iqpTfLocApp.getLmtAccNo())) {
                String guarMode = iqpTfLocApp.getGuarMode();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    bailAmt = contHighAmt.multiply(bailPerc);
                    recoverAmtCny = cvtCnyAmt;
                } else {
                    recoverSpacAmtCny = cvtCnyAmt;
                    bailAmt = bailPerc.multiply(contHighAmt.divide(BigDecimal.ONE.subtract(bailPerc),2,BigDecimal.ROUND_HALF_UP));
                    recoverAmtCny = cvtCnyAmt.add(bailAmt);
                }

                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpTfLocApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpTfLocApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpTfLocApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpTfLocApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                log.info("开证合同申请【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("开证合同申请【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", iqpSerno, JSON.toJSONString(resultDto));
                if(!"0".equals(resultDto.getCode())){
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,code, resultDto.getData().getErrorCode());
                }
            }
        } catch (BizException e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new BizException(null,e.getErrorCode(),null,e.getMessage());
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String serno) throws Exception{
        if (StringUtils.isBlank(serno)) {
            throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("审批通过-获取开证合同申请" + serno + "申请主表信息");
        IqpTfLocApp iqpTfLocApp = iqpTfLocAppMapper.selectByTfLocSernoKey(serno);
        if (iqpTfLocApp == null) {
            throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }
        //判断是否需要反向生成低风险分项，以及后续的逻辑处理
        if(iqpTfLocApp.getLmtAccNo()==null||"".equals(iqpTfLocApp.getLmtAccNo())){
            boolean result = lmtReplyAccService.generaLmtReplyAccForLowRisk(iqpTfLocApp.getContAmt(), iqpTfLocApp.getCusId(), iqpTfLocApp.getBusiType(), iqpTfLocApp.getBailPerc(), iqpTfLocApp.getChrgRate(),iqpTfLocApp.getEndDate(),CmisCommonConstants.STD_ZB_YES_NO_0);
            if(!result){
                log.error("开证合同申请: "+serno+"反向生成低风险额度分项异常");
                throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            String replySerno = "";
            String lmtAccNo = "";
            log.info("开证合同申请: "+serno+"获取批复编号和授信额度编号开始");
            // 根据客户号以及产品编号查询生成的分项下对应的低风险分项明细
            CmisLmt0056ReqDto cmisLmt0056ReqDto = new CmisLmt0056ReqDto();
            cmisLmt0056ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpTfLocApp.getManagerBrId()));
            cmisLmt0056ReqDto.setPrdId(iqpTfLocApp.getPrdId());
            cmisLmt0056ReqDto.setCusId(iqpTfLocApp.getCusId());
            log.info("开证合同业务申请【{}】，前往额度系统查询低风险分项明细,请求报文为:【{}】", serno, JSON.toJSONString(cmisLmt0056ReqDto));
            ResultDto<CmisLmt0056RespDto> resultDto = cmisLmtClientService.cmislmt0056(cmisLmt0056ReqDto);
            log.info("开证合同业务申请【{}】，前往额度系统查询低风险分项明细,响应报文为:【{}】", serno, JSON.toJSONString(resultDto));
            if(!"0".equals(resultDto.getCode())){
                log.error("额度0056接口调用异常！");
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            if(!"0000".equals(resultDto.getData().getErrorCode())){
                log.error("查询低风险分项明细失败！异常信息为【{}】",resultDto.getData().getErrorMsg());
                throw BizException.error(null,resultDto.getData().getErrorCode(),resultDto.getData().getErrorMsg());
            }
            lmtAccNo = resultDto.getData().getApprSubSerno();
            log.info("开证合同申请【{}】的授信额度编号为【{}】",serno,lmtAccNo);
            // 根据授信额度编号获取批复流水号
            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(lmtAccNo);
            if(Objects.isNull(lmtReplyAccSubPrd)){
                log.error("根据授信额度编号【{}】查询授信产品明细为空！",lmtAccNo);
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }

            Map map = new HashMap();
            map.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
            LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
            if(Objects.isNull(lmtReplyAccSub)){
                log.error("根据分项额度编号【{}】查询授信分项明细为空！",lmtReplyAccSubPrd.getAccSubNo());
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            replySerno = lmtReplyAccSub.getReplySerno();
            log.info("开证合同申请【{}】的批复编号为【{}】",serno,replySerno);
            //生成低风险分项后，更新申请数据
            int updateCount = 0;
            log.info("更新开证业务申请" + serno + "开始");
            iqpTfLocApp.setLmtAccNo(lmtAccNo);
            iqpTfLocApp.setReplyNo(replySerno);
            log.info("批复编号为【{}】,授信额度编号为【{}】",lmtAccNo,replySerno);
            updateCount = this.updateSelective(iqpTfLocApp);
            if (updateCount < 0) {
                log.error("更新开证业务申请【{}】异常",serno);
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            log.info("更新开证业务申请" + serno + "结束");
        }

        //占额成功后，更新审批状态
        int updateCount = 0;
        log.info("审批通过-更新开证业务申请" + serno + "流程审批状态为【997】-通过");
        iqpTfLocApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        updateCount = iqpTfLocAppMapper.updateByPrimaryKeySelective(iqpTfLocApp);
        if (updateCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }

        IqpTfLocApp iqpTfLocAppObj = iqpTfLocAppMapper.selectByTfLocSernoKey(serno);
        //审批通过以后，生成合同数据
        log.info("审批通过生成开证合同" + serno + "开始");
        CtrTfLocCont ctrTfLocCont = new CtrTfLocCont();
        BeanUtils.copyProperties(iqpTfLocAppObj, ctrTfLocCont);
        ctrTfLocCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);
        ctrTfLocCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_0);
        int insertCount = ctrTfLocContService.insertSelective(ctrTfLocCont);
        if (insertCount < 0) {
            throw BizException.error(null, EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.key, EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.value);
        }
        // 判断是否续签合同，如果是续签合同则原合同置中止
        if(Objects.equals("1",iqpTfLocApp.getIsRenew())) {
            // 获取原合同
            String origiContNo = iqpTfLocApp.getOrigiContNo();
            if(StringUtils.nonBlank(origiContNo)) {
                CtrTfLocCont origiCtrTfLocCont = ctrTfLocContService.selectByContNo(origiContNo);
                if(Objects.nonNull(origiCtrTfLocCont)) {
                    origiCtrTfLocCont.setContStatus(CmisBizConstants.IQP_CONT_STS_500);
                    updateCount = ctrTfLocContService.updateSelective(origiCtrTfLocCont);
                    if (updateCount < 1) {
                        throw new YuspException(EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.key, EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.value);
                    }
                }
            }
        }

        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpTfLocApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpTfLocApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        //低风险足额的占额处理
        String guarMode = iqpTfLocAppObj.getGuarMode();
        //判断是否是低风险的一步流程，是：进行低风险占额后再按照正常流程走；否：直接按照正常流程走
        if((CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode) ||
                CmisCommonConstants.GUAR_MODE_40.equals(guarMode))) {

            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpTfLocApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpTfLocAppObj.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpTfLocAppObj.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpTfLocAppObj.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpTfLocAppObj.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpTfLocAppObj.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpTfLocAppObj.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpTfLocAppObj.getCvtCnyAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(iqpTfLocAppObj.getBailPerc());//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(iqpTfLocAppObj.getContHighAvlAmt().multiply(iqpTfLocAppObj.getBailPerc()));//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpTfLocAppObj.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpTfLocAppObj.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpTfLocAppObj.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpTfLocAppObj.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpTfLocAppObj.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpTfLocAppObj.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpTfLocAppObj.getContHighAvlAmt());//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpTfLocAppObj.getCvtCnyAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("开证合同业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", serno, JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("开证合同业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", serno, JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }
        log.info("审批通过生成开证合同" + serno + "结束");
    }
    /**
     * 获取基本信息
     *
     * @param iqpSerno
     * @return
     */
    public IqpTfLocApp selectByTfLocSernoKey(String iqpSerno) {
        return iqpTfLocAppMapper.selectByTfLocSernoKey(iqpSerno);
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpTfLocApp> toSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("busiType",CmisCommonConstants.STD_BUSI_TYPE_06);
        return iqpTfLocAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpTfLocApp> doneSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("busiType",CmisCommonConstants.STD_BUSI_TYPE_06);
        return iqpTfLocAppMapper.selectByModel(model);
    }

    /**
     * @方法名称：selectForLmtAccNo
     * @方法描述：查授信台账号对应的用信申请
     * @创建人：zhangming12
     * @创建时间：2021/5/17 21:19
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<IqpTfLocApp> selectForLmtAccNo(String lmtAccNo){
        return iqpTfLocAppMapper.selectByLmtAccNo(lmtAccNo);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(IqpTfLocApp iqpTfLocApp) {
        if(CmisCommonConstants.WF_STATUS_000.equals(iqpTfLocApp.getApproveStatus())){
            //删除关联的担保合同及担保合同与押品关系表
            bizCommonService.logicDeleteGrtGuarCont(iqpTfLocApp.getSerno());

            iqpTfLocApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            iqpTfLocAppMapper.updateByPrimaryKeySelective(iqpTfLocApp);
        }else if(CmisCommonConstants.WF_STATUS_992.equals(iqpTfLocApp.getApproveStatus())){
            iqpTfLocApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
            iqpTfLocAppMapper.updateByPrimaryKeySelective(iqpTfLocApp);
            ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(iqpTfLocApp.getSerno());
            // 折算金额和保证金比例
            BigDecimal contHighAmt = iqpTfLocApp.getContHighAvlAmt();
            BigDecimal cvtCnyAmt = iqpTfLocApp.getCvtCnyAmt();
            BigDecimal bailPerc = iqpTfLocApp.getBailPerc();
            // 保证金金额和实际占额金额
            BigDecimal bailAmtCny = BigDecimal.ZERO;
            if (iqpTfLocApp.getLmtAccNo() != null && !"".equals(iqpTfLocApp.getLmtAccNo())) {
                String guarMode = iqpTfLocApp.getGuarMode();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    bailAmtCny = contHighAmt.multiply(bailPerc);
                    recoverAmtCny = cvtCnyAmt;
                } else {
                    recoverSpacAmtCny = cvtCnyAmt;
                    bailAmtCny = bailPerc.multiply(contHighAmt.divide(BigDecimal.ONE.subtract(bailPerc),2,BigDecimal.ROUND_HALF_UP));
                    recoverAmtCny = cvtCnyAmt.add(bailAmtCny);
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpTfLocApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpTfLocApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpTfLocApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpTfLocApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                log.info("开证合同申请【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", iqpTfLocApp.getSerno(), JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("开证合同申请【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", iqpTfLocApp.getSerno(), JSON.toJSONString(resultDto));
                if(!"0".equals(resultDto.getCode())){
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,code, resultDto.getData().getErrorCode());
                }
            }
        }
        return iqpTfLocAppMapper.updateByPrimaryKey(iqpTfLocApp);
    }
    /**
     * @方法名称: selectByIqpSerno
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public IqpTfLocApp selectByIqpSerno(String serno) {
        return iqpTfLocAppMapper.selectByIqpSerno(serno);
    }

    /**
     * @方法名称: checkLmtAmtIsEnough
     * @方法描述: 开证合同申请判断是否足额
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String checkLmtAmtIsEnough(IqpTfLocApp iqpTfLocApp) {
        String result = CmisCommonConstants.STD_ZB_YES_NO_1;
        log.info("开证合同申请判断是否足额,流水号【{}】", iqpTfLocApp.getSerno());
        // 组装额度报文
        CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
        cmisLmt0026ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpTfLocApp.getManagerBrId()));//金融机构代码
        cmisLmt0026ReqDto.setSubSerno(iqpTfLocApp.getLmtAccNo());//分项编号
        cmisLmt0026ReqDto.setQueryType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//分项类型
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度", iqpTfLocApp.getLmtAccNo());
        CmisLmt0026RespDto cmisLmt0026RespDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto).getData();
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度返回报文：" + iqpTfLocApp.getLmtAccNo());
        BigDecimal avlAvailAmt = new BigDecimal("0.0");
        if(cmisLmt0026RespDto.getAvlAvailAmt() != null){
            avlAvailAmt = cmisLmt0026RespDto.getAvlAvailAmt();
        }
        if(cmisLmt0026RespDto != null && iqpTfLocApp.getCvtCnyAmt().compareTo(avlAvailAmt) > 0 ){
            // 如果是申请额度大于授信总额可用
            result = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        return result;
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 开证合同申请流程参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        log.info("开证合同申请更新流程参数,流水号{}", serno);
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        IqpTfLocApp iqpTfLocApp = this.selectByIqpSerno(serno);
        Map<String, Object> params = new HashMap<>();
        if(CmisCommonConstants.GUAR_MODE_60.equals(iqpTfLocApp.getGuarMode())
                || CmisCommonConstants.GUAR_MODE_21.equals(iqpTfLocApp.getGuarMode())
                || CmisCommonConstants.GUAR_MODE_40.equals(iqpTfLocApp.getGuarMode())){
            // 是否低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_1);
            // 单户低风险总额
            params.put("lowRiskAmtTotal", iqpTfLocApp.getCvtCnyAmt().add(iqpHighAmtAgrAppService.queryCusLowRiskUseAmt(iqpTfLocApp.getCusId(), iqpTfLocApp.getManagerBrId())));
        }else{
            // 是否低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 单户低风险总额
            params.put("lowRiskAmtTotal", new BigDecimal("0"));
        }

        // 需要反向生成低风险分项时，额度确定足额
        if(iqpTfLocApp.getLmtAccNo()!=null){
            params.put("isAmtEnough", checkLmtAmtIsEnough(iqpTfLocApp));
        }else {
            params.put("isAmtEnough", CmisCommonConstants.STD_ZB_YES_NO_0);
        }
        // 申请金额
        params.put("amt", iqpTfLocApp.getCvtCnyAmt());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }
    /**
     * @方法名称: riskItem0039
     * @方法描述: 保证金账户录入校验
     * @参数与返回说明:
     * @算法描述:若保证金比例大于0时，如果没有录入保证金账户，则拦截
     * @创建人: yfs
     * @创建时间: 2021-07-13 10:47:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Object riskItem0039(QueryModel queryModel) {
        String serno = (String) queryModel.getCondition().get("bizId");
        String bizType = (String) queryModel.getCondition().get("bizType");
        log.info("保证金账户录入校验开始*******************业务流水号：【{}】,流程编号：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 银承合同申请
        if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX006, bizType)){
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey(serno);
            if(Objects.isNull(iqpAccpApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03902);
                return riskResultDto;
            }
            if(new BigDecimal("0").compareTo(iqpAccpApp.getBailPerc()) < 0) {
                List<BailAccInfo> list = bailAccInfoService.selectBySerno(serno);
                if (CollectionUtils.isEmpty(list) || StringUtils.isBlank(list.get(0).getBailAccNo())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03901);
                    return riskResultDto;
                }
            }
        }
        // 保函合同申请
        if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX007, bizType)){
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectByCvrgSernoKey(serno);
            if(Objects.isNull(iqpCvrgApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03902);
                return riskResultDto;
            }
            if(new BigDecimal("0").compareTo(iqpCvrgApp.getBailPerc()) < 0) {
                List<BailAccInfo> list = bailAccInfoService.selectBySerno(serno);
                if (CollectionUtils.isEmpty(list) || StringUtils.isBlank(list.get(0).getBailAccNo())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03901);
                    return riskResultDto;
                }
            }
        }

        // 开证合同申请
        if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX005, bizType)){
            IqpTfLocApp iqpTfLocApp = selectByTfLocSernoKey(serno);
            if(Objects.isNull(iqpTfLocApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03902);
                return riskResultDto;
            }
            if(new BigDecimal("0").compareTo(iqpTfLocApp.getBailPerc()) < 0) {
                List<BailAccInfo> list = bailAccInfoService.selectBySerno(serno);
                if (CollectionUtils.isEmpty(list) || StringUtils.isBlank(list.get(0).getBailAccNo())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03901);
                    return riskResultDto;
                }
            }
        }
        log.info("保证金账户录入校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: sendOnlinePldRemind
     * @方法描述: 首页消息提醒
     * @参数与返回说明:
     * @创建者：qw
     * @算法描述: 无
     */
    public void sendOnlinePldRemind(ResultInstanceDto resultInstanceDto, String serno) throws Exception {
        log.info("业务申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
        //针对流程到办结节点，进行以下处理
        //首页消息提醒
        IqpTfLocApp iqpTfLocApp = this.selectByIqpSerno(serno);
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpTfLocApp.getIsOlPld())){
            String managerId = iqpTfLocApp.getManagerId();
            String managerBrId = iqpTfLocApp.getManagerBrId();
            if (StringUtil.isNotEmpty(managerId)) {
                try {
                    //执行发送借款人操作
                    String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                    String messageType = "MSG_DG_M_0001";//短信编号
                    // 翻译管护经理名称和管护经理机构名称
                    ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(managerId);
                    if (!"0".equals(adminSmUserDtoResultDto.getCode())) {
                        log.error("业务申请: " + serno + " 查询用户数据异常");
                        throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                    }
                    String managerName = adminSmUserDtoResultDto.getData().getUserName();
                    ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerBrId);
                    if (!"0".equals(adminSmOrgDtoResultDto.getCode())) {
                        log.error("业务申请: " + serno + " 查询用户数据异常");
                        throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                    }
                    String managerBrName = adminSmOrgDtoResultDto.getData().getOrgName();
                    //短信填充参数
                    Map paramMap = new HashMap();
                    paramMap.put("cusName", iqpTfLocApp.getCusName());
                    paramMap.put("managerId", managerName);
                    paramMap.put("managerBrId", managerBrName);
                    paramMap.put("instanceId", resultInstanceDto.getInstanceId());
                    paramMap.put("nodeSign", resultInstanceDto.getNodeSign());
                    //执行发送客户经理操作
                    messageCommonService.sendonlinepldremind(messageType, receivedUserType, paramMap);
                } catch (Exception e) {
                    throw new Exception("发送短信失败！");
                }
            }
        }
    }

}
