/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpLoanExtApp;
import cn.com.yusys.yusp.service.IqpLoanExtAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanExtAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-11 16:25:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqploanextapp")
public class IqpLoanExtAppResource {
    private final Logger log = LoggerFactory.getLogger(IqpLoanExtAppResource.class);
    @Autowired
    private IqpLoanExtAppService iqpLoanExtAppService;

	/**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpLoanExtApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpLoanExtApp> list = iqpLoanExtAppService.selectAll(queryModel);
        return new ResultDto<List<IqpLoanExtApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpLoanExtApp>> index(QueryModel queryModel) {
        List<IqpLoanExtApp> list = iqpLoanExtAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanExtApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{extSerno}")
    protected ResultDto<IqpLoanExtApp> show(@PathVariable("extSerno") String extSerno) {
        IqpLoanExtApp iqpLoanExtApp = iqpLoanExtAppService.selectByPrimaryKey(extSerno);
        return new ResultDto<IqpLoanExtApp>(iqpLoanExtApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpLoanExtApp> create(@RequestBody IqpLoanExtApp iqpLoanExtApp) throws URISyntaxException {
        iqpLoanExtAppService.insert(iqpLoanExtApp);
        return new ResultDto<IqpLoanExtApp>(iqpLoanExtApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpLoanExtApp iqpLoanExtApp) throws URISyntaxException {
        int result = iqpLoanExtAppService.update(iqpLoanExtApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{extSerno}")
    protected ResultDto<Integer> delete(@PathVariable("extSerno") String extSerno) {
        int result = iqpLoanExtAppService.deleteByPrimaryKey(extSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpLoanExtAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 新增业务展期信息
     * @param iqpLoanExtApp
     * @return
     */
    @PostMapping("/addIqpLoanExtApp")
    protected ResultDto<IqpLoanExtApp> addIqpLoanExtApp(@RequestBody IqpLoanExtApp iqpLoanExtApp){
        log.info("新增业务展期信息{}"+ JSON.toJSONString(iqpLoanExtApp));
        iqpLoanExtAppService.addIqpLoanExtApp(iqpLoanExtApp);
        ResultDto<IqpLoanExtApp> resultDto = new ResultDto<>(iqpLoanExtApp);
        return resultDto;
    }

    /**
     * 删除业务展期信息
     * @param iqpLoanExtApp
     * @return
     */
    @PostMapping("deleteIqpLoanExtAppInfo")
    protected ResultDto<IqpLoanExtApp> deleteIqpLoanExtAppInfo(@RequestBody IqpLoanExtApp iqpLoanExtApp){
        log.info("删除业务展期信息{}"+ JSON.toJSONString(iqpLoanExtApp));
        iqpLoanExtAppService.deleteIqpLoanExtAppInfo(iqpLoanExtApp);
        ResultDto<IqpLoanExtApp> resultDto = new ResultDto<>(iqpLoanExtApp);
        return resultDto;
    }

    /**
     * @函数名称:queryIqpLoanExtAppList
     * @函数描述:展期借据查询列表
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @创建人：周茂伟
     * @算法描述:
     */
    @ApiOperation(value = "展期借据列表信息查询")
    @PostMapping("/queryIqpLoanExtAppList")
    protected ResultDto<List<IqpLoanExtApp>> queryIqpLoanExtAppList(@RequestBody QueryModel queryModel) {
        List<IqpLoanExtApp> list = iqpLoanExtAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanExtApp>>(list);
    }
    /**
     * @函数名称:queryIqpLoanExtApp
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @ApiOperation(value = "展期借据信息查询")
    @PostMapping("/queryIqpLoanExtApp")
    protected ResultDto<IqpLoanExtApp> queryIqpLoanExtApp(@RequestBody IqpLoanExtApp iqpLoanExtApp) {
        IqpLoanExtApp iqpLoanExtApps = iqpLoanExtAppService.selectByPrimaryKey(iqpLoanExtApp.getExtSerno());
        return new ResultDto<IqpLoanExtApp>(iqpLoanExtApps);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "展期借据删除")
    @PostMapping("/delete/deleteIqpLoanExtAppByExtSerno")
    protected ResultDto<Integer> deleteIqpLoanExtAppByExtSerno(@RequestBody IqpLoanExtApp iqpLoanExtApp) {
        int result = iqpLoanExtAppService.deleteByPrimaryKey(iqpLoanExtApp.getExtSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:saveIqpLoanExtApp
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "展期借据保存")
    @PostMapping("/saveIqpLoanExtApp")
    protected ResultDto<Integer> saveIqpLoanExtApp(@RequestBody IqpLoanExtApp iqpLoanExtApp) throws URISyntaxException {
       int result=iqpLoanExtAppService.saveIqpLoanExtApp(iqpLoanExtApp);
        return new ResultDto<Integer>(result);
    }
}
