package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CoopPlanAppConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.req.CmisLmt0027ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.resp.CmisLmt0027RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.github.pagehelper.PageHelper;
import feign.QueryMap;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.checkerframework.checker.units.qual.A;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.Query;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-13 10:06:49
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPlanAppService {

    private static final Logger log = LoggerFactory.getLogger(CoopPlanAppService.class);
    @Autowired
    private CoopPlanAppMapper coopPlanAppMapper;

    @Autowired
    private CoopPlanAccInfoMapper coopPlanAccInfoMapper;

    @Autowired
    private CoopPlanProInfoMapper coopPlanProInfoMapper;

    @Autowired
    private CoopPlanAptiInfoMapper coopPlanAptiInfoMapper;

    @Autowired
    private CoopPlanEspecQuotaCtrlMapper coopPlanEspecQuotaCtrlMapper;

    @Autowired
    private CoopPlanSuitOrgInfoMapper coopPlanSuitOrgInfoMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private Dscms2CoreDpClientService dscms2CoreDpClientService;

    @Autowired
    private CoopProAccInfoMapper coopProAccInfoMapper;

    @Autowired
    private CoopPartnerLstInfoMapper coopPartnerLstInfoMapper;

    @Autowired
    private BusiImageRelInfoMapper busiImageRelInfoMapper;

    @Autowired
    private CreditQryBizRealMapper creditQryBizRealMapper;

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CoopReplyAppMapper coopReplyAppMapper;

    @Autowired
    private CoopReplyAppProMapper coopReplyAppProMapper;

    @Autowired
    private CoopReplyAppSubMapper coopReplyAppSubMapper;

    @Autowired
    private CoopReplyAppCondMapper coopReplyAppCondMapper;

    @Autowired
    private CoopReplyAppPspMapper coopReplyAppPspMapper;

    @Autowired
    private CoopReplyAccMapper coopReplyAccMapper;

    @Autowired
    private CoopReplyAccProMapper coopReplyAccProMapper;

    @Autowired
    private CoopReplyAccSubMapper coopReplyAccSubMapper;

    @Autowired
    private CoopReplyAccCondMapper coopReplyAccCondMapper;

    @Autowired
    private CoopReplyAccPspMapper coopReplyAccPspMapper;

    @Autowired
    private BusinessInformationService businessInformationService;

    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private MessageSendService messageSendService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CoopPlanApp selectByPrimaryKey(String serno) {
        return coopPlanAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CoopPlanApp> selectAll(QueryModel model) {
        List<CoopPlanApp> records = (List<CoopPlanApp>) coopPlanAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CoopPlanApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPlanApp> list = coopPlanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 合作方新增和变更操作
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto<CoopPlanApp> insert(CoopPlanApp record) {
        QueryModel preModel = new QueryModel();
        preModel.addCondition("partnerNo",record.getPartnerNo());
        preModel.addCondition("partnerStatus","1");
        List<CoopPartnerLstInfo> coopPartnerLstInfoList = coopPartnerLstInfoMapper.selectByModel(preModel);
        String preStatus = coopPartnerLstInfoList.size() == 0 ? "000,111,992" : "000,111,992,997";
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("partnerNo", record.getPartnerNo());
        queryModel.addCondition("oprType", record.getOprType());
//        String apprStatus = ("1").equals(record.getOprType()) ? "000,111,992,997" : "000,111,992";
        String apprStatus = ("1").equals(record.getOprType()) ? preStatus : "000,111,992";
        queryModel.addCondition("apprStatus", apprStatus);
        List<CoopPlanApp> listCoopPlanApp = coopPlanAppMapper.selectByModel(queryModel);
        if (!CollectionUtils.isEmpty(listCoopPlanApp)) {
            String title = ("1").equals(record.getOprType()) ? "该合作方已有准入新增申请！" : "该合作方已有准入变更申请！";
            throw BizException.error(null, null, title);
        }

        ResultDto<CoopPlanApp> dto = new ResultDto();
        String oprType = record.getOprType();
        //如果是新增则直接插入数据
        if (CoopPlanAppConstant.OPR_TYPE_ADD.equalsIgnoreCase(oprType)) {
            record.setCoopStartDate(DateUtils.getCurrDateStr());
            record.setCoopEndDate(DateUtils.getCurrDateStr());
            record.setImageNo(record.getSerno());
            //存在性校验
            QueryModel model = new QueryModel();
            model.addCondition("partnerNo", record.getPartnerNo());
            model.addCondition("oprType", "1");
            model.addCondition("apprStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
            List<CoopPlanApp> list = coopPlanAppMapper.selectByModel(model);
            if (list.isEmpty()) {
                // 网金业务合作方 合作起始日/合作到日期不赋值
                if ("12" .equals(record.getPartnerType())) {
                    record.setCoopStartDate(null);
                    record.setCoopEndDate(null);
                }
                //新增初始化数据、影像数据
                coopPlanAppMapper.insert(record);
                insertImage(record);
            } else {
                //已存在准入申请
                throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_ALTER.key, EcbEnum.LMT_COOP_PLAN_APP_ALTER.value);
            }
            return new ResultDto<>(record);
        }
        /************************下面是合作方案变更***********************/
        QueryModel model = new QueryModel();
        model.addCondition("partnerNo", record.getPartnerNo());
        model.addCondition("oprType", "2");
        model.addCondition("apprStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
        List lt = coopPlanAppMapper.selectByModel(model);
        if (!lt.isEmpty()) {
            throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_APPLY.key, EcbEnum.LMT_COOP_PLAN_APP_APPLY.value);
        }
        //公共的操作人信息处理
        CoopPlanAppCommonDto commonDto = new CoopPlanAppCommonDto();
        BeanUtils.beanCopy(record, commonDto);
        //如果是变更，则需要将合作方方案台账信息查询出来
        List<CoopReplyAcc> list = null;
        List<CoopPlanAccInfo> coopPlanAccInfoList = null;
        if (CoopPlanAppConstant.OPR_TYPE_MODIFY.equalsIgnoreCase(oprType)) {
            QueryModel qm = new QueryModel();
            qm.getCondition().put("coopPlanNo", record.getCoopPlanNo());
            list = coopReplyAccMapper.selectByModel(qm);
            coopPlanAccInfoList = coopPlanAccInfoMapper.selectByModel(qm);
        }
        //数据查询失败或者多条数据都是异常情况
        CoopReplyAcc accInfo =null;
        if (coopPlanAccInfoList == null || coopPlanAccInfoList.size() == CoopPlanAppConstant.NUMBER_ZERO ) {
            dto.setCode(CoopPlanAppConstant.RESULT_FIAL);
            throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_BG1.key, EcbEnum.LMT_COOP_PLAN_APP_BG1.value);
        }else if(coopPlanAccInfoList!=null && coopPlanAccInfoList.size() > CoopPlanAppConstant.NUMBER_ONE){
            dto.setCode(CoopPlanAppConstant.RESULT_FIAL);
            throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_BG2.key, EcbEnum.LMT_COOP_PLAN_APP_BG2.value);
        }
        // 复制合作协议方案台账数据
        if(list!=null && list.size()>0){
            accInfo = list.get(0);
        }else {
            dto.setCode(CoopPlanAppConstant.RESULT_FIAL);
            throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_BG3.key, EcbEnum.LMT_COOP_PLAN_APP_BG3.value);
        }

        CoopPlanAccInfo coopPlanAccInfo = coopPlanAccInfoList.get(0);
        CoopPlanApp app = new CoopPlanApp();
        BeanUtils.beanCopy(coopPlanAccInfo, app);
        app.setApprStatus(CoopPlanAppConstant.APPR_STATUS_START);
        app.setSerno(record.getSerno());
        //公共数据拷贝
        BeanUtils.beanCopy(commonDto, app);
        //插入合作方申请数据
        coopPlanAppMapper.insert(app);
        QueryModel qm = new QueryModel();
        qm.getCondition().put(CoopPlanAppConstant.COOP_PLAN_SERNO, coopPlanAccInfo.getSerno());
        //合作方协议特殊限额信息
        QueryModel replyQm = new QueryModel();
        replyQm.addCondition("replyNo",accInfo.getReplyNo());
        List<CoopReplyAccSub> especInfo = coopReplyAccSubMapper.selectByModel(replyQm);
        especInfo.stream().forEach(espec -> {
            //查询接口的CoopPlanProInfoDto对象经过拼装，组装上了合作方名称等信息，保存操作时需要还原原有对象
//            espec.setSerno(record.getSerno());
            //公共数据拷贝
            BeanUtils.beanCopy(commonDto, espec);
            espec.setPkId(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            CoopReplyAppSub cras = new CoopReplyAppSub();
            BeanUtils.beanCopy(espec, cras);
            cras.setSerno(record.getSerno());
            coopReplyAppSubMapper.insertSelective(cras);
            CoopPlanEspecQuotaCtrl ctrl = new CoopPlanEspecQuotaCtrl();
            BeanUtils.beanCopy(espec, ctrl);
            ctrl.setSerno(record.getSerno());
            coopPlanEspecQuotaCtrlMapper.insert(ctrl);
        });
        //复制项目信息
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("serno",coopPlanAccInfo.getSerno());
        List<CoopPlanProInfoDto> proList = coopPlanProInfoMapper.selectByModel(queryModel1);
        proList.stream().forEach(pro -> {
            CoopReplyAppPro coopReplyAppPro = new CoopReplyAppPro();
            BeanUtils.beanCopy(pro, coopReplyAppPro);
            coopReplyAppPro.setSerno(record.getSerno());
            //公共数据拷贝
            BeanUtils.beanCopy(commonDto, coopReplyAppPro);
            coopReplyAppPro.setPkId(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            coopReplyAppProMapper.insertSelective(coopReplyAppPro);
            //查询接口的CoopPlanProInfoDto对象经过拼装，组装上了合作方名称等信息，保存操作时需要还原原有对象
            CoopPlanProInfo proInfo = new CoopPlanProInfo();
            BeanUtils.beanCopy(pro, proInfo);
            proInfo.setSerno(record.getSerno());
            //公共数据拷贝
            BeanUtils.beanCopy(commonDto, proInfo);
            proInfo.setPkId(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            coopPlanProInfoMapper.insert(proInfo);
        });
        //复制用信条件信息
        List<CoopReplyAccCond> coopReplyAccCondList = coopReplyAccCondMapper.selectByModel(replyQm);
        coopReplyAccCondList.stream().forEach(crac ->{
            CoopReplyAppCond coopReplyAppCond = new CoopReplyAppCond();
            BeanUtils.beanCopy(crac,coopReplyAppCond);
            coopReplyAppCond.setSerno(record.getSerno());
            //公共数据拷贝
            BeanUtils.beanCopy(commonDto,coopReplyAppCond);
            coopReplyAppCondMapper.insertSelective(coopReplyAppCond);
        });
        //复制贷后管理要求信息
        List<CoopReplyAccPsp> coopReplyAccPspList = coopReplyAccPspMapper.selectByModel(replyQm);
        coopReplyAccPspList.stream().forEach(crap ->{
            CoopReplyAppPsp coopReplyAppPsp = new CoopReplyAppPsp();
            BeanUtils.beanCopy(crap,coopReplyAppPsp);
            coopReplyAppPsp.setSerno(record.getSerno());
            //公共数据拷贝
            BeanUtils.beanCopy(commonDto,coopReplyAppPsp);
            coopReplyAppPspMapper.insertSelective(coopReplyAppPsp);
        });
        //合作方协议适用机构信息
        List<CoopPlanSuitOrgInfo> orgInfo = coopPlanSuitOrgInfoMapper.selectByModel(qm);
        orgInfo.stream().forEach(org -> {
            //查询接口的CoopPlanProInfoDto对象经过拼装，组装上了合作方名称等信息，保存操作时需要还原原有对象
            org.setSerno(record.getSerno());
            //公共数据拷贝
            BeanUtils.beanCopy(commonDto, org);
            org.setPkId(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            coopPlanSuitOrgInfoMapper.insert(org);
        });
        //复制资质信息
        List<CoopPlanAptiInfoDto> aptiList = coopPlanAptiInfoMapper.selectByModel(qm);
        aptiList.stream().forEach(apti -> {
            //查询接口的CoopPlanProInfoDto对象经过拼装，组装上了合作方名称等信息，保存操作时需要还原原有对象
            CoopPlanAptiInfo aptiInfo = new CoopPlanAptiInfo();
            BeanUtils.beanCopy(apti, aptiInfo);
            aptiInfo.setSerno(record.getSerno());
            //公共数据拷贝
            BeanUtils.beanCopy(commonDto, aptiInfo);
            aptiInfo.setPkId(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            coopPlanAptiInfoMapper.insert(aptiInfo);
        });
        //复制影像信息
        List<BusiImageRelInfo> imageList = busiImageRelInfoMapper.selectByModel(qm);
        imageList.stream().forEach(image -> {
            BusiImageRelInfo imageInfo = new BusiImageRelInfo();
            BeanUtils.beanCopy(image, imageInfo);
            imageInfo.setSerno(record.getSerno());
            //公共数据拷贝
            BeanUtils.beanCopy(commonDto, imageInfo);
            imageInfo.setPkId(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            busiImageRelInfoMapper.insert(imageInfo);
        });
        //复制征信查询信息
        qm.getCondition().put("bizSerno", coopPlanAccInfo.getSerno());
        List<CreditQryBizReal> creditList = creditQryBizRealMapper.selectByModel(qm);
        creditList.stream().forEach(credit -> {
            String crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>());
            String authbookNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
            CreditQryBizReal creditInfo = new CreditQryBizReal();
            BeanUtils.beanCopy(credit, creditInfo);
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(creditInfo.getCrqlSerno());
            creditInfo.setBizSerno(record.getSerno());
            creditInfo.setCrqlSerno(crqlSerno);
            creditInfo.setCqbrSerno(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            creditQryBizRealMapper.insert(creditInfo);
            creditReportQryLst.setCrqlSerno(crqlSerno);
            creditReportQryLst.setApproveStatus("000");
            creditReportQryLst.setAuthbookNo(authbookNo);
            creditReportQryLstMapper.insert(creditReportQryLst);
        });
        dto.setData(app);
        return dto;
    }

    /**
     * @方法名称: insertImage
     * @方法描述: 插入业务影像关联表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public void insertImage(CoopPlanApp record) {
        String partnerType = record.getPartnerType();
//        房地产开发商1	楼盘合作影像	授信基本资料	借款人资料	担保资料[总的]	项目类资料	委托人资料	授信利率批复	分支行用信审核材料	总行意见及授信批复
//        专业担保公司2	担保公司-授信资料
//        集群贷4	授信基本资料	借款人资料	担保资料[总的]	项目类资料	委托人资料	授信利率批复	分支行用信审核材料	总行意见及授信批复
//        光伏设备生产企业5	授信基本资料	借款人资料	担保资料[总的]	项目类资料	委托人资料	授信利率批复	分支行用信审核材料	总行意见及授信批复
//        教育机构15	无
//        核心企业8	授信基本资料	借款人资料	担保资料[总的]	项目类资料	委托人资料	授信利率批复	分支行用信审核材料	总行意见及授信批复
//        保险公司3	担保公司-授信资料
//        押品评估机构11	押品评估机构准入资料
        BusiImageRelInfo busiImageRelInfo = new BusiImageRelInfo();
        busiImageRelInfo.setSerno(record.getSerno());

        // 客户资料--查看权限
        busiImageRelInfo.setImageDesc("对公客户资料");
        busiImageRelInfo.setImageNo(record.getPartnerNo());
        busiImageRelInfo.setTopOutsystemCode("DGKHZL");
        busiImageRelInfo.setKeywordType("custid");
        busiImageRelInfo.setAuthority("download");
        busiImageRelInfo.setImageOrder(1);
        busiImageRelInfo.setPkId(null);
        busiImageRelInfoMapper.insert(busiImageRelInfo);
        if("1".equals(partnerType)){
            //楼盘合作方在新增项目时，以项目维度同步新增授信资料
        }else if("2".equals(partnerType) || "3".equals(partnerType)){
            busiImageRelInfo.setImageDesc("授信资料");
            busiImageRelInfo.setImageNo(record.getImageNo());
            busiImageRelInfo.setTopOutsystemCode("DBGSSXZL");
            busiImageRelInfo.setKeywordType("businessid");
            busiImageRelInfo.setAuthority("import;insert;download;scan;delImg");
            busiImageRelInfo.setImageOrder(2);
            busiImageRelInfo.setPkId(UUID.randomUUID().toString());
            busiImageRelInfoMapper.insert(busiImageRelInfo);
        }else if("4".equals(partnerType) || "5".equals(partnerType) || "8".equals(partnerType)){
            busiImageRelInfo.setImageDesc("授信资料");
            busiImageRelInfo.setImageNo(record.getImageNo());
            busiImageRelInfo.setTopOutsystemCode("SXJBZL;JKRZL;DBZL;XMLZL;SXLLPF;XD_FZHYXCL;ZHSXPF");
            busiImageRelInfo.setKeywordType("businessid");
            busiImageRelInfo.setAuthority("import;insert;download;scan;delImg");
            busiImageRelInfo.setImageOrder(2);
            busiImageRelInfo.setPkId(null);
            busiImageRelInfoMapper.insert(busiImageRelInfo);
        }else if("11".equals(partnerType)){
            busiImageRelInfo.setImageDesc("授信资料");
            busiImageRelInfo.setImageNo(record.getImageNo());
            busiImageRelInfo.setTopOutsystemCode("XXD_YPZRZL");
            busiImageRelInfo.setKeywordType("businessid");
            busiImageRelInfo.setAuthority("import;insert;download;scan;delImg");
            busiImageRelInfo.setImageOrder(2);
            busiImageRelInfo.setPkId(UUID.randomUUID().toString());
            busiImageRelInfoMapper.insert(busiImageRelInfo);
        }
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CoopPlanApp record) {
        return coopPlanAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CoopPlanApp record) {
        return coopPlanAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CoopPlanApp record) {
        return coopPlanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateTotalAmt
     * @方法描述: 根据主键更新 - 更新总合作额度
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateTotalAmt(CoopPlanApp record) {
        return coopPlanAppMapper.updateTotalAmt(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String serno) {
        return coopPlanAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除 ，开启手动事务处理。
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = false, rollbackFor = {Exception.class, RuntimeException.class})
    public int deleteByIds(String ids) {
        SqlSession session = null;
        int result = 0;
        try {
            session = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH, false);
            String ids_[] = ids.split(",");
            SqlSession finalSession = session;
            Arrays.stream(ids_).forEach(id -> {
                //合作方案申请表
                CoopPlanAppMapper coopPlanAppMapper = finalSession.getMapper(CoopPlanAppMapper.class);
                //查询合作方案数据获取关联的合作方案编号
                CoopPlanApp coopPlanApp = coopPlanAppMapper.selectByPrimaryKey(id);
                //特殊限额表
                CoopPlanEspecQuotaCtrlMapper coopPlanEspecQuotaCtrlMapper = finalSession.getMapper(CoopPlanEspecQuotaCtrlMapper.class);
                //关联机构表
                CoopPlanSuitOrgInfoMapper coopPlanSuitOrgInfoMapper = finalSession.getMapper(CoopPlanSuitOrgInfoMapper.class);
                //项目表
                CoopPlanProInfoMapper coopPlanProInfoMapper = finalSession.getMapper(CoopPlanProInfoMapper.class);
                //资质表
                CoopPlanAptiInfoMapper coopPlanAptiInfoMapper = finalSession.getMapper(CoopPlanAptiInfoMapper.class);
                //特殊限额级联删除
                CoopPlanEspecQuotaCtrl ctrl = new CoopPlanEspecQuotaCtrl();
                ctrl.setSerno(coopPlanApp.getSerno());
                coopPlanEspecQuotaCtrlMapper.deleteBySerno(ctrl);
                //机构表级联删除
                CoopPlanSuitOrgInfo info = new CoopPlanSuitOrgInfo();
                info.setSerno(coopPlanApp.getSerno());
                coopPlanSuitOrgInfoMapper.deleteAll(info);
                //项目删除
                CoopPlanProInfo proInfo = new CoopPlanProInfo();
                proInfo.setSerno(coopPlanApp.getSerno());
                coopPlanProInfoMapper.deleteBySerno(proInfo);
                //资质删除
                CoopPlanAccInfo accInfo = new CoopPlanAccInfo();
                accInfo.setSerno(coopPlanApp.getSerno());
                coopPlanAptiInfoMapper.deleteBySerno(proInfo);
                //删除影像数据
                BusiImageRelInfo busiImageRelInfo = new BusiImageRelInfo();
                busiImageRelInfo.setSerno(coopPlanApp.getSerno());
                busiImageRelInfoMapper.deleteBySerno(busiImageRelInfo);
                //合作方案删除
                coopPlanAppMapper.deleteByPrimaryKey(coopPlanApp.getSerno());
            });
            //提交事务
            session.flushStatements();
            session.commit();
        } catch (Exception e) {
            if (null != session) {
                session.rollback();
            }
            log.error(e.getMessage(), e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return result;
    }

    /**
     * @方法名称: queryCoopLmtAmt
     * @方法描述: 查询合作方可用额度
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto<CmisLmt0027RespDto> queryCoopLmtAmt(String partnerNo, String instuCde) {
        CmisLmt0027ReqDto dto = new CmisLmt0027ReqDto();
        dto.setCusId(partnerNo);
        dto.setInstuCde(instuCde);
        return cmisLmtClientService.cmislmt0027(dto);
    }

    /**
     * @方法名称: queryBail
     * @方法描述: 查询合作方可用额度
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto queryBail(String bailAccNo) {
        ResultDto resultDto = new ResultDto();
        Dp2099ReqDto reqDto = new Dp2099ReqDto();
        reqDto.setKehuzhao(bailAccNo);
        reqDto.setChxunbis(1000);
        reqDto.setQishibis(0);
//        reqDto.setZhhaoxuh("00001");
        ResultDto<Dp2099RespDto> respDto = dscms2CoreDpClientService.dp2099(reqDto);
        String code = respDto.getCode();
        Dp2099RespDto dto = respDto.getData();
        if (code.equals("0")) {
            resultDto.setData(dto);
            return resultDto;
        } else {
            resultDto.setCode(1);
            resultDto.setMessage(respDto.getMessage());
        }
        return resultDto;
    }

    /**
     * @函数名称:checkSubmit
     * @函数描述:检查是否有在途的数据
     * @参数与返回说明:
     * @算法描述:
     */
    public ResultDto checkSubmit(CoopPlanApp coopPlanApp) {
        ResultDto dto = new ResultDto();
        QueryModel model = new QueryModel();
        model.addCondition("partnerNo", coopPlanApp.getPartnerNo());
        model.addCondition("oprType", coopPlanApp.getOprType());
        model.addCondition("apprStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
        List<CoopPlanApp> lt = coopPlanAppMapper.selectByModel(model);
        if (lt.size() > 0) {
            lt = lt.stream().filter(cp -> !cp.getSerno().equals(coopPlanApp.getSerno())).collect(Collectors.toList());
        }
        if (CoopPlanAppConstant.OPR_TYPE_ADD.equalsIgnoreCase(coopPlanApp.getOprType()) && !lt.isEmpty()) {
            throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_ALTER.key, EcbEnum.LMT_COOP_PLAN_APP_ALTER.value);
        }
        if (CoopPlanAppConstant.OPR_TYPE_MODIFY.equalsIgnoreCase(coopPlanApp.getOprType()) && !lt.isEmpty()) {
            throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_APPLY.key, EcbEnum.LMT_COOP_PLAN_APP_APPLY.value);
        }
        if ("2".equals(coopPlanApp.getPartnerType()) || "1".equals(coopPlanApp.getPartnerType()) || "5".equals(coopPlanApp.getPartnerType()) || "12".equals(coopPlanApp.getPartnerType()) || "3".equals(coopPlanApp.getPartnerType()) || "4".equals(coopPlanApp.getPartnerType()) || "8".equals(coopPlanApp.getPartnerType()) || "15".equals(coopPlanApp.getPartnerType())) {
            // 查询合作方案关联机构
            QueryModel orgQueryModel = new QueryModel();
            orgQueryModel.addCondition("serno", coopPlanApp.getSerno());
            List orgQueryList = coopPlanSuitOrgInfoMapper.selectByModel(orgQueryModel);
            if (orgQueryList.size() == 0) {
                throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_ORG.key, EcbEnum.LMT_COOP_PLAN_APP_ORG.value);
            }
        }
        if ("2".equals(coopPlanApp.getPartnerType())) {
            // 查询合作方案关联特殊限额
            QueryModel especQueryModel = new QueryModel();
            especQueryModel.addCondition("serno", coopPlanApp.getSerno());
            List especQueryList = coopPlanEspecQuotaCtrlMapper.selectByModel(especQueryModel);
            if (especQueryList.size() == 0) {
                throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_ESPEC.key, EcbEnum.LMT_COOP_PLAN_APP_ESPEC.value);
            }
        }
        if (!"13".equals(coopPlanApp.getPartnerType())) {
            // 查询合作方案资质信息
            QueryModel ptiQueryModel = new QueryModel();
            ptiQueryModel.addCondition("serno", coopPlanApp.getSerno());
            List ptiQueryList = coopPlanAptiInfoMapper.selectByModel(ptiQueryModel);
            if (ptiQueryList.size() == 0) {
                throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_PTI.key, EcbEnum.LMT_COOP_PLAN_APP_PTI.value);
            }
        }
        if ("1".equals(coopPlanApp.getPartnerType())) {
            // 查询合作方案项目
            QueryModel proQueryModel = new QueryModel();
            proQueryModel.addCondition("serno", coopPlanApp.getSerno());
            List proQueryList = coopPlanProInfoMapper.selectByModel(proQueryModel);
            if (proQueryList.size() == 0) {
                throw BizException.error(null, EcbEnum.LMT_COOP_PLAN_APP_PRO.key, EcbEnum.LMT_COOP_PLAN_APP_PRO.value);
            }
        }

        return dto;
    }

    /**
     * @函数名称:wfApply
     * @函数描述: 合作方申请流程后处理方法
     * @参数与返回说明:
     * @算法描述:
     */
    @Transactional(readOnly = false, rollbackFor = {Exception.class, RuntimeException.class})
    public void wfApply(String serno) {
        CoopPlanApp coopPlanApp = coopPlanAppMapper.selectByPrimaryKey(serno);
        coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_PASS);
        log.info("合作方准入申请【{}】流程后处理逻辑", serno);
        //申请通过后自动设置合作的有效期的截止日期
        if (coopPlanApp.getCoopEndDate() != null) {
            Date d = DateUtils.parseDateByDef(coopPlanApp.getCoopEndDate());
            Calendar calendar = Calendar.getInstance();
            d.setTime(d.getTime());
            Integer term = coopPlanApp.getCoopTerm();
            if (term != null) {
                calendar.add(Calendar.MONTH, term);
                calendar.add(Calendar.DAY_OF_MONTH, CoopPlanAppConstant.COOP_PLAN_END_DATE_INDEX);
                SimpleDateFormat sf = new SimpleDateFormat(CoopPlanAppConstant.COOP_PLAN_DATE_FORMAT);
                String date = sf.format(calendar.getTime());
                coopPlanApp.setCoopEndDate(date);
            }
        }
        if("6".equals(coopPlanApp.getPartnerType()) || "7".equals(coopPlanApp.getPartnerType()) || "9".equals(coopPlanApp.getPartnerType()) || "10".equals(coopPlanApp.getPartnerType()) || "11".equals(coopPlanApp.getPartnerType()) || "12".equals(coopPlanApp.getPartnerType()) || "13".equals(coopPlanApp.getPartnerType()) || "14".equals(coopPlanApp.getPartnerType())){
            // 旅游公司/物联网动产贷交易平台归属方/监管公司/仓储公司/押品评估机构/网金业务合作方/律师事务所/其他合作方
            coopPlanApp.setCoopStartDate("");
            coopPlanApp.setCoopEndDate("");
        }

        //更新结果到方案表
        coopPlanAppMapper.updateByPrimaryKey(coopPlanApp);

        //批复台账表中插入数据
        CoopReplyApp coopReplyApp = coopReplyAppMapper.selectByPrimaryKey(serno);
        String replyNo = coopReplyApp.getReplyNo();

        QueryModel queryModelReplyNo = new QueryModel();
        queryModelReplyNo.addCondition("replyNo",replyNo);
        //QueryModel queryModel = new QueryModel();
        //queryModelReplyNo.addCondition("coopPlanNo",coopReplyApp.getCoopPlanNo());
        List<CoopReplyAcc> coopReplyAccList = coopReplyAccMapper.selectByModel(queryModelReplyNo);
        CoopReplyAcc coopReplyAcc = new CoopReplyAcc();
        BeanUtils.beanCopy(coopReplyApp,coopReplyAcc);
        //批复台账状态设为生效
        coopReplyAcc.setReplyStatus("01");
        if(coopReplyAccList == null || coopReplyAccList.size() == 0) {
            coopReplyAccMapper.insertSelective(coopReplyAcc);
        } else {
            coopReplyAcc.setReplyNo(coopReplyAccList.get(0).getReplyNo());
            coopReplyAccMapper.updateByPrimaryKeySelective(coopReplyAcc);
        }
        //批复项目表中插入数据
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("serno",serno);
        List<CoopReplyAppPro> coopReplyAppProList = coopReplyAppProMapper.selectByModel(queryModel1);
        coopReplyAppProList.stream().forEach(dto -> {
            CoopReplyAccPro coopReplyAccPro = new CoopReplyAccPro();
            BeanUtils.beanCopy(dto, coopReplyAccPro);
            coopReplyAccPro.setProStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
            QueryModel queryModel2 = new QueryModel();
            queryModel2.addCondition("proNo",coopReplyAccPro.getProNo());
            List<CoopReplyAccPro> coopReplyAccProList = coopReplyAccProMapper.selectByModel(queryModel2);
            if (coopReplyAccProList == null || coopReplyAccProList.size() == 0){
                coopReplyAccPro.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccProMapper.insertSelective(coopReplyAccPro);
            } else {
                coopReplyAccPro.setPkId(coopReplyAccProList.get(0).getPkId());
                coopReplyAccPro.setReplyNo(coopReplyAccProList.get(0).getReplyNo());
                coopReplyAccProMapper.updateByPrimaryKeySelective(coopReplyAccPro);
            }
            //更新CoopReplyAppPro的proStatus状态为正常
            dto.setProStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
            coopReplyAppProMapper.updateByPrimaryKeySelective(dto);
            //根据批复信息更新项目信息
            this.updateProInfo(serno,coopPlanApp,dto);
        });

        //批复分项表中插入数据
        List<CoopReplyAppSub> coopReplyAppSubs = coopReplyAppSubMapper.selectByModel(queryModel1);
        coopReplyAppSubs.stream().forEach(dto -> {
            CoopReplyAccSub coopReplyAccSub = new CoopReplyAccSub();
            BeanUtils.beanCopy(dto, coopReplyAccSub);
            QueryModel queryModelSub = new QueryModel();
            queryModelSub.addCondition("subNo",coopReplyAccSub.getSubNo());
            List<CoopReplyAccSub> coopReplyAccSubList = coopReplyAccSubMapper.selectByModel(queryModelSub);
            if (coopReplyAccSubList == null || coopReplyAccSubList.size() == 0){
                coopReplyAccSub.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccSubMapper.insertSelective(coopReplyAccSub);
            } else {
                coopReplyAccSub.setPkId((coopReplyAccSubList.get(0).getPkId()));
                coopReplyAccSub.setReplyNo(coopReplyAccSubList.get(0).getReplyNo());
                coopReplyAccSubMapper.updateByPrimaryKeySelective(coopReplyAccSub);
            }
        });

        //批复台账条件表中插入数据
        List<CoopReplyAppCond> coopReplyAppCondList = coopReplyAppCondMapper.selectByModel(queryModel1);
        List<CoopReplyAccCond> coopReplyAccCondList = coopReplyAccCondMapper.selectByModel(queryModelReplyNo);
        if (coopReplyAccCondList == null ||coopReplyAccCondList.size() == 0 ){
            coopReplyAppCondList.stream().forEach(dto -> {
                CoopReplyAccCond coopReplyAccCond = new CoopReplyAccCond();
                BeanUtils.beanCopy(dto,coopReplyAccCond);
                coopReplyAccCond.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccCondMapper.insertSelective(coopReplyAccCond);
            });
        } else {
            for ( CoopReplyAccCond coopReplyAccCond : coopReplyAccCondList){
                coopReplyAccCondMapper.deleteByPrimaryKey(coopReplyAccCond.getPkId(),coopReplyAccCond.getReplyNo());
            }
            coopReplyAppCondList.stream().forEach(dto -> {
                CoopReplyAccCond coopReplyAccCond = new CoopReplyAccCond();
                BeanUtils.beanCopy(dto,coopReplyAccCond);
                coopReplyAccCond.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccCondMapper.insertSelective(coopReplyAccCond);
            });
        }
        //批复台账要求管理表中插入数据
        List<CoopReplyAppPsp> coopReplyAppPspList = coopReplyAppPspMapper.selectByModel(queryModel1);
        List<CoopReplyAccPsp> coopReplyAccPspList = coopReplyAccPspMapper.selectByModel(queryModelReplyNo);
        if (coopReplyAccPspList == null || coopReplyAccPspList.size() == 0){
            coopReplyAppPspList.stream().forEach(dto ->{
                CoopReplyAccPsp coopReplyAccPsp = new CoopReplyAccPsp();
                BeanUtils.beanCopy(dto,coopReplyAccPsp);
                coopReplyAccPsp.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccPspMapper.insertSelective(coopReplyAccPsp);
            });
        } else {
            for (CoopReplyAccPsp coopReplyAccPsp : coopReplyAccPspList){
                coopReplyAccPspMapper.deleteByPrimaryKey(coopReplyAccPsp.getPkId(),coopReplyAccPsp.getReplyNo());
            }
            coopReplyAppPspList.stream().forEach(dto ->{
                CoopReplyAccPsp coopReplyAccPsp = new CoopReplyAccPsp();
                BeanUtils.beanCopy(dto,coopReplyAccPsp);
                coopReplyAccPsp.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccPspMapper.insertSelective(coopReplyAccPsp);
            });
        }
        //申请通过后插入合作方数据到合作方名单管理中
        CoopPartnerLstInfo lstInfo = coopPartnerLstInfoMapper.selectByPrimaryKey(coopPlanApp.getPartnerNo());
        if (lstInfo == null) {
            lstInfo = new CoopPartnerLstInfo();
            BeanUtils.beanCopy(coopPlanApp, lstInfo);
            lstInfo.setPartnerStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
            coopPartnerLstInfoMapper.insert(lstInfo);
        } else {
            BeanUtils.beanCopy(coopPlanApp, lstInfo);
            lstInfo.setPartnerStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
            coopPartnerLstInfoMapper.updateByPrimaryKey(lstInfo);
        }

        // 针对流程到办结节点，进行以下处理,批复台账申请修改后需同步到方案台账
        QueryModel model = new QueryModel();
        model.getCondition().put(CoopPlanAppConstant.COOP_PLAN_NO, coopPlanApp.getCoopPlanNo());
        List<CoopPlanAccInfo> coopPlanAccInfos = coopPlanAccInfoMapper.selectByModel(model);
        CoopPlanAccInfo info = new CoopPlanAccInfo();
        BeanUtils.beanCopy(coopPlanApp, info);
        info.setTotlCoopLmtAmt(coopPlanApp.getTotlCoopLmtAmt());
        info.setCoopPlanStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
        BeanUtils.beanCopy(coopReplyAcc, info);
        if(coopPlanAccInfos == null || coopPlanAccInfos.size() == 0){
            info.setPkId(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            coopPlanAccInfoMapper.insert(info);
        }else{
            String pkId = coopPlanAccInfos.get(0).getPkId();
            info.setPkId(pkId);
            coopPlanAccInfoMapper.updateByPrimaryKey(info);
        }
    }

    public void updateProInfo(String serno,CoopPlanApp coopPlanApp,CoopReplyAppPro coopReplyAppPro){
        //记录项目台账
        QueryModel qm = new QueryModel();
        qm.addCondition("serno",serno);
        qm.addCondition("proNo",coopReplyAppPro.getProNo());
        List<CoopPlanProInfoDto> list = coopPlanProInfoMapper.selectByModel(qm);
        list.stream().forEach(dto -> {
            CoopProAccInfo accInfo = new CoopProAccInfo();
            BeanUtils.beanCopy(dto, accInfo);
            accInfo.setTotalArchSqu(dto.getTotlArchSqu());
            accInfo.setCoopPlanNo(coopPlanApp.getCoopPlanNo());
            accInfo.setProStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
            accInfo.setProName(coopReplyAppPro.getProName());
            accInfo.setProLmt(coopReplyAppPro.getProLmt());
            //变更时检查项目台账是否存在，不存在新增，存在更新
            CoopProAccInfo coopProAccInfo = coopProAccInfoMapper.selectByPrimaryKey(accInfo.getProNo());
            if (coopProAccInfo != null) {
                coopProAccInfoMapper.updateByPrimaryKeySelective(accInfo);
            } else {
                coopProAccInfoMapper.insert(accInfo);
            }
            //更新coopPlanProInfo的proStatus状态为正常
            CoopPlanProInfo coopPlanProInfo = new CoopPlanProInfo();
            coopPlanProInfo.setPkId(dto.getPkId());
            coopPlanProInfo.setProStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
            coopPlanProInfoMapper.updateByPrimaryKeySelective(coopPlanProInfo);
        });
    }
    /**
     * @函数名称:wfAlter
     * @函数描述: 合作方变更流程后处理方法
     * @参数与返回说明:
     * @算法描述:
     */
    @Transactional(readOnly = false, rollbackFor = {Exception.class, RuntimeException.class})
    public void wfAlter(String serno) {
        CoopPlanApp coopPlanApp = coopPlanAppMapper.selectByPrimaryKey(serno);
        coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_PASS);
        log.info("合作方准入变更申请【{}】", serno);
        //申请通过后自动设置合作的有效期的截止日期
        Date d = DateUtils.parseDateByDef(coopPlanApp.getCoopEndDate());
        Calendar calendar = Calendar.getInstance();
        d.setTime(d.getTime());
        Integer term = coopPlanApp.getCoopTerm();
        if (term != null) {
            calendar.add(Calendar.MONTH, term);
            calendar.add(Calendar.DAY_OF_MONTH, CoopPlanAppConstant.COOP_PLAN_END_DATE_INDEX);
            SimpleDateFormat sf = new SimpleDateFormat(CoopPlanAppConstant.COOP_PLAN_DATE_FORMAT);
            String date = sf.format(calendar.getTime());
            coopPlanApp.setCoopEndDate(date);
        }
        if("6".equals(coopPlanApp.getPartnerType()) || "7".equals(coopPlanApp.getPartnerType()) || "9".equals(coopPlanApp.getPartnerType()) || "10".equals(coopPlanApp.getPartnerType()) || "11".equals(coopPlanApp.getPartnerType()) || "12".equals(coopPlanApp.getPartnerType()) || "13".equals(coopPlanApp.getPartnerType()) || "14".equals(coopPlanApp.getPartnerType())){
            // 旅游公司/物联网动产贷交易平台归属方/监管公司/仓储公司/押品评估机构/网金业务合作方/律师事务所/其他合作方
            coopPlanApp.setCoopStartDate("");
            coopPlanApp.setCoopEndDate("");
        }
        //更新结果到方案表
        coopPlanAppMapper.updateByPrimaryKey(coopPlanApp);

        //批复台账表中插入数据
        CoopReplyApp coopReplyApp = coopReplyAppMapper.selectByPrimaryKey(serno);
        String replyNo = coopReplyApp.getReplyNo();
        QueryModel queryModelReplyNo = new QueryModel();
        queryModelReplyNo.addCondition("replyNo",replyNo);
        // 查询批复台账信息
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("coopPlanNo",coopReplyApp.getCoopPlanNo());
        List<CoopReplyAcc> coopReplyAccList = coopReplyAccMapper.selectByModel(queryModel);
        CoopReplyAcc coopReplyAcc = new CoopReplyAcc();
        BeanUtils.beanCopy(coopReplyApp,coopReplyAcc);
        //批复台账状态设为生效
        coopReplyAcc.setReplyStatus("01");
        if(coopReplyAccList == null || coopReplyAccList.size() == 0) {
            coopReplyAccMapper.insertSelective(coopReplyAcc);
        } else {
            replyNo = coopReplyAccList.get(0).getReplyNo();
            coopReplyAcc.setReplyNo(replyNo);
            coopReplyAccMapper.updateByPrimaryKeySelective(coopReplyAcc);
        }
        //批复台账项目表中插入数据
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("serno",serno);
        List<CoopReplyAppPro> coopReplyAppProList = coopReplyAppProMapper.selectByModel(queryModel1);
        coopReplyAppProList.stream().forEach(dto -> {
            CoopReplyAccPro coopReplyAccPro = new CoopReplyAccPro();
            BeanUtils.beanCopy(dto, coopReplyAccPro);
            coopReplyAccPro.setProStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
            QueryModel queryModel2 = new QueryModel();
            queryModel2.addCondition("proNo",dto.getProNo());
            List<CoopReplyAccPro> coopReplyAccProList = coopReplyAccProMapper.selectByModel(queryModel2);
            if (coopReplyAccProList == null || coopReplyAccProList.size() == 0){
                coopReplyAccPro.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccProMapper.insertSelective(coopReplyAccPro);
            } else {
                coopReplyAccPro.setPkId(coopReplyAccProList.get(0).getPkId());
                coopReplyAccPro.setReplyNo(coopReplyAccProList.get(0).getReplyNo());
                coopReplyAccProMapper.updateByPrimaryKeySelective(coopReplyAccPro);
            }
            //更新CoopReplyAppPro的proStatus状态为正常
            dto.setProStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
            coopReplyAppProMapper.updateByPrimaryKeySelective(dto);
            //根据批复信息更新项目信息
            this.updateProInfo(serno,coopPlanApp,dto);
        });
        //批复台账分项表中插入数据
        List<CoopReplyAppSub> coopReplyAppSubs = coopReplyAppSubMapper.selectByModel(queryModel1);
        coopReplyAppSubs.stream().forEach(dto -> {
            CoopReplyAccSub coopReplyAccSub = new CoopReplyAccSub();
            BeanUtils.beanCopy(dto, coopReplyAccSub);
            QueryModel queryModelSub = new QueryModel();
            queryModelSub.addCondition("subNo",dto.getSubNo());
            List<CoopReplyAccSub> coopReplyAccSubList = coopReplyAccSubMapper.selectByModel(queryModelSub);
            if (coopReplyAccSubList == null || coopReplyAccSubList.size() == 0){
                coopReplyAccSub.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccSubMapper.insertSelective(coopReplyAccSub);
            } else {
                coopReplyAccSub.setPkId((coopReplyAccSubList.get(0).getPkId()));
                coopReplyAccSub.setReplyNo(coopReplyAccSubList.get(0).getReplyNo());
                coopReplyAccSubMapper.updateByPrimaryKeySelective(coopReplyAccSub);
            }
        });
        //批复台账条件表中插入数据
        List<CoopReplyAppCond> coopReplyAppCondList = coopReplyAppCondMapper.selectByModel(queryModel1);
        List<CoopReplyAccCond> coopReplyAccCondList = coopReplyAccCondMapper.selectByModel(queryModelReplyNo);
        if (coopReplyAccCondList == null || coopReplyAccCondList.size() == 0 ){
            coopReplyAppCondList.stream().forEach(dto -> {
                CoopReplyAccCond coopReplyAccCond = new CoopReplyAccCond();
                BeanUtils.beanCopy(dto,coopReplyAccCond);
                coopReplyAccCond.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccCondMapper.insertSelective(coopReplyAccCond);
            });
        } else {
            for ( CoopReplyAccCond coopReplyAccCond : coopReplyAccCondList){
                coopReplyAccCondMapper.deleteByPrimaryKey(coopReplyAccCond.getPkId(),coopReplyAccCond.getReplyNo());
            }
            coopReplyAppCondList.stream().forEach(dto -> {
                CoopReplyAccCond coopReplyAccCond = new CoopReplyAccCond();
                BeanUtils.beanCopy(dto,coopReplyAccCond);
                coopReplyAccCond.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccCondMapper.insertSelective(coopReplyAccCond);
            });
        }
        //批复台账要求管理表中插入数据
        List<CoopReplyAppPsp> coopReplyAppPspList = coopReplyAppPspMapper.selectByModel(queryModel1);
        List<CoopReplyAccPsp> coopReplyAccPspList = coopReplyAccPspMapper.selectByModel(queryModelReplyNo);
        if (coopReplyAccPspList == null || coopReplyAccPspList.size() == 0){
            coopReplyAppPspList.stream().forEach(dto ->{
                CoopReplyAccPsp coopReplyAccPsp = new CoopReplyAccPsp();
                BeanUtils.beanCopy(dto,coopReplyAccPsp);
                coopReplyAccPsp.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccPspMapper.insertSelective(coopReplyAccPsp);
            });
        } else {
            for (CoopReplyAccPsp coopReplyAccPsp : coopReplyAccPspList){
                coopReplyAccPspMapper.deleteByPrimaryKey(coopReplyAccPsp.getPkId(),coopReplyAccPsp.getReplyNo());
            }
            coopReplyAppPspList.stream().forEach(dto ->{
                CoopReplyAccPsp coopReplyAccPsp = new CoopReplyAccPsp();
                BeanUtils.beanCopy(dto,coopReplyAccPsp);
                coopReplyAccPsp.setReplyNo(coopReplyApp.getReplyNo());
                coopReplyAccPspMapper.insertSelective(coopReplyAccPsp);
            });
        }
        //申请通过后插入合作方数据到合作方名单管理中
        CoopPartnerLstInfo lstInfo = coopPartnerLstInfoMapper.selectByPrimaryKey(coopPlanApp.getPartnerNo());
        if (lstInfo == null) {
            lstInfo = new CoopPartnerLstInfo();
            BeanUtils.beanCopy(coopPlanApp, lstInfo);
            lstInfo.setPartnerStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
            coopPartnerLstInfoMapper.insert(lstInfo);
        } else {
            BeanUtils.beanCopy(coopPlanApp, lstInfo);
            lstInfo.setPartnerStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
            coopPartnerLstInfoMapper.updateByPrimaryKey(lstInfo);
        }

        // 针对流程到办结节点，进行以下处理,批复台账申请修改后需同步到方案台账
        QueryModel model = new QueryModel();
        model.getCondition().put(CoopPlanAppConstant.COOP_PLAN_NO, coopPlanApp.getCoopPlanNo());
        List<CoopPlanAccInfo> coopPlanAccInfos = coopPlanAccInfoMapper.selectByModel(model);
        CoopPlanAccInfo info = new CoopPlanAccInfo();
        BeanUtils.beanCopy(coopPlanApp, info);
        info.setTotlCoopLmtAmt(coopPlanApp.getTotlCoopLmtAmt());
        info.setCoopPlanStatus(CoopPlanAppConstant.COOP_PLAN_STATUS_SX);
        BeanUtils.beanCopy(coopReplyAcc, info);
        if(coopPlanAccInfos == null || coopPlanAccInfos.size() == 0){
            info.setPkId(UUID.randomUUID().toString().replaceAll(CoopPlanAppConstant.REPLACE_LINE, CoopPlanAppConstant.REPLACE_SPACE));
            coopPlanAccInfoMapper.insert(info);
        }else{
            String pkId = coopPlanAccInfos.get(0).getPkId();
            info.setPkId(pkId);
            //变更-更新合作方案台账的流水号
            info.setSerno(serno);
            info.setImageNo(serno);
            coopPlanAccInfoMapper.updateByPrimaryKey(info);
        }
    }

    /**
     * 资料未全生成影像补扫任务
     * @author jijian_yx
     * @date 2021/8/27 16:56
     **/
    public void createImageSpplInfo(String serno, String bizType, String instanceId,String iqpName) {
        //查询审批结果
        BusinessInformation businessInformation = businessInformationService.selectByPrimaryKey(serno, bizType);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            // 资料不齐全
            CoopPlanApp coopPlanApp = coopPlanAppMapper.selectByPrimaryKey(serno);
            DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
            docImageSpplClientDto.setBizSerno(serno);// 关联业务流水号
            docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
            docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_04);// 影像补扫类型 04:合作方准入影像补扫
            docImageSpplClientDto.setCusId(coopPlanApp.getPartnerNo());// 客户号
            docImageSpplClientDto.setCusName(coopPlanApp.getPartnerName());// 客户名称
            docImageSpplClientDto.setInputId(coopPlanApp.getManagerId());// 登记人
            docImageSpplClientDto.setInputBrId(coopPlanApp.getManagerBrId());// 登记机构
            docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);

            //生成首页提醒,对象：责任机构下所有资料扫描岗FZH03
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto = new GetUserInfoByDutyCodeDto();
            getUserInfoByDutyCodeDto.setDutyCode("FZH03");
            getUserInfoByDutyCodeDto.setPageNum(1);
            getUserInfoByDutyCodeDto.setPageSize(300);
            List<AdminSmUserDto> adminSmUserDtoList = commonService.getUserInfoByDutyCodeDtoNew(getUserInfoByDutyCodeDto);
            if (null != adminSmUserDtoList && adminSmUserDtoList.size() > 0) {
                for (AdminSmUserDto adminSmUserDto : adminSmUserDtoList) {
                    if (Objects.equals(adminSmUserDto.getOrgId(), coopPlanApp.getManagerBrId())) {
                        ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                        receivedUserDto.setReceivedUserType("1");// 发送人员类型
                        receivedUserDto.setUserId(adminSmUserDto.getUserCode());// 客户经理工号
                        receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                        receivedUserList.add(receivedUserDto);
                    }
                }
            }
            MessageSendDto messageSendDto = new MessageSendDto();
            Map<String, String> map = new HashMap<>();
            map.put("cusName", coopPlanApp.getPartnerName());
            map.put("iqpName", iqpName);
            messageSendDto.setMessageType("MSG_DA_M_0001");
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        }
    }

    /**
     * 根据流水号更新保证金账号信息
     * @param serno
     * @return
     */
    public int updateBailAccInfoEmptyBySerno(String serno) {
        return coopPlanAppMapper.updateBailAccInfoEmptyBySerno(serno);
    }
}
