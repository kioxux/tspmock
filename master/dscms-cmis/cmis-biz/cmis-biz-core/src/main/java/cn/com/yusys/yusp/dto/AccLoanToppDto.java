package cn.com.yusys.yusp.dto;

import javax.persistence.Column;

public class AccLoanToppDto {

    /** 借据编号 **/
    private String billNo;

    /** 全局流水号 **/
    private String serno;

    /** 合同编号 **/
    private String contNo;

    /** 客户编号 **/
    private String cusId;

    /** 客户编号 **/
    private String cusName;

    /** 交易对手账号 **/
    private String toppAcctNo;

    /** 交易对手名称 **/
    private String toppName;

    /** 交易对手金额 **/
    private String toppAmt;

    /** 开户行行号 **/
    private String acctsvcrNo;

    /** 开户行名称 **/
    private String acctsvcrName;

    public String getAcctsvcrNo() {
        return acctsvcrNo;
    }

    public void setAcctsvcrNo(String acctsvcrNo) {
        this.acctsvcrNo = acctsvcrNo;
    }

    public String getAcctsvcrName() {
        return acctsvcrName;
    }

    public void setAcctsvcrName(String acctsvcrName) {
        this.acctsvcrName = acctsvcrName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getToppAcctNo() {
        return toppAcctNo;
    }

    public void setToppAcctNo(String toppAcctNo) {
        this.toppAcctNo = toppAcctNo;
    }

    public String getToppName() {
        return toppName;
    }

    public void setToppName(String toppName) {
        this.toppName = toppName;
    }

    public String getToppAmt() {
        return toppAmt;
    }

    public void setToppAmt(String toppAmt) {
        this.toppAmt = toppAmt;
    }
}
