/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.BusinessInformation;
import cn.com.yusys.yusp.service.BusinessInformationService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BusinessInformationResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-10 16:59:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/businessinformation")
public class BusinessInformationResource {
    @Autowired
    private BusinessInformationService businessInformationService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BusinessInformation>> query() {
        QueryModel queryModel = new QueryModel();
        List<BusinessInformation> list = businessInformationService.selectAll(queryModel);
        return new ResultDto<List<BusinessInformation>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BusinessInformation>> index(QueryModel queryModel) {
        List<BusinessInformation> list = businessInformationService.selectByModel(queryModel);
        return new ResultDto<List<BusinessInformation>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BusinessInformation> create(@RequestBody BusinessInformation businessInformation) throws URISyntaxException {
        businessInformationService.insert(businessInformation);
        return new ResultDto<BusinessInformation>(businessInformation);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BusinessInformation businessInformation) throws URISyntaxException {
        int result = businessInformationService.update(businessInformation);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String bizId, String bizType) {
        int result = businessInformationService.deleteByPrimaryKey(bizId, bizType);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/updateinfComplete")
    protected ResultDto<Integer> updateinfComplete(@RequestBody Map<String,String> params) throws URISyntaxException {
        int result = businessInformationService.updateComplete(params);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/updatesignet")
    protected ResultDto<Integer> updatesignet(@RequestBody Map<String,String> params) throws URISyntaxException {
        int result = businessInformationService.updatesignet(params);
        return new ResultDto<Integer>(result);
    }


}
