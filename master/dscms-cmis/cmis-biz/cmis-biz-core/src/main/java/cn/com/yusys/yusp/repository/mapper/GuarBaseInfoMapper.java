/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.xdls0006.resp.GuarList;
import cn.com.yusys.yusp.dto.server.xdxw0025.req.Xdxw0025DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarBaseInfoMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2020-12-01 21:10:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface GuarBaseInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    GuarBaseInfo selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GuarBaseInfo> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(GuarBaseInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(GuarBaseInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(GuarBaseInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(GuarBaseInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 获取押品信息
     * @param guarBaseInfoDto
     * @return
     */
    List<GuarBaseInfoDto> getGuarBaseInfo(GuarBaseInfoDto guarBaseInfoDto);

    /**
     * 获取押品数
     * @param guarTypeCd
     * @return
     */
    int getGuarBaseCount(@Param("guarTypeCd") String guarTypeCd);

    /**
     * 根据押品统一代码获取押品基本和担保分类信息
     * @param guarNo
     * @return
     */
    GuarBaseInfoDto getGuarBaseAndGuarClassByGuarNo(@Param("guarNo") String guarNo);

    /**
     * 逻辑删除押品信息，只改变押品操作状态为删除
     * @param guarNo
     * @return
     */
    int deleteGuarByGuarNo(@Param("guarNo") String guarNo);

    /**
     * 更新押品所在业务阶段
     * @param guarBaseInfoClientDto
     * @return
     */
    int updateGuarBusistate(GuarBaseInfoClientDto guarBaseInfoClientDto);

    /**
     * 根据担保分类代码查询押品信息
     * @param params
     * @return
     */
    List<GuarBaseInfoDto> selectGuarBaseInfoByParams(Map params);

    /**
     * 交易描述：根据客户名查询抵押物类型
     *
     * @param QueryMap
     * @return
     * @throws Exception
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xddb0020.resp.List> selectGuarBaseInfoByCusName(Map QueryMap);

    /**
     * 重摇获取押品信息
     * @param guarRelotInfoClientDto
     * @return
     *//*
    List<GuarEvalRelotResultDto> getGuarRelotInfo(GuarRelotInfoClientDto guarRelotInfoClientDto);*/

    /**
     * 交易描述：押品信息查询
     *
     * @param QueryMap
     * @return
     * @throws Exception
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xddb0016.resp.List> selectGuarBaseInfoByGuarName(Map QueryMap);

    /**
     * 交易描述：查询在线抵押信息
     *
     * @param model
     * @return
     * @throws Exception
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xddb0008.resp.List> selectGuarBaseInfoByManagerId(QueryModel model);

    /**
     * 根据业务流水号查询获取抵质押品信息
     * @param queryModel
     * @return
     */
    List<GuarBizRelGuarBaseDto> selectByIqpSernoModel(QueryModel queryModel);

    /**
     * @函数描述:获取业务与押品关系表当前业务流水号下optype为01,02的抵押物信息
     * @算法描述:
     * @创建者：zhnagliang15
     */
    List<GuarBizRelGuarBaseDto> querFddGuarinforel(QueryModel queryModel);

    /**
     * @方法名称: selectByModel
     * @方法描述: 根据担保合同查询所有押品信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GuarBaseInfo> selectByGuarContNoModel(QueryModel model);

    /**
     * @方法名称: selectByModel
     * @方法描述: 根据担保合同查询所有押品信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GuarBaseInfo> selectDetailsByGuarContNoModel(QueryModel model);

    /**
     * 根据借据号查询抵质押无信息
     * @param xdxw0025DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0025.resp.List> getGuarBaseListByBillNo(Xdxw0025DataReqDto xdxw0025DataReqDto);

    /**
     * 根据押品编号查询客户经理工号
     * @param guarNo
     * @return
     */
    String getGuarInfoManageridByGuarNo(String guarNo);
    /**
     * 根据押品编号查询对应的押品信息
     * @param ypbhs
     * @return
     */
    List<GuarBaseInfo> selectByGuarNo(@Param("ypbhs") List<String> ypbhs);

    /**
     * 押品编号查询对应的押品信息
     * @param guarid
     * @return
     */
    GuarBaseInfo queryBaseInfoByGuarId(@Param("guarid")String guarid);


    /**
     * 不动产取子项中房产的数据
     * @param guarid
     * @return
     */
    GuarBaseInfo getGuarInfoByGuarId(@Param("guarid")String guarid);

    /**
     * 批量插入
     * @param guarBaseInfoList
     * @return
     */
    int insertGuarBaseInfoList(@Param("list")List<GuarBaseInfo> guarBaseInfoList);

    /**
     * @函数名称:getGuarBaseInfoByGuarNo
     * @函数描述:根据押品编号获取押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    GuarBaseInfo getGuarBaseInfoByGuarNo(@Param("guarNo") String guarNo);

    /**
     * @函数名称:selectGuarBaseInfoByGuarContNo
     * @函数描述:根据担保合同编号获取押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    List<GuarBaseInfo> selectGuarBaseInfoByGuarContNo(QueryModel queryModel);

    /**
     * @函数名称:selectGrtGuarContRelByGuarContNo
     * @函数描述:根据担保合同编号获取押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    List<GuarBaseInfoDto2> selectGrtGuarContRelByGuarContNo(QueryModel queryModel);

    /**
     * @函数名称:selectGuarBaseInfoForMortgageLogOut
     * @函数描述:根据担保合同编号获取押品信息(针对抵押注销)
     * @参数与返回说明:
     * @算法描述:
     */
    List<GuarBaseInfoDto> selectGuarBaseInfoForMortgageLogOut(QueryModel queryModel);

    /**
     * @函数名称:queryGuarInfoIsUnderLmt
     * @函数描述:根据业务流水号查询授信项下的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    List<GuarBaseInfo> queryGuarInfoIsUnderLmt(QueryModel queryModel);

    /**
     * @方法名称: querySxkdGrtAmtBySubSerno
     * @方法描述: 获取省心快贷授信下担保品金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal querySxkdGrtAmtBySubSerno(String subSerno);


    /**
     * @方法名称: selectGuarBaseInfoBySubSerno
     * @方法描述: 根据授信分项流水获取押品基本信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GuarBaseInfo> selectGuarBaseInfoBySubSerno(String subSerno);
    /**
     * 查询白领贷额度信息
     *
     * @param params
     * @return
     */
    java.util.List<GuarList> selectMortInfoBySerno(HashMap<String,String> params);
    /**
     * @函数名称:deleteGuarBaseInfoByGuarContNo
     * @函数描述:根据担保合同编号关联删除担保合同下押品
     * @参数与返回说明:guarContNo 担保合同编号
     * @算法描述:
     */
    int deleteGuarBaseInfoByGuarContNo(String guarContNo);

    /**
     * 根据押品编号查询客户经理工号
     *
     * @param guarNo
     * @return
     */
    String queryManagerIdByContNo(String guarNo);

    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    /**
     * @Description:根据押品编号查询是否存在业务信息
     * @Author: zhangliang15
     * @Date: 2021/8/9 23:40
     * @param guarNo: 押品编号
     * @return: int
     **/
    int selectContByGuarNo(@Param("guarNo") String guarNo);

    /**
     * @Description:根据担保合同号查询车辆抵押的个数
     * @param guarContNo: 担保合同号
     * @return: String
     **/
    String queryCountByContNo(@Param("guarContNo") String guarContNo);
    /**
     * @函数名称:queryExistsNoCK
     * @函数描述:根据参数查询是否存在押品信息（非正常出库和非部分出库的）
     * @参数与返回说明:
     * @算法描述:
     */
    List<GuarBaseInfo>  queryExistsNoCK(QueryModel queryModel);
    /**
     * @函数名称:queryExistsWrk
     * @函数描述:根据参数查询是否存在押品信息（未入库）
     * @参数与返回说明:
     * @算法描述:
     */
    List<GuarBaseInfo>  queryExistsWrk(QueryModel queryModel);

    /**
     * @函数名称:deleteGuarBaseInfoByRightCertNo
     * @函数描述:根据权证编号，删除担保合同下押品
     * @参数与返回说明:权证编号
     * @算法描述:
     */
    int deleteGuarBaseInfoByRightCertNo(GuarBaseInfo guarBaseInfo);

    /**
     * @函数名称:queryExistsYrk
     * @函数描述:根据参数查询是否存在押品信息（已入库）
     * @参数与返回说明:
     * @算法描述:
     */
    String queryExistsYrk(QueryModel queryModel);
    /**
     * @函数名称:queryYpStatus
     * @函数描述:根据参数查询押品状态
     * @参数与返回说明:
     * @算法描述:
     */
    String queryYpStatus(QueryModel queryModel);
    /**
     * @函数名称:queryExistsNork
     * @函数描述:根据参数权证编号，查询是否存在押品信息【非（未入库、正常出库、部分出库）】
     * @参数与返回说明:
     * @算法描述:
     */
    String  queryExistsNork(QueryModel queryModel);

    /**
     * @函数名称:queryYpnoYpnameByBookSerno
     * @函数描述:通过入库流水号，查询押品所有人编号、押品所有人名称、抵质押物名称
     * @参数与返回说明:
     * @算法描述:
     */

    List<GuarBaseInfo>  queryYpnoYpnameByBookSerno(String bookSerno);


    /**
     * @方法名称: updateYpstatusByGuarNo
     * @方法描述: 通过入库流水号，更新抵质押基本信息表的押品状态为出库正常10008 ，票据池状态为出池2，出池日期
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateYpstatusByGuarNo(QueryModel queryModel);

    /**
     * @函数名称:queryBaseInfobyGuarContOn
     * @函数描述:通过合同编号，获取抵质押基本信息
     * @参数与返回说明:
     * @算法描述:
     */

    List<GuarBaseInfo>  queryBaseInfobyGuarContOn(String guarContNo);

    /**
     * 根据申请流水号查询抵质押品信息
     * @param map
     * @return
     */
    List<LmtReViewPortDto> queryGuarBaseInfo(Map map);

    /**
     * 校验该统一押品编号是已否存在
     * @param guarNoList
     * @return
     */
    List<String> selectByGuarNoKList(@Param("list") List<String> guarNoList);

    /**
     * 根据统一押品编号集合查询
     * @param guarNoList
     * @return
     */
    List<GuarBaseInfo> selectByGuarNoList(@Param("list")List<String> guarNoList);

    /**
     * @函数名称:queryGuarBaseInfoDataByParams
     * @函数描述:根据业务流水号查询授信项下的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    List<GuarBaseInfo> queryGuarBaseInfoDataByParams(QueryModel queryModel);

    /**
     * @函数名称:queryGuarBaseInfoByWarrantInSerno
     * @函数描述:根据权证入库流水号查询权证入库里的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    List<GuarBaseInfo> queryGuarBaseInfoByWarrantInSerno(QueryModel queryModel);

    /**
     * @函数名称:queryGuarBaseInfoByCoreGuarantyNo
     * @函数描述:根据核心担保编号查询押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    List<GuarBaseInfo> queryGuarBaseInfoByCoreGuarantyNo(QueryModel queryModel);

    /**
     * @函数名称:queryGuarBaseInfoDataByGuarContNo
     * @函数描述:根据担保合同编号查询关联的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    List<GuarBaseInfo> queryGuarBaseInfoDataByGuarContNo(QueryModel queryModel);

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：刘权
     */

    List<GuarBaseInfo> selectByCusId(QueryModel model);

    /**
     * @方法名称: selectByGuarNo
     * @方法描述: 根据押品编号查询抵质押信息
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：刘权
     */

    List<GuarBaseInfo> selectBaseInfoByGuarNo(QueryModel model);

    /**
     * @方法名称: selectContByContNos
     * @方法描述: 通过担保合同编号查询抵质押基本信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GuarBaseInfoClientDto> selectByGuarBaseInfo(String guarContNo);

    /**
     * @函数名称:queryYpRegState
     * @函数描述:根据参数查询押品出入库状态
     * @参数与返回说明:
     * @算法描述:
     */
    String queryYpRegState(QueryModel querymodel);

    /**
     * @函数名称:selectByContNo
     * @函数描述:通过业务合同号查询押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    List<GuarBaseInfo> selectByContNo(String contNo);

    /**
     * 根据押品编号查询记录数
     * @param guarNo
     * @return
     */
    int countByGuarNo(@Param("guarNo") String guarNo);

    String selectGuarNoBySerno(@Param("serno") String serno);

    List<GuarBaseInfoRelDto> queryGuarBaseInfoRelDtoByParams(QueryModel queryModel);

    /**
     * @方法名称: queryGuarBaseInfoByLmtSerno
     * @方法描述: 通过授信申请流水号查询申请时的抵质押基本信息
     * @参数与返回说明:
     * @创建人： css
     */

    List<Map> queryGuarBaseInfoByLmtSerno(@Param("serno") String serno);

    /**
     * 根据统一押品编号查询
     * @param guarNo
     * @return
     */
    GuarBaseInfo selectInfoByGuarNo(@Param("guarNo")String guarNo);

    /**
     * 根据入参查询
     * @param map
     * @return
     */
    GuarBaseInfo queryByGuarNoAndGrtFlag(HashMap<String,String> map);


    /**
     * 根据业务编号查询项下的所有抵质押评估金额之和
     * @param serno
     * @return
     */
    BigDecimal queryEvalAmtSumBySerno(@Param("serno")String serno);

    /**
     * 根据担保业务编号查询项下的所有抵质押我行可用价值之和
     * @param serno
     * @return
     */
    BigDecimal queryMaxMortagageAmtSumBySerno(@Param("serno")String serno);

    /**
     * @param serno
     * @return int
     * @author hubp
     * @date 2021/10/9 23:13
     * @version 1.0.0
     * @desc  根据流水号查询是否存在质押存单
     * @修改历史: 修改时间    修改人员    修改原因
     */
    int getBaseInfo(@Param("serno")String serno);

    /**
     * 根据授信分项编号查询关联的押品
     * @param queryModel
     * @return
     */
    String selectGuarNosByLmtAccNo(QueryModel queryModel);
}