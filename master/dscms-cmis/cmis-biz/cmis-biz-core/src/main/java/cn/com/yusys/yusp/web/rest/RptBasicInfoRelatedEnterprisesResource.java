/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptBasicInfoRelatedEnterprises;
import cn.com.yusys.yusp.service.RptBasicInfoRelatedEnterprisesService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoRelatedEnterprisesResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-10 17:17:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptbasicinforelatedenterprises")
public class RptBasicInfoRelatedEnterprisesResource {
    @Autowired
    private RptBasicInfoRelatedEnterprisesService rptBasicInfoRelatedEnterprisesService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptBasicInfoRelatedEnterprises>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptBasicInfoRelatedEnterprises> list = rptBasicInfoRelatedEnterprisesService.selectAll(queryModel);
        return new ResultDto<List<RptBasicInfoRelatedEnterprises>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptBasicInfoRelatedEnterprises>> index(QueryModel queryModel) {
        List<RptBasicInfoRelatedEnterprises> list = rptBasicInfoRelatedEnterprisesService.selectByModel(queryModel);
        return new ResultDto<List<RptBasicInfoRelatedEnterprises>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptBasicInfoRelatedEnterprises> show(@PathVariable("pkId") String pkId) {
        RptBasicInfoRelatedEnterprises rptBasicInfoRelatedEnterprises = rptBasicInfoRelatedEnterprisesService.selectByPrimaryKey(pkId);
        return new ResultDto<RptBasicInfoRelatedEnterprises>(rptBasicInfoRelatedEnterprises);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptBasicInfoRelatedEnterprises> create(@RequestBody RptBasicInfoRelatedEnterprises rptBasicInfoRelatedEnterprises) throws URISyntaxException {
        rptBasicInfoRelatedEnterprisesService.insert(rptBasicInfoRelatedEnterprises);
        return new ResultDto<RptBasicInfoRelatedEnterprises>(rptBasicInfoRelatedEnterprises);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptBasicInfoRelatedEnterprises rptBasicInfoRelatedEnterprises) throws URISyntaxException {
        int result = rptBasicInfoRelatedEnterprisesService.update(rptBasicInfoRelatedEnterprises);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptBasicInfoRelatedEnterprisesService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptBasicInfoRelatedEnterprisesService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptBasicInfoRelatedEnterprises>> selectByModel(@RequestBody QueryModel model) {
        return new ResultDto<List<RptBasicInfoRelatedEnterprises>>(rptBasicInfoRelatedEnterprisesService.selectByModel(model));
    }

    @PostMapping("/deleteEnterprises")
    protected ResultDto<Integer> deleteEnterprises(@RequestBody RptBasicInfoRelatedEnterprises rptBasicInfoRelatedEnterprises) {
        String pkId = rptBasicInfoRelatedEnterprises.getPkId();
        return new ResultDto<Integer>(rptBasicInfoRelatedEnterprisesService.deleteByPrimaryKey(pkId));
    }

    @PostMapping("/insertEnterprises")
    protected ResultDto<Integer> insertEnterprises(@RequestBody RptBasicInfoRelatedEnterprises rptBasicInfoRelatedEnterprises) {
        rptBasicInfoRelatedEnterprises.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptBasicInfoRelatedEnterprisesService.insertSelective(rptBasicInfoRelatedEnterprises));
    }

    @PostMapping("/updateEnterprises")
    protected ResultDto<Integer> updateEnterprises(@RequestBody RptBasicInfoRelatedEnterprises rptBasicInfoRelatedEnterprises) {
        return new ResultDto<Integer>(rptBasicInfoRelatedEnterprisesService.updateSelective(rptBasicInfoRelatedEnterprises));
    }
    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptBasicInfoRelatedEnterprises>> selectBySerno(@RequestBody Map map){
        String serno = map.get("serno").toString();
        return new ResultDto<List<RptBasicInfoRelatedEnterprises>>(rptBasicInfoRelatedEnterprisesService.selectBySerno(serno));
    }
}
