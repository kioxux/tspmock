package cn.com.yusys.yusp.service.server.xdtz0038;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.server.xdtz0038.req.Xdtz0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0038.resp.List;
import cn.com.yusys.yusp.dto.server.xdtz0038.resp.Xdtz0038DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑类:台账信息通用列表查询
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdtz0038Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0038Service.class);
    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 交易码：xdtz0038
     * 交易描述：台账信息通用列表查询
     *
     * @param xdtz0038DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0038DataRespDto xdtz0038(Xdtz0038DataReqDto xdtz0038DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, JSON.toJSONString(xdtz0038DataReqDto));
        Xdtz0038DataRespDto xdtz0038DataRespDto = new Xdtz0038DataRespDto();
        try {
            // 从xdtz0038DataReqDto获取业务值进行业务逻辑处理
            String cusId = xdtz0038DataReqDto.getCusId();//客户编号
            String centNo = xdtz0038DataReqDto.getCentNo();//证件号
            String billNo = xdtz0038DataReqDto.getBillNo();//借据编号
            String cusName = xdtz0038DataReqDto.getCusName();//客户名称
            String prdNo = xdtz0038DataReqDto.getPrdNo();//产品编号
            String prdName = xdtz0038DataReqDto.getPrdName();//产品名称
            String chnl = xdtz0038DataReqDto.getChnl();//渠道来源
            String contNo = xdtz0038DataReqDto.getContNo();//合同编号
            String cnContNo = xdtz0038DataReqDto.getCnContNo();//中文合同编号
            int startPageNum = Integer.parseInt(StringUtils.isBlank(xdtz0038DataReqDto.getStartPageNum()) ? "0" : xdtz0038DataReqDto.getStartPageNum());//开始页数
            int pageSize = Integer.parseInt(StringUtils.isBlank(xdtz0038DataReqDto.getPageSize()) ? "5" : xdtz0038DataReqDto.getPageSize());//条数
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId", cusId);//客户编号
            queryModel.addCondition("cusName", cusName);//客户名称
            queryModel.addCondition("prdId", prdNo);//产品编号
            queryModel.addCondition("prdName", prdName);//产品名称
            queryModel.addCondition("contNo", contNo);//合同编号z
            PageHelper.startPage(startPageNum, pageSize);
            java.util.List<AccLoan> accLoanList = Optional.ofNullable(accLoanMapper.selectByModel(queryModel)).orElse(new ArrayList<>());
            long total = new PageInfo<>(accLoanList).getTotal();
            xdtz0038DataRespDto.setTotalQnt(String.valueOf(total));// 总数
            java.util.List<List> xdtz0038DataRespList = new ArrayList<>();
            if (Objects.nonNull(accLoanList)) {
                for (AccLoan accLoan : accLoanList) {
                    List list = new List();
                    BeanUtils.copyProperties(accLoan, list);
                    list.setLoanBal(accLoan.getLoanBalance());
                    xdtz0038DataRespList.add(list);
                }
                xdtz0038DataRespDto.setList(xdtz0038DataRespList);
            }
//            list.setPkId(StringUtils.EMPTY);// 主键
//            list.setBillNo(StringUtils.EMPTY);// 借据编号
//            list.setContNo(StringUtils.EMPTY);// 合同编号
//            list.setCusId(StringUtils.EMPTY);// 客户编号
//            list.setCusName(StringUtils.EMPTY);// 客户名称
//            list.setPrdId(StringUtils.EMPTY);// 产品编号
//            list.setPrdName(StringUtils.EMPTY);// 产品名称
//            list.setGuarMode(StringUtils.EMPTY);// 担保方式
//            list.setLoanModal(StringUtils.EMPTY);// 贷款形式
//            list.setContCurType(StringUtils.EMPTY);// 贷款发放币种
//            list.setExchangeRate(new BigDecimal(0L));// 汇率
//            list.setLoanAmt(new BigDecimal(0L));// 贷款金额
//            list.setLoanBal(new BigDecimal(0L));// 贷款余额
//            list.setExchangeRmbAmt(new BigDecimal(0L));// 折合人民币金额
//            list.setExchangeRmbBal(new BigDecimal(0L));// 折合人民币余额
//            list.setInnerDebitInterest(new BigDecimal(0L));// 表内欠息
//            list.setOffDebitInterest(new BigDecimal(0L));// 表外欠息
//            list.setLoanStartDate(StringUtils.EMPTY);// 贷款起始日
//            list.setLoanEndDate(StringUtils.EMPTY);// 贷款到期日
//            list.setLoanTerm(StringUtils.EMPTY);// 贷款期限
//            list.setLoanTermUnit(StringUtils.EMPTY);// 贷款期限单位
//            list.setExtTimes(StringUtils.EMPTY);// 展期次数
//            list.setOverdueDay(StringUtils.EMPTY);// 逾期天数
//            list.setOverdueTimes(StringUtils.EMPTY);// 逾期期数
//            list.setSettlDate(StringUtils.EMPTY);// 结清日期
//            list.setRateAdjMode(StringUtils.EMPTY);// 利率调整方式
//            list.setIsSegInterest(StringUtils.EMPTY);// 是否分段计息
//            list.setLprRateIntval(StringUtils.EMPTY);// LPR授信利率区间
//            list.setCurtLprRate(new BigDecimal(0L));// 当前LPR利率
//            list.setRateFloatPoint(new BigDecimal(0L));// 浮动点数
//            list.setExecRateYear(new BigDecimal(0L));// 执行利率(年)
//            list.setOverdueRatePefloat(new BigDecimal(0L));// 逾期利率浮动比
//            list.setOverdueExecRate(new BigDecimal(0L));// 逾期执行利率(年利率)
//            list.setCiRatePefloat(new BigDecimal(0L));// 复息利率浮动比
//            list.setCiExecRate(new BigDecimal(0L));// 复息执行利率(年利率)
//            list.setRateAdjType(StringUtils.EMPTY);// 利率调整选项
//            list.setNextRateAdjInterval(StringUtils.EMPTY);// 下一次利率调整间隔
//            list.setNextRateAdjUnit(StringUtils.EMPTY);// 下一次利率调整间隔单位
//            list.setFirstAdjDate(StringUtils.EMPTY);// 第一次调整日
//            list.setRepayMode(StringUtils.EMPTY);// 还款方式
//            list.setEiIntervalCycle(StringUtils.EMPTY);// 结息间隔周期
//            list.setEiIntervalUnit(StringUtils.EMPTY);// 结息间隔周期单位
//            list.setDeductType(StringUtils.EMPTY);// 扣款方式
//            list.setDeductDay(StringUtils.EMPTY);// 扣款日
//            list.setLoanPayoutAccno(StringUtils.EMPTY);// 贷款发放账号
//            list.setLoanPayoutSubNo(StringUtils.EMPTY);// 贷款发放账号子序号
//            list.setPayoutAcctName(StringUtils.EMPTY);// 发放账号名称
//            list.setIsBeEntrustedPay(StringUtils.EMPTY);// 是否受托支付
//            list.setRepayAccno(StringUtils.EMPTY);// 贷款还款账号
//            list.setRepaySubAccno(StringUtils.EMPTY);// 贷款还款账户子序号
//            list.setRepayAcctName(StringUtils.EMPTY);// 还款账户名称
//            list.setLoanTer(StringUtils.EMPTY);// 贷款投向
//            list.setLoanUseType(StringUtils.EMPTY);// 借款用途类型
//            list.setSubjectNo(StringUtils.EMPTY);// 科目号
//            list.setAgriType(StringUtils.EMPTY);// 农户类型
//            list.setAgriLoanTer(StringUtils.EMPTY);// 涉农贷款投向
//            list.setLoanPromiseFlag(StringUtils.EMPTY);// 贷款承诺标志
//            list.setLoanPromiseType(StringUtils.EMPTY);// 贷款承诺类型
//            list.setIsSbsy(StringUtils.EMPTY);// 是否贴息
//            list.setSbsyDepAccno(StringUtils.EMPTY);// 贴息人存款账号
//            list.setSbsyPerc(new BigDecimal(0L));// 贴息比例
//            list.setSbysEnddate(StringUtils.EMPTY);// 贴息到期日
//            list.setIsUseLmtAmt(StringUtils.EMPTY);// 是否使用授信额度
//            list.setLmtAccNo(StringUtils.EMPTY);// 授信额度编号
//            list.setReplyNo(StringUtils.EMPTY);// 批复编号
//            list.setLoanType(StringUtils.EMPTY);// 贷款类别
//            list.setIsPactLoan(StringUtils.EMPTY);// 是否落实贷款
//            list.setIsGreenIndustry(StringUtils.EMPTY);// 是否绿色产业
//            list.setIsOperPropertyLoan(StringUtils.EMPTY);// 是否经营性物业贷款
//            list.setIsSteelLoan(StringUtils.EMPTY);// 是否钢贸行业贷款
//            list.setIsStainlessLoan(StringUtils.EMPTY);// 是否不锈钢行业贷款
//            list.setIsPovertyReliefLoan(StringUtils.EMPTY);// 是否扶贫贴息贷款
//            list.setIsLaborIntenSbsyLoan(StringUtils.EMPTY);// 是否劳动密集型小企业贴息贷款
//            list.setGoverSubszHouseLoan(StringUtils.EMPTY);// 保障性安居工程贷款
//            list.setEngyEnviProteLoan(StringUtils.EMPTY);// 项目贷款节能环保
//            list.setIsCphsRurDelpLoan(StringUtils.EMPTY);// 是否农村综合开发贷款标志
//            list.setRealproLoan(StringUtils.EMPTY);// 房地产贷款
//            list.setRealproLoanRate(StringUtils.EMPTY);// 房产开发贷款资本金比例
//            list.setGuarDetailMode(StringUtils.EMPTY);// 担保方式细分
//            list.setFinaBrId(StringUtils.EMPTY);// 账务机构编号
//            list.setFinaBrIdName(StringUtils.EMPTY);// 账务机构名称
//            list.setDisbOrgNo(StringUtils.EMPTY);// 放款机构编号
//            list.setDisbOrgName(StringUtils.EMPTY);// 放款机构名称
//            list.setFiveClass(StringUtils.EMPTY);// 五级分类
//            list.setTenClass(StringUtils.EMPTY);// 十级分类
//            list.setClassDate(StringUtils.EMPTY);// 分类日期
//            list.setAccStatus(StringUtils.EMPTY);// 台账状态
//            list.setBelgLine(StringUtils.EMPTY);// 所属条线
//            list.setInputId(StringUtils.EMPTY);// 登记人
//            list.setInputBrId(StringUtils.EMPTY);// 登记机构
//            list.setInputDate(StringUtils.EMPTY);// 登记日期
//            list.setUpdId(StringUtils.EMPTY);// 最近修改人
//            list.setUpdBrId(StringUtils.EMPTY);// 最近修改机构
//            list.setUpdDate(StringUtils.EMPTY);// 最近修改日期
//            list.setManagerId(StringUtils.EMPTY);// 主管客户经理
//            list.setManagerBrId(StringUtils.EMPTY);// 主管机构
//            list.setCreateTime(StringUtils.EMPTY);// 创建时间
//            list.setUpdateTime(StringUtils.EMPTY);// 修改时间
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, JSON.toJSONString(xdtz0038DataRespDto));
        return xdtz0038DataRespDto;
    }
}
