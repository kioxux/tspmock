package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo;
import cn.com.yusys.yusp.dto.DutyAndRoleDto;
import cn.com.yusys.yusp.service.ManagerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/27 10:09
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@RestController
@Api(tags = "客户经理信息")
@RequestMapping("/api/managerresource")
public class ManagerResource {

    @Autowired
    private ManagerService managerService;

    /**
     * @param lmtSurveyReportMainInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.AdminSmUserDto>
     * @author hubp
     * @date 2021/7/27 10:11
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/getmanagerinfo")
    protected ResultDto<DutyAndRoleDto> query(@RequestBody LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        return new ResultDto<>(managerService.getManagerInfo(lmtSurveyReportMainInfo.getManagerId()));
    }
}
