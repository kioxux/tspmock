package cn.com.yusys.yusp.web.server.xdtz0060;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0060.req.Xdtz0060DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0060.resp.Xdtz0060DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0060.Xdtz0060Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询我行有未结清贷款
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0060:查询我行有未结清贷款")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0060Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0060Resource.class);

    @Autowired
    private Xdtz0060Service xdtz0060Service;

    /**
     * 交易码：xdtz0060
     * 交易描述：查询我行有未结清贷款
     *
     * @param xdtz0060DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询我行有未结清贷款")
    @PostMapping("/xdtz0060")
    protected @ResponseBody
    ResultDto<Xdtz0060DataRespDto> xdtz0060(@Validated @RequestBody Xdtz0060DataReqDto xdtz0060DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, JSON.toJSONString(xdtz0060DataReqDto));
        Xdtz0060DataRespDto xdtz0060DataRespDto = new Xdtz0060DataRespDto();// 响应Dto:查询我行有未结清贷款
        ResultDto<Xdtz0060DataRespDto> xdtz0060DataResultDto = new ResultDto<>();

        try {
            // 从xdtz0060DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, JSON.toJSONString(xdtz0060DataReqDto));
            xdtz0060DataRespDto = xdtz0060Service.xdtz0060(xdtz0060DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, JSON.toJSONString(xdtz0060DataRespDto));
            // 封装xdtz0060DataResultDto中正确的返回码和返回信息
            xdtz0060DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0060DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, e.getMessage());
            // 封装xdtz0060DataResultDto中异常返回码和返回信息
            xdtz0060DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0060DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0060DataRespDto到xdtz0060DataResultDto中
        xdtz0060DataResultDto.setData(xdtz0060DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, JSON.toJSONString(xdtz0060DataResultDto));
        return xdtz0060DataResultDto;
    }
}
