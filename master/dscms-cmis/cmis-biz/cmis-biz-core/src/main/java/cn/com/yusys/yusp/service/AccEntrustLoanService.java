/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccEntrustLoan;
import cn.com.yusys.yusp.repository.mapper.AccEntrustLoanMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.AccEntrustLoanVo;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AccEntrustLoanService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-02 21:23:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AccEntrustLoanService {

    @Autowired
    private AccEntrustLoanMapper accEntrustLoanMapper;
    @Autowired
    private CommonService commonService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccEntrustLoan selectByPrimaryKey(String pkId) {
        return accEntrustLoanMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AccEntrustLoan> selectAll(QueryModel model) {
        List<AccEntrustLoan> records = (List<AccEntrustLoan>) accEntrustLoanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccEntrustLoan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccEntrustLoan> list = accEntrustLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(AccEntrustLoan record) {
        return accEntrustLoanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(AccEntrustLoan record) {
        return accEntrustLoanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(AccEntrustLoan record) {
        return accEntrustLoanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(AccEntrustLoan record) {
        return accEntrustLoanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return accEntrustLoanMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return accEntrustLoanMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectAccLoanShow
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @author:zhanyb
     */
    public List<AccEntrustLoan> selectAccLoanShow(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccEntrustLoan> list = accEntrustLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称：selectForAccEntrustLoanInfo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/17 15:21
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<AccEntrustLoan> selectForAccEntrustLoanInfo(String cusId) {
        return accEntrustLoanMapper.selectForAccEntrustLoanInfo(cusId);
    }

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 16:01
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccEntrustLoan> selectByContNo(String contNo) {
        return accEntrustLoanMapper.selectByContNo(contNo);
    }

    /**
     * @方法名称：selectAccEntrustLoanBySernoKey
     * @方法描述：
     * @创建人：zxz
     * @创建时间：2021/5/21 16:01
     * @修改记录：修改时间 修改人员 修改时间
     */
    public AccEntrustLoan selectAccEntrustLoanBySernoKey(String serno) {
        return accEntrustLoanMapper.selectAccEntrustLoanBySernoKey(serno);
    }

    /**
     * @param contNo
     * @return countEntrustLoanCountByContNo
     * @desc 根据普通合同编号查询台账数量
     * @修改历史:
     */
    public int countEntrustLoanCountByContNo(String contNo) {
        return accEntrustLoanMapper.countEntrustLoanCountByContNo(contNo);
    }

    /**
     * @param contNo
     * @return selectLoanSumAmtByContNo
     * @desc 根据合同编号获取当前合同的委托贷款用信金额
     * @修改历史:
     */
    public BigDecimal selectLoanSumAmtByContNo(String contNo) {
        return accEntrustLoanMapper.selectLoanSumAmtByContNo(contNo);
    }


    /**
     * @方法名称: queryByBillNo
     * @方法描述: 根据借据编号查询委托贷款台账数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccEntrustLoan queryByBillNo(String billNo) {
        return accEntrustLoanMapper.queryByBillNo(billNo);
    }

    /**
     * 异步导出委托贷款台账列表数据
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportAccEntrustLoan(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            queryModeTemp.setSort("loanStartDate desc");
            String apiUrl = "/api/accentrustloan/exportAccEntrustLoan";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if (cn.com.yusys.yusp.commons.util.StringUtils.nonBlank(dataAuth)) {
                queryModeTemp.setDataAuth(dataAuth);
            }
            return selectAccEntrustLoanServiceByExcel(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(AccEntrustLoanVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    public List<AccEntrustLoanVo> selectAccEntrustLoanServiceByExcel(QueryModel model) {
        List<AccEntrustLoanVo> plaBadDebtWriteoffBillVoList = new ArrayList<>();
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccEntrustLoan> list = accEntrustLoanMapper.querymodelByCondition(model);
        PageHelper.clearPage();

        for (AccEntrustLoan accEntrustLoan : list) {
            AccEntrustLoanVo accEntrustLoanVo = new AccEntrustLoanVo();
            String orgName = OcaTranslatorUtils.getOrgName(accEntrustLoan.getManagerBrId());//责任机构
            String userName = OcaTranslatorUtils.getUserName(accEntrustLoan.getManagerId());   //责任人
            org.springframework.beans.BeanUtils.copyProperties(accEntrustLoan, accEntrustLoanVo);
            accEntrustLoanVo.setManagerBrIdName(orgName);
            accEntrustLoanVo.setManagerIdName(userName);
            plaBadDebtWriteoffBillVoList.add(accEntrustLoanVo);
        }
        return plaBadDebtWriteoffBillVoList;
    }

    /**
     * @方法名称: querymodelByCondition
     * @方法描述: 根据查询条件查询台账信息并返回
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccEntrustLoan> querymodelByCondition(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccEntrustLoan> list = accEntrustLoanMapper.querymodelByCondition(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param queryMap: 借据编号
     * @Description:根据借据编号更新委托贷款台账信息
     * @Date: 2021/6/13 23:40
     * @return: int
     **/
    public int updateAccEntrustLoanByBillNo(Map<String, String> queryMap) {
        return accEntrustLoanMapper.updateAccEntrustLoanByBillNo(queryMap);
    }

    /**
     * 风险分类审批结束更新客户未结清委托贷款台账五十级分类结果
     * @author jijian_yx
     * @date 2021/10/25 23:38
     **/
    public int updateLoanFiveAndTenClassByCusId(Map<String, String> map) {
        return accEntrustLoanMapper.updateLoanFiveAndTenClassByCusId(map);
    }
}
