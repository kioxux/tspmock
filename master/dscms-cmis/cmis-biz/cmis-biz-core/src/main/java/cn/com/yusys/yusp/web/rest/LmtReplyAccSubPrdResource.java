/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.IqpHighAmtAgrApp;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtReplyAccSub;
import cn.com.yusys.yusp.service.LmtReplyAccSubPrdService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplyAccSubPrd;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccSubPrdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-09 16:14:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplyaccsubprd")
public class LmtReplyAccSubPrdResource {
    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyAccSubPrd>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyAccSubPrd> list = lmtReplyAccSubPrdService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyAccSubPrd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyAccSubPrd>> index(QueryModel queryModel) {
        List<LmtReplyAccSubPrd> list = lmtReplyAccSubPrdService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyAccSubPrd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyAccSubPrd> show(@PathVariable("pkId") String pkId) {
        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyAccSubPrd>(lmtReplyAccSubPrd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplyAccSubPrd> create(@RequestBody LmtReplyAccSubPrd lmtReplyAccSubPrd) throws URISyntaxException {
        lmtReplyAccSubPrdService.insert(lmtReplyAccSubPrd);
        return new ResultDto<LmtReplyAccSubPrd>(lmtReplyAccSubPrd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyAccSubPrd lmtReplyAccSubPrd) throws URISyntaxException {
        int result = lmtReplyAccSubPrdService.update(lmtReplyAccSubPrd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyAccSubPrdService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyAccSubPrdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryLmtReplyAccSubPrdDataByParams
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过入参查询数据")
    @PostMapping("/querylmtreplyaccsubprddatabyparams")
    protected ResultDto<Object> queryLmtReplyAccSubPrdDataByParams(@RequestBody Map map) {
        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdDataByParams((String)map.get("cusId"));
        return new ResultDto<>(lmtReplyAccSubPrd);
    }

    /**
     * @函数名称:getLmtReplyAccSubPrdByAccSubNo
     * @函数描述:根据分项额度编号查询
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据分项额度编号查询")
    @PostMapping("/getLmtReplyAccSubPrdByAccSubNo")
    protected ResultDto<LmtReplyAccSubPrd> getLmtReplyAccSubPrdByAccSubNo(@RequestBody String  accSubNo) {
        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.getLmtReplyAccSubPrdByAccSubNo(accSubNo);
        return new ResultDto<>(lmtReplyAccSubPrd);
    }

    /**
     * @函数名称:queryLmtReplyAccSubPrdByParams
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过入参查询数据")
    @PostMapping("/querylmtreplyaccsubprdbyparams")
    protected ResultDto<Object> queryLmtReplyAccSubPrdByParams(@RequestBody Map map) {
        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryByParams(map);
        return new ResultDto<>(lmtReplyAccSubPrd);
    }


    /**
     * @函数名称:getLmtReplyAccSubDataByAccSubPrdNo
     * @函数描述:通过分项品种编号查询分项流水号
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过分项品种编号查询分项流水号")
    @PostMapping("/getlmtreplyaccsubdatabyaccsubprdno")
    protected ResultDto<String> getLmtReplyAccSubDataByAccSubPrdNo(@RequestBody String  accSubPrdNo) {
        String subSerno = lmtReplyAccSubPrdService.getLmtReplyAccSubDataByAccSubPrdNo(accSubPrdNo);
        return new ResultDto<String>(subSerno);
    }

    /**
     * @函数名称:getLmtReplyAccSubDataByAccSubPrdNo
     * @函数描述:通过分项品种编号查询分项流水号
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过分项额度号查询分项流水号")
    @PostMapping("/getLmtReplyAccSubDataByAccSubNo")
    protected ResultDto<String> getLmtReplyAccSubDataByAccSubNo(@RequestBody String  accSubNo) {
        String subSerno = lmtReplyAccSubPrdService.getLmtReplyAccSubDataByAccSubNo(accSubNo);
        return new ResultDto<String>(subSerno);
    }

    /**
     * @函数名称:getLmtReplyAccSubPrdByReplyNo
     * @函数描述:根据批复编号查询分项明细信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据批复编号查询分项明细信息")
    @PostMapping("/getLmtReplyAccSubPrdByReplyNo")
    protected ResultDto<List<LmtReplyAccSubPrd>> getLmtReplyAccSubPrdByReplyNo(@RequestBody String replyNo){
        return new ResultDto<List<LmtReplyAccSubPrd>>(lmtReplyAccSubPrdService.getLmtReplyAccSubPrdByReplyNo(replyNo));
    }
}
