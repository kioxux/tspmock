/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.CtrLoanContDto;
import cn.com.yusys.yusp.dto.LmtSubPrdCtrLoanContRelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSubPrdCtrLoanContRel;
import cn.com.yusys.yusp.service.LmtSubPrdCtrLoanContRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSubPrdCtrLoanContRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 21:20:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsubprdctrloancontrel")
public class LmtSubPrdCtrLoanContRelResource {
    @Autowired
    private LmtSubPrdCtrLoanContRelService lmtSubPrdCtrLoanContRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSubPrdCtrLoanContRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSubPrdCtrLoanContRel> list = lmtSubPrdCtrLoanContRelService.selectAll(queryModel);
        return new ResultDto<List<LmtSubPrdCtrLoanContRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSubPrdCtrLoanContRel>> index(QueryModel queryModel) {
        List<LmtSubPrdCtrLoanContRel> list = lmtSubPrdCtrLoanContRelService.selectByModel(queryModel);
        return new ResultDto<List<LmtSubPrdCtrLoanContRel>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSubPrdCtrLoanContRel> create(@RequestBody LmtSubPrdCtrLoanContRel lmtSubPrdCtrLoanContRel) throws URISyntaxException {
        lmtSubPrdCtrLoanContRelService.insert(lmtSubPrdCtrLoanContRel);
        return new ResultDto<LmtSubPrdCtrLoanContRel>(lmtSubPrdCtrLoanContRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSubPrdCtrLoanContRel lmtSubPrdCtrLoanContRel) throws URISyntaxException {
        int result = lmtSubPrdCtrLoanContRelService.update(lmtSubPrdCtrLoanContRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String subPrdSerno) {
        int result = lmtSubPrdCtrLoanContRelService.deleteByPrimaryKey(pkId, subPrdSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * 查询诚易融关联合同
     * @param param
     * @return
     */
    @PostMapping("/selectBySubPrdSerno")
    protected ResultDto<List<LmtSubPrdCtrLoanContRelDto>> selectBySubPrdSerno(@RequestBody Map param){
        String subPrdSerno = param.get("subPrdSerno").toString();
        return new ResultDto<List<LmtSubPrdCtrLoanContRelDto>>(lmtSubPrdCtrLoanContRelService.selectBySubPrdSerno(subPrdSerno));
    }

    /**
     * @创建人 qw
     * @方法名 saveCtrLoanContDtoData
     * @创建时间 2021-08-02 17:01
     * @描述 关联合同下一步新增
     */
    @PostMapping("/savectrLoancontdtodata")
    protected ResultDto<Integer> saveCtrLoanContDtoData(@RequestBody Map param) {
        return new ResultDto<Integer>(lmtSubPrdCtrLoanContRelService.saveCtrLoanContDtoData(param));
    }

    /**
     * 根据主键逻辑删除
     * @param pkId
     * @return
     */
    @PostMapping("/logicDelete")
    protected ResultDto<Integer> logicDelete(@RequestBody String pkId){
        return new ResultDto<Integer>(lmtSubPrdCtrLoanContRelService.logicDelete(pkId));
    }
}
