/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.GuarBaseInfoRelDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.web.rest.LmtReplyChgSubResource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtReplySubPrdMapper;
import org.springframework.util.unit.DataUnit;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplySubPrdService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:34:18
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplySubPrdService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtReplySubPrdService.class);

    @Autowired
    private LmtReplySubPrdMapper lmtReplySubPrdMapper;

    // 单一客户授信审批分项适用品种
    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;

    // 单一客户授信批复变更分项适用品种
    @Autowired
    private LmtReplyChgSubPrdService lmtReplyChgSubPrdService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private LmtReplyService lmtReplyService;

    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private LmtAppService lmtAppService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    @Autowired
    private LmtApprSubService lmtApprSubService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private RptLmtRepayAnysGuarPldDetailService rptLmtRepayAnysGuarPldDetailService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtGrpReplyService lmtGrpReplyService;

    @Autowired
    private LmtGrpReplyAccService lmtGrpReplyAccService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtReplySubPrd selectByPrimaryKey(String pkId) {
        return lmtReplySubPrdMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtReplySubPrd> selectAll(QueryModel model) {
        List<LmtReplySubPrd> records = (List<LmtReplySubPrd>) lmtReplySubPrdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReplySubPrd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplySubPrd> list = lmtReplySubPrdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtReplySubPrd record) {
        return lmtReplySubPrdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtReplySubPrd record) {
        return lmtReplySubPrdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtReplySubPrd record) {
        return lmtReplySubPrdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtReplySubPrd record) {
        return lmtReplySubPrdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplySubPrdMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplySubPrdMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryLmtReplySubPrdByParams
     * @方法描述: 通过参数查询授信批复分项使用品种数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtReplySubPrd> queryLmtReplySubPrdByParams(HashMap<String, String> queryMaps) {
        return lmtReplySubPrdMapper.queryLmtReplySubPrdByParams(queryMaps);
    }


    /**
     * @方法名称: queryLmtReplySubPrdByReplySubPrdSerno
     * @方法描述: 通过参数查询授信批复分项使用品种数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtReplySubPrd queryLmtReplySubPrdByReplySubPrdSerno(String replySubPrdSerno) {
        return lmtReplySubPrdMapper.queryLmtReplySubPrdByReplySubPrdSerno(replySubPrdSerno);
    }

    /**
     * @方法名称: generateNewLmtReplySub
     * @方法描述: 获取审批授信分项适用品种生成授信批复分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateNewLmtReplySubPrdByLmtApprSub(String currentUserId, String currentOrgId, LmtApprSub lmtApprSub, String repaySubSerno) throws Exception {
        HashMap<String, String> subPrdQueryMap = new HashMap<String, String>();
        subPrdQueryMap.put("apprSubSerno", lmtApprSub.getApproveSubSerno());
        subPrdQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 2.2 获取授信审批中分项适用品种信息
        List<LmtApprSubPrd> lmtApprSubPrdList = lmtApprSubPrdService.selectLmtApprSubPrdByParams(subPrdQueryMap);
        log.info("查询授信审批中的分项信息:" + JSON.toJSONString(lmtApprSubPrdList));
        if (lmtApprSubPrdList != null && lmtApprSubPrdList.size() > 0) {
            for (LmtApprSubPrd lmtApprSubPrd : lmtApprSubPrdList) {
                String repaySubPrdSerno = generateNewLmtReplySubPrdByLmtApprSubPrd(currentUserId, currentOrgId, repaySubSerno, lmtApprSubPrd);
                log.info("生成新的授信批复分项适用品种流水号:" + repaySubPrdSerno);
            }
        } else {
            throw new Exception("查询审批分项使用品种数据异常！");
        }
    }

    /**
     * @方法名称: generateNewLmtReplySubPrdByLmtApprSubPrd
     * @方法描述: 获取审批授信分项适用品种生成授信批复分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateNewLmtReplySubPrdByLmtApprSubPrd(String currentUserId, String currentOrgId, String repaySubSerno, LmtApprSubPrd lmtApprSubPrd) throws Exception {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        String repaySubPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
        log.info("生成新的授信批复分项适用品种流水号:" + repaySubPrdSerno);
        LmtReplySubPrd lmtReplySubPrd = new LmtReplySubPrd();
        BeanUtils.copyProperties(lmtApprSubPrd, lmtReplySubPrd);
        LmtReplySub lmtReplySub = lmtReplySubService.queryLmtReplySubByReplySubSerno(repaySubSerno);
        LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(lmtReplySub.getReplySerno());
        lmtReplySubPrd.setPkId(UUID.randomUUID().toString());
        lmtReplySubPrd.setReplySubSerno(repaySubSerno);
        lmtReplySubPrd.setReplySubPrdSerno(repaySubPrdSerno);
        lmtReplySubPrd.setInputId(currentUserId);
        lmtReplySubPrd.setInputBrId(currentOrgId);
        lmtReplySubPrd.setInputDate(openday);
        log.info("当前营业日期为："+ openday);
        log.info("根据原分项明细[{}]查询当前授信类型,只有授信新增和续作时才会重新计算起始日到期日.", lmtApprSubPrd.getApprSubPrdSerno());
        LmtApprSub lmtApprSub = lmtApprSubService.selectBySubSerno(lmtApprSubPrd.getApproveSubSerno());
        LmtAppr lmtAppr = lmtApprService.selectByApproveSerno(lmtApprSub.getApproveSerno());
        if (!StringUtils.isBlank(String.valueOf(lmtReplySubPrd.getLmtTerm())) && !StringUtils.isBlank(openday)) {
            Date openDate = DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue());
            //判断是否集团授信
            //如果是集团授信
            boolean ifJoin = false;
            if(CmisCommonConstants.YES_NO_1.equals(lmtAppr.getIsGrp())){
                LmtGrpMemRel lmtGrpMemRelSingle = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(lmtReplySub.getSerno());
                if(CmisCommonConstants.YES_NO_1.equals(lmtGrpMemRelSingle.getIsPrtcptCurtDeclare())){
                    ifJoin = true;
                }
                List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpMemRelSingle.getGrpSerno());
                boolean allTake = true;
                boolean once = true;
                Date notTakeRevoEndDate = null;
                String notTakeRevoEndString = "";
                //判断集团所有成员是否全部参与本次申报
                for(LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList){
                    if(!CmisCommonConstants.YES_NO_1.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())){
                        //存在成员客户不参与本次申报
                        allTake = false;
                        LmtReplyAcc lmtReplyAccNotTake = lmtReplyAccService.getLastLmtReplyAcc(lmtGrpMemRel.getCusId());
                        if(lmtReplyAccNotTake != null){
                            notTakeRevoEndString = lmtReplyAccSubPrdService.getRevolimitLastDateByAccNo(lmtReplyAccNotTake.getAccNo());
                            if(StringUtils.nonEmpty(notTakeRevoEndString)){
                                Date endDateNotTakeThis = DateUtils.parseDate(notTakeRevoEndString, DateFormatEnum.DEFAULT.getValue());
                                if(endDateNotTakeThis.compareTo(openDate) > 0){
                                    if(once){
                                        notTakeRevoEndDate = endDateNotTakeThis;
                                        once = false;
                                    }else{
                                        if(endDateNotTakeThis.compareTo(notTakeRevoEndDate) < 0){
                                            notTakeRevoEndDate = endDateNotTakeThis;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //获取集团主体批复
                LmtGrpReply lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByGrpSerno(lmtGrpMemRelSingle.getGrpSerno());
                List<String> cusIdListNow = new ArrayList<>();    //集团现在的成员客户集合
                List<LmtGrpMemRel> lmtGrpMemRelNow = new ArrayList<>();
                LmtReplyAcc inGrp = new LmtReplyAcc();
                Date realRevoEndDate =null;
                if(StringUtils.nonEmpty(lmtGrpReply.getOrigiGrpReplySerno())){
                    lmtGrpMemRelNow = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpReply.getGrpSerno());
                    for(LmtGrpMemRel lmtGrpMemRel :lmtGrpMemRelNow){
                        cusIdListNow.add(lmtGrpMemRel.getCusId());
                    }
                }
                Date endDate = DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue());
                boolean firstTime = true;
                for(String cusId : cusIdListNow){
                    inGrp = lmtReplyAccService.getLastLmtReplyAcc(cusId);
                    if(inGrp != null){
                        List<LmtReplyAccSubPrd> origiSubPrdList =  lmtReplyAccSubPrdService.getLmtReplyAccSubPrdByReplyNo(inGrp.getReplySerno());
                        for(LmtReplyAccSubPrd lmtReplyAccSubPrd: origiSubPrdList){
                            if(CmisCommonConstants.YES_NO_1.equals(lmtReplyAccSubPrd.getIsRevolvLimit())){
                                Date dateItem = DateUtils.parseDate(lmtReplyAccSubPrd.getEndDate(), DateFormatEnum.DEFAULT.getValue());
                                if(dateItem.compareTo(openDate) > 0 ){
                                    if(firstTime){
                                        endDate = dateItem;
                                        firstTime = false;
                                    }else{
                                        if(dateItem.compareTo(endDate) < 0){
                                            endDate = dateItem;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                if(endDate.compareTo(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue())) >0 ){
                    realRevoEndDate = endDate;
                }
                if(ifJoin){
                    if(CmisCommonConstants.LMT_TYPE_01.equals(lmtAppr.getLmtType()) || CmisCommonConstants.LMT_TYPE_06.equals(lmtAppr.getLmtType())|| CmisCommonConstants.LMT_TYPE_03.equals(lmtAppr.getLmtType())){
                        if(StringUtils.nonEmpty(lmtReplySubPrd.getOrigiLmtAccSubPrdNo())){
                            if(CmisCommonConstants.YES_NO_1.equals(lmtReplySubPrd.getIsRevolvLimit())){
                                if(notTakeRevoEndDate == null){
                                    lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                                    lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(lmtReply.getReplyInureDate(), DateFormatEnum.DEFAULT.getValue()), lmtReply.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                                }else{
                                    lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                                    lmtReplySubPrd.setEndDate(DateUtils.formatDate(notTakeRevoEndDate,DateFormatEnum.DEFAULT.getValue()));
                                }
                            }else{
                                Map<String,String> map = new HashMap<>();
                                map.put("accSubPrdNo",lmtReplySubPrd.getOrigiLmtAccSubPrdNo());
                                LmtReplyAccSubPrd replyAccSubPrd = lmtReplyAccSubPrdService.queryByParams(map);
                                lmtReplySubPrd.setStartDate(replyAccSubPrd.getStartDate());
                                lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(replyAccSubPrd.getStartDate(), DateFormatEnum.DEFAULT.getValue()), lmtReplySubPrd.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                            }
                        }else{
                            if(CmisCommonConstants.YES_NO_1.equals(lmtReplySubPrd.getIsRevolvLimit())){
                                if(notTakeRevoEndDate == null){
                                    lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                                    lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(lmtReply.getReplyInureDate(), DateFormatEnum.DEFAULT.getValue()), lmtReply.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                                }else{
                                    lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                                    lmtReplySubPrd.setEndDate(DateUtils.formatDate(notTakeRevoEndDate,DateFormatEnum.DEFAULT.getValue()));
                                }
                            }else{
                                lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                                lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(lmtReply.getReplyInureDate(), DateFormatEnum.DEFAULT.getValue()), lmtReplySubPrd.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                            }
                        }
                    } else {
                        if(StringUtils.nonEmpty(lmtReplySubPrd.getOrigiLmtAccSubPrdNo())){
                            Map<String,String> map = new HashMap<>();
                            map.put("accSubPrdNo",lmtReplySubPrd.getOrigiLmtAccSubPrdNo());
                            LmtReplyAccSubPrd replyAccSubPrd = lmtReplyAccSubPrdService.queryByParams(map);
                            if(CmisCommonConstants.YES_NO_1.equals(lmtReplySubPrd.getIsRevolvLimit())){
                                if(realRevoEndDate == null){
                                    lmtReplySubPrd.setStartDate(replyAccSubPrd.getStartDate());
                                    lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(lmtReply.getReplyInureDate(), DateFormatEnum.DEFAULT.getValue()), lmtReply.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                                }else{
                                    Date currEndDateRevo = DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()), lmtReply.getLmtTerm()),-1);
                                    Date endDateNow = getEarlyDate(realRevoEndDate,currEndDateRevo);
                                    lmtReplySubPrd.setStartDate(replyAccSubPrd.getStartDate());
                                    lmtReplySubPrd.setEndDate(DateUtils.formatDate(endDateNow,DateFormatEnum.DEFAULT.getValue()));
                                }
                            }else{
                                lmtReplySubPrd.setStartDate(replyAccSubPrd.getStartDate());
                                lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(replyAccSubPrd.getStartDate(), DateFormatEnum.DEFAULT.getValue()), lmtReplySubPrd.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                            }
                        }else{
                            if(CmisCommonConstants.YES_NO_1.equals(lmtReplySubPrd.getIsRevolvLimit())){
                                if(realRevoEndDate == null){
                                    lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                                    lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(lmtReply.getReplyInureDate(), DateFormatEnum.DEFAULT.getValue()), lmtReply.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                                }else{
                                    Date currEndDateRevo = DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()), lmtReply.getLmtTerm()),-1);
                                    Date endDateNow = getEarlyDate(realRevoEndDate,currEndDateRevo);
                                    lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                                    lmtReplySubPrd.setEndDate(DateUtils.formatDate(endDateNow,DateFormatEnum.DEFAULT.getValue()));
                                }
                            }else{
                                lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                                lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(lmtReply.getReplyInureDate(), DateFormatEnum.DEFAULT.getValue()), lmtReplySubPrd.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                            }
                        }
                    }
                }else{
                    if(StringUtils.nonEmpty(lmtReplySubPrd.getOrigiLmtAccSubPrdNo())){
                        Map<String,String> map = new HashMap<>();
                        map.put("accSubPrdNo",lmtReplySubPrd.getOrigiLmtAccSubPrdNo());
                        LmtReplyAccSubPrd replyAccSubPrdOrigi = lmtReplyAccSubPrdService.queryByParams(map);
                        lmtReplySubPrd.setStartDate(replyAccSubPrdOrigi.getStartDate());
                        lmtReplySubPrd.setEndDate(replyAccSubPrdOrigi.getEndDate());
                    }
                }
            }else{
                //如果是单一客户授信

                //新增、续作（再议）
                if(CmisCommonConstants.LMT_TYPE_01.equals(lmtAppr.getLmtType()) || CmisCommonConstants.LMT_TYPE_06.equals(lmtAppr.getLmtType())|| CmisCommonConstants.LMT_TYPE_03.equals(lmtAppr.getLmtType())){
                    if(CmisCommonConstants.YES_NO_1.equals(lmtReplySubPrd.getIsRevolvLimit())){
                        lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                        lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(lmtReply.getReplyInureDate(), DateFormatEnum.DEFAULT.getValue()), lmtReply.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                    }else{
                        if(StringUtils.nonBlank(lmtReplySubPrd.getOrigiLmtAccSubPrdNo())){
                            Map<String,String> map = new HashMap<>();
                            map.put("accSubPrdNo",lmtReplySubPrd.getOrigiLmtAccSubPrdNo());
                            LmtReplyAccSubPrd replyAccSubPrdOrigi = lmtReplyAccSubPrdService.queryByParams(map);
                            lmtReplySubPrd.setStartDate(replyAccSubPrdOrigi.getStartDate());
                            lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(replyAccSubPrdOrigi.getStartDate(), DateFormatEnum.DEFAULT.getValue()), lmtReplySubPrd.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                        }else{
                            lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                            lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(lmtReply.getReplyInureDate(), DateFormatEnum.DEFAULT.getValue()), lmtReplySubPrd.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                        }
                    }
                }else{
                    //变更等
                    //判断是否循环额度
                    if(CmisCommonConstants.YES_NO_1.equals(lmtReplySubPrd.getIsRevolvLimit())){
                        List<LmtReplyAccSubPrd> origiSubPrdList =  lmtReplyAccSubPrdService.getLmtReplyAccSubPrdByReplyNo(lmtReply.getOrigiLmtReplySerno());
                        String endDateRevo = "";
                        for(LmtReplyAccSubPrd lmtReplyAccSubPrd: origiSubPrdList){
                            if(CmisCommonConstants.YES_NO_1.equals(lmtReplyAccSubPrd.getIsRevolvLimit())){
                                Date dateOne = DateUtils.parseDate(lmtReplyAccSubPrd.getEndDate(), DateFormatEnum.DEFAULT.getValue());
                                Date dateOpen = DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue());
                                if(dateOpen.compareTo(dateOne) < 0){
                                    endDateRevo = lmtReplyAccSubPrd.getEndDate();
                                    break;
                                }
                            }
                        }
                        Date realEndDateRevo = new Date();
                        if(StringUtils.nonEmpty(endDateRevo)){
                            Date origiEndDateRevo = DateUtils.parseDate(endDateRevo, DateFormatEnum.DEFAULT.getValue());
                            Date currEndDateRevo = DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()), lmtReplySubPrd.getLmtTerm()),-1);
                            realEndDateRevo = getEarlyDate(origiEndDateRevo,currEndDateRevo);
                        }else{
                            realEndDateRevo = DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()), lmtReplySubPrd.getLmtTerm()),-1);
                        }
                        String realEndDateRevoString =DateUtils.formatDate(realEndDateRevo, DateFormatEnum.DEFAULT.getValue());
                        if(StringUtils.nonBlank(lmtReplySubPrd.getOrigiLmtAccSubPrdNo())){
                            Map<String,String> map = new HashMap<>();
                            map.put("accSubPrdNo",lmtReplySubPrd.getOrigiLmtAccSubPrdNo());
                            LmtReplyAccSubPrd replyAccSubPrd = lmtReplyAccSubPrdService.queryByParams(map);
                            if(replyAccSubPrd != null && !StringUtils.isBlank(String.valueOf(lmtReplySubPrd.getLmtTerm()))){
                                lmtReplySubPrd.setStartDate(replyAccSubPrd.getStartDate());
                                lmtReplySubPrd.setEndDate(realEndDateRevoString);
                            } else{
                                throw new Exception("原产品额度信息查询错误！");
                            }
                        }else{
                            lmtReplySubPrd.setStartDate(lmtReply.getReplyInureDate());
                            lmtReplySubPrd.setEndDate(realEndDateRevoString);
                        }
                    }else{
                        if(StringUtils.nonBlank(lmtReplySubPrd.getOrigiLmtAccSubPrdNo())){
                            Map<String,String> map = new HashMap<>();
                            map.put("accSubPrdNo",lmtReplySubPrd.getOrigiLmtAccSubPrdNo());
                            LmtReplyAccSubPrd replyAccSubPrd = lmtReplyAccSubPrdService.queryByParams(map);
                            if(replyAccSubPrd != null && !StringUtils.isBlank(String.valueOf(lmtReplySubPrd.getLmtTerm()))){
                                lmtReplySubPrd.setStartDate(replyAccSubPrd.getStartDate());
                                lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(replyAccSubPrd.getStartDate(), DateFormatEnum.DEFAULT.getValue()), lmtReplySubPrd.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                            } else{
                                throw new Exception("原产品额度信息查询错误！");
                            }
                        }else{
                            lmtReplySubPrd.setStartDate(openday);
                            lmtReplySubPrd.setEndDate(DateUtils.formatDate(DateUtils.addDay(DateUtils.addMonth(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()), lmtReplySubPrd.getLmtTerm()),-1), DateFormatEnum.DEFAULT.getValue()));
                        }
                    }
                }
            }
        }

        lmtReplySubPrd.setUpdId(currentUserId);
        lmtReplySubPrd.setUpdBrId(currentOrgId);
        lmtReplySubPrd.setUpdDate(openday);
        lmtReplySubPrd.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        lmtReplySubPrd.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        this.insert(lmtReplySubPrd);

        // 新增批复套表中还款计划数据
        List<RepayCapPlan> list = repayCapPlanService.selectBySerno(lmtApprSubPrd.getApprSubPrdSerno());
        log.info("新增还款计划落库处理----------上一审批人审批时还款计划为:" + JSON.toJSONString(list));
        for (RepayCapPlan repayCapPlan : list) {
            repayCapPlan.setPkId(UUID.randomUUID().toString());
            repayCapPlan.setSerno(repaySubPrdSerno);
            repayCapPlan.setUpdId(currentUserId);
            repayCapPlan.setUpdBrId(currentOrgId);
            repayCapPlan.setUpdDate(openday);
            repayCapPlan.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
            repayCapPlanService.insert(repayCapPlan);
        }

        log.info("生成新的授信批复分项适用品种:" + repaySubPrdSerno);
        return repaySubPrdSerno;

    }

    /**
     * @方法名称: generateNewLmtReplySubPrdByLmtReplyChgSub
     * @方法描述: 通过授信批复变更分项生成授信批复分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplySubPrdByLmtReplyChgSub(String currentUserId, String currentOrgId, LmtReplyChgSub lmtReplyChgSub, String repaySubSerno) throws Exception {
        HashMap<String, String> lmtReplyChgSubPrdQueryMap = new HashMap<String, String>();
        lmtReplyChgSubPrdQueryMap.put("subSerno", lmtReplyChgSub.getSubSerno());
        lmtReplyChgSubPrdQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 2.2 获取授信批复变更分项适用品种信息
        List<LmtReplyChgSubPrd> lmtReplyChgSubPrdList = lmtReplyChgSubPrdService.queryLmtReplyChgSubPrdByParams(lmtReplyChgSubPrdQueryMap);

        HashMap<String, String> lmtReplySubPrdQueryMap = new HashMap<String, String>();
        lmtReplySubPrdQueryMap.put("repaySerno", repaySubSerno);
        lmtReplySubPrdQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplySubPrd> lmtReplySubPrdList = this.queryLmtReplySubPrdByParams(lmtReplySubPrdQueryMap);

        if (lmtReplyChgSubPrdList != null && lmtReplyChgSubPrdList.size() > 0 && lmtReplySubPrdList != null && lmtReplySubPrdList.size() > 0) {
            for (LmtReplyChgSubPrd lmtReplyChgSubPrd : lmtReplyChgSubPrdList) {
                LmtReplySubPrd lmtReplySubPrd = new LmtReplySubPrd();
                boolean isNew = true;
                for (LmtReplySubPrd item : lmtReplySubPrdList) {
                    if (item.getReplySubSerno().equals(lmtReplyChgSubPrd.getReplySubPrdSerno())) {
                        BeanUtils.copyProperties(lmtReplySubPrd, lmtReplyChgSubPrd);
                        lmtReplySubPrd.setPkId(item.getPkId());
                        this.update(lmtReplySubPrd);
                        log.info("更新授信批复分项适用品种:" + lmtReplySubPrd.getReplySubSerno());
                        isNew = false;
                        break;
                    }
                }
                if (isNew) {
                    this.generateNewLmtReplySubPrdByLmtReplyChgSubPrd(currentUserId, currentOrgId, repaySubSerno, lmtReplyChgSubPrd);
                }
            }
        } else {
            throw new Exception("查询批复变更分适用品种项或批复分项适用品种异常");
        }
    }

    /**
     * @方法名称: generateNewLmtReplySubPrdByLmtReplyChgSub
     * @方法描述: 通过授信批复变更分项生成授信批复分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateNewLmtReplySubPrdByLmtReplyChgSub(String currentUserId, String currentOrgId, LmtReplyChgSub lmtReplyChgSub, String repaySubSerno) throws Exception {
        HashMap<String, String> subPrdQueryMap = new HashMap<String, String>();
        subPrdQueryMap.put("subSerno", lmtReplyChgSub.getSubSerno());
        subPrdQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 2.2 获取授信审批中分项适用品种信息
        List<LmtReplyChgSubPrd> lmtApprSubPrdList = lmtReplyChgSubPrdService.queryLmtReplyChgSubPrdByParams(subPrdQueryMap);
        log.info("查询授信审批中的分项信息:" + lmtApprSubPrdList.toString());
        if (lmtApprSubPrdList != null && lmtApprSubPrdList.size() > 0) {
            for (LmtReplyChgSubPrd lmtReplyChgSubPrd : lmtApprSubPrdList) {
                generateNewLmtReplySubPrdByLmtReplyChgSubPrd(currentUserId, currentOrgId, repaySubSerno, lmtReplyChgSubPrd);
            }
        } else {
            throw new Exception("查询审批分项使用品种数据异常！");
        }
    }

    /**
     * @方法名称: generateNewLmtReplySubPrdByLmtReplyChgSubPrd
     * @方法描述: 通过授信批复变更分项生成授信批复分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateNewLmtReplySubPrdByLmtReplyChgSubPrd(String currentUserId, String currentOrgId, String repaySubSerno, LmtReplyChgSubPrd lmtReplyChgSubPrd) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        LmtReplySubPrd lmtReplySubPrd = new LmtReplySubPrd();
        String repaySubPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
        log.info("生成新的授信批复分项适用品种流水号:" + repaySubPrdSerno);
        BeanUtils.copyProperties(lmtReplyChgSubPrd, lmtReplySubPrd);
        lmtReplySubPrd.setPkId(UUID.randomUUID().toString());
        lmtReplySubPrd.setReplySubSerno(repaySubSerno);
        lmtReplySubPrd.setReplySubPrdSerno(repaySubPrdSerno);
        lmtReplySubPrd.setInputId(currentUserId);
        lmtReplySubPrd.setInputBrId(currentOrgId);
        lmtReplySubPrd.setInputDate(openday);
        lmtReplySubPrd.setUpdId(currentUserId);
        lmtReplySubPrd.setUpdBrId(currentOrgId);
        lmtReplySubPrd.setUpdDate(openday);
        lmtReplySubPrd.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        lmtReplySubPrd.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        this.insert(lmtReplySubPrd);
        log.info("生成新的授信批复分项适用品种:" + repaySubPrdSerno);
    }

    /**
     * @方法名称: copyToBySubSerno
     * @方法描述: 将原授信分项下的适用授信产品挂载到生成的新分项下
     * @创建人: zhuzr
     * @参数与返回说明:
     * @算法描述: 无
     */

    public boolean copyToBySubSerno(String originSubSerno, String currentSubSerno) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        LmtApp lmtApp = lmtAppService.getLmtAppBySubSerno(currentSubSerno);
        log.info("授信申请信息:"+lmtApp);
        HashMap<String, String> params = new HashMap();
        String originSubPrdSerno = "";
        String subPrdSerno = "";
        LmtReplySubPrd lmtReplySubPrd = new LmtReplySubPrd();
        LmtAppSubPrd lmtAppSubPrd = new LmtAppSubPrd();
        params.put("replySubSerno", originSubSerno);
        params.put("oprType", CmisCommonConstants.ADD_OPR);
        List<LmtReplySubPrd> subPrdList = lmtReplySubPrdMapper.queryLmtReplySubPrdByParams(params);
        LmtReplyAccSubPrd lmtReplyAccSubPrd = new LmtReplyAccSubPrd();
        if (subPrdList != null && subPrdList.size() > 0) {
            for (int i = 0; i < subPrdList.size(); i++) {
                lmtReplySubPrd = subPrdList.get(i);
                originSubPrdSerno = lmtReplySubPrd.getSubPrdSerno();
                BeanUtils.copyProperties(lmtReplySubPrd, lmtAppSubPrd);
                subPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
                lmtAppSubPrd.setSubPrdSerno(subPrdSerno);
                lmtAppSubPrd.setSubSerno(currentSubSerno);
                lmtAppSubPrd.setPkId(UUID.randomUUID().toString());
                lmtAppSubPrd.setOrigiLmtAccSubPrdAmt(lmtReplySubPrd.getLmtAmt());
                HashMap<String, String> queryMap = new HashMap<String, String>();
                queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                queryMap.put("replySubPrdSerno", lmtReplySubPrd.getReplySubPrdSerno());
                List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByParams(queryMap);
                if (lmtReplyAccSubPrdList != null && lmtReplyAccSubPrdList.size() == 1) {
                    lmtReplyAccSubPrd = lmtReplyAccSubPrdList.get(0);
                    lmtAppSubPrd.setOrigiLmtAccSubPrdNo(lmtReplyAccSubPrd.getAccSubPrdNo());
                } else {
                    // 如果是授信申请 否决后发起复议使用的是查不到台账编号的
                    // throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",挂载授信适用产品失败！");
                }
                lmtAppSubPrd.setOrigiLmtAccSubPrdTerm(lmtReplySubPrd.getLmtTerm());
                lmtAppSubPrd.setIsSfcaLmt(CmisCommonConstants.STD_ZB_YES_NO_1);
                User userInfo = SessionUtils.getUserInformation();
                if (userInfo == null) {
                    lmtAppSubPrd.setInputId(lmtReplySubPrd.getInputId());
                    lmtAppSubPrd.setInputBrId(lmtReplySubPrd.getInputBrId());
                    lmtAppSubPrd.setInputDate(openday);
                    lmtAppSubPrd.setUpdId(lmtReplySubPrd.getInputId());
                    lmtAppSubPrd.setUpdBrId(lmtReplySubPrd.getInputBrId());
                    lmtAppSubPrd.setUpdDate(openday);
                    lmtAppSubPrd.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
                    lmtAppSubPrd.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
                } else {
                    lmtAppSubPrd.setInputId(userInfo.getLoginCode());
                    lmtAppSubPrd.setInputBrId(userInfo.getOrg().getCode());
                    lmtAppSubPrd.setInputDate(openday);
                    lmtAppSubPrd.setUpdId(userInfo.getLoginCode());
                    lmtAppSubPrd.setUpdBrId(userInfo.getOrg().getCode());
                    lmtAppSubPrd.setUpdDate(openday);
                    lmtAppSubPrd.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
                    lmtAppSubPrd.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
                }
                //  TODO 变更标识字典项未定
                lmtAppSubPrd.setChgFlag("0");
                if(lmtApp.getIsGrp().equals(CmisCommonConstants.YES_NO_1)){
                    log.info("校验新纳入成员二级自有授信期限是否与修改后的成员授信期限保持一致,不一致则自动与成员授信申请数据保持一致-------start--------");
                    if(CmisCommonConstants.LMT_TYPE_01.equals(lmtApp.getLmtType()) || CmisCommonConstants.LMT_TYPE_03.equals(lmtApp.getLmtType()) || CmisCommonConstants.LMT_TYPE_06.equals(lmtApp.getLmtType())){
                        log.info("新增,续作,再议情况下,校验新纳入成员分项中自有授信期限与修改后的成员授信期限不一致,自动与保持一致");
                        if(CmisCommonConstants.YES_NO_1.equals(lmtAppSubPrd.getIsRevolvLimit())){
                            log.info("校验新纳入成员一级自有授信期限是否与修改后的成员授信期限保持一致!");
                            lmtAppSubPrd.setLmtTerm(lmtApp.getLmtTerm());
                        }
                    }else{
                        if(CmisCommonConstants.YES_NO_1.equals(lmtAppSubPrd.getIsRevolvLimit()) && lmtAppSubPrd.getLmtTerm().compareTo(lmtApp.getLmtTerm())>0){
                            lmtAppSubPrd.setLmtTerm(lmtApp.getLmtTerm());
                        }
                    }
                    log.info("校验新纳入成员二级自有授信期限是否与修改后的成员授信期限保持一致,不一致则自动与成员授信申请数据保持一致--------end-------");
                }
                lmtAppSubPrd.setCusName(lmtApp.getCusName());
                int count = lmtAppSubPrdService.insert(lmtAppSubPrd);
                //boolean lmtRepayPlan = lmtRepayCapPlanService.copyLmtRepayPlan(originSubPrdSerno,subPrdSerno);
                if (count != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",挂载授信适用产品失败！");
                }
            }
        }
        return true;
    }

    /**
     * @方法名称: generateLmtReplySubPrdByLmtAppSub
     * @方法描述: 获取授信申请分项适用品种生成授信批复分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-09-16 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtReplySubPrdByLmtAppSub(String currentUserId, String currentOrgId, LmtAppSub lmtAppSub, String repaySubSerno) throws Exception {
        HashMap<String, String> subPrdQueryMap = new HashMap<String, String>();
        subPrdQueryMap.put("subSerno", lmtAppSub.getSubSerno());
        subPrdQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 2.2 获取授信审批中分项适用品种信息
        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectByParams(subPrdQueryMap);
        log.info("查询授信申请中的分项信息:" + lmtAppSubPrdList.toString());
        if (lmtAppSubPrdList != null && lmtAppSubPrdList.size() > 0) {
            for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                String repaySubPrdSerno = generateLmtReplySubPrdByLmtAppSubPrd(currentUserId, currentOrgId, repaySubSerno, lmtAppSubPrd);
                log.info("生成新的授信批复分项适用品种流水号:" + repaySubPrdSerno);
            }
        } else {
            throw new Exception("查询审批分项使用品种数据异常！");
        }
    }

    /**
     * @方法名称: generateLmtReplySubPrdByLmtAppSubPrd
     * @方法描述: 获取授信申请分项适用品种生成授信批复分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-09-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateLmtReplySubPrdByLmtAppSubPrd(String currentUserId, String currentOrgId, String repaySubSerno, LmtAppSubPrd lmtAppSubPrd) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        String repaySubPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
        log.info("生成新的授信批复分项适用品种流水号:" + repaySubPrdSerno);
        LmtReplySubPrd lmtReplySubPrd = new LmtReplySubPrd();
        BeanUtils.copyProperties(lmtAppSubPrd, lmtReplySubPrd);
        lmtReplySubPrd.setPkId(UUID.randomUUID().toString());
        lmtReplySubPrd.setReplySubSerno(repaySubSerno);
        lmtReplySubPrd.setReplySubPrdSerno(repaySubPrdSerno);
        lmtReplySubPrd.setInputId(currentUserId);
        lmtReplySubPrd.setInputBrId(currentOrgId);
        lmtReplySubPrd.setInputDate(openday);
        lmtReplySubPrd.setUpdId(currentUserId);
        lmtReplySubPrd.setUpdBrId(currentOrgId);
        lmtReplySubPrd.setUpdDate(openday);
        lmtReplySubPrd.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        lmtReplySubPrd.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
        this.insert(lmtReplySubPrd);
        log.info("生成新的授信批复分项适用品种:" + repaySubPrdSerno);
        return repaySubPrdSerno;

    }

    /**
     * @方法名称: copyToBySubSerno
     * @方法描述: 将原授信分项下的适用授信产品挂载到生成的新分项下
     * @创建人: zhuzr
     * @参数与返回说明:
     * @算法描述: 无
     */

    public boolean copyToByGuarBizRel(String originSubSerno, String currentSubSerno) {
        QueryModel model = new QueryModel();
        model.getCondition().put("serno", originSubSerno);
        model.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
        // 查询担保品信息（抵质押物信息以及保证担保信息）
        List<GuarBizRel> guarBizRelList = guarBizRelService.selectByModel(model);
        GuarBizRel guarBizRelNew = null;
        String pkId = null;
        if (CollectionUtils.nonEmpty(guarBizRelList)) {
            for (GuarBizRel guarBizRel : guarBizRelList) {
                guarBizRelNew = new GuarBizRel();
                BeanUtils.copyProperties(guarBizRel, guarBizRelNew);
                pkId = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                guarBizRelNew.setPkId(pkId);
                guarBizRelNew.setSerno(currentSubSerno);
                int count = guarBizRelService.insert(guarBizRelNew);
                if (count != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",同步抵质押物信息以及保证担保信息失败！");
                }
            }
        }
        return true;
    }

    /**
     * 查询上期授信情况
     *
     * @param model
     * @return
     */
    public List<Map> selectLastLmt(QueryModel model) {
        if (model.getCondition() == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        String type = model.getCondition().get("type").toString();
        List<Map> list = new ArrayList<>();
        Map tempMap = null;
        List<LmtReply> lmtReplyList = lmtReplyService.selectByModel(model);
        if (lmtReplyList != null && lmtReplyList.size() > 0) {
            LmtReply lmtReply = lmtReplyList.get(0);
            if (lmtReply != null) {
                String serno = lmtReply.getSerno();
                List<LmtReplySub> lmtReplySubList = null;
                if ("normal".equals(type)) {
                    lmtReplySubList = lmtReplySubService.getNormal(serno);
                } else if ("low".equals(type)) {
                    lmtReplySubList = lmtReplySubService.getLow(serno);
                }
                if (lmtReplySubList != null && lmtReplySubList.size() > 0) {
                    for (LmtReplySub lmtReplySub : lmtReplySubList) {
                        String replySubSerno = lmtReplySub.getReplySubSerno();
                        List<LmtReplySubPrd> lmtReplySubPrdList = new ArrayList<>();
                        lmtReplySubPrdList = lmtReplySubPrdMapper.selectByReplySubSerno(replySubSerno);
                        tempMap = new HashMap();
                        tempMap.put("pkId", lmtReplySub.getPkId());
                        tempMap.put("serno", serno);
                        tempMap.put("subPrdSerno", lmtReplySub.getSubSerno());
                        tempMap.put("origiLmtAccSubPrdNo", lmtReplySub.getOrigiLmtAccSubNo());
                        tempMap.put("lmtBizTypeName", lmtReplySub.getSubName());
                        tempMap.put("isPreLmt", lmtReplySub.getIsPreLmt());
                        tempMap.put("guarMode", lmtReplySub.getGuarMode());
                        tempMap.put("origiLmtAccSubPrdAmt", lmtReplySub.getOrigiLmtAccSubAmt());
                        tempMap.put("origiLmtAccSubPrdTerm", lmtReplySub.getOrigiLmtAccSubTerm());
                        tempMap.put("lmtAmt", lmtReplySub.getLmtAmt());
                        tempMap.put("children", lmtReplySubPrdList);
                        list.add(tempMap);
                    }
                }
            }
        }
        return list;
    }

    /**
     * 查询上期一般授信分项明细
     * @param replySubSerno
     * @return
     */
    public List<LmtReplySubPrd> selectByReplySubSerno(String replySubSerno) {
        return lmtReplySubPrdMapper.selectByReplySubSerno(replySubSerno);
    }

    /**
     * 查询上期低风险授信分项明细
     * @param replySubSerno
     * @return
     */
    public List<LmtReplySubPrd> selectLastByReplySubSerno(String replySubSerno) {
        return lmtReplySubPrdMapper.selectLastByReplySubSerno(replySubSerno);
    }


    /**
     * 比较日期大小并返回较早日期
     * @param
     * @return
     */
    public Date getEarlyDate(Date dateA , Date dateB) {
        if(dateA.compareTo(dateB) < 0){
            return dateA;
        }else{
            return dateB;
        }
    }
}