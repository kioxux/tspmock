/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSub;
import cn.com.yusys.yusp.service.LmtSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2021-01-23 10:19:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsub")
public class LmtSubResource {
    @Autowired
    private LmtSubService lmtSubService;

	/**
     * 全表查询.
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSub> list = lmtSubService.selectAll(queryModel);
        return new ResultDto<List<LmtSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSub>> index(QueryModel queryModel) {
        List<LmtSub> list = lmtSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{lmtLimitNo}")
    protected ResultDto<LmtSub> show(@PathVariable("lmtLimitNo") String lmtLimitNo) {
        LmtSub lmtSub = lmtSubService.selectByPrimaryKey(lmtLimitNo);
        return new ResultDto<LmtSub>(lmtSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSub> create(@RequestBody LmtSub lmtSub) throws URISyntaxException {
        lmtSubService.insert(lmtSub);
        return new ResultDto<LmtSub>(lmtSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSub lmtSub) throws URISyntaxException {
        int result = lmtSubService.update(lmtSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{lmtLimitNo}")
    protected ResultDto<Integer> delete(@PathVariable("lmtLimitNo") String lmtLimitNo) {
        int result = lmtSubService.deleteByPrimaryKey(lmtLimitNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 1、校验引入的担保合同总额是否能够覆盖授信金额
     * 2、保存额度分项数据
     * @param params
     * @return
     */
    @PostMapping("/updateLmtSub")
    public ResultDto<Map> updateLmtSub(@RequestBody Map params){
        Map rtnData = lmtSubService.updateLmtSub(params);
        return new ResultDto<Map>(rtnData);
    }

    /**
     * 1、通过额度编号更新担保合同关系的操作类型为删除->opr_type=02
     * 2、更新额度分项主表的操作类型为删除->opr_type=02
     * @param lmtLimitNo
     * @return
     */
    @GetMapping("/deleteLmtSub/{lmtLimitNo}")
    protected ResultDto<Map> deleteLmtSub(@PathVariable("lmtLimitNo") String lmtLimitNo) {
        Map delMap = lmtSubService.deleteLmtSub(lmtLimitNo);
        return new ResultDto<Map>(delMap);
    }
}
