package cn.com.yusys.yusp.service.server.xdxw0027;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0027.req.Xdxw0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0027.resp.Xdxw0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyConInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0027Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-17 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0027Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0027Service.class);

    @Resource
    private LmtSurveyConInfoMapper lmtSurveyConInfoMapper;
    /**
     * 客户调查信息详情查看
      * @param xdxw0027DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0027DataRespDto getXdxw0027(Xdxw0027DataReqDto xdxw0027DataReqDto)throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, JSON.toJSONString(xdxw0027DataReqDto));
        Xdxw0027DataRespDto xdxw0027DataRespDto = new Xdxw0027DataRespDto();
        String servey = xdxw0027DataReqDto.getIndgtSerno();
        try {
            if(StringUtils.isEmpty(servey)){
                throw BizException.error(null, EcbEnum.ECB010009.key,EcbEnum.ECB010009.value);
            }else{
                xdxw0027DataRespDto = lmtSurveyConInfoMapper.selectBySurveySerno(servey);

                String prdId = xdxw0027DataRespDto.getBizType();
                /**1：综合消费贷
                 2：车易贷
                 3：经营贷二
                 4：经营贷一
                 5：创易贷
                 6：经营贷三
                 7：无还本续贷
                 8：优农贷
                 9：优低贷
                 10：增享贷
                 11：惠享贷*
                 * **/
                if("SC010010".equals(prdId)){//房抵循环贷
                    xdxw0027DataRespDto.setLoanType("7");//无还本续贷
                }else if("SC060001".equals(prdId)){//惠享贷
                    xdxw0027DataRespDto.setLoanType("11");
                }else if("SC010014".equals(prdId)) {//增享贷
                    xdxw0027DataRespDto.setLoanType("10");
                }else if("SC020009".equals(prdId)) {//优低贷
                    xdxw0027DataRespDto.setLoanType("9");
                }else if("SC020010".equals(prdId)) {//优农贷
                    xdxw0027DataRespDto.setLoanType("8");
                }else{
                    xdxw0027DataRespDto.setLoanType("4");
                }
                // 2021年10月26日10:30:51 hubp 修改日期长度
                if (StringUtils.nonBlank(xdxw0027DataRespDto.getCheckDate())) {
                    String tempDate = xdxw0027DataRespDto.getCheckDate().substring(0, 10);
                    logger.info("修改CheckDate时间长度，原时间：【{}】，修改后：【{}】", xdxw0027DataRespDto.getCheckDate(), tempDate);
                    xdxw0027DataRespDto.setCheckDate(tempDate);
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e){
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, JSON.toJSONString(xdxw0027DataReqDto));
        return xdxw0027DataRespDto;
    }
}
