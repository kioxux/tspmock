/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherCnyRateApp;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherForRateApp;
import cn.com.yusys.yusp.service.OtherForRateAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherForRateAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-04 16:07:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otherforrateapp")
public class OtherForRateAppResource {
    @Autowired
    private OtherForRateAppService otherForRateAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherForRateApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherForRateApp> list = otherForRateAppService.selectAll(queryModel);
        return new ResultDto<List<OtherForRateApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherForRateApp>> index(QueryModel queryModel) {
        List<OtherForRateApp> list = otherForRateAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherForRateApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherForRateApp> show(@PathVariable("serno") String serno) {
        OtherForRateApp otherForRateApp = otherForRateAppService.selectByPrimaryKey(serno);
        return new ResultDto<OtherForRateApp>(otherForRateApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherForRateApp> create(@RequestBody OtherForRateApp otherForRateApp) throws URISyntaxException {
        otherForRateAppService.insert(otherForRateApp);
        return new ResultDto<OtherForRateApp>(otherForRateApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherForRateApp otherForRateApp) throws URISyntaxException {
        int result = otherForRateAppService.update(otherForRateApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherForRateAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherForRateAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteInfo
     * @函数描述:单个对象删除，将操作类型置为删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteInfo/{serno}")
    protected ResultDto<Integer> deleteInfo(@PathVariable("serno") String serno) {
        int result = otherForRateAppService.deleteInfo(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateOprTypeUnderLmt
     * @函数描述:授信场景下 逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateoprtypeunderlmt")
    protected ResultDto<Integer> updateOprTypeUnderLmt(@RequestBody String serno) {
        int result = otherForRateAppService.deleteInfo(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<OtherForRateApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<OtherForRateApp> list = otherForRateAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherForRateApp>>(list);
    }

    /**
     * @方法名称: getotherforrateapp
     * @方法描述: 根据入参获取当前客户经理名下外币利率定价申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotherforrateapp")
    protected ResultDto<List<OtherForRateApp>> getOtherForRateApp(@RequestBody QueryModel model) {
        List<OtherForRateApp> list = otherForRateAppService.getOtherForRateAppByModel(model);
        return new ResultDto<List<OtherForRateApp>>(list);
    }

    /**
     * @方法名称: getotherforrateappHis
     * @方法描述: 根据入参获取当前客户经理名下外币利率定价申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotherforrateapphis")
    protected ResultDto<List<OtherForRateApp>> getOtherForRateAppHis(@RequestBody QueryModel model) {
        List<OtherForRateApp> list = otherForRateAppService.getOtherForRateAppHis(model);
        return new ResultDto<List<OtherForRateApp>>(list);
    }

    /**
     * @方法名称: addotherforrateapp
     * @方法描述: 新增外币利率定价申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/addotherforrateapp")
    protected ResultDto addotherforrateapp(@RequestBody OtherForRateApp otherForRateApp) {
        return otherForRateAppService.addotherforrateapp(otherForRateApp);
    }

    /**
     * @方法名称: updateotherforrateapp
     * @方法描述: 修改外币利率定价申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/updateotherforrateapp")
    protected ResultDto updateotherforrateapp(@RequestBody OtherForRateApp otherForRateApp) {
        return otherForRateAppService.updateotherforrateapp(otherForRateApp);
    }
}
