/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CusLstZxd
 * @类描述: cus_lst_zxd数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-07-26 20:11:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_lst_zxd")
public class CusLstZxd extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Id
	@Column(name = "CUS_ID")
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = false, length = 80)
	private String cusName;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 借据号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 合同号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 借据金额 **/
	@Column(name = "BILL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billAmt;
	
	/** 借据余额 **/
	@Column(name = "BILL_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billBal;
	
	/** 贷款起始日 **/
	@Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 10)
	private String loanStartDate;
	
	/** 贷款截止日 **/
	@Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 10)
	private String loanEndDate;
	
	/** 申请日期 **/
	@Column(name = "APP_DATE", unique = false, nullable = true, length = 10)
	private String appDate;
	
	/** 联系电话 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;
	
	/** 配偶名称 **/
	@Column(name = "SPOUSE_CUS_NAME", unique = false, nullable = true, length = 80)
	private String spouseCusName;
	
	/** 配偶证件号码 **/
	@Column(name = "SPOUSE_CERT_CODE", unique = false, nullable = true, length = 20)
	private String spouseCertCode;
	
	/** 办理状态 **/
	@Column(name = "APPLY_STATUS", unique = false, nullable = true, length = 5)
	private String applyStatus;
	
	/** 转换原因 **/
	@Column(name = "CHANGE_RS", unique = false, nullable = true, length = 500)
	private String changeRs;
	
	/** 婚姻状态 **/
	@Column(name = "SPOUSE_MARRY_VAL", unique = false, nullable = true, length = 5)
	private String spouseMarryVal;
	
	/** 配偶电话 **/
	@Column(name = "SPOUSE_PHONE", unique = false, nullable = true, length = 30)
	private String spousePhone;
	
	/** 押品他项权证金额 **/
	@Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalAmt;
	
	/** 批复余额 **/
	@Column(name = "APPROVAL_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal approvalBal;
	
	/** 配偶客户编号 **/
	@Column(name = "SPOUSE_CUS_ID", unique = false, nullable = true, length = 32)
	private String spouseCusId;
	
	/** 配偶借据号 **/
	@Column(name = "SPOUSE_BILL_NO", unique = false, nullable = true, length = 32)
	private String spouseBillNo;
	
	/** 账务机构 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 20)
	private String finaBrId;
	
	/** 配偶的客户经理 **/
	@Column(name = "SPOUSE_MANAGER_ID", unique = false, nullable = true, length = 10)
	private String spouseManagerId;
	
	/** 客户经理 **/
	@Column(name = "MANAGER_NAME", unique = false, nullable = true, length = 10)
	private String managerName;
	
	/** 配偶客户经理 **/
	@Column(name = "SPOUSE_MANAGER_NAME", unique = false, nullable = true, length = 10)
	private String spouseManagerName;
	
	/** 执行利率(年) **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;
	
	/** 房产类型编码 **/
	@Column(name = "LAND_USE_WAY", unique = false, nullable = true, length = 10)
	private String landUseWay;
	
	/** 还款期数 **/
	@Column(name = "REPAY_TERM", unique = false, nullable = true, length = 10)
	private Integer repayTerm;
	
	/** 本金逾期记录 **/
	@Column(name = "OVERDUE_COUNT", unique = false, nullable = true, length = 10)
	private Integer overdueCount;
	
	/** 借据号汇总 **/
	@Column(name = "BILL_NO_ALL", unique = false, nullable = true, length = 2000)
	private String billNoAll;
	
	/** 配偶本金逾期记录 **/
	@Column(name = "MEMBER_OVERDUE_COUNT", unique = false, nullable = true, length = 10)
	private Integer memberOverdueCount;
	
	/** 配偶借据号汇总 **/
	@Column(name = "MEMBER_BILL_NO_ALL", unique = false, nullable = true, length = 2000)
	private String memberBillNoAll;
	
	/** 调查流水号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billAmt
	 */
	public void setBillAmt(java.math.BigDecimal billAmt) {
		this.billAmt = billAmt;
	}
	
    /**
     * @return billAmt
     */
	public java.math.BigDecimal getBillAmt() {
		return this.billAmt;
	}
	
	/**
	 * @param billBal
	 */
	public void setBillBal(java.math.BigDecimal billBal) {
		this.billBal = billBal;
	}
	
    /**
     * @return billBal
     */
	public java.math.BigDecimal getBillBal() {
		return this.billBal;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}
	
    /**
     * @return loanStartDate
     */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}
	
    /**
     * @return loanEndDate
     */
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}
	
    /**
     * @return appDate
     */
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param spouseCusName
	 */
	public void setSpouseCusName(String spouseCusName) {
		this.spouseCusName = spouseCusName;
	}
	
    /**
     * @return spouseCusName
     */
	public String getSpouseCusName() {
		return this.spouseCusName;
	}
	
	/**
	 * @param spouseCertCode
	 */
	public void setSpouseCertCode(String spouseCertCode) {
		this.spouseCertCode = spouseCertCode;
	}
	
    /**
     * @return spouseCertCode
     */
	public String getSpouseCertCode() {
		return this.spouseCertCode;
	}
	
	/**
	 * @param applyStatus
	 */
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}
	
    /**
     * @return applyStatus
     */
	public String getApplyStatus() {
		return this.applyStatus;
	}
	
	/**
	 * @param changeRs
	 */
	public void setChangeRs(String changeRs) {
		this.changeRs = changeRs;
	}
	
    /**
     * @return changeRs
     */
	public String getChangeRs() {
		return this.changeRs;
	}
	
	/**
	 * @param spouseMarryVal
	 */
	public void setSpouseMarryVal(String spouseMarryVal) {
		this.spouseMarryVal = spouseMarryVal;
	}
	
    /**
     * @return spouseMarryVal
     */
	public String getSpouseMarryVal() {
		return this.spouseMarryVal;
	}
	
	/**
	 * @param spousePhone
	 */
	public void setSpousePhone(String spousePhone) {
		this.spousePhone = spousePhone;
	}
	
    /**
     * @return spousePhone
     */
	public String getSpousePhone() {
		return this.spousePhone;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return evalAmt
     */
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param approvalBal
	 */
	public void setApprovalBal(java.math.BigDecimal approvalBal) {
		this.approvalBal = approvalBal;
	}
	
    /**
     * @return approvalBal
     */
	public java.math.BigDecimal getApprovalBal() {
		return this.approvalBal;
	}
	
	/**
	 * @param spouseCusId
	 */
	public void setSpouseCusId(String spouseCusId) {
		this.spouseCusId = spouseCusId;
	}
	
    /**
     * @return spouseCusId
     */
	public String getSpouseCusId() {
		return this.spouseCusId;
	}
	
	/**
	 * @param spouseBillNo
	 */
	public void setSpouseBillNo(String spouseBillNo) {
		this.spouseBillNo = spouseBillNo;
	}
	
    /**
     * @return spouseBillNo
     */
	public String getSpouseBillNo() {
		return this.spouseBillNo;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param spouseManagerId
	 */
	public void setSpouseManagerId(String spouseManagerId) {
		this.spouseManagerId = spouseManagerId;
	}
	
    /**
     * @return spouseManagerId
     */
	public String getSpouseManagerId() {
		return this.spouseManagerId;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	
    /**
     * @return managerName
     */
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param spouseManagerName
	 */
	public void setSpouseManagerName(String spouseManagerName) {
		this.spouseManagerName = spouseManagerName;
	}
	
    /**
     * @return spouseManagerName
     */
	public String getSpouseManagerName() {
		return this.spouseManagerName;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return execRateYear
     */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay;
	}
	
    /**
     * @return landUseWay
     */
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param repayTerm
	 */
	public void setRepayTerm(Integer repayTerm) {
		this.repayTerm = repayTerm;
	}
	
    /**
     * @return repayTerm
     */
	public Integer getRepayTerm() {
		return this.repayTerm;
	}
	
	/**
	 * @param overdueCount
	 */
	public void setOverdueCount(Integer overdueCount) {
		this.overdueCount = overdueCount;
	}
	
    /**
     * @return overdueCount
     */
	public Integer getOverdueCount() {
		return this.overdueCount;
	}
	
	/**
	 * @param billNoAll
	 */
	public void setBillNoAll(String billNoAll) {
		this.billNoAll = billNoAll;
	}
	
    /**
     * @return billNoAll
     */
	public String getBillNoAll() {
		return this.billNoAll;
	}
	
	/**
	 * @param memberOverdueCount
	 */
	public void setMemberOverdueCount(Integer memberOverdueCount) {
		this.memberOverdueCount = memberOverdueCount;
	}
	
    /**
     * @return memberOverdueCount
     */
	public Integer getMemberOverdueCount() {
		return this.memberOverdueCount;
	}
	
	/**
	 * @param memberBillNoAll
	 */
	public void setMemberBillNoAll(String memberBillNoAll) {
		this.memberBillNoAll = memberBillNoAll;
	}
	
    /**
     * @return memberBillNoAll
     */
	public String getMemberBillNoAll() {
		return this.memberBillNoAll;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}