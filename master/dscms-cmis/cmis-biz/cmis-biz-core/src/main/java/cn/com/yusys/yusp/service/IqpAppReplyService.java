package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.IqpAppReply;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.repository.mapper.IqpAppReplyMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAppReplyService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: wangyukun
 * @创建时间: 2021-05-05 13:46:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpAppReplyService {

    private static final Logger logger = LoggerFactory.getLogger(IqpAppReplyService.class);

    @Autowired
    private IqpAppReplyMapper iqpAppReplyMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpAppReply selectByPrimaryKey(String replyNo) {
        return iqpAppReplyMapper.selectByPrimaryKey(replyNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpAppReply> selectAll(QueryModel model) {
        List<IqpAppReply> records = (List<IqpAppReply>) iqpAppReplyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpAppReply> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpAppReply> list = iqpAppReplyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpAppReply record) {
        return iqpAppReplyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpAppReply record) {
        return iqpAppReplyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpAppReply record) {
        return iqpAppReplyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpAppReply record) {
        return iqpAppReplyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String replyNo) {
        return iqpAppReplyMapper.deleteByPrimaryKey(replyNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpAppReplyMapper.deleteByIds(ids);
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.domain.IqpAppReply
     * @author 王玉坤
     * @date 2021/5/5 16:17
     * @version 1.0.0
     * @desc 生成零售批复信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public IqpAppReply insertIqpAppReply(IqpLoanApp iqpLoanApp) {
        logger.info("根据申请流水号【{}】生成批复信息开始", iqpLoanApp.getIqpSerno());

        // 对象声明
        IqpAppReply iqpAppReply = null;
        String replyNo = null;
        int nums = 0;
        try {
            iqpAppReply = new IqpAppReply();
            cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(iqpLoanApp, iqpAppReply);
            // 生成批复序列号
            replyNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
            logger.info("根据申请流水号【{}】生成批复编号【{}】", iqpLoanApp.getIqpSerno(), replyNo);

            iqpAppReply.setReplyNo(replyNo);
            //iqpAppReply.setAppAmt(iqpLoanApp.getContAmt());
            iqpAppReply.setCurType("CHN");
            iqpAppReply.setGuarMode(iqpLoanApp.getGuarWay());
            iqpAppReply.setReplyTime(DateUtils.getCurrDateStr());
            iqpAppReply.setTerm(iqpLoanApp.getAppTerm().intValue());
            iqpAppReply.setRepayMode(iqpLoanApp.getRepayMode());
            iqpAppReply.setReplyStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            iqpAppReply.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            iqpAppReply.setUpdateTime(DateUtils.getCurrDate());
            iqpAppReply.setCreateTime(DateUtils.getCurrDate());

            nums = this.insert(iqpAppReply);
            Asserts.isTrue(nums > 0, "插入批复信息出错！");
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            logger.info("根据申请流水号【{}】生成批复信息结束", iqpLoanApp.getIqpSerno());
        }
        return iqpAppReply;
    }
}
