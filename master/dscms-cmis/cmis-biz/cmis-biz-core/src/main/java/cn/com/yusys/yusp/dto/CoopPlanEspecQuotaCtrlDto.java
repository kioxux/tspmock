package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanEspecQuotaCtrl
 * @类描述: coop_plan_espec_quota_ctrl数据实体类
 * @功能描述:
 * @创建人: pc
 * @创建时间: 2021-04-15 22:00:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CoopPlanEspecQuotaCtrlDto {
    /**
     * 合作方案
     **/
    private String coopPrdName;
    /**
     * 主键
     **/
    private String pkId;

    /**
     * 申请流水号
     **/
    private String serno;

    /**
     * 合作方案编号
     **/
    private String coopPlanNo;

    /**
     * 合作产品编号
     **/
    private String coopPrdId;

    /** 产品属性 **/
    private String prdTypeProp;

    /**
     * 单个产品合作额度(元)
     **/
    private java.math.BigDecimal singlePrdCoopLmt;

    /**
     * 单笔最低缴存金额(元)
     **/
    private java.math.BigDecimal sigLowDepositAmt;

    /**
     * 保证金比例
     **/
    private java.math.BigDecimal bailPerc;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最近修改人
     **/
    private String updId;

    /**
     * 最近修改机构
     **/
    private String updBrId;

    /**
     * 最近修改日期
     **/
    private String updDate;

    /**
     * 操作类型
     **/
    private String oprType;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;


    public String getCoopPrdName() {
        return coopPrdName;
    }

    public void setCoopPrdName(String coopPrdName) {
        this.coopPrdName = coopPrdName;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCoopPlanNo() {
        return coopPlanNo;
    }

    public void setCoopPlanNo(String coopPlanNo) {
        this.coopPlanNo = coopPlanNo;
    }

    public String getCoopPrdId() {
        return coopPrdId;
    }

    public void setCoopPrdId(String coopPrdId) {
        this.coopPrdId = coopPrdId;
    }

    public String getPrdTypeProp() {
        return prdTypeProp;
    }

    public void setPrdTypeProp(String prdTypeProp) {
        this.prdTypeProp = prdTypeProp;
    }

    public BigDecimal getSinglePrdCoopLmt() {
        return singlePrdCoopLmt;
    }

    public void setSinglePrdCoopLmt(BigDecimal singlePrdCoopLmt) {
        this.singlePrdCoopLmt = singlePrdCoopLmt;
    }

    public BigDecimal getSigLowDepositAmt() {
        return sigLowDepositAmt;
    }

    public void setSigLowDepositAmt(BigDecimal sigLowDepositAmt) {
        this.sigLowDepositAmt = sigLowDepositAmt;
    }

    public BigDecimal getBailPerc() {
        return bailPerc;
    }

    public void setBailPerc(BigDecimal bailPerc) {
        this.bailPerc = bailPerc;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}