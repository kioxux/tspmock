/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtModelApprResultInfo
 * @类描述: lmt_model_appr_result_info数据实体类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-04-19 10:41:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_model_appr_result_info")
public class LmtModelApprResultInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SURVEY_SERNO")
	private String surveySerno;
	
	/** 模型审批时间 **/
	@Column(name = "MODEL_APPR_TIME", unique = false, nullable = true, length = 20)
	private String modelApprTime;
	
	/** 模型评级 **/
	@Column(name = "MODEL_GRADE", unique = false, nullable = true, length = 5)
	private String modelGrade;
	
	/** 模型得分 **/
	@Column(name = "MODEL_SCORE", unique = false, nullable = true, length = 10)
	private String modelScore;
	
	/** 模型金额 **/
	@Column(name = "MODEL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelAmt;
	
	/** 模型利率 **/
	@Column(name = "MODEL_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelRate;
	
	/** 司法案件判断 **/
	@Column(name = "JOJC", unique = false, nullable = true, length = 20)
	private String jojc;
	
	/** 模型结果状态 **/
	@Column(name = "MODEL_RST_STATUS", unique = false, nullable = true, length = 5)
	private String modelRstStatus;
	
	/** 业务唯一编号 **/
	@Column(name = "BIZ_UNIQUE_NO", unique = false, nullable = true, length = 40)
	private String bizUniqueNo;
	
	/** 模型意见 **/
	@Column(name = "MODEL_ADVICE", unique = false, nullable = true, length = 500)
	private String modelAdvice;
	
	/** 模型初始额度 **/
	@Column(name = "MODEL_FST_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelFstLmt;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param modelApprTime
	 */
	public void setModelApprTime(String modelApprTime) {
		this.modelApprTime = modelApprTime;
	}
	
    /**
     * @return modelApprTime
     */
	public String getModelApprTime() {
		return this.modelApprTime;
	}
	
	/**
	 * @param modelGrade
	 */
	public void setModelGrade(String modelGrade) {
		this.modelGrade = modelGrade;
	}
	
    /**
     * @return modelGrade
     */
	public String getModelGrade() {
		return this.modelGrade;
	}
	
	/**
	 * @param modelScore
	 */
	public void setModelScore(String modelScore) {
		this.modelScore = modelScore;
	}
	
    /**
     * @return modelScore
     */
	public String getModelScore() {
		return this.modelScore;
	}
	
	/**
	 * @param modelAmt
	 */
	public void setModelAmt(java.math.BigDecimal modelAmt) {
		this.modelAmt = modelAmt;
	}
	
    /**
     * @return modelAmt
     */
	public java.math.BigDecimal getModelAmt() {
		return this.modelAmt;
	}
	
	/**
	 * @param modelRate
	 */
	public void setModelRate(java.math.BigDecimal modelRate) {
		this.modelRate = modelRate;
	}
	
    /**
     * @return modelRate
     */
	public java.math.BigDecimal getModelRate() {
		return this.modelRate;
	}
	
	/**
	 * @param jojc
	 */
	public void setJojc(String jojc) {
		this.jojc = jojc;
	}
	
    /**
     * @return jojc
     */
	public String getJojc() {
		return this.jojc;
	}
	
	/**
	 * @param modelRstStatus
	 */
	public void setModelRstStatus(String modelRstStatus) {
		this.modelRstStatus = modelRstStatus;
	}
	
    /**
     * @return modelRstStatus
     */
	public String getModelRstStatus() {
		return this.modelRstStatus;
	}
	
	/**
	 * @param bizUniqueNo
	 */
	public void setBizUniqueNo(String bizUniqueNo) {
		this.bizUniqueNo = bizUniqueNo;
	}
	
    /**
     * @return bizUniqueNo
     */
	public String getBizUniqueNo() {
		return this.bizUniqueNo;
	}
	
	/**
	 * @param modelAdvice
	 */
	public void setModelAdvice(String modelAdvice) {
		this.modelAdvice = modelAdvice;
	}
	
    /**
     * @return modelAdvice
     */
	public String getModelAdvice() {
		return this.modelAdvice;
	}
	
	/**
	 * @param modelFstLmt
	 */
	public void setModelFstLmt(java.math.BigDecimal modelFstLmt) {
		this.modelFstLmt = modelFstLmt;
	}
	
    /**
     * @return modelFstLmt
     */
	public java.math.BigDecimal getModelFstLmt() {
		return this.modelFstLmt;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}