package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;

public class AccEntrustLoanTxnDetailsDto {
    private static final long serialVersionUID = 1L;

    /** 核心交易流水号 **/
    private String hxSerno;

    /** 合同编号 **/
    private String contNo;

    /** 借据编号 **/
    private String billNo;

    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 交易明细类型 **/
    private String txnDetailType;

    /** 币种 **/
    private String curType;

    /** 交易本金金额 **/
    private BigDecimal txnBjAmt;

    /** 交易本金利息 **/
    private BigDecimal txnBjInt;

    /** 交易日期 **/
    private String tranDate;

    /** 余额 **/
    private BigDecimal txnBjBalance;

    /** 增减 **/
    private String AddOrReduce;

    public String getHxSerno() {
        return hxSerno;
    }

    public void setHxSerno(String hxSerno) {
        this.hxSerno = hxSerno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getTxnDetailType() {
        return txnDetailType;
    }

    public void setTxnDetailType(String txnDetailType) {
        this.txnDetailType = txnDetailType;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getTxnBjAmt() {
        return txnBjAmt;
    }

    public void setTxnBjAmt(BigDecimal txnBjAmt) {
        this.txnBjAmt = txnBjAmt;
    }

    public BigDecimal getTxnBjInt() {
        return txnBjInt;
    }

    public void setTxnBjInt(BigDecimal txnBjInt) {
        this.txnBjInt = txnBjInt;
    }

    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    public BigDecimal getTxnBjBalance() {
        return txnBjBalance;
    }

    public void setTxnBjBalance(BigDecimal txnBjBalance) {
        this.txnBjBalance = txnBjBalance;
    }

    public String getAddOrReduce() {
        return AddOrReduce;
    }

    public void setAddOrReduce(String addOrReduce) {
        AddOrReduce = addOrReduce;
    }
}
