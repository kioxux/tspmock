package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-eval-core模块
 * @类名称: GuarEvalOutEvalInfo
 * @类描述: guar_eval_out_eval_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-29 10:05:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarEvalOutEvalInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 外评流水号 **/
	private String outEvalSerno;
	
	/** 估值申请流水号 **/
	private String evalApplySerno;
	
	/** 评估机构名称 **/
	private String evalOrgName;
	
	/** 外部评估机构组织机构代码 **/
	private String evalOrgNo;
	
	/** 是否有外部预评估报告 **/
	private String isPreReport;
	
	/** 外部预评估报告的评估价值 **/
	private java.math.BigDecimal preOutAmt;
	
	/** 外部正式评估报告的评估价值 **/
	private java.math.BigDecimal evalOutAmt;
	
	/** 外部评估价值币种 **/
	private String evalInCurrency;
	
	/** 外部机构评估日期 **/
	private String evalOutDate;
	
	/** 外部评估价值有效期截止日 **/
	private String outEndDate;
	
	/** 评估师姓名 **/
	private String evalerName;
	
	/** 评估师证书编号 **/
	private String evalerLiceNo;
	
	/** 评估师身份证号码 **/
	private String evalerNo;
	
	/** 评估师移动电话 **/
	private String evalerPhone;
	
	/** 评估师评估类型 **/
	private String evalerType;
	
	/** 房地产评估师资质等级 **/
	private String roomAptiLevel;
	
	/** 土地评估师资质等级 **/
	private String landAptiLevel;
	
	/** 资产评估师资质等级 **/
	private String propertyAptiLevel;
	
	/** 其他评估类型 **/
	private String otherType;
	
	/** 其他评估师资质等级 **/
	private String otherAptiLevel;
	
	/** 评估类型 **/
	private String evalType;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	
	/**
	 * @param outEvalSerno
	 */
	public void setOutEvalSerno(String outEvalSerno) {
		this.outEvalSerno = outEvalSerno == null ? null : outEvalSerno.trim();
	}
	
    /**
     * @return OutEvalSerno
     */	
	public String getOutEvalSerno() {
		return this.outEvalSerno;
	}
	
	/**
	 * @param evalApplySerno
	 */
	public void setEvalApplySerno(String evalApplySerno) {
		this.evalApplySerno = evalApplySerno == null ? null : evalApplySerno.trim();
	}
	
    /**
     * @return EvalApplySerno
     */	
	public String getEvalApplySerno() {
		return this.evalApplySerno;
	}
	
	/**
	 * @param evalOrgName
	 */
	public void setEvalOrgName(String evalOrgName) {
		this.evalOrgName = evalOrgName == null ? null : evalOrgName.trim();
	}
	
    /**
     * @return EvalOrgName
     */	
	public String getEvalOrgName() {
		return this.evalOrgName;
	}
	
	/**
	 * @param evalOrgNo
	 */
	public void setEvalOrgNo(String evalOrgNo) {
		this.evalOrgNo = evalOrgNo == null ? null : evalOrgNo.trim();
	}
	
    /**
     * @return EvalOrgNo
     */	
	public String getEvalOrgNo() {
		return this.evalOrgNo;
	}
	
	/**
	 * @param isPreReport
	 */
	public void setIsPreReport(String isPreReport) {
		this.isPreReport = isPreReport == null ? null : isPreReport.trim();
	}
	
    /**
     * @return IsPreReport
     */	
	public String getIsPreReport() {
		return this.isPreReport;
	}
	
	/**
	 * @param preOutAmt
	 */
	public void setPreOutAmt(java.math.BigDecimal preOutAmt) {
		this.preOutAmt = preOutAmt;
	}
	
    /**
     * @return PreOutAmt
     */	
	public java.math.BigDecimal getPreOutAmt() {
		return this.preOutAmt;
	}
	
	/**
	 * @param evalOutAmt
	 */
	public void setEvalOutAmt(java.math.BigDecimal evalOutAmt) {
		this.evalOutAmt = evalOutAmt;
	}
	
    /**
     * @return EvalOutAmt
     */	
	public java.math.BigDecimal getEvalOutAmt() {
		return this.evalOutAmt;
	}
	
	/**
	 * @param evalInCurrency
	 */
	public void setEvalInCurrency(String evalInCurrency) {
		this.evalInCurrency = evalInCurrency == null ? null : evalInCurrency.trim();
	}
	
    /**
     * @return EvalInCurrency
     */	
	public String getEvalInCurrency() {
		return this.evalInCurrency;
	}
	
	/**
	 * @param evalOutDate
	 */
	public void setEvalOutDate(String evalOutDate) {
		this.evalOutDate = evalOutDate == null ? null : evalOutDate.trim();
	}
	
    /**
     * @return EvalOutDate
     */	
	public String getEvalOutDate() {
		return this.evalOutDate;
	}
	
	/**
	 * @param outEndDate
	 */
	public void setOutEndDate(String outEndDate) {
		this.outEndDate = outEndDate == null ? null : outEndDate.trim();
	}
	
    /**
     * @return OutEndDate
     */	
	public String getOutEndDate() {
		return this.outEndDate;
	}
	
	/**
	 * @param evalerName
	 */
	public void setEvalerName(String evalerName) {
		this.evalerName = evalerName == null ? null : evalerName.trim();
	}
	
    /**
     * @return EvalerName
     */	
	public String getEvalerName() {
		return this.evalerName;
	}
	
	/**
	 * @param evalerLiceNo
	 */
	public void setEvalerLiceNo(String evalerLiceNo) {
		this.evalerLiceNo = evalerLiceNo == null ? null : evalerLiceNo.trim();
	}
	
    /**
     * @return EvalerLiceNo
     */	
	public String getEvalerLiceNo() {
		return this.evalerLiceNo;
	}
	
	/**
	 * @param evalerNo
	 */
	public void setEvalerNo(String evalerNo) {
		this.evalerNo = evalerNo == null ? null : evalerNo.trim();
	}
	
    /**
     * @return EvalerNo
     */	
	public String getEvalerNo() {
		return this.evalerNo;
	}
	
	/**
	 * @param evalerPhone
	 */
	public void setEvalerPhone(String evalerPhone) {
		this.evalerPhone = evalerPhone == null ? null : evalerPhone.trim();
	}
	
    /**
     * @return EvalerPhone
     */	
	public String getEvalerPhone() {
		return this.evalerPhone;
	}
	
	/**
	 * @param evalerType
	 */
	public void setEvalerType(String evalerType) {
		this.evalerType = evalerType == null ? null : evalerType.trim();
	}
	
    /**
     * @return EvalerType
     */	
	public String getEvalerType() {
		return this.evalerType;
	}
	
	/**
	 * @param roomAptiLevel
	 */
	public void setRoomAptiLevel(String roomAptiLevel) {
		this.roomAptiLevel = roomAptiLevel == null ? null : roomAptiLevel.trim();
	}
	
    /**
     * @return RoomAptiLevel
     */	
	public String getRoomAptiLevel() {
		return this.roomAptiLevel;
	}
	
	/**
	 * @param landAptiLevel
	 */
	public void setLandAptiLevel(String landAptiLevel) {
		this.landAptiLevel = landAptiLevel == null ? null : landAptiLevel.trim();
	}
	
    /**
     * @return LandAptiLevel
     */	
	public String getLandAptiLevel() {
		return this.landAptiLevel;
	}
	
	/**
	 * @param propertyAptiLevel
	 */
	public void setPropertyAptiLevel(String propertyAptiLevel) {
		this.propertyAptiLevel = propertyAptiLevel == null ? null : propertyAptiLevel.trim();
	}
	
    /**
     * @return PropertyAptiLevel
     */	
	public String getPropertyAptiLevel() {
		return this.propertyAptiLevel;
	}
	
	/**
	 * @param otherType
	 */
	public void setOtherType(String otherType) {
		this.otherType = otherType == null ? null : otherType.trim();
	}
	
    /**
     * @return OtherType
     */	
	public String getOtherType() {
		return this.otherType;
	}
	
	/**
	 * @param otherAptiLevel
	 */
	public void setOtherAptiLevel(String otherAptiLevel) {
		this.otherAptiLevel = otherAptiLevel == null ? null : otherAptiLevel.trim();
	}
	
    /**
     * @return OtherAptiLevel
     */	
	public String getOtherAptiLevel() {
		return this.otherAptiLevel;
	}
	
	/**
	 * @param evalType
	 */
	public void setEvalType(String evalType) {
		this.evalType = evalType == null ? null : evalType.trim();
	}
	
    /**
     * @return EvalType
     */	
	public String getEvalType() {
		return this.evalType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}


}