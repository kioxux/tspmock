/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanFirstPayInfo
 * @类描述: iqp_loan_first_pay_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 10:07:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_loan_first_pay_info")
public class IqpLoanFirstPayInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = true, length = 40)
	private String iqpSerno;
	
	/** 购房合同号 **/
	@Column(name = "PUR_HOUSE_CONT", unique = false, nullable = true, length = 150)
	private String purHouseCont;
	
	/** 支付日期 **/
	@Column(name = "DEFRAY_DATE", unique = false, nullable = true, length = 10)
	private String defrayDate;
	
	/** 支付方式 **/
	@Column(name = "PAY_MODE", unique = false, nullable = true, length = 5)
	private String payMode;
	
	/** 支付金额 **/
	@Column(name = "PAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal payAmt;
	
	/** 是否通过银行流水核实 **/
	@Column(name = "BANK_FLOW_VERIFY_FLAG", unique = false, nullable = true, length = 1)
	private String bankFlowVerifyFlag;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param purHouseCont
	 */
	public void setPurHouseCont(String purHouseCont) {
		this.purHouseCont = purHouseCont;
	}
	
    /**
     * @return purHouseCont
     */
	public String getPurHouseCont() {
		return this.purHouseCont;
	}
	
	/**
	 * @param defrayDate
	 */
	public void setDefrayDate(String defrayDate) {
		this.defrayDate = defrayDate;
	}
	
    /**
     * @return defrayDate
     */
	public String getDefrayDate() {
		return this.defrayDate;
	}
	
	/**
	 * @param payMode
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	
    /**
     * @return payMode
     */
	public String getPayMode() {
		return this.payMode;
	}
	
	/**
	 * @param payAmt
	 */
	public void setPayAmt(java.math.BigDecimal payAmt) {
		this.payAmt = payAmt;
	}
	
    /**
     * @return payAmt
     */
	public java.math.BigDecimal getPayAmt() {
		return this.payAmt;
	}
	
	/**
	 * @param bankFlowVerifyFlag
	 */
	public void setBankFlowVerifyFlag(String bankFlowVerifyFlag) {
		this.bankFlowVerifyFlag = bankFlowVerifyFlag;
	}
	
    /**
     * @return bankFlowVerifyFlag
     */
	public String getBankFlowVerifyFlag() {
		return this.bankFlowVerifyFlag;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}