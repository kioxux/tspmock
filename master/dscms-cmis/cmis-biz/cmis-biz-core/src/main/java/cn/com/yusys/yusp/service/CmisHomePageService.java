package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.HomePageDto;
import cn.com.yusys.yusp.dto.BizAccLoanDto;
import cn.com.yusys.yusp.dto.BizCtrLoanContDto;
import cn.com.yusys.yusp.enums.online.DscmsBizEnum;
import cn.com.yusys.yusp.flow.api.WFBenchClient;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceTodoDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: 首页模块
 * @类名称: CmisHomePageService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 15430
 * @创建时间: 2020-11-14 09:59:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisHomePageService {
    private static final Logger log = LoggerFactory.getLogger(CmisHomePageService.class);

    @Autowired
    private WFBenchClient wfBenchClient;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    // @Autowired
    // private IGuarClientService iGuarClientService;

    /**
     * 待签合同业务
     **/
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private AccLoanService accLoanService;


    @Autowired
    private LmtGrpAppService lmtGrpAppService;
    /**
     * 待办任务
     *
     * @param queryModel
     * @return
     */
    public List<HomePageDto> todo(QueryModel queryModel) {
        //获取当前用户的待办任务
        ResultDto<List<ResultInstanceTodoDto>> resultDto = wfBenchClient.todo(queryModel);
        List<ResultInstanceTodoDto> resultInstanceTodoDtos = resultDto.getData();
        /** 额度审批 **/
        List<ResultInstanceTodoDto> lmtApps = new ArrayList<ResultInstanceTodoDto>();
        /** 业务办理 **/
        List<ResultInstanceTodoDto> busHands = new ArrayList<ResultInstanceTodoDto>();
        /** 待签合同 **/
        List<BizCtrLoanContDto> todoConts = ctrLoanContService.
                selectCtrLoanContByCorreManagerId((String) queryModel.getCondition().get("userId"));
        /** 待放款 **/
        List<ResultInstanceTodoDto> todoLoans = new ArrayList<ResultInstanceTodoDto>();
        for (ResultInstanceTodoDto resultInstanceTodoDto : resultInstanceTodoDtos) {
            String bizType = resultInstanceTodoDto.getBizType();
            //额度审批:个人额度申请 LMT_INDIV_APP;合作方额度申请 WF_LMT_COOP_APP ;融资担保额度申请(待定)
            if (DscmsBizEnum.WF_LMT_COOP_APP.value.equals(bizType) ||
                    DscmsBizEnum.LMT_INDIV_APP.value.equals(bizType)) {
                lmtApps.add(resultInstanceTodoDto);
            }
            //业务办理:单笔单批申请;额度项下申请;特殊业务申请
            else if (DscmsBizEnum.IQP_SINGLE_BATCH_APP.value.equals(bizType) ||
                    DscmsBizEnum.IQP_UNDER_QUATO_APP.value.equals(bizType) ||
                    DscmsBizEnum.IQP_SPECIAL_APP.value.equals(bizType)) {
                busHands.add(resultInstanceTodoDto);
            }
            //贷款签放:放款申请;展期出账申请(待定)
            else if (DscmsBizEnum.PVP_LOAN_APP.value.equals(bizType)) {
                todoLoans.add(resultInstanceTodoDto);
            }
        }

        List<HomePageDto> homePageDtos = new ArrayList<HomePageDto>();
        /** 额度审批 **/
        HomePageDto lmtAppHomePageDto = new HomePageDto();
        lmtAppHomePageDto.setTitle(CmisBizConstants.TATLE_LMT_APP);
        // 返回给前端的业务种类
        lmtAppHomePageDto.setBizType(new String[]{DscmsBizEnum.WF_LMT_COOP_APP.value, DscmsBizEnum.LMT_INDIV_APP.value});
        lmtAppHomePageDto.setTotal(BigDecimal.ZERO.intValue());
        lmtAppHomePageDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        lmtAppHomePageDto.setData(null);
        if (!CollectionUtils.isEmpty(lmtApps) && lmtApps.size() > 0) {
            lmtAppHomePageDto.setTotal(lmtApps.size());
        }
        homePageDtos.add(lmtAppHomePageDto);

        /** 业务办理 **/
        HomePageDto busHandHomePageDto = new HomePageDto();
        busHandHomePageDto.setTitle(CmisBizConstants.TATLE_BUS_HAND);
        busHandHomePageDto.setBizType(new String[]{DscmsBizEnum.IQP_SINGLE_BATCH_APP.value(), DscmsBizEnum.IQP_UNDER_QUATO_APP.value(), DscmsBizEnum.IQP_SPECIAL_APP.value()});
        busHandHomePageDto.setTotal(BigDecimal.ZERO.intValue());
        busHandHomePageDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        busHandHomePageDto.setData(null);
        if (!CollectionUtils.isEmpty(busHands) && busHands.size() > 0) {
            busHandHomePageDto.setTotal(busHands.size());
        }
        homePageDtos.add(busHandHomePageDto);

        /** 待签合同 **/
        HomePageDto contHomePageDto = new HomePageDto();
        contHomePageDto.setTitle(CmisBizConstants.TATLE_TODO_CONT);
        contHomePageDto.setBizType(new String[]{CmisBizConstants.BIZ_TYPE_TODO_CONT});
        contHomePageDto.setTotal(BigDecimal.ZERO.intValue());
        contHomePageDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_1);
        contHomePageDto.setData(todoConts);
        if (!CollectionUtils.isEmpty(todoConts) && todoConts.size() > 0) {
            contHomePageDto.setTotal(todoConts.size());
        }
        homePageDtos.add(contHomePageDto);

        /** 待放款 **/
        HomePageDto loanHomePageDto = new HomePageDto();
        loanHomePageDto.setTitle(CmisBizConstants.TATLE_TODO_LOAN);
        loanHomePageDto.setBizType(new String[]{DscmsBizEnum.PVP_LOAN_APP.value});
        loanHomePageDto.setTotal(BigDecimal.ZERO.intValue());
        loanHomePageDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        loanHomePageDto.setData(null);
        if (!CollectionUtils.isEmpty(todoLoans) && todoLoans.size() > 0) {
            loanHomePageDto.setTotal(todoLoans.size());
        }

        //授信明细
        HomePageDto memAppPageDto = new HomePageDto();
        memAppPageDto.setTitle(CmisBizConstants.MEM_LIST);
        memAppPageDto.setBizType(new String[]{DscmsBizEnum.PVP_LOAN_APP.value});
        memAppPageDto.setTotal(BigDecimal.ZERO.intValue());
        memAppPageDto.setFlag("3");
        //用于传送流水号
        User userInfo = SessionUtils.getUserInformation();
        int count = lmtGrpAppService.queryRemindCountByManagerId(userInfo.getLoginCode());
        memAppPageDto.setTotal(count);


        homePageDtos.add(memAppPageDto);
        homePageDtos.add(loanHomePageDto);
        return homePageDtos;
    }

    /**
     * 业务提醒
     *
     * @param queryModel
     * @return
     */
    public List<HomePageDto> busRemind(QueryModel queryModel) {
        Map map = queryModel.getCondition();
		String userId = (String) map.get("userId");
        String type = (String) map.get("type");
        if (StringUtils.isBlank(type)) {
            type = CmisBizConstants.FLOW_QUERY_TYPE_2;
        }
        ResultDto<List<ResultCommentDto>> resultDto = workflowCoreClient.getCommentsByUserId(userId, type);

        /*wfBenchClient.todo(queryModel);*/
        List<ResultCommentDto> resultCommentDtos = resultDto.getData();
        //审批打回
        List<ResultCommentDto> callBackList = new ArrayList<ResultCommentDto>();
        //审批否决
        List<ResultCommentDto> refuseList = new ArrayList<ResultCommentDto>();
        for (ResultCommentDto resultCommentDto : resultCommentDtos) {
            //意见标记
            String commentSign = resultCommentDto.getCommentSign();
            if (OpType.CALL_BACK.equals(commentSign)) {
                //打回操作
                callBackList.add(resultCommentDto);
            } else if (OpType.REFUSE.equals(commentSign)) {
                //否决操作
                refuseList.add(resultCommentDto);
            }
        }
        List<HomePageDto> homePageDtos = new ArrayList<HomePageDto>();
        /** 审批打回 **/
        callBack(callBackList, homePageDtos);
        /** 审批否決 **/
        refuse(refuseList, homePageDtos);
        /** 15天内待还款 **/
        waitRepayForFifteenDay(map, homePageDtos);
        /** 权证借出 **/
        warrantLoan(map, homePageDtos);
        /** 逾期 **/
        overdue(map, homePageDtos);
        /** 到期前十的业务 **/
        matureToptenForBus(map, homePageDtos);
        return homePageDtos;
    }

    /**
     * 业务统计
     *
     * @param queryModel
     * @return
     */
    public List<HomePageDto> busStatistics(QueryModel queryModel) {
        Map map = queryModel.getCondition();
        List<HomePageDto> homePageDtos = new ArrayList<HomePageDto>();
        //业务统计-笔数
        getBusRows(map, homePageDtos);
        //业务统计-金额（万元）
        getBusAmt(map, homePageDtos);
        //业务统计-余额（万元）
        getBusBalance(map, homePageDtos);
        return homePageDtos;
    }

    /**
     * 业务统计-笔数
     *
     * @param map
     * @param homePageDtos
     */
    public void getBusRows(Map map, List<HomePageDto> homePageDtos) {
        List<Map> busRowsList = ctrLoanContService.getBusRows(map);
        HomePageDto busRowsDto = new HomePageDto();
        busRowsDto.setTitle(CmisBizConstants.TATLE_BUS_ROWS);
        busRowsDto.setBizType(new String[]{CmisBizConstants.BIZ_TYPE_BUS_ROWS});
        busRowsDto.setTotal(BigDecimal.ZERO.intValue());
        busRowsDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        busRowsDto.setData(busRowsList);
        if (!CollectionUtils.isEmpty(busRowsList) && busRowsList.size() > 0) {
            busRowsDto.setTotal(busRowsList.size());
        }
        homePageDtos.add(busRowsDto);
    }

    /**
     * 业务统计-金额（万元）
     *
     * @param map
     * @param homePageDtos
     */
    public void getBusAmt(Map map, List<HomePageDto> homePageDtos) {
        List<Map> busAmtList = ctrLoanContService.getBusAmt(map);
        HomePageDto busAmtDto = new HomePageDto();
        busAmtDto.setTitle(CmisBizConstants.TATLE_BUS_AMT);
        busAmtDto.setBizType(new String[]{CmisBizConstants.BIZ_TYPE_BUS_AMT});
        busAmtDto.setTotal(BigDecimal.ZERO.intValue());
        busAmtDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        busAmtDto.setData(busAmtList);
        if (!CollectionUtils.isEmpty(busAmtList) && busAmtList.size() > 0) {
            busAmtDto.setTotal(busAmtList.size());
        }
        homePageDtos.add(busAmtDto);
    }

    /**
     * 业务统计-余额（万元）
     *
     * @param map
     * @param homePageDtos
     */
    public void getBusBalance(Map map, List<HomePageDto> homePageDtos) {
        List<Map> lists = accLoanService.getBusBalance(map);
        HomePageDto homePageDto = new HomePageDto();
        homePageDto.setTitle(CmisBizConstants.TATLE_BUS_BALANCE);
        homePageDto.setBizType(new String[]{CmisBizConstants.BIZ_TYPE_BUS_BALANCE});
        homePageDto.setTotal(BigDecimal.ZERO.intValue());
        homePageDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        homePageDto.setData(lists);
        if (!CollectionUtils.isEmpty(lists) && lists.size() > 0) {
            homePageDto.setTotal(lists.size());
        }
        homePageDtos.add(homePageDto);
    }

    /**
     * 审批打回
     *
     * @param list
     * @param homePageDtos
     */
    public void callBack(List<ResultCommentDto> list, List<HomePageDto> homePageDtos) {

        HomePageDto callBackDto = new HomePageDto();
        callBackDto.setTitle(CmisBizConstants.TATLE_CALL_BACK);
        callBackDto.setBizType(new String[]{CmisBizConstants.BIZ_TYPE_CALL_BACK});
        callBackDto.setTotal(BigDecimal.ZERO.intValue());
        callBackDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        callBackDto.setData(null);
        //过滤重复的instanceid，流程涉及到重复打回
        if (!CollectionUtils.isEmpty(list) && list.size() > 0) {
            Set<String> instanceSet = new HashSet<String>();
            for (ResultCommentDto resultCommentDto : list) {
                instanceSet.add(resultCommentDto.getInstanceId());
            }
            callBackDto.setData(instanceSet);
            callBackDto.setTotal(instanceSet.size());
        }
        log.info("输出========================ii======================输出：");
        homePageDtos.add(callBackDto);
    }

    /**
     * 审批否決
     *
     * @param list
     * @param homePageDtos
     */
    public void refuse(List<ResultCommentDto> list, List<HomePageDto> homePageDtos) {
        HomePageDto refuseDto = new HomePageDto();
        refuseDto.setTitle(CmisBizConstants.TATLE_REFUSE);
        refuseDto.setBizType(new String[]{CmisBizConstants.BIZ_TYPE_REFUSE});
        refuseDto.setTotal(BigDecimal.ZERO.intValue());
        refuseDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        if (!CollectionUtils.isEmpty(list) && list.size() > 0) {
            refuseDto.setTotal(list.size());
        }
        refuseDto.setData(null);
        homePageDtos.add(refuseDto);
    }

    /**
     * 15天内待还款
     * 需要查询查询还款计划表，暂保留
     *
     * @param map
     * @param homePageDtos
     */
    public void waitRepayForFifteenDay(Map map, List<HomePageDto> homePageDtos) {
        HomePageDto waitRepayDto = new HomePageDto();
        waitRepayDto.setTitle(CmisBizConstants.TATLE_WAIT_REPAY_FOR_FIFTEEN_DAYS);
        waitRepayDto.setBizType(new String[]{CmisBizConstants.BIZ_TYPE_WAIT_REPAY_FOR_FIFTEEN_DAYS});
        waitRepayDto.setTotal(BigDecimal.ZERO.intValue());
        waitRepayDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        waitRepayDto.setData(null);
        waitRepayDto.setTotal(accLoanService.countAccLoanByDays(15));
        homePageDtos.add(waitRepayDto);
    }

    /**
     * 权证借出
     *
     * @param map
     * @param homePageDtos
     * @return
     */
    public void warrantLoan(Map map, List<HomePageDto> homePageDtos) {
//        GuarClientRsDto guarClientRsDto = null;//iGuarClientService.getGuarCertiRelaByCertiState(map);
//        List warrantLoanList = (List) guarClientRsDto.getData();
//        HomePageDto warrantLoanDto = new HomePageDto();
//        warrantLoanDto.setTitle(CmisBizConstants.TATLE_WARRANT_LOAN);
//        warrantLoanDto.setBizType(new String[]{CmisBizConstants.BIZ_TYPE_WARRANT_LOAN});
//        warrantLoanDto.setTotal(BigDecimal.ZERO.intValue());
//        warrantLoanDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
//        warrantLoanDto.setData(warrantLoanList);
//        if (!CollectionUtils.isEmpty(warrantLoanList) && warrantLoanList.size() > 0) {
//            warrantLoanDto.setTotal(warrantLoanList.size());
//        }
//        homePageDtos.add(warrantLoanDto);
    }

    /**
     * 逾期
     *
     * @param map
     * @return
     */
    public void overdue(Map map, List<HomePageDto> homePageDtos) {
        BizAccLoanDto bizAccLoanDto = new BizAccLoanDto();
        bizAccLoanDto.setManagerId((String) map.get("userId"));
        bizAccLoanDto.setBizType(CmisBizConstants.BIZ_TYPE_OVERDUE);
        List<BizAccLoanDto> overdueList = accLoanService.getAccLoan(bizAccLoanDto);
        HomePageDto overdueDto = new HomePageDto();
        overdueDto.setTitle(CmisBizConstants.TATLE_OVERDUE);
        overdueDto.setBizType(new String[]{CmisBizConstants.BIZ_TYPE_OVERDUE});
        overdueDto.setTotal(BigDecimal.ZERO.intValue());
        overdueDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        overdueDto.setData(overdueList);
        if (!CollectionUtils.isEmpty(overdueList) && overdueList.size() > 0) {
            overdueDto.setTotal(overdueList.size());
        }
        homePageDtos.add(overdueDto);
    }

    /**
     * 到期前十天业务(贷款台账)
     *
     * @param map
     * @return
     */
    public void matureToptenForBus(Map map, List<HomePageDto> homePageDtos) {
        BizAccLoanDto bizAccLoanDto = new BizAccLoanDto();
        bizAccLoanDto.setManagerId((String) map.get("userId"));
        bizAccLoanDto.setBizType(CmisBizConstants.BIZ_TYPE_MATURE_TOPTEN_FOR_BUS);
        HomePageDto toptenDto = new HomePageDto();
        toptenDto.setTitle(CmisBizConstants.TATLE_MATURE_TOPTEN_FOR_BUS);
        toptenDto.setBizType(new String[]{CmisBizConstants.BIZ_TYPE_MATURE_TOPTEN_FOR_BUS});
        toptenDto.setTotal(BigDecimal.ZERO.intValue());
        toptenDto.setFlag(CmisBizConstants.PAGE_VIEW_FLAG_0);
        toptenDto.setData(null);
        toptenDto.setTotal(accLoanService.countAccLoanByDays(10));
        homePageDtos.add(toptenDto);
    }
	
	
	 /**
    * 根据commentSign状态码查询该用用户参与的流程
    *
    * */
    public ResultDto<List<ResultInstanceTodoDto>> queryInstance(String condition) {
        QueryModel queryModel = new QueryModel();
        queryModel.setCondition(condition);
        Map map = queryModel.getCondition();
        String userId = (String) map.get("userId");
        String commentSign = (String) map.get("commentSign");
        // 判断是待办todo还完结his(默认todo)
        String instanceType = (String) map.get("instanceType");
        if (StringUtils.isBlank(instanceType)) {
            instanceType = "todo";
        }
        // 查询所有流程节点
        ResultDto<List<ResultCommentDto>> instancsResultDto = workflowCoreClient.getCommentsByUserId(userId, CmisBizConstants.FLOW_QUERY_TYPE_2);
        //获取流程节点数据
        List<ResultCommentDto> resultCommentDtos = instancsResultDto.getData();
        // 获要所有查询的流程实例编号 （需要去重）
        Set<String> queryInstances = new HashSet<>();
        for (ResultCommentDto resultCommentDto : resultCommentDtos) {
            String resultCommentSign = resultCommentDto.getCommentSign();
            if (commentSign.equals(resultCommentSign)) {
                queryInstances.add(resultCommentDto.getInstanceId());
            }
        }
        List<ResultInstanceTodoDto>  resultInstanceTodoDto = new ArrayList<>();
        if(instanceType.equals("todo")){
            for(String instanceId :queryInstances){
                queryModel.setCondition("{\"instanceId\": \""+instanceId+"\"}");
                resultInstanceTodoDto.addAll(wfBenchClient.todo(queryModel).getData());
            }
        }else if(instanceType.equals("his")){
            for(String instanceId :queryInstances){
                queryModel.setCondition("{\"instanceId\": \""+instanceId+"\"}");
                resultInstanceTodoDto.addAll(wfBenchClient.his(queryModel).getData());
            }
        }
        ResultDto resultDto = new ResultDto();
        resultDto.setData(resultInstanceTodoDto);
        return resultDto;
    }
}