/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptAppInfoEffWar
 * @类描述: rpt_app_info_eff_war数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 21:11:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_app_info_eff_war")
public class RptAppInfoEffWar extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 预警时间 **/
	@Column(name = "ALT_TIME", unique = false, nullable = true, length = 20)
	private String altTime;
	
	/** 预警种类 **/
	@Column(name = "ALT_TYPE", unique = false, nullable = true, length = 5)
	private String altType;
	
	/** 预警子项 **/
	@Column(name = "ALT_SUB_TYPE", unique = false, nullable = true, length = 40)
	private String altSubType;
	
	/** 预警输出描述 **/
	@Column(name = "ALT_OUT_DESC", unique = false, nullable = true, length = 65535)
	private String altOutDesc;
	
	/** 指标风险等级 **/
	@Column(name = "ALT_RISK_LVL", unique = false, nullable = true, length = 5)
	private String altRiskLvl;
	
	/** 预警信号对企业影响 **/
	@Column(name = "ALT_RISK_EFFECT_CUS", unique = false, nullable = true, length = 65535)
	private String altRiskEffectCus;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param altTime
	 */
	public void setAltTime(String altTime) {
		this.altTime = altTime;
	}
	
    /**
     * @return altTime
     */
	public String getAltTime() {
		return this.altTime;
	}
	
	/**
	 * @param altType
	 */
	public void setAltType(String altType) {
		this.altType = altType;
	}
	
    /**
     * @return altType
     */
	public String getAltType() {
		return this.altType;
	}
	
	/**
	 * @param altSubType
	 */
	public void setAltSubType(String altSubType) {
		this.altSubType = altSubType;
	}
	
    /**
     * @return altSubType
     */
	public String getAltSubType() {
		return this.altSubType;
	}
	
	/**
	 * @param altOutDesc
	 */
	public void setAltOutDesc(String altOutDesc) {
		this.altOutDesc = altOutDesc;
	}
	
    /**
     * @return altOutDesc
     */
	public String getAltOutDesc() {
		return this.altOutDesc;
	}
	
	/**
	 * @param altRiskLvl
	 */
	public void setAltRiskLvl(String altRiskLvl) {
		this.altRiskLvl = altRiskLvl;
	}
	
    /**
     * @return altRiskLvl
     */
	public String getAltRiskLvl() {
		return this.altRiskLvl;
	}
	
	/**
	 * @param altRiskEffectCus
	 */
	public void setAltRiskEffectCus(String altRiskEffectCus) {
		this.altRiskEffectCus = altRiskEffectCus;
	}
	
    /**
     * @return altRiskEffectCus
     */
	public String getAltRiskEffectCus() {
		return this.altRiskEffectCus;
	}


}