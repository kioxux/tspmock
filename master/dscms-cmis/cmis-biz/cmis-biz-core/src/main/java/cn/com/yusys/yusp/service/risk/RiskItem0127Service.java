package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpAccpApp;
import cn.com.yusys.yusp.domain.IqpAccpAppPorderSub;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.RepayCapPlan;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author quwen
 * @version 1.0.0
 * @date 2021-10-14 21:30:00
 * @desc 银承合同申请（一般合同）票据明细校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0127Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0127Service.class);

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private IqpAccpAppPorderSubService iqpAccpAppPorderSubService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author quwen
     * @date 2021年10月14日01:06:37
     * @version 1.0.0
     * @desc    银承合同申请（一般合同）票据明细校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0127(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = (String)queryModel.getCondition().get("bizId");
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 通过申请流水号获取申请信息
        IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey(serno);
        if(Objects.isNull(iqpAccpApp)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007); //通过合同申请流水号未获取到对应的合同申请信息
            return riskResultDto;
        }
        //判断是否为一般合同
        String contType = iqpAccpApp.getContType();
        BigDecimal totalDrftAmt = BigDecimal.ZERO;
        if(CmisCommonConstants.STD_CONT_TYPE_1.equals(contType)){
            Map map = new HashMap();
            map.put("serno",serno);
            map.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
            List<IqpAccpAppPorderSub> iqpAccpAppPorderSubList = iqpAccpAppPorderSubService.selectPorderSubByParams(map);
            if(CollectionUtils.isEmpty(iqpAccpAppPorderSubList)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_012701); //银承合同申请未录入票据明细
                return riskResultDto;
            }
            for (IqpAccpAppPorderSub iqpAccpAppPorderSub : iqpAccpAppPorderSubList) {
                totalDrftAmt = totalDrftAmt.add(iqpAccpAppPorderSub.getDrftAmt());
            }
            log.info("票据明细总金额为【{}】",totalDrftAmt);
            log.info("银承合同申请金额为【{}】",iqpAccpApp.getContHighAvlAmt());
            if(totalDrftAmt.compareTo(iqpAccpApp.getContHighAvlAmt())!=0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_012702); //银承合同申请票据总金额不等于申请金额
                return riskResultDto;
            }
        }else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); //校验通过
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
