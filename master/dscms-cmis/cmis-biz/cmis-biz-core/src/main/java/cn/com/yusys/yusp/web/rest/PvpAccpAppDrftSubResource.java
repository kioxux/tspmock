/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.PvpAccpApp;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.req.Xdpj23ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.Xdpj23RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2PjxtClientService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PvpAccpAppDrftSub;
import cn.com.yusys.yusp.service.PvpAccpAppDrftSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAccpAppDrftSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-26 09:07:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pvpaccpappdrftsub")
public class PvpAccpAppDrftSubResource {

    private static final Logger log = LoggerFactory.getLogger(PvpLoanAppService.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");

    @Autowired
    private PvpAccpAppDrftSubService pvpAccpAppDrftSubService;

    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpAccpAppDrftSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpAccpAppDrftSub> list = pvpAccpAppDrftSubService.selectAll(queryModel);
        return new ResultDto<List<PvpAccpAppDrftSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpAccpAppDrftSub>> index(QueryModel queryModel) {
        List<PvpAccpAppDrftSub> list = pvpAccpAppDrftSubService.selectByModel(queryModel);
        return new ResultDto<List<PvpAccpAppDrftSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PvpAccpAppDrftSub> show(@PathVariable("pkId") String pkId) {
        PvpAccpAppDrftSub pvpAccpAppDrftSub = pvpAccpAppDrftSubService.selectByPrimaryKey(pkId);
        return new ResultDto<PvpAccpAppDrftSub>(pvpAccpAppDrftSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpAccpAppDrftSub> create(@RequestBody PvpAccpAppDrftSub pvpAccpAppDrftSub) throws URISyntaxException {
        pvpAccpAppDrftSubService.insert(pvpAccpAppDrftSub);
        return new ResultDto<PvpAccpAppDrftSub>(pvpAccpAppDrftSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpAccpAppDrftSub pvpAccpAppDrftSub) throws URISyntaxException {
        int result = pvpAccpAppDrftSubService.update(pvpAccpAppDrftSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pvpAccpAppDrftSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpAccpAppDrftSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:showList
     * @函数描述:查询票据明细
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showlist")
    protected ResultDto<List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List>> showList(@RequestBody Map params) {
        ResultDto<List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List>> resultDto = null;
        Xdpj23ReqDto xdpj23ReqDto = new Xdpj23ReqDto();
        xdpj23ReqDto.setsBatchNo((String) params.get("serno"));
        xdpj23ReqDto.setTurnPageShowNum("100");
        xdpj23ReqDto.setTurnPageBeginPos("1");
        LocalDateTime now = LocalDateTime.now();
        xdpj23ReqDto.setMac("");//mac地址
        xdpj23ReqDto.setIpaddr("");//ip地址
        xdpj23ReqDto.setServdt(tranDateFormtter.format(now));//交易日期
        xdpj23ReqDto.setServti(tranTimestampFormatter.format(now));//交易时间
        List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List> list = null;
        log.info("通过批次号【{}】，前往票据系统查询票据明细开始,请求报文为:【{}】", (String) params.get("serno"), xdpj23ReqDto.toString());
        ResultDto<Xdpj23RespDto> xdpj23RespDtoResultDto = dscms2PjxtClientService.xdpj23(xdpj23ReqDto);
        log.info("通过批次号【{}】，前往票据系统查询票据明细结束,响应报文为:【{}】", (String) params.get("serno"), xdpj23RespDtoResultDto.toString());
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdpj23RespDtoResultDto.getCode())
                && Objects.nonNull(xdpj23RespDtoResultDto.getData())) {
            Xdpj23RespDto data = xdpj23RespDtoResultDto.getData();
            list = data.getList();
//            if (CollectionUtils.nonEmpty(list)) {
//                collect = list.stream().map(e -> {
//                    PvpAccpAppDrftSub pvpAccpAppDrftSub = new PvpAccpAppDrftSub();
//                    pvpAccpAppDrftSub.setDrftAmt(e.getfBillAmount());
//                    pvpAccpAppDrftSub.setDrftNo(e.getBillno());
//                    pvpAccpAppDrftSub.setEndDate(e.getDueDt());
//                    pvpAccpAppDrftSub.setDrwr(e.getDrwrNm());
//                    pvpAccpAppDrftSub.setPyee(e.getPyeeNm());
//                    pvpAccpAppDrftSub.setAccptr(e.getAccptrNm());
//                    return pvpAccpAppDrftSub;
//                }).collect(Collectors.toList());
//            }
            resultDto = new ResultDto<List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List>>(list).code("200").message("查询成功！");
        } else {
            resultDto = new ResultDto<List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List>>(list).code("200").message("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querybyaccppvpseq")
    protected ResultDto<List<PvpAccpAppDrftSub>> queryByAccpPvpSeq(@RequestBody Map map) {
        String accpPvpSeq = (String) map.get("accpPvpSeq");
        List<PvpAccpAppDrftSub> result = pvpAccpAppDrftSubService.queryByAccpPvpSeq(accpPvpSeq);
        return new ResultDto<List<PvpAccpAppDrftSub>>(result);
    }
}
