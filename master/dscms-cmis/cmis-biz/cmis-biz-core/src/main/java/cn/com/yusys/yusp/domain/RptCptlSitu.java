/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSitu
 * @类描述: rpt_cptl_situ数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 09:34:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_cptl_situ")
public class RptCptlSitu extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 详细描述融资波动原因 **/
	@Column(name = "CPTL_FLUC_RESN", unique = false, nullable = true, length = 65535)
	private String cptlFlucResn;
	
	/** 名下融资情况其他说明 **/
	@Column(name = "CPTL_SITU_OTHER_MEMO", unique = false, nullable = true, length = 65535)
	private String cptlSituOtherMemo;
	
	/** 法人代表融资情况其他情况 **/
	@Column(name = "LEGAL_REPRE_CPTL_SITU_OTHER_MEMO", unique = false, nullable = true, length = 65535)
	private String legalRepreCptlSituOtherMemo;
	
	/** 集团对外担保总金额 **/
	@Column(name = "GRP_EXT_GUAR_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal grpExtGuarTotalAmt;
	
	/** 行政处罚 **/
	@Column(name = "ADMINISTRATIVE_PENALTY", unique = false, nullable = true, length = 10)
	private Integer administrativePenalty;
	
	/** 经营异常 **/
	@Column(name = "ABNORMAL_OPERATION", unique = false, nullable = true, length = 10)
	private Integer abnormalOperation;
	
	/** 环保处罚 **/
	@Column(name = "ENVIRONMENTAL_PUNISHMENT", unique = false, nullable = true, length = 10)
	private Integer environmentalPunishment;
	
	/** 欠税公告 **/
	@Column(name = "TAX_ARREARS_NOTICE", unique = false, nullable = true, length = 10)
	private Integer taxArrearsNotice;
	
	/** 税收违法 **/
	@Column(name = "ILLEGAL_TAXATION", unique = false, nullable = true, length = 10)
	private Integer illegalTaxation;
	
	/** 股权出质 **/
	@Column(name = "EQUITY_OUT_PLEDGE", unique = false, nullable = true, length = 10)
	private Integer equityOutPledge;
	
	/** 股权质押 **/
	@Column(name = "EQUITY_PLEDGE", unique = false, nullable = true, length = 10)
	private Integer equityPledge;
	
	/** 股权冻结 **/
	@Column(name = "EQUITY_FREEZE", unique = false, nullable = true, length = 10)
	private Integer equityFreeze;
	
	/** 动产抵押 **/
	@Column(name = "CHATTEL_MORTGAGE", unique = false, nullable = true, length = 10)
	private Integer chattelMortgage;
	
	/** 知识产权出质 **/
	@Column(name = "INTELLECTUAL_PROPERTY_PLEDGE", unique = false, nullable = true, length = 10)
	private Integer intellectualPropertyPledge;
	
	/** 失信被执行人 **/
	@Column(name = "DISHONEST_EXECUTED_PERSON", unique = false, nullable = true, length = 10)
	private Integer dishonestExecutedPerson;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String otherDesc;
	
	/** 行政处罚异常情况说明 **/
	@Column(name = "ADMINISTRATIVE_PENALTY_DESC", unique = false, nullable = true, length = 65535)
	private String administrativePenaltyDesc;
	
	/** 经营异常异常情况说明 **/
	@Column(name = "ABNORMAL_OPERATION_DESC", unique = false, nullable = true, length = 65535)
	private String abnormalOperationDesc;
	
	/** 环保处罚异常情况说明 **/
	@Column(name = "ENVIRONMENTAL_PUNISHMENT_DESC", unique = false, nullable = true, length = 65535)
	private String environmentalPunishmentDesc;
	
	/** 欠税公告异常情况说明 **/
	@Column(name = "TAX_ARREARS_NOTICE_DESC", unique = false, nullable = true, length = 65535)
	private String taxArrearsNoticeDesc;
	
	/** 税收违法异常情况说明 **/
	@Column(name = "ILLEGAL_TAXATION_DESC", unique = false, nullable = true, length = 65535)
	private String illegalTaxationDesc;
	
	/** 股权出质异常情况说明 **/
	@Column(name = "EQUITY_OUT_PLEDGE_DESC", unique = false, nullable = true, length = 65535)
	private String equityOutPledgeDesc;
	
	/** 股权质押异常情况说明 **/
	@Column(name = "EQUITY_PLEDGE_DESC", unique = false, nullable = true, length = 65535)
	private String equityPledgeDesc;
	
	/** 股权冻结异常情况说明 **/
	@Column(name = "EQUITY_FREEZE_DESC", unique = false, nullable = true, length = 65535)
	private String equityFreezeDesc;
	
	/** 动产抵押异常情况说明 **/
	@Column(name = "CHATTEL_MORTGAGE_DESC", unique = false, nullable = true, length = 65535)
	private String chattelMortgageDesc;
	
	/** 知识产权出质异常情况说明 **/
	@Column(name = "INTELLECTUAL_PROPERTY_PLEDGE_DESC", unique = false, nullable = true, length = 65535)
	private String intellectualPropertyPledgeDesc;
	
	/** 失信被执行人异常情况说明 **/
	@Column(name = "DISHONEST_EXECUTED_PERSON_DESC", unique = false, nullable = true, length = 65535)
	private String dishonestExecutedPersonDesc;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cptlFlucResn
	 */
	public void setCptlFlucResn(String cptlFlucResn) {
		this.cptlFlucResn = cptlFlucResn;
	}
	
    /**
     * @return cptlFlucResn
     */
	public String getCptlFlucResn() {
		return this.cptlFlucResn;
	}
	
	/**
	 * @param cptlSituOtherMemo
	 */
	public void setCptlSituOtherMemo(String cptlSituOtherMemo) {
		this.cptlSituOtherMemo = cptlSituOtherMemo;
	}
	
    /**
     * @return cptlSituOtherMemo
     */
	public String getCptlSituOtherMemo() {
		return this.cptlSituOtherMemo;
	}
	
	/**
	 * @param legalRepreCptlSituOtherMemo
	 */
	public void setLegalRepreCptlSituOtherMemo(String legalRepreCptlSituOtherMemo) {
		this.legalRepreCptlSituOtherMemo = legalRepreCptlSituOtherMemo;
	}
	
    /**
     * @return legalRepreCptlSituOtherMemo
     */
	public String getLegalRepreCptlSituOtherMemo() {
		return this.legalRepreCptlSituOtherMemo;
	}
	
	/**
	 * @param grpExtGuarTotalAmt
	 */
	public void setGrpExtGuarTotalAmt(java.math.BigDecimal grpExtGuarTotalAmt) {
		this.grpExtGuarTotalAmt = grpExtGuarTotalAmt;
	}
	
    /**
     * @return grpExtGuarTotalAmt
     */
	public java.math.BigDecimal getGrpExtGuarTotalAmt() {
		return this.grpExtGuarTotalAmt;
	}
	
	/**
	 * @param administrativePenalty
	 */
	public void setAdministrativePenalty(Integer administrativePenalty) {
		this.administrativePenalty = administrativePenalty;
	}
	
    /**
     * @return administrativePenalty
     */
	public Integer getAdministrativePenalty() {
		return this.administrativePenalty;
	}
	
	/**
	 * @param abnormalOperation
	 */
	public void setAbnormalOperation(Integer abnormalOperation) {
		this.abnormalOperation = abnormalOperation;
	}
	
    /**
     * @return abnormalOperation
     */
	public Integer getAbnormalOperation() {
		return this.abnormalOperation;
	}
	
	/**
	 * @param environmentalPunishment
	 */
	public void setEnvironmentalPunishment(Integer environmentalPunishment) {
		this.environmentalPunishment = environmentalPunishment;
	}
	
    /**
     * @return environmentalPunishment
     */
	public Integer getEnvironmentalPunishment() {
		return this.environmentalPunishment;
	}
	
	/**
	 * @param taxArrearsNotice
	 */
	public void setTaxArrearsNotice(Integer taxArrearsNotice) {
		this.taxArrearsNotice = taxArrearsNotice;
	}
	
    /**
     * @return taxArrearsNotice
     */
	public Integer getTaxArrearsNotice() {
		return this.taxArrearsNotice;
	}
	
	/**
	 * @param illegalTaxation
	 */
	public void setIllegalTaxation(Integer illegalTaxation) {
		this.illegalTaxation = illegalTaxation;
	}
	
    /**
     * @return illegalTaxation
     */
	public Integer getIllegalTaxation() {
		return this.illegalTaxation;
	}
	
	/**
	 * @param equityOutPledge
	 */
	public void setEquityOutPledge(Integer equityOutPledge) {
		this.equityOutPledge = equityOutPledge;
	}
	
    /**
     * @return equityOutPledge
     */
	public Integer getEquityOutPledge() {
		return this.equityOutPledge;
	}
	
	/**
	 * @param equityPledge
	 */
	public void setEquityPledge(Integer equityPledge) {
		this.equityPledge = equityPledge;
	}
	
    /**
     * @return equityPledge
     */
	public Integer getEquityPledge() {
		return this.equityPledge;
	}
	
	/**
	 * @param equityFreeze
	 */
	public void setEquityFreeze(Integer equityFreeze) {
		this.equityFreeze = equityFreeze;
	}
	
    /**
     * @return equityFreeze
     */
	public Integer getEquityFreeze() {
		return this.equityFreeze;
	}
	
	/**
	 * @param chattelMortgage
	 */
	public void setChattelMortgage(Integer chattelMortgage) {
		this.chattelMortgage = chattelMortgage;
	}
	
    /**
     * @return chattelMortgage
     */
	public Integer getChattelMortgage() {
		return this.chattelMortgage;
	}
	
	/**
	 * @param intellectualPropertyPledge
	 */
	public void setIntellectualPropertyPledge(Integer intellectualPropertyPledge) {
		this.intellectualPropertyPledge = intellectualPropertyPledge;
	}
	
    /**
     * @return intellectualPropertyPledge
     */
	public Integer getIntellectualPropertyPledge() {
		return this.intellectualPropertyPledge;
	}
	
	/**
	 * @param dishonestExecutedPerson
	 */
	public void setDishonestExecutedPerson(Integer dishonestExecutedPerson) {
		this.dishonestExecutedPerson = dishonestExecutedPerson;
	}
	
    /**
     * @return dishonestExecutedPerson
     */
	public Integer getDishonestExecutedPerson() {
		return this.dishonestExecutedPerson;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param administrativePenaltyDesc
	 */
	public void setAdministrativePenaltyDesc(String administrativePenaltyDesc) {
		this.administrativePenaltyDesc = administrativePenaltyDesc;
	}
	
    /**
     * @return administrativePenaltyDesc
     */
	public String getAdministrativePenaltyDesc() {
		return this.administrativePenaltyDesc;
	}
	
	/**
	 * @param abnormalOperationDesc
	 */
	public void setAbnormalOperationDesc(String abnormalOperationDesc) {
		this.abnormalOperationDesc = abnormalOperationDesc;
	}
	
    /**
     * @return abnormalOperationDesc
     */
	public String getAbnormalOperationDesc() {
		return this.abnormalOperationDesc;
	}
	
	/**
	 * @param environmentalPunishmentDesc
	 */
	public void setEnvironmentalPunishmentDesc(String environmentalPunishmentDesc) {
		this.environmentalPunishmentDesc = environmentalPunishmentDesc;
	}
	
    /**
     * @return environmentalPunishmentDesc
     */
	public String getEnvironmentalPunishmentDesc() {
		return this.environmentalPunishmentDesc;
	}
	
	/**
	 * @param taxArrearsNoticeDesc
	 */
	public void setTaxArrearsNoticeDesc(String taxArrearsNoticeDesc) {
		this.taxArrearsNoticeDesc = taxArrearsNoticeDesc;
	}
	
    /**
     * @return taxArrearsNoticeDesc
     */
	public String getTaxArrearsNoticeDesc() {
		return this.taxArrearsNoticeDesc;
	}
	
	/**
	 * @param illegalTaxationDesc
	 */
	public void setIllegalTaxationDesc(String illegalTaxationDesc) {
		this.illegalTaxationDesc = illegalTaxationDesc;
	}
	
    /**
     * @return illegalTaxationDesc
     */
	public String getIllegalTaxationDesc() {
		return this.illegalTaxationDesc;
	}
	
	/**
	 * @param equityOutPledgeDesc
	 */
	public void setEquityOutPledgeDesc(String equityOutPledgeDesc) {
		this.equityOutPledgeDesc = equityOutPledgeDesc;
	}
	
    /**
     * @return equityOutPledgeDesc
     */
	public String getEquityOutPledgeDesc() {
		return this.equityOutPledgeDesc;
	}
	
	/**
	 * @param equityPledgeDesc
	 */
	public void setEquityPledgeDesc(String equityPledgeDesc) {
		this.equityPledgeDesc = equityPledgeDesc;
	}
	
    /**
     * @return equityPledgeDesc
     */
	public String getEquityPledgeDesc() {
		return this.equityPledgeDesc;
	}
	
	/**
	 * @param equityFreezeDesc
	 */
	public void setEquityFreezeDesc(String equityFreezeDesc) {
		this.equityFreezeDesc = equityFreezeDesc;
	}
	
    /**
     * @return equityFreezeDesc
     */
	public String getEquityFreezeDesc() {
		return this.equityFreezeDesc;
	}
	
	/**
	 * @param chattelMortgageDesc
	 */
	public void setChattelMortgageDesc(String chattelMortgageDesc) {
		this.chattelMortgageDesc = chattelMortgageDesc;
	}
	
    /**
     * @return chattelMortgageDesc
     */
	public String getChattelMortgageDesc() {
		return this.chattelMortgageDesc;
	}
	
	/**
	 * @param intellectualPropertyPledgeDesc
	 */
	public void setIntellectualPropertyPledgeDesc(String intellectualPropertyPledgeDesc) {
		this.intellectualPropertyPledgeDesc = intellectualPropertyPledgeDesc;
	}
	
    /**
     * @return intellectualPropertyPledgeDesc
     */
	public String getIntellectualPropertyPledgeDesc() {
		return this.intellectualPropertyPledgeDesc;
	}
	
	/**
	 * @param dishonestExecutedPersonDesc
	 */
	public void setDishonestExecutedPersonDesc(String dishonestExecutedPersonDesc) {
		this.dishonestExecutedPersonDesc = dishonestExecutedPersonDesc;
	}
	
    /**
     * @return dishonestExecutedPersonDesc
     */
	public String getDishonestExecutedPersonDesc() {
		return this.dishonestExecutedPersonDesc;
	}


}