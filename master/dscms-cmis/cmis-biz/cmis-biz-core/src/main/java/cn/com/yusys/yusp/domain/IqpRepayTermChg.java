/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayTermChg
 * @类描述: iqp_repay_term_chg数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-01-18 19:24:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_repay_term_chg")
public class IqpRepayTermChg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	@Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanBalance;
	
	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = false, length = 10)
	private String endDate;
	
	/** 原还款方式 STD_ZB_REPAY_TYP **/
	@Column(name = "OLD_REPAY_MODE", unique = false, nullable = true, length = 5)
	private String oldRepayMode;
	
	/** 原停本付息期间 STD_ZB_PINT_TERM **/
	@Column(name = "OLD_STOP_PINT_TERM", unique = false, nullable = true, length = 5)
	private String oldStopPintTerm;
	
	/** 原还款间隔周期 STD_ZB_REPAY_TERM **/
	@Column(name = "OLD_REPAY_TERM", unique = false, nullable = true, length = 5)
	private String oldRepayTerm;
	
	/** 原还款间隔 STD_ZB_REPAY_SPACE **/
	@Column(name = "OLD_REPAY_SPACE", unique = false, nullable = true, length = 5)
	private String oldRepaySpace;
	
	/** 原还款日确定规则 STD_ZB_REPAY_RULE **/
	@Column(name = "OLD_REPAY_RULE", unique = false, nullable = true, length = 5)
	private String oldRepayRule;
	
	/** 原还款日类型 STD_ZB_REPAY_DT_TYPE **/
	@Column(name = "OLD_REPAY_DT_TYPE", unique = false, nullable = true, length = 5)
	private String oldRepayDtType;
	
	/** 原还款日 **/
	@Column(name = "OLD_REPAY_DATE", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal oldRepayDate;
	
	/** 还款间隔周期 STD_ZB_REPAY_TERM **/
	@Column(name = "REPAY_TERM", unique = false, nullable = true, length = 5)
	private String repayTerm;
	
	/** 还款间隔 STD_ZB_REPAY_SPACE **/
	@Column(name = "REPAY_SPACE", unique = false, nullable = true, length = 5)
	private String repaySpace;
	
	/** 还款日 **/
	@Column(name = "REPAY_DATE", unique = false, nullable = true, length = 5)
	private String repayDate;
	
	/** 调整原因 **/
	@Column(name = "CHANGE_RESN", unique = false, nullable = true, length = 250)
	private String changeResn;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return loanBalance
     */
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param oldRepayMode
	 */
	public void setOldRepayMode(String oldRepayMode) {
		this.oldRepayMode = oldRepayMode;
	}
	
    /**
     * @return oldRepayMode
     */
	public String getOldRepayMode() {
		return this.oldRepayMode;
	}
	
	/**
	 * @param oldStopPintTerm
	 */
	public void setOldStopPintTerm(String oldStopPintTerm) {
		this.oldStopPintTerm = oldStopPintTerm;
	}
	
    /**
     * @return oldStopPintTerm
     */
	public String getOldStopPintTerm() {
		return this.oldStopPintTerm;
	}
	
	/**
	 * @param oldRepayTerm
	 */
	public void setOldRepayTerm(String oldRepayTerm) {
		this.oldRepayTerm = oldRepayTerm;
	}
	
    /**
     * @return oldRepayTerm
     */
	public String getOldRepayTerm() {
		return this.oldRepayTerm;
	}
	
	/**
	 * @param oldRepaySpace
	 */
	public void setOldRepaySpace(String oldRepaySpace) {
		this.oldRepaySpace = oldRepaySpace;
	}
	
    /**
     * @return oldRepaySpace
     */
	public String getOldRepaySpace() {
		return this.oldRepaySpace;
	}
	
	/**
	 * @param oldRepayRule
	 */
	public void setOldRepayRule(String oldRepayRule) {
		this.oldRepayRule = oldRepayRule;
	}
	
    /**
     * @return oldRepayRule
     */
	public String getOldRepayRule() {
		return this.oldRepayRule;
	}
	
	/**
	 * @param oldRepayDtType
	 */
	public void setOldRepayDtType(String oldRepayDtType) {
		this.oldRepayDtType = oldRepayDtType;
	}
	
    /**
     * @return oldRepayDtType
     */
	public String getOldRepayDtType() {
		return this.oldRepayDtType;
	}
	
	/**
	 * @param oldRepayDate
	 */
	public void setOldRepayDate(java.math.BigDecimal oldRepayDate) {
		this.oldRepayDate = oldRepayDate;
	}
	
    /**
     * @return oldRepayDate
     */
	public java.math.BigDecimal getOldRepayDate() {
		return this.oldRepayDate;
	}
	
	/**
	 * @param repayTerm
	 */
	public void setRepayTerm(String repayTerm) {
		this.repayTerm = repayTerm;
	}
	
    /**
     * @return repayTerm
     */
	public String getRepayTerm() {
		return this.repayTerm;
	}
	
	/**
	 * @param repaySpace
	 */
	public void setRepaySpace(String repaySpace) {
		this.repaySpace = repaySpace;
	}
	
    /**
     * @return repaySpace
     */
	public String getRepaySpace() {
		return this.repaySpace;
	}
	
	/**
	 * @param repayDate
	 */
	public void setRepayDate(String repayDate) {
		this.repayDate = repayDate;
	}
	
    /**
     * @return repayDate
     */
	public String getRepayDate() {
		return this.repayDate;
	}
	
	/**
	 * @param changeResn
	 */
	public void setChangeResn(String changeResn) {
		this.changeResn = changeResn;
	}
	
    /**
     * @return changeResn
     */
	public String getChangeResn() {
		return this.changeResn;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}