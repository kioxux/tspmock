package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtAppSub;
import cn.com.yusys.yusp.domain.LmtReplyAcc;
import cn.com.yusys.yusp.domain.OtherRecordSpecialLoanApp;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx.QywxReqDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 特殊贷款用信备案申请流程业务处理类
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class QTSX05BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(QTSX05BizService.class);

    @Autowired
    private OtherRecordSpecialLoanAppService otherRecordSpecialLoanAppService;
    
    @Autowired
    private LmtReplyAccService lmtReplyAccService;
    
    @Autowired
    private LmtAppSubService lmtAppSubService;
    
    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    MessageCommonService messageCommonService;
    @Autowired
    private AdminSmUserService adminSmUserService;//用户信息模块
    
    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();
        String bizType = instanceInfo.getBizType();
        String currentOrgId = instanceInfo.getCurrentOrgId();
        log.info("后业务处理类型:" + currentOpType);
        //用信审核备案审批流程
        if (BizFlowConstant.QT010.equals(bizType) ){
        	// 流程路由参数
        	Map<String, Object> params = instanceInfo.getParam();
            WFBizParamDto param = new WFBizParamDto();
            
//            /**
//             * 机构类型:
//             * 1-异地支行（有分行）
//             * 2-异地支行（无分行）
//             * 3-异地分行
//             * 4-中心支行
//             * 5-综合支行
//             * 6-对公支行
//             */
//            String orgType = "6"; // 机构类型默认 6-对公支行
//            CommonService commonService = new CommonService();
//            AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(currentOrgId);
//            if(Objects.nonNull(adminSmOrgDto) && null != adminSmOrgDto.getOrgType()){
//                orgType = adminSmOrgDto.getOrgType();
//            }
//            params.put("orgType",orgType);
            
            //获取批复编号
            String replySerno = (String) params.get("replySerno");
            //  根据批复编号获取批复信息
            HashMap map = new HashMap();
            map.put("replySerno",replySerno);
            map.put("oprType", CommonConstance.OPR_TYPE_ADD);
            map.put("accStatus",CommonConstance.ACC_STATUS_01);
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(map);
            // 获取授信流水号
            String LmtSerno ="";
            if(null != lmtReplyAcc ){
                LmtSerno = lmtReplyAcc.getSerno();
                //敞口金额是否大于10000000
                String openTotalType = CommonConstance.STD_YES_OR_NO_N;
                BigDecimal OpenTotalLmtAmt  = lmtReplyAcc.getOpenTotalLmtAmt();
                if(OpenTotalLmtAmt.compareTo(new BigDecimal(10000000)) > 0){
                	openTotalType = CommonConstance.STD_YES_OR_NO_Y;
                }
                //判断是否仅有低风险
                String islow = CommonConstance.STD_YES_OR_NO_Y;
                List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(LmtSerno);
                for (LmtAppSub lmtAppSub : lmtAppSubList) {
                    // 低风险
                    if (!CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())) { 
                    	islow = CommonConstance.STD_YES_OR_NO_N;
                    	break;
                    }
                }
                
                // 设置路由
                params.put("openTotalType",openTotalType); // 敞口金额是否大于10000000
                params.put("islow",islow); // 是否低风险
                // 敞口金额合计 是否大于5000000
                String amtType = CommonConstance.STD_YES_OR_NO_N;
                if(OpenTotalLmtAmt.compareTo(new BigDecimal(5000000)) > 0){
                    amtType = CommonConstance.STD_YES_OR_NO_Y;
                }
                params.put("amtType",amtType); // 是否大于5000000
                param.setParam(params);
                param.setBizId(instanceInfo.getBizId());
                param.setInstanceId(instanceInfo.getInstanceId());
                workflowCoreClient.updateFlowParam(param);
            }
            QT002biz(instanceInfo);

        }
        
    }
    
    public void QT002biz(ResultInstanceDto instanceInfo) {
    	String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();
        OtherRecordSpecialLoanApp otherRecordSpecialLoanApp  = otherRecordSpecialLoanAppService.selectByPrimaryKey(serno);
        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
                otherRecordSpecialLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                // 结束后处理
                otherRecordSpecialLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_997);
                //  消息提醒
                String mgrTel = "";
                String managerId=otherRecordSpecialLoanApp.getManagerId();
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = "MSG_CF_M_0016";//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", otherRecordSpecialLoanApp.getCusName());
                        paramMap.put("prdName", "用信审核备案申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }

                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                otherRecordSpecialLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                //  消息提醒
                String mgrTel = "";
                String managerId=otherRecordSpecialLoanApp.getManagerId();
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = "MSG_CF_M_0016";//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", otherRecordSpecialLoanApp.getCusName());
                        paramMap.put("prdName", "用信审核备案申请");
                        paramMap.put("result", "打回");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }

            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                otherRecordSpecialLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                //  消息提醒
                String mgrTel = "";
                String managerId=otherRecordSpecialLoanApp.getManagerId();
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = "MSG_CF_M_0016";//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", otherRecordSpecialLoanApp.getCusName());
                        paramMap.put("prdName", "用信审核备案申请");
                        paramMap.put("result", "打回");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                otherRecordSpecialLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.QTSX05.equals(flowCode);
    }

}
