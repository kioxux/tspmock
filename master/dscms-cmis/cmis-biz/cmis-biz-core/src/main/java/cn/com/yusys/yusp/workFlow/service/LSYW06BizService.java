package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CtrLoanContService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @param
 * @return
 * @author shenli
 * @date 2021-6-29 21:7:27
 * @version 1.0.0
 * @desc 零售生成打印合同签订审核流程
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class LSYW06BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(LSYW06BizService.class);
    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {

        String currentOpType = resultInstanceDto.getCurrentOpType();
        log.info("后业务处理类型:" + currentOpType);
        String bizType = resultInstanceDto.getBizType();
        log.info("进入业务类型【{}】、业务处理类型【{}】流程处理逻辑开始！", bizType, currentOpType);
        if("LS008".equals(bizType)){

            handlerLS008(resultInstanceDto);

        }
        log.info("进入业务类型【{}】流程处理逻辑结束！", bizType);
    }


    //零售生成打印合同签订
    private void handlerLS008(ResultInstanceDto resultInstanceDto){
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String extSerno = resultInstanceDto.getBizId();
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                updateContStatus(extSerno);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_997);
                ctrLoanContService.changelmt(extSerno);
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                log.info("发送异常消息开始:" + resultInstanceDto);
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
                log.info("发送异常消息开始结束");
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.LSYW06.equals(flowCode);
    }

    /**
     * @创建人 shenli
     * @创建时间 2021-5-10 15:07:49
     * @注释 审批状态更新
     */
    public void updateStatus(String serno,String state){
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(serno);
        ctrLoanCont.setApproveStatus(state);
        ctrLoanContService.updateSelective(ctrLoanCont);
    }
    /**
     * @创建人 shenli
     * @创建时间 2021-5-10 15:08:32
     * @注释 更新合同状态为“未生效”
     */
    public void updateContStatus(String extSerno) {
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(extSerno);
        if("000".equals(ctrLoanCont.getApproveStatus())){
            ctrLoanCont.setContStatus("100");
            ctrLoanContService.updateSelective(ctrLoanCont);
        }
    }

}
