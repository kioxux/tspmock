package cn.com.yusys.yusp.service.client.bsp.core.ln3100;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.server.xddh0005.req.Xddh0005DataReqDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：贷款信息查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Ln3100Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw01Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * 业务逻辑处理方法：贷款信息查询
     *
     * @param ln3100ReqDto
     * @return
     */
    @Transactional
    public Ln3100RespDto ln3100(Ln3100ReqDto ln3100ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3100.key, EsbEnum.TRADE_CODE_LN3100.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3100.key, EsbEnum.TRADE_CODE_LN3100.value, JSON.toJSONString(ln3100ReqDto));
        ResultDto<Ln3100RespDto> ln3100ResultDto = dscms2CoreLnClientService.ln3100(ln3100ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3100.key, EsbEnum.TRADE_CODE_LN3100.value, JSON.toJSONString(ln3100ResultDto));
        String ln3100Code = Optional.ofNullable(ln3100ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3100Meesage = Optional.ofNullable(ln3100ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3100RespDto ln3100RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3100ResultDto.getCode())) {
            //  获取相关的值并解析
            ln3100RespDto = ln3100ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, ln3100Code, ln3100Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3100.key, EsbEnum.TRADE_CODE_LN3100.value);
        return ln3100RespDto;
    }

    /**
     * 根据 [请求Data：提前还款 ]组装[请求Dto：根据借据号或者贷款账号查询单笔借据的详细信息]
     *
     * @param xddh0005DataReqDto
     * @return
     */
    public Ln3100ReqDto buildLn3100ReqDto(Xddh0005DataReqDto xddh0005DataReqDto) {
        Ln3100ReqDto ln3100ReqDto = GenericBuilder.of(Ln3100ReqDto::new)
                .with(Ln3100ReqDto::setDkzhangh, StringUtils.EMPTY)//贷款账号
                .with(Ln3100ReqDto::setDkjiejuh, xddh0005DataReqDto.getBillNo())// 贷款借据号
                .with(Ln3100ReqDto::setQishibis, null) // 起始笔数
                .with(Ln3100ReqDto::setChxunbis, null) // 查询笔数
                .build();
        return ln3100ReqDto;
    }
}
