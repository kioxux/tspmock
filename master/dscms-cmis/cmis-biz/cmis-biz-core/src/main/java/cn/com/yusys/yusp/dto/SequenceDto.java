package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.IqpLoanAppAssist;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanApp
 * @类描述: iqp_loan_app数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-10 14:42:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class SequenceDto implements Serializable{
    private static final long serialVersionUID = 1L;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    //流水号
    private String serno;
    //序列号
    private String code;

}
