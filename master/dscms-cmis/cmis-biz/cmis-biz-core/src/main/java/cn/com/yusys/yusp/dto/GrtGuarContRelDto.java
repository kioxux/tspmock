package cn.com.yusys.yusp.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarContRel
 * @类描述: grt_guar_cont_rel数据实体类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-08 16:58:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GrtGuarContRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 担保合同编号 **/
	private String guarContNo;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 抵质押合同编号 **/
	private String mortContNo;
	
	/** 是否主押品 STD_ZB_YES_NO **/
	private String isMainGuar;

	/** 权利价值 **/
	private java.math.BigDecimal certiAmt;

	/** 备注 **/
	private String remark;
	
	/** 状态 STD_ZB_STATUS **/
	private String status;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;

	/**新增dto字段*/
	/**担保分类代码*/
	private String guarTypeCd;

	/**押品名称**/
	private String pldimnMemo;

	/**押品顺位标示**/
	private String pldOrder;

	/** 押品评估价值 **/
	private java.math.BigDecimal evalAmt;

	/** 押品认定价值 **/
	private java.math.BigDecimal confirmAmt;

	/** 押品所有人编号 **/
	private String guarCusId;

	/** 押品所有人名称 **/
	private String guarCusName;

	/**抵质押类别**/
	private String grtFlag;

	/** 创建时间 **/
	private Date createTime;

	/** 修改时间 **/
	private Date updateTime;

	/** 抵质押率 **/
	private java.math.BigDecimal mortagageRate;

	public String getGrtFlag() {
		return grtFlag;
	}

	public void setGrtFlag(String grtFlag) {
		this.grtFlag = grtFlag;
	}

	public String getGuarTypeCd() {
		return guarTypeCd;
	}

	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}

	public String getPldimnMemo() {
		return pldimnMemo;
	}

	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}

	public String getPldOrder() {
		return pldOrder;
	}

	public void setPldOrder(String pldOrder) {
		this.pldOrder = pldOrder;
	}

	public BigDecimal getEvalAmt() {
		return evalAmt;
	}

	public void setEvalAmt(BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}

	public BigDecimal getConfirmAmt() {
		return confirmAmt;
	}

	public void setConfirmAmt(BigDecimal confirmAmt) {
		this.confirmAmt = confirmAmt;
	}

	public String getGuarCusId() {
		return guarCusId;
	}

	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}

	public String getGuarCusName() {
		return guarCusName;
	}

	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}
	
    /**
     * @return GuarContNo
     */	
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param mortContNo
	 */
	public void setMortContNo(String mortContNo) {
		this.mortContNo = mortContNo == null ? null : mortContNo.trim();
	}
	
    /**
     * @return MortContNo
     */	
	public String getMortContNo() {
		return this.mortContNo;
	}
	
	/**
	 * @param isMainGuar
	 */
	public void setIsMainGuar(String isMainGuar) {
		this.isMainGuar = isMainGuar == null ? null : isMainGuar.trim();
	}
	
    /**
     * @return IsMainGuar
     */	
	public String getIsMainGuar() {
		return this.isMainGuar;
	}

	public BigDecimal getCertiAmt() {
		return certiAmt;
	}

	public void setCertiAmt(BigDecimal certiAmt) {
		this.certiAmt = certiAmt;
	}

	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}

	public BigDecimal getMortagageRate() {
		return mortagageRate;
	}

	public void setMortagageRate(BigDecimal mortagageRate) {
		this.mortagageRate = mortagageRate;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}