/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRiskChkDebtCfrm
 * @类描述: iqp_risk_chk_debt_cfrm数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-25 17:33:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_risk_chk_debt_cfrm")
public class IqpRiskChkDebtCfrm extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 32)
	private String iqpSerno;
	
	/** APPT_CODE **/
	@Column(name = "APPT_CODE", unique = false, nullable = false, length = 32)
	private String apptCode;
	
	/** 客户编号 **/
	@Column(name = "CUST_ID", unique = false, nullable = true, length = 20)
	private String custId;
	
	/** 场景 **/
	@Column(name = "SECEN", unique = false, nullable = true, length = 5)
	private String secen;
	
	/** 有负债金额汇总 **/
	@Column(name = "DEBT_AMT_SUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtAmtSum;
	
	/** 有负债金额汇总(折算后) **/
	@Column(name = "DEBT_AMT_SUMZ", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtAmtSumz;
	
	/** 已有负债月还款额汇总 **/
	@Column(name = "M_DEBT_AMT_SUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mDebtAmtSum;
	
	/** 已有负债月还款额汇总(折算后) **/
	@Column(name = "M_DEBT_AMT_SUMZ", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mDebtAmtSumz;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode;
	}
	
    /**
     * @return apptCode
     */
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param secen
	 */
	public void setSecen(String secen) {
		this.secen = secen;
	}
	
    /**
     * @return secen
     */
	public String getSecen() {
		return this.secen;
	}
	
	/**
	 * @param debtAmtSum
	 */
	public void setDebtAmtSum(java.math.BigDecimal debtAmtSum) {
		this.debtAmtSum = debtAmtSum;
	}
	
    /**
     * @return debtAmtSum
     */
	public java.math.BigDecimal getDebtAmtSum() {
		return this.debtAmtSum;
	}
	
	/**
	 * @param debtAmtSumz
	 */
	public void setDebtAmtSumz(java.math.BigDecimal debtAmtSumz) {
		this.debtAmtSumz = debtAmtSumz;
	}
	
    /**
     * @return debtAmtSumz
     */
	public java.math.BigDecimal getDebtAmtSumz() {
		return this.debtAmtSumz;
	}
	
	/**
	 * @param mDebtAmtSum
	 */
	public void setMDebtAmtSum(java.math.BigDecimal mDebtAmtSum) {
		this.mDebtAmtSum = mDebtAmtSum;
	}
	
    /**
     * @return mDebtAmtSum
     */
	public java.math.BigDecimal getMDebtAmtSum() {
		return this.mDebtAmtSum;
	}
	
	/**
	 * @param mDebtAmtSumz
	 */
	public void setMDebtAmtSumz(java.math.BigDecimal mDebtAmtSumz) {
		this.mDebtAmtSumz = mDebtAmtSumz;
	}
	
    /**
     * @return mDebtAmtSumz
     */
	public java.math.BigDecimal getMDebtAmtSumz() {
		return this.mDebtAmtSumz;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}