package cn.com.yusys.yusp.service.client.cus.cmiscus0007;


import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0007.req.CmisCus0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CmisCus0007RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CusIndivDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：查询个人客户基本信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CmisCus0007Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0007Service.class);

    // 1）注入：封装的接口类:客户管理模块
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 业务逻辑处理方法：查询个人客户基本信息
     *
     * @param cmisCus0007ReqDto
     * @return
     */
    @Transactional
    public CmisCus0007RespDto cmisCus0007(CmisCus0007ReqDto cmisCus0007ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0007ReqDto));
        ResultDto<CmisCus0007RespDto> cmisCus0007ResultDto = cmisCusClientService.cmiscus0007(cmisCus0007ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0007ResultDto));

        String cmisCus0007Code = Optional.ofNullable(cmisCus0007ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisCus0007Meesage = Optional.ofNullable(cmisCus0007ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisCus0007RespDto cmisCus0007RespDto = null;
        List<CusIndivDto> cusIndivList = new ArrayList<>();
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisCus0007ResultDto.getCode())) {
            //  获取相关的值并解析
            cmisCus0007RespDto = cmisCus0007ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(cmisCus0007Code, cmisCus0007Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value);
        return cmisCus0007RespDto;
    }
}
