package cn.com.yusys.yusp.web.server.xdls0006;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdls0006.resp.List;
import cn.com.yusys.yusp.service.server.xdls0006.Xdls0006Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdls0006.req.Xdls0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0006.resp.Xdls0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 接口处理类:白领贷额度查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDLS0006:白领贷额度查询")
@RestController
@RequestMapping("/api/bizls4bsp")
public class BizXdls0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdls0006Resource.class);

    @Autowired
    private Xdls0006Service xdls0006Service;
    /**
     * 交易码：xdls0006
     * 交易描述：白领贷额度查询
     *
     * @param xdls0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("白领贷额度查询")
    @PostMapping("/xdls0006")
    protected @ResponseBody
    ResultDto<Xdls0006DataRespDto> xdls0006(@Validated @RequestBody Xdls0006DataReqDto xdls0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0006.key, DscmsEnum.TRADE_CODE_XDLS0006.value, JSON.toJSONString(xdls0006DataReqDto));
        Xdls0006DataRespDto xdls0006DataRespDto = new Xdls0006DataRespDto();// 响应Dto:白领贷额度查询
        ResultDto<Xdls0006DataRespDto> xdls0006DataResultDto = new ResultDto<>();
        String certNo = xdls0006DataReqDto.getCertNo();//证件号
        String biz_type = xdls0006DataReqDto.getPrdType();//产品类型
        if(certNo==null||"".equals(certNo)){
            xdls0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdls0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
            return xdls0006DataResultDto;
        }
        try {
            // 从xdls0006DataReqDto获取业务值进行业务逻辑处理
            xdls0006DataRespDto = xdls0006Service.xdls0006(xdls0006DataReqDto);
            // 封装xdls0006DataResultDto中正确的返回码和返回信息
            xdls0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdls0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0006.key, DscmsEnum.TRADE_CODE_XDLS0006.value, e.getMessage());
            // 封装xdls0006DataResultDto中异常返回码和返回信息
            xdls0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdls0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdls0006DataRespDto到xdls0006DataResultDto中
        xdls0006DataResultDto.setData(xdls0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0006.key, DscmsEnum.TRADE_CODE_XDLS0006.value, JSON.toJSONString(xdls0006DataResultDto));
        return xdls0006DataResultDto;
    }
}
