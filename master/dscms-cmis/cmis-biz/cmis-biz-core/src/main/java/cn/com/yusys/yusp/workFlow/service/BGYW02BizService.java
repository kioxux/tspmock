package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.core.ln3057.resp.Ln3057RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3074.req.Ln3074ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3074.resp.Ln3074RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.req.CmisCus0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.resp.CmisCus0001RespDto;
import cn.com.yusys.yusp.enums.out.common.DicTranEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/2119:56
 * @desc 展期申请
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BGYW02BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(BGYW02BizService.class);//定义log

    @Autowired
    private IqpContExtService iqpContExtService;

    @Autowired
    private IqpContExtBillService iqpContExtBillService;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private OtherItemAppService otherItemAppService;

    @Autowired
    private OtherCnyRateAppSubService otherCnyRateAppSubService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    MessageCommonService messageCommonService;
    @Autowired
    private AdminSmUserService adminSmUserService;//用户信息模块

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private OtherCnyRateApprSubService otherCnyRateApprSubService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private LmtGrpReplyAccService lmtGrpReplyAccService;
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        //展期申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_BG005.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG006.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG007.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SGH05.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_DHH05.equals(bizType)) {
            iqpContExtApply(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else if (CmisFlowConstants.FLOW_TYPE_TYPE_BG026.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG027.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG028.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SGH06.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_DHH06.equals(bizType)) {
            iqpContExtApplySign(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }



    private void iqpContExtApply(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型" + currentOpType);
        try {
            IqpContExt iqpContExt = iqpContExtService.selectByPrimaryKey(serno);
            // 路由条件处理
            // 获取批复编号
            String replySerno = iqpContExt.getReplyNo();
            if(null != replySerno && !"".equals(replySerno)){
                //  根据批复编号获取批复信息
                HashMap map = new HashMap();
                map.put("replySerno",replySerno);
                map.put("oprType", CommonConstance.OPR_TYPE_ADD);
                LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(map);
                // 获取授信流水号
                String LmtSerno ="";
                if(null != lmtReplyAcc ){
                    LmtSerno = lmtReplyAcc.getSerno();
                }
                if(!"".equals(LmtSerno) && null != LmtSerno){
                    // 加载路由条件
                    put2VarParam(resultInstanceDto, LmtSerno);
                }
            }

            // 企业类型处理
            WFBizParamDto param = new WFBizParamDto();
            Map<String, Object> params = new HashMap<>();
            param.setBizId(resultInstanceDto.getBizId());
            param.setInstanceId(resultInstanceDto.getInstanceId());
            String isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_0;
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(iqpContExt.getCusId()).getData();
            if (cusCorpDto != null && CmisCommonConstants.STD_ZB_YES_NO_1.equals(cusCorpDto.getIsSmconCus())) {
                isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_1;
            }
            params.put("isSmconCus",isSmconCus);
            // 是否对公客户，判断协办客户经理节点
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(iqpContExt.getCusId());
            log.info("获取客户基本信息数据:{}", cusBaseClientDto.toString());
            if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
                params.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_1);
            } else {
                params.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_0);
            }
            //  获取产品
            String prdId = "";
            String prdName = "";
            QueryModel modelDel = new QueryModel();
            modelDel.addCondition("iqpSerno",serno);
            List<IqpContExtBill> iqpContExtBillListmodelDel = iqpContExtBillService.selectByModel(modelDel);//查询展期借据
            for (int i = 0; i < iqpContExtBillListmodelDel.size(); i++) {
                IqpContExtBill contExtBill = iqpContExtBillListmodelDel.get(i);
                prdId = contExtBill.getPrdId();
                prdName = contExtBill.getPrdName();
            }
            params.put("prdId", prdId);
            params.put("prdName", prdName);
            // 获取分成比例协办客户经理编号
            String managerIdXB = null;
            CusMgrDividePercDto cusMgrDividePercDto = iCusClientService.selectXBManagerId(iqpContExt.getCusId()).getData();
            log.info("根据客户号【{}】查询客户协办客户经理，返回为{}", iqpContExt.getCusId(), cusMgrDividePercDto);
            if (cusMgrDividePercDto != null && !StringUtils.isBlank(cusMgrDividePercDto.getManagerId())) {
                managerIdXB = cusMgrDividePercDto.getManagerId();
            }
            params.put("managerId", managerIdXB);
            if (!OpType.REFUSE.equals(currentOpType)) { // 否决除外
                params.put("nextSubmitNodeId", resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
            }

            params.put("commitDeptTypeXDGLB","0");
            params.put("isUpAppr","0");
            params.put("upApprType","0");
            params.put("isDownAppr","0");
            params.put("isDAELmtAppr","0");

            // 会签模式处理
            String apprMode = "";
            try {
                // 获取客户批复
                LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(iqpContExt.getCusId());
                if(Objects.nonNull(lmtReplyAcc)){
                    if(null != lmtReplyAcc.getApprMode() && !"".equals(lmtReplyAcc.getApprMode())){
                        apprMode = lmtReplyAcc.getApprMode();
                    }
                }
            } catch (Exception e) {
                log.info("未获取到授信批复会签模式信息");
            }

            // 集团会签模式处理
            try {
                // 根据客户号获取集团客户号
                String grpNo = "";
                /**
                 * 获取集团客户号 cmiscus0001
                 */
                CmisCus0001ReqDto cmisCus0001ReqDto = new CmisCus0001ReqDto();
                cmisCus0001ReqDto.setCusId(iqpContExt.getCusId());
                ResultDto<CmisCus0001RespDto> cmisCus0001RespDtoResultDto = cmisCusClientService.cmiscus0001(cmisCus0001ReqDto);
                if (cmisCus0001RespDtoResultDto != null
                        && SuccessEnum.SUCCESS.key.equals(cmisCus0001RespDtoResultDto.getData().getErrorCode())
                        && !"40012".equals(cmisCus0001RespDtoResultDto.getData().getErrorCode())){
                    CmisCus0001RespDto data = cmisCus0001RespDtoResultDto.getData();
                    if(Objects.nonNull(data)){
                        grpNo = data.getGrpNo();//集团客户号
                    }
                }
                if(!"".equals(grpNo) && null != grpNo){
                    // 获取集团批复
                    LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccService.getLastLmtReplyAcc(grpNo);
                    if(Objects.nonNull(lmtGrpReplyAcc)){
                        if(null != lmtGrpReplyAcc.getApprMode() && !"".equals(lmtGrpReplyAcc.getApprMode())){
                            apprMode = lmtGrpReplyAcc.getApprMode();
                        }
                    }
                }
            } catch (Exception e) {
                log.info("未获取到集团授信批复会签模式信息");
            }

            if("".equals(apprMode) || null == apprMode){
                apprMode = "05";
            }
            params.put("apprMode",apprMode);
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);

            if (OpType.STRAT.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                iqpContExt.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                iqpContExtService.update(iqpContExt);
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,serno);
                // iqpContExtService.createImageSpplInfo(serno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId());
                log.info("合作方协议申请【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                iqpContExt.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                iqpContExtService.update(iqpContExt);
                log.info("合作方协议申请【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                iqpContExt.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                iqpContExt.setContApproveStatus(CmisBizConstants.APPLY_STATE_TODO); // 同时将协议审批状态更新为待发起
                iqpContExtService.update(iqpContExt);
                // 针对流程到办结节点，进行以下处理
                iqpContExtService.handleAfterEnd(iqpContExt,currentUserId,currentOrgId,CmisBizConstants.APPLY_STATE_PASS,resultInstanceDto);

                QueryModel model = new QueryModel();
                model.addCondition("iqpSerno",serno);
                List<IqpContExtBill> iqpContExtBillList = iqpContExtBillService.selectByModel(model);//查询展期借据
                if(iqpContExtBillList.size() > 0) {
                	for(IqpContExtBill iqpContExtBill : iqpContExtBillList){
                		AccLoan accLoan = accLoanService.selectByBillNo(iqpContExtBill.getBillNo());
                        QueryModel model1 = new QueryModel();
                        model1.addCondition("serno",serno);
                        model1.addCondition("oprType","01");
                        model1.addCondition("approveStatusS","");
                        List<OtherAppDto> list = otherItemAppService.selectOtherItemByIqpSerno(model1);//查询其他事项
                        if(list.size() > 0) {
                            for (OtherAppDto otherAppDto : list) {
                                String otherAppType = otherAppDto.getOtherAppType();//其他事项类型
                                String otherSerno = otherAppDto.getSerno();//其他事项业务流水
                                String approveStatus = otherAppDto.getApproveStatus();//其他事项审批状态
                                //如果存在 审批通过的 人民币利率申请，修改展期利率
                            /*    if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_01.equals(otherAppType) && "997".equals(approveStatus)) {
                                    QueryModel model2 = new QueryModel();
                                    model2.addCondition("serno", otherSerno);
                                    // 展期利率定价  2	展期利率定价
                                    model2.addCondition("rateAppType", "2");
                                    List<OtherCnyRateAppSub> list2 = otherCnyRateAppSubService.selectByModel(model2);//查询人民币利率申请
                                    if (list2.size() > 0) {
                                        OtherCnyRateAppSub otherCnyRateAppSub = list2.get(0);//只获取一笔利率申请的数据
                                        iqpContExtBill.setExtRate(otherCnyRateAppSub.getBankAppRate());
                                        iqpContExtBillService.updateSelective(iqpContExtBill);
                                    }
                                }*/
                                 //  以审批利率为准
                                // 查询定价审批信息 other_cny_rate_appr_sub  otherCnyRateApprSub
                                if (CmisBizConstants.STD_LMT_OTHER_APP_TYPE_01.equals(otherAppType) && "997".equals(approveStatus)) {
                                    QueryModel apprSubModel = new QueryModel();
                                    apprSubModel.addCondition("serno",otherSerno);
                                    apprSubModel.addCondition("oprType","01");
                                    List<OtherCnyRateApprSub> otherCnyRateApprSubList  = otherCnyRateApprSubService.selectByModel(apprSubModel);
                                    for (int j = 0; j < otherCnyRateApprSubList.size() ; j++) {
                                        OtherCnyRateApprSub otherCnyRateApprSub = otherCnyRateApprSubList.get(j);
                                        if(!"".equals(otherCnyRateApprSub.getApprLoanRate()) && null != otherCnyRateApprSub.getApprLoanRate()){
                                            iqpContExtBill.setExtRate(otherCnyRateApprSub.getApprLoanRate());
                                            iqpContExtBillService.updateSelective(iqpContExtBill);
                                            break;
                                        }
                                    }
                                }
                            }
                        }else{
                            iqpContExtBill.setExtRate(accLoan.getExecRateYear());
                            iqpContExtBillService.updateSelective(iqpContExtBill);
                        }
                	}
                    
                }

                //多笔展期借据利率不同时生成多笔展期协议cjd
                QueryModel model1 = new QueryModel();
                model.addCondition("iqpSerno",serno);
                List<IqpContExtBill> iqpContExtBillList1 = iqpContExtBillService.selectByModel(model);//查询展期借据
                if(iqpContExtBillList1.size() > 1) {
                    //组装利率用于后面判断
                    List<BigDecimal> rateList = new ArrayList<BigDecimal>();
                    for(IqpContExtBill iqpContExtBill : iqpContExtBillList1){
                        rateList.add(iqpContExtBill.getExtRate());
                    }
                    //生成协议开始
                    for(int i =1 ; i<iqpContExtBillList1.size(); i++){
                        int ifEqual = iqpContExtBillList1.get(i).getExtRate().compareTo(iqpContExtBillList1.get(0).getExtRate());//是否与第一个利率不同
                        int firstPos = find_first_elem(rateList,iqpContExtBillList1.get(i).getExtRate());//第一次出现的位置
                        if(ifEqual != 0 && firstPos == i){//与第一笔利率不同且第一次出现就生成新的展期协议
                            IqpContExt iqpContExt1 =  (IqpContExt)SerializationUtils.clone(iqpContExt);
                            iqpContExt1.setIqpSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP, new HashMap<>()));
                            iqpContExtService.insert(iqpContExt1);
                            IqpContExtBill iqpContExtBill =  iqpContExtBillList1.get(i);
                            iqpContExtBillService.deleteByPrimaryKey(iqpContExtBill.getSerno());
                            iqpContExtBill.setIqpSerno(iqpContExt1.getIqpSerno());
                            iqpContExtBillService.insert(iqpContExtBill);
                        }
                    }
                }

                // 资料未全生成影像补扫任务
//                if(!CmisFlowConstants.FLOW_TYPE_TYPE_BG007.equals(resultInstanceDto.getBizType())){
//                    log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,serno);
//                    iqpContExtService.createImageSpplInfo(serno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),"展期申请",CmisCommonConstants.STD_SPPL_TYPE_07);
//                }
                log.info("合作方协议申请【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);

                //  消息提醒
                String mgrTel = "";
                String managerId=iqpContExt.getManagerId();
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = "MSG_CF_M_0016";//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpContExt.getCusName());
                        paramMap.put("prdName", "展期申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
                iqpContExt.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                iqpContExtService.update(iqpContExt);
                //  消息提醒
                String mgrTel = "";
                String managerId=iqpContExt.getManagerId();
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = "MSG_CF_M_0016";//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpContExt.getCusName());
                        paramMap.put("prdName", "展期申请");
                        paramMap.put("result", "打回");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }

            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpContExt.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    iqpContExtService.update(iqpContExt);
                    //  消息提醒
                    String mgrTel = "";
                    String managerId=iqpContExt.getManagerId();
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = "MSG_CF_M_0016";//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", iqpContExt.getCusName());
                            paramMap.put("prdName", "展期申请");
                            paramMap.put("result", "打回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                    log.info("合作方协议申请【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                }
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                iqpContExt.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                log.info("合作方协议申请流程申请启用授信申报审批流程申请启用【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //流程打回至发起人，则更新申请表数据为打回状态，发送首页提醒
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    extractedIqpContExt(resultInstanceDto, currentUserId, currentOrgId, iqpContExt);
                }
                iqpContExtService.update(iqpContExt);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                iqpContExt.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                iqpContExtService.update(iqpContExt);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                // 否决改变标志 审批中 111-> 审批不通过 998
                iqpContExt.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                iqpContExtService.update(iqpContExt);
                iqpContExtService.handleAfterEnd(iqpContExt,currentUserId,currentOrgId,CmisBizConstants.APPLY_STATE_REFUSE,resultInstanceDto);
            } else {
                log.info("合作方协议申请【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("合作方协议申请后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        try{
            WFBizParamDto param = new WFBizParamDto();
            param.setBizId(resultInstanceDto.getBizId());
            param.setInstanceId(resultInstanceDto.getInstanceId());
            Map<String, Object> params = new HashMap<>();
            params = lmtAppService.getRouterMapResult(serno);
            // 会签模式判断 apprMode
            String apprMode = lmtAppService.getApprMode(serno);
            if(null == apprMode || "".equals(apprMode)){
                apprMode = "01";
            }
            params.put("apprMode",apprMode);
            params.remove("approveStatus");
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);
        }catch(Exception e){
            log.info("未找到原授信流程");
        }

    }
    /**
     * @作者:
     * @方法名称: extractedIntbankApp
     * @方法描述:  申请执行打回或拿会操作
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/8/3 14:48
     * @param resultInstanceDto:
     * @param currentUserId:
     * @param currentOrgId:
     * @param iqpContExt:
     * @return: void
     * @算法描述: 无
     */
    private void extractedIqpContExt(ResultInstanceDto resultInstanceDto, String currentUserId, String currentOrgId, IqpContExt iqpContExt) throws Exception {
        log.info("同业授信申报审批流程申请启用授信申报审批流程申请启用【{}】，流程打回或退回操作", iqpContExt.getIqpSerno());
        //针对流程到办结节点，进行以下处理
        iqpContExtService.update(iqpContExt);

        //推送首页提醒事项 add by zhangjw 20210630
        iqpContExtService.sendWbMsgNotice(iqpContExt,CmisBizConstants.STD_WB_NOTICE_TYPE_1,
                resultInstanceDto.getComment().getUserComment(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getCurrentOrgId());
    }


    private void iqpContExtApplySign(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型" + currentOpType);
        try {
            IqpContExt iqpContExt = iqpContExtService.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                //档案
                String bizType = resultInstanceDto.getBizType();
                String nodeId = resultInstanceDto.getNodeId();
                if ((CmisFlowConstants.FLOW_TYPE_TYPE_BG026.equals(bizType) && "243_5".equals(nodeId)) ||
                        (CmisFlowConstants.FLOW_TYPE_TYPE_BG027.equals(bizType))){
                    this.createCentralFileTask(bizType,serno,resultInstanceDto);
                }
                iqpContExt.setContApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                iqpContExtService.update(iqpContExt);
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",bizType,currentOpType,serno);
                // iqpContExtService.createImageSpplInfo(serno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId());
                log.info("合作方协议申请【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                iqpContExt.setContApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                iqpContExtService.update(iqpContExt);
                log.info("合作方协议申请【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                /*
                 *************************************************************************
                 * 审批通过调用核心期限变更接口ln3057   ------------> 核心确认展期调用   ln3074
                 * *************************************************************************/
                // 获取责任人
                String managerBrId = iqpContExt.getManagerBrId();
                if(null == managerBrId ){
                    throw BizException.error(null, EcbEnum.ECB010001.key, "责任机构为空请确认!");
                }
                // 账务机构
                String brchno = managerBrId.substring(0, managerBrId.length()-1) +"1";
                // 村镇银行账务机构处理
                if(brchno.startsWith("80")){ // 寿光村镇银行
                    brchno = "800101";  // 寿光村镇银行营业部
                }
                if(brchno.startsWith("81")){ // 东海村镇银行
                    brchno = "810101";  // 东海村镇银行营业部
                }
                QueryModel model = new QueryModel();
                model.addCondition("iqpSerno",serno);
                List<IqpContExtBill> list = iqpContExtBillService.selectByModel(model);
                DecimalFormat df = new DecimalFormat("0.000000");
                // 判断成功还是失败 默认成功
                String flag = "S";
                /**   ln3074 展期发送核心开始 **/
                for (int i = 0; i < list.size(); i++) {
                    IqpContExtBill iqpContExtBillDetail = list.get(i);
                    // 获取借据信息
                    AccLoan accLoan = accLoanService.selectByBillNo(iqpContExtBillDetail.getBillNo());
                    Ln3074ReqDto reqDto = new Ln3074ReqDto();
                    // 账务机构
                    reqDto.setBrchno(brchno);
                    // 1.业务操作标志 daikczbz  3 直通
                    reqDto.setDaikczbz("3");
                    // 2.贷款借据号 dkjiejuh
                    reqDto.setDkjiejuh(iqpContExtBillDetail.getBillNo());
                    // 3.展期序号 zhanqixh  默认 0
                    reqDto.setZhanqixh(0);
                    // 4.展期合同号 zhanqhth
                    // reqDto.setZhanqhth(iqpContExtBillDetail.getContNo());
                    // 5.展期日期 zhanqirq
                    reqDto.setZhanqirq(iqpContExtBillDetail.getLoanEndDate().replace("-",""));
                    // 6.展期到期日 zhanqdqr
                    reqDto.setZhanqdqr(iqpContExtBillDetail.getExtEndDate().replace("-",""));
                    // 7.展期金额 zhanqije
                    reqDto.setZhanqije(iqpContExtBillDetail.getExtAmt());
                    // 8.还款方式 huankfsh
                     reqDto.setHuankfsh("");

                    // 10.利率类型 lilvleix
                    String lilvleix = accLoan.getRateAdjMode(); // 利率调整方式
                    String lilvfdfs = accLoan.getIrFloatType(); //  利率浮动方式

                    //   利率类型核心：1--PBOC、2--SHIBOR、3--HIBOR、4--LIBOR、5--SIBOR、6--固定利率
                    //   利率类型新信贷：01 固定利率 02 浮动利率

                    if("01".equals(lilvleix)){ // 01 ---> 6 固定利率
                        lilvleix = "6";
                        lilvfdfs = "0";
                    } else {
                        lilvleix = "1";// 02 --> 1 浮动利率
                        if("00".equals(lilvfdfs)){ //  00	LPR加点
                            lilvfdfs = "1";
                        } else { // 百分比
                            lilvfdfs = "2";
                        }
                    }
                     reqDto.setLilvleix(lilvleix);
                    // 11.正常利率(展期利率) zhchlilv
                    BigDecimal zhchlilv = BigDecimal.ZERO;
                    zhchlilv = iqpContExtBillDetail.getExtRate().multiply(new BigDecimal(100)).setScale(7, BigDecimal.ROUND_HALF_UP);
                    reqDto.setZhchlilv(zhchlilv);
                    // 12.利率调整方式 lilvtzfs
                    String lilvtzfs = accLoan.getRateAdjType();
                    if("IMM".equals(lilvtzfs)){ //立即调整
                        lilvtzfs = "4";
                    } else if("DDA".equals(lilvtzfs)){ //满一年
                        lilvtzfs = "1";
                    } else if("NYF".equals(lilvtzfs)){ //每年1.1
                        lilvtzfs = "2";
                    } else if("FIX".equals(lilvtzfs)){ //固定日
                        lilvtzfs = "2";
                    } else if("FIA".equals(lilvtzfs)){ //按月
                        lilvtzfs = "2";
                    } else {
                        lilvtzfs = "";
                    }


                    if("6".equals(lilvleix)){ // 01 ---> 6 固定利率
                        lilvtzfs = "0";
                    }

                     reqDto.setLilvtzfs(lilvtzfs);

                    // 13.利率调整周期 lilvtzzq
                    String lilvtzzq = "";
                    // 结息间隔周期
                    String eiIntervalCycle = accLoan.getEiIntervalCycle();
                    // 结息周期单位
                    String eiIntervalUnit = accLoan.getEiIntervalUnit();
                    // 还款日
                    String repayDay = accLoan.getDeductDay();
                    repayDay = repayDay.length() == 1 ? "0".concat(repayDay) : repayDay;

                    // 标识位 对公客户号以8开头的
                    String sy_flag = "A";
                    // 节假日顺延标志位,对公客户默认展示1
                    if(accLoan.getCusId() !=null && accLoan.getCusId().startsWith("8")){
                        sy_flag="N";
                    }

                    if("Q".equals(eiIntervalUnit)){ // 季
                        lilvtzzq = eiIntervalCycle + eiIntervalUnit + sy_flag + repayDay + "E";
                    } else if ("M".equals(eiIntervalUnit)){ // 按月
                        lilvtzzq =  eiIntervalCycle + eiIntervalUnit + sy_flag + repayDay;
                    } else if ("Y".equals(eiIntervalUnit)){ // 按年
                        lilvtzzq =   eiIntervalCycle + eiIntervalUnit + sy_flag + "12" + repayDay;
                    } else {
                        lilvtzzq = "";
                    }
                    reqDto.setLilvtzzq(lilvtzzq);

                    // 14.利率浮动方式 lilvfdfs
                     reqDto.setLilvfdfs(lilvfdfs);

                    // 15.利率浮动值 lilvfdzh
                    BigDecimal lilvfdzh = BigDecimal.ZERO;
                    String zclilvbh ="";
                    if("1".equals(lilvfdfs)){ //  00	LPR加点
                        lilvfdzh = accLoan.getRateFloatPoint();
                    } else { // 百分比
                        lilvfdzh = accLoan.getIrFloatRate().multiply(new BigDecimal(100));
                    }
                    reqDto.setLilvfdzh(lilvfdzh.setScale(7, BigDecimal.ROUND_HALF_UP));

                    // 16.贷款账号 dkzhangh
                    // reqDto.setDkzhangh("");
                    // 17.合同编号 hetongbh
                    reqDto.setHetongbh(iqpContExtBillDetail.getContNo());
                     // 18.客户号 kehuhaoo
                    reqDto.setKehuhaoo(iqpContExt.getCusId());
                    // 19.客户名称 kehmingc
                    reqDto.setKehmingc(iqpContExt.getCusName());
                    // 20.货币代号 huobdhao 01 人民币
                    reqDto.setHuobdhao(DicTranEnum.lookup("CUR_TYPE_XDTOHX_" + accLoan.getContCurType()));
                    // 21.是否取消预约 shifyyqx 0 否
                    reqDto.setShifyyqx("0");
                    // 22.正常利率编号 zclilvbh
                  /*  if(!"6".equals(lilvleix)){ // 01 ---> 6 固定利率
                        if("1".equals(lilvfdfs)){ //  00	LPR加点
                            zclilvbh ="LPR1";
                        } else { // 百分比
                            zclilvbh ="KDDK1";
                        }

                    } else {
                        zclilvbh = "";
                    }*/
                    reqDto.setZclilvbh("");// 核心确认后传空

                    // 23.年/月利率标识 nyuelilv
                     reqDto.setNyuelilv("Y");
                    // 24.逾期利率参考类型 yqllcklx
                     reqDto.setYqllcklx("");
                    // 25.逾期利率编号 yuqillbh
                     reqDto.setYuqillbh("");
                    // 26.逾期利率调整方式 yuqitzfs
                     reqDto.setYuqitzfs("");
                    // 27.逾期利率调整周期 yuqitzzq
                     reqDto.setYuqitzzq("");
                    // 28.逾期罚息浮动方式 yqfxfdfs
                     reqDto.setYqfxfdfs("2");
                    // 29.逾期罚息浮动值 yqfxfdzh
                     reqDto.setYqfxfdzh(new BigDecimal("50"));
                    // 30.逾期年月利率 yuqinyll
                     reqDto.setYuqinyll("Y");
                    // 31.逾期利率 yuqililv
                    BigDecimal tempYuqililv = BigDecimal.ZERO;
                    tempYuqililv = accLoan.getOverdueExecRate().multiply(new BigDecimal("100")).setScale(7, BigDecimal.ROUND_HALF_UP);
                    reqDto.setYuqililv(tempYuqililv);
                    // 32.复利利率参考类型 flllcklx
                     reqDto.setFlllcklx("");
                    // 33.复利利率编号 fulilvbh
                     reqDto.setFulilvbh("");
                    // 34.复利利率调整方式 fulitzfs
                     reqDto.setFulitzfs("");
                    // 35.复利利率调整周期 fulitzzq
                     reqDto.setFulitzzq("");
                    // 36.复利利率浮动方式 fulifdfs
                     reqDto.setFulifdfs("");
                    // 37.复利利率浮动值 fulifdzh
                     reqDto.setFulifdzh(new BigDecimal("50"));
                    // 38.复利利率年月标识 fulilvny
                     reqDto.setFulilvny("");
                    // 39.复利利率 fulililv
                     reqDto.setFulililv(accLoan.getCiExecRate().multiply(new BigDecimal(100)).setScale(7, BigDecimal.ROUND_HALF_UP));
                    // 40.出账号 chuzhhao
                     reqDto.setChuzhhao("");
                    // 41.复核机构 fuhejgou
                     reqDto.setFuhejgou("");
                    // 42.分段计息标志 fdjixibz
                     reqDto.setFdjixibz("");
                    // 9.还款周期 hkzhouqi
                    reqDto.setHkzhouqi(lilvtzzq);
                    log.info("展期协议流程中调用[ln3074|贷款预展期]逻辑开始,请求参数为:[{}]", JSON.toJSONString(reqDto));
                    ResultDto<Ln3074RespDto> ln3074ResultDto = dscms2CoreLnClientService.ln3074(reqDto);
                    log.info("展期协议流程中调用[ln3074|贷款预展期]逻辑结束,响应参数为:[{}]", JSON.toJSONString(ln3074ResultDto));
                    String ln3074Code = Optional.ofNullable(ln3074ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String ln3074Meesage = Optional.ofNullable(ln3074ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    Ln3057RespDto l = null;
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3074ResultDto.getCode())) {
                       // this.changeAccLoan(serno,iqpContExt.getExtType());
                        this.changStatus(serno,"1",ln3074Meesage);
                    } else {
                        this.changStatus(serno,"0",ln3074Meesage);
                        flag = "F";
                    }
                }
                /**   ln3074 展期发送核心 结束 **/


              /*  list.parallelStream().map(extBill->{
                    Ln3057ReqDto reqDto = new Ln3057ReqDto();
                    reqDto.setDaikczbz("3");		//业务操作标志
                    reqDto.setDkjiejuh(extBill.getBillNo());		//贷款借据号
                    reqDto.setDkzhangh("");		//贷款账号
                    reqDto.setKehuhaoo(iqpContExt.getCusId());		//客户号
                    reqDto.setKehuzwmc(iqpContExt.getCusName());		//客户名
                    reqDto.setQixiriqi("");		//起息日期
                    reqDto.setDkqixian("");		//贷款期限(月)
                    reqDto.setDaoqriqi(extBill.getExtEndDate().replaceAll("-",""));		//到期日期
                    reqDto.setLilvleix("");		//利率类型
                    reqDto.setZclilvbh("");		//正常利率编号
                    reqDto.setZhchlilv(extBill.getExtRate());		//正常利率
//                    reqDto.setNyuelilv("");		//年/月利率标识
//                    reqDto.setLilvtzfs("");		//利率调整方式
//                    reqDto.setLilvtzzq("");		//利率调整周期
//                    reqDto.setLilvfdfs("");		//利率浮动方式
//                    reqDto.setLilvfdzh("");		//利率浮动值
//                    reqDto.setYqllcklx("");		//逾期利率参考类型
//                    reqDto.setYuqillbh("");		//逾期利率编号
//                    reqDto.setYuqinyll("");		//逾期年月利率
//                    reqDto.setYuqililv("");		//逾期利率
//                    reqDto.setYuqitzfs("");		//逾期利率调整方式
//                    reqDto.setYqfxfdzh("");		//逾期罚息浮动值
//                    reqDto.setFlllcklx("");		//复利利率参考类型
//                    reqDto.setFulilvbh("");		//复利利率编号
//                    reqDto.setFulilvny("");		//复利利率年月标识
//                    reqDto.setFulililv("");		//复利利率
//                    reqDto.setFulitzfs("");		//复利利率调整方式
//                    reqDto.setFulitzzq("");		//复利利率调整周期
//                    reqDto.setFulifdfs("");		//复利利率浮动方式
//                    reqDto.setFulifdzh("");		//复利利率浮动值
//                    reqDto.setLlqxkdfs("");		//利率期限靠档方式
//                    reqDto.setLilvqixx("");		//利率期限
                    ResultDto<Ln3057RespDto> ln3057ResultDto = dscms2CoreLnClientService.ln3057(reqDto);
                    String ln3057Code = Optional.ofNullable(ln3057ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String ln3057Meesage = Optional.ofNullable(ln3057ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    Ln3057RespDto l = null;
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3057ResultDto.getCode())) {
                        //  获取相关的值并解析
                        l = ln3057ResultDto.getData();
                        this.changeAccLoan(serno);
                        this.changStatus(serno,"1");
                    } else {
                        //  抛出错误异常
                        this.changStatus(serno,"0");
                        throw BizException.error(null, ln3057Code, ln3057Meesage);

                    }
                    return l;
                }).count();*/
                /*
                 *************************************************************************
                 * 审批通过调用核心期限变更接口结束ln3057
                 * *************************************************************************/
                iqpContExt.setContApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                if("F".equals(flag)){ // 失败
                    iqpContExt.setStatus("0");
                } else {
                    iqpContExt.setStatus("1");
                }
                iqpContExtService.update(iqpContExt);

                // 生成归档任务
                log.info("开始系统生成档案归档信息");
                String cusId = iqpContExt.getCusId();
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                docArchiveClientDto.setArchiveMode("");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
                docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
                docArchiveClientDto.setDocType("23");//业务变更-展期业务
                docArchiveClientDto.setBizSerno(serno);
                docArchiveClientDto.setCusId(cusBaseClientDto.getCusId());
                docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
                docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                docArchiveClientDto.setManagerId(iqpContExt.getManagerId());
                docArchiveClientDto.setManagerBrId(iqpContExt.getManagerBrId());
                docArchiveClientDto.setInputId(iqpContExt.getInputId());
                docArchiveClientDto.setInputBrId(iqpContExt.getInputBrId());
                docArchiveClientDto.setContNo(iqpContExt.getContNo());
                docArchiveClientDto.setLoanAmt(iqpContExt.getContAmt());
                docArchiveClientDto.setStartDate(iqpContExt.getContStartDate());
                docArchiveClientDto.setEndDate(iqpContExt.getContEndDate());
                int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                if (num < 1) {
                    log.info("系统生成档案归档信息失败");
                }
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,serno);
                iqpContExtService.createImageSpplInfo(serno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),"展期协议",CmisCommonConstants.STD_SPPL_TYPE_09);
                log.info("合作方协议申请【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
                // 推送用印系统
                try {
                    cmisBizXwCommonService.sendYk(currentUserId,serno,iqpContExt.getCusName());
                    log.info("推送印系统成功:【{}】", serno);
                } catch (Exception e) {
                    log.info("推送印系统异常:【{}】", e.getMessage());
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpContExt.setContApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    iqpContExtService.update(iqpContExt);
                    log.info("合作方协议申请【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                }
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                iqpContExt.setContApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                iqpContExtService.update(iqpContExt);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                iqpContExt.setContApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                iqpContExtService.update(iqpContExt);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                iqpContExt.setContApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                iqpContExtService.update(iqpContExt);
            } else {
                log.info("合作方协议申请【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("合作方协议申请后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    public void changeAccLoan(String iqpSerno, String extType){
        QueryModel model = new QueryModel();
        model.addCondition("iqpSerno",iqpSerno);
        List<IqpContExtBill> list = iqpContExtBillService.selectByModel(model);
        for (IqpContExtBill iqpContExtBill: list) {
            String bill_no = iqpContExtBill.getBillNo();
            AccLoan accLoan = accLoanService.selectByBillNo(bill_no);
            accLoan.setLoanEndDate(iqpContExtBill.getExtEndDate());
            accLoan.setExecRateYear(iqpContExtBill.getExtRate());
            accLoan.setExtTimes(iqpContExtBill.getExtTimes());
            accLoan.setExtType(extType);
            accLoanService.update(accLoan);
        }

    }

    public  void changStatus(String iqpSerno,String num,String meesage){
        IqpContExt iqpContExt = iqpContExtService.selectByPrimaryKey(iqpSerno);
        iqpContExt.setContApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
        iqpContExt.setStatus(num);
        iqpContExt.setResn(meesage);
        iqpContExtService.update(iqpContExt);
    }

    public void createCentralFileTask(String bizType , String iqpSerno,ResultInstanceDto instanceInfo){
        IqpContExt iqpContExt = iqpContExtService.selectByPrimaryKey(iqpSerno);
        try {
            ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(iqpContExt.getInputBrId());
            String orgType = resultDto.getData().getOrgType();
            //        0-总行部室
            //        1-异地支行（有分行）
            //        2-异地支行（无分行）
            //        3-异地分行
            //        4-中心支行
            //        5-综合支行
            //        6-对公支行
            //        7-零售支行
            if(!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType)){
                //新增临时档案任务
                CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                centralFileTaskdto.setSerno(iqpContExt.getIqpSerno());
                centralFileTaskdto.setCusId(iqpContExt.getCusId());
                centralFileTaskdto.setCusName(iqpContExt.getCusName());
                centralFileTaskdto.setBizType(bizType); //
                centralFileTaskdto.setInstanceId(instanceInfo.getInstanceId());
                centralFileTaskdto.setNodeId(instanceInfo.getNextNodeInfos().get(0).getNextNodeId()); // 集中作业档案岗节点id
                centralFileTaskdto.setInputId(iqpContExt.getInputId());
                centralFileTaskdto.setInputBrId(iqpContExt.getInputBrId());
                centralFileTaskdto.setOptType("02"); // 非纯指令
                centralFileTaskdto.setTaskType("02"); // 档案暂存
                centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                cmisBizClientService.createCentralFileTask(centralFileTaskdto);
            }
        }catch (Exception e){
            log.info("利率变更审批流程："+"流水号："+iqpSerno+"-归档异常------------------"+e);
        }
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_BGYW02.equals(flowCode);
    }
    private int find_first_elem(List<BigDecimal> rateList,BigDecimal elem){
        for(int i = 0 ; i < rateList.size() ; i++){
            if(rateList.get(i).compareTo(elem)==0){
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        BigDecimal a = new BigDecimal(795.000000000);

    }
}

