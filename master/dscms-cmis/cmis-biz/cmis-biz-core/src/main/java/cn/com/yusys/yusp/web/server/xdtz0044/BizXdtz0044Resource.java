package cn.com.yusys.yusp.web.server.xdtz0044;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0044.req.Xdtz0044DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0044.resp.Xdtz0044DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0044.Xdtz0044Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 接口处理类:根据客户号前往信贷查找房贷借据信息
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0044:根据客户号前往信贷查找房贷借据信息")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0044Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0044Resource.class);

    @Autowired
    private Xdtz0044Service xdtz0044Service;

    /**
     * 交易码：xdtz0044
     * 交易描述：根据客户号前往信贷查找房贷借据信息
     *
     * @param xdtz0044DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号前往信贷查找房贷借据信息")
    @PostMapping("/xdtz0044")
    protected @ResponseBody
    ResultDto<Xdtz0044DataRespDto> xdtz0044(@Validated @RequestBody Xdtz0044DataReqDto xdtz0044DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, JSON.toJSONString(xdtz0044DataReqDto));
        Xdtz0044DataRespDto xdtz0044DataRespDto = new Xdtz0044DataRespDto();// 响应Dto:根据客户号前往信贷查找房贷借据信息
        ResultDto<Xdtz0044DataRespDto> xdtz0044DataResultDto = new ResultDto<>();
        String cusId = xdtz0044DataReqDto.getCusId();
        // 从xdtz0044DataReqDto获取业务值进行业务逻辑处理
        try {
            // 调用xdtz0044Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, cusId);
            xdtz0044DataRespDto = Optional.ofNullable(xdtz0044Service.getHouseLoanContByCusId(cusId))
                    .orElse(new Xdtz0044DataRespDto(StringUtils.EMPTY, StringUtils.EMPTY));
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, JSON.toJSONString(xdtz0044DataRespDto));
            // 封装xdtz0044DataResultDto中正确的返回码和返回信息
            xdtz0044DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0044DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, e.getMessage());
            // 封装xdtz0044DataResultDto中异常返回码和返回信息
            xdtz0044DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0044DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0044DataRespDto到xdtz0044DataResultDto中
        xdtz0044DataResultDto.setData(xdtz0044DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, JSON.toJSONString(xdtz0044DataResultDto));
        return xdtz0044DataResultDto;
    }
}
