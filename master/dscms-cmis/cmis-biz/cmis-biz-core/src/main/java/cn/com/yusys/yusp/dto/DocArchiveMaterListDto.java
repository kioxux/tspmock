package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocArchiveMaterList
 * @类描述: doc_archive_mater_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-20 10:54:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class DocArchiveMaterListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 清单流水号 **/
	private String admlSerno;

	/** 档案流水号 **/
	private String docSerno;
	
	/** 材料类型 **/
	private String docTypeData;
	
	/** 材料清单 **/
	private String docListData;
	
	/** 资料页数 **/
	private String pagesNum;
	
	/** 是否必须原件 **/
	private String isSourceFlag;
	
	/** 备注 **/
	private String memo;
	
	/** 排序 **/
	private String sort;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 一级影像目录编号 **/
	private String topOutsystemCode;
	
	/** 二级影像目录编号 **/
	private String secOutsystemCode;
	
	/** 文件影像目录编号 **/
	private String fileOutsystemCode;
	
	
	/**
	 * @param admlSerno
	 */
	public void setAdmlSerno(String admlSerno) {
		this.admlSerno = admlSerno == null ? null : admlSerno.trim();
	}
	
    /**
     * @return AdmlSerno
     */	
	public String getAdmlSerno() {
		return this.admlSerno;
	}
	
	/**
	 * @param docSerno
	 */
	public void setDocSerno(String docSerno) {
		this.docSerno = docSerno == null ? null : docSerno.trim();
	}
	
    /**
     * @return DocSerno
     */	
	public String getDocSerno() {
		return this.docSerno;
	}
	
	/**
	 * @param docTypeData
	 */
	public void setDocTypeData(String docTypeData) {
		this.docTypeData = docTypeData == null ? null : docTypeData.trim();
	}
	
    /**
     * @return DocTypeData
     */	
	public String getDocTypeData() {
		return this.docTypeData;
	}
	
	/**
	 * @param docListData
	 */
	public void setDocListData(String docListData) {
		this.docListData = docListData == null ? null : docListData.trim();
	}
	
    /**
     * @return DocListData
     */	
	public String getDocListData() {
		return this.docListData;
	}
	
	/**
	 * @param pagesNum
	 */
	public void setPagesNum(String pagesNum) {
		this.pagesNum = pagesNum == null ? null : pagesNum.trim();
	}
	
    /**
     * @return PagesNum
     */	
	public String getPagesNum() {
		return this.pagesNum;
	}
	
	/**
	 * @param isSourceFlag
	 */
	public void setIsSourceFlag(String isSourceFlag) {
		this.isSourceFlag = isSourceFlag == null ? null : isSourceFlag.trim();
	}
	
    /**
     * @return IsSourceFlag
     */	
	public String getIsSourceFlag() {
		return this.isSourceFlag;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}
	
    /**
     * @return Memo
     */	
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param sort
	 */
	public void setSort(String sort) {
		this.sort = sort == null ? null : sort.trim();
	}
	
    /**
     * @return Sort
     */	
	public String getSort() {
		return this.sort;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param topOutsystemCode
	 */
	public void setTopOutsystemCode(String topOutsystemCode) {
		this.topOutsystemCode = topOutsystemCode == null ? null : topOutsystemCode.trim();
	}
	
    /**
     * @return TopOutsystemCode
     */	
	public String getTopOutsystemCode() {
		return this.topOutsystemCode;
	}
	
	/**
	 * @param secOutsystemCode
	 */
	public void setSecOutsystemCode(String secOutsystemCode) {
		this.secOutsystemCode = secOutsystemCode == null ? null : secOutsystemCode.trim();
	}
	
    /**
     * @return SecOutsystemCode
     */	
	public String getSecOutsystemCode() {
		return this.secOutsystemCode;
	}
	
	/**
	 * @param fileOutsystemCode
	 */
	public void setFileOutsystemCode(String fileOutsystemCode) {
		this.fileOutsystemCode = fileOutsystemCode == null ? null : fileOutsystemCode.trim();
	}
	
    /**
     * @return FileOutsystemCode
     */	
	public String getFileOutsystemCode() {
		return this.fileOutsystemCode;
	}


}