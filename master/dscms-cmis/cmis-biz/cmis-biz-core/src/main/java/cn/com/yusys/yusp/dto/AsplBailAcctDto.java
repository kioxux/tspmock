package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoan
 * @类描述: acc_loan数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-04 14:09:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class AsplBailAcctDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 流水号 **/
	private String serno;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 资产池协议编号 **/
	private String contNo;

	/** 保证金账号 **/
	private String bailAccNo;

	/** 保证金账号户名 **/
	private String bailAccName;

	/** 保证金留存比例 **/
	private String bailRate;

	/** 结算账号 **/
	private String acctNo;

	/** 结算账号户名 **/
	private String acctName;

	/** 手续费比例 **/
	private java.math.BigDecimal chrgRate;

	/** 子序号 **/
	private java.math.BigDecimal bailAccNoSub;

	/** 授信品种编号 **/
	private String lmtBizType;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getSerno() {
		return serno;
	}

	public void setSerno(String serno) {
		this.serno = serno;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getBailAccNo() {
		return bailAccNo;
	}

	public void setBailAccNo(String bailAccNo) {
		this.bailAccNo = bailAccNo;
	}

	public String getBailAccName() {
		return bailAccName;
	}

	public void setBailAccName(String bailAccName) {
		this.bailAccName = bailAccName;
	}

	public String getBailRate() {
		return bailRate;
	}

	public void setBailRate(String bailRate) {
		this.bailRate = bailRate;
	}

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public String getAcctName() {
		return acctName;
	}

	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}

	public BigDecimal getChrgRate() {
		return chrgRate;
	}

	public void setChrgRate(BigDecimal chrgRate) {
		this.chrgRate = chrgRate;
	}

	public BigDecimal getBailAccNoSub() {
		return bailAccNoSub;
	}

	public void setBailAccNoSub(BigDecimal bailAccNoSub) {
		this.bailAccNoSub = bailAccNoSub;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getLmtBizType() {
		return lmtBizType;
	}

	public void setLmtBizType(String lmtBizType) {
		this.lmtBizType = lmtBizType;
	}

}