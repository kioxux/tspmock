package cn.com.yusys.yusp.web.server.xdxw0005;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0005.req.List;
import cn.com.yusys.yusp.dto.server.xdxw0005.req.Xdxw0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0005.resp.Xdxw0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:小微营业额信息维护
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0005:小微营业额信息维护")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0005Resource.class);

    /**
     * 交易码：xdxw0005
     * 交易描述：小微营业额信息维护
     *
     * @param xdxw0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微营业额信息维护")
    @PostMapping("/xdxw0005")
    protected @ResponseBody
    ResultDto<Xdxw0005DataRespDto> xdxw0005(@Validated @RequestBody Xdxw0005DataReqDto xdxw0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0005.key, DscmsEnum.TRADE_CODE_XDXW0005.value, JSON.toJSONString(xdxw0005DataReqDto));
        Xdxw0005DataRespDto xdxw0005DataRespDto = new Xdxw0005DataRespDto();// 响应Dto:小微营业额信息维护
        ResultDto<Xdxw0005DataRespDto> xdxw0005DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0005DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
			String surveyNo = xdxw0005DataReqDto.getSurveyNo();//调查流水号
			String surveyType = xdxw0005DataReqDto.getSurveyType();//调查表类型
			List list = xdxw0005DataReqDto.getList();//start
            // TODO 调用XXXXXService层结束
            // TODO 封装xdxw0005DataRespDto对象开始
			xdxw0005DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
			xdxw0005DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xdxw0005DataRespDto对象结束
            // 封装xdxw0005DataResultDto中正确的返回码和返回信息
            xdxw0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0005.key, DscmsEnum.TRADE_CODE_XDXW0005.value, e.getMessage());
            // 封装xdxw0005DataResultDto中异常返回码和返回信息
            xdxw0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0005DataRespDto到xdxw0005DataResultDto中
        xdxw0005DataResultDto.setData(xdxw0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0005.key, DscmsEnum.TRADE_CODE_XDXW0005.value, JSON.toJSONString(xdxw0005DataResultDto));
        return xdxw0005DataResultDto;
    }
}
