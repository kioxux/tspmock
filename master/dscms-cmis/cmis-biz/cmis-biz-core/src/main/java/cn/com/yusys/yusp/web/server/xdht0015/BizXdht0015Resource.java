package cn.com.yusys.yusp.web.server.xdht0015;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0015.req.Xdht0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0015.resp.Xdht0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdht0015.Xdht0015Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:合同房产人员信息查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0015:合同房产人员信息查询")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0015Resource.class);

    @Autowired
    private Xdht0015Service xdht0015Service;

    /**
     * 交易码：xdht0015
     * 交易描述：合同房产人员信息查询
     *
     * @param xdht0015DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同房产人员信息查询")
    @PostMapping("/xdht0015")
    protected @ResponseBody
    ResultDto<Xdht0015DataRespDto> xdht0015(@Validated @RequestBody Xdht0015DataReqDto xdht0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, JSON.toJSONString(xdht0015DataReqDto));
        Xdht0015DataRespDto xdht0015DataRespDto = new Xdht0015DataRespDto();// 响应Dto:合同房产人员信息查询
        ResultDto<Xdht0015DataRespDto> xdht0015DataResultDto = new ResultDto<>();
        try {
            // 身份证号码
            String certNo = xdht0015DataReqDto.getCertNo();
            // 产品码
            String prdCode = xdht0015DataReqDto.getPrdCode();
            if (StringUtils.isEmpty(certNo)) {
                throw BizException.error(null, EcbEnum.ECB010048.key, EcbEnum.ECB010048.value);
            }
            if (StringUtils.isEmpty(prdCode)) {
                throw BizException.error(null, EcbEnum.ECB010051.key, EcbEnum.ECB010051.value);
            }
            xdht0015DataRespDto = xdht0015Service.xdht0015Execute(xdht0015DataReqDto);
            xdht0015DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0015DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }  catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, e.getMessage());
            // 封装xdkh0001DataResultDto中异常返回码和返回信息
            xdht0015DataResultDto.setCode(e.getErrorCode());
            xdht0015DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, e.getMessage());
            // 封装xdht0015DataResultDto中异常返回码和返回信息
            xdht0015DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0015DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0015DataRespDto到xdht0015DataResultDto中
        xdht0015DataResultDto.setData(xdht0015DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, JSON.toJSONString(xdht0015DataRespDto));
        return xdht0015DataResultDto;
    }
}
