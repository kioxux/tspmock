package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021年7月28日16:04:21
 * @desc 个人住房按揭贷款担保方式校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0054Service {


    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021年7月28日16:04:21
     * @version 1.0.0
     * @desc    贷款品种为个人住房按揭类贷款，担保方式不能为保证
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0054(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String iqpSerno = queryModel.getCondition().get("bizId").toString();
        String prdType = StringUtils.EMPTY;
        String fundUnionFlag = StringUtils.EMPTY;
        if (StringUtils.isBlank(iqpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 通过申请流水号获取申请信息
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);
        if (null != iqpLoanApp && !StringUtils.isBlank(iqpLoanApp.getPrdId())) {
            ResultDto<CfgPrdBasicinfoDto> basicInfo = iCmisCfgClientService.queryCfgPrdBasicInfo(iqpLoanApp.getPrdId());
            //C000700020001:->业务品种->个人消费类贷款->零售消费类贷款->按揭类
            //C000700020002:->业务品种->个人消费类贷款->零售消费类贷款->非按揭类
            prdType = basicInfo.getData().getPrdType();

            ;
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04505); //通过申请流水号获取申请信息失败
            return riskResultDto;
        }
        // 只有按揭类才会进行校验
        if(prdType.equals("10")){
            if("022031".equals(iqpLoanApp.getPrdId())){  // 拍卖贷可以提供阶段性担保   022031
                // 拍卖贷可以申请是可以用抵押、保证、质押3种，然后做押品置换。   公积金组合选是只能是保证
                    if(!"30".equals(iqpLoanApp.getGuarWay()) && !"20".equals(iqpLoanApp.getGuarWay()) && !"10".equals(iqpLoanApp.getGuarWay())){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05401);
                        return riskResultDto;
                    }
            } else{
                if("30".equals(iqpLoanApp.getGuarWay())){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04517);
                    return riskResultDto;
                }
            }

        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
