package cn.com.yusys.yusp.web.server.xdxw0044;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0044.req.Xdxw0044DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0044.resp.Xdxw0044DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0044.Xdxw0044Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:优抵贷待办事项接口
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0044:优抵贷待办事项接口")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0044Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0044Resource.class);

    @Autowired
    private Xdxw0044Service xdxw0044Service;

    /**
     * 交易码：xdxw0044
     * 交易描述：优抵贷待办事项接口
     *
     * @param xdxw0044DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优抵贷待办事项接口")
    @PostMapping("/xdxw0044")
    protected @ResponseBody
    ResultDto<Xdxw0044DataRespDto> xdxw0044(@Validated @RequestBody Xdxw0044DataReqDto xdxw0044DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, JSON.toJSONString(xdxw0044DataReqDto));
        Xdxw0044DataRespDto xdxw0044DataRespDto = new Xdxw0044DataRespDto();// 响应Dto:优抵贷待办事项接口
        ResultDto<Xdxw0044DataRespDto> xdxw0044DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, JSON.toJSONString(xdxw0044DataReqDto));
            xdxw0044DataRespDto = xdxw0044Service.xdxw0044(xdxw0044DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, JSON.toJSONString(xdxw0044DataRespDto));
            // 封装xdxw0044DataResultDto中正确的返回码和返回信息
            String opFlag = xdxw0044DataRespDto.getOpFlag();
            String opMessage = xdxw0044DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdxw0044DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0044DataResultDto.setMessage(opMessage);
            } else {
                xdxw0044DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0044DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, e.getMessage());
            xdxw0044DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0044DataResultDto.setMessage(e.getMessage());
            // 封装xdxw0047DataResultDto中异常返回码和返回信息
            xdxw0044DataResultDto.setCode(e.getErrorCode());
            xdxw0044DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, e.getMessage());
            xdxw0044DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0044DataResultDto.setMessage(e.getMessage());
            // 封装xdxw0044DataResultDto中异常返回码和返回信息
            xdxw0044DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0044DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0044DataRespDto到xdxw0044DataResultDto中
        xdxw0044DataResultDto.setData(xdxw0044DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, JSON.toJSONString(xdxw0044DataResultDto));
        return xdxw0044DataResultDto;
    }
}
