package cn.com.yusys.yusp.web.server.xdxw0006;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0006.req.List;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.server.xdxw0006.Xdxw0006Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdxw0006.req.Xdxw0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0006.resp.Xdxw0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:联系人信息维护
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0006:联系人信息维护")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0006Resource.class);

    @Autowired
    private Xdxw0006Service xdxw0006Service;

    /**
     * 交易码：xdxw0006
     * 交易描述：联系人信息维护
     *
     * @param xdxw0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("联系人信息维护")
    @PostMapping("/xdxw0006")
    protected @ResponseBody
    ResultDto<Xdxw0006DataRespDto> xdxw0006(@Validated @RequestBody Xdxw0006DataReqDto xdxw0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, JSON.toJSONString(xdxw0006DataReqDto));
        Xdxw0006DataRespDto xdxw0006DataRespDto = new Xdxw0006DataRespDto();// 响应Dto:联系人信息维护
        ResultDto<Xdxw0006DataRespDto> xdxw0006DataResultDto = new ResultDto<>();
        try {
            String surveySerno = xdxw0006DataReqDto.getSurveySerno();//批复号
            java.util.List<List> list = xdxw0006DataReqDto.getList();
            // 非空校验
            if (CollectionUtils.isEmpty(list)) {
                xdxw0006DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0006DataResultDto.setMessage("请求体【lxrList】不能为空！");
                return xdxw0006DataResultDto;
            }
            if (StringUtil.isEmpty(surveySerno)) {
                xdxw0006DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0006DataResultDto.setMessage("批复号【certCode】不能为空！");
                return xdxw0006DataResultDto;
            }
            // 从xdxw0006DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, JSON.toJSONString(xdxw0006DataReqDto));
            xdxw0006DataRespDto = xdxw0006Service.getXdxw0006(xdxw0006DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, JSON.toJSONString(xdxw0006DataRespDto));
            // 封装xdxw0006DataResultDto中正确的返回码和返回信息
            String opFlag = xdxw0006DataRespDto.getOpFlag();
            String opMessage = xdxw0006DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdxw0006DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0006DataResultDto.setMessage(opMessage);
            } else {
                xdxw0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, e.getMessage());
            // 封装xdxw0006DataResultDto中异常返回码和返回信息
            xdxw0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0006DataRespDto到xdxw0006DataResultDto中
        xdxw0006DataResultDto.setData(xdxw0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, JSON.toJSONString(xdxw0006DataResultDto));
        return xdxw0006DataResultDto;
    }
}
