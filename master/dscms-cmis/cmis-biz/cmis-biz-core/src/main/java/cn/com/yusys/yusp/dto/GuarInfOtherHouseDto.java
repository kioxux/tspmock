package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfOtherHouse
 * @类描述: guar_inf_other_house数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:43:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarInfOtherHouseDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 现房/期房标识 STD_ZB_HOUSE_STATUS **/
	private String readyOrPeriodHouse;
	
	/** 预购商品房预告登记证明号 **/
	private String purchseHouseNo;
	
	/** 预购商品房抵押权预告登记证明号 **/
	private String purchseHouseRegNo;
	
	/** 预售许可证编号 **/
	private String presellPermitNo;
	
	/** 预售许可证有效期 **/
	private String presellPermitValDate;
	
	/** 预计交房年月 **/
	private String predictOthersDate;
	
	/** 一手/二手标识 STD_ZB_YESBS **/
	private String isUsed;
	
	/** 是否两证合一 STD_ZB_YES_NO **/
	private String twocard2oneInd;
	
	/** 产权证号 **/
	private String houseLandNo;
	
	/** 销售许可证编号 **/
	private String marketPermitNo;
	
	/** 房、地是否均已抵押我行 STD_ZB_YES_NO **/
	private String houseLandPledgeInd;
	
	/** 该产证是否全部抵押 STD_ZB_YES_NO **/
	private String houseAllPledgeInd;
	
	/** 部分抵押描述 **/
	private String partRegPositionDesc;
	
	/** 房地产买卖合同编号 **/
	private String businessHouseNo;
	
	/** 购买日期 **/
	private String purchaseDate;
	
	/** 购买价格（元） **/
	private java.math.BigDecimal purchaseAccnt;
	
	/** 是否本次申请所购房产 STD_ZB_YES_NO **/
	private String applyForHouse;
	
	/** 抵押住房是否权属人唯一住所 STD_ZB_YES_NO **/
	private String houseOwnershipInd;
	
	/** 建筑面积 **/
	private String buildArea;
	
	/** 建成年份 **/
	private String activateYears;
	
	/** 房屋产权期限信息 **/
	private String housePrDesc;
	
	/** 楼龄 **/
	private String floorAge;
	
	/** 朝向 STD_ZB_FWCX **/
	private String orientations;
	
	/** 房屋结构 STD_ZB_HOUSE_STRUC **/
	private String houseStructure;
	
	/** 地面构造 STD_ZB_DMWDGZ **/
	private String groundStructure;
	
	/** 屋顶构造 STD_ZB_DMWDGZ **/
	private String roofStructure;
	
	/** 所在/注册省份 **/
	private String provinceCd;
	
	/** 所在/注册市 **/
	private String cityCd;
	
	/** 所在县（区） **/
	private String countyCd;
	
	/** 街道/村镇/路名 **/
	private String street;
	
	/** 门牌号/弄号 **/
	private String houseNo;
	
	/** 楼号 **/
	private String buildingRoomNum;
	
	/** 室号 **/
	private String roomNum;
	
	/** 产权地址 **/
	private String pocAddr;
	
	/** 楼盘（社区）名称 **/
	private String communityName;
	
	/** 层次（标的楼层） **/
	private String bdlc;
	
	/** 层数（标的楼高） **/
	private String bdgd;
	
	/** 房地产所在地段情况 STD_ZB_FCDDQK **/
	private String housePlaceInfo;
	
	/** 建筑物说明 **/
	private String buildDesc;
	
	/** 是否包含土地 STD_ZB_YES_NO **/
	private String fullLand;
	
	/** 土地证号 **/
	private String landNo;
	
	/** 土地使用权性质 STD_ZB_LAND_TYPE **/
	private String landUseQual;
	
	/** 土地使用权取得方式 STD_ZB_LAND_GAIN **/
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	private String landUseEndDate;
	
	/** 土地使用年限 **/
	private String landUseYears;
	
	/** 土地用途 STD_ZB_LANDYT **/
	private String landPurp;
	
	/** 土地说明 **/
	private String landExplain;
	
	/** 土地使用权面积 **/
	private String landUseArea;
	
	/** 所属土地使用权人 **/
	private String landUseUnit;
	
	/** 房产类别 STD_ZB_INV_PTY_TYP **/
	private String realproCls;
	
	/** 房屋用途 STD_ZB_HOUSE_USE_TYPE **/
	private String houseUseType;
	
	/** 承租人名称 **/
	private String lesseeName;
	
	/** 年租金（元） **/
	private java.math.BigDecimal annualRent;
	
	/** 租期（月） **/
	private String lease;
	
	/** 剩余租期（月） **/
	private String leftLease;
	
	/** 房产取得方式  STD_ZB_ESTATE_ACQUIRE_WAY **/
	private String estateAcquireWay;
	
	/** 已使用年限 **/
	private String inUseYear;
	
	/** 所属地段 **/
	private String belongArea;
	
	/** 墙壁结构 **/
	private String wallStructure;
	
	/** 物业情况 **/
	private String propertyCase;
	
	/** 房产使用情况 **/
	private String houseProperty;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private java.util.Date updDate;
	
	/** 操作类型   **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param readyOrPeriodHouse
	 */
	public void setReadyOrPeriodHouse(String readyOrPeriodHouse) {
		this.readyOrPeriodHouse = readyOrPeriodHouse == null ? null : readyOrPeriodHouse.trim();
	}
	
    /**
     * @return ReadyOrPeriodHouse
     */	
	public String getReadyOrPeriodHouse() {
		return this.readyOrPeriodHouse;
	}
	
	/**
	 * @param purchseHouseNo
	 */
	public void setPurchseHouseNo(String purchseHouseNo) {
		this.purchseHouseNo = purchseHouseNo == null ? null : purchseHouseNo.trim();
	}
	
    /**
     * @return PurchseHouseNo
     */	
	public String getPurchseHouseNo() {
		return this.purchseHouseNo;
	}
	
	/**
	 * @param purchseHouseRegNo
	 */
	public void setPurchseHouseRegNo(String purchseHouseRegNo) {
		this.purchseHouseRegNo = purchseHouseRegNo == null ? null : purchseHouseRegNo.trim();
	}
	
    /**
     * @return PurchseHouseRegNo
     */	
	public String getPurchseHouseRegNo() {
		return this.purchseHouseRegNo;
	}
	
	/**
	 * @param presellPermitNo
	 */
	public void setPresellPermitNo(String presellPermitNo) {
		this.presellPermitNo = presellPermitNo == null ? null : presellPermitNo.trim();
	}
	
    /**
     * @return PresellPermitNo
     */	
	public String getPresellPermitNo() {
		return this.presellPermitNo;
	}
	
	/**
	 * @param presellPermitValDate
	 */
	public void setPresellPermitValDate(String presellPermitValDate) {
		this.presellPermitValDate = presellPermitValDate == null ? null : presellPermitValDate.trim();
	}
	
    /**
     * @return PresellPermitValDate
     */	
	public String getPresellPermitValDate() {
		return this.presellPermitValDate;
	}
	
	/**
	 * @param predictOthersDate
	 */
	public void setPredictOthersDate(String predictOthersDate) {
		this.predictOthersDate = predictOthersDate == null ? null : predictOthersDate.trim();
	}
	
    /**
     * @return PredictOthersDate
     */	
	public String getPredictOthersDate() {
		return this.predictOthersDate;
	}
	
	/**
	 * @param isUsed
	 */
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed == null ? null : isUsed.trim();
	}
	
    /**
     * @return IsUsed
     */	
	public String getIsUsed() {
		return this.isUsed;
	}
	
	/**
	 * @param twocard2oneInd
	 */
	public void setTwocard2oneInd(String twocard2oneInd) {
		this.twocard2oneInd = twocard2oneInd == null ? null : twocard2oneInd.trim();
	}
	
    /**
     * @return Twocard2oneInd
     */	
	public String getTwocard2oneInd() {
		return this.twocard2oneInd;
	}
	
	/**
	 * @param houseLandNo
	 */
	public void setHouseLandNo(String houseLandNo) {
		this.houseLandNo = houseLandNo == null ? null : houseLandNo.trim();
	}
	
    /**
     * @return HouseLandNo
     */	
	public String getHouseLandNo() {
		return this.houseLandNo;
	}
	
	/**
	 * @param marketPermitNo
	 */
	public void setMarketPermitNo(String marketPermitNo) {
		this.marketPermitNo = marketPermitNo == null ? null : marketPermitNo.trim();
	}
	
    /**
     * @return MarketPermitNo
     */	
	public String getMarketPermitNo() {
		return this.marketPermitNo;
	}
	
	/**
	 * @param houseLandPledgeInd
	 */
	public void setHouseLandPledgeInd(String houseLandPledgeInd) {
		this.houseLandPledgeInd = houseLandPledgeInd == null ? null : houseLandPledgeInd.trim();
	}
	
    /**
     * @return HouseLandPledgeInd
     */	
	public String getHouseLandPledgeInd() {
		return this.houseLandPledgeInd;
	}
	
	/**
	 * @param houseAllPledgeInd
	 */
	public void setHouseAllPledgeInd(String houseAllPledgeInd) {
		this.houseAllPledgeInd = houseAllPledgeInd == null ? null : houseAllPledgeInd.trim();
	}
	
    /**
     * @return HouseAllPledgeInd
     */	
	public String getHouseAllPledgeInd() {
		return this.houseAllPledgeInd;
	}
	
	/**
	 * @param partRegPositionDesc
	 */
	public void setPartRegPositionDesc(String partRegPositionDesc) {
		this.partRegPositionDesc = partRegPositionDesc == null ? null : partRegPositionDesc.trim();
	}
	
    /**
     * @return PartRegPositionDesc
     */	
	public String getPartRegPositionDesc() {
		return this.partRegPositionDesc;
	}
	
	/**
	 * @param businessHouseNo
	 */
	public void setBusinessHouseNo(String businessHouseNo) {
		this.businessHouseNo = businessHouseNo == null ? null : businessHouseNo.trim();
	}
	
    /**
     * @return BusinessHouseNo
     */	
	public String getBusinessHouseNo() {
		return this.businessHouseNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate == null ? null : purchaseDate.trim();
	}
	
    /**
     * @return PurchaseDate
     */	
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return PurchaseAccnt
     */	
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param applyForHouse
	 */
	public void setApplyForHouse(String applyForHouse) {
		this.applyForHouse = applyForHouse == null ? null : applyForHouse.trim();
	}
	
    /**
     * @return ApplyForHouse
     */	
	public String getApplyForHouse() {
		return this.applyForHouse;
	}
	
	/**
	 * @param houseOwnershipInd
	 */
	public void setHouseOwnershipInd(String houseOwnershipInd) {
		this.houseOwnershipInd = houseOwnershipInd == null ? null : houseOwnershipInd.trim();
	}
	
    /**
     * @return HouseOwnershipInd
     */	
	public String getHouseOwnershipInd() {
		return this.houseOwnershipInd;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(String buildArea) {
		this.buildArea = buildArea == null ? null : buildArea.trim();
	}
	
    /**
     * @return BuildArea
     */	
	public String getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param activateYears
	 */
	public void setActivateYears(String activateYears) {
		this.activateYears = activateYears == null ? null : activateYears.trim();
	}
	
    /**
     * @return ActivateYears
     */	
	public String getActivateYears() {
		return this.activateYears;
	}
	
	/**
	 * @param housePrDesc
	 */
	public void setHousePrDesc(String housePrDesc) {
		this.housePrDesc = housePrDesc == null ? null : housePrDesc.trim();
	}
	
    /**
     * @return HousePrDesc
     */	
	public String getHousePrDesc() {
		return this.housePrDesc;
	}
	
	/**
	 * @param floorAge
	 */
	public void setFloorAge(String floorAge) {
		this.floorAge = floorAge == null ? null : floorAge.trim();
	}
	
    /**
     * @return FloorAge
     */	
	public String getFloorAge() {
		return this.floorAge;
	}
	
	/**
	 * @param orientations
	 */
	public void setOrientations(String orientations) {
		this.orientations = orientations == null ? null : orientations.trim();
	}
	
    /**
     * @return Orientations
     */	
	public String getOrientations() {
		return this.orientations;
	}
	
	/**
	 * @param houseStructure
	 */
	public void setHouseStructure(String houseStructure) {
		this.houseStructure = houseStructure == null ? null : houseStructure.trim();
	}
	
    /**
     * @return HouseStructure
     */	
	public String getHouseStructure() {
		return this.houseStructure;
	}
	
	/**
	 * @param groundStructure
	 */
	public void setGroundStructure(String groundStructure) {
		this.groundStructure = groundStructure == null ? null : groundStructure.trim();
	}
	
    /**
     * @return GroundStructure
     */	
	public String getGroundStructure() {
		return this.groundStructure;
	}
	
	/**
	 * @param roofStructure
	 */
	public void setRoofStructure(String roofStructure) {
		this.roofStructure = roofStructure == null ? null : roofStructure.trim();
	}
	
    /**
     * @return RoofStructure
     */	
	public String getRoofStructure() {
		return this.roofStructure;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd == null ? null : provinceCd.trim();
	}
	
    /**
     * @return ProvinceCd
     */	
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd == null ? null : cityCd.trim();
	}
	
    /**
     * @return CityCd
     */	
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd == null ? null : countyCd.trim();
	}
	
    /**
     * @return CountyCd
     */	
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street == null ? null : street.trim();
	}
	
    /**
     * @return Street
     */	
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param houseNo
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo == null ? null : houseNo.trim();
	}
	
    /**
     * @return HouseNo
     */	
	public String getHouseNo() {
		return this.houseNo;
	}
	
	/**
	 * @param buildingRoomNum
	 */
	public void setBuildingRoomNum(String buildingRoomNum) {
		this.buildingRoomNum = buildingRoomNum == null ? null : buildingRoomNum.trim();
	}
	
    /**
     * @return BuildingRoomNum
     */	
	public String getBuildingRoomNum() {
		return this.buildingRoomNum;
	}
	
	/**
	 * @param roomNum
	 */
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum == null ? null : roomNum.trim();
	}
	
    /**
     * @return RoomNum
     */	
	public String getRoomNum() {
		return this.roomNum;
	}
	
	/**
	 * @param pocAddr
	 */
	public void setPocAddr(String pocAddr) {
		this.pocAddr = pocAddr == null ? null : pocAddr.trim();
	}
	
    /**
     * @return PocAddr
     */	
	public String getPocAddr() {
		return this.pocAddr;
	}
	
	/**
	 * @param communityName
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName == null ? null : communityName.trim();
	}
	
    /**
     * @return CommunityName
     */	
	public String getCommunityName() {
		return this.communityName;
	}
	
	/**
	 * @param bdlc
	 */
	public void setBdlc(String bdlc) {
		this.bdlc = bdlc == null ? null : bdlc.trim();
	}
	
    /**
     * @return Bdlc
     */	
	public String getBdlc() {
		return this.bdlc;
	}
	
	/**
	 * @param bdgd
	 */
	public void setBdgd(String bdgd) {
		this.bdgd = bdgd == null ? null : bdgd.trim();
	}
	
    /**
     * @return Bdgd
     */	
	public String getBdgd() {
		return this.bdgd;
	}
	
	/**
	 * @param housePlaceInfo
	 */
	public void setHousePlaceInfo(String housePlaceInfo) {
		this.housePlaceInfo = housePlaceInfo == null ? null : housePlaceInfo.trim();
	}
	
    /**
     * @return HousePlaceInfo
     */	
	public String getHousePlaceInfo() {
		return this.housePlaceInfo;
	}
	
	/**
	 * @param buildDesc
	 */
	public void setBuildDesc(String buildDesc) {
		this.buildDesc = buildDesc == null ? null : buildDesc.trim();
	}
	
    /**
     * @return BuildDesc
     */	
	public String getBuildDesc() {
		return this.buildDesc;
	}
	
	/**
	 * @param fullLand
	 */
	public void setFullLand(String fullLand) {
		this.fullLand = fullLand == null ? null : fullLand.trim();
	}
	
    /**
     * @return FullLand
     */	
	public String getFullLand() {
		return this.fullLand;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo == null ? null : landNo.trim();
	}
	
    /**
     * @return LandNo
     */	
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual == null ? null : landUseQual.trim();
	}
	
    /**
     * @return LandUseQual
     */	
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay == null ? null : landUseWay.trim();
	}
	
    /**
     * @return LandUseWay
     */	
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate == null ? null : landUseBeginDate.trim();
	}
	
    /**
     * @return LandUseBeginDate
     */	
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate == null ? null : landUseEndDate.trim();
	}
	
    /**
     * @return LandUseEndDate
     */	
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(String landUseYears) {
		this.landUseYears = landUseYears == null ? null : landUseYears.trim();
	}
	
    /**
     * @return LandUseYears
     */	
	public String getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp == null ? null : landPurp.trim();
	}
	
    /**
     * @return LandPurp
     */	
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain == null ? null : landExplain.trim();
	}
	
    /**
     * @return LandExplain
     */	
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param landUseArea
	 */
	public void setLandUseArea(String landUseArea) {
		this.landUseArea = landUseArea == null ? null : landUseArea.trim();
	}
	
    /**
     * @return LandUseArea
     */	
	public String getLandUseArea() {
		return this.landUseArea;
	}
	
	/**
	 * @param landUseUnit
	 */
	public void setLandUseUnit(String landUseUnit) {
		this.landUseUnit = landUseUnit == null ? null : landUseUnit.trim();
	}
	
    /**
     * @return LandUseUnit
     */	
	public String getLandUseUnit() {
		return this.landUseUnit;
	}
	
	/**
	 * @param realproCls
	 */
	public void setRealproCls(String realproCls) {
		this.realproCls = realproCls == null ? null : realproCls.trim();
	}
	
    /**
     * @return RealproCls
     */	
	public String getRealproCls() {
		return this.realproCls;
	}
	
	/**
	 * @param houseUseType
	 */
	public void setHouseUseType(String houseUseType) {
		this.houseUseType = houseUseType == null ? null : houseUseType.trim();
	}
	
    /**
     * @return HouseUseType
     */	
	public String getHouseUseType() {
		return this.houseUseType;
	}
	
	/**
	 * @param lesseeName
	 */
	public void setLesseeName(String lesseeName) {
		this.lesseeName = lesseeName == null ? null : lesseeName.trim();
	}
	
    /**
     * @return LesseeName
     */	
	public String getLesseeName() {
		return this.lesseeName;
	}
	
	/**
	 * @param annualRent
	 */
	public void setAnnualRent(java.math.BigDecimal annualRent) {
		this.annualRent = annualRent;
	}
	
    /**
     * @return AnnualRent
     */	
	public java.math.BigDecimal getAnnualRent() {
		return this.annualRent;
	}
	
	/**
	 * @param lease
	 */
	public void setLease(String lease) {
		this.lease = lease == null ? null : lease.trim();
	}
	
    /**
     * @return Lease
     */	
	public String getLease() {
		return this.lease;
	}
	
	/**
	 * @param leftLease
	 */
	public void setLeftLease(String leftLease) {
		this.leftLease = leftLease == null ? null : leftLease.trim();
	}
	
    /**
     * @return LeftLease
     */	
	public String getLeftLease() {
		return this.leftLease;
	}
	
	/**
	 * @param estateAcquireWay
	 */
	public void setEstateAcquireWay(String estateAcquireWay) {
		this.estateAcquireWay = estateAcquireWay == null ? null : estateAcquireWay.trim();
	}
	
    /**
     * @return EstateAcquireWay
     */	
	public String getEstateAcquireWay() {
		return this.estateAcquireWay;
	}
	
	/**
	 * @param inUseYear
	 */
	public void setInUseYear(String inUseYear) {
		this.inUseYear = inUseYear == null ? null : inUseYear.trim();
	}
	
    /**
     * @return InUseYear
     */	
	public String getInUseYear() {
		return this.inUseYear;
	}
	
	/**
	 * @param belongArea
	 */
	public void setBelongArea(String belongArea) {
		this.belongArea = belongArea == null ? null : belongArea.trim();
	}
	
    /**
     * @return BelongArea
     */	
	public String getBelongArea() {
		return this.belongArea;
	}
	
	/**
	 * @param wallStructure
	 */
	public void setWallStructure(String wallStructure) {
		this.wallStructure = wallStructure == null ? null : wallStructure.trim();
	}
	
    /**
     * @return WallStructure
     */	
	public String getWallStructure() {
		return this.wallStructure;
	}
	
	/**
	 * @param propertyCase
	 */
	public void setPropertyCase(String propertyCase) {
		this.propertyCase = propertyCase == null ? null : propertyCase.trim();
	}
	
    /**
     * @return PropertyCase
     */	
	public String getPropertyCase() {
		return this.propertyCase;
	}
	
	/**
	 * @param houseProperty
	 */
	public void setHouseProperty(String houseProperty) {
		this.houseProperty = houseProperty == null ? null : houseProperty.trim();
	}
	
    /**
     * @return HouseProperty
     */	
	public String getHouseProperty() {
		return this.houseProperty;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return InputDate
     */	
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return UpdDate
     */	
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}


}