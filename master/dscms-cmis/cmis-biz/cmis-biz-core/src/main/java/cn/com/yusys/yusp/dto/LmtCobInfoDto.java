package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCobInfo
 * @类描述: lmt_cob_info数据实体类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-04-25 19:49:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtCobInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 共借人姓名 **/
	private String commonDebitCusName;
	
	/** 共借人证件号码 **/
	private String commonDebitCertCode;
	
	/** 共借人客户编号 **/
	private String commonDebitCusId;
	
	/** 共借人手机 **/
	private String commonDebitMobile;
	
	/** 共借人地址 **/
	private String commonDebitAddr;
	
	/** 共借人关系 **/
	private String commonDebitRela;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param commonDebitCusName
	 */
	public void setCommonDebitCusName(String commonDebitCusName) {
		this.commonDebitCusName = commonDebitCusName == null ? null : commonDebitCusName.trim();
	}
	
    /**
     * @return CommonDebitCusName
     */	
	public String getCommonDebitCusName() {
		return this.commonDebitCusName;
	}
	
	/**
	 * @param commonDebitCertCode
	 */
	public void setCommonDebitCertCode(String commonDebitCertCode) {
		this.commonDebitCertCode = commonDebitCertCode == null ? null : commonDebitCertCode.trim();
	}
	
    /**
     * @return CommonDebitCertCode
     */	
	public String getCommonDebitCertCode() {
		return this.commonDebitCertCode;
	}
	
	/**
	 * @param commonDebitCusId
	 */
	public void setCommonDebitCusId(String commonDebitCusId) {
		this.commonDebitCusId = commonDebitCusId == null ? null : commonDebitCusId.trim();
	}
	
    /**
     * @return CommonDebitCusId
     */	
	public String getCommonDebitCusId() {
		return this.commonDebitCusId;
	}
	
	/**
	 * @param commonDebitMobile
	 */
	public void setCommonDebitMobile(String commonDebitMobile) {
		this.commonDebitMobile = commonDebitMobile == null ? null : commonDebitMobile.trim();
	}
	
    /**
     * @return CommonDebitMobile
     */	
	public String getCommonDebitMobile() {
		return this.commonDebitMobile;
	}
	
	/**
	 * @param commonDebitAddr
	 */
	public void setCommonDebitAddr(String commonDebitAddr) {
		this.commonDebitAddr = commonDebitAddr == null ? null : commonDebitAddr.trim();
	}
	
    /**
     * @return CommonDebitAddr
     */	
	public String getCommonDebitAddr() {
		return this.commonDebitAddr;
	}
	
	/**
	 * @param commonDebitRela
	 */
	public void setCommonDebitRela(String commonDebitRela) {
		this.commonDebitRela = commonDebitRela == null ? null : commonDebitRela.trim();
	}
	
    /**
     * @return CommonDebitRela
     */	
	public String getCommonDebitRela() {
		return this.commonDebitRela;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}