/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayPlanChg
 * @类描述: iqp_repay_plan_chg数据实体类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-27 21:32:04
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_repay_plan_chg")
public class IqpRepayPlanChg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "IQP_SERNO")
    private String iqpSerno;

    /**
     * 合同编号
     **/
    @Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
    private String contNo;

    /**
     * 借据编号
     **/
    @Column(name = "BILL_NO", unique = false, nullable = false, length = 40)
    private String billNo;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 币种 STD_ZB_CUR_TYP
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 贷款金额
     **/
    @Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
    private Double loanAmt;

    /**
     * 贷款余额
     **/
    @Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
    private Double loanBalance;

    /**
     * 发放日期
     **/
    @Column(name = "DISTR_DATE", unique = false, nullable = true, length = 10)
    private String distrDate;

    /**
     * 到期日期
     **/
    @Column(name = "END_DATE", unique = false, nullable = true, length = 10)
    private String endDate;

    /**
     * 调整原因
     **/
    @Column(name = "CHANGE_RESN", unique = false, nullable = true, length = 250)
    private String changeResn;

    /**
     * 主办人
     **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 主办机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 申请状态 STD_ZB_APP_ST
     **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 原还款日确定规则 STD_ZB_REPAY_RULE
     **/
    @Column(name = "OLD_REPAY_RULE", unique = false, nullable = true, length = 255)
    private String oldRepayRule;

    /**
     * 原还款日类型 STD_ZB_REPAY_DT_TYPE
     **/
    @Column(name = "OLD_REPAY_DT_TYPE", unique = false, nullable = true, length = 255)
    private String oldRepayDtType;


    /**
     * @param iqpSerno
     */
    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    /**
     * @return iqpSerno
     */
    public String getIqpSerno() {
        return this.iqpSerno;
    }

    /**
     * @param contNo
     */
    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    /**
     * @return contNo
     */
    public String getContNo() {
        return this.contNo;
    }

    /**
     * @param billNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * @return billNo
     */
    public String getBillNo() {
        return this.billNo;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param loanAmt
     */
    public void setLoanAmt(Double loanAmt) {
        this.loanAmt = loanAmt;
    }

    /**
     * @return loanAmt
     */
    public Double getLoanAmt() {
        return this.loanAmt;
    }

    /**
     * @param loanBalance
     */
    public void setLoanBalance(Double loanBalance) {
        this.loanBalance = loanBalance;
    }

    /**
     * @return loanBalance
     */
    public Double getLoanBalance() {
        return this.loanBalance;
    }

    /**
     * @param distrDate
     */
    public void setDistrDate(String distrDate) {
        this.distrDate = distrDate;
    }

    /**
     * @return distrDate
     */
    public String getDistrDate() {
        return this.distrDate;
    }

    /**
     * @param endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return endDate
     */
    public String getEndDate() {
        return this.endDate;
    }

    /**
     * @param changeResn
     */
    public void setChangeResn(String changeResn) {
        this.changeResn = changeResn;
    }

    /**
     * @return changeResn
     */
    public String getChangeResn() {
        return this.changeResn;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param approveStatus
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    /**
     * @return approveStatus
     */
    public String getApproveStatus() {
        return this.approveStatus;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oldRepayRule
     */
    public void setOldRepayRule(String oldRepayRule) {
        this.oldRepayRule = oldRepayRule;
    }

    /**
     * @return oldRepayRule
     */
    public String getOldRepayRule() {
        return this.oldRepayRule;
    }

    /**
     * @param oldRepayDtType
     */
    public void setOldRepayDtType(String oldRepayDtType) {
        this.oldRepayDtType = oldRepayDtType;
    }

    /**
     * @return oldRepayDtType
     */
    public String getOldRepayDtType() {
        return this.oldRepayDtType;
    }


}