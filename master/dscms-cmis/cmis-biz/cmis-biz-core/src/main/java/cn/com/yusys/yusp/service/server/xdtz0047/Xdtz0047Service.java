package cn.com.yusys.yusp.service.server.xdtz0047;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0047.req.Xdtz0047DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0047.resp.Xdtz0047DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 业务逻辑类:商贷分户实时查询
 *
 * @author chenyong
 * @version 1.0
 */
@Service
public class Xdtz0047Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0047Service.class);
    @Resource
    private AccLoanMapper accLoanMapper;
    @Autowired
    private CommonService commonService;

    public Xdtz0047DataRespDto xdtz0047(Xdtz0047DataReqDto xdtz0047DataReqDto) throws Exception {

        Xdtz0047DataRespDto xdtz0047DataRespDto = new Xdtz0047DataRespDto();
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key,
                DscmsEnum.TRADE_CODE_XDTZ0011.value, JSON.toJSONString(xdtz0047DataReqDto));
        try {
            List<cn.com.yusys.yusp.dto.server.xdtz0047.resp.List> lists = null;
            String query_type = xdtz0047DataReqDto.getQuery_type();//查询类型
            String cert_code = xdtz0047DataReqDto.getCert_code();//证件号
            String cus_id = xdtz0047DataReqDto.getCus_id();//客户编号
            int record = 0;//查询结果
            //查询参数
            logger.info("借据信息,查询参数为:{}", xdtz0047DataReqDto);
            //query_type 01-按个人查询, 02-按公司查询
            if (query_type.equals("01")) {//01-按个人查询
                //根据(证件号：cert_code)从cus服务查询（客户编号:cus_id)
                List<String> cusBaseByCertCodeList = new ArrayList<>();
                StringBuilder appendCusId = new StringBuilder();
                String cusId = "";//最终拼接cusid 以逗号拼接
                if (cert_code.length() > 0 && cert_code.indexOf(",") > 0) {
                    String[] splitStr = cert_code.split(",");
                    for (int i = 0; i < splitStr.length; i++) {
                        cusBaseByCertCodeList = commonService.getCusBaseByCertCode(splitStr[i]);
                        if (CollectionUtils.nonEmpty(cusBaseByCertCodeList)) {
                            appendCusId.append(cusBaseByCertCodeList.get(0)).append(",");
                        }
                    }
                    cusId = appendCusId.substring(0, appendCusId.toString().length() - 1).toString();
                } else {
                    cusBaseByCertCodeList = commonService.getCusBaseByCertCode(cert_code);
                    if (CollectionUtils.nonEmpty(cusBaseByCertCodeList)) {
                        cusId = appendCusId.append(cusBaseByCertCodeList.get(0)).toString();
                    }
                }

                if (cusId.length() > 0) {
                    lists = accLoanMapper.selectForAccLoanInfoByCusId(cusId);
                } else {
                    logger.info("***************未查询到cusId*****************");
                }

            } else if (query_type.equals("02")) {// 02-按公司查询
                lists = accLoanMapper.selectForAccLoanInfoByCusId(cus_id);
                logger.info("***********" + lists.toString() + "*********");
            } else {
                logger.info("************无此类型查询query_type：" + query_type + "*****************");
            }
            xdtz0047DataRespDto.setList(lists);
            //查询参数
            logger.info("借据信息查询（按证件号）,查询参数为:{}", JSON.toJSONString(xdtz0047DataReqDto));
            logger.info("借据信息查询（按证件号）,反回参数为:{}", JSON.toJSONString(xdtz0047DataRespDto));
        } catch (BizException e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, JSON.toJSONString(xdtz0047DataRespDto));
        return xdtz0047DataRespDto;
    }

}
