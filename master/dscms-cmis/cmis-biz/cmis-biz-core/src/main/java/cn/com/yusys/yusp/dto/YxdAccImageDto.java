package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: YxdAccImage
 * @类描述: yxd_acc_image数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-09 13:50:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class YxdAccImageDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 流水号 **/
	private String surveySerno;
	
	/** 借据影像 **/
	private String accImageno;
	
	/** 额度支用授权书 **/
	private String accAuthno;
	
	/** 贷款用途声明书 **/
	private String accLoanDirect;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param accImageno
	 */
	public void setAccImageno(String accImageno) {
		this.accImageno = accImageno == null ? null : accImageno.trim();
	}
	
    /**
     * @return AccImageno
     */	
	public String getAccImageno() {
		return this.accImageno;
	}
	
	/**
	 * @param accAuthno
	 */
	public void setAccAuthno(String accAuthno) {
		this.accAuthno = accAuthno == null ? null : accAuthno.trim();
	}
	
    /**
     * @return AccAuthno
     */	
	public String getAccAuthno() {
		return this.accAuthno;
	}
	
	/**
	 * @param accLoanDirect
	 */
	public void setAccLoanDirect(String accLoanDirect) {
		this.accLoanDirect = accLoanDirect == null ? null : accLoanDirect.trim();
	}
	
    /**
     * @return AccLoanDirect
     */	
	public String getAccLoanDirect() {
		return this.accLoanDirect;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}