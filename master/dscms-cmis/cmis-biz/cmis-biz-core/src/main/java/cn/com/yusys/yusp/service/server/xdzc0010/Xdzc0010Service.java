package cn.com.yusys.yusp.service.server.xdzc0010;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.OpRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0010.req.Xdzc0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0010.resp.Xdzc0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.CtrAsplDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 接口处理类:资产池超短贷放款校验
 *
 * @Author xs
 * @Date 2021/10/03 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0010Service {

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;


    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0010.Xdzc0010Service.class);

    /**
     * 交易码：xdzc0010
     * 交易描述：
     * ►资产池协议状态为有效；
     * ►（本次出账金额+资产池下超短贷余额）<= 资产池下超短贷额度；
     * ►本次出账期限<=6个月，且到期日<=资产池协议到期日 + 授信批复宽限期；
     * ►为控制贷款规模，禁止在月末最后2个工作日（支持可配置工作日时间）发起超短贷提款；
     * 受托信息为必填项
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0010DataRespDto xdzc0010Service(Xdzc0010DataReqDto xdzc0010DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value);
        Xdzc0010DataRespDto xdzc0010DataRespDto = new Xdzc0010DataRespDto();
        xdzc0010DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
        xdzc0010DataRespDto.setOpMsg("出账校验逻辑报错");// 描述信息
        String contNo = xdzc0010DataReqDto.getContNo();
        BigDecimal sumAmt = xdzc0010DataReqDto.getAppAmt();
        int loanTerm = xdzc0010DataReqDto.getLoanTerm();// 出账期限（月）
        String endDate = xdzc0010DataReqDto.getEndDate();// 贷款到期日
        String sartDate =  xdzc0010DataReqDto.getStartDate();// 贷款起始日

        try {
            logger.info("资产池超短贷款出账校验开始");
            OpRespDto opRespDto = ctrAsplDetailsService.isPvpLoanApp(contNo,sumAmt,endDate,loanTerm);
            logger.info("资产池超短贷款出账校验结束");
            xdzc0010DataRespDto.setOpFlag(opRespDto.getOpFlag());// 成功失败标志
            xdzc0010DataRespDto.setOpMsg(opRespDto.getOpMsg());// 描述信息
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdzc0010DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            xdzc0010DataRespDto.setOpMsg("资产池超短贷放款校验,逻辑错误");// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value);
        return xdzc0010DataRespDto;
    }


    /**
     * 工作日计算计算两个日期之间的工作日
     * @Author xs
     * @Date 2021/10/03 20:20
     * @Version 1.0
    */
    public int getWorkDays(Date dateOne, Date dateTwo){

        return 0;
    }
}