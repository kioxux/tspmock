/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ZxdPreLmtApply;
import cn.com.yusys.yusp.service.ZxdPreLmtApplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: ZxdPreLmtApplyResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: wangqing
 * @创建时间: 2021-08-06 16:49:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "征信贷预授信评分卡")
@RequestMapping("/api/zxdprelmtapply")
public class ZxdPreLmtApplyResource {

    @Autowired
    private ZxdPreLmtApplyService zxdPreLmtApplyService;

    /**
     * @函数名称:selectByPrimaryKey
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbyprimarykey/{corpId}")
    protected ResultDto<Map> selectByPrimaryKey(@PathVariable("corpId") String corpId) {
        Map<String, String> map = zxdPreLmtApplyService.selectByModel(corpId);
        return new ResultDto<>(map);
    }

    /**
     * @函数名称:queryZxdPreLmtApplyList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     * @创建者：zhangliang15
     */
    @ApiOperation(value = "征信贷预授信评分卡列表查询")
    @PostMapping("/queryZxdPreLmtApplyList")
    protected ResultDto<List<ZxdPreLmtApply>> queryVisaXdRiskList(@RequestBody QueryModel queryModel) {
        List<ZxdPreLmtApply> list = zxdPreLmtApplyService.queryZxdPreLmtApplyList(queryModel);
        return new ResultDto<List<ZxdPreLmtApply>>(list);
    }

    /**
     * @方法名称: queryZxdPreLmtApplyByParams
     * @方法描述: 根据入参查询房屋信息数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("根据入参查询数据")
    @PostMapping("/queryZxdPreLmtApplyByParams")
    protected ResultDto<ZxdPreLmtApply> queryZxdPreLmtApplyByParams(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("applyRecordId",(String)map.get("applyRecordId"));
        ZxdPreLmtApply zxdPreLmtApply = zxdPreLmtApplyService.queryZxdPreLmtApplyByParams(queryData);
        return new ResultDto<ZxdPreLmtApply>(zxdPreLmtApply);
    }

    /**
     * @函数名称:ZXDSendToWx
     * @函数描述:征信贷预授信评分卡手动准入
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("征信贷预授信评分卡手动准入")
    @PostMapping("/ZXDSendToWx")
    protected ResultDto<Map> ZXDSendToWx(@RequestBody ZxdPreLmtApply zxdPreLmtApply) throws URISyntaxException {
        Map rtnData = zxdPreLmtApplyService.ZXDSendToWx(zxdPreLmtApply);
        return new ResultDto<>(rtnData);
    }
    /**
     * @函数名称:ZXDSendToWxpRefuse
     * @函数描述:征信贷预授信评分卡手动拒绝
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("征信贷预授信评分卡手动拒绝")
    @PostMapping("/ZXDSendToWxpRefuse")
    protected ResultDto<Map> ZXDSendToWxpRefuse(@RequestBody ZxdPreLmtApply zxdPreLmtApply) throws URISyntaxException {
        Map rtnData = zxdPreLmtApplyService.ZXDSendToWxpRefuse(zxdPreLmtApply);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称: queryZxdPreLmtApply
     * @函数描述:查询对象列表
     * @创建者: zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryZxdPreLmtApply")
    protected ResultDto<List<ZxdPreLmtApply>> queryZxdPreLmtApply(@RequestBody QueryModel queryModel) {
        List<ZxdPreLmtApply> list = zxdPreLmtApplyService.queryZxdPreLmtApply(queryModel);
        return new ResultDto<List<ZxdPreLmtApply>>(list);
    }
}
