package cn.com.yusys.yusp.service.server.xdxw0030;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0030.req.Xdxw0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0030.resp.Xdxw0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtGuareCloestInfoMapper;
import cn.com.yusys.yusp.service.server.xdxw0027.Xdxw0027Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0030Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-17 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0030Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0027Service.class);

    @Resource
    private LmtGuareCloestInfoMapper lmtGuareCloestInfoMapper;

    /**
     * 根据云估计流水号查询房屋信息
     *
     * @param xdxw0030DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0030DataRespDto getXdxw0030(Xdxw0030DataReqDto xdxw0030DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, JSON.toJSONString(xdxw0030DataReqDto));
        Xdxw0030DataRespDto xdxw0030DataRespDto = new Xdxw0030DataRespDto();
        try {
            java.util.List<cn.com.yusys.yusp.dto.server.xdxw0030.resp.List> listResp = new ArrayList<>();
            Map param = new HashMap<>();
            //查询类型
            String queryType = xdxw0030DataReqDto.getQueryType();
            if (Objects.equals(CommonConstance.QUERY_TYPE_01, queryType)) {
                //云估计流水号
                String ygjSerno = xdxw0030DataReqDto.getYgjSerno();
                if (StringUtils.isEmpty(ygjSerno)) {
                    throw BizException.error(null, EcbEnum.ECB010009.key, EcbEnum.ECB010009.value);
                }
                param.put("ygjSerno", ygjSerno);
            } else if (Objects.equals(CommonConstance.QUERY_TYPE_02, queryType)) {
                //查询人员ID
                String qryId = xdxw0030DataReqDto.getQryId();
                //电话
                String phone = xdxw0030DataReqDto.getPhone();
                if (StringUtils.isEmpty(qryId)) {
                    throw BizException.error(null, EcbEnum.ECB010022.key, EcbEnum.ECB010022.value);
                }
                if (StringUtils.isEmpty(phone)) {
                    throw BizException.error(null, EcbEnum.ECB010023.key, EcbEnum.ECB010023.value);
                }
                param.put("qryId", qryId);
                param.put("phone", phone);//2021-07-15与王洪林确认：电话号码用or条件查询（即查询当前号码下所有记录）
            } else {
                throw BizException.error(null, EcbEnum.ECB010013.key, EcbEnum.ECB010013.value);
            }
            if (!param.isEmpty()) {
                listResp = lmtGuareCloestInfoMapper.selectBySernoList(param);
            }

            xdxw0030DataRespDto.setList(listResp);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, JSON.toJSONString(xdxw0030DataRespDto));
        return xdxw0030DataRespDto;
    }

}
