/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppSubPrd
 * @类描述: lmt_app_sub_prd数据实体类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-19 21:43:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_app_sub_prd")
public class LmtAppSubPrdDto extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 分项适用产品流水号 **/
	@Column(name = "SUB_PRD_SERNO", unique = false, nullable = true, length = 40)
	private String subPrdSerno;

	/** 授信分项流水号 **/
	@Column(name = "SUB_SERNO", unique = false, nullable = true, length = 40)
	private String subSerno;

	/** 授信品种编号 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 40)
	private String bizType;

	/** 授信品种名称 **/
	@Column(name = "BIZ_TYPE_NAME", unique = false, nullable = true, length = 80)
	private String bizTypeName;

	/** 是否循环额度 **/
	@Column(name = "IS_REVOLV_LIMIT", unique = false, nullable = true, length = 5)
	private String isRevolvLimit;

	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 255)
	private String cusName;

	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = false, length = 5)
	private String guarMode;

	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 授信额度 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;

	/** 授信起始日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;

	/** 授信到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;

	/** 额度期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;

	/** 宽限期 **/
	@Column(name = "GRAPER", unique = false, nullable = true, length = 10)
	private Integer graper;

	/** 原额度分项编号 **/
	@Column(name = "ORIGI_LMT_ACC_SUB_PRD_NO", unique = false, nullable = true, length = 40)
	private String origiLmtAccSubPrdNo;

	/** 原额度分项金额 **/
	@Column(name = "ORIGI_LMT_ACC_SUB_PRD_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiLmtAccSubPrdAmt;

	/** 原额度分项期限 **/
	@Column(name = "ORIGI_LMT_ACC_SUB_PRD_TERM", unique = false, nullable = true, length = 10)
	private Integer origiLmtAccSubPrdTerm;

	/** 变更标志 **/
	@Column(name = "CHG_FLAG", unique = false, nullable = true, length = 5)
	private String chgFlag;

	/** 是否预授信额度 **/
	@Column(name = "IS_PRE_LMT", unique = false, nullable = true, length = 40)
	private String isPreLmt;

	/** 调剂标志 **/
	@Column(name = "ADJUST_FLAG", unique = false, nullable = true, length = 5)
	private String adjustFlag;

	/** 是否本次细化 **/
	@Column(name = "IS_CURT_REFINE", unique = false, nullable = true, length = 5)
	private String isCurtRefine;

	/** 是否存量授信标志 **/
	@Column(name = "IS_SFCA_LMT", unique = false, nullable = true, length = 5)
	private String isSfcaLmt;

	/** 保证金预留比例 **/
	@Column(name = "BAIL_PRE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPreRate;

	/** 年利率 **/
	@Column(name = "RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rateYear;

	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;

	/** 结息方式 **/
	@Column(name = "EI_MODE", unique = false, nullable = true, length = 5)
	private String eiMode;

	/** 是否借新还旧 **/
	@Column(name = "IS_REFINANCE", unique = false, nullable = true, length = 5)
	private String isRefinance;

	/** 是否无还本续贷 **/
	@Column(name = "IS_RWROP", unique = false, nullable = true, length = 5)
	private String isRwrop;

	/** 手续费率 **/
	@Column(name = "CHRG_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgRate;

	/** 手续费收取方式 **/
	@Column(name = "CHRG_COLLECT_MODE", unique = false, nullable = true, length = 5)
	private String chrgCollectMode;

	/** 委托人类型 **/
	@Column(name = "CONSIGNOR_TYPE", unique = false, nullable = true, length = 5)
	private String consignorType;

	/** 委托人客户号 **/
	@Column(name = "CONSIGNOR_CUS_ID", unique = false, nullable = true, length = 40)
	private String consignorCusId;

	/** 还款计划描述 **/
	@Column(name = "REPAY_PLAN_DESC", unique = false, nullable = true, length = 4000)
	private String repayPlanDesc;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = false, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = false, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = false, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = false, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = false, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = false, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 19)
	private Date updateTime;




	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param subPrdSerno
	 */
	public void setSubPrdSerno(String subPrdSerno) {
		this.subPrdSerno = subPrdSerno;
	}

	/**
	 * @return subPrdSerno
	 */
	public String getSubPrdSerno() {
		return this.subPrdSerno;
	}

	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno;
	}

	/**
	 * @return subSerno
	 */
	public String getSubSerno() {
		return this.subSerno;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	/**
	 * @return bizType
	 */
	public String getBizType() {
		return this.bizType;
	}

	/**
	 * @param bizTypeName
	 */
	public void setBizTypeName(String bizTypeName) {
		this.bizTypeName = bizTypeName;
	}

	/**
	 * @return bizTypeName
	 */
	public String getBizTypeName() {
		return this.bizTypeName;
	}

	/**
	 * @param isRevolvLimit
	 */
	public void setIsRevolvLimit(String isRevolvLimit) {
		this.isRevolvLimit = isRevolvLimit;
	}

	/**
	 * @return isRevolvLimit
	 */
	public String getIsRevolvLimit() {
		return this.isRevolvLimit;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	/**
	 * @return guarMode
	 */
	public String getGuarMode() {
		return this.guarMode;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

	/**
	 * @return curType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}

	/**
	 * @return lmtAmt
	 */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}

	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return startDate
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return endDate
	 */
	public String getEndDate() {
		return this.endDate;
	}

	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}

	/**
	 * @return lmtTerm
	 */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}

	/**
	 * @param graper
	 */
	public void setGraper(Integer graper) {
		this.graper = graper;
	}

	/**
	 * @return graper
	 */
	public Integer getGraper() {
		return this.graper;
	}

	/**
	 * @param origiLmtAccSubPrdNo
	 */
	public void setOrigiLmtAccSubPrdNo(String origiLmtAccSubPrdNo) {
		this.origiLmtAccSubPrdNo = origiLmtAccSubPrdNo;
	}

	/**
	 * @return origiLmtAccSubPrdNo
	 */
	public String getOrigiLmtAccSubPrdNo() {
		return this.origiLmtAccSubPrdNo;
	}

	/**
	 * @param origiLmtAccSubPrdAmt
	 */
	public void setOrigiLmtAccSubPrdAmt(java.math.BigDecimal origiLmtAccSubPrdAmt) {
		this.origiLmtAccSubPrdAmt = origiLmtAccSubPrdAmt;
	}

	/**
	 * @return origiLmtAccSubPrdAmt
	 */
	public java.math.BigDecimal getOrigiLmtAccSubPrdAmt() {
		return this.origiLmtAccSubPrdAmt;
	}

	/**
	 * @param origiLmtAccSubPrdTerm
	 */
	public void setOrigiLmtAccSubPrdTerm(Integer origiLmtAccSubPrdTerm) {
		this.origiLmtAccSubPrdTerm = origiLmtAccSubPrdTerm;
	}

	/**
	 * @return origiLmtAccSubPrdTerm
	 */
	public Integer getOrigiLmtAccSubPrdTerm() {
		return this.origiLmtAccSubPrdTerm;
	}

	/**
	 * @param chgFlag
	 */
	public void setChgFlag(String chgFlag) {
		this.chgFlag = chgFlag;
	}

	/**
	 * @return chgFlag
	 */
	public String getChgFlag() {
		return this.chgFlag;
	}

	/**
	 * @param isPreLmt
	 */
	public void setIsPreLmt(String isPreLmt) {
		this.isPreLmt = isPreLmt;
	}

	/**
	 * @return isPreLmt
	 */
	public String getIsPreLmt() {
		return this.isPreLmt;
	}

	/**
	 * @param adjustFlag
	 */
	public void setAdjustFlag(String adjustFlag) {
		this.adjustFlag = adjustFlag;
	}

	/**
	 * @return adjustFlag
	 */
	public String getAdjustFlag() {
		return this.adjustFlag;
	}

	/**
	 * @param isCurtRefine
	 */
	public void setIsCurtRefine(String isCurtRefine) {
		this.isCurtRefine = isCurtRefine;
	}

	/**
	 * @return isCurtRefine
	 */
	public String getIsCurtRefine() {
		return this.isCurtRefine;
	}

	/**
	 * @param isSfcaLmt
	 */
	public void setIsSfcaLmt(String isSfcaLmt) {
		this.isSfcaLmt = isSfcaLmt;
	}

	/**
	 * @return isSfcaLmt
	 */
	public String getIsSfcaLmt() {
		return this.isSfcaLmt;
	}

	/**
	 * @param bailPreRate
	 */
	public void setBailPreRate(java.math.BigDecimal bailPreRate) {
		this.bailPreRate = bailPreRate;
	}

	/**
	 * @return bailPreRate
	 */
	public java.math.BigDecimal getBailPreRate() {
		return this.bailPreRate;
	}

	/**
	 * @param rateYear
	 */
	public void setRateYear(java.math.BigDecimal rateYear) {
		this.rateYear = rateYear;
	}

	/**
	 * @return rateYear
	 */
	public java.math.BigDecimal getRateYear() {
		return this.rateYear;
	}

	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}

	/**
	 * @return repayMode
	 */
	public String getRepayMode() {
		return this.repayMode;
	}

	/**
	 * @param eiMode
	 */
	public void setEiMode(String eiMode) {
		this.eiMode = eiMode;
	}

	/**
	 * @return eiMode
	 */
	public String getEiMode() {
		return this.eiMode;
	}

	/**
	 * @param isRefinance
	 */
	public void setIsRefinance(String isRefinance) {
		this.isRefinance = isRefinance;
	}

	/**
	 * @return isRefinance
	 */
	public String getIsRefinance() {
		return this.isRefinance;
	}

	/**
	 * @param isRwrop
	 */
	public void setIsRwrop(String isRwrop) {
		this.isRwrop = isRwrop;
	}

	/**
	 * @return isRwrop
	 */
	public String getIsRwrop() {
		return this.isRwrop;
	}

	/**
	 * @param chrgRate
	 */
	public void setChrgRate(java.math.BigDecimal chrgRate) {
		this.chrgRate = chrgRate;
	}

	/**
	 * @return chrgRate
	 */
	public java.math.BigDecimal getChrgRate() {
		return this.chrgRate;
	}

	/**
	 * @param chrgCollectMode
	 */
	public void setChrgCollectMode(String chrgCollectMode) {
		this.chrgCollectMode = chrgCollectMode;
	}

	/**
	 * @return chrgCollectMode
	 */
	public String getChrgCollectMode() {
		return this.chrgCollectMode;
	}

	/**
	 * @param consignorType
	 */
	public void setConsignorType(String consignorType) {
		this.consignorType = consignorType;
	}

	/**
	 * @return consignorType
	 */
	public String getConsignorType() {
		return this.consignorType;
	}

	/**
	 * @param consignorCusId
	 */
	public void setConsignorCusId(String consignorCusId) {
		this.consignorCusId = consignorCusId;
	}

	/**
	 * @return consignorCusId
	 */
	public String getConsignorCusId() {
		return this.consignorCusId;
	}

	/**
	 * @param repayPlanDesc
	 */
	public void setRepayPlanDesc(String repayPlanDesc) {
		this.repayPlanDesc = repayPlanDesc;
	}

	/**
	 * @return repayPlanDesc
	 */
	public String getRepayPlanDesc() {
		return this.repayPlanDesc;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public Date getUpdateTime() {
		return this.updateTime;
	}



}