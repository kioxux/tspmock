/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;


import javax.persistence.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgSftpInfo
 * @类描述: cfg_sftp_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-25 08:53:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_sftp_info")
public class CfgSftpInfo {
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 地址 **/
	@Column(name = "HOST", unique = false, nullable = true, length = 20)
	private String host;
	
	/** 端口 **/
	@Column(name = "PORT", unique = false, nullable = true, length = 20)
	private String port;
	
	/** 用户名 **/
	@Column(name = "USERNAME", unique = false, nullable = true, length = 64)
	private String username;
	
	/** 密码 **/
	@Column(name = "PASSWORD", unique = false, nullable = true, length = 64)
	private String password;
	
	/** 本地地址 **/
	@Column(name = "LOCALPATH", unique = false, nullable = true, length = 128)
	private String localpath;
	
	/** 服务器地址 **/
	@Column(name = "SERVERPATH", unique = false, nullable = true, length = 128)
	private String serverpath;

	/** 发送短信手机号码 **/
	@Column(name = "SEND_MSG_PHONES", unique = false, nullable = true, length = 255)
	private String sendMsgPhones;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param host
	 */
	public void setHost(String host) {
		this.host = host;
	}
	
    /**
     * @return host
     */
	public String getHost() {
		return this.host;
	}
	
	/**
	 * @param port
	 */
	public void setPort(String port) {
		this.port = port;
	}
	
    /**
     * @return port
     */
	public String getPort() {
		return this.port;
	}
	
	/**
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
    /**
     * @return username
     */
	public String getUsername() {
		return this.username;
	}
	
	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
    /**
     * @return password
     */
	public String getPassword() {
		return this.password;
	}
	
	/**
	 * @param localpath
	 */
	public void setLocalpath(String localpath) {
		this.localpath = localpath;
	}
	
    /**
     * @return localpath
     */
	public String getLocalpath() {
		return this.localpath;
	}
	
	/**
	 * @param serverpath
	 */
	public void setServerpath(String serverpath) {
		this.serverpath = serverpath;
	}
	
    /**
     * @return serverpath
     */
	public String getServerpath() {
		return this.serverpath;
	}

	public String getSendMsgPhones() {
		return sendMsgPhones;
	}

	public void setSendMsgPhones(String sendMsgPhones) {
		this.sendMsgPhones = sendMsgPhones;
	}


}