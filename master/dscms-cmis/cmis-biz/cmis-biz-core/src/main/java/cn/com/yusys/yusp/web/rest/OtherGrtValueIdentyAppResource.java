/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.OtherCnyRateAppFinDetails;
import cn.com.yusys.yusp.domain.OtherForRateApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherGrtValueIdentyApp;
import cn.com.yusys.yusp.service.OtherGrtValueIdentyAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherGrtValueIdentyAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-10 20:36:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/othergrtvalueidentyapp")
public class OtherGrtValueIdentyAppResource {
    @Autowired
    private OtherGrtValueIdentyAppService otherGrtValueIdentyAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherGrtValueIdentyApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherGrtValueIdentyApp> list = otherGrtValueIdentyAppService.selectAll(queryModel);
        return new ResultDto<List<OtherGrtValueIdentyApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherGrtValueIdentyApp>> index(QueryModel queryModel) {
        List<OtherGrtValueIdentyApp> list = otherGrtValueIdentyAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherGrtValueIdentyApp>>(list);
    }

    /**
     * @函数名称:selectByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<OtherGrtValueIdentyApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<OtherGrtValueIdentyApp> list = otherGrtValueIdentyAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherGrtValueIdentyApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherGrtValueIdentyApp> show(@PathVariable("serno") String serno) {
        OtherGrtValueIdentyApp otherGrtValueIdentyApp = otherGrtValueIdentyAppService.selectByPrimaryKey(serno);
        return new ResultDto<OtherGrtValueIdentyApp>(otherGrtValueIdentyApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherGrtValueIdentyApp> create(@RequestBody OtherGrtValueIdentyApp otherGrtValueIdentyApp) throws URISyntaxException {
        otherGrtValueIdentyAppService.insert(otherGrtValueIdentyApp);
        return new ResultDto<OtherGrtValueIdentyApp>(otherGrtValueIdentyApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherGrtValueIdentyApp otherGrtValueIdentyApp) throws URISyntaxException {
        int result = otherGrtValueIdentyAppService.update(otherGrtValueIdentyApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherGrtValueIdentyAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherGrtValueIdentyAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteInfo
     * @函数描述:单个对象删除，将操作类型置为删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteInfo/{serno}")
    protected ResultDto<Integer> deleteInfo(@PathVariable("serno") String serno) {
        int result = otherGrtValueIdentyAppService.deleteInfo(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateOprTypeUnderLmt
     * @函数描述:授信场景下 逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateoprtypeunderlmt")
    protected ResultDto<Integer> updateOprTypeUnderLmt(@RequestBody String serno) {
        int result = otherGrtValueIdentyAppService.deleteInfo(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: getothergrtvalueidentyapp
     * @方法描述: 根据入参获取当前客户经理名下授信抵押物价值认定申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getothergrtvalueidentyapp")
    protected ResultDto<List<OtherGrtValueIdentyApp>> getOtherGrtValueIdentyApp(@RequestBody QueryModel model) {
        List<OtherGrtValueIdentyApp> list = otherGrtValueIdentyAppService.getOtherGrtValueIdentyAppByModel(model);
        return new ResultDto<List<OtherGrtValueIdentyApp>>(list);
    }

    /**
     * @方法名称: getothergrtvalueidentyappHis
     * @方法描述: 根据入参获取当前客户经理名下授信抵押物价值认定申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getothergrtvalueidentyapphis")
    protected ResultDto<List<OtherGrtValueIdentyApp>> getOtherGrtValueIdentyAppHis(@RequestBody QueryModel model) {
        List<OtherGrtValueIdentyApp> list = otherGrtValueIdentyAppService.getOtherGrtValueIdentyAppHis(model);
        return new ResultDto<List<OtherGrtValueIdentyApp>>(list);
    }

    /**
     * @方法名称: addothergrtvalueidentyapp
     * @方法描述: 新增授信抵押物价值认定申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/addothergrtvalueidentyapp")
    protected ResultDto addothergrtvalueidentyapp(@RequestBody OtherGrtValueIdentyApp otherGrtValueIdentyApp) {
        return otherGrtValueIdentyAppService.addothergrtvalueidentyapp(otherGrtValueIdentyApp);
    }

    /**
     * @方法名称: updateothergrtvalueidentyapp
     * @方法描述: 修改授信抵押物价值认定申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/updateothergrtvalueidentyapp")
    protected ResultDto updateothergrtvalueidentyapp(@RequestBody OtherGrtValueIdentyApp otherGrtValueIdentyApp) {
        return otherGrtValueIdentyAppService.updateothergrtvalueidentyapp(otherGrtValueIdentyApp);
    }

    /**
     * @方法名称: checkothergrtvalueidentyapp
     * @方法描述: 校验抵押物价值认定信息是否存在
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/checkothergrtvalueidentyapp")
    protected ResultDto checkothergrtvalueidentyapp(@RequestBody OtherGrtValueIdentyApp otherGrtValueIdentyApp) {
        return otherGrtValueIdentyAppService.checkothergrtvalueidentyapp(otherGrtValueIdentyApp);
    }
}
