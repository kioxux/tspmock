package cn.com.yusys.yusp.service.server.xdxw0038;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0038.req.Xdxw0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0038.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0038.resp.Xdxw0038DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0036Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-05-05 19:46:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0038Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0038Service.class);

    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;

    /**
     * 查询调查表和其他关联信息
     *
     * @param Xdxw0038DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0038DataRespDto xdxw0038(Xdxw0038DataReqDto Xdxw0038DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value);
        Xdxw0038DataRespDto xdxw0038DataRespDto = new Xdxw0038DataRespDto();
        try {
            String certCode = Xdxw0038DataReqDto.getCert_code();//证件号
            String cusId = Xdxw0038DataReqDto.getCusId();//客户号

            Map queryModel = new HashMap();
            queryModel.put("cusId", cusId);//客户号
            queryModel.put("certCode", certCode);//证件号
            logger.info("***********XDXW0038***查询调查表和其他关联信息开始,查询参数为:{}", JSON.toJSONString(queryModel));
            java.util.List<List> lists = lmtSurveyReportMainInfoMapper.selectLmtSurveyReportInfoByCusId(queryModel);
            logger.info("***********XDXW0038***查询调查表和其他关联信息结束,返回结果为:{}", JSON.toJSONString(lists));
            //返回信息
            if (CollectionUtils.nonEmpty(lists)) {//查询结果不为空
                xdxw0038DataRespDto.setList(lists);
            } else {
                cn.com.yusys.yusp.dto.server.xdxw0038.resp.List list = new cn.com.yusys.yusp.dto.server.xdxw0038.resp.List();
                list.setSerno(StringUtils.EMPTY);// 流水号
                list.setApply_type(StringUtils.EMPTY);// 申请类型
                list.setCus_relay_people(StringUtils.EMPTY);// 评审人员
                list.setAppr_status(StringUtils.EMPTY);// 状态
                list.setManager_name(StringUtils.EMPTY);// 管户经理名称
                list.setOrg_name(StringUtils.EMPTY);// 机构名称
                list.setCus_fy_type(StringUtils.EMPTY);// 复议类型
                list.setPrd_name(StringUtils.EMPTY);// 产品名称
                list.setCus_name(StringUtils.EMPTY);// 客户名称
                list.setCert_type(StringUtils.EMPTY);// 证件类型
                list.setCert_code(StringUtils.EMPTY);// 证件号
                list.setCus_id(StringUtils.EMPTY);// 客户号
                list.setNow_dq_amount(new BigDecimal(0L));// 申请金额
                list.setNow_dq_bit(new BigDecimal(0L));// 申请利率
                list.setNow_dq_term(new BigDecimal(0L));// 申请期限
                list.setNow_dq_ass(StringUtils.EMPTY);// 申请担保方式
                list.setInput_date(StringUtils.EMPTY);// 申请日期
                xdxw0038DataRespDto.setList(Arrays.asList(list));
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value);
        return xdxw0038DataRespDto;
    }
}
