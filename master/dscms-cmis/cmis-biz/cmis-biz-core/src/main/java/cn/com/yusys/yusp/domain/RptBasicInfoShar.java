/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoShar
 * @类描述: rpt_basic_info_shar数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-10 15:46:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_basic_info_shar")
public class RptBasicInfoShar extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 股东客户编号 **/
	@Column(name = "SHD_CUS_ID", unique = false, nullable = true, length = 40)
	private String shdCusId;
	
	/** 股东客户姓名 **/
	@Column(name = "SHD_CUS_NAME", unique = false, nullable = true, length = 40)
	private String shdCusName;
	
	/** 认缴金额 **/
	@Column(name = "PERMIUM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal permiumAmt;
	
	/** 占比 **/
	@Column(name = "PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal perc;
	
	/** 实收资本 **/
	@Column(name = "PAID_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidCap;
	
	/** 出资方式 **/
	@Column(name = "INV_APP", unique = false, nullable = true, length = 5)
	private String invApp;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 65535)
	private String remark;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param shdCusId
	 */
	public void setShdCusId(String shdCusId) {
		this.shdCusId = shdCusId;
	}
	
    /**
     * @return shdCusId
     */
	public String getShdCusId() {
		return this.shdCusId;
	}
	
	/**
	 * @param shdCusName
	 */
	public void setShdCusName(String shdCusName) {
		this.shdCusName = shdCusName;
	}
	
    /**
     * @return shdCusName
     */
	public String getShdCusName() {
		return this.shdCusName;
	}
	
	/**
	 * @param permiumAmt
	 */
	public void setPermiumAmt(java.math.BigDecimal permiumAmt) {
		this.permiumAmt = permiumAmt;
	}
	
    /**
     * @return permiumAmt
     */
	public java.math.BigDecimal getPermiumAmt() {
		return this.permiumAmt;
	}
	
	/**
	 * @param perc
	 */
	public void setPerc(java.math.BigDecimal perc) {
		this.perc = perc;
	}
	
    /**
     * @return perc
     */
	public java.math.BigDecimal getPerc() {
		return this.perc;
	}
	
	/**
	 * @param paidCap
	 */
	public void setPaidCap(java.math.BigDecimal paidCap) {
		this.paidCap = paidCap;
	}
	
    /**
     * @return paidCap
     */
	public java.math.BigDecimal getPaidCap() {
		return this.paidCap;
	}
	
	/**
	 * @param invApp
	 */
	public void setInvApp(String invApp) {
		this.invApp = invApp;
	}
	
    /**
     * @return invApp
     */
	public String getInvApp() {
		return this.invApp;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}


}