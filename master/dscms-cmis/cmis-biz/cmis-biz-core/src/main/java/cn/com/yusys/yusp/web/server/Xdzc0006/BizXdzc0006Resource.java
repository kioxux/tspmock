package cn.com.yusys.yusp.web.server.Xdzc0006;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0006.req.Xdzc0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0006.resp.Xdzc0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0006.Xdzc0006Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池出池校验接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0006:资产池出池校验接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0006Resource.class);

    @Autowired
    private Xdzc0006Service xdzc0006Service;
    /**
     * 交易码：xdzc0006
     * 交易描述：资产池出池校验接口
     *
     * @param xdzc0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池出池校验接口")
    @PostMapping("/xdzc0006")
    protected @ResponseBody
    ResultDto<Xdzc0006DataRespDto> xdzc0006(@Validated @RequestBody Xdzc0006DataReqDto xdzc0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0006.key, DscmsEnum.TRADE_CODE_XDZC0006.value, JSON.toJSONString(xdzc0006DataReqDto));
        Xdzc0006DataRespDto xdzc0006DataRespDto = new Xdzc0006DataRespDto();// 响应Dto:资产池出池校验接口
        ResultDto<Xdzc0006DataRespDto> xdzc0006DataResultDto = new ResultDto<>();

        try {
            // 从xdzc0006DataReqDto获取业务值进行业务逻辑处理
            xdzc0006DataRespDto = xdzc0006Service.xdzc0006Service(xdzc0006DataReqDto);
            xdzc0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0006.key, DscmsEnum.TRADE_CODE_XDZC0006.value, e.getMessage());
            // 封装xdzc0006DataResultDto中异常返回码和返回信息
            xdzc0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0006DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdzc0006DataRespDto到xdzc0006DataResultDto中
        xdzc0006DataResultDto.setData(xdzc0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0006.key, DscmsEnum.TRADE_CODE_XDZC0006.value, JSON.toJSONString(xdzc0006DataResultDto));
        return xdzc0006DataResultDto;
    }
}
