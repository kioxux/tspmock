package cn.com.yusys.yusp.service.client.bsp.core.ln3102;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Ln3102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Ln3102RespDto;
import cn.com.yusys.yusp.dto.server.xddh0005.req.Xddh0005DataReqDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：贷款期供查询试算
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Ln3102Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw01Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * 业务逻辑处理方法：贷款期供查询试算
     *
     * @param ln3102ReqDto
     * @return
     */
    @Transactional
    public Ln3102RespDto ln3102(Ln3102ReqDto ln3102ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3102.key, EsbEnum.TRADE_CODE_LN3102.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3102.key, EsbEnum.TRADE_CODE_LN3102.value, JSON.toJSONString(ln3102ReqDto));
        ResultDto<Ln3102RespDto> ln3102ResultDto = dscms2CoreLnClientService.ln3102(ln3102ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3102.key, EsbEnum.TRADE_CODE_LN3102.value, JSON.toJSONString(ln3102ResultDto));
        String ln3102Code = Optional.ofNullable(ln3102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3102Meesage = Optional.ofNullable(ln3102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3102RespDto ln3102RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3102ResultDto.getCode())) {
            //  获取相关的值并解析
            ln3102RespDto = ln3102ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, ln3102Code, ln3102Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3102.key, EsbEnum.TRADE_CODE_LN3102.value);
        return ln3102RespDto;
    }

    /**
     * 根据 [请求Data：提前还款 ]组装[请求Dto：贷款期供查询试算]
     *
     * @param xddh0005DataReqDto
     * @return
     */
    public Ln3102ReqDto buildLn3102ReqDto(Xddh0005DataReqDto xddh0005DataReqDto) {
        String billNo = xddh0005DataReqDto.getBillNo();//借据号
        Ln3102ReqDto ln3102ReqDto = GenericBuilder.of(Ln3102ReqDto::new)
                .with(Ln3102ReqDto::setDkzhangh, StringUtils.EMPTY)// 贷款账号
                .with(Ln3102ReqDto::setDkjiejuh, billNo)// 贷款借据号
                .with(Ln3102ReqDto::setQishiqsh, null)// 起始期数
                .with(Ln3102ReqDto::setZhzhiqsh, null)// 终止期数
                .with(Ln3102ReqDto::setQishibis, null)// 起始笔数
                .with(Ln3102ReqDto::setChxunbis, null)// 查询笔数
                .with(Ln3102ReqDto::setQishriqi, StringUtils.EMPTY)// 起始日期
                .with(Ln3102ReqDto::setZhzhriqi, StringUtils.EMPTY)// 终止日期
                .with(Ln3102ReqDto::setJixiqish, null)// 计息期数
                .with(Ln3102ReqDto::setBenqizht, StringUtils.EMPTY)// 本期状态
                .with(Ln3102ReqDto::setShisriqi, StringUtils.EMPTY)// 试算日期
                .with(Ln3102ReqDto::setChaxunzl, StringUtils.EMPTY)// 查询种类
                .with(Ln3102ReqDto::setShifoudy, StringUtils.EMPTY)// 是否打印
                .build();
        return ln3102ReqDto;
    }
}
