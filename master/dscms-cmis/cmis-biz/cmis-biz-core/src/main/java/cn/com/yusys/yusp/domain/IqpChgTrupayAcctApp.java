/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-dto模块
 * @类名称: IqpChgTrupayAcctApp
 * @类描述: iqp_chg_trupay_acct_app数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-13 10:20:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_chg_trupay_acct_app")
public class IqpChgTrupayAcctApp extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 20)
	private String serno;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;

	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 交易对手账户 **/
	@Column(name = "TOPP_ACCNO", unique = false, nullable = true, length = 40)
	private String toppAccno;

	/** 交易对手名称 **/
	@Column(name = "TOPP_NAME", unique = false, nullable = true, length = 80)
	private String toppName;

	/** 交易对手金额 **/
	@Column(name = "TOPP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal toppAmt;

	/** 原交易对手账户 **/
	@Column(name = "ORIGI_TOPP_ACCNO", unique = false, nullable = true, length = 40)
	private String origiToppAccno;

	/** 原交易对手名称 **/
	@Column(name = "ORIGI_TOPP_NAME", unique = false, nullable = true, length = 80)
	private String origiToppName;

	/** 原交易对手金额 **/
	@Column(name = "ORIGI_TOPP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiToppAmt;

	/** 审批状态  **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 是否客户修改 STD_ZB_YES_NO **/
	@Column(name = "IS_OWNER", unique = false, nullable = true, length = 5)
	private String isOwner;

	/** 核心流水号 **/
	@Column(name = "HX_SERNO", unique = false, nullable = true, length = 40)
	private String hxSerno;

	/** 受托支付账户变更原因 **/
	@Column(name = "CHG_RESN", unique = false, nullable = true, length = 255)
	private String chgResn;

	/** 授权状态 STD_ZB_AUTH_ST **/
	@Column(name = "AUTH_STATUS", unique = false, nullable = true, length = 5)
	private String authStatus;

	/** 交易对手开户行号 **/
	@Column(name = "TOPP_BANK_NO", unique = false, nullable = true, length = 40)
	private String toppBankNo;

	/** 交易对手开户行名称 **/
	@Column(name = "TOPP_BANK_NAME", unique = false, nullable = true, length = 100)
	private String toppBankName;

	/** 原交易对手开户行号 **/
	@Column(name = "ORIGI_TOPP_BANKNO", unique = false, nullable = true, length = 40)
	private String origiToppBankno;

	/** 原交易对手开户行名称 **/
	@Column(name = "ORIGI_TOPP_BANKNAME", unique = false, nullable = true, length = 100)
	private String origiToppBankname;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	/**
	 * @return billNo
	 */
	public String getBillNo() {
		return this.billNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param toppAccno
	 */
	public void setToppAccno(String toppAccno) {
		this.toppAccno = toppAccno;
	}

	/**
	 * @return toppAccno
	 */
	public String getToppAccno() {
		return this.toppAccno;
	}

	/**
	 * @param toppName
	 */
	public void setToppName(String toppName) {
		this.toppName = toppName;
	}

	/**
	 * @return toppName
	 */
	public String getToppName() {
		return this.toppName;
	}

	/**
	 * @param toppAmt
	 */
	public void setToppAmt(java.math.BigDecimal toppAmt) {
		this.toppAmt = toppAmt;
	}

	/**
	 * @return toppAmt
	 */
	public java.math.BigDecimal getToppAmt() {
		return this.toppAmt;
	}

	/**
	 * @param origiToppAccno
	 */
	public void setOrigiToppAccno(String origiToppAccno) {
		this.origiToppAccno = origiToppAccno;
	}

	/**
	 * @return origiToppAccno
	 */
	public String getOrigiToppAccno() {
		return this.origiToppAccno;
	}

	/**
	 * @param origiToppName
	 */
	public void setOrigiToppName(String origiToppName) {
		this.origiToppName = origiToppName;
	}

	/**
	 * @return origiToppName
	 */
	public String getOrigiToppName() {
		return this.origiToppName;
	}

	/**
	 * @param origiToppAmt
	 */
	public void setOrigiToppAmt(java.math.BigDecimal origiToppAmt) {
		this.origiToppAmt = origiToppAmt;
	}

	/**
	 * @return origiToppAmt
	 */
	public java.math.BigDecimal getOrigiToppAmt() {
		return this.origiToppAmt;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param isOwner
	 */
	public void setIsOwner(String isOwner) {
		this.isOwner = isOwner;
	}

	/**
	 * @return isOwner
	 */
	public String getIsOwner() {
		return this.isOwner;
	}

	/**
	 * @param hxSerno
	 */
	public void setHxSerno(String hxSerno) {
		this.hxSerno = hxSerno;
	}

	/**
	 * @return hxSerno
	 */
	public String getHxSerno() {
		return this.hxSerno;
	}

	/**
	 * @return chgResn
	 */
	public String getChgResn() {
		return this.chgResn;
	}

	/**
	 * @param chgResn
	 */
	public void setChgResn(String chgResn) {
		this.chgResn = chgResn;
	}

	/**
	 * @param authStatus
	 */
	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}

	/**
	 * @return authStatus
	 */
	public String getAuthStatus() {
		return this.authStatus;
	}

	/**
	 * @param toppBankNo
	 */
	public void setToppBankNo(String toppBankNo) {
		this.toppBankNo = toppBankNo;
	}

	/**
	 * @return toppBankNo
	 */
	public String getToppBankNo() {
		return this.toppBankNo;
	}

	/**
	 * @param toppBankName
	 */
	public void setToppBankName(String toppBankName) {
		this.toppBankName = toppBankName;
	}

	/**
	 * @return toppBankName
	 */
	public String getToppBankName() {
		return this.toppBankName;
	}

	/**
	 * @param origiToppBankno
	 */
	public void setOrigiToppBankno(String origiToppBankno) {
		this.origiToppBankno = origiToppBankno;
	}

	/**
	 * @return origiToppBankno
	 */
	public String getOrigiToppBankno() {
		return this.origiToppBankno;
	}

	/**
	 * @param origiToppBankname
	 */
	public void setOrigiToppBankname(String origiToppBankname) {
		this.origiToppBankname = origiToppBankname;
	}

	/**
	 * @return origiToppBankname
	 */
	public String getOrigiToppBankname() {
		return this.origiToppBankname;
	}


}