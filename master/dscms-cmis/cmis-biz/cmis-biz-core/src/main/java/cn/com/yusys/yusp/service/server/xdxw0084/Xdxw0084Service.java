package cn.com.yusys.yusp.service.server.xdxw0084;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CfgGenerateTempFileDto;
import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import cn.com.yusys.yusp.dto.server.xdxw0084.req.Xdxw0084DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0084.resp.Xdxw0084DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 接口处理类:共借人送达地址确认书文本生成PDF
 *
 * @author zrcbank
 * @version 1.0
 */
@Service
public class Xdxw0084Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0084Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;

    /**
     * 共借人送达地址确认书文本生成PDF
     *
     * @param xdxw0084DataReqDto
     * @return
     */
    public Xdxw0084DataRespDto xdxw0084(Xdxw0084DataReqDto xdxw0084DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, JSON.toJSONString(xdxw0084DataReqDto));
        Xdxw0084DataRespDto xdxw0084DataRespDto = new Xdxw0084DataRespDto();

        try {
            String cusName = xdxw0084DataReqDto.getCusName();
            String address = xdxw0084DataReqDto.getAddress();
            String signDate = xdxw0084DataReqDto.getSignDate();
            String phone = xdxw0084DataReqDto.getPhone();


            //1、第一步：确认要生成prd的模板名称
            String cptModelName = "sddzqr.cpt";//帆软模板名称
            String pdfFileName = "xdxwbook" + phone + ""; //生成的pdf文件名称

            // 获取配置信息
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("pkId", "00001");
            ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
            List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
            CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);
            //查询存储地址信息
            String ip = cfgGenerateTempFileDto.getLoginIp();
            String port = cfgGenerateTempFileDto.getLoginPort();
            String username = cfgGenerateTempFileDto.getLoginUsername();
            String password = cfgGenerateTempFileDto.getLoginPwd();
            String path = cfgGenerateTempFileDto.getFilePath();
            String filePath = cfgGenerateTempFileDto.getFilePath();
            String url = cfgGenerateTempFileDto.getMemo();

            //调用帆软的生成pdf的方法
            //2、传入帆软报表生成需要的参数
            HashMap<String, Object> parameterMap = new HashMap<String, Object>();
            parameterMap.put("cusName", cusName);//客户名
            parameterMap.put("address", address);
            parameterMap.put("signDate", signDate);
            parameterMap.put("phone", phone);
            //传入公共参数
            parameterMap.put("TempleteName", cptModelName);//模板名称（附带路径）
            parameterMap.put("saveFileName", pdfFileName);//待生成的PDF文件名称
            parameterMap.put("path", path);

            //2、调用公共方法生成pdf
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            if (StringUtils.isEmpty(url)) {
                // url = "http://10.28.206.191:8090/dscms/frpt/api/frpt/createFrptPdf";//本地测试地址
            }
            // 生成PDF文件
            try {
                FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                frptPdfArgsDto.setPdfFileName(cptModelName);//模板名称
                frptPdfArgsDto.setNewFileName(pdfFileName);//待生成的PDF文件名称
                frptPdfArgsDto.setSerno(phone);//设置流水
                frptPdfArgsDto.setPath(path);//路径
                frptPdfArgsDto.setIp(ip);//IP地址
                frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                frptPdfArgsDto.setUserName(username);
                frptPdfArgsDto.setPassWord(password);
                frptPdfArgsDto.setMap(parameterMap);
                HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                String code = responseEntity.getBody();
            } catch (Exception e) {
                logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            }

            //返回参数
            xdxw0084DataRespDto.setPdfurl(path);
            xdxw0084DataRespDto.setPdffilename(pdfFileName+ ".pdf");
            xdxw0084DataRespDto.setHtip(ip);
            xdxw0084DataRespDto.setPort(port);
            xdxw0084DataRespDto.setUsername(username);
            xdxw0084DataRespDto.setPassword(password);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }

        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, JSON.toJSONString(xdxw0084DataRespDto));
        return xdxw0084DataRespDto;
    }
}
