/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.domain.LmtReplyAcc;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtGrpReplyAcc;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyAccMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:40:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtGrpReplyAccMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtGrpReplyAcc selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtGrpReplyAcc> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtGrpReplyAcc record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtGrpReplyAcc record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtGrpReplyAcc record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtGrpReplyAcc record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 通过入参查询授信申请信息
     *
     * @param queryMap
     * @return
     */
    List<LmtGrpReplyAcc> selectByParams(Map queryMap);

    /**
     * 获取当前登陆人名下可进行预授信细化的授信批复台账
     *
     * @param params
     * @return
     */
    List<LmtGrpReplyAcc> getReplyAccForChg(QueryModel params);


    /**
     * @方法名称：queryByManagerId
     * @方法描述：根据条件查询授信批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-05-24 上午 8:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    List<LmtGrpReplyAcc> queryByManagerId(HashMap<String, String> paramMap);

    /**
     * @方法名称：queryByGrpCusId
     * @方法描述：根据集团客户号查询申请信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-06-22 上午 8:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    List<LmtGrpReplyAcc> queryByGrpCusId(QueryModel queryModel);

    /**
     * @方法名称: selectByGrpAccNo
     * @方法描述: 根据业务主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    LmtGrpReplyAcc selectByGrpAccNo(@Param("grpAccNo") String grpAccNo);

    /**
     * 通过入参查询授信申请信息
     *
     * @param model
     * @return
     */
    List<LmtGrpReplyAcc> queryAll(QueryModel model);

    /**
     * @方法名称: queryDetailByGrpReplySerno
     * @方法描述: 通过批复号 获取批复台账信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-07-15 10:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */

    LmtGrpReplyAcc queryDetailByGrpReplySerno(Map map);

    /**
     * 集团流水查批复
     * @param grpSerno
     * @return
     */

    LmtGrpReplyAcc queryDetailByGrpSerno(@Param("grpSerno") String grpSerno);
}