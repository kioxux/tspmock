package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtGrpAppSubDto;
import cn.com.yusys.yusp.dto.QueryAreaInfoDto;
import cn.com.yusys.yusp.dto.server.cmislmt0042.req.CmisLmt0042ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0042.resp.CmisLmt0042ListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0042.resp.CmisLmt0042RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047LmtSubDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047ContRelDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtAppSubMapper;
import cn.com.yusys.yusp.repository.mapper.LmtAppSubPrdMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.MapUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppSubPrdService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:23:31
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtAppSubPrdService {

    private static final Logger log = LoggerFactory.getLogger(LmtAppSubPrdService.class);

    @Resource
    private LmtAppSubPrdMapper lmtAppSubPrdMapper;

    @Resource
    private LmtAppSubMapper lmtAppPrdMapper;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtRepayCapPlanService lmtRepayCapPlanService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtSubPrdLowGuarRelService lmtSubPrdLowGuarRelService;

    @Autowired
    private BizMustCheckDetailsService bizMustCheckDetailsService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    //担保方式 00信用 10抵押 20质押30保证
    // 00信用
    Map<String, String> creditMap = new HashMap<String, String>() {
        {
            put("P001", "00");
            put("P004", "00");
            put("P007", "00");
            put("P008", "00");
            put("P009", "00");
            put("P010", "00");
            put("P013", "00");
            put("P014", "00");
            put("P015", "00");
            put("P016", "00");
            put("P029", "00");
            put("P032", "00");
            put("P033", "00");
            put("P040", "00");
            put("P041", "00");
            put("P042", "00");
        }
    };
    // 10抵押
    Map<String, String> mortgageMap = new HashMap<String, String>() {
        {
            put("P001", "10");
            put("P007", "10");
            put("P008", "10");
            put("P011", "10");
            put("P019", "10");
            put("P025", "10");
            put("P028", "10");
            put("P034", "10");
            put("P035", "10");
            put("P036", "10");
            put("P040", "10");
            put("P041", "10");
        }
    };
    // 20质押
    Map<String, String> pledgeMap = new HashMap<String, String>() {
        {
            put("P001", "20");
            put("P007", "20");
            put("P008", "20");
            put("P018", "20");
            put("P019", "20");
            put("P026", "20");
            put("P027", "20");
            put("P036", "20");
            put("P038", "20");
            put("P039", "20");
            put("P040", "20");
            put("P041", "20");
        }
    };
    // 30保证
    Map<String, String> ensureMap = new HashMap<String, String>() {
        {
            put("P001", "30");
            put("P002", "30");
            put("P003", "30");
            put("P004", "30");
            put("P005", "30");
            put("P007", "30");
            put("P008", "30");
            put("P009", "30");
            put("P010", "30");
            put("P013", "30");
            put("P014", "30");
            put("P015", "30");
            put("P017", "30");
            put("P018", "30");
            put("P019", "30");
            put("P020", "30");
            put("P021", "30");
            put("P022", "30");
            put("P023", "30");
            put("P024", "30");
            put("P028", "30");
            put("P029", "30");
            put("P030", "30");
            put("P031", "30");
            put("P032", "30");
            put("P033", "30");
            put("P035", "30");
            put("P036", "30");
            put("P037", "30");
            put("P038", "30");
            put("P039", "30");
            put("P040", "30");
            put("P041", "30");
            put("P042", "30");

        }
    };

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtAppSubPrd selectByPrimaryKey(String pkId) {
        return lmtAppSubPrdMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtAppSubPrd> selectAll(QueryModel model) {
        List<LmtAppSubPrd> records = (List<LmtAppSubPrd>) lmtAppSubPrdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtAppSubPrd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtAppSubPrd> list = lmtAppSubPrdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtAppSubPrd record) {
        return lmtAppSubPrdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtAppSubPrd record) {
        return lmtAppSubPrdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtAppSubPrd record) {
        return lmtAppSubPrdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtAppSubPrd record) {
        return lmtAppSubPrdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtAppSubPrdMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtAppSubPrdMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: saveLmtAppSubPrd
     * @方法描述: 新增使用授信分项明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map saveLmtAppSubPrd(LmtAppSubPrd lmtAppSubPrd) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        Map pramas = new HashMap();
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<String> list = new ArrayList<>();
        String newName = "";
        String subPrdSerno = "";
        try {
            // TODO 保存前校验 待补充
            String subSerno = lmtAppSubPrd.getSubSerno();
            String lmtBizType = lmtAppSubPrd.getLmtBizType();
            String subPrdName = lmtAppSubPrd.getLmtBizTypeName();
            LmtAppSub lmtAppSub = lmtAppSubService.selectBySubSerno(subSerno);
            String subName = lmtAppSub.getSubName();
            BigDecimal lmtAmt = lmtAppSub.getLmtAmt();
            //BigDecimal lmtAmtTotal = lmtAppSubPrdMapper.calLmtAmt(subSerno);
            BigDecimal lmtAmtSubPrd = lmtAppSubPrd.getLmtAmt();
            if (lmtAmt.compareTo(lmtAmtSubPrd) < 0) {
                rtnCode = EcbEnum.ECB010007.key;
                rtnMsg = EcbEnum.ECB010007.value;
                return rtnData;
            }
            // 信用的时候
            if (Objects.equals("00", lmtAppSub.getGuarMode())) {
                if (StringUtils.nonBlank(lmtAppSubPrd.getLmtBizTypeProp()) && !Objects.equals("00", creditMap.get(lmtAppSubPrd.getLmtBizTypeProp()))) {
                    rtnCode = EcbEnum.ECB020020.key;
                    rtnMsg = EcbEnum.ECB020020.value;
                    return rtnData;
                }
            }
            // 抵押的时候
            if (Objects.equals("10", lmtAppSub.getGuarMode())) {
                if (StringUtils.nonBlank(lmtAppSubPrd.getLmtBizTypeProp()) && !Objects.equals("10", mortgageMap.get(lmtAppSubPrd.getLmtBizTypeProp()))) {
                    rtnCode = EcbEnum.ECB020020.key;
                    rtnMsg = EcbEnum.ECB020020.value;
                    return rtnData;
                }
            }
            // 质押的时候
            if (Objects.equals("20", lmtAppSub.getGuarMode())) {
                if (StringUtils.nonBlank(lmtAppSubPrd.getLmtBizTypeProp()) && !Objects.equals("20", pledgeMap.get(lmtAppSubPrd.getLmtBizTypeProp()))) {
                    rtnCode = EcbEnum.ECB020020.key;
                    rtnMsg = EcbEnum.ECB020020.value;
                    return rtnData;
                }
            }
            // 保证的时候
            if (Objects.equals("30", lmtAppSub.getGuarMode())) {
                if (StringUtils.nonBlank(lmtAppSubPrd.getLmtBizTypeProp()) && !Objects.equals("30", ensureMap.get(lmtAppSubPrd.getLmtBizTypeProp()))) {
                    rtnCode = EcbEnum.ECB020020.key;
                    rtnMsg = EcbEnum.ECB020020.value;
                    return rtnData;
                }
            }

            LmtApp lmtApp = lmtAppService.getLmtAppBySubSerno(lmtAppSub.getSubSerno());
            if (lmtApp.getLmtTerm() != null) {
                if (lmtApp.getLmtTerm().compareTo(lmtAppSubPrd.getLmtTerm()) == -1 && CmisCommonConstants.YES_NO_1.equals(lmtAppSubPrd.getIsRevolvLimit())) {
                    rtnCode = EcbEnum.ECB020040.key;
                    rtnMsg = EcbEnum.ECB020040.value;
                    return rtnData;
                }
            } else {
                rtnCode = EcbEnum.ECB020052.key;
                rtnMsg = EcbEnum.ECB020052.value;
                return rtnData;
            }

            subPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(subPrdSerno)) {
                rtnCode = EcbEnum.ECB010003.key;
                rtnMsg = EcbEnum.ECB010003.value;
                return rtnData;
            }
            log.info("保存适用授信产品分项数据,生成流水号{}", subPrdSerno);
            // 流水号
            lmtAppSubPrd.setSubPrdSerno(subPrdSerno);
            // 数据操作标志为新增
            lmtAppSubPrd.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 币种
            lmtAppSubPrd.setCurType(CmisCommonConstants.CUR_TYPE_CNY);
            // 是否存量授信
            lmtAppSubPrd.setIsSfcaLmt(CmisCommonConstants.STD_ZB_YES_NO_0);
            // 变更标识
            //  TODO 变更标识字典项未定
            lmtAppSubPrd.setChgFlag("1");
            log.info("保存授信适用产品数据{}-获取当前登录用户数据", subPrdSerno);
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                return rtnData;
            } else {
                lmtAppSubPrd.setPkId(UUID.randomUUID().toString());
                lmtAppSubPrd.setInputId(userInfo.getLoginCode());
                lmtAppSubPrd.setInputBrId(userInfo.getOrg().getCode());
                lmtAppSubPrd.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtAppSubPrd.setUpdId(userInfo.getLoginCode());
                lmtAppSubPrd.setUpdBrId(userInfo.getOrg().getCode());
                lmtAppSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtAppSubPrd.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtAppSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            log.info("保存适用授信产品明细数据,流水号{}", subPrdSerno);
            int count = lmtAppSubPrdMapper.insert(lmtAppSubPrd);
            List<LmtAppSubPrd> prdList = lmtAppSubPrdMapper.selectBySubSerno(subSerno);
            boolean isPre = true;
            StringBuilder subNameAppend = new StringBuilder();
            for(LmtAppSubPrd lmtAppSubPrdItem :prdList){
                subNameAppend.append(lmtAppSubPrdItem.getLmtBizTypeName()).append(",");
            }
            if(subNameAppend.length() >0 ){
                subNameAppend.deleteCharAt(subNameAppend.length()-1);
            }
            lmtAppSub.setSubName(subNameAppend.toString());
            if (prdList != null && prdList.size() > 0) {
                for (LmtAppSubPrd lmtAppSubPrdItem : prdList) {
                    if(!CmisCommonConstants.YES_NO_0.equals(lmtAppSubPrdItem.getIsPreLmt())){
                        isPre = false;
                        break;
                    }
                }
            }
            if(isPre){
                lmtAppSub.setIsPreLmt(CmisCommonConstants.YES_NO_0);
            }
//            if (prdList != null && !prdList.isEmpty()) {
//                for (LmtAppSubPrd lmtAppSubPrdItem : prdList) {
//                    list.add(lmtAppSubPrdItem.getLmtBizTypeName());
//                }
//                newName = org.apache.commons.lang.StringUtils.join(list, ",");
//            }
//            if (prdList != null && prdList.size() != 0) {
//                int isPreLmtNum = 0;
//                for (LmtAppSubPrd lmtAppSubPrd1 : prdList) {
//                    if (CmisCommonConstants.YES_NO_0.equals(lmtAppSubPrd1.getIsPreLmt())) {
//                        isPreLmtNum++;
//                    }
//                }
//                if (isPreLmtNum == list.size() && isPreLmtNum > 0) {
//                    lmtAppSub.setIsPreLmt(CmisCommonConstants.YES_NO_0);
//                }
//            }
//            lmtAppSub.setSubName(newName);
            log.info("保存适用分项数据,流水号{}", subSerno);
            int countSub = lmtAppSubService.update(lmtAppSub);
            if (count != 1 || countSub != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",保存失败！");
            }

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存适用产品授信分项数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("subPrdSerno", subPrdSerno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    ;

    /**
     * @方法名称: updateLmtAppSubPrdBy
     * @方法描述: 新增使用授信分项明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map updateLmtAppSubPrdByLmtApp(LmtAppSubPrd lmtAppSubPrd) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {

            // TODO 保存前校验 待补充
            LmtAppSubPrd lmtAppSubPrdOrigin = lmtAppSubPrdMapper.selectByPrimaryKey(lmtAppSubPrd.getPkId());
            String subSerno = lmtAppSubPrd.getSubSerno();
            LmtAppSub lmtAppSub = lmtAppSubService.selectBySubSerno(subSerno);
            BigDecimal lmtAmt = lmtAppSub.getLmtAmt();
            BigDecimal lmtAmtSubPrd = lmtAppSubPrd.getLmtAmt();
            if (lmtAmt.compareTo(lmtAmtSubPrd) == -1) {
                rtnCode = EcbEnum.ECB010007.key;
                rtnMsg = EcbEnum.ECB010007.value;
                return rtnData;
            }
            User userInfo = SessionUtils.getUserInformation();
            lmtAppSubPrd.setUpdId(userInfo.getLoginCode());
            lmtAppSubPrd.setUpdBrId(userInfo.getOrg().getCode());
            lmtAppSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtAppSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            if (!StringUtils.isBlank(lmtAppSubPrd.getOrigiLmtAccSubPrdNo())) {
                LmtApp lmtApp = lmtAppService.selectLmtAppDataBySubSerno(subSerno);
                LmtAppSubPrd lmtAppSubPrdOrigi = lmtAppSubPrdMapper.selectBySubPrdSerno(lmtAppSubPrd.getOrigiLmtAccSubPrdNo());
                if (lmtApp != null) {
                    if (lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_02) || lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_04)) {
                        //  TODO 变更标识字典项未定
                        if (!lmtAppSubPrd.getOrigiLmtAccSubPrdAmt().equals(lmtAppSubPrd.getLmtAmt()) || !lmtAppSubPrd.getOrigiLmtAccSubPrdTerm().equals(lmtAppSubPrd.getLmtTerm())) {
                            lmtAppSubPrd.setChgFlag("2");
                        } else {
                            lmtAppSubPrd.setChgFlag("0");
                        }
                    } else if (lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_07)) {
                        //  TODO 本次细化标识字典项未定
                        if (!lmtAppSubPrdOrigin.getLmtBizType().equals(lmtAppSubPrd.getLmtBizType())) {
                            lmtAppSubPrd.setIsCurtRefine(CmisCommonConstants.STD_ZB_YES_NO_1);
                        }
                    }
                }
            }
            //更新授信适用产品数据
            log.info(String.format("更新授信适用产品信息"));
            int count = lmtAppSubPrdMapper.updateByPrimaryKey(lmtAppSubPrd);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",更新授信适用产品信息失败！");
            }
            // 当原产品层授信金额下调为0的话，对应分项层预授信额度设置为否,自动删除当前数据
            if (CmisCommonConstants.YES_NO_1.equals(lmtAppSubPrd.getIsPreLmt()) && new BigDecimal(0.00).compareTo(lmtAppSubPrd.getLmtAmt()) >= 0) {
                lmtAppSubPrd.setOprType(CmisCommonConstants.OP_TYPE_02);
                update(lmtAppSubPrd);
            }
            // 当原产品层没有预授信额度为是的数据时,也要更新当前分项的预授信额度为否
            List<LmtAppSubPrd> list = this.selectBySubSerno(lmtAppSub.getSubSerno());
            boolean isPre = true;
            StringBuilder subNameAppend = new StringBuilder();
            if (list != null && list.size() > 0) {
                for (LmtAppSubPrd lmtAppSubPrdItem : list) {
                    if(!CmisCommonConstants.YES_NO_0.equals(lmtAppSubPrdItem.getIsPreLmt())){
                        isPre = false;
                        break;
                    }
                }
            }
            if(isPre){
                lmtAppSub.setIsPreLmt(CmisCommonConstants.YES_NO_0);
            }

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("更新授信分项信息出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: deleteByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map deleteByPkId(LmtAppSubPrd record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<String> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        String newName = "";
        try {
            LmtAppSub lmtAppSub = lmtAppSubService.selectBySubSerno(record.getSubSerno());
            LmtApp lmtApp = lmtAppService.getLmtAppBySubSerno(lmtAppSub.getSubSerno());
            if (!CmisCommonConstants.LMT_TYPE_01.equals(lmtApp.getLmtType()) && record.getOrigiLmtAccSubPrdNo() != null && !"".equals(record.getOrigiLmtAccSubPrdNo())) {
                CmisLmt0047ReqDto cmisLmt0047ReqDto = new CmisLmt0047ReqDto();
                HashMap<String, String> queryMap = new HashMap<>();
                queryMap.put("accSubNo", lmtAppSub.getOrigiLmtAccSubNo());
                LmtReplyAccSubPrd replyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(record.getOrigiLmtAccSubPrdNo());
                cmisLmt0047ReqDto.setQueryType(CmisCommonConstants.QUERY_TYPE_02);
                List<CmisLmt0047LmtSubDtoList> lmtSubDtoList = new ArrayList<>();
                if (replyAccSubPrd != null) {
                    log.info("发送额度系统校验分项产品下是否存在未结清的业务,分项编号:" + record.getOrigiLmtAccSubPrdNo());
                    CmisLmt0047LmtSubDtoList cmisLmt0047LmtSubDtoList = new CmisLmt0047LmtSubDtoList();
                    cmisLmt0047LmtSubDtoList.setAccSubNo(record.getOrigiLmtAccSubPrdNo());
                    lmtSubDtoList.add(cmisLmt0047LmtSubDtoList);
                }
                cmisLmt0047ReqDto.setLmtSubDtoList(lmtSubDtoList);
                log.info("发送额度系统校验----------start-----------------,请求报文{}", JSON.toJSONString(cmisLmt0047ReqDto));
                ResultDto<CmisLmt0047RespDto> cmisLmt0047RespDto = cmisLmtClientService.cmislmt0047(cmisLmt0047ReqDto);
                if (cmisLmt0047RespDto != null && cmisLmt0047RespDto.getData() != null && "0000".equals(cmisLmt0047RespDto.getData().getErrorCode())) {
                    log.info("发送额度系统校验成功!");
                    List<CmisLmt0047ContRelDtoList> cmisLmt0047ContRelDtoLists = cmisLmt0047RespDto.getData().getContRelDtoList();
                    if (!cmisLmt0047ContRelDtoLists.isEmpty() && cmisLmt0047ContRelDtoLists.size() > 0) {
                        log.info("当前授信分项适用产品下存在未结清的业务,不可删除!");
                        rtnCode = EcbEnum.ECB020042.key;
                        rtnMsg = EcbEnum.ECB020042.value;
                        return rtnData;
                    }
                }
            }
            int count = lmtAppSubPrdMapper.updateByPkId(record);
            //如果产品为专项额度贷款
            if (record.getLmtBizType().startsWith("14")) {
                String serno = lmtAppSub.getSerno();
                QueryModel model = new QueryModel();
                model.addCondition("serno", serno);
                model.addCondition("pageId", "zydkxx");
                List<BizMustCheckDetails> bizMustCheckDetailsList = bizMustCheckDetailsService.selectByModel(model);
                if (CollectionUtils.nonEmpty(bizMustCheckDetailsList)) {
                    for (BizMustCheckDetails bizMustCheckDetails : bizMustCheckDetailsList) {
                        bizMustCheckDetailsService.deleteByPrimaryKey(bizMustCheckDetails.getPkId());
                    }
                }
            }
            List<LmtAppSubPrd> prdList = lmtAppSubPrdMapper.selectBySubSerno(record.getSubSerno());
            boolean isPre = true;
            StringBuilder subNameAppend = new StringBuilder();
            for(LmtAppSubPrd lmtAppSubPrdItem :prdList){
                subNameAppend.append(lmtAppSubPrdItem.getLmtBizTypeName()).append(",");
            }
            if(subNameAppend.length() >0 ){
                subNameAppend.deleteCharAt(subNameAppend.length()-1);
            }
            lmtAppSub.setSubName(subNameAppend.toString());
            if (prdList != null && prdList.size() > 0) {
                for (LmtAppSubPrd lmtAppSubPrdItem : prdList) {
                    if(!CmisCommonConstants.YES_NO_0.equals(lmtAppSubPrdItem.getIsPreLmt())){
                        isPre = false;
                        break;
                    }
                }
            }
            if(isPre){
                lmtAppSub.setIsPreLmt(CmisCommonConstants.YES_NO_0);
            }
//            if (prdList != null && !prdList.isEmpty()) {
//                for (LmtAppSubPrd lmtAppSubPrd : prdList) {
//                    list.add(lmtAppSubPrd.getLmtBizTypeName());
//                }
//                newName = org.apache.commons.lang.StringUtils.join(list, ",");
//            }
//            if (prdList != null && prdList.size() != 0) {
//                int isPreLmtNum = 0;
//                for (LmtAppSubPrd lmtAppSubPrd1 : prdList) {
//                    if (CmisCommonConstants.YES_NO_0.equals(lmtAppSubPrd1.getIsPreLmt())) {
//                        isPreLmtNum++;
//                    }
//                }
//                if (isPreLmtNum == list.size() && isPreLmtNum > 0) {
//                    lmtAppSub.setIsPreLmt(CmisCommonConstants.YES_NO_0);
//                }
//            }
//            lmtAppSub.setSubName(newName);
            int subCount = lmtAppSubService.update(lmtAppSub);
            if (count != 1 || subCount != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("删除授信明细情况！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }


    /**
     * @方法名称: updateLmtAppSubPrd
     * @方法描述: 根据前台传入表单数据更新授信分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map updateLmtAppSubPrd(LmtAppSubPrd lmtAppSubPrd) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {

            // TODO 保存前校验 待补充
            LmtAppSubPrd lmtAppSubPrdOrigin = lmtAppSubPrdMapper.selectByPrimaryKey(lmtAppSubPrd.getPkId());
            String subSerno = lmtAppSubPrd.getSubSerno();
            LmtAppSub lmtAppSub = lmtAppSubService.selectBySubSerno(subSerno);
            BigDecimal lmtAmt = lmtAppSub.getLmtAmt();
            BigDecimal lmtAmtSubPrd = lmtAppSubPrd.getLmtAmt();
            if (lmtAmt.compareTo(lmtAmtSubPrd) == -1) {
                rtnCode = EcbEnum.ECB010007.key;
                rtnMsg = EcbEnum.ECB010007.value;
                return rtnData;
            }
            User userInfo = SessionUtils.getUserInformation();
            lmtAppSubPrd.setUpdId(userInfo.getLoginCode());
            lmtAppSubPrd.setUpdBrId(userInfo.getOrg().getCode());
            lmtAppSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtAppSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            if (!StringUtils.isBlank(lmtAppSubPrd.getOrigiLmtAccSubPrdNo())) {
                LmtApp lmtApp = lmtAppService.selectLmtAppDataBySubSerno(subSerno);
                if (lmtApp != null) {
                    if (lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_02) || lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_04)) {
                        //  TODO 变更标识字典项未定
                        if (!lmtAppSubPrd.getOrigiLmtAccSubPrdAmt().equals(lmtAppSubPrd.getLmtAmt()) || !lmtAppSubPrd.getOrigiLmtAccSubPrdTerm().equals(lmtAppSubPrd.getLmtTerm())) {
                            lmtAppSubPrd.setChgFlag("2");
                        } else {
                            lmtAppSubPrd.setChgFlag("0");
                        }
                    } else if (lmtApp.getLmtType().equals(CmisCommonConstants.LMT_TYPE_07)) {
                        //  TODO 本次细化标识字典项未定
                        if (!lmtAppSubPrdOrigin.getLmtBizType().equals(lmtAppSubPrd.getLmtBizType())) {
                            lmtAppSubPrd.setIsCurtRefine(CmisCommonConstants.STD_ZB_YES_NO_1);
                        }
                    }
                }
            }
            LmtApp lmtApp = lmtAppService.getLmtAppBySubSerno(lmtAppSub.getSubSerno());
            if (lmtApp.getLmtTerm().compareTo(lmtAppSubPrd.getLmtTerm()) == -1 && CmisCommonConstants.YES_NO_1.equals(lmtAppSubPrd.getIsRevolvLimit())) {
                rtnCode = EcbEnum.ECB020040.key;
                rtnMsg = EcbEnum.ECB020040.value;
                return rtnData;
            }
            //更新授信适用产品数据
            log.info(String.format("更新授信适用产品信息"));
            int count = lmtAppSubPrdMapper.updateByPrimaryKey(lmtAppSubPrd);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",更新授信适用产品信息失败！");
            }
            // 当原产品层授信金额下调为0的话，对应分项层预授信额度设置为否,自动删除当前数据
            if (CmisCommonConstants.YES_NO_1.equals(lmtAppSubPrd.getIsPreLmt()) && new BigDecimal(0.00).compareTo(lmtAppSubPrd.getLmtAmt()) >= 0) {
                lmtAppSubPrd.setOprType(CmisCommonConstants.OP_TYPE_02);
                update(lmtAppSubPrd);
            }
            // 当原产品层没有预授信额度为是的数据时,也要更新当前分项的预授信额度为否
            List<LmtAppSubPrd> list = this.selectBySubSerno(lmtAppSub.getSubSerno());
            boolean isPre = true;
            StringBuilder subNameAppend = new StringBuilder();
            for(LmtAppSubPrd lmtAppSubPrdItem :list){
                subNameAppend.append(lmtAppSubPrdItem.getLmtBizTypeName()).append(",");
            }
            if(subNameAppend.length() >0 ){
                subNameAppend.deleteCharAt(subNameAppend.length()-1);
            }
            lmtAppSub.setSubName(subNameAppend.toString());
            if (list != null && list.size() > 0) {
                for (LmtAppSubPrd lmtAppSubPrdItem : list) {
                    if(!CmisCommonConstants.YES_NO_0.equals(lmtAppSubPrdItem.getIsPreLmt())){
                        isPre = false;
                        break;
                    }
                }
            }
            if(isPre){
                lmtAppSub.setIsPreLmt(CmisCommonConstants.YES_NO_0);
            }
            int subCount = lmtAppSubService.update(lmtAppSub);
            if (subCount != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("更新授信分项信息出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: queryLmtAppSubPrdByParams
     * @方法描述: 根据查询条件查询授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtAppSubPrd> selectByParams(Map queryMap) {
        log.info("通用获取分项明细校验，查询参数："+JSON.toJSONString(queryMap));
        // 不知道从何处出来的Map中会多一个dataAuth
        if(queryMap.containsKey("dataAuth")){
            queryMap.remove("dataAuth");
        }
        log.info("通用获取分项明细校验，查询参数："+JSON.toJSONString(queryMap));
        if (!MapUtils.checkExistsEmptyEnum(queryMap)) {
            return lmtAppSubPrdMapper.selectByParams(queryMap);
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
    }

    /**
     * @方法名称: copyBySubSerno
     * @方法描述: 将原授信分项下的适用授信产品挂载到生成的新分项下
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean copyBySubSerno(String originSubSerno, String currentSubSerno) {
        Map params = new HashMap();
        String originSubPrdSerno = "";
        String subPrdSerno = "";
        LmtAppSubPrd lmtAppSubPrd = new LmtAppSubPrd();
        params.put("subSerno", originSubSerno);
        List<LmtAppSubPrd> subPrdList = this.selectByParams(params);
        if (subPrdList != null && subPrdList.size() > 0) {
            for (int i = 0; i < subPrdList.size(); i++) {
                lmtAppSubPrd = subPrdList.get(i);
                originSubPrdSerno = lmtAppSubPrd.getSubPrdSerno();
                subPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
                lmtAppSubPrd.setSubPrdSerno(subPrdSerno);
                lmtAppSubPrd.setSubSerno(currentSubSerno);
                lmtAppSubPrd.setPkId(UUID.randomUUID().toString());
                lmtAppSubPrd.setIsSfcaLmt(CmisCommonConstants.STD_ZB_YES_NO_1);
                User userInfo = SessionUtils.getUserInformation();
                if (userInfo == null) {
                    throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                } else {
                    lmtAppSubPrd.setInputId(userInfo.getLoginCode());
                    lmtAppSubPrd.setInputBrId(userInfo.getOrg().getCode());
                    lmtAppSubPrd.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtAppSubPrd.setUpdId(userInfo.getLoginCode());
                    lmtAppSubPrd.setUpdBrId(userInfo.getOrg().getCode());
                    lmtAppSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtAppSubPrd.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    lmtAppSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                }
                int count = lmtAppSubPrdMapper.insert(lmtAppSubPrd);
                boolean lmtRepayPlan = lmtRepayCapPlanService.copyLmtRepayPlan(originSubPrdSerno, subPrdSerno);
                if (count != 1 || !lmtRepayPlan) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",挂载授信适用产品失败！");
                }
            }
        }
        return true;
    }


    /**
     * @方法名称: deleteBySubSerno
     * @方法描述: 根据授信分项流水号删除适用授信产品
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public boolean deleteBySubSerno(String subSerno) {
        boolean result = true;
        Map map = new HashMap();
        map.put("subSerno", subSerno);
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtAppSubPrd> list = this.selectByParams(map);
        if (list.size() == 0) {
            return result;
        }
        for (LmtAppSubPrd lmtAppSubPrd : list) {
            //如果产品为专项额度贷款
            if (lmtAppSubPrd.getLmtBizType().startsWith("14")) {
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("subSerno", subSerno);
                List<LmtAppSub> lmtAppSubList = lmtAppSubService.selectByModel(queryModel);
                LmtAppSub lmtAppSub = lmtAppSubList.get(0);
                String serno = lmtAppSub.getSerno();
                QueryModel model = new QueryModel();
                model.addCondition("serno", serno);
                model.addCondition("pageId", "zydkxx");
                List<BizMustCheckDetails> bizMustCheckDetailsList = bizMustCheckDetailsService.selectByModel(model);
                if (CollectionUtils.nonEmpty(bizMustCheckDetailsList)) {
                    BizMustCheckDetails bizMustCheckDetail = bizMustCheckDetailsList.get(0);
                    bizMustCheckDetailsService.deleteByPrimaryKey(bizMustCheckDetail.getPkId());
                }
            }
        }
        int count = lmtAppSubPrdMapper.updateBySubSerno(subSerno);
        if (count == 0) {
            //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
            throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
        }
        return result;
    }

    /**
     * @方法名称: selectBySubSerno
     * @方法描述: 根据授信分项流水号查询适用授信产品
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-05-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtAppSubPrd> selectBySubSerno(String subSerno) {
        Map map = new HashMap();
        map.put("subSerno", subSerno);
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtAppSubPrd> list = this.selectByParams(map);
        return list;
    }

    /**
     * @方法名称: selectBySubPrdSerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtAppSubPrd selectBySubPrdSerno(String subPrdSerno) {
        return lmtAppSubPrdMapper.selectBySubPrdSerno(subPrdSerno);
    }

    /**
     * @函数名称: queryLmtGrpAppSubSumByGrpSerno
     * @函数描述: 根据集团授信申请流水号获取集团分项明细
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public ResultDto<List<LmtGrpAppSubDto>> queryLmtGrpAppSubSumByGrpSerno(@RequestBody String grpSerno) {
        ArrayList<LmtGrpAppSubDto> lmtGrpAppSubDtoList = new ArrayList<>();

        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            if (lmtGrpMemRel.getSingleSerno() != null && !"".equals(lmtGrpMemRel.getSingleSerno())) {
                List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtGrpMemRel.getSingleSerno());
                for (LmtAppSub lmtAppSub : lmtAppSubList) {
                    LmtGrpAppSubDto subDto = new LmtGrpAppSubDto();
                    subDto.setCusId(lmtGrpMemRel.getCusId());
                    subDto.setCusName(lmtGrpMemRel.getCusName());
                    subDto.setSubPrdSerno(lmtAppSub.getSubSerno());
                    subDto.setOrigiLmtAccSubPrdNo(lmtAppSub.getOrigiLmtAccSubNo());
                    subDto.setLmtBizTypeName(lmtAppSub.getSubName());
                    subDto.setIsPreLmt(lmtAppSub.getIsPreLmt());
                    subDto.setGuarMode(lmtAppSub.getGuarMode());
                    subDto.setOrigiLmtAccSubPrdAmt(lmtAppSub.getOrigiLmtAccSubAmt());
                    subDto.setLmtAmt(lmtAppSub.getLmtAmt());
                    subDto.setOrigiLmtAccSubPrdTerm(lmtAppSub.getOrigiLmtAccSubTerm());
                    // subDto.setLmtTerm(lmtAppSub.getLmtTerm()); 一级分项层不需要展示期限
                    List<LmtAppSubPrd> lmtAppSubPrdList = this.selectBySubSerno(lmtAppSub.getSubSerno());
                    List<LmtGrpAppSubDto> subPrdDtoList = new ArrayList<>();
                    for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                        LmtGrpAppSubDto subPrdDto = new LmtGrpAppSubDto();
                        subPrdDto.setCusId(lmtAppSubPrd.getCusId());
                        subPrdDto.setCusName(lmtAppSubPrd.getCusName());
                        subPrdDto.setSubPrdSerno(lmtAppSubPrd.getSubPrdSerno());
                        subPrdDto.setOrigiLmtAccSubPrdNo(lmtAppSubPrd.getOrigiLmtAccSubPrdNo());
                        subPrdDto.setLmtBizType(lmtAppSubPrd.getLmtBizType());
                        subPrdDto.setLmtBizTypeName(lmtAppSubPrd.getLmtBizTypeName());
                        subPrdDto.setIsRevolvLimit(lmtAppSubPrd.getIsRevolvLimit());
                        subPrdDto.setIsPreLmt(lmtAppSubPrd.getIsPreLmt());
                        subPrdDto.setGuarMode(lmtAppSubPrd.getGuarMode());
                        subPrdDto.setOrigiLmtAccSubPrdAmt(lmtAppSubPrd.getOrigiLmtAccSubPrdAmt());
                        subPrdDto.setLmtAmt(lmtAppSubPrd.getLmtAmt());
                        subPrdDto.setOrigiLmtAccSubPrdTerm(lmtAppSubPrd.getOrigiLmtAccSubPrdTerm());
                        subPrdDto.setLmtTerm(lmtAppSubPrd.getLmtTerm());
                        subPrdDto.setChgFlag(lmtAppSubPrd.getChgFlag());
                        subPrdDto.setIsSfcaLmt(lmtAppSubPrd.getIsSfcaLmt());
                        subPrdDto.setIsCurtRefine(lmtAppSubPrd.getIsCurtRefine());
                        subPrdDtoList.add(subPrdDto);
                    }
                    subDto.setChildrenLmtGrpAppSubDtoList(subPrdDtoList);
                    lmtGrpAppSubDtoList.add(subDto);
                }
            }
        }
        // 分项中存在期限为 0  的，置为 null
//        for (int i = 0; i < lmtGrpAppSubDtoList.size(); i++) {
//            if (lmtGrpAppSubDtoList.get(i).getLmtTerm().compareTo(0) == 0) {
//                lmtGrpAppSubDtoList.get(i).setLmtTerm(null);
//            }
//        }
        return new ResultDto<>(lmtGrpAppSubDtoList);
    }

    /**
     * @方法名称: selectHighestLmtAmt
     * @方法描述: 获取分项下授信金额最大值
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BigDecimal selectHighestLmtAmt(String subSerno) {
        return lmtAppSubPrdMapper.selectHighestLmtAmt(subSerno);
    }

    /**
     * 调查报告查询本次授信分项信息
     *
     * @param model
     * @return
     */
    public List<Map> selectThisLmt(QueryModel model) {
        if (model.getCondition() == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        List<Map> list = new ArrayList<>();
        Map tempMap = null;
        String type = model.getCondition().get("type").toString();
        String serno = model.getCondition().get("serno").toString();
        List<LmtAppSub> lmtAppSubList = null;
        if ("normal".equals(type)) {
            lmtAppSubList = lmtAppSubService.getNormal(serno);
        } else if ("low".equals(type)) {
            lmtAppSubList = lmtAppSubService.getLow(serno);
        }
        if (lmtAppSubList != null && lmtAppSubList.size() > 0) {
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                //分项额度为0时押品不引入显示
                BigDecimal amt = lmtAppSub.getLmtAmt();
                if(Objects.isNull(amt)||amt.compareTo(BigDecimal.ZERO) ==0){
                    continue;
                }
                String subSerno = lmtAppSub.getSubSerno();
                List<LmtAppSubPrd> lmtAppSubPrdList = new ArrayList<>();
                QueryModel queryModel = new QueryModel();
                queryModel.getCondition().put("subSerno", subSerno);
                queryModel.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
                lmtAppSubPrdList = lmtAppSubPrdMapper.selectByModel(queryModel);
                tempMap = new HashMap();
                tempMap.put("pkId", lmtAppSub.getPkId());
                tempMap.put("serno", serno);
                tempMap.put("subPrdSerno", lmtAppSub.getSubSerno());
                tempMap.put("origiLmtAccSubPrdNo", lmtAppSub.getOrigiLmtAccSubNo());
                tempMap.put("lmtBizTypeName", lmtAppSub.getSubName());
                tempMap.put("isPreLmt", lmtAppSub.getIsPreLmt());
                tempMap.put("guarMode", lmtAppSub.getGuarMode());
                tempMap.put("origiLmtAccSubPrdAmt", lmtAppSub.getOrigiLmtAccSubAmt());
                tempMap.put("origiLmtAccSubPrdTerm", lmtAppSub.getOrigiLmtAccSubTerm());
                tempMap.put("lmtAmt", lmtAppSub.getLmtAmt());
                tempMap.put("children", lmtAppSubPrdList);
                list.add(tempMap);
            }
        }
        return list;
    }


    /**
     * @函数名称: queryDetailByLmtSerno
     * @函数描述: 根据单一客户授信申请流水号获取授信分项品种数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtAppSubPrd> queryAllLmtAppSubPrdBySerno(String serno) {
        return lmtAppSubPrdMapper.queryAllLmtAppSubPrdBySerno(serno);
    }

    /**
     * @函数名称: queryAllLmtAppSubPrdByGrpSerno
     * @函数描述: 根据集团授信申请流水号获取所有的授信分项品种数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtAppSubPrd> queryAllLmtAppSubPrdByGrpSerno(String serno) {
        return lmtAppSubPrdMapper.queryAllLmtAppSubPrdByGrpSerno(serno);
    }

    /**
     * @方法名称: queryLmtAppSubPrdByParams
     * @方法描述: 根据查询条件查询授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public int selectCountsByParams(Map queryMap) {
        return lmtAppSubPrdMapper.selectCountsByParams(queryMap);
    }

    /**
     * @方法名称: queryLmtAppSubPrdByParams
     * @方法描述: 根据查询条件查询保函授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: cainingbo
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public int selectNewCountsByParams(String subSerno) {
        return lmtAppSubPrdMapper.selectNewCountsByParams(subSerno);
    }

    /**
     * @函数名称: selectByCusId
     * @函数描述: 根据客户号查询所有的授信申请分项
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtAppSubPrd> selectByCusId(QueryModel model) {
        return lmtAppSubPrdMapper.selectByModel(model);
    }

    /**
     * 查看一般授信额度或委托贷款授信额度
     *
     * @param model
     * @return
     */
    public List<Map> getNormalOrBailLmt(QueryModel model) {
        List<Map> result = new ArrayList<>();
        String serno = model.getCondition().get("serno").toString();
        String type = model.getCondition().get("type").toString();
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
        if (CollectionUtils.nonEmpty(lmtAppSubList)) {
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                BigDecimal amt = lmtAppSub.getLmtAmt();
                if(Objects.isNull(amt)||amt.compareTo(BigDecimal.ZERO) ==0){
                    continue;
                }
                Map tempMap = new HashMap();
                Map paramMap = new HashMap();
                paramMap.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
                paramMap.put("serno",lmtAppSub.getSubSerno());
                List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdMapper.selectPrdByParams(paramMap);
                List<LmtAppSubPrd> bailAppSubPrdList = new ArrayList<>();
                if (CollectionUtils.nonEmpty(lmtAppSubPrds)) {
                    for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrds) {
                        if ("bail".equals(type)) {
                            if (lmtAppSubPrd.getLmtBizType().startsWith("14010101")) {
                                bailAppSubPrdList.add(lmtAppSubPrd);
                            }
                        }
                    }
                }
                tempMap.put("pkId", lmtAppSub.getPkId());
                tempMap.put("serno", serno);
                tempMap.put("subPrdSerno", lmtAppSub.getSubSerno());
                tempMap.put("origiLmtAccSubPrdNo", lmtAppSub.getOrigiLmtAccSubNo());
                tempMap.put("lmtBizTypeName", lmtAppSub.getSubName());
                tempMap.put("isPreLmt", lmtAppSub.getIsPreLmt());
                tempMap.put("guarMode", lmtAppSub.getGuarMode());
                tempMap.put("origiLmtAccSubPrdAmt", lmtAppSub.getOrigiLmtAccSubAmt());
                tempMap.put("origiLmtAccSubPrdTerm", lmtAppSub.getOrigiLmtAccSubTerm());
                tempMap.put("lmtAmt", lmtAppSub.getLmtAmt());
                if ("normal".equals(type)) {
                    tempMap.put("children", lmtAppSubPrds);
                    result.add(tempMap);
                } else if ("bail".equals(type)) {
                    tempMap.put("children", bailAppSubPrdList);
                    if (CollectionUtils.nonEmpty(bailAppSubPrdList)) {
                        result.add(tempMap);
                    }
                }
            }
        }

        return result;
    }

    /**
     * 根据集团申请流水号查询授信分项品种数据
     *
     * @param grpSerno
     * @return
     */
    public ArrayList<LmtAppSubPrd> queryLmtApprSubByGrpSerno(String grpSerno) {
        return lmtAppSubPrdMapper.queryLmtApprSubByGrpSerno(grpSerno);
    }

    /**
     * 根据业务流水号查询授信分项
     *
     * @param serno
     * @return
     */
    public List<LmtAppSubPrd> selectSubPrdBySerno(String serno) {
        return lmtAppSubPrdMapper.selectSubPrdBySerno(serno);
    }

    /**
     * 查看集团一般授信额度或委托贷款授信额度
     *
     * @param model
     * @return
     */
    public List<Map> getGrpNormalOrBailLmt(QueryModel model) {
        String grpSerno = model.getCondition().get("grpSerno").toString();
        String type = model.getCondition().get("type").toString();
        List<Map> result = new ArrayList<>();
        List<LmtApp> lmtAppList = lmtAppService.getLmtAppByGrpSernoIsDeclare(grpSerno);
        for (LmtApp lmtApp : lmtAppList) {
            String serno = lmtApp.getSerno();
            List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
            if (CollectionUtils.nonEmpty(lmtAppSubList)) {
                for (LmtAppSub lmtAppSub : lmtAppSubList) {
                    BigDecimal amt = lmtAppSub.getLmtAmt();
                    if(Objects.isNull(amt)||amt.compareTo(BigDecimal.ZERO) ==0){
                        continue;
                    }
                    Map tempMap = new HashMap();
                    List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdMapper.selectBySubSerno(lmtAppSub.getSubSerno());
                    List<LmtAppSubPrd> bailAppSubPrdList = new ArrayList<>();
                    if (CollectionUtils.nonEmpty(lmtAppSubPrds)) {
                        for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrds) {
                            if ("bail".equals(type)) {
                                if (lmtAppSubPrd.getLmtBizType().startsWith("14010101")) {
                                    bailAppSubPrdList.add(lmtAppSubPrd);
                                }
                            }
                        }
                    }
                    tempMap.put("pkId", lmtAppSub.getPkId());
                    tempMap.put("serno", serno);
                    tempMap.put("subPrdSerno", lmtAppSub.getSubSerno());
                    tempMap.put("cusName", lmtAppSub.getCusName());
                    tempMap.put("origiLmtAccSubPrdNo", lmtAppSub.getOrigiLmtAccSubNo());
                    tempMap.put("lmtBizTypeName", lmtAppSub.getSubName());
                    tempMap.put("isPreLmt", lmtAppSub.getIsPreLmt());
                    tempMap.put("guarMode", lmtAppSub.getGuarMode());
                    tempMap.put("origiLmtAccSubPrdAmt", lmtAppSub.getOrigiLmtAccSubAmt());
                    tempMap.put("origiLmtAccSubPrdTerm", lmtAppSub.getOrigiLmtAccSubTerm());
                    tempMap.put("lmtAmt", lmtAppSub.getLmtAmt());
                    if ("normal".equals(type)) {
                        tempMap.put("children", lmtAppSubPrds);
                        result.add(tempMap);
                    } else if ("bail".equals(type)) {
                        tempMap.put("children", bailAppSubPrdList);
                        if (CollectionUtils.nonEmpty(bailAppSubPrdList)) {
                            result.add(tempMap);
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * 根据适用品种编号获取可选产品属性
     */
    public Map getSelectPropByPrdNo(Map<String, String> map) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String returnString = "";
        String tempString = "P001,P002,P003,P004,P005,P006,P007,P008,P009,P010,P011,P013,P014,P015,P016,P017,P018,P019,P020,P021,P022,P023,P024,P025,P026,P027,P028,P029,P030,P031,P032,P033,P034,P035,P036,P037,P038,P039";
        String[] attr = tempString.split(",");
        List<String> list = new ArrayList<>();
        Collections.addAll(list, attr);
        CmisLmt0042ReqDto cmisLmt0042ReqDto = new CmisLmt0042ReqDto();
        cmisLmt0042ReqDto.setLimitSubNo(map.get("prdNo"));
        ResultDto<CmisLmt0042RespDto> resultDto = cmisLmtClientService.cmislmt0042(cmisLmt0042ReqDto);
        if (resultDto.getCode().equals("0")) {
            CmisLmt0042RespDto cmisLmt0042RespDto = resultDto.getData();
            List<CmisLmt0042ListRespDto> listRep = cmisLmt0042RespDto.getCfgPrdTypePropertiesList();
            if (listRep != null && listRep.size() > 0) {
                for (CmisLmt0042ListRespDto cmisLmt0042ListRespDto : listRep) {
                    //list.add(cmisLmt0042ListRespDto.getTypePropNo());
                    String prdNo = cmisLmt0042ListRespDto.getTypePropNo();
                    if (list.contains(prdNo)) {
                        list.remove(prdNo);
                    }
                }
                returnString = String.join(",", list);
                log.info("该产品适用品种可选产品属性不包括" + returnString);
            }
        }
        rtnData.put("rtnData", returnString);
        return rtnData;
    }


    /**
     * @方法名称: judgePrdByParams
     * @方法描述: 根据查询条件查询授信申请数据中的产品
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtAppSubPrd> judgePrdByParams(Map queryMap) {
        if (!MapUtils.checkExistsEmptyEnum(queryMap)) {
            return lmtAppSubPrdMapper.selectPrdByParams(queryMap);
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
    }

    /**
     * @方法名称: selectLmtAppSubPrdByParams
     * @方法描述: 根据申请流水获取产品分项明细
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtAppSubPrd> selectLmtAppSubPrdByParams(Map queryMap) {
        // 不知道从何处出来的Map中会多一个dataAuth
        if(queryMap.containsKey("dataAuth")){
            queryMap.remove("dataAuth");
        }
        if (!MapUtils.checkExistsEmptyEnum(queryMap)) {
            String serno = queryMap.containsKey("serno") ? queryMap.get("serno").toString() : StringUtils.EMPTY;
            List<String> subSernos = lmtAppPrdMapper.getSubSernoBySerno(serno);

            List<LmtAppSubPrd> result = new ArrayList<>();
            if(CollectionUtils.nonEmpty(subSernos)){
                queryMap.put("subSernos",subSernos);
                result = lmtAppSubPrdMapper.selectBySubSernos(queryMap);
            }
            return result;
            //return lmtAppSubPrdMapper.selectLmtAppSubPrdByParams(queryMap);
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
    }

    /**
     * 获取小微对公流程路由条件
     *
     * @param serno
     * @return
     */
    public Map getGuarModeDetail(String serno) {
        Map result = new HashMap();
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
        //信用申请额度
        BigDecimal xyLmtAmt = BigDecimal.ZERO;
        //保证申请额度
        BigDecimal bzLmtAmt = BigDecimal.ZERO;
        //理财 存单质押申请额度
        BigDecimal cdlcLmtAmt = BigDecimal.ZERO;
        //其他抵质押申请额度
        BigDecimal pldLmtAmt = BigDecimal.ZERO;

        log.info("*******************获取担保类型细分额度开始,流水号:{}*******************", serno);
        if (CollectionUtils.nonEmpty(lmtAppSubList)) {
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                String subSerno = lmtAppSub.getSubSerno();
                String guarMode = lmtAppSub.getGuarMode();
                BigDecimal lmtAmt = lmtAppSub.getLmtAmt();
                if (CmisCommonConstants.GUAR_MODE_00.equals(guarMode)) {
                    xyLmtAmt = xyLmtAmt.add(lmtAmt);
                } else if (CmisCommonConstants.GUAR_MODE_30.equals(guarMode)) {
                    bzLmtAmt = bzLmtAmt.add(lmtAmt);
                } else if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode)) {
                    List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdMapper.selectBySubSerno(subSerno);
                    if (CollectionUtils.nonEmpty(lmtAppSubPrds)) {
                        for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrds) {
                            String subPrdSerno = lmtAppSubPrd.getSubPrdSerno();
                            QueryModel model = new QueryModel();
                            model.addCondition("subPrdSerno", subPrdSerno);
                            model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                            List<LmtSubPrdLowGuarRel> lmtSubPrdLowGuarRels = lmtSubPrdLowGuarRelService.selectByModel(model);
                            if (CollectionUtils.nonEmpty(lmtSubPrdLowGuarRels)) {
                                for (LmtSubPrdLowGuarRel lmtSubPrdLowGuarRel : lmtSubPrdLowGuarRels) {
                                    if (CmisBizConstants.STD_ZB_DB_DETAIL_10.equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail()) || CmisBizConstants.STD_ZB_DB_DETAIL_14.equals(lmtSubPrdLowGuarRel.getLowGuarModeDetail())) {
                                        cdlcLmtAmt = cdlcLmtAmt.add(lmtSubPrdLowGuarRel.getLmtAmt());
                                    }
                                }
                            }
                        }
                    }
                } else if (CmisCommonConstants.GUAR_MODE_10.equals(guarMode) || CmisCommonConstants.GUAR_MODE_20.equals(guarMode)) {
                    pldLmtAmt = pldLmtAmt.add(lmtAmt);
                }
            }
        }
        log.info("*******************获取担保类型细分额度结束,流水号:{}*******************", serno);
        result.put("xyLmtAmt", xyLmtAmt);
        result.put("bzLmtAmt", bzLmtAmt);
        result.put("cdlcLmtAmt", cdlcLmtAmt);
        result.put("pldLmtAmt", pldLmtAmt);
        return result;
    }
}
