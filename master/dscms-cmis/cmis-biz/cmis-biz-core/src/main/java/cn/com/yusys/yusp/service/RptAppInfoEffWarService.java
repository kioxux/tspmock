/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.LmtGrpMemRel;
import cn.com.yusys.yusp.domain.bat.BatBizRiskSign;
import cn.com.yusys.yusp.dto.RptAppInfoEffWarDto;
import cn.com.yusys.yusp.service.bat.BatBizRiskSignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptAppInfoEffWar;
import cn.com.yusys.yusp.repository.mapper.RptAppInfoEffWarMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptAppInfoEffWarService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 21:11:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptAppInfoEffWarService {

    @Autowired
    private RptAppInfoEffWarMapper rptAppInfoEffWarMapper;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private BatBizRiskSignService batBizRiskSignService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptAppInfoEffWar selectByPrimaryKey(String pkId) {
        return rptAppInfoEffWarMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptAppInfoEffWar> selectAll(QueryModel model) {
        List<RptAppInfoEffWar> records = (List<RptAppInfoEffWar>) rptAppInfoEffWarMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptAppInfoEffWar> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptAppInfoEffWar> list = rptAppInfoEffWarMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptAppInfoEffWar record) {
        return rptAppInfoEffWarMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptAppInfoEffWar record) {
        return rptAppInfoEffWarMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptAppInfoEffWar record) {
        return rptAppInfoEffWarMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptAppInfoEffWar record) {
        return rptAppInfoEffWarMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptAppInfoEffWarMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptAppInfoEffWarMapper.deleteByIds(ids);
    }

    /**
     * 查询集团有效预警信息
     */
    public List<RptAppInfoEffWarDto> selectGrpEffWar(QueryModel model) {
        String grpSerno = model.getCondition().get("grpSerno").toString();
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        List<RptAppInfoEffWarDto> result = new ArrayList<>();
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            String serno = lmtGrpMemRel.getSingleSerno();
            String cusName = lmtGrpMemRel.getCusName();
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("serno", serno);
            List<RptAppInfoEffWar> rptAppInfoEffWars = rptAppInfoEffWarMapper.selectByModel(queryModel);
            if (rptAppInfoEffWars != null && rptAppInfoEffWars.size() > 0) {
                for (int i = 0; i < rptAppInfoEffWars.size(); i++) {
                    RptAppInfoEffWar rptAppInfoEffWar = rptAppInfoEffWars.get(i);
                    RptAppInfoEffWarDto rptAppInfoEffWarDto = new RptAppInfoEffWarDto();
                    rptAppInfoEffWarDto.setPkId(rptAppInfoEffWar.getPkId());
                    rptAppInfoEffWarDto.setSerno(rptAppInfoEffWar.getSerno());
                    rptAppInfoEffWarDto.setCusName(cusName);
                    if (null != rptAppInfoEffWar.getAltOutDesc()) {
                        rptAppInfoEffWarDto.setAltOutDesc(rptAppInfoEffWar.getAltOutDesc());
                    }
                    if (null != rptAppInfoEffWar.getAltRiskEffectCus()) {
                        rptAppInfoEffWarDto.setAltRiskEffectCus(rptAppInfoEffWar.getAltRiskEffectCus());
                    }
                    if (null != rptAppInfoEffWar.getAltRiskLvl()) {
                        rptAppInfoEffWarDto.setAltRiskLvl(rptAppInfoEffWar.getAltRiskLvl());
                    }
                    if (null != rptAppInfoEffWar.getAltSubType()) {
                        rptAppInfoEffWarDto.setAltSubType(rptAppInfoEffWar.getAltSubType());
                    }
                    if (null != rptAppInfoEffWar.getAltTime()) {
                        rptAppInfoEffWarDto.setAltTime(rptAppInfoEffWar.getAltTime());
                    }
                    if (null != rptAppInfoEffWar.getAltType()) {
                        rptAppInfoEffWarDto.setAltType(rptAppInfoEffWar.getAltType());
                    }
                    result.add(rptAppInfoEffWarDto);
                }
            }
        }
        return result;
    }
    public List<RptAppInfoEffWar> initEffWar(QueryModel model){
        String serno = model.getCondition().get("serno").toString();
        String cusId = model.getCondition().get("cusId").toString();
        List<RptAppInfoEffWar> result = new ArrayList<>();
        List<RptAppInfoEffWar> rptAppInfoEffWars = rptAppInfoEffWarMapper.selectBySerno(serno);
        if(CollectionUtils.nonEmpty(rptAppInfoEffWars)){
            return rptAppInfoEffWars;
        }else {
            List<BatBizRiskSign> batBizRiskSigns = batBizRiskSignService.selectRiskByCustId(cusId);
            if(CollectionUtils.nonEmpty(batBizRiskSigns)){
                for (BatBizRiskSign batBizRiskSign : batBizRiskSigns) {
                    RptAppInfoEffWar rptAppInfoEffWar = new RptAppInfoEffWar();
                    rptAppInfoEffWar.setPkId(StringUtils.getUUID());
                    rptAppInfoEffWar.setSerno(serno);
                    rptAppInfoEffWar.setAltTime(batBizRiskSign.getRiskDate());
                    rptAppInfoEffWar.setAltType(batBizRiskSign.getRiskBig());
                    rptAppInfoEffWar.setAltSubType(batBizRiskSign.getRiskSub());
                    rptAppInfoEffWar.setAltOutDesc(batBizRiskSign.getRiskOutReasion());
                    rptAppInfoEffWar.setAltRiskLvl(batBizRiskSign.getRiskLvl());
                    rptAppInfoEffWarMapper.insert(rptAppInfoEffWar);
                    result.add(rptAppInfoEffWar);
                }
            }
        }
        return result;
    }
}
