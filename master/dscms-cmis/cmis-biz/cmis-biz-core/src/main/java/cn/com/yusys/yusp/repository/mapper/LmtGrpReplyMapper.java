/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.LmtGrpReplyChg;
import cn.com.yusys.yusp.domain.LmtReply;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtGrpReply;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-20 11:06:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtGrpReplyMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtGrpReply selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtGrpReply> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtGrpReply record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtGrpReply record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtGrpReply record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtGrpReply record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称：getOriginReplyNo
     * @方法描述：根据台账批复号查批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-04-20 下午 1:47
     * @修改记录：修改时间   修改人员  修改原因
     */
    LmtGrpReply getReply(@Param("grpReplySerno") String grpReplySerno);


    /**
     * @方法名称：getReplyNO
     * @方法描述：用原复号获取历史批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-04-20 上午 10:57
     * @修改记录：修改时间   修改人员  修改原因
     */
    LmtGrpReply getOriginReply(@Param("originGrpReplySerno") String originReplySerno);

    /**
     * @方法名称：selectForChangeReply
     * @方法描述：查可变更的批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-04-25 下午 5:39
     * @修改记录：修改时间   修改人员  修改原因
     */
    List<LmtGrpReply> selectForChangeReply(QueryModel queryModel);

    /**
     * @方法名称: selectByParams
     * @方法描述: 根据批复编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtGrpReply> selectByParams(Map map);

    /**
     * @方法名称：getReplyByReplyNo
     * @方法描述：根据条件查询授信批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-05-4 上午 8:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    List<LmtGrpReply> queryLmtReplyDataByParams(HashMap<String, String> paramMap);

    /**
     * @方法名称：queryUnchangeableReply
     * @方法描述：查可不变更的批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：qw
     * @创建时间：2021-05-21 下午 2:49
     * @修改记录：修改时间   修改人员  修改原因
     */
    List<LmtGrpReply> queryUnchangeableReply(QueryModel queryModel);

    /**
     * @方法名称：viewLmtGrpReplyByGrpReplySerno
     * @方法描述：集团授信原批复查看
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-07-5 下午 9:07
     * @修改记录：修改时间   修改人员  修改原因
     */

    LmtGrpReply viewLmtGrpReplyByGrpReplySerno(String grpReplySerno);
}