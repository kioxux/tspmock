package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @className XTGL02BizService
 * @Description 空白凭证修改作废审批流程
 * @author dumd
 * @Date 2020/08/20
 */
@Service
public class XTGL02BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(XTGL02BizService.class);//定义log

    @Autowired
    private BlankCertiInfoService blankCertiInfoService;

    @Autowired
    private BlankCertiModifyAppService blankCertiModifyAppService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String logPrefix = resultInstanceDto.getFlowName()+serno+"流程操作:";
        log.info(logPrefix + currentOpType+"后业务处理");
        String bizType = resultInstanceDto.getBizType();

        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程提交操作，流程参数："+ resultInstanceDto);
                blankCertiModifyAppService.handleBusinessAfterStart(serno,bizType);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数："+ resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数："+ resultInstanceDto);
                blankCertiModifyAppService.handleBusinessAfterEnd(serno,bizType);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数："+ resultInstanceDto);
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    blankCertiModifyAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数："+ resultInstanceDto);
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    blankCertiModifyAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数："+ resultInstanceDto);
                blankCertiModifyAppService.handleBusinessAfterRefuse(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.XTGL02.equals(flowCode);
    }
}