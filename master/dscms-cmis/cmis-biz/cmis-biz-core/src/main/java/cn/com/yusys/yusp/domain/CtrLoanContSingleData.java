/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLoanCont
 * @类描述: ctr_loan_cont数据实体类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-05-07 19:19:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "ctr_loan_cont")
public class CtrLoanContSingleData extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 表单 **/
	@Column(name = "CTR_LOAN_CONT")
	private CtrLoanCont ctrLoanCont;

	/** liebiao **/
	@Column(name = "GUAR_CONT")
	private List<GrtGuarCont> guarCont;

	/**
	 * @param ctrLoanCont
	 */
	public void setCtrLoanCont(CtrLoanCont ctrLoanCont) {
		this.ctrLoanCont = ctrLoanCont;
	}

	/**
	 * @return ctrLoanCont
	 */
	public CtrLoanCont getCtrLoanCont() {
		return this.ctrLoanCont;
	}

	/**
	 * @param guarCont
	 */
	public void setGuarCont(List<GrtGuarCont> guarCont) {
		this.guarCont = guarCont;
	}

	/**
	 * @return guarCont
	 */
	public List<GrtGuarCont> getGuarCont() {
		return this.guarCont;
	}


}