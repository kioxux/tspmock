/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.dto.OtherAppDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.OtherRecordSpecialLoanApp;
import cn.com.yusys.yusp.repository.mapper.OtherRecordSpecialLoanAppMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherRecordSpecialLoanAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xiaomei
 * @创建时间: 2021-06-10 13:47:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherRecordSpecialLoanAppService {
    private static final Logger log = LoggerFactory.getLogger(OtherRecordSpecialLoanAppService.class);

    @Autowired
    private WorkflowCoreClient workflowCoreClient;


    @Autowired
    private OtherRecordSpecialLoanAppMapper otherRecordSpecialLoanAppMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public OtherRecordSpecialLoanApp selectByPrimaryKey(String serno) {
        return otherRecordSpecialLoanAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherRecordSpecialLoanApp> selectAll(QueryModel model) {
        List<OtherRecordSpecialLoanApp> records = (List<OtherRecordSpecialLoanApp>) otherRecordSpecialLoanAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<OtherRecordSpecialLoanApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherRecordSpecialLoanApp> list = otherRecordSpecialLoanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(OtherRecordSpecialLoanApp record) {
        return otherRecordSpecialLoanAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(OtherRecordSpecialLoanApp record) {
        return otherRecordSpecialLoanAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(OtherRecordSpecialLoanApp record) {
        return otherRecordSpecialLoanAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(OtherRecordSpecialLoanApp record) {
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return otherRecordSpecialLoanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return otherRecordSpecialLoanAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherRecordSpecialLoanAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteInfo
     * @方法描述: 根据主键将操作类型置为删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteInfo(String serno) {
        OtherRecordSpecialLoanApp otherRecordSpecialLoanApp = otherRecordSpecialLoanAppMapper.selectByPrimaryKey(serno);

        if (otherRecordSpecialLoanApp == null){
            throw BizException.error(null, "999999", "删除：用信审核备案申请异常");
        }

        if(CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherRecordSpecialLoanApp.getApproveStatus())){
            otherRecordSpecialLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            otherRecordSpecialLoanApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            //TODO 流程否决结束 2021-05-18
            log.info("授信申请流程删除 bizId: {}",otherRecordSpecialLoanApp.getSerno());
            workflowCoreClient.deleteByBizId(otherRecordSpecialLoanApp.getSerno());
            int i = otherRecordSpecialLoanAppMapper.updateByPrimaryKeySelective(otherRecordSpecialLoanApp);
            if (i != 1) {
                throw BizException.error(null, "999999", "删除：用信审核备案申请异常");
            }
            return i;
        }else {
            //操作类型  删除
            otherRecordSpecialLoanApp.setOprType(CmisBizConstants.OPR_TYPE_02);
            //转换日期 ASSURE_CERT_CODE
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            User user = SessionUtils.getUserInformation();
            otherRecordSpecialLoanApp.setUpdateTime(DateUtils.getCurrDate());
            otherRecordSpecialLoanApp.setUpdDate(dateFormat.format(new Date()));
            otherRecordSpecialLoanApp.setUpdId(user.getLoginCode());
            otherRecordSpecialLoanApp.setUpdBrId(user.getOrg().getCode());

            int i = otherRecordSpecialLoanAppMapper.updateByPrimaryKeySelective(otherRecordSpecialLoanApp);
            if (i != 1) {
                throw BizException.error(null, "999999", "删除：用信审核备案申请异常");
            }

            return i;
        }
    }

    /**
     * @方法名称: getotherrecordspecialloanapp
     * @方法描述: 根据入参获取当前客户经理名下用信审核备案申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherRecordSpecialLoanApp> getOtherRecordSpecialLoanAppByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("apprStatuss",CmisCommonConstants.WF_STATUS_000+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_111
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_990+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_991
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_992+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_993);
        List<OtherRecordSpecialLoanApp> list = otherRecordSpecialLoanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getotherrecordspecialloanapphis
     * @方法描述: 根据入参获取当前客户经理名下用信审核备案申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherRecordSpecialLoanApp> getOtherRecordSpecialLoanAppHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("apprStatuss",CmisCommonConstants.WF_STATUS_996+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_997
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_998
        );
        List<OtherRecordSpecialLoanApp> list = otherRecordSpecialLoanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: addotherrecordspecialloanapp
     * @方法描述: 新增用信审核备案申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto addotherrecordspecialloanapp(OtherRecordSpecialLoanApp otherrecordspecialloanapp) {
        {
            OtherRecordSpecialLoanApp otherRecordSpecialLoanApp = new OtherRecordSpecialLoanApp();

            try {
                //克隆对象
                BeanUtils.copyProperties(otherrecordspecialloanapp, otherRecordSpecialLoanApp);

                //补充 克隆缺少数据
                //放入必填参数 操作类型 新增
                otherRecordSpecialLoanApp.setOprType(CmisBizConstants.OPR_TYPE_01);
                //审批状态
                otherRecordSpecialLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
                //时间 登记人 等级机构相关

                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherRecordSpecialLoanApp.setManagerBrId(user.getOrg().getCode());
                otherRecordSpecialLoanApp.setManagerId(user.getLoginCode());
                otherRecordSpecialLoanApp.setInputId(user.getLoginCode());
                otherRecordSpecialLoanApp.setInputBrId(user.getOrg().getCode());
                otherRecordSpecialLoanApp.setInputDate(dateFormat.format(new Date()) );
                otherRecordSpecialLoanApp.setCreateTime(DateUtils.getCurrDate());
                otherRecordSpecialLoanApp.setUpdateTime(DateUtils.getCurrDate());
                otherRecordSpecialLoanApp.setUpdDate(dateFormat.format(new Date()) );
                otherRecordSpecialLoanApp.setUpdId(user.getLoginCode());
                otherRecordSpecialLoanApp.setUpdBrId(user.getOrg().getCode());

                int i = otherRecordSpecialLoanAppMapper.insertSelective(otherRecordSpecialLoanApp);
                if (i != 1) {
                    throw BizException.error(null, "999999", "新增：用信审核备案申请异常");
                }

            }catch (Exception e) {
                log.error("用信审核备案申请新增异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherRecordSpecialLoanApp);

        }
    }

    /**
     * @方法名称: updateotherrecordspecialloanapp
     * @方法描述: 修改用信审核备案申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto updateotherrecordspecialloanapp(OtherRecordSpecialLoanApp otherrecordspecialloanapp) {
        {
            OtherRecordSpecialLoanApp otherRecordSpecialLoanApp = new OtherRecordSpecialLoanApp();

            try {
                //克隆对象
                BeanUtils.copyProperties(otherrecordspecialloanapp, otherRecordSpecialLoanApp);

                //补充 克隆缺少数据
                OtherRecordSpecialLoanApp oldotherRecordSpecialLoanApp = otherRecordSpecialLoanAppMapper.selectByPrimaryKey(otherRecordSpecialLoanApp.getSerno());
                //放入必填参数 操作类型 新增
                otherRecordSpecialLoanApp.setOprType(oldotherRecordSpecialLoanApp.getOprType());
                //审批状态
                otherRecordSpecialLoanApp.setApproveStatus(oldotherRecordSpecialLoanApp.getApproveStatus());
                //时间 登记人 等级机构相关

                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherRecordSpecialLoanApp.setUpdateTime(DateUtils.getCurrDate());
                otherRecordSpecialLoanApp.setUpdDate(dateFormat.format(new Date()) );
                otherRecordSpecialLoanApp.setUpdId(user.getLoginCode());
                otherRecordSpecialLoanApp.setUpdBrId(user.getOrg().getCode());

                int i = otherRecordSpecialLoanAppMapper.updateByPrimaryKeySelective(otherRecordSpecialLoanApp);
                if (i != 1) {
                    throw BizException.error(null, "999999", "修改：用信审核备案申请异常");
                }

            }catch (Exception e) {
                log.error("用信审核备案修改异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherRecordSpecialLoanApp);

        }
    }

    /**
     * 更新审批状态
     *
     * @author lyh
     */
    public int updateApproveStatus(String serno, String approveStatus) {
        OtherRecordSpecialLoanApp otherRecordSpecialLoanApp = new OtherRecordSpecialLoanApp();
        otherRecordSpecialLoanApp.setSerno(serno);
        otherRecordSpecialLoanApp.setApproveStatus(approveStatus);
        return updateSelective(otherRecordSpecialLoanApp);
    }

    /**
     * 根据流水号查询其他事项申报数据
     *
     * @author css
     */

    public OtherAppDto selectOtherAppDtoDataByParam(Map map) {
        return otherRecordSpecialLoanAppMapper.selectOtherAppDtoDataByParam(map);
    }

    /**
     * @方法名称: selectByLimitSubNo
     * @方法描述: 根据授信分项编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherRecordSpecialLoanApp> selectByLimitSubNo(Map params) {
        String  limitSubNo = (String) params.get("limitSubNo");
        if(null!=limitSubNo || !"".equals(limitSubNo)) {
            return otherRecordSpecialLoanAppMapper.selectByLimitSubNo(params);
        }else {
            throw new RuntimeException("Oops: limitSubNo can't be null!" );
        }
    }
}
