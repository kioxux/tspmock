package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AllAccContDto implements Serializable {

    /** 主键 **/
    private String pkId;

    /** 放款流水号 **/
    private String pvpSerno;

    /** 逾期余额 **/
    private java.math.BigDecimal overdueBalance;

    /** 基准利率 **/
    private java.math.BigDecimal rulingIr;

    /** 借据编号 **/
    private String billNo;

    /** 台账类型 **/
    private String billType;

    /** 合同编号 **/
    private String contNo;

    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 产品编号 **/
    private String prdId;

    /** 产品名称 **/
    private String prdName;

    /** 担保方式 **/
    private String guarMode;

    /** 贷款形式 **/
    private String loanModal;

    /** 贷款发放币种 **/
    private String contCurType;

    /** 汇率 **/
    private java.math.BigDecimal exchangeRate;

    /** 贷款金额 **/
    private java.math.BigDecimal loanAmt;

    /** 贷款余额 **/
    private java.math.BigDecimal loanBalance;

    /** 折合人民币金额 **/
    private java.math.BigDecimal exchangeRmbAmt;

    /** 折合人民币余额 **/
    private java.math.BigDecimal exchangeRmbBal;

    /** 表内欠息 **/
    private java.math.BigDecimal innerDebitInterest;

    /** 表外欠息 **/
    private java.math.BigDecimal offDebitInterest;

    /** 贷款起始日 **/
    private String loanStartDate;

    /** 贷款到期日 **/
    private String loanEndDate;

    /** 贷款期限 **/
    private String loanTerm;

    /** 贷款期限单位 **/
    private String loanTermUnit;

    /** 展期次数 **/
    private String extTimes;

    /** 逾期天数 **/
    private String overdueDay;

    /** 逾期期数 **/
    private String overdueTimes;

    /** 结清日期 **/
    private String settlDate;

    /** 利率调整方式 **/
    private String rateAdjMode;

    /** 是否分段计息 **/
    private String isSegInterest;

    /** LPR授信利率区间 **/
    private String lprRateIntval;

    /** 当前LPR利率 **/
    private java.math.BigDecimal curtLprRate;

    /** 浮动点数 **/
    private java.math.BigDecimal rateFloatPoint;

    /** 执行年利率 **/
    private java.math.BigDecimal execRateYear;

    /** 逾期利率浮动比 **/
    private java.math.BigDecimal overdueRatePefloat;

    /** 逾期执行利率(年利率) **/
    private java.math.BigDecimal overdueExecRate;

    /** 复息利率浮动比 **/
    private java.math.BigDecimal ciRatePefloat;

    /** 复息执行利率(年利率) **/
    private java.math.BigDecimal ciExecRate;

    /** 利率调整选项 **/
    private String rateAdjType;

    /** 下一次利率调整间隔 **/
    private String nextRateAdjInterval;

    /** 下一次利率调整间隔单位 **/
    private String nextRateAdjUnit;

    /** 第一次调整日 **/

    /** 还款方式 **/
    private String repayMode;

    /** 结息间隔周期 **/
    private String eiIntervalCycle;

    /** 结息间隔周期单位 **/
    private String eiIntervalUnit;

    /** 扣款方式 **/
    private String deductType;

    /** 扣款日 **/
    private String deductDay;

    /** 贷款发放账号 **/
    private String loanPayoutAccno;

    /** 贷款发放账号子序号 **/
    private String loanPayoutSubNo;

    /** 发放账号名称 **/
    private String payoutAcctName;

    /** 是否受托支付 **/
    private String isBeEntrustedPay;

    /** 贷款还款账号 **/
    private String repayAccno;

    /** 贷款还款账户子序号 **/
    private String repaySubAccno;

    /** 还款账户名称 **/
    private String repayAcctName;

    /** 贷款投向 **/
    private String loanTer;

    /** 借款用途类型 **/
    private String loanUseType;

    /** 台账状态 **/
    private String accStatus;

    /** 操作类型 **/
    private String oprType;

    /** 主管客户经理 **/
    private String managerId;

    /** 主管机构 **/
    private String managerBrId;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public BigDecimal getOverdueBalance() {
        return overdueBalance;
    }

    public void setOverdueBalance(BigDecimal overdueBalance) {
        this.overdueBalance = overdueBalance;
    }

    public BigDecimal getRulingIr() {
        return rulingIr;
    }

    public void setRulingIr(BigDecimal rulingIr) {
        this.rulingIr = rulingIr;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getContCurType() {
        return contCurType;
    }

    public void setContCurType(String contCurType) {
        this.contCurType = contCurType;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getExchangeRmbAmt() {
        return exchangeRmbAmt;
    }

    public void setExchangeRmbAmt(BigDecimal exchangeRmbAmt) {
        this.exchangeRmbAmt = exchangeRmbAmt;
    }

    public BigDecimal getExchangeRmbBal() {
        return exchangeRmbBal;
    }

    public void setExchangeRmbBal(BigDecimal exchangeRmbBal) {
        this.exchangeRmbBal = exchangeRmbBal;
    }

    public BigDecimal getInnerDebitInterest() {
        return innerDebitInterest;
    }

    public void setInnerDebitInterest(BigDecimal innerDebitInterest) {
        this.innerDebitInterest = innerDebitInterest;
    }

    public BigDecimal getOffDebitInterest() {
        return offDebitInterest;
    }

    public void setOffDebitInterest(BigDecimal offDebitInterest) {
        this.offDebitInterest = offDebitInterest;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getLoanTermUnit() {
        return loanTermUnit;
    }

    public void setLoanTermUnit(String loanTermUnit) {
        this.loanTermUnit = loanTermUnit;
    }

    public String getExtTimes() {
        return extTimes;
    }

    public void setExtTimes(String extTimes) {
        this.extTimes = extTimes;
    }

    public String getOverdueDay() {
        return overdueDay;
    }

    public void setOverdueDay(String overdueDay) {
        this.overdueDay = overdueDay;
    }

    public String getOverdueTimes() {
        return overdueTimes;
    }

    public void setOverdueTimes(String overdueTimes) {
        this.overdueTimes = overdueTimes;
    }

    public String getSettlDate() {
        return settlDate;
    }

    public void setSettlDate(String settlDate) {
        this.settlDate = settlDate;
    }

    public String getRateAdjMode() {
        return rateAdjMode;
    }

    public void setRateAdjMode(String rateAdjMode) {
        this.rateAdjMode = rateAdjMode;
    }

    public String getIsSegInterest() {
        return isSegInterest;
    }

    public void setIsSegInterest(String isSegInterest) {
        this.isSegInterest = isSegInterest;
    }

    public String getLprRateIntval() {
        return lprRateIntval;
    }

    public void setLprRateIntval(String lprRateIntval) {
        this.lprRateIntval = lprRateIntval;
    }

    public BigDecimal getCurtLprRate() {
        return curtLprRate;
    }

    public void setCurtLprRate(BigDecimal curtLprRate) {
        this.curtLprRate = curtLprRate;
    }

    public BigDecimal getRateFloatPoint() {
        return rateFloatPoint;
    }

    public void setRateFloatPoint(BigDecimal rateFloatPoint) {
        this.rateFloatPoint = rateFloatPoint;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public BigDecimal getOverdueRatePefloat() {
        return overdueRatePefloat;
    }

    public void setOverdueRatePefloat(BigDecimal overdueRatePefloat) {
        this.overdueRatePefloat = overdueRatePefloat;
    }

    public BigDecimal getOverdueExecRate() {
        return overdueExecRate;
    }

    public void setOverdueExecRate(BigDecimal overdueExecRate) {
        this.overdueExecRate = overdueExecRate;
    }

    public BigDecimal getCiRatePefloat() {
        return ciRatePefloat;
    }

    public void setCiRatePefloat(BigDecimal ciRatePefloat) {
        this.ciRatePefloat = ciRatePefloat;
    }

    public BigDecimal getCiExecRate() {
        return ciExecRate;
    }

    public void setCiExecRate(BigDecimal ciExecRate) {
        this.ciExecRate = ciExecRate;
    }

    public String getRateAdjType() {
        return rateAdjType;
    }

    public void setRateAdjType(String rateAdjType) {
        this.rateAdjType = rateAdjType;
    }

    public String getNextRateAdjInterval() {
        return nextRateAdjInterval;
    }

    public void setNextRateAdjInterval(String nextRateAdjInterval) {
        this.nextRateAdjInterval = nextRateAdjInterval;
    }

    public String getNextRateAdjUnit() {
        return nextRateAdjUnit;
    }

    public void setNextRateAdjUnit(String nextRateAdjUnit) {
        this.nextRateAdjUnit = nextRateAdjUnit;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getEiIntervalCycle() {
        return eiIntervalCycle;
    }

    public void setEiIntervalCycle(String eiIntervalCycle) {
        this.eiIntervalCycle = eiIntervalCycle;
    }

    public String getEiIntervalUnit() {
        return eiIntervalUnit;
    }

    public void setEiIntervalUnit(String eiIntervalUnit) {
        this.eiIntervalUnit = eiIntervalUnit;
    }

    public String getDeductType() {
        return deductType;
    }

    public void setDeductType(String deductType) {
        this.deductType = deductType;
    }

    public String getDeductDay() {
        return deductDay;
    }

    public void setDeductDay(String deductDay) {
        this.deductDay = deductDay;
    }

    public String getLoanPayoutAccno() {
        return loanPayoutAccno;
    }

    public void setLoanPayoutAccno(String loanPayoutAccno) {
        this.loanPayoutAccno = loanPayoutAccno;
    }

    public String getLoanPayoutSubNo() {
        return loanPayoutSubNo;
    }

    public void setLoanPayoutSubNo(String loanPayoutSubNo) {
        this.loanPayoutSubNo = loanPayoutSubNo;
    }

    public String getPayoutAcctName() {
        return payoutAcctName;
    }

    public void setPayoutAcctName(String payoutAcctName) {
        this.payoutAcctName = payoutAcctName;
    }

    public String getIsBeEntrustedPay() {
        return isBeEntrustedPay;
    }

    public void setIsBeEntrustedPay(String isBeEntrustedPay) {
        this.isBeEntrustedPay = isBeEntrustedPay;
    }

    public String getRepayAccno() {
        return repayAccno;
    }

    public void setRepayAccno(String repayAccno) {
        this.repayAccno = repayAccno;
    }

    public String getRepaySubAccno() {
        return repaySubAccno;
    }

    public void setRepaySubAccno(String repaySubAccno) {
        this.repaySubAccno = repaySubAccno;
    }

    public String getRepayAcctName() {
        return repayAcctName;
    }

    public void setRepayAcctName(String repayAcctName) {
        this.repayAcctName = repayAcctName;
    }

    public String getLoanTer() {
        return loanTer;
    }

    public void setLoanTer(String loanTer) {
        this.loanTer = loanTer;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
