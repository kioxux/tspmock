package cn.com.yusys.yusp.service.server.xdcz0023;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusIndivAllDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0023.req.Xdcz0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0023.resp.Xdcz0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.PvpJxhjRepayLoanService;
import cn.com.yusys.yusp.service.PvpLoanAppRepayBillRellService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.service.client.bsp.core.ln3100.Ln3100Service;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.cus.queryAllCusIndiv.QueryAllCusIndivService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;

/**
 * 接口处理类:小企业无还本续贷放款
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdcz0023Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0023Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private PvpLoanAppRepayBillRelMapper pvpLoanAppRepayBillRelMapper;

    @Autowired
    private PvpAuthorizeMapper pvpAuthorizeMapper;

    @Autowired
    private CommonService commonService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private PrdRateStdMapper prdRateStdMapper;

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private Ln3100Service ln3100Service;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private PvpJxhjRepayLoanService pvpJxhjRepayLoanService;

    @Autowired
    private PvpLoanAppRepayBillRellMapper pvpLoanAppRepayBillRellMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    public Xdcz0023DataRespDto loanWithoutRepayForSmall(Xdcz0023DataReqDto xdcz0023DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, JSON.toJSONString(xdcz0023DataReqDto));
        Xdcz0023DataRespDto xdcz0023DataRespDto = new Xdcz0023DataRespDto();
        String message = "系统检测到";
        boolean flag = Boolean.TRUE;
        String manager_br_id = xdcz0023DataReqDto.getManager_br_id();
        if (StringUtils.isBlank(manager_br_id)) {
            flag = Boolean.FALSE;
            message += "管户机构为空!";
        }
        String fina_br_id = xdcz0023DataReqDto.getFina_br_id();
        if (StringUtils.isBlank(fina_br_id)) {
            flag = Boolean.FALSE;
            message += "账务机构为空!";
        }
        String certid = xdcz0023DataReqDto.getCertid();
        if (StringUtils.isBlank(certid)) {
            flag = Boolean.FALSE;
            message += "用户身份证号码为空!";
        }
        BigDecimal apply_amount = xdcz0023DataReqDto.getApply_amount();
        if (Objects.isNull(apply_amount)) {
            flag = Boolean.FALSE;
            message += "合同金额为空!";
        }
        String loan_start_date = xdcz0023DataReqDto.getLoan_start_date();
        if (StringUtils.isBlank(loan_start_date)) {
            flag = Boolean.FALSE;
            message += "合同开始日期为空!";
        }
        String loan_end_date = xdcz0023DataReqDto.getLoan_end_date();
        if (StringUtils.isBlank(loan_end_date)) {
            flag = Boolean.FALSE;
            message += "合同结束日期为空!";
        }
        BigDecimal rate = xdcz0023DataReqDto.getRate();
        if (Objects.isNull(rate)) {
            flag = Boolean.FALSE;
            message += "利率为空!";
        }
        String bill_no = xdcz0023DataReqDto.getBill_no();
        if (StringUtils.isBlank(bill_no)) {
            flag = Boolean.FALSE;
            message += "借据号为空!";
        }
        String loan_cont_no = xdcz0023DataReqDto.getLoan_cont_no();
        if (StringUtils.isBlank(loan_cont_no)) {
            flag = Boolean.FALSE;
            message += "合同号为空!";
        }
        String cn_cont_no = xdcz0023DataReqDto.getCn_cont_no();
        if (StringUtils.isBlank(cn_cont_no)) {
            flag = Boolean.FALSE;
            message += "中文合同号为空!";
        }
        String zhifu_type = xdcz0023DataReqDto.getZhifu_type();
        if (StringUtils.isBlank(zhifu_type)) {
            flag = Boolean.FALSE;
            message += "支付方式为空!";
        }
        String cust_manager_id = xdcz0023DataReqDto.getCust_manager_id();
        if (StringUtils.isBlank(cust_manager_id)) {
            flag = Boolean.FALSE;
            message += "客户经理号为空!";
        }
        BigDecimal loan_amount = xdcz0023DataReqDto.getLoan_amount();
        if (Objects.isNull(loan_amount)) {
            flag = Boolean.FALSE;
            message += "出账金额为空!";
        }
        String repayment_account = xdcz0023DataReqDto.getRepayment_account();
        if (StringUtils.isBlank(repayment_account)) {
            flag = Boolean.FALSE;
            message += "借款人账号为空!";
        }
        String loan_start_date1 = xdcz0023DataReqDto.getLoan_start_date1();
        if (StringUtils.isBlank(loan_start_date1)) {
            flag = Boolean.FALSE;
            message += "借据开始日期为空!";
        }
        String loan_end_date1 = xdcz0023DataReqDto.getLoan_end_date1();
        if (StringUtils.isBlank(loan_end_date1)) {
            flag = Boolean.FALSE;
            message += "借据到期日期为空!";
        }
        String loan_paym_mtd = xdcz0023DataReqDto.getLoan_paym_mtd();
        if (StringUtils.isBlank(loan_paym_mtd)) {
            flag = Boolean.FALSE;
            message += "还款方式为空!";
        }
        BigDecimal yzrate = xdcz0023DataReqDto.getYzrate();
        String loan_term = xdcz0023DataReqDto.getLoan_term();
        if (Objects.isNull(yzrate)) {
            flag = Boolean.FALSE;
            message += "逾期利率为空!";
        }
        String old_bill_no = xdcz0023DataReqDto.getOld_bill_no();
        if (StringUtils.isBlank(old_bill_no)) {
            flag = Boolean.FALSE;
            message += "旧借据号为空!";
        }
        CtrLoanCont ctrLoanCont = Optional.ofNullable(ctrLoanContMapper.selectByPrimaryKey(loan_cont_no)).orElse(new CtrLoanCont());
        String biz_type = xdcz0023DataReqDto.getBiz_type();
        if (Objects.equals("12", biz_type)) {
            biz_type = ctrLoanCont.getBizType();
        } else {
            flag = Boolean.FALSE;
            message += "品种类型不正确!";
        }

        CusIndivAllDto cusIndiv = commonService.getCusIndivAllDtoByCertCode(certid);
        if (StringUtils.isBlank(cusIndiv.getCusId())) {
            flag = Boolean.FALSE;
            message += "该身份号未开户，请先开户!";
        }

        int count = accLoanMapper.getAccLoanCountByBillNo(bill_no);
        if (count > 0) {
            flag = Boolean.FALSE;
            message += "借据号已被占用";
        }


        try {
            if (flag) {
                Map<String, String> param = new HashMap<>();
                String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, param);
                String assure_means_main="10"; //抵押类

                Period p = Period.between(LocalDate.parse(loan_start_date), LocalDate.parse(loan_end_date));
                int term = p.getMonths();

//                BigDecimal jzRate = prdRateStdMapper.getRateByTerm(term);
                BigDecimal default_ir = rate.add(rate.divide(new BigDecimal("2")));
                String loanNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_ACCT_SERNO, new HashMap<>());

                CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
                cmisLmt0010ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0010ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrLoanCont.getManagerBrId()));//金融机构代码
                cmisLmt0010ReqDto.setBizNo(loan_cont_no);//合同编号
                CmisLmt0010ReqDealBizListDto cmisLmt0010ReqDealBizListDto = new CmisLmt0010ReqDealBizListDto();
                cmisLmt0010ReqDealBizListDto.setDealBizNo(bill_no);
                cmisLmt0010ReqDealBizListDto.setIsFollowBiz("1");
                // 无缝衔接时，录入以下的值
                //原交易业务编号
                //原交易业务恢复类型
                //原交易业务状态
                //原交易属性
                cmisLmt0010ReqDealBizListDto.setOrigiDealBizNo("");
                cmisLmt0010ReqDealBizListDto.setOrigiRecoverType("");
                cmisLmt0010ReqDealBizListDto.setOrigiDealBizStatus("");
                cmisLmt0010ReqDealBizListDto.setOrigiBizAttr("");

                cmisLmt0010ReqDealBizListDto.setCusId(cusIndiv.getCusId());
                cmisLmt0010ReqDealBizListDto.setCusName(cusIndiv.getCusName());
                cmisLmt0010ReqDealBizListDto.setPrdId(ctrLoanCont.getPrdId());
                cmisLmt0010ReqDealBizListDto.setPrdName(ctrLoanCont.getPrdName());
                cmisLmt0010ReqDealBizListDto.setDealBizAmt(loan_amount);
                cmisLmt0010ReqDealBizListDto.setDealBizSpacAmt(loan_amount);

                cmisLmt0010ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
                cmisLmt0010ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
                cmisLmt0010ReqDealBizListDto.setEndDate(loan_end_date1);
                List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList = new ArrayList<CmisLmt0010ReqDealBizListDto>();
                cmisLmt0010ReqDealBizListDtoList.add(cmisLmt0010ReqDealBizListDto);
                cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(cmisLmt0010ReqDealBizListDtoList);
                logger.info("根据业务申请编号【" + serno + "】前往额度系统-台账校验请求报文：" + JSON.toJSONString(cmisLmt0010ReqDto));
                ResultDto<CmisLmt0010RespDto> cmisLmt0010RespDto = cmisLmtClientService.cmisLmt0010(cmisLmt0010ReqDto);
                logger.info("根据业务申请编号【" + serno + "】前往额度系统-台账校验返回报文：" + JSON.toJSONString(cmisLmt0010RespDto));
                // 响应内容为空
                if(Objects.nonNull(cmisLmt0010RespDto) && Objects.equals(cmisLmt0010RespDto.getCode(), SuccessEnum.CMIS_SUCCSESS.key)) {
                    String code = cmisLmt0010RespDto.getData().getErrorCode();
                    if (Objects.equals(SuccessEnum.SUCCESS.key,code)) {
                        CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
                        cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                        cmisLmt0013ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrLoanCont.getManagerBrId()));//金融机构代码
                        cmisLmt0013ReqDto.setSerno(serno);
                        cmisLmt0013ReqDto.setInputBrId(ctrLoanCont.getInputBrId());
                        cmisLmt0013ReqDto.setInputDate(ctrLoanCont.getInputDate());
                        cmisLmt0013ReqDto.setInputId(ctrLoanCont.getInputId());
                        cmisLmt0013ReqDto.setBizNo(loan_cont_no);
                        List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDtos = new ArrayList<CmisLmt0013ReqDealBizListDto>();
                        CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizListDto = new CmisLmt0013ReqDealBizListDto();
                        cmisLmt0013ReqDealBizListDto.setDealBizNo(bill_no);//台账编号
                        cmisLmt0013ReqDealBizListDto.setIsFollowBiz("1");//是否无缝衔接
                        cmisLmt0013ReqDealBizListDto.setOrigiDealBizNo("");//原交易业务编号
                        cmisLmt0013ReqDealBizListDto.setDealBizStatus("");//原交易业务状态
                        cmisLmt0013ReqDealBizListDto.setOrigiRecoverType("");//原交易业务恢复类型
                        cmisLmt0013ReqDealBizListDto.setOrigiBizAttr("");//原交易属性
                        cmisLmt0013ReqDealBizListDto.setCusId(ctrLoanCont.getCusId());
                        cmisLmt0013ReqDealBizListDto.setCusName(ctrLoanCont.getCusName());
                        cmisLmt0013ReqDealBizListDto.setPrdId(ctrLoanCont.getPrdId());
                        cmisLmt0013ReqDealBizListDto.setPrdName(ctrLoanCont.getPrdName());
                        cmisLmt0013ReqDealBizListDto.setPrdTypeProp(ctrLoanCont.getPrdTypeProp());
                        cmisLmt0013ReqDealBizListDto.setDealBizAmtCny(loan_amount);
                        cmisLmt0013ReqDealBizListDto.setDealBizSpacAmtCny(loan_amount);
                        cmisLmt0013ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
                        cmisLmt0013ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
                        cmisLmt0013ReqDealBizListDto.setStartDate(loan_start_date1);
                        cmisLmt0013ReqDealBizListDto.setEndDate(loan_end_date1);
                        cmisLmt0013ReqDealBizListDto.setDealBizStatus("100");
                        cmisLmt0013ReqDealBizListDtos.add(cmisLmt0013ReqDealBizListDto);
                        cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDtos);


                        logger.info("根据业务申请编号【" + serno + "】前往额度系统-台账占用请求报文：" + JSON.toJSONString(cmisLmt0013ReqDto));
                        ResultDto<CmisLmt0013RespDto> cmisLmt0013RespDto = cmisLmtClientService.cmisLmt0013(cmisLmt0013ReqDto);
                        logger.info("根据业务申请编号【" + serno + "】前往额度系统-台账占用返回报文：" + JSON.toJSONString(cmisLmt0013RespDto));

                    }
                }

                PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell = new PvpLoanAppRepayBillRell();
                pvpLoanAppRepayBillRell.setPkId(cn.com.yusys.yusp.commons.util.StringUtils.uuid(true));
                pvpLoanAppRepayBillRell.setBillNo(old_bill_no);
                pvpLoanAppRepayBillRell.setSerno(serno);
                pvpLoanAppRepayBillRell.setOprType(CmisCommonConstants.OP_TYPE_01);
                pvpLoanAppRepayBillRell.setInputId(Optional.ofNullable(ctrLoanCont.getInputId()).orElse(StringUtils.EMPTY));
                pvpLoanAppRepayBillRell.setInputBrId(Optional.ofNullable(ctrLoanCont.getInputBrId()).orElse(StringUtils.EMPTY));
                pvpLoanAppRepayBillRell.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                pvpLoanAppRepayBillRell.setUpdBrId(Optional.ofNullable(ctrLoanCont.getUpdBrId()).orElse(StringUtils.EMPTY));
                pvpLoanAppRepayBillRell.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                pvpLoanAppRepayBillRell.setUpdId(Optional.ofNullable(ctrLoanCont.getInputId()).orElse(StringUtils.EMPTY));
                Date date = new Date();
                pvpLoanAppRepayBillRell.setCreateTime(date);
                pvpLoanAppRepayBillRell.setUpdateTime(date);
                pvpLoanAppRepayBillRellMapper.insertSelective(pvpLoanAppRepayBillRell);

                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("billNo", old_bill_no);

                List<PvpLoanApp> pvpLoanApps = pvpLoanAppMapper.selectByModel(queryModel);
                PvpLoanApp pvpLoanApp = new PvpLoanApp();
                if (CollectionUtils.nonEmpty(pvpLoanApps)) {
                    BeanUtils.copyProperties(pvpLoanApps.get(0), pvpLoanApp);
                }
                pvpLoanApp.setPvpSerno(serno);
                pvpLoanApp.setIqpSerno(loanNo);
                pvpLoanApp.setContNo(loan_cont_no);
                pvpLoanApp.setBillNo(bill_no);
                pvpLoanApp.setCusId(cusIndiv.getCusId());
                pvpLoanApp.setCusName(cusIndiv.getCusName());
                pvpLoanApp.setPvpAmt(loan_amount);
                pvpLoanApp.setCvtCnyAmt(loan_amount.multiply(Optional.ofNullable(pvpLoanApp.getExchangeRate()).orElse(new BigDecimal("1"))));
                pvpLoanApp.setStartDate(loan_start_date);
                pvpLoanApp.setEndDate(loan_end_date);
                pvpLoanApp.setLoanStartDate(loan_start_date1);
                pvpLoanApp.setLoanEndDate(loan_end_date1);
                pvpLoanApp.setLoanTerm(loan_term);
                pvpLoanApp.setContCnNo(Optional.ofNullable(ctrLoanCont.getContCnNo()).orElse(StringUtils.EMPTY));
                pvpLoanApp.setPrdId(Optional.ofNullable(ctrLoanCont.getPrdId()).orElse(StringUtils.EMPTY));
                pvpLoanApp.setPrdName(Optional.ofNullable(ctrLoanCont.getPrdName()).orElse(StringUtils.EMPTY));
                pvpLoanApp.setPrdTypeProp(Optional.ofNullable(ctrLoanCont.getPrdTypeProp()).orElse(StringUtils.EMPTY));
                pvpLoanApp.setContAmt(Optional.ofNullable(ctrLoanCont.getContAmt()).orElse(BigDecimal.ZERO));
                pvpLoanApp.setInputId(Optional.ofNullable(ctrLoanCont.getInputId()).orElse(StringUtils.EMPTY));
                pvpLoanApp.setManagerId(Optional.ofNullable(ctrLoanCont.getManagerId()).orElse(StringUtils.EMPTY));
                pvpLoanApp.setManagerBrId(Optional.ofNullable(ctrLoanCont.getManagerBrId()).orElse(StringUtils.EMPTY));
                pvpLoanApp.setLoanTypeDetail("07");
                pvpLoanApp.setLoanTer(Optional.ofNullable(ctrLoanCont.getLoanTer()).orElse(StringUtils.EMPTY));
                pvpLoanApp.setRepayAccno(repayment_account);
                pvpLoanApp.setLoanPayoutAccno(repayment_account);
                /*********************************** 默认值 ***********************************/
                pvpLoanApp.setLoanPromiseFlag("2");
                pvpLoanApp.setRateAdjMode("01");
                pvpLoanApp.setLoanModal("8");
                pvpLoanApp.setIsUtilLmt("0");
                pvpLoanApp.setIsMeterCi("1");
                pvpLoanApp.setIsSbsy("0");
                pvpLoanApp.setContCurType("CNY");
                pvpLoanApp.setCiRatePefloat(BigDecimal.ZERO);
                pvpLoanApp.setTermType("001");
                pvpLoanApp.setDeductDay("21");
                pvpLoanApp.setEiIntervalUnit("M");
                pvpLoanApp.setDeductType("AUTO");
                pvpLoanApp.setEiIntervalCycle("1");
                pvpLoanApp.setFtp("0");
                pvpLoanApp.setIrFloatRate(BigDecimal.ZERO);
                pvpLoanApp.setOverdueRatePefloat(default_ir);
                pvpLoanApp.setIsSteelLoan("0");
                pvpLoanApp.setIsStainlessLoan("0");
                pvpLoanApp.setIsPovertyReliefLoan("0");
                pvpLoanApp.setGoverSubszHouseLoan("0");
                pvpLoanApp.setGuarMode("10");//担保方式
                pvpLoanApp.setRealproLoan("0");
                pvpLoanApp.setIsCphsRurDelpLoan("0");
                pvpLoanApp.setEngyEnviProteLoan("0");
                pvpLoanApp.setIsOperPropertyLoan("0");
                pvpLoanApp.setFinaBrId(fina_br_id);
                pvpLoanApp.setIsBeEntrustedPay(zhifu_type);
                pvpLoanApp.setApproveStatus("997");
                pvpLoanApp.setOprType(CmisCommonConstants.OP_TYPE_01);//操作类型
                pvpLoanApp.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);//所属条线
                pvpLoanApp.setExecRateYear(rate);//执行年利率
                pvpLoanApp.setOverdueExecRate(yzrate);//逾期年利率
                pvpLoanApp.setCiExecRate(yzrate);//复息年利率
                String isAgri = cusIndiv.getIsAgri();
                if("1".equals(isAgri)){
                    pvpLoanApp.setAgriLoanTer("F13");//涉农投向：非涉农贷款       其它涉农贷款其它涉农贷款 F13
                }else{
                    pvpLoanApp.setAgriLoanTer("F14");//涉农投向：非涉农贷款       其它涉农贷款其它涉农贷款 F13
                }
                pvpLoanAppMapper.insertSelective(pvpLoanApp);
                queryModel.addCondition("billNo", "");
                queryModel.addCondition("serno", serno);
                pvpJxhjRepayLoanService.ClaJxhjLoanRepay(queryModel);

                PvpAuthorize pvpAuthorize = new PvpAuthorize();
                pvpAuthorize.setPvpSerno(serno);
                pvpAuthorize.setTranSerno(loanNo);//出账号
                pvpAuthorize.setInputId(ctrLoanCont.getInputId());//登记人
                String openDay = stringRedisTemplate.opsForValue().get("openDay");
                pvpAuthorize.setTranDate(openDay);         	    						//出账日期取系统时间
                String subjectNo = accLoanMapper.getSubjectNoByBillNo(old_bill_no);
                pvpAuthorize.setFldvalue01(subjectNo);        							//贷款科目代码
                pvpAuthorize.setFldvalue02("LN003");    									//***贷款类型
                pvpAuthorize.setContNo(loan_cont_no);                    					//合同号
                pvpAuthorize.setPrdId(ctrLoanCont.getPrdId());//产品编号
                pvpAuthorize.setPrdName(ctrLoanCont.getPrdName());//产品名称
                pvpAuthorize.setPrdTypeProp(ctrLoanCont.getPrdTypeProp());//产品类型属性
                pvpAuthorize.setCurType(ctrLoanCont.getCurType());//币种
                pvpAuthorize.setSendTimes(BigDecimal.ZERO);//发送次数
                pvpAuthorize.setTradeStatus("1");//发送类型
                pvpAuthorize.setAuthStatus("3");//权限状态
                pvpAuthorize.setTranId(CmisCommonConstants.DKCZ);                    					//核心通信交易码
                pvpAuthorize.setAuthStatus("1");
                pvpAuthorize.setCusId(cusIndiv.getCusId());                      						//借款人客户号
                pvpAuthorize.setCusName(cusIndiv.getCusName());                   						//借款人客户名称
                pvpAuthorize.setBillNo(bill_no);												//借据号
                pvpAuthorize.setFldvalue03(repayment_account);           					//收款人账户
                pvpAuthorize.setFldvalue04(repayment_account);     							//借款人结算账号
                pvpAuthorize.setFinaBrId(fina_br_id);                 						//账务机构
                pvpAuthorize.setManagerBrId(manager_br_id);           						//受理机构
                pvpAuthorize.setFldvalue41(manager_br_id);               					//放贷机构
                pvpAuthorize.setFldvalue08(loan_end_date1);            						//到期日
                pvpAuthorize.setFldvalue10("CNY");       									//币种
                pvpAuthorize.setTranAmt(loan_amount);            					//放款金额
                pvpAuthorize.setFldvalue11("1");											//本金自动归还标志    0否1是
                pvpAuthorize.setFldvalue12("1");         									//利息自动归还标志     0否1是
                pvpAuthorize.setFldvalue13("G");           									//重定价代码
                pvpAuthorize.setFldvalue16("0.5");  										//逾期贷款利率浮动比例
                pvpAuthorize.setFldvalue17(yzrate.toString());   									//逾期贷款执行利率
                pvpAuthorize.setFldvalue18(rate.toString()); 									//正常贷款执行利率
                pvpAuthorize.setFldvalue19("1");             							//还款方式      1-普通贷款 2-按揭分期贷款3-非按揭分期贷款
                pvpAuthorize.setFldvalue20("IP001");										//普通贷款结息方式代码
                pvpAuthorize.setFldvalue24(String.valueOf(loan_term));      				    			//借据期限
                pvpAuthorize.setFldvalue25("2");      										//贷款承诺标志
                pvpAuthorize.setFldvalue27(cusIndiv.getCusName());     									//放款账户名
                pvpAuthorize.setFldvalue29("8");                   							//贷款类型 1新增，3借新还旧  8为小企业无还本续贷
                pvpAuthorize.setManagerId(cust_manager_id);									//责任人
//                pvpAuthorize.setJbManagerId(cust_manager_id);								//经办人
                pvpAuthorize.setFldvalue30("1");             								//是否计复息   0不计算、1计算
                pvpAuthorize.setFldvalue31("19");       									//用途代码     00  流动资金   01  固定资金  02  技术改造  04  流资转期
                pvpAuthorize.setFldvalue32(zhifu_type);  									//是否受托支付
                pvpAuthorize.setFldvalue33("0"); 											//委托贷款手续费收取比例
                pvpAuthorize.setFldvalue34("0");   											//委托贷款手续费
                pvpAuthorize.setFldvalue36("1");   											//贷款利率浮动方式 1  加百分比 2 加点 3正常加点逾期加百分比
                pvpAuthorize.setFldvalue37("0"); 											//正常利率浮动点数(年利率)
                pvpAuthorize.setFldvalue38("0");  											//逾期利率浮动点数(年利率)
                pvpAuthorize.setFldvalue39(biz_type); //业务品种
                pvpAuthorize.setFldvalue40("【贷款】");
                pvpAuthorize.setFldvalue42("0");            								//借用  执行利率方式
                pvpAuthorize.setFldvalue43("0");     										//复利年利率
                pvpAuthorize.setFldvalue44(loan_end_date1);									//贷款到期日
    //            pvpAuthorize.setTradeCode(TradeCodeConstant.DKCZ);							//贷款出账交易码
    //            pvpAuthorize.setLoanFrom(Loan_from);
                pvpAuthorizeMapper.insertSelective(pvpAuthorize);

                AccLoan accLoanOld = accLoanMapper.selectByBillNo(old_bill_no);
                AccLoan accLoan = new AccLoan();
                if (Objects.nonNull(accLoanOld)) {
                    BeanUtils.copyProperties(accLoanOld, accLoan);
                }
                accLoan.setPkId(cn.com.yusys.yusp.commons.util.StringUtils.uuid(true));
                accLoan.setPvpSerno(serno);
                accLoan.setBillNo(bill_no);//借据编号
                accLoan.setContNo(loan_cont_no);//合同编号
                accLoan.setPrdId(Optional.of(ctrLoanCont.getPrdId()).orElse(StringUtils.EMPTY));	  //产品编号
                accLoan.setPrdName(Optional.of(ctrLoanCont.getPrdName()).orElse(StringUtils.EMPTY));
                accLoan.setPrdTypeProp(Optional.of(ctrLoanCont.getPrdTypeProp()).orElse(StringUtils.EMPTY));
                accLoan.setCusId(cusIndiv.getCusId());
                accLoan.setCusName(cusIndiv.getCusName());
                accLoan.setSubjectNo(subjectNo);
                accLoan.setGuarMode(assure_means_main);
                accLoan.setContCurType("CNY");
                accLoan.setLoanAmt(loan_amount);
                accLoan.setLoanBalance(loan_amount);
                accLoan.setLoanStartDate(loan_start_date1);
                accLoan.setLoanEndDate(loan_end_date1);
                accLoan.setRulingIr(rate); //基准利率(年)
                accLoan.setExecRateYear(rate); //执行利率
                accLoan.setOverdueExecRate(yzrate); //逾期利率
                accLoan.setCiRatePefloat(new BigDecimal("0.5"));
                accLoan.setCiExecRate(yzrate);
                accLoan.setOverdueBalance(BigDecimal.ZERO);
                //贷款投向
                accLoan.setLoanTer(Optional.ofNullable(ctrLoanCont.getLoanTer()).orElse(StringUtils.EMPTY));  //贷款投向  是字典项
                accLoan.setLoanTypeDetail("07");//贷款类别
                if("1".equals(isAgri)){
                    accLoan.setAgriLoanTer("F13"); //其它涉农贷款
                }else{
                    accLoan.setAgriLoanTer("F14"); //非涉农贷款
                }
                accLoan.setExtTimes("0");
                accLoan.setOverdueTimes("0");
                Map map = pvpLoanAppService.getFiveAndTenClass(ctrLoanCont.getCusId());
                accLoan.setFiveClass((String) map.get("fiveClass"));
                accLoan.setManagerId(cust_manager_id);
                accLoan.setFinaBrId(fina_br_id);//账务机构
                accLoan.setManagerBrId(manager_br_id);
                accLoan.setUpdDate(openDay);
                accLoan.setUpdId(cust_manager_id);
                accLoan.setUpdBrId(manager_br_id);
                accLoan.setInputDate(openDay);
                accLoan.setAccStatus("6");//台账状态
                accLoan.setClassDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                accLoan.setTenClass("11");
                accLoan.setInputId(cust_manager_id);
                accLoan.setInputBrId(manager_br_id);
                accLoan.setRateAdjMode("01");
                accLoan.setLoanUseType("19");
                accLoan.setDeductDay("21");
                accLoan.setEiIntervalUnit("M");
                accLoan.setEiIntervalCycle("1");
                accLoan.setRepayMode(loan_paym_mtd);
                accLoan.setDeductType("AUTO");
                accLoan.setNextRateAdjInterval("1");
                accLoan.setOverdueRatePefloat(new BigDecimal("0.5"));
                accLoan.setCreateTime(date);//创建时间
                accLoan.setUpdateTime(date);//更新时间
                accLoanMapper.insertSelective(accLoan);
            }
            xdcz0023DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);
            xdcz0023DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, e.getMessage());
            xdcz0023DataRespDto.setOpFlag(CmisBizConstants.FAIL);
            xdcz0023DataRespDto.setOpMsg("系统异常");
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, JSON.toJSONString(xdcz0023DataRespDto));
        return xdcz0023DataRespDto;
    }
}
