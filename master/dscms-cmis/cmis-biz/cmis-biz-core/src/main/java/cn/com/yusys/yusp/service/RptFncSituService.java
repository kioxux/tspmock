/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.FinanIndicAnalyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptFncSitu;
import cn.com.yusys.yusp.repository.mapper.RptFncSituMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptFncSituService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-06-18 18:54:54
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptFncSituService {

    @Autowired
    private RptFncSituMapper rptFncSituMapper;
    @Autowired
    private ICusClientService iCusClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptFncSitu selectByPrimaryKey(String serno) {
        return rptFncSituMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptFncSitu> selectAll(QueryModel model) {
        List<RptFncSitu> records = (List<RptFncSitu>) rptFncSituMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptFncSitu> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptFncSitu> list = rptFncSituMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptFncSitu record) {
        return rptFncSituMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptFncSitu record) {
        return rptFncSituMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptFncSitu record) {
        return rptFncSituMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptFncSitu record) {
        return rptFncSituMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptFncSituMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptFncSituMapper.deleteByIds(ids);
    }

    /**
     * 保存信息
     *
     * @param rptFncSitu
     * @return
     */
    public int saveFncSitu(RptFncSitu rptFncSitu) {
        String serno = rptFncSitu.getSerno();
        RptFncSitu temp = this.selectByPrimaryKey(serno);
        if (null != temp) {
            return this.updateSelective(rptFncSitu);
        } else {
            return this.insertSelective(rptFncSitu);
        }
    }

    /**
     * 初始化信息
     *
     * @param map
     * @return
     */
    public RptFncSitu initFncSitu(Map map) {
        String serno = map.get("serno").toString();
        RptFncSitu rptFncSitu1 = rptFncSituMapper.selectByPrimaryKey(serno);
        if (Objects.nonNull(rptFncSitu1)) {
            return rptFncSitu1;
        } else {
            ResultDto<List<FinanIndicAnalyDto>> rptFncTotalProfit = iCusClientService.getRptFncTotalProfit(map);
            if (rptFncTotalProfit != null && rptFncTotalProfit.getData() != null) {
                RptFncSitu rptFncSitu = new RptFncSitu();
                rptFncSitu.setSerno(serno);
                List<FinanIndicAnalyDto> data = rptFncTotalProfit.getData();
                for (FinanIndicAnalyDto finanIndicAnalyDto : data) {
                    if("经营活动现金净流量".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearNcfo(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearNcfo(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearNcfo(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("投资活动现金净流量".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearNcfia(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearNcfia(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearNcfia(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("筹资活动现金净流量".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearNcffa(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearNcffa(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearNcffa(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("资产负债率".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearDebtRate(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearDebtRate(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearDebtRate(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("流动比率".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearCurrentRatio(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearCurrentRatio(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearCurrentRatio(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("速动比率".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearQuickRatio(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearQuickRatio(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearQuickRatio(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("营运资金".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearWorkCap(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearWorkCap(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearWorkCap(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("应收账款周转率".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearArvto(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearArvto(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearArvto(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("存货周转率".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearItto(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearItto(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearItto(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("总资产报酬率".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearRota(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearRota(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearRota(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("净资产收益率".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearRona(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearRona(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearRona(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                    if("营业收入增长率".equals(finanIndicAnalyDto.getItemName())){
                        rptFncSitu.setLastTwoYearRevinr(finanIndicAnalyDto.getNearSecondValue());
                        rptFncSitu.setLastYearRevinr(finanIndicAnalyDto.getNearFirstValue());
                        rptFncSitu.setCurYearRevinr(finanIndicAnalyDto.getCurYmValue());
                        rptFncSitu.setAcquisitionDate(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                }
                int count = rptFncSituMapper.insert(rptFncSitu);
                if(count>0){
                    return rptFncSitu;
                }
            }
        }
        return null;
    }

    public RptFncSitu resetFnc(Map map){
        String serno = map.get("serno").toString();
        RptFncSitu rptFncSitu = rptFncSituMapper.selectByPrimaryKey(serno);
        if(Objects.nonNull(rptFncSitu)){
            rptFncSituMapper.deleteByPrimaryKey(serno);
        }
        return initFncSitu(map);
    }
}
