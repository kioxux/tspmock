/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpApptConnInfo;
import cn.com.yusys.yusp.service.IqpApptConnInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApptConnInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-10 10:16:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpapptconninfo")
public class IqpApptConnInfoResource {
    @Autowired
    private IqpApptConnInfoService iqpApptConnInfoService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpApptConnInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpApptConnInfo> list = iqpApptConnInfoService.selectAll(queryModel);
        return new ResultDto<List<IqpApptConnInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpApptConnInfo>> index(QueryModel queryModel) {
        List<IqpApptConnInfo> list = iqpApptConnInfoService.selectByModel(queryModel);
        return new ResultDto<List<IqpApptConnInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<IqpApptConnInfo> show(@PathVariable("serno") String serno) {
        IqpApptConnInfo iqpApptConnInfo = iqpApptConnInfoService.selectByPrimaryKey(serno);
        return new ResultDto<IqpApptConnInfo>(iqpApptConnInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpApptConnInfo> create(@RequestBody IqpApptConnInfo iqpApptConnInfo) throws URISyntaxException {
        iqpApptConnInfoService.insert(iqpApptConnInfo);
        return new ResultDto<IqpApptConnInfo>(iqpApptConnInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpApptConnInfo iqpApptConnInfo) throws URISyntaxException {
        int result = iqpApptConnInfoService.update(iqpApptConnInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = iqpApptConnInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpApptConnInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
