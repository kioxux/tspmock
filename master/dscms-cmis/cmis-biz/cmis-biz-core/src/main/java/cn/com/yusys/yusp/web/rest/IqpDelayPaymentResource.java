/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpDelayPayment;
import cn.com.yusys.yusp.service.IqpDelayPaymentService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDelayPaymentResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-26 15:07:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpdelaypayment")
public class IqpDelayPaymentResource {
    @Autowired
    private IqpDelayPaymentService iqpDelayPaymentService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpDelayPayment>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpDelayPayment> list = iqpDelayPaymentService.selectAll(queryModel);
        return new ResultDto<List<IqpDelayPayment>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<IqpDelayPayment>> index(@RequestBody QueryModel queryModel) {
        List<IqpDelayPayment> list = iqpDelayPaymentService.selectByModel(queryModel);
        return new ResultDto<List<IqpDelayPayment>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{delaySerno}")
    protected ResultDto<IqpDelayPayment> show(@PathVariable("delaySerno") String delaySerno) {
        IqpDelayPayment iqpDelayPayment = iqpDelayPaymentService.selectByPrimaryKey(delaySerno);
        return new ResultDto<IqpDelayPayment>(iqpDelayPayment);
    }

    @PostMapping("/{delaySerno}")
    protected ResultDto<IqpDelayPayment> show1(@PathVariable("delaySerno") String delaySerno) {
        IqpDelayPayment iqpDelayPayment = iqpDelayPaymentService.selectByPrimaryKey(delaySerno);
        return new ResultDto<IqpDelayPayment>(iqpDelayPayment);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<IqpDelayPayment> create(@RequestBody IqpDelayPayment iqpDelayPayment) throws URISyntaxException {
        iqpDelayPaymentService.insertSelective(iqpDelayPayment);
        return new ResultDto<IqpDelayPayment>(iqpDelayPayment);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpDelayPayment iqpDelayPayment) throws URISyntaxException {
        int result = iqpDelayPaymentService.update(iqpDelayPayment);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{delaySerno}")
    protected ResultDto<Integer> delete(@PathVariable("delaySerno") String delaySerno) {
        int result = iqpDelayPaymentService.deleteByPrimaryKey(delaySerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpDelayPaymentService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
