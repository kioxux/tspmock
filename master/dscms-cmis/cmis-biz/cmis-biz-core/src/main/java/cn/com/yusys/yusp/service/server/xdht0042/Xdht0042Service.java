package cn.com.yusys.yusp.service.server.xdht0042;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.req.CmisCus0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CmisCus0006RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdht0042.req.Xdht0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0042.resp.Xdht0042DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizHtEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.client.cus.cmiscus0006.CmisCus0006Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0042Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-05-05 19:46:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0042Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0042Service.class);
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private CmisCus0006Service cmisCus0006Service;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * 优企贷、优农贷合同信息查询
     *
     * @param xdht0042DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0042DataRespDto queryContDeTailsInfoByCertCode(Xdht0042DataReqDto xdht0042DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0042.key, DscmsEnum.TRADE_CODE_XDHT0042.value);
        Xdht0042DataRespDto xdht0042DataRespDto = new Xdht0042DataRespDto();
        CmisCus0006ReqDto cmisCus0006ReqDto = new CmisCus0006ReqDto();//请求Dto：查询客户基本信息
        CmisCus0006RespDto cmisCus0006RespDto = new CmisCus0006RespDto();//响应Dto：查询客户基本信息

        String queryType = xdht0042DataReqDto.getQuery_type();//查询类型
        String cusCertNo = xdht0042DataReqDto.getCert_code();//证件号
        String cusidList = xdht0042DataReqDto.getCus_id();//客户号

        try {
            if (DscmsBizHtEnum.QUERY_TYPE_01.key.equals(queryType)) {//个人取数
                //通过客户证件号查询客户信息
                logger.info("*********XDHT0042**通过客户证件号查询客户信息****START*********:");
                cmisCus0006ReqDto.setCertCode(cusCertNo);
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
                cmisCus0006RespDto = cmisCus0006Service.cmisCus0006(cmisCus0006ReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
                List<CusBaseDto> cusBaseList = Optional.ofNullable(cmisCus0006RespDto.getCusBaseList()).orElse(new ArrayList<>());
                String cusId = null;// 客户ID
                for (int i = 0; i < cusBaseList.size(); i++) {
                    CusBaseDto cusBaseDto = cusBaseList.get(i);
                    if (Objects.equals(cusBaseDto.getCertCode(), cusCertNo)) {
                        cusId = cusBaseDto.getCusId();
                    }
                }
                //根据客户号查询客户合同信息
                if (StringUtils.isBlank(cusId)) {
                    xdht0042DataRespDto.setList(new ArrayList<>());
                } else {
                    logger.info("*********XDHT0042**通过客户证件号查询客户信息开始,查询参数为:{}", JSON.toJSONString(cusId));
                    java.util.List<cn.com.yusys.yusp.dto.server.xdht0042.resp.List> lists = ctrLoanContMapper.queryYqdCtrLoanContdetails(cusId);
                    logger.info("*********XDHT0042**通过客户证件号查询客户信息结束,返回结果为:{}", JSON.toJSONString(lists));
                    lists = changePrdType(lists);
                    xdht0042DataRespDto.setList(lists);
                }
            } else {//公司取数
                logger.info("*********XDHT0042**公司取数****START*********:");
                //入参的客户号，可多个，以【,】进行分割
                String cusIds = "";
                if (cusidList.contains(",")) {
                    String[] cusidAry = cusidList.split(",");
                    List<Map> list = new ArrayList<>();
                    for (int j = 0; j < cusidAry.length; j++) {
                        String cusNo = cusidAry[j];
                        cusIds = cusIds + "'" + cusNo + "',";
                    }
                    if (StringUtils.nonBlank(cusIds)) {
                        cusIds = cusIds.substring(0, cusIds.lastIndexOf(","));
                    }
                } else {
                    cusIds = cusIds + "'" + cusidList + "'";
                }
                //根据客户号查询客户合同信息
                logger.info("*********XDHT0042**根据客户号查询客户合同信息开始,查询参数为:{}", JSON.toJSONString(cusIds));
                java.util.List<cn.com.yusys.yusp.dto.server.xdht0042.resp.List> lists = ctrLoanContMapper.queryComYqdCtrLoanContdetails(cusIds);
                logger.info("*********XDHT0042**根据客户号查询客户合同信息结束,返回结果为:{}", JSON.toJSONString(lists));
                lists = changePrdType(lists);
                xdht0042DataRespDto.setList(lists);
            }
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0042.key,
                    DscmsEnum.TRADE_CODE_XDHT0042.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0042.key,
                    DscmsEnum.TRADE_CODE_XDHT0042.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0042.key, DscmsEnum.TRADE_CODE_XDHT0042.value);
        return xdht0042DataRespDto;
    }

    /**
     * 根据产品id查询对应的产品类型
     *
     * @param lists
     * @return
     */
    public java.util.List<cn.com.yusys.yusp.dto.server.xdht0042.resp.List> changePrdType(java.util.List<cn.com.yusys.yusp.dto.server.xdht0042.resp.List> lists) {
        try {
            logger.info("***********调用iCmisCfgClientService查询产品类别开始*START**************");
            lists = lists.parallelStream().map(ret -> {
                cn.com.yusys.yusp.dto.server.xdht0042.resp.List temp = new cn.com.yusys.yusp.dto.server.xdht0042.resp.List();
                BeanUtils.copyProperties(ret, temp);
                //担保方式码值转换
                String prdId = temp.getPrd_type();// 获取产品ID
                ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                String prdCode = prdresultDto.getCode();//返回结果
                String prdType = StringUtils.EMPTY;
                if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                    CfgPrdBasicinfoDto CfgPrdBasicinfoDto = prdresultDto.getData();
                    if (CfgPrdBasicinfoDto != null) {
                        prdType = CfgPrdBasicinfoDto.getPrdType();
                    }
                }
                temp.setPrd_type(prdType);
                //返回列表
                return temp;
            }).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        logger.info("***********调用iCmisCfgClientService查询产品类别结束*END**************");
        return lists;
    }
}
