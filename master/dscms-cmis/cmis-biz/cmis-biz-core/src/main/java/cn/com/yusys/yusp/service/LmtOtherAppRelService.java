/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.OtherAccpPerferFeeApp;
import cn.com.yusys.yusp.dto.LmtOtherAppRelDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtOtherAppRel;
import cn.com.yusys.yusp.repository.mapper.LmtOtherAppRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtOtherAppRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:18:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtOtherAppRelService {
    private final Logger log = LoggerFactory.getLogger(LmtOtherAppRelService.class);

    @Autowired
    private LmtOtherAppRelMapper lmtOtherAppRelMapper;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtOtherAppRel selectByPrimaryKey(String pkId) {
        if (StringUtils.isBlank(pkId)){
            throw BizException.error(null,EcbEnum.ECB010001.key,EcbEnum.ECB010001.value);
        }
        return lmtOtherAppRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtOtherAppRel> selectAll(QueryModel model) {
        List<LmtOtherAppRel> records = (List<LmtOtherAppRel>) lmtOtherAppRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtOtherAppRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtOtherAppRel> list = lmtOtherAppRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtOtherAppRel record) {
        return lmtOtherAppRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtOtherAppRel lmtOtherAppRel) {
        if(lmtOtherAppRel==null){
            throw BizException.error(null, EcbEnum.BIZ_CORRE_MAN_ADD_PARAM_EXP.key,EcbEnum.BIZ_CORRE_MAN_ADD_PARAM_EXP.value);
        }
        return lmtOtherAppRelMapper.insertSelective(lmtOtherAppRel);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtOtherAppRel record) {
        return lmtOtherAppRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtOtherAppRel lmtOtherAppRel) {
        if(lmtOtherAppRel==null){
            throw BizException.error(null, EcbEnum.BIZ_CORRE_MAN_ADD_PARAM_EXP.key,EcbEnum.BIZ_CORRE_MAN_ADD_PARAM_EXP.value);
        }
        return lmtOtherAppRelMapper.updateByPrimaryKeySelective(lmtOtherAppRel);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtOtherAppRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtOtherAppRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：selectByLmtSerno
     * @方法描述：根据业务流水号查询
     * @创建人：zhangming12
     * @创建时间：2021/5/18 21:49
     * @修改记录：修改时间 修改人员 修改时间
    */
    public List<LmtOtherAppRel> selectByLmtSerno(QueryModel model){
        PageHelper.startPage(model.getPage(), model.getSize());
        Map<String, Object> condition = model.getCondition();
        String lmtSerno = (String) condition.get("lmtSerno");
        List<LmtOtherAppRel> lmtOtherAppRelList = lmtOtherAppRelMapper.selectByLmtSerno(lmtSerno);
        PageHelper.clearPage();
        return lmtOtherAppRelList;
    }

    /**
     * @方法名称：queryByGrpLmtSerno
     * @方法描述：根据业务流水号查询
     * @创建人：yangwl
     * @创建时间：2021/6/18 21:49
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<LmtOtherAppRelDto> queryByGrpLmtSerno(QueryModel model){
        PageHelper.startPage(model.getPage(), model.getSize());
        Map<String, Object> condition = model.getCondition();
        String grpSerno = (String) condition.get("grpSerno");
        List<LmtOtherAppRelDto> lmtOtherAppRelList = lmtOtherAppRelMapper.queryByGrpLmtSerno(grpSerno);
        PageHelper.clearPage();
        return lmtOtherAppRelList;
    }


    /**
     * @方法名称：updateOprType
     * @方法描述：根据主键逻辑删除
     * @创建人：
     * @创建时间：
     * @修改记录：修改时间 修改人员 修改时间
    */
    public int updateOprType(String pkId){
        if (StringUtils.isBlank(pkId)){
            throw BizException.error(null,EcbEnum.ECB010001.key,EcbEnum.ECB010001.value);
        }
        //2.判断当前申请状态是否为退回，修改为自行退出
        LmtOtherAppRel lmtOtherAppRel = this.selectByPrimaryKey(pkId);
        if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(lmtOtherAppRel.getApproveStatus())){
            //流程删除 修改为自行退出
            log.info("流程删除==》bizId：", lmtOtherAppRel.getLmtOtherAppSerno());
            // 删除流程实例
            workflowCoreClient.deleteByBizId(lmtOtherAppRel.getLmtOtherAppSerno());
            lmtOtherAppRel.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            this.update(lmtOtherAppRel);
            log.info("其他申请事项审批表删除结束。。。");
        }
        return lmtOtherAppRelMapper.updateOprType(pkId);
    }

    /**
     * @方法名称：selectLmtOtherAppRelDataByParams
     * @方法描述：根据入参查询
     * @创建人：
     * @创建时间：
     * @修改记录：修改时间 修改人员 修改时间
     */

    public List<LmtOtherAppRel> selectLmtOtherAppRelDataByParams(Map map) {
        return lmtOtherAppRelMapper.selectLmtOtherAppRelDataByParams(map);
    }

    /**
     * @方法名称：updateOprType
     * @方法描述：逻辑删除
     * @创建人：css
     * @创建时间：2021/7/2 10:44
     * @修改记录：修改时间 修改人员 修改时间
     */

    public int deleteMemRelData(String serno) {
        HashMap map = new HashMap();
        int num = 0;
        map.put("serno",serno);
        List<LmtOtherAppRel> lmtOtherAppRelList = this.selectLmtOtherAppRelDataByParams(map);
        if(!lmtOtherAppRelList.isEmpty() && lmtOtherAppRelList.size() > 0){
            LmtOtherAppRel lmtOtherAppRel =lmtOtherAppRelList.get(0);
            // List中只有一条数据
            if(lmtOtherAppRelList.get(0).getApproveStatus().equals(CmisBizConstants.STD_ZB_APP_ST_992)){
                // 如果是992状态下删除,更改状态为自行退出
                lmtOtherAppRel.setApproveStatus(CmisBizConstants.STD_ZB_APP_ST_996);
            }else{
                lmtOtherAppRel.setOprType(CmisCommonConstants.OP_TYPE_02);
            }
            num = updateSelective(lmtOtherAppRel);
        }
        return num;
    }
}
