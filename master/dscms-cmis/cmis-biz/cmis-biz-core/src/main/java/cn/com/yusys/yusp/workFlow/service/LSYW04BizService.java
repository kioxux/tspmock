package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CentralFileTask;
import cn.com.yusys.yusp.domain.PvpAuthorize;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.PvpAuthorizeMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @param 
 * @return 
 * @author shenli
 * @date 2021/6/7 20:48
 * @version 1.0.0
 * @desc 零售放款审核流程-生成打印模式
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class LSYW04BizService implements ClientBizInterface{

        private final Logger log = LoggerFactory.getLogger(LSYW04BizService.class);
        @Autowired
        private AmqpTemplate amqpTemplate;
        @Autowired
        private PvpLoanAppService pvpLoanAppService;
        @Autowired
        private CmisBizClientService cmisBizClientService;
        @Autowired
        private AdminSmOrgService adminSmOrgService;
        @Autowired
        private CentralFileTaskService centralFileTaskService;
        @Autowired
        private PvpAuthorizeMapper pvpAuthorizeMapper;
        @Autowired
        private PvpAuthorizeService pvpAuthorizeService;
        @Autowired
        private CmisBizXwCommonService cmisBizXwCommonService;
        @Autowired
        private MessageCommonService messageCommonService;
        @Autowired
        private AdminSmUserService adminSmUserService;
        @Autowired
        private WorkflowCoreClient workflowCoreClient;
        @Autowired
        private BizCommonService bizCommonService;

        @Override
        public void bizOp(ResultInstanceDto resultInstanceDto) {

            String currentOpType = resultInstanceDto.getCurrentOpType();
            log.info("后业务处理类型:" + currentOpType);
            String bizType = resultInstanceDto.getBizType();
            log.info("进入业务类型【{}】、业务处理类型【{}】流程处理逻辑开始！", bizType, currentOpType);
            if("LS006".equals(bizType)){

                handlerLS006(resultInstanceDto);

            }
            log.info("进入业务类型【{}】流程处理逻辑结束！", bizType);
        }

    //零售放款申请（生成打印模式）
    private void handlerLS006(ResultInstanceDto resultInstanceDto){
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String extSerno = resultInstanceDto.getBizId();
        String nodeId = resultInstanceDto.getNodeId();
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                createCentralFileTask(resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,extSerno);
                // pvpLoanAppService.createImageSpplInfo(extSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),CmisCommonConstants.STD_SPPL_BIZ_TYPE_03);
                if("233_6".equals(nodeId) || "233_7".equals(nodeId)){
                    cmisBizXwCommonService.sendYKTask(extSerno, resultInstanceDto.getBizType(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getBizUserName());
                }
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                end(extSerno);
                sendImage(resultInstanceDto);
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,extSerno);
                pvpLoanAppService.createImageSpplInfo(extSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),CmisCommonConstants.STD_SPPL_BIZ_TYPE_03,"零售放款审核（生成打印模式）");
                if("233_6".equals(nodeId) || "233_7".equals(nodeId)){
                    cmisBizXwCommonService.sendYKTask(extSerno, resultInstanceDto.getBizType(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getBizUserName());
                }
                sendMessage(resultInstanceDto,"通过");
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    sendMessage(resultInstanceDto,"退回");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpLoanAppService.retailRestore(extSerno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    sendMessage(resultInstanceDto,"退回");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpLoanAppService.retailRestore(extSerno);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_998);
                pvpLoanAppService.retailRestore(extSerno);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                log.info("发送异常消息开始:" + resultInstanceDto);
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
                log.info("发送异常消息开始结束");
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }



        /**
         * @创建人 WH
         * @创建时间 2021-05-10 19:35
         * @注释 此处改为自己对应的流程名称 WF_PVP_LOAN_APP_LS
         */
        @Override
        public boolean should(ResultInstanceDto resultInstanceDto) {
            String flowCode = resultInstanceDto.getFlowCode();
            return CmisFlowConstants.LSYW04.equals(flowCode);

        }

        /**
         * @创建人 WH
         * @创建时间 2021-04-25 21:53
         * @注释 审批状态更换   替换对应的 service
         */
        public void updateStatus(String serno,String state){
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            //零售-放款申请提交-额度占用
            if("000".equals(pvpLoanApp.getApproveStatus()) || "992".equals(pvpLoanApp.getApproveStatus())){
                pvpLoanAppService.retaillimitOccupy(serno);
            }
            pvpLoanApp.setApproveStatus(state);
            pvpLoanAppService.updateSelective(pvpLoanApp);
        }
        /**
         * @创建人 WH
         * @创建时间 2021-04-25 21:53
         * @注释 审批通过 替换对应的service和 自己对应的修改完成后的方法 reviewend
         */
        public void end(String extSerno) {
            //审批通过的操作
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(extSerno);
            pvpLoanAppService.reviewend(pvpLoanApp);
            PvpAuthorize pvpAuthorize = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpLoanApp.getPvpSerno());
            log.info("零售出账开始："+pvpAuthorize.getTranSerno());
            pvpAuthorizeService.sendAuthToCoreForXd(pvpAuthorize);
        }

    public void createCentralFileTask (ResultInstanceDto resultInstanceDto){
        if("233_5".equals(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId())){
            try {
                PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(resultInstanceDto.getBizId());
                log.info("零售放款审核流程-生成打印模式："+"流水号："+resultInstanceDto.getBizId()+"-归档开始");
                ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(pvpLoanApp.getInputBrId());
                String orgType = resultDto.getData().getOrgType();
                //        0-总行部室
                //        1-异地支行（有分行）
                //        2-异地支行（无分行）
                //        3-异地分行
                //        4-中心支行
                //        5-综合支行
                //        6-对公支行
                //        7-零售支行
                if(!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType) &&!"8".equals(orgType) && !"9".equals(orgType) && !"A".equals(orgType)){

                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("instanceId",resultInstanceDto.getInstanceId());
                    queryModel.addCondition("bizType","LS006");
                    List<CentralFileTask> CentralFileTaskList =  centralFileTaskService.selectAll(queryModel);
                    String taskType = "";
                    if(CentralFileTaskList!=null && CentralFileTaskList.size() >0){//大于0,代表存打回
                        taskType = "02"; // 档案暂存
                    }else{
                        taskType = "03"; // 档案派发
                    }

                    //新增临时档案任务
                    CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                    centralFileTaskdto.setSerno(pvpLoanApp.getIqpSerno());
                    centralFileTaskdto.setTraceId(pvpLoanApp.getContNo());
                    centralFileTaskdto.setCusId(pvpLoanApp.getCusId());
                    centralFileTaskdto.setCusName(pvpLoanApp.getCusName());
                    centralFileTaskdto.setBizType("LS006"); // LS006	零售放款申请（生成打印模式）
                    centralFileTaskdto.setInputId(pvpLoanApp.getInputId());
                    centralFileTaskdto.setInputBrId(pvpLoanApp.getInputBrId());
                    centralFileTaskdto.setOptType("02"); // 纯指令
                    centralFileTaskdto.setTaskType(taskType); // 档案派发
                    centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                    centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                    centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                    cmisBizClientService.createCentralFileTask(centralFileTaskdto);
                    log.info("零售放款审核流程-生成打印模式："+"流水号："+resultInstanceDto.getBizId()+"-归档成功");
                }
                log.info("零售放款审核流程-生成打印模式："+"流水号："+resultInstanceDto.getBizId()+"-归档结束");
            }catch (Exception e){
                log.info("零售放款审核流程-生成打印模式："+"流水号："+resultInstanceDto.getBizId()+"-归档异常------------------"+e);
            }
        }else{
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(resultInstanceDto.getBizId());
            String  approveStatus = pvpLoanApp.getApproveStatus();
            if(CmisCommonConstants.WF_STATUS_000.equals(approveStatus) || CmisCommonConstants.WF_STATUS_992.equals(approveStatus)) {
                log.info("零售放款审核流程-生成打印模式：" + "流水号：" + resultInstanceDto.getBizId() + "-归档开始");
                ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(pvpLoanApp.getInputBrId());
                String orgType = resultDto.getData().getOrgType();
                //        0-总行部室
                //        1-异地支行（有分行）
                //        2-异地支行（无分行）
                //        3-异地分行
                //        4-中心支行
                //        5-综合支行
                //        6-对公支行
                //        7-零售支行selectbyiqpsernoretail
                if(!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType) &&!"8".equals(orgType) && !"9".equals(orgType) && !"A".equals(orgType)){
                    if (!"10".equals(pvpLoanApp.getGuarMode())) {//非抵押
                        //新增临时档案任务
                        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                        centralFileTaskdto.setSerno(pvpLoanApp.getIqpSerno());
                        centralFileTaskdto.setTraceId(pvpLoanApp.getContNo());
                        centralFileTaskdto.setCusId(pvpLoanApp.getCusId());
                        centralFileTaskdto.setCusName(pvpLoanApp.getCusName());
                        centralFileTaskdto.setBizType("LS006"); // LS006	零售放款申请（生成打印模式）
                        centralFileTaskdto.setInputId(pvpLoanApp.getInputId());
                        centralFileTaskdto.setInputBrId(pvpLoanApp.getInputBrId());
                        centralFileTaskdto.setOptType("02"); // 纯指令
                        centralFileTaskdto.setTaskType("02"); // 档案暂存
                        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                        centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                        centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                        cmisBizClientService.createCentralFileTask(centralFileTaskdto);
                        log.info("零售放款审核流程-生成打印模式：" + "流水号：" + resultInstanceDto.getBizId() + "-归档成功");
                    }
                }
            }
        }
    }

    /**
     * 发送短信
     * @author shenli
     * @date 2021-9-6 09:34:28
     **/
    private void sendMessage(ResultInstanceDto resultInstanceDto,String result) {
        String pvpSerno = resultInstanceDto.getBizId();
        log.info("零售放款审核流程-生成打印模式："+"流水号："+pvpSerno+"-发送短信开始------------------");
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
        String managerId = pvpLoanApp.getManagerId();

        try {
            String mgrTel = "";
            log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }

            if (!StringUtils.isBlank(mgrTel)) {
                String messageType = "MSG_LS_M_0001";// 短信编号
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", resultInstanceDto.getBizUserName());
                paramMap.put("flowName", "零售放款审核");
                paramMap.put("result", result);

                //执行发送借款人操作
                log.info("零售放款审核流程-生成打印模式："+"流水号："+resultInstanceDto.getBizId()+"-发送短信------------------");
                ResultDto<Integer> resultMessageDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);

            }
        } catch (Exception e) {
            log.info("零售放款审核流程-生成打印模式："+"流水号："+pvpSerno+"-发送短信失败------------------"+e);
        }
        log.info("零售放款审核流程-生成打印模式："+"流水号："+pvpSerno+"-发送短信结束------------------");
    }

    /**
     * 推送影像审批信息
     *
     * @author shenli
     * @date 2021-9-29 19:52:42
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {

        //根据流程实例获取所有审批意见
        ResultDto<List<ResultCommentDto>> resultCommentDtos = workflowCoreClient.getAllComments(resultInstanceDto.getInstanceId());
        List<ResultCommentDto> data = resultCommentDtos.getData();
        //审批人
        String approveUserId = "";
        for (ResultCommentDto resultCommentDto : data) {
            String nodeId = resultCommentDto.getNodeId();

            if ("233_6".equals(nodeId) || "233_7".equals(nodeId)){//集中作业零售合同审批岗A,集中作业零售合同审批岗B
                String userId = resultCommentDto.getUserId();
                if(!approveUserId.contains(userId)){
                    //审批人不能重复
                    approveUserId = approveUserId+","+userId;
                }
            }
        }

        if(approveUserId.length() > 0){
            approveUserId = approveUserId.substring(1);
            Map<String, Object> params = new HashMap<>();
            params = resultInstanceDto.getParam();
            String contNo = (String) params.get("contNo");
            bizCommonService.sendImage(contNo,"GRXFDKCZJB;GRXFDKCZDY;GRXFDKCZZY;GRXFDKCZBZDB;GRXFDKCZDCCZ",approveUserId);
        }
    }
}

