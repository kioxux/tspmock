/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContExt
 * @类描述: ctr_cont_ext数据实体类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-27 20:03:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "ctr_cont_ext")
public class CtrContExt extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 展期协议编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "EXT_CTR_NO")
    private String extCtrNo;

    /**
     * 展期申请流水号
     **/
    @Column(name = "EXT_SERNO", unique = false, nullable = false, length = 40)
    private String extSerno;

    /**
     * 原合同编号
     **/
    @Column(name = "OLD_CONT_NO", unique = false, nullable = true, length = 40)
    private String oldContNo;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 合同金额
     **/
    @Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal contAmt;

    /**
     * 合同起始日
     **/
    @Column(name = "CONT_START_DATE", unique = false, nullable = true, length = 10)
    private String contStartDate;

    /**
     * 合同到期日
     **/
    @Column(name = "CONT_END_DATE", unique = false, nullable = true, length = 10)
    private String contEndDate;

    /**
     * 贷款合同类型
     **/
    @Column(name = "CONT_TYPE", unique = false, nullable = true, length = 5)
    private String contType;

    /**
     * 是否使用授信额度
     **/
    @Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
    private String isUtilLmt;

    /**
     * 授信额度编号
     **/
    @Column(name = "LMT_NO", unique = false, nullable = true, length = 40)
    private String lmtNo;

    /**
     * 批复编号
     **/
    @Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
    private String replyNo;

    /**
     * 备注
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 200)
    private String remark;

    /**
     * 签订日期
     **/
    @Column(name = "SIGN_DATE", unique = false, nullable = true, length = 10)
    private String signDate;

    /**
     * 撤销日期
     **/
    @Column(name = "LOGOUT_DATE", unique = false, nullable = true, length = 10)
    private String logoutDate;

    /**
     * 主办机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 展期协议状态 STD_ZB_CTR_ST
     **/
    @Column(name = "EXT_CTR_STATUS", unique = false, nullable = true, length = 5)
    private String extCtrStatus;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;


    /**
     * @param extCtrNo
     */
    public void setExtCtrNo(String extCtrNo) {
        this.extCtrNo = extCtrNo;
    }

    /**
     * @return extCtrNo
     */
    public String getExtCtrNo() {
        return this.extCtrNo;
    }

    /**
     * @param extSerno
     */
    public void setExtSerno(String extSerno) {
        this.extSerno = extSerno;
    }

    /**
     * @return extSerno
     */
    public String getExtSerno() {
        return this.extSerno;
    }

    /**
     * @param oldContNo
     */
    public void setOldContNo(String oldContNo) {
        this.oldContNo = oldContNo;
    }

    /**
     * @return oldContNo
     */
    public String getOldContNo() {
        return this.oldContNo;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param contAmt
     */
    public void setContAmt(java.math.BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    /**
     * @return contAmt
     */
    public java.math.BigDecimal getContAmt() {
        return this.contAmt;
    }

    /**
     * @param contStartDate
     */
    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    /**
     * @return contStartDate
     */
    public String getContStartDate() {
        return this.contStartDate;
    }

    /**
     * @param contEndDate
     */
    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    /**
     * @return contEndDate
     */
    public String getContEndDate() {
        return this.contEndDate;
    }

    /**
     * @param contType
     */
    public void setContType(String contType) {
        this.contType = contType;
    }

    /**
     * @return contType
     */
    public String getContType() {
        return this.contType;
    }

    /**
     * @param isUtilLmt
     */
    public void setIsUtilLmt(String isUtilLmt) {
        this.isUtilLmt = isUtilLmt;
    }

    /**
     * @return isUtilLmt
     */
    public String getIsUtilLmt() {
        return this.isUtilLmt;
    }

    /**
     * @param lmtNo
     */
    public void setLmtNo(String lmtNo) {
        this.lmtNo = lmtNo;
    }

    /**
     * @return lmtNo
     */
    public String getLmtNo() {
        return this.lmtNo;
    }

    /**
     * @param replyNo
     */
    public void setReplyNo(String replyNo) {
        this.replyNo = replyNo;
    }

    /**
     * @return replyNo
     */
    public String getReplyNo() {
        return this.replyNo;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param signDate
     */
    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    /**
     * @return signDate
     */
    public String getSignDate() {
        return this.signDate;
    }

    /**
     * @param logoutDate
     */
    public void setLogoutDate(String logoutDate) {
        this.logoutDate = logoutDate;
    }

    /**
     * @return logoutDate
     */
    public String getLogoutDate() {
        return this.logoutDate;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param extCtrStatus
     */
    public void setExtCtrStatus(String extCtrStatus) {
        this.extCtrStatus = extCtrStatus;
    }

    /**
     * @return extCtrStatus
     */
    public String getExtCtrStatus() {
        return this.extCtrStatus;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}