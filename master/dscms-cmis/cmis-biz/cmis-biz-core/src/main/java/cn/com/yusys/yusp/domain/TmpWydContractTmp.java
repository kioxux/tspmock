/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


import javax.persistence.Id;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydContract
 * @类描述: tmp_wyd_contract数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_contract_tmp")
public class TmpWydContractTmp {
	
	/** 合同号 **/
	@Id
	@Column(name = "CONTRACT_NO")
	private String contractNo;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 额度编号 **/
	@Column(name = "LIMIT_NO", unique = false, nullable = true, length = 64)
	private String limitNo;
	
	/** 机构号 **/
	@Column(name = "ORG_ID", unique = false, nullable = true, length = 20)
	private String orgId;
	
	/** 客户号 **/
	@Column(name = "CUST_ID", unique = false, nullable = true, length = 40)
	private String custId;
	
	/** 客户证件类型 **/
	@Column(name = "CUST_IDTYPE", unique = false, nullable = true, length = 20)
	private String custIdtype;
	
	/** 客户证件号码 **/
	@Column(name = "CUST_IDNO", unique = false, nullable = true, length = 30)
	private String custIdno;
	
	/** 客户名称 **/
	@Column(name = "CUST_NAME", unique = false, nullable = true, length = 60)
	private String custName;
	
	/** 贷款合同生效日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 贷款合同原始到期日 **/
	@Column(name = "MATURITY_DATE", unique = false, nullable = true, length = 10)
	private String maturityDate;
	
	/** 贷款合同终止日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 币种 **/
	@Column(name = "CCY_CD", unique = false, nullable = true, length = 10)
	private String ccyCd;
	
	/** 银（社）团贷款标志 **/
	@Column(name = "LOAN_FLG", unique = false, nullable = true, length = 1)
	private String loanFlg;
	
	/** 转贷款标志 **/
	@Column(name = "SUB_LOAN_FLG", unique = false, nullable = true, length = 1)
	private String subLoanFlg;
	
	/** 贷款合同金额 **/
	@Column(name = "CONTRACT_AMT", unique = false, nullable = true, length = 20)
	private String contractAmt;
	
	/** 帐号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 40)
	private String acctNo;
	
	/** 账户类型 **/
	@Column(name = "ACCT_TYPE", unique = false, nullable = true, length = 10)
	private String acctType;
	
	/** 企业电子邮箱 **/
	@Column(name = "ENTERPRISE_EMAIL", unique = false, nullable = true, length = 100)
	private String enterpriseEmail;
	
	/** 法定代表人邮箱 **/
	@Column(name = "LEGAL_EMAIL", unique = false, nullable = true, length = 100)
	private String legalEmail;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param limitNo
	 */
	public void setLimitNo(String limitNo) {
		this.limitNo = limitNo;
	}
	
    /**
     * @return limitNo
     */
	public String getLimitNo() {
		return this.limitNo;
	}
	
	/**
	 * @param orgId
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
    /**
     * @return orgId
     */
	public String getOrgId() {
		return this.orgId;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custIdtype
	 */
	public void setCustIdtype(String custIdtype) {
		this.custIdtype = custIdtype;
	}
	
    /**
     * @return custIdtype
     */
	public String getCustIdtype() {
		return this.custIdtype;
	}
	
	/**
	 * @param custIdno
	 */
	public void setCustIdno(String custIdno) {
		this.custIdno = custIdno;
	}
	
    /**
     * @return custIdno
     */
	public String getCustIdno() {
		return this.custIdno;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param maturityDate
	 */
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	
    /**
     * @return maturityDate
     */
	public String getMaturityDate() {
		return this.maturityDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param ccyCd
	 */
	public void setCcyCd(String ccyCd) {
		this.ccyCd = ccyCd;
	}
	
    /**
     * @return ccyCd
     */
	public String getCcyCd() {
		return this.ccyCd;
	}
	
	/**
	 * @param loanFlg
	 */
	public void setLoanFlg(String loanFlg) {
		this.loanFlg = loanFlg;
	}
	
    /**
     * @return loanFlg
     */
	public String getLoanFlg() {
		return this.loanFlg;
	}
	
	/**
	 * @param subLoanFlg
	 */
	public void setSubLoanFlg(String subLoanFlg) {
		this.subLoanFlg = subLoanFlg;
	}
	
    /**
     * @return subLoanFlg
     */
	public String getSubLoanFlg() {
		return this.subLoanFlg;
	}
	
	/**
	 * @param contractAmt
	 */
	public void setContractAmt(String contractAmt) {
		this.contractAmt = contractAmt;
	}
	
    /**
     * @return contractAmt
     */
	public String getContractAmt() {
		return this.contractAmt;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctType
	 */
	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}
	
    /**
     * @return acctType
     */
	public String getAcctType() {
		return this.acctType;
	}
	
	/**
	 * @param enterpriseEmail
	 */
	public void setEnterpriseEmail(String enterpriseEmail) {
		this.enterpriseEmail = enterpriseEmail;
	}
	
    /**
     * @return enterpriseEmail
     */
	public String getEnterpriseEmail() {
		return this.enterpriseEmail;
	}
	
	/**
	 * @param legalEmail
	 */
	public void setLegalEmail(String legalEmail) {
		this.legalEmail = legalEmail;
	}
	
    /**
     * @return legalEmail
     */
	public String getLegalEmail() {
		return this.legalEmail;
	}


}