/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfLivingRoom
 * @类描述: guar_inf_living_room数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:42:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_inf_living_room")
public class GuarInfLivingRoom extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 现房/期房标识 STD_ZB_PERIOD_HOUSE **/
	@Column(name = "READY_OR_PERIOD_HOUSE", unique = false, nullable = true, length = 10)
	private String readyOrPeriodHouse;
	
	/** 预购商品房预告登记证明号 **/
	@Column(name = "PURCHSE_HOUSE_NO", unique = false, nullable = true, length = 40)
	private String purchseHouseNo;
	
	/** 预购商品房抵押权预告登记证明号 **/
	@Column(name = "PURCHSE_HOUSE_REG_NO", unique = false, nullable = true, length = 40)
	private String purchseHouseRegNo;
	
	/** 预售许可证编号 **/
	@Column(name = "PRESELL_PERMIT_NO", unique = false, nullable = true, length = 40)
	private String presellPermitNo;
	
	/** 预售许可证有效期 **/
	@Column(name = "PRESELL_PERMIT_VAL_DATE", unique = false, nullable = true, length = 10)
	private String presellPermitValDate;
	
	/** 预计房屋交付时间 **/
	@Column(name = "P_GET_DATE", unique = false, nullable = true, length = 10)
	private String pGetDate;
	
	/** 预计预抵押凭证取得时间 **/
	@Column(name = "P_CREDIT_GET_DATE", unique = false, nullable = true, length = 10)
	private String pCreditGetDate;
	
	/** 预计产权证办理时间 **/
	@Column(name = "P_DO_DATE", unique = false, nullable = true, length = 10)
	private String pDoDate;
	
	/** 预计他项权证取得时间 **/
	@Column(name = "P_OTHER_DATE", unique = false, nullable = true, length = 10)
	private String pOtherDate;
	
	/** 一手/二手标识 STD_ZB_IS_USED **/
	@Column(name = "IS_USED", unique = false, nullable = true, length = 10)
	private String isUsed;
	
	/** 是否两证合一 STD_ZB_YES_NO **/
	@Column(name = "TWOCARD2ONE_IND", unique = false, nullable = true, length = 10)
	private String twocard2oneInd;
	
	/** 产权证号 **/
	@Column(name = "HOUSE_LAND_NO", unique = false, nullable = true, length = 40)
	private String houseLandNo;
	
	/** 销售许可证编号 **/
	@Column(name = "MARKET_PERMIT_NO", unique = false, nullable = true, length = 40)
	private String marketPermitNo;
	
	/** 该产证是否全部抵押  STD_ZB_YES_NO **/
	@Column(name = "HOUSE_ALL_PLEDGE_IND", unique = false, nullable = true, length = 10)
	private String houseAllPledgeInd;
	
	/** 部分抵押描述 **/
	@Column(name = "PART_REG_POSITION_DESC", unique = false, nullable = true, length = 750)
	private String partRegPositionDesc;
	
	/** 房地产买卖合同编号 **/
	@Column(name = "BUSINESS_HOUSE_NO", unique = false, nullable = true, length = 40)
	private String businessHouseNo;
	
	/** 合同签订日期 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 购买价格（元） **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 销售备案  STD_ZB_YES_NO **/
	@Column(name = "SALE_BAK", unique = false, nullable = true, length = 10)
	private String saleBak;
	
	/** 是否本次申请所购房产  STD_ZB_YES_NO **/
	@Column(name = "APPLY_FOR_HOUSE", unique = false, nullable = true, length = 10)
	private String applyForHouse;
	
	/** 抵押住房是否权属人唯一住所 STD_ZB_YES_NO **/
	@Column(name = "HOUSE_OWNERSHIP_IND", unique = false, nullable = true, length = 10)
	private String houseOwnershipInd;
	
	/** 建筑面积 **/
	@Column(name = "BUILD_AREA", unique = false, nullable = true, length = 6)
	private Double buildArea;
	
	/** 建成年份 **/
	@Column(name = "ACTIVATE_YEARS", unique = false, nullable = true, length = 10)
	private String activateYears;
	
	/** 房屋产权期限信息（年） **/
	@Column(name = "HOUSE_PR_DESC", unique = false, nullable = true, length = 3)
	private String housePrDesc;
	
	/** 楼龄 **/
	@Column(name = "FLOOR_AGE", unique = false, nullable = true, length = 3)
	private String floorAge;
	
	/** 套型 STD_ZB_F_STY **/
	@Column(name = "F_STY", unique = false, nullable = true, length = 10)
	private String fSty;
	
	/** 朝向 STD_ZB_ORIENTATIONS **/
	@Column(name = "ORIENTATIONS", unique = false, nullable = true, length = 10)
	private String orientations;
	
	/** 房屋结构 STD_ZB_HOUSE_STRUCTURE **/
	@Column(name = "HOUSE_STRUCTURE", unique = false, nullable = true, length = 10)
	private String houseStructure;
	
	/** 地面构造 STD_ZB_GROUND_STRUCTURE **/
	@Column(name = "GROUND_STRUCTURE", unique = false, nullable = true, length = 10)
	private String groundStructure;
	
	/** 屋顶构造 STD_ZB_GROUND_STRUCTURE **/
	@Column(name = "ROOF_STRUCTURE", unique = false, nullable = true, length = 10)
	private String roofStructure;
	
	/** 房屋状态 STD_ZB_HOUSE_STA **/
	@Column(name = "HOUSE_STA", unique = false, nullable = true, length = 10)
	private String houseSta;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 街道/村镇/路名 **/
	@Column(name = "STREET", unique = false, nullable = true, length = 100)
	private String street;
	
	/** 门牌号/弄号 **/
	@Column(name = "HOUSE_NO", unique = false, nullable = true, length = 100)
	private String houseNo;
	
	/** 楼号 **/
	@Column(name = "BUILDING_ROOM_NUM", unique = false, nullable = true, length = 100)
	private String buildingRoomNum;
	
	/** 室号 **/
	@Column(name = "ROOM_NUM", unique = false, nullable = true, length = 100)
	private String roomNum;
	
	/** 产权地址 **/
	@Column(name = "POC_ADDR", unique = false, nullable = true, length = 100)
	private String pocAddr;
	
	/** 楼盘（社区）名称 **/
	@Column(name = "COMMUNITY_NAME", unique = false, nullable = true, length = 100)
	private String communityName;
	
	/** 层次（标的楼层） **/
	@Column(name = "BDLC", unique = false, nullable = true, length = 3)
	private String bdlc;
	
	/** 层数（标的楼高） **/
	@Column(name = "BDGD", unique = false, nullable = true, length = 3)
	private String bdgd;
	
	/** 房地产所在地段情况   STD_ZB_HOUSE_PLACE_INFO **/
	@Column(name = "HOUSE_PLACE_INFO", unique = false, nullable = true, length = 10)
	private String housePlaceInfo;
	
	/** 周边环境 STD_ZB_ARR_ENV **/
	@Column(name = "ARR_ENV", unique = false, nullable = true, length = 10)
	private String arrEnv;
	
	/** 建筑物说明 **/
	@Column(name = "BUILD_DESC", unique = false, nullable = true, length = 750)
	private String buildDesc;
	
	/** 土地证号 **/
	@Column(name = "LAND_NO", unique = false, nullable = true, length = 40)
	private String landNo;
	
	/** 土地使用权性质  STD_ZB_LAND_USE_QUAL **/
	@Column(name = "LAND_USE_QUAL", unique = false, nullable = true, length = 10)
	private String landUseQual;
	
	/** 土地使用权取得方式 STD_ZB_LAND_USE_WAY **/
	@Column(name = "LAND_USE_WAY", unique = false, nullable = true, length = 10)
	private String landUseWay;
	
	/** 土地使用权面积 **/
	@Column(name = "LAND_USE_AREA", unique = false, nullable = true, length = 18)
	private String landUseArea;
	
	/** 土地使用权单位 **/
	@Column(name = "LAND_USE_UNIT", unique = false, nullable = true, length = 100)
	private String landUseUnit;
	
	/** 土地使用权使用年限起始日期 **/
	@Column(name = "LAND_USE_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	@Column(name = "LAND_USE_END_DATE", unique = false, nullable = true, length = 10)
	private String landUseEndDate;
	
	/** 土地使用年限 **/
	@Column(name = "LAND_USE_YEARS", unique = false, nullable = true, length = 100)
	private String landUseYears;
	
	/** 土地用途 STD_ZB_LAND_PURP **/
	@Column(name = "LAND_PURP", unique = false, nullable = true, length = 10)
	private String landPurp;
	
	/** 土地说明 **/
	@Column(name = "LAND_EXPLAIN", unique = false, nullable = true, length = 750)
	private String landExplain;
	
	/** 竣工日期 **/
	@Column(name = "COMPLETE_DATE", unique = false, nullable = true, length = 10)
	private String completeDate;
	
	/** 公共配套 STD_ZB_PUBLIC_FACILITIES **/
	@Column(name = "PUBLIC_FACILITIES", unique = false, nullable = true, length = 10)
	private String publicFacilities;
	
	/** 楼层情况 **/
	@Column(name = "FLOOR", unique = false, nullable = true, length = 10)
	private String floor;
	
	/** 装修状况 STD_ZB_DECORATION **/
	@Column(name = "DECORATION", unique = false, nullable = true, length = 10)
	private String decoration;
	
	/** 通风采光 STD_ZB_AND_LIGHTING **/
	@Column(name = "VENTILATION_AND_LIGHTING", unique = false, nullable = true, length = 10)
	private String ventilationAndLighting;
	
	/** 临街状况 STD_ZB_STREET_SITUATION **/
	@Column(name = "STREET_SITUATION", unique = false, nullable = true, length = 10)
	private String streetSituation;
	
	/** 房屋用途 STD_ZB_HOUSE_USE_TYPE **/
	@Column(name = "HOUSE_USE_TYPE", unique = false, nullable = true, length = 10)
	private String houseUseType;
	
	/** 承租人名称 **/
	@Column(name = "LESSEE_NAME", unique = false, nullable = true, length = 10)
	private String lesseeName;
	
	/** 市场租金 **/
	@Column(name = "MARKET_RENT", unique = false, nullable = true, length = 26)
	private java.math.BigDecimal marketRent;
	
	/** 租赁合同生效日 **/
	@Column(name = "LEASE_CON_EFT_DT", unique = false, nullable = true, length = 10)
	private String leaseConEftDt;
	
	/** 租赁合同到期日 **/
	@Column(name = "LEASE_CON_END_DT", unique = false, nullable = true, length = 10)
	private String leaseConEndDt;
	
	/** 年租金（元） **/
	@Column(name = "ANNUAL_RENT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal annualRent;
	
	/** 租期（月） **/
	@Column(name = "LEASE", unique = false, nullable = true, length = 10)
	private String lease;
	
	/** 剩余租期（月） **/
	@Column(name = "LEFT_LEASE", unique = false, nullable = true, length = 10)
	private String leftLease;
	
	/** 修改次数 **/
	@Column(name = "MODIFY_NUM", unique = false, nullable = true, length = 10)
	private Integer modifyNum;
	
	/** 房产取得方式 STD_ZB_ESTATE_ACQUIRE_WAY **/
	@Column(name = "ESTATE_ACQUIRE_WAY", unique = false, nullable = true, length = 2)
	private String estateAcquireWay;
	
	/** 已使用年限 **/
	@Column(name = "IN_USE_YEAR", unique = false, nullable = true, length = 10)
	private String inUseYear;
	
	/** 居住用房类型 STD_ZB_HOUSE_TYPE **/
	@Column(name = "HOUSE_TYPE", unique = false, nullable = true, length = 2)
	private String houseType;
	
	/** 租赁形式 STD_ZB_LEASE_TYPE **/
	@Column(name = "LEASE_TYPE", unique = false, nullable = true, length = 2)
	private String leaseType;
	
	/** 开发商名称 **/
	@Column(name = "DEVELOPERS_NAME", unique = false, nullable = true, length = 100)
	private String developersName;
	
	/** 预计不动产权证取得日期 **/
	@Column(name = "FETCH_CERTI_DATE", unique = false, nullable = true, length = 10)
	private String fetchCertiDate;
	
	/** 发证日期 **/
	@Column(name = "ISSUE_CERTI_DATE", unique = false, nullable = true, length = 10)
	private String issueCertiDate;
	
	/** 分摊面积 **/
	@Column(name = "APPORT_AREA", unique = false, nullable = true, length = 100)
	private String apportArea;
	
	/** 平面布局 **/
	@Column(name = "PLANE_LAYOUT", unique = false, nullable = true, length = 500)
	private String planeLayout;
	
	/** 物业情况 **/
	@Column(name = "PROPERTY_CASE", unique = false, nullable = true, length = 10)
	private String propertyCase;
	
	/** 房产使用情况 **/
	@Column(name = "HOUSE_PROPERTY", unique = false, nullable = true, length = 10)
	private String houseProperty;
	
	/** 是否有独立产权证 STD_ZB_YES_NO **/
	@Column(name = "PROPERTY_PERMITS_IND", unique = false, nullable = true, length = 10)
	private String propertyPermitsInd;
	
	/** 车库/车位类型 STD_ZB_CARPORT_TYPE **/
	@Column(name = "CARPORT_TYPE", unique = false, nullable = true, length = 10)
	private String carportType;
	
	/** 车库具体位置 **/
	@Column(name = "DETAIL_ADD", unique = false, nullable = true, length = 100)
	private String detailAdd;
	
	/** 所属地段 **/
	@Column(name = "BELONG_AREA", unique = false, nullable = true, length = 2)
	private String belongArea;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 操作类型   **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param readyOrPeriodHouse
	 */
	public void setReadyOrPeriodHouse(String readyOrPeriodHouse) {
		this.readyOrPeriodHouse = readyOrPeriodHouse;
	}
	
    /**
     * @return readyOrPeriodHouse
     */
	public String getReadyOrPeriodHouse() {
		return this.readyOrPeriodHouse;
	}
	
	/**
	 * @param purchseHouseNo
	 */
	public void setPurchseHouseNo(String purchseHouseNo) {
		this.purchseHouseNo = purchseHouseNo;
	}
	
    /**
     * @return purchseHouseNo
     */
	public String getPurchseHouseNo() {
		return this.purchseHouseNo;
	}
	
	/**
	 * @param purchseHouseRegNo
	 */
	public void setPurchseHouseRegNo(String purchseHouseRegNo) {
		this.purchseHouseRegNo = purchseHouseRegNo;
	}
	
    /**
     * @return purchseHouseRegNo
     */
	public String getPurchseHouseRegNo() {
		return this.purchseHouseRegNo;
	}
	
	/**
	 * @param presellPermitNo
	 */
	public void setPresellPermitNo(String presellPermitNo) {
		this.presellPermitNo = presellPermitNo;
	}
	
    /**
     * @return presellPermitNo
     */
	public String getPresellPermitNo() {
		return this.presellPermitNo;
	}
	
	/**
	 * @param presellPermitValDate
	 */
	public void setPresellPermitValDate(String presellPermitValDate) {
		this.presellPermitValDate = presellPermitValDate;
	}
	
    /**
     * @return presellPermitValDate
     */
	public String getPresellPermitValDate() {
		return this.presellPermitValDate;
	}
	
	/**
	 * @param pGetDate
	 */
	public void setPGetDate(String pGetDate) {
		this.pGetDate = pGetDate;
	}
	
    /**
     * @return pGetDate
     */
	public String getPGetDate() {
		return this.pGetDate;
	}
	
	/**
	 * @param pCreditGetDate
	 */
	public void setPCreditGetDate(String pCreditGetDate) {
		this.pCreditGetDate = pCreditGetDate;
	}
	
    /**
     * @return pCreditGetDate
     */
	public String getPCreditGetDate() {
		return this.pCreditGetDate;
	}
	
	/**
	 * @param pDoDate
	 */
	public void setPDoDate(String pDoDate) {
		this.pDoDate = pDoDate;
	}
	
    /**
     * @return pDoDate
     */
	public String getPDoDate() {
		return this.pDoDate;
	}
	
	/**
	 * @param pOtherDate
	 */
	public void setPOtherDate(String pOtherDate) {
		this.pOtherDate = pOtherDate;
	}
	
    /**
     * @return pOtherDate
     */
	public String getPOtherDate() {
		return this.pOtherDate;
	}
	
	/**
	 * @param isUsed
	 */
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed;
	}
	
    /**
     * @return isUsed
     */
	public String getIsUsed() {
		return this.isUsed;
	}
	
	/**
	 * @param twocard2oneInd
	 */
	public void setTwocard2oneInd(String twocard2oneInd) {
		this.twocard2oneInd = twocard2oneInd;
	}
	
    /**
     * @return twocard2oneInd
     */
	public String getTwocard2oneInd() {
		return this.twocard2oneInd;
	}
	
	/**
	 * @param houseLandNo
	 */
	public void setHouseLandNo(String houseLandNo) {
		this.houseLandNo = houseLandNo;
	}
	
    /**
     * @return houseLandNo
     */
	public String getHouseLandNo() {
		return this.houseLandNo;
	}
	
	/**
	 * @param marketPermitNo
	 */
	public void setMarketPermitNo(String marketPermitNo) {
		this.marketPermitNo = marketPermitNo;
	}
	
    /**
     * @return marketPermitNo
     */
	public String getMarketPermitNo() {
		return this.marketPermitNo;
	}
	
	/**
	 * @param houseAllPledgeInd
	 */
	public void setHouseAllPledgeInd(String houseAllPledgeInd) {
		this.houseAllPledgeInd = houseAllPledgeInd;
	}
	
    /**
     * @return houseAllPledgeInd
     */
	public String getHouseAllPledgeInd() {
		return this.houseAllPledgeInd;
	}
	
	/**
	 * @param partRegPositionDesc
	 */
	public void setPartRegPositionDesc(String partRegPositionDesc) {
		this.partRegPositionDesc = partRegPositionDesc;
	}
	
    /**
     * @return partRegPositionDesc
     */
	public String getPartRegPositionDesc() {
		return this.partRegPositionDesc;
	}
	
	/**
	 * @param businessHouseNo
	 */
	public void setBusinessHouseNo(String businessHouseNo) {
		this.businessHouseNo = businessHouseNo;
	}
	
    /**
     * @return businessHouseNo
     */
	public String getBusinessHouseNo() {
		return this.businessHouseNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param saleBak
	 */
	public void setSaleBak(String saleBak) {
		this.saleBak = saleBak;
	}
	
    /**
     * @return saleBak
     */
	public String getSaleBak() {
		return this.saleBak;
	}
	
	/**
	 * @param applyForHouse
	 */
	public void setApplyForHouse(String applyForHouse) {
		this.applyForHouse = applyForHouse;
	}
	
    /**
     * @return applyForHouse
     */
	public String getApplyForHouse() {
		return this.applyForHouse;
	}
	
	/**
	 * @param houseOwnershipInd
	 */
	public void setHouseOwnershipInd(String houseOwnershipInd) {
		this.houseOwnershipInd = houseOwnershipInd;
	}
	
    /**
     * @return houseOwnershipInd
     */
	public String getHouseOwnershipInd() {
		return this.houseOwnershipInd;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(Double buildArea) {
		this.buildArea = buildArea;
	}
	
    /**
     * @return buildArea
     */
	public Double getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param activateYears
	 */
	public void setActivateYears(String activateYears) {
		this.activateYears = activateYears;
	}
	
    /**
     * @return activateYears
     */
	public String getActivateYears() {
		return this.activateYears;
	}
	
	/**
	 * @param housePrDesc
	 */
	public void setHousePrDesc(String housePrDesc) {
		this.housePrDesc = housePrDesc;
	}
	
    /**
     * @return housePrDesc
     */
	public String getHousePrDesc() {
		return this.housePrDesc;
	}
	
	/**
	 * @param floorAge
	 */
	public void setFloorAge(String floorAge) {
		this.floorAge = floorAge;
	}
	
    /**
     * @return floorAge
     */
	public String getFloorAge() {
		return this.floorAge;
	}
	
	/**
	 * @param fSty
	 */
	public void setFSty(String fSty) {
		this.fSty = fSty;
	}
	
    /**
     * @return fSty
     */
	public String getFSty() {
		return this.fSty;
	}
	
	/**
	 * @param orientations
	 */
	public void setOrientations(String orientations) {
		this.orientations = orientations;
	}
	
    /**
     * @return orientations
     */
	public String getOrientations() {
		return this.orientations;
	}
	
	/**
	 * @param houseStructure
	 */
	public void setHouseStructure(String houseStructure) {
		this.houseStructure = houseStructure;
	}
	
    /**
     * @return houseStructure
     */
	public String getHouseStructure() {
		return this.houseStructure;
	}
	
	/**
	 * @param groundStructure
	 */
	public void setGroundStructure(String groundStructure) {
		this.groundStructure = groundStructure;
	}
	
    /**
     * @return groundStructure
     */
	public String getGroundStructure() {
		return this.groundStructure;
	}
	
	/**
	 * @param roofStructure
	 */
	public void setRoofStructure(String roofStructure) {
		this.roofStructure = roofStructure;
	}
	
    /**
     * @return roofStructure
     */
	public String getRoofStructure() {
		return this.roofStructure;
	}
	
	/**
	 * @param houseSta
	 */
	public void setHouseSta(String houseSta) {
		this.houseSta = houseSta;
	}
	
    /**
     * @return houseSta
     */
	public String getHouseSta() {
		return this.houseSta;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param houseNo
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}
	
    /**
     * @return houseNo
     */
	public String getHouseNo() {
		return this.houseNo;
	}
	
	/**
	 * @param buildingRoomNum
	 */
	public void setBuildingRoomNum(String buildingRoomNum) {
		this.buildingRoomNum = buildingRoomNum;
	}
	
    /**
     * @return buildingRoomNum
     */
	public String getBuildingRoomNum() {
		return this.buildingRoomNum;
	}
	
	/**
	 * @param roomNum
	 */
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}
	
    /**
     * @return roomNum
     */
	public String getRoomNum() {
		return this.roomNum;
	}
	
	/**
	 * @param pocAddr
	 */
	public void setPocAddr(String pocAddr) {
		this.pocAddr = pocAddr;
	}
	
    /**
     * @return pocAddr
     */
	public String getPocAddr() {
		return this.pocAddr;
	}
	
	/**
	 * @param communityName
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}
	
    /**
     * @return communityName
     */
	public String getCommunityName() {
		return this.communityName;
	}
	
	/**
	 * @param bdlc
	 */
	public void setBdlc(String bdlc) {
		this.bdlc = bdlc;
	}
	
    /**
     * @return bdlc
     */
	public String getBdlc() {
		return this.bdlc;
	}
	
	/**
	 * @param bdgd
	 */
	public void setBdgd(String bdgd) {
		this.bdgd = bdgd;
	}
	
    /**
     * @return bdgd
     */
	public String getBdgd() {
		return this.bdgd;
	}
	
	/**
	 * @param housePlaceInfo
	 */
	public void setHousePlaceInfo(String housePlaceInfo) {
		this.housePlaceInfo = housePlaceInfo;
	}
	
    /**
     * @return housePlaceInfo
     */
	public String getHousePlaceInfo() {
		return this.housePlaceInfo;
	}
	
	/**
	 * @param arrEnv
	 */
	public void setArrEnv(String arrEnv) {
		this.arrEnv = arrEnv;
	}
	
    /**
     * @return arrEnv
     */
	public String getArrEnv() {
		return this.arrEnv;
	}
	
	/**
	 * @param buildDesc
	 */
	public void setBuildDesc(String buildDesc) {
		this.buildDesc = buildDesc;
	}
	
    /**
     * @return buildDesc
     */
	public String getBuildDesc() {
		return this.buildDesc;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo;
	}
	
    /**
     * @return landNo
     */
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual;
	}
	
    /**
     * @return landUseQual
     */
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay;
	}
	
    /**
     * @return landUseWay
     */
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseArea
	 */
	public void setLandUseArea(String landUseArea) {
		this.landUseArea = landUseArea;
	}
	
    /**
     * @return landUseArea
     */
	public String getLandUseArea() {
		return this.landUseArea;
	}
	
	/**
	 * @param landUseUnit
	 */
	public void setRightUseUnit(String landUseUnit) {
		this.landUseUnit = landUseUnit;
	}
	
    /**
     * @return landUseUnit
     */
	public String getRightUseUnit() {
		return this.landUseUnit;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate;
	}
	
    /**
     * @return landUseBeginDate
     */
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate;
	}
	
    /**
     * @return landUseEndDate
     */
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(String landUseYears) {
		this.landUseYears = landUseYears;
	}
	
    /**
     * @return landUseYears
     */
	public String getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp;
	}
	
    /**
     * @return landPurp
     */
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain;
	}
	
    /**
     * @return landExplain
     */
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param completeDate
	 */
	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate;
	}
	
    /**
     * @return completeDate
     */
	public String getCompleteDate() {
		return this.completeDate;
	}
	
	/**
	 * @param publicFacilities
	 */
	public void setPublicFacilities(String publicFacilities) {
		this.publicFacilities = publicFacilities;
	}
	
    /**
     * @return publicFacilities
     */
	public String getPublicFacilities() {
		return this.publicFacilities;
	}
	
	/**
	 * @param floor
	 */
	public void setFloor(String floor) {
		this.floor = floor;
	}
	
    /**
     * @return floor
     */
	public String getFloor() {
		return this.floor;
	}
	
	/**
	 * @param decoration
	 */
	public void setDecoration(String decoration) {
		this.decoration = decoration;
	}
	
    /**
     * @return decoration
     */
	public String getDecoration() {
		return this.decoration;
	}
	
	/**
	 * @param ventilationAndLighting
	 */
	public void setVentilationAndLighting(String ventilationAndLighting) {
		this.ventilationAndLighting = ventilationAndLighting;
	}
	
    /**
     * @return ventilationAndLighting
     */
	public String getVentilationAndLighting() {
		return this.ventilationAndLighting;
	}
	
	/**
	 * @param streetSituation
	 */
	public void setStreetSituation(String streetSituation) {
		this.streetSituation = streetSituation;
	}
	
    /**
     * @return streetSituation
     */
	public String getStreetSituation() {
		return this.streetSituation;
	}
	
	/**
	 * @param houseUseType
	 */
	public void setHouseUseType(String houseUseType) {
		this.houseUseType = houseUseType;
	}
	
    /**
     * @return houseUseType
     */
	public String getHouseUseType() {
		return this.houseUseType;
	}
	
	/**
	 * @param lesseeName
	 */
	public void setLesseeName(String lesseeName) {
		this.lesseeName = lesseeName;
	}
	
    /**
     * @return lesseeName
     */
	public String getLesseeName() {
		return this.lesseeName;
	}
	
	/**
	 * @param marketRent
	 */
	public void setMarketRent(java.math.BigDecimal marketRent) {
		this.marketRent = marketRent;
	}
	
    /**
     * @return marketRent
     */
	public java.math.BigDecimal getMarketRent() {
		return this.marketRent;
	}
	
	/**
	 * @param leaseConEftDt
	 */
	public void setLeaseConEftDt(String leaseConEftDt) {
		this.leaseConEftDt = leaseConEftDt;
	}
	
    /**
     * @return leaseConEftDt
     */
	public String getLeaseConEftDt() {
		return this.leaseConEftDt;
	}
	
	/**
	 * @param leaseConEndDt
	 */
	public void setLeaseConEndDt(String leaseConEndDt) {
		this.leaseConEndDt = leaseConEndDt;
	}
	
    /**
     * @return leaseConEndDt
     */
	public String getLeaseConEndDt() {
		return this.leaseConEndDt;
	}
	
	/**
	 * @param annualRent
	 */
	public void setAnnualRent(java.math.BigDecimal annualRent) {
		this.annualRent = annualRent;
	}
	
    /**
     * @return annualRent
     */
	public java.math.BigDecimal getAnnualRent() {
		return this.annualRent;
	}
	
	/**
	 * @param lease
	 */
	public void setLease(String lease) {
		this.lease = lease;
	}
	
    /**
     * @return lease
     */
	public String getLease() {
		return this.lease;
	}
	
	/**
	 * @param leftLease
	 */
	public void setLeftLease(String leftLease) {
		this.leftLease = leftLease;
	}
	
    /**
     * @return leftLease
     */
	public String getLeftLease() {
		return this.leftLease;
	}
	
	/**
	 * @param modifyNum
	 */
	public void setModifyNum(Integer modifyNum) {
		this.modifyNum = modifyNum;
	}
	
    /**
     * @return modifyNum
     */
	public Integer getModifyNum() {
		return this.modifyNum;
	}
	
	/**
	 * @param estateAcquireWay
	 */
	public void setEstateAcquireWay(String estateAcquireWay) {
		this.estateAcquireWay = estateAcquireWay;
	}
	
    /**
     * @return estateAcquireWay
     */
	public String getEstateAcquireWay() {
		return this.estateAcquireWay;
	}
	
	/**
	 * @param inUseYear
	 */
	public void setInUseYear(String inUseYear) {
		this.inUseYear = inUseYear;
	}
	
    /**
     * @return inUseYear
     */
	public String getInUseYear() {
		return this.inUseYear;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	
    /**
     * @return houseType
     */
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param leaseType
	 */
	public void setLeaseType(String leaseType) {
		this.leaseType = leaseType;
	}
	
    /**
     * @return leaseType
     */
	public String getLeaseType() {
		return this.leaseType;
	}
	
	/**
	 * @param developersName
	 */
	public void setDevelopersName(String developersName) {
		this.developersName = developersName;
	}
	
    /**
     * @return developersName
     */
	public String getDevelopersName() {
		return this.developersName;
	}
	
	/**
	 * @param fetchCertiDate
	 */
	public void setFetchCertiDate(String fetchCertiDate) {
		this.fetchCertiDate = fetchCertiDate;
	}
	
    /**
     * @return fetchCertiDate
     */
	public String getFetchCertiDate() {
		return this.fetchCertiDate;
	}
	
	/**
	 * @param issueCertiDate
	 */
	public void setIssueCertiDate(String issueCertiDate) {
		this.issueCertiDate = issueCertiDate;
	}
	
    /**
     * @return issueCertiDate
     */
	public String getIssueCertiDate() {
		return this.issueCertiDate;
	}
	
	/**
	 * @param apportArea
	 */
	public void setApportArea(String apportArea) {
		this.apportArea = apportArea;
	}
	
    /**
     * @return apportArea
     */
	public String getApportArea() {
		return this.apportArea;
	}
	
	/**
	 * @param planeLayout
	 */
	public void setPlaneLayout(String planeLayout) {
		this.planeLayout = planeLayout;
	}
	
    /**
     * @return planeLayout
     */
	public String getPlaneLayout() {
		return this.planeLayout;
	}
	
	/**
	 * @param propertyCase
	 */
	public void setPropertyCase(String propertyCase) {
		this.propertyCase = propertyCase;
	}
	
    /**
     * @return propertyCase
     */
	public String getPropertyCase() {
		return this.propertyCase;
	}
	
	/**
	 * @param houseProperty
	 */
	public void setHouseProperty(String houseProperty) {
		this.houseProperty = houseProperty;
	}
	
    /**
     * @return houseProperty
     */
	public String getHouseProperty() {
		return this.houseProperty;
	}
	
	/**
	 * @param propertyPermitsInd
	 */
	public void setPropertyPermitsInd(String propertyPermitsInd) {
		this.propertyPermitsInd = propertyPermitsInd;
	}
	
    /**
     * @return propertyPermitsInd
     */
	public String getPropertyPermitsInd() {
		return this.propertyPermitsInd;
	}
	
	/**
	 * @param carportType
	 */
	public void setCarportType(String carportType) {
		this.carportType = carportType;
	}
	
    /**
     * @return carportType
     */
	public String getCarportType() {
		return this.carportType;
	}
	
	/**
	 * @param detailAdd
	 */
	public void setDetailAdd(String detailAdd) {
		this.detailAdd = detailAdd;
	}
	
    /**
     * @return detailAdd
     */
	public String getDetailAdd() {
		return this.detailAdd;
	}
	
	/**
	 * @param belongArea
	 */
	public void setBelongArea(String belongArea) {
		this.belongArea = belongArea;
	}
	
    /**
     * @return belongArea
     */
	public String getBelongArea() {
		return this.belongArea;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}