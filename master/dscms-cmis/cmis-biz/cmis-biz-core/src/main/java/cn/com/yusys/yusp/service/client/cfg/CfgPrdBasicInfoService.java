package cn.com.yusys.yusp.service.client.cfg;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author 王玉坤
 * @version 1.0.0se
 * @date 2021-09-01
 * @desc 产品相关类
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CfgPrdBasicInfoService {
    private Logger logger = LoggerFactory.getLogger(CfgPrdBasicInfoService.class);

    @Autowired
    DscmsCfgClientService dscmsCfgClientService;

    /**
     * @param prdId
     * @return cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto
     * @author 王玉坤
     * @date 2021/9/1 20:26
     * @version 1.0.0
     * @desc 根据产品代码查询生效的产品信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CfgPrdBasicinfoDto getPrdBasicInfo(String prdId) {
        logger.info("根据产品代码【{}】查询产品信息开始", prdId);
        ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoResultDto = null;
        CfgPrdBasicinfoDto cfgPrdBasicinfoTemp = null;
        CfgPrdBasicinfoDto cfgPrdBasicinfo = null;
        try {
            cfgPrdBasicinfoTemp = new CfgPrdBasicinfoDto();
            cfgPrdBasicinfoTemp.setPrdId(prdId);
            cfgPrdBasicinfoDtoResultDto = dscmsCfgClientService.queryCfgprdInfoByprcId(cfgPrdBasicinfoTemp);

            // 判断产品条件
            if (!Objects.isNull(cfgPrdBasicinfoDtoResultDto) && !Objects.isNull(cfgPrdBasicinfoDtoResultDto.getData())) {
                cfgPrdBasicinfo = cfgPrdBasicinfoDtoResultDto.getData();
            } else {
                logger.info("根据产品代码【{}】, 未查询到产品信息", prdId);
                throw BizException.error(null, null, "未查询到产品信息！");
            }

            // 判断产品是否生效 01--生效 02--失效
            if (!"01".equals(cfgPrdBasicinfo.getPrdStatus())) {
                logger.info("根据产品代码【{}】,产品状态不为正常状态！", prdId);
                throw BizException.error(null, null, "产品状态不为正常状态或系统维护中，请稍后重试！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("根据产品代码【{}】查询产品信息开始", prdId);
        return cfgPrdBasicinfo;
    }

}
