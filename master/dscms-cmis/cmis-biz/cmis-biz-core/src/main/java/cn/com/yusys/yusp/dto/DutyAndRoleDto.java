package cn.com.yusys.yusp.dto;

import java.util.List;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/27 14:15
 * @desc  用户角色和岗位
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class DutyAndRoleDto {

    private List<UserAndDutyRespDto> dutyList; // 岗位

    private List<CommonUserQueryRespDto> roleList; // 角色

    public List<UserAndDutyRespDto> getDutyList() {
        return dutyList;
    }

    public void setDutyList(List<UserAndDutyRespDto> dutyList) {
        this.dutyList = dutyList;
    }

    public List<CommonUserQueryRespDto> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<CommonUserQueryRespDto> roleList) {
        this.roleList = roleList;
    }
}
