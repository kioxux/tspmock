package cn.com.yusys.yusp.service.server.xdxw0023;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0023.req.Xdxw0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0023.resp.Xdxw0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import javax.annotation.Resource;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0023Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ys
 * @创建时间: 2021-06-04 08:50:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0023Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0023Service.class);

    @Resource
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;

    /**
     * 在小贷是否有调查申请记录
     *
     * @param xdxw0023DataReqDto
     * @return xdxw0023DataRespDto
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0023DataRespDto xdxw0023(Xdxw0023DataReqDto xdxw0023DataReqDto) throws Exception {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, JSON.toJSONString(xdxw0023DataReqDto));
        Xdxw0023DataRespDto xdxw0023DataRespDto = new Xdxw0023DataRespDto();
        try {
            if (StringUtils.isEmpty(xdxw0023DataReqDto.getCertNo())) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            } else {
                // 证件号
                String certCode = xdxw0023DataReqDto.getCertNo();
               // 总笔数
                int num = lmtSurveyReportMainInfoMapper.getLmtSurveyReportMainInfoCountByCertCode(certCode);
                xdxw0023DataRespDto.setNum(num);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, JSON.toJSONString(xdxw0023DataRespDto));
        return xdxw0023DataRespDto;
    }
}
