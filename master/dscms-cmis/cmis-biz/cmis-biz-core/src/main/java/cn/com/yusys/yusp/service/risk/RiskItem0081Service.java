package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.BizMustCheckDetails;
import cn.com.yusys.yusp.domain.LmtAppRelGuar;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import cn.com.yusys.yusp.domain.LmtSigInvestSubApp;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.repository.mapper.LmtAppRelGuarMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestAppMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestSubAppMapper;
import cn.com.yusys.yusp.service.BizInvestCommonService;
import cn.com.yusys.yusp.service.BizMustCheckDetailsService;
import cn.com.yusys.yusp.service.LmtSigInvestAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 页面必输性校验
 */
@Service
public class RiskItem0081Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0081Service.class);

    //biz_must_check_details
    @Autowired
    private BizMustCheckDetailsService bizMustCheckDetailsService;

    @Autowired
    private LmtSigInvestSubAppMapper lmtSigInvestSubAppMapper;

    @Autowired
    private LmtAppRelGuarMapper lmtAppRelGuarMapper;

    /**
     * 页面必输性校验
     * @param queryModel
     * @return
     */
    public RiskResultDto riskItem0081(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("页面必输性校验开始*******************业务流水号：【{}】", serno);
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }

        //担保与增新人校验
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("serno",serno);
        queryModel1.addCondition("isExistGuarCreditect", CmisBizConstants.STD_ZB_YES_NO_Y);
        queryModel1.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestSubApp> lmtSigInvestSubAppList = lmtSigInvestSubAppMapper.selectByModel(queryModel1);
        if (CollectionUtils.nonEmpty(lmtSigInvestSubAppList)){
            //判断是否存在担保记录
            List<LmtAppRelGuar> lmtAppRelGuars = lmtAppRelGuarMapper.selectByModel(queryModel1);
            if (CollectionUtils.isEmpty(lmtAppRelGuars)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("担保及增信情况信息不完整！");
                return riskResultDto;
            }
        }

        List<BizMustCheckDetails> bizMustCheckDetails = bizMustCheckDetailsService.selectBySerno(serno);
        if (CollectionUtils.nonEmpty(bizMustCheckDetails)){
            for (BizMustCheckDetails bizMustCheckDetail : bizMustCheckDetails) {
                if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(bizMustCheckDetail.getFinFlag())){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(bizMustCheckDetail.getPageName()+"信息不完整！");
                    return riskResultDto;
                }
            }
        }else{
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
            return riskResultDto;
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }

}
