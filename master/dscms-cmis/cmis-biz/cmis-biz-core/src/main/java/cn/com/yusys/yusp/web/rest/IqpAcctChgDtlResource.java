package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpAcctChgDtl;
import cn.com.yusys.yusp.service.IqpAcctChgDtlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAcctChgDtlResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-29 15:50:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpacctchgdtl")
public class IqpAcctChgDtlResource {
    @Autowired
    private IqpAcctChgDtlService iqpAcctChgDtlService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpAcctChgDtl>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpAcctChgDtl> list = iqpAcctChgDtlService.selectAll(queryModel);
        return new ResultDto<List<IqpAcctChgDtl>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpAcctChgDtl>> index(QueryModel queryModel) {
        List<IqpAcctChgDtl> list = iqpAcctChgDtlService.selectByModel(queryModel);
        return new ResultDto<List<IqpAcctChgDtl>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpAcctChgDtl> show(@PathVariable("pkId") String pkId) {
        IqpAcctChgDtl iqpAcctChgDtl = iqpAcctChgDtlService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpAcctChgDtl>(iqpAcctChgDtl);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpAcctChgDtl> create(@RequestBody IqpAcctChgDtl iqpAcctChgDtl) throws URISyntaxException {
        iqpAcctChgDtlService.insert(iqpAcctChgDtl);
        return new ResultDto<IqpAcctChgDtl>(iqpAcctChgDtl);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpAcctChgDtl iqpAcctChgDtl) throws URISyntaxException {
        int result = iqpAcctChgDtlService.update(iqpAcctChgDtl);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpAcctChgDtlService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpAcctChgDtlService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
