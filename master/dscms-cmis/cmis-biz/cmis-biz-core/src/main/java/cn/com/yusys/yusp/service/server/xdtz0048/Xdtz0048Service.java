package cn.com.yusys.yusp.service.server.xdtz0048;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.domain.PvpBillContOnlineRel;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgGenerateTempFileDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import cn.com.yusys.yusp.dto.server.xdtz0048.req.Xdtz0048DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0048.resp.Xdtz0048DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.PvpBillContOnlineRelMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.*;

/**
 * 接口处理类:小贷借款借据文本生成pdf
 *
 * @author zoubiao
 * @version 1.0
 */
@Service
public class Xdtz0048Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0048Service.class);

    @Resource
    private DscmsCfgClientService dscmsCfgClientService;
    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号
    @Autowired
    private PvpBillContOnlineRelMapper pvpBillContOnlineRelMapper;
    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    /**
     * 交易描述：小贷借款借据文本生成pdf
     *
     * @param xdtz0048DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdtz0048DataRespDto getXdtz0048(Xdtz0048DataReqDto xdtz0048DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, JSON.toJSONString(xdtz0048DataReqDto));
        //返回对象
        Xdtz0048DataRespDto xdtz0048DataRespDto = new Xdtz0048DataRespDto();
        try {
            //********************************************获取传参********************************************
            String cusName = xdtz0048DataReqDto.getCusName();//客户名称
            String contNo = xdtz0048DataReqDto.getContNo();//合同编号
            String cnContNo = xdtz0048DataReqDto.getCnContNo();//中文合同编号
            String billNo = xdtz0048DataReqDto.getBillNo();//借据编号
            String loanAmt = xdtz0048DataReqDto.getLoanAmt();//放款金额
            String curType = xdtz0048DataReqDto.getCurType();//币种 CNY
            String loanUseType = xdtz0048DataReqDto.getLoanUseType();//借款用途
            String yearRate = xdtz0048DataReqDto.getYearRate();//年利率
            String payType = xdtz0048DataReqDto.getPayType();//支付方式 01受托支付 02自主支付 03	借新还旧/无还本续贷
            String loanStartDate = xdtz0048DataReqDto.getLoanStartDate();//借款起始日期
            String loanEndDate = xdtz0048DataReqDto.getLoanEndDate();//借款到期日期
            String loanAcctNo = xdtz0048DataReqDto.getLoanAcctNo();//放款账号
            String huser = xdtz0048DataReqDto.getHuser();//经办人
            String applyDate = xdtz0048DataReqDto.getApplyDate();//申请日期
            String isdzqy = xdtz0048DataReqDto.getIsdzqy();//是否电子签约：1-是 0-否

            //********************************************请求参数必输校验********************************************
            if (StringUtils.isBlank(cusName)) {
                throw new Exception("客户名称不能为空！");
            } else if (StringUtils.isBlank(contNo)) {
                throw new Exception("合同编号不能为空！");
            } else if (StringUtils.isBlank(cnContNo)) {
                //throw new Exception("中文合同编号不能为空！");
            } else if (StringUtils.isBlank(billNo)) {
                //throw new Exception("借据编号不能为空！");
            } else if (StringUtils.isBlank(loanAmt)) {
                //throw new Exception("放款金额不能为空！");
            } else if (StringUtils.isBlank(curType)) {
                //throw new Exception("币种不能为空！");
            } else if (StringUtils.isBlank(loanUseType)) {
                //throw new Exception("借款用途不能为空！");
            } else if (StringUtils.isBlank(yearRate)) {
                //throw new Exception("年利率不能为空！");
            } else if (StringUtils.isBlank(loanStartDate)) {
                //throw new Exception("借款起始日期不能为空！");
            } else if (StringUtils.isBlank(loanEndDate)) {
                //throw new Exception("借款到期日期不能为空！");
            } else if (StringUtils.isBlank(loanAcctNo)) {
                //throw new Exception("放款账号不能为空！");
            } else if (StringUtils.isBlank(applyDate)) {
                //throw new Exception("申请日期不能为空！");
            }
            // ********************************************查询合同信息********************************************
            logger.info("根据合同编号【{}】查询合同信息开始", contNo);
            CtrLoanCont ctrLoanCont = Optional.ofNullable(ctrLoanContMapper.selectByPrimaryKey(contNo)).orElse(new CtrLoanCont());
            logger.info("***************根据合同编号【{}】查询合同信息结束,查询结果信息【{}】", contNo, JSON.toJSONString(ctrLoanCont));


            // ********************************************查询参数********************************************
            Map queryMap = new HashMap<String, String>();
            queryMap.put("billNo", billNo);//借据号
            queryMap.put("contNo", contNo);//合同号
            String pvpSerno = pvpLoanAppMapper.selectPvpLoanAppSernoSumByContno(queryMap);

            String TempleteName = "xwyx-jjht.cpt";// 合同模板名字
            String saveFileName = "jkjj_" + contNo + "";//借款借据 （模板名字+合同申请流水号）

            //********************************************查询服务器配置********************************************
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("pkId", "00001");// 查询配置信息
            ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
            List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
            CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);
            //**********************查询存储地址信息**********************
            String ip = cfgGenerateTempFileDto.getLoginIp();
            String port = cfgGenerateTempFileDto.getLoginPort();
            String username = cfgGenerateTempFileDto.getLoginUsername();
            String password = cfgGenerateTempFileDto.getLoginPwd();
            String path = cfgGenerateTempFileDto.getFilePath();
            String url = cfgGenerateTempFileDto.getMemo();

            //**********************调用帆软的生成pdf的方法，传入帆软报表生成需要的参数*********************
            String prdId = ctrLoanCont.getPrdId();
            HashMap<String, Object> parameterMap = new HashMap<String, Object>();
            if (StringUtil.isNotEmpty(billNo)) {
                parameterMap.put("billNo", billNo);//借据
            } else if ("SC020009".equals(prdId) || "SC020010".equals(prdId) || "SC010008".equals(prdId)) {
                //优抵贷、优农贷、优企贷自动生成借据编号
                Map seqMap = new HashMap();
                seqMap.put("contNo", contNo);
                billNo = cmisBizXwCommonService.getBillNo(contNo);

                //先修改同一个合同且状态为0的记录的状态，确保唯一性
                logger.info("**********XDTZ0048**先修改同一个合同且状态为0的记录的状态，确保唯一性,插入参数为:{}", JSON.toJSONString(seqMap));
                int num = pvpBillContOnlineRelMapper.updatePvpBillContByPrimaryKey(seqMap);
                logger.info("**********XDTZ0048**先修改同一个合同且状态为0的记录的状态,返回参数为:{}", JSON.toJSONString(num));
                parameterMap.put("billNo", billNo);//借据
                String pkid = UUID.randomUUID().toString();
                PvpBillContOnlineRel record = new PvpBillContOnlineRel();
                record.setPkId(pkid);
                record.setBillNo(billNo);
                record.setContNo(contNo);
                record.setStatus("0");
                logger.info("**********XDTZ0048**优抵贷、优农贷、优企贷自动生成借据编号记录开始,插入参数为:{}", JSON.toJSONString(record));
                int count = pvpBillContOnlineRelMapper.insert(record);
                logger.info("**********XDTZ0048**优抵贷、优农贷、优企贷自动生成借据编号记录结束,返回参数为:{}", JSON.toJSONString(count));
            }
            parameterMap.put("cusName", cusName);//客户名称
            parameterMap.put("loanAcctNo", loanAcctNo);//放款账号
            parameterMap.put("pvpSerno", pvpSerno);//
            parameterMap.put("contNo", contNo);//
            parameterMap.put("contCnNo", ctrLoanCont.getContCnNo());//中文合同号
            /**********************借款用途码值转换**
             * 经营性贷款：资金周转
             * 消费性贷款：循环消费是综合消费，一次性用信是按实际借款用途填，抓取合同上的借款用途
             * 无还本续贷是：归还贷款
             * *******************/
            if (StringUtil.isNotEmpty(prdId)) {
                LmtCrdReplyInfo lmtCrdReplyInfo = Optional.ofNullable(lmtCrdReplyInfoMapper.selectBySurveySerno(ctrLoanCont.getSurveySerno())).orElse(new LmtCrdReplyInfo());
                String isWxbxd = lmtCrdReplyInfo.getIsWxbxd();
                String limitType = lmtCrdReplyInfo.getLimitType();//[{"key":"01","value":"临时额度"},{"key":"02","value":"循环额度"}]
                if ("1".equals(isWxbxd)) {//无还本续贷
                    loanUseType = "归还贷款";
                } else {
                    logger.info("***********调用iCmisCfgClientService查询产品类别*START**************");
                    ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                    String prdCode = prdresultDto.getCode();//返回结果
                    String prdType = net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils.EMPTY;
                    if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                        CfgPrdBasicinfoDto CfgPrdBasicinfoDto = prdresultDto.getData();
                        if (CfgPrdBasicinfoDto != null) {
                            prdType = CfgPrdBasicinfoDto.getPrdType();
                        }
                    }
                    if (DscmsBizTzEnum.PRDTYPE_08.key.equals(prdType)) {//经营
                        loanUseType = "资金周转";
                    } else if (DscmsBizTzEnum.PRDTYPE_09.key.equals(prdType)) {//消费
                        if ("02".equals(limitType)) {
                            loanUseType = "综合消费";
                        } else {
                            loanUseType = ctrLoanCont.getLoanPurp();
                        }
                    } else {
                        loanUseType = ctrLoanCont.getLoanPurp();
                    }
                    logger.info("***********调用iCmisCfgClientService查询产品类别*END**************");
                }
                parameterMap.put("loanUseType", loanUseType);//借款用途
            } else {
                parameterMap.put("loanUseType", loanUseType);//借款用途
            }
            parameterMap.put("loanAmt", loanAmt);//借款金额
            parameterMap.put("yearRate", yearRate);//借款利率
            parameterMap.put("loanStartDate", loanStartDate);//借款起始日
            parameterMap.put("loanEndDate", loanEndDate);//借款到期日
            parameterMap.put("payType", payType);//借款支付方式
            parameterMap.put("applyDate", applyDate);//申请日期
            parameterMap.put("isdzqy", isdzqy);//是否电子签约
            //获取客户经理
            String managerId = ctrLoanCont.getManagerId();
            if (StringUtil.isNotEmpty(managerId)) {
                String uaerName = managerId;
                logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                String code = resultDto.getCode();//返回结果
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    uaerName = adminSmUserDto.getUserName();
                }
                parameterMap.put("uaerName", uaerName);//经办人
            }
            //2、调用公共方法生成pdf
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            if (StringUtils.isEmpty(url)) {
                // url = "http://10.28.206.191:8090/dscms/frpt/api/frpt/createFrptPdf";//本地测试地址
            }
            // ********************************************生成PDF文件********************************************
            try {
                FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                frptPdfArgsDto.setPdfFileName(TempleteName);//模板名称
                frptPdfArgsDto.setNewFileName(saveFileName);//待生成的PDF文件名称
                frptPdfArgsDto.setSerno(contNo);//设置流水
                frptPdfArgsDto.setPath(path);//路径
                frptPdfArgsDto.setIp(ip);//IP地址
                frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                frptPdfArgsDto.setUserName(username);
                frptPdfArgsDto.setPassWord(password);
                frptPdfArgsDto.setMap(parameterMap);
                HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                String code = responseEntity.getBody();
            } catch (Exception e) {
                logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            }

            //********************************************返回参数********************************************
            String pdfDepoAddr = saveFileName + "#" + ip + "#" + port + "#" + username + "#" + password + "#" + path;
            String pdfFileName = saveFileName + ".pdf";
            xdtz0048DataRespDto.setPdfDepoAddr(path);
            xdtz0048DataRespDto.setPdfFileName(pdfFileName);
            xdtz0048DataRespDto.setFtpAddr(ip);
            xdtz0048DataRespDto.setPort(port);
            xdtz0048DataRespDto.setUserName(username);
            xdtz0048DataRespDto.setPassword(password);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, JSON.toJSONString(xdtz0048DataRespDto));
        return xdtz0048DataRespDto;
    }
}