/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.domain.IqpAccpAppPorderSub;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.vo.IqpDiscAppPorderSubImportVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpDiscAppPorderSub;
import cn.com.yusys.yusp.service.IqpDiscAppPorderSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDiscAppPorderSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-12 15:06:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "贴现协议申请汇票明细")
@RequestMapping("/api/iqpdiscapppordersub")
public class IqpDiscAppPorderSubResource {
    private static final Logger log = LoggerFactory.getLogger(IqpDiscAppPorderSubResource.class);
    @Autowired
    private IqpDiscAppPorderSubService iqpDiscAppPorderSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpDiscAppPorderSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpDiscAppPorderSub> list = iqpDiscAppPorderSubService.selectAll(queryModel);
        return new ResultDto<List<IqpDiscAppPorderSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpDiscAppPorderSub>> index(QueryModel queryModel) {
        List<IqpDiscAppPorderSub> list = iqpDiscAppPorderSubService.selectByModel(queryModel);
        return new ResultDto<List<IqpDiscAppPorderSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpDiscAppPorderSub> show(@PathVariable("pkId") String pkId) {
        IqpDiscAppPorderSub iqpDiscAppPorderSub = iqpDiscAppPorderSubService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpDiscAppPorderSub>(iqpDiscAppPorderSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpDiscAppPorderSub> create(@RequestBody IqpDiscAppPorderSub iqpDiscAppPorderSub) throws URISyntaxException {
        iqpDiscAppPorderSubService.insert(iqpDiscAppPorderSub);
        return new ResultDto<IqpDiscAppPorderSub>(iqpDiscAppPorderSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpDiscAppPorderSub iqpDiscAppPorderSub) throws URISyntaxException {
        int result = iqpDiscAppPorderSubService.update(iqpDiscAppPorderSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpDiscAppPorderSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpDiscAppPorderSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * 贴现汇票明细保存方法
     * @param iqpDiscAppPorderSub
     * @return
     */
    @ApiOperation("贴现汇票明细保存方法")
    @PostMapping("/saveidapordersubinfo")
    public ResultDto<Map> saveIqpDiscAppPorderSubInfo(@RequestBody IqpDiscAppPorderSub iqpDiscAppPorderSub){
        Map result = iqpDiscAppPorderSubService.saveIadPorderSubInfo(iqpDiscAppPorderSub);
        return new ResultDto<>(result);
    }

    /**
     * 贴现汇票明细信息逻辑删除方法
     * @param iqpDiscAppPorderSub
     * @return
     */
    @ApiOperation("银承票据明细信息逻辑删除方法")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody IqpDiscAppPorderSub iqpDiscAppPorderSub){
        Integer result = iqpDiscAppPorderSubService.logicDelete(iqpDiscAppPorderSub);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询数据列表")
    @PostMapping("/selectlist")
    protected ResultDto<List<IqpDiscAppPorderSub>> selectList(@RequestBody QueryModel queryModel) {
        List<IqpDiscAppPorderSub> list = iqpDiscAppPorderSubService.selectByModel(queryModel);
        return new ResultDto<List<IqpDiscAppPorderSub>>(list);
    }

    /**
     * 异步下载贴现汇票明细模板
     */
    @PostMapping("/exportIqpDiscAppPorderSubTemp")
    public ResultDto<ProgressDto> asyncExportIqpDiscAppPorderSubTemp() {
        ProgressDto progressDto = iqpDiscAppPorderSubService.asyncExportIqpDiscAppPorderSubTemp();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据，批量导入贴现汇票明细
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importIqpDiscAppPorderSub")
    public ResultDto<ProgressDto> asyncImportIqpDiscAppPorderSub(@RequestParam("fileId") String fileId, @RequestBody Map map) throws IOException{
        String serno;
        if(map.containsKey("serno")){
            serno = (String) map.get("serno");
        }else{
            return ResultDto.error(500,"业务流水号缺失！导入失败");
        }
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        } catch (IOException e) {
            log.error(EclEnum.ECL070099.value,e);
            throw BizException.error(null, EclEnum.ECL070099.key,EclEnum.ECL070099.value);
        }

        // 将文件内容导入数据库，StudentScore为导入数据的类
        ProgressDto progressDto = ExcelUtils.asyncImport(ImportContext.of(IqpDiscAppPorderSubImportVo.class)
                // 批量操作需要将batch设置为true
                .batch(true)
                .file(tempFile)
                .dataStorage(ExcelUtils.batchConsumer(iqpDiscAppPorderSubService::insertIqpDiscAppPorderSub)));
        log.info("开始执行异步导出，导入taskId为[{}];", progressDto.getTaskId());
        return ResultDto.success(progressDto);
    }
}
