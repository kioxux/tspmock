/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtChgDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtAdjustDetail;
import cn.com.yusys.yusp.repository.mapper.LmtAdjustDetailMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAdjustDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-09 23:10:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtAdjustDetailService {

    @Resource
    private LmtAdjustDetailMapper lmtAdjustDetailMapper;

    @Autowired
    private  LmtAppService lmtAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtAdjustDetail selectByPrimaryKey(String pkId) {
        return lmtAdjustDetailMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtAdjustDetail> selectAll(QueryModel model) {
        List<LmtAdjustDetail> records = (List<LmtAdjustDetail>) lmtAdjustDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtAdjustDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtAdjustDetail> list = lmtAdjustDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtAdjustDetail record) {
        return lmtAdjustDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtAdjustDetail record) {
        return lmtAdjustDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtAdjustDetail record) {
        return lmtAdjustDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtAdjustDetail record) {
        return lmtAdjustDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtAdjustDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtAdjustDetailMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:selectByLmtSerno
     * @函数描述:查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    public LmtAdjustDetail selectByLmtSerno(String lmtSerno) {
        return lmtAdjustDetailMapper.selectByLmtSerno(lmtSerno);
    }

    /**
     * @函数名称:queryDetailByLmtSerno
     * @函数描述:根据lmtSerno查询分项数据
     * @参数与返回说明:
     * @算法描述:
     */
    public List<Map<String, String>> queryDetailByLmtSerno(String lmtSerno) {
        List<Map<String,String>> list = new ArrayList<>();
        try{
            List<LmtAdjustDetail> lmtAdjustDetails = lmtAdjustDetailMapper.queryDetailByLmtSerno(lmtSerno);
            if(lmtAdjustDetails.isEmpty() || lmtAdjustDetails.size() == 0){
                return list;
            }else{
                for(LmtAdjustDetail lmtAdjustDetail : lmtAdjustDetails){
                    HashMap<String , String> map = new HashMap();
                    // 处理成员客户
                    // 获取成员客户号
                    LmtApp lmtApp = lmtAppService.selectBySerno(lmtAdjustDetail.getLmtSerno());
                    if(lmtApp != null ){
                        map.put("cusId",lmtApp.getCusId());
                        map.put("cusName",lmtApp.getCusName());
                    }
                    map.put("pkId",lmtAdjustDetail.getPkId());
                    map.put("lmtSerno",lmtAdjustDetail.getLmtSerno());
                    map.put("lmtAdjustContent",lmtAdjustDetail.getLmtAdjustContent());
                    map.put("lmtAdjustResn",lmtAdjustDetail.getLmtAdjustResn());
                    map.put("inputId",lmtAdjustDetail.getInputId());
                    map.put("inputBrId",lmtAdjustDetail.getInputBrId());
                    map.put("inputDate",lmtAdjustDetail.getInputDate());
                    list.add(map);
                }
            }
        }catch(Exception e){
            throw BizException.error(null, e.getLocalizedMessage(), e.getMessage());
        }
        return list;
    }
}
