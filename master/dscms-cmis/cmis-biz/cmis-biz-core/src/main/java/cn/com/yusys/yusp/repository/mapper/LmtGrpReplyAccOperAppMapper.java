/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.LmtGrpReplyChg;
import cn.com.yusys.yusp.domain.LmtReplyAccOperApp;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtGrpReplyAccOperApp;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyAccOperAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:42:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtGrpReplyAccOperAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtGrpReplyAccOperApp selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtGrpReplyAccOperApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtGrpReplyAccOperApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtGrpReplyAccOperApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtGrpReplyAccOperApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtGrpReplyAccOperApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: queryAll
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtGrpReplyAccOperApp> queryAll(QueryModel model);

    /**
     * @方法名称: selectAll
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtGrpReplyAccOperApp> queryHis(QueryModel model);

    /**
     * @方法名称: showdetial
     * @方法描述: 根据集团客户号查询授信成员信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtGrpReplyAccOperApp> showdetial(String grpSerno);

    /**
     * @方法名称: queryLmtgrpReplyAccOperAppByParams
     * @方法描述: 通过条件查询数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: yangwl
     * @创建时间: 2021-05-016 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<LmtGrpReplyAccOperApp> queryLmtgrpReplyAccOperAppByParams(HashMap<String, String> queryMap);

    /**
     * @方法名称: queryByGrpCusId
     * @方法描述: 根据集团客户号查询授信成员信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtReplyAccOperApp> queryByGrpCusId(String grpCusId);

    /**
     * @函数名称:selectLmtReplyAccOperAppByGrpSerno
     * @函数描述:客户额度冻结/解冻/终止申请根据流水号查询
     * @创建者: yangwl
     * @创建时间: 2021/5/21
     * @参数与返回说明:
     * @算法描述:
     */
    LmtGrpReplyAccOperApp selectLmtGrpReplyAccOperAppByGrpSerno(String grpSerno);

    List<LmtGrpReplyAccOperApp> selectLmtGrpReplyAccOperApp(LmtGrpReplyAccOperApp lmtGrpReplyAccOperApp);
}