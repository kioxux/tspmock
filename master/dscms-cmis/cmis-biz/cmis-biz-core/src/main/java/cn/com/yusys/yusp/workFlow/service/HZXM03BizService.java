package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constant.PartnerTypeEnums;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.BusiImageRelInfo;
import cn.com.yusys.yusp.domain.CoopPartnerAgrAccInfo;
import cn.com.yusys.yusp.domain.CoopPartnerAgrApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 合作方协议申请/变更
 */
@Service
public class HZXM03BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(HZXM03BizService.class);

    @Autowired
    private CtrContImageAuditAppService ctrContImageAuditAppService;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CoopPartnerAgrAppService coopPartnerAgrAppService;

    @Autowired
    private CoopPartnerAgrAccInfoService coopPartnerAgrAccInfoService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private BusiImageRelInfoService busiImageRelInfoService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("后业务处理类型" + currentOpType);
        try {
            CoopPartnerAgrApp coopPartnerAgrApp = coopPartnerAgrAppService.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                coopPartnerAgrApp.setApprStatus(CmisBizConstants.APPLY_STATE_APP);
                coopPartnerAgrAppService.update(coopPartnerAgrApp);
                boolean createDoc = false;
                // bug10810 临时档案任务合作方特殊生成规则：合作方是房企，只有第一次生成任务池和档案入档任务，之后的变更不生成任务池和归档任务
                if(Objects.equals(coopPartnerAgrApp.getPartnerType(), PartnerTypeEnums.PARTNER_TYPE_1.getValue())
                        && Objects.equals(resultInstanceDto.getBizType(),"HZ005")){
                    createDoc = true;
                }else if(!Objects.equals(coopPartnerAgrApp.getPartnerType(), PartnerTypeEnums.PARTNER_TYPE_1.getValue())){
                    createDoc = true;
                }
                if("307_9".equals(resultInstanceDto.getCurrentNodeId()) && createDoc) {
                    try {
                        ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(coopPartnerAgrApp.getInputBrId());
                        AdminSmOrgDto adminSmOrgDto = resultDto.getData();
                        if ("1".equals(adminSmOrgDto.getOrgType()) || "2".equals(adminSmOrgDto.getOrgType()) || "3".equals(adminSmOrgDto.getOrgType())){
                            log.info("异地机构无法生成档案池任务");
                        } else {
                            CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                            centralFileTaskdto.setSerno(coopPartnerAgrApp.getSerno());
                            centralFileTaskdto.setCusId(coopPartnerAgrApp.getPartnerNo());
                            centralFileTaskdto.setCusName(coopPartnerAgrApp.getPartnerName());
                            centralFileTaskdto.setBizType(resultInstanceDto.getBizType()); // 合作协议资料
                            centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                            centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                            centralFileTaskdto.setInputId(coopPartnerAgrApp.getInputId());
                            centralFileTaskdto.setInputBrId(coopPartnerAgrApp.getInputBrId());
                            centralFileTaskdto.setOptType("02"); // 非纯指令
                            centralFileTaskdto.setTaskType("02"); // 档案暫存
                            centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                            ResultDto<Integer> centralFileTask = cmisBizClientService.createCentralFileTask(centralFileTaskdto);
                            if(!centralFileTask.getCode().equals("0")){
                                log.info("生成档案池任务失败" +centralFileTask.getMessage());
                            }
                        }
                    } catch (Exception e) {
                        log.info("生成档案池任务失败"+ e);
                    }
                }
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,serno);
                // coopPartnerAgrAppService.createImageSpplInfo(serno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId());
                log.info("合作方协议申请【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                coopPartnerAgrApp.setApprStatus(CmisBizConstants.APPLY_STATE_APP);
                coopPartnerAgrAppService.update(coopPartnerAgrApp);
                log.info("合作方协议申请【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，更新协议申请信息，流程参数【{}】", serno, CmisBizConstants.APPLY_STATE_PASS);
                // 针对流程到办结节点，进行以下处理
                // 1、流程办理结束增加合作方协议台账
                CoopPartnerAgrAccInfo info = new CoopPartnerAgrAccInfo();
                BeanUtils.beanCopy(coopPartnerAgrApp, info);
                info.setCoopAgrSerno(coopPartnerAgrApp.getSerno());
                info.setAgrStatus("01");
                if ("1".equals(coopPartnerAgrApp.getCoopAgrOprType())) {// 新增则插入
                    coopPartnerAgrAccInfoService.wfinsert(info);
                } else if ("2".equals(coopPartnerAgrApp.getCoopAgrOprType())) { // 变更-更新
                    coopPartnerAgrAccInfoService.wfupdate(info);
                }
                else if ("3".equals(coopPartnerAgrApp.getCoopAgrOprType())) { // 续签
                    coopPartnerAgrAccInfoService.wfupdate4resign(info);
                }else {
                    log.error("合作方协议申请【{}】，流程结束操作，未知操作类型【{}】", serno, coopPartnerAgrApp.getCoopAgrOprType());
                }
                try {
                    // 生成系统归档任务
                    log.info("开始合作方系统生成档案归档信息");
                    CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(coopPartnerAgrApp.getPartnerNo());
                    DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                    // 判断是否是异地机构
                    ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(coopPartnerAgrApp.getInputBrId());
                    AdminSmOrgDto adminSmOrgDto = resultDto.getData();
                    if ("1".equals(adminSmOrgDto.getOrgType()) || "2".equals(adminSmOrgDto.getOrgType()) || "3".equals(adminSmOrgDto.getOrgType())){
                        log.info("异地机构生成档案归档信息");
                        docArchiveClientDto.setArchiveMode("01");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
                    } else {
                        if ("1".equals(coopPartnerAgrApp.getPartnerType())||"2".equals(coopPartnerAgrApp.getPartnerType())||"4".equals(coopPartnerAgrApp.getPartnerType())||"8".equals(coopPartnerAgrApp.getPartnerType())){
                            // 走集中作业岗
                            docArchiveClientDto.setArchiveMode("02");
                        } else {
                            // 不走集中作业岗
                            docArchiveClientDto.setArchiveMode("03");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
                        }
                    }

                    boolean createDoc = false;
                    // 档案任务合作方特殊生成规则：合作方是房企，只有第一次生成任务池和档案入档任务，之后的变更不生成任务池和归档任务
                    if(Objects.equals(coopPartnerAgrApp.getPartnerType(), PartnerTypeEnums.PARTNER_TYPE_1.getValue())
                            && Objects.equals(resultInstanceDto.getBizType(),"HZ005")){
                        createDoc = true;
                    }else if(!Objects.equals(coopPartnerAgrApp.getPartnerType(), PartnerTypeEnums.PARTNER_TYPE_1.getValue())){
                        createDoc = true;
                    }
                    if(createDoc) {
                        docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
                        docArchiveClientDto.setDocType("10");// 合作方协议
                        docArchiveClientDto.setBizSerno(serno);
                        docArchiveClientDto.setCusId(coopPartnerAgrApp.getPartnerNo());
                        docArchiveClientDto.setCusName(coopPartnerAgrApp.getPartnerName());
                        docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                        docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                        docArchiveClientDto.setManagerId(coopPartnerAgrApp.getManagerId());
                        docArchiveClientDto.setManagerBrId(coopPartnerAgrApp.getManagerBrId());
                        docArchiveClientDto.setInputId(coopPartnerAgrApp.getInputId());
                        docArchiveClientDto.setInputBrId(coopPartnerAgrApp.getInputBrId());
                        docArchiveClientDto.setContNo(coopPartnerAgrApp.getCoopAgrNo());
                        docArchiveClientDto.setLoanAmt(coopPartnerAgrApp.getCoopAgrAmt());
                        docArchiveClientDto.setStartDate(coopPartnerAgrApp.getCoopAgrStartDate());
                        docArchiveClientDto.setEndDate(coopPartnerAgrApp.getCoopAgrEndDate());
                        int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                        if (num < 1) {
                            log.info("系统生成合作方档案归档信息失败");
                        }
                    }
                } catch (Exception e){
                    log.info("系统生成合作方档案归档信息失败"+ e);
                }
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,serno);
                coopPartnerAgrAppService.createImageSpplInfo(serno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),"合作方协议");
                coopPartnerAgrApp.setApprStatus(CmisBizConstants.APPLY_STATE_PASS);
                coopPartnerAgrAppService.update(coopPartnerAgrApp);

                //调用影像平台交易，为该笔业务影像批量打上已审核标识。
                sendImage(resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                List<NextNodeInfoDto> list = resultInstanceDto.getNextNodeInfos();
                list.stream().forEach(next->{
                    ResultDto<Boolean> flowResultDto = workflowCoreClient.isFirstNode(next.getNextNodeId());
                    if (flowResultDto.getData()) {
                        coopPartnerAgrApp.setApprStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                        coopPartnerAgrAppService.update(coopPartnerAgrApp);
                    }
                });
                log.info("合作方协议申请【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                coopPartnerAgrApp.setApprStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                coopPartnerAgrAppService.update(coopPartnerAgrApp);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                coopPartnerAgrApp.setApprStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                coopPartnerAgrAppService.update(coopPartnerAgrApp);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("合作方协议申请【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                coopPartnerAgrApp.setApprStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                coopPartnerAgrAppService.update(coopPartnerAgrApp);
            } else {
                log.info("合作方协议申请【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("合作方协议申请后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "HZXM03".equals(flowCode);
    }

    /**
     * 审批流程通过后，调用影像平台交易，为该笔业务影像批量打上已审核标识。审核人工号取【集中作业放款初审岗、集中作业放款复审岗】的用户号
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {
        String serno = resultInstanceDto.getBizId();

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        List<BusiImageRelInfo> busiImageRelInfos = busiImageRelInfoService.selectAll(queryModel);

        if (CollectionUtils.isNotEmpty(busiImageRelInfos)){
            //根据流程实例获取所有审批意见
            ResultDto<List<ResultCommentDto>> resultCommentDtos = workflowCoreClient.getAllComments(resultInstanceDto.getInstanceId());

            List<ResultCommentDto> data = resultCommentDtos.getData();
            //审批人
            String approveUserId = "";

            for (ResultCommentDto resultCommentDto : data) {
                String nodeId = resultCommentDto.getNodeId();

                if ("307_21".equals(nodeId) || "307_23".equals(nodeId)){
                    //如果是【集中作业放款初审岗】【集中作业放款复核岗】
                    String userId = resultCommentDto.getUserId();

                    if(!approveUserId.contains(userId)){
                        //审批人不能重复
                        approveUserId = approveUserId+","+userId;
                    }
                }
            }
            log.info("合作协议新增、变更、续签申请流程，流水号【"+serno+"】,approveUserId："+approveUserId);
            approveUserId = approveUserId.substring(1);

            for (BusiImageRelInfo busiImageRelInfo : busiImageRelInfos) {
                String topOutsystemCode = busiImageRelInfo.getTopOutsystemCode();
                //影像编号
                String imageNo = busiImageRelInfo.getImageNo();
                log.info("合作协议新增、变更、续签申请流程，业务流水号【"+serno+"】,影像编号【"+imageNo+"】，topOutsystemCode："+topOutsystemCode);
                bizCommonService.sendImage(imageNo,topOutsystemCode,approveUserId);
            }
        }
    }
}