package cn.com.yusys.yusp.service.client.bsp.core.ln3110;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/21 10:33
 * @desc 贷款还款计划试算
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Ln3110Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw01Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * @param ln3110ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110RespDto
     * @author hubp
     * @date 2021/5/21 10:41
     * @version 1.0.0
     * @desc 贷款还款计划试算
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Ln3110RespDto ln3110(Ln3110ReqDto ln3110ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3110.key, EsbEnum.TRADE_CODE_LN3110.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3110.key, EsbEnum.TRADE_CODE_LN3110.value, JSON.toJSONString(ln3110ReqDto));
        ResultDto<Ln3110RespDto> ln3110ResultDto = dscms2CoreLnClientService.ln3110(ln3110ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3110.key, EsbEnum.TRADE_CODE_LN3110.value, JSON.toJSONString(ln3110ResultDto));
        String ln3110Code = Optional.ofNullable(ln3110ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3110Meesage = Optional.ofNullable(ln3110ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3110RespDto ln3110RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3110ResultDto.getCode())) {
            //  获取相关的值并解析
            ln3110RespDto = ln3110ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(ln3110Code, ln3110Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3110.key, EsbEnum.TRADE_CODE_LN3110.value);
        return ln3110RespDto;
    }
}
