package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCertiInoutApp
 * @类描述: iqp_certi_inout_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-20 15:04:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpCertiInoutAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 担保合同流水号  **/
	private String guarPkId;
	
	/** 担保合同编号 **/
	private String guarContNo;
	
	/** 借款人编号 **/
	private String borrowerId;
	
	/** 担保合同类型 STD_ZB_GRT_TYP **/
	private String guarContType;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 担保金额 **/
	private java.math.BigDecimal guarAmt;
	
	/** 担保起始日 **/
	private String guarStartDate;
	
	/** 担保终止日 **/
	private String guarEndDate;
	
	/** 担保方式 STD_ZB_GUAR_WAY **/
	private String guarWay;
	
	/** 押品包号 **/
	private String packageNo;
	
	/** 待入库权证数量 **/
	private java.math.BigDecimal waitInCertiQnt;
	
	/** 待出库权证数量 **/
	private java.math.BigDecimal waitOutCertiQnt;
	
	/** 取出还贷类型 STD_ZB_TOTLN_TYP **/
	private String extrLoanType;
	
	/** 理由 **/
	private String remark;
	
	/** 权证出入库类型 STD_ZB_EXWA_TYP **/
	private String inoutApptype;
	
	/** 权证状态 **/
	private String certiStatu;
	
	/** 是否记账 STD_ZB_YES_NO **/
	private String isRecord;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 保管机构 **/
	private String keepBrId;
	
	/** 权证临时借用人名称 **/
	private String tempBorrowerName;
	
	/** 权证临时出库日期 **/
	private String tempOutDate;
	
	/** 权证预计归还时间 **/
	private String preBackDate;
	
	/** 权证实际归还日期 **/
	private String realBackDate;
	
	/** 权证临时出库原因 **/
	private String tempOutReason;
	
	/** 账务机构 **/
	private String finaBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarPkId
	 */
	public void setGuarPkId(String guarPkId) {
		this.guarPkId = guarPkId == null ? null : guarPkId.trim();
	}
	
    /**
     * @return GuarPkId
     */	
	public String getGuarPkId() {
		return this.guarPkId;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}
	
    /**
     * @return GuarContNo
     */	
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param borrowerId
	 */
	public void setBorrowerId(String borrowerId) {
		this.borrowerId = borrowerId == null ? null : borrowerId.trim();
	}
	
    /**
     * @return BorrowerId
     */	
	public String getBorrowerId() {
		return this.borrowerId;
	}
	
	/**
	 * @param guarContType
	 */
	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType == null ? null : guarContType.trim();
	}
	
    /**
     * @return GuarContType
     */	
	public String getGuarContType() {
		return this.guarContType;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return GuarAmt
     */	
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param guarStartDate
	 */
	public void setGuarStartDate(String guarStartDate) {
		this.guarStartDate = guarStartDate == null ? null : guarStartDate.trim();
	}
	
    /**
     * @return GuarStartDate
     */	
	public String getGuarStartDate() {
		return this.guarStartDate;
	}
	
	/**
	 * @param guarEndDate
	 */
	public void setGuarEndDate(String guarEndDate) {
		this.guarEndDate = guarEndDate == null ? null : guarEndDate.trim();
	}
	
    /**
     * @return GuarEndDate
     */	
	public String getGuarEndDate() {
		return this.guarEndDate;
	}
	
	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay == null ? null : guarWay.trim();
	}
	
    /**
     * @return GuarWay
     */	
	public String getGuarWay() {
		return this.guarWay;
	}
	
	/**
	 * @param packageNo
	 */
	public void setPackageNo(String packageNo) {
		this.packageNo = packageNo == null ? null : packageNo.trim();
	}
	
    /**
     * @return PackageNo
     */	
	public String getPackageNo() {
		return this.packageNo;
	}
	
	/**
	 * @param waitInCertiQnt
	 */
	public void setWaitInCertiQnt(java.math.BigDecimal waitInCertiQnt) {
		this.waitInCertiQnt = waitInCertiQnt;
	}
	
    /**
     * @return WaitInCertiQnt
     */	
	public java.math.BigDecimal getWaitInCertiQnt() {
		return this.waitInCertiQnt;
	}
	
	/**
	 * @param waitOutCertiQnt
	 */
	public void setWaitOutCertiQnt(java.math.BigDecimal waitOutCertiQnt) {
		this.waitOutCertiQnt = waitOutCertiQnt;
	}
	
    /**
     * @return WaitOutCertiQnt
     */	
	public java.math.BigDecimal getWaitOutCertiQnt() {
		return this.waitOutCertiQnt;
	}
	
	/**
	 * @param extrLoanType
	 */
	public void setExtrLoanType(String extrLoanType) {
		this.extrLoanType = extrLoanType == null ? null : extrLoanType.trim();
	}
	
    /**
     * @return ExtrLoanType
     */	
	public String getExtrLoanType() {
		return this.extrLoanType;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inoutApptype
	 */
	public void setInoutApptype(String inoutApptype) {
		this.inoutApptype = inoutApptype == null ? null : inoutApptype.trim();
	}
	
    /**
     * @return InoutApptype
     */	
	public String getInoutApptype() {
		return this.inoutApptype;
	}
	
	/**
	 * @param certiStatu
	 */
	public void setCertiStatu(String certiStatu) {
		this.certiStatu = certiStatu == null ? null : certiStatu.trim();
	}
	
    /**
     * @return certiStatu
     */	
	public String getCertiStatu() {
		return this.certiStatu;
	}
	
	/**
	 * @param isRecord
	 */
	public void setIsRecord(String isRecord) {
		this.isRecord = isRecord == null ? null : isRecord.trim();
	}
	
    /**
     * @return IsRecord
     */	
	public String getIsRecord() {
		return this.isRecord;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param keepBrId
	 */
	public void setKeepBrId(String keepBrId) {
		this.keepBrId = keepBrId == null ? null : keepBrId.trim();
	}
	
    /**
     * @return KeepBrId
     */	
	public String getKeepBrId() {
		return this.keepBrId;
	}
	
	/**
	 * @param tempBorrowerName
	 */
	public void setTempBorrowerName(String tempBorrowerName) {
		this.tempBorrowerName = tempBorrowerName == null ? null : tempBorrowerName.trim();
	}
	
    /**
     * @return TempBorrowerName
     */	
	public String getTempBorrowerName() {
		return this.tempBorrowerName;
	}
	
	/**
	 * @param tempOutDate
	 */
	public void setTempOutDate(String tempOutDate) {
		this.tempOutDate = tempOutDate == null ? null : tempOutDate.trim();
	}
	
    /**
     * @return TempOutDate
     */	
	public String getTempOutDate() {
		return this.tempOutDate;
	}
	
	/**
	 * @param preBackDate
	 */
	public void setPreBackDate(String preBackDate) {
		this.preBackDate = preBackDate == null ? null : preBackDate.trim();
	}
	
    /**
     * @return PreBackDate
     */	
	public String getPreBackDate() {
		return this.preBackDate;
	}
	
	/**
	 * @param realBackDate
	 */
	public void setRealBackDate(String realBackDate) {
		this.realBackDate = realBackDate == null ? null : realBackDate.trim();
	}
	
    /**
     * @return RealBackDate
     */	
	public String getRealBackDate() {
		return this.realBackDate;
	}
	
	/**
	 * @param tempOutReason
	 */
	public void setTempOutReason(String tempOutReason) {
		this.tempOutReason = tempOutReason == null ? null : tempOutReason.trim();
	}
	
    /**
     * @return TempOutReason
     */	
	public String getTempOutReason() {
		return this.tempOutReason;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId == null ? null : finaBrId.trim();
	}
	
    /**
     * @return FinaBrId
     */	
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/** 权证主键 **/
	private String certiPkId;
	/**
	 * @param certiPkId
	 */
	public void setCertiPkId(String certiPkId) {
		this.certiPkId = certiPkId == null ? null : certiPkId.trim();
	}
	
	/**
	 * @return CertiPkId
	 */
	public String getCertiPkId() {
		return this.certiPkId;
	}
	
	/** 权证正常出库日期 **/
	private String outDate;
	/**
	 * @param outDate
	 */
	public void setOutDate(String outDate) {
		this.outDate = outDate == null ? null : outDate.trim();
	}
	
	/**
	 * @return outDate
	 */
	public String getOutDate() {
		return this.outDate;
	}
	/** 权证入库日期 **/
	private String inDate;
	/**
	 * @param inDate
	 */
	public void setInDate(String inDate) {
		this.inDate = inDate == null ? null : inDate.trim();
	}
	
	/**
	 * @return inDate
	 */
	public String getInDate() {
		return this.inDate;
	}
	
}