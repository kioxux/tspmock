/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpHighAmtAgrApp;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.vo.ImgCondDetailsQueryVo;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ImgCondDetails;
import cn.com.yusys.yusp.service.ImgCondDetailsService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ImgCondDetailsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-13 14:57:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/imgconddetails")
public class ImgCondDetailsResource {
    @Autowired
    private ImgCondDetailsService imgCondDetailsService;

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ImgCondDetails> show(@PathVariable("pkId") String pkId) {
        ImgCondDetails imgCondDetails = imgCondDetailsService.selectByPrimaryKey(pkId);
        return ResultDto.success(imgCondDetails);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ImgCondDetails> create(@RequestBody ImgCondDetails imgCondDetails) {
        imgCondDetailsService.insert(imgCondDetails);
        return ResultDto.success(imgCondDetails);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ImgCondDetails imgCondDetails) {
        int result = imgCondDetailsService.updateSelective(imgCondDetails);
        return ResultDto.success(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = imgCondDetailsService.deleteByPrimaryKey(pkId);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = imgCondDetailsService.deleteByIds(ids);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:queryImgCondDetailsContNo
     * @函数描述:根据合同查询授信落实条件
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryImgCondDetailsContNo")
    protected ResultDto<List<Map<String,Object>>> queryImgCondDetailsContNo(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = imgCondDetailsService.queryImgCondDetailsContNo(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:generateImgCondDetailsData
     * @函数描述:查询授信批复的用信落实情况数据，添加到用信落实情况表
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    @PostMapping("/generateimgconddetailsdata")
    protected int generateImgCondDetailsData(@RequestBody Map map) {
        return imgCondDetailsService.generateImgCondDetailsData(map);
    }

    /**
     * 用信条件落实情况保存
     * @param imgCondDetails
     * @return
     */
    @ApiOperation("新增保存")
    @PostMapping("/saveimgconddetails")
    public ResultDto<Map> saveImgCondDetails(@RequestBody ImgCondDetails imgCondDetails) {
        Map result = imgCondDetailsService.saveImgCondDetails(imgCondDetails);
        return new ResultDto<>(result);
    }
}
