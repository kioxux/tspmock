package cn.com.yusys.yusp.web.server.xdcz0025;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0025.req.Xdcz0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0025.resp.Xdcz0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0025.Xdcz0025Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:未中标业务注销
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0025:未中标业务注销")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0025Resource.class);

    @Autowired
    private Xdcz0025Service xdcz0025Service;

    /**
     * 交易码：xdcz0025
     * 交易描述：未中标业务注销
     *
     * @param xdcz0025DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("未中标业务注销")
    @PostMapping("/xdcz0025")
    protected @ResponseBody
    ResultDto<Xdcz0025DataRespDto> xdcz0025(@Validated @RequestBody Xdcz0025DataReqDto xdcz0025DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, JSON.toJSONString(xdcz0025DataReqDto));
        Xdcz0025DataRespDto xdcz0025DataRespDto = new Xdcz0025DataRespDto();// 响应Dto:小贷用途承诺书文本生成pdf
        ResultDto<Xdcz0025DataRespDto> xdcz0025DataResultDto = new ResultDto<>();
        String cusName = xdcz0025DataReqDto.getBillno();//借据编号
        String loanUseType = xdcz0025DataReqDto.getContno();//保函协议编号
        try {
            // 从xdcz0025DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, JSON.toJSONString(xdcz0025DataReqDto));
            xdcz0025DataRespDto = xdcz0025Service.accSettle(xdcz0025DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, JSON.toJSONString(xdcz0025DataRespDto));
            // 封装xdcz0025DataResultDto中正确的返回码和返回信息
            xdcz0025DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0025DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, e.getMessage());
            // 封装xdcz0025DataResultDto中异常返回码和返回信息
            xdcz0025DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0025DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0025DataRespDto到xdcz0025DataResultDto中
        xdcz0025DataResultDto.setData(xdcz0025DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, JSON.toJSONString(xdcz0025DataRespDto));
        return xdcz0025DataResultDto;
    }
}
