/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CtrCvrgCont;
import cn.com.yusys.yusp.domain.IqpCvrgApp;
import cn.com.yusys.yusp.domain.IqpEntrustLoanApp;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.service.IqpCvrgAppService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCvrgAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-13 15:33:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpcvrgapp")
public class IqpCvrgAppResource {
    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;

	/**
     * 全表查询.
     * 
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpCvrgApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpCvrgApp> list = iqpCvrgAppService.selectAll(queryModel);
        return new ResultDto<List<IqpCvrgApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpCvrgApp>> index(QueryModel queryModel) {
        List<IqpCvrgApp> list = iqpCvrgAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpCvrgApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpCvrgApp> show(@PathVariable("pkId") String pkId) {
        IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpCvrgApp>(iqpCvrgApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpCvrgApp> create(@RequestBody IqpCvrgApp iqpCvrgApp) throws URISyntaxException {
        iqpCvrgAppService.insert(iqpCvrgApp);
        return new ResultDto<IqpCvrgApp>(iqpCvrgApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpCvrgApp iqpCvrgApp) throws URISyntaxException {
        int result = iqpCvrgAppService.update(iqpCvrgApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpCvrgAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpCvrgAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteIqpWriteOffDetailLogic
     * @函数描述:通过主键对保函申请进行逻辑删除
     * @参数与返回说明: iqpSerno
     * @算法描述: 对数据类型进行更新，并修改明细列表中的数据类型
     */
    @ApiOperation("保函合同申请删除操作")
    @PostMapping("/deleteIqpCvrgAppinfo")
    protected ResultDto<Map> deleteIqpCvrgAppinfo(@RequestBody Map params) {
        Map map = iqpCvrgAppService.deleteIqpCvrgAppinfo((String)params.get("serno"));
        return new ResultDto<Map>(map);
    }
    /**
     * @函数名称:toSignlist
     * @函数描述:待签定列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("待签定列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<IqpCvrgApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpCvrgApp> list = iqpCvrgAppService.toSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpCvrgApp>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:历史列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("历史列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<IqpCvrgApp>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpCvrgApp> list = iqpCvrgAppService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpCvrgApp>>(list);
    }

    /**
     * 保函申请新增操作
     * @param iqpCvrgApp
     * @return
     */
    @ApiOperation("保函合同申请保存新增操作")
    @PostMapping("/saveIqpCvrgAppInfo")
    public ResultDto<Map> saveIqpCvrgAppInfo(@RequestBody IqpCvrgApp iqpCvrgApp) {
        Map result = iqpCvrgAppService.saveIqpCvrgAppInfo(iqpCvrgApp);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:map
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("保函合同申请明细展示")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody  Map map) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        IqpCvrgApp temp = new IqpCvrgApp();
        IqpCvrgApp studyDemo = iqpCvrgAppService.selectBySerno((String)map.get("serno"));
        if (studyDemo != null) {
            resultDto.setCode(200);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @方法名称：sendctrcont
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhanyb
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("合同生成方法")
    @PostMapping("/sendctrcont")
    protected  ResultDto<Object> sendCtrcont(@RequestBody Map map) throws Exception {
        ResultDto<Object> result = new ResultDto<>();
        String serno = (String)map.get("serno");
        iqpCvrgAppService.handleBusinessDataAfterEnd(serno);
        return  result;
    }

    /**
     * 保函合同申请通用的保存方法
     * @param params
     * @return
     */
    @ApiOperation("修改保存")
    @PostMapping("/commsaveiqpcvrgappinfo")
    public ResultDto<Map> commSaveIqpCvrgAppInfo(@RequestBody Map params){
        Map rtnData = iqpCvrgAppService.commSaveIqpCvrgAppInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:iqpCvrgAppSubmitNoFlow
     * @函数描述:无流程提交后业务处理
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("无流程提交后业务处理")
    @PostMapping("/iqpCvrgAppSubmitNoFlow")
    protected ResultDto<Map> iqpCvrgAppSubmitNoFlow(@RequestBody IqpCvrgApp iqpCvrgApp) throws URISyntaxException {
        Map rtnData = iqpCvrgAppService.iqpCvrgAppSubmitNoFlow(iqpCvrgApp.getSerno());
        return new ResultDto<>(rtnData);
    }
}
