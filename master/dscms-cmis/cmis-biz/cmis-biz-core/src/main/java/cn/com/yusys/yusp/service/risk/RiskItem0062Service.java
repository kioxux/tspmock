package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpContExtBill;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.IqpContExtBillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0062Service
 * @类描述: 贷款期限与展期期限校验
 * @功能描述: 贷款期限与展期期限校验
 * @创建人: macm
 * @创建时间: 2021年7月28日23:32:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0062Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0062Service.class);

    @Autowired
    private IqpContExtBillService iqpContExtBillService;

    /**
     * @方法名称: riskItem0062
     * @方法描述: 贷款期限与展期期限校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: macm
     * @创建时间: 2021年7月28日23:32:20
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0062(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("贷款期限与展期期限校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        //  根据流水号获取展期申请中借据列表
        QueryModel queryMap = new QueryModel();
        queryMap.addCondition("iqpSerno",serno);
        List<IqpContExtBill> list = iqpContExtBillService.selectByModel(queryMap);
        if(CollectionUtils.isEmpty(list)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06201);
            return riskResultDto;
        }
        for(IqpContExtBill iqpContExtBill :list) {
            // 获取贷款到期日
            String loanEndDate = iqpContExtBill.getLoanEndDate();
            if(StringUtils.isBlank(loanEndDate)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06202);
                return riskResultDto;
            }
            // 获取展期后到期日
            String extEndDate = iqpContExtBill.getExtEndDate();
            if(StringUtils.isBlank(extEndDate)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06203);
                return riskResultDto;
            }
            // 获取贷款起始日期
            String loanStartDate = iqpContExtBill.getLoanStartDate();
            if(StringUtils.isBlank(loanStartDate)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06204);
                return riskResultDto;
            }
            // 1、原贷款期限小于等于1年（含），展期期限需小于等于原贷款期限；
            if(DateUtils.compare(DateUtils.addMonth(DateUtils.parseDate(loanStartDate,"yyyy-MM-dd"),12),
                    DateUtils.parseDate(loanEndDate,"yyyy-MM-dd")) >= 0) {
                if(DateUtils.getDaysByTwoDates(DateUtils.parseDate(loanStartDate,"yyyy-MM-dd"),DateUtils.parseDate(loanEndDate,"yyyy-MM-dd"))
                < DateUtils.getDaysByTwoDates(DateUtils.parseDate(loanEndDate,"yyyy-MM-dd"),DateUtils.parseDate(extEndDate,"yyyy-MM-dd"))) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06205);
                    return riskResultDto;
                }
            }
            // 2、原贷款期限在1年以上（不含1年）5年以下（含5年）的贷款，展期期限需小于等于原贷款期限的一半；
            if(DateUtils.compare(DateUtils.addMonth(DateUtils.parseDate(loanStartDate,"yyyy-MM-dd"),12),
                    DateUtils.parseDate(loanEndDate,"yyyy-MM-dd")) < 0
                    && DateUtils.compare(DateUtils.addMonth(DateUtils.parseDate(loanStartDate,"yyyy-MM-dd"),60),
                    DateUtils.parseDate(loanEndDate,"yyyy-MM-dd")) >= 0) {
                if(DateUtils.getDaysByTwoDates(DateUtils.parseDate(loanStartDate,"yyyy-MM-dd"),DateUtils.parseDate(loanEndDate,"yyyy-MM-dd"))
                        < DateUtils.getDaysByTwoDates(DateUtils.parseDate(loanEndDate,"yyyy-MM-dd"),DateUtils.parseDate(extEndDate,"yyyy-MM-dd")) * 2) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06206);
                    return riskResultDto;
                }
            }
            // 3、原贷款期限在5年（不含）以上的贷款，展期期限累计不得超过3年；
            if(DateUtils.compare(DateUtils.addMonth(DateUtils.parseDate(loanStartDate,"yyyy-MM-dd"),60),
                    DateUtils.parseDate(loanEndDate,"yyyy-MM-dd")) < 0) {
                if(DateUtils.getMonthsByTwoDates(DateUtils.parseDate(loanEndDate,"yyyy-MM-dd"),DateUtils.parseDate(extEndDate,"yyyy-MM-dd")) > 36) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06207);
                    return riskResultDto;
                }
            }
        }

        log.info("贷款期限与展期期限校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
