/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.service.LmtAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.MajorGrade;
import cn.com.yusys.yusp.service.MajorGradeService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: MajorGradeResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 20:06:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/majorgrade")
public class MajorGradeResource {
    @Autowired
    private MajorGradeService majorGradeService;

    @Autowired
    private LmtAppService lmtAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<MajorGrade>> query() {
        QueryModel queryModel = new QueryModel();
        List<MajorGrade> list = majorGradeService.selectAll(queryModel);
        return new ResultDto<List<MajorGrade>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<MajorGrade>> index(QueryModel queryModel) {
        List<MajorGrade> list = majorGradeService.selectByModel(queryModel);
        return new ResultDto<List<MajorGrade>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<MajorGrade> show(@PathVariable("pkId") String pkId) {
        MajorGrade majorGrade = majorGradeService.selectByPrimaryKey(pkId);
        return new ResultDto<MajorGrade>(majorGrade);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<MajorGrade> create(@RequestBody MajorGrade majorGrade) throws URISyntaxException {
        majorGradeService.insert(majorGrade);
        return new ResultDto<MajorGrade>(majorGrade);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody MajorGrade majorGrade) throws URISyntaxException {
        int result = majorGradeService.update(majorGrade);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = majorGradeService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = majorGradeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:checkHasPic
     * @函数描述:查询是否存在授信分项
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkHasPic")
    protected HashMap<String,String> checkHasPic(@RequestBody MajorGrade majorGrade) throws URISyntaxException {
        String serno = majorGrade.getLmtSerno();
        int count=0;
        HashMap<String,String> result = new HashMap<String,String>();
        if(!"".equals(serno)){//授信编号不为空
                //根据申请流水号查询是否存在多笔
                int sumFx = majorGradeService.getSumFromLmtAppDetails(serno);
                count = sumFx;
            if(count>0){
                result.put("flag", "00");
                result.put("jsonStr", "已经录入授信分项信息");
            }
        }
        return result;
    }


    /**
     * @函数名称:save
     * @函数描述:保存方法发送接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveMajorGrade")
    protected ResultDto<Integer> save(@RequestBody MajorGrade majorGrade) throws URISyntaxException {
            String lmtSerno = majorGrade.getLmtSerno();
            LmtApp lmtApp = lmtAppService.selectBySerno(majorGrade.getLmtSerno());
            majorGrade.setCusId(lmtApp.getCusId());
            majorGrade.setCusName(lmtApp.getCusName());
            majorGrade.setOprType(CmisCommonConstants.OP_TYPE_01);
            MajorGrade majorGrade1 = majorGradeService.selectByLmtSerno(lmtSerno);
            if(majorGrade1!=null){
                majorGradeService.update(majorGrade);
            }else{
                majorGradeService.insert(majorGrade);
            }
            int result = 0;
            if("00".equals(majorGrade.getLoanPurp())||"01".equals(majorGrade.getLoanPurp())){
                result = majorGradeService.sendIrs19(majorGrade);
            }
            return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:show
     * @函数描述:进入页面查询数据返回前台
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<MajorGrade> selectBySerno(@RequestBody String lmtSerno) {
        MajorGrade majorGrade = majorGradeService.selectByLmtSerno(lmtSerno);
        return new ResultDto<MajorGrade>(majorGrade);
    }

}
