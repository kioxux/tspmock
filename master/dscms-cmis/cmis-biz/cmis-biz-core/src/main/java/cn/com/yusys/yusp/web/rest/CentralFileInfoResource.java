/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.vo.CentralFileInfoOptVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CentralFileInfo;
import cn.com.yusys.yusp.service.CentralFileInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralFileInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-08 23:33:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/centralfileinfo")
public class CentralFileInfoResource {
    @Autowired
    private CentralFileInfoService centralFileInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CentralFileInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CentralFileInfo> list = centralFileInfoService.selectAll(queryModel);
        return new ResultDto<List<CentralFileInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CentralFileInfo>> index(QueryModel queryModel) {
        List<CentralFileInfo> list = centralFileInfoService.selectByModel(queryModel);
        return new ResultDto<List<CentralFileInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CentralFileInfo>> query(@RequestBody QueryModel queryModel) {
        List<CentralFileInfo> list = centralFileInfoService.selectByModel(queryModel);
        return new ResultDto<List<CentralFileInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{fileNo}")
    protected ResultDto<CentralFileInfo> show(@PathVariable("fileNo") String fileNo) {
        CentralFileInfo centralFileInfo = centralFileInfoService.selectByPrimaryKey(fileNo);
        return new ResultDto<CentralFileInfo>(centralFileInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CentralFileInfo> create(@RequestBody CentralFileInfo centralFileInfo) throws URISyntaxException {
        centralFileInfoService.insert(centralFileInfo);
        return new ResultDto<CentralFileInfo>(centralFileInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CentralFileInfo centralFileInfo) throws URISyntaxException {
        int result = centralFileInfoService.update(centralFileInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{fileNo}")
    protected ResultDto<Integer> delete(@PathVariable("fileNo") String fileNo) {
        int result = centralFileInfoService.deleteByPrimaryKey(fileNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = centralFileInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 获取可用的库位号
     * @return
     */
    @PostMapping("/getTempLocationNo")
    protected ResultDto<Integer> getTempLocationNo(){
        Integer tempLocationNo = centralFileInfoService.queryTempLocationNo();
        return new ResultDto<>(tempLocationNo);
    }

    @PostMapping("/batchUpdateSelective")
    protected ResultDto<Integer> batchUpdateSelective(@RequestBody CentralFileInfoOptVo centralFileInfoOptVo){
        int result = centralFileInfoService.batchUpdateSelective(centralFileInfoOptVo);
        return new ResultDto<>(result);
    }
    @PostMapping("/selectByModel")
    protected ResultDto<List<CentralFileInfo>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<CentralFileInfo>>(centralFileInfoService.selectByModel(model));
    }
}
