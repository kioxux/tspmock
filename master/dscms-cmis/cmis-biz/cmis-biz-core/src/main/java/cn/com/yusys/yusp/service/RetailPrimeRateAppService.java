/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.RetailPrimeRateAppr;
import cn.com.yusys.yusp.dto.CfgRetailPrimeRateDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.repository.mapper.RetailPrimeRateApprMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RetailPrimeRateApp;
import cn.com.yusys.yusp.repository.mapper.RetailPrimeRateAppMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RetailPrimeRateAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:18:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RetailPrimeRateAppService {
    private Logger log = LoggerFactory.getLogger(RetailPrimeRateAppService.class);

    @Autowired
    private RetailPrimeRateAppMapper retailPrimeRateAppMapper;

    @Autowired
    private RetailPrimeRateApprMapper retailPrimeRateApprMapper;

    @Autowired
    private RetailPrimeRateApprService retailPrimeRateApprService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RetailPrimeRateApp selectByPrimaryKey(String serno) {
        return retailPrimeRateAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据流水号查询查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RetailPrimeRateApp selectByIqpSerno(String iqpserno) {
        return retailPrimeRateAppMapper.selectByIqpSerno(iqpserno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RetailPrimeRateApp> selectAll(QueryModel model) {
        List<RetailPrimeRateApp> records = (List<RetailPrimeRateApp>) retailPrimeRateAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RetailPrimeRateApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RetailPrimeRateApp> list = retailPrimeRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RetailPrimeRateApp record) {
        record.setApproveStatus("000");
        return retailPrimeRateAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RetailPrimeRateApp record) {
        return retailPrimeRateAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RetailPrimeRateApp record) {
        return retailPrimeRateAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: update
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int repair(RetailPrimeRateAppr RetailPrimeRateAppr) {
        int count = 0;
        BigDecimal reply_Rate = new BigDecimal(0);
        String serno = RetailPrimeRateAppr.getSerno();
        if(RetailPrimeRateAppr.getReplyRate() !=null){
            reply_Rate = new BigDecimal(RetailPrimeRateAppr.getReplyRate().toString());
        }
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        List<RetailPrimeRateAppr> retailPrimeRateApprs = retailPrimeRateApprMapper.selectByModel(queryModel);
        if(retailPrimeRateApprs.size()>0){
            RetailPrimeRateAppr retailPrimeRateAppr = retailPrimeRateApprs.get(0);
            retailPrimeRateAppr.setReplyRate(reply_Rate);
            count =  retailPrimeRateApprMapper.updateByPrimaryKeySelective(retailPrimeRateAppr);
        }else{
            RetailPrimeRateApp retailPrimeRateApp = retailPrimeRateAppMapper.selectByPrimaryKey(serno);
            RetailPrimeRateAppr retailPrimeRateAppr = new  RetailPrimeRateAppr();
            retailPrimeRateAppr.setPkId(UUID.randomUUID().toString());
            retailPrimeRateAppr.setSerno(serno);
            retailPrimeRateAppr.setOfferRate(retailPrimeRateApp.getOfferRate());
            retailPrimeRateAppr.setAppRate(retailPrimeRateApp.getAppRate());
            retailPrimeRateAppr.setReplyRate(reply_Rate);
            count = retailPrimeRateApprMapper.insertSelective(retailPrimeRateAppr);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RetailPrimeRateApp record) {
        return retailPrimeRateAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        RetailPrimeRateApp RetailPrimeRateApp = retailPrimeRateAppMapper.selectByPrimaryKey(serno);
        //判断当前申请状态是否为退回，修改为自行退出
        if (RetailPrimeRateApp != null){
            if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(RetailPrimeRateApp.getApproveStatus())){
                //流程删除 修改为自行退出
                log.info("流程删除==》bizId：",serno);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(serno);
            }
        }
        return retailPrimeRateAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: delete
     * @方法描述: 根据申请流水号iqpSerno删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteiqpserno(String iqpSerno) {
        return retailPrimeRateAppMapper.deleteiqpserno(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return retailPrimeRateAppMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: updateRetail
     * @方法描述: 零售业务申请关联优惠利率 shenli
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateRetail(RetailPrimeRateApp record) {
        RetailPrimeRateApp retailPrimeRateApp1 = retailPrimeRateAppMapper.selectByIqpSerno(record.getIqpSerno());
        if(retailPrimeRateApp1 != null){
            retailPrimeRateApp1.setIqpSerno("");
            retailPrimeRateAppMapper.updateByPrimaryKey(retailPrimeRateApp1);
        }
        RetailPrimeRateApp retailPrimeRateApp2 = retailPrimeRateAppMapper.selectByPrimaryKey(record.getSerno());
        retailPrimeRateApp2.setIqpSerno(record.getIqpSerno());
        return retailPrimeRateAppMapper.updateByPrimaryKey(retailPrimeRateApp2);

    }

    /**
     * @param instanceInfo
     * @return cn.com.yusys.yusp.domain.RetailPrimeRateApp
     * @author 王玉坤
     * @date 2021/9/10 22:49
     * @version 1.0.0
     * @desc  更新零售优惠利率流程参数
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<String> updateflowparam(ResultInstanceDto instanceInfo) {
        log.info("优惠利率权限判断信息开始,申请流水号【{}】", instanceInfo.getBizId());
        String msg = "";
        try {
            if ("193_7".equals(instanceInfo.getNodeId()) || "193_8".equals(instanceInfo.getNodeId())) {
                log.info("根据流水号【{}】, 查询最新零售优惠利率申请信息开始", instanceInfo.getBizId());
                RetailPrimeRateApp retailPrimeRateApp = this.selectByPrimaryKey(instanceInfo.getBizId());
                log.info("根据流水号【{}】, 查询最新零售优惠利率申请信息结束", instanceInfo.getBizId());
                // 查询最新一条零售优惠利率审批信息
                log.info("根据流水号【{}】, 查询最新零售优惠利率审批信息开始", retailPrimeRateApp.getSerno());
                RetailPrimeRateAppr retailPrimeRateAppr = retailPrimeRateApprService.selectBySernoOrderByDate(retailPrimeRateApp.getSerno());
                log.info("根据流水号【{}】, 查询最新零售优惠利率审批信息结束,查询结果【{}】", retailPrimeRateApp.getSerno(), Objects.isNull(retailPrimeRateAppr));
                if (Objects.isNull(retailPrimeRateAppr)) {
                    log.info("根据流水号【{}】,未查询到审批信息！", retailPrimeRateApp.getSerno());
                    throw BizException.error(null, null, "未查询到审批信息！");
                }

                // 更新审批意见、审批结论
                //            retailPrimeRateAppr.setApproveConclusion(instanceInfo.getComment().getUserComment());
                //            retailPrimeRateAppr.setApproveConclusion(instanceInfo.getComment().getCommentSign());
                //            retailPrimeRateApprService.update(retailPrimeRateAppr);

                // 根据审批利率计算审批权限
                // 根据产品代码、岗位查询优惠利率配置信息
                // 查询条件DTO
                CfgRetailPrimeRateDto cfgRetailPrimeRateDtoTemp = new CfgRetailPrimeRateDto();
                // 实际优惠利率审批对象
                CfgRetailPrimeRateDto cfgRetailPrimeRateDto = null;
                String duty = "";
                // 根据节点映射岗位信息
                if ("193_7".equals(instanceInfo.getNodeId())) {
                    duty = "KXB03";// 卡与消金部负责人
                } else if ("193_8".equals(instanceInfo.getNodeId())) {
                    duty = "ZHT09";// 零售业务分管行长
                }
                cfgRetailPrimeRateDtoTemp.setPrdId(retailPrimeRateApp.getPrdId());
                cfgRetailPrimeRateDtoTemp.setDutyCode(duty);
                cfgRetailPrimeRateDtoTemp.setOrgCode(retailPrimeRateApp.getInputBrId());
                log.info("根据流水号【{}】, 产品代码【{}】, 岗位【{}】, 机构【{}】查询优惠利率权限配置信息开始", retailPrimeRateApp.getSerno()
                        , retailPrimeRateApp.getPrdId(), duty, retailPrimeRateApp.getInputBrId());
                List<CfgRetailPrimeRateDto> cfgRetailPrimeRateDtoListResultDto = iCmisCfgClientService.selectRetailPrimerateByCondition(cfgRetailPrimeRateDtoTemp);
                if (CollectionUtils.nonEmpty(cfgRetailPrimeRateDtoListResultDto)) {
                    log.info("根据流水号【{}】, 产品代码【{}】, 岗位【{}】, 机构【{}】查询优惠利率权限配置信息结束", retailPrimeRateApp.getSerno()
                            , retailPrimeRateApp.getPrdId(), duty, retailPrimeRateApp.getInputBrId());
                    cfgRetailPrimeRateDto = cfgRetailPrimeRateDtoListResultDto.get(0);
                    log.info("根据流水号【{}】, 产品代码【{}】, 岗位【{}】, 机构【{}】查询优惠利率权限配置信息结束,审批利率bp点【{}】", retailPrimeRateApp.getSerno()
                            , retailPrimeRateApp.getPrdId(), duty, retailPrimeRateApp.getInputBrId(), cfgRetailPrimeRateDto.getRateMax());
                } else {
                    log.info("根据流水号【{}】, 产品代码【{}】, 岗位【{}】, 机构【{}】未查询优惠利率权限配置信息", retailPrimeRateApp.getSerno()
                            , retailPrimeRateApp.getPrdId(), duty, retailPrimeRateApp.getInputBrId());
                    throw BizException.error(null, null, "未查询到优惠利率配置信息！");
                }
                // 审批人审批利率临界值
                log.info("根据申请时报价利率【{}】、审批人审批bp点【{}】计算审批人最大审批利率开始！", retailPrimeRateApp.getOfferRate(), cfgRetailPrimeRateDto.getRateMax()
                );
                BigDecimal rateBorder = retailPrimeRateApp.getOfferRate().subtract(new BigDecimal(cfgRetailPrimeRateDto.getRateMax())
                        .divide(new BigDecimal("10000")));
                log.info("根据申请时报价利率【{}】、审批人审批bp点【{}】计算审批人最大审批利率【{}】！", retailPrimeRateApp.getOfferRate(), cfgRetailPrimeRateDto.getRateMax()
                        , rateBorder.stripTrailingZeros().toPlainString());
                // 判断当前申请利率是否在此权限内
                // 权限标识
                String powerFlag = "false";
                log.info("根据审批利率【{}】、临界利率【{}】计算权限开始", retailPrimeRateAppr.getReplyRate(), rateBorder.stripTrailingZeros().toPlainString());
                if (retailPrimeRateAppr.getReplyRate().compareTo(rateBorder) >= 0) {
                    // 审批利率在权限内，直接结束流程
                    powerFlag = "true";
                    msg = "申批利率在权限内，直接结束流程";
                } else {
                    msg = "申批利率不在权限内，即将提交至下一节点！";
                }

                // 更新流程参数
                log.info("更新流程参数【{}】开始", instanceInfo.getParam());
                instanceInfo.getParam().put("powerFlag", powerFlag);
                WFBizParamDto param = new WFBizParamDto();
                param.setBizId(instanceInfo.getBizId());
                param.setInstanceId(instanceInfo.getInstanceId());
                param.setParam(instanceInfo.getParam());
                workflowCoreClient.updateFlowParam(param);
                log.info("更新流程参数【{}】结束", instanceInfo.getParam());
                //将审批结论保存至批复表中
                Map params = new HashMap();
                params.put("serno",instanceInfo.getBizId());
                params.put("approvePost",instanceInfo.getNodeId());
                RetailPrimeRateAppr record = retailPrimeRateApprMapper.selectBySernoAndNode(params);
                if(record != null){
                    record.setApproveConclusion(instanceInfo.getComment().getCommentSign());
                    record.setApproveAdvice(instanceInfo.getComment().getUserComment());
                    retailPrimeRateApprMapper.updateByPrimaryKeySelective(record);
                }else{
                    RetailPrimeRateAppr record2 = retailPrimeRateApprMapper.selectBySernoOrderByDate(instanceInfo.getBizId());
                    record2.setPkId(UUID.randomUUID().toString());
                    record2.setApproveConclusion(instanceInfo.getComment().getCommentSign());
                    record2.setApproveAdvice(instanceInfo.getComment().getUserComment());
                    record2.setApprovePost(instanceInfo.getNodeId());
                    retailPrimeRateApprMapper.insertSelective(record2);
                }
                return new ResultDto<String>(powerFlag).message(msg);
            } else {
                log.info("该节点无需审批利率！");
            }
        } catch (BizException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        log.info("优惠利率权限判断信息结束,申请流水号【{}】", instanceInfo.getBizId());
        return new ResultDto<String>("");
    }

    public RetailPrimeRateApp addRetailApp(RetailPrimeRateApp retailPrimeRateApp) {
        String serno = retailPrimeRateApp.getSerno();
        retailPrimeRateApp.setApproveStatus("000");
        if(retailPrimeRateAppMapper.selectByPrimaryKey(serno) == null ){
            int result = retailPrimeRateAppMapper.insertSelective(retailPrimeRateApp);
            if(result !=1){
                throw BizException.error(null, "999999", "新增异常！");
            }
            //新增审批数据
            RetailPrimeRateAppr retailPrimeRateAppr = new RetailPrimeRateAppr();
            retailPrimeRateAppr.setPkId(UUID.randomUUID().toString());
            retailPrimeRateAppr.setSerno(serno);
            retailPrimeRateAppr.setOfferRate(retailPrimeRateApp.getOfferRate());
            retailPrimeRateAppr.setReplyRate(retailPrimeRateApp.getAppRate());
            retailPrimeRateAppr.setAppRate(retailPrimeRateApp.getAppRate());
            retailPrimeRateAppr.setApprovePost("193_7");
            retailPrimeRateApprService.insertSelective(retailPrimeRateAppr);
            if(result !=1){
                throw BizException.error(null, "999999", "新增异常！");
            }
        }else{
            int result = retailPrimeRateAppMapper.updateByPrimaryKey(retailPrimeRateApp);
            if(result !=1){
                throw BizException.error(null, "999999", "新增异常！");
            }
            RetailPrimeRateAppr retailPrimeRateAppr = retailPrimeRateApprService.selectBySernoOrderByDate(serno);
            retailPrimeRateAppr.setAppRate(retailPrimeRateApp.getAppRate());
            result = retailPrimeRateApprService.updateSelective(retailPrimeRateAppr);
            if(result !=1){
                throw BizException.error(null, "999999", "新增异常！");
            }
        }
        return retailPrimeRateApp;
    }



    public int CheckRetailPrimeRateApp (RetailPrimeRateApp retailPrimeRateApp) {
        String iqpSerno = retailPrimeRateApp.getIqpSerno() == null ?"" : retailPrimeRateApp.getIqpSerno();
        if(!"".equals(iqpSerno)){

            IqpLoanApp IqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
            String cusId = IqpLoanApp.getCusId();
            String prdId = IqpLoanApp.getPrdId();
            String approveStatus = IqpLoanApp.getApproveStatus();


            if(!cusId.equals(retailPrimeRateApp.getCusId())){
                throw BizException.error(null, "999999", "关联授信客户不一致！");
            }

            if(!prdId.equals(retailPrimeRateApp.getPrdId())){
                throw BizException.error(null, "999999", "关联授信客户不一致！");
            }


            if(!"000".equals(approveStatus) && !"997".equals(approveStatus)){
                throw BizException.error(null, "999999", "关联授信审批状态必须为待发起或通过！");
            }
        }

        return 0;
    }

}
