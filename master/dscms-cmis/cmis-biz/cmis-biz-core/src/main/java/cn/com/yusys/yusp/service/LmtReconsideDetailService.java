/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtReconsideDetail;
import cn.com.yusys.yusp.repository.mapper.LmtReconsideDetailMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReconsideDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: mashun
 * @创建时间: 2021-06-10 00:27:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReconsideDetailService {

    @Resource
    private LmtReconsideDetailMapper lmtReconsideDetailMapper;

    @Autowired
    private LmtAppService lmtAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtReconsideDetail selectByPrimaryKey(String pkId) {
        return lmtReconsideDetailMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtReconsideDetail> selectAll(QueryModel model) {
        List<LmtReconsideDetail> records = (List<LmtReconsideDetail>) lmtReconsideDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtReconsideDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReconsideDetail> list = lmtReconsideDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtReconsideDetail record) {
        return lmtReconsideDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtReconsideDetail record) {
        return lmtReconsideDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtReconsideDetail record) {
        return lmtReconsideDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtReconsideDetail record) {
        return lmtReconsideDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReconsideDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReconsideDetailMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号查询复议详情返回数据
     * @param lmtSerno
     * @return
     */
    public LmtReconsideDetail selectByLmtSerno(String lmtSerno) {
        return lmtReconsideDetailMapper.selectByLmtSerno(lmtSerno);
    }

    /**
     * 根据申请流水号查询分项数据
     * @param lmtSerno
     * @return
     */
    public List<Map<String,String>> queryDetailByLmtSerno(String lmtSerno) {
        List<Map<String,String>> list = new ArrayList<>();
        // 获取成员客户 复议信息
        List<LmtReconsideDetail> lmtReconsideDetails = lmtReconsideDetailMapper.queryDetailByLmtSerno(lmtSerno);
        for(LmtReconsideDetail lmtReconsideDetail : lmtReconsideDetails){
            HashMap<String , String> map = new HashMap();
            // 处理成员客户
            // 获取成员客户号
            LmtApp lmtApp = lmtAppService.selectBySerno(lmtReconsideDetail.getLmtSerno());
            if(lmtApp != null ){
                map.put("cusId",lmtApp.getCusId());
                map.put("cusName",lmtApp.getCusName());
            }
            map.put("pkId",lmtReconsideDetail.getPkId());
            map.put("lmtSerno",lmtReconsideDetail.getLmtSerno());
            map.put("grpRecoAppMemo",lmtReconsideDetail.getGrpRecoAppMemo());
            map.put("lastLmtCondition",lmtReconsideDetail.getLastLmtCondition());
            map.put("lmtRediContent",lmtReconsideDetail.getLmtRediContent());
            map.put("keepFinaReason",lmtReconsideDetail.getKeepFinaReason());
            map.put("riskGuardMeasu",lmtReconsideDetail.getRiskGuardMeasu());
            map.put("otherResn",lmtReconsideDetail.getOtherResn());
            map.put("inputId",lmtReconsideDetail.getInputId());
            map.put("inputBrId",lmtReconsideDetail.getInputBrId());
            map.put("inputDate",lmtReconsideDetail.getInputDate());
            list.add(map);
        }
        return list;
    }

    public int updateByLmtSerno(LmtReconsideDetail lmtReconsideDetail) {
        return lmtReconsideDetailMapper.updateByLmtSerno(lmtReconsideDetail);
    }
}
