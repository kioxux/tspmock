/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaAdminUser
 * @类描述: area_admin_user数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-15 21:36:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "area_admin_user")
public class AreaAdminUser extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 用户号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "USER_NO")
	private String userNo;
	
	/** 调查模式 **/
	@Column(name = "SURVEY_MODE", unique = false, nullable = true, length = 5)
	private String surveyMode;
	
	/** 是否派遣员工 **/
	@Column(name = "IS_DISPATCH_EMPLOYEE", unique = false, nullable = true, length = 5)
	private String isDispatchEmployee;
	
	/** 是否新员工 **/
	@Column(name = "IS_NEW_EMPLOYEE", unique = false, nullable = true, length = 5)
	private String isNewEmployee;
	
	/** 直营团队类型 **/
	@Column(name = "TEAM_TYPE", unique = false, nullable = true, length = 5)
	private String teamType;
	
	/** 小微线上产品权限 **/
	@Column(name = "ONLINE_PRD_MANAGER", unique = false, nullable = true, length = 20)
	private String onlinePrdManager;
	
	/** 大额信用权限 **/
	@Column(name = "IS_LARGE_CREDIT", unique = false, nullable = true, length = 5)
	private String isLargeCredit;
	
	/** 机构代码 **/
	@Column(name = "ORG_CODE", unique = false, nullable = true, length = 100)
	private String orgCode;
	
	/** 机构名称 **/
	@Column(name = "ORG_NAME", unique = false, nullable = true, length = 100)
	private String orgName;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param userNo
	 */
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	
    /**
     * @return userNo
     */
	public String getUserNo() {
		return this.userNo;
	}
	
	/**
	 * @param surveyMode
	 */
	public void setSurveyMode(String surveyMode) {
		this.surveyMode = surveyMode;
	}
	
    /**
     * @return surveyMode
     */
	public String getSurveyMode() {
		return this.surveyMode;
	}
	
	/**
	 * @param isDispatchEmployee
	 */
	public void setIsDispatchEmployee(String isDispatchEmployee) {
		this.isDispatchEmployee = isDispatchEmployee;
	}
	
    /**
     * @return isDispatchEmployee
     */
	public String getIsDispatchEmployee() {
		return this.isDispatchEmployee;
	}
	
	/**
	 * @param isNewEmployee
	 */
	public void setIsNewEmployee(String isNewEmployee) {
		this.isNewEmployee = isNewEmployee;
	}
	
    /**
     * @return isNewEmployee
     */
	public String getIsNewEmployee() {
		return this.isNewEmployee;
	}
	
	/**
	 * @param teamType
	 */
	public void setTeamType(String teamType) {
		this.teamType = teamType;
	}
	
    /**
     * @return teamType
     */
	public String getTeamType() {
		return this.teamType;
	}
	
	/**
	 * @param onlinePrdManager
	 */
	public void setOnlinePrdManager(String onlinePrdManager) {
		this.onlinePrdManager = onlinePrdManager;
	}
	
    /**
     * @return onlinePrdManager
     */
	public String getOnlinePrdManager() {
		return this.onlinePrdManager;
	}
	
	/**
	 * @param isLargeCredit
	 */
	public void setIsLargeCredit(String isLargeCredit) {
		this.isLargeCredit = isLargeCredit;
	}
	
    /**
     * @return isLargeCredit
     */
	public String getIsLargeCredit() {
		return this.isLargeCredit;
	}
	
	/**
	 * @param orgCode
	 */
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	
    /**
     * @return orgCode
     */
	public String getOrgCode() {
		return this.orgCode;
	}
	
	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
    /**
     * @return orgName
     */
	public String getOrgName() {
		return this.orgName;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}