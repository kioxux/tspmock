/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgModelGroupDetail;
import cn.com.yusys.yusp.dto.CfgModelConfigItem;
import cn.com.yusys.yusp.repository.mapper.CfgModelGroupDetailMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgModelGroupDetailService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 15430
 * @创建时间: 2020-11-23 10:33:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgModelGroupDetailService {

    @Autowired
    private CfgModelGroupDetailMapper cfgModelGroupDetailMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgModelGroupDetail selectByPrimaryKey(String pkId) {
        return cfgModelGroupDetailMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgModelGroupDetail> selectAll(QueryModel model) {
        List<CfgModelGroupDetail> records = (List<CfgModelGroupDetail>) cfgModelGroupDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgModelGroupDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgModelGroupDetail> list = cfgModelGroupDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgModelGroupDetail record) {
        return cfgModelGroupDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgModelGroupDetail record) {
        return cfgModelGroupDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgModelGroupDetail record) {
        return cfgModelGroupDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgModelGroupDetail record) {
        return cfgModelGroupDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgModelGroupDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgModelGroupDetailMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByCfgModelGroupNo
     * @方法描述: 根据模板组编号批量删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByCfgModelGroupNo(String groupNo) {
        return cfgModelGroupDetailMapper.deleteByCfgModelGroupNo(groupNo);
    }

    /**
     * @方法名称: selectAllSubConfig
     * @方法描述: 获取所有子页面和子模板模板配置
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgModelConfigItem> selectAllSubConfig() {
        return cfgModelGroupDetailMapper.selectAllSubConfig();
    }
}
