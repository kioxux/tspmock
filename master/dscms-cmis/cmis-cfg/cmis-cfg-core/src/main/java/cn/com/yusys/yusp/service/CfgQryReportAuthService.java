/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgQryReportAuth;
import cn.com.yusys.yusp.repository.mapper.CfgQryReportAuthMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgQryReportAuthService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-02-25 21:16:53
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgQryReportAuthService {

    private static final Logger log = LoggerFactory.getLogger(CfgQryReportAuthService.class);
    @Autowired
    private CfgQryReportAuthMapper cfgQryReportAuthMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgQryReportAuth selectByPrimaryKey(String pkId) {
        return cfgQryReportAuthMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgQryReportAuth> selectAll(QueryModel model) {
        List<CfgQryReportAuth> records = (List<CfgQryReportAuth>) cfgQryReportAuthMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgQryReportAuth> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgQryReportAuth> list = cfgQryReportAuthMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgQryReportAuth record) {
        return cfgQryReportAuthMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgQryReportAuth record) {
        return cfgQryReportAuthMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgQryReportAuth record) {
        return cfgQryReportAuthMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgQryReportAuth record) {
        return cfgQryReportAuthMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgQryReportAuthMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgQryReportAuthMapper.deleteByIds(ids);
    }

    /**
     * 根据角色编号查询权限下的报表数据
     *
     * @param map
     * @return
     * @author xuchao
     */
    public List<CfgQryReportAuth> queryReportListByRoleCode(Map map) {
        return cfgQryReportAuthMapper.queryReportListByRoleCode(map);
    }

    /**
     * 根据传入的传入的权限数据与原来的权限数据进行对比，对选中角色权限维护
     *
     * @param map
     * @return
     */
    public int updateReportListByRoleCode(Map map) {
        int ret = 0;
        try {
            //原角色权限下列表
            List<CfgQryReportAuth> list = cfgQryReportAuthMapper.queryReportListByRoleCode(map);
            List<String> oldList = new ArrayList<String>();
            for (CfgQryReportAuth cfgQryReportAuth : list) {
                oldList.add(cfgQryReportAuth.getQryCode());
            }
            //新角色权限下列表
            List<LinkedHashMap> rowData = (List<LinkedHashMap>) map.get("rowData");
            List<String> newList = new ArrayList<String>();
            for (LinkedHashMap linkedHashMap : rowData) {
                if (linkedHashMap.containsKey("qryCode")) {
                    newList.add((String) linkedHashMap.get("qryCode"));
                }
            }
            //需要新增的数据
            List middleList = new ArrayList<String>();
            middleList.addAll(newList);
            //取差集
            middleList.removeAll(oldList);
            //批量新增
            //数据组装
            if (middleList.size() > 0) {
                List<CfgQryReportAuth> addList = new ArrayList<CfgQryReportAuth>();
                String roleCode = (String) map.get("roleCode");
                String inputId = (String) map.get("inputId");
                String inputBrId = (String) map.get("inputBrId");
                String inputDate = (String) map.get("inputDate");
                for (int i = 0; i < middleList.size(); i++) {
                    CfgQryReportAuth cfgQryReportAuth = new CfgQryReportAuth();
                    cfgQryReportAuth.setPkId(UUID.randomUUID().toString().trim().replace("-", ""));
                    cfgQryReportAuth.setQryCode((String) middleList.get(i));
                    cfgQryReportAuth.setRoleCode(roleCode);
                    cfgQryReportAuth.setInputId(inputId);
                    cfgQryReportAuth.setInputBrId(inputBrId);
                    cfgQryReportAuth.setInputDate(inputDate);
                    addList.add(cfgQryReportAuth);
                }
                ret = cfgQryReportAuthMapper.batchInsertAuthInfo(addList);
            }
            //需要删除的数据
            oldList.removeAll(newList);
            //批量删除
            if (oldList.size() > 0) {
                Map parameter = new HashMap();
                parameter.put("roleCode", map.get("roleCode"));
                parameter.put("qryCode", org.apache.commons.lang.StringUtils.join(oldList.toArray(), ","));
                ret = cfgQryReportAuthMapper.batchDeleteAuthInfo(parameter);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ret;
    }

    /**
     * 根据报表编号联动删除权限数据
     *
     * @param qryCode 灵活查询报表编号
     * @return
     */
    public int deleteInfoByQryCode(String qryCode) {
        return cfgQryReportAuthMapper.deleteInfoByRoleCode(qryCode);
    }
}
