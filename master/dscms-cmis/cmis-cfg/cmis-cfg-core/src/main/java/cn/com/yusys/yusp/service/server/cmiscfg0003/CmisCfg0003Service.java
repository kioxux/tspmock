package cn.com.yusys.yusp.service.server.cmiscfg0003;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.cmiscfg0003.req.CmisCfg0003ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0003.resp.CmisCfg0003RespDto;
import cn.com.yusys.yusp.service.CfgBankInfoService;
import cn.com.yusys.yusp.service.CfgBankNoRelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg模块
 * @类名称: CmisCfg0003Service
 * @类描述: #对内服务类
 * @功能描述: 根据行号获取上级行号
 * @创建时间: 2021-07-14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisCfg0003Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisCfg0003Service.class);

    @Autowired
    private CfgBankInfoService cfgBankInfoService;

    @Autowired
    private CfgBankNoRelService cfgBankNoRelService;

    @Transactional
    public CmisCfg0003RespDto execute(CmisCfg0003ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0003.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0003.value);
        CmisCfg0003RespDto respDto = new CmisCfg0003RespDto();
        try {

//            String superBankNo = cfgBankInfoService.querySuperBankNoByBankNo(reqDto.getBankNo());
            String superBankNo = cfgBankNoRelService.querySuperBankNoByBankNo(reqDto.getBankNo());

            respDto.setSuperBankNo(superBankNo);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsCfgEnum.TRADE_CODE_CMISCFG0003.value, e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0003.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0003.value);
        return respDto;
    }

}


