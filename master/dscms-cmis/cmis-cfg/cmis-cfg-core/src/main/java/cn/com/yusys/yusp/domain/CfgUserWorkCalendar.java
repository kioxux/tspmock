/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgUserWorkCalendar
 * @类描述: cfg_user_work_calendar数据实体类
 * @功能描述:
 * @创建人: Admin
 * @创建时间: 2021-03-31 16:45:04
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_user_work_calendar")
public class CfgUserWorkCalendar extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 用户登录代码
     **/
    @Column(name = "LOGIN_CODE", unique = false, nullable = false, length = 100)
    private String loginCode;

    /**
     * 登记类型
     **/
    @Column(name = "REG_TYPE", unique = false, nullable = true, length = 10)
    private String regType;

    /**
     * 日期
     **/
    @Column(name = "CALENDAR_DATE", unique = false, nullable = false, length = 20)
    private String calendarDate;

    /**
     * 标题
     **/
    @Column(name = "TITLE", unique = false, nullable = false, length = 100)
    private String title;

    /**
     * 内容
     **/
    @Column(name = "CONTENT", unique = false, nullable = false, length = 1000)
    private String content;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 更新日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
    private String oprType;

    public CfgUserWorkCalendar() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return loginCode
     */
    public String getLoginCode() {
        return this.loginCode;
    }

    /**
     * @param loginCode
     */
    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    /**
     * @return regType
     */
    public String getRegType() {
        return this.regType;
    }

    /**
     * @param regType
     */
    public void setRegType(String regType) {
        this.regType = regType;
    }

    /**
     * @return calendarDate
     */
    public String getCalendarDate() {
        return this.calendarDate;
    }

    /**
     * @param calendarDate
     */
    public void setCalendarDate(String calendarDate) {
        this.calendarDate = calendarDate;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return content
     */
    public String getContent() {
        return this.content;
    }

    /**
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}