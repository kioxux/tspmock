/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgApproveAuthority;
import cn.com.yusys.yusp.dto.CfgApproveAuthorityDto;
import cn.com.yusys.yusp.repository.mapper.CfgApproveAuthorityMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgApproveAuthorityService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-05-24 10:32:53
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgApproveAuthorityService {

    @Autowired
    private CfgApproveAuthorityMapper cfgApproveAuthorityMapper;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgApproveAuthority selectByPrimaryKey(String pkId) {
        return cfgApproveAuthorityMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgApproveAuthority> selectAll(QueryModel model) {
        List<CfgApproveAuthority> records = (List<CfgApproveAuthority>) cfgApproveAuthorityMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgApproveAuthority> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgApproveAuthority> list = cfgApproveAuthorityMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgApproveAuthority record) {
        return cfgApproveAuthorityMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgApproveAuthority record) {
        return cfgApproveAuthorityMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgApproveAuthority record) {
        return cfgApproveAuthorityMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgApproveAuthority record) {
        return cfgApproveAuthorityMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgApproveAuthorityMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgApproveAuthorityMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryDutyByCusDebtLevel
     * @方法描述: 获取内评审批岗位
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgApproveAuthorityDto queryDutyByCusDebtLevel(CfgApproveAuthorityDto cfgApproveAuthorityDto) {
        return cfgApproveAuthorityMapper.queryDutyByCusDebtLevel(cfgApproveAuthorityDto);
    }

    /**
     * @方法名称: queryRateByCusDebtLevel
     * @方法描述: 获取审批人的资本占用率
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgApproveAuthorityDto queryRateByCusDebtLevel(CfgApproveAuthorityDto cfgApproveAuthorityDto) {
        return cfgApproveAuthorityMapper.queryRateByCusDebtLevel(cfgApproveAuthorityDto);
    }

}
