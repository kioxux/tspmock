package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * @className CfgPrdFileTplRelDto
 * @Date 2021/5/9 : 20:09
 */

public class CfgPrdFileTplRelDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String pkId;
    private String fileTplNo;

    private String prdId;

    private String fileTplClass;

    private String oprType;

    private String fileName;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getFileTplNo() {
        return fileTplNo;
    }

    public void setFileTplNo(String fileTplNo) {
        this.fileTplNo = fileTplNo;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getFileTplClass() {
        return fileTplClass;
    }

    public void setFileTplClass(String fileTplClass) {
        this.fileTplClass = fileTplClass;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
