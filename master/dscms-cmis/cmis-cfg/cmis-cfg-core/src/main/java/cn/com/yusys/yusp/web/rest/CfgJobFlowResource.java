/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgJobFlow;
import cn.com.yusys.yusp.service.CfgJobFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgJobFlowResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: qhy
 * @创建时间: 2021-05-06 21:54:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgjobflow")
public class CfgJobFlowResource {
    @Autowired
    private CfgJobFlowService cfgJobFlowService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgJobFlow>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgJobFlow> list = cfgJobFlowService.selectAll(queryModel);
        return new ResultDto<List<CfgJobFlow>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgJobFlow>> index(QueryModel queryModel) {
        List<CfgJobFlow> list = cfgJobFlowService.selectByModel(queryModel);
        return new ResultDto<List<CfgJobFlow>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{jobFlowId}")
    protected ResultDto<CfgJobFlow> show(@PathVariable("jobFlowId") String jobFlowId) {
        CfgJobFlow cfgJobFlow = cfgJobFlowService.selectByPrimaryKey(jobFlowId);
        return new ResultDto<CfgJobFlow>(cfgJobFlow);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgJobFlow> create(@RequestBody CfgJobFlow cfgJobFlow) throws URISyntaxException {
        cfgJobFlowService.insert(cfgJobFlow);
        return new ResultDto<CfgJobFlow>(cfgJobFlow);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgJobFlow cfgJobFlow) throws URISyntaxException {
        int result = cfgJobFlowService.update(cfgJobFlow);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{jobFlowId}")
    protected ResultDto<Integer> delete(@PathVariable("jobFlowId") String jobFlowId) {
        int result = cfgJobFlowService.deleteByPrimaryKey(jobFlowId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgJobFlowService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
