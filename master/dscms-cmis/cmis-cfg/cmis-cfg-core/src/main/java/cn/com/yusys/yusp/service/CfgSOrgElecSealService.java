/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgSOrgElecSeal;
import cn.com.yusys.yusp.repository.mapper.CfgSOrgElecSealMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSOrgElecSealService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-06-10 22:30:16
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgSOrgElecSealService {

    @Autowired
    private CfgSOrgElecSealMapper cfgSOrgElecSealMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgSOrgElecSeal selectByPrimaryKey(String orgNo) {
        return cfgSOrgElecSealMapper.selectByPrimaryKey(orgNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgSOrgElecSeal> selectAll(QueryModel model) {
        List<CfgSOrgElecSeal> records = (List<CfgSOrgElecSeal>) cfgSOrgElecSealMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据机构号查询获取对应的章编码
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgSOrgElecSeal selectByOrgNo(String orgNo) {
        return cfgSOrgElecSealMapper.selectByPrimaryKey(orgNo);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgSOrgElecSeal> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgSOrgElecSeal> list = cfgSOrgElecSealMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgSOrgElecSeal record) {
        return cfgSOrgElecSealMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgSOrgElecSeal record) {
        return cfgSOrgElecSealMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgSOrgElecSeal record) {
        return cfgSOrgElecSealMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgSOrgElecSeal record) {
        return cfgSOrgElecSealMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String orgNo) {
        return cfgSOrgElecSealMapper.deleteByPrimaryKey(orgNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgSOrgElecSealMapper.deleteByIds(ids);
    }
}
