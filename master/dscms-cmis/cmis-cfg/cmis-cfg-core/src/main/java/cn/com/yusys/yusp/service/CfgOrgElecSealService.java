/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgOrgElecSeal;
import cn.com.yusys.yusp.repository.mapper.CfgOrgElecSealMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrgElecSealService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-07-12 08:57:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgOrgElecSealService {

    @Autowired
    private CfgOrgElecSealMapper cfgOrgElecSealMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgOrgElecSeal selectByPrimaryKey(String orgCode) {
        return cfgOrgElecSealMapper.selectByPrimaryKey(orgCode);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgOrgElecSeal> selectAll(QueryModel model) {
        List<CfgOrgElecSeal> records = (List<CfgOrgElecSeal>) cfgOrgElecSealMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgOrgElecSeal> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgOrgElecSeal> list = cfgOrgElecSealMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgOrgElecSeal record) {
        return cfgOrgElecSealMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgOrgElecSeal record) {
        return cfgOrgElecSealMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgOrgElecSeal record) {
        return cfgOrgElecSealMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgOrgElecSeal record) {
        return cfgOrgElecSealMapper.updateByPrimaryKeySelective(record);
    }


    /**
     * @方法名称: selectFromCfgByOrgNo
     * @方法描述: 根据机构号查询获取对应的章编码
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgOrgElecSeal selectFromCfgByOrgNo(String orgNo) {
        return cfgOrgElecSealMapper.selectByPrimaryKey(orgNo);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String orgCode) {
        return cfgOrgElecSealMapper.deleteByPrimaryKey(orgCode);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgOrgElecSealMapper.deleteByIds(ids);
    }
}
