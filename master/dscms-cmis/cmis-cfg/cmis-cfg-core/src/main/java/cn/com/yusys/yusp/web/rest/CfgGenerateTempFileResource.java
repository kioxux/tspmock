/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgGenerateTempFile;
import cn.com.yusys.yusp.service.CfgGenerateTempFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgGenerateTempFileResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-06-10 14:17:36
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfggeneratetempfile")
public class CfgGenerateTempFileResource {
    @Autowired
    private CfgGenerateTempFileService cfgGenerateTempFileService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgGenerateTempFile>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgGenerateTempFile> list = cfgGenerateTempFileService.selectAll(queryModel);
        return new ResultDto<List<CfgGenerateTempFile>>(list);
    }

    /**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/queryCfgFile")
    protected ResultDto<List<CfgGenerateTempFile>> queryCfgFile(@RequestBody QueryModel queryModel) {
        List<CfgGenerateTempFile> list = cfgGenerateTempFileService.selectAll(queryModel);
        return new ResultDto<List<CfgGenerateTempFile>>(list);
    }

    /**
     * 查询临时存储文件配置表(提供给frpt服务)
     *
     * @return
     */
    @PostMapping("/query/queryCfgFilebyPkid")
    protected ResultDto<List<CfgGenerateTempFile>> queryCfgFilebyPkid(@RequestBody String pkid) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("pkId", pkid);
        List<CfgGenerateTempFile> list = cfgGenerateTempFileService.selectAll(queryModel);
        return new ResultDto<List<CfgGenerateTempFile>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgGenerateTempFile>> index(QueryModel queryModel) {
        List<CfgGenerateTempFile> list = cfgGenerateTempFileService.selectByModel(queryModel);
        return new ResultDto<List<CfgGenerateTempFile>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgGenerateTempFile> show(@PathVariable("pkId") String pkId) {
        CfgGenerateTempFile cfgGenerateTempFile = cfgGenerateTempFileService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgGenerateTempFile>(cfgGenerateTempFile);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgGenerateTempFile> create(@RequestBody CfgGenerateTempFile cfgGenerateTempFile) throws URISyntaxException {
        cfgGenerateTempFileService.insert(cfgGenerateTempFile);
        return new ResultDto<CfgGenerateTempFile>(cfgGenerateTempFile);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgGenerateTempFile cfgGenerateTempFile) throws URISyntaxException {
        int result = cfgGenerateTempFileService.update(cfgGenerateTempFile);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgGenerateTempFileService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgGenerateTempFileService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
