package cn.com.yusys.yusp.service.server.xdqt0007;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CfgPrdBasicinfo;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CfgPrdBasicinfoMapper;
import cn.com.yusys.yusp.server.xdqt0007.req.Xdqt0007DataReqDto;
import cn.com.yusys.yusp.server.xdqt0007.resp.Xdqt0007DataRespDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 接口处理类:新微贷产品更新
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdqt0007Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdqt0007Service.class);

    @Resource
    private CfgPrdBasicinfoMapper cfgPrdBasicinfoMapper;

    /**
     * 新微贷产品信息同步
     *
     * @param xdqt0007DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdqt0007DataRespDto updateXWDPrdBasicInfo(Xdqt0007DataReqDto xdqt0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value);
        //返回对象
        Xdqt0007DataRespDto xdqt0007DataRespDto = new Xdqt0007DataRespDto();

        String prdCatalogFirstLevel = xdqt0007DataReqDto.getPrdCatalogFirstLevel();//产品一级目录
        String prdCatalogSecondLevel = xdqt0007DataReqDto.getPrdCatalogSecondLevel();//产品二级分类
        String prdCatalogThreeLevel = xdqt0007DataReqDto.getPrdCatalogThreeLevel();//产品三级分类
        String prdCatalogFourLevel = xdqt0007DataReqDto.getPrdCatalogFourLevel();//产品四级分类
        String prdName = xdqt0007DataReqDto.getPrdName();//产品名称
        String prdBelgLine = xdqt0007DataReqDto.getPrdBelgLine();//产品所属线条
        String prdType = xdqt0007DataReqDto.getPrdType();//产品类别
        String loanCha = xdqt0007DataReqDto.getLoanCha();//贷款性质
        String corePrdId = xdqt0007DataReqDto.getCorePrdId();//核心产品号
        String suitIndgtReportType = xdqt0007DataReqDto.getSuitIndgtReportType();//适用调查报告类型
        String isStopOffline = xdqt0007DataReqDto.getIsStopOffline();//是否需要线下调查
        String suitContType = xdqt0007DataReqDto.getSuitContType();//适用合同类型
        String suitCurType = xdqt0007DataReqDto.getSuitCurType();//适用币种
        String suitGuarMode = xdqt0007DataReqDto.getSuitGuarMode();//担保方式
        String suitPayMode = xdqt0007DataReqDto.getSuitPayMode();//支付方式
        String suitRepayMode = xdqt0007DataReqDto.getSuitRepayMode();//还款方式
        String isAllowExt = xdqt0007DataReqDto.getIsAllowExt();//是否允许展期
        String isAllowSignOnline = xdqt0007DataReqDto.getIsAllowSignOnline();//是否允许线上签约
        String isAllowDisbOnline = xdqt0007DataReqDto.getIsAllowDisbOnline();//是否允许线上放款
        String advRepayEndDate = xdqt0007DataReqDto.getAdvRepayEndDate();//提前还款截止日
        String prdStatus = xdqt0007DataReqDto.getPrdStatus();//产品状态

        try {
            CfgPrdBasicinfo cfgPrdBasicModel = new CfgPrdBasicinfo();
            cfgPrdBasicModel.setPrdId(prdCatalogFourLevel);
            cfgPrdBasicModel.setPrdName(prdName);
            cfgPrdBasicModel.setCatalogId(prdCatalogThreeLevel);
            String catalogLevelName = "->" + prdCatalogFirstLevel + "->" + prdCatalogSecondLevel + "->" + prdCatalogThreeLevel;
            cfgPrdBasicModel.setCatalogLevelName(catalogLevelName);
            cfgPrdBasicModel.setPrdBelgLine(prdBelgLine);
            cfgPrdBasicModel.setPrdType(prdType);
            cfgPrdBasicModel.setLoanCha(loanCha);
            cfgPrdBasicModel.setCorePrdId(corePrdId);
            cfgPrdBasicModel.setSuitIndgtReportType(suitIndgtReportType);
            cfgPrdBasicModel.setIsStopOffline(isStopOffline);
            cfgPrdBasicModel.setSuitContType(suitContType);
            cfgPrdBasicModel.setSuitCurType(suitCurType);
            cfgPrdBasicModel.setSuitGuarMode(suitGuarMode);
            cfgPrdBasicModel.setSuitPayMode(suitPayMode);
            cfgPrdBasicModel.setSuitRepayMode(suitRepayMode);
            cfgPrdBasicModel.setIsAllowExt(isAllowExt);
            cfgPrdBasicModel.setIsAllowSignOnline(isAllowSignOnline);
            cfgPrdBasicModel.setIsAllowDisbOnline(isAllowDisbOnline);
            cfgPrdBasicModel.setAdvRepayEndDate(advRepayEndDate);
            cfgPrdBasicModel.setPrdStatus(prdStatus);

            CfgPrdBasicinfo cfgPrdBasicinfo = cfgPrdBasicinfoMapper.selectByPrimaryKey(prdCatalogFourLevel);
            if (cfgPrdBasicinfo != null) {
                cfgPrdBasicinfoMapper.updateByPrimaryKeySelective(cfgPrdBasicModel);
            } else {
                cfgPrdBasicinfoMapper.insertSelective(cfgPrdBasicModel);
            }
            xdqt0007DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
            xdqt0007DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value);
        return xdqt0007DataRespDto;
    }
}
