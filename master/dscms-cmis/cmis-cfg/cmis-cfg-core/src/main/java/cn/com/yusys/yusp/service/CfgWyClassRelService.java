/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.dto.CfgWyClassRelClientDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgWyClassRel;
import cn.com.yusys.yusp.repository.mapper.CfgWyClassRelMapper;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgWyClassRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-07-29 10:45:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgWyClassRelService {

    @Autowired
    private CfgWyClassRelMapper cfgWyClassRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CfgWyClassRel selectByPrimaryKey(String pkId) {
        return cfgWyClassRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgWyClassRel> selectAll(QueryModel model) {
        List<CfgWyClassRel> records = (List<CfgWyClassRel>) cfgWyClassRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CfgWyClassRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgWyClassRel> list = cfgWyClassRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CfgWyClassRel record) {
        return cfgWyClassRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CfgWyClassRel record) {
        return cfgWyClassRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CfgWyClassRel record) {
        return cfgWyClassRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CfgWyClassRel record) {
        return cfgWyClassRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgWyClassRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgWyClassRelMapper.deleteByIds(ids);
    }

    /**
     * @函数名称: selectByCondition
     * @函数描述: 根据贷款用途，产品代码，是否农业，是否长期查询网银支用科目对应关系
     * @参数与返回说明:
     * @算法描述:
     */
    public CfgWyClassRel selectByCondition(CfgWyClassRelClientDto cfgWyClassRelClientDto) {
        return cfgWyClassRelMapper.selectLoanpurpAndPrdcodeAndIsNyAndIsCq(
                cfgWyClassRelClientDto.getLoanPurp(),
                cfgWyClassRelClientDto.getPrdCode(),
                cfgWyClassRelClientDto.getIsNy(),
                cfgWyClassRelClientDto.getIsCq()
        );
    }
}
