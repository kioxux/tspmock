/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.domain.CfgPrdBasicinfo;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.service.CfgPrdBasicinfoService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdBasicinfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-07 11:27:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgprdbasicinfo")
public class CfgPrdBasicinfoResource {
    private static final Logger log = LoggerFactory.getLogger(CfgPrdBasicinfoResource.class);

    @Autowired
    private CfgPrdBasicinfoService cfgPrdBasicinfoService;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgPrdBasicinfoDto>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgPrdBasicinfoDto> list = cfgPrdBasicinfoService.selectAll(queryModel);
        return new ResultDto<List<CfgPrdBasicinfoDto>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgPrdBasicinfoDto>> index(QueryModel queryModel) {
        List<CfgPrdBasicinfoDto> list = cfgPrdBasicinfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgPrdBasicinfoDto>>(list);
    }


    /**
     * @param queryModel 分页查询类
     * @函数名称:selectbymodel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<CfgPrdBasicinfoDto>> selectByModel(@RequestBody QueryModel queryModel) {
        List<CfgPrdBasicinfoDto> list = cfgPrdBasicinfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgPrdBasicinfoDto>>(list);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{prdId}")
    protected ResultDto<CfgPrdBasicinfo> show(@PathVariable("prdId") String prdId) {
        CfgPrdBasicinfo cfgPrdBasicinfo = cfgPrdBasicinfoService.selectByPrimaryKey(prdId);
        return new ResultDto<CfgPrdBasicinfo>(cfgPrdBasicinfo);
    }

    /**
     * @param cfgPrdBasicinfoDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CfgPrdBasicinfo>
     * @author 王玉坤
     * @date 2021/9/1 22:47
     * @version 1.0.0
     * @desc  根据产品代码查询产品信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/querycfgprdinfobyprcid")
    protected ResultDto<CfgPrdBasicinfoDto> queryCfgprdInfoByprcId(@RequestBody CfgPrdBasicinfoDto cfgPrdBasicinfoDto) {
        CfgPrdBasicinfo cfgPrdBasicinfo = cfgPrdBasicinfoService.selectByPrimaryKey(cfgPrdBasicinfoDto.getPrdId());
        CfgPrdBasicinfoDto prdBasicinfoDto = new CfgPrdBasicinfoDto();
        if (!Objects.isNull(cfgPrdBasicinfo)) {
            BeanUtils.beanCopy(cfgPrdBasicinfo, prdBasicinfoDto);
        } else {
            prdBasicinfoDto = null;
        }
        return new ResultDto<CfgPrdBasicinfoDto>(prdBasicinfoDto);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgPrdBasicinfo> create(@RequestBody CfgPrdBasicinfo cfgPrdBasicinfo) throws URISyntaxException {
        cfgPrdBasicinfoService.insert(cfgPrdBasicinfo);
        return new ResultDto<CfgPrdBasicinfo>(cfgPrdBasicinfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgPrdBasicinfo cfgPrdBasicinfo) throws URISyntaxException {
        int result = cfgPrdBasicinfoService.update(cfgPrdBasicinfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{prdId}")
    protected ResultDto<Integer> delete(@PathVariable("prdId") String prdId) {
        int result = cfgPrdBasicinfoService.deleteByPrimaryKey(prdId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgPrdBasicinfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 复制产品保存功能，在保存主表产品基本信息时，联动保存关联的子表数据
     *
     * @param cfgPrdBasicinfo 复制的产品基本信息
     * @param oldPrdId        被复制的老产品编号
     * @return
     */
    @PostMapping("/copyPrdBasicAndOtherSave/{oldPrdId}")
    protected ResultDto<Integer> copyPrdBasicAndOtherSave(@RequestBody CfgPrdBasicinfo cfgPrdBasicinfo, @PathVariable String oldPrdId) {
        log.info("被复制的产品编号【{}】", JSONObject.toJSON(oldPrdId));
        log.info("复制后的产品基本信息【{}】", JSONObject.toJSON(cfgPrdBasicinfo));
        int result = cfgPrdBasicinfoService.copyPrdBasicAndOtherSave(cfgPrdBasicinfo, oldPrdId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    /*@PostMapping("/logicalDelete/{prdId}")
    protected ResultDto<Integer> logicalDelete(@PathVariable("prdId") String prdId) {
        int result = cfgPrdBasicinfoService.logicalDeleteByPrimaryKey(prdId);
        return new ResultDto<Integer>(result);
    }*/

    /**
     * 逻辑删除产品功能，联动逻辑删除关联的子表数据
     *
     * @param cfgPrdBasicinfo 产品基本信息
     * @return
     */
    @PostMapping("/deleteCfgPrdBasicinfo")
    protected ResultDto<Integer> deleteCfgPrdBasicinfoAndChild(@RequestBody CfgPrdBasicinfo cfgPrdBasicinfo) {
        int result = cfgPrdBasicinfoService.deleteCfgPrdBasicinfoAndChild(cfgPrdBasicinfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * 通过入参的参数编号查询产品名称
     *
     * @param param
     * @return
     */
    @PostMapping("/getSuitPrdName")
    public ResultDto<String> getSuitPrdName(@RequestBody Map param) {
        return new ResultDto<String>(cfgPrdBasicinfoService.getSuitPrdName(param));
    }

    /**
     * @param map:字典码值
     * @Description:提供给biz服务接口XDTZ0056使用
     * @Author: YX-WJ
     * @Date: 2021/6/5 15:38
     * @return: cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     **/
    @PostMapping("/queryContInfoByEnName4Biz")
    protected ResultDto<Integer> queryContInfoByEnName4Bi(@RequestBody Map<String, String> map) {
        String prdId = map.get("prdId");
        Integer count = cfgPrdBasicinfoService.queryContInfoByEnName4Biz(prdId);
        return new ResultDto<>(count);
    }

    /**
     * 根据产品编号查询产品信息
     *
     * @param prdId
     * @return
     * @author xuchao
     */
    @PostMapping("/queryCfgPrdBasicInfo")
    protected ResultDto<CfgPrdBasicinfoDto> queryCfgPrdBasicInfo(@RequestBody String prdId) {
        CfgPrdBasicinfoDto cfgPrdBasicinfoDto = cfgPrdBasicinfoService.queryCfgPrdBasicInfo(prdId);
        return ResultDto.success(cfgPrdBasicinfoDto);
    }

    /**
     * 根据产品目录层级catalogid查询产品列表
     *
     * @param catalogId
     * @return
     * @author xuchao
     */
    @PostMapping("/queryBasicInfoByCatalogId")
    protected ResultDto<List<CfgPrdBasicinfoDto>> queryBasicInfoByCatalogId(@RequestBody String catalogId) {
        List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtos = cfgPrdBasicinfoService.queryBasicInfoByCatalogId(catalogId);
        return ResultDto.success(cfgPrdBasicinfoDtos);
    }

    /**
     * 根据产品类别查询所有的产品信息
     *
     * @param prdType
     * @return
     * @author xuchao
     */
    @PostMapping("/queryCfgPrdBasicInfoByPrdType")
    protected ResultDto<List<CfgPrdBasicinfoDto>> queryCfgPrdBasicInfoByPrdType(@RequestBody String prdType) {
        List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtos = cfgPrdBasicinfoService.queryCfgPrdBasicInfoByPrdType(prdType);
        return ResultDto.success(cfgPrdBasicinfoDtos);
    }

    /**
     * 根据目录层级查询产品信息
     *
     * @param cataloglevelName
     * @return
     * @author xuchao
     */
    @PostMapping("/querybasicinfobycatalog")
    protected ResultDto<List<CfgPrdBasicinfoDto>> queryBasicInfoByCatalog(@RequestBody String cataloglevelName) {
        List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtos = cfgPrdBasicinfoService.queryBasicInfoByCatalog(cataloglevelName);
        return ResultDto.success(cfgPrdBasicinfoDtos);
    }

    /**
     * @param queryModel
     * @函数名称:selectcfgprdbasicinfodata
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectcfgprdbasicinfodata")
    protected ResultDto<List<CfgPrdBasicinfoDto>> selectCfgPrdBasicinfoData(@RequestBody QueryModel queryModel) {
        List<CfgPrdBasicinfoDto> list = cfgPrdBasicinfoService.selectCfgPrdBasicinfoData(queryModel);
        return new ResultDto<List<CfgPrdBasicinfoDto>>(list);
    }
}
