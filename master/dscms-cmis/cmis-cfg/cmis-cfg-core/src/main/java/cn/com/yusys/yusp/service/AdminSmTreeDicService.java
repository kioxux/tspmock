package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AdminSmTreeDic;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AdminSmTreeDicMapper;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: AdminSmTreeDicService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 12651
 * @创建时间: 2021-05-07 16:07:01
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AdminSmTreeDicService {

    @Autowired
    private AdminSmTreeDicMapper adminSmTreeDicMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AdminSmTreeDic selectByPrimaryKey(String pkId) {
        return adminSmTreeDicMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AdminSmTreeDic> selectAll(QueryModel model) {
        List<AdminSmTreeDic> records = (List<AdminSmTreeDic>) adminSmTreeDicMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AdminSmTreeDic> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AdminSmTreeDic> list = adminSmTreeDicMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(AdminSmTreeDic record) {
        return adminSmTreeDicMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(AdminSmTreeDic record) {
        return adminSmTreeDicMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(AdminSmTreeDic record) {
        return adminSmTreeDicMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(AdminSmTreeDic record) {
        return adminSmTreeDicMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return adminSmTreeDicMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return adminSmTreeDicMapper.deleteByIds(ids);
    }

    public List<Map<String, Object>> queryAdminSmDicTree(String optType, String rootId) {
        QueryModel model = new QueryModel();
        model.addCondition("optType", optType);
        List<AdminSmTreeDic> list = adminSmTreeDicMapper.selectByModel(model);
        //将所有对象放到map中 格式是 <pid,List<Node>>
        Map<String, ArrayList<AdminSmTreeDic>> allMap = new HashMap<>(16);
        for (AdminSmTreeDic item : list) {
            item.setAbvenName(StringUtils.trimToEmpty(item.getAbvenName()));
            if (allMap.containsKey(item.getAbvenName())) {
                allMap.get(item.getAbvenName()).add(item);
            } else {
                ArrayList<AdminSmTreeDic> sublist = new ArrayList<>();
                sublist.add(item);
                allMap.put(item.getAbvenName(), sublist);
            }
        }
        return getTree(rootId, allMap);
    }

    /**
     * @param root, all]
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author tangxun
     * @modify guyh 修改根节点支持null配置
     * @date 2021/1/12 9:52
     * @version 1.0.0
     * @desc 组装前端要求的格式
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private List<Map<String, Object>> getTree(String root, Map<String, ArrayList<AdminSmTreeDic>> all) {
        if (all.containsKey(StringUtils.trimToEmpty(root))) {
            return all.get(StringUtils.trimToEmpty(root)).stream().map(item -> {
                Map<String, Object> map = new LinkedHashMap<>();
                map.put("label", item.getCnName());
                map.put("id", item.getEnName());
                map.put("levels", item.getLevels());
                //System.out.println("id"+item.getEnName()+",label"+item.getCnName());
                List list = getTree(item.getEnName(), all);
                if (!list.isEmpty()) {
                    map.put("children", list);
                }
                return map;
            }).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * @param
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author xuchi
     * @date 2021/1/12 9:52
     * @version 1.0.0
     * @desc 组装前端要求的格式
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Cacheable(value = "queryAdminSmDicTree", key = "#optType+'-'+#abvenName")
    public List<Map<String, Object>> queryAdminSmDicTree(String optType, String rootId, String maxLevels, String abvenName) {
        if (StringUtils.trimToEmpty(abvenName).equals("")) {
            throw BizException.error(null, EpbEnum.EPB090009.key, EpbEnum.EPB090009.value);
        }
        //获取当前节点的层级所有下一层级的节点
        QueryModel modellist = new QueryModel();
        modellist.addCondition("optType", optType);
        modellist.addCondition("abvenName", abvenName);
        modellist.addCondition("maxLevels", maxLevels);
        List<AdminSmTreeDic> list = adminSmTreeDicMapper.selectByModel(modellist);
        //返回字典列表
        return list.stream().map((item) -> {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("label", item.getCnName());
            map.put("id", item.getEnName());
            map.put("levels", item.getLevels());
            return map;
        }).collect(Collectors.toList());
    }

    public List<Map<String, Object>> queryAdminSmSelectTree(String optType, String levels, String abvenName) {
        if (StringUtils.trimToEmpty(abvenName).equals("")) {
            throw BizException.error(null, EpbEnum.EPB090009.key, EpbEnum.EPB090009.value);
        }
        //获取当前节点的层级所有下一层级的节点
        QueryModel modellist = new QueryModel();
        modellist.addCondition("optType", optType);
        modellist.addCondition("abvenName", abvenName);
        List<AdminSmTreeDic> list = adminSmTreeDicMapper.selectByModel(modellist);
        //返回字典列表
        return list.stream().map((item) -> {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("key", item.getEnName());
            map.put("value", item.getCnName());
            return map;
        }).collect(Collectors.toList());
    }

    public List<Map<String, Object>> queryAdminSmDicTreeInputName(String optType, String rootId) {
        QueryModel model = new QueryModel();
        model.addCondition("optType", optType);
        List<AdminSmTreeDic> list = adminSmTreeDicMapper.selectByModel(model);
        //将所有对象放到map中 格式是 <pid,List<Node>>
        Map<String, ArrayList<AdminSmTreeDic>> allMap = new HashMap<>(16);
        for (AdminSmTreeDic item : list) {
            item.setAbvenName(StringUtils.trimToEmpty(item.getAbvenName()));
            if (allMap.containsKey(item.getAbvenName())) {
                allMap.get(item.getAbvenName()).add(item);
            } else {
                ArrayList<AdminSmTreeDic> sublist = new ArrayList<>();
                sublist.add(item);
                allMap.put(item.getAbvenName(), sublist);
            }
        }
        return getTreeInputName(rootId, allMap);
    }

    private List<Map<String, Object>> getTreeInputName(String root, Map<String, ArrayList<AdminSmTreeDic>> all) {
        if (all.containsKey(StringUtils.trimToEmpty(root))) {
            return all.get(StringUtils.trimToEmpty(root)).stream().map(item -> {
                Map<String, Object> map = new LinkedHashMap<>();
                map.put("label", item.getCnName());
                map.put("id", item.getEnName());
                map.put("pkId", item.getPkId());
                map.put("locate", item.getLocate());
                if (Objects.nonNull(item.getInputId())) {
                    map.put("inputIdName", OcaTranslatorUtils.getUserName(item.getInputId()));
                    map.put("inputBrIdName", OcaTranslatorUtils.getOrgName(item.getInputBrId()));
                }
                List list = getTreeInputName(item.getEnName(), all);
                if (!list.isEmpty()) {
                    map.put("children", list);
                }
                return map;
            }).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public int treeDdeleteByPrimaryKey(AdminSmTreeDicDto adminSmTreeDicDto) {
        adminSmTreeDicDto.setLocate(adminSmTreeDicDto.getLocate() + '%');
        int count = adminSmTreeDicMapper.deleteBylocate(adminSmTreeDicDto);
        return count;
    }

    public AdminSmTreeDicDto queryCfgtreebBycode(AdminSmTreeDicDto adminSmTreeDicDto) {
        return adminSmTreeDicMapper.queryCfgtreebBycode(adminSmTreeDicDto);
    }

    public AdminSmTreeDicDto queryHighCfgtreebBycode(AdminSmTreeDicDto adminSmTreeDicDto) {
        return adminSmTreeDicMapper.queryHighCfgtreebBycode(adminSmTreeDicDto);
    }
}
