/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.risk.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: yusp-npam-biz-core模块
 * @类名称: PrdPreventRisk
 * @类描述: PRD_PREVENT_RISK数据实体类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2020-07-27 17:26:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "PRD_PREVENT_RISK")
public class PrdPreventRisk extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 风险拦截方案编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PREVENT_ID")
    private String preventId;

    /**
     * 风险拦截方案名称
     **/
    @Column(name = "PREVENT_DESC", unique = false, nullable = true, length = 100)
    private String preventDesc;

    /**
     * 是否启用
     **/
    @Column(name = "USED_IND", unique = false, nullable = true, length = 1)
    private String usedInd;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 备注
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 200)
    private String memo;

    /**
     * 是否适用流程
     **/
    @Column(name = "USED_FLOW", unique = false, nullable = true, length = 1)
    private String usedFlow;

    /**
     * 方案分类
     **/
    @Column(name = "ITEM_TYPE", unique = false, nullable = true, length = 2)
    private String itemType;

    /**
     * INSTU_CDE
     **/
    @Column(name = "INSTU_CDE", unique = false, nullable = true, length = 10)
    private String instuCde;

    /**
     * @return preventId
     */
    public String getPreventId() {
        return this.preventId;
    }

    /**
     * @param preventId
     */
    public void setPreventId(String preventId) {
        this.preventId = preventId;
    }

    /**
     * @return preventDesc
     */
    public String getPreventDesc() {
        return this.preventDesc;
    }

    /**
     * @param preventDesc
     */
    public void setPreventDesc(String preventDesc) {
        this.preventDesc = preventDesc;
    }

    /**
     * @return usedInd
     */
    public String getUsedInd() {
        return this.usedInd;
    }

    /**
     * @param usedInd
     */
    public void setUsedInd(String usedInd) {
        this.usedInd = usedInd;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return usedFlow
     */
    public String getUsedFlow() {
        return this.usedFlow;
    }

    /**
     * @param usedFlow
     */
    public void setUsedFlow(String usedFlow) {
        this.usedFlow = usedFlow;
    }

    /**
     * @return itemType
     */
    public String getItemType() {
        return this.itemType;
    }

    /**
     * @param itemType
     */
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    /**
     * @return instuCde
     */
    public String getInstuCde() {
        return this.instuCde;
    }

    /**
     * @param instuCde
     */
    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }


}