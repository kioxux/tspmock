/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.CfgContModeBookmark;
import cn.com.yusys.yusp.domain.CfgContTplLabel;
import cn.com.yusys.yusp.repository.mapper.CfgContTplLabelMapper;
import cn.com.yusys.yusp.util.WordBookmarksUtil;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgContTplLabelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-17 20:52:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgContTplLabelService {

    private static final Logger log = LoggerFactory.getLogger(CfgContTplLabelService.class);

    @Autowired
    private CfgContTplLabelMapper cfgContTplLabelMapper;

    @Autowired
    private CfgContModeBookmarkService cfgContModeBookmarkService;

    // @Value("${application.cont.localPath}")
    private String deposeFilesDir = "";

    // @Value("${application.cont.remotePath}")
    private String remotePath = "";

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgContTplLabel selectByPrimaryKey(String pkId) {
        return cfgContTplLabelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CfgContTplLabel> selectAll(QueryModel model) {
        List<CfgContTplLabel> records = (List<CfgContTplLabel>) cfgContTplLabelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgContTplLabel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgContTplLabel> list = cfgContTplLabelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CfgContTplLabel record) {
        return cfgContTplLabelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CfgContTplLabel record) {
        return cfgContTplLabelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CfgContTplLabel record) {
        return cfgContTplLabelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CfgContTplLabel record) {
        return cfgContTplLabelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return cfgContTplLabelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return cfgContTplLabelMapper.deleteByIds(ids);
    }

    /**
     * 这边上传模版时候得调然后得记录下写的书签，后面合同出来时去一个个的替换调
     *
     * @throws Exception
     */
    public Map<String, String> uploadFilesInfo(MultipartFile file) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        String fileName = file.getOriginalFilename();
        String nowRemotePath = "";
        int index = fileName.lastIndexOf("\\");
        if (index > 0) {
            fileName = fileName.substring(index + 1);
        }
        long fileSize = file.getSize();
        // 当文件有后缀名时
        if (fileName.indexOf(".") >= 0) {
            // split()中放正则表达式; 转义字符"\\."代表 "."
            String[] fileNameSplitArray = fileName.split("\\.");
            // 加上random戳,防止附件重名覆盖原文件
            fileName = fileNameSplitArray[0] + (int) (Math.random() * 100000) + "." + fileNameSplitArray[1];
        }
        // 当文件无后缀名时(如C盘下的hosts文件就没有后缀名)
        if (fileName.indexOf(".") < 0) {
            // 加上random戳,防止附件重名覆盖原文件
            fileName = fileName + (int) (Math.random() * 100000);
        }
        File dest = new File(deposeFilesDir + fileName);
        log.info("文件名:" + deposeFilesDir + fileName);
        // 如果该文件的上级文件夹不存在，则创建该文件的上级文件夹及其祖辈级文件夹;
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            // 将获取到的附件file,transferTo写入到指定的位置(即:创建dest时，指定的路径)
            file.transferTo(dest);
            nowRemotePath = remotePath + "/" + DateUtils.getCurrDateStr();
            // 上传方式有问题
            // SftpUtil.getSftpUtil().upload(dest, nowRemotePath);  // 前端weboffice打开来文件，那个文件的地方得有个nginx，先借前端工程服务器用用
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        try {
            WordBookmarksUtil docxTest = new WordBookmarksUtil();
            List<String> bookmarks = docxTest.getBookmarks(deposeFilesDir + fileName);
            if (!bookmarks.isEmpty()) {
                String marks = bookmarks.toString();
                marks = marks.substring(1, marks.length() - 1);
                marks = marks.replace(" ", "");
                map.put("bookmarks", marks);
                log.info(marks);
            } else {
                log.info("文件没有书签");
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        //这个filepath用于远程打开页面，需要处理
        String returnPath = nowRemotePath + "/" + fileName;
        String pathBefore = returnPath.substring(0, returnPath.indexOf("html"));
        String pathReal = returnPath.substring(pathBefore.length() + 4, returnPath.length());

        map.put("filePath", pathReal);
        map.put("fileSize", fileSize / 1024 + "KB");
        map.put("fileDate", DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        return map;
    }

    public CfgContTplLabel getContCfg(String contTplCode) {
        return cfgContTplLabelMapper.getContCfg(contTplCode);
    }

    /**
     * 保存修改后的文件到远程
     *
     * @param file
     * @param pkId
     * @return
     * @throws Exception
     */
    public String saveFilesInfo(MultipartFile file, String pkId) throws Exception {
        CfgContTplLabel cfgContTplLabel = cfgContTplLabelMapper.selectByPrimaryKey(pkId);
        String orgiFilePath = cfgContTplLabel.getContTplPath(); // 需要从这个里面获取到文件原名 用新文件覆盖原文件
        int index = orgiFilePath.lastIndexOf("/");
        String orgiFileName = "";
        String orgiDate = "";
        if (index > 0) {
            orgiFileName = orgiFilePath.substring(index + 1);
            orgiDate = orgiFilePath.substring(index - 10, index);
            orgiFilePath = orgiFilePath.substring(0, index);
        }

        File dest = new File(deposeFilesDir + orgiFileName);
        // 如果该文件的上级文件夹不存在，则创建该文件的上级文件夹及其祖辈级文件夹;
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            // 将获取到的附件file,transferTo写入到指定的位置(即:创建dest时，指定的路径)
            file.transferTo(dest);
            // 上传方式有问题
            // SftpUtil.getSftpUtil().upload(dest,  remotePath + "/" + orgiDate);  //前端weboffice打开来文件，那个文件的地方得有个nginx，先借前端工程服务器用用
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        try {
            WordBookmarksUtil docxTest = new WordBookmarksUtil();
            List<String> bookmarks = docxTest.getBookmarks(deposeFilesDir + orgiFileName);
            if (!bookmarks.isEmpty()) {
                String marks = bookmarks.toString();
                marks = marks.substring(1, marks.length() - 1);
                marks = marks.replace(" ", "");
                CfgContModeBookmark cfgContModeBookmark = new CfgContModeBookmark();
                cfgContModeBookmark.setBookmarkName(marks);
                cfgContModeBookmark.setContTplCode(cfgContTplLabel.getContTplCode());
                cfgContModeBookmarkService.autoMark(cfgContModeBookmark);
            } else {
                log.info("文件没有书签");
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return "true";
    }
}
