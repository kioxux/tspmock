/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.dto.CfgRetailPrimeRateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgRetailPrimeRate;
import cn.com.yusys.yusp.repository.mapper.CfgRetailPrimeRateMapper;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgRetailPrimeRateService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:26:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgRetailPrimeRateService {

    @Autowired
    private CfgRetailPrimeRateMapper cfgRetailPrimeRateMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CfgRetailPrimeRate selectByPrimaryKey(String pkId) {
        return cfgRetailPrimeRateMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgRetailPrimeRate> selectAll(QueryModel model) {
        List<CfgRetailPrimeRate> records = (List<CfgRetailPrimeRate>) cfgRetailPrimeRateMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CfgRetailPrimeRate> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgRetailPrimeRate> list = cfgRetailPrimeRateMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CfgRetailPrimeRate record) {
        return cfgRetailPrimeRateMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CfgRetailPrimeRate record) {
        return cfgRetailPrimeRateMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CfgRetailPrimeRate record) {
        return cfgRetailPrimeRateMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CfgRetailPrimeRate record) {
        return cfgRetailPrimeRateMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgRetailPrimeRateMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgRetailPrimeRateMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectbyPrdId
     * @方法描述: 根据产品编号查询最新的数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgRetailPrimeRate selectbyPrdId(String prdId) {
        return cfgRetailPrimeRateMapper.selectbyPrdId(prdId);
    }
    
    /**
     * @param cfgRetailPrimeRateDto
     * @return java.util.List<cn.com.yusys.yusp.domain.CfgRetailPrimeRate>
     * @author 王玉坤
     * @date 2021/9/10 15:38
     * @version 1.0.0
     * @desc 根据条件查询优惠申请利率配置信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CfgRetailPrimeRateDto> selectRetailPrimerateByCondition(CfgRetailPrimeRateDto cfgRetailPrimeRateDto) {
        return cfgRetailPrimeRateMapper.selectRetailPrimerateByCondition(cfgRetailPrimeRateDto);
    }
}
