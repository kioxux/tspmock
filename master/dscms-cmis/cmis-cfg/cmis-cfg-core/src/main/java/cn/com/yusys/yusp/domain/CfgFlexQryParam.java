/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg模块
 * @类名称: CfgFlexQryParam
 * @类描述: cfg_flex_qry_param数据实体类
 * @功能描述:
 * @创建人: mashun
 * @创建时间: 2021-02-02 11:29:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_flex_qry_param")
public class CfgFlexQryParam extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 参数英文名
     **/
    @Column(name = "PARAM_KEY", unique = false, nullable = false, length = 100)
    private String paramKey;

    /**
     * 参数中文描述
     **/
    @Column(name = "PARAM_NAME", unique = false, nullable = true, length = 255)
    private String paramName;

    /**
     * 参数值
     **/
    @Column(name = "PARAM_VALUE", unique = false, nullable = true, length = 100)
    private String paramValue;

    /**
     * 参数类型
     **/
    @Column(name = "PARAM_TYPE", unique = false, nullable = true, length = 5)
    private String paramType;

    /**
     * 查询报表编号
     **/
    @Column(name = "QRY_CODE", unique = false, nullable = false, length = 40)
    private String qryCode;

    /**
     * 显示排序字段
     **/
    @Column(name = "SHOW_ORDER", unique = false, nullable = true, length = 10)
    private Integer showOrder;

    public CfgFlexQryParam() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return paramKey
     */
    public String getParamKey() {
        return this.paramKey;
    }

    /**
     * @param paramKey
     */
    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    /**
     * @return paramName
     */
    public String getParamName() {
        return this.paramName;
    }

    /**
     * @param paramName
     */
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    /**
     * @return paramValue
     */
    public String getParamValue() {
        return this.paramValue;
    }

    /**
     * @param paramValue
     */
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    /**
     * @return paramType
     */
    public String getParamType() {
        return this.paramType;
    }

    /**
     * @param paramType
     */
    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    /**
     * @return qryCode
     */
    public String getQryCode() {
        return this.qryCode;
    }

    /**
     * @param qryCode
     */
    public void setQryCode(String qryCode) {
        this.qryCode = qryCode;
    }

    /**
     * @return showOrder
     */
    public Integer getShowOrder() {
        return this.showOrder;
    }

    /**
     * @param showOrder
     */
    public void setShowOrder(Integer showOrder) {
        this.showOrder = showOrder;
    }


}