package cn.com.yusys.yusp.util;

import java.util.ArrayList;

public class CopsStringUtil {

    public static String getParamString(String string, char leftFlag, char rightFlag) {
        String param;
        for (param = ""; string.indexOf(rightFlag) < string.indexOf(leftFlag); string = string
                .substring(string.indexOf(rightFlag) + 1)) {
        }
        if (string.indexOf(leftFlag) >= 0 && string.indexOf(rightFlag) >= 0) {
            int pos1 = string.indexOf(leftFlag);
            int pos2 = string.indexOf(rightFlag);
            param = string.substring(pos1 + 1, pos2);
        }
        return param;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static String[] strToArray(String string, char separator) {
        String[] returnArray;
        if (string != null && !string.equalsIgnoreCase("")) {
            int lastpos = 0;
            ArrayList pos = new ArrayList();
            pos.add(String.valueOf(-1));

            for (int i = 0; i < string.length(); ++i) {
                if (string.charAt(i) == separator) {
                    lastpos = i;
                    pos.add(String.valueOf(i));
                }
            }

            if (lastpos != string.length()) {
                pos.add(String.valueOf(string.length()));
            }

            int[] ps = new int[pos.size()];

            int i;
            for (i = 0; i < ps.length; ++i) {
                ps[i] = Integer.parseInt(String.valueOf(pos.get(i)));
            }

            returnArray = new String[ps.length - 1];

            for (i = 0; i < returnArray.length; ++i) {
                returnArray[i] = string.substring(ps[i] + 1, ps[i + 1]).trim();
            }
        } else {
            returnArray = new String[]{""};
        }
        return returnArray;
    }

    public static String replace(String str, String str1, String str2) {
        for (int pos = str.indexOf(str1); pos >= 0; pos = str.indexOf(str1)) {
            str = new String(
                    (new StringBuffer(str.substring(0, pos))).append(str2).append(str.substring(str1.length() + pos)));
        }

        return str;
    }

}
