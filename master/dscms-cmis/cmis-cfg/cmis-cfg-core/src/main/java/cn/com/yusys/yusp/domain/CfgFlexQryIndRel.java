/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryIndRel
 * @类描述: cfg_flex_qry_ind_rel数据实体类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-30 17:23:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_flex_qry_ind_rel")
public class CfgFlexQryIndRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 关联左表指标配置编码
     **/
    @Column(name = "LEFT_INDEX_CODE", unique = false, nullable = true, length = 40)
    private String leftIndexCode;

    /**
     * 关联左表表名
     **/
    @Column(name = "LEFT_TABLE_NAME", unique = false, nullable = true, length = 100)
    private String leftTableName;

    /**
     * 关联右表指标配置编码
     **/
    @Column(name = "RIGHT_INDEX_CODE", unique = false, nullable = true, length = 40)
    private String rightIndexCode;

    /**
     * 关联右表表名
     **/
    @Column(name = "RIGHT_TABLE_NAME", unique = false, nullable = true, length = 100)
    private String rightTableName;

    /**
     * 左表关联字段
     **/
    @Column(name = "LEFT_COL_NAME", unique = false, nullable = true, length = 100)
    private String leftColName;

    /**
     * 右表关联字段
     **/
    @Column(name = "RIGHT_COL_NAME", unique = false, nullable = true, length = 100)
    private String rightColName;

    /**
     * 左表别名
     **/
    @Column(name = "LEFT_TABLE_ALIAS", unique = false, nullable = true, length = 30)
    private String leftTableAlias;

    /**
     * 右表别名
     **/
    @Column(name = "RIGHT_TABLE_ALIAS", unique = false, nullable = true, length = 30)
    private String rightTableAlias;

    /**
     * 关联方式
     **/
    @Column(name = "JOIN_WAY", unique = false, nullable = true, length = 10)
    private String joinWay;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgFlexQryIndRel() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return leftIndexCode
     */
    public String getLeftIndexCode() {
        return this.leftIndexCode;
    }

    /**
     * @param leftIndexCode
     */
    public void setLeftIndexCode(String leftIndexCode) {
        this.leftIndexCode = leftIndexCode;
    }

    /**
     * @return leftTableName
     */
    public String getLeftTableName() {
        return this.leftTableName;
    }

    /**
     * @param leftTableName
     */
    public void setLeftTableName(String leftTableName) {
        this.leftTableName = leftTableName;
    }

    /**
     * @return rightIndexCode
     */
    public String getRightIndexCode() {
        return this.rightIndexCode;
    }

    /**
     * @param rightIndexCode
     */
    public void setRightIndexCode(String rightIndexCode) {
        this.rightIndexCode = rightIndexCode;
    }

    /**
     * @return rightTableName
     */
    public String getRightTableName() {
        return this.rightTableName;
    }

    /**
     * @param rightTableName
     */
    public void setRightTableName(String rightTableName) {
        this.rightTableName = rightTableName;
    }

    /**
     * @return leftColName
     */
    public String getLeftColName() {
        return this.leftColName;
    }

    /**
     * @param leftColName
     */
    public void setLeftColName(String leftColName) {
        this.leftColName = leftColName;
    }

    /**
     * @return rightColName
     */
    public String getRightColName() {
        return this.rightColName;
    }

    /**
     * @param rightColName
     */
    public void setRightColName(String rightColName) {
        this.rightColName = rightColName;
    }

    /**
     * @return leftTableAlias
     */
    public String getLeftTableAlias() {
        return this.leftTableAlias;
    }

    /**
     * @param leftTableAlias
     */
    public void setLeftTableAlias(String leftTableAlias) {
        this.leftTableAlias = leftTableAlias;
    }

    /**
     * @return rightTableAlias
     */
    public String getRightTableAlias() {
        return this.rightTableAlias;
    }

    /**
     * @param rightTableAlias
     */
    public void setRightTableAlias(String rightTableAlias) {
        this.rightTableAlias = rightTableAlias;
    }

    /**
     * @return joinWay
     */
    public String getJoinWay() {
        return this.joinWay;
    }

    /**
     * @param joinWay
     */
    public void setJoinWay(String joinWay) {
        this.joinWay = joinWay;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}