/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdChnlRel
 * @类描述: cfg_prd_chnl_rel数据实体类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-17 15:49:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_prd_chnl_rel")
public class CfgPrdChnlRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 渠道编号
     **/
    @Column(name = "CHNL_ID", unique = false, nullable = true, length = 40)
    private String chnlId;

    /**
     * 渠道名称
     **/
    @Column(name = "CHNL_NAME", unique = false, nullable = true, length = 40)
    private String chnlName;

    /**
     * 产品编号
     **/
    @Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
    private String prdId;

    /**
     * 渠道标识 STD_ZB_CHANNEL
     **/
    @Column(name = "CHNL_FLAG", unique = false, nullable = true, length = 5)
    private String chnlFlag;

    /**
     * 是否启用 STD_ZB_YES_NO
     **/
    @Column(name = "IS_BEGIN", unique = false, nullable = true, length = 5)
    private String isBegin;

    /**
     * 备注
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 5)
    private String remark;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgPrdChnlRel() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return chnlId
     */
    public String getChnlId() {
        return this.chnlId;
    }

    /**
     * @param chnlId
     */
    public void setChnlId(String chnlId) {
        this.chnlId = chnlId;
    }

    /**
     * @return prdId
     */
    public String getPrdId() {
        return this.prdId;
    }

    /**
     * @param prdId
     */
    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    /**
     * @return chnlFlag
     */
    public String getChnlFlag() {
        return this.chnlFlag;
    }

    /**
     * @param chnlFlag
     */
    public void setChnlFlag(String chnlFlag) {
        this.chnlFlag = chnlFlag;
    }

    /**
     * @return isBegin
     */
    public String getIsBegin() {
        return this.isBegin;
    }

    /**
     * @param isBegin
     */
    public void setIsBegin(String isBegin) {
        this.isBegin = isBegin;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return chnlName
     */
    public String getChnlName() {
        return this.chnlName;
    }

    /**
     * @param chnlName
     */
    public void setChnlName(String chnlName) {
        this.chnlName = chnlName;
    }


}