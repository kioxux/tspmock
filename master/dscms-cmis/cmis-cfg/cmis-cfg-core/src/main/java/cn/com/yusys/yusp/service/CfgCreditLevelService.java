/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgCreditLevel;
import cn.com.yusys.yusp.dto.CfgCreditLevelDto;
import cn.com.yusys.yusp.repository.mapper.CfgCreditLevelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgCreditLevelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-01 09:24:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgCreditLevelService {

    @Autowired
    private CfgCreditLevelMapper cfgCreditLevelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgCreditLevel selectByPrimaryKey(String pkId) {
        return cfgCreditLevelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgCreditLevel> selectAll(QueryModel model) {
        List<CfgCreditLevel> records = (List<CfgCreditLevel>) cfgCreditLevelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgCreditLevel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgCreditLevel> list = cfgCreditLevelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgCreditLevel record) {
        return cfgCreditLevelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgCreditLevel record) {
        return cfgCreditLevelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgCreditLevel record) {
        return cfgCreditLevelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgCreditLevel record) {
        return cfgCreditLevelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgCreditLevelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgCreditLevelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryCfgCreditLevelByType
     * @方法描述: 根据cfgType查询
     * 01 资产抵押率配置（默认）
     * 02 信用等级配置
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CfgCreditLevel> queryCfgCreditLevelByType(CfgCreditLevelDto cfgCreditLevelDto) {
        String cfgType = CommonConstant.CFG_TYPE_01;
        if (StringUtils.nonEmpty(cfgCreditLevelDto.getCfgType())) {
            cfgType = cfgCreditLevelDto.getCfgType();
        }
        return cfgCreditLevelMapper.queryCfgCreditLevelByType(cfgType);
    }
}
