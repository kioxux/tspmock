/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBizRulePlan
 * @类描述: cfg_biz_rule_plan数据实体类
 * @功能描述:
 * @创建人: liuch
 * @创建时间: 2020-12-23 11:22:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_biz_rule_plan")
public class CfgBizRulePlan extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务规则方案编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PLAN_ID")
    private String planId;

    /**
     * 业务规则方案名称
     **/
    @Column(name = "PLAN_NAME", unique = false, nullable = true, length = 40)
    private String planName;

    /**
     * 方案分类 STD_ZB_PLAN_TYPE
     **/
    @Column(name = "PLAN_TYPE", unique = false, nullable = true, length = 5)
    private String planType;

    /**
     * 拦截类型 STD_ZB_NOTI_TYPE
     **/
    @Column(name = "NOTI_TYPE", unique = false, nullable = true, length = 5)
    private String notiType;

    /**
     * 是否启用 STD_ZB_YES_NO
     **/
    @Column(name = "USED_IND", unique = false, nullable = true, length = 5)
    private String usedInd;

    /**
     * 是否适用流程 STD_ZB_YES_NO
     **/
    @Column(name = "USED_FLOW", unique = false, nullable = true, length = 5)
    private String usedFlow;

    /**
     * 是否自动规则 STD_ZB_YES_NO
     **/
    @Column(name = "USED_AUTO", unique = false, nullable = true, length = 5)
    private String usedAuto;

    /**
     * 备注
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 400)
    private String remark;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgBizRulePlan() {
        // Not compliant
    }

    /**
     * @return planId
     */
    public String getPlanId() {
        return this.planId;
    }

    /**
     * @param planId
     */
    public void setPlanId(String planId) {
        this.planId = planId;
    }

    /**
     * @return planName
     */
    public String getPlanName() {
        return this.planName;
    }

    /**
     * @param planName
     */
    public void setPlanName(String planName) {
        this.planName = planName;
    }

    /**
     * @return planType
     */
    public String getPlanType() {
        return this.planType;
    }

    /**
     * @param planType
     */
    public void setPlanType(String planType) {
        this.planType = planType;
    }

    /**
     * @return notiType
     */
    public String getNotiType() {
        return this.notiType;
    }

    /**
     * @param notiType
     */
    public void setNotiType(String notiType) {
        this.notiType = notiType;
    }

    /**
     * @return usedInd
     */
    public String getUsedInd() {
        return this.usedInd;
    }

    /**
     * @param usedInd
     */
    public void setUsedInd(String usedInd) {
        this.usedInd = usedInd;
    }

    /**
     * @return usedFlow
     */
    public String getUsedFlow() {
        return this.usedFlow;
    }

    /**
     * @param usedFlow
     */
    public void setUsedFlow(String usedFlow) {
        this.usedFlow = usedFlow;
    }

    /**
     * @return usedAuto
     */
    public String getUsedAuto() {
        return this.usedAuto;
    }

    /**
     * @param usedAuto
     */
    public void setUsedAuto(String usedAuto) {
        this.usedAuto = usedAuto;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}