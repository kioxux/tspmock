/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.dto.CfgXdLoanOrgDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgXdLoanOrg;
import cn.com.yusys.yusp.service.CfgXdLoanOrgService;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgXdLoanOrgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-20 20:03:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgxdloanorg")
public class CfgXdLoanOrgResource {
    @Autowired
    private CfgXdLoanOrgService cfgXdLoanOrgService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgXdLoanOrg>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgXdLoanOrg> list = cfgXdLoanOrgService.selectAll(queryModel);
        return new ResultDto<List<CfgXdLoanOrg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgXdLoanOrg>> index(QueryModel queryModel) {
        List<CfgXdLoanOrg> list = cfgXdLoanOrgService.selectByModel(queryModel);
        return new ResultDto<List<CfgXdLoanOrg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{xdOrg}")
    protected ResultDto<CfgXdLoanOrg> show(@PathVariable("xdOrg") String xdOrg) {
        CfgXdLoanOrg cfgXdLoanOrg = cfgXdLoanOrgService.selectByPrimaryKey(xdOrg);
        return new ResultDto<CfgXdLoanOrg>(cfgXdLoanOrg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgXdLoanOrg> create(@RequestBody CfgXdLoanOrg cfgXdLoanOrg) throws URISyntaxException {
        cfgXdLoanOrgService.insert(cfgXdLoanOrg);
        return new ResultDto<CfgXdLoanOrg>(cfgXdLoanOrg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgXdLoanOrg cfgXdLoanOrg) throws URISyntaxException {
        int result = cfgXdLoanOrgService.update(cfgXdLoanOrg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{xdOrg}")
    protected ResultDto<Integer> delete(@PathVariable("xdOrg") String xdOrg) {
        int result = cfgXdLoanOrgService.deleteByPrimaryKey(xdOrg);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgXdLoanOrgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param cfgXdLoanOrgDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CfgXdLoanOrg>
     * @author 王玉坤
     * @date 2021/10/20 20:20
     * @version 1.0.0
     * @desc 根据所属机构查询小贷账务机构信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/querycfgxdloanorg")
    protected ResultDto<CfgXdLoanOrgDto> queryCfgXdLoanOrg(@RequestBody CfgXdLoanOrgDto cfgXdLoanOrgDto) {
        CfgXdLoanOrg cfgXdLoanOrg = cfgXdLoanOrgService.selectByPrimaryKey(cfgXdLoanOrgDto.getXdOrg());
        if (Objects.isNull(cfgXdLoanOrg)) {
            return new ResultDto<CfgXdLoanOrgDto>(null);
        }
        CfgXdLoanOrgDto xdLoanOrgDto = new CfgXdLoanOrgDto();
        BeanUtils.beanCopy(cfgXdLoanOrg, xdLoanOrgDto);
        return new ResultDto<CfgXdLoanOrgDto>(xdLoanOrgDto);
    }
}
