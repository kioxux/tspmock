package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrgElecSeal
 * @类描述: cfg_org_elec_seal数据实体类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-07-12 08:57:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgOrgElecSealDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 机构代码
     **/
    private String orgCode;

    /**
     * 机构名称
     **/
    private String orgName;

    /**
     * 信贷合同章编码
     **/
    private String sealCode;

    /**
     * 信贷合同章名称
     **/
    private String sealName;

    /**
     * 信贷合同章密码
     **/
    private String sealPassword;

    /**
     * 是否启用
     **/
    private String isBegin;

    /**
     * 抵押合同章编码
     **/
    private String gaurSealCode;

    /**
     * 抵押合同章名称
     **/
    private String gaurSealName;

    /**
     * 抵押合同章密码
     **/
    private String gaurSealPassword;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 最后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 创建时间
     **/
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    private java.util.Date updateTime;

    /**
     * @return OrgCode
     */
    public String getOrgCode() {
        return this.orgCode;
    }

    /**
     * @param orgCode
     */
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode == null ? null : orgCode.trim();
    }

    /**
     * @return OrgName
     */
    public String getOrgName() {
        return this.orgName;
    }

    /**
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    /**
     * @return SealCode
     */
    public String getSealCode() {
        return this.sealCode;
    }

    /**
     * @param sealCode
     */
    public void setSealCode(String sealCode) {
        this.sealCode = sealCode == null ? null : sealCode.trim();
    }

    /**
     * @return SealName
     */
    public String getSealName() {
        return this.sealName;
    }

    /**
     * @param sealName
     */
    public void setSealName(String sealName) {
        this.sealName = sealName == null ? null : sealName.trim();
    }

    /**
     * @return SealPassword
     */
    public String getSealPassword() {
        return this.sealPassword;
    }

    /**
     * @param sealPassword
     */
    public void setSealPassword(String sealPassword) {
        this.sealPassword = sealPassword == null ? null : sealPassword.trim();
    }

    /**
     * @return IsBegin
     */
    public String getIsBegin() {
        return this.isBegin;
    }

    /**
     * @param isBegin
     */
    public void setIsBegin(String isBegin) {
        this.isBegin = isBegin == null ? null : isBegin.trim();
    }

    /**
     * @return GaurSealCode
     */
    public String getGaurSealCode() {
        return this.gaurSealCode;
    }

    /**
     * @param gaurSealCode
     */
    public void setGaurSealCode(String gaurSealCode) {
        this.gaurSealCode = gaurSealCode == null ? null : gaurSealCode.trim();
    }

    /**
     * @return GaurSealName
     */
    public String getGaurSealName() {
        return this.gaurSealName;
    }

    /**
     * @param gaurSealName
     */
    public void setGaurSealName(String gaurSealName) {
        this.gaurSealName = gaurSealName == null ? null : gaurSealName.trim();
    }

    /**
     * @return GaurSealPassword
     */
    public String getGaurSealPassword() {
        return this.gaurSealPassword;
    }

    /**
     * @param gaurSealPassword
     */
    public void setGaurSealPassword(String gaurSealPassword) {
        this.gaurSealPassword = gaurSealPassword == null ? null : gaurSealPassword.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return CreateTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return UpdateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }


}