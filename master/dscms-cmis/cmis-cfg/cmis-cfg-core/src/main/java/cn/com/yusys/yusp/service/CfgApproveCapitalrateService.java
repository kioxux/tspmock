/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgApproveCapitalrate;
import cn.com.yusys.yusp.dto.CfgApproveCapitalrateDto;
import cn.com.yusys.yusp.repository.mapper.CfgApproveCapitalrateMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgApproveCapitalrateService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-13 12:35:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgApproveCapitalrateService {

    @Autowired
    private CfgApproveCapitalrateMapper cfgApproveCapitalrateMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgApproveCapitalrate selectByPrimaryKey(String pkId) {
        return cfgApproveCapitalrateMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgApproveCapitalrate> selectAll(QueryModel model) {
        List<CfgApproveCapitalrate> records = (List<CfgApproveCapitalrate>) cfgApproveCapitalrateMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgApproveCapitalrate> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgApproveCapitalrate> list = cfgApproveCapitalrateMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgApproveCapitalrate record) {
        return cfgApproveCapitalrateMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgApproveCapitalrate record) {
        return cfgApproveCapitalrateMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgApproveCapitalrate record) {
        return cfgApproveCapitalrateMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgApproveCapitalrate record) {
        return cfgApproveCapitalrateMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgApproveCapitalrateMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgApproveCapitalrateMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryDutyByRate
     * @方法描述: 根据占用率获取岗位
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgApproveCapitalrateDto queryDutyByRate(String rate) {
        return cfgApproveCapitalrateMapper.queryDutyByRate(rate);
    }

    /**
     * @方法名称: updateStatusByCusType
     * @方法描述: 根据客户类型关联更新历史数据状态为失效
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateStatusByCusType(String rate) {
        return cfgApproveCapitalrateMapper.updateStatusByCusType(rate);
    }

}
