/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.CfgBankInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgBankNoRel;
import cn.com.yusys.yusp.repository.mapper.CfgBankNoRelMapper;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBankNoRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-24 17:40:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgBankNoRelService {

    @Autowired
    private CfgBankNoRelMapper cfgBankNoRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CfgBankNoRel selectByPrimaryKey(String bankNo) {
        return cfgBankNoRelMapper.selectByPrimaryKey(bankNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgBankNoRel> selectAll(QueryModel model) {
        List<CfgBankNoRel> records = (List<CfgBankNoRel>) cfgBankNoRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CfgBankNoRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgBankNoRel> list = cfgBankNoRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CfgBankNoRel record) {
        return cfgBankNoRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CfgBankNoRel record) {
        return cfgBankNoRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CfgBankNoRel record) {
        return cfgBankNoRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CfgBankNoRel record) {
        return cfgBankNoRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String bankNo) {
        return cfgBankNoRelMapper.deleteByPrimaryKey(bankNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgBankNoRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectAllByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgBankNoRel> selectAllByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgBankNoRel> list = cfgBankNoRelMapper.selectAllByModel(model);
        PageHelper.clearPage();
        return list;
    }
    /**
     * 根据行号查上级行号
     *
     * @param bankNo
     * @return
     */
    public String querySuperBankNoByBankNo(String bankNo) {
        return cfgBankNoRelMapper.querySuperBankNoByBankNo(bankNo);
    }

    /**
     * 根据行号查询数据
     * @param bankNo
     * @return
     */
    public CfgBankNoRel selectByBankNo(String bankNo) {
        return cfgBankNoRelMapper.selectByBankNo(bankNo);
    }
}
