package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgExchgRate
 * @类描述: cfg_exchg_rate数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-12 15:29:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgExchgRateDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 原币种  STD_ZB_CUR_TYP
     **/
    private String origiCurType;

    /**
     * 对照币种 STD_ZB_CUR_TYP
     **/
    private String compCurType;

    /**
     * 钞买价
     **/
    private java.math.BigDecimal cshBuyRate;

    /**
     * 中间汇率
     **/
    private java.math.BigDecimal midRemit;

    /**
     * 汇买入汇率
     **/
    private java.math.BigDecimal buyExchgRate;

    /**
     * 汇卖出汇率
     **/
    private java.math.BigDecimal sldExchgRate;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 逻辑删除标识符
     **/
    private String oprType;

    /**
     * 返回标志位
     **/
    private String rtnCode;
    /**
     * 返回信息
     **/
    private String rtnMsg;

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    /**
     * @return PkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    /**
     * @return OrigiCurType
     */
    public String getOrigiCurType() {
        return this.origiCurType;
    }

    /**
     * @param origiCurType
     */
    public void setOrigiCurType(String origiCurType) {
        this.origiCurType = origiCurType == null ? null : origiCurType.trim();
    }

    /**
     * @return CompCurType
     */
    public String getCompCurType() {
        return this.compCurType;
    }

    /**
     * @param compCurType
     */
    public void setCompCurType(String compCurType) {
        this.compCurType = compCurType == null ? null : compCurType.trim();
    }

    /**
     * @return CshBuyRate
     */
    public java.math.BigDecimal getCshBuyRate() {
        return this.cshBuyRate;
    }

    /**
     * @param cshBuyRate
     */
    public void setCshBuyRate(java.math.BigDecimal cshBuyRate) {
        this.cshBuyRate = cshBuyRate;
    }

    /**
     * @return MidRemit
     */
    public java.math.BigDecimal getMidRemit() {
        return this.midRemit;
    }

    /**
     * @param midRemit
     */
    public void setMidRemit(java.math.BigDecimal midRemit) {
        this.midRemit = midRemit;
    }

    /**
     * @return BuyExchgRate
     */
    public java.math.BigDecimal getBuyExchgRate() {
        return this.buyExchgRate;
    }

    /**
     * @param buyExchgRate
     */
    public void setBuyExchgRate(java.math.BigDecimal buyExchgRate) {
        this.buyExchgRate = buyExchgRate;
    }

    /**
     * @return SldExchgRate
     */
    public java.math.BigDecimal getSldExchgRate() {
        return this.sldExchgRate;
    }

    /**
     * @param sldExchgRate
     */
    public void setSldExchgRate(java.math.BigDecimal sldExchgRate) {
        this.sldExchgRate = sldExchgRate;
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }


}