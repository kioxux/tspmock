package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.SftpUtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/SftpUtilResource")
public class SftpUtilResource {

    @Autowired
    private SftpUtilService sftpUtilService;

    /**
     * 上传文件
     *
     * @param paramMap
     * @return
     */
    @PostMapping("/upload")
    protected ResultDto<Boolean> upload(@RequestBody Map<String, String> paramMap) {
        boolean flag = sftpUtilService.upload(paramMap);
        return new ResultDto<>(flag);
    }

}