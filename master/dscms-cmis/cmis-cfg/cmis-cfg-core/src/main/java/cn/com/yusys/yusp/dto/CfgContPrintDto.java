package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgContPrint
 * @类描述: CfgContPrint数据实体类
 * @功能描述:
 * @创建人: cenqs
 * @创建时间: 2021-7-5 15:27:51
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgContPrintDto implements Serializable {
    private static final long serialVersionUID = 1L;
	/**合同类型 1借款合同 2担保合同**/
	private String contType;

    /**
     * 报表英文名称
     **/
    private String suitReportName;

    /**
     * 报表中文名称
     **/
    private String tempName;

	/** 是否电子用印 **/
	private String isESeal;

	/** 合同版面标识 **/
	private String contPageType;

	/** 合同参数 **/
	private String contParams;

	/** 合同号 **/
	private String contNo;

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getSerno() {
		return serno;
	}

	public void setSerno(String serno) {
		this.serno = serno;
	}

	/** 流水号 **/
	private String serno;

    /**
     * @return suitReportName
     */
    public String getSuitReportName() {
        return this.suitReportName;
    }

    /**
     * @param suitReportName
     */
    public void setSuitReportName(String suitReportName) {
        this.suitReportName = suitReportName == null ? null : suitReportName.trim();
    }

    /**
     * @return tempName
     */
    public String getTempName() {
        return this.tempName;
    }

    /**
     * @param tempName
     */
    public void setTempName(String tempName) {
        this.tempName = tempName == null ? null : tempName.trim();
    }

	public String getContParams() {
		return contParams;
	}

	public void setContParams(String contParams) {
		this.contParams = contParams == null ? null : contParams.trim();
	}

	public String getContType() {
		return contType;
	}

	public void setContType(String contType) {
		this.contType = contType == null ? null : contType.trim();
	}

	public String getIsESeal() {
		return isESeal;
	}

	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal == null ? null : isESeal.trim();
	}

	public String getContPageType() {
		return contPageType;
	}

	public void setContPageType(String contPageType) {
		this.contPageType = contPageType == null ? null : contPageType.trim();
	}
}