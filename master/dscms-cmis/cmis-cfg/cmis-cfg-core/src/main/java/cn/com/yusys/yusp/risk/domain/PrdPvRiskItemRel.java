/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.risk.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: yusp-npam-biz-core模块
 * @类名称: PrdPvRiskItemRel
 * @类描述: PRD_PV_RISK_ITEM_REL数据实体类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2020-07-27 17:26:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "PRD_PV_RISK_ITEM_REL")
public class PrdPvRiskItemRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 方案编号
     **/
    @Id
    @Column(name = "PREVENT_ID")
    private String preventId;

    /**
     * 项目编号
     **/
    @Id
    @Column(name = "ITEM_ID")
    private String itemId;

    public PrdPvRiskItemRel(String preventId, String itemId) {
        this.preventId = preventId;
        this.itemId = itemId;
    }

    /**
     * @return preventId
     */
    public String getPreventId() {
        return this.preventId;
    }

    /**
     * @param preventId
     */
    public void setPreventId(String preventId) {
        this.preventId = preventId;
    }

    /**
     * @return itemId
     */
    public String getItemId() {
        return this.itemId;
    }

    /**
     * @param itemId
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }


}