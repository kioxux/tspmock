/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgBizParamInfo;
import cn.com.yusys.yusp.dto.CfgClientParamDto;
import cn.com.yusys.yusp.service.CfgBizParamInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBizParamInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2021-02-04 20:27:29
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgbizparaminfo")
public class CfgBizParamInfoResource {
    @Autowired
    private CfgBizParamInfoService cfgBizParamInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgBizParamInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgBizParamInfo> list = cfgBizParamInfoService.selectAll(queryModel);
        return new ResultDto<List<CfgBizParamInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgBizParamInfo>> index(QueryModel queryModel) {
        List<CfgBizParamInfo> list = cfgBizParamInfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgBizParamInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgBizParamInfo> show(@PathVariable("pkId") String pkId) {
        CfgBizParamInfo cfgBizParamInfo = cfgBizParamInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgBizParamInfo>(cfgBizParamInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgBizParamInfo> create(@RequestBody CfgBizParamInfo cfgBizParamInfo) throws URISyntaxException {
        cfgBizParamInfoService.insert(cfgBizParamInfo);
        return new ResultDto<CfgBizParamInfo>(cfgBizParamInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgBizParamInfo cfgBizParamInfo) throws URISyntaxException {
        int result = cfgBizParamInfoService.updateSelective(cfgBizParamInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgBizParamInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgBizParamInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * cfg服务对外提供参数查询接口
     *
     * @param cfgClientParamDto
     * @return
     */
    @PostMapping("/getCfgBizParamInfoByClient")
    public CfgClientParamDto getCfgBizParamInfoByClient(@RequestBody CfgClientParamDto cfgClientParamDto) {
        return cfgBizParamInfoService.getCfgBizParamInfoByClient(cfgClientParamDto);
    }
}
