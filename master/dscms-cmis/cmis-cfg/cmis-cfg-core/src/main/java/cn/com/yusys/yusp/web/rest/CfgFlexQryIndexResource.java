/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgFlexQryIndex;
import cn.com.yusys.yusp.service.CfgFlexQryIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryIndexResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-28 20:43:19
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgflexqryindex")
public class CfgFlexQryIndexResource {
    @Autowired
    private CfgFlexQryIndexService cfgFlexQryIndexService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgFlexQryIndex>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgFlexQryIndex> list = cfgFlexQryIndexService.selectAll(queryModel);
        return new ResultDto<List<CfgFlexQryIndex>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgFlexQryIndex>> index(QueryModel queryModel) {
        List<CfgFlexQryIndex> list = cfgFlexQryIndexService.selectByModel(queryModel);
        return new ResultDto<List<CfgFlexQryIndex>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{indexCode}")
    protected ResultDto<CfgFlexQryIndex> show(@PathVariable("indexCode") String indexCode) {
        CfgFlexQryIndex cfgFlexQryIndex = cfgFlexQryIndexService.selectByPrimaryKey(indexCode);
        return new ResultDto<CfgFlexQryIndex>(cfgFlexQryIndex);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgFlexQryIndex> create(@RequestBody CfgFlexQryIndex cfgFlexQryIndex) throws URISyntaxException {
        cfgFlexQryIndexService.insert(cfgFlexQryIndex);
        return new ResultDto<CfgFlexQryIndex>(cfgFlexQryIndex);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgFlexQryIndex cfgFlexQryIndex) throws URISyntaxException {
        int result = cfgFlexQryIndexService.update(cfgFlexQryIndex);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{indexCode}")
    protected ResultDto<Integer> delete(@PathVariable("indexCode") String indexCode) {
        int result = cfgFlexQryIndexService.deleteByPrimaryKey(indexCode);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgFlexQryIndexService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicDelete
     * @函数描述:单个对象逻辑删除，同步删除子表关联数据，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicDelete/{indexCode}")
    protected ResultDto<Integer> logicDelete(@PathVariable("indexCode") String indexCode) {
        int result = cfgFlexQryIndexService.logicDelete(indexCode);
        return new ResultDto<Integer>(result);
    }

}
