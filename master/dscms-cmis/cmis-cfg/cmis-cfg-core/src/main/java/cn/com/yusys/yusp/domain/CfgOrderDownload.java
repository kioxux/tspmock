/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrderDownload
 * @类描述: cfg_order_download数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-27 17:08:01
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_order_download")
public class CfgOrderDownload extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 数据源(比如是biz，就在biz自动起个任务，然后接口发的一个记录给cfg)
     **/
    @Column(name = "DATA_SOURCE", unique = false, nullable = false, length = 20)
    private String dataSource;

    /**
     * 预约时间cron
     **/
    @Column(name = "CRON_TIME", unique = false, nullable = false, length = 20)
    private String cronTime;

    /**
     * 预约次数 下载要执行几次
     **/
    @Column(name = "ORDER_NUM", unique = false, nullable = true, length = 5)
    private Integer orderNum;

    /**
     * 下载报表的查询类 编号 或者 其他具体待定
     **/
    @Column(name = "DOWNLOAD_ACTION", unique = false, nullable = true, length = 200)
    private String downloadAction;

    /**
     * 过滤条件 灵活查询的时候可以用用
     **/
    @Column(name = "QUERY_CONS", unique = false, nullable = true, length = 200)
    private String queryCons;

    /**
     * 文件名
     **/
    @Column(name = "FILE_NAME", unique = false, nullable = true, length = 200)
    private String fileName;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 10)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 15)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 状态 1运行中 2未运行
     **/
    @Column(name = "STATUS", unique = false, nullable = true, length = 5)
    private String status;

    /**
     * 导出类型 csv 还是  xlsx
     **/
    @Column(name = "EXPORT_SOUR_TYPE", unique = false, nullable = true, length = 20)
    private String exportSourType;

    /**
     * 关联水印配置id 待定
     **/
    @Column(name = "WATERMARK_ID", unique = false, nullable = true, length = 40)
    private String watermarkId;

    /**
     * 禁止修改字段
     **/
    @Column(name = "ROW_CFG_ID", unique = false, nullable = true, length = 4000)
    private String rowCfgId;

    /**
     * 水印字体大小
     **/
    @Column(name = "WATER_FONT_SIZE", unique = false, nullable = true, length = 3)
    private Short waterFontSize;

    /**
     * 水印内容 1 用户 2 机构 3时间 4自定义文字
     **/
    @Column(name = "WATER_CONTENT", unique = false, nullable = true, length = 10)
    private String waterContent;

    /**
     * 水印文字
     **/
    @Column(name = "WATER_WORDS", unique = false, nullable = true, length = 60)
    private String waterWords;

    private Integer downloadCount;

    public CfgOrderDownload() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return dataSource
     */
    public String getDataSource() {
        return this.dataSource;
    }

    /**
     * @param dataSource
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @return cronTime
     */
    public String getCronTime() {
        return this.cronTime;
    }

    /**
     * @param cronTime
     */
    public void setCronTime(String cronTime) {
        this.cronTime = cronTime;
    }

    /**
     * @return orderNum
     */
    public Integer getOrderNum() {
        return this.orderNum;
    }

    /**
     * @param orderNum
     */
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * @return downloadAction
     */
    public String getDownloadAction() {
        return this.downloadAction;
    }

    /**
     * @param downloadAction
     */
    public void setDownloadAction(String downloadAction) {
        this.downloadAction = downloadAction;
    }

    /**
     * @return queryCons
     */
    public String getQueryCons() {
        return this.queryCons;
    }

    /**
     * @param queryCons
     */
    public void setQueryCons(String queryCons) {
        this.queryCons = queryCons;
    }

    /**
     * @return fileName
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return exportSourType
     */
    public String getExportSourType() {
        return this.exportSourType;
    }

    /**
     * @param exportSourType
     */
    public void setExportSourType(String exportSourType) {
        this.exportSourType = exportSourType;
    }

    /**
     * @return watermarkId
     */
    public String getWatermarkId() {
        return this.watermarkId;
    }

    /**
     * @param watermarkId
     */
    public void setWatermarkId(String watermarkId) {
        this.watermarkId = watermarkId;
    }

    /**
     * @return rowCfgId
     */
    public String getRowCfgId() {
        return this.rowCfgId;
    }

    /**
     * @param rowCfgId
     */
    public void setRowCfgId(String rowCfgId) {
        this.rowCfgId = rowCfgId;
    }

    /**
     * @return waterFontSize
     */
    public Short getWaterFontSize() {
        return this.waterFontSize;
    }

    /**
     * @param waterFontSize
     */
    public void setWaterFontSize(Short waterFontSize) {
        this.waterFontSize = waterFontSize;
    }

    /**
     * @return waterContent
     */
    public String getWaterContent() {
        return this.waterContent;
    }

    /**
     * @param waterContent
     */
    public void setWaterContent(String waterContent) {
        this.waterContent = waterContent;
    }

    /**
     * @return waterWords
     */
    public String getWaterWords() {
        return this.waterWords;
    }

    /**
     * @param waterWords
     */
    public void setWaterWords(String waterWords) {
        this.waterWords = waterWords;
    }

    public Integer getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(Integer downloadCount) {
        this.downloadCount = downloadCount;
    }


}