package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBlacklist
 * @类描述: cfg_blacklist数据实体类
 * @功能描述:
 * @创建人: xuwei
 * @创建时间: 2021-04-29 13:45:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgBlacklistDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 担保方式
     **/
    private String guarWay;

    /**
     * 本期逾期天数 STD_ZB_OVDUE_DAYS
     **/
    private String overdueDay;

    /**
     * 不宜贷款户形式 STD_ZB_BLKLIST_MODAL
     **/
    private String blackModal;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 最后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 操作类型 STD_ZB_OPR_TYPE
     **/
    private String oprType;

    /**
     * @return PkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    /**
     * @return GuarWay
     */
    public String getGuarWay() {
        return this.guarWay;
    }

    /**
     * @param guarWay
     */
    public void setGuarWay(String guarWay) {
        this.guarWay = guarWay == null ? null : guarWay.trim();
    }

    /**
     * @return OverdueDay
     */
    public String getOverdueDay() {
        return this.overdueDay;
    }

    /**
     * @param overdueDay
     */
    public void setOverdueDay(String overdueDay) {
        this.overdueDay = overdueDay == null ? null : overdueDay.trim();
    }

    /**
     * @return BlackModal
     */
    public String getBlackModal() {
        return this.blackModal;
    }

    /**
     * @param blackModal
     */
    public void setBlackModal(String blackModal) {
        this.blackModal = blackModal == null ? null : blackModal.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }


}