/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgFlexQryIndex;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryIndexMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-28 20:43:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CfgFlexQryIndexMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CfgFlexQryIndex selectByPrimaryKey(@Param("indexCode") String indexCode);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgFlexQryIndex> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CfgFlexQryIndex record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CfgFlexQryIndex record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CfgFlexQryIndex record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CfgFlexQryIndex record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("indexCode") String indexCode);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);


    /**
     * @方法名称: updateByIndexCodeOrParentId
     * @方法描述: 根据指标编号或上级指标编号更新数据的操作类型
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByIndexCodeOrParentId(CfgFlexQryIndex record);

    /**
     * @方法名称: getMainCfgFlexQryIndexByQryCode
     * @方法描述: 根据灵活查询流水号查询主指标
     * @参数与返回说明:
     * @算法描述: 无
     */
    CfgFlexQryIndex getMainCfgFlexQryIndexByQryCode(@Param("qryCode") String qryCode);

    /**
     * @return
     * @方法名称: selectCfgFlexQryIndexByQryCode
     * @方法描述: 通过查询报表编号查询子表的参数配置中指标配置编码及指标详情
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgFlexQryIndex> selectCfgFlexQryIndexByQryCode(HashMap<String, String> queryMap);
}