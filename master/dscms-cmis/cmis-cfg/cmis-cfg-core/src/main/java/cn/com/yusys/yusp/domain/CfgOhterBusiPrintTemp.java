/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOhterBusiPrintTemp
 * @类描述: cfg_ohter_busi_print_temp数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 19:33:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_ohter_busi_print_temp")
public class CfgOhterBusiPrintTemp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 模板编号
     **/
    @Column(name = "TEMP_NO", unique = false, nullable = true, length = 40)
    private String tempNo;

    /**
     * 模板名称
     **/
    @Column(name = "TEMP_NAME", unique = false, nullable = true, length = 80)
    private String tempName;

    /**
     * 适用的业务场景
     **/
    @Column(name = "SUIT_GRT_BUSI_SCENE", unique = false, nullable = true, length = 5)
    private String suitGrtBusiScene;

    /**
     * 适用报表全称
     **/
    @Column(name = "SUIT_REPORT_NAME", unique = false, nullable = true, length = 400)
    private String suitReportName;

    /**
     * 版本描述
     **/
    @Column(name = "VER_DEC", unique = false, nullable = true, length = 40)
    private String verDec;

    /**
     * 发布日期
     **/
    @Column(name = "RELEASE_DATE", unique = false, nullable = true, length = 40)
    private String releaseDate;

    /**
     * 模板状态
     **/
    @Column(name = "MUBAN_STATUS", unique = false, nullable = true, length = 5)
    private String mubanStatus;

    /**
     * 操作类型
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public CfgOhterBusiPrintTemp() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return tempNo
     */
    public String getTempNo() {
        return this.tempNo;
    }

    /**
     * @param tempNo
     */
    public void setTempNo(String tempNo) {
        this.tempNo = tempNo;
    }

    /**
     * @return tempName
     */
    public String getTempName() {
        return this.tempName;
    }

    /**
     * @param tempName
     */
    public void setTempName(String tempName) {
        this.tempName = tempName;
    }

    /**
     * @return suitGrtBusiScene
     */
    public String getSuitGrtBusiScene() {
        return this.suitGrtBusiScene;
    }

    /**
     * @param suitGrtBusiScene
     */
    public void setSuitGrtBusiScene(String suitGrtBusiScene) {
        this.suitGrtBusiScene = suitGrtBusiScene;
    }

    /**
     * @return suitReportName
     */
    public String getSuitReportName() {
        return this.suitReportName;
    }

    /**
     * @param suitReportName
     */
    public void setSuitReportName(String suitReportName) {
        this.suitReportName = suitReportName;
    }

    /**
     * @return verDec
     */
    public String getVerDec() {
        return this.verDec;
    }

    /**
     * @param verDec
     */
    public void setVerDec(String verDec) {
        this.verDec = verDec;
    }

    /**
     * @return releaseDate
     */
    public String getReleaseDate() {
        return this.releaseDate;
    }

    /**
     * @param releaseDate
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * @return mubanStatus
     */
    public String getMubanStatus() {
        return this.mubanStatus;
    }

    /**
     * @param mubanStatus
     */
    public void setMubanStatus(String mubanStatus) {
        this.mubanStatus = mubanStatus;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }


}