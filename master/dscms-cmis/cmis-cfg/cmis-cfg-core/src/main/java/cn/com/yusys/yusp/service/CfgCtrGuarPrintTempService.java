/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CfgCtrGuarPrintTemp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CfgCtrGuarPrintTempMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgCtrGuarPrintTempService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 15:05:19
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgCtrGuarPrintTempService {
    private static final Logger log = LoggerFactory.getLogger(CfgDataFlowService.class);

    @Autowired
    private CfgCtrGuarPrintTempMapper cfgCtrGuarPrintTempMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgCtrGuarPrintTemp selectByPrimaryKey(String pkId) {
        return cfgCtrGuarPrintTempMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgCtrGuarPrintTemp> selectAll(QueryModel model) {
        List<CfgCtrGuarPrintTemp> records = (List<CfgCtrGuarPrintTemp>) cfgCtrGuarPrintTempMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgCtrGuarPrintTemp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgCtrGuarPrintTemp> list = cfgCtrGuarPrintTempMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgCtrGuarPrintTemp record) {
        return cfgCtrGuarPrintTempMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgCtrGuarPrintTemp record) {
        return cfgCtrGuarPrintTempMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgCtrGuarPrintTemp record) {
        return cfgCtrGuarPrintTempMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgCtrGuarPrintTemp record) {
        return cfgCtrGuarPrintTempMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgCtrGuarPrintTempMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgCtrGuarPrintTempMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: toSignList
     * @方法描述: 查询列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CfgCtrGuarPrintTemp> toSignList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return cfgCtrGuarPrintTempMapper.selectByModel(model);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int logicDelete(CfgCtrGuarPrintTemp cfgCtrGuarPrintTemp) {
        cfgCtrGuarPrintTemp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return cfgCtrGuarPrintTempMapper.updateByPrimaryKey(cfgCtrGuarPrintTemp);
    }

    /**
     * @方法名称: queryCfgCtrGuarPrintTempDataByParams
     * @方法描述: 根据入参查询数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:45:45
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CfgCtrGuarPrintTemp queryCfgCtrGuarPrintTempDataByParams(Map map) {
        return cfgCtrGuarPrintTempMapper.queryCfgCtrGuarPrintTempDataByParams(map);
    }

    /**
     * 担保合同打印模板配置新增页面点击下一步
     *
     * @param cfgCtrGuarPrintTemp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveCfgCtrGuarPrintTempInfo(CfgCtrGuarPrintTemp cfgCtrGuarPrintTemp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (cfgCtrGuarPrintTemp == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                cfgCtrGuarPrintTemp.setInputId(userInfo.getLoginCode());
                cfgCtrGuarPrintTemp.setInputBrId(userInfo.getOrg().getCode());
                cfgCtrGuarPrintTemp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            Map seqMap = new HashMap();

            String tempNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, seqMap);

            cfgCtrGuarPrintTemp.setTempNo(tempNo);
            cfgCtrGuarPrintTemp.setPkId(StringUtils.uuid(true));
            cfgCtrGuarPrintTemp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);

            int insertCount = cfgCtrGuarPrintTempMapper.insertSelective(cfgCtrGuarPrintTemp);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("tempNo", tempNo);
            log.info("担保合同打印模板配置" + tempNo + "-保存成功！");
        } catch (YuspException e) {
            log.error("担保合同打印模板配置新增保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("保存担保合同打印模板配置信息异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 担保合同打印模板配置通用保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveCfgCtrGuarPrintTempInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String tempNo = "";
        try {
            //获取申请流水号
            tempNo = (String) params.get("tempNo");
            if (StringUtils.isBlank(tempNo)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到担保合同打印模板配置主键信息";
                return result;
            }

            String logPrefix = "担保合同打印模板配置" + tempNo;

            log.info(logPrefix + "获取担保合同打印模板配置数据");
            CfgCtrGuarPrintTemp cfgCtrGuarPrintTemp = JSONObject.parseObject(JSON.toJSONString(params), CfgCtrGuarPrintTemp.class);
            if (cfgCtrGuarPrintTemp == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存担保合同打印模板配置-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                cfgCtrGuarPrintTemp.setUpdId(userInfo.getLoginCode());
                cfgCtrGuarPrintTemp.setUpdBrId(userInfo.getOrg().getCode());
                cfgCtrGuarPrintTemp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(logPrefix + "保存担保合同打印模板配置数据");
            int updCount = cfgCtrGuarPrintTempMapper.updateByPrimaryKeySelective(cfgCtrGuarPrintTemp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存担保合同打印模板配置" + tempNo + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }
}
