/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgSftp;
import cn.com.yusys.yusp.dto.CfgSftpDto;
import cn.com.yusys.yusp.service.CfgSftpService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSftpResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 17:34:48
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgsftp")
public class CfgSftpResource {
    @Autowired
    private CfgSftpService cfgSftpService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgSftp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgSftp> list = cfgSftpService.selectAll(queryModel);
        return new ResultDto<List<CfgSftp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgSftp>> index(QueryModel queryModel) {
        List<CfgSftp> list = cfgSftpService.selectByModel(queryModel);
        return new ResultDto<List<CfgSftp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{host}")
    protected ResultDto<CfgSftp> show(@PathVariable("host") String host) {
        CfgSftp cfgSftp = cfgSftpService.selectByPrimaryKey(host);
        return new ResultDto<CfgSftp>(cfgSftp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgSftp> create(@RequestBody CfgSftp cfgSftp) throws URISyntaxException {
        cfgSftpService.insert(cfgSftp);
        return new ResultDto<CfgSftp>(cfgSftp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgSftp cfgSftp) throws URISyntaxException {
        int result = cfgSftpService.update(cfgSftp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{host}")
    protected ResultDto<Integer> delete(@PathVariable("host") String host) {
        int result = cfgSftpService.deleteByPrimaryKey(host);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgSftpService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据渠道标识查询配置信息
     *
     * @param bizScenseType
     * @return
     */
    @PostMapping("/queryCfgSftpByBizScenseType")
    protected ResultDto<CfgSftpDto> queryCfgSftpByBizScenseType(@RequestBody String bizScenseType) {
        CfgSftpDto cfgSftpDto = new CfgSftpDto();
        CfgSftp cfgSftp = cfgSftpService.queryCfgSftpByBizScenseType(bizScenseType);
        if (null == cfgSftp) {
            cfgSftp = new CfgSftp();
        }
        BeanUtils.copyProperties(cfgSftp, cfgSftpDto);
        return new ResultDto<>(cfgSftpDto);

    }
}
