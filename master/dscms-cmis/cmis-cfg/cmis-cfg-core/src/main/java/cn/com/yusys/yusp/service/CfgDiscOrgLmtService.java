/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgDiscOrgLmt;
import cn.com.yusys.yusp.dto.CfgDiscOrgLmtRespDto;
import cn.com.yusys.yusp.repository.mapper.CfgDiscOrgLmtMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgDiscOrgLmtService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-11 17:38:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgDiscOrgLmtService {

    @Autowired
    private CfgDiscOrgLmtMapper cfgDiscOrgLmtMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgDiscOrgLmt selectByPrimaryKey(String serno) {
        return cfgDiscOrgLmtMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgDiscOrgLmt> selectAll(QueryModel model) {
        List<CfgDiscOrgLmt> records = (List<CfgDiscOrgLmt>) cfgDiscOrgLmtMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgDiscOrgLmt> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgDiscOrgLmt> list = cfgDiscOrgLmtMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgDiscOrgLmt record) {
        return cfgDiscOrgLmtMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgDiscOrgLmt record) {
        return cfgDiscOrgLmtMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgDiscOrgLmt record) {
        return cfgDiscOrgLmtMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgDiscOrgLmt record) {
        return cfgDiscOrgLmtMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cfgDiscOrgLmtMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgDiscOrgLmtMapper.deleteByIds(ids);
    }

    /**
     * 获取配置信息list
     *
     * @param queryMap
     * @return
     */
    public List<CfgDiscOrgLmtRespDto> getListByQueryMap(Map queryMap) {
        return cfgDiscOrgLmtMapper.getListByQueryMap(queryMap);
    }
}
