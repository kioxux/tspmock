/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.CfgBankInfo;
import cn.com.yusys.yusp.domain.CfgBankNoRel;
import cn.com.yusys.yusp.service.CfgBankNoRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBankNoRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-24 17:40:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgbanknorel")
public class CfgBankNoRelResource {
    @Autowired
    private CfgBankNoRelService cfgBankNoRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgBankNoRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgBankNoRel> list = cfgBankNoRelService.selectAll(queryModel);
        return new ResultDto<List<CfgBankNoRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgBankNoRel>> index(QueryModel queryModel) {
        List<CfgBankNoRel> list = cfgBankNoRelService.selectByModel(queryModel);
        return new ResultDto<List<CfgBankNoRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{bankNo}")
    protected ResultDto<CfgBankNoRel> show(@PathVariable("bankNo") String bankNo) {
        CfgBankNoRel cfgBankNoRel = cfgBankNoRelService.selectByPrimaryKey(bankNo);
        return new ResultDto<CfgBankNoRel>(cfgBankNoRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CfgBankNoRel> create(@RequestBody CfgBankNoRel cfgBankNoRel) throws URISyntaxException {
        cfgBankNoRel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        cfgBankNoRelService.insert(cfgBankNoRel);
        return new ResultDto<CfgBankNoRel>(cfgBankNoRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgBankNoRel cfgBankNoRel) throws URISyntaxException {
        int result = cfgBankNoRelService.update(cfgBankNoRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{bankNo}")
    protected ResultDto<Integer> delete(@PathVariable("bankNo") String bankNo) {
        int result = cfgBankNoRelService.deleteByPrimaryKey(bankNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgBankNoRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<CfgBankNoRel>> queryList(@RequestBody QueryModel queryModel) {
        List<CfgBankNoRel> list = cfgBankNoRelService.selectAllByModel(queryModel);
        return new ResultDto<List<CfgBankNoRel>>(list);
    }

}
