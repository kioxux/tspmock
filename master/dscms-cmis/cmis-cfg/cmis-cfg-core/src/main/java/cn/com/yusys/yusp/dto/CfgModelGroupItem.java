package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 模板组主子表关联配置项
 */
public class CfgModelGroupItem implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 模版组编号
     **/
    private String modelGroupNo;

    /**
     * 模板组名称
     **/
    private String modelGroupName;

    /**
     * 模板显示方式 STD_ZB_SHOW_MODE
     **/
    private String showMode;

    /**
     * 版本号
     **/
    private String ver;

    /**
     * 是否关联作业流 STD_ZB_YES_NO
     **/
    private String isJobFlow;

    /**
     * 作业流编号
     **/
    private String jobFlow;

    /**
     * 业务规则方案编号
     **/
    private String planId;

    /**
     * 备注
     **/
    private String remark;

    /**
     * 主键
     **/
    private String pkId;

    private String relType;

    /**
     * 页面ID
     **/
    private String funcId;

    /**
     * 页面名称
     **/
    private String funcName;

    /**
     * 页面URL
     **/
    private String funcUrl;

    /**
     * 是否主页面 STD_ZB_YES_NO
     **/
    private String isMainFunc;

    /**
     * 页面显示顺序
     **/
    private Integer seqNo;

    /**
     * 从页面显示条件
     **/
    private String showCond;

    /**
     * 从页面过滤条件
     **/
    private String filterCond;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 更新人
     **/
    private String updId;

    /**
     * 更新机构
     **/
    private String updBrId;

    /**
     * 更新日期
     **/
    private String updDate;

    public String getRelType() {
        return relType;
    }

    public void setRelType(String relType) {
        this.relType = relType;
    }

    public String getModelGroupNo() {
        return modelGroupNo;
    }

    public void setModelGroupNo(String modelGroupNo) {
        this.modelGroupNo = modelGroupNo;
    }

    public String getModelGroupName() {
        return modelGroupName;
    }

    public void setModelGroupName(String modelGroupName) {
        this.modelGroupName = modelGroupName;
    }

    public String getShowMode() {
        return showMode;
    }

    public void setShowMode(String showMode) {
        this.showMode = showMode;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getIsJobFlow() {
        return isJobFlow;
    }

    public void setIsJobFlow(String isJobFlow) {
        this.isJobFlow = isJobFlow;
    }

    public String getJobFlow() {
        return jobFlow;
    }

    public void setJobFlow(String jobFlow) {
        this.jobFlow = jobFlow;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getFuncId() {
        return funcId;
    }

    public void setFuncId(String funcId) {
        this.funcId = funcId;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public String getFuncUrl() {
        return funcUrl;
    }

    public void setFuncUrl(String funcUrl) {
        this.funcUrl = funcUrl;
    }

    public String getIsMainFunc() {
        return isMainFunc;
    }

    public void setIsMainFunc(String isMainFunc) {
        this.isMainFunc = isMainFunc;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public String getShowCond() {
        return showCond;
    }

    public void setShowCond(String showCond) {
        this.showCond = showCond;
    }

    public String getFilterCond() {
        return filterCond;
    }

    public void setFilterCond(String filterCond) {
        this.filterCond = filterCond;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }
}
