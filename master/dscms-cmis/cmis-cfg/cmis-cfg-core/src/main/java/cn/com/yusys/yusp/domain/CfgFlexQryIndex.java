/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryIndex
 * @类描述: cfg_flex_qry_index数据实体类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-28 20:43:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_flex_qry_index")
public class CfgFlexQryIndex extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 指标配置编码
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "INDEX_CODE")
    private String indexCode;

    /**
     * 创建类型 STD_ZB_CREATE_TYPE
     **/
    @Column(name = "CREATE_TYPE", unique = false, nullable = true, length = 5)
    private String createType;

    /**
     * 父级对象
     **/
    @Column(name = "PARENT_OBJ", unique = false, nullable = true, length = 40)
    private String parentObj;

    /**
     * 对象名称/分组名称
     **/
    @Column(name = "OBJ_GROUP_NAME", unique = false, nullable = true, length = 200)
    private String objGroupName;

    /**
     * 排序
     **/
    @Column(name = "SORT", unique = false, nullable = true, length = 6)
    private java.math.BigDecimal sort;

    /**
     * 数据源
     **/
    @Column(name = "DATASOURCE", unique = false, nullable = true, length = 20)
    private String datasource;

    /**
     * 来源表
     **/
    @Column(name = "TABLE_SOUR", unique = false, nullable = true, length = 100)
    private String tableSour;

    /**
     * 表别名
     **/
    @Column(name = "ALIAS", unique = false, nullable = true, length = 20)
    private String alias;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgFlexQryIndex() {
        // Not compliant
    }

    /**
     * @return indexCode
     */
    public String getIndexCode() {
        return this.indexCode;
    }

    /**
     * @param indexCode
     */
    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

    /**
     * @return createType
     */
    public String getCreateType() {
        return this.createType;
    }

    /**
     * @param createType
     */
    public void setCreateType(String createType) {
        this.createType = createType;
    }

    /**
     * @return parentObj
     */
    public String getParentObj() {
        return this.parentObj;
    }

    /**
     * @param parentObj
     */
    public void setParentObj(String parentObj) {
        this.parentObj = parentObj;
    }

    /**
     * @return objGroupName
     */
    public String getObjGroupName() {
        return this.objGroupName;
    }

    /**
     * @param objGroupName
     */
    public void setObjGroupName(String objGroupName) {
        this.objGroupName = objGroupName;
    }

    /**
     * @return sort
     */
    public java.math.BigDecimal getSort() {
        return this.sort;
    }

    /**
     * @param sort
     */
    public void setSort(java.math.BigDecimal sort) {
        this.sort = sort;
    }

    /**
     * @return datasource
     */
    public String getDatasource() {
        return this.datasource;
    }

    /**
     * @param datasource
     */
    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    /**
     * @return tableSour
     */
    public String getTableSour() {
        return this.tableSour;
    }

    /**
     * @param tableSour
     */
    public void setTableSour(String tableSour) {
        this.tableSour = tableSour;
    }

    /**
     * @return alias
     */
    public String getAlias() {
        return this.alias;
    }

    /**
     * @param alias
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}