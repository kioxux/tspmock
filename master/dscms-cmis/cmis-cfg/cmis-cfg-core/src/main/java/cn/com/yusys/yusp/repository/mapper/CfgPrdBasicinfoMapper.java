/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgPrdBasicinfo;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdBasicinfoMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-07 11:27:07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CfgPrdBasicinfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CfgPrdBasicinfo selectByPrimaryKey(@Param("prdId") String prdId);

    /**
     * @方法名称: selectByplanId
     * @方法描述: 根据业务规则方案编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgPrdBasicinfo> selectByPlanId(@Param("planId") String planId, @Param("oprType") String oprType);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgPrdBasicinfoDto> selectByModel(QueryModel model);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgPrdBasicinfo> selectByModelToDomain(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CfgPrdBasicinfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CfgPrdBasicinfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CfgPrdBasicinfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CfgPrdBasicinfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("prdId") String prdId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 通过入参信息查询数据
     *
     * @param queryMap
     * @return
     */
    List<CfgPrdBasicinfo> selectByParams(Map queryMap);

    /**
     * @param prdId:字典码值
     * @Description:提供给biz服务接口XDTZ0056使用
     * @Author: YX-WJ
     * @Date: 2021/6/5 15:50
     * @return: int
     **/
    int queryContInfoByEnName4Biz(@Param("prdId") String prdId);

    /**
     * 根据产品目录层级catalogid查询产品列表
     *
     * @param catalogId
     * @return
     */
    List<CfgPrdBasicinfo> queryBasicInfoByCatalogId(@Param("catalogId") String catalogId);

    /**
     * 根据产品类别查询所有的产品信息
     *
     * @param prdType
     * @return
     */
    List<CfgPrdBasicinfo> queryCfgPrdBasicInfoByPrdType(@Param("prdType") String prdType);

    /**
     * 根据目录层级查询产品信息
     *
     * @param cataloglevelName
     * @return
     */
    List<CfgPrdBasicinfo> queryBasicInfoByCatalog(@Param("cataloglevelName") String cataloglevelName);

    /**
     * @方法名称: selectCfgPrdBasicinfoDG
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgPrdBasicinfoDto> selectCfgPrdBasicinfoDG(QueryModel model);

    /**
     * @方法名称: selectCfgPrdBasicinfoGR
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgPrdBasicinfoDto> selectCfgPrdBasicinfoGR(QueryModel model);
}
