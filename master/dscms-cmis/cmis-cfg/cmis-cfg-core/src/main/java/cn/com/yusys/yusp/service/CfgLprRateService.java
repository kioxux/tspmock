/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgLprRate;
import cn.com.yusys.yusp.dto.CfgLprRateDto;
import cn.com.yusys.yusp.repository.mapper.CfgLprRateMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgLprRateService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-15 18:51:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgLprRateService {

    @Autowired
    private CfgLprRateMapper cfgLprRateMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgLprRate selectByPrimaryKey(String pkId) {
        return cfgLprRateMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgLprRate> selectAll(QueryModel model) {
        List<CfgLprRate> records = (List<CfgLprRate>) cfgLprRateMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgLprRate> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgLprRate> list = cfgLprRateMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgLprRate record) {
        return cfgLprRateMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgLprRate record) {
        return cfgLprRateMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgLprRate record) {
        return cfgLprRateMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgLprRate record) {
        return cfgLprRateMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgLprRateMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgLprRateMapper.deleteByIds(ids);
    }


    /**
     * @创建人 WH
     * @创建时间 2021/6/15 18:54
     * @注释 增加LPR利率查询接口
     */
    public ResultDto selectlpr(CfgLprRate cfgLprRate) {
        if (cfgLprRate == null || cfgLprRate.getRateTypeName() == null || cfgLprRate.getRateTypeName().isEmpty()) {
            return new ResultDto(null).code(9999).message("利率类型为空");
        }
        List list = cfgLprRateMapper.selectlpr(cfgLprRate);
        return new ResultDto(list);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/16 11:26
     * @注释 查询单条
     */
    public ResultDto<CfgLprRateDto> selectone(QueryModel queryModel) {
        CfgLprRateDto selectone = cfgLprRateMapper.selectone(queryModel);
        return new ResultDto<>(selectone);
    }


    /**
     * @创建人 shenli
     * @创建时间 2021-9-17 17:21:44
     * @注释 根据签订日期找之后最近的lpr
     */
    public ResultDto<CfgLprRateDto> selectAfterLpr(CfgLprRateDto cfgLprRateDto) {
        return new ResultDto<>(cfgLprRateMapper.selectAfterLpr(cfgLprRateDto));
    }

    /**
     * @创建人 shenli
     * @创建时间 2021-9-17 17:21:44
     * @注释 根据签订日期找之前最近的lpr
     */
    public ResultDto<CfgLprRateDto> selectFrontLpr(CfgLprRateDto cfgLprRateDto) {
        return new ResultDto<>(cfgLprRateMapper.selectFrontLpr(cfgLprRateDto));
    }

}
