/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgContModeBookmark
 * @类描述: cfg_cont_mode_bookmark数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-17 17:32:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_cont_mode_bookmark")
public class CfgContModeBookmark extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 合同模板编号
     **/
    @Column(name = "CONT_TPL_CODE", unique = false, nullable = false, length = 40)
    private String contTplCode;

    /**
     * 书签名称
     **/
    @Column(name = "BOOKMARK_NAME", unique = false, nullable = true, length = 40)
    private String bookmarkName;

    /**
     * 数据字段
     **/
    @Column(name = "DATA_COL", unique = false, nullable = true, length = 40)
    private String dataCol;

    /**
     * 是否可编辑 STD_ZB_YES_NO
     **/
    @Column(name = "IS_EDIT", unique = false, nullable = true, length = 5)
    private String isEdit;

    /**
     * 描述
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 400)
    private String memo;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgContModeBookmark() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return contTplCode
     */
    public String getContTplCode() {
        return this.contTplCode;
    }

    /**
     * @param contTplCode
     */
    public void setContTplCode(String contTplCode) {
        this.contTplCode = contTplCode;
    }

    /**
     * @return bookmarkName
     */
    public String getBookmarkName() {
        return this.bookmarkName;
    }

    /**
     * @param bookmarkName
     */
    public void setBookmarkName(String bookmarkName) {
        this.bookmarkName = bookmarkName;
    }

    /**
     * @return dataCol
     */
    public String getDataCol() {
        return this.dataCol;
    }

    /**
     * @param dataCol
     */
    public void setDataCol(String dataCol) {
        this.dataCol = dataCol;
    }

    /**
     * @return isEdit
     */
    public String getIsEdit() {
        return this.isEdit;
    }

    /**
     * @param isEdit
     */
    public void setIsEdit(String isEdit) {
        this.isEdit = isEdit;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}