package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CfgBizRulePlan;
import cn.com.yusys.yusp.domain.CfgRuleItem;

import java.io.Serializable;
import java.util.List;

public class CfgBizRulePlanDto implements Serializable {
    private CfgBizRulePlan cfgBizRulePlan;

    private List<CfgRuleItem> ruleItems;

    public CfgBizRulePlan getCfgBizRulePlan() {
        return cfgBizRulePlan;
    }

    public void setCfgBizRulePlan(CfgBizRulePlan cfgBizRulePlan) {
        this.cfgBizRulePlan = cfgBizRulePlan;
    }

    public List<CfgRuleItem> getRuleItems() {
        return ruleItems;
    }

    public void setRuleItems(List<CfgRuleItem> ruleItems) {
        this.ruleItems = ruleItems;
    }
}
