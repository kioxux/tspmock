/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgChnlMana
 * @类描述: cfg_chnl_mana数据实体类
 * @功能描述:
 * @创建人: qhy
 * @创建时间: 2021-05-08 09:32:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_chnl_mana")
public class CfgChnlMana extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 渠道代码
     **/
    @Column(name = "CHNL_CODE", unique = false, nullable = true, length = 40)
    private String chnlCode;

    /**
     * 渠道名称
     **/
    @Column(name = "CHNL_NAME", unique = false, nullable = true, length = 80)
    private String chnlName;

    /**
     * 渠道类型 STD_ZB_CHNL_TYPE
     **/
    @Column(name = "CHNL_TYPE", unique = false, nullable = true, length = 5)
    private String chnlType;

    /**
     * 渠道性质 STD_ZB_CHNL_CHA
     **/
    @Column(name = "CHNL_CHA", unique = false, nullable = true, length = 5)
    private String chnlCha;

    /**
     * 备注
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 200)
    private String remark;

    /**
     * 登记人
     **/
    @RedisCacheTranslator(redisCacheKey = "userName")
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @RedisCacheTranslator(redisCacheKey = "orgName")
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgChnlMana() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return chnlCode
     */
    public String getChnlCode() {
        return this.chnlCode;
    }

    /**
     * @param chnlCode
     */
    public void setChnlCode(String chnlCode) {
        this.chnlCode = chnlCode;
    }

    /**
     * @return chnlName
     */
    public String getChnlName() {
        return this.chnlName;
    }

    /**
     * @param chnlName
     */
    public void setChnlName(String chnlName) {
        this.chnlName = chnlName;
    }

    /**
     * @return chnlType
     */
    public String getChnlType() {
        return this.chnlType;
    }

    /**
     * @param chnlType
     */
    public void setChnlType(String chnlType) {
        this.chnlType = chnlType;
    }

    /**
     * @return chnlCha
     */
    public String getChnlCha() {
        return this.chnlCha;
    }

    /**
     * @param chnlCha
     */
    public void setChnlCha(String chnlCha) {
        this.chnlCha = chnlCha;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}