/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CfgCtrContPrintTemp;
import cn.com.yusys.yusp.domain.CfgCtrGuarPrintTemp;
import cn.com.yusys.yusp.dto.CfgContPrintDto;
import cn.com.yusys.yusp.dto.CfgCtrContPrintReqDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CfgCtrContPrintTempMapper;
import cn.com.yusys.yusp.repository.mapper.CfgCtrGuarPrintTempMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgCtrContPrintTempService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-16 21:05:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgCtrContPrintTempService {
    private static final Logger log = LoggerFactory.getLogger(CfgDataFlowService.class);

    @Autowired
    private CfgCtrContPrintTempMapper cfgCtrContPrintTempMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private CfgCtrGuarPrintTempMapper cfgCtrGuarPrintTempMapper;//担保合同打印模板配置

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgCtrContPrintTemp selectByPrimaryKey(String pkId) {
        return cfgCtrContPrintTempMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgCtrContPrintTemp> selectAll(QueryModel model) {
        List<CfgCtrContPrintTemp> records = (List<CfgCtrContPrintTemp>) cfgCtrContPrintTempMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgCtrContPrintTemp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgCtrContPrintTemp> list = cfgCtrContPrintTempMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgCtrContPrintTemp record) {
        return cfgCtrContPrintTempMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgCtrContPrintTemp record) {
        return cfgCtrContPrintTempMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgCtrContPrintTemp record) {
        return cfgCtrContPrintTempMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgCtrContPrintTemp record) {
        return cfgCtrContPrintTempMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgCtrContPrintTempMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgCtrContPrintTempMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: toSignList
     * @方法描述: 查询列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CfgCtrContPrintTemp> toSignList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return cfgCtrContPrintTempMapper.selectByModel(model);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int logicDelete(CfgCtrContPrintTemp cfgCtrContPrintTemp) {
        cfgCtrContPrintTemp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return cfgCtrContPrintTempMapper.updateByPrimaryKey(cfgCtrContPrintTemp);
    }

    /**
     * @方法名称: queryCfgCtrContPrintTempDataByParams
     * @方法描述: 根据入参查询数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:45:45
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CfgCtrContPrintTemp queryCfgCtrContPrintTempDataByParams(Map map) {
        return cfgCtrContPrintTempMapper.queryCfgCtrContPrintTempDataByParams(map);
    }

    /**
     * @方法名称: querySuitReportNameInfo
     * @方法描述: 根据入参查询数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:45:45
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String querySuitReportName(CfgCtrContPrintReqDto cfgCtrContPrintReqDto) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("suitContType",cfgCtrContPrintReqDto.getSuitContType());
        queryModel.addCondition("suitPrd",cfgCtrContPrintReqDto.getSuitPrd());
        String suitReportName = cfgCtrContPrintTempMapper.querySuitReportName(queryModel);
        return suitReportName;
    }

    /**
     * 主合同打印模板配置新增页面点击下一步
     *
     * @param cfgCtrContPrintTemp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveCfgCtrContPrintTempInfo(CfgCtrContPrintTemp cfgCtrContPrintTemp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (cfgCtrContPrintTemp == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                cfgCtrContPrintTemp.setInputId(userInfo.getLoginCode());
                cfgCtrContPrintTemp.setInputBrId(userInfo.getOrg().getCode());
                cfgCtrContPrintTemp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            Map seqMap = new HashMap();

            String tempNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, seqMap);

            cfgCtrContPrintTemp.setTempNo(tempNo);
            cfgCtrContPrintTemp.setPkId(StringUtils.uuid(true));
            cfgCtrContPrintTemp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);

            int insertCount = cfgCtrContPrintTempMapper.insertSelective(cfgCtrContPrintTemp);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("tempNo", tempNo);
            log.info("主合同打印模板配置" + tempNo + "-保存成功！");
        } catch (YuspException e) {
            log.error("主合同打印模板配置新增保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("保存主合同打印模板配置信息异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 贴现申请提交保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveCfgCtrContPrintTempInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String tempNo = "";
        try {
            //获取申请流水号
            tempNo = (String) params.get("tempNo");
            if (StringUtils.isBlank(tempNo)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "贴现申请" + tempNo;

            log.info(logPrefix + "获取申请数据");
            CfgCtrContPrintTemp cfgCtrContPrintTemp = JSONObject.parseObject(JSON.toJSONString(params), CfgCtrContPrintTemp.class);
            if (cfgCtrContPrintTemp == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存主合同打印模板配置-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                cfgCtrContPrintTemp.setUpdId(userInfo.getLoginCode());
                cfgCtrContPrintTemp.setUpdBrId(userInfo.getOrg().getCode());
                cfgCtrContPrintTemp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(logPrefix + "保存主合同打印模板配置数据");
            int updCount = cfgCtrContPrintTempMapper.updateByPrimaryKeySelective(cfgCtrContPrintTemp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存主合同打印模板配置" + tempNo + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 查询借款合同与担保合同方法
     *
     * @param queryModel
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public List<CfgContPrintDto> queryPrintContAndGuar(QueryModel queryModel) {
        Map<String, Object> condition = queryModel.getCondition();
        //获取前台查询参数
        String str = (String) condition.get("list");
        List queryList = JSON.parseArray(str);
        List<CfgContPrintDto> returnList = new ArrayList<>();
        if (queryList != null && queryList.size() > 0) {
            for (int i = 0; i < queryList.size(); i++) {
                //查询参数重新封装
                Map map = (Map) queryList.get(i);
                condition.clear();
                condition.putAll(map);
                if ("1".equals(map.get("contType"))) {//借款合同
                    //先查询主合同信息数据
                    List<CfgCtrContPrintTemp> contList = cfgCtrContPrintTempMapper.queryContPrintCfgByPrintParams(queryModel);
                    for (CfgCtrContPrintTemp cont : contList) {
                        //将数据装载进dto类
                        CfgContPrintDto printDto = new CfgContPrintDto();
                        BeanUtils.beanCopy(cont, printDto);
                        //将查询参数的合同号或流水号装载进报表参数中
                        printDto.setContType("1");
                        // 报表参数
                        String contParams = "";
                        if (map.containsKey("contNo")) {
                            contParams += "&contNo=" + (String) map.get("contNo");
                        }
                        if (map.containsKey("serno")) {
                            contParams += "&serno=" + (String) map.get("serno");
                        }
                        printDto.setContParams(contParams);
                        printDto.setContNo((String) map.get("contNo"));
                        printDto.setSerno((String) map.get("serno"));
                        returnList.add(printDto);
                    }
                } else if ("2".equals(map.get("contType"))) {
                    //其次查询担保合同信息数据
                    List<CfgCtrGuarPrintTemp> guarList = cfgCtrGuarPrintTempMapper.queryGuarPrintCfgByPrintParams(queryModel);
                    for (CfgCtrGuarPrintTemp guar : guarList) {
                        //将数据装载进dto类
                        CfgContPrintDto printDto = new CfgContPrintDto();
                        BeanUtils.beanCopy(guar, printDto);
                        //将查询参数的合同号或流水号装载进报表参数中
                        printDto.setContType("2");
                        // 报表参数
                        String contParams = "";
                        if (map.containsKey("contNo")) { //借款合同号
                            contParams += "&contNo=" + (String) map.get("contNo");
                        }
                        if (map.containsKey("serno")) { //借款合同流水号
                            contParams += "&serno=" + (String) map.get("serno");
                        }
                        if (map.containsKey("guarContNo")) { //担保合同号
                            contParams += "&guarContNo=" + (String) map.get("guarContNo");
                        }
                        if (map.containsKey("guarSerno")) { // 担保合同流水号
                            contParams += "&guarSerno=" + (String) map.get("guarSerno");
                        }
                        printDto.setContNo((String) map.get("guarContNo"));
                        printDto.setSerno((String) map.get("serno"));
                        printDto.setContParams(contParams);
                        returnList.add(printDto);
                    }
                }
            }
        }
        return returnList;
    }
}
