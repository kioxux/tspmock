package cn.com.yusys.yusp.service.server.cmiscfg0002;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CfgPrdTypeProperties;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.cmiscfg0002.req.CmisCfg0002ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002ListRespDto;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002RespDto;
import cn.com.yusys.yusp.service.CfgPrdTypePropertiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg模块
 * @类名称: CmisCfg0002Service
 * @类描述: #对内服务类
 * @功能描述: 根据产品编号获取产品属性
 * @创建时间: 2021-07-14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisCfg0002Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisCfg0002Service.class);

    @Autowired
    private CfgPrdTypePropertiesService cfgPrdTypePropertiesService;

    @Transactional
    public CmisCfg0002RespDto execute(CmisCfg0002ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.value);
        CmisCfg0002RespDto respDto = new CmisCfg0002RespDto();
        try {

            QueryModel model = new QueryModel();
            model.addCondition("prdId", reqDto.getPrdId());
            List<CfgPrdTypeProperties> list = cfgPrdTypePropertiesService.selectAll(model);
            List<CmisCfg0002ListRespDto> cmisCfg0042ListRespDtoList = new ArrayList<CmisCfg0002ListRespDto>();

            if (list != null && list.size() > 0) {
                for (CfgPrdTypeProperties cfg : list) {
                    CmisCfg0002ListRespDto cmisCfg0002ListRespDto = new CmisCfg0002ListRespDto();
                    BeanUtils.copyProperties(cfg, cmisCfg0002ListRespDto);
                    cmisCfg0042ListRespDtoList.add(cmisCfg0002ListRespDto);
                }
            }
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            respDto.setCfgPrdTypePropertiesList(cmisCfg0042ListRespDtoList);
        } catch (YuspException e) {
            logger.error(DscmsCfgEnum.TRADE_CODE_CMISCFG0002.value, e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.value);
        return respDto;
    }

}


