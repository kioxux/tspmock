/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.risk.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: yusp-npam-biz-core模块
 * @类名称: PrdPvRiskScene
 * @类描述: PRD_PV_RISK_SCENE数据实体类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2020-07-27 17:26:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "PRD_PV_RISK_SCENE")
public class PrdPvRiskScene extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 流程节点
     **/
    @Id
    @Column(name = "SCENE_ID")
    private String sceneId;

    /**
     * 方案编号
     **/
    @Id
    @Column(name = "PREVENT_ID")
    private String preventId;

    /**
     * 项目编号
     **/
    @Id
    @Column(name = "ITEM_ID")
    private String itemId;

    /**
     * 适用物理业务类型
     **/
    @Id
    @Column(name = "BIZ_TYPE")
    private String bizType;

    /**
     * 业务类型描述
     **/
    @Column(name = "BIZ_TYPE_DESC", unique = false, nullable = true, length = 255)
    private String bizTypeDesc;

    /**
     * 流程编号
     **/
    @Column(name = "FLOW_ID", unique = false, nullable = true, length = 32)
    private String flowId;

    /**
     * 流程名称
     **/
    @Column(name = "FLOW_NAME", unique = false, nullable = true, length = 255)
    private String flowName;

    /**
     * 拦截类型 强制/预警/提示
     **/
    @Column(name = "RISK_LEVEL", unique = false, nullable = true, length = 1)
    private String riskLevel;

    /**
     * 项目名称
     **/
    @Column(name = "ITEM_NAME", unique = false, nullable = true, length = 500)
    private String itemName;

    /**
     * @return sceneId
     */
    public String getSceneId() {
        return this.sceneId;
    }

    /**
     * @param sceneId
     */
    public void setSceneId(String sceneId) {
        this.sceneId = sceneId;
    }

    /**
     * @return preventId
     */
    public String getPreventId() {
        return this.preventId;
    }

    /**
     * @param preventId
     */
    public void setPreventId(String preventId) {
        this.preventId = preventId;
    }

    /**
     * @return itemId
     */
    public String getItemId() {
        return this.itemId;
    }

    /**
     * @param itemId
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * @return bizType
     */
    public String getBizType() {
        return this.bizType;
    }

    /**
     * @param bizType
     */
    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    /**
     * @return bizTypeDesc
     */
    public String getBizTypeDesc() {
        return this.bizTypeDesc;
    }

    /**
     * @param bizTypeDesc
     */
    public void setBizTypeDesc(String bizTypeDesc) {
        this.bizTypeDesc = bizTypeDesc;
    }

    /**
     * @return flowId
     */
    public String getFlowId() {
        return this.flowId;
    }

    /**
     * @param flowId
     */
    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    /**
     * @return flowName
     */
    public String getFlowName() {
        return this.flowName;
    }

    /**
     * @param flowName
     */
    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    /**
     * @return riskLevel
     */
    public String getRiskLevel() {
        return this.riskLevel;
    }

    /**
     * @param riskLevel
     */
    public void setRiskLevel(String riskLevel) {
        this.riskLevel = riskLevel;
    }

    /**
     * @return itemName
     */
    public String getItemName() {
        return this.itemName;
    }

    /**
     * @param itemName
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }


}