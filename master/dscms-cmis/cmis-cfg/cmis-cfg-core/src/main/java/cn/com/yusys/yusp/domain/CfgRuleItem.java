/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgRuleItem
 * @类描述: cfg_rule_item数据实体类
 * @功能描述:
 * @创建人: liuch
 * @创建时间: 2020-12-23 11:51:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_rule_item")
public class CfgRuleItem extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 规则项目编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "RULE_ITEM_ID")
    private String ruleItemId;

    /**
     * 规则项目名称
     **/
    @Column(name = "RULE_ITEM_NAME", unique = false, nullable = true, length = 100)
    private String ruleItemName;

    /**
     * 检查说明
     **/
    @Column(name = "RULE_ITEM_DESC", unique = false, nullable = true, length = 200)
    private String ruleItemDesc;

    /**
     * 拦截类型 STD_ZB_NOTI_TYPE
     **/
    @Column(name = "NOTI_TYPE", unique = false, nullable = true, length = 5)
    private String notiType;

    /**
     * 业务规则类型 STD_ZB_RULE_ITEM_TYPE
     **/
    @Column(name = "RULE_ITEM_TYPE", unique = false, nullable = true, length = 5)
    private String ruleItemType;

    /**
     * 规则编号
     **/
    @Column(name = "RULE_ID", unique = false, nullable = true, length = 40)
    private String ruleId;

    /**
     * 规则集编码
     **/
    @Column(name = "RULE_SET_ID", unique = false, nullable = true, length = 40)
    private String ruleSetId;

    /**
     * 扩展类路径
     **/
    @Column(name = "ITEM_CLASS", unique = false, nullable = true, length = 100)
    private String itemClass;

    /**
     * 外部链接实现类
     **/
    @Column(name = "LINK_URL", unique = false, nullable = true, length = 100)
    private String linkUrl;

    /**
     * 是否启用 STD_ZB_YES_NO
     **/
    @Column(name = "USED_IND", unique = false, nullable = true, length = 5)
    private String usedInd;

    /**
     * 备注
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 400)
    private String remark;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgRuleItem() {
        // Not compliant
    }

    /**
     * @return ruleItemId
     */
    public String getRuleItemId() {
        return this.ruleItemId;
    }

    /**
     * @param ruleItemId
     */
    public void setRuleItemId(String ruleItemId) {
        this.ruleItemId = ruleItemId;
    }

    /**
     * @return ruleItemName
     */
    public String getRuleItemName() {
        return this.ruleItemName;
    }

    /**
     * @param ruleItemName
     */
    public void setRuleItemName(String ruleItemName) {
        this.ruleItemName = ruleItemName;
    }

    /**
     * @return ruleItemDesc
     */
    public String getRuleItemDesc() {
        return this.ruleItemDesc;
    }

    /**
     * @param ruleItemDesc
     */
    public void setRuleItemDesc(String ruleItemDesc) {
        this.ruleItemDesc = ruleItemDesc;
    }

    /**
     * @return notiType
     */
    public String getNotiType() {
        return this.notiType;
    }

    /**
     * @param notiType
     */
    public void setNotiType(String notiType) {
        this.notiType = notiType;
    }

    /**
     * @return ruleItemType
     */
    public String getRuleItemType() {
        return this.ruleItemType;
    }

    /**
     * @param ruleItemType
     */
    public void setRuleItemType(String ruleItemType) {
        this.ruleItemType = ruleItemType;
    }

    /**
     * @return ruleId
     */
    public String getRuleId() {
        return this.ruleId;
    }

    /**
     * @param ruleId
     */
    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    /**
     * @return ruleSetId
     */
    public String getRuleSetId() {
        return this.ruleSetId;
    }

    /**
     * @param ruleSetId
     */
    public void setRuleSetId(String ruleSetId) {
        this.ruleSetId = ruleSetId;
    }

    /**
     * @return itemClass
     */
    public String getItemClass() {
        return this.itemClass;
    }

    /**
     * @param itemClass
     */
    public void setItemClass(String itemClass) {
        this.itemClass = itemClass;
    }

    /**
     * @return linkUrl
     */
    public String getLinkUrl() {
        return this.linkUrl;
    }

    /**
     * @param linkUrl
     */
    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    /**
     * @return usedInd
     */
    public String getUsedInd() {
        return this.usedInd;
    }

    /**
     * @param usedInd
     */
    public void setUsedInd(String usedInd) {
        this.usedInd = usedInd;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}