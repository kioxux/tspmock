/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFileReport
 * @类描述: cfg_file_report数据实体类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2021-05-08 19:42:03
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_file_report")
public class CfgFileReport extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 模板编号
     **/
    @Column(name = "MODE_CODE", unique = false, nullable = false, length = 40)
    private String modeCode;

    /**
     * 模板名称
     **/
    @Column(name = "MODE_NAME", unique = false, nullable = true, length = 100)
    private String modeName;

    /**
     * 文件路径
     **/
    @Column(name = "FILE_PATH", unique = false, nullable = true, length = 100)
    private String filePath;

    /**
     * 文件大小
     **/
    @Column(name = "FILE_SIZE", unique = false, nullable = true, length = 20)
    private String fileSize;

    /**
     * 文件日期
     **/
    @Column(name = "FILE_DATE", unique = false, nullable = true, length = 20)
    private String fileDate;

    /**
     * 模板版本号
     **/
    @Column(name = "VER", unique = false, nullable = true, length = 5)
    private String ver;

    /**
     * 模板状态 STD_ZB_PRD_ST
     **/
    @Column(name = "STATUS", unique = false, nullable = true, length = 5)
    private String status;

    /**
     * 报告类型 STD_ZB_FILE_REP_TYPE
     **/
    @Column(name = "FILE_REP_TYPE", unique = false, nullable = true, length = 2)
    private String fileRepType;

    /**
     * 文件编号
     **/
    @Column(name = "FILE_CODE", unique = false, nullable = true, length = 40)
    private String fileCode;

    /**
     * 文件名称
     **/
    @Column(name = "FILE_CODE_NAME", unique = false, nullable = true, length = 80)
    private String fileCodeName;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 状态
     **/
    @Column(name = "opr_type", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgFileReport() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return modeCode
     */
    public String getModeCode() {
        return this.modeCode;
    }

    /**
     * @param modeCode
     */
    public void setModeCode(String modeCode) {
        this.modeCode = modeCode;
    }

    /**
     * @return modeName
     */
    public String getModeName() {
        return this.modeName;
    }

    /**
     * @param modeName
     */
    public void setModeName(String modeName) {
        this.modeName = modeName;
    }

    /**
     * @return filePath
     */
    public String getFilePath() {
        return this.filePath;
    }

    /**
     * @param filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return fileSize
     */
    public String getFileSize() {
        return this.fileSize;
    }

    /**
     * @param fileSize
     */
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @return fileDate
     */
    public String getFileDate() {
        return this.fileDate;
    }

    /**
     * @param fileDate
     */
    public void setFileDate(String fileDate) {
        this.fileDate = fileDate;
    }

    /**
     * @return ver
     */
    public String getVer() {
        return this.ver;
    }

    /**
     * @param ver
     */
    public void setVer(String ver) {
        this.ver = ver;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return fileRepType
     */
    public String getFileRepType() {
        return this.fileRepType;
    }

    /**
     * @param fileRepType
     */
    public void setFileRepType(String fileRepType) {
        this.fileRepType = fileRepType;
    }

    /**
     * @return fileCode
     */
    public String getFileCode() {
        return this.fileCode;
    }

    /**
     * @param fileCode
     */
    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    /**
     * @return fileCodeName
     */
    public String getFileCodeName() {
        return this.fileCodeName;
    }

    /**
     * @param fileCodeName
     */
    public void setFileCodeName(String fileCodeName) {
        this.fileCodeName = fileCodeName;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}