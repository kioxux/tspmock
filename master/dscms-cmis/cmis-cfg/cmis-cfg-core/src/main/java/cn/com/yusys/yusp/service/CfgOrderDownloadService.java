/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgOrderDownload;
import cn.com.yusys.yusp.domain.CfgOrderDownloadRecord;
import cn.com.yusys.yusp.repository.mapper.CfgOrderDownloadMapper;
import cn.com.yusys.yusp.repository.mapper.CfgOrderDownloadRecordMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrderDownloadService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-24 10:20:50
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgOrderDownloadService {

    @Autowired
    private CfgOrderDownloadMapper cfgOrderDownloadMapper;

    @Autowired
    private CfgOrderDownloadRecordMapper cfgOrderDownloadRecordMapper;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgOrderDownload selectByPrimaryKey(String pkId) {
        return cfgOrderDownloadMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CfgOrderDownload> selectAll(QueryModel model) {
        List<CfgOrderDownload> records = (List<CfgOrderDownload>) cfgOrderDownloadMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgOrderDownload> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgOrderDownload> list = cfgOrderDownloadMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CfgOrderDownload record) {
        return cfgOrderDownloadMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CfgOrderDownload record) {
        return cfgOrderDownloadMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CfgOrderDownload record) {
        return cfgOrderDownloadMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CfgOrderDownload record) {
        return cfgOrderDownloadMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return cfgOrderDownloadMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return cfgOrderDownloadMapper.deleteByIds(ids);
    }

    /**
     * 获取预约配置
     *
     * @param pkId
     * @return
     */
    public CfgOrderDownload getOrderCfg(String pkId) {
        CfgOrderDownload cfgOrderDownload = cfgOrderDownloadMapper.selectByPrimaryKey(pkId);
        int downloadCount = 0;
        QueryModel model = new QueryModel();
        model.addCondition("relPkId", pkId);
        List<CfgOrderDownloadRecord> list = cfgOrderDownloadRecordMapper.selectByModel(model);
        if (list != null && list.size() > 0) {
            downloadCount = list.size();
        }
        cfgOrderDownload.setDownloadCount(downloadCount);
        return cfgOrderDownload;
    }

    public List<CfgOrderDownload> getOrderCfgByDataSource(String dataSource) {
        return cfgOrderDownloadMapper.getOrderCfgByDataSource(dataSource);
    }
}
