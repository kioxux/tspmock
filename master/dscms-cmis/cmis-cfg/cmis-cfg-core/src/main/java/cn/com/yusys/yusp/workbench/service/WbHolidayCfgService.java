/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.workbench.domain.WbHolidayCfg;
import cn.com.yusys.yusp.workbench.repository.mapper.WbHolidayCfgMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbHolidayCfgService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 21:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class WbHolidayCfgService {

    @Autowired
    private WbHolidayCfgMapper wbHolidayCfgMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public WbHolidayCfg selectByPrimaryKey(String calendarDate) {
        return wbHolidayCfgMapper.selectByPrimaryKey(calendarDate);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<WbHolidayCfg> selectAll(QueryModel model) {
        List<WbHolidayCfg> records = (List<WbHolidayCfg>) wbHolidayCfgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<WbHolidayCfg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<WbHolidayCfg> list = wbHolidayCfgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(WbHolidayCfg record) {
        return wbHolidayCfgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(WbHolidayCfg record) {
        return wbHolidayCfgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(WbHolidayCfg record) {
        return wbHolidayCfgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(WbHolidayCfg record) {
        return wbHolidayCfgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String calendarDate) {
        return wbHolidayCfgMapper.deleteByPrimaryKey(calendarDate);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return wbHolidayCfgMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:getWorkDays
     * @函数描述: 获取当前时间到月末的工作日天数
     * @参数与返回说明:
     * @算法描述:
     */
    public int getWorkDaysByMonth(Date nowDate) {
        String nowDateString = DateUtils.formatDate(nowDate, CommonConstant.DATE_FORMAT_YMD);
        // 查询当前月剩余的工作日
        List<String> dateStringList = wbHolidayCfgMapper.getWorkDaysByMonth(nowDateString);
        // 统计剩余工作日天数
        int daySize = dateStringList.size();
        return daySize;
    }
}
