/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgFlexQry;
import cn.com.yusys.yusp.service.CfgFlexQryParamService;
import cn.com.yusys.yusp.service.CfgFlexQryService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2021-01-22 16:58:54
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgflexqry")
public class CfgFlexQryResource {
    private static final Logger log = LoggerFactory.getLogger(CfgPrdBasicinfoResource.class);
    @Autowired
    private CfgFlexQryService cfgFlexQryService;
    @Autowired
    private CfgFlexQryParamService cfgFlexQryParamService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgFlexQry>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgFlexQry> list = cfgFlexQryService.selectAll(queryModel);
        return new ResultDto<List<CfgFlexQry>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgFlexQry>> index(QueryModel queryModel) {
        List<CfgFlexQry> list = cfgFlexQryService.selectByModel(queryModel);
        return new ResultDto<List<CfgFlexQry>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{qryCode}")
    protected ResultDto<CfgFlexQry> show(@PathVariable("qryCode") String qryCode) {
        CfgFlexQry cfgFlexQry = cfgFlexQryService.selectByPrimaryKey(qryCode);
        return new ResultDto<CfgFlexQry>(cfgFlexQry);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgFlexQry> create(@RequestBody CfgFlexQry cfgFlexQry) throws URISyntaxException {
        cfgFlexQryService.insert(cfgFlexQry);
        return new ResultDto<CfgFlexQry>(cfgFlexQry);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgFlexQry cfgFlexQry) throws URISyntaxException {
        int result = cfgFlexQryService.update(cfgFlexQry);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{qryCode}")
    protected ResultDto<Integer> delete(@PathVariable("qryCode") String qryCode) {
        int result = cfgFlexQryService.deleteByPrimaryKey(qryCode);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgFlexQryService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 保存配置的查询条件、展示字段等 子表数据
     *
     * @param childData 配置的查询条件、展示字段等 子表数据
     * @return
     */
    @PostMapping("/cfgFlexQryChildSave")
    protected ResultDto<Integer> cfgFlexQryChildSave(@RequestBody Map childData) {
        log.info("保存配置的查询条件、展示字段等 子表数据【{}】", JSONObject.toJSON(childData));
        int result = cfgFlexQryParamService.insertParam(childData);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:logicDelete
     * @函数描述:单个对象逻辑删除，同步删除子表关联数据，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicDelete/{qryCode}")
    protected ResultDto<Integer> logicDelete(@PathVariable("qryCode") String qryCode) {
        int result = cfgFlexQryService.logicDelete(qryCode);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据灵活查询流水号获取列表查询SQL 列表模板 数据源等参数
     *
     * @param qryCode 灵活查询流水号
     * @return
     */
    @GetMapping("/generateQueryListSQLByQryCode/{qryCode}")
    protected ResultDto<Map> generateQueryListSQLByQryCode(@PathVariable String qryCode) {
        log.info("根据灵活查询流水号获取列表查询SQL 列表模板 数据源等参数");
        Map result = cfgFlexQryService.generateQueryListSQLByQryCode(qryCode);
        return new ResultDto<Map>(result);
    }

    @PostMapping("/getshowlist/{qryCode}")
    protected List<Map<String, String>> getShowList(@PathVariable String qryCode) {
        List<Map<String, String>> result = cfgFlexQryService.getShowList(qryCode);
        return result;
    }

    /**
     * 根据灵活查询流水号获取汇总 查询SQL 汇总列表模板 数据源等参数
     *
     * @param qryCode 灵活查询流水号
     * @return
     */
    @PostMapping("/generateQueryGroupSQLByQryCode/{qryCode}")
    protected ResultDto<Map> generateQueryGroupSQLByQryCode(@PathVariable String qryCode) {
        log.info("根据灵活查询流水号获取汇总 查询SQL 汇总列表模板 数据源等参数");
        Map result = cfgFlexQryService.generateQueryGroupSQLByQryCode(qryCode);
        return new ResultDto<Map>(result);
    }

    /**
     * 根据角色编号查询登录角色权限下报表数据
     *
     * @param map 角色编号
     * @return
     */
    @PostMapping("/querycfgFlexQryInfoByRoleCode")
    protected ResultDto<List<CfgFlexQry>> querycfgFlexQryInfoByRoleCode(@RequestBody Map map) {
        log.info("角色编号数据【{}】", JSONObject.toJSON(map));
        List<CfgFlexQry> list = cfgFlexQryService.querycfgFlexQryInfoByRoleCode(map);
        return new ResultDto<List<CfgFlexQry>>(list);
    }

}
