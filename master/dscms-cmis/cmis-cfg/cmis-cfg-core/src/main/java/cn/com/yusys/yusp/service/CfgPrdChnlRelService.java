/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgPrdChnlRel;
import cn.com.yusys.yusp.repository.mapper.CfgPrdChnlRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdChnlRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-07 11:27:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgPrdChnlRelService {

    @Autowired
    private CfgPrdChnlRelMapper cfgPrdChnlRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgPrdChnlRel selectByPrimaryKey(String pkId) {
        return cfgPrdChnlRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgPrdChnlRel> selectAll(QueryModel model) {
        List<CfgPrdChnlRel> records = (List<CfgPrdChnlRel>) cfgPrdChnlRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgPrdChnlRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPrdChnlRel> list = cfgPrdChnlRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgPrdChnlRel record) {
        return cfgPrdChnlRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgPrdChnlRel record) {
        return cfgPrdChnlRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgPrdChnlRel record) {
        return cfgPrdChnlRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgPrdChnlRel record) {
        return cfgPrdChnlRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgPrdChnlRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgPrdChnlRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: copyCfgPrdChnlRel
     * @方法描述: 复制产品时，将老产品配置的产品适用渠道联动复制保存
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int copyCfgPrdChnlRel(String prdId, String oldPrdId) {
        return cfgPrdChnlRelMapper.copyCfgPrdChnlRel(prdId, oldPrdId, CommonConstant.ADD_OPR);
    }

    /**
     * @方法名称: deleteCfgPrdChnlRel
     * @方法描述: 删除产品时，将产品配置的产品适用渠道联动删除，此处为逻辑删除更新操作类型为“删除”，不进行物理删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteCfgPrdChnlRel(String prdId, String oprType) {
        return cfgPrdChnlRelMapper.deleteCfgPrdChnlRel(prdId, oprType);
    }

}
