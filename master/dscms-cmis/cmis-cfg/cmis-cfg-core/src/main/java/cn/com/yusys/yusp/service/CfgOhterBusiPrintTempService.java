/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CfgOhterBusiPrintTemp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CfgOhterBusiPrintTempMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOhterBusiPrintTempService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 19:33:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgOhterBusiPrintTempService {
    private static final Logger log = LoggerFactory.getLogger(CfgDataFlowService.class);

    @Autowired
    private CfgOhterBusiPrintTempMapper cfgOhterBusiPrintTempMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgOhterBusiPrintTemp selectByPrimaryKey(String pkId) {
        return cfgOhterBusiPrintTempMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgOhterBusiPrintTemp> selectAll(QueryModel model) {
        List<CfgOhterBusiPrintTemp> records = (List<CfgOhterBusiPrintTemp>) cfgOhterBusiPrintTempMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgOhterBusiPrintTemp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgOhterBusiPrintTemp> list = cfgOhterBusiPrintTempMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgOhterBusiPrintTemp record) {
        return cfgOhterBusiPrintTempMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgOhterBusiPrintTemp record) {
        return cfgOhterBusiPrintTempMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgOhterBusiPrintTemp record) {
        return cfgOhterBusiPrintTempMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgOhterBusiPrintTemp record) {
        return cfgOhterBusiPrintTempMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgOhterBusiPrintTempMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgOhterBusiPrintTempMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: toSignList
     * @方法描述: 查询列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CfgOhterBusiPrintTemp> toSignList(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return cfgOhterBusiPrintTempMapper.selectByModel(model);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int logicDelete(CfgOhterBusiPrintTemp cfgOhterBusiPrintTemp) {
        cfgOhterBusiPrintTemp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return cfgOhterBusiPrintTempMapper.updateByPrimaryKey(cfgOhterBusiPrintTemp);
    }

    /**
     * @方法名称: queryCfgOhterBusiPrintTempDataByParams
     * @方法描述: 根据入参查询数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:45:45
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CfgOhterBusiPrintTemp queryCfgOhterBusiPrintTempDataByParams(Map map) {
        return cfgOhterBusiPrintTempMapper.queryCfgOhterBusiPrintTempDataByParams(map);
    }

    /**
     * 其他业务打印模板配置新增页面点击下一步
     *
     * @param cfgOhterBusiPrintTemp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveCfgOhterBusiPrintTempInfo(CfgOhterBusiPrintTemp cfgOhterBusiPrintTemp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (cfgOhterBusiPrintTemp == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                cfgOhterBusiPrintTemp.setInputId(userInfo.getLoginCode());
                cfgOhterBusiPrintTemp.setInputBrId(userInfo.getOrg().getCode());
                cfgOhterBusiPrintTemp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            Map seqMap = new HashMap();

            String tempNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, seqMap);

            cfgOhterBusiPrintTemp.setTempNo(tempNo);
            cfgOhterBusiPrintTemp.setPkId(StringUtils.uuid(true));
            cfgOhterBusiPrintTemp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);

            int insertCount = cfgOhterBusiPrintTempMapper.insertSelective(cfgOhterBusiPrintTemp);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("tempNo", tempNo);
            log.info("其他业务打印模板配置" + tempNo + "-保存成功！");
        } catch (YuspException e) {
            log.error("其他业务打印模板配置新增保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("保存其他业务打印模板配置信息异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 其他业务打印模板配置通用保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveCfgOhterBusiPrintTempInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String tempNo = "";
        try {
            //获取申请流水号
            tempNo = (String) params.get("tempNo");
            if (StringUtils.isBlank(tempNo)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到其他业务打印模板配置主键信息";
                return result;
            }

            String logPrefix = "其他业务打印模板配置" + tempNo;

            log.info(logPrefix + "获取其他业务打印模板配置数据");
            CfgOhterBusiPrintTemp cfgOhterBusiPrintTemp = JSONObject.parseObject(JSON.toJSONString(params), CfgOhterBusiPrintTemp.class);
            if (cfgOhterBusiPrintTemp == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存其他业务打印模板配置-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                cfgOhterBusiPrintTemp.setUpdId(userInfo.getLoginCode());
                cfgOhterBusiPrintTemp.setUpdBrId(userInfo.getOrg().getCode());
                cfgOhterBusiPrintTemp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(logPrefix + "保存其他业务打印模板配置数据");
            int updCount = cfgOhterBusiPrintTempMapper.updateByPrimaryKeySelective(cfgOhterBusiPrintTemp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存其他业务打印模板配置" + tempNo + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }
}
