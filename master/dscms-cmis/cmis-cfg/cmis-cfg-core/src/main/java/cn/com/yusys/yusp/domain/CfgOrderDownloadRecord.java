/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrderDownloadRecord
 * @类描述: cfg_order_download_record数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-24 17:03:54
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_order_download_record")
public class CfgOrderDownloadRecord extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 关联预约配置主键
     **/
    @Column(name = "REL_PK_ID", unique = false, nullable = false, length = 40)
    private String relPkId;

    /**
     * 文件路径或者文件服务给的索引，待定
     **/
    @Column(name = "FILE_PATH", unique = false, nullable = true, length = 400)
    private String filePath;

    /**
     * 文件数据获取时间
     **/
    @Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
    private String dataDate;

    public CfgOrderDownloadRecord() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return relPkId
     */
    public String getRelPkId() {
        return this.relPkId;
    }

    /**
     * @param relPkId
     */
    public void setRelPkId(String relPkId) {
        this.relPkId = relPkId;
    }

    /**
     * @return filePath
     */
    public String getFilePath() {
        return this.filePath;
    }

    /**
     * @param filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return dataDate
     */
    public String getDataDate() {
        return this.dataDate;
    }

    /**
     * @param dataDate
     */
    public void setDataDate(String dataDate) {
        this.dataDate = dataDate;
    }


}