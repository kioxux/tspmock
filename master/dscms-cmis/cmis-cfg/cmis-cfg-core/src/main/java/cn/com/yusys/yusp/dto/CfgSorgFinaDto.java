package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSorgFina
 * @类描述: cfg_sorg_fina数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 10:13:25
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgSorgFinaDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    private String pkId;

    /**
     * 财务机构号
     **/
    private String finaBrNo;

    /**
     * 管理机构号
     **/
    private String managerBrNo;

    /**
     * 财务类型
     **/
    private String finaType;

    /**
     * 财务类型
     **/
    private String finaBrName;


    /**
     * @return PkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    /**
     * @return FinaBrNo
     */
    public String getFinaBrNo() {
        return this.finaBrNo;
    }

    /**
     * @param finaBrNo
     */
    public void setFinaBrNo(String finaBrNo) {
        this.finaBrNo = finaBrNo == null ? null : finaBrNo.trim();
    }

    /**
     * @return ManagerBrNo
     */
    public String getManagerBrNo() {
        return this.managerBrNo;
    }

    /**
     * @param managerBrNo
     */
    public void setManagerBrNo(String managerBrNo) {
        this.managerBrNo = managerBrNo == null ? null : managerBrNo.trim();
    }

    /**
     * @return FinaType
     */
    public String getFinaType() {
        return this.finaType;
    }

    /**
     * @param finaType
     */
    public void setFinaType(String finaType) {
        this.finaType = finaType == null ? null : finaType.trim();
    }

    /**
     * @return finaBrName
     */
    public String getFinaBrName() {
        return this.finaBrName;
    }

    /**
     * @param finaBrName
     */
    public void setFinaBrName(String finaBrName) {
        this.finaBrName = finaBrName;
    }


}