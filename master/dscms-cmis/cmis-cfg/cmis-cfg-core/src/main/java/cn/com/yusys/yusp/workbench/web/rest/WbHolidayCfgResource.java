/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.workbench.domain.WbHolidayCfg;
import cn.com.yusys.yusp.workbench.service.WbHolidayCfgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbHolidayCfgResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 21:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/wbholidaycfg")
public class WbHolidayCfgResource {
    @Autowired
    private WbHolidayCfgService wbHolidayCfgService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<WbHolidayCfg>> query() {
        QueryModel queryModel = new QueryModel();
        List<WbHolidayCfg> list = wbHolidayCfgService.selectAll(queryModel);
        return new ResultDto<List<WbHolidayCfg>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<WbHolidayCfg>> index(@RequestBody QueryModel queryModel) {
        List<WbHolidayCfg> list = wbHolidayCfgService.selectByModel(queryModel);
        return new ResultDto<List<WbHolidayCfg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{calendarDate}")
    protected ResultDto<WbHolidayCfg> show(@PathVariable("calendarDate") String calendarDate) {
        WbHolidayCfg wbHolidayCfg = wbHolidayCfgService.selectByPrimaryKey(calendarDate);
        return new ResultDto<WbHolidayCfg>(wbHolidayCfg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<WbHolidayCfg> create(@RequestBody WbHolidayCfg wbHolidayCfg) throws URISyntaxException {
        wbHolidayCfgService.insert(wbHolidayCfg);
        return new ResultDto<WbHolidayCfg>(wbHolidayCfg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody WbHolidayCfg wbHolidayCfg) throws URISyntaxException {
        int result = wbHolidayCfgService.update(wbHolidayCfg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{calendarDate}")
    protected ResultDto<Integer> delete(@PathVariable("calendarDate") String calendarDate) {
        int result = wbHolidayCfgService.deleteByPrimaryKey(calendarDate);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = wbHolidayCfgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getWorkDays
     * @函数描述: 获取当前时间到月末的工作日天数
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取当前时间到月末的工作日天数")
    @PostMapping("/getWorkDays")
    protected ResultDto<Integer> getWorkDaysByMonth(@RequestBody Date nowdate) {
        int result = wbHolidayCfgService.getWorkDaysByMonth(nowdate);
        return new ResultDto<Integer>(result);
    }
}
