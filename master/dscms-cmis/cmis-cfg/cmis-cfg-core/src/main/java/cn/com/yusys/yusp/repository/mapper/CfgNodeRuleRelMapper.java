/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgNodeRuleRel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgNodeRuleRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-03 21:47:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CfgNodeRuleRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CfgNodeRuleRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgNodeRuleRel> selectByModel(QueryModel model);

    /**
     * @方法名称: queryByPlanWfiNode
     * @方法描述: 根据业务规则方案编号、流程编号、节点编号查询当前流程节点已保存的关联规则项列表
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<CfgNodeRuleRel> queryByPlanWfiNode(CfgNodeRuleRel record);

    /**
     * @方法名称: queryCfgPlanRuleRelByPlan
     * @方法描述: 根据业务规则方案编号查询当前业务规则方案所有配置的业务规则方案关联规则项列表
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<CfgNodeRuleRel> queryCfgPlanRuleRelByPlan(CfgNodeRuleRel record);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CfgNodeRuleRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CfgNodeRuleRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CfgNodeRuleRel record);

    /**
     * @方法名称: updateByRuleItemId
     * @方法描述: 根据规则项目编号更新"是否启用"字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByRuleItemId(CfgNodeRuleRel record);

    /**
     * @方法名称: updateByWfiSignId
     * @方法描述: 逻辑删除，根据工作流编号、业务规则方案编号 "操作标识"字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByWfiSignId(@Param("wfiSignId") String wfiSignId, @Param("planId") String planId, @Param("oprType") String oprType);

    /**
     * @方法名称: updateByPlanId
     * @方法描述: 逻辑删除，根据规业务规则方案更新"操作标识"字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPlanId(@Param("planId") String planId, @Param("oprType") String oprType);

    /**
     * @方法名称: updateByRuleItemIdAndPlanId
     * @方法描述: 逻辑删除，根据节点编号,规业务规则方案更新"操作标识"字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByRuleItemIdAndPlanId(@Param("ruleItemId") String ruleItemId, @Param("planId") String planId, @Param("oprType") String oprType);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CfgNodeRuleRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);


    /**
     * @方法名称: queryByPlanWfiNodeInfo
     * @方法描述: 根据流程编号、节点编号查询当前流程节点已保存的关联规则项列表
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<CfgNodeRuleRel> queryByPlanWfiNodeInfo(CfgNodeRuleRel record);
}