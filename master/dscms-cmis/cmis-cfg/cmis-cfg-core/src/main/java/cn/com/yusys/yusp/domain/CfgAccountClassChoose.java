/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-dto模块
 * @类名称: CfgAccountClassChoose
 * @类描述: cfg_account_class_choose数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-20 21:23:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_account_class_choose")
public class CfgAccountClassChoose extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 科目配置流水号
     **/
    @Column(name = "SEQ_NO", unique = false, nullable = false, length = 5)
    private String seqNo;

    /**
     * 对公对私
     **/
    @Column(name = "PUBLIC_PERSON", unique = false, nullable = true, length = 10)
    private String publicPerson;

    /**
     * 科目号
     **/
    @Column(name = "ACCOUNT_CLASS", unique = false, nullable = true, length = 100)
    private String accountClass;

    /**
     * 核心科目号
     **/
    @Column(name = "HX_ACCOUNT_CLASS", unique = false, nullable = true, length = 100)
    private String hxAccountClass;

    /**
     * 科目名称
     **/
    @Column(name = "ACCOUNT_CLASS_NAME", unique = false, nullable = true, length = 500)
    private String accountClassName;

    /**
     * 核心科目名称
     **/
    @Column(name = "HX_ACCOUNT_CLASS_NAME", unique = false, nullable = true, length = 500)
    private String hxAccountClassName;

    /**
     * 产品范围
     **/
    @Column(name = "BIZ_TYPE_SUB", unique = false, nullable = true, length = 550)
    private String bizTypeSub;

    /**
     * 客户类型
     **/
    @Column(name = "CUS_TYPE", unique = false, nullable = true, length = 10)
    private String cusType;

    /**
     * 城乡类型
     **/
    @Column(name = "CITY_VILLAGE", unique = false, nullable = true, length = 10)
    private String cityVillage;

    /**
     * 企业类型
     **/
    @Column(name = "FACTORY_TYPE", unique = false, nullable = true, length = 10)
    private String factoryType;

    /**
     * 主营业务
     **/
    @Column(name = "MAIN_DEAL", unique = false, nullable = true, length = 100)
    private String mainDeal;

    /**
     * 涉农标识
     **/
    @Column(name = "FARM_FLAG", unique = false, nullable = true, length = 10)
    private String farmFlag;

    /**
     * 涉农投向大类
     **/
    @Column(name = "FARM_DIRECTION", unique = false, nullable = true, length = 100)
    private String farmDirection;

    /**
     * 投向行业大类
     **/
    @Column(name = "DIRECTION_OPTION", unique = false, nullable = true, length = 100)
    private String directionOption;

    /**
     * 担保方式
     **/
    @Column(name = "GUAR_MODE", unique = false, nullable = true, length = 100)
    private String guarMode;

    /**
     * 贷款类别
     **/
    @Column(name = "loan_Type", unique = false, nullable = true, length = 100)
    private String loanType;

    /**
     * 期限
     **/
    @Column(name = "LOAN_TERM", unique = false, nullable = true, length = 100)
    private String loanTerm;


    public CfgAccountClassChoose() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return seqNo
     */
    public String getSeqNo() {
        return this.seqNo;
    }

    /**
     * @param seqNo
     */
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * @return publicPerson
     */
    public String getPublicPerson() {
        return this.publicPerson;
    }

    /**
     * @param publicPerson
     */
    public void setPublicPerson(String publicPerson) {
        this.publicPerson = publicPerson;
    }

    /**
     * @return accountClass
     */
    public String getAccountClass() {
        return this.accountClass;
    }

    /**
     * @param accountClass
     */
    public void setAccountClass(String accountClass) {
        this.accountClass = accountClass;
    }

    /**
     * @return hxAccountClass
     */
    public String getHxAccountClass() {
        return this.hxAccountClass;
    }

    /**
     * @param hxAccountClass
     */
    public void setHxAccountClass(String hxAccountClass) {
        this.hxAccountClass = hxAccountClass;
    }

    /**
     * @return accountClassName
     */
    public String getAccountClassName() {
        return this.accountClassName;
    }

    /**
     * @param accountClassName
     */
    public void setAccountClassName(String accountClassName) {
        this.accountClassName = accountClassName;
    }

    /**
     * @return hxAccountClassName
     */
    public String getHxAccountClassName() {
        return this.hxAccountClassName;
    }

    /**
     * @param hxAccountClassName
     */
    public void setHxAccountClassName(String hxAccountClassName) {
        this.hxAccountClassName = hxAccountClassName;
    }

    /**
     * @return bizTypeSub
     */
    public String getBizTypeSub() {
        return this.bizTypeSub;
    }

    /**
     * @param bizTypeSub
     */
    public void setBizTypeSub(String bizTypeSub) {
        this.bizTypeSub = bizTypeSub;
    }

    /**
     * @return cusType
     */
    public String getCusType() {
        return this.cusType;
    }

    /**
     * @param cusType
     */
    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    /**
     * @return cityVillage
     */
    public String getCityVillage() {
        return this.cityVillage;
    }

    /**
     * @param cityVillage
     */
    public void setCityVillage(String cityVillage) {
        this.cityVillage = cityVillage;
    }

    /**
     * @return factoryType
     */
    public String getFactoryType() {
        return this.factoryType;
    }

    /**
     * @param factoryType
     */
    public void setFactoryType(String factoryType) {
        this.factoryType = factoryType;
    }

    /**
     * @return mainDeal
     */
    public String getMainDeal() {
        return this.mainDeal;
    }

    /**
     * @param mainDeal
     */
    public void setMainDeal(String mainDeal) {
        this.mainDeal = mainDeal;
    }

    /**
     * @return farmFlag
     */
    public String getFarmFlag() {
        return this.farmFlag;
    }

    /**
     * @param farmFlag
     */
    public void setFarmFlag(String farmFlag) {
        this.farmFlag = farmFlag;
    }

    /**
     * @return farmDirection
     */
    public String getFarmDirection() {
        return this.farmDirection;
    }

    /**
     * @param farmDirection
     */
    public void setFarmDirection(String farmDirection) {
        this.farmDirection = farmDirection;
    }

    /**
     * @return directionOption
     */
    public String getDirectionOption() {
        return this.directionOption;
    }

    /**
     * @param directionOption
     */
    public void setDirectionOption(String directionOption) {
        this.directionOption = directionOption;
    }

    /**
     * @return guarMode
     */
    public String getGuarMode() {
        return this.guarMode;
    }

    /**
     * @param guarMode
     */
    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    /**
     * @return loanType
     */
    public String getLoanType() {
        return this.loanType;
    }

    /**
     * @param loanType
     */
    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    /**
     * @return loanTerm
     */
    public String getLoanTerm() {
        return this.loanTerm;
    }

    /**
     * @param loanTerm
     */
    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }


}