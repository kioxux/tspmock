/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgPrdCatalog;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoTranDto;
import cn.com.yusys.yusp.dto.CfgPrdCatalogBasicInfoDto;
import cn.com.yusys.yusp.dto.CfgPrdCatalogDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfDto;
import cn.com.yusys.yusp.service.CfgPrdCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdCatalogResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2021-04-06 21:29:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgprdcatalog")
public class CfgPrdCatalogResource {
    @Autowired
    private CfgPrdCatalogService cfgPrdCatalogService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgPrdCatalog>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgPrdCatalog> list = cfgPrdCatalogService.selectAll(queryModel);
        return new ResultDto<List<CfgPrdCatalog>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgPrdCatalog>> index(QueryModel queryModel) {
        List<CfgPrdCatalog> list = cfgPrdCatalogService.selectByModel(queryModel);
        return new ResultDto<List<CfgPrdCatalog>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{catalogId}")
    protected ResultDto<CfgPrdCatalog> show(@PathVariable("catalogId") String catalogId) {
        CfgPrdCatalog cfgPrdCatalog = cfgPrdCatalogService.selectByPrimaryKey(catalogId);
        return new ResultDto<CfgPrdCatalog>(cfgPrdCatalog);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgPrdCatalog> create(@RequestBody CfgPrdCatalog cfgPrdCatalog) throws URISyntaxException {
        cfgPrdCatalogService.insert(cfgPrdCatalog);
        return new ResultDto<CfgPrdCatalog>(cfgPrdCatalog);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgPrdCatalog cfgPrdCatalog) throws URISyntaxException {
        int result = cfgPrdCatalogService.updateSelective(cfgPrdCatalog);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{catalogId}")
    protected ResultDto<Integer> delete(@PathVariable("catalogId") String catalogId) {
        int result = cfgPrdCatalogService.deleteByPrimaryKey(catalogId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgPrdCatalogService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/getLmtSubPrdMappConf")
    protected List<LmtSubPrdMappConfDto> getLmtSubPrdMappConf(@RequestBody List<CfgPrdCatalogDto> cfgPrdCatalogDtos) {
        List<LmtSubPrdMappConfDto> result = cfgPrdCatalogService.getLmtSubPrdMappConf(cfgPrdCatalogDtos);
        return result;
    }

    @GetMapping("/getCfgPrdCatalogBasicInfo")
    protected List<CfgPrdCatalogBasicInfoDto> getCfgPrdCatalogBasicInfo(QueryModel model) {
        List<CfgPrdCatalogBasicInfoDto> result = cfgPrdCatalogService.getCfgPrdCatalogBasicInfo(model);
        return result;
    }

    @PostMapping("/tree")
    protected ResultDto<List<Map<String, Object>>> getCfgPrdCatalogTree(@RequestBody Map<String, String> map) {
        ResultDto resultDto = new ResultDto();

        // String optType = map.get("optType");
        String rootId = Optional.ofNullable(map.get("root")).orElse("");
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("oprType", "01");
        List<CfgPrdCatalog> list = cfgPrdCatalogService.selectAll(queryModel);
        Map<String, ArrayList<CfgPrdCatalog>> allMap = new HashMap<>(16);
        for (CfgPrdCatalog item : list) {
            String upId = Optional.ofNullable(item.getSupCatalogId()).orElse("");
            if (allMap.containsKey(upId)) {
                allMap.get(upId).add(item);
            } else {
                ArrayList<CfgPrdCatalog> sublist = new ArrayList<>();
                sublist.add(item);
                allMap.put(upId, sublist);
            }
        }
        List<Map<String, Object>> tree = getTree(rootId, allMap);
        resultDto.setData(tree);
        return resultDto;
    }

    /**
     * @param root, all]
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author tangxun
     * @date 2021/1/12 9:52
     * @version 1.0.0
     * @desc 组装前端要求的格式
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private List<Map<String, Object>> getTree(String root, Map<String, ArrayList<CfgPrdCatalog>> all) {
        if (all.containsKey(root)) {
            return all.get(root).stream().map(item -> {
                Map<String, Object> map = new LinkedHashMap<>();
                map.put("label", item.getCatalogName());
                map.put("id", item.getCatalogId());
                map.put("path", item.getCatalogLevelId() + "->" + item.getCatalogName());
                List list = getTree(item.getCatalogId(), all);
                if (!list.isEmpty()) {
                    map.put("children", list);
                }
                return map;
            }).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }


    @PostMapping("/queryPrdTree")
    protected ResultDto<List<CfgPrdBasicinfoTranDto>> getTreedic(@RequestBody Map<String, String> map) {
        String id = Optional.ofNullable(map.get("id")).orElse("");
        List<CfgPrdBasicinfoTranDto> list = cfgPrdCatalogService.queryCfgPrdCatalogTree(id);
        return new ResultDto<>(list);
    }
}
