package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgLprRate
 * @类描述: cfg_lpr_rate数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-15 19:50:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgLprRateDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 利率类型代码
     **/
    private String rateTypeId;

    /**
     * 利率类型
     **/
    private String rateTypeName;

    /**
     * 利率值
     **/
    private java.math.BigDecimal rate;

    /**
     * 生效日期
     **/
    private String validDate;

    /**
     * 利率基准
     **/
    private String rateDatum;

    /**
     * 是否启用
     **/
    private String usedInd;

    /**
     * 币种
     **/
    private String curType;

    /**
     * 利率名称
     **/
    private String rateName;

    /**
     * 操作类型
     **/
    private String oprType;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最近修改人
     **/
    private String updId;

    /**
     * 最近修改机构
     **/
    private String updBrId;

    /**
     * 最近修改日期
     **/
    private String updDate;

    /**
     * 创建时间
     **/
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    private java.util.Date updateTime;

    /**
     * @return PkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    /**
     * @return RateTypeId
     */
    public String getRateTypeId() {
        return this.rateTypeId;
    }

    /**
     * @param rateTypeId
     */
    public void setRateTypeId(String rateTypeId) {
        this.rateTypeId = rateTypeId == null ? null : rateTypeId.trim();
    }

    /**
     * @return RateTypeName
     */
    public String getRateTypeName() {
        return this.rateTypeName;
    }

    /**
     * @param rateTypeName
     */
    public void setRateTypeName(String rateTypeName) {
        this.rateTypeName = rateTypeName == null ? null : rateTypeName.trim();
    }

    /**
     * @return Rate
     */
    public java.math.BigDecimal getRate() {
        return this.rate;
    }

    /**
     * @param rate
     */
    public void setRate(java.math.BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * @return ValidDate
     */
    public String getValidDate() {
        return this.validDate;
    }

    /**
     * @param validDate
     */
    public void setValidDate(String validDate) {
        this.validDate = validDate == null ? null : validDate.trim();
    }

    /**
     * @return RateDatum
     */
    public String getRateDatum() {
        return this.rateDatum;
    }

    /**
     * @param rateDatum
     */
    public void setRateDatum(String rateDatum) {
        this.rateDatum = rateDatum == null ? null : rateDatum.trim();
    }

    /**
     * @return UsedInd
     */
    public String getUsedInd() {
        return this.usedInd;
    }

    /**
     * @param usedInd
     */
    public void setUsedInd(String usedInd) {
        this.usedInd = usedInd == null ? null : usedInd.trim();
    }

    /**
     * @return CurType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType == null ? null : curType.trim();
    }

    /**
     * @return RateName
     */
    public String getRateName() {
        return this.rateName;
    }

    /**
     * @param rateName
     */
    public void setRateName(String rateName) {
        this.rateName = rateName == null ? null : rateName.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return CreateTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return UpdateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }


}