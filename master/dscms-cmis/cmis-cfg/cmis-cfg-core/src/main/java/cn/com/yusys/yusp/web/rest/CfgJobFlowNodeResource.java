/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgJobFlowNode;
import cn.com.yusys.yusp.service.CfgJobFlowNodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgJobFlowNodeResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: qhy
 * @创建时间: 2021-05-06 21:52:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgjobflownode")
public class CfgJobFlowNodeResource {
    @Autowired
    private CfgJobFlowNodeService cfgJobFlowNodeService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgJobFlowNode>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgJobFlowNode> list = cfgJobFlowNodeService.selectAll(queryModel);
        return new ResultDto<List<CfgJobFlowNode>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgJobFlowNode>> index(QueryModel queryModel) {
        List<CfgJobFlowNode> list = cfgJobFlowNodeService.selectByModel(queryModel);
        return new ResultDto<List<CfgJobFlowNode>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgJobFlowNode> show(@PathVariable("pkId") String pkId) {
        CfgJobFlowNode cfgJobFlowNode = cfgJobFlowNodeService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgJobFlowNode>(cfgJobFlowNode);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgJobFlowNode> create(@RequestBody CfgJobFlowNode cfgJobFlowNode) throws URISyntaxException {
        cfgJobFlowNodeService.insert(cfgJobFlowNode);
        return new ResultDto<CfgJobFlowNode>(cfgJobFlowNode);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgJobFlowNode cfgJobFlowNode) throws URISyntaxException {
        int result = cfgJobFlowNodeService.update(cfgJobFlowNode);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgJobFlowNodeService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgJobFlowNodeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
