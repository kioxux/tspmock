package cn.com.yusys.yusp.constant;

public class CfgModelConstant {

    /**
     * 01-页面
     **/
    public static final String PAGE = "01";

    /**
     * 02-子模板组
     **/
    public static final String MODEL_GROUP = "02";
}