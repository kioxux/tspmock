/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgCreditLevel;
import cn.com.yusys.yusp.dto.CfgCreditLevelDto;
import cn.com.yusys.yusp.service.CfgCreditLevelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgCreditLevelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-01 09:24:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgcreditlevel")
public class CfgCreditLevelResource {
    @Autowired
    private CfgCreditLevelService cfgCreditLevelService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgCreditLevel>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgCreditLevel> list = cfgCreditLevelService.selectAll(queryModel);
        return new ResultDto<List<CfgCreditLevel>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgCreditLevel>> index(QueryModel queryModel) {
        List<CfgCreditLevel> list = cfgCreditLevelService.selectByModel(queryModel);
        return new ResultDto<List<CfgCreditLevel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgCreditLevel> show(@PathVariable("pkId") String pkId) {
        CfgCreditLevel cfgCreditLevel = cfgCreditLevelService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgCreditLevel>(cfgCreditLevel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgCreditLevel> create(@RequestBody CfgCreditLevel cfgCreditLevel) throws URISyntaxException {
        cfgCreditLevelService.insertSelective(cfgCreditLevel);
        return new ResultDto<CfgCreditLevel>(cfgCreditLevel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgCreditLevel cfgCreditLevel) throws URISyntaxException {
        int result = cfgCreditLevelService.updateSelective(cfgCreditLevel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgCreditLevelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgCreditLevelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:cfgCreditLevelList
     * @函数描述:重写查询对象列表
     * @参数与返回说明: 分页查询类
     * @算法描述:
     */
    @PostMapping("/cfgcreditlevellist")
    protected ResultDto<List<CfgCreditLevel>> cfgCreditLevelList(@RequestBody QueryModel queryModel) {
        List<CfgCreditLevel> list = cfgCreditLevelService.selectByModel(queryModel);
        return new ResultDto<List<CfgCreditLevel>>(list);
    }

    /**
     * @函数名称:showDetail
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showdetail")
    protected ResultDto<CfgCreditLevel> showDetail(@RequestBody String pkId) {
        CfgCreditLevel cfgCreditLevel = cfgCreditLevelService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgCreditLevel>(cfgCreditLevel);
    }

    /**
     * @函数名称:queryCfgCreditLevelByType
     * @函数描述:根据cfgType 01 资产抵押率配置(默认)
     * 02 信用等级配置
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据配置类型查询信用配置信息")
    @PostMapping("/queryCfgCreditLevelByType")
    protected ResultDto<List<CfgCreditLevelDto>> queryCfgCreditLevelByType(@RequestBody CfgCreditLevelDto cfgCreditLevelDto) {
        List<CfgCreditLevelDto> cfgCreditLevelDtoList = cfgCreditLevelService
                .queryCfgCreditLevelByType(cfgCreditLevelDto)
                .parallelStream()
                .map(e -> {
                    CfgCreditLevelDto cfgCreditLevelDtoResp = new CfgCreditLevelDto();
                    BeanUtils.copyProperties(e, cfgCreditLevelDtoResp);
                    return cfgCreditLevelDtoResp;
                }).collect(Collectors.toList());
        return new ResultDto<List<CfgCreditLevelDto>>(cfgCreditLevelDtoList);
    }

}
