/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSorgLoanMany
 * @类描述: cfg_sorg_loan_many数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 10:13:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_sorg_loan_many")
public class CfgSorgLoanMany extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "pk_id")
    private String pkId;

    /**
     * 管理机构号
     **/
    @Column(name = "manager_br_no", unique = false, nullable = false, length = 40)
    private String managerBrNo;

    /**
     * 放贷机构号
     **/
    @Column(name = "loan_br_no", unique = false, nullable = false, length = 40)
    private String loanBrNo;

    /**
     * 放贷机构名
     **/
    @Column(name = "loan_br_no_name", unique = false, nullable = true, length = 200)
    private String loanBrNoName;

    /**
     * 账务机构
     **/
    @Column(name = "fina_br_no", unique = false, nullable = false, length = 40)
    private String finaBrNo;

    public CfgSorgLoanMany() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return managerBrNo
     */
    public String getManagerBrNo() {
        return this.managerBrNo;
    }

    /**
     * @param managerBrNo
     */
    public void setManagerBrNo(String managerBrNo) {
        this.managerBrNo = managerBrNo;
    }

    /**
     * @return loanBrNo
     */
    public String getLoanBrNo() {
        return this.loanBrNo;
    }

    /**
     * @param loanBrNo
     */
    public void setLoanBrNo(String loanBrNo) {
        this.loanBrNo = loanBrNo;
    }

    /**
     * @return loanBrNoName
     */
    public String getLoanBrNoName() {
        return this.loanBrNoName;
    }

    /**
     * @param loanBrNoName
     */
    public void setLoanBrNoName(String loanBrNoName) {
        this.loanBrNoName = loanBrNoName;
    }

    /**
     * @return finaBrNo
     */
    public String getFinaBrNo() {
        return this.finaBrNo;
    }

    /**
     * @param finaBrNo
     */
    public void setFinaBrNo(String finaBrNo) {
        this.finaBrNo = finaBrNo;
    }


}