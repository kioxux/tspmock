/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CfgServiceExceptionDefEnums;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.constant.SQLKeyWordsConstant;
import cn.com.yusys.yusp.domain.CfgFlexQry;
import cn.com.yusys.yusp.domain.CfgFlexQryIndex;
import cn.com.yusys.yusp.dto.CfgFlexQryParamIndexDto;
import cn.com.yusys.yusp.repository.mapper.CfgFlexQryMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2021-01-22 16:58:54
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgFlexQryService {
    private static final Logger log = LoggerFactory.getLogger(CfgPrdBasicinfoService.class);

    @Autowired
    private CfgFlexQryMapper cfgFlexQryMapper;

    @Autowired
    private CfgFlexQryParamService cfgFlexQryParamService;

    @Autowired
    private CfgFlexQryIndRelService cfgFlexQryIndRelService;

    @Autowired
    private CfgFlexQryIndexService cfgFlexQryIndexService;

    @Autowired
    private CfgQryReportAuthService cfgQryReportAuthService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgFlexQry selectByPrimaryKey(String qryCode) {
        return cfgFlexQryMapper.selectByPrimaryKey(qryCode);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgFlexQry> selectAll(QueryModel model) {
        List<CfgFlexQry> records = (List<CfgFlexQry>) cfgFlexQryMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgFlexQry> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgFlexQry> list = cfgFlexQryMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgFlexQry record) {
        return cfgFlexQryMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgFlexQry record) {
        return cfgFlexQryMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgFlexQry record) {
        return cfgFlexQryMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgFlexQry record) {
        return cfgFlexQryMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String qryCode) {
        return cfgFlexQryMapper.deleteByPrimaryKey(qryCode);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgFlexQryMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除，根据查询报表编号将数据的操作类型修改为“删除”,同步删除关联子表数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int logicDelete(String qryCode) {
        //查询报表对象
        CfgFlexQry cfgFlexQry = new CfgFlexQry();
        //指标编号
        cfgFlexQry.setQryCode(qryCode);
        //操作标识
        cfgFlexQry.setOprType(CommonConstant.DELETE_OPR);
        int num = cfgFlexQryMapper.updateByPrimaryKeySelective(cfgFlexQry);
        int numDetail = 0;
        int numAuth = 0;
        if (num > 0) {
            //联动删除配置明细数据
            numDetail = cfgFlexQryParamService.deleteAllFlexQryParamByQryCode(qryCode);
            //联动删除配置的报表权限数据
            numAuth = cfgQryReportAuthService.deleteInfoByQryCode(qryCode);
        }
        log.info("***************删除查询报表编号条数【" + num + "】条,删除查询报表参数详细条数【" + numDetail + "】条,删除配置的查询报表权限条数【" + numAuth + "】条***************");

        return num;
    }


    /**
     * @方法名称: generateQueryListSQLByQryCode
     * @方法描述: 根据灵活查询流水号生成列表查询语句与列表模板数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map generateQueryListSQLByQryCode(String qryCode) {
        Map rtnData = new HashMap();
        String rtnCode = CfgServiceExceptionDefEnums.INFO_SUCCESS_UPD.getExceptionCode();
        String rtnMsg = CfgServiceExceptionDefEnums.INFO_SUCCESS_UPD.getExceptionDesc();
        // 列表查询sql
        StringBuffer bufferSql = new StringBuffer();
        List<CfgFlexQryParamIndexDto> cfgFlexQryParamIndexDtoList = new ArrayList<>();
        String dataSource = "";
        try {
            bufferSql.append(SQLKeyWordsConstant.SELECT)
                    .append(cfgFlexQryParamService.generateColumnsShow(qryCode))
                    .append(SQLKeyWordsConstant.FROM)
                    .append(cfgFlexQryIndRelService.generateTablesRel(qryCode))
                    .append(SQLKeyWordsConstant.WHERE)
                    .append(cfgFlexQryParamService.generateColumnsCond(qryCode));
            //拼接数据权限过滤条件,此处写死，前台模板会将权限过滤sql自动替换${QueryCons}
            bufferSql.append(" and ${QueryCons} ");
            //如果配置查询条件，则拼接order by  排序语句
            StringBuffer orderSql = cfgFlexQryParamService.generateColumnsOrderBy(qryCode);

            //排序sql拼接
            if (orderSql != null && orderSql.length() != 0) {
                bufferSql.append(SQLKeyWordsConstant.ORDER_BY)
                        .append(cfgFlexQryParamService.generateColumnsOrderBy(qryCode));
            }

            // 拼接查询列表模板
            cfgFlexQryParamIndexDtoList = cfgFlexQryParamService.queryListShowColumnsTempByQryCode(qryCode);

            // 查询对象信息，获取数据源配置
            CfgFlexQryIndex cfgFlexQryIndex = cfgFlexQryIndexService.getMainCfgFlexQryIndexByQryCode(qryCode);
            dataSource = cfgFlexQryIndex.getDatasource();

        } catch (YuspException e) {
            log.error("根据灵活查询流水号生成列表查询语句与列表模板数据出现异常: {}", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } /*catch (Exception e) {
            log.error("根据灵活查询流水号生成列表查询语句与列表模板数据出现异常！", e);
            rtnCode = CfgServiceExceptionDefEnums.INFO_SUCCESS_UPD.getExceptionCode();
            rtnMsg = CfgServiceExceptionDefEnums.INFO_SUCCESS_UPD.getExceptionDesc() + "," + e.getMessage();
        }*/ finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("querySql", bufferSql);
            rtnData.put("listTemp", cfgFlexQryParamIndexDtoList);
            rtnData.put("dataSource", dataSource);
        }
        return rtnData;
    }

    /**
     * @方法名称: generateQueryGroupSQLByQryCode
     * @方法描述: 根据灵活查询流水号获取汇总 查询SQL 汇总列表模板 数据源等参数
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map generateQueryGroupSQLByQryCode(String qryCode) {
        Map rtnData = new HashMap();
        String rtnCode = CfgServiceExceptionDefEnums.INFO_SUCCESS_UPD.getExceptionCode();
        String rtnMsg = CfgServiceExceptionDefEnums.INFO_SUCCESS_UPD.getExceptionDesc();
        // 列表查询sql
        StringBuffer bufferSql = new StringBuffer();
        List<CfgFlexQryParamIndexDto> cfgFlexQryParamIndexDtoList = new ArrayList<>();
        String dataSource = "";
        try {

            // 拼接汇总语句
            bufferSql.append(SQLKeyWordsConstant.SELECT)
                    .append(cfgFlexQryParamService.generateColumnsGroupShow(qryCode))
                    .append(SQLKeyWordsConstant.FROM)
                    .append(cfgFlexQryIndRelService.generateTablesRel(qryCode))
                    .append(SQLKeyWordsConstant.WHERE)
                    .append(cfgFlexQryParamService.generateColumnsCond(qryCode))
                    .append(" and ${QueryCons} ")//拼接数据权限过滤条件,此处写死，前台模板会将权限过滤sql自动替换${QueryCons}
                    .append(SQLKeyWordsConstant.GROUP_BY)
                    .append(cfgFlexQryParamService.generateColumnsGroupBy(qryCode));

            // 拼接查询列表模板
            cfgFlexQryParamIndexDtoList = cfgFlexQryParamService.queryGroupShowColumnsTempByQryCode(qryCode);

            // 查询对象信息，获取数据源配置
            CfgFlexQryIndex cfgFlexQryIndex = cfgFlexQryIndexService.getMainCfgFlexQryIndexByQryCode(qryCode);
            dataSource = cfgFlexQryIndex.getDatasource();

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("根据灵活查询流水号生成列表查询语句与列表模板数据出现异常！", e);
            rtnCode = CfgServiceExceptionDefEnums.INFO_SUCCESS_UPD.getExceptionCode();
            rtnMsg = CfgServiceExceptionDefEnums.INFO_SUCCESS_UPD.getExceptionDesc() + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("querySql", bufferSql);
            rtnData.put("listTemp", cfgFlexQryParamIndexDtoList);
            rtnData.put("dataSource", dataSource);
        }
        return rtnData;
    }

    /**
     * 根据角色编号查询登录角色权限下报表数据
     *
     * @param map
     * @return
     */
    public List<CfgFlexQry> querycfgFlexQryInfoByRoleCode(Map map) {
        return cfgFlexQryMapper.querycfgFlexQryInfoByRoleCode(map);
    }

    public List<Map<String, String>> getShowList(String qryCode) {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        List<CfgFlexQryParamIndexDto> list = cfgFlexQryParamService.queryListShowColumnsTempByQryCode(qryCode);
        for (CfgFlexQryParamIndexDto cfgFlexQryParamIndexDto : list) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("key", cfgFlexQryParamIndexDto.getColNameEn());
            map.put("value", cfgFlexQryParamIndexDto.getParamValue());
            result.add(map);
        }
        return result;
    }
}

