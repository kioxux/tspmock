/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgBlacklist;
import cn.com.yusys.yusp.service.CfgBlacklistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBlacklistResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: xuwei
 * @创建时间: 2021-04-29 13:45:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgblacklist")
public class CfgBlacklistResource {
    @Autowired
    private CfgBlacklistService cfgBlacklistService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgBlacklist>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgBlacklist> list = cfgBlacklistService.selectAll(queryModel);
        return new ResultDto<List<CfgBlacklist>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgBlacklist>> index(QueryModel queryModel) {
        List<CfgBlacklist> list = cfgBlacklistService.selectByModel(queryModel);
        return new ResultDto<List<CfgBlacklist>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgBlacklist> show(@PathVariable("pkId") String pkId) {
        CfgBlacklist cfgBlacklist = cfgBlacklistService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgBlacklist>(cfgBlacklist);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgBlacklist> create(@RequestBody CfgBlacklist cfgBlacklist) throws URISyntaxException {
        cfgBlacklistService.insert(cfgBlacklist);
        return new ResultDto<CfgBlacklist>(cfgBlacklist);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgBlacklist cfgBlacklist) throws URISyntaxException {
        int result = cfgBlacklistService.updateSelective(cfgBlacklist);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgBlacklistService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgBlacklistService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
