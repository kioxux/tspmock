/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.SQLKeyWordsConstant;
import cn.com.yusys.yusp.domain.CfgAccountClassChoose;
import cn.com.yusys.yusp.dto.CfgAccountClassChooseDto;
import cn.com.yusys.yusp.enums.returncode.EcfEnum;
import cn.com.yusys.yusp.repository.mapper.CfgAccountClassChooseMapper;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgAccountClassChooseService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 16:37:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgAccountClassChooseService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(CfgAccountClassChooseService.class);
    @Autowired
    private CfgAccountClassChooseMapper cfgAccountClassChooseMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgAccountClassChoose selectByPrimaryKey(String seqNo) {
        return cfgAccountClassChooseMapper.selectByPrimaryKey(seqNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgAccountClassChoose> selectAll(QueryModel model) {
        List<CfgAccountClassChoose> records = (List<CfgAccountClassChoose>) cfgAccountClassChooseMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgAccountClassChoose> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgAccountClassChoose> list = cfgAccountClassChooseMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgAccountClassChoose record) {
        return cfgAccountClassChooseMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgAccountClassChoose record) {
        return cfgAccountClassChooseMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgAccountClassChoose record) {
        return cfgAccountClassChooseMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgAccountClassChoose record) {
        return cfgAccountClassChooseMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String seqNo) {
        return cfgAccountClassChooseMapper.deleteByPrimaryKey(seqNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgAccountClassChooseMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryHxAccountClassByProps
     * @方法描述: 通过属性查询对应的核心科目号
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 马顺
     * @创建时间: 2021-06-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CfgAccountClassChooseDto queryHxAccountClassByProps(CfgAccountClassChooseDto cfgAccountClassChooseDto) {
        CfgAccountClassChoose CfgAccountClassChoose = new CfgAccountClassChoose();
        log.info("接收科目请求查询参数{}", cfgAccountClassChooseDto.toString());
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("publicPerson", paramForLike(cfgAccountClassChooseDto.getPublicPerson()));
        queryMap.put("bizTypeSub", paramForLike(cfgAccountClassChooseDto.getBizTypeSub()));
        queryMap.put("cusType", paramForLike(cfgAccountClassChooseDto.getCusType()));
        queryMap.put("cityVillage", paramForLike(cfgAccountClassChooseDto.getCityVillage()));
        queryMap.put("factoryType", paramForLike(cfgAccountClassChooseDto.getFactoryType()));
        queryMap.put("mainDeal", paramForLike(cfgAccountClassChooseDto.getMainDeal()));
        queryMap.put("farmFlag", paramForLike(cfgAccountClassChooseDto.getFarmFlag()));
        queryMap.put("farmDirection", paramForLike(cfgAccountClassChooseDto.getFarmDirection()));
        queryMap.put("directionOption", paramForLike(cfgAccountClassChooseDto.getDirectionOption()));
        queryMap.put("guarMode", paramForLike(cfgAccountClassChooseDto.getGuarMode()));
        queryMap.put("loanType", paramForLike(cfgAccountClassChooseDto.getLoanType()));
        queryMap.put("loanTerm", paramForLike(cfgAccountClassChooseDto.getLoanTerm()));
        List<CfgAccountClassChoose> cfgAccountClassChooseList = cfgAccountClassChooseMapper.queryHxAccountClassByProps(queryMap);
        if (cfgAccountClassChooseList != null && cfgAccountClassChooseList.size() > 0) {
            CfgAccountClassChoose firstCfgAccountClassChoose = cfgAccountClassChooseList.get(0);
            cfgAccountClassChooseDto.setRtnCode(EcfEnum.ECF020000.key);
            cfgAccountClassChooseDto.setRtnMsg(EcfEnum.ECF020000.value);
            cfgAccountClassChooseDto.setHxAccountClass(firstCfgAccountClassChoose.getHxAccountClass());
            cfgAccountClassChooseDto.setHxAccountClassName(firstCfgAccountClassChoose.getHxAccountClassName());
            cfgAccountClassChooseDto.setAccountClass(firstCfgAccountClassChoose.getAccountClass());
            cfgAccountClassChooseDto.setAccountClassName(firstCfgAccountClassChoose.getAccountClassName());
            log.info("查询核心科目为{},{}", firstCfgAccountClassChoose.getHxAccountClass(), firstCfgAccountClassChoose.getHxAccountClassName());
        } else {
            cfgAccountClassChooseDto.setRtnCode(EcfEnum.ECF020019.key);
            cfgAccountClassChooseDto.setRtnMsg(EcfEnum.ECF020019.value);
            log.info(EcfEnum.ECF020019.value);
        }
        return cfgAccountClassChooseDto;
    }

    /**
     * @方法名称: queryHxAccountClassByAcccountClass
     * @方法描述: 通过科目号查询对应的核心会计类别
     * @参数与返回说明:
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-06-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CfgAccountClassChooseDto queryHxAccountClassByAcccountClass(String accountClass) {
        CfgAccountClassChooseDto cfgAccountClassChooseDto = new CfgAccountClassChooseDto();
        try {
            log.info("********************接收科目请求查询参数{}", accountClass);
            if(StringUtils.isEmpty(accountClass)){//参数不能为空
                return cfgAccountClassChooseDto;
            }
            List<CfgAccountClassChoose> cfgAccountClassChooseList = cfgAccountClassChooseMapper.queryHxAccountClassByAcccountClass(accountClass);
            if (cfgAccountClassChooseList != null && cfgAccountClassChooseList.size() > 0) {
                CfgAccountClassChoose firstCfgAccountClassChoose = cfgAccountClassChooseList.get(0);
                cfgAccountClassChooseDto.setRtnCode(EcfEnum.ECF020000.key);
                cfgAccountClassChooseDto.setRtnMsg(EcfEnum.ECF020000.value);
                cfgAccountClassChooseDto.setHxAccountClass(firstCfgAccountClassChoose.getHxAccountClass());
                cfgAccountClassChooseDto.setHxAccountClassName(firstCfgAccountClassChoose.getHxAccountClassName());
                cfgAccountClassChooseDto.setAccountClass(firstCfgAccountClassChoose.getAccountClass());
                cfgAccountClassChooseDto.setAccountClassName(firstCfgAccountClassChoose.getAccountClassName());
                log.info("********************查询核心科目为{},{}", firstCfgAccountClassChoose.getHxAccountClass(), firstCfgAccountClassChoose.getHxAccountClassName());
            } else {
                cfgAccountClassChooseDto.setRtnCode(EcfEnum.ECF020019.key);
                cfgAccountClassChooseDto.setRtnMsg(EcfEnum.ECF020019.value);
                log.info(EcfEnum.ECF020019.value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cfgAccountClassChooseDto;
    }

    public String paramForLike(String param) {
        if (!StringUtils.isBlank(param)) {
            return SQLKeyWordsConstant.PERCENT + param.trim() + SQLKeyWordsConstant.PERCENT;
        }
        return null;
    }

}
