package cn.com.yusys.yusp.service.server.cmiscfg0004;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CfgBankInfo;
import cn.com.yusys.yusp.domain.CfgBankNoRel;
import cn.com.yusys.yusp.domain.CfgBicBankInfo;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.cmiscfg0004.req.CmisCfg0004ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0004.resp.CmisCfg0004RespDto;
import cn.com.yusys.yusp.service.CfgBankInfoService;
import cn.com.yusys.yusp.service.CfgBankNoRelService;
import cn.com.yusys.yusp.service.CfgBicBankInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CmisCfg0004Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCfg0004Service.class);

    @Autowired
    private CfgBankInfoService cfgBankInfoService;

    @Autowired
    private CfgBicBankInfoService cfgBicBankInfoService;
    @Autowired
    private CfgBankNoRelService cfgBankNoRelService;

    /**
     * 根据分支机构获取总行行号或BICCODE
     *
     * @param reqDto
     * @return
     */
    public CmisCfg0004RespDto execute(CmisCfg0004ReqDto reqDto) {
        /**
         * cmiscfg0004 ：
         * 1.行号不为空，则到cfg_bank_info 根据bankNo查找 SUPER_BANK_NO作为总行行号返回；
         * 2.bicCode不为空，则到cfg_bic_bank_info根据bicCode查找SUPER_BIC_CODE作为总行BICCODE返回
         */
        CmisCfg0004RespDto cmisCfg0004RespDto = new CmisCfg0004RespDto();

        try {
            String bankNo = reqDto.getBankNo();
            String bicCode = reqDto.getBicCode();

            if (StringUtils.nonBlank(bankNo)) {
//                CfgBankInfo cfgBankInfo = cfgBankInfoService.selectByBankNo(bankNo);
                CfgBankNoRel cfgBankNoRel = cfgBankNoRelService.selectByBankNo(bankNo);
                if (cfgBankNoRel != null) {
                    cmisCfg0004RespDto.setSuperBankNo(cfgBankNoRel.getSuperBankNo());
                }
            }
            if (StringUtils.nonBlank(bicCode)) {
                CfgBicBankInfo cfgBicBankInfo = cfgBicBankInfoService.selectByPrimaryKey(bicCode);
                if (cfgBicBankInfo != null) {
                    cmisCfg0004RespDto.setSuperBicCode(cfgBicBankInfo.getSuperBicCode());
                }
            }

            cmisCfg0004RespDto.setErrorCode(SuccessEnum.SUCCESS.key);
            cmisCfg0004RespDto.setErrorMsg(SuccessEnum.SUCCESS.value);

        } catch (YuspException e) {
            logger.error(DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value, e);
            cmisCfg0004RespDto.setErrorCode(e.getCode());
            cmisCfg0004RespDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value);
        return cmisCfg0004RespDto;

    }
}
