/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.risk.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: yusp-npam-biz-core模块
 * @类名称: PrdPvRiskItem
 * @类描述: PRD_PV_RISK_ITEM数据实体类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2020-07-27 17:26:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "PRD_PV_RISK_ITEM")
public class PrdPvRiskItem extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 项目编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "ITEM_ID")
    private String itemId;

    /**
     * 检查说明
     **/
    @Column(name = "ITEM_DESC", unique = false, nullable = true, length = 500)
    private String itemDesc;

    /**
     * 项目名称
     **/
    @Column(name = "ITEM_NAME", unique = false, nullable = true, length = 500)
    private String itemName;

    /**
     * 检查规则
     **/
    @Column(name = "ITEM_RULES", unique = false, nullable = true, length = 100)
    private String itemRules;

    /**
     * 外部链接
     **/
    @Column(name = "LINK_URL", unique = false, nullable = true, length = 1000)
    private String linkUrl;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 是否启用
     **/
    @Column(name = "USED_IND", unique = false, nullable = true, length = 1)
    private String usedInd;

    /**
     * 风险拦截实现类
     **/
    @Column(name = "ITEM_CLASS", unique = false, nullable = true, length = 300)
    private String itemClass;

    /**
     * 是否适用规则
     **/
    @Column(name = "CHOOSE_ONE", unique = false, nullable = true, length = 1)
    private String chooseOne;

    /**
     * 方案分类
     **/
    @Column(name = "ITEM_TYPE", unique = false, nullable = true, length = 2)
    private String itemType;

    /**
     * 预警级别
     **/
    @Column(name = "RISK_LEVEL", unique = false, nullable = true, length = 2)
    private String riskLevel;

    /**
     * 触发类别
     **/
    @Column(name = "RISK_TYPE", unique = false, nullable = true, length = 2)
    private String riskType;

    /**
     * 预警频度
     **/
    @Column(name = "RISK_FREQ", unique = false, nullable = true, length = 2)
    private String riskFreq;

    /**
     * INSTU_CDE
     **/
    @Column(name = "INSTU_CDE", unique = false, nullable = true, length = 10)
    private String instuCde;

    /**
     * @return itemId
     */
    public String getItemId() {
        return this.itemId;
    }

    /**
     * @param itemId
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * @return itemDesc
     */
    public String getItemDesc() {
        return this.itemDesc;
    }

    /**
     * @param itemDesc
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * @return itemName
     */
    public String getItemName() {
        return this.itemName;
    }

    /**
     * @param itemName
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * @return itemRules
     */
    public String getItemRules() {
        return this.itemRules;
    }

    /**
     * @param itemRules
     */
    public void setItemRules(String itemRules) {
        this.itemRules = itemRules;
    }

    /**
     * @return linkUrl
     */
    public String getLinkUrl() {
        return this.linkUrl;
    }

    /**
     * @param linkUrl
     */
    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return usedInd
     */
    public String getUsedInd() {
        return this.usedInd;
    }

    /**
     * @param usedInd
     */
    public void setUsedInd(String usedInd) {
        this.usedInd = usedInd;
    }

    /**
     * @return itemClass
     */
    public String getItemClass() {
        return this.itemClass;
    }

    /**
     * @param itemClass
     */
    public void setItemClass(String itemClass) {
        this.itemClass = itemClass;
    }

    /**
     * @return chooseOne
     */
    public String getChooseOne() {
        return this.chooseOne;
    }

    /**
     * @param chooseOne
     */
    public void setChooseOne(String chooseOne) {
        this.chooseOne = chooseOne;
    }

    /**
     * @return itemType
     */
    public String getItemType() {
        return this.itemType;
    }

    /**
     * @param itemType
     */
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    /**
     * @return riskLevel
     */
    public String getRiskLevel() {
        return this.riskLevel;
    }

    /**
     * @param riskLevel
     */
    public void setRiskLevel(String riskLevel) {
        this.riskLevel = riskLevel;
    }

    /**
     * @return riskType
     */
    public String getRiskType() {
        return this.riskType;
    }

    /**
     * @param riskType
     */
    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    /**
     * @return riskFreq
     */
    public String getRiskFreq() {
        return this.riskFreq;
    }

    /**
     * @param riskFreq
     */
    public void setRiskFreq(String riskFreq) {
        this.riskFreq = riskFreq;
    }

    /**
     * @return instuCde
     */
    public String getInstuCde() {
        return this.instuCde;
    }

    /**
     * @param instuCde
     */
    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }


}