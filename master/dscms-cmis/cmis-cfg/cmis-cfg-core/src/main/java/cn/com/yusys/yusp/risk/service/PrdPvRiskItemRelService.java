/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.risk.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.risk.domain.PrdPvRiskItemRel;
import cn.com.yusys.yusp.risk.repository.mapper.PrdPvRiskItemRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: yusp-npam-biz-core模块
 * @类名称: PrdPvRiskItemRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2020-07-27 17:26:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PrdPvRiskItemRelService {

    @Autowired
    private PrdPvRiskItemRelMapper prdPvRiskItemRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PrdPvRiskItemRel selectByPrimaryKey(String preventId, String itemId) {
        return prdPvRiskItemRelMapper.selectByPrimaryKey(preventId, itemId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PrdPvRiskItemRel> selectAll(QueryModel model) {
        List<PrdPvRiskItemRel> records = (List<PrdPvRiskItemRel>) prdPvRiskItemRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PrdPvRiskItemRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PrdPvRiskItemRel> list = prdPvRiskItemRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PrdPvRiskItemRel record) {
        return prdPvRiskItemRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PrdPvRiskItemRel record) {
        return prdPvRiskItemRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PrdPvRiskItemRel record) {
        return prdPvRiskItemRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PrdPvRiskItemRel record) {
        return prdPvRiskItemRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String preventId, List<String> itemId) {
        itemId.forEach((item) -> {
            prdPvRiskItemRelMapper.deleteByPrimaryKey(preventId, item);
        });
        return itemId.size();
    }

    public int insert(List<String> arr, String preventId) {
        arr.forEach((item) -> {
            prdPvRiskItemRelMapper.insert(new PrdPvRiskItemRel(preventId, item));
        });
        return arr.size();
    }
}
