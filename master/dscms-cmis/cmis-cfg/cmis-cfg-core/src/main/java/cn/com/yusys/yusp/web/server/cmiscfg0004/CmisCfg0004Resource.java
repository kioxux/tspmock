package cn.com.yusys.yusp.web.server.cmiscfg0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.server.cmiscfg0004.req.CmisCfg0004ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0004.resp.CmisCfg0004RespDto;
import cn.com.yusys.yusp.service.server.cmiscfg0004.CmisCfg0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 根据分支机构获取总行行号或BICCODE
 *
 * @author lixy 20210830
 * @version 1.0
 */
@Api(tags = "cmiscfg0004:根据分支机构获取总行行号或BICCODE")
@RestController
@RequestMapping("/api/cfg4inner")
public class CmisCfg0004Resource {

    private static final Logger logger = LoggerFactory.getLogger(CmisCfg0004Resource.class);

    @Autowired
    private CmisCfg0004Service cmisCfg0004Service;
    

    @ApiOperation("根据分支机构获取总行行号或BICCODE")
    @PostMapping("/cmiscfg0004")
    protected @ResponseBody
    ResultDto<CmisCfg0004RespDto> cmisCfg0004(@Validated @RequestBody CmisCfg0004ReqDto reqDto) {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value, JSON.toJSONString(reqDto));
        ResultDto<CmisCfg0004RespDto> CmisCfg0004RespDtoResultDto = new ResultDto<>();
        CmisCfg0004RespDto CmisCfg0004RespDto = new CmisCfg0004RespDto();
        try {
            // 调用对应的service层
            CmisCfg0004RespDto = cmisCfg0004Service.execute(reqDto);

        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value, e.getMessage());
            // 封装CmisCfg0004RespDto中异常返回码和返回信息
            CmisCfg0004RespDto.setErrorCode(EpbEnum.EPB099999.key);
            CmisCfg0004RespDto.setErrorMsg(EpbEnum.EPB099999.value);
        }
        // 封装CmisCfg0004RespDto到CmisCfg0004RespDtoResultDto中
        CmisCfg0004RespDtoResultDto.setData(CmisCfg0004RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value, JSON.toJSONString(CmisCfg0004RespDto));
        return CmisCfg0004RespDtoResultDto;
    }

}
