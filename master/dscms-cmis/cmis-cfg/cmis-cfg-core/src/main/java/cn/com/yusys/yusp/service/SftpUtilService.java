package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.util.SftpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

@Service
public class SftpUtilService {
    private static final Logger log = LoggerFactory.getLogger(SftpUtilService.class);

    /**
     * 上传问文件
     *
     * @param paramMap
     * @return
     */
    public boolean upload(Map<String, String> paramMap) {
        String path = paramMap.get("localFilePath");
        String fileNewName = paramMap.get("fileNewName");
        String ftpPath = paramMap.get("ftpPath");
        String ip = paramMap.get("ip");
        Integer port = Integer.parseInt(paramMap.get("port"));
        String username = paramMap.get("username");
        String password = paramMap.get("password");
        boolean upLoadFlag = Boolean.FALSE;
        try {
            SftpUtil sftpUtil = SftpUtil.getSftpUtil(ip, port, username, password);
            upLoadFlag = sftpUtil.upload(new File(path), fileNewName, ftpPath);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            SftpUtil.release();
        }
        return upLoadFlag;
    }

}