/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.server.cmiscfg0001.req.CmisCfg0001ReqDto;
import cn.com.yusys.yusp.workbench.domain.WbWorkCal;
import cn.com.yusys.yusp.workbench.repository.mapper.WbWorkCalMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbWorkCalService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 21:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class WbWorkCalService {

    @Autowired
    private WbWorkCalMapper wbWorkCalMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public WbWorkCal selectByPrimaryKey(String serno) {
        return wbWorkCalMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<WbWorkCal> selectAll(QueryModel model) {
        List<WbWorkCal> records = (List<WbWorkCal>) wbWorkCalMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<WbWorkCal> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<WbWorkCal> list = wbWorkCalMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(WbWorkCal record) {
        return wbWorkCalMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(WbWorkCal record) {
        return wbWorkCalMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(WbWorkCal record) {
        return wbWorkCalMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(WbWorkCal record) {
        return wbWorkCalMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return wbWorkCalMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return wbWorkCalMapper.deleteByIds(ids);
    }

    /**
     * 闹钟轮询接口 wb_work_cal
     * @param
     */
    public List<WbWorkCal>  alarmClock(QueryModel queryModel){
        Date currDate = new Date();
        // 根据时间查询
        QueryModel model = new QueryModel ();
        Date pre5mDate = DateUtils.add(currDate, Calendar.MINUTE, 5);
        Date pre10mDate = DateUtils.add(currDate, Calendar.MINUTE, 10);
        String currDateStr = DateUtils.formatDateTimeByDef(currDate).substring(0,17);
        String pre5mDateStr = DateUtils.formatDateTimeByDef(pre5mDate).substring(0,17);
        String pre10DateStr = DateUtils.formatDateTimeByDef(pre10mDate).substring(0,17);
        model.addCondition("calendarDate",currDateStr + "00");
        model.addCondition("calendarDatePre5m",pre5mDateStr + "00");
        model.addCondition("calendarDatePre10m",pre10DateStr + "00");
        // 1、获取当前登录人信息
        model.addCondition("inputId",queryModel.getCondition().get("inputId"));
        List<WbWorkCal> list = wbWorkCalMapper.queryAccessCalendarTip(model);
        return list;
    }


}
