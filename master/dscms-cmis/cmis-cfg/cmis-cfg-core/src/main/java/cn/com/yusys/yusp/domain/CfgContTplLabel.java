/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgContTplLabel
 * @类描述: cfg_cont_tpl_label数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-17 20:52:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_cont_tpl_label")
public class CfgContTplLabel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 合同模板编号
     **/
    @Column(name = "CONT_TPL_CODE", unique = false, nullable = false, length = 40)
    private String contTplCode;

    /**
     * 合同模板名称
     **/
    @Column(name = "CONT_TPL_NAME", unique = false, nullable = true, length = 80)
    private String contTplName;

    /**
     * 合同模板类型 STD_ZB_CONT_TPL_TYPE
     **/
    @Column(name = "CONT_TPL_TYPE", unique = false, nullable = true, length = 5)
    private String contTplType;

    /**
     * 模版路径
     **/
    @Column(name = "CONT_TPL_PATH", unique = false, nullable = true, length = 400)
    private String contTplPath;

    /**
     * 文件大小
     **/
    @Column(name = "FILE_SIZE", unique = false, nullable = true, length = 20)
    private String fileSize;

    /**
     * 文件日期
     **/
    @Column(name = "FILE_DATE", unique = false, nullable = true, length = 20)
    private String fileDate;

    /**
     * 模板版本号
     **/
    @Column(name = "TPL_VER", unique = false, nullable = true, length = 5)
    private String tplVer;

    /**
     * 模板状态 STD_ZB_PRD_ST
     **/
    @Column(name = "TPL_STATUS", unique = false, nullable = true, length = 5)
    private String tplStatus;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    public CfgContTplLabel() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return contTplCode
     */
    public String getContTplCode() {
        return this.contTplCode;
    }

    /**
     * @param contTplCode
     */
    public void setContTplCode(String contTplCode) {
        this.contTplCode = contTplCode;
    }

    /**
     * @return contTplName
     */
    public String getContTplName() {
        return this.contTplName;
    }

    /**
     * @param contTplName
     */
    public void setContTplName(String contTplName) {
        this.contTplName = contTplName;
    }

    /**
     * @return contTplType
     */
    public String getContTplType() {
        return this.contTplType;
    }

    /**
     * @param contTplType
     */
    public void setContTplType(String contTplType) {
        this.contTplType = contTplType;
    }

    /**
     * @return contTplPath
     */
    public String getContTplPath() {
        return this.contTplPath;
    }

    /**
     * @param contTplPath
     */
    public void setContTplPath(String contTplPath) {
        this.contTplPath = contTplPath;
    }

    /**
     * @return fileSize
     */
    public String getFileSize() {
        return this.fileSize;
    }

    /**
     * @param fileSize
     */
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @return fileDate
     */
    public String getFileDate() {
        return this.fileDate;
    }

    /**
     * @param fileDate
     */
    public void setFileDate(String fileDate) {
        this.fileDate = fileDate;
    }

    /**
     * @return tplVer
     */
    public String getTplVer() {
        return this.tplVer;
    }

    /**
     * @param tplVer
     */
    public void setTplVer(String tplVer) {
        this.tplVer = tplVer;
    }

    /**
     * @return tplStatus
     */
    public String getTplStatus() {
        return this.tplStatus;
    }

    /**
     * @param tplStatus
     */
    public void setTplStatus(String tplStatus) {
        this.tplStatus = tplStatus;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }


}