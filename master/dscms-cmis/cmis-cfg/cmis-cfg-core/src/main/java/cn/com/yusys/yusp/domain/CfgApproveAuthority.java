/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgApproveAuthority
 * @类描述: cfg_approve_authority数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-05-24 10:32:53
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_approve_authority")
public class CfgApproveAuthority extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 岗位号
     **/
    @Column(name = "DUTY_CODE", unique = false, nullable = true, length = 40)
    private String dutyCode;

    /**
     * 客户等级
     **/
    @Column(name = "CUS_LEVEL", unique = false, nullable = true, length = 5)
    private String cusLevel;

    /**
     * 债项等级
     **/
    @Column(name = "DEBT_LEVEL", unique = false, nullable = true, length = 5)
    private String debtLevel;

    /**
     * 授权金额（元）
     **/
    @Column(name = "AUTH_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal authAmt;

    /**
     * 规则状态
     **/
    @Column(name = "RULE_STATUS", unique = false, nullable = true, length = 40)
    private String ruleStatus;

    /**
     * 区分规则类型
     **/
    @Column(name = "RULE_TYPE", unique = false, nullable = true, length = 40)
    private String ruleType;

    /**
     * 客户类型
     **/
    @Column(name = "CUS_TYPE", unique = false, nullable = true, length = 40)
    private String cusType;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /**
     * 操作类型 STD_ZB_OPER_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgApproveAuthority() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return dutyCode
     */
    public String getDutyCode() {
        return this.dutyCode;
    }

    /**
     * @param dutyCode
     */
    public void setDutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
    }

    /**
     * @return cusLevel
     */
    public String getCusLevel() {
        return this.cusLevel;
    }

    /**
     * @param cusLevel
     */
    public void setCusLevel(String cusLevel) {
        this.cusLevel = cusLevel;
    }

    /**
     * @return debtLevel
     */
    public String getDebtLevel() {
        return this.debtLevel;
    }

    /**
     * @param debtLevel
     */
    public void setDebtLevel(String debtLevel) {
        this.debtLevel = debtLevel;
    }

    /**
     * @return authAmt
     */
    public java.math.BigDecimal getAuthAmt() {
        return this.authAmt;
    }

    /**
     * @param authAmt
     */
    public void setAuthAmt(java.math.BigDecimal authAmt) {
        this.authAmt = authAmt;
    }

    /**
     * @return ruleStatus
     */
    public String getRuleStatus() {
        return this.ruleStatus;
    }

    /**
     * @param ruleStatus
     */
    public void setRuleStatus(String ruleStatus) {
        this.ruleStatus = ruleStatus;
    }

    /**
     * @return ruleType
     */
    public String getRuleType() {
        return this.ruleType;
    }

    /**
     * @param ruleType
     */
    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    /**
     * @return cusType
     */
    public String getCusType() {
        return this.cusType;
    }

    /**
     * @param cusType
     */
    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}