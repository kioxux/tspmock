/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgDiscOrgLmt
 * @类描述: cfg_disc_org_lmt数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-11 17:38:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_disc_org_lmt")
public class CfgDiscOrgLmt extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 申请流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "SERNO")
    private String serno;

    /**
     * 申请年月
     **/
    @Column(name = "APP_DATE", unique = false, nullable = true, length = 7)
    private String appDate;

    /**
     * 机构代码
     **/
    @Column(name = "ORG_CODE", unique = false, nullable = true, length = 40)
    private String orgCode;

    /**
     * 机构名称
     **/
    @Column(name = "ORG_NAME", unique = false, nullable = true, length = 5)
    private String orgName;

    /**
     * 申请额度
     **/
    @Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal appAmt;

    /**
     * 审批额度
     **/
    @Column(name = "APPR_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal apprAmt;

    /**
     * 状态
     **/
    @Column(name = "STATUS", unique = false, nullable = true, length = 5)
    private String status;

    public CfgDiscOrgLmt() {
        // Not compliant
    }

    /**
     * @return serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    /**
     * @return orgCode
     */
    public String getOrgCode() {
        return this.orgCode;
    }

    /**
     * @param orgCode
     */
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    /**
     * @return orgName
     */
    public String getOrgName() {
        return this.orgName;
    }

    /**
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return appAmt
     */
    public java.math.BigDecimal getAppAmt() {
        return this.appAmt;
    }

    /**
     * @param appAmt
     */
    public void setAppAmt(java.math.BigDecimal appAmt) {
        this.appAmt = appAmt;
    }

    /**
     * @return apprAmt
     */
    public java.math.BigDecimal getApprAmt() {
        return this.apprAmt;
    }

    /**
     * @param apprAmt
     */
    public void setApprAmt(java.math.BigDecimal apprAmt) {
        this.apprAmt = apprAmt;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }


}