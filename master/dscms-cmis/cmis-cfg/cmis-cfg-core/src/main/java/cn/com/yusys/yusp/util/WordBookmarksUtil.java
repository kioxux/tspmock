package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.commons.util.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Bookmarks;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class WordBookmarksUtil {

    /**
     * 使用word 2003版本时的文件后缀
     */
    private static final String WORD2003 = "doc";
    /**
     * 使用word 2007版本时的文件后缀
     */
    private static final String WORD2007 = "docx";
    /**
     * 使用excel 2003版本时的文件后缀
     */
    private static final String EXCEL2003 = "xls";
    /**
     * 使用excel 2007版本时的文件后缀
     */
    private static final String EXCEL2007 = "xlsx";
    /**
     * XWPFDocument为word 2007版本使用
     */
    private XWPFDocument xdocument = null;
    /**
     * HWPFDocument为word 2003版本使用
     */
    private HWPFDocument hdocument = null;

    /**
     * 构造方法
     */
    public WordBookmarksUtil() {
        super();
    }

    /**
     * <p>
     * <h2>简述</h2>
     *      <ol>获取书签</ol>
     * <h2>功能描述</h2>
     *      <ol>请添加功能详细的描述</ol>
     * </p>
     *
     * @param filenameWithUrl 包含文件名的文件路径
     * @return
     * @throws Exception
     */
    public List<String> getBookmarks(String filenameWithUrl) throws Exception {
        // 判断包含文件名的路径是否为空
        List<String> bookmarks = null;
        try {
            if (StringUtils.nonBlank(filenameWithUrl)) {
                // 获得文件的文件类型-直接通过路径截取
                String[] s = filenameWithUrl.split("\\.");
                if (s.length > 1) {
                    String fileType = s[1];
                    if (WORD2003.equals(fileType)) {
                        // word 2003版本 .doc
                        bookmarks = this.getBookmarksFromDoc(filenameWithUrl);
                    } else if (WORD2007.equals(fileType)) {
                        // word 2007版本 .docx
                        bookmarks = this.getBookmarksFromDocs(filenameWithUrl);
                    } else if (EXCEL2003.equals(fileType)) {
                        // excel 2003版本 .xls
                        bookmarks = this.getNameFromXls(filenameWithUrl);
                    } else if (EXCEL2007.equals(fileType)) {
                        // excel 2007版本 .xlsx
                        bookmarks = this.getNameFromXlsx(filenameWithUrl);
                    } else {
                        throw new Exception("文件类型只支持 .doc 和 .docx类型的word 文件或.xls和.xlsx类型的excel文件！");
                    }
                } else {
                    throw new Exception("文件路径请包含文件的文件类型");
                }
            } else {
                throw new Exception("包含文件名的文件路径不能为空！");
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
        return bookmarks;
    }

    /**
     * <p>
     * <h2>简述</h2>
     * 		<ol>对文件类型为docx的word文件进行读书签操作</ol>
     * <h2>功能描述</h2>
     * 		<ol>特定为docx文件</ol>
     * </p>
     *
     * @param filenameWithUrl
     * @return
     * @throws IOException
     * @throws Exception
     */
    private List<String> getBookmarksFromDocs(String filenameWithUrl) throws IOException {
        File file = null;
        FileInputStream fis = null;
        try {
            // 首先读取文件，并将文件内容放到XWPFDocument中
            file = new File(filenameWithUrl);
            fis = new FileInputStream(file);
            this.xdocument = new XWPFDocument(fis);
        } finally {
            if (fis != null) {
                fis.close();
                fis = null;
            }
        }
        // 书签集合
        List<String> bookmarknames = new ArrayList<String>();
        Iterator<XWPFParagraph> paraIter = null;
        XWPFParagraph para = null;
        List<CTBookmark> bookmarkList = null;
        Iterator<CTBookmark> bookmarkIter = null;
        CTBookmark bookmark = null;
        // 读取文本中的书签
        paraIter = this.xdocument.getParagraphs().iterator();
        // 对文件内容的段落集合进行迭代
        while (paraIter.hasNext()) {
            para = paraIter.next();
            // 书签是以w:bookmarkStart开头，w:bookmarkEnd结尾的
            bookmarkList = para.getCTP().getBookmarkStartList();
            bookmarkIter = bookmarkList.iterator();

            while (bookmarkIter.hasNext()) {
                // 获得书签的名称
                bookmark = bookmarkIter.next();
                // 在2010版本的 MS
                // word中，会出现_GoBack这种书签，此为word自己隐藏的书签，不属于用户定义的书签，要排除掉
                if (StringUtils.nonBlank(bookmark.getName()) && !bookmark.getName().startsWith("_")) {
                    bookmarknames.add(bookmark.getName());
                }
            }
        }

        // 读取table中的书签
        Iterator<XWPFTable> tableIter = null;
        XWPFTable tabl = null;
        List<XWPFTableRow> rowList = null;
        Iterator<XWPFTableRow> rowIter = null;
        List<XWPFTableCell> cellList = null;
        Iterator<XWPFTableCell> cellIter = null;
        XWPFTableRow row = null;
        XWPFTableCell cell = null;
        tableIter = this.xdocument.getTables().iterator();
        while (tableIter.hasNext()) {
            tabl = tableIter.next();
            rowList = tabl.getRows();
            rowIter = rowList.iterator();
            while (rowIter.hasNext()) {
                row = rowIter.next();
                cellList = row.getTableCells();
                cellIter = cellList.iterator();
                while (cellIter.hasNext()) {
                    cell = cellIter.next();
                    List<XWPFParagraph> paragraphs = cell.getParagraphs();
                    paraIter = paragraphs.iterator();
                    while (paraIter.hasNext()) {
                        para = paraIter.next();
                        bookmarkList = para.getCTP().getBookmarkStartList();
                        bookmarkIter = bookmarkList.iterator();
                        while (bookmarkIter.hasNext()) {
                            // 获得书签的名称
                            bookmark = bookmarkIter.next();
                            // 在2010版本的 MS
                            // word中，会出现_GoBack这种书签，此为word自己隐藏的书签，不属于用户定义的书签，要排除掉
                            // 当word 从docx 另存为doc时会出现这种"_GoBack"格式，以下划线开头的书签
                            if (StringUtils.nonBlank(bookmark.getName()) && !bookmark.getName().startsWith("_")) {
                                bookmarknames.add(bookmark.getName());
                            }
                        }
                    }
                }
            }
        }

        return bookmarknames;
    }

    /**
     * <p>
     * <h2>简述</h2>
     *      <ol>对文件类型为doc的word文件进行读书签操作</ol>
     * <h2>功能描述</h2>
     *      <ol>特定为doc文件</ol>
     * </p>
     *
     * @param filenameWithUrl
     * @return
     * @throws IOException
     * @throws Exception
     */
    private List<String> getBookmarksFromDoc(String filenameWithUrl) throws IOException {
        List<String> bookmarks = new ArrayList<String>();
        File file = null;
        FileInputStream fis = null;
        try {
            // 首先读取文件，并将文件内容放到XWPFDocument中
            file = new File(filenameWithUrl);
            fis = new FileInputStream(file);
            this.hdocument = new HWPFDocument(fis);
            Bookmarks bks = this.hdocument.getBookmarks();
            int count = bks.getBookmarksCount();
            for (int i = 0; i < count; i++) {
                if (StringUtils.nonBlank(String.valueOf(bks.getBookmark(i).getName())) && !String.valueOf(bks.getBookmark(i).getName()).startsWith("_")) {
                    bookmarks.add(String.valueOf(bks.getBookmark(i).getName()));
                }
            }
        } finally {
            if (fis != null) {
                fis.close();
                fis = null;
            }
        }
        return bookmarks;
    }

    /**
     * <p>
     * <h2>简述</h2>
     * 		<ol>读取格式为xls的Excel文件中的类似书签的“定义名称”</ol>
     * <h2>功能描述</h2>
     * 		<ol>请添加功能详细的描述</ol>
     * </p>
     *
     * @param filenameWithUrl 上传文件路径
     * @return 定义名称的集合
     * @throws IOException
     * @throws Exception
     */
    private List<String> getNameFromXls(String filenameWithUrl) throws IOException {
        List<String> bookmarks = new ArrayList<String>();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filenameWithUrl);
            POIFSFileSystem fs = new POIFSFileSystem(fis);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            int numberOfNames = wb.getNumberOfNames();

            for (int i = 0; i < numberOfNames; i++) {
                Name name = wb.getNameAt(i);
                bookmarks.add(name.getNameName());
            }
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
        return bookmarks;
    }

    /**
     * <p>
     * <h2>简述</h2>
     * 		<ol>读取格式为xlsx的Excel文件中的类似书签的“定义名称”</ol>
     * <h2>功能描述</h2>
     * 		<ol>请添加功能详细的描述</ol>
     * </p>
     *
     * @param filenameWithUrl 上传文件路径
     * @return 定义名称的集合
     * @throws IOException
     * @throws InvalidFormatException
     * @throws Exception
     */
    private List<String> getNameFromXlsx(String filenameWithUrl) throws IOException, InvalidFormatException {
        List<String> bookmarks = new ArrayList<String>();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filenameWithUrl);
            OPCPackage pkg = OPCPackage.open(fis);
            XSSFWorkbook wb = new XSSFWorkbook(pkg);
            int numberOfNames = wb.getNumberOfNames();

            for (int i = 0; i < numberOfNames; i++) {
                Name name = wb.getNameAt(i);
                bookmarks.add(name.getNameName());
            }
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
        return bookmarks;
    }

//	public static void main(String[] args) {
//		try {
//			// 对读取Excel表格标题测试
//			InputStream is = new FileInputStream("d:\\test.xlsx");
//			WordBookmarksUtil util = new WordBookmarksUtil();
//			util.getTest(is);
//		} catch (FileNotFoundException e) {
//			EMPLog.log(CMISExtConstance.LOGGER_NAME_CMIS, EMPLog.ERROR, 0, e.getMessage(), e);
//		}
//	}
//	public void getTest(InputStream is) {
//		try {
//			OPCPackage pkg = OPCPackage.open(is);
//			XSSFWorkbook wb = new XSSFWorkbook(pkg);
//			int numberOfNames = wb.getNumberOfNames();
//
//			for (int i = 0; i < numberOfNames; i++) {
//				Name name = wb.getNameAt(i);
//				// System.out.println("sss:" + name.getNameName());
//				EMPLog.log(CMISExtConstance.LOGGER_NAME_CMIS, EMPLog.INFO, 0, name.getNameName());
//			}
//		} catch (Exception e) {
//			EMPLog.log(CMISExtConstance.LOGGER_NAME_CMIS, EMPLog.ERROR, 0, e.getMessage(), e);
//		}
//	}

}
