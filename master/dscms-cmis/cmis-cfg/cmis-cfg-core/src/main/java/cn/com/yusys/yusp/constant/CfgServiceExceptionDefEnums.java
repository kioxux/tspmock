package cn.com.yusys.yusp.constant;

/**
 * 配置信息服务常量类
 */
public enum CfgServiceExceptionDefEnums {

    //默认的;分隔符
    DEF_SPLIT_SEMICOLON(";"),
    //默认的_分隔符
    DEF_SPLIT_UNDERLINE("_"),
    //默认的=分隔符
    DEF_SPLIT_EQUALS("="),

    E_DEF_QUERY_SUCCESS("000000", "查询成功"),
    E_DEF_HANDLE_SUCCESS("000000", "查询成功"),

    E_DEF_QUERY_EXCEPTION("999999", "查询失败！原因："),

    E_CFGEXCHANGERATE_PARAM_ERROR("CER000001", "参数信息获取失败！"),
    E_CFGEXCHANGERATE_NOT_EXISTS_ERROR("CER000002", "查询的汇率信息不存在！"),
    E_CFG_PRD_ORG_REL_EXISTS_ERROR("CFG000001", "该机构已存在，请重新选择！"),
    E_CFG_PRD_BASE_INFO_EXISTS_ERROR("CFG00002", "该数据已被产品引用，不能删除！"),

    E_CFGFLEXQRY_ERROR("CFG000010", "配置参数所在表不在同一数据源中,无法查询，请重新配置！"),


    //对外提供数据流接口查询的异常定义
    E_CFGDATAFLOWOUT_DEF_SUCCESS("000000", "查询成功"),
    E_CFGDATAFLOWOUT_DEF_EXCEPTION("999999", "查询异常！异常原因："),
    E_CFGDATAFLOWOUT_PARAM_EXCEPTION("CDFO000001", "参数缺失！"),
    E_CFGDATAFLOWOUT_DATANOTEXISTS_EXCEPTION("CDFO000002", "数据流配置不存在！"),
    E_CFGDATAFLOWOUT_MAPNOTEXISTS_EXCEPTION("CDFO000003", "数据流映射配置不存在！"),
    E_QUERY_FAILED("ECFG000012", "数据已存在"),

    //对外提供参数配置查询接口的异常定义
    E_CFGPARAMINFO_DEF_EXCEPTION("999999", "cfg服务查询参数配置异常！异常原因："),
    E_CFGPARAMINFO_PARAMSNULL_EXCEPTION("CPIE000001", "查询参数配置-入参为空！"),
    E_CFGPARAMINFO_PARAMINFONULL_EXCEPTION("CPIE000002", "查询参数配置-未获取到指定的参数配置信息！"),
    E_CFGPARAMINFO_PARAMSMORE_EXCEPTION("CPIE000003", "查询参数配置-存在多条生效的参数配置！"),

    INFO_SUCCESS_INSERT("000000", "新增成功"),
    INFO_SUCCESS_UPD("000000", "修改成功"),
    INFO_SUCCESS_DELETE("000000", "删除成功"),
    INFO_SUCCESS_QUERY("000000", "查询成功"),

    E_CFGTPRULEREL_UPDATEOLD_EXCEPTION("999991", "cfg服务变更项目池当前分配规则关联状态时异常！异常原因："),
    E_CFGTPRULEREL_UPDATE_EXCEPTION("999992", "cfg服务更新当前分配规则登记信息时异常！异常原因："),
    E_CFGTPRULEREL_INSERT_EXCEPTION("999993", "cfg服务新增项目池与分配规则关联信息时异常！异常原因："),

    E_TASKPOOL_EXCEPTION_01("TP999901", "项目池未关联分配规则"),
    E_TASKPOOL_EXCEPTION_02("TP999902", "查询项目池关联岗位信息失败");
    /**
     * 查询成功的标记
     **/
    public final static String TP_SUCCESS_FLAG = "ok";
    /**
     * 异常编码
     **/
    private String exceptionCode;
    /**
     * 异常描述
     **/
    private String exceptionDesc;
    //参数值
    private String value;

    CfgServiceExceptionDefEnums(String value) {
        this.value = value;
    }

    CfgServiceExceptionDefEnums(String exceptionCode, String exceptionDesc) {
        this.exceptionCode = exceptionCode;
        this.exceptionDesc = exceptionDesc;
    }

    public String getValue() {
        return value;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public String getExceptionDesc() {
        return exceptionDesc;
    }
}