/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgDataFlow;
import cn.com.yusys.yusp.dto.CfgToBizFlowDataDto;
import cn.com.yusys.yusp.service.CfgDataFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgDataFlowResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-28 17:02:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgdataflow")
public class CfgDataFlowResource {
    @Autowired
    private CfgDataFlowService cfgDataFlowService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgDataFlow>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgDataFlow> list = cfgDataFlowService.selectAll(queryModel);
        return new ResultDto<List<CfgDataFlow>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgDataFlow>> index(QueryModel queryModel) {
        List<CfgDataFlow> list = cfgDataFlowService.selectByModel(queryModel);
        return new ResultDto<List<CfgDataFlow>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{dataFlowId}")
    protected ResultDto<CfgDataFlow> show(@PathVariable("dataFlowId") String dataFlowId) {
        CfgDataFlow cfgDataFlow = cfgDataFlowService.selectByPrimaryKey(dataFlowId);
        return new ResultDto<CfgDataFlow>(cfgDataFlow);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgDataFlow> create(@RequestBody CfgDataFlow cfgDataFlow) throws URISyntaxException {
        cfgDataFlowService.insert(cfgDataFlow);
        return new ResultDto<CfgDataFlow>(cfgDataFlow);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgDataFlow cfgDataFlow) throws URISyntaxException {
        int result = cfgDataFlowService.update(cfgDataFlow);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{dataFlowId}")
    protected ResultDto<Integer> delete(@PathVariable("dataFlowId") String dataFlowId) {
        int result = cfgDataFlowService.deleteByPrimaryKey(dataFlowId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgDataFlowService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 对外提供数据流映射查询的方法
     *
     * @param cfgToBizFlowDataDto
     * @return
     */
    @PostMapping("/queryCfgDataFlowInfo4Out")
    public CfgToBizFlowDataDto queryCfgDataFlowInfo4Out(@RequestBody CfgToBizFlowDataDto cfgToBizFlowDataDto) {
        return cfgDataFlowService.queryCfgDataFlowInfo4Out(cfgToBizFlowDataDto);
    }

    /**
     * 对外提供数据流映射查询的方法-针对list的数据流映射，list映射仅处理映射关系的字段，若是返回的对象中需要增加数据则需要在对应的业务代码中自行处理
     *
     * @param cfgToBizFlowDataDto
     * @return
     */
    @PostMapping("/queryCfgDataFlowInfo4Out4List")
    public CfgToBizFlowDataDto queryCfgDataFlowInfo4Out4List(@RequestBody CfgToBizFlowDataDto cfgToBizFlowDataDto) {
        return cfgDataFlowService.queryCfgDataFlowInfo4Out4List(cfgToBizFlowDataDto);
    }
}
