/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbRiskNotice
 * @类描述: wb_risk_notice数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 21:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "wb_risk_notice")
public class WbRiskNotice extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "SERNO")
    private String serno;

    /**
     * 业务表主键ID
     **/
    @Column(name = "BIZ_PKID", unique = false, nullable = true, length = 40)
    private String bizPkid;

    /**
     * 业务表业务流水号
     **/
    @Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 40)
    private String bizSerno;

    /**
     * 消息类型
     **/
    @Column(name = "MESSAGE_TYPE", unique = false, nullable = true, length = 32)
    private String messageType;

    /**
     * 业务编号
     **/
    @Column(name = "LWA_SERNO", unique = false, nullable = true, length = 40)
    private String lwaSerno;

    /**
     * 业务类别
     **/
    @Column(name = "BUSI_CLS", unique = false, nullable = true, length = 10)
    private String busiCls;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /**
     * 到期日期
     **/
    @Column(name = "END_DATE", unique = false, nullable = true, length = 20)
    private String endDate;

    /**
     * 路由地址
     **/
    @Column(name = "ROUTE_PATH", unique = false, nullable = true, length = 500)
    private String routePath;

    /**
     * 路由参数
     **/
    @Column(name = "ROUTE_PARAM", unique = false, nullable = true, length = 500)
    private String routeParam;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 主管客户经理
     **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 主管机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 操作类型
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /**
     * @return serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno;
    }

    /**
     * @return bizPkid
     */
    public String getbizPkid() {
        return this.bizPkid;
    }

    /**
     * @param bizPkid
     */
    public void setbizPkid(String bizPkid) {
        this.bizPkid = bizPkid;
    }

    /**
     * @return bizSerno
     */
    public String getbizSerno() {
        return this.bizSerno;
    }

    /**
     * @param bizSerno
     */
    public void setbizSerno(String bizSerno) {
        this.bizSerno = bizSerno;
    }

    /**
     * @return messageType
     */
    public String getMessageType() {
        return this.messageType;
    }

    /**
     * @param messageType
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
     * @return lwaSerno
     */
    public String getLwaSerno() {
        return this.lwaSerno;
    }

    /**
     * @param lwaSerno
     */
    public void setLwaSerno(String lwaSerno) {
        this.lwaSerno = lwaSerno;
    }

    /**
     * @return busiCls
     */
    public String getBusiCls() {
        return this.busiCls;
    }

    /**
     * @param busiCls
     */
    public void setBusiCls(String busiCls) {
        this.busiCls = busiCls;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return endDate
     */
    public String getEndDate() {
        return this.endDate;
    }

    /**
     * @param endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return routePath
     */
    public String getRoutePath() {
        return this.routePath;
    }

    /**
     * @param routePath
     */
    public void setRoutePath(String routePath) {
        this.routePath = routePath;
    }

    /**
     * @return routeParam
     */
    public String getRouteParam() {
        return this.routeParam;
    }

    /**
     * @param routeParam
     */
    public void setRouteParam(String routeParam) {
        this.routeParam = routeParam;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }


}