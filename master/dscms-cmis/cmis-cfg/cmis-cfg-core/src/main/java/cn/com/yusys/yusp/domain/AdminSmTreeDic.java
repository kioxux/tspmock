/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: AdminSmTreeDic
 * @类描述: admin_sm_tree_dic数据实体类
 * @功能描述:
 * @创建人: 12651
 * @创建时间: 2021-05-07 16:07:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "admin_sm_tree_dic")
public class AdminSmTreeDic extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 通用代码
     **/
    @Column(name = "EN_NAME", unique = false, nullable = false, length = 20)
    private String enName;

    /**
     * 通用代码描述
     **/
    @Column(name = "CN_NAME", unique = false, nullable = false, length = 90)
    private String cnName;

    /**
     * 上级通用代码
     **/
    @Column(name = "ABVEN_NAME", unique = false, nullable = true, length = 20)
    private String abvenName;

    /**
     * 通用代码层级路径
     **/
    @Column(name = "LOCATE", unique = false, nullable = false, length = 100)
    private String locate;

    /**
     * 代码类别
     **/
    @Column(name = "OPT_TYPE", unique = false, nullable = false, length = 20)
    private String optType;

    /**
     * 代码类别描述
     **/
    @Column(name = "MEMO", unique = false, nullable = true, length = 45)
    private String memo;

    /**
     * 状态    通用类型(NORM_STS) I-无效 A-生效
     **/
    @Column(name = "COM_STS", unique = false, nullable = true, length = 3)
    private String comSts;

    /**
     * 层级标识
     **/
    @Column(name = "LEVELS", unique = false, nullable = true, length = 2)
    private String levels;

    /**
     * 排序字段
     **/
    @Column(name = "ORDERID", unique = false, nullable = true, length = 10)
    private Integer orderid;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    public AdminSmTreeDic() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return enName
     */
    public String getEnName() {
        return this.enName;
    }

    /**
     * @param enName
     */
    public void setEnName(String enName) {
        this.enName = enName;
    }

    /**
     * @return cnName
     */
    public String getCnName() {
        return this.cnName;
    }

    /**
     * @param cnName
     */
    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    /**
     * @return abvenName
     */
    public String getAbvenName() {
        return this.abvenName;
    }

    /**
     * @param abvenName
     */
    public void setAbvenName(String abvenName) {
        this.abvenName = abvenName;
    }

    /**
     * @return locate
     */
    public String getLocate() {
        return this.locate;
    }

    /**
     * @param locate
     */
    public void setLocate(String locate) {
        this.locate = locate;
    }

    /**
     * @return optType
     */
    public String getOptType() {
        return this.optType;
    }

    /**
     * @param optType
     */
    public void setOptType(String optType) {
        this.optType = optType;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return comSts
     */
    public String getComSts() {
        return this.comSts;
    }

    /**
     * @param comSts
     */
    public void setComSts(String comSts) {
        this.comSts = comSts;
    }

    /**
     * @return levels
     */
    public String getLevels() {
        return this.levels;
    }

    /**
     * @param levels
     */
    public void setLevels(String levels) {
        this.levels = levels;
    }

    /**
     * @return orderid
     */
    public Integer getOrderid() {
        return this.orderid;
    }

    /**
     * @param orderid
     */
    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }
}