package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgTaskpoolRule
 * @类描述: cfg_taskpool_rule数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2021-04-08 21:12:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgTaskpoolRuleDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 规则方案编号
     **/
    private String taskpoolRuleId;

    /**
     * 分配方式  STD_ZB_TP_ALLOT_WAY
     **/
    private String allotWay;

    /**
     * 系统分配起始时间
     **/
    private String syatemAllotStartTime;

    /**
     * 系统分配终止时间
     **/
    private String syatemAllotEndTime;

    /**
     * 管理员分配起始时间
     **/
    private String manaAllotStartTime;

    /**
     * 管理员分配终止时间
     **/
    private String manaAllotEndTime;

    /**
     * 分配方法  STD_ZB_TP_ALLOT_MODE
     **/
    private String allotMode;

    /**
     * 是否启用  STD_ZB_YES_NO
     **/
    private String isBegin;

    /**
     * 分配时间间隔
     **/
    private Integer allotTimeInterval;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 最后修改日期
     **/
    private String inputDate;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 最后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    private String oprType;

    /**
     * 关联岗位
     **/
    private String correDuty;

    /**
     * 有权人分配所属岗位
     **/
    private String allotPrivDuty;

    /**
     * 有权人分配所属机构
     **/
    private String allotPrivOrg;

    /**
     * 有权人分配
     **/
    private String allotPriv;

    /**
     * @return TaskpoolRuleId
     */
    public String getTaskpoolRuleId() {
        return this.taskpoolRuleId;
    }

    /**
     * @param taskpoolRuleId
     */
    public void setTaskpoolRuleId(String taskpoolRuleId) {
        this.taskpoolRuleId = taskpoolRuleId == null ? null : taskpoolRuleId.trim();
    }

    /**
     * @return AllotWay
     */
    public String getAllotWay() {
        return this.allotWay;
    }

    /**
     * @param allotWay
     */
    public void setAllotWay(String allotWay) {
        this.allotWay = allotWay == null ? null : allotWay.trim();
    }

    /**
     * @return SyatemAllotStartTime
     */
    public String getSyatemAllotStartTime() {
        return this.syatemAllotStartTime;
    }

    /**
     * @param syatemAllotStartTime
     */
    public void setSyatemAllotStartTime(String syatemAllotStartTime) {
        this.syatemAllotStartTime = syatemAllotStartTime == null ? null : syatemAllotStartTime.trim();
    }

    /**
     * @return SyatemAllotEndTime
     */
    public String getSyatemAllotEndTime() {
        return this.syatemAllotEndTime;
    }

    /**
     * @param syatemAllotEndTime
     */
    public void setSyatemAllotEndTime(String syatemAllotEndTime) {
        this.syatemAllotEndTime = syatemAllotEndTime == null ? null : syatemAllotEndTime.trim();
    }

    /**
     * @return ManaAllotStartTime
     */
    public String getManaAllotStartTime() {
        return this.manaAllotStartTime;
    }

    /**
     * @param manaAllotStartTime
     */
    public void setManaAllotStartTime(String manaAllotStartTime) {
        this.manaAllotStartTime = manaAllotStartTime == null ? null : manaAllotStartTime.trim();
    }

    /**
     * @return ManaAllotEndTime
     */
    public String getManaAllotEndTime() {
        return this.manaAllotEndTime;
    }

    /**
     * @param manaAllotEndTime
     */
    public void setManaAllotEndTime(String manaAllotEndTime) {
        this.manaAllotEndTime = manaAllotEndTime == null ? null : manaAllotEndTime.trim();
    }

    /**
     * @return AllotMode
     */
    public String getAllotMode() {
        return this.allotMode;
    }

    /**
     * @param allotMode
     */
    public void setAllotMode(String allotMode) {
        this.allotMode = allotMode == null ? null : allotMode.trim();
    }

    /**
     * @return IsBegin
     */
    public String getIsBegin() {
        return this.isBegin;
    }

    /**
     * @param isBegin
     */
    public void setIsBegin(String isBegin) {
        this.isBegin = isBegin == null ? null : isBegin.trim();
    }

    /**
     * @return AllotTimeInterval
     */
    public Integer getAllotTimeInterval() {
        return this.allotTimeInterval;
    }

    /**
     * @param allotTimeInterval
     */
    public void setAllotTimeInterval(Integer allotTimeInterval) {
        this.allotTimeInterval = allotTimeInterval;
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }

    /**
     * @return CorreDuty
     */
    public String getCorreDuty() {
        return this.correDuty;
    }

    /**
     * @param correDuty
     */
    public void setCorreDuty(String correDuty) {
        this.correDuty = correDuty == null ? null : correDuty.trim();
    }

    /**
     * @return AllotPrivDuty
     */
    public String getAllotPrivDuty() {
        return this.allotPrivDuty;
    }

    /**
     * @param allotPrivDuty
     */
    public void setAllotPrivDuty(String allotPrivDuty) {
        this.allotPrivDuty = allotPrivDuty == null ? null : allotPrivDuty.trim();
    }

    /**
     * @return AllotPrivOrg
     */
    public String getAllotPrivOrg() {
        return this.allotPrivOrg;
    }

    /**
     * @param allotPrivOrg
     */
    public void setAllotPrivOrg(String allotPrivOrg) {
        this.allotPrivOrg = allotPrivOrg == null ? null : allotPrivOrg.trim();
    }

    /**
     * @return AllotPriv
     */
    public String getAllotPriv() {
        return this.allotPriv;
    }

    /**
     * @param allotPriv
     */
    public void setAllotPriv(String allotPriv) {
        this.allotPriv = allotPriv == null ? null : allotPriv.trim();
    }


}