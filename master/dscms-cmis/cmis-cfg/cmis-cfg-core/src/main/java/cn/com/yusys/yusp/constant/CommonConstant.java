package cn.com.yusys.yusp.constant;

public class CommonConstant {

    /**
     * 01-新增
     **/
    public static final String ADD_OPR = "01";

    /**
     * 02-删除
     **/
    public static final String DELETE_OPR = "02";

    /**
     * 配置类型
     * 01 资产抵押率配置
     * 02 信用等级配置
     */
    public static final String CFG_TYPE_01 = "01";
    public static final String CFG_TYPE_02 = "02";

    //日期格式化
    public static final String DATE_FORMAT_YMD = "yyyy-MM-dd";

}
