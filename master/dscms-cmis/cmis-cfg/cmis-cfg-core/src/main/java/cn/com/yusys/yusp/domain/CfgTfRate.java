/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgTfRate
 * @类描述: cfg_tf_rate数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-11 09:46:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_tf_rate")
public class CfgTfRate extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 币种编号
     **/
    @Column(name = "CRCYCD", unique = false, nullable = true, length = 2)
    private String crcycd;

    /**
     * 开始日期
     **/
    @Column(name = "START_DATE", unique = false, nullable = true, length = 20)
    private String startDate;

    /**
     * 时间
     **/
    @Column(name = "TIME", unique = false, nullable = true, length = 20)
    private String time;

    /**
     * 结束日期
     **/
    @Column(name = "END_TIME", unique = false, nullable = true, length = 20)
    private String endTime;

    /**
     * 结汇汇率
     **/
    @Column(name = "EXRTST", unique = false, nullable = true, length = 1)
    private String exrtst;

    /**
     * 中文币种
     **/
    @Column(name = "CUR_TYPE_NAME", unique = false, nullable = true, length = 20)
    private String curTypeName;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 20)
    private String curType;

    /**
     * 基准价
     **/
    @Column(name = "EXUNIT", unique = false, nullable = true, length = 10)
    private Integer exunit;

    /**
     * 汇买价
     **/
    @Column(name = "CSBYPR", unique = false, nullable = true, length = 18)
    private java.math.BigDecimal csbypr;

    /**
     * 汇卖价
     **/
    @Column(name = "CSSLPR", unique = false, nullable = true, length = 18)
    private java.math.BigDecimal csslpr;

    /**
     * 钞买价
     **/
    @Column(name = "EXBYPR", unique = false, nullable = true, length = 18)
    private java.math.BigDecimal exbypr;

    /**
     * 钞卖价
     **/
    @Column(name = "EXSLPR", unique = false, nullable = true, length = 18)
    private java.math.BigDecimal exslpr;

    /**
     * 中间价
     **/
    @Column(name = "MIDDPR", unique = false, nullable = true, length = 18)
    private java.math.BigDecimal middpr;

    /**
     * WGMDPR
     **/
    @Column(name = "WGMDPR", unique = false, nullable = true, length = 18)
    private java.math.BigDecimal wgmdpr;

    /**
     * 汇率
     **/
    @Column(name = "RATE", unique = false, nullable = true, length = 18)
    private java.math.BigDecimal rate;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 责任人
     **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 责任机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public CfgTfRate() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return crcycd
     */
    public String getCrcycd() {
        return this.crcycd;
    }

    /**
     * @param crcycd
     */
    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    /**
     * @return startDate
     */
    public String getStartDate() {
        return this.startDate;
    }

    /**
     * @param startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return time
     */
    public String getTime() {
        return this.time;
    }

    /**
     * @param time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return endTime
     */
    public String getEndTime() {
        return this.endTime;
    }

    /**
     * @param endTime
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return exrtst
     */
    public String getExrtst() {
        return this.exrtst;
    }

    /**
     * @param exrtst
     */
    public void setExrtst(String exrtst) {
        this.exrtst = exrtst;
    }

    /**
     * @return curTypeName
     */
    public String getCurTypeName() {
        return this.curTypeName;
    }

    /**
     * @param curTypeName
     */
    public void setCurTypeName(String curTypeName) {
        this.curTypeName = curTypeName;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return exunit
     */
    public Integer getExunit() {
        return this.exunit;
    }

    /**
     * @param exunit
     */
    public void setExunit(Integer exunit) {
        this.exunit = exunit;
    }

    /**
     * @return csbypr
     */
    public java.math.BigDecimal getCsbypr() {
        return this.csbypr;
    }

    /**
     * @param csbypr
     */
    public void setCsbypr(java.math.BigDecimal csbypr) {
        this.csbypr = csbypr;
    }

    /**
     * @return csslpr
     */
    public java.math.BigDecimal getCsslpr() {
        return this.csslpr;
    }

    /**
     * @param csslpr
     */
    public void setCsslpr(java.math.BigDecimal csslpr) {
        this.csslpr = csslpr;
    }

    /**
     * @return exbypr
     */
    public java.math.BigDecimal getExbypr() {
        return this.exbypr;
    }

    /**
     * @param exbypr
     */
    public void setExbypr(java.math.BigDecimal exbypr) {
        this.exbypr = exbypr;
    }

    /**
     * @return exslpr
     */
    public java.math.BigDecimal getExslpr() {
        return this.exslpr;
    }

    /**
     * @param exslpr
     */
    public void setExslpr(java.math.BigDecimal exslpr) {
        this.exslpr = exslpr;
    }

    /**
     * @return middpr
     */
    public java.math.BigDecimal getMiddpr() {
        return this.middpr;
    }

    /**
     * @param middpr
     */
    public void setMiddpr(java.math.BigDecimal middpr) {
        this.middpr = middpr;
    }

    /**
     * @return wgmdpr
     */
    public java.math.BigDecimal getWgmdpr() {
        return this.wgmdpr;
    }

    /**
     * @param wgmdpr
     */
    public void setWgmdpr(java.math.BigDecimal wgmdpr) {
        this.wgmdpr = wgmdpr;
    }

    /**
     * @return rate
     */
    public java.math.BigDecimal getRate() {
        return this.rate;
    }

    /**
     * @param rate
     */
    public void setRate(java.math.BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }


}