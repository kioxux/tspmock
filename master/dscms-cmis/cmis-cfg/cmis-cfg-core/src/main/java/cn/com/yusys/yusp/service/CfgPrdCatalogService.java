/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgPrdBasicinfo;
import cn.com.yusys.yusp.domain.CfgPrdCatalog;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoTranDto;
import cn.com.yusys.yusp.dto.CfgPrdCatalogBasicInfoDto;
import cn.com.yusys.yusp.dto.CfgPrdCatalogDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfDto;
import cn.com.yusys.yusp.repository.mapper.CfgPrdBasicinfoMapper;
import cn.com.yusys.yusp.repository.mapper.CfgPrdCatalogMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdCatalogService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2021-04-06 21:29:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgPrdCatalogService {

    @Autowired
    private CfgPrdCatalogMapper cfgPrdCatalogMapper;

    @Autowired
    private CfgPrdBasicinfoMapper cfgPrdBasicinfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgPrdCatalog selectByPrimaryKey(String catalogId) {
        return cfgPrdCatalogMapper.selectByPrimaryKey(catalogId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgPrdCatalog> selectAll(QueryModel model) {
        List<CfgPrdCatalog> records = (List<CfgPrdCatalog>) cfgPrdCatalogMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgPrdCatalog> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPrdCatalog> list = cfgPrdCatalogMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgPrdCatalog record) {
        return cfgPrdCatalogMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgPrdCatalog record) {
        return cfgPrdCatalogMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgPrdCatalog record) {
        return cfgPrdCatalogMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgPrdCatalog record) {
        return cfgPrdCatalogMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String catalogId) {
        return cfgPrdCatalogMapper.deleteByPrimaryKey(catalogId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgPrdCatalogMapper.deleteByIds(ids);
    }

    public List<LmtSubPrdMappConfDto> getLmtSubPrdMappConf(List<CfgPrdCatalogDto> cfgPrdCatalogDtos) {
        return cfgPrdCatalogMapper.getLmtSubPrdMappConf(cfgPrdCatalogDtos);
    }

    public List<CfgPrdCatalogBasicInfoDto> getCfgPrdCatalogBasicInfo(QueryModel model) {
        return cfgPrdCatalogMapper.getCfgPrdCatalogBasicInfo(model);
    }

    /***
     * @param id 根节点ID
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author tangxun
     * @date 2021/4/26 10:01 上午
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CfgPrdBasicinfoTranDto> queryCfgPrdCatalogTree(String id) {
        QueryModel model = new QueryModel();
        List<CfgPrdCatalog> list = cfgPrdCatalogMapper.selectByModel(model);
        //将所有对象放到map中 格式是 <pid,List<Node>>
        Map<String, ArrayList<CfgPrdCatalog>> allMap = new HashMap<>(16);
        for (CfgPrdCatalog item : list) {
            if (item.getSupCatalogId() == null) {
                item.setSupCatalogId("");
            }
            if (allMap.containsKey(item.getSupCatalogId())) {
                allMap.get(item.getSupCatalogId()).add(item);
            } else {
                ArrayList<CfgPrdCatalog> sublist = new ArrayList<>();
                sublist.add(item);
                allMap.put(item.getSupCatalogId(), sublist);
            }
        }
        return getTree(id, allMap);
    }


    /**
     * @param root, all]
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author tangxun
     * @date 2021/1/12 9:52
     * @version 1.0.0
     * @desc 组装前端要求的格式
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private List<CfgPrdBasicinfoTranDto> getTree(String root, Map<String, ArrayList<CfgPrdCatalog>> all) {
        if (all.containsKey(root)) {
            return all.get(root).stream().map(item -> {
                CfgPrdBasicinfoTranDto cfgPrdBasicinfoDto = new CfgPrdBasicinfoTranDto();
                cfgPrdBasicinfoDto.setId(item.getCatalogId());
                cfgPrdBasicinfoDto.setLabel(item.getCatalogName());
                List<CfgPrdBasicinfoTranDto> list = getTree(item.getCatalogId(), all);
                cfgPrdBasicinfoDto.setChildren(list);
                return cfgPrdBasicinfoDto;
            }).collect(Collectors.toList());
        } else {
            QueryModel model = new QueryModel();
            model.addCondition("catalogId", root);
            List<CfgPrdBasicinfo> list = cfgPrdBasicinfoMapper.selectByModelToDomain(model);
            return list.stream().map(item -> {
                CfgPrdBasicinfoTranDto cfgPrdBasicinfoDto = new CfgPrdBasicinfoTranDto();
                cfgPrdBasicinfoDto.setId(item.getPrdId());
                cfgPrdBasicinfoDto.setLabel(item.getPrdName());
                cfgPrdBasicinfoDto.setCatalogId(item.getCatalogId());
                return cfgPrdBasicinfoDto;
            }).collect(Collectors.toList());
        }
    }
}
