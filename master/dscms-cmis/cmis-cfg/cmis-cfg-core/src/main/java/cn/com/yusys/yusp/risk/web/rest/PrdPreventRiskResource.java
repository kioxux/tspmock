/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.risk.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.risk.domain.PrdPreventRisk;
import cn.com.yusys.yusp.risk.service.PrdPreventRiskService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: yusp-npam-biz-core模块
 * @类名称: PrdPreventRiskResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2020-07-27 17:26:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(description = "风险拦截方案")
@RestController
@RequestMapping("/api/prdpreventrisk")
public class PrdPreventRiskResource {
    @Autowired
    private PrdPreventRiskService prdPreventRiskService;


    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PrdPreventRisk>> query() {
        QueryModel queryModel = new QueryModel();
        List<PrdPreventRisk> list = prdPreventRiskService.selectAll(queryModel);
        return new ResultDto<List<PrdPreventRisk>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PrdPreventRisk>> index(QueryModel queryModel) {
        List<PrdPreventRisk> list = prdPreventRiskService.selectByModel(queryModel);
        return new ResultDto<List<PrdPreventRisk>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{preventId}")
    protected ResultDto<PrdPreventRisk> show(@PathVariable("preventId") String preventId) {
        PrdPreventRisk prdPreventRisk = prdPreventRiskService.selectByPrimaryKey(preventId);
        return new ResultDto<PrdPreventRisk>(prdPreventRisk);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PrdPreventRisk> create(@RequestBody PrdPreventRisk prdPreventRisk) throws URISyntaxException {
        prdPreventRiskService.insert(prdPreventRisk);
        return new ResultDto<PrdPreventRisk>(prdPreventRisk);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PrdPreventRisk prdPreventRisk) throws URISyntaxException {
        int result = prdPreventRiskService.update(prdPreventRisk);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{preventId}")
    protected ResultDto<Integer> delete(@PathVariable("preventId") String preventId) {
        int result = prdPreventRiskService.deleteByPrimaryKey(preventId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = prdPreventRiskService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
