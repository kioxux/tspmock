/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CfgRetailPrimeRateDto;
import cn.com.yusys.yusp.dto.CfgWyClassRelClientDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgWyClassRel;
import cn.com.yusys.yusp.service.CfgWyClassRelService;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgWyClassRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-07-29 10:45:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgwyclassrel")
public class CfgWyClassRelResource {
    @Autowired
    private CfgWyClassRelService cfgWyClassRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgWyClassRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgWyClassRel> list = cfgWyClassRelService.selectAll(queryModel);
        return new ResultDto<List<CfgWyClassRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgWyClassRel>> index(QueryModel queryModel) {
        List<CfgWyClassRel> list = cfgWyClassRelService.selectByModel(queryModel);
        return new ResultDto<List<CfgWyClassRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgWyClassRel> show(@PathVariable("pkId") String pkId) {
        CfgWyClassRel cfgWyClassRel = cfgWyClassRelService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgWyClassRel>(cfgWyClassRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgWyClassRel> create(@RequestBody CfgWyClassRel cfgWyClassRel) throws URISyntaxException {
        cfgWyClassRelService.insert(cfgWyClassRel);
        return new ResultDto<CfgWyClassRel>(cfgWyClassRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgWyClassRel cfgWyClassRel) throws URISyntaxException {
        int result = cfgWyClassRelService.update(cfgWyClassRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgWyClassRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgWyClassRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: selectByCondition
     * @函数描述: 根据贷款用途，产品代码，是否农业，是否长期查询网银支用科目对应关系
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbycondition")
    public ResultDto<CfgWyClassRelClientDto> selectByCondition(@RequestBody CfgWyClassRelClientDto cfgWyClassRelClientDto) {
        CfgWyClassRel cfgWyClassRel = cfgWyClassRelService.selectByCondition(cfgWyClassRelClientDto);
        BeanUtils.copyProperties(cfgWyClassRel, cfgWyClassRelClientDto);
        return new ResultDto<CfgWyClassRelClientDto>(cfgWyClassRelClientDto);
    }
}
