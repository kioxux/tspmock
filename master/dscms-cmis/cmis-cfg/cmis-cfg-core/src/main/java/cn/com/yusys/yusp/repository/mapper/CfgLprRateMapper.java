/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgLprRate;
import cn.com.yusys.yusp.dto.CfgLprRateDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgLprRateMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-15 18:51:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CfgLprRateMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CfgLprRate selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgLprRate> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CfgLprRate record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CfgLprRate record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CfgLprRate record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CfgLprRate record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);


    /**
     * @创建人 WH
     * @创建时间 2021/6/15 19:02
     * @注释 返回两条利率
     */
    List<CfgLprRate> selectlpr(CfgLprRate cfgLprRate);

    /**
     * @创建人 WH
     * @创建时间 2021/6/16 11:27
     * @注释 查询单条数据
     */
    CfgLprRateDto selectone(QueryModel queryModel);

    /**
     * @创建人 shenli
     * @创建时间 2021-9-17 17:20:08
     * @注释 根据签订日期找之后最近的lpr
     */
    CfgLprRateDto selectAfterLpr(CfgLprRateDto cfgLprRateDto);

    /**
     * @创建人 shenli
     * @创建时间 2021-9-17 17:20:03
     * @注释 根据签订日期找之前最近的lpr
     */
    CfgLprRateDto selectFrontLpr(CfgLprRateDto cfgLprRateDto);

}