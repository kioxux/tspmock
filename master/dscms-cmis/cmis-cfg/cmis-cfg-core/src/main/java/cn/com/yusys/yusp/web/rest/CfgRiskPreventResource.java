package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/api/cfgriskprevent")
public class CfgRiskPreventResource {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @GetMapping("/beanNameSet")
    protected ResultDto<Set<String>> beanNameSet() {
        Set<String> set = redisTemplate.opsForSet().members(CmisCommonConstants.RISK_PREVENT_BEAN_REDIS_KEY);
        return new ResultDto<Set<String>>(set);
    }
}
