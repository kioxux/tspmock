/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgPrdFileTplRel;
import cn.com.yusys.yusp.dto.CfgPrdFileTplRelDto;
import cn.com.yusys.yusp.service.CfgPrdFileTplRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdFileTplRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-07 11:27:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgprdfiletplrel")
public class CfgPrdFileTplRelResource {
    @Autowired
    private CfgPrdFileTplRelService cfgPrdFileTplRelService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgPrdFileTplRelDto>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgPrdFileTplRelDto> list = cfgPrdFileTplRelService.selectAll(queryModel);
        return new ResultDto<List<CfgPrdFileTplRelDto>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgPrdFileTplRelDto>> index(QueryModel queryModel) {
        List<CfgPrdFileTplRelDto> list = cfgPrdFileTplRelService.selectByModel(queryModel);
        return new ResultDto<List<CfgPrdFileTplRelDto>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgPrdFileTplRel> show(@PathVariable("pkId") String pkId) {
        CfgPrdFileTplRel cfgPrdFileTplRel = cfgPrdFileTplRelService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgPrdFileTplRel>(cfgPrdFileTplRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgPrdFileTplRel> create(@RequestBody CfgPrdFileTplRel cfgPrdFileTplRel) throws URISyntaxException {
        cfgPrdFileTplRelService.insert(cfgPrdFileTplRel);
        return new ResultDto<CfgPrdFileTplRel>(cfgPrdFileTplRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgPrdFileTplRel cfgPrdFileTplRel) throws URISyntaxException {
        int result = cfgPrdFileTplRelService.update(cfgPrdFileTplRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgPrdFileTplRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgPrdFileTplRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
