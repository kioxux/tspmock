package cn.com.yusys.yusp.service.server.xdsx0009;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCfgConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CfgInLimitRule;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CfgInLimitRuleMapper;
import cn.com.yusys.yusp.server.xdsx0009.req.Xdsx0009DataReqDto;
import cn.com.yusys.yusp.server.xdsx0009.resp.Xdsx0009DataRespDto;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdsx0009Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-24 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdsx0009Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0009Service.class);

    @Resource
    private CfgInLimitRuleMapper cfgInLimitRuleMapper;

    /**
     * 客户准入级别同步
     *
     * @param xdsx0009DataReqDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Xdsx0009DataRespDto getXdsx0009(Xdsx0009DataReqDto xdsx0009DataReqDto) throws ParseException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, JSON.toJSONString(xdsx0009DataReqDto));
        Xdsx0009DataRespDto xdsx0009DataRespDto = new Xdsx0009DataRespDto();
        try {
            List<cn.com.yusys.yusp.server.xdsx0009.req.MapsetList> mapsetLists = xdsx0009DataReqDto.getMapsetList();

            mapsetLists.stream().forEach(
                    mapsetList -> {
                        CfgInLimitRule cfgInLimitRule = new CfgInLimitRule();
                        String uuid = UUID.randomUUID().toString().replace("-", "");
                        cfgInLimitRule.setPkId(uuid);
                        String modelId = mapsetList.getEvalModel();
                        //将之前的规则全部置为无效状态
                        cfgInLimitRuleMapper.updateRuleStatusByModelId(modelId);
                        //模型ID
                        cfgInLimitRule.setRuleStatus(CmisCfgConstants.RULE_STATUS_1);
                        cfgInLimitRule.setModelId(modelId);

                        //更新时间
                        String upDateTime = mapsetList.getSyncDate();
                        //内评一次按岗位编号将所有数据同步过来，
                        //将历史数据设置为无效
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date updateDate = null;
                        try {
                            updateDate = simpleDateFormat.parse(upDateTime);
                        } catch (ParseException e) {
                            logger.error("更新时间解析异常");
                            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
                        }
                        cfgInLimitRule.setCreateTime(updateDate);
                        cfgInLimitRule.setRuleInValue(String.valueOf(mapsetList.getCusEvalAdmitLine()));
                        cfgInLimitRule.setOprType(CmisBizConstants.OPR_TYPE_01);
                        cfgInLimitRuleMapper.insertSelective(cfgInLimitRule);
                    }
            );

            xdsx0009DataRespDto.setOpFlag(CmisCfgConstants.OP_FLAG_S);
            xdsx0009DataRespDto.setOpMsg(CmisCfgConstants.OP_MSG_S);
        } catch (BizException e) {
            xdsx0009DataRespDto.setOpFlag(CmisCfgConstants.OP_FLAG_F);
            xdsx0009DataRespDto.setOpMsg(CmisCfgConstants.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdsx0009DataRespDto.setOpFlag(CmisCfgConstants.OP_FLAG_F);
            xdsx0009DataRespDto.setOpMsg(CmisCfgConstants.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, JSON.toJSONString(xdsx0009DataRespDto));
        return xdsx0009DataRespDto;
    }

}
