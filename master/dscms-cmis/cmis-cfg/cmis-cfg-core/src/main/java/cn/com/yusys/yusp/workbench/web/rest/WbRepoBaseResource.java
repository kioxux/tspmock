/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.workbench.domain.WbRepoBase;
import cn.com.yusys.yusp.workbench.service.WbRepoBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbRepoBaseResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-15 15:31:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/wbrepobase")
public class WbRepoBaseResource {
    @Autowired
    private WbRepoBaseService wbRepoBaseService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<WbRepoBase>> query() {
        QueryModel queryModel = new QueryModel();
        List<WbRepoBase> list = wbRepoBaseService.selectAll(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<WbRepoBase>> index(@RequestBody QueryModel queryModel) {
        List<WbRepoBase> list = wbRepoBaseService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<WbRepoBase> show(@PathVariable("serno") String serno) {
        WbRepoBase wbRepoBase = wbRepoBaseService.selectByPrimaryKey(serno);
        return new ResultDto<>(wbRepoBase);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<WbRepoBase> create(@RequestBody WbRepoBase wbRepoBase) throws URISyntaxException {
        wbRepoBaseService.insertSelective(wbRepoBase);
        return new ResultDto<>(wbRepoBase);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody WbRepoBase wbRepoBase) throws URISyntaxException {
        int result = wbRepoBaseService.updateSelective(wbRepoBase);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = wbRepoBaseService.deleteByPrimaryKey(serno);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = wbRepoBaseService.deleteByIds(ids);
        return new ResultDto<>(result);
    }
}
