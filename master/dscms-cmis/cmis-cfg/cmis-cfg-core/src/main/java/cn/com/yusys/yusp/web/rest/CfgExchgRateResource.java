/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgExchgRate;
import cn.com.yusys.yusp.dto.CfgExchgRateDto;
import cn.com.yusys.yusp.service.CfgExchgRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgExchgRateResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-12 15:29:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgexchgrate")
public class CfgExchgRateResource {
    @Autowired
    private CfgExchgRateService cfgExchgRateService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgExchgRate>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgExchgRate> list = cfgExchgRateService.selectAll(queryModel);
        return new ResultDto<List<CfgExchgRate>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgExchgRate>> index(QueryModel queryModel) {
        List<CfgExchgRate> list = cfgExchgRateService.selectByModel(queryModel);
        return new ResultDto<List<CfgExchgRate>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgExchgRate> show(@PathVariable("pkId") String pkId) {
        CfgExchgRate cfgExchgRate = cfgExchgRateService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgExchgRate>(cfgExchgRate);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgExchgRate> create(@RequestBody CfgExchgRate cfgExchgRate) throws URISyntaxException {
        cfgExchgRateService.insert(cfgExchgRate);
        return new ResultDto<CfgExchgRate>(cfgExchgRate);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgExchgRate cfgExchgRate) throws URISyntaxException {
        int result = cfgExchgRateService.update(cfgExchgRate);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgExchgRateService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgExchgRateService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 通过币种查询实时汇率信息
     *
     * @param cfgExchgRateDto
     * @return
     */
    @PostMapping("/queryRatioByCurType")
    public CfgExchgRateDto queryRatioByCurType(@RequestBody CfgExchgRateDto cfgExchgRateDto) {
        return cfgExchgRateService.queryRatioByCurType(cfgExchgRateDto);
    }
}
