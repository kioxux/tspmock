/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgContTplLabel;
import cn.com.yusys.yusp.service.CfgContTplLabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgContTplLabelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-17 20:52:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgconttpllabel")
public class CfgContTplLabelResource {
    @Autowired
    private CfgContTplLabelService cfgContTplLabelService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgContTplLabel>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgContTplLabel> list = cfgContTplLabelService.selectAll(queryModel);
        return new ResultDto<List<CfgContTplLabel>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgContTplLabel>> index(QueryModel queryModel) {
        List<CfgContTplLabel> list = cfgContTplLabelService.selectByModel(queryModel);
        return new ResultDto<List<CfgContTplLabel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgContTplLabel> show(@PathVariable("pkId") String pkId) {
        CfgContTplLabel cfgContTplLabel = cfgContTplLabelService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgContTplLabel>(cfgContTplLabel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgContTplLabel> create(@RequestBody CfgContTplLabel cfgContTplLabel) throws URISyntaxException {
        cfgContTplLabelService.insert(cfgContTplLabel);
        return new ResultDto<CfgContTplLabel>(cfgContTplLabel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgContTplLabel cfgContTplLabel) throws URISyntaxException {
        int result = cfgContTplLabelService.update(cfgContTplLabel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgContTplLabelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgContTplLabelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 1。传文件 2。读取 书签
     *
     * @param file
     * @param fileName
     * @return
     * @throws Exception
     */
    @PostMapping("/upload")
    public ResultDto<Map<String, String>> uploadFile(MultipartFile file, String fileName) throws Exception {
        Map<String, String> fileInfo = cfgContTplLabelService.uploadFilesInfo(file);
        return ResultDto.success(fileInfo);
    }


    /**
     * 保存修改后的文件到服务器上
     *
     * @param file 文件
     * @param pkId 修改的模版的主键
     * @return
     * @throws Exception
     */
    @PostMapping("/save")
    public String saveFile(@RequestParam(value = "docFile") MultipartFile file, @RequestParam(value = "pkId") String pkId) throws Exception {
        String result = cfgContTplLabelService.saveFilesInfo(file, pkId);
        return result;
    }


    /**
     * 通过模版号获取模版信息
     *
     * @param contTplCode
     * @return
     */
    @GetMapping("/getcontcfg/{contTplCode}")
    public ResultDto<CfgContTplLabel> getContCfg(@PathVariable("contTplCode") String contTplCode) {
        CfgContTplLabel record = cfgContTplLabelService.getContCfg(contTplCode);
        return new ResultDto<CfgContTplLabel>(record);
    }

}
