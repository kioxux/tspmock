/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSxkdGuarDiscount
 * @类描述: cfg_sxkd_guar_discount数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-12 15:32:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_sxkd_guar_discount")
public class CfgSxkdGuarDiscount extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 抵押物类型 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "GUAR_TYPE_CD")
	private String guarTypeCd;
	
	/** 抵押物名称 **/
	@Column(name = "GUAR_NAME", unique = false, nullable = false, length = 50)
	private String guarName;
	
	/** 折率 **/
	@Column(name = "DISCOUNT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal discountRate;
	
	/** 有效标志(Y:有效, N:无效) **/
	@Column(name = "USED_IND", unique = false, nullable = true, length = 2)
	private String usedInd;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}
	
    /**
     * @return guarTypeCd
     */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}
	
	/**
	 * @param guarName
	 */
	public void setGuarName(String guarName) {
		this.guarName = guarName;
	}
	
    /**
     * @return guarName
     */
	public String getGuarName() {
		return this.guarName;
	}
	
	/**
	 * @param discountRate
	 */
	public void setDiscountRate(java.math.BigDecimal discountRate) {
		this.discountRate = discountRate;
	}
	
    /**
     * @return discountRate
     */
	public java.math.BigDecimal getDiscountRate() {
		return this.discountRate;
	}
	
	/**
	 * @param usedInd
	 */
	public void setUsedInd(String usedInd) {
		this.usedInd = usedInd;
	}
	
    /**
     * @return usedInd
     */
	public String getUsedInd() {
		return this.usedInd;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}