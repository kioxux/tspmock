/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgFlexQryDetail;
import cn.com.yusys.yusp.service.CfgFlexQryDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryDetailResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-28 20:43:08
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgflexqrydetail")
public class CfgFlexQryDetailResource {
    @Autowired
    private CfgFlexQryDetailService cfgFlexQryDetailService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgFlexQryDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgFlexQryDetail> list = cfgFlexQryDetailService.selectAll(queryModel);
        return new ResultDto<List<CfgFlexQryDetail>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgFlexQryDetail>> index(QueryModel queryModel) {
        List<CfgFlexQryDetail> list = cfgFlexQryDetailService.selectByModel(queryModel);
        return new ResultDto<List<CfgFlexQryDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgFlexQryDetail> show(@PathVariable("pkId") String pkId) {
        CfgFlexQryDetail cfgFlexQryDetail = cfgFlexQryDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgFlexQryDetail>(cfgFlexQryDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgFlexQryDetail> create(@RequestBody CfgFlexQryDetail cfgFlexQryDetail) throws URISyntaxException {
        cfgFlexQryDetailService.insert(cfgFlexQryDetail);
        return new ResultDto<CfgFlexQryDetail>(cfgFlexQryDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgFlexQryDetail cfgFlexQryDetail) throws URISyntaxException {
        int result = cfgFlexQryDetailService.update(cfgFlexQryDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgFlexQryDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgFlexQryDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchInsert
     * @函数描述:指标详情批量保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/detailbatchInsert")
    protected ResultDto<Integer> batchInsert(@RequestBody Map params) throws URISyntaxException {
        List<CfgFlexQryDetail> recordList = (List) params.get("dataList");
        int result = cfgFlexQryDetailService.batchInsert(recordList);
        return new ResultDto<Integer>(result);
    }

}
