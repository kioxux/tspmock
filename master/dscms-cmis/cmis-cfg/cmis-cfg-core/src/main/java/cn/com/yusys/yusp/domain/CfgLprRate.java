/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgLprRate
 * @类描述: cfg_lpr_rate数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-15 18:51:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_lpr_rate")
public class CfgLprRate extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 利率类型代码
     **/
    @Column(name = "RATE_TYPE_ID", unique = false, nullable = true, length = 5)
    private String rateTypeId;

    /**
     * 利率类型
     **/
    @Column(name = "RATE_TYPE_NAME", unique = false, nullable = true, length = 5)
    private String rateTypeName;

    /**
     * 利率值
     **/
    @Column(name = "RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal rate;

    /**
     * 生效日期
     **/
    @Column(name = "VALID_DATE", unique = false, nullable = true, length = 10)
    private String validDate;

    /**
     * 利率基准
     **/
    @Column(name = "RATE_DATUM", unique = false, nullable = true, length = 1)
    private String rateDatum;

    /**
     * 是否启用
     **/
    @Column(name = "USED_IND", unique = false, nullable = true, length = 5)
    private String usedInd;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 利率名称
     **/
    @Column(name = "RATE_NAME", unique = false, nullable = true, length = 100)
    private String rateName;

    /**
     * 操作类型
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public CfgLprRate() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return rateTypeId
     */
    public String getRateTypeId() {
        return this.rateTypeId;
    }

    /**
     * @param rateTypeId
     */
    public void setRateTypeId(String rateTypeId) {
        this.rateTypeId = rateTypeId;
    }

    /**
     * @return rateTypeName
     */
    public String getRateTypeName() {
        return this.rateTypeName;
    }

    /**
     * @param rateTypeName
     */
    public void setRateTypeName(String rateTypeName) {
        this.rateTypeName = rateTypeName;
    }

    /**
     * @return rate
     */
    public java.math.BigDecimal getRate() {
        return this.rate;
    }

    /**
     * @param rate
     */
    public void setRate(java.math.BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * @return validDate
     */
    public String getValidDate() {
        return this.validDate;
    }

    /**
     * @param validDate
     */
    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    /**
     * @return rateDatum
     */
    public String getRateDatum() {
        return this.rateDatum;
    }

    /**
     * @param rateDatum
     */
    public void setRateDatum(String rateDatum) {
        this.rateDatum = rateDatum;
    }

    /**
     * @return usedInd
     */
    public String getUsedInd() {
        return this.usedInd;
    }

    /**
     * @param usedInd
     */
    public void setUsedInd(String usedInd) {
        this.usedInd = usedInd;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return rateName
     */
    public String getRateName() {
        return this.rateName;
    }

    /**
     * @param rateName
     */
    public void setRateName(String rateName) {
        this.rateName = rateName;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }


}