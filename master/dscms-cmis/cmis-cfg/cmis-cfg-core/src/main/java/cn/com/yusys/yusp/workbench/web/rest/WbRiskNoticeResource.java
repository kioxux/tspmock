/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.workbench.domain.WbRiskNotice;
import cn.com.yusys.yusp.workbench.service.WbRiskNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbRiskNoticeResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 21:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/wbrisknotice")
public class WbRiskNoticeResource {
    @Autowired
    private WbRiskNoticeService wbRiskNoticeService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<WbRiskNotice>> query() {
        QueryModel queryModel = new QueryModel();
        List<WbRiskNotice> list = wbRiskNoticeService.selectAll(queryModel);
        return new ResultDto<List<WbRiskNotice>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<WbRiskNotice>> index(@RequestBody QueryModel queryModel) {
        List<WbRiskNotice> list = wbRiskNoticeService.selectByModel(queryModel);
        return new ResultDto<List<WbRiskNotice>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<WbRiskNotice> show(@PathVariable("serno") String serno) {
        WbRiskNotice wbRiskNotice = wbRiskNoticeService.selectByPrimaryKey(serno);
        return new ResultDto<WbRiskNotice>(wbRiskNotice);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<WbRiskNotice> create(@RequestBody WbRiskNotice wbRiskNotice) throws URISyntaxException {
        wbRiskNoticeService.insert(wbRiskNotice);
        return new ResultDto<WbRiskNotice>(wbRiskNotice);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody WbRiskNotice wbRiskNotice) throws URISyntaxException {
        int result = wbRiskNoticeService.update(wbRiskNotice);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = wbRiskNoticeService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = wbRiskNoticeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
