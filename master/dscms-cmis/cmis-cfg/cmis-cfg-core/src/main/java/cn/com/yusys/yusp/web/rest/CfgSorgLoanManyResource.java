/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgSorgLoanMany;
import cn.com.yusys.yusp.dto.CfgSorgLoanManyDto;
import cn.com.yusys.yusp.service.CfgSorgLoanManyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSorgLoanManyResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 10:13:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgsorgloanmany")
public class CfgSorgLoanManyResource {
    @Autowired
    private CfgSorgLoanManyService cfgSorgLoanManyService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgSorgLoanMany>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgSorgLoanMany> list = cfgSorgLoanManyService.selectAll(queryModel);
        return new ResultDto<List<CfgSorgLoanMany>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgSorgLoanMany>> index(QueryModel queryModel) {
        List<CfgSorgLoanMany> list = cfgSorgLoanManyService.selectByModel(queryModel);
        return new ResultDto<List<CfgSorgLoanMany>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgSorgLoanMany> show(@PathVariable("pkId") String pkId) {
        CfgSorgLoanMany cfgSorgLoanMany = cfgSorgLoanManyService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgSorgLoanMany>(cfgSorgLoanMany);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgSorgLoanMany> create(@RequestBody CfgSorgLoanMany cfgSorgLoanMany) throws URISyntaxException {
        cfgSorgLoanManyService.insert(cfgSorgLoanMany);
        return new ResultDto<CfgSorgLoanMany>(cfgSorgLoanMany);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgSorgLoanMany cfgSorgLoanMany) throws URISyntaxException {
        int result = cfgSorgLoanManyService.update(cfgSorgLoanMany);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgSorgLoanManyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgSorgLoanManyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 zxz
     * @创建时间 2021/6/21 11:24
     * @注释 入账机构查询
     */
    @PostMapping("/selecsorgmany")
    protected ResultDto<List<CfgSorgLoanManyDto>> selecSorgLoanMan(@RequestBody QueryModel queryModel) {
        List<CfgSorgLoanManyDto> list = cfgSorgLoanManyService.selecSorgLoanMany(queryModel);
        return new ResultDto<List<CfgSorgLoanManyDto>>(list);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/5 19:21
     * @注释 放贷机构查询 出账
     */
    @PostMapping("/selectForXw")
    protected ResultDto<List<CfgSorgLoanMany>> selectForXw(@RequestBody QueryModel queryModel) {
        List<CfgSorgLoanMany> list = cfgSorgLoanManyService.selectByModel(queryModel);
        return new ResultDto<List<CfgSorgLoanMany>>(list);
    }
}
