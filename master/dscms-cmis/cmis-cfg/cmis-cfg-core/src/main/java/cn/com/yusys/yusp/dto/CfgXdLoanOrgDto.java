package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgXdLoanOrg
 * @类描述: cfg_xd_loan_org数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-20 20:03:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgXdLoanOrgDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 小贷所属机构 **/
	private String xdOrg;
	
	/** 小贷所属机构名称 **/
	private String xdOrgName;
	
	/** 地区账务机构,用于贷款出账 **/
	private String xdFinaOrg;
	
	/** 地区账务机构名称 **/
	private String xdFinaOrgName;
	
	/** 片区账务机构,用于抵质押品入库账务 **/
	private String xdFinaManagerOrg;
	
	/** 片区账务机构名称 **/
	private String xdFinaManagerOrgName;
	
	/** 片区管理机构,放款机构 **/
	private String xdPvpOrg;
	
	/** 放款机构名称 **/
	private String xdPvpOrgName;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param xdOrg
	 */
	public void setXdOrg(String xdOrg) {
		this.xdOrg = xdOrg == null ? null : xdOrg.trim();
	}
	
    /**
     * @return XdOrg
     */	
	public String getXdOrg() {
		return this.xdOrg;
	}
	
	/**
	 * @param xdOrgName
	 */
	public void setXdOrgName(String xdOrgName) {
		this.xdOrgName = xdOrgName == null ? null : xdOrgName.trim();
	}
	
    /**
     * @return XdOrgName
     */	
	public String getXdOrgName() {
		return this.xdOrgName;
	}
	
	/**
	 * @param xdFinaOrg
	 */
	public void setXdFinaOrg(String xdFinaOrg) {
		this.xdFinaOrg = xdFinaOrg == null ? null : xdFinaOrg.trim();
	}
	
    /**
     * @return XdFinaOrg
     */	
	public String getXdFinaOrg() {
		return this.xdFinaOrg;
	}
	
	/**
	 * @param xdFinaOrgName
	 */
	public void setXdFinaOrgName(String xdFinaOrgName) {
		this.xdFinaOrgName = xdFinaOrgName == null ? null : xdFinaOrgName.trim();
	}
	
    /**
     * @return XdFinaOrgName
     */	
	public String getXdFinaOrgName() {
		return this.xdFinaOrgName;
	}
	
	/**
	 * @param xdFinaManagerOrg
	 */
	public void setXdFinaManagerOrg(String xdFinaManagerOrg) {
		this.xdFinaManagerOrg = xdFinaManagerOrg == null ? null : xdFinaManagerOrg.trim();
	}
	
    /**
     * @return XdFinaManagerOrg
     */	
	public String getXdFinaManagerOrg() {
		return this.xdFinaManagerOrg;
	}
	
	/**
	 * @param xdFinaManagerOrgName
	 */
	public void setXdFinaManagerOrgName(String xdFinaManagerOrgName) {
		this.xdFinaManagerOrgName = xdFinaManagerOrgName == null ? null : xdFinaManagerOrgName.trim();
	}
	
    /**
     * @return XdFinaManagerOrgName
     */	
	public String getXdFinaManagerOrgName() {
		return this.xdFinaManagerOrgName;
	}
	
	/**
	 * @param xdPvpOrg
	 */
	public void setXdPvpOrg(String xdPvpOrg) {
		this.xdPvpOrg = xdPvpOrg == null ? null : xdPvpOrg.trim();
	}
	
    /**
     * @return XdPvpOrg
     */	
	public String getXdPvpOrg() {
		return this.xdPvpOrg;
	}
	
	/**
	 * @param xdPvpOrgName
	 */
	public void setXdPvpOrgName(String xdPvpOrgName) {
		this.xdPvpOrgName = xdPvpOrgName == null ? null : xdPvpOrgName.trim();
	}
	
    /**
     * @return XdPvpOrgName
     */	
	public String getXdPvpOrgName() {
		return this.xdPvpOrgName;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}