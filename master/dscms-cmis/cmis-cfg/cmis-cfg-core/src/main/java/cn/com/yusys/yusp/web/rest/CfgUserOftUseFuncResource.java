/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgUserOftUseFunc;
import cn.com.yusys.yusp.dto.CfgUserOftUseFuncClientDto;
import cn.com.yusys.yusp.service.CfgUserOftUseFuncService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgUserOftUseFuncResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2021-01-27 16:34:03
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfguseroftusefunc")
public class CfgUserOftUseFuncResource {
    private static final Logger log = LoggerFactory.getLogger(CfgUserOftUseFuncResource.class);

    @Autowired
    private CfgUserOftUseFuncService cfgUserOftUseFuncService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgUserOftUseFunc>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgUserOftUseFunc> list = cfgUserOftUseFuncService.selectAll(queryModel);
        return new ResultDto<List<CfgUserOftUseFunc>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgUserOftUseFunc>> index(QueryModel queryModel) {
        List<CfgUserOftUseFunc> list = cfgUserOftUseFuncService.selectByModel(queryModel);
        return new ResultDto<List<CfgUserOftUseFunc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgUserOftUseFunc> show(@PathVariable("pkId") String pkId) {
        CfgUserOftUseFunc cfgUserOftUseFunc = cfgUserOftUseFuncService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgUserOftUseFunc>(cfgUserOftUseFunc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgUserOftUseFunc> create(@RequestBody CfgUserOftUseFunc cfgUserOftUseFunc) throws URISyntaxException {
        cfgUserOftUseFuncService.insert(cfgUserOftUseFunc);
        return new ResultDto<CfgUserOftUseFunc>(cfgUserOftUseFunc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgUserOftUseFunc cfgUserOftUseFunc) throws URISyntaxException {
        int result = cfgUserOftUseFuncService.update(cfgUserOftUseFunc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgUserOftUseFuncService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgUserOftUseFuncService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 新增用户常用功能
     *
     * @param cfgUserOftUseFuncClientDto
     * @return
     */
    @PostMapping(value = "/insertUserOftUseFunc")
    public ResultDto<Integer> insertUserOftUseFunc(@RequestBody CfgUserOftUseFuncClientDto cfgUserOftUseFuncClientDto) {
        log.info("新增用户常用功能，请求报文【{}】", JSONObject.toJSONString(cfgUserOftUseFuncClientDto));
        CfgUserOftUseFunc cfgUserOftUseFunc = new CfgUserOftUseFunc();
        BeanUtils.copyProperties(cfgUserOftUseFuncClientDto, cfgUserOftUseFunc);
        int count = cfgUserOftUseFuncService.insert(cfgUserOftUseFunc);
        return new ResultDto<Integer>(count);
    }

    /**
     * 删除用户常用功能
     *
     * @param cfgUserOftUseFuncClientDto
     * @return
     */
    @PostMapping(value = "/deleteUserOftUseFunc")
    public int deleteUserOftUseFunc(@RequestBody CfgUserOftUseFuncClientDto cfgUserOftUseFuncClientDto) {
        log.info("删除用户常用功能，请求报文【{}】", cfgUserOftUseFuncClientDto);
        return cfgUserOftUseFuncService.updateByPkId(cfgUserOftUseFuncClientDto);
    }

    /**
     * 获取用户常用功能
     *
     * @param loginCode
     * @return
     */
    @PostMapping(value = "/getUserOftUseFunc")
    public List<CfgUserOftUseFuncClientDto> getUserOftUseFunc(@RequestBody String loginCode) {
        List<CfgUserOftUseFuncClientDto> cfgUserOftUseFuncClientDtos = cfgUserOftUseFuncService.getUserOftUseFuncByLoginCode(loginCode);
        log.info("获取用户常用功能，用户登录ID【{}】,返回结果【{}】", loginCode, JSONObject.toJSONString(cfgUserOftUseFuncClientDtos));
        return cfgUserOftUseFuncClientDtos;
    }
}
