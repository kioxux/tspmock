/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgGenerateTempFile
 * @类描述: cfg_generate_temp_file数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-06-10 14:17:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_generate_temp_file")
public class CfgGenerateTempFile extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "pk_id")
    private String pkId;

    /**
     * 存储文件服务器ip地址
     **/
    @Column(name = "login_ip", unique = false, nullable = true, length = 40)
    private String loginIp;

    /**
     * 登录端口
     **/
    @Column(name = "login_port", unique = false, nullable = true, length = 10)
    private String loginPort;

    /**
     * 登录用户名
     **/
    @Column(name = "login_username", unique = false, nullable = true, length = 20)
    private String loginUsername;

    /**
     * 登录密码
     **/
    @Column(name = "login_pwd", unique = false, nullable = true, length = 40)
    private String loginPwd;

    /**
     * 文件存储路径
     **/
    @Column(name = "file_path", unique = false, nullable = true, length = 200)
    private String filePath;

    /**
     * 备注
     **/
    @Column(name = "memo", unique = false, nullable = true, length = 1000)
    private String memo;

    public CfgGenerateTempFile() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return loginIp
     */
    public String getLoginIp() {
        return this.loginIp;
    }

    /**
     * @param loginIp
     */
    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    /**
     * @return loginPort
     */
    public String getLoginPort() {
        return this.loginPort;
    }

    /**
     * @param loginPort
     */
    public void setLoginPort(String loginPort) {
        this.loginPort = loginPort;
    }

    /**
     * @return loginUsername
     */
    public String getLoginUsername() {
        return this.loginUsername;
    }

    /**
     * @param loginUsername
     */
    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername;
    }

    /**
     * @return loginPwd
     */
    public String getLoginPwd() {
        return this.loginPwd;
    }

    /**
     * @param loginPwd
     */
    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    /**
     * @return filePath
     */
    public String getFilePath() {
        return this.filePath;
    }

    /**
     * @param filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }


}