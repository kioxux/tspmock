/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgApproveAuthority;
import cn.com.yusys.yusp.dto.CfgApproveAuthorityDto;
import cn.com.yusys.yusp.service.CfgApproveAuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgApproveAuthorityResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-05-24 10:32:53
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgapproveauthority")
public class CfgApproveAuthorityResource {
    @Autowired
    private CfgApproveAuthorityService cfgApproveAuthorityService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgApproveAuthority>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgApproveAuthority> list = cfgApproveAuthorityService.selectAll(queryModel);
        return new ResultDto<List<CfgApproveAuthority>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgApproveAuthority>> index(QueryModel queryModel) {
        List<CfgApproveAuthority> list = cfgApproveAuthorityService.selectByModel(queryModel);
        return new ResultDto<List<CfgApproveAuthority>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgApproveAuthority> show(@PathVariable("pkId") String pkId) {
        CfgApproveAuthority cfgApproveAuthority = cfgApproveAuthorityService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgApproveAuthority>(cfgApproveAuthority);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgApproveAuthority> create(@RequestBody CfgApproveAuthority cfgApproveAuthority) throws URISyntaxException {
        cfgApproveAuthorityService.insert(cfgApproveAuthority);
        return new ResultDto<CfgApproveAuthority>(cfgApproveAuthority);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgApproveAuthority cfgApproveAuthority) throws URISyntaxException {
        int result = cfgApproveAuthorityService.update(cfgApproveAuthority);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgApproveAuthorityService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgApproveAuthorityService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: queryDutyByCusDebtLevel
     * @方法描述: 获取内评审批岗位
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/querydutybycusdebtlevel")
    public ResultDto<CfgApproveAuthorityDto> queryDutyByCusDebtLevel(@RequestBody CfgApproveAuthorityDto cfgApproveAuthorityDto) {
        return ResultDto.success(cfgApproveAuthorityService.queryDutyByCusDebtLevel(cfgApproveAuthorityDto));
    }

    /**
     * @方法名称: queryRateByCusDebtLevel
     * @方法描述: 获取审批人的资本占用率
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/queryratebycusdebtlevel")
    public ResultDto<CfgApproveAuthorityDto> queryRateByCusDebtLevel(@RequestBody CfgApproveAuthorityDto cfgApproveAuthorityDto) {
        return ResultDto.success(cfgApproveAuthorityService.queryRateByCusDebtLevel(cfgApproveAuthorityDto));
    }

}
