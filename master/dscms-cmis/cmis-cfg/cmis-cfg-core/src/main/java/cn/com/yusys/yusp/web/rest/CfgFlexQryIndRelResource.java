/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgFlexQryIndRel;
import cn.com.yusys.yusp.service.CfgFlexQryIndRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryIndRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-30 17:23:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgflexqryindrel")
public class CfgFlexQryIndRelResource {
    @Autowired
    private CfgFlexQryIndRelService cfgFlexQryIndRelService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgFlexQryIndRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgFlexQryIndRel> list = cfgFlexQryIndRelService.selectAll(queryModel);
        return new ResultDto<List<CfgFlexQryIndRel>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgFlexQryIndRel>> index(QueryModel queryModel) {
        List<CfgFlexQryIndRel> list = cfgFlexQryIndRelService.selectByModel(queryModel);
        return new ResultDto<List<CfgFlexQryIndRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgFlexQryIndRel> show(@PathVariable("pkId") String pkId) {
        CfgFlexQryIndRel cfgFlexQryIndRel = cfgFlexQryIndRelService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgFlexQryIndRel>(cfgFlexQryIndRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgFlexQryIndRel> create(@RequestBody CfgFlexQryIndRel cfgFlexQryIndRel) throws URISyntaxException {
        cfgFlexQryIndRelService.insert(cfgFlexQryIndRel);
        return new ResultDto<CfgFlexQryIndRel>(cfgFlexQryIndRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgFlexQryIndRel cfgFlexQryIndRel) throws URISyntaxException {
        int result = cfgFlexQryIndRelService.update(cfgFlexQryIndRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateSelective
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody CfgFlexQryIndRel cfgFlexQryIndRel) throws URISyntaxException {
        int result = cfgFlexQryIndRelService.updateSelective(cfgFlexQryIndRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgFlexQryIndRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgFlexQryIndRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getExistCfgFlexQryIndRel
     * @函数描述:获取已经存在的关联关系
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getExistCfgFlexQryIndRel")
    protected ResultDto<List<CfgFlexQryIndRel>> getExistCfgFlexQryIndRel(@RequestBody CfgFlexQryIndRel cfgFlexQryIndRel) {
        List<CfgFlexQryIndRel> list = cfgFlexQryIndRelService.queryByIndexCode(cfgFlexQryIndRel);
        return new ResultDto<>(list);
    }
}
