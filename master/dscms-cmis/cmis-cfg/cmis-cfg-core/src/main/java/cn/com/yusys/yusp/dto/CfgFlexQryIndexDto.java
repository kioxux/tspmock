package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryIndex
 * @类描述: cfg_flex_qry_index数据实体类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-28 20:43:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgFlexQryIndexDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 指标配置编码
     **/
    private String indexCode;

    /**
     * 创建类型 STD_ZB_CREATE_TYPE
     **/
    private String createType;

    /**
     * 父级对象
     **/
    private String parentObj;

    /**
     * 对象名称/分组名称
     **/
    private String objGroupName;

    /**
     * 排序
     **/
    private java.math.BigDecimal sort;

    /**
     * 数据源
     **/
    private String datasource;

    /**
     * 来源表
     **/
    private String tableSour;

    /**
     * 表别名
     **/
    private String alias;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 最后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    private String oprType;

    /**
     * @return IndexCode
     */
    public String getIndexCode() {
        return this.indexCode;
    }

    /**
     * @param indexCode
     */
    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode == null ? null : indexCode.trim();
    }

    /**
     * @return CreateType
     */
    public String getCreateType() {
        return this.createType;
    }

    /**
     * @param createType
     */
    public void setCreateType(String createType) {
        this.createType = createType == null ? null : createType.trim();
    }

    /**
     * @return ParentObj
     */
    public String getParentObj() {
        return this.parentObj;
    }

    /**
     * @param parentObj
     */
    public void setParentObj(String parentObj) {
        this.parentObj = parentObj == null ? null : parentObj.trim();
    }

    /**
     * @return ObjGroupName
     */
    public String getObjGroupName() {
        return this.objGroupName;
    }

    /**
     * @param objGroupName
     */
    public void setObjGroupName(String objGroupName) {
        this.objGroupName = objGroupName == null ? null : objGroupName.trim();
    }

    /**
     * @return Sort
     */
    public java.math.BigDecimal getSort() {
        return this.sort;
    }

    /**
     * @param sort
     */
    public void setSort(java.math.BigDecimal sort) {
        this.sort = sort;
    }

    /**
     * @return Datasource
     */
    public String getDatasource() {
        return this.datasource;
    }

    /**
     * @param datasource
     */
    public void setDatasource(String datasource) {
        this.datasource = datasource == null ? null : datasource.trim();
    }

    /**
     * @return TableSour
     */
    public String getTableSour() {
        return this.tableSour;
    }

    /**
     * @param tableSour
     */
    public void setTableSour(String tableSour) {
        this.tableSour = tableSour == null ? null : tableSour.trim();
    }

    /**
     * @return Alias
     */
    public String getAlias() {
        return this.alias;
    }

    /**
     * @param alias
     */
    public void setAlias(String alias) {
        this.alias = alias == null ? null : alias.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }


}