package cn.com.yusys.yusp.service.server.xdxt0013;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AdminSmTreeDicMapper;
import cn.com.yusys.yusp.server.xdxt0013.req.Xdxt0013DataReqDto;
import cn.com.yusys.yusp.server.xdxt0013.resp.OptList;
import cn.com.yusys.yusp.server.xdxt0013.resp.Xdxt0013DataRespDto;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg模块
 * @类名称: Xdxt0013Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxt0013Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0013Service.class);

    @Resource
    private AdminSmTreeDicMapper adminSmTreeDicMapper;

    /**
     * 树形字典通用列表查询
     *
     * @param xdxt0013DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0013DataRespDto getXdxt0013(Xdxt0013DataReqDto xdxt0013DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0013.key, DscmsEnum.TRADE_CODE_XDXT0013.value, JSON.toJSONString(xdxt0013DataReqDto));
        Xdxt0013DataRespDto xdxt0013DataRespDto = new Xdxt0013DataRespDto();
        try {
            java.util.List<OptList> optLists = new ArrayList<>();
            optLists = adminSmTreeDicMapper.getXdxt0013(xdxt0013DataReqDto);
            xdxt0013DataRespDto.setOptList(optLists);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0013.key, DscmsEnum.TRADE_CODE_XDXT0013.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0013.key, DscmsEnum.TRADE_CODE_XDXT0013.value, JSON.toJSONString(xdxt0013DataRespDto));
        return xdxt0013DataRespDto;
    }

}
