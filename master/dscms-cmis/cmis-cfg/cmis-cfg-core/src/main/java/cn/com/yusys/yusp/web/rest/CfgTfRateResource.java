/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CfgTfRate;
import cn.com.yusys.yusp.dto.CfgSorgFinaDto;
import cn.com.yusys.yusp.dto.CfgTfRateDto;
import cn.com.yusys.yusp.dto.CfgTfRateQueryDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CfgTfRateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgTfRateResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-11 09:46:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgtfrate")
public class CfgTfRateResource {

    private static final Logger logger = LoggerFactory.getLogger(CfgTfRateResource.class);

    @Autowired
    private CfgTfRateService cfgTfRateService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgTfRate>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgTfRate> list = cfgTfRateService.selectAll(queryModel);
        return new ResultDto<List<CfgTfRate>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgTfRate>> index(QueryModel queryModel) {
        List<CfgTfRate> list = cfgTfRateService.selectByModel(queryModel);
        return new ResultDto<List<CfgTfRate>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgTfRate> show(@PathVariable("pkId") String pkId) {
        CfgTfRate cfgTfRate = cfgTfRateService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgTfRate>(cfgTfRate);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgTfRate> create(@RequestBody CfgTfRate cfgTfRate) throws URISyntaxException {
        cfgTfRateService.insert(cfgTfRate);
        return new ResultDto<CfgTfRate>(cfgTfRate);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgTfRate cfgTfRate) throws URISyntaxException {
        int result = cfgTfRateService.update(cfgTfRate);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgTfRateService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgTfRateService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param cfgTfRateQueryDto
     * @return
     */
    @PostMapping("/querycfgtfrate")
    protected ResultDto<CfgTfRateDto> queryCfgTfRate(@RequestBody CfgTfRateQueryDto cfgTfRateQueryDto) {
        logger.info("调用cfg接口查询汇率信息开始", "请求参数" + cfgTfRateQueryDto.toString());
        CfgTfRateDto cfgTfRateDto = new CfgTfRateDto();
        ResultDto<CfgTfRateDto> cfgTfRateDtoResultDto = new ResultDto<>();
        try {
            cfgTfRateDto = cfgTfRateService.queryCfgTfRate(cfgTfRateQueryDto);
            // 封装xdsx0026DataResultDto中正确的返回码和返回信息
            cfgTfRateDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cfgTfRateDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, e.getMessage());
            // 封装xdsx0026DataResultDto中异常返回码和返回信息
            cfgTfRateDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cfgTfRateDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0026DataRespDto到xdsx0026DataResultDto中
        cfgTfRateDtoResultDto.setData(cfgTfRateDto);
        logger.info("调用cfg接口查询汇率信息结束", "响应参数" + cfgTfRateDto.toString());
        return cfgTfRateDtoResultDto;
    }

    /**
     * @param model
     * @return
     */
    @PostMapping("/queryallcfgtfrate")
    protected ResultDto<List<CfgTfRateDto>> queryAllCfgTfRate(@RequestBody QueryModel model) {
        List<CfgTfRateDto> records = (List<CfgTfRateDto>) cfgTfRateService.queryAllCfgTfRate(model);
        return new ResultDto<List<CfgTfRateDto>>(records);
    }
}
