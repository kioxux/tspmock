package cn.com.yusys.yusp.web.server.cmiscfg0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.server.cmiscfg0001.req.CmisCfg0001ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0001.resp.CmisCfg0001RespDto;
import cn.com.yusys.yusp.service.server.cmiscfg0001.CmisCfg0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 新增首页提醒事项
 *
 * @author zhangjw 20210630
 * @version 1.0
 */
@Api(tags = "cmiscfg0001:新增首页提醒事项")
@RestController
@RequestMapping("/api/cfg4inner")
public class CmisCfg0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCfg0001Resource.class);

    @Autowired
    private CmisCfg0001Service cmisCfg0001Service;

    /**
     * 交易码：cmiscfg0001
     * 交易描述：新增首页提醒事项
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("新增首页提醒事项")
    @PostMapping("/cmiscfg0001")
    protected @ResponseBody
    ResultDto<CmisCfg0001RespDto> cmisCfg0001(@Validated @RequestBody CmisCfg0001ReqDto reqDto) {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.value, JSON.toJSONString(reqDto));
        ResultDto<CmisCfg0001RespDto> cmisCfg0001RespDtoResultDto = new ResultDto<>();
        CmisCfg0001RespDto cmisCfg0001RespDto = new CmisCfg0001RespDto();
        try {
            // 调用对应的service层
            cmisCfg0001RespDto = cmisCfg0001Service.execute(reqDto);

        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.value, e.getMessage());
            // 封装cmisCfg0001RespDto中异常返回码和返回信息
            cmisCfg0001RespDto.setErrorCode(EpbEnum.EPB099999.key);
            cmisCfg0001RespDto.setErrorMsg(EpbEnum.EPB099999.value);
        }
        // 封装cmisCfg0001RespDto到cmisCfg0001RespDtoResultDto中
        cmisCfg0001RespDtoResultDto.setData(cmisCfg0001RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.value, JSON.toJSONString(cmisCfg0001RespDto));
        return cmisCfg0001RespDtoResultDto;
    }
}
