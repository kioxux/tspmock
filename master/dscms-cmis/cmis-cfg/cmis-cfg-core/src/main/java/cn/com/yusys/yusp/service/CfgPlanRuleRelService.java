/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgPlanRuleRel;
import cn.com.yusys.yusp.repository.mapper.CfgPlanRuleRelMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPlanRuleRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-04 17:07:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgPlanRuleRelService {
    private static final Logger log = LoggerFactory.getLogger(CfgPrdBasicinfoService.class);

    @Autowired
    private CfgPlanRuleRelMapper cfgPlanRuleRelMapper;
    @Autowired
    private CfgNodeRuleRelService cfgNodeRuleRelService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgPlanRuleRel selectByPrimaryKey(String pkId) {
        return cfgPlanRuleRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgPlanRuleRel> selectAll(QueryModel model) {
        List<CfgPlanRuleRel> records = (List<CfgPlanRuleRel>) cfgPlanRuleRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgPlanRuleRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPlanRuleRel> list = cfgPlanRuleRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgPlanRuleRel record) {
        return cfgPlanRuleRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgPlanRuleRel record) {
        return cfgPlanRuleRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgPlanRuleRel record) {
        return cfgPlanRuleRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgPlanRuleRel record) {
        return cfgPlanRuleRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgPlanRuleRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgPlanRuleRelMapper.deleteByIds(ids);
    }

    /**
     * 根据获取方案编号、节点编号,逻辑删除该业务规则方案关联规则项的配置
     *
     * @param cfgPlanRuleRel
     * @return
     */
    @Transactional
    public int deleteCfgPlanAndNodeRuleRel(CfgPlanRuleRel cfgPlanRuleRel) {
        int result = cfgPlanRuleRelMapper.updateByRuleItemIdAndPlanId(cfgPlanRuleRel.getRuleItemId(), cfgPlanRuleRel.getPlanId(), CommonConstant.DELETE_OPR);
        log.info("根据方案编号、节点编号删除业务规则方案关联规则项条数：{}", result);
        int numNode = cfgNodeRuleRelService.updateByRuleItemIdAndPlanId(cfgPlanRuleRel.getRuleItemId(), cfgPlanRuleRel.getPlanId(), CommonConstant.DELETE_OPR);
        log.info("根据方案编号、节点编号删除业务规则方案关联流程条数：{}", numNode);
        return result;
    }
}
