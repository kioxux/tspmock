/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.server.cmiscfg0001.req.CmisCfg0001ReqDto;
import cn.com.yusys.yusp.workbench.domain.WbMsgNotice;
import cn.com.yusys.yusp.workbench.service.WbMsgNoticeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbMsgNoticeResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 21:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/wbmsgnotice")
public class WbMsgNoticeResource {
    private static final Logger log = LoggerFactory.getLogger(WebSocketController.class);
    @Autowired
    private WbMsgNoticeService wbMsgNoticeService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<WbMsgNotice>> query() {
        QueryModel queryModel = new QueryModel();
        List<WbMsgNotice> list = wbMsgNoticeService.selectAll(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<WbMsgNotice>> index(@RequestBody QueryModel queryModel) {
        List<WbMsgNotice> list = wbMsgNoticeService.selectByModel(queryModel);
        if (queryModel.getCondition().containsKey("firstQuery")) {
            long count = list.stream().map(item -> {
                CmisCfg0001ReqDto wbMessageDto = new CmisCfg0001ReqDto();
                wbMessageDto.setMessageType(item.getMessageType());
                wbMessageDto.setInputId(item.getInputId());
                wbMessageDto.setContent(item.getContent());
                rabbitTemplate.convertAndSend(CmisBizConstants.MESSAGE_EXCHANGE, CmisBizConstants.MESSAGE_KEY, wbMessageDto);
                return wbMessageDto;
            }).count();
            log.info("弹出首页弹窗数:{}", count);
        }
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<WbMsgNotice> show(@PathVariable("serno") String serno) {
        WbMsgNotice wbMsgNotice = wbMsgNoticeService.selectByPrimaryKey(serno);
        return new ResultDto<WbMsgNotice>(wbMsgNotice);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<WbMsgNotice> create(@RequestBody WbMsgNotice wbMsgNotice) throws URISyntaxException {
        wbMsgNoticeService.insert(wbMsgNotice);
        return new ResultDto<WbMsgNotice>(wbMsgNotice);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody WbMsgNotice wbMsgNotice) throws URISyntaxException {
        int result = wbMsgNoticeService.update(wbMsgNotice);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = wbMsgNoticeService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = wbMsgNoticeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchupdate
     * @函数描述:批量标注已读
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchupdate/{ids}")
    protected ResultDto<Integer> updates(@PathVariable String ids) {
        int result = wbMsgNoticeService.updateByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/clearAll/{loginCode}")
    protected ResultDto<Integer> updatesByUser(@PathVariable String loginCode) {
        int result = wbMsgNoticeService.clearAll(loginCode);
        return new ResultDto<Integer>(result);
    }
}
