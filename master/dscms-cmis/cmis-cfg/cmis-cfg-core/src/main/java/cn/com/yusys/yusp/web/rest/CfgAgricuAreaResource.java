/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgAgricuArea;
import cn.com.yusys.yusp.service.CfgAgricuAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgAgricuAreaResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-06-15 22:26:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgagricuarea")
public class CfgAgricuAreaResource {
    @Autowired
    private CfgAgricuAreaService cfgAgricuAreaService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgAgricuArea>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgAgricuArea> list = cfgAgricuAreaService.selectAll(queryModel);
        return new ResultDto<List<CfgAgricuArea>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgAgricuArea>> index(QueryModel queryModel) {
        List<CfgAgricuArea> list = cfgAgricuAreaService.selectByModel(queryModel);
        return new ResultDto<List<CfgAgricuArea>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{areaNo}")
    protected ResultDto<CfgAgricuArea> show(@PathVariable("areaNo") String areaNo) {
        CfgAgricuArea cfgAgricuArea = cfgAgricuAreaService.selectByPrimaryKey(areaNo);
        return new ResultDto<CfgAgricuArea>(cfgAgricuArea);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgAgricuArea> create(@RequestBody CfgAgricuArea cfgAgricuArea) throws URISyntaxException {
        cfgAgricuAreaService.insert(cfgAgricuArea);
        return new ResultDto<CfgAgricuArea>(cfgAgricuArea);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgAgricuArea cfgAgricuArea) throws URISyntaxException {
        int result = cfgAgricuAreaService.update(cfgAgricuArea);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{areaNo}")
    protected ResultDto<Integer> delete(@PathVariable("areaNo") String areaNo) {
        int result = cfgAgricuAreaService.deleteByPrimaryKey(areaNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgAgricuAreaService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/queryAreaNum")
    protected ResultDto<Integer> queryAreaNum(@RequestParam("areaCode") String areaCode, @RequestParam("agriFlg") String agriFlg) {
        int result = cfgAgricuAreaService.queryAreaNum(areaCode, agriFlg);
        ResultDto resultDto = new ResultDto();
        resultDto.setData(result);
        return resultDto;
    }
}
