package cn.com.yusys.yusp.web.server.xdqt0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdqt0001.req.Xdqt0001DataReqDto;
import cn.com.yusys.yusp.server.xdqt0001.resp.Xdqt0001DataRespDto;
import cn.com.yusys.yusp.service.server.xdqt0001.Xdqt0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据产品号查询产品名称
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0001:根据产品号查询产品名称")
@RestController
@RequestMapping("/api/cfgqt4bsp")
public class CfgXdqt0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(CfgXdqt0001Resource.class);

    @Autowired
    private Xdqt0001Service xdqt0001Service;

    /**
     * 交易码：xdqt0001
     * 交易描述：根据产品号查询产品名称
     *
     * @param xdqt0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据产品号查询产品名称")
    @PostMapping("/xdqt0001")
    protected @ResponseBody
    ResultDto<Xdqt0001DataRespDto> xdqt0001(@Validated @RequestBody Xdqt0001DataReqDto xdqt0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, JSON.toJSONString(xdqt0001DataReqDto));
        Xdqt0001DataRespDto xdqt0001DataRespDto = new Xdqt0001DataRespDto();// 响应Dto:根据产品号查询产品名称
        ResultDto<Xdqt0001DataRespDto> xdqt0001DataResultDto = new ResultDto<>();
        String prdId = xdqt0001DataReqDto.getPrdId();//产品代码
        try {
            // 从xdqt0001DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value);
            xdqt0001DataRespDto = xdqt0001Service.getCfgPrdBasicInfoByPrdId(prdId);
            xdqt0001DataRespDto.setPrdName(xdqt0001DataRespDto.getPrdName());// 产品名称
            //xdqt0001DataRespDto.setPrdName(StringUtils.EMPTY);// 产品名称
            // 封装xdqt0001DataResultDto中正确的返回码和返回信息
            xdqt0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdqt0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, e.getMessage());
            xdqt0001DataResultDto.setCode(e.getErrorCode());
            xdqt0001DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, e.getMessage());
            // 封装xdqt0001DataResultDto中异常返回码和返回信息
            xdqt0001DataResultDto.setCode(EcbEnum.ECB019999.key);
            xdqt0001DataResultDto.setMessage(EcbEnum.ECB019999.value);
        }
        // 封装xdqt0001DataRespDto到xdqt0001DataResultDto中
        xdqt0001DataResultDto.setData(xdqt0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, JSON.toJSONString(xdqt0001DataResultDto));
        return xdqt0001DataResultDto;
    }
}
