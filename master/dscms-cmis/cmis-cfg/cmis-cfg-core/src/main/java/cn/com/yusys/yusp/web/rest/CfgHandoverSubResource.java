/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgHandoverSub;
import cn.com.yusys.yusp.service.CfgHandoverSubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgHandoverSubResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2020-11-20 13:55:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfghandoversub")
public class CfgHandoverSubResource {
    @Autowired
    private CfgHandoverSubService cfgHandoverSubService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgHandoverSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgHandoverSub> list = cfgHandoverSubService.selectAll(queryModel);
        return new ResultDto<List<CfgHandoverSub>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgHandoverSub>> index(QueryModel queryModel) {
        List<CfgHandoverSub> list = cfgHandoverSubService.selectByModel(queryModel);
        return new ResultDto<List<CfgHandoverSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{subId}")
    protected ResultDto<CfgHandoverSub> show(@PathVariable("subId") String subId) {
        CfgHandoverSub cfgHandoverSub = cfgHandoverSubService.selectByPrimaryKey(subId);
        return new ResultDto<CfgHandoverSub>(cfgHandoverSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgHandoverSub> create(@RequestBody CfgHandoverSub cfgHandoverSub) throws URISyntaxException {
        cfgHandoverSubService.insert(cfgHandoverSub);
        return new ResultDto<CfgHandoverSub>(cfgHandoverSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgHandoverSub cfgHandoverSub) throws URISyntaxException {
        int result = cfgHandoverSubService.updateSelective(cfgHandoverSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{subId}")
    protected ResultDto<Integer> delete(@PathVariable("subId") String subId) {
        int result = cfgHandoverSubService.deleteByPrimaryKey(subId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgHandoverSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
