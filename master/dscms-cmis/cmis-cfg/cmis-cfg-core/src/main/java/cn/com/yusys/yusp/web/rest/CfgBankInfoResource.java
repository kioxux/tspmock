/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgBankInfo;
import cn.com.yusys.yusp.dto.CfgBankInfoDto;
import cn.com.yusys.yusp.dto.CfgWyClassRelClientDto;
import cn.com.yusys.yusp.service.CfgBankInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBankInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Empty
 * @创建时间: 2021-04-29 14:15:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgbankinfo")
public class CfgBankInfoResource {
    @Autowired
    private CfgBankInfoService cfgBankInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgBankInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgBankInfo> list = cfgBankInfoService.selectAll(queryModel);
        return new ResultDto<List<CfgBankInfo>>(list);
    }

    @GetMapping("/")
    protected ResultDto<List<CfgBankInfo>> index(QueryModel queryModel) {
        List<CfgBankInfo> list = cfgBankInfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgBankInfo>>(list);
    }

    @PostMapping("/")
    protected ResultDto<List<CfgBankInfo>> index1 (@RequestBody  QueryModel queryModel) {
        List<CfgBankInfo> list = cfgBankInfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgBankInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<CfgBankInfo>> queryList(@RequestBody QueryModel queryModel) {
        List<CfgBankInfo> list = cfgBankInfoService.selectAllByModel(queryModel);
        return new ResultDto<List<CfgBankInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/bankNo")
    protected ResultDto<CfgBankInfo> show(@RequestBody String bankNo) {
        CfgBankInfo cfgBankInfo = cfgBankInfoService.selectByBankNo(bankNo);
        return new ResultDto<CfgBankInfo>(cfgBankInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<CfgBankInfo> create(@RequestBody CfgBankInfo cfgBankInfo) throws URISyntaxException {
        cfgBankInfoService.insert(cfgBankInfo);
        return new ResultDto<CfgBankInfo>(cfgBankInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgBankInfo cfgBankInfo) throws URISyntaxException {
        int result = cfgBankInfoService.updateSelective(cfgBankInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody String bankNo) {
        int result = cfgBankInfoService.deleteByPrimaryKey(bankNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgBankInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: queryCfgBankInfoDataByParams
     * @方法描述: 根据入参查询行号数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("根据入参查询行号数据")
    @PostMapping("/querycfgbankinfodatabyparams")
    protected ResultDto<List<CfgBankInfo>> queryCfgBankInfoDataByParams(@RequestBody Map queryMap) {
        List<CfgBankInfo> list = cfgBankInfoService.queryCfgBankInfoDataByParams(queryMap);
        return new ResultDto<List<CfgBankInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<CfgBankInfo>> selectByModel(@RequestBody QueryModel queryModel) {
        List<CfgBankInfo> list = cfgBankInfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgBankInfo>>(list);
    }

    /**
     * @param bankNo
     * @函数名称:selectbybankno
     * @函数描述:公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbybankno")
    @ApiOperation("根据行号查询信息")
    protected ResultDto<CfgBankInfoDto> selectbybankno(@RequestBody String bankNo) {
        CfgBankInfo cfgBankInfo = cfgBankInfoService.selectByBankNo(bankNo);
        CfgBankInfoDto cfgBankInfoDto = new CfgBankInfoDto();
        BeanUtils.copyProperties(cfgBankInfo, cfgBankInfoDto);
        return new ResultDto<CfgBankInfoDto>(cfgBankInfoDto);
    }
}
