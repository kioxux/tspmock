package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgStandardTemplate
 * @类描述: cfg_standard_template数据实体类
 * @功能描述:
 * @创建人: LQC
 * @创建时间: 2021-04-25 16:39:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgStandardTemplateDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 模板编码 STD_STANDARD_TEMPLATE
     **/
    private String tplCode;

    /**
     * 模板文件路径
     **/
    private String tplFilePath;

    /**
     * 模板文件名称
     **/
    private String tplFileName;

    /**
     * @return tplCode
     */
    public String getTplCode() {
        return this.tplCode;
    }

    /**
     * @param tplCode
     */
    public void setTplCode(String tplCode) {
        this.tplCode = tplCode == null ? null : tplCode.trim();
    }

    /**
     * @return tplFilePath
     */
    public String getTplFilePath() {
        return this.tplFilePath;
    }

    /**
     * @param tplFilePath
     */
    public void setTplFilePath(String tplFilePath) {
        this.tplFilePath = tplFilePath == null ? null : tplFilePath.trim();
    }

    /**
     * @return tplFileName
     */
    public String getTplFileName() {
        return this.tplFileName;
    }

    /**
     * @param tplFileName
     */
    public void setTplFileName(String tplFileName) {
        this.tplFileName = tplFileName == null ? null : tplFileName.trim();
    }


}