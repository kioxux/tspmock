/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgBizRulePlan;
import cn.com.yusys.yusp.dto.CfgBizRulePlanDto;
import cn.com.yusys.yusp.service.CfgBizRulePlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBizRulePlanResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: liuch
 * @创建时间: 2020-12-23 11:22:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgbizruleplan")
public class CfgBizRulePlanResource {
    @Autowired
    private CfgBizRulePlanService cfgBizRulePlanService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgBizRulePlan>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgBizRulePlan> list = cfgBizRulePlanService.selectAll(queryModel);
        return new ResultDto<List<CfgBizRulePlan>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgBizRulePlan>> index(QueryModel queryModel) {
        List<CfgBizRulePlan> list = cfgBizRulePlanService.selectByModel(queryModel);
        return new ResultDto<List<CfgBizRulePlan>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{planId}")
    protected ResultDto<CfgBizRulePlan> show(@PathVariable("planId") String planId) {
        CfgBizRulePlan cfgBizRulePlan = cfgBizRulePlanService.selectByPrimaryKey(planId);
        return new ResultDto<CfgBizRulePlan>(cfgBizRulePlan);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgBizRulePlan> create(@RequestBody CfgBizRulePlan cfgBizRulePlan) throws URISyntaxException {
        cfgBizRulePlanService.insert(cfgBizRulePlan);
        return new ResultDto<CfgBizRulePlan>(cfgBizRulePlan);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgBizRulePlan cfgBizRulePlan) throws URISyntaxException {
        int result = cfgBizRulePlanService.update(cfgBizRulePlan);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{planId}")
    protected ResultDto<Integer> delete(@PathVariable("planId") String planId) {
        int result = cfgBizRulePlanService.deleteByPrimaryKey(planId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgBizRulePlanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据业务规则项目编号查询所有引用该规则的业务规则方案
     *
     * @param ruleItemId
     * @return
     */
    @GetMapping("/queryQuotePlan/{ruleItemId}")
    protected ResultDto<List<CfgBizRulePlan>> queryQuotePlan(@PathVariable("ruleItemId") String ruleItemId) {
        List<CfgBizRulePlan> list = cfgBizRulePlanService.queryQuotePlanByRuleItemId(ruleItemId);
        return new ResultDto<List<CfgBizRulePlan>>(list);
    }


    /**
     * 根据方案ID获取规则方案及其关联的规则项信息
     *
     * @param planId
     * @return
     */
    @GetMapping("/allrules/{planId}")
    protected ResultDto<CfgBizRulePlanDto> showWithSubs(@PathVariable("planId") String planId) {
        CfgBizRulePlanDto cfgBizRulePlanDto = cfgBizRulePlanService.queryAllRuleDetailsById(planId);
        return new ResultDto<CfgBizRulePlanDto>(cfgBizRulePlanDto);
    }


    /**
     * 根据流程标识与节点id获取规则方案及其关联的规则项信息
     *
     * @param wfiSignId
     * @param nodeId
     * @return
     */
    @GetMapping("/wfnodeallrules/{wfiSignId}/{nodeId}")
    protected ResultDto<CfgBizRulePlanDto> showWithSubsByFlow(@PathVariable("wfiSignId") String wfiSignId, @PathVariable("nodeId") String nodeId) {
        CfgBizRulePlanDto cfgBizRulePlanDto = cfgBizRulePlanService.queryWfNodeAllRuleDetails(wfiSignId, nodeId);
        return new ResultDto<CfgBizRulePlanDto>(cfgBizRulePlanDto);
    }


    /**
     * 根据获取业务规则方案编号逻辑删除该业务规则方案及关联的子表（业务规则方案关联规则项、业务规则方案关联流程）
     *
     * @param planId
     * @return
     */
    @PostMapping("/deleteCfgBizRulePlanAndChild/{planId}")
    protected ResultDto<Integer> deleteCfgBizRulePlanAndChild(@PathVariable("planId") String planId) {
        int result = cfgBizRulePlanService.deleteCfgBizRulePlanAndChild(planId);
        return new ResultDto<Integer>(result);
    }
}
