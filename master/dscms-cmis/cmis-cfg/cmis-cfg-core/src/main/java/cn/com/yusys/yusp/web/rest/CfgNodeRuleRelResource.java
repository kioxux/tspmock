/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgNodeRuleRel;
import cn.com.yusys.yusp.service.CfgNodeRuleRelService;
import cn.com.yusys.yusp.service.CfgPlanRuleRelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgNodeRuleRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-03 21:47:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgnoderulerel")
public class CfgNodeRuleRelResource {
    private static final Logger log = LoggerFactory.getLogger(CfgHandoverResource.class);
    @Autowired
    private CfgNodeRuleRelService cfgNodeRuleRelService;
    @Autowired
    private CfgPlanRuleRelService cfgPlanRuleRelService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgNodeRuleRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgNodeRuleRel> list = cfgNodeRuleRelService.selectAll(queryModel);
        return new ResultDto<List<CfgNodeRuleRel>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgNodeRuleRel>> index(QueryModel queryModel) {
        List<CfgNodeRuleRel> list = cfgNodeRuleRelService.selectByModel(queryModel);
        return new ResultDto<List<CfgNodeRuleRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgNodeRuleRel> show(@PathVariable("pkId") String pkId) {
        CfgNodeRuleRel cfgNodeRuleRel = cfgNodeRuleRelService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgNodeRuleRel>(cfgNodeRuleRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<CfgNodeRuleRel> create(@RequestBody CfgNodeRuleRel cfgNodeRuleRel) throws URISyntaxException {
        cfgNodeRuleRelService.insert(cfgNodeRuleRel);
        return new ResultDto<CfgNodeRuleRel>(cfgNodeRuleRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgNodeRuleRel cfgNodeRuleRel) throws URISyntaxException {
        int result = cfgNodeRuleRelService.update(cfgNodeRuleRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgNodeRuleRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgNodeRuleRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * 根据获取方案编号、工作流编号,逻辑删除该业务规则方案关联流程的配置
     *
     * @param cfgNodeRuleRel
     * @return
     */
    @PostMapping("/deleteCfgNodeRuleRel")
    protected ResultDto<Integer> deleteCfgNodeRuleRel(@RequestBody CfgNodeRuleRel cfgNodeRuleRel) {
        int result = cfgNodeRuleRelService.deleteCfgNodeRuleRel(cfgNodeRuleRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据业务规则方案编号、流程编号、节点编号查询的关联规则项列表.
     *
     * @param planId
     * @param nodeId
     * @return
     * @Param wfiSignId
     */
    @GetMapping("/getNodePlanRuleList")
    protected ResultDto<List<CfgNodeRuleRel>> getNodePlanRuleList(@RequestParam("planId") String planId, @RequestParam("wfiSignId") String wfiSignId, @RequestParam("nodeId") String nodeId) {
        log.info("业务规则方案编号【{}】", planId);
        log.info("流程编号【{}】", wfiSignId);
        log.info("节点编号【{}】", nodeId);
        CfgNodeRuleRel cfgNodeRuleRel = new CfgNodeRuleRel();
        cfgNodeRuleRel.setPlanId(planId);
        cfgNodeRuleRel.setWfiSignId(wfiSignId);
        cfgNodeRuleRel.setNodeId(nodeId);
        cfgNodeRuleRel.setOprType(CommonConstant.ADD_OPR);//操作类型=新增的数据
        List<CfgNodeRuleRel> list = cfgNodeRuleRelService.getNodePlanRuleList(cfgNodeRuleRel);
        return new ResultDto<List<CfgNodeRuleRel>>(list);
    }
}
