/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgStandardTemplate
 * @类描述: cfg_standard_template数据实体类
 * @功能描述:
 * @创建人: LQC
 * @创建时间: 2021-04-25 16:39:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_standard_template")
public class CfgStandardTemplate extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 模板编码 STD_STANDARD_TEMPLATE
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "TPL_CODE")
    private String tplCode;

    /**
     * 模板文件路径
     **/
    @Column(name = "TPL_FILE_PATH", unique = false, nullable = false, length = 200)
    private String tplFilePath;

    /**
     * 模板文件名称
     **/
    @Column(name = "TPL_FILE_NAME", unique = false, nullable = true, length = 80)
    private String tplFileName;

    public CfgStandardTemplate() {
        // Not compliant
    }

    /**
     * @return tplCode
     */
    public String getTplCode() {
        return this.tplCode;
    }

    /**
     * @param tplCode
     */
    public void setTplCode(String tplCode) {
        this.tplCode = tplCode;
    }

    /**
     * @return tplFilePath
     */
    public String getTplFilePath() {
        return this.tplFilePath;
    }

    /**
     * @param tplFilePath
     */
    public void setTplFilePath(String tplFilePath) {
        this.tplFilePath = tplFilePath;
    }

    /**
     * @return tplFileName
     */
    public String getTplFileName() {
        return this.tplFileName;
    }

    /**
     * @param tplFileName
     */
    public void setTplFileName(String tplFileName) {
        this.tplFileName = tplFileName;
    }


}