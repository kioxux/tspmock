/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBankInfo
 * @类描述: cfg_bank_info数据实体类
 * @功能描述:
 * @创建人: Empty
 * @创建时间: 2021-04-29 14:15:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_bank_info")
public class CfgBankInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 行号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "BANK_NO")
    private String bankNo;

    /**
     * 行名
     **/
    @Column(name = "BANK_NAME", unique = false, nullable = true, length = 250)
    private String bankName;

    /**
     * 地区代码
     **/
    @Column(name = "AREA_CODE", unique = false, nullable = true, length = 6)
    private String areaCode;

    /**
     * 联系电话
     **/
    @Column(name = "PHONE", unique = false, nullable = true, length = 20)
    private String phone;

    /**
     * 邮政编码
     **/
    @Column(name = "POSTCODE", unique = false, nullable = true, length = 6)
    private String postcode;

    /**
     * 地址
     **/
    @Column(name = "ADDR", unique = false, nullable = true, length = 60)
    private String addr;

    /**
     * 上级行
     **/
    @Column(name = "SUPER_BANK_NO", unique = false, nullable = true, length = 30)
    private String superBankNo;

    /**
     * 责任人
     **/
    @RedisCacheTranslator(redisCacheKey = "userName")
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 50)
    private String managerId;

    /**
     * 责任机构
     **/
    @RedisCacheTranslator(redisCacheKey = "orgName")
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 50)
    private String managerBrId;

    /**
     * 生效日期
     **/
    @Column(name = "INURE_DATE", unique = false, nullable = true, length = 10)
    private String inureDate;

    /**
     * 注销日期
     **/
    @Column(name = "LOGOUT_DATE", unique = false, nullable = true, length = 10)
    private String logoutDate;

    /**
     * 状态 STD_ZB_PRD_ST
     **/
    @Column(name = "BANK_STATUS", unique = false, nullable = true, length = 5)
    private String bankStatus;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 50)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 50)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 50)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 50)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgBankInfo() {
        // Not compliant
    }

    /**
     * @return bankNo
     */
    public String getBankNo() {
        return this.bankNo;
    }

    /**
     * @param bankNo
     */
    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    /**
     * @return bankName
     */
    public String getBankName() {
        return this.bankName;
    }

    /**
     * @param bankName
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return areaCode
     */
    public String getAreaCode() {
        return this.areaCode;
    }

    /**
     * @param areaCode
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * @return phone
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return postcode
     */
    public String getPostcode() {
        return this.postcode;
    }

    /**
     * @param postcode
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return addr
     */
    public String getAddr() {
        return this.addr;
    }

    /**
     * @param addr
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * @return superBankNo
     */
    public String getSuperBankNo() {
        return this.superBankNo;
    }

    /**
     * @param superBankNo
     */
    public void setSuperBankNo(String superBankNo) {
        this.superBankNo = superBankNo;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return inureDate
     */
    public String getInureDate() {
        return this.inureDate;
    }

    /**
     * @param inureDate
     */
    public void setInureDate(String inureDate) {
        this.inureDate = inureDate;
    }

    /**
     * @return logoutDate
     */
    public String getLogoutDate() {
        return this.logoutDate;
    }

    /**
     * @param logoutDate
     */
    public void setLogoutDate(String logoutDate) {
        this.logoutDate = logoutDate;
    }

    /**
     * @return bankStatus
     */
    public String getBankStatus() {
        return this.bankStatus;
    }

    /**
     * @param bankStatus
     */
    public void setBankStatus(String bankStatus) {
        this.bankStatus = bankStatus;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}