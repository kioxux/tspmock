/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgAccountClassChoose;
import cn.com.yusys.yusp.dto.CfgAccountClassChooseDto;
import cn.com.yusys.yusp.service.CfgAccountClassChooseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgAccountClassChooseResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 16:37:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgaccountclasschoose")
public class CfgAccountClassChooseResource {
    @Autowired
    private CfgAccountClassChooseService cfgAccountClassChooseService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgAccountClassChoose>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgAccountClassChoose> list = cfgAccountClassChooseService.selectAll(queryModel);
        return new ResultDto<List<CfgAccountClassChoose>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgAccountClassChoose>> index(QueryModel queryModel) {
        List<CfgAccountClassChoose> list = cfgAccountClassChooseService.selectByModel(queryModel);
        return new ResultDto<List<CfgAccountClassChoose>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{seqNo}")
    protected ResultDto<CfgAccountClassChoose> show(@PathVariable("seqNo") String seqNo) {
        CfgAccountClassChoose cfgAccountClassChoose = cfgAccountClassChooseService.selectByPrimaryKey(seqNo);
        return new ResultDto<CfgAccountClassChoose>(cfgAccountClassChoose);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgAccountClassChoose> create(@RequestBody CfgAccountClassChoose cfgAccountClassChoose) throws URISyntaxException {
        cfgAccountClassChooseService.insert(cfgAccountClassChoose);
        return new ResultDto<CfgAccountClassChoose>(cfgAccountClassChoose);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgAccountClassChoose cfgAccountClassChoose) throws URISyntaxException {
        int result = cfgAccountClassChooseService.update(cfgAccountClassChoose);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{seqNo}")
    protected ResultDto<Integer> delete(@PathVariable("seqNo") String seqNo) {
        int result = cfgAccountClassChooseService.deleteByPrimaryKey(seqNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgAccountClassChooseService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: queryHxAccountClassByProps
     * @方法描述: 通过属性查询对应的核心科目号
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 马顺
     * @创建时间: 2021-06-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryHxAccountClassByProps")
    protected ResultDto<CfgAccountClassChooseDto> queryHxAccountClassByProps(@RequestBody CfgAccountClassChooseDto cfgAccountClassChooseDto) {
        return ResultDto.success(cfgAccountClassChooseService.queryHxAccountClassByProps(cfgAccountClassChooseDto));
    }

    /**
     * @方法名称: queryHxAccountClassByAcccountClass
     * @方法描述: 通过科目号查询对应的核心会计类别
     * @参数与返回说明:
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-06-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryHxAccountClassByAcccountClass")
    protected ResultDto<CfgAccountClassChooseDto> queryHxAccountClassByAcccountClass(@RequestBody String accountClass) {
        return ResultDto.success(cfgAccountClassChooseService.queryHxAccountClassByAcccountClass(accountClass));
    }

}
