package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgJobFlowNode
 * @类描述: cfg_job_flow_node数据实体类
 * @功能描述:
 * @创建人: qhy
 * @创建时间: 2021-05-06 21:52:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgJobFlowNodeDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 作业流编号（关联外键）
     **/
    private String jobFlowId;

    /**
     * 节点编号
     **/
    private String nodeId;

    /**
     * 工作流编号
     **/
    private String wfiSignId;

    /**
     * 审批查看模板组
     **/
    private String apprViewUrl;

    /**
     * 审批调整模板组
     **/
    private String apprAdjustTpl;

    /**
     * 是否需要打回/退回原因 STD_ZB_YES_NO
     **/
    private String isOptTyp;

    /**
     * 业务规则方案编号
     **/
    private String planId;

    /**
     * 备注
     **/
    private String remark;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 最后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * @return PkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    /**
     * @return JobFlowId
     */
    public String getJobFlowId() {
        return this.jobFlowId;
    }

    /**
     * @param jobFlowId
     */
    public void setJobFlowId(String jobFlowId) {
        this.jobFlowId = jobFlowId == null ? null : jobFlowId.trim();
    }

    /**
     * @return NodeId
     */
    public String getNodeId() {
        return this.nodeId;
    }

    /**
     * @param nodeId
     */
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId == null ? null : nodeId.trim();
    }

    /**
     * @return WfiSignId
     */
    public String getWfiSignId() {
        return this.wfiSignId;
    }

    /**
     * @param wfiSignId
     */
    public void setWfiSignId(String wfiSignId) {
        this.wfiSignId = wfiSignId == null ? null : wfiSignId.trim();
    }

    /**
     * @return ApprViewUrl
     */
    public String getApprViewUrl() {
        return this.apprViewUrl;
    }

    /**
     * @param apprViewUrl
     */
    public void setApprViewUrl(String apprViewUrl) {
        this.apprViewUrl = apprViewUrl == null ? null : apprViewUrl.trim();
    }

    /**
     * @return ApprAdjustTpl
     */
    public String getApprAdjustTpl() {
        return this.apprAdjustTpl;
    }

    /**
     * @param apprAdjustTpl
     */
    public void setApprAdjustTpl(String apprAdjustTpl) {
        this.apprAdjustTpl = apprAdjustTpl == null ? null : apprAdjustTpl.trim();
    }

    /**
     * @return IsOptTyp
     */
    public String getIsOptTyp() {
        return this.isOptTyp;
    }

    /**
     * @param isOptTyp
     */
    public void setIsOptTyp(String isOptTyp) {
        this.isOptTyp = isOptTyp == null ? null : isOptTyp.trim();
    }

    /**
     * @return PlanId
     */
    public String getPlanId() {
        return this.planId;
    }

    /**
     * @param planId
     */
    public void setPlanId(String planId) {
        this.planId = planId == null ? null : planId.trim();
    }

    /**
     * @return Remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }


}