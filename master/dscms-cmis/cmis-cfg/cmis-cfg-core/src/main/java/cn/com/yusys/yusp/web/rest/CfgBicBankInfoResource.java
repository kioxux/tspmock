/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgBicBankInfo;
import cn.com.yusys.yusp.service.CfgBicBankInfoService;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBicBankInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-26 16:38:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgbicbankinfo")
public class CfgBicBankInfoResource {
    @Autowired
    private CfgBicBankInfoService cfgBicBankInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgBicBankInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgBicBankInfo> list = cfgBicBankInfoService.selectAll(queryModel);
        return new ResultDto<List<CfgBicBankInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgBicBankInfo>> index(QueryModel queryModel) {
        List<CfgBicBankInfo> list = cfgBicBankInfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgBicBankInfo>>(list);
    }

    /**
     * @函数名称:selectbymodel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<CfgBicBankInfo>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<CfgBicBankInfo> list = cfgBicBankInfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgBicBankInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{bicCode}")
    protected ResultDto<CfgBicBankInfo> show(@PathVariable("bicCode") String bicCode) {
        CfgBicBankInfo cfgBicBankInfo = cfgBicBankInfoService.selectByPrimaryKey(bicCode);
        return new ResultDto<CfgBicBankInfo>(cfgBicBankInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgBicBankInfo> create(@RequestBody CfgBicBankInfo cfgBicBankInfo) throws URISyntaxException {
        cfgBicBankInfoService.insert(cfgBicBankInfo);
        return new ResultDto<CfgBicBankInfo>(cfgBicBankInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgBicBankInfo cfgBicBankInfo) throws URISyntaxException {
        int result = cfgBicBankInfoService.update(cfgBicBankInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{bicCode}")
    protected ResultDto<Integer> delete(@PathVariable("bicCode") String bicCode) {
        int result = cfgBicBankInfoService.deleteByPrimaryKey(bicCode);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgBicBankInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
