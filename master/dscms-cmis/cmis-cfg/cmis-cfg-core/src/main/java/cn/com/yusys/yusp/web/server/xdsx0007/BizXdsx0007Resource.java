package cn.com.yusys.yusp.web.server.xdsx0007;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdsx0007.req.Xdsx0007DataReqDto;
import cn.com.yusys.yusp.server.xdsx0007.resp.Xdsx0007DataRespDto;
import cn.com.yusys.yusp.service.server.xdsx0007.Xdsx0007Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:授信业务授权同步、资本占用率参数表同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDSX0007:授信业务授权同步、资本占用率参数表同步")
@RestController
@RequestMapping("/api/cfgsx4bsp")
public class BizXdsx0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0007Resource.class);

    @Autowired
    private Xdsx0007Service xdsx0007Service;

    /**
     * 交易码：xdsx0007
     * 交易描述：授信业务授权同步、资本占用率参数表同步
     *
     * @param xdsx0007DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("授信业务授权同步、资本占用率参数表同步")
    @PostMapping("/xdsx0007")
    protected @ResponseBody
    ResultDto<Xdsx0007DataRespDto> xdsx0007(@Validated @RequestBody Xdsx0007DataReqDto xdsx0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, JSON.toJSONString(xdsx0007DataReqDto));
        Xdsx0007DataRespDto xdsx0007DataRespDto = new Xdsx0007DataRespDto();// 响应Dto:授信业务授权同步、资本占用率参数表同步
        ResultDto<Xdsx0007DataRespDto> xdsx0007DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, JSON.toJSONString(xdsx0007DataReqDto));
            xdsx0007DataRespDto = xdsx0007Service.getXdsx0007(xdsx0007DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, JSON.toJSONString(xdsx0007DataRespDto));

            // 封装xdsx0007DataResultDto中正确的返回码和返回信息
            xdsx0007DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0007DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, e.getMessage());
            // 封装xdsx0007DataResultDto中异常返回码和返回信息
            xdsx0007DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0007DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0007DataRespDto到xdsx0007DataResultDto中
        xdsx0007DataResultDto.setData(xdsx0007DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, JSON.toJSONString(xdsx0007DataResultDto));
        return xdsx0007DataResultDto;
    }
}
