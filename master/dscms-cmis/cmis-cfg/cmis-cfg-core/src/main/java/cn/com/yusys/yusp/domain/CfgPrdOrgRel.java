/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdOrgRel
 * @类描述: cfg_prd_org_rel数据实体类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-17 15:49:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_prd_org_rel")
public class CfgPrdOrgRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 机构编号
     **/
    @Column(name = "ORG_ID", unique = false, nullable = false, length = 40)
    private String orgId;

    /**
     * 产品编号
     **/
    @Column(name = "PRD_ID", unique = false, nullable = false, length = 40)
    private String prdId;

    /**
     * 机构名称
     **/
    @Column(name = "ORG_NAME", unique = false, nullable = true, length = 80)
    private String orgName;

    /**
     * 适用范围
     **/
    @Column(name = "SUIT_SCOPE", unique = false, nullable = true, length = 5)
    private String suitScope;

    /**
     * 适用类型
     **/
    @Column(name = "SUIT_TYPE", unique = false, nullable = true, length = 5)
    private String suitType;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgPrdOrgRel() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return orgId
     */
    public String getOrgId() {
        return this.orgId;
    }

    /**
     * @param orgId
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return prdId
     */
    public String getPrdId() {
        return this.prdId;
    }

    /**
     * @param prdId
     */
    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    /**
     * @return orgName
     */
    public String getOrgName() {
        return this.orgName;
    }

    /**
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return suitScope
     */
    public String getSuitScope() {
        return this.suitScope;
    }

    /**
     * @param suitScope
     */
    public void setSuitScope(String suitScope) {
        this.suitScope = suitScope;
    }

    /**
     * @return suitType
     */
    public String getSuitType() {
        return this.suitType;
    }

    /**
     * @param suitType
     */
    public void setSuitType(String suitType) {
        this.suitType = suitType;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}