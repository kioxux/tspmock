package cn.com.yusys.yusp.web.server.cmiscfg0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.server.cmiscfg0003.req.CmisCfg0003ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0003.resp.CmisCfg0003RespDto;
import cn.com.yusys.yusp.service.server.cmiscfg0003.CmisCfg0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 根据行号获取上级行号
 *
 * @author zhangjw 20210727
 * @version 1.0
 */
@Api(tags = "cmiscfg0003:根据行号获取上级行号")
@RestController
@RequestMapping("/api/cfg4inner")
public class CmisCfg0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCfg0003Resource.class);

    @Autowired
    private CmisCfg0003Service cmisCfg0003Service;

    /**
     * 交易码：cmiscfg0003
     * 交易描述：新增首页提醒事项
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据行号获取上级行号")
    @PostMapping("/cmiscfg0003")
    protected @ResponseBody
    ResultDto<CmisCfg0003RespDto> cmisCfg0003(@Validated @RequestBody CmisCfg0003ReqDto reqDto) {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0003.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0003.value, JSON.toJSONString(reqDto));
        ResultDto<CmisCfg0003RespDto> cmisCfg0003RespDtoResultDto = new ResultDto<>();
        CmisCfg0003RespDto cmisCfg0003RespDto = new CmisCfg0003RespDto();
        try {
            // 调用对应的service层
            cmisCfg0003RespDto = cmisCfg0003Service.execute(reqDto);

        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0003.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0003.value, e.getMessage());
            // 封装cmisCfg0003RespDto中异常返回码和返回信息
            cmisCfg0003RespDto.setErrorCode(EpbEnum.EPB099999.key);
            cmisCfg0003RespDto.setErrorMsg(EpbEnum.EPB099999.value);
        }
        // 封装cmisCfg0003RespDto到cmisCfg0003RespDtoResultDto中
        cmisCfg0003RespDtoResultDto.setData(cmisCfg0003RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0003.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0003.value, JSON.toJSONString(cmisCfg0003RespDto));
        return cmisCfg0003RespDtoResultDto;
    }
}
