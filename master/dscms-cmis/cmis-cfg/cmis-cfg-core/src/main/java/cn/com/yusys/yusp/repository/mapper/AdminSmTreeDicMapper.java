/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AdminSmTreeDic;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.server.xdxt0012.req.Xdxt0012DataReqDto;
import cn.com.yusys.yusp.server.xdxt0013.req.Xdxt0013DataReqDto;
import cn.com.yusys.yusp.server.xdxt0013.resp.OptList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: AdminSmTreeDicMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 12651
 * @创建时间: 2021-05-07 16:07:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AdminSmTreeDicMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    AdminSmTreeDic selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AdminSmTreeDic> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(AdminSmTreeDic record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(AdminSmTreeDic record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(AdminSmTreeDic record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(AdminSmTreeDic record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 树形字典通用列表查询
     *
     * @param xdxt0013DataReqDto
     * @return
     */
    java.util.List<OptList> getXdxt0013(Xdxt0013DataReqDto xdxt0013DataReqDto);

    int deleteBylocate(AdminSmTreeDicDto adminSmTreeDicDto);

    /**
     * 根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
     *
     * @param xdxt0012DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.server.xdxt0012.resp.List> getXdxt0012(Xdxt0012DataReqDto xdxt0012DataReqDto);

    AdminSmTreeDicDto queryCfgtreebBycode(AdminSmTreeDicDto adminSmTreeDicDto);

    AdminSmTreeDicDto queryHighCfgtreebBycode(AdminSmTreeDicDto adminSmTreeDicDto);
}