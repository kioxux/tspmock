/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgStandardTemplate;
import cn.com.yusys.yusp.service.CfgStandardTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgStandardTemplateResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: LQC
 * @创建时间: 2021-04-25 16:39:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgstandardtemplate")
public class CfgStandardTemplateResource {
    @Autowired
    private CfgStandardTemplateService cfgStandardTemplateService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgStandardTemplate>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgStandardTemplate> list = cfgStandardTemplateService.selectAll(queryModel);
        return new ResultDto<List<CfgStandardTemplate>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgStandardTemplate>> index(QueryModel queryModel) {
        List<CfgStandardTemplate> list = cfgStandardTemplateService.selectByModel(queryModel);
        return new ResultDto<List<CfgStandardTemplate>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{templateCode}")
    protected ResultDto<CfgStandardTemplate> show(@PathVariable("templateCode") String templateCode) {
        CfgStandardTemplate cfgStandardTemplate = cfgStandardTemplateService.selectByPrimaryKey(templateCode);
        return new ResultDto<CfgStandardTemplate>(cfgStandardTemplate);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgStandardTemplate> create(@RequestBody CfgStandardTemplate cfgStandardTemplate) throws URISyntaxException {
        cfgStandardTemplateService.insert(cfgStandardTemplate);
        return new ResultDto<CfgStandardTemplate>(cfgStandardTemplate);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgStandardTemplate cfgStandardTemplate) throws URISyntaxException {
        int result = cfgStandardTemplateService.update(cfgStandardTemplate);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{templateCode}")
    protected ResultDto<Integer> delete(@PathVariable("templateCode") String templateCode) {
        int result = cfgStandardTemplateService.deleteByPrimaryKey(templateCode);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgStandardTemplateService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
