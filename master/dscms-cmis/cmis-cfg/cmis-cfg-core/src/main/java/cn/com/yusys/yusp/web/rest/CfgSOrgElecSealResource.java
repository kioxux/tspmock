/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgSOrgElecSeal;
import cn.com.yusys.yusp.service.CfgSOrgElecSealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSOrgElecSealResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-06-10 22:30:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgsorgelecseal")
public class CfgSOrgElecSealResource {
    @Autowired
    private CfgSOrgElecSealService cfgSOrgElecSealService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgSOrgElecSeal>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgSOrgElecSeal> list = cfgSOrgElecSealService.selectAll(queryModel);
        return new ResultDto<List<CfgSOrgElecSeal>>(list);
    }

    /**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/queryCfgSeal")
    protected ResultDto<List<CfgSOrgElecSeal>> queryCfgSeal(@RequestBody QueryModel queryModel) {
        List<CfgSOrgElecSeal> list = cfgSOrgElecSealService.selectAll(queryModel);
        return new ResultDto<List<CfgSOrgElecSeal>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgSOrgElecSeal>> index(QueryModel queryModel) {
        List<CfgSOrgElecSeal> list = cfgSOrgElecSealService.selectByModel(queryModel);
        return new ResultDto<List<CfgSOrgElecSeal>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{orgNo}")
    protected ResultDto<CfgSOrgElecSeal> show(@PathVariable("orgNo") String orgNo) {
        CfgSOrgElecSeal cfgSOrgElecSeal = cfgSOrgElecSealService.selectByPrimaryKey(orgNo);
        return new ResultDto<CfgSOrgElecSeal>(cfgSOrgElecSeal);
    }

    /**
     * @函数名称:selectByOrgNo
     * @函数描述:根据机构号查询获取对应的章编码
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByOrgNo")
    protected ResultDto<CfgSOrgElecSeal> selectByOrgNo(@RequestBody String orgNo) {
        CfgSOrgElecSeal cfgSOrgElecSeal = cfgSOrgElecSealService.selectByOrgNo(orgNo);
        return new ResultDto<CfgSOrgElecSeal>(cfgSOrgElecSeal);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgSOrgElecSeal> create(@RequestBody CfgSOrgElecSeal cfgSOrgElecSeal) throws URISyntaxException {
        cfgSOrgElecSealService.insert(cfgSOrgElecSeal);
        return new ResultDto<CfgSOrgElecSeal>(cfgSOrgElecSeal);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgSOrgElecSeal cfgSOrgElecSeal) throws URISyntaxException {
        int result = cfgSOrgElecSealService.update(cfgSOrgElecSeal);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{orgNo}")
    protected ResultDto<Integer> delete(@PathVariable("orgNo") String orgNo) {
        int result = cfgSOrgElecSealService.deleteByPrimaryKey(orgNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgSOrgElecSealService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
