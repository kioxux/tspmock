/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgPrdTypeProperties;
import cn.com.yusys.yusp.dto.CfgPrdTypePropertiesDto;
import cn.com.yusys.yusp.service.CfgPrdTypePropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdTypePropertiesResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-07-02 10:30:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgprdtypeproperties")
public class CfgPrdTypePropertiesResource {
    @Autowired
    private CfgPrdTypePropertiesService cfgPrdTypePropertiesService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgPrdTypeProperties>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgPrdTypeProperties> list = cfgPrdTypePropertiesService.selectAll(queryModel);
        return new ResultDto<List<CfgPrdTypeProperties>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgPrdTypeProperties>> index(QueryModel queryModel) {
        List<CfgPrdTypeProperties> list = cfgPrdTypePropertiesService.selectByModel(queryModel);
        return new ResultDto<List<CfgPrdTypeProperties>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{prdTypePropNo}")
    protected ResultDto<CfgPrdTypeProperties> show(@PathVariable("prdTypePropNo") String prdTypePropNo) {
        CfgPrdTypeProperties cfgPrdTypeProperties = cfgPrdTypePropertiesService.selectByPrimaryKey(prdTypePropNo);
        return new ResultDto<CfgPrdTypeProperties>(cfgPrdTypeProperties);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgPrdTypeProperties> create(@RequestBody CfgPrdTypeProperties cfgPrdTypeProperties) throws URISyntaxException {
        cfgPrdTypePropertiesService.insert(cfgPrdTypeProperties);
        return new ResultDto<CfgPrdTypeProperties>(cfgPrdTypeProperties);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgPrdTypeProperties cfgPrdTypeProperties) throws URISyntaxException {
        int result = cfgPrdTypePropertiesService.update(cfgPrdTypeProperties);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{prdTypePropNo}")
    protected ResultDto<Integer> delete(@PathVariable("prdTypePropNo") String prdTypePropNo) {
        int result = cfgPrdTypePropertiesService.deleteByPrimaryKey(prdTypePropNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgPrdTypePropertiesService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 查询产品扩展属性
     *
     * @param typePropNo
     * @return
     * @author xll
     */
    @PostMapping("/queryCfgPrdTypePropertiesByProrNo")
    protected ResultDto<List<CfgPrdTypePropertiesDto>> queryCfgPrdTypePropertiesByProrNo(@RequestBody String typePropNo) {
        List<CfgPrdTypePropertiesDto> CfgPrdTypePropertiesDtos = cfgPrdTypePropertiesService.queryCfgPrdTypePropertiesByProrNo(typePropNo);
        return ResultDto.success(CfgPrdTypePropertiesDtos);
    }

}
