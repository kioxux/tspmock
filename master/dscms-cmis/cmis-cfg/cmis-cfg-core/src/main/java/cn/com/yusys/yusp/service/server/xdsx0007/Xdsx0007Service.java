package cn.com.yusys.yusp.service.server.xdsx0007;

import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCfgConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CfgApproveAuthority;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CfgApproveAuthorityMapper;
import cn.com.yusys.yusp.server.xdsx0007.req.EmpoinfoList;
import cn.com.yusys.yusp.server.xdsx0007.req.Xdsx0007DataReqDto;
import cn.com.yusys.yusp.server.xdsx0007.resp.Xdsx0007DataRespDto;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdsx0007Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-24 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdsx0007Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0007Service.class);

    @Resource
    private CfgApproveAuthorityMapper cfgApproveAuthorityMapper;

    /**
     * 授信业务授权同步、资本占用率参数表同步
     *
     * @param xdsx0007DataReqDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Xdsx0007DataRespDto getXdsx0007(Xdsx0007DataReqDto xdsx0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, JSON.toJSONString(xdsx0007DataReqDto));
        Xdsx0007DataRespDto xdsx0007DataRespDto = new Xdsx0007DataRespDto();
        try {
            //区分标识
            String dtghFlag = xdsx0007DataReqDto.getDtghFlag();
            //客户类型
            String cusType = xdsx0007DataReqDto.getCusType();
            //更新时间
            String upDateTime = xdsx0007DataReqDto.getSyncDate();
            //岗位编号
            String dutyCode = xdsx0007DataReqDto.getDutyId();
            Map param = new HashMap<>();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            //内评一次按岗位编号将所有数据同步过来，
            //将历史数据设置为无效
            Date updateDate = simpleDateFormat.parse(upDateTime);
            param.put("update", updateDate);
            param.put("ruleType", dtghFlag);
            param.put("cusType", cusType);
            if (Objects.equals(dtghFlag, CmisCfgConstants.RULE_TYPE_010)) {
                param.put("dutyCode", dutyCode);
                cfgApproveAuthorityMapper.updateRuleStatusByParam(param);
            } else if (Objects.equals(dtghFlag, CmisCfgConstants.RULE_TYPE_020)) {
                cfgApproveAuthorityMapper.updateRuleStatusByParam(param);
            }
            List<EmpoinfoList> empoinfoLists = xdsx0007DataReqDto.getEmpoinfoList();
            if (CollectionUtils.nonEmpty(empoinfoLists)) {
                List<CfgApproveAuthority> cfgApproveAuthorityList = empoinfoLists.stream().map(empoinfoList -> {
                    CfgApproveAuthority cfgApproveAuthority = new CfgApproveAuthority();
                    String uuid = UUID.randomUUID().toString().replace("-", "");
                    cfgApproveAuthority.setPkId(uuid);
                    cfgApproveAuthority.setDutyCode(dutyCode);
                    cfgApproveAuthority.setCusLevel(empoinfoList.getCusLvl());
                    cfgApproveAuthority.setDebtLevel(empoinfoList.getDebtLvl());
                    cfgApproveAuthority.setAuthAmt(new BigDecimal(empoinfoList.getAuthAmt()));
                    cfgApproveAuthority.setRuleStatus(CmisCfgConstants.RULE_STATUS_1);
                    cfgApproveAuthority.setRuleType(dtghFlag);
                    cfgApproveAuthority.setCreateTime(updateDate);
                    cfgApproveAuthority.setCusType(cusType);
                    cfgApproveAuthority.setOprType(CmisBizConstants.OPR_TYPE_01);
                    return cfgApproveAuthority;
                }).collect(Collectors.toList());
                cfgApproveAuthorityMapper.insertCfgApproveAuthorityList(cfgApproveAuthorityList);
            }
            xdsx0007DataRespDto.setOpFlag(CmisCfgConstants.OP_FLAG_S);
            xdsx0007DataRespDto.setOpMsg(CmisCfgConstants.OP_MSG_S);
        } catch (Exception e) {
            xdsx0007DataRespDto.setOpFlag(CmisCfgConstants.OP_FLAG_F);
            xdsx0007DataRespDto.setOpMsg(CmisCfgConstants.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, JSON.toJSONString(xdsx0007DataRespDto));
        return xdsx0007DataRespDto;
    }
}
