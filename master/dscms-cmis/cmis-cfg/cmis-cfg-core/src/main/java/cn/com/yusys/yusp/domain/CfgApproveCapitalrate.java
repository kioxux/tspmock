/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgApproveCapitalrate
 * @类描述: cfg_approve_capitalrate数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-13 12:35:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_approve_capitalrate")
public class CfgApproveCapitalrate extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 客户类型
     **/
    @Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
    private String cusType;

    /**
     * 岗位号
     **/
    @Column(name = "DUTY_CODE", unique = false, nullable = true, length = 40)
    private String dutyCode;

    /**
     * 上限值(含)
     **/
    @Column(name = "MAX_VAL", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal maxVal;

    /**
     * 下限值
     **/
    @Column(name = "MIN_VAL", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal minVal;

    /**
     * 状态
     **/
    @Column(name = "STATUS", unique = false, nullable = true, length = 5)
    private String status;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    public CfgApproveCapitalrate() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return cusType
     */
    public String getCusType() {
        return this.cusType;
    }

    /**
     * @param cusType
     */
    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    /**
     * @return dutyCode
     */
    public String getDutyCode() {
        return this.dutyCode;
    }

    /**
     * @param dutyCode
     */
    public void setDutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
    }

    /**
     * @return maxVal
     */
    public java.math.BigDecimal getMaxVal() {
        return this.maxVal;
    }

    /**
     * @param maxVal
     */
    public void setMaxVal(java.math.BigDecimal maxVal) {
        this.maxVal = maxVal;
    }

    /**
     * @return minVal
     */
    public java.math.BigDecimal getMinVal() {
        return this.minVal;
    }

    /**
     * @param minVal
     */
    public void setMinVal(java.math.BigDecimal minVal) {
        this.minVal = minVal;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }


}