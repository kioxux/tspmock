/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgPlanRuleRel;
import cn.com.yusys.yusp.service.CfgPlanRuleRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPlanRuleRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-04 17:07:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgplanrulerel")
public class CfgPlanRuleRelResource {
    @Autowired
    private CfgPlanRuleRelService cfgPlanRuleRelService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgPlanRuleRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgPlanRuleRel> list = cfgPlanRuleRelService.selectAll(queryModel);
        return new ResultDto<List<CfgPlanRuleRel>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgPlanRuleRel>> index(QueryModel queryModel) {
        List<CfgPlanRuleRel> list = cfgPlanRuleRelService.selectByModel(queryModel);
        return new ResultDto<List<CfgPlanRuleRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgPlanRuleRel> show(@PathVariable("pkId") String pkId) {
        CfgPlanRuleRel cfgPlanRuleRel = cfgPlanRuleRelService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgPlanRuleRel>(cfgPlanRuleRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgPlanRuleRel> create(@RequestBody CfgPlanRuleRel cfgPlanRuleRel) throws URISyntaxException {
        cfgPlanRuleRelService.insert(cfgPlanRuleRel);
        return new ResultDto<CfgPlanRuleRel>(cfgPlanRuleRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgPlanRuleRel cfgPlanRuleRel) throws URISyntaxException {
        int result = cfgPlanRuleRelService.update(cfgPlanRuleRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgPlanRuleRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgPlanRuleRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 逻根据获取方案编号、节点编号,逻辑删除该业务规则方案关联规则项的配置
     *
     * @param cfgPlanRuleRel
     * @return
     */
    @PostMapping("/deleteCfgPlanRuleRel")
    protected ResultDto<Integer> deleteCfgPlanRuleRel(@RequestBody CfgPlanRuleRel cfgPlanRuleRel) {
        int result = cfgPlanRuleRelService.deleteCfgPlanAndNodeRuleRel(cfgPlanRuleRel);
        return new ResultDto<Integer>(result);
    }
}
