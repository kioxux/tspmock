/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgInLimitRule;
import cn.com.yusys.yusp.service.CfgInLimitRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgInLimitRuleResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-05-24 10:33:29
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfginlimitrule")
public class CfgInLimitRuleResource {
    @Autowired
    private CfgInLimitRuleService cfgInLimitRuleService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgInLimitRule>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgInLimitRule> list = cfgInLimitRuleService.selectAll(queryModel);
        return new ResultDto<List<CfgInLimitRule>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgInLimitRule>> index(QueryModel queryModel) {
        List<CfgInLimitRule> list = cfgInLimitRuleService.selectByModel(queryModel);
        return new ResultDto<List<CfgInLimitRule>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgInLimitRule> show(@PathVariable("pkId") String pkId) {
        CfgInLimitRule cfgInLimitRule = cfgInLimitRuleService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgInLimitRule>(cfgInLimitRule);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgInLimitRule> create(@RequestBody CfgInLimitRule cfgInLimitRule) throws URISyntaxException {
        cfgInLimitRuleService.insert(cfgInLimitRule);
        return new ResultDto<CfgInLimitRule>(cfgInLimitRule);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgInLimitRule cfgInLimitRule) throws URISyntaxException {
        int result = cfgInLimitRuleService.update(cfgInLimitRule);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgInLimitRuleService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgInLimitRuleService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
