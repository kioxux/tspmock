package cn.com.yusys.yusp.constant;

/**
 * <p>
 * <h2>简述</h2>
 * 		<ol>新增修改删除状态码及其信息枚举类</ol>
 * <h2>功能描述</h2>
 * 		<ol>无</ol>
 * <h2>修改历史</h2>
 *    <ol>无</ol>
 * </p>
 *
 * @author duchao
 * @version 1.0
 * @2021-02-19
 */
public class StasYardConstance {
    /**
     * 成功
     */
    public final static int SUCC = 200;

    /**
     * 失败
     */
    public final static int FAIL = 300;

    /**
     * 提示信息
     */
    public final static int PRMP_INFO = 100;

    /**
     * 零
     */
    public final static int ZERO = 0;

    /**
     * 一
     */
    public final static int ONE = 1;

    /**
     * 新增成功
     */
    public final static String ADD_SUCC = "新增成功";

    /**
     * 新增失败
     */
    public final static String ADD_FAIL = "新增失败";

    /**
     * 更新成功
     */
    public final static String UPDT_SUCC = "更新成功";

    /**
     * 更新失败
     */
    public final static String UPDT_FAIL = "更新失败";

    /**
     * 删除成功
     */
    public final static String DEL_SUCC = "删除成功";

    /**
     * 删除失败
     */
    public final static String DEL_FAIL = "删除失败";

    /**
     * 数据已存在
     */
    public final static String DATA_EXST = "数据已存在!";

    /**
     * 删除失败
     */
    public final static String TRUE = "true";

    /**
     * 数据已存在
     */
    public final static String FALSE = "false";

    /**
     * 数据生效中，请勿重复操作！
     */
    public final static String IN_EFFECT = "数据生效中，请勿重复操作！";

    /**
     * 数据失效中，请勿重复操作！
     */
    public final static String FAILING = "数据失效中，请勿重复操作！";

    /**
     * 操作成功，数据已生效
     */
    public final static String IN_FORCE = "操作成功，数据已生效";

    /**
     * 操作成功，数据已失效
     */
    public final static String EXPIRED = "操作成功，数据已失效";

    /**
     * 操作成功，数据已失效
     */
    public final static String UPDT_STUS_FAIL = "更新状态失败";
}