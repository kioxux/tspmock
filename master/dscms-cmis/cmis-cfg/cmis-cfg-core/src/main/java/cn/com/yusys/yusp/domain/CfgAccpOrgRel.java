/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgAccpOrgRel
 * @类描述: cfg_accp_org_rel数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-18 21:12:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_accp_org_rel")
public class CfgAccpOrgRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 承兑机构号 **/
	@Column(name = "PAY_BR_NO", unique = false, nullable = true, length = 10)
	private String payBrNo;
	
	/** 管理机构 **/
	@Column(name = "MANAGER_BR_NO", unique = false, nullable = true, length = 10)
	private String managerBrNo;
	
	/** 承兑机构名称 **/
	@Column(name = "PAY_BR_NAME", unique = false, nullable = true, length = 200)
	private String payBrName;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param payBrNo
	 */
	public void setPayBrNo(String payBrNo) {
		this.payBrNo = payBrNo;
	}
	
    /**
     * @return payBrNo
     */
	public String getPayBrNo() {
		return this.payBrNo;
	}
	
	/**
	 * @param managerBrNo
	 */
	public void setManagerBrNo(String managerBrNo) {
		this.managerBrNo = managerBrNo;
	}
	
    /**
     * @return managerBrNo
     */
	public String getManagerBrNo() {
		return this.managerBrNo;
	}
	
	/**
	 * @param payBrName
	 */
	public void setPayBrName(String payBrName) {
		this.payBrName = payBrName;
	}
	
    /**
     * @return payBrName
     */
	public String getPayBrName() {
		return this.payBrName;
	}


}