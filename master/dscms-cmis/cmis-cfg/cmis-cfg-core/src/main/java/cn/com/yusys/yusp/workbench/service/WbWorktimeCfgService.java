/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.workbench.domain.WbWorktimeCfg;
import cn.com.yusys.yusp.workbench.repository.mapper.WbWorktimeCfgMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbWorktimeCfgService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 21:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class WbWorktimeCfgService {

    @Autowired
    private WbWorktimeCfgMapper wbWorktimeCfgMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public WbWorktimeCfg selectByPrimaryKey(String serno) {
        return wbWorktimeCfgMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<WbWorktimeCfg> selectAll(QueryModel model) {
        List<WbWorktimeCfg> records = (List<WbWorktimeCfg>) wbWorktimeCfgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<WbWorktimeCfg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<WbWorktimeCfg> list = wbWorktimeCfgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(WbWorktimeCfg record) {
        return wbWorktimeCfgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(WbWorktimeCfg record) {
        return wbWorktimeCfgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(WbWorktimeCfg record) {
        return wbWorktimeCfgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(WbWorktimeCfg record) {
        return wbWorktimeCfgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return wbWorktimeCfgMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return wbWorktimeCfgMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: getWorktimePm
     * @方法描述: 获取上下班时间
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map<String, String> getWorktimePm() {
        Map<String, String> resultMap = new HashMap<String, String>();
        String date = DateUtils.getCurrDateStr();
        String yearMonth = date.substring(5);
        Map<String, String> map = wbWorktimeCfgMapper.getWorktimePm(yearMonth);
        String work_time_am = map.get("workTimeAm");
        String work_time_pm = map.get("workTimePm");
        if (work_time_am != null && !"".equals(work_time_am)) {
            String[] arr = work_time_am.split("--");
            resultMap.put("am", arr[0]);
        } else {
            resultMap.put("am", "");
        }
        if (work_time_pm != null && !"".equals(work_time_pm)) {
            String[] arr = work_time_pm.split("--");
            resultMap.put("pm", arr[1]);
        } else {
            resultMap.put("pm", "");
        }
        return resultMap;
    }
}
