package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSOrgElecSeal
 * @类描述: cfg_s_org_elec_seal数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-06-10 22:30:15
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgSOrgElecSealDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 机构号
     **/
    private String orgNo;

    /**
     * 机构名称
     **/
    private String orgName;

    /**
     * 信贷合同章编码
     **/
    private String sealCode;

    /**
     * 信贷合同章名称
     **/
    private String sealName;

    /**
     * 信贷合同章密码
     **/
    private String sealPwd;

    /**
     * 抵押合同章编码
     **/
    private String sealPldCode;

    /**
     * 抵押合同章名称
     **/
    private String sealPldName;

    /**
     * 抵押合同章密码
     **/
    private String sealPldPwd;

    /**
     * 区域发展分类
     **/
    private String areaDevCateCd;

    /**
     * 是否集中作业
     **/
    private String isJzzyOrg;

    /**
     * 是否小贷
     **/
    private String isXwd;

    /**
     * 机构电子章是否启用
     **/
    private String sealStatus;

    /**
     * @return OrgNo
     */
    public String getOrgNo() {
        return this.orgNo;
    }

    /**
     * @param orgNo
     */
    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo == null ? null : orgNo.trim();
    }

    /**
     * @return OrgName
     */
    public String getOrgName() {
        return this.orgName;
    }

    /**
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    /**
     * @return SealCode
     */
    public String getSealCode() {
        return this.sealCode;
    }

    /**
     * @param sealCode
     */
    public void setSealCode(String sealCode) {
        this.sealCode = sealCode == null ? null : sealCode.trim();
    }

    /**
     * @return SealName
     */
    public String getSealName() {
        return this.sealName;
    }

    /**
     * @param sealName
     */
    public void setSealName(String sealName) {
        this.sealName = sealName == null ? null : sealName.trim();
    }

    /**
     * @return SealPwd
     */
    public String getSealPwd() {
        return this.sealPwd;
    }

    /**
     * @param sealPwd
     */
    public void setSealPwd(String sealPwd) {
        this.sealPwd = sealPwd == null ? null : sealPwd.trim();
    }

    /**
     * @return SealPldCode
     */
    public String getSealPldCode() {
        return this.sealPldCode;
    }

    /**
     * @param sealPldCode
     */
    public void setSealPldCode(String sealPldCode) {
        this.sealPldCode = sealPldCode == null ? null : sealPldCode.trim();
    }

    /**
     * @return SealPldName
     */
    public String getSealPldName() {
        return this.sealPldName;
    }

    /**
     * @param sealPldName
     */
    public void setSealPldName(String sealPldName) {
        this.sealPldName = sealPldName == null ? null : sealPldName.trim();
    }

    /**
     * @return SealPldPwd
     */
    public String getSealPldPwd() {
        return this.sealPldPwd;
    }

    /**
     * @param sealPldPwd
     */
    public void setSealPldPwd(String sealPldPwd) {
        this.sealPldPwd = sealPldPwd == null ? null : sealPldPwd.trim();
    }

    /**
     * @return AreaDevCateCd
     */
    public String getAreaDevCateCd() {
        return this.areaDevCateCd;
    }

    /**
     * @param areaDevCateCd
     */
    public void setAreaDevCateCd(String areaDevCateCd) {
        this.areaDevCateCd = areaDevCateCd == null ? null : areaDevCateCd.trim();
    }

    /**
     * @return IsJzzyOrg
     */
    public String getIsJzzyOrg() {
        return this.isJzzyOrg;
    }

    /**
     * @param isJzzyOrg
     */
    public void setIsJzzyOrg(String isJzzyOrg) {
        this.isJzzyOrg = isJzzyOrg == null ? null : isJzzyOrg.trim();
    }

    /**
     * @return IsXwd
     */
    public String getIsXwd() {
        return this.isXwd;
    }

    /**
     * @param isXwd
     */
    public void setIsXwd(String isXwd) {
        this.isXwd = isXwd == null ? null : isXwd.trim();
    }

    /**
     * @return SealStatus
     */
    public String getSealStatus() {
        return this.sealStatus;
    }

    /**
     * @param sealStatus
     */
    public void setSealStatus(String sealStatus) {
        this.sealStatus = sealStatus == null ? null : sealStatus.trim();
    }


}