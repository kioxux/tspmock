/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgSorgFina;
import cn.com.yusys.yusp.dto.CfgSorgFinaDto;
import cn.com.yusys.yusp.service.CfgSorgFinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSorgFinaResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 10:13:25
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgsorgfina")
public class CfgSorgFinaResource {
    @Autowired
    private CfgSorgFinaService cfgSorgFinaService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgSorgFina>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgSorgFina> list = cfgSorgFinaService.selectAll(queryModel);
        return new ResultDto<List<CfgSorgFina>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgSorgFina>> index(QueryModel queryModel) {
        List<CfgSorgFina> list = cfgSorgFinaService.selectByModel(queryModel);
        return new ResultDto<List<CfgSorgFina>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgSorgFina> show(@PathVariable("pkId") String pkId) {
        CfgSorgFina cfgSorgFina = cfgSorgFinaService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgSorgFina>(cfgSorgFina);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgSorgFina> create(@RequestBody CfgSorgFina cfgSorgFina) throws URISyntaxException {
        cfgSorgFinaService.insert(cfgSorgFina);
        return new ResultDto<CfgSorgFina>(cfgSorgFina);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgSorgFina cfgSorgFina) throws URISyntaxException {
        int result = cfgSorgFinaService.update(cfgSorgFina);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgSorgFinaService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgSorgFinaService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 zxz
     * @创建时间 2021/6/21 11:24
     * @注释 入账机构查询
     */
    @PostMapping("/selecsorgfina")
    protected ResultDto<List<CfgSorgFinaDto>> selecSorgFina(@RequestBody QueryModel queryModel) {
        List<CfgSorgFinaDto> list = cfgSorgFinaService.selecSorgFina(queryModel);
        return new ResultDto<List<CfgSorgFinaDto>>(list);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/5 19:21
     * @注释 财务机构查询
     */
    @PostMapping("/selectForXw")
    protected ResultDto<List<CfgSorgFina>> selectForXw(@RequestBody QueryModel queryModel) {
        List<CfgSorgFina> list = cfgSorgFinaService.selectForXw(queryModel);
        return new ResultDto<List<CfgSorgFina>>(list);
    }
}
