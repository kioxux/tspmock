/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.CfgPrdTypeProperties;
import cn.com.yusys.yusp.dto.CfgPrdTypePropertiesDto;
import cn.com.yusys.yusp.repository.mapper.CfgPrdTypePropertiesMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdTypePropertiesService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-07-02 10:30:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgPrdTypePropertiesService {

    @Autowired
    private CfgPrdTypePropertiesMapper cfgPrdTypePropertiesMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgPrdTypeProperties selectByPrimaryKey(String prdTypePropNo) {
        return cfgPrdTypePropertiesMapper.selectByPrimaryKey(prdTypePropNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgPrdTypeProperties> selectAll(QueryModel model) {
        List<CfgPrdTypeProperties> records = (List<CfgPrdTypeProperties>) cfgPrdTypePropertiesMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgPrdTypeProperties> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPrdTypeProperties> list = cfgPrdTypePropertiesMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgPrdTypeProperties record) {
        return cfgPrdTypePropertiesMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgPrdTypeProperties record) {
        return cfgPrdTypePropertiesMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgPrdTypeProperties record) {
        return cfgPrdTypePropertiesMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgPrdTypeProperties record) {
        return cfgPrdTypePropertiesMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String prdTypePropNo) {
        return cfgPrdTypePropertiesMapper.deleteByPrimaryKey(prdTypePropNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgPrdTypePropertiesMapper.deleteByIds(ids);
    }

    /**
     * 查询产品扩展属性
     *
     * @param typePropNo
     * @return
     */
    public List<CfgPrdTypePropertiesDto> queryCfgPrdTypePropertiesByProrNo(String typePropNo) {
        List<CfgPrdTypeProperties> cfgPrdTypePropertiesList = new ArrayList<>();
        List<CfgPrdTypePropertiesDto> cfgPrdTypePropertiesDtos = new ArrayList<>();
        cfgPrdTypePropertiesList = cfgPrdTypePropertiesMapper.queryCfgPrdTypePropertiesByProrNo(typePropNo);
        if (CollectionUtils.nonNull(cfgPrdTypePropertiesList) && cfgPrdTypePropertiesList.size() > 0) {
            cfgPrdTypePropertiesDtos = cfgPrdTypePropertiesList.stream().map(cfgPrdTypeProperties -> {
                CfgPrdTypePropertiesDto cfgPrdTypePropertiesDto = new CfgPrdTypePropertiesDto();
                BeanUtils.copyProperties(cfgPrdTypeProperties, cfgPrdTypePropertiesDto);
                return cfgPrdTypePropertiesDto;
            }).collect(Collectors.toList());
        }
        return cfgPrdTypePropertiesDtos;
    }


}
