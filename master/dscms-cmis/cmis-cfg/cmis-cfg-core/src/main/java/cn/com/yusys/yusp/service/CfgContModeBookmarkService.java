/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.CfgContModeBookmark;
import cn.com.yusys.yusp.repository.mapper.CfgContModeBookmarkMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgContModeBookmarkService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-17 17:32:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgContModeBookmarkService {

    @Autowired
    private CfgContModeBookmarkMapper cfgContModeBookmarkMapper;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgContModeBookmark selectByPrimaryKey(String pkId) {
        return cfgContModeBookmarkMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CfgContModeBookmark> selectAll(QueryModel model) {
        List<CfgContModeBookmark> records = (List<CfgContModeBookmark>) cfgContModeBookmarkMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgContModeBookmark> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgContModeBookmark> list = cfgContModeBookmarkMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CfgContModeBookmark record) {
        return cfgContModeBookmarkMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CfgContModeBookmark record) {
        return cfgContModeBookmarkMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CfgContModeBookmark record) {
        return cfgContModeBookmarkMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CfgContModeBookmark record) {
        return cfgContModeBookmarkMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return cfgContModeBookmarkMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return cfgContModeBookmarkMapper.deleteByIds(ids);
    }

    public int autoMark(CfgContModeBookmark cfgContModeBookmark) {
        int i = 0;
        //先删掉再加;
        cfgContModeBookmarkMapper.deleteByContTplCode(cfgContModeBookmark.getContTplCode(), cfgContModeBookmark.getBookmarkName());
        String bookmarks = cfgContModeBookmark.getBookmarkName();
        if (StringUtils.nonBlank(bookmarks)) {
            String[] marks = bookmarks.split(",");
            List<String> markList = Arrays.asList(marks);
            for (String mark : markList) {
                CfgContModeBookmark newCfgContModeBookmark = new CfgContModeBookmark();
                BeanUtils.copyProperties(cfgContModeBookmark, newCfgContModeBookmark);
                String pkValue = sequenceTemplateClient.getSequenceTemplate("PK_VALUE", new HashMap<>());
                newCfgContModeBookmark.setPkId(pkValue);
                newCfgContModeBookmark.setBookmarkName(mark);
                //有的话不插了
                QueryModel model = new QueryModel();
                model.addCondition("contTplCode", cfgContModeBookmark.getContTplCode());
                model.addCondition("bookmarkName", cfgContModeBookmark.getBookmarkName());
                List<CfgContModeBookmark> result = cfgContModeBookmarkMapper.selectByModel(model);
                if (result == null || result.size() < 1) {
                    i = cfgContModeBookmarkMapper.insert(newCfgContModeBookmark);
                }

            }
        }

        return i;
    }

    public List<CfgContModeBookmark> getListByContTplCode(String contTplCode) {
        return cfgContModeBookmarkMapper.getListByContTplCode(contTplCode);

    }
}
