/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgContModeBookmark;
import cn.com.yusys.yusp.service.CfgContModeBookmarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgContModeBookmarkResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-17 17:32:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgcontmodebookmark")
public class CfgContModeBookmarkResource {
    @Autowired
    private CfgContModeBookmarkService cfgContModeBookmarkService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgContModeBookmark>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgContModeBookmark> list = cfgContModeBookmarkService.selectAll(queryModel);
        return new ResultDto<List<CfgContModeBookmark>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgContModeBookmark>> index(QueryModel queryModel) {
        List<CfgContModeBookmark> list = cfgContModeBookmarkService.selectByModel(queryModel);
        return new ResultDto<List<CfgContModeBookmark>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgContModeBookmark> show(@PathVariable("pkId") String pkId) {
        CfgContModeBookmark cfgContModeBookmark = cfgContModeBookmarkService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgContModeBookmark>(cfgContModeBookmark);
    }


    @GetMapping("/getlistbyconttplcode/{contTplCode}")
    protected ResultDto<List<CfgContModeBookmark>> getListByContTplCode(@PathVariable("contTplCode") String contTplCode) {
        List<CfgContModeBookmark> result = cfgContModeBookmarkService.getListByContTplCode(contTplCode);
        return new ResultDto<List<CfgContModeBookmark>>(result);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgContModeBookmark> create(@RequestBody CfgContModeBookmark cfgContModeBookmark) throws URISyntaxException {
        cfgContModeBookmarkService.insert(cfgContModeBookmark);
        return new ResultDto<CfgContModeBookmark>(cfgContModeBookmark);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgContModeBookmark cfgContModeBookmark) throws URISyntaxException {
        int result = cfgContModeBookmarkService.update(cfgContModeBookmark);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgContModeBookmarkService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgContModeBookmarkService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 加书签表
     *
     * @param cfgContModeBookmark
     * @return
     * @throws URISyntaxException
     * @throws IOException
     */
    @PostMapping("/automark")
    protected ResultDto<Integer> autoMark(@RequestBody CfgContModeBookmark cfgContModeBookmark) throws URISyntaxException, IOException {
        int result = cfgContModeBookmarkService.autoMark(cfgContModeBookmark);
        return new ResultDto<Integer>(result);
    }
}
