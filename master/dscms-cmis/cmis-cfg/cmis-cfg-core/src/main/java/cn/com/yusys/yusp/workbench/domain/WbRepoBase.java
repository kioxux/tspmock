/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbRepoBase
 * @类描述: wb_repo_base数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-15 15:31:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "wb_repo_base")
public class WbRepoBase extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "SERNO")
    private String serno;

    /**
     * 目录编号
     **/
    @Column(name = "CATALOG_ID", unique = false, nullable = true, length = 32)
    private String catalogId;

    /**
     * 目录名称
     **/
    @Column(name = "CATALOG_NAME", unique = false, nullable = true, length = 256)
    private String catalogName;

    /**
     * 标题
     **/
    @Column(name = "TITLE", unique = false, nullable = true, length = 500)
    private String title;

    /**
     * 内容
     **/
    @Column(name = "CONTENT", unique = false, nullable = true, length = 1000)
    private String content;

    /**
     * 角色权限
     **/
    @Column(name = "ROLE_RIGHT", unique = false, nullable = true, length = 40)
    private String roleRight;

    /**
     * 部分角色范围
     **/
    @Column(name = "ROLE_RANGE", unique = false, nullable = true, length = 500)
    private String roleRange;

    /**
     * 机构权限
     **/
    @Column(name = "ORG_RIGHT", unique = false, nullable = true, length = 40)
    private String orgRight;

    /**
     * 部分机构范围
     **/
    @Column(name = "ORG_RANGE", unique = false, nullable = true, length = 500)
    private String orgRange;

    /**
     * 附件
     **/
    @Column(name = "ACCESSORY", unique = false, nullable = true, length = 1000)
    private String accessory;

    /**
     * 状态
     **/
    @Column(name = "STATUS", unique = false, nullable = true, length = 5)
    private String status;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 主管客户经理
     **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 主管机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 操作类型
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /**
     * @return serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno;
    }

    /**
     * @return catalogId
     */
    public String getCatalogId() {
        return this.catalogId;
    }

    /**
     * @param catalogId
     */
    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    /**
     * @return catalogName
     */
    public String getCatalogName() {
        return this.catalogName;
    }

    /**
     * @param catalogName
     */
    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return content
     */
    public String getContent() {
        return this.content;
    }

    /**
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return roleRight
     */
    public String getRoleRight() {
        return this.roleRight;
    }

    /**
     * @param roleRight
     */
    public void setRoleRight(String roleRight) {
        this.roleRight = roleRight;
    }

    /**
     * @return roleRange
     */
    public String getRoleRange() {
        return this.roleRange;
    }

    /**
     * @param roleRange
     */
    public void setRoleRange(String roleRange) {
        this.roleRange = roleRange;
    }

    /**
     * @return orgRight
     */
    public String getOrgRight() {
        return this.orgRight;
    }

    /**
     * @param orgRight
     */
    public void setOrgRight(String orgRight) {
        this.orgRight = orgRight;
    }

    /**
     * @return orgRange
     */
    public String getOrgRange() {
        return this.orgRange;
    }

    /**
     * @param orgRange
     */
    public void setOrgRange(String orgRange) {
        this.orgRange = orgRange;
    }

    /**
     * @return accessory
     */
    public String getAccessory() {
        return this.accessory;
    }

    /**
     * @param accessory
     */
    public void setAccessory(String accessory) {
        this.accessory = accessory;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }


}