package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgDataFlow
 * @类描述: cfg_data_flow数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-28 17:02:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgDataFlowDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 数据流编号
     **/
    private String dataFlowId;

    /**
     * 数据流名称
     **/
    private String dataFlowName;

    /**
     * 源数据表
     **/
    private String sourTable;

    /**
     * 汇聚表
     **/
    private String convTable;

    /**
     * 映射内容
     **/
    private String mappContent;

    /**
     * 备注
     **/
    private String remark;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 最后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 源数据表数据源
     **/
    private String sourDatasource;

    /**
     * 汇聚表数据源
     **/
    private String convDatasource;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    private String oprType;

    /**
     * @return DataFlowId
     */
    public String getDataFlowId() {
        return this.dataFlowId;
    }

    /**
     * @param dataFlowId
     */
    public void setDataFlowId(String dataFlowId) {
        this.dataFlowId = dataFlowId == null ? null : dataFlowId.trim();
    }

    /**
     * @return DataFlowName
     */
    public String getDataFlowName() {
        return this.dataFlowName;
    }

    /**
     * @param dataFlowName
     */
    public void setDataFlowName(String dataFlowName) {
        this.dataFlowName = dataFlowName == null ? null : dataFlowName.trim();
    }

    /**
     * @return SourTable
     */
    public String getSourTable() {
        return this.sourTable;
    }

    /**
     * @param sourTable
     */
    public void setSourTable(String sourTable) {
        this.sourTable = sourTable == null ? null : sourTable.trim();
    }

    /**
     * @return ConvTable
     */
    public String getConvTable() {
        return this.convTable;
    }

    /**
     * @param convTable
     */
    public void setConvTable(String convTable) {
        this.convTable = convTable == null ? null : convTable.trim();
    }

    /**
     * @return MappContent
     */
    public String getMappContent() {
        return this.mappContent;
    }

    /**
     * @param mappContent
     */
    public void setMappContent(String mappContent) {
        this.mappContent = mappContent == null ? null : mappContent.trim();
    }

    /**
     * @return Remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return SourDatasource
     */
    public String getSourDatasource() {
        return this.sourDatasource;
    }

    /**
     * @param sourDatasource
     */
    public void setSourDatasource(String sourDatasource) {
        this.sourDatasource = sourDatasource == null ? null : sourDatasource.trim();
    }

    /**
     * @return ConvDatasource
     */
    public String getConvDatasource() {
        return this.convDatasource;
    }

    /**
     * @param convDatasource
     */
    public void setConvDatasource(String convDatasource) {
        this.convDatasource = convDatasource == null ? null : convDatasource.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }


}