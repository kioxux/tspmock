/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgModelGroup
 * @类描述: cfg_model_group数据实体类
 * @功能描述:
 * @创建人: 15430
 * @创建时间: 2020-11-20 16:46:49
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_model_group")
public class CfgModelGroup extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 模版组编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "MODEL_GROUP_NO")
    private String modelGroupNo;

    /**
     * 模板组名称
     **/
    @Column(name = "MODEL_GROUP_NAME", unique = false, nullable = true, length = 100)
    private String modelGroupName;

    /**
     * 模板显示方式 STD_ZB_SHOW_MODE
     **/
    @Column(name = "SHOW_MODE", unique = false, nullable = true, length = 5)
    private String showMode;

    /**
     * 版本号
     **/
    @Column(name = "VER", unique = false, nullable = true, length = 5)
    private String ver;

    /**
     * 是否关联作业流 STD_ZB_YES_NO
     **/
    @Column(name = "IS_JOB_FLOW", unique = false, nullable = true, length = 5)
    private String isJobFlow;

    /**
     * 作业流编号
     **/
    @Column(name = "JOB_FLOW", unique = false, nullable = true, length = 40)
    private String jobFlow;

    /**
     * 业务规则方案编号
     **/
    @Column(name = "PLAN_ID", unique = false, nullable = true, length = 40)
    private String planId;

    /**
     * 备注
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 400)
    private String remark;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 更新人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 更新机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 更新日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    public CfgModelGroup() {
        // Not compliant
    }

    /**
     * @return modelGroupNo
     */
    public String getModelGroupNo() {
        return this.modelGroupNo;
    }

    /**
     * @param modelGroupNo
     */
    public void setModelGroupNo(String modelGroupNo) {
        this.modelGroupNo = modelGroupNo;
    }

    /**
     * @return modelGroupName
     */
    public String getModelGroupName() {
        return this.modelGroupName;
    }

    /**
     * @param modelGroupName
     */
    public void setModelGroupName(String modelGroupName) {
        this.modelGroupName = modelGroupName;
    }

    /**
     * @return showMode
     */
    public String getShowMode() {
        return this.showMode;
    }

    /**
     * @param showMode
     */
    public void setShowMode(String showMode) {
        this.showMode = showMode;
    }

    /**
     * @return ver
     */
    public String getVer() {
        return this.ver;
    }

    /**
     * @param ver
     */
    public void setVer(String ver) {
        this.ver = ver;
    }

    /**
     * @return isJobFlow
     */
    public String getIsJobFlow() {
        return this.isJobFlow;
    }

    /**
     * @param isJobFlow
     */
    public void setIsJobFlow(String isJobFlow) {
        this.isJobFlow = isJobFlow;
    }

    /**
     * @return jobFlow
     */
    public String getJobFlow() {
        return this.jobFlow;
    }

    /**
     * @param jobFlow
     */
    public void setJobFlow(String jobFlow) {
        this.jobFlow = jobFlow;
    }

    /**
     * @return planId
     */
    public String getPlanId() {
        return this.planId;
    }

    /**
     * @param planId
     */
    public void setPlanId(String planId) {
        this.planId = planId;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }


}