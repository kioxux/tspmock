/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgOrderDownload;
import cn.com.yusys.yusp.service.CfgOrderDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrderDownloadResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-24 10:20:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgorderdownload")
public class CfgOrderDownloadResource {
    @Autowired
    private CfgOrderDownloadService cfgOrderDownloadService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgOrderDownload>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgOrderDownload> list = cfgOrderDownloadService.selectAll(queryModel);
        return new ResultDto<List<CfgOrderDownload>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgOrderDownload>> index(QueryModel queryModel) {
        List<CfgOrderDownload> list = cfgOrderDownloadService.selectByModel(queryModel);
        return new ResultDto<List<CfgOrderDownload>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgOrderDownload> show(@PathVariable("pkId") String pkId) {
        CfgOrderDownload cfgOrderDownload = cfgOrderDownloadService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgOrderDownload>(cfgOrderDownload);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgOrderDownload> create(@RequestBody CfgOrderDownload cfgOrderDownload) throws URISyntaxException {
        cfgOrderDownloadService.insert(cfgOrderDownload);
        return new ResultDto<CfgOrderDownload>(cfgOrderDownload);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgOrderDownload cfgOrderDownload) throws URISyntaxException {
        int result = cfgOrderDownloadService.update(cfgOrderDownload);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgOrderDownloadService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgOrderDownloadService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 获取配置
     *
     * @param pkId
     * @return
     */
    @GetMapping("getordercfg/{pkId}")
    protected CfgOrderDownload getOrderCfg(@PathVariable("pkId") String pkId) {
        return cfgOrderDownloadService.getOrderCfg(pkId);
    }

    /**
     * 获取预约下载的运行中的配置
     *
     * @param dataSource 各个调用预约下载的微服务的编号
     * @return
     */
    @GetMapping("getordercfgbydatasource/{dataSource}")
    protected List<CfgOrderDownload> getOrderCfgByDataSource(@PathVariable("dataSource") String dataSource) {
        return cfgOrderDownloadService.getOrderCfgByDataSource(dataSource);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatestatus")
    protected ResultDto<Integer> updateSatus(@RequestBody CfgOrderDownload cfgOrderDownload) throws URISyntaxException {
        int result = cfgOrderDownloadService.updateSelective(cfgOrderDownload);
        return new ResultDto<Integer>(result);
    }

}
