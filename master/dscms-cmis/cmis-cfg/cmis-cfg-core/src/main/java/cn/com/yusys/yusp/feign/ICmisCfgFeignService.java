package cn.com.yusys.yusp.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 调用yuxpservice提供的API接口
 */
@FeignClient("yuxpservice")
public interface ICmisCfgFeignService {
    /**
     * 根据某岗位ID查询岗位信息
     *
     * @param dutyId
     * @return
     */
    @GetMapping(value = "/api/authoritysearch/getDutys/{dutyId}")
    public String getDuty(@PathVariable("dutyId") String dutyId);
}
