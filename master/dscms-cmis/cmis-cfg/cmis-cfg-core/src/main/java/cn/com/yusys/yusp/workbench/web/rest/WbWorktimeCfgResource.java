/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.workbench.domain.WbWorktimeCfg;
import cn.com.yusys.yusp.workbench.service.WbWorktimeCfgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbWorktimeCfgResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 21:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/wbworktimecfg")
public class WbWorktimeCfgResource {
    @Autowired
    private WbWorktimeCfgService wbWorktimeCfgService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<WbWorktimeCfg>> query() {
        QueryModel queryModel = new QueryModel();
        List<WbWorktimeCfg> list = wbWorktimeCfgService.selectAll(queryModel);
        return new ResultDto<List<WbWorktimeCfg>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<WbWorktimeCfg>> index(@RequestBody QueryModel queryModel) {
        List<WbWorktimeCfg> list = wbWorktimeCfgService.selectByModel(queryModel);
        return new ResultDto<List<WbWorktimeCfg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<WbWorktimeCfg> show(@PathVariable("serno") String serno) {
        WbWorktimeCfg wbWorktimeCfg = wbWorktimeCfgService.selectByPrimaryKey(serno);
        return new ResultDto<WbWorktimeCfg>(wbWorktimeCfg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<WbWorktimeCfg> create(@RequestBody WbWorktimeCfg wbWorktimeCfg) throws URISyntaxException {
        wbWorktimeCfgService.insert(wbWorktimeCfg);
        return new ResultDto<WbWorktimeCfg>(wbWorktimeCfg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody WbWorktimeCfg wbWorktimeCfg) throws URISyntaxException {
        int result = wbWorktimeCfgService.update(wbWorktimeCfg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = wbWorktimeCfgService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = wbWorktimeCfgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updatebylist
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatebylist")
    protected ResultDto<Integer> updatebylist(@RequestBody List<WbWorktimeCfg> list) {
        int result = 0;
        for (int i = 0; i < list.size(); i++) {
            WbWorktimeCfg wbWorktimeCfg = list.get(i);
            result = wbWorktimeCfgService.update(wbWorktimeCfg);
            if (result <= 0) break;
        }
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getWorktimePm
     * @函数描述:获取上下班时间
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/getWorktimePm")
    protected ResultDto<Map<String, String>> getWorktimePm() {
        Map<String, String> result = wbWorktimeCfgService.getWorktimePm();
        return new ResultDto<Map<String, String>>(result);
    }

}
