/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CfgOhterBusiPrintTemp;
import cn.com.yusys.yusp.service.CfgOhterBusiPrintTempService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOhterBusiPrintTempResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 19:33:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "其他业务打印模板配置表")
@RequestMapping("/api/cfgohterbusiprinttemp")
public class CfgOhterBusiPrintTempResource {
    @Autowired
    private CfgOhterBusiPrintTempService cfgOhterBusiPrintTempService;

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgOhterBusiPrintTemp> create(@RequestBody CfgOhterBusiPrintTemp cfgOhterBusiPrintTemp) throws URISyntaxException {
        cfgOhterBusiPrintTempService.insert(cfgOhterBusiPrintTemp);
        return new ResultDto<CfgOhterBusiPrintTemp>(cfgOhterBusiPrintTemp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgOhterBusiPrintTemp cfgOhterBusiPrintTemp) throws URISyntaxException {
        int result = cfgOhterBusiPrintTempService.update(cfgOhterBusiPrintTemp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgOhterBusiPrintTempService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgOhterBusiPrintTempService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:其他业务打印模板配置列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("其他业务打印模板配置列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<CfgOhterBusiPrintTemp>> toSignList(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CfgOhterBusiPrintTemp> list = cfgOhterBusiPrintTempService.toSignList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CfgOhterBusiPrintTemp>>(list);
    }

    /**
     * 其他业务打印模板配置逻辑删除
     *
     * @param cfgOhterBusiPrintTemp
     * @return
     */
    @ApiOperation("其他业务打印模板配置逻辑删除")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody CfgOhterBusiPrintTemp cfgOhterBusiPrintTemp) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        Integer result = cfgOhterBusiPrintTempService.logicDelete(cfgOhterBusiPrintTemp);
        if (result > 0) {
            resultDto.setCode(0);
            resultDto.setData(1);
            resultDto.setMessage("删除成功！");
        } else {
            resultDto.setCode(000);
            resultDto.setData(0);
            resultDto.setMessage("删除失败！");
        }
        return resultDto;
    }

    /**
     * @函数名称:queryCfgOhterBusiPrintTempDataByParams
     * @函数描述:通过入参查询，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过入参查询数据")
    @PostMapping("/querycfgohterbusiprinttempdatabyparams")
    protected ResultDto<CfgOhterBusiPrintTemp> queryCfgOhterBusiPrintTempDataByParams(@RequestBody Map map) {
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        CfgOhterBusiPrintTemp cfgOhterBusiPrintTemp = cfgOhterBusiPrintTempService.queryCfgOhterBusiPrintTempDataByParams(map);
        return new ResultDto<CfgOhterBusiPrintTemp>(cfgOhterBusiPrintTemp);
    }

    /**
     * 其他业务打印模板配置操作
     *
     * @param cfgOhterBusiPrintTemp
     * @return
     */
    @ApiOperation("其他业务打印模板配置新增保存操作")
    @PostMapping("/savecfgohterbusiprinttempinfo")
    public ResultDto<Map> saveCfgOhterBusiPrintTempInfo(@RequestBody CfgOhterBusiPrintTemp cfgOhterBusiPrintTemp) {
        Map result = cfgOhterBusiPrintTempService.saveCfgOhterBusiPrintTempInfo(cfgOhterBusiPrintTemp);
        return new ResultDto<>(result);
    }

    /**
     * 其他业务打印模板配置通用的保存方法
     *
     * @param params
     * @return
     */
    @ApiOperation("其他业务打印模板配置通用的保存方法")
    @PostMapping("/commonsavecfgohterbusiprinttempinfo")
    public ResultDto<Map> commonSaveCfgOhterBusiPrintTempInfo(@RequestBody Map params) {
        Map rtnData = cfgOhterBusiPrintTempService.commonSaveCfgOhterBusiPrintTempInfo(params);
        return new ResultDto<>(rtnData);
    }
}
