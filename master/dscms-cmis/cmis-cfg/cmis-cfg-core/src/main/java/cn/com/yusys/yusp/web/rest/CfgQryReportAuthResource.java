/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgQryReportAuth;
import cn.com.yusys.yusp.service.CfgQryReportAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgQryReportAuthResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-02-25 21:16:53
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgqryreportauth")
public class CfgQryReportAuthResource {
    @Autowired
    private CfgQryReportAuthService cfgQryReportAuthService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgQryReportAuth>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgQryReportAuth> list = cfgQryReportAuthService.selectAll(queryModel);
        return new ResultDto<List<CfgQryReportAuth>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgQryReportAuth>> index(QueryModel queryModel) {
        List<CfgQryReportAuth> list = cfgQryReportAuthService.selectByModel(queryModel);
        return new ResultDto<List<CfgQryReportAuth>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgQryReportAuth> show(@PathVariable("pkId") String pkId) {
        CfgQryReportAuth cfgQryReportAuth = cfgQryReportAuthService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgQryReportAuth>(cfgQryReportAuth);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgQryReportAuth> create(@RequestBody CfgQryReportAuth cfgQryReportAuth) throws URISyntaxException {
        cfgQryReportAuthService.insert(cfgQryReportAuth);
        return new ResultDto<CfgQryReportAuth>(cfgQryReportAuth);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgQryReportAuth cfgQryReportAuth) throws URISyntaxException {
        int result = cfgQryReportAuthService.update(cfgQryReportAuth);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgQryReportAuthService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgQryReportAuthService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据角色编号查询该角色权限下的报表列表
     *
     * @param map
     * @return
     */
    @PostMapping("/queryReportListByRoleCode")
    protected ResultDto<List<CfgQryReportAuth>> queryReportListByRoleCode(@RequestBody Map map) {
        List<CfgQryReportAuth> rtnList = cfgQryReportAuthService.queryReportListByRoleCode(map);
        return new ResultDto<List<CfgQryReportAuth>>(rtnList);
    }

    /**
     * 维护角色的报表权限
     *
     * @param map
     * @return
     */
    @PostMapping("/updateReportListByRoleCode")
    protected ResultDto<Integer> updateReportListByRoleCode(@RequestBody Map map) {
        int result = cfgQryReportAuthService.updateReportListByRoleCode(map);
        return new ResultDto<Integer>(result);
    }
}
