/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.risk.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.risk.domain.PrdPvRiskScene;
import cn.com.yusys.yusp.risk.repository.mapper.PrdPvRiskSceneMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: yusp-npam-biz-core模块
 * @类名称: PrdPvRiskSceneService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2020-07-27 17:26:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PrdPvRiskSceneService {

    @Autowired
    private PrdPvRiskSceneMapper prdPvRiskSceneMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PrdPvRiskScene selectByPrimaryKey(String sceneId, String preventId, String itemId, String wfid, String riskLevel, String itemName) {
        return prdPvRiskSceneMapper.selectByPrimaryKey(sceneId, preventId, itemId, wfid, riskLevel, itemName);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map<String, String>> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, String>> list = prdPvRiskSceneMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(List<PrdPvRiskScene> records) {
        records.forEach(item -> {
            prdPvRiskSceneMapper.insert(item);
        });
        return records.size();
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PrdPvRiskScene record) {
        return prdPvRiskSceneMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PrdPvRiskScene record) {
        return prdPvRiskSceneMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PrdPvRiskScene record) {
        return prdPvRiskSceneMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String sceneId, String preventId, String bizType) {
        return prdPvRiskSceneMapper.deleteByPrimaryKey(sceneId, preventId, bizType);
    }

    /**
     * <br/>
     * 1.0 tangxun:2020/7/29 9:19 上午: <br/>
     *
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.String>>
     * @author tangxun
     * @date ​2020/7/29 9:19 上午
     * @version 1.0
     * @since 2020/7/29 9:19 上午
     */
    public List<Map<String, String>> selectDistinctScence(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, String>> list = prdPvRiskSceneMapper.selectDistinctScence(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * <br/>
     * 1.0 tangxun:2020/7/29 9:22 上午: <br/>
     *
     * @param [model]
     * @return java.util.List<cn.com.yusys.yusp.risk.domain.PrdPvRiskScene>
     * @author tangxun
     * @date ​2020/7/29 9:22 上午
     * @version 1.0
     * @since 2020/7/29 9:22 上午
     */
    public List<Map<String, String>> selectInitScence(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, String>> list = prdPvRiskSceneMapper.selectInitScence(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * <br/>
     * 1.0 tangxun:2020/7/30 10:50 上午: <br/>
     *
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.String>>
     * @author tangxun
     * @date ​2020/7/30 10:50 上午
     * @version 1.0
     * @since 2020/7/30 10:50 上午
     */
    public List<Map<String, String>> getRiskItem(QueryModel model) {
        List<Map<String, String>> list = prdPvRiskSceneMapper.getRiskItem(model);
        return list;
    }

    /**
     * <br/>
     * 1.0 zhangliang15:2021/09/14 10:50 上午: <br/>
     *
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.String>>
     * @author zhangliang15
     * @date ​2021/09/14 10:50 上午
     * @version 1.0
     * @since 2021/09/14 10:50 上午
     */
    public List<Map<String, String>> getRiskNoFlowItem(QueryModel model) {
        List<Map<String, String>> list = prdPvRiskSceneMapper.getRiskNoFlowItem(model);
        return list;
    }

}
