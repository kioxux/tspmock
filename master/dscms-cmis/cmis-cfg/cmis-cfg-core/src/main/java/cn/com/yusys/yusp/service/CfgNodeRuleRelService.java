/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgNodeRuleRel;
import cn.com.yusys.yusp.repository.mapper.CfgNodeRuleRelMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgNodeRuleRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-03 21:47:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgNodeRuleRelService {
    private static final Logger log = LoggerFactory.getLogger(CfgNodeRuleRelService.class);

    @Autowired
    private CfgNodeRuleRelMapper cfgNodeRuleRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgNodeRuleRel selectByPrimaryKey(String pkId) {
        return cfgNodeRuleRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgNodeRuleRel> selectAll(QueryModel model) {
        List<CfgNodeRuleRel> records = (List<CfgNodeRuleRel>) cfgNodeRuleRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgNodeRuleRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgNodeRuleRel> list = cfgNodeRuleRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgNodeRuleRel record) {
        return cfgNodeRuleRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgNodeRuleRel record) {
        return cfgNodeRuleRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgNodeRuleRel record) {
        return cfgNodeRuleRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgNodeRuleRel record) {
        return cfgNodeRuleRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 根据获取工作流编号,逻辑删除该业务规则方案关联流程的配置
     *
     * @param cfgNodeRuleRel
     * @return
     */
    public int deleteCfgNodeRuleRel(CfgNodeRuleRel cfgNodeRuleRel) {
        int result = cfgNodeRuleRelMapper.updateByWfiSignId(cfgNodeRuleRel.getWfiSignId(), cfgNodeRuleRel.getPlanId(), CommonConstant.DELETE_OPR);
        log.info("根据工作流编号删除业务规则方案关联流程条数：{}", result);
        return result;
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgNodeRuleRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgNodeRuleRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateByRuleItemIdAndPlanId
     * @方法描述: 逻辑删除，根据节点编号,规业务规则方案更新"操作标识"字段
     * @参数与返回说明: ruleItemId
     * @参数与返回说明: planId
     * @参数与返回说明: opType
     * @算法描述: 无
     */
    public int updateByRuleItemIdAndPlanId(String ruleItemId, String planId, String opType) {
        return cfgNodeRuleRelMapper.updateByRuleItemIdAndPlanId(ruleItemId, planId, opType);
    }

    /**
     * @方法名称: getNodePlanRuleList
     * @方法描述: 据业务规则方案编号、流程编号、节点编号查询的关联规则项列表.
     * @参数与返回说明: CfgNodeRuleRel列表
     * @算法描述: 无
     */
    public List<CfgNodeRuleRel> getNodePlanRuleList(CfgNodeRuleRel cfgNodeRuleRel) {
        //获取已保存的流程节点关联的业务规则项
        List<CfgNodeRuleRel> listCfgNodeRuleRel = cfgNodeRuleRelMapper.queryByPlanWfiNode(cfgNodeRuleRel);
        log.info("节点关联规则项数：{}", listCfgNodeRuleRel.size());

        //获取配置的业务规则方案关联规则项
        List<CfgNodeRuleRel> listCfgPlanRuleRel = cfgNodeRuleRelMapper.queryCfgPlanRuleRelByPlan(cfgNodeRuleRel);
        log.info("总联规则项数：{}", listCfgPlanRuleRel.size());
        //如果 ‘已保存的流程节点关联的业务规则项’ 数量小于 ‘配置的业务规则方案关联规则项’数量，则将其他未保存的规则补充后默认展示在界面
        //如果‘已保存的流程节点关联的业务规则项’数量=0，则直接将 ‘配置的业务规则方案关联规则项’全部补充
        if (listCfgNodeRuleRel.size() <= listCfgPlanRuleRel.size() && listCfgNodeRuleRel.size() != 0) {
            for (CfgNodeRuleRel tempNode : listCfgNodeRuleRel) {
                for (int i = 0; i < listCfgPlanRuleRel.size(); i++) {
                    CfgNodeRuleRel tempPlan = listCfgPlanRuleRel.get(i);
                    if (tempNode.getRuleItemId().equals(tempPlan.getRuleItemId())) {
                        listCfgPlanRuleRel.remove(i);
                        continue;
                    }
                }
            }
        }
        listCfgNodeRuleRel.addAll(listCfgPlanRuleRel);
        return listCfgNodeRuleRel;
    }
}
