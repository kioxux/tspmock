/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgApproveCapitalrate;
import cn.com.yusys.yusp.dto.CfgApproveCapitalrateDto;
import cn.com.yusys.yusp.service.CfgApproveCapitalrateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgApproveCapitalrateResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-13 12:35:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgapprovecapitalrate")
public class CfgApproveCapitalrateResource {
    @Autowired
    private CfgApproveCapitalrateService cfgApproveCapitalrateService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgApproveCapitalrate>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgApproveCapitalrate> list = cfgApproveCapitalrateService.selectAll(queryModel);
        return new ResultDto<List<CfgApproveCapitalrate>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgApproveCapitalrate>> index(QueryModel queryModel) {
        List<CfgApproveCapitalrate> list = cfgApproveCapitalrateService.selectByModel(queryModel);
        return new ResultDto<List<CfgApproveCapitalrate>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgApproveCapitalrate> show(@PathVariable("pkId") String pkId) {
        CfgApproveCapitalrate cfgApproveCapitalrate = cfgApproveCapitalrateService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgApproveCapitalrate>(cfgApproveCapitalrate);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgApproveCapitalrate> create(@RequestBody CfgApproveCapitalrate cfgApproveCapitalrate) throws URISyntaxException {
        cfgApproveCapitalrateService.insert(cfgApproveCapitalrate);
        return new ResultDto<CfgApproveCapitalrate>(cfgApproveCapitalrate);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgApproveCapitalrate cfgApproveCapitalrate) throws URISyntaxException {
        int result = cfgApproveCapitalrateService.update(cfgApproveCapitalrate);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgApproveCapitalrateService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgApproveCapitalrateService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: queryDutyByRate
     * @方法描述: 根据占用率获取岗位
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/querydutybyrate")
    protected ResultDto<CfgApproveCapitalrateDto> queryDutyByRate(@RequestBody CfgApproveCapitalrateDto cfgApproveCapitalrateDto) {
        return ResultDto.success(cfgApproveCapitalrateService.queryDutyByRate(cfgApproveCapitalrateDto.getRate()));
    }
}
