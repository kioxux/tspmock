/*
 * Automatically generated by code generator
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgRateCalculation;
import cn.com.yusys.yusp.repository.mapper.CfgRateCalculationMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @Project Name: cmis-cfg-coreModule
 * @Class Name: CfgRateCalculationService
 * @Class Description: #Service Class
 * @Function Description:
 * @Creator: hhj123456
 * @Create Time: 2021-06-18 17:23:40
 * @Modification Note:
 * @Modification Records: Modified Time    Modified By    Modified Reason
 * -------------------------------------------------------------
 * @Copyright (c) Yusys Technologies Co., Ltd. All Rights Reserved.
 */
@Service
@Transactional
public class CfgRateCalculationService {

    @Autowired
    private CfgRateCalculationMapper cfgRateCalculationMapper;

    /**
     * @Method Name: selectByPrimaryKey
     * @Method Description: Query by primary key
     * @Parameter and Return Description:
     * @Algorithm Description:
     */
    public CfgRateCalculation selectByPrimaryKey(String pkId) {
        return cfgRateCalculationMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @Method Name: selectAll
     * @Method Description: Query all data
     * @Parameter and Return Description:
     * @Algorithm Description:
     */
    @Transactional(readOnly = true)
    public List<CfgRateCalculation> selectAll(QueryModel model) {
        List<CfgRateCalculation> records = (List<CfgRateCalculation>) cfgRateCalculationMapper.selectByModel(model);
        return records;
    }

    /**
     * @Method Name: selectByModel
     * @Method Description: Query by condition - query paging
     * @Parameter and Return Description:
     * @Algorithm Description:
     */

    public List<CfgRateCalculation> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgRateCalculation> list = cfgRateCalculationMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @Method Name: insert
     * @Method Description: Insert
     * @Parameter and Return Description:
     * @Algorithm Description:
     */
    public int insert(CfgRateCalculation record) {
        return cfgRateCalculationMapper.insert(record);
    }

    /**
     * @Method Name: insertSelective
     * @Method Description: Insert - Insert only non-empty fields
     * @Parameter and Return Description:
     * @Algorithm Description:
     */
    public int insertSelective(CfgRateCalculation record) {
        return cfgRateCalculationMapper.insertSelective(record);
    }

    /**
     * @Method Name: update
     * @Method Description: Update by primary key
     * @Parameter and Return Description:
     * @Algorithm Description:
     */
    public int update(CfgRateCalculation record) {
        return cfgRateCalculationMapper.updateByPrimaryKey(record);
    }

    /**
     * @Method Name: updateSelective
     * @Method Description: Update by primary key - update only non-empty fields
     * @Parameter and Return Description:
     * @Algorithm Description:
     */
    public int updateSelective(CfgRateCalculation record) {
        return cfgRateCalculationMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @Method Name: deleteByPrimaryKey
     * @Method Description: Delete by primary key
     * @Parameter and Return Description:
     * @Algorithm Description:
     */
    public int deleteByPrimaryKey(String pkId) {
        return cfgRateCalculationMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @Method Name: deleteByIds
     * @Method Description: Delete by primary keys
     * @Parameter and Return Description:
     * @Algorithm Description:
     */
    public int deleteByIds(String ids) {
        return cfgRateCalculationMapper.deleteByIds(ids);
    }
}
