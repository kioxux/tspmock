/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgXdLoanOrg
 * @类描述: cfg_xd_loan_org数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-20 20:03:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_xd_loan_org")
public class CfgXdLoanOrg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 小贷所属机构 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "XD_ORG")
	private String xdOrg;
	
	/** 小贷所属机构名称 **/
	@Column(name = "XD_ORG_NAME", unique = false, nullable = false, length = 200)
	private String xdOrgName;
	
	/** 地区账务机构,用于贷款出账 **/
	@Column(name = "XD_FINA_ORG", unique = false, nullable = false, length = 10)
	private String xdFinaOrg;
	
	/** 地区账务机构名称 **/
	@Column(name = "XD_FINA_ORG_NAME", unique = false, nullable = true, length = 200)
	private String xdFinaOrgName;
	
	/** 片区账务机构,用于抵质押品入库账务 **/
	@Column(name = "XD_FINA_MANAGER_ORG", unique = false, nullable = true, length = 10)
	private String xdFinaManagerOrg;
	
	/** 片区账务机构名称 **/
	@Column(name = "XD_FINA_MANAGER_ORG_NAME", unique = false, nullable = true, length = 200)
	private String xdFinaManagerOrgName;
	
	/** 片区管理机构,放款机构 **/
	@Column(name = "XD_PVP_ORG", unique = false, nullable = true, length = 10)
	private String xdPvpOrg;
	
	/** 放款机构名称 **/
	@Column(name = "XD_PVP_ORG_NAME", unique = false, nullable = true, length = 200)
	private String xdPvpOrgName;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param xdOrg
	 */
	public void setXdOrg(String xdOrg) {
		this.xdOrg = xdOrg;
	}
	
    /**
     * @return xdOrg
     */
	public String getXdOrg() {
		return this.xdOrg;
	}
	
	/**
	 * @param xdOrgName
	 */
	public void setXdOrgName(String xdOrgName) {
		this.xdOrgName = xdOrgName;
	}
	
    /**
     * @return xdOrgName
     */
	public String getXdOrgName() {
		return this.xdOrgName;
	}
	
	/**
	 * @param xdFinaOrg
	 */
	public void setXdFinaOrg(String xdFinaOrg) {
		this.xdFinaOrg = xdFinaOrg;
	}
	
    /**
     * @return xdFinaOrg
     */
	public String getXdFinaOrg() {
		return this.xdFinaOrg;
	}
	
	/**
	 * @param xdFinaOrgName
	 */
	public void setXdFinaOrgName(String xdFinaOrgName) {
		this.xdFinaOrgName = xdFinaOrgName;
	}
	
    /**
     * @return xdFinaOrgName
     */
	public String getXdFinaOrgName() {
		return this.xdFinaOrgName;
	}
	
	/**
	 * @param xdFinaManagerOrg
	 */
	public void setXdFinaManagerOrg(String xdFinaManagerOrg) {
		this.xdFinaManagerOrg = xdFinaManagerOrg;
	}
	
    /**
     * @return xdFinaManagerOrg
     */
	public String getXdFinaManagerOrg() {
		return this.xdFinaManagerOrg;
	}
	
	/**
	 * @param xdFinaManagerOrgName
	 */
	public void setXdFinaManagerOrgName(String xdFinaManagerOrgName) {
		this.xdFinaManagerOrgName = xdFinaManagerOrgName;
	}
	
    /**
     * @return xdFinaManagerOrgName
     */
	public String getXdFinaManagerOrgName() {
		return this.xdFinaManagerOrgName;
	}
	
	/**
	 * @param xdPvpOrg
	 */
	public void setXdPvpOrg(String xdPvpOrg) {
		this.xdPvpOrg = xdPvpOrg;
	}
	
    /**
     * @return xdPvpOrg
     */
	public String getXdPvpOrg() {
		return this.xdPvpOrg;
	}
	
	/**
	 * @param xdPvpOrgName
	 */
	public void setXdPvpOrgName(String xdPvpOrgName) {
		this.xdPvpOrgName = xdPvpOrgName;
	}
	
    /**
     * @return xdPvpOrgName
     */
	public String getXdPvpOrgName() {
		return this.xdPvpOrgName;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}