/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgJobFlowNode
 * @类描述: cfg_job_flow_node数据实体类
 * @功能描述:
 * @创建人: qhy
 * @创建时间: 2021-05-06 21:52:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_job_flow_node")
public class CfgJobFlowNode extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 作业流编号（关联外键）
     **/
    @Column(name = "JOB_FLOW_ID", unique = false, nullable = false, length = 40)
    private String jobFlowId;

    /**
     * 节点编号
     **/
    @Column(name = "NODE_ID", unique = false, nullable = false, length = 40)
    private String nodeId;

    /**
     * 工作流编号
     **/
    @Column(name = "WFI_SIGN_ID", unique = false, nullable = true, length = 40)
    private String wfiSignId;

    /**
     * 审批查看模板组
     **/
    @Column(name = "APPR_VIEW_URL", unique = false, nullable = true, length = 200)
    private String apprViewUrl;

    /**
     * 审批调整模板组
     **/
    @Column(name = "APPR_ADJUST_TPL", unique = false, nullable = true, length = 200)
    private String apprAdjustTpl;

    /**
     * 是否需要打回/退回原因 STD_ZB_YES_NO
     **/
    @Column(name = "IS_OPT_TYP", unique = false, nullable = true, length = 5)
    private String isOptTyp;

    /**
     * 业务规则方案编号
     **/
    @Column(name = "PLAN_ID", unique = false, nullable = true, length = 40)
    private String planId;

    /**
     * 备注
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 400)
    private String remark;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    public CfgJobFlowNode() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return jobFlowId
     */
    public String getJobFlowId() {
        return this.jobFlowId;
    }

    /**
     * @param jobFlowId
     */
    public void setJobFlowId(String jobFlowId) {
        this.jobFlowId = jobFlowId;
    }

    /**
     * @return nodeId
     */
    public String getNodeId() {
        return this.nodeId;
    }

    /**
     * @param nodeId
     */
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * @return wfiSignId
     */
    public String getWfiSignId() {
        return this.wfiSignId;
    }

    /**
     * @param wfiSignId
     */
    public void setWfiSignId(String wfiSignId) {
        this.wfiSignId = wfiSignId;
    }

    /**
     * @return apprViewUrl
     */
    public String getApprViewUrl() {
        return this.apprViewUrl;
    }

    /**
     * @param apprViewUrl
     */
    public void setApprViewUrl(String apprViewUrl) {
        this.apprViewUrl = apprViewUrl;
    }

    /**
     * @return apprAdjustTpl
     */
    public String getApprAdjustTpl() {
        return this.apprAdjustTpl;
    }

    /**
     * @param apprAdjustTpl
     */
    public void setApprAdjustTpl(String apprAdjustTpl) {
        this.apprAdjustTpl = apprAdjustTpl;
    }

    /**
     * @return isOptTyp
     */
    public String getIsOptTyp() {
        return this.isOptTyp;
    }

    /**
     * @param isOptTyp
     */
    public void setIsOptTyp(String isOptTyp) {
        this.isOptTyp = isOptTyp;
    }

    /**
     * @return planId
     */
    public String getPlanId() {
        return this.planId;
    }

    /**
     * @param planId
     */
    public void setPlanId(String planId) {
        this.planId = planId;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }


}