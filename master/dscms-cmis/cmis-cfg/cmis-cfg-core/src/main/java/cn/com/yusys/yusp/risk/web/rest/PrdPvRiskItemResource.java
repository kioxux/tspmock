/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.risk.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.risk.domain.PrdPvRiskItem;
import cn.com.yusys.yusp.risk.service.PrdPvRiskItemService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @version 1.0.0
 * @项目名称: yusp-npam-biz-core模块
 * @类名称: PrdPvRiskItemResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2020-07-27 17:26:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(description = "风险拦截分项")
@RestController
@RequestMapping("/api/prdpvriskitem")
public class PrdPvRiskItemResource {
    private static final Logger log = LoggerFactory.getLogger(PrdPvRiskItemResource.class);
    @Autowired
    private PrdPvRiskItemService prdPvRiskItemService;

    /**
     * 全表查询.
     *
     * @param queryModel
     * @return
     */
    @PostMapping("/demo")
    protected ResultDto<Map<String, String>> demo(@RequestBody QueryModel queryModel) throws InterruptedException, NoSuchAlgorithmException {
        System.out.println(queryModel.getCondition());
        HashMap<String, String> map = new HashMap<>();

        Random random = null;
        try {
            random = SecureRandom.getInstanceStrong();
            Thread.sleep(random.nextInt(1000));
            int res = random.nextInt(10);
            if (res >= 5) {
                map.put("result", "3");
                map.put("resultDesc", "检查不通过，拦截");
            } else {
                map.put("result", "1");
                map.put("resultDesc", "检查通过!");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return new ResultDto<>(map);
    }

    @PostMapping("/demo2")
    protected ResultDto<Map<String, String>> demo2(@RequestBody QueryModel queryModel) throws InterruptedException {
        System.out.println(queryModel.getCondition());
        HashMap<String, String> map = new HashMap<>();
        Random random = null;
        try {
            random = SecureRandom.getInstanceStrong();
            int res = random.nextInt(10);
            Thread.sleep(random.nextInt(5000));
            if (res >= 5) {
                map.put("result", "3");
                map.put("resultDesc", "检查不通过，拦截");
            } else {
                map.put("result", "1");
                map.put("resultDesc", "检查通过!");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return new ResultDto<>(map);
    }

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PrdPvRiskItem>> query() {
        QueryModel queryModel = new QueryModel();
        List<PrdPvRiskItem> list = prdPvRiskItemService.selectAll(queryModel);
        return new ResultDto<List<PrdPvRiskItem>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PrdPvRiskItem>> index(QueryModel queryModel) {
        List<PrdPvRiskItem> list = prdPvRiskItemService.selectByModel(queryModel);
        return new ResultDto<List<PrdPvRiskItem>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{itemId}")
    protected ResultDto<PrdPvRiskItem> show(@PathVariable("itemId") String itemId) {
        PrdPvRiskItem prdPvRiskItem = prdPvRiskItemService.selectByPrimaryKey(itemId);
        return new ResultDto<PrdPvRiskItem>(prdPvRiskItem);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PrdPvRiskItem> create(@RequestBody PrdPvRiskItem prdPvRiskItem) throws URISyntaxException {
        prdPvRiskItemService.insert(prdPvRiskItem);
        return new ResultDto<PrdPvRiskItem>(prdPvRiskItem);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PrdPvRiskItem prdPvRiskItem) throws URISyntaxException {
        int result = prdPvRiskItemService.update(prdPvRiskItem);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{itemId}")
    protected ResultDto<Integer> delete(@PathVariable("itemId") String itemId) {
        int result = prdPvRiskItemService.deleteByPrimaryKey(itemId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = prdPvRiskItemService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
