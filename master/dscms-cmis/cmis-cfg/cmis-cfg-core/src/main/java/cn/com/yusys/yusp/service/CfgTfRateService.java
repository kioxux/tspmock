/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgTfRate;
import cn.com.yusys.yusp.dto.CfgTfRateDto;
import cn.com.yusys.yusp.dto.CfgTfRateQueryDto;
import cn.com.yusys.yusp.repository.mapper.CfgTfRateMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgTfRateService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-11 09:46:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgTfRateService {

    @Autowired
    private CfgTfRateMapper cfgTfRateMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgTfRate selectByPrimaryKey(String pkId) {
        return cfgTfRateMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgTfRate> selectAll(QueryModel model) {
        List<CfgTfRate> records = (List<CfgTfRate>) cfgTfRateMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgTfRate> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgTfRate> list = cfgTfRateMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgTfRate record) {
        return cfgTfRateMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgTfRate record) {
        return cfgTfRateMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgTfRate record) {
        return cfgTfRateMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgTfRate record) {
        return cfgTfRateMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgTfRateMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgTfRateMapper.deleteByIds(ids);
    }

    /**
     * 查询汇率信息
     *
     * @param cfgTfRateQueryDto
     * @return
     */
    public CfgTfRateDto queryCfgTfRate(CfgTfRateQueryDto cfgTfRateQueryDto) {
        CfgTfRate cfgTfRate = cfgTfRateMapper.queryCfgTfRate(cfgTfRateQueryDto);
        CfgTfRateDto cfgTfRateDto = new CfgTfRateDto();
        if (Objects.nonNull(cfgTfRate)) {
            BeanUtils.copyProperties(cfgTfRate, cfgTfRateDto);
        }
        return cfgTfRateDto;
    }

    /**
     * 查询所有汇率信息
     *
     * @param model
     * @return
     */
    public List<CfgTfRateDto> queryAllCfgTfRate(QueryModel model) {
        List<CfgTfRate> cfgTfRateList = cfgTfRateMapper.selectByModel(model);
        List<CfgTfRateDto> cfgTfRateDtoList = new ArrayList<>();
        for(CfgTfRate cfgTfRate : cfgTfRateList){
            CfgTfRateDto cfgTfRateDto = new CfgTfRateDto();
            if (Objects.nonNull(cfgTfRate)) {
                BeanUtils.copyProperties(cfgTfRate, cfgTfRateDto);
                cfgTfRateDtoList.add(cfgTfRateDto);
            }
        }
        return cfgTfRateDtoList;
    }
}
