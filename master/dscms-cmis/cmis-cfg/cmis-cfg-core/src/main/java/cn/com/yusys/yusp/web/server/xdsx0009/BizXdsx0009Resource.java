package cn.com.yusys.yusp.web.server.xdsx0009;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdsx0009.req.Xdsx0009DataReqDto;
import cn.com.yusys.yusp.server.xdsx0009.resp.Xdsx0009DataRespDto;
import cn.com.yusys.yusp.service.server.xdsx0009.Xdsx0009Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户准入级别同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDSX0009:客户准入级别同步")
@RestController
@RequestMapping("/api/cfgsx4bsp")
public class BizXdsx0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0009Resource.class);

    @Autowired
    private Xdsx0009Service xdsx0009Service;

    /**
     * 交易码：xdsx0009
     * 交易描述：客户准入级别同步
     *
     * @param xdsx0009DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户准入级别同步")
    @PostMapping("/xdsx0009")
    protected @ResponseBody
    ResultDto<Xdsx0009DataRespDto> xdsx0009(@Validated @RequestBody Xdsx0009DataReqDto xdsx0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, JSON.toJSONString(xdsx0009DataReqDto));
        Xdsx0009DataRespDto xdsx0009DataRespDto = new Xdsx0009DataRespDto();// 响应Dto:客户准入级别同步
        ResultDto<Xdsx0009DataRespDto> xdsx0009DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, JSON.toJSONString(xdsx0009DataReqDto));
            xdsx0009DataRespDto = xdsx0009Service.getXdsx0009(xdsx0009DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, JSON.toJSONString(xdsx0009DataRespDto));
            // 封装xdsx0009DataResultDto中正确的返回码和返回信息
            xdsx0009DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0009DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, e.getMessage());
            xdsx0009DataResultDto.setCode(e.getErrorCode());
            xdsx0009DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, e.getMessage());
            // 封装xdsx0009DataResultDto中异常返回码和返回信息
            xdsx0009DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0009DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0009DataRespDto到xdsx0009DataResultDto中
        xdsx0009DataResultDto.setData(xdsx0009DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, JSON.toJSONString(xdsx0009DataResultDto));
        return xdsx0009DataResultDto;
    }
}
