package cn.com.yusys.yusp.web.server.xdsx0025;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdsx0025.req.Xdsx0025DataReqDto;
import cn.com.yusys.yusp.server.xdsx0025.resp.Xdsx0025DataRespDto;
import cn.com.yusys.yusp.service.server.xdsx0025.Xdsx0025Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:审批人资本占用率参数表同步
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDSX0025:审批人资本占用率参数表同步")
@RestController
@RequestMapping("/api/cfgsx4bsp")
public class BizXdsx0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0025Resource.class);
    @Autowired
    private Xdsx0025Service xdsx0025Service;

    /**
     * 交易码：xdsx0025
     * 交易描述：审批人资本占用率参数表同步
     *
     * @param xdsx0025DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("审批人资本占用率参数表同步")
    @PostMapping("/xdsx0025")
    protected @ResponseBody
    ResultDto<Xdsx0025DataRespDto> xdsx0025(@Validated @RequestBody Xdsx0025DataReqDto xdsx0025DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, JSON.toJSONString(xdsx0025DataReqDto));
        Xdsx0025DataRespDto xdsx0025DataRespDto = new Xdsx0025DataRespDto();// 响应Dto:审批人资本占用率参数表同步
        ResultDto<Xdsx0025DataRespDto> xdsx0025DataResultDto = new ResultDto<>();
        try {
            // 从xdsx0025DataReqDto获取业务值进行业务逻辑处理
            //  调用xdsx0025Service层开始
            xdsx0025DataRespDto = xdsx0025Service.xdsx0025(xdsx0025DataReqDto);
            //  调用xdsx0025Service层结束
            // 封装xdsx0025DataResultDto中正确的返回码和返回信息
            xdsx0025DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0025DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, e.getMessage());
            // 封装xdsx0025DataResultDto中异常返回码和返回信息
            xdsx0025DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0025DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0025DataRespDto到xdsx0025DataResultDto中
        xdsx0025DataResultDto.setData(xdsx0025DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, JSON.toJSONString(xdsx0025DataRespDto));
        return xdsx0025DataResultDto;
    }
}
