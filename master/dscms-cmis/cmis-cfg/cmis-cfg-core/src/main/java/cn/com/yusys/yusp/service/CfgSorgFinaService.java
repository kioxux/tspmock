/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgSorgFina;
import cn.com.yusys.yusp.dto.CfgSorgFinaDto;
import cn.com.yusys.yusp.repository.mapper.CfgSorgFinaMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSorgFinaService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 10:13:25
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgSorgFinaService {

    @Autowired
    private CfgSorgFinaMapper cfgSorgFinaMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgSorgFina selectByPrimaryKey(String pkId) {
        return cfgSorgFinaMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgSorgFina> selectAll(QueryModel model) {
        List<CfgSorgFina> records = (List<CfgSorgFina>) cfgSorgFinaMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgSorgFina> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgSorgFina> list = cfgSorgFinaMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgSorgFina record) {
        return cfgSorgFinaMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgSorgFina record) {
        return cfgSorgFinaMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgSorgFina record) {
        return cfgSorgFinaMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgSorgFina record) {
        return cfgSorgFinaMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgSorgFinaMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgSorgFinaMapper.deleteByIds(ids);
    }

    /**
     * @创建人 zxz
     * @创建时间 2021/6/21 11:24
     * @注释 入账机构查询
     */
    public List<CfgSorgFinaDto> selecSorgFina(QueryModel queryModel) {
        return cfgSorgFinaMapper.selecSorgFina(queryModel);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/5 18:59
     * @注释 查询入账机构 管理机构和财务机构对应表
     */
    public List<CfgSorgFina> selectForXw(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgSorgFina> list = cfgSorgFinaMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

}
