package cn.com.yusys.yusp.constant;

/**
 * 序列模板编号常量类，用于生成序列号
 */
public class SeqConstant {

    /**
     * 序列模板编号，用于生成序列号作为项目池与分配规则关联关系表的主键
     */
    public static final String TP_RULE_PK = "TP_RULE_PK";
}
