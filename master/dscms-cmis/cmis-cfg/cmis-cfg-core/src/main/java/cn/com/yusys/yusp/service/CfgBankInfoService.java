/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgBankInfo;
import cn.com.yusys.yusp.repository.mapper.CfgBankInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBankInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Empty
 * @创建时间: 2021-04-29 14:15:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgBankInfoService {

    @Autowired
    private CfgBankInfoMapper cfgBankInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgBankInfo selectByPrimaryKey(String bankNo) {
        CfgBankInfo cfgBankInfo =cfgBankInfoMapper.selectByPrimaryKey(bankNo);
        return cfgBankInfo;
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgBankInfo> selectAll(QueryModel model) {
        List<CfgBankInfo> records = (List<CfgBankInfo>) cfgBankInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgBankInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgBankInfo> list = cfgBankInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgBankInfo record) {
        return cfgBankInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgBankInfo record) {
        return cfgBankInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgBankInfo record) {
        return cfgBankInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgBankInfo record) {
        return cfgBankInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String bankNo) {
        return cfgBankInfoMapper.deleteByPrimaryKey(bankNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgBankInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectAllByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgBankInfo> selectAllByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgBankInfo> list = cfgBankInfoMapper.selectAllByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryCfgBankInfoDataByParams
     * @方法描述: 根据入参查询行号数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-07-26 20:00:00
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<CfgBankInfo> queryCfgBankInfoDataByParams(Map queryMap) {
        return cfgBankInfoMapper.queryCfgBankInfoDataByParams(queryMap);
    }

    /**
     * 根据行号查上级行号
     *
     * @param bankNo
     * @return
     */
    public String querySuperBankNoByBankNo(String bankNo) {
        return cfgBankInfoMapper.querySuperBankNoByBankNo(bankNo);
    }

    /**
     * 根据行号查询数据
     * @param bankNo
     * @return
     */
    public CfgBankInfo selectByBankNo(String bankNo) {
        return cfgBankInfoMapper.selectByBankNo(bankNo);
    }
}
