/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgExchgRate
 * @类描述: cfg_exchg_rate数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-12 15:29:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_exchg_rate")
public class CfgExchgRate extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 原币种  STD_ZB_CUR_TYP
     **/
    @Column(name = "ORIGI_CUR_TYPE", unique = false, nullable = true, length = 5)
    private String origiCurType;

    /**
     * 对照币种 STD_ZB_CUR_TYP
     **/
    @Column(name = "COMP_CUR_TYPE", unique = false, nullable = true, length = 5)
    private String compCurType;

    /**
     * 钞买价
     **/
    @Column(name = "CSH_BUY_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal cshBuyRate;

    /**
     * 中间汇率
     **/
    @Column(name = "MID_REMIT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal midRemit;

    /**
     * 汇买入汇率
     **/
    @Column(name = "BUY_EXCHG_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal buyExchgRate;

    /**
     * 汇卖出汇率
     **/
    @Column(name = "SLD_EXCHG_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal sldExchgRate;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 逻辑删除标识符
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgExchgRate() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return origiCurType
     */
    public String getOrigiCurType() {
        return this.origiCurType;
    }

    /**
     * @param origiCurType
     */
    public void setOrigiCurType(String origiCurType) {
        this.origiCurType = origiCurType;
    }

    /**
     * @return compCurType
     */
    public String getCompCurType() {
        return this.compCurType;
    }

    /**
     * @param compCurType
     */
    public void setCompCurType(String compCurType) {
        this.compCurType = compCurType;
    }

    /**
     * @return cshBuyRate
     */
    public java.math.BigDecimal getCshBuyRate() {
        return this.cshBuyRate;
    }

    /**
     * @param cshBuyRate
     */
    public void setCshBuyRate(java.math.BigDecimal cshBuyRate) {
        this.cshBuyRate = cshBuyRate;
    }

    /**
     * @return midRemit
     */
    public java.math.BigDecimal getMidRemit() {
        return this.midRemit;
    }

    /**
     * @param midRemit
     */
    public void setMidRemit(java.math.BigDecimal midRemit) {
        this.midRemit = midRemit;
    }

    /**
     * @return buyExchgRate
     */
    public java.math.BigDecimal getBuyExchgRate() {
        return this.buyExchgRate;
    }

    /**
     * @param buyExchgRate
     */
    public void setBuyExchgRate(java.math.BigDecimal buyExchgRate) {
        this.buyExchgRate = buyExchgRate;
    }

    /**
     * @return sldExchgRate
     */
    public java.math.BigDecimal getSldExchgRate() {
        return this.sldExchgRate;
    }

    /**
     * @param sldExchgRate
     */
    public void setSldExchgRate(java.math.BigDecimal sldExchgRate) {
        this.sldExchgRate = sldExchgRate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}