package cn.com.yusys.yusp.web.server.cmiscfg0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.server.cmiscfg0002.req.CmisCfg0002ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002RespDto;
import cn.com.yusys.yusp.service.server.cmiscfg0002.CmisCfg0002Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 根据产品编号获取产品扩展属性
 *
 * @author zhangjw 20210714
 * @version 1.0
 */
@Api(tags = "cmiscfg0002:根据产品编号获取产品扩展属性")
@RestController
@RequestMapping("/api/cfg4inner")
public class CmisCfg0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCfg0002Resource.class);

    @Autowired
    private CmisCfg0002Service cmisCfg0002Service;

    /**
     * 交易码：cmiscfg0002
     * 交易描述：新增首页提醒事项
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据产品编号获取产品扩展属性")
    @PostMapping("/cmiscfg0002")
    protected @ResponseBody
    ResultDto<CmisCfg0002RespDto> cmisCfg0002(@Validated @RequestBody CmisCfg0002ReqDto reqDto) {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.value, JSON.toJSONString(reqDto));
        ResultDto<CmisCfg0002RespDto> cmisCfg0002RespDtoResultDto = new ResultDto<>();
        CmisCfg0002RespDto cmisCfg0002RespDto = new CmisCfg0002RespDto();
        try {
            // 调用对应的service层
            cmisCfg0002RespDto = cmisCfg0002Service.execute(reqDto);

        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.value, e.getMessage());
            // 封装cmisCfg0002RespDto中异常返回码和返回信息
            cmisCfg0002RespDto.setErrorCode(EpbEnum.EPB099999.key);
            cmisCfg0002RespDto.setErrorMsg(EpbEnum.EPB099999.value);
        }
        // 封装cmisCfg0002RespDto到cmisCfg0002RespDtoResultDto中
        cmisCfg0002RespDtoResultDto.setData(cmisCfg0002RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0002.value, JSON.toJSONString(cmisCfg0002RespDto));
        return cmisCfg0002RespDtoResultDto;
    }
}
