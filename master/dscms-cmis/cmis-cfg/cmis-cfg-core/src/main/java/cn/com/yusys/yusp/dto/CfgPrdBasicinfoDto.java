package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class CfgPrdBasicinfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 目录编号
     */
    private String catalogId;

    /**
     * 目录名称
     */
    private String catalogName;

    /**
     * 目录层级
     */
    private String catalogLevelId;

    /**
     * 上级目录编码
     */
    private String supCatalogId;

    /**
     * 产品编号
     */
    private String prdId;

    /**
     * 上级产品编号
     */
    private String supPrdId;

    /**
     * 产品名称
     */
    private String prdName;

    /**
     * 目录层级名称
     */
    private String catalogLevelName;

    /**
     * 产品版本号
     */
    private String prdVersion;

    /**
     * 是否基础产品
     */
    private String isBasicPrd;

    /**
     * 基础产品编号
     */
    private String basicPrdId;

    /**
     * 所属法人机构
     */
    private String instuCde;

    /**
     * 生效日期
     */
    private String startDate;

    /**
     * 失效日期
     */
    private String endDate;

    /**
     * 产品描述
     */
    private String prdDescribe;

    /**
     * 产品状态
     */
    private String prdStatus;


    /**
     * 放款方式
     */
    private String disbWay;

    /**
     * 产品入口作业流编号
     */
    private String wfiSignId;

    /**
     * 业务规则方案编号
     */
    private String planId;

    /**
     * 产品模版编号
     */
    private String modelGroupNo;

    /**
     * 核心产品号
     */
    private String corePrdId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 排序
     */
    private java.math.BigDecimal serialNumber;

    /**
     * 登记人
     */
    private String inputId;

    /**
     * 登记机构
     */
    private String inputBrId;

    /**
     * 登记日期
     */
    private String inputDate;

    /**
     * 更新人
     */
    private String updId;

    /**
     * 更新机构
     */
    private String updBrId;

    /**
     * 更新日期
     */
    private String updDate;

    /**
     * 适用币种
     */
    private String suitCurType;

    /**
     * 操作类型
     */
    private String oprType;

    /**
     * 适用调查报告类型
     */
    private String suitIndgtReportType;

    /**
     * 适用合同类型
     */
    private String suitContType;

    /**
     * 适用担保方式
     */
    private String suitGuarMode;

    /**
     * 适用支付方式
     */
    private String suitPayMode;

    /**
     * 适用还款方式
     */
    private String SuitRepayMode;

    /**
     * 是否需要线下调查
     */
    private String isStopOffline;

    /**
     * 是否允许展期
     */
    private String isAllowExt;

    /**
     * 是否允许线上签约
     */
    private String isAllowSignOnline;

    /**
     * 是否允许线上放款
     */
    private String isAllowDisbOnline;

    /**
     * 产品所属条线
     */
    private String prdBelgLine;
    private String planName;
    private String prdType;
    private String modelGroupName;

    public String getDisbWay() {
        return disbWay;
    }

    public void setDisbWay(String disbWay) {
        this.disbWay = disbWay;
    }

    public String getIsStopOffline() {
        return isStopOffline;
    }

    public void setIsStopOffline(String isStopOffline) {
        this.isStopOffline = isStopOffline;
    }

    public String getIsAllowExt() {
        return isAllowExt;
    }

    public void setIsAllowExt(String isAllowExt) {
        this.isAllowExt = isAllowExt;
    }

    public String getIsAllowSignOnline() {
        return isAllowSignOnline;
    }

    public void setIsAllowSignOnline(String isAllowSignOnline) {
        this.isAllowSignOnline = isAllowSignOnline;
    }

    public String getIsAllowDisbOnline() {
        return isAllowDisbOnline;
    }

    public void setIsAllowDisbOnline(String isAllowDisbOnline) {
        this.isAllowDisbOnline = isAllowDisbOnline;
    }

    public String getSuitIndgtReportType() {
        return suitIndgtReportType;
    }

    public void setSuitIndgtReportType(String suitIndgtReportType) {
        this.suitIndgtReportType = suitIndgtReportType;
    }

    public String getSuitContType() {
        return suitContType;
    }

    public void setSuitContType(String suitContType) {
        this.suitContType = suitContType;
    }

    public String getSuitGuarMode() {
        return suitGuarMode;
    }

    public void setSuitGuarMode(String suitGuarMode) {
        this.suitGuarMode = suitGuarMode;
    }

    public String getSuitPayMode() {
        return suitPayMode;
    }

    public void setSuitPayMode(String suitPayMode) {
        this.suitPayMode = suitPayMode;
    }

    public String getSuitRepayMode() {
        return SuitRepayMode;
    }

    public void setSuitRepayMode(String suitRepayMode) {
        SuitRepayMode = suitRepayMode;
    }

    public String getSuitCurType() {
        return suitCurType;
    }

    public void setSuitCurType(String suitCurType) {
        this.suitCurType = suitCurType;
    }

    public String getPrdType() {
        return prdType;
    }

    public void setPrdType(String prdType) {
        this.prdType = prdType;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getModelGroupName() {
        return modelGroupName;
    }

    public void setModelGroupName(String modelGroupName) {
        this.modelGroupName = modelGroupName;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public String getCatalogLevelId() {
        return catalogLevelId;
    }

    public void setCatalogLevelId(String catalogLevelId) {
        this.catalogLevelId = catalogLevelId;
    }

    public String getSupCatalogId() {
        return supCatalogId;
    }

    public void setSupCatalogId(String supCatalogId) {
        this.supCatalogId = supCatalogId;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getSupPrdId() {
        return supPrdId;
    }

    public void setSupPrdId(String supPrdId) {
        this.supPrdId = supPrdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCatalogLevelName() {
        return catalogLevelName;
    }

    public void setCatalogLevelName(String catalogLevelName) {
        this.catalogLevelName = catalogLevelName;
    }

    public String getPrdVersion() {
        return prdVersion;
    }

    public void setPrdVersion(String prdVersion) {
        this.prdVersion = prdVersion;
    }

    public String getIsBasicPrd() {
        return isBasicPrd;
    }

    public void setIsBasicPrd(String isBasicPrd) {
        this.isBasicPrd = isBasicPrd;
    }

    public String getBasicPrdId() {
        return basicPrdId;
    }

    public void setBasicPrdId(String basicPrdId) {
        this.basicPrdId = basicPrdId;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPrdDescribe() {
        return prdDescribe;
    }

    public void setPrdDescribe(String prdDescribe) {
        this.prdDescribe = prdDescribe;
    }

    public String getPrdStatus() {
        return prdStatus;
    }

    public void setPrdStatus(String prdStatus) {
        this.prdStatus = prdStatus;
    }


    public String getWfiSignId() {
        return wfiSignId;
    }

    public void setWfiSignId(String wfiSignId) {
        this.wfiSignId = wfiSignId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getModelGroupNo() {
        return modelGroupNo;
    }

    public void setModelGroupNo(String modelGroupNo) {
        this.modelGroupNo = modelGroupNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(BigDecimal serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getCorePrdId() {
        return corePrdId;
    }

    public void setCorePrdId(String corePrdId) {
        this.corePrdId = corePrdId;
    }

    public String getPrdBelgLine() {
        return prdBelgLine;
    }

    public void setPrdBelgLine(String prdBelgLine) {
        this.prdBelgLine = prdBelgLine;
    }


}
