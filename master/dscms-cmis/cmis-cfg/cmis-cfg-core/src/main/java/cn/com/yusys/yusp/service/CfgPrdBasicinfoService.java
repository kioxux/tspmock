/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgPrdBasicinfo;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.repository.mapper.CfgPrdBasicinfoMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdBasicinfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-07 11:27:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgPrdBasicinfoService {
    private static final Logger log = LoggerFactory.getLogger(CfgPrdBasicinfoService.class);

    @Autowired
    private CfgPrdBasicinfoMapper cfgPrdBasicinfoMapper;
    @Autowired
    private CfgPrdChnlRelService cfgPrdChnlRelService;
    @Autowired
    private CfgPrdDataFlowRelService cfgPrdDataFlowRelService;
    @Autowired
    private CfgPrdFileTplRelService cfgPrdFileTplRelService;
    @Autowired
    private CfgPrdOrgRelService cfgPrdOrgRelService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgPrdBasicinfo selectByPrimaryKey(String prdId) {
        return cfgPrdBasicinfoMapper.selectByPrimaryKey(prdId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgPrdBasicinfoDto> selectAll(QueryModel model) {
        List<CfgPrdBasicinfoDto> records = (List<CfgPrdBasicinfoDto>) cfgPrdBasicinfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgPrdBasicinfoDto> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPrdBasicinfoDto> list = cfgPrdBasicinfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgPrdBasicinfo record) {
        return cfgPrdBasicinfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgPrdBasicinfo record) {
        return cfgPrdBasicinfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgPrdBasicinfo record) {
        return cfgPrdBasicinfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgPrdBasicinfo record) {
        return cfgPrdBasicinfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String prdId) {
        int result = cfgPrdBasicinfoMapper.deleteByPrimaryKey(prdId);
        //联动删除子表数据
        return result;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgPrdBasicinfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: copyPrdBasicAndOtherSave
     * @方法描述: 保存产品主表产品基本信息时，联动保存关联的子表数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int copyPrdBasicAndOtherSave(CfgPrdBasicinfo record, String oldPrdId) {
        int result = cfgPrdBasicinfoMapper.insert(record);
        if (result > 0) {
            String prdId = record.getPrdId();
            int saveNum = 0;
            saveNum = cfgPrdChnlRelService.copyCfgPrdChnlRel(prdId, oldPrdId);
            log.info("产品适用渠道复制条数：{}", saveNum);
            saveNum = cfgPrdDataFlowRelService.copyCfgPrdDataFlowRel(prdId, oldPrdId);
            log.info("产品适用数据流复制条数：{}", saveNum);
            saveNum = cfgPrdFileTplRelService.copyCfgPrdFileTplRel(prdId, oldPrdId);
            log.info("合同模板及格式化报告模板复制条数：{}", saveNum);
            saveNum = cfgPrdOrgRelService.copyCfgPrdOrgRel(prdId, oldPrdId);
            log.info("产品适用机构复制条数：{}", saveNum);
        }
        return result;
    }

    /**
     * @方法名称: deleteCfgPrdBasicinfoAndChild
     * @方法描述: 根据主键（产品编号）进行逻辑删除，及将产品信息、及关联的子表数据中操作类型更新为“删除”
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int deleteCfgPrdBasicinfoAndChild(CfgPrdBasicinfo cfgPrdBasicinfo) {
        String prdId = cfgPrdBasicinfo.getPrdId();
        //将操作标识修改为 删除
        String oprType = CommonConstant.DELETE_OPR;
        cfgPrdBasicinfo.setOprType(oprType);
        int result = cfgPrdBasicinfoMapper.deleteByIds(cfgPrdBasicinfo.getPrdId());
        log.info("*******【产品删除】删除的产品编号：{}", prdId);
        if (result > 0) {
            int saveNum = 0;
            saveNum = cfgPrdChnlRelService.deleteCfgPrdChnlRel(prdId, oprType);
            log.info("产品适用渠道删除条数：{}", saveNum);
            saveNum = cfgPrdDataFlowRelService.deleteCfgPrdDataFlowRel(prdId, oprType);
            log.info("产品适用数据流删除条数：{}", saveNum);
            saveNum = cfgPrdFileTplRelService.deleteCfgPrdFileTplRel(prdId, oprType);
            log.info("合同模板及格式化报告模板删除条数：{}", saveNum);
            saveNum = cfgPrdOrgRelService.deleteCfgPrdOrgRel(prdId, oprType);
            log.info("产品适用机构删除条数：{}", saveNum);
        }
        //联动删除子表数据
        return result;
    }

    /**
     * 通过入参的参数编号查询产品名称
     *
     * @param param
     * @return
     */
    public String getSuitPrdName(Map param) {
        String prdNames = "";
        String suitPrdNo = (String) param.get("suitPrdNo");
        log.info("产品名称查询入参信息为：{}", suitPrdNo);
        Map queryMap = new HashMap();
        queryMap.put("prdNos", suitPrdNo);
        List<CfgPrdBasicinfo> cfgPrdBasicinfoList = cfgPrdBasicinfoMapper.selectByParams(queryMap);
        if (CollectionUtils.nonEmpty(cfgPrdBasicinfoList) && cfgPrdBasicinfoList.size() > 0) {
            for (CfgPrdBasicinfo cfgPrdBasicinfo : cfgPrdBasicinfoList) {
                prdNames += cfgPrdBasicinfo.getPrdName() + ",";
            }
        }

        if (prdNames.length() > 0) {
            prdNames = prdNames.substring(0, prdNames.length() - 1);
        }

        return prdNames;
    }

    /**
     * @param prdId: 字典码值
     * @Description:提供给biz服务接口XDTZ0056使用
     * @Author: YX-WJ
     * @Date: 2021/6/5 15:49
     * @return: java.lang.Integer
     **/
    public Integer queryContInfoByEnName4Biz(String prdId) {
        int count = cfgPrdBasicinfoMapper.queryContInfoByEnName4Biz(prdId);
        return count;
    }

    /**
     * 根据产品编号查询产品信息
     *
     * @param prdId
     * @return
     */
    public CfgPrdBasicinfoDto queryCfgPrdBasicInfo(String prdId) {
        CfgPrdBasicinfoDto cfgPrdBasicinfoDto = new CfgPrdBasicinfoDto();
        CfgPrdBasicinfo cfgPrdBasicinfo = cfgPrdBasicinfoMapper.selectByPrimaryKey(prdId);
        if (Objects.nonNull(cfgPrdBasicinfo)) {
            BeanUtils.copyProperties(cfgPrdBasicinfo, cfgPrdBasicinfoDto);
        } else {
            cfgPrdBasicinfoDto = null;
        }
        return cfgPrdBasicinfoDto;
    }

    /**
     * 根据产品目录层级catalogid查询产品列表
     *
     * @param catalogId
     * @return
     */
    public List<CfgPrdBasicinfoDto> queryBasicInfoByCatalogId(String catalogId) {
        List<CfgPrdBasicinfo> cfgPrdBasicinfos = new ArrayList<>();
        List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtos = new ArrayList<>();
        cfgPrdBasicinfos = cfgPrdBasicinfoMapper.queryBasicInfoByCatalogId(catalogId);
        if (CollectionUtils.nonNull(cfgPrdBasicinfos) && cfgPrdBasicinfos.size() > 0) {
            cfgPrdBasicinfoDtos = cfgPrdBasicinfos.stream().map(cfgPrdBasicinfo -> {
                CfgPrdBasicinfoDto cfgPrdBasicinfoDto = new CfgPrdBasicinfoDto();
                BeanUtils.copyProperties(cfgPrdBasicinfo, cfgPrdBasicinfoDto);
                return cfgPrdBasicinfoDto;
            }).collect(Collectors.toList());
        }
        return cfgPrdBasicinfoDtos;
    }

    /**
     * 根据产品类别查询所有的产品信息
     *
     * @param prdType
     * @return
     */
    public List<CfgPrdBasicinfoDto> queryCfgPrdBasicInfoByPrdType(String prdType) {
        List<CfgPrdBasicinfo> cfgPrdBasicinfos = new ArrayList<>();
        List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtos = new ArrayList<>();
        cfgPrdBasicinfos = cfgPrdBasicinfoMapper.queryCfgPrdBasicInfoByPrdType(prdType);
        if (CollectionUtils.nonNull(cfgPrdBasicinfos) && cfgPrdBasicinfos.size() > 0) {
            cfgPrdBasicinfoDtos = cfgPrdBasicinfos.stream().map(cfgPrdBasicinfo -> {
                CfgPrdBasicinfoDto cfgPrdBasicinfoDto = new CfgPrdBasicinfoDto();
                BeanUtils.copyProperties(cfgPrdBasicinfo, cfgPrdBasicinfoDto);
                return cfgPrdBasicinfoDto;
            }).collect(Collectors.toList());
        }
        return cfgPrdBasicinfoDtos;
    }

    /**
     * 根据目录层级查询产品信息
     *
     * @param cataloglevelName
     * @return
     */
    public List<CfgPrdBasicinfoDto> queryBasicInfoByCatalog(String cataloglevelName) {
        List<CfgPrdBasicinfo> cfgPrdBasicinfos = new ArrayList<>();
        List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtos = new ArrayList<>();
        cfgPrdBasicinfos = cfgPrdBasicinfoMapper.queryBasicInfoByCatalog(cataloglevelName);
        if (CollectionUtils.nonNull(cfgPrdBasicinfos) && cfgPrdBasicinfos.size() > 0) {
            cfgPrdBasicinfoDtos = cfgPrdBasicinfos.stream().map(cfgPrdBasicinfo -> {
                CfgPrdBasicinfoDto cfgPrdBasicinfoDto = new CfgPrdBasicinfoDto();
                BeanUtils.copyProperties(cfgPrdBasicinfo, cfgPrdBasicinfoDto);
                return cfgPrdBasicinfoDto;
            }).collect(Collectors.toList());
        }
        return cfgPrdBasicinfoDtos;
    }

    /**
     * @方法名称: selectCfgPrdBasicinfoData
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgPrdBasicinfoDto> selectCfgPrdBasicinfoData(QueryModel model) {
        List<CfgPrdBasicinfoDto> list = new ArrayList<>();
        PageHelper.startPage(model.getPage(), model.getSize());
        if("2".equals(model.getCondition().get("cusCatalog"))){
            list = cfgPrdBasicinfoMapper.selectCfgPrdBasicinfoDG(model);
        }
        if("1".equals(model.getCondition().get("cusCatalog"))){
            list = cfgPrdBasicinfoMapper.selectCfgPrdBasicinfoGR(model);
        }
        PageHelper.clearPage();
        return list;
    }
}
