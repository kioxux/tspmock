/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgUserWorkCalendar;
import cn.com.yusys.yusp.service.CfgUserWorkCalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgUserWorkCalendarResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Admin
 * @创建时间: 2021-03-31 16:45:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfguserworkcalendar")
public class CfgUserWorkCalendarResource {
    @Autowired
    private CfgUserWorkCalendarService cfgUserWorkCalendarService;

    /**
     * 根据年月 查询当月的日志或备忘录的 记录.
     *
     * @return
     */
    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryByMonth")
    protected ResultDto<List<CfgUserWorkCalendar>> queryByMonth(@RequestBody Map map) {
        String calendarDate = (String) map.get("year") + "-" + (String) map.get("month") + "-%";
        String loginCode = (String) map.get("loginCode");
        List<CfgUserWorkCalendar> result = cfgUserWorkCalendarService.queryByMonth(calendarDate, loginCode);
        return new ResultDto<List<CfgUserWorkCalendar>>(result);
    }
}
