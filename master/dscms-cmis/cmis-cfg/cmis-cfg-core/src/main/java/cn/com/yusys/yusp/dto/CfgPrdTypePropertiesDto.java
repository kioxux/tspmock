package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdTypeProperties
 * @类描述: cfg_prd_type_properties数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-07-02 10:30:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgPrdTypePropertiesDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 产品类型属性配置编号
     **/
    private String prdTypePropNo;

    /**
     * 产品编号
     **/
    private String prdId;

    /**
     * 产品类型属性编号
     **/
    private String typePropNo;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 更新人
     **/
    private String updId;

    /**
     * 更新机构
     **/
    private String updBrId;

    /**
     * 更新日期
     **/
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    private String oprType;

    /**
     * @return PrdTypePropNo
     */
    public String getPrdTypePropNo() {
        return this.prdTypePropNo;
    }

    /**
     * @param prdTypePropNo
     */
    public void setPrdTypePropNo(String prdTypePropNo) {
        this.prdTypePropNo = prdTypePropNo == null ? null : prdTypePropNo.trim();
    }

    /**
     * @return PrdId
     */
    public String getPrdId() {
        return this.prdId;
    }

    /**
     * @param prdId
     */
    public void setPrdId(String prdId) {
        this.prdId = prdId == null ? null : prdId.trim();
    }

    /**
     * @return TypePropNo
     */
    public String getTypePropNo() {
        return this.typePropNo;
    }

    /**
     * @param typePropNo
     */
    public void setTypePropNo(String typePropNo) {
        this.typePropNo = typePropNo == null ? null : typePropNo.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }


}