/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgAgricuArea
 * @类描述: cfg_agricu_area数据实体类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-06-15 22:26:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_agricu_area")
public class CfgAgricuArea extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 区域编码
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "AREA_NO")
    private String areaNo;

    /**
     * 地级市名称
     **/
    @Column(name = "PREFECT_CITY_NAME", unique = false, nullable = true, length = 50)
    private String prefectCityName;

    /**
     * 区县名称
     **/
    @Column(name = "COUNTY_NAME", unique = false, nullable = true, length = 100)
    private String countyName;

    /**
     * 街道名称
     **/
    @Column(name = "STREET_NAME", unique = false, nullable = true, length = 50)
    private String streetName;

    /**
     * 居委会名称
     **/
    @Column(name = "NEIGH_COMMIT_NAME", unique = false, nullable = true, length = 500)
    private String neighCommitName;

    /**
     * 是否农户 STD_ZX_YES_NO
     **/
    @Column(name = "AGRI_FLG", unique = false, nullable = true, length = 5)
    private String agriFlg;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public CfgAgricuArea() {
        // Not compliant
    }

    /**
     * @return areaNo
     */
    public String getAreaNo() {
        return this.areaNo;
    }

    /**
     * @param areaNo
     */
    public void setAreaNo(String areaNo) {
        this.areaNo = areaNo;
    }

    /**
     * @return prefectCityName
     */
    public String getPrefectCityName() {
        return this.prefectCityName;
    }

    /**
     * @param prefectCityName
     */
    public void setPrefectCityName(String prefectCityName) {
        this.prefectCityName = prefectCityName;
    }

    /**
     * @return countyName
     */
    public String getCountyName() {
        return this.countyName;
    }

    /**
     * @param countyName
     */
    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    /**
     * @return streetName
     */
    public String getStreetName() {
        return this.streetName;
    }

    /**
     * @param streetName
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     * @return neighCommitName
     */
    public String getNeighCommitName() {
        return this.neighCommitName;
    }

    /**
     * @param neighCommitName
     */
    public void setNeighCommitName(String neighCommitName) {
        this.neighCommitName = neighCommitName;
    }

    /**
     * @return agriFlg
     */
    public String getAgriFlg() {
        return this.agriFlg;
    }

    /**
     * @param agriFlg
     */
    public void setAgriFlg(String agriFlg) {
        this.agriFlg = agriFlg;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }


}