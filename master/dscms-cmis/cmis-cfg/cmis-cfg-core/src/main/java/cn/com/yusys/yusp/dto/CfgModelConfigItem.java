package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 1. 主模板组: id为模板组id,pid为空
 * 2. 页面: id为页面id,pid为主模板组id
 * 3. 子模板组: id为子模板组id,pid为主模板组id
 */
public class CfgModelConfigItem implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 节点id
     */
    private String id;

    /**
     * 父节点id
     */
    private String pid;

    /**
     * 名称
     **/
    private String name;

    /**
     * 模板显示方式 STD_ZB_SHOW_MODE
     **/
    private String showMode;

    /**
     * 版本号
     **/
    private String ver;

    /**
     * 是否关联作业流 STD_ZB_YES_NO
     **/
    private String isJobFlow;

    /**
     * 作业流编号
     **/
    private String jobFlow;

    /**
     * 业务规则方案编号
     **/
    private String planId;

    /**
     * 备注
     **/
    private String remark;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 关联类型
     */
    private String relType;

    /**
     * 页面URL
     **/
    private String funcUrl;

    /**
     * 是否主页面 STD_ZB_YES_NO
     **/
    private String isMainFunc;

    /**
     * 页面显示顺序
     **/
    private Integer seqNo;

    /**
     * 从页面显示条件
     **/
    private String showCond;

    /**
     * 从页面过滤条件
     **/
    private String filterCond;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShowMode() {
        return showMode;
    }

    public void setShowMode(String showMode) {
        this.showMode = showMode;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getIsJobFlow() {
        return isJobFlow;
    }

    public void setIsJobFlow(String isJobFlow) {
        this.isJobFlow = isJobFlow;
    }

    public String getJobFlow() {
        return jobFlow;
    }

    public void setJobFlow(String jobFlow) {
        this.jobFlow = jobFlow;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getRelType() {
        return relType;
    }

    public void setRelType(String relType) {
        this.relType = relType;
    }

    public String getFuncUrl() {
        return funcUrl;
    }

    public void setFuncUrl(String funcUrl) {
        this.funcUrl = funcUrl;
    }

    public String getIsMainFunc() {
        return isMainFunc;
    }

    public void setIsMainFunc(String isMainFunc) {
        this.isMainFunc = isMainFunc;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public String getShowCond() {
        return showCond;
    }

    public void setShowCond(String showCond) {
        this.showCond = showCond;
    }

    public String getFilterCond() {
        return filterCond;
    }

    public void setFilterCond(String filterCond) {
        this.filterCond = filterCond;
    }
}
