/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgPrdDataFlowRel;
import cn.com.yusys.yusp.repository.mapper.CfgPrdDataFlowRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdDataFlowRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-07 11:27:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgPrdDataFlowRelService {

    @Autowired
    private CfgPrdDataFlowRelMapper cfgPrdDataFlowRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgPrdDataFlowRel selectByPrimaryKey(String pkId) {
        return cfgPrdDataFlowRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgPrdDataFlowRel> selectAll(QueryModel model) {
        List<CfgPrdDataFlowRel> records = (List<CfgPrdDataFlowRel>) cfgPrdDataFlowRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgPrdDataFlowRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPrdDataFlowRel> list = cfgPrdDataFlowRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgPrdDataFlowRel record) {
        return cfgPrdDataFlowRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgPrdDataFlowRel record) {
        return cfgPrdDataFlowRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgPrdDataFlowRel record) {
        return cfgPrdDataFlowRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgPrdDataFlowRel record) {
        return cfgPrdDataFlowRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgPrdDataFlowRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgPrdDataFlowRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: copyCfgPrdDataFlowRel
     * @方法描述: 复制产品时，将老产品配置的产品适用数据流联动复制保存
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int copyCfgPrdDataFlowRel(String prdId, String oldPrdId) {
        return cfgPrdDataFlowRelMapper.copyCfgPrdDataFlowRel(prdId, oldPrdId, CommonConstant.ADD_OPR);
    }

    /**
     * @方法名称: deleteCfgPrdDataFlowRel
     * @方法描述: 删除产品时，将产品配置的产品适用数据流联动删除，此处为逻辑删除更新操作类型为“删除”，不进行物理删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteCfgPrdDataFlowRel(String prdId, String oprType) {
        return cfgPrdDataFlowRelMapper.deleteCfgPrdDataFlowRel(prdId, oprType);
    }
}

