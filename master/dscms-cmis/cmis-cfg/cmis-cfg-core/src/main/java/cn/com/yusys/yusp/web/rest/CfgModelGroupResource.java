/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgModelGroup;
import cn.com.yusys.yusp.dto.CfgModelConfigItem;
import cn.com.yusys.yusp.service.CfgModelGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgModelGroupResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 15430
 * @创建时间: 2020-11-20 16:46:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgmodelgroup")
public class CfgModelGroupResource {
    @Autowired
    private CfgModelGroupService cfgModelGroupService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgModelGroup>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgModelGroup> list = cfgModelGroupService.selectAll(queryModel);
        return new ResultDto<List<CfgModelGroup>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgModelGroup>> index(QueryModel queryModel) {
        List<CfgModelGroup> list = cfgModelGroupService.selectByModel(queryModel);
        return new ResultDto<List<CfgModelGroup>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{modelGroupNo}")
    protected ResultDto<CfgModelGroup> show(@PathVariable("modelGroupNo") String modelGroupNo) {
        CfgModelGroup cfgModelGroup = cfgModelGroupService.selectByPrimaryKey(modelGroupNo);
        return new ResultDto<CfgModelGroup>(cfgModelGroup);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgModelGroup> create(@RequestBody CfgModelGroup cfgModelGroup) throws URISyntaxException {
        cfgModelGroupService.insert(cfgModelGroup);
        return new ResultDto<CfgModelGroup>(cfgModelGroup);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgModelGroup cfgModelGroup) throws URISyntaxException {
        int result = cfgModelGroupService.update(cfgModelGroup);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{modelGroupNo}")
    protected ResultDto<Integer> delete(@PathVariable("modelGroupNo") String modelGroupNo) {
        int result = cfgModelGroupService.deleteByPrimaryKey(modelGroupNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deletecas
     * @函数描述:关联删除模板子表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletecas/{modelGroupNo}")
    protected ResultDto<Integer> deleteCas(@PathVariable("modelGroupNo") String modelGroupNo) {
        int result = cfgModelGroupService.deleteCas(modelGroupNo);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgModelGroupService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getModelGroupConfig
     * @函数描述:获取模板配置
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/config/{modelGroupNo}")
    protected ResultDto<List<CfgModelConfigItem>> getModelGroupConfig(@PathVariable("modelGroupNo") String modelGroupNo) {
        List<CfgModelConfigItem> list = cfgModelGroupService.getModelGroupConfig(modelGroupNo);
        return new ResultDto<>(list);
    }
}
