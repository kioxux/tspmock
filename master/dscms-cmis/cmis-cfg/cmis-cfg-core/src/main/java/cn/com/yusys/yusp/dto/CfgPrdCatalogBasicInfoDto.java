package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class CfgPrdCatalogBasicInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String prdId;

    private String prdName;

    private String catalogId;

    private String hPrdId;

    private String isHeadPrd;

    private String serialNumber;

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String gethPrdId() {
        return hPrdId;
    }

    public void sethPrdId(String hPrdId) {
        this.hPrdId = hPrdId;
    }

    public String getIsHeadPrd() {
        return isHeadPrd;
    }

    public void setIsHeadPrd(String isHeadPrd) {
        this.isHeadPrd = isHeadPrd;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
