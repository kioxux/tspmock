/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryDetail
 * @类描述: cfg_flex_qry_detail数据实体类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-29 21:34:31
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_flex_qry_detail")
public class CfgFlexQryDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 指标配置编码
     **/
    @Column(name = "INDEX_CODE", unique = false, nullable = false, length = 40)
    private String indexCode;

    /**
     * 指标英文
     **/
    @Column(name = "COL_NAME_EN", unique = false, nullable = true, length = 100)
    private String colNameEn;

    /**
     * 指标名称
     **/
    @Column(name = "COL_NAME_CN", unique = false, nullable = true, length = 200)
    private String colNameCn;

    /**
     * 数据源
     **/
    @Column(name = "DATASOURCE", unique = false, nullable = true, length = 20)
    private String datasource;

    /**
     * 来源表
     **/
    @Column(name = "TABLE_SOUR", unique = false, nullable = true, length = 100)
    private String tableSour;

    /**
     * 指标长度
     **/
    @Column(name = "COL_SIZE", unique = false, nullable = true, length = 20)
    private String colSize;

    /**
     * 指标类型
     **/
    @Column(name = "COL_TYPE", unique = false, nullable = true, length = 20)
    private String colType;

    /**
     * 是否为空
     **/
    @Column(name = "IS_NULL", unique = false, nullable = true, length = 5)
    private String isNull;

    /**
     * 是否主键
     **/
    @Column(name = "PRIMARYKEY_FLAG", unique = false, nullable = true, length = 5)
    private String primarykeyFlag;

    /**
     * 数据字典类型
     **/
    @Column(name = "NOTES", unique = false, nullable = true, length = 20)
    private String notes;

    /**
     * 是否可用 STD_ZB_YES_NO
     **/
    @Column(name = "IS_ENABLE", unique = false, nullable = true, length = 5)
    private String isEnable;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 控件类型
     **/
    @Column(name = "ITEMTYPE", unique = false, nullable = true, length = 30)
    private String itemtype;

    /**
     * 控件定义
     **/
    @Column(name = "ITEMDEFINE", unique = false, nullable = true, length = 1000)
    private String itemdefine;

    public CfgFlexQryDetail() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return indexCode
     */
    public String getIndexCode() {
        return this.indexCode;
    }

    /**
     * @param indexCode
     */
    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

    /**
     * @return colNameEn
     */
    public String getColNameEn() {
        return this.colNameEn;
    }

    /**
     * @param colNameEn
     */
    public void setColNameEn(String colNameEn) {
        this.colNameEn = colNameEn;
    }

    /**
     * @return colNameCn
     */
    public String getColNameCn() {
        return this.colNameCn;
    }

    /**
     * @param colNameCn
     */
    public void setColNameCn(String colNameCn) {
        this.colNameCn = colNameCn;
    }

    /**
     * @return datasource
     */
    public String getDatasource() {
        return this.datasource;
    }

    /**
     * @param datasource
     */
    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    /**
     * @return tableSour
     */
    public String getTableSour() {
        return this.tableSour;
    }

    /**
     * @param tableSour
     */
    public void setTableSour(String tableSour) {
        this.tableSour = tableSour;
    }

    /**
     * @return colSize
     */
    public String getColSize() {
        return this.colSize;
    }

    /**
     * @param colSize
     */
    public void setColSize(String colSize) {
        this.colSize = colSize;
    }

    /**
     * @return colType
     */
    public String getColType() {
        return this.colType;
    }

    /**
     * @param colType
     */
    public void setColType(String colType) {
        this.colType = colType;
    }

    /**
     * @return isNull
     */
    public String getIsNull() {
        return this.isNull;
    }

    /**
     * @param isNull
     */
    public void setIsNull(String isNull) {
        this.isNull = isNull;
    }

    /**
     * @return primarykeyFlag
     */
    public String getPrimarykeyFlag() {
        return this.primarykeyFlag;
    }

    /**
     * @param primarykeyFlag
     */
    public void setPrimarykeyFlag(String primarykeyFlag) {
        this.primarykeyFlag = primarykeyFlag;
    }

    /**
     * @return notes
     */
    public String getNotes() {
        return this.notes;
    }

    /**
     * @param notes
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return isEnable
     */
    public String getIsEnable() {
        return this.isEnable;
    }

    /**
     * @param isEnable
     */
    public void setIsEnable(String isEnable) {
        this.isEnable = isEnable;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return itemtype
     */
    public String getItemtype() {
        return this.itemtype;
    }

    /**
     * @param itemtype
     */
    public void setItemtype(String itemtype) {
        this.itemtype = itemtype;
    }

    /**
     * @return itemdefine
     */
    public String getItemdefine() {
        return this.itemdefine;
    }

    /**
     * @param itemdefine
     */
    public void setItemdefine(String itemdefine) {
        this.itemdefine = itemdefine;
    }


}