/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CfgDiscOrgLmt;
import cn.com.yusys.yusp.dto.CfgDiscOrgLmtRespDto;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CfgDiscOrgLmtService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgDiscOrgLmtResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-11 17:38:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgdiscorglmt")
public class CfgDiscOrgLmtResource {

    private static final Logger logger = LoggerFactory.getLogger(CfgDiscOrgLmtResource.class);
    @Autowired
    private CfgDiscOrgLmtService cfgDiscOrgLmtService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgDiscOrgLmt>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgDiscOrgLmt> list = cfgDiscOrgLmtService.selectAll(queryModel);
        return new ResultDto<List<CfgDiscOrgLmt>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgDiscOrgLmt>> index(QueryModel queryModel) {
        List<CfgDiscOrgLmt> list = cfgDiscOrgLmtService.selectByModel(queryModel);
        return new ResultDto<List<CfgDiscOrgLmt>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<CfgDiscOrgLmt>> selectByModel(@RequestBody QueryModel queryModel) {
        List<CfgDiscOrgLmt> list = cfgDiscOrgLmtService.selectByModel(queryModel);
        return new ResultDto<List<CfgDiscOrgLmt>>(list);
    }

    /*

  /**
   * @函数名称:show
   * @函数描述:查询单个对象，公共API接口
   * @参数与返回说明:
   * @算法描述:
   */
    @GetMapping("/{serno}")
    protected ResultDto<CfgDiscOrgLmt> show(@PathVariable("serno") String serno) {
        CfgDiscOrgLmt cfgDiscOrgLmt = cfgDiscOrgLmtService.selectByPrimaryKey(serno);
        return new ResultDto<CfgDiscOrgLmt>(cfgDiscOrgLmt);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgDiscOrgLmt> create(@RequestBody CfgDiscOrgLmt cfgDiscOrgLmt) throws URISyntaxException {
        cfgDiscOrgLmtService.insert(cfgDiscOrgLmt);
        return new ResultDto<CfgDiscOrgLmt>(cfgDiscOrgLmt);
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgDiscOrgLmt cfgDiscOrgLmt) throws URISyntaxException {
        int result = cfgDiscOrgLmtService.update(cfgDiscOrgLmt);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cfgDiscOrgLmtService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgDiscOrgLmtService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 获取配置信息list
     *
     * @param queryMap
     * @return
     */
    @PostMapping("/getListByQueryMap")
    protected @ResponseBody
    ResultDto<List<CfgDiscOrgLmtRespDto>> getListByQueryMap(@Validated @RequestBody Map queryMap) {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.CFG_DISC_ORG_LMT_GETLISTBYQUERYMAP.key, DscmsCfgEnum.CFG_DISC_ORG_LMT_GETLISTBYQUERYMAP.value, JSON.toJSONString(queryMap));
        ResultDto<List<CfgDiscOrgLmtRespDto>> resultDto = new ResultDto<>();
        List<CfgDiscOrgLmtRespDto> list = new ArrayList<>();
        try {
            list = cfgDiscOrgLmtService.getListByQueryMap(queryMap);
            // 封装xdcz0005DataResultDto中正确的返回码和返回信息
            resultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            resultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsCfgEnum.CFG_DISC_ORG_LMT_GETLISTBYQUERYMAP.key, DscmsCfgEnum.CFG_DISC_ORG_LMT_GETLISTBYQUERYMAP.value, e.getMessage());
            // 封装xdcz0005DataResultDto中异常返回码和返回信息
            resultDto.setCode(EpbEnum.EPB099999.key);
            resultDto.setMessage(EpbEnum.EPB099999.value);
        }
        resultDto.setData(list);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsCfgEnum.CFG_DISC_ORG_LMT_GETLISTBYQUERYMAP.key, DscmsCfgEnum.CFG_DISC_ORG_LMT_GETLISTBYQUERYMAP.value, JSON.toJSONString(resultDto));
        return resultDto;
    }
}
