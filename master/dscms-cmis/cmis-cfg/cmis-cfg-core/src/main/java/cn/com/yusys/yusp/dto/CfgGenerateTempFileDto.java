package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgGenerateTempFile
 * @类描述: cfg_generate_temp_file数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-06-10 14:17:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgGenerateTempFileDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 存储文件服务器ip地址
     **/
    private String loginIp;

    /**
     * 登录端口
     **/
    private String loginPort;

    /**
     * 登录用户名
     **/
    private String loginUsername;

    /**
     * 登录密码
     **/
    private String loginPwd;

    /**
     * 文件存储路径
     **/
    private String filePath;

    /**
     * 备注
     **/
    private String memo;

    /**
     * @return PkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    /**
     * @return LoginIp
     */
    public String getLoginIp() {
        return this.loginIp;
    }

    /**
     * @param loginIp
     */
    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp == null ? null : loginIp.trim();
    }

    /**
     * @return LoginPort
     */
    public String getLoginPort() {
        return this.loginPort;
    }

    /**
     * @param loginPort
     */
    public void setLoginPort(String loginPort) {
        this.loginPort = loginPort == null ? null : loginPort.trim();
    }

    /**
     * @return LoginUsername
     */
    public String getLoginUsername() {
        return this.loginUsername;
    }

    /**
     * @param loginUsername
     */
    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername == null ? null : loginUsername.trim();
    }

    /**
     * @return LoginPwd
     */
    public String getLoginPwd() {
        return this.loginPwd;
    }

    /**
     * @param loginPwd
     */
    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd == null ? null : loginPwd.trim();
    }

    /**
     * @return FilePath
     */
    public String getFilePath() {
        return this.filePath;
    }

    /**
     * @param filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath == null ? null : filePath.trim();
    }

    /**
     * @return Memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }


}