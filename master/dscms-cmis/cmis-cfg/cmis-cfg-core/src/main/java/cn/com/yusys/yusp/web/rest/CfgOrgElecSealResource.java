/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgOrgElecSeal;
import cn.com.yusys.yusp.service.CfgOrgElecSealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrgElecSealResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-07-12 08:57:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgorgelecseal")
public class CfgOrgElecSealResource {
    @Autowired
    private CfgOrgElecSealService cfgOrgElecSealService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgOrgElecSeal>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgOrgElecSeal> list = cfgOrgElecSealService.selectAll(queryModel);
        return new ResultDto<List<CfgOrgElecSeal>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgOrgElecSeal>> index(QueryModel queryModel) {
        List<CfgOrgElecSeal> list = cfgOrgElecSealService.selectByModel(queryModel);
        return new ResultDto<List<CfgOrgElecSeal>>(list);
    }


    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<CfgOrgElecSeal>> selectByModel(@RequestBody QueryModel queryModel) {
        List<CfgOrgElecSeal> list = cfgOrgElecSealService.selectByModel(queryModel);
        return new ResultDto<List<CfgOrgElecSeal>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{orgCode}")
    protected ResultDto<CfgOrgElecSeal> show(@PathVariable("orgCode") String orgCode) {
        CfgOrgElecSeal cfgOrgElecSeal = cfgOrgElecSealService.selectByPrimaryKey(orgCode);
        return new ResultDto<CfgOrgElecSeal>(cfgOrgElecSeal);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgOrgElecSeal> create(@RequestBody CfgOrgElecSeal cfgOrgElecSeal) throws URISyntaxException {
        cfgOrgElecSealService.insert(cfgOrgElecSeal);
        return new ResultDto<CfgOrgElecSeal>(cfgOrgElecSeal);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgOrgElecSeal cfgOrgElecSeal) throws URISyntaxException {
        int result = cfgOrgElecSealService.update(cfgOrgElecSeal);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{orgCode}")
    protected ResultDto<Integer> delete(@PathVariable("orgCode") String orgCode) {
        int result = cfgOrgElecSealService.deleteByPrimaryKey(orgCode);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgOrgElecSealService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectByOrgNo
     * @函数描述:根据机构号查询获取对应的章编码
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectFromCfgByOrgNo")
    protected ResultDto<CfgOrgElecSeal> selectFromCfgByOrgNo(@RequestBody String orgNo) {
        CfgOrgElecSeal cfgOrgElecSeal = cfgOrgElecSealService.selectFromCfgByOrgNo(orgNo);
        return new ResultDto<CfgOrgElecSeal>(cfgOrgElecSeal);
    }

    /**
     * @函数名称:selectByCondition
     * @函数描述:查询机构电子章配置表
     * @参数与返回说明:
     * @算法描述:
     * @anthor: cqs
     */
    @PostMapping("/selectbycondition")
    protected ResultDto<List<CfgOrgElecSeal>> selectByCondition(@RequestBody Map<String, String> map) {
        QueryModel queryModel = new QueryModel();
        for (String key: map.keySet()) {
            queryModel.addCondition(key, map.get(key));
        }
        List<CfgOrgElecSeal> list = cfgOrgElecSealService.selectByModel(queryModel);
        return new ResultDto<List<CfgOrgElecSeal>>(list);
    }
}
