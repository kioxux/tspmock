/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.risk.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.risk.domain.PrdPvRiskScene;
import cn.com.yusys.yusp.risk.service.PrdPvRiskSceneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: yusp-npam-biz-core模块
 * @类名称: PrdPvRiskSceneResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2020-07-27 17:26:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(description = "风险拦截场景")
@RestController
@RequestMapping("/api/prdpvriskscene")
public class PrdPvRiskSceneResource {
    @Autowired
    private PrdPvRiskSceneService prdPvRiskSceneService;


    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<Map<String, String>>> index(QueryModel queryModel) {
        List<Map<String, String>> list;
        String sceneIdKey = "sceneId";
        queryModel.setSize(1000);
        if (queryModel.getCondition().get(sceneIdKey) == null) {
            String riskLevel = "riskLevel";
            queryModel.addCondition(riskLevel, "1");
            list = prdPvRiskSceneService.selectInitScence(queryModel);
        } else {
            list = prdPvRiskSceneService.selectByModel(queryModel);
            // 风险等级默认值
            for (int i = 0; i < list.size(); i++) {
                Map<String, String> item = list.get(i);
                if (!item.containsKey("riskLevel")) {
                    item.put("riskLevel", "1");
                }
            }
        }
        return new ResultDto<>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PrdPvRiskScene>> create(@RequestBody List<PrdPvRiskScene> prdPvRiskScene, String preventId, String sceneId, String bizType) throws URISyntaxException {
        prdPvRiskSceneService.deleteByPrimaryKey(sceneId, preventId, bizType);
        prdPvRiskSceneService.insert(prdPvRiskScene);
        return new ResultDto<>(prdPvRiskScene);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PrdPvRiskScene prdPvRiskScene) throws URISyntaxException {
        int result = prdPvRiskSceneService.update(prdPvRiskScene);
        return new ResultDto<>(result);
    }

    /**
     * 列表查询
     *
     * @param queryModel
     * @return
     */
    @GetMapping("/distinctScene")
    protected ResultDto<List<Map<String, String>>> selectDistinctScence(QueryModel queryModel) {
        List<Map<String, String>> list = prdPvRiskSceneService.selectDistinctScence(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * 流程中使用，查询需要执行的风险拦截信息
     *
     * @param queryModel
     * @return
     */
    @ApiOperation(value = "getRiskItem", notes = "查询需要拦截的风险拦截")
    @PostMapping("/getRiskItem")
    protected ResultDto<List<Map<String, String>>> getRiskItem(@RequestBody QueryModel queryModel) {
        List<Map<String, String>> list = prdPvRiskSceneService.getRiskItem(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * 无流程风险拦截中使用，查询需要执行的风险拦截信息
     *
     * @param queryModel
     * @return
     */
    @ApiOperation(value = "getRiskNoFlowItem", notes = "查询需要拦截的风险拦截")
    @PostMapping("/getRiskNoFlowItem")
    protected ResultDto<List<Map<String, String>>> getRiskNoFlowItem(@RequestBody QueryModel queryModel) {
        List<Map<String, String>> list = prdPvRiskSceneService.getRiskNoFlowItem(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param preventId
     * @param bizType
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String preventId, String bizType) throws URISyntaxException {
        int result = prdPvRiskSceneService.deleteByPrimaryKey("", preventId, bizType);
        return new ResultDto<>(result);
    }


}
