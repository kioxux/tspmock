package cn.com.yusys.yusp.workbench.web.rest;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.server.cmiscfg0001.req.CmisCfg0001ReqDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/7/113:14
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@RestController
@ServerEndpoint("/message/{loginCode}")
public class WebSocketController {
    private static final Logger log = LoggerFactory.getLogger(WebSocketController.class);
    private static final ConcurrentHashMap<String, Set<Session>> map = new ConcurrentHashMap<>();

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @OnOpen
    public void open(@PathParam("loginCode") String loginCode, Session wsSession) {
        if (map.containsKey(loginCode)) {
            map.get(loginCode).add(wsSession);
        } else {
            CopyOnWriteArraySet<Session> list = new CopyOnWriteArraySet<>();
            list.add(wsSession);
            map.put(loginCode, list);
        }
    }

    @OnClose
    public void onclose(@PathParam("loginCode") String loginCode, Session wsSession) {
        if (map.get(loginCode).size() == 1) {
            map.remove(loginCode);
        } else {
            map.get(loginCode).remove(wsSession);
        }
    }


    /***
     * @param loginCode, message
     * @return java.lang.String
     * @author tangxun
     * @date 2021/7/1 15:50
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @GetMapping("/sendToUser")
    public String sendToUser(String loginCode, String message) {
        CmisCfg0001ReqDto wbMessageDto = new CmisCfg0001ReqDto();
        wbMessageDto.setInputId(loginCode);
        wbMessageDto.setContent(message);
        log.info("发送消息:{}", wbMessageDto);
        rabbitTemplate.convertAndSend(CmisBizConstants.MESSAGE_EXCHANGE, CmisBizConstants.MESSAGE_KEY, wbMessageDto);
        return "success";
    }

    /**
     * 随机的queueID
     *
     * @param dto
     * @throws IOException
     */
    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(),
                    exchange = @Exchange(type = "topic", name = "CMIS_CFG_TOPIC"),
                    key = {"MESSAGE"}
            ),
    })
    public void sendToUser2(CmisCfg0001ReqDto dto) {
        if (Objects.nonNull(map.get(dto.getInputId()))) {
            log.info("发送消息到页面:{}", dto.getContent());
            map.get(dto.getInputId()).forEach(item ->
                    {
                        try {
                            item.getBasicRemote().sendText(decode(Objects.toString(dto.getMessageType())) + "," + dto.getContent());
                        } catch (IOException e) {
                            log.error("推送消息失败", e);
                        }
                    }
            );
        }
    }

    /**
     * @return
     */
    public String decode(String type) {
        String res = "通知";
        switch (type) {
            case "1":
                res = "业务审批退回提醒";
                break;
            case "2":
                res = "业务终审通过提醒";
                break;
            case "3":
                res = "业务终审否决提醒";
                break;
            case "4":
                res = "档案延期归还提醒";
                break;
            case "5":
                res = "工作日历待办提醒";
                break;
            default:
        }
        return res;
    }

}
