/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgRuleItem;
import cn.com.yusys.yusp.service.CfgRuleItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgRuleItemResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: liuch
 * @创建时间: 2020-12-23 11:51:42
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgruleitem")
public class CfgRuleItemResource {
    @Autowired
    private CfgRuleItemService cfgRuleItemService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgRuleItem>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgRuleItem> list = cfgRuleItemService.selectAll(queryModel);
        return new ResultDto<List<CfgRuleItem>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgRuleItem>> index(QueryModel queryModel) {
        List<CfgRuleItem> list = cfgRuleItemService.selectByModel(queryModel);
        return new ResultDto<List<CfgRuleItem>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{ruleItemId}")
    protected ResultDto<CfgRuleItem> show(@PathVariable("ruleItemId") String ruleItemId) {
        CfgRuleItem cfgRuleItem = cfgRuleItemService.selectByPrimaryKey(ruleItemId);
        return new ResultDto<CfgRuleItem>(cfgRuleItem);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgRuleItem> create(@RequestBody CfgRuleItem cfgRuleItem) throws URISyntaxException {
        cfgRuleItemService.insert(cfgRuleItem);
        return new ResultDto<CfgRuleItem>(cfgRuleItem);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgRuleItem cfgRuleItem) throws URISyntaxException {
        int result = cfgRuleItemService.update(cfgRuleItem);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{ruleItemId}")
    protected ResultDto<Integer> delete(@PathVariable("ruleItemId") String ruleItemId) {
        int result = cfgRuleItemService.deleteByPrimaryKey(ruleItemId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 逻辑删除
     *
     * @param cfgRuleItem
     * @return
     */
    @PostMapping("/delete/")
    protected ResultDto<Integer> logicDelete(@RequestBody CfgRuleItem cfgRuleItem) {
        int result = cfgRuleItemService.updateSelective(cfgRuleItem);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgRuleItemService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
