/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgAccpOrgSign
 * @类描述: cfg_accp_org_sign数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-18 20:55:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_accp_org_sign")
public class CfgAccpOrgSign extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 管理机构 **/
	@Id
	@Column(name = "MANAGER_BR_NO")
	private String managerBrNo;
	
	/** 签发机构 **/
	@Column(name = "SIGN_BR_NO", unique = false, nullable = true, length = 10)
	private String signBrNo;
	
	/** 签发机构名称 **/
	@Column(name = "SIGN_BR_NAME", unique = false, nullable = true, length = 200)
	private String signBrName;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param managerBrNo
	 */
	public void setManagerBrNo(String managerBrNo) {
		this.managerBrNo = managerBrNo;
	}
	
    /**
     * @return managerBrNo
     */
	public String getManagerBrNo() {
		return this.managerBrNo;
	}
	
	/**
	 * @param signBrNo
	 */
	public void setSignBrNo(String signBrNo) {
		this.signBrNo = signBrNo;
	}
	
    /**
     * @return signBrNo
     */
	public String getSignBrNo() {
		return this.signBrNo;
	}
	
	/**
	 * @param signBrName
	 */
	public void setSignBrName(String signBrName) {
		this.signBrName = signBrName;
	}
	
    /**
     * @return signBrName
     */
	public String getSignBrName() {
		return this.signBrName;
	}


}