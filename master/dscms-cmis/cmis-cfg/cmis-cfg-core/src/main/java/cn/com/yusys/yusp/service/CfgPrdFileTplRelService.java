/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgPrdFileTplRel;
import cn.com.yusys.yusp.dto.CfgPrdFileTplRelDto;
import cn.com.yusys.yusp.repository.mapper.CfgPrdFileTplRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdFileTplRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-07 11:27:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgPrdFileTplRelService {

    @Autowired
    private CfgPrdFileTplRelMapper cfgPrdFileTplRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgPrdFileTplRel selectByPrimaryKey(String pkId) {
        return cfgPrdFileTplRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgPrdFileTplRelDto> selectAll(QueryModel model) {
        List<CfgPrdFileTplRelDto> records = (List<CfgPrdFileTplRelDto>) cfgPrdFileTplRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgPrdFileTplRelDto> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPrdFileTplRelDto> list = cfgPrdFileTplRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgPrdFileTplRel record) {
        return cfgPrdFileTplRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgPrdFileTplRel record) {
        return cfgPrdFileTplRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgPrdFileTplRel record) {
        return cfgPrdFileTplRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgPrdFileTplRel record) {
        return cfgPrdFileTplRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgPrdFileTplRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgPrdFileTplRelMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: copyCfgPrdFileTplRel
     * @方法描述: 复制产品时，将老产品配置的产品适用合同模板或格式化报告模板联动复制保存
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int copyCfgPrdFileTplRel(String prdId, String oldPrdId) {
        return cfgPrdFileTplRelMapper.copyCfgPrdFileTplRel(prdId, oldPrdId, CommonConstant.ADD_OPR);
    }

    /**
     * @方法名称: deleteCfgPrdFileTplRel
     * @方法描述: 删除产品时，将产品配置的产品适用合同模板或格式化报告模板联动删除，此处为逻辑删除更新操作类型为“删除”，不进行物理删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteCfgPrdFileTplRel(String prdId, String oprType) {
        return cfgPrdFileTplRelMapper.deleteCfgPrdFileTplRel(prdId, oprType);
    }
}
