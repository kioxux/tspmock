/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CfgServiceExceptionDefEnums;
import cn.com.yusys.yusp.domain.CfgDataFlow;
import cn.com.yusys.yusp.dto.CfgToBizFlowDataDto;
import cn.com.yusys.yusp.repository.mapper.CfgDataFlowMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgDataFlowService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-28 17:02:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgDataFlowService {

    private static final Logger log = LoggerFactory.getLogger(CfgDataFlowService.class);

    @Autowired
    private CfgDataFlowMapper cfgDataFlowMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgDataFlow selectByPrimaryKey(String dataFlowId) {
        return cfgDataFlowMapper.selectByPrimaryKey(dataFlowId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgDataFlow> selectAll(QueryModel model) {
        List<CfgDataFlow> records = (List<CfgDataFlow>) cfgDataFlowMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgDataFlow> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgDataFlow> list = cfgDataFlowMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgDataFlow record) {
        return cfgDataFlowMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgDataFlow record) {
        return cfgDataFlowMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgDataFlow record) {
        return cfgDataFlowMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgDataFlow record) {
        return cfgDataFlowMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dataFlowId) {
        return cfgDataFlowMapper.deleteByPrimaryKey(dataFlowId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgDataFlowMapper.deleteByIds(ids);
    }

    /**
     * 对外提供数据流映射查询的方法-单个对象
     *
     * @param cfgToBizFlowDataDto
     * @return
     */
    public CfgToBizFlowDataDto queryCfgDataFlowInfo4Out(CfgToBizFlowDataDto cfgToBizFlowDataDto) {
        String rtnCode = CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_SUCCESS.getExceptionCode();
        String rtnMsg = CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_SUCCESS.getExceptionDesc();
        List<String> mapList = new ArrayList<String>();
        try {
            //定义处理标志位
            String sourceTableName = cfgToBizFlowDataDto.getSourceTableName();
            String distTableName = cfgToBizFlowDataDto.getDistTableName();
            Map sourceMap = cfgToBizFlowDataDto.getSourceMap();
            Map distMap = cfgToBizFlowDataDto.getDistMap();

            if (StringUtils.isBlank(sourceTableName) || StringUtils.isBlank(distTableName)) {
                throw new YuspException(CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_PARAM_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_PARAM_EXCEPTION.getExceptionDesc() + "，未获取到映射源表名以及目标表名");
            }

            log.info("获取数据流映射关系-源表：" + sourceTableName + "，目标表：" + distTableName);
            //处理标志位设置为true
            CfgDataFlow cfgDataFlow = new CfgDataFlow();
            cfgDataFlow.setSourTable(sourceTableName);
            cfgDataFlow.setConvTable(distTableName);
            cfgDataFlow = cfgDataFlowMapper.queryCfgDataFlowByParam(cfgDataFlow);
            if (cfgDataFlow == null) {
                throw new YuspException(CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DATANOTEXISTS_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DATANOTEXISTS_EXCEPTION.getExceptionDesc());
            }
            String mapContent = cfgDataFlow.getMappContent();
            if (StringUtils.isBlank(mapContent)) {
                throw new YuspException(CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_MAPNOTEXISTS_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_MAPNOTEXISTS_EXCEPTION.getExceptionDesc());
            }
            //将字段映射关系处理为list集合
            //1、数据源映射为多个字段以【;】分割
            //2、映射的格式为【源表字段=目标表字段】，因此通过【=】分割出源表键值以及目标表键值
            //3、数据源映射的字段为下划线的格式，而入参的map数据集合为驼峰，因此由map集合中获取数据需要将下划线格式转化为驼峰格式
            //4、将源表集合中的键值对应的字段值放入目标表中对应键值的value中并进行返回
            String[] mapContentAry = mapContent.split(CfgServiceExceptionDefEnums.DEF_SPLIT_SEMICOLON.getValue());

            //针对入参的源表集合不为空，则进行单表的数据转化的映射处理
            if (CollectionUtils.nonEmpty(sourceMap)) {
                if (distMap == null) {
                    distMap = new HashMap();
                }

                for (String value : mapContentAry) {
                    String[] values = value.split(CfgServiceExceptionDefEnums.DEF_SPLIT_EQUALS.getValue());
                    String sourceKey = underlineToHump(values[0]);
                    String distKey = underlineToHump(values[1]);
                    distMap.put(distKey, sourceMap.get(sourceKey));
                }
                log.info("映射处理-转化后的数据集合作为接口结果返回");
                cfgToBizFlowDataDto.setReturnMap(distMap);
            } else {
                throw new YuspException(CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_EXCEPTION.getExceptionDesc() + "映射源数据为空！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取数据流信息异常！", e);
            rtnCode = CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_EXCEPTION.getExceptionCode();
            rtnMsg = CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_EXCEPTION.getExceptionDesc() + e.getMessage();
        } finally {
            cfgToBizFlowDataDto.setRtnCode(rtnCode);
            cfgToBizFlowDataDto.setRtnMsg(rtnMsg);
            cfgToBizFlowDataDto.setMappingList(mapList);
        }
        return cfgToBizFlowDataDto;
    }

    /**
     * 对外提供数据流映射查询的方法-集合对象
     *
     * @param cfgToBizFlowDataDto
     * @return
     */
    public CfgToBizFlowDataDto queryCfgDataFlowInfo4Out4List(CfgToBizFlowDataDto cfgToBizFlowDataDto) {
        String rtnCode = CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_SUCCESS.getExceptionCode();
        String rtnMsg = CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_SUCCESS.getExceptionDesc();
        List<String> mapList = new ArrayList<String>();
        try {
            //定义处理标志位
            String sourceTableName = cfgToBizFlowDataDto.getSourceTableName();
            String distTableName = cfgToBizFlowDataDto.getDistTableName();

            if (StringUtils.isBlank(sourceTableName) || StringUtils.isBlank(distTableName)) {
                throw new YuspException(CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_PARAM_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_PARAM_EXCEPTION.getExceptionDesc() + "，未获取到映射源表名以及目标表名");
            }

            log.info("获取数据流映射关系-源表：" + sourceTableName + "，目标表：" + distTableName);
            //处理标志位设置为true
            CfgDataFlow cfgDataFlow = new CfgDataFlow();
            cfgDataFlow.setSourTable(sourceTableName);
            cfgDataFlow.setConvTable(distTableName);
            cfgDataFlow = cfgDataFlowMapper.queryCfgDataFlowByParam(cfgDataFlow);
            if (cfgDataFlow == null) {
                throw new YuspException(CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DATANOTEXISTS_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DATANOTEXISTS_EXCEPTION.getExceptionDesc());
            }
            String mapContent = cfgDataFlow.getMappContent();
            if (StringUtils.isBlank(mapContent)) {
                throw new YuspException(CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_MAPNOTEXISTS_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_MAPNOTEXISTS_EXCEPTION.getExceptionDesc());
            }
            //将字段映射关系处理为list集合
            //1、数据源映射为多个字段以【;】分割
            //2、映射的格式为【源表字段=目标表字段】，因此通过【=】分割出源表键值以及目标表键值
            //3、数据源映射的字段为下划线的格式，而入参的map数据集合为驼峰，因此由map集合中获取数据需要将下划线格式转化为驼峰格式
            //4、将源表集合中的键值对应的字段值放入目标表中对应键值的value中并进行返回
            String[] mapContentAry = mapContent.split(CfgServiceExceptionDefEnums.DEF_SPLIT_SEMICOLON.getValue());

            //针对list集合
            List<Map> sourceObjList = cfgToBizFlowDataDto.getSourObjList();
            if (CollectionUtils.nonEmpty(sourceObjList)) {
                List<Map> rtnObjList = new ArrayList<Map>();
                for (Map sourMap : sourceObjList) {
                    Map rtnMap = new HashMap();
                    if (CollectionUtils.isEmpty(sourMap)) {
                        rtnCode = CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_EXCEPTION.getExceptionCode();
                        rtnMsg = CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_EXCEPTION.getExceptionDesc() + "入参的参数list中存在空对象！";
                        return cfgToBizFlowDataDto;
                    }
                    //每次处理完成都将上次处理的集合清空
                    rtnMap.clear();
                    for (String value : mapContentAry) {
                        String[] values = value.split(CfgServiceExceptionDefEnums.DEF_SPLIT_EQUALS.getValue());
                        String sourceKey = underlineToHump(values[0]);
                        String distKey = underlineToHump(values[1]);
                        rtnMap.put(distKey, sourMap.get(sourceKey));
                    }
                    rtnObjList.add(rtnMap);
                }
                cfgToBizFlowDataDto.setRtnObjList(rtnObjList);
            }
            log.info("获取数据流映射关系-源表：" + sourceTableName + "，目标表：" + distTableName + ".转化完成");
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取数据流信息异常！", e);
            rtnCode = CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_EXCEPTION.getExceptionCode();
            rtnMsg = CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_DEF_EXCEPTION.getExceptionDesc() + e.getMessage();
        } finally {
            cfgToBizFlowDataDto.setRtnCode(rtnCode);
            cfgToBizFlowDataDto.setRtnMsg(rtnMsg);
            cfgToBizFlowDataDto.setMappingList(mapList);
        }
        return cfgToBizFlowDataDto;
    }

    /**
     * 驼峰命名转为下划线命名
     *
     * @param para 驼峰命名的字符串
     */
    private String humpToUnderline(String para) {
        String firstLetter = para.substring(0, 1);
        para = firstLetter.toLowerCase() + para.substring(1, para.length());
        StringBuilder sb = new StringBuilder(para);
        int temp = 0;//定位
        if (!para.contains(CfgServiceExceptionDefEnums.DEF_SPLIT_UNDERLINE.getValue())) {
            for (int i = 0; i < para.length(); i++) {
                if (Character.isUpperCase(para.charAt(i))) {
                    sb.insert(i + temp, CfgServiceExceptionDefEnums.DEF_SPLIT_UNDERLINE.getValue());
                    temp += 1;
                }
            }
        }
        return sb.toString().toLowerCase();
    }

    /***
     * 下划线命名转为驼峰命名
     * @param para 下划线命名的字符串
     */
    private String underlineToHump(String para) {
        StringBuilder result = new StringBuilder();
        String a[] = para.split(CfgServiceExceptionDefEnums.DEF_SPLIT_UNDERLINE.getValue());
        for (String s : a) {
            if (!para.contains(CfgServiceExceptionDefEnums.DEF_SPLIT_UNDERLINE.getValue())) {
                result.append(s);
                continue;
            }
            if (result.length() == 0) {
                result.append(s.toLowerCase());
            } else {
                result.append(s.substring(0, 1).toUpperCase());
                result.append(s.substring(1).toLowerCase());
            }
        }
        return result.toString();
    }
}
