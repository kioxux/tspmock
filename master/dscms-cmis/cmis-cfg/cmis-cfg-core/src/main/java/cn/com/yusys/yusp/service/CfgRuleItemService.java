/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgNodeRuleRel;
import cn.com.yusys.yusp.domain.CfgPlanRuleRel;
import cn.com.yusys.yusp.domain.CfgRuleItem;
import cn.com.yusys.yusp.repository.mapper.CfgNodeRuleRelMapper;
import cn.com.yusys.yusp.repository.mapper.CfgPlanRuleRelMapper;
import cn.com.yusys.yusp.repository.mapper.CfgRuleItemMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgRuleItemService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: liuch
 * @创建时间: 2020-12-23 11:51:42
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgRuleItemService {

    @Autowired
    private CfgRuleItemMapper cfgRuleItemMapper;
    @Autowired
    private CfgPlanRuleRelMapper cfgPlanRuleRelMapper;
    @Autowired
    private CfgNodeRuleRelMapper cfgNodeRuleRelMapper;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgRuleItem selectByPrimaryKey(String ruleItemId) {
        return cfgRuleItemMapper.selectByPrimaryKey(ruleItemId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgRuleItem> selectAll(QueryModel model) {
        List<CfgRuleItem> records = (List<CfgRuleItem>) cfgRuleItemMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgRuleItem> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgRuleItem> list = cfgRuleItemMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgRuleItem record) {
        return cfgRuleItemMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgRuleItem record) {
        return cfgRuleItemMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新, 若“是否启用”修改为否，则联动将业务规则方案中引用该规则的“是否启用”同步修改为“否”
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int update(CfgRuleItem record) {
        int num = cfgRuleItemMapper.updateByPrimaryKey(record);
        //若“是否启用”修改为否
        if (num > 0 && "N".equals(record.getUsedInd())) {
            //修改业务规则方案关联规则项
            CfgPlanRuleRel cfgPlanRuleRel = new CfgPlanRuleRel();
            cfgPlanRuleRel.setRuleItemId(record.getRuleItemId());
            cfgPlanRuleRel.setUsedInd(record.getUsedInd());
            cfgPlanRuleRel.setUpdId(record.getUpdId());
            cfgPlanRuleRel.setUpdBrId(record.getUpdBrId());
            cfgPlanRuleRel.setUpdDate(record.getUpdDate());
            cfgPlanRuleRel.setOprType(record.getOprType());
            cfgPlanRuleRelMapper.updateByRuleItemId(cfgPlanRuleRel);
            //修改业务规则方案关联流程节点
            CfgNodeRuleRel cfgNodeRuleRel = new CfgNodeRuleRel();
            cfgNodeRuleRel.setRuleItemId(record.getRuleItemId());
            cfgNodeRuleRel.setUsedInd(record.getUsedInd());
            cfgNodeRuleRel.setUpdId(record.getUpdId());
            cfgNodeRuleRel.setUpdBrId(record.getUpdBrId());
            cfgNodeRuleRel.setUpdDate(record.getUpdDate());
            cfgNodeRuleRel.setOprType(record.getOprType());
            cfgNodeRuleRelMapper.updateByRuleItemId(cfgNodeRuleRel);

        }
        return num;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgRuleItem record) {
        return cfgRuleItemMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String ruleItemId) {
        return cfgRuleItemMapper.deleteByPrimaryKey(ruleItemId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgRuleItemMapper.deleteByIds(ids);
    }
}
