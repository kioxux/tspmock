/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.CfgSOrgElecSeal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgSxkdGuarDiscount;
import cn.com.yusys.yusp.repository.mapper.CfgSxkdGuarDiscountMapper;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSxkdGuarDiscountService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-12 15:32:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgSxkdGuarDiscountService {

    @Autowired
    private CfgSxkdGuarDiscountMapper cfgSxkdGuarDiscountMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CfgSxkdGuarDiscount selectByPrimaryKey(String guarTypeCd) {
        return cfgSxkdGuarDiscountMapper.selectByPrimaryKey(guarTypeCd);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgSxkdGuarDiscount> selectAll(QueryModel model) {
        List<CfgSxkdGuarDiscount> records = (List<CfgSxkdGuarDiscount>) cfgSxkdGuarDiscountMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CfgSxkdGuarDiscount> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgSxkdGuarDiscount> list = cfgSxkdGuarDiscountMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CfgSxkdGuarDiscount record) {
        return cfgSxkdGuarDiscountMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CfgSxkdGuarDiscount record) {
        return cfgSxkdGuarDiscountMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CfgSxkdGuarDiscount record) {
        return cfgSxkdGuarDiscountMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CfgSxkdGuarDiscount record) {
        return cfgSxkdGuarDiscountMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String guarTypeCd) {
        return cfgSxkdGuarDiscountMapper.deleteByPrimaryKey(guarTypeCd);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgSxkdGuarDiscountMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectCfgSxkdGuarDiscountByGuarTypeCd
     * @方法描述: 查询授信抵押物折率配置表信息
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgSxkdGuarDiscount selectCfgSxkdGuarDiscountByGuarTypeCd(String guarTypeCd) {
        return cfgSxkdGuarDiscountMapper.selectByPrimaryKey(guarTypeCd);
    }
}
