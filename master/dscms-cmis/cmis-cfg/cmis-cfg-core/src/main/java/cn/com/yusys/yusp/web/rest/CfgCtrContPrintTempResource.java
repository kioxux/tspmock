/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CfgCtrContPrintTemp;
import cn.com.yusys.yusp.dto.CfgContPrintDto;
import cn.com.yusys.yusp.service.CfgCtrContPrintTempService;
import cn.com.yusys.yusp.dto.CfgCtrContPrintReqDto;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgCtrContPrintTempResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-16 21:05:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "主合同打印模板配置表")
@RequestMapping("/api/cfgctrcontprinttemp")
public class CfgCtrContPrintTempResource {
    @Autowired
    private CfgCtrContPrintTempService cfgCtrContPrintTempService;

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgCtrContPrintTemp> create(@RequestBody CfgCtrContPrintTemp cfgCtrContPrintTemp) throws URISyntaxException {
        cfgCtrContPrintTempService.insert(cfgCtrContPrintTemp);
        return new ResultDto<CfgCtrContPrintTemp>(cfgCtrContPrintTemp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgCtrContPrintTemp cfgCtrContPrintTemp) throws URISyntaxException {
        int result = cfgCtrContPrintTempService.update(cfgCtrContPrintTemp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgCtrContPrintTempService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgCtrContPrintTempService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:主合同打印模板配置列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("主合同打印模板配置列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<CfgCtrContPrintTemp>> toSignList(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CfgCtrContPrintTemp> list = cfgCtrContPrintTempService.toSignList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CfgCtrContPrintTemp>>(list);
    }

    /**
     * 主合同打印模板配置逻辑删除
     *
     * @param cfgCtrContPrintTemp
     * @return
     */
    @ApiOperation("主合同打印模板配置逻辑删除")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody CfgCtrContPrintTemp cfgCtrContPrintTemp) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        Integer result = cfgCtrContPrintTempService.logicDelete(cfgCtrContPrintTemp);
        if (result > 0) {
            resultDto.setCode(0);
            resultDto.setData(1);
            resultDto.setMessage("删除成功！");
        } else {
            resultDto.setCode(000);
            resultDto.setData(0);
            resultDto.setMessage("删除失败！");
        }
        return resultDto;
    }

    /**
     * @函数名称:queryCfgCtrContPrintTempDataByParams
     * @函数描述:通过入参查询，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过入参查询数据")
    @PostMapping("/querycfgctrcontprinttempdatabyparams")
    protected ResultDto<CfgCtrContPrintTemp> queryCfgCtrContPrintTempDataByParams(@RequestBody Map map) {
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        CfgCtrContPrintTemp cfgCtrContPrintTemp = cfgCtrContPrintTempService.queryCfgCtrContPrintTempDataByParams(map);
        return new ResultDto<CfgCtrContPrintTemp>(cfgCtrContPrintTemp);
    }

    /**
     * 主合同打印模板配置操作
     *
     * @param cfgCtrCocntPrintTemp
     * @return
     */
    @ApiOperation("主合同打印模板配置新增保存操作")
    @PostMapping("/savecfgctrcontprinttempinfo")
    public ResultDto<Map> saveCfgCtrContPrintTempInfo(@RequestBody CfgCtrContPrintTemp cfgCtrCocntPrintTemp) {
        Map result = cfgCtrContPrintTempService.saveCfgCtrContPrintTempInfo(cfgCtrCocntPrintTemp);
        return new ResultDto<>(result);
    }

    /**
     * 主合同打印模板配置通用的保存方法
     *
     * @param params
     * @return
     */
    @ApiOperation("主合同打印模板配置通用的保存方法")
    @PostMapping("/commonsavecfgctrcontprinttempinfo")
    public ResultDto<Map> commonSaveCfgCtrContPrintTempInfo(@RequestBody Map params) {
        Map rtnData = cfgCtrContPrintTempService.commonSaveCfgCtrContPrintTempInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * 通过参数查询主合同打印配置与担保合同打印配置
     *
     * @param queryModel
     * @return
     */
    @ApiOperation("通过参数查询主合同打印配置与担保合同打印配置")
    @PostMapping("/queryContPrint")
    protected ResultDto<List<CfgContPrintDto>> queryContPrint(@RequestBody QueryModel queryModel) {
        List<CfgContPrintDto> list = cfgCtrContPrintTempService.queryPrintContAndGuar(queryModel);
        return new ResultDto<List<CfgContPrintDto>>(list);
    }

    /**
     * @函数名称:queryCfgCtrContPrintTempDataByParams
     * @函数描述:通过入参查询，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过入参查询数据")
    @PostMapping("/querysuitreportname")
    protected String querySuitReportName(@RequestBody CfgCtrContPrintReqDto cfgCtrContPrintReqDto) {
        String suitReportName = cfgCtrContPrintTempService.querySuitReportName(cfgCtrContPrintReqDto);
        return suitReportName;
    }
}
