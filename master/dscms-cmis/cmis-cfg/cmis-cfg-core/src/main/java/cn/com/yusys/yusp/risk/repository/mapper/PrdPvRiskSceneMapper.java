/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.risk.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.risk.domain.PrdPvRiskScene;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: yusp-npam-biz-core模块
 * @类名称: PrdPvRiskSceneMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2020-07-27 17:26:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PrdPvRiskSceneMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    PrdPvRiskScene selectByPrimaryKey(@Param("sceneId") String sceneId, @Param("preventId") String preventId, @Param("itemId") String itemId, @Param("wfid") String wfid, @Param("riskLevel") String riskLevel, @Param("itemName") String itemName);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String, String>> selectByModel(QueryModel model);


    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(PrdPvRiskScene record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(PrdPvRiskScene record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(PrdPvRiskScene record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(PrdPvRiskScene record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("sceneId") String sceneId, @Param("preventId") String preventId, @Param("bizType") String bizType);

    /**
     * <br/>
     * 1.0 tangxun:2020/7/28 7:27 下午: <br/>
     *
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.String>>
     * @author tangxun
     * @date ​2020/7/28 7:27 下午
     * @version 1.0
     * @since 2020/7/28 7:27 下午
     */
    List<Map<String, String>> selectDistinctScence(QueryModel model);

    /**
     * <br/>
     * 1.0 tangxun:2020/7/29 9:18 上午: <br/>
     *
     * @return java.util.List<cn.com.yusys.yusp.risk.domain.PrdPvRiskScene>
     * @author tangxun
     * @date ​2020/7/29 9:18 上午
     * @version 1.0
     * @since 2020/7/29 9:18 上午
     */
    List<Map<String, String>> selectInitScence(QueryModel model);

    /**
     * <br/>
     * 1.0 tangxun:2020/7/30 10:50 上午: <br/>
     *
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.String>>
     * @author tangxun
     * @date ​2020/7/30 10:50 上午
     * @version 1.0
     * @since 2020/7/30 10:50 上午
     */
    List<Map<String, String>> getRiskItem(QueryModel model);


    /**
     * <br/>
     * 1.0 zhangliang15:2021/09/14 10:50 上午: <br/>
     *
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.String>>
     * @author zhangliang15
     * @date ​2021/09/14 10:50 上午
     * @version 1.0
     * @since 2021/09/14 10:50 上午
     */
    List<Map<String, String>> getRiskNoFlowItem(QueryModel model);

}