/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.constant.QryFlexConstant;
import cn.com.yusys.yusp.domain.CfgFlexQryDetail;
import cn.com.yusys.yusp.domain.CfgFlexQryIndex;
import cn.com.yusys.yusp.repository.mapper.CfgFlexQryIndexMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryIndexService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-28 20:43:16
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgFlexQryIndexService {
    private static final Logger log = LoggerFactory.getLogger(CfgPrdBasicinfoService.class);

    @Autowired
    private CfgFlexQryIndexMapper cfgFlexQryIndexMapper;
    @Autowired
    private CfgFlexQryDetailService cfgFlexQryDetailService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgFlexQryIndex selectByPrimaryKey(String indexCode) {
        return cfgFlexQryIndexMapper.selectByPrimaryKey(indexCode);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgFlexQryIndex> selectAll(QueryModel model) {
        List<CfgFlexQryIndex> records = (List<CfgFlexQryIndex>) cfgFlexQryIndexMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgFlexQryIndex> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgFlexQryIndex> list = cfgFlexQryIndexMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgFlexQryIndex record) {
        return cfgFlexQryIndexMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgFlexQryIndex record) {
        return cfgFlexQryIndexMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgFlexQryIndex record) {
        return cfgFlexQryIndexMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgFlexQryIndex record) {
        return cfgFlexQryIndexMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String indexCode) {
        return cfgFlexQryIndexMapper.deleteByPrimaryKey(indexCode);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgFlexQryIndexMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除，根据指标编号将数据的操作类型修改为“删除”
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int logicDelete(String indexCode) {
        //指标配置对象
        CfgFlexQryIndex cfgFlexQryIndex = new CfgFlexQryIndex();
        //指标明细对象
        CfgFlexQryDetail cfgFlexQryDetail = new CfgFlexQryDetail();
        //指标编号
        cfgFlexQryIndex.setIndexCode(indexCode);
        //若删除的为对象指标，联动删除下级分组指标
        cfgFlexQryIndex.setParentObj(indexCode);
        //操作标识
        cfgFlexQryIndex.setOprType(CommonConstant.DELETE_OPR);
        int num = cfgFlexQryIndexMapper.updateByIndexCodeOrParentId(cfgFlexQryIndex);
        int numDetail = 0;
        if (num > 0) {
            //联动删除指标明细
            cfgFlexQryDetail.setIndexCode(indexCode);
            cfgFlexQryDetail.setOprType(CommonConstant.DELETE_OPR);
            numDetail = cfgFlexQryDetailService.updateByIndexCode(cfgFlexQryDetail);
        }
        log.info("***************删除指标条数【" + num + "】条,删除指标参数详细条数【" + numDetail + "】条***************");

        return num;
    }

    /**
     * @方法名称: getMainCfgFlexQryIndexByQryCode
     * @方法描述: 根据灵活查询流水号查询主指标
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgFlexQryIndex getMainCfgFlexQryIndexByQryCode(String qryCode) {
        return cfgFlexQryIndexMapper.getMainCfgFlexQryIndexByQryCode(qryCode);
    }

    /**
     * @方法名称: selectCfgFlexQryIndexByQryCode
     * @方法描述: 通过灵活查询报表编号查询子表的指标配置编码及详情
     * @参数与返回说明: 查询所有类型为条件字段的参数条件，将paramkey按排序拼接，除了__cond_title结尾的不需要
     * @算法描述: 无
     */
    public List<CfgFlexQryIndex> selectCfgFlexQryIndexByQryCode(String qryCode) {
        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("qryCode", qryCode);
        queryMap.put("paramType", QryFlexConstant.PARAM_TYPE_SHOW_COLUMN);
        queryMap.put("paramKey", QryFlexConstant.PARAM_KEY_SHOW_COLUMN);
        List<CfgFlexQryIndex> indexCodeList = cfgFlexQryIndexMapper.selectCfgFlexQryIndexByQryCode(queryMap);
        return indexCodeList;
    }
}
