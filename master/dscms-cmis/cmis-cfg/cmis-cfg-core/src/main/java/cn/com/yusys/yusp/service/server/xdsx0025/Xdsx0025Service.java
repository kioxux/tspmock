package cn.com.yusys.yusp.service.server.xdsx0025;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCfgConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CfgApproveCapitalrate;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CfgApproveCapitalrateMapper;
import cn.com.yusys.yusp.server.xdsx0025.req.Xdsx0025DataReqDto;
import cn.com.yusys.yusp.server.xdsx0025.req.Xdsx0025SubData;
import cn.com.yusys.yusp.server.xdsx0025.resp.Xdsx0025DataRespDto;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg模块
 * @类名称: Xdsx0025Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-24 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdsx0025Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0025Service.class);
    @Resource
    private CfgApproveCapitalrateMapper cfgApproveCapitalrateMapper;

    /**
     * 审批人资本占用率参数表同步
     *
     * @param xdsx0025DataReqDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Xdsx0025DataRespDto xdsx0025(Xdsx0025DataReqDto xdsx0025DataReqDto) throws ParseException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, JSON.toJSONString(xdsx0025DataReqDto));
        Xdsx0025DataRespDto xdsx0025DataRespDto = new Xdsx0025DataRespDto();
        try {
            if (xdsx0025DataReqDto != null && !"".equals(xdsx0025DataReqDto.getCusttype())) {
                String cusType = xdsx0025DataReqDto.getCusttype();
                String updateTime = xdsx0025DataReqDto.getUpdate_time();
                List<Xdsx0025SubData> xdsx0025SubDataList = xdsx0025DataReqDto.getList();
                if (xdsx0025SubDataList != null && xdsx0025SubDataList.size() > 0) {
                    //根据客户类型关联更新历史数据状态为失效
                    cfgApproveCapitalrateMapper.updateStatusByCusType(cusType);
                    Xdsx0025SubData xdsx0025SubData = new Xdsx0025SubData();
                    CfgApproveCapitalrate cfgApproveCapitalrate = new CfgApproveCapitalrate();
                    for (int i = 0; i < xdsx0025SubDataList.size(); i++) {
                        xdsx0025SubData = xdsx0025SubDataList.get(i);
                        cfgApproveCapitalrate.setCusType(cusType);
                        cfgApproveCapitalrate.setDutyCode(xdsx0025SubData.getDutyno());
                        cfgApproveCapitalrate.setMaxVal(new BigDecimal(xdsx0025SubData.getUpval()));
                        cfgApproveCapitalrate.setMinVal(new BigDecimal(xdsx0025SubData.getLowerval()));
                        cfgApproveCapitalrate.setStatus("1");//生效
                        cfgApproveCapitalrate.setInputDate(updateTime);
                        cfgApproveCapitalrate.setUpdDate(updateTime);
                        cfgApproveCapitalrateMapper.insert(cfgApproveCapitalrate);
                    }
                }
                xdsx0025DataRespDto.setOpFlag("S");
                xdsx0025DataRespDto.setOpMsg("交易成功");
            } else {
                xdsx0025DataRespDto.setOpFlag("F");
                xdsx0025DataRespDto.setOpMsg("客户类型不能为空");
            }

        } catch (BizException e) {
            xdsx0025DataRespDto.setOpFlag(CmisCfgConstants.OP_FLAG_F);
            xdsx0025DataRespDto.setOpMsg(CmisCfgConstants.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdsx0025DataRespDto.setOpFlag(CmisCfgConstants.OP_FLAG_F);
            xdsx0025DataRespDto.setOpMsg(CmisCfgConstants.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, JSON.toJSONString(xdsx0025DataRespDto));
        return xdsx0025DataRespDto;
    }

}
