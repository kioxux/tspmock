/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgRateCalculation
 * @类描述: cfg_rate_calculation数据实体类
 * @功能描述:
 * @创建人: hhj123456
 * @创建时间: 2021-06-18 17:21:54
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_rate_calculation")
public class CfgRateCalculation extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 权限类型
     **/
    @Column(name = "PERMISSION_TYPE", unique = false, nullable = true, length = 20)
    private String permissionType;

    /**
     * 测算类型
     **/
    @Column(name = "CALCULATION_TYPE", unique = false, nullable = true, length = 20)
    private String calculationType;

    /**
     * 指导利率
     **/
    @Column(name = "GUIDANCE_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal guidanceRate;

    /**
     * 保本利率
     **/
    @Column(name = "BREAK_EVEN_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal breakEvenRate;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最近更新人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近更新机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近更新日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private Date updateTime;

    public CfgRateCalculation() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return permissionType
     */
    public String getPermissionType() {
        return this.permissionType;
    }

    /**
     * @param permissionType
     */
    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    /**
     * @return calculationType
     */
    public String getCalculationType() {
        return this.calculationType;
    }

    /**
     * @param calculationType
     */
    public void setCalculationType(String calculationType) {
        this.calculationType = calculationType;
    }

    /**
     * @return guidanceRate
     */
    public java.math.BigDecimal getGuidanceRate() {
        return this.guidanceRate;
    }

    /**
     * @param guidanceRate
     */
    public void setGuidanceRate(java.math.BigDecimal guidanceRate) {
        this.guidanceRate = guidanceRate;
    }

    /**
     * @return breakEvenRate
     */
    public java.math.BigDecimal getBreakEvenRate() {
        return this.breakEvenRate;
    }

    /**
     * @param breakEvenRate
     */
    public void setBreakEvenRate(java.math.BigDecimal breakEvenRate) {
        this.breakEvenRate = breakEvenRate;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return createTime
     */
    public Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }


}