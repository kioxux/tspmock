/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.domain.CfgUserWorkCalendar;
import cn.com.yusys.yusp.repository.mapper.CfgUserWorkCalendarMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgUserWorkCalendarService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Admin
 * @创建时间: 2021-03-31 16:45:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgUserWorkCalendarService {

    @Autowired
    private CfgUserWorkCalendarMapper cfgUserWorkCalendarMapper;

    /**
     * @方法名称: queryByMonth
     * @方法描述: 根据年月 查询当月的日志或备忘录记录.
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgUserWorkCalendar> queryByMonth(String calendarDate, String loginCode) {
        return cfgUserWorkCalendarMapper.queryByMonth(calendarDate, loginCode, "01");
    }
}
