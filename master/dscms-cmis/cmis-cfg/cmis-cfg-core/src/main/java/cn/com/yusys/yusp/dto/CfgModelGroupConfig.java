package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

public class CfgModelGroupConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    private CfgModelGroupItem cfgModelGroupItem;
    private List<CfgModelGroupItem> list;

    public cn.com.yusys.yusp.dto.CfgModelGroupItem getCfgModelGroupItem() {
        return cfgModelGroupItem;
    }

    public void setCfgModelGroupItem(cn.com.yusys.yusp.dto.CfgModelGroupItem cfgModelGroupItem) {
        this.cfgModelGroupItem = cfgModelGroupItem;
    }

    public List<cn.com.yusys.yusp.dto.CfgModelGroupItem> getList() {
        return list;
    }

    public void setList(List<cn.com.yusys.yusp.dto.CfgModelGroupItem> list) {
        this.list = list;
    }
}
