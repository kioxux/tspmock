/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgModelGroupDetail
 * @类描述: cfg_model_group_detail数据实体类
 * @功能描述:
 * @创建人: 15430
 * @创建时间: 2020-11-24 11:18:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_model_group_detail")
public class CfgModelGroupDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 模版组编号
     **/
    @Column(name = "MODEL_GROUP_NO", unique = false, nullable = false, length = 40)
    private String modelGroupNo;

    /**
     * 页面ID
     **/
    @Column(name = "FUNC_ID", unique = false, nullable = true, length = 50)
    private String funcId;

    /**
     * 页面名称
     **/
    @Column(name = "FUNC_NAME", unique = false, nullable = true, length = 100)
    private String funcName;

    /**
     * 页面URL
     **/
    @Column(name = "FUNC_URL", unique = false, nullable = true, length = 400)
    private String funcUrl;

    /**
     * 是否主页面 STD_ZB_YES_NO
     **/
    @Column(name = "IS_MAIN_FUNC", unique = false, nullable = false, length = 5)
    private String isMainFunc;

    /**
     * 页面显示顺序
     **/
    @Column(name = "SEQ_NO", unique = false, nullable = false, length = 10)
    private Integer seqNo;

    /**
     * 从页面显示条件
     **/
    @Column(name = "SHOW_COND", unique = false, nullable = true, length = 100)
    private String showCond;

    /**
     * 从页面过滤条件
     **/
    @Column(name = "FILTER_COND", unique = false, nullable = true, length = 100)
    private String filterCond;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = false, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = false, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = false, length = 20)
    private String inputDate;

    /**
     * 更新人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = false, length = 20)
    private String updId;

    /**
     * 更新机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = false, length = 20)
    private String updBrId;

    /**
     * 更新日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = false, length = 20)
    private String updDate;

    /**
     * 关联类型
     **/
    @Column(name = "REL_TYPE", unique = false, nullable = true, length = 5)
    private String relType;

    public CfgModelGroupDetail() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return modelGroupNo
     */
    public String getModelGroupNo() {
        return this.modelGroupNo;
    }

    /**
     * @param modelGroupNo
     */
    public void setModelGroupNo(String modelGroupNo) {
        this.modelGroupNo = modelGroupNo;
    }

    /**
     * @return funcId
     */
    public String getFuncId() {
        return this.funcId;
    }

    /**
     * @param funcId
     */
    public void setFuncId(String funcId) {
        this.funcId = funcId;
    }

    /**
     * @return funcName
     */
    public String getFuncName() {
        return this.funcName;
    }

    /**
     * @param funcName
     */
    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    /**
     * @return funcUrl
     */
    public String getFuncUrl() {
        return this.funcUrl;
    }

    /**
     * @param funcUrl
     */
    public void setFuncUrl(String funcUrl) {
        this.funcUrl = funcUrl;
    }

    /**
     * @return isMainFunc
     */
    public String getIsMainFunc() {
        return this.isMainFunc;
    }

    /**
     * @param isMainFunc
     */
    public void setIsMainFunc(String isMainFunc) {
        this.isMainFunc = isMainFunc;
    }

    /**
     * @return seqNo
     */
    public Integer getSeqNo() {
        return this.seqNo;
    }

    /**
     * @param seqNo
     */
    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    /**
     * @return showCond
     */
    public String getShowCond() {
        return this.showCond;
    }

    /**
     * @param showCond
     */
    public void setShowCond(String showCond) {
        this.showCond = showCond;
    }

    /**
     * @return filterCond
     */
    public String getFilterCond() {
        return this.filterCond;
    }

    /**
     * @param filterCond
     */
    public void setFilterCond(String filterCond) {
        this.filterCond = filterCond;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return relType
     */
    public String getRelType() {
        return this.relType;
    }

    /**
     * @param relType
     */
    public void setRelType(String relType) {
        this.relType = relType;
    }


}