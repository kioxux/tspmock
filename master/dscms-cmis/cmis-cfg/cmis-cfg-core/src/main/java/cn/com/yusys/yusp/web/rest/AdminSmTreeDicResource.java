/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AdminSmTreeDic;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.AdminSmTreeDicService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: AdminSmTreeDicResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 12651
 * @创建时间: 2021-05-07 16:07:02
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/adminsmtreedic")
public class AdminSmTreeDicResource {
    @Autowired
    private AdminSmTreeDicService adminSmTreeDicService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private SequenceTemplateService sequenceTemplateService;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AdminSmTreeDic>> query() {
        QueryModel queryModel = new QueryModel();
        List<AdminSmTreeDic> list = adminSmTreeDicService.selectAll(queryModel);
        return new ResultDto<List<AdminSmTreeDic>>(list);
    }

    @PostMapping("/openday")
    protected ResultDto<String> openday() {
        String openday = Optional.ofNullable(stringRedisTemplate.opsForValue().get("openDay")).orElse(LocalDate.now().toString()).replaceAll("-", "").replaceAll("\"","");//当前日期
        return new ResultDto<>(openday);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AdminSmTreeDic>> index(QueryModel queryModel) {
        List<AdminSmTreeDic> list = adminSmTreeDicService.selectByModel(queryModel);
        return new ResultDto<List<AdminSmTreeDic>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AdminSmTreeDic> show(@PathVariable("pkId") String pkId) {
        AdminSmTreeDic adminSmTreeDic = adminSmTreeDicService.selectByPrimaryKey(pkId);
        return new ResultDto<AdminSmTreeDic>(adminSmTreeDic);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<AdminSmTreeDic> create(@RequestBody AdminSmTreeDic adminSmTreeDic) throws URISyntaxException {
        String enName = sequenceTemplateService.getSequenceTemplate("RULE_PLAN_ID", new HashMap<>(1));
        adminSmTreeDic.setEnName(enName);
        adminSmTreeDic.setPkId(enName);
        adminSmTreeDic.setLocate(adminSmTreeDic.getLocate() + enName + ",");
        adminSmTreeDicService.insertSelective(adminSmTreeDic);
        return new ResultDto<AdminSmTreeDic>(adminSmTreeDic);
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AdminSmTreeDic adminSmTreeDic) throws URISyntaxException {
        int result = adminSmTreeDicService.updateSelective(adminSmTreeDic);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = adminSmTreeDicService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = adminSmTreeDicService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述: 单个查询方法
     */
    @PostMapping("/query")
    protected ResultDto<List<Map<String, String>>> querySingle(@RequestBody AdminSmTreeDicDto adminSmTreeDicDto) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("optType", adminSmTreeDicDto.getOptType());
        if (StringUtils.trimToEmpty(adminSmTreeDicDto.getCode()).equals("")) {
            return new ResultDto<>();
        } else {
            queryModel.addCondition("enName", adminSmTreeDicDto.getCode());
        }
        List<AdminSmTreeDic> list = adminSmTreeDicService.selectAll(queryModel);
        if (list.isEmpty()) {
            return new ResultDto<>();
        } else {
            return new ResultDto<>(list.stream().map(item -> {
                Map<String, String> map = new HashMap<>();
                map.put("id", item.getEnName());
                map.put("label", item.getCnName());
                return map;
            }).collect(Collectors.toList()));
        }
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述: 单个查询方法
     */
    @PostMapping("/queryLabelPath")
    protected ResultDto<Map<String, String>> querySingleLabelPath(@RequestBody AdminSmTreeDicDto adminSmTreeDicDto) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("optType", adminSmTreeDicDto.getOptType());
        if (StringUtils.trimToEmpty(adminSmTreeDicDto.getCode()).equals("")) {
            return new ResultDto<>();
        } else {
            queryModel.addCondition("enName", adminSmTreeDicDto.getCode());
        }
        List<AdminSmTreeDic> list = adminSmTreeDicService.selectAll(queryModel);
        if (list.isEmpty()) {
            return new ResultDto<>();
        } else {
            //
            AdminSmTreeDic adminSmTreeDic = list.get(0);
            String locate = adminSmTreeDic.getLocate();
            String[] locates = locate.split(",");
            locates = (String[]) ArrayUtils.remove(locates, 0); // 删除第一个元素
            queryModel.addCondition("enName", StringUtils.join(locates, ","));
            queryModel.setSort("levels");
            queryModel.addCondition("optType", adminSmTreeDic.getOptType());
            List<AdminSmTreeDic> parentlist = adminSmTreeDicService.selectAll(queryModel);
            if (parentlist.isEmpty()) {
                return new ResultDto<>();
            } else {
                StringBuilder cnSb = new StringBuilder();
             /*   parentlist.stream().forEach(item -> {
                    cnSb.append(item.getCnName());
                });*/
                for(int i=0;i<parentlist.size();i++){
                    if(i==(parentlist.size()-1)){
                        cnSb.append(parentlist.get(i).getCnName());
                    }else{
                        cnSb.append(parentlist.get(i).getCnName()+"->");
                    }
                }
                Map<String, String> map = new HashMap<>();
                map.put("codePath", StringUtils.join(locates, ","));
                map.put("labelPath", cnSb.toString());
                return new ResultDto<>(map);
            }
        }
    }

    /***
     * @param [map]
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List < java.util.Map < java.lang.String, java.lang.Object>>>
     * @author tangxun
     * @date 2021/6/15 10:47
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/tree")
    protected ResultDto<List<Map<String, Object>>> getTreedic(@RequestBody Map<String, String> map) {
        String optType = map.get("optType");
        String rootId = map.get("root");
        List<Map<String, Object>> list = adminSmTreeDicService.queryAdminSmDicTree(optType, rootId);
        return new ResultDto<>(list);
    }


    /***
     * @param map
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List < java.util.Map < java.lang.String, java.lang.Object>>>
     * @author tangxun
     * @date 2021/6/15 10:47
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/treeInput")
    protected ResultDto<List<Map<String, Object>>> getTreedicInputName(@RequestBody Map<String, String> map) {
        String optType = map.get("optType");
        String rootId = map.get("root");
        List<Map<String, Object>> list = adminSmTreeDicService.queryAdminSmDicTreeInputName(optType, rootId);
        return new ResultDto<>(list);
    }

    /***
     * @param [pkId]
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author tangxun
     * @date 2021/6/15 13:50
     * @version 1.0.0
     * @desc 删除当前节点及所有子节点
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/deleteTree")
    protected ResultDto<Integer> treeDelete(@RequestBody AdminSmTreeDicDto adminSmTreeDicDto) {
        int result = adminSmTreeDicService.treeDdeleteByPrimaryKey(adminSmTreeDicDto);
        return new ResultDto<Integer>(result);
    }


    @PostMapping("/lazytree")
    protected ResultDto<List<Map<String, Object>>> getLazyTreedic(@RequestBody Map<String, String> map) {
        String optType = map.get("optType");
        String rootId = map.get("root");
        String levels = map.get("levels");
        String id = map.get("id");
        List<Map<String, Object>> list = adminSmTreeDicService.queryAdminSmDicTree(optType, rootId, levels, id);
        return new ResultDto<>(list);
    }

    @PostMapping("/selecttree")
    protected ResultDto<List<Map<String, Object>>> getSelectTreedic(@RequestBody Map<String, String> map) {
        String optType = map.get("optType");
        String levels = map.get("levels");
        String id = map.get("id");
        List<Map<String, Object>> list = adminSmTreeDicService.queryAdminSmSelectTree(optType, levels, id);
        return new ResultDto<>(list);
    }

    @PostMapping("/querycfgtreebycode")
    protected ResultDto<AdminSmTreeDicDto> queryCfgtreebBycode(@RequestBody AdminSmTreeDicDto adminSmTreeDicDto) {
            AdminSmTreeDicDto record = adminSmTreeDicService.queryCfgtreebBycode(adminSmTreeDicDto);
        return new ResultDto<>(record);
    }
    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/9/17 0:10
     * @version 1.0.0
     * @desc  查询上级目录
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryhighcfgtreebycode")
    protected ResultDto<AdminSmTreeDicDto> queryHighCfgtreebBycode(@RequestBody AdminSmTreeDicDto adminSmTreeDicDto) {
        AdminSmTreeDicDto record = adminSmTreeDicService.queryHighCfgtreebBycode(adminSmTreeDicDto);
        return new ResultDto<>(record);
    }
}
