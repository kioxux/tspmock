/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CfgServiceExceptionDefEnums;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgBizRulePlan;
import cn.com.yusys.yusp.domain.CfgNodeRuleRel;
import cn.com.yusys.yusp.domain.CfgPrdBasicinfo;
import cn.com.yusys.yusp.domain.CfgRuleItem;
import cn.com.yusys.yusp.dto.CfgBizRulePlanDto;
import cn.com.yusys.yusp.repository.mapper.*;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBizRulePlanService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: liuch
 * @创建时间: 2020-12-23 11:22:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgBizRulePlanService {
    private static final Logger log = LoggerFactory.getLogger(CfgPrdBasicinfoService.class);

    @Autowired
    private CfgBizRulePlanMapper cfgBizRulePlanMapper;

    @Autowired
    private CfgRuleItemMapper cfgRuleItemMapper;

    @Autowired
    private CfgNodeRuleRelMapper cfgNodeRuleRelMapper;

    @Autowired
    private CfgPrdBasicinfoMapper cfgPrdBasicinfoMapper;

    @Autowired
    private CfgPlanRuleRelMapper cfgPlanRuleRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgBizRulePlan selectByPrimaryKey(String planId) {
        return cfgBizRulePlanMapper.selectByPrimaryKey(planId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgBizRulePlan> selectAll(QueryModel model) {
        List<CfgBizRulePlan> records = (List<CfgBizRulePlan>) cfgBizRulePlanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgBizRulePlan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgBizRulePlan> list = cfgBizRulePlanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgBizRulePlan record) {
        return cfgBizRulePlanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgBizRulePlan record) {
        return cfgBizRulePlanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgBizRulePlan record) {
        return cfgBizRulePlanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgBizRulePlan record) {
        return cfgBizRulePlanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String planId) {
        return cfgBizRulePlanMapper.deleteByPrimaryKey(planId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgBizRulePlanMapper.deleteByIds(ids);
    }


    /**
     * 根据业务规则项目编号查询所有引用该规则的业务规则方案
     *
     * @param ruleItemId
     * @return
     */
    public List<CfgBizRulePlan> queryQuotePlanByRuleItemId(String ruleItemId) {
        String oprType = CommonConstant.ADD_OPR;//操作标识=新增
        List<CfgBizRulePlan> records = (List<CfgBizRulePlan>) cfgBizRulePlanMapper.queryQuotePlanByRuleItemId(ruleItemId, oprType);
        return records;
    }

    /**
     * 获取所有【启用】的规则信息
     *
     * @param planId
     * @return
     */
    public CfgBizRulePlanDto queryAllRuleDetailsById(String planId) {
        CfgBizRulePlanDto cfgBizRulePlanDto = new CfgBizRulePlanDto();
        CfgBizRulePlan cfgBizRulePlan = cfgBizRulePlanMapper.selectEffectByPrimaryKey(planId);
        List<CfgRuleItem> ruleItems = cfgRuleItemMapper.selectEffectRuleItems(planId);
        cfgBizRulePlanDto.setCfgBizRulePlan(cfgBizRulePlan);
        cfgBizRulePlanDto.setRuleItems(ruleItems);
        return cfgBizRulePlanDto;
    }

    /**
     * 获取工作流节点所有【启用】的规则信息
     *
     * @param wfiSignId
     * @param nodeId
     * @return
     */
    public CfgBizRulePlanDto queryWfNodeAllRuleDetails(String wfiSignId, String nodeId) {
        CfgBizRulePlanDto cfgBizRulePlanDto = new CfgBizRulePlanDto();
        CfgNodeRuleRel param = new CfgNodeRuleRel();
        param.setWfiSignId(wfiSignId);
        param.setNodeId(nodeId);
        // 获取对应的方案ID
        List<CfgNodeRuleRel> tempList = cfgNodeRuleRelMapper.queryByPlanWfiNodeInfo(param);
        String planId = tempList.get(0).getPlanId();
        // 根据方案id获取方案基本信息
        CfgBizRulePlan cfgBizRulePlan = cfgBizRulePlanMapper.selectEffectByPrimaryKey(planId);
        // 获取该节点有效的规则项
        param.setPlanId(planId);
        List<CfgRuleItem> ruleItems = cfgRuleItemMapper.selectWfNodeEffectRuleItems(param);
        cfgBizRulePlanDto.setCfgBizRulePlan(cfgBizRulePlan);
        cfgBizRulePlanDto.setRuleItems(ruleItems);
        return cfgBizRulePlanDto;
    }


    /**
     * 根据获取业务规则方案编号逻辑删除该业务规则方案及关联的子表（业务规则方案关联规则项、业务规则方案关联流程）
     *
     * @param planId
     * @return
     */
    @Transactional
    public int deleteCfgBizRulePlanAndChild(String planId) {
        int result = 0;

        //先查询是否存在引用该业务规则方案的产品，若存在则不允许删除
        List<CfgPrdBasicinfo> prdList = cfgPrdBasicinfoMapper.selectByPlanId(planId, CommonConstant.ADD_OPR);
        if (prdList != null && prdList.size() > 0) {
            log.info("***************当前业务规则方案已被【" + prdList.size() + "】产品引用，不能删除！***************");
            throw new YuspException(CfgServiceExceptionDefEnums.E_CFG_PRD_BASE_INFO_EXISTS_ERROR.getExceptionCode()
                    , CfgServiceExceptionDefEnums.E_CFG_PRD_BASE_INFO_EXISTS_ERROR.getExceptionDesc());
        }
        //若不存在，则逻辑删除该业务规则方案及关联的子表
        //操作类型(02-删除)
        String OprType = CommonConstant.DELETE_OPR;
        CfgBizRulePlan cfgBizRulePlan = new CfgBizRulePlan();
        cfgBizRulePlan.setPlanId(planId);
        cfgBizRulePlan.setOprType(OprType);
        //逻辑删除 业务规则方案 中操作标识更新为 02
        result = cfgBizRulePlanMapper.updateByPrimaryKeySelective(cfgBizRulePlan);
        log.info("***************删除业务规则方案【" + result + "】条！***************");
        int relNum = cfgPlanRuleRelMapper.updateByPlanId(planId, OprType);
        log.info("***************删除业务规则方案关联规则项【" + relNum + "】条！***************");
        int nodeNum = cfgNodeRuleRelMapper.updateByPlanId(planId, OprType);
        log.info("***************删除业务规则方案关联流程【" + nodeNum + "】条！***************");

        return result;
    }
}
