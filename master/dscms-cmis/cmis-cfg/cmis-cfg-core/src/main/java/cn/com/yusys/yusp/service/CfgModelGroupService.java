/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CfgModelConstant;
import cn.com.yusys.yusp.domain.CfgModelGroup;
import cn.com.yusys.yusp.dto.CfgModelConfigItem;
import cn.com.yusys.yusp.repository.mapper.CfgModelGroupMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgModelGroupService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 15430
 * @创建时间: 2020-11-20 16:46:49
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgModelGroupService {

    @Autowired
    private CfgModelGroupMapper cfgModelGroupMapper;

    @Autowired
    private CfgModelGroupDetailService cfgModelGroupDetailService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgModelGroup selectByPrimaryKey(String modelGroupNo) {
        return cfgModelGroupMapper.selectByPrimaryKey(modelGroupNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgModelGroup> selectAll(QueryModel model) {
        List<CfgModelGroup> records = (List<CfgModelGroup>) cfgModelGroupMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgModelGroup> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgModelGroup> list = cfgModelGroupMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgModelGroup record) {
        return cfgModelGroupMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgModelGroup record) {
        return cfgModelGroupMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgModelGroup record) {
        return cfgModelGroupMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgModelGroup record) {
        return cfgModelGroupMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String modelGroupNo) {
        return cfgModelGroupMapper.deleteByPrimaryKey(modelGroupNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgModelGroupMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteCas
     * @方法描述: 关联删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional()
    public int deleteCas(String modelGroupNo) {
        cfgModelGroupDetailService.deleteByCfgModelGroupNo(modelGroupNo);
        return cfgModelGroupMapper.deleteByPrimaryKey(modelGroupNo);
    }

    /**
     * @方法名称: getModelGroupConfig
     * @方法描述: 获取模板工厂配置
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgModelConfigItem> getModelGroupConfig(String modelGroupNo) {
        List<CfgModelConfigItem> mainConfigList = cfgModelGroupMapper.selectAllMainConfig();
        List<CfgModelConfigItem> subConfigList = cfgModelGroupDetailService.selectAllSubConfig();

        List<CfgModelConfigItem> resultList = new ArrayList<>();

        List<CfgModelConfigItem> childList = new ArrayList<>();
        for (CfgModelConfigItem item : mainConfigList) {
            if (modelGroupNo.equals(item.getId())) {
                childList.add(item);
            }
        }

        do {
            resultList.addAll(childList);
            childList = getChilds(childList, subConfigList);
        } while (!CollectionUtils.isEmpty(childList));

        // 模板组的部分属性复制过来
        resultList.stream().forEach(item -> {
            if (CfgModelConstant.MODEL_GROUP.equals(item.getRelType())) {
                CfgModelConfigItem cfgModelConfigItem = this.getCfgModelGroup(item.getId(), mainConfigList);

                if (cfgModelConfigItem != null) {
                    item.setShowMode(cfgModelConfigItem.getShowMode());
                    item.setVer(cfgModelConfigItem.getVer());
                    item.setIsJobFlow(cfgModelConfigItem.getIsJobFlow());
                    item.setJobFlow(cfgModelConfigItem.getJobFlow());
                    item.setPlanId(cfgModelConfigItem.getPlanId());
                    item.setRemark(cfgModelConfigItem.getRemark());
                }
            }
        });
        return resultList;
    }

    private List<CfgModelConfigItem> getChilds(List<CfgModelConfigItem> tempList, List<CfgModelConfigItem> subConfigList) {
        List<CfgModelConfigItem> childList = new ArrayList<>();
        if (CollectionUtils.isEmpty(tempList)) {
            return childList;
        }

        tempList.stream().forEach(item -> {
            childList.addAll(getChilds(item, subConfigList));
        });
        return childList;
    }

    private List<CfgModelConfigItem> getChilds(CfgModelConfigItem fatherItem, List<CfgModelConfigItem> subConfigList) {
        List<CfgModelConfigItem> childList = new ArrayList<>();
        if (fatherItem == null || fatherItem.getId() == null) {
            return childList;
        }

        // 页面不用查子节点
        if (fatherItem.getPkId() != null && fatherItem.getRelType() != null
                && CfgModelConstant.PAGE.equals(fatherItem.getRelType())) {
            return childList;
        }

        subConfigList.stream().forEach(item -> {
            if (fatherItem.getId().equals(item.getPid())) {
                childList.add(item);
            }
        });
        return childList;
    }

    private CfgModelConfigItem getCfgModelGroup(String groupNo, List<CfgModelConfigItem> mainConfigList) {

        if (StringUtils.isEmpty(groupNo) || CollectionUtils.isEmpty(mainConfigList)) {
            return null;
        }

        List<CfgModelConfigItem> list = mainConfigList.stream()
                .filter(item -> groupNo.equals(item.getId()))
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }
}
