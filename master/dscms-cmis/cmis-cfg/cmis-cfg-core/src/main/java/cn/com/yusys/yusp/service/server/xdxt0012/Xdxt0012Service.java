package cn.com.yusys.yusp.service.server.xdxt0012;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AdminSmTreeDicMapper;
import cn.com.yusys.yusp.server.xdxt0012.req.Xdxt0012DataReqDto;
import cn.com.yusys.yusp.server.xdxt0012.resp.Xdxt0012DataRespDto;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg模块
 * @类名称: Xdxt0012Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxt0012Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0012Service.class);

    @Resource
    private AdminSmTreeDicMapper adminSmTreeDicMapper;

    /**
     * 根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
     *
     * @param xdxt0012DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0012DataRespDto getXdxt0012(Xdxt0012DataReqDto xdxt0012DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, JSON.toJSONString(xdxt0012DataReqDto));
        Xdxt0012DataRespDto xdxt0012DataRespDto = new Xdxt0012DataRespDto();
        java.util.List<cn.com.yusys.yusp.server.xdxt0012.resp.List> list = adminSmTreeDicMapper.getXdxt0012(xdxt0012DataReqDto);
        xdxt0012DataRespDto.setList(list);
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, JSON.toJSONString(xdxt0012DataReqDto));
        return xdxt0012DataRespDto;
    }

}
