package cn.com.yusys.yusp.service.server.cmiscfg0001;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.cmiscfg0001.req.CmisCfg0001ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0001.resp.CmisCfg0001RespDto;
import cn.com.yusys.yusp.workbench.domain.WbMsgNotice;
import cn.com.yusys.yusp.workbench.service.WbMsgNoticeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg模块
 * @类名称: CmisCfg0001Service
 * @类描述: #对内服务类
 * @功能描述: 新增首页提醒事项
 * @创建时间: 2021-06-30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisCfg0001Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisCfg0001Service.class);

    @Autowired
    private WbMsgNoticeService wbMsgNoticeService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Transactional
    public CmisCfg0001RespDto execute(CmisCfg0001ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value);
        CmisCfg0001RespDto respDto = new CmisCfg0001RespDto();
        try {
            WbMsgNotice wbMsgNotice = new WbMsgNotice();
            //拷贝数据
            BeanUtils.copyProperties(reqDto, wbMsgNotice);

            //默认未读
            wbMsgNotice.setMessageStatus("1");
            wbMsgNotice.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            wbMsgNotice.setUpdId(reqDto.getInputId());
            wbMsgNotice.setUpdBrId(reqDto.getInputBrId());
            wbMsgNotice.setUpdDate(reqDto.getInputDate());
            wbMsgNotice.setCreateTime(DateUtils.getCurrTimestamp());
            wbMsgNotice.setUpdateTime(DateUtils.getCurrTimestamp());

            //保存数据
            wbMsgNoticeService.insertSelective(wbMsgNotice);
            //发送提醒
            rabbitTemplate.convertAndSend(CmisBizConstants.MESSAGE_EXCHANGE, CmisBizConstants.MESSAGE_KEY, reqDto);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsCfgEnum.TRADE_CODE_CMISCFG0001.value, e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.value);
        return respDto;
    }

}


