/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CfgRetailPrimeRateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgRetailPrimeRate;
import cn.com.yusys.yusp.service.CfgRetailPrimeRateService;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgRetailPrimeRateResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:26:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgretailprimerate")
public class CfgRetailPrimeRateResource {
    @Autowired
    private CfgRetailPrimeRateService cfgRetailPrimeRateService;

	/**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<CfgRetailPrimeRate>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgRetailPrimeRate> list = cfgRetailPrimeRateService.selectAll(queryModel);
        return new ResultDto<List<CfgRetailPrimeRate>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/show/")
    protected ResultDto<List<CfgRetailPrimeRate>> index(@RequestBody QueryModel queryModel) {
        List<CfgRetailPrimeRate> list = cfgRetailPrimeRateService.selectByModel(queryModel);
        return new ResultDto<List<CfgRetailPrimeRate>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/show/{pkId}")
    protected ResultDto<CfgRetailPrimeRate> show(@PathVariable("pkId") String pkId) {
        CfgRetailPrimeRate cfgRetailPrimeRate = cfgRetailPrimeRateService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgRetailPrimeRate>(cfgRetailPrimeRate);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgRetailPrimeRate> create(@RequestBody CfgRetailPrimeRate cfgRetailPrimeRate) throws URISyntaxException {
        cfgRetailPrimeRateService.insert(cfgRetailPrimeRate);
        return new ResultDto<CfgRetailPrimeRate>(cfgRetailPrimeRate);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgRetailPrimeRate cfgRetailPrimeRate) throws URISyntaxException {
        int result = cfgRetailPrimeRateService.update(cfgRetailPrimeRate);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgRetailPrimeRateService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgRetailPrimeRateService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:selectbyPrdId
     * @函数描述: 根据产品编号查询最新的数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbyprdid")
    protected ResultDto<CfgRetailPrimeRate> selectbyPrdId(@RequestBody String prdid) {
        CfgRetailPrimeRate cfgRetailPrimeRate = cfgRetailPrimeRateService.selectbyPrdId(prdid);
        return new ResultDto<CfgRetailPrimeRate>(cfgRetailPrimeRate);
    }

    /**
     * @param cfgRetailPrimeRateDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CfgRetailPrimeRate>
     * @author 王玉坤
     * @date 2021/9/10 15:35
     * @version 1.0.0
     * @desc 根据条件查询优惠申请利率配置信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectretailprimeratebycondition")
    protected List<CfgRetailPrimeRateDto> selectRetailPrimerateByCondition(@RequestBody CfgRetailPrimeRateDto cfgRetailPrimeRateDto) {
        List<CfgRetailPrimeRateDto> cfgRetailPrimeRate = cfgRetailPrimeRateService.selectRetailPrimerateByCondition(cfgRetailPrimeRateDto);
        return cfgRetailPrimeRate;
    }
}
