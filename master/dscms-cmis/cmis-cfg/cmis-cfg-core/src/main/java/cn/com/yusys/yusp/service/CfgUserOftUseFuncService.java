/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgUserOftUseFunc;
import cn.com.yusys.yusp.dto.CfgUserOftUseFuncClientDto;
import cn.com.yusys.yusp.repository.mapper.CfgUserOftUseFuncMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgUserOftUseFuncService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2021-01-27 16:34:02
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgUserOftUseFuncService {

    @Autowired
    private CfgUserOftUseFuncMapper cfgUserOftUseFuncMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgUserOftUseFunc selectByPrimaryKey(String pkId) {
        return cfgUserOftUseFuncMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgUserOftUseFunc> selectAll(QueryModel model) {
        List<CfgUserOftUseFunc> records = (List<CfgUserOftUseFunc>) cfgUserOftUseFuncMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgUserOftUseFunc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgUserOftUseFunc> list = cfgUserOftUseFuncMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgUserOftUseFunc record) {
        List<CfgUserOftUseFuncClientDto> cfgUserOftUseFuncClientDtos = cfgUserOftUseFuncMapper.selectUserOftUseFuncByLoginCode(record.getLoginCode(), record.getFuncId(), CommonConstant.ADD_OPR);
        if (!CollectionUtils.isEmpty(cfgUserOftUseFuncClientDtos) && cfgUserOftUseFuncClientDtos.size() > 0) {
            // throw new YuspException(CfgServiceExceptionDefEnums.E_QUERY_FAILED.getExceptionCode(),CfgServiceExceptionDefEnums.E_QUERY_FAILED.getExceptionDesc());
            return 0;
        }
        if (StringUtils.isBlank(record.getPkId())) {
            record.setPkId(StringUtils.getUUID());
        }
        record.setInputDate(DateUtils.getCurrDateTimeStr());
        record.setUpdDate(DateUtils.getCurrDateTimeStr());
        record.setOprType(CommonConstant.ADD_OPR);
        return cfgUserOftUseFuncMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgUserOftUseFunc record) {
        return cfgUserOftUseFuncMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgUserOftUseFunc record) {
        return cfgUserOftUseFuncMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgUserOftUseFunc record) {
        return cfgUserOftUseFuncMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgUserOftUseFuncMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgUserOftUseFuncMapper.deleteByIds(ids);
    }

    /**
     * 获取当前用户常用功能
     *
     * @param loginCode
     * @return
     */
    public List<CfgUserOftUseFuncClientDto> getUserOftUseFuncByLoginCode(String loginCode) {
        return cfgUserOftUseFuncMapper.selectUserOftUseFuncByLoginCode(loginCode, null, CommonConstant.ADD_OPR);
    }

    /**
     * 根据主键ID修改状态，更新时间
     *
     * @param cfgUserOftUseFuncClientDto
     * @return
     */
    public int updateByPkId(CfgUserOftUseFuncClientDto cfgUserOftUseFuncClientDto) {
        cfgUserOftUseFuncClientDto.setUpdDate(DateUtils.getCurrDateTimeStr());
        cfgUserOftUseFuncClientDto.setOprType(CommonConstant.DELETE_OPR);
        return cfgUserOftUseFuncMapper.updateByPkId(cfgUserOftUseFuncClientDto);
    }
}
