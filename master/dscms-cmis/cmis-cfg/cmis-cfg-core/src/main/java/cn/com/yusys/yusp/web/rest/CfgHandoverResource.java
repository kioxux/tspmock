/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgHandover;
import cn.com.yusys.yusp.dto.CfgHandoverClientDto;
import cn.com.yusys.yusp.service.CfgHandoverService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgHandoverResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2020-11-20 13:55:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfghandover")
public class CfgHandoverResource {
    private static final Logger log = LoggerFactory.getLogger(CfgHandoverResource.class);
    @Autowired
    private CfgHandoverService cfgHandoverService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgHandover>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgHandover> list = cfgHandoverService.selectAll(queryModel);
        return new ResultDto<List<CfgHandover>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgHandover>> index(QueryModel queryModel) {
        List<CfgHandover> list = cfgHandoverService.selectByModel(queryModel);
        return new ResultDto<List<CfgHandover>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgHandover> show(@PathVariable("pkId") String pkId) {
        CfgHandover cfgHandover = cfgHandoverService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgHandover>(cfgHandover);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgHandover> create(@RequestBody CfgHandover cfgHandover) throws URISyntaxException {
        cfgHandoverService.insert(cfgHandover);
        return new ResultDto<CfgHandover>(cfgHandover);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgHandover cfgHandover) throws URISyntaxException {
        int result = cfgHandoverService.updateSelective(cfgHandover);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgHandoverService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgHandoverService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 获取移交配置
     *
     * @param cfgHandoverClientDto
     * @return
     */
    @PostMapping("/queryCfgHandover")
    protected CfgHandoverClientDto queryCfgHandover(@RequestBody CfgHandoverClientDto cfgHandoverClientDto) {
        log.info("客户移交配置信息【{}】", JSONObject.toJSON(cfgHandoverClientDto));
        return cfgHandoverService.queryCfgHandover(cfgHandoverClientDto);
    }
}
