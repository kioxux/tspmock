/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSorgFina
 * @类描述: cfg_sorg_fina数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 10:13:25
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_sorg_fina")
public class CfgSorgFina extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "pk_id")
    private String pkId;

    /**
     * 财务机构号
     **/
    @Column(name = "fina_br_no", unique = false, nullable = false, length = 40)
    @RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="finaBrNoName" )
    private String finaBrNo;

    /**
     * 管理机构号
     **/
    @Column(name = "manager_br_no", unique = false, nullable = true, length = 200)
    private String managerBrNo;

    /**
     * 财务类型
     **/
    @Column(name = "fina_type", unique = false, nullable = false, length = 40)
    private String finaType;

    /**
     * 财务机构名称
     **/
    @Column(name = "fina_br_name", unique = false, nullable = false, length = 40)
    private String finaBrName;

    public CfgSorgFina() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return finaBrNo
     */
    public String getFinaBrNo() {
        return this.finaBrNo;
    }

    /**
     * @param finaBrNo
     */
    public void setFinaBrNo(String finaBrNo) {
        this.finaBrNo = finaBrNo;
    }

    /**
     * @return managerBrNo
     */
    public String getManagerBrNo() {
        return this.managerBrNo;
    }

    /**
     * @param managerBrNo
     */
    public void setManagerBrNo(String managerBrNo) {
        this.managerBrNo = managerBrNo;
    }

    /**
     * @return finaType
     */
    public String getFinaType() {
        return this.finaType;
    }

    /**
     * @param finaType
     */
    public void setFinaType(String finaType) {
        this.finaType = finaType;
    }

    /**
     * @return finaBrName
     */
    public String getFinaBrName() {
        return this.finaBrName;
    }

    /**
     * @param finaBrName
     */
    public void setFinaBrName(String finaBrName) {
        this.finaBrName = finaBrName;
    }


}