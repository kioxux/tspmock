package cn.com.yusys.yusp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;

/**
 * @author guchuan
 * @version 1.0.0
 * @项目名称: UUID生成器
 * @类名称: UNIDProducer
 * @类描述:
 * @功能描述:
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class UNIDProducer {
    private final Logger log = LoggerFactory.getLogger(UNIDProducer.class);//定义log
    private SecureRandom seeder;

    public UNIDProducer() {
        this.seeder = new SecureRandom();
    }

    public String getUNID() {
        StringBuffer buf = new StringBuffer();
        long time = System.currentTimeMillis();
        int timeLow = (int) time & 0xFFFFFFFF;
        int node = this.seeder.nextInt();
        String midString = null;

        try {
            InetAddress inet = InetAddress.getLocalHost();
            byte[] bytes = inet.getAddress();
            String hexAddress = hexFormat(getInt(bytes), 8);
            String hash = hexFormat(System.identityHashCode(this), 8);
            midString = String.valueOf(hexAddress) + hash;
        } catch (UnknownHostException e) {
            log.error(e.getMessage(), e);
        }

        if (midString == null) {
            midString = "0000000000000000";
        }
        buf.append(midString).append(hexFormat(timeLow, 8)).append(hexFormat(node, 8));
        return buf.toString();
    }

    private String hexFormat(int number, int digits) {
        String hex = Integer.toHexString(number).toUpperCase();
        if (hex.length() >= digits) {
            return hex.substring(0, digits);
        }
        int padding = digits - hex.length();
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < padding; i++) {
            buf.append("0");
        }
        buf.append(hex);
        return buf.toString();
    }

    private int getInt(byte[] bytes) {
        int size = (bytes.length > 32) ? 32 : bytes.length;
        int result = 0;
        for (int i = size - 1; i >= 0; i--) {
            if (i == size - 1) {
                result += bytes[i];
            } else {
                result += (bytes[i] << 4 * (size - 1 - i));
            }
        }
        return result;
    }
}
