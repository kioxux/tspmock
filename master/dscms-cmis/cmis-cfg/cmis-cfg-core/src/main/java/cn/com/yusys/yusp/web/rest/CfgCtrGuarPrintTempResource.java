/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CfgCtrGuarPrintTemp;
import cn.com.yusys.yusp.service.CfgCtrGuarPrintTempService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgCtrGuarPrintTempResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 15:05:19
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "担保合同打印模板配置表")
@RequestMapping("/api/cfgctrguarprinttemp")
public class CfgCtrGuarPrintTempResource {
    @Autowired
    private CfgCtrGuarPrintTempService cfgCtrGuarPrintTempService;

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgCtrGuarPrintTemp> create(@RequestBody CfgCtrGuarPrintTemp cfgCtrGuarPrintTemp) throws URISyntaxException {
        cfgCtrGuarPrintTempService.insert(cfgCtrGuarPrintTemp);
        return new ResultDto<CfgCtrGuarPrintTemp>(cfgCtrGuarPrintTemp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgCtrGuarPrintTemp cfgCtrGuarPrintTemp) throws URISyntaxException {
        int result = cfgCtrGuarPrintTempService.update(cfgCtrGuarPrintTemp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgCtrGuarPrintTempService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgCtrGuarPrintTempService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:担保合同打印模板配置列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("担保合同打印模板配置列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<CfgCtrGuarPrintTemp>> toSignList(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CfgCtrGuarPrintTemp> list = cfgCtrGuarPrintTempService.toSignList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CfgCtrGuarPrintTemp>>(list);
    }

    /**
     * 担保合同打印模板配置逻辑删除
     *
     * @param cfgCtrGuarPrintTemp
     * @return
     */
    @ApiOperation("担保合同打印模板配置逻辑删除")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody CfgCtrGuarPrintTemp cfgCtrGuarPrintTemp) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        Integer result = cfgCtrGuarPrintTempService.logicDelete(cfgCtrGuarPrintTemp);
        if (result > 0) {
            resultDto.setCode(0);
            resultDto.setData(1);
            resultDto.setMessage("删除成功！");
        } else {
            resultDto.setCode(000);
            resultDto.setData(0);
            resultDto.setMessage("删除失败！");
        }
        return resultDto;
    }

    /**
     * @函数名称:queryCfgCtrGuarPrintTempDataByParams
     * @函数描述:通过入参查询，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过入参查询数据")
    @PostMapping("/querycfgctrguarprinttempdatabyparams")
    protected ResultDto<CfgCtrGuarPrintTemp> queryCfgCtrGuarPrintTempDataByParams(@RequestBody Map map) {
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        CfgCtrGuarPrintTemp cfgCtrGuarPrintTemp = cfgCtrGuarPrintTempService.queryCfgCtrGuarPrintTempDataByParams(map);
        return new ResultDto<CfgCtrGuarPrintTemp>(cfgCtrGuarPrintTemp);
    }

    /**
     * 担保合同打印模板配置操作
     *
     * @param cfgCtrCocntPrintTemp
     * @return
     */
    @ApiOperation("担保合同打印模板配置新增保存操作")
    @PostMapping("/savecfgctrguarprinttempinfo")
    public ResultDto<Map> saveCfgCtrGuarPrintTempInfo(@RequestBody CfgCtrGuarPrintTemp cfgCtrCocntPrintTemp) {
        Map result = cfgCtrGuarPrintTempService.saveCfgCtrGuarPrintTempInfo(cfgCtrCocntPrintTemp);
        return new ResultDto<>(result);
    }

    /**
     * 担保合同打印模板配置通用的保存方法
     *
     * @param params
     * @return
     */
    @ApiOperation("担保合同打印模板配置通用的保存方法")
    @PostMapping("/commonsavecfgctrguarprinttempinfo")
    public ResultDto<Map> commonSaveCfgCtrGuarPrintTempInfo(@RequestBody Map params) {
        Map rtnData = cfgCtrGuarPrintTempService.commonSaveCfgCtrGuarPrintTempInfo(params);
        return new ResultDto<>(rtnData);
    }
}

