/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CfgServiceExceptionDefEnums;
import cn.com.yusys.yusp.constant.QryFlexConstant;
import cn.com.yusys.yusp.constant.SQLKeyWordsConstant;
import cn.com.yusys.yusp.domain.CfgFlexQryParam;
import cn.com.yusys.yusp.dto.CfgFlexQryParamIndexDto;
import cn.com.yusys.yusp.repository.mapper.CfgFlexQryParamMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryParamService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2021-01-25 20:11:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgFlexQryParamService {

    @Autowired
    private CfgFlexQryParamMapper cfgFlexQryParamMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgFlexQryParam selectByPrimaryKey(String pkId) {
        return cfgFlexQryParamMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgFlexQryParam> selectAll(QueryModel model) {
        List<CfgFlexQryParam> records = (List<CfgFlexQryParam>) cfgFlexQryParamMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgFlexQryParam> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgFlexQryParam> list = cfgFlexQryParamMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgFlexQryParam record) {
        return cfgFlexQryParamMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgFlexQryParam record) {
        return cfgFlexQryParamMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgFlexQryParam record) {
        return cfgFlexQryParamMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgFlexQryParam record) {
        return cfgFlexQryParamMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgFlexQryParamMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteAllFlexQryParamByQryCode
     * @方法描述: 根据灵活查询流水号删除所有参数配置
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteAllFlexQryParamByQryCode(String qry_code) {
        return cfgFlexQryParamMapper.deleteAllFlexQryParamByQryCode(qry_code);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgFlexQryParamMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByQryCode
     * @方法描述: 根据查询报表编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgFlexQryParamIndexDto> selectCfgFlexQryParamIndexByQryCode(String qryCode) {
        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("qryCode", qryCode);
        return cfgFlexQryParamMapper.selectCfgFlexQryParamIndexByParams(queryMap);
    }

    /**
     * @方法名称: insertParam
     * @方法描述: 保存灵活配置的查询条件、展示字段等 子表数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insertParam(Map childData) {
        //获取配置的查询条件参数
        Map condMap = (Map) childData.get("cfg_flex_qry_cond");
        ArrayList<LinkedHashMap<String, String>> condTempMap = (ArrayList<LinkedHashMap<String, String>>) childData.get("cfg_flex_qry_cond_temp");
        //将map参数转换成list<domain>
        List<CfgFlexQryParam> list = mapToCfgFlexQryParam(condMap, QryFlexConstant.PARAM_TYPE_COND_COLUMN, condTempMap);

        //获取配置的展示列参数
        Map showMap = (Map) childData.get("cfg_flex_qry_show");
        ArrayList<LinkedHashMap<String, String>> showTempMap = (ArrayList<LinkedHashMap<String, String>>) childData.get("cfg_flex_qry_show_temp");
        //将map参数转换成list<domain>
        list.addAll(mapToCfgFlexQryParam(showMap, QryFlexConstant.PARAM_TYPE_SHOW_COLUMN, showTempMap));

        //获取配置的展示列参数
        Map groupByMap = (Map) childData.get("cfg_flex_group_by");
        ArrayList<LinkedHashMap<String, String>> groupByTempMap = (ArrayList<LinkedHashMap<String, String>>) childData.get("cfg_flex_group_by_temp");
        //将map参数转换成list<domain>
        list.addAll(mapToCfgFlexQryParam(groupByMap, QryFlexConstant.PARAM_TYPE_GROUP_BY_COLUMN, groupByTempMap));

        //获取配置的展示列参数
        Map groupShowMap = (Map) childData.get("cfg_flex_group_show");
        ArrayList<LinkedHashMap<String, String>> groupShowTempMap = (ArrayList<LinkedHashMap<String, String>>) childData.get("cfg_flex_group_show_temp");
        //将map参数转换成list<domain>
        list.addAll(mapToCfgFlexQryParam(groupShowMap, QryFlexConstant.PARAM_TYPE_GROUP_SHOW_COLUMN, groupShowTempMap));


        //获取 查询报表编号
        String qry_code = childData.get("qryCode") == null ? "" : childData.get("qryCode").toString();

        //先将该查询报表配置下的配置删除，然后重新保存修改后子表配置
        int result = deleteAllFlexQryParamByQryCode(qry_code);
        for (CfgFlexQryParam temp : list) {
            //将主表中的查询报表编号联动保存至子表
            temp.setQryCode(qry_code);
            result = insert(temp);
        }
        return result;

    }

    /**
     * @方法名称: mapToCfgFlexQryParam
     * @方法描述: 将前台配置的展示字段配置转换成domain
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgFlexQryParam> mapToCfgFlexQryParam(Map<String, String> paramMap, String paramType, ArrayList<LinkedHashMap<String, String>> tempMap) {
        List<CfgFlexQryParam> list = new ArrayList<CfgFlexQryParam>();
        //报错字段名称信息存储
        String errorName = "";
        //遍历map，取出key和value
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            Optional<LinkedHashMap<String, String>> result = tempMap.stream().filter(stringStringLinkedHashMap -> entry.getKey().equals(stringStringLinkedHashMap.get("itemkey"))).findFirst();
            String itemName = "";
            Integer showOrder = 0;
            if (result.isPresent()) {
                itemName = result.get().get("itemname");
                showOrder = Integer.valueOf(result.get().get("showorder"));
            }
            CfgFlexQryParam temp = new CfgFlexQryParam();
            String pk_id = StringUtils.uuid(true);
            //获取key
            String keyStr = entry.getKey();

            //获取value
            String valueStr = entry.getValue();
            //如果获取的value为空，则抛出异常提醒到前台
            if (valueStr == null || valueStr.isEmpty()) {
                errorName += "[" + itemName + "]";
            }

            temp.setPkId(pk_id);
            temp.setParamKey(keyStr);
            temp.setParamValue(valueStr);
            temp.setParamType(paramType);
            temp.setParamName(itemName);
            temp.setShowOrder(showOrder);
            list.add(temp);
        }

        //校验如果参数值为空，则抛出异常
        if (!errorName.isEmpty()) {
            throw new YuspException(CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_PARAM_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFGDATAFLOWOUT_PARAM_EXCEPTION.getExceptionDesc() + errorName + "的值不能为空！");
        }

        return list;
    }


    /**
     * @方法名称: generateColumnsShow
     * @方法描述: 通过灵活查询流水号生成显示字段SQL
     * @参数与返回说明: 查询到paramKey以'__show_index_code'结尾的数据，将 indexCode与colNameEn用.拼接，并按照,进行分隔处理为字符串
     * @算法描述: 无
     */
    public StringBuffer generateColumnsShow(String qryCode) {
        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("qryCode", qryCode);
        queryMap.put("paramKey", SQLKeyWordsConstant.TRANSFER_CHARACTER + QryFlexConstant.PARAM_KEY_SHOW_COLUMN);
        List<CfgFlexQryParamIndexDto> cfgFlexQryParamIndexDtoList = cfgFlexQryParamMapper.selectCfgFlexQryParamIndexByParams(queryMap);
        //如果查询出的数据为空，则抛出异常
        if (cfgFlexQryParamIndexDtoList == null || cfgFlexQryParamIndexDtoList.isEmpty()) {
            throw new YuspException(CfgServiceExceptionDefEnums.E_DEF_QUERY_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_DEF_QUERY_EXCEPTION.getExceptionDesc() + "未配置显示列！");
        }
        String columnsSql = cfgFlexQryParamIndexDtoList.stream()
                .map(cfgFlexQryParamIndexDto ->
                        cfgFlexQryParamIndexDto.getIndexCode()
                                .concat(SQLKeyWordsConstant.DOT)
                                .concat(cfgFlexQryParamIndexDto.getColNameEn())
                )
                .collect(Collectors.joining(SQLKeyWordsConstant.COMMA));
        return new StringBuffer(columnsSql);
    }

    /**
     * @方法名称: generateColumnsCond
     * @方法描述: 通过灵活查询流水号生成查询条件SQL
     * @参数与返回说明: 查询所有类型为条件字段的参数条件，将paramkey按排序拼接，除了__cond_title结尾的不需要， __cond_index_code拼接时候加上colnameen
     * @算法描述: 无
     */
    public StringBuffer generateColumnsCond(String qryCode) {
        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("qryCode", qryCode);
        queryMap.put("paramType", QryFlexConstant.PARAM_TYPE_COND_COLUMN);
        List<CfgFlexQryParamIndexDto> cfgFlexQryParamIndexDtoList = cfgFlexQryParamMapper.selectCfgFlexQryParamIndexByParams(queryMap);
        //如果查询出的数据为空，则抛出异常
        if (cfgFlexQryParamIndexDtoList == null || cfgFlexQryParamIndexDtoList.isEmpty()) {
            throw new YuspException(CfgServiceExceptionDefEnums.E_DEF_QUERY_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_DEF_QUERY_EXCEPTION.getExceptionDesc() + "未配置查询条件！");
        }
        StringBuffer columnsSql = new StringBuffer();
        // 上一个操作符
        String lastOperation = "";
        for (CfgFlexQryParamIndexDto cfgFlexQryParamIndexDto : cfgFlexQryParamIndexDtoList) {
            if (cfgFlexQryParamIndexDto.getParamKey().endsWith(QryFlexConstant.PARAM_KEY_COND_COLUMN)) {
                //__cond_index_code 结尾的参数
                columnsSql
                        .append(cfgFlexQryParamIndexDto.getParamValue())
                        .append(SQLKeyWordsConstant.DOT)
                        .append(cfgFlexQryParamIndexDto.getColNameEn())
                        .append(SQLKeyWordsConstant.SPACE);
                lastOperation = cfgFlexQryParamIndexDto.getParamValue();
                continue;
            }
            if (cfgFlexQryParamIndexDto.getParamKey().endsWith(QryFlexConstant.PARAM_KEY_COND_TITLE)) {
                // 不做处理
                continue;
            }
            if (cfgFlexQryParamIndexDto.getParamKey().endsWith(QryFlexConstant.PARAM_KEY_COND_VALUE)) {
                columnsSql.append(getSqlCondValueByLastOpr(cfgFlexQryParamIndexDto, lastOperation));
                continue;
            }
            // 默认处理
            columnsSql
                    .append(cfgFlexQryParamIndexDto.getParamValue())
                    .append(SQLKeyWordsConstant.SPACE);
            lastOperation = cfgFlexQryParamIndexDto.getParamValue();

        }
        return columnsSql;
    }

    /**
     * @方法名称: generateColumnsOrderBy
     * @方法描述: 通过灵活查询流水号生成排序条件SQL
     * @参数与返回说明:查询到paramKey以'__show_order_by'结尾的数据，将 indexCode、colNameEn与paramvalue按照sql写法拼接,进行分隔处理为字符串
     * @算法描述: 无
     */
    public StringBuffer generateColumnsOrderBy(String qryCode) {
        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("qryCode", qryCode);
        queryMap.put("paramKey", SQLKeyWordsConstant.TRANSFER_CHARACTER + QryFlexConstant.PARAM_KEY_SHOW_ORDER_BY);
        List<CfgFlexQryParamIndexDto> cfgFlexQryParamIndexDtoList = cfgFlexQryParamMapper.selectCfgFlexQryParamIndexByParams(queryMap);
        String columnsSql = cfgFlexQryParamIndexDtoList.stream()
                .filter(cfgFlexQryParamIndexDto -> !QryFlexConstant.ORDER_BY_TYPE_NO.equals(cfgFlexQryParamIndexDto.getParamValue()))
                .map(cfgFlexQryParamIndexDto ->
                        cfgFlexQryParamIndexDto.getIndexCode()
                                .concat(SQLKeyWordsConstant.DOT)
                                .concat(cfgFlexQryParamIndexDto.getColNameEn())
                                .concat(SQLKeyWordsConstant.SPACE)
                                .concat(cfgFlexQryParamIndexDto.getParamValue())
                                .concat(SQLKeyWordsConstant.SPACE)
                )
                .collect(Collectors.joining(SQLKeyWordsConstant.COMMA));
        return new StringBuffer(columnsSql);
    }

    /**
     * @方法名称: queryShowColumnsTempByQryCode
     * @方法描述: 通过灵活查询流水号查询列表显示字段的模板
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgFlexQryParamIndexDto> queryListShowColumnsTempByQryCode(String qryCode) {

        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("qryCode", qryCode);
        queryMap.put("paramKey", SQLKeyWordsConstant.TRANSFER_CHARACTER + QryFlexConstant.PARAM_KEY_SHOW_SHOW_TITLE);
        return cfgFlexQryParamMapper.selectCfgFlexQryParamIndexByParams(queryMap);
    }


    /**
     * @方法名称: queryShowColumnsTempByQryCode
     * @方法描述: 通过灵活查询流水号查询汇总显示字段的模板, 需要替换一下__group_show_name 的 col_name_en 为 col_name_en + "_" +group_show_type 的形式
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgFlexQryParamIndexDto> queryGroupShowColumnsTempByQryCode(String qryCode) {
        List<CfgFlexQryParamIndexDto> returnCfgFlexQryParamIndexDtoList = new ArrayList<CfgFlexQryParamIndexDto>();
        HashMap showTypeQueryMap = new HashMap<String, String>();
        showTypeQueryMap.put("qryCode", qryCode);
        showTypeQueryMap.put("paramKey", QryFlexConstant.PARAM_KEY_GROUP_SHOW_TYPE);
        List<CfgFlexQryParamIndexDto> showTypeCfgFlexQryParamIndexDtoList = cfgFlexQryParamMapper.selectCfgFlexQryParamIndexByParams(showTypeQueryMap);
        HashMap showNameQueryMap = new HashMap<String, String>();
        showNameQueryMap.put("qryCode", qryCode);
        showNameQueryMap.put("paramKey", QryFlexConstant.PARAM_KEY_GROUP_SHOW_NAME);
        List<CfgFlexQryParamIndexDto> showNameCfgFlexQryParamIndexDtoList = cfgFlexQryParamMapper.selectCfgFlexQryParamIndexByParams(showNameQueryMap);
        for (CfgFlexQryParamIndexDto showTypeCfgFlexQryParamIndexDto : showTypeCfgFlexQryParamIndexDtoList) {
            for (CfgFlexQryParamIndexDto showNameCfgFlexQryParamIndexDto : showNameCfgFlexQryParamIndexDtoList) {
                if (showTypeCfgFlexQryParamIndexDto.getColNameEn().equals(showNameCfgFlexQryParamIndexDto.getColNameEn())) {
                    showNameCfgFlexQryParamIndexDto.setColNameEn(showTypeCfgFlexQryParamIndexDto.getColNameEn()
                            .concat(SQLKeyWordsConstant.UNDERLINE).concat(showTypeCfgFlexQryParamIndexDto.getParamValue()));
                    returnCfgFlexQryParamIndexDtoList.add(showNameCfgFlexQryParamIndexDto);
                }
            }
        }
        return returnCfgFlexQryParamIndexDtoList;
    }

    /**
     * @方法名称: getSqlCondValueByLastOpr
     * @方法描述: 特殊运算符的处理，考虑需要抽象化操作将来支持更多语法功能
     * @参数与返回说明:
     * @算法描述: 无
     */
    public StringBuffer getSqlCondValueByLastOpr(CfgFlexQryParamIndexDto cfgFlexQryParamIndexDto, String lastOperation) {
        StringBuffer columnsSql = new StringBuffer();
        switch (StringUtils.upperCase(lastOperation)) {
            case SQLKeyWordsConstant.COND_OPERATION_IN:
                columnsSql
                        .append(SQLKeyWordsConstant.OPEN_PAREN)
                        .append(Arrays.stream(cfgFlexQryParamIndexDto.getParamValue().split(SQLKeyWordsConstant.COMMA))
                                .map(s -> SQLKeyWordsConstant.SINGLE_QUOTES + s + SQLKeyWordsConstant.SINGLE_QUOTES)
                                .collect(Collectors.joining(SQLKeyWordsConstant.COMMA)))
                        .append(SQLKeyWordsConstant.CLOSE_PAREN)
                        .append(SQLKeyWordsConstant.SPACE);
                break;
            case SQLKeyWordsConstant.COND_OPERATION_LIKE:
                columnsSql
                        .append(SQLKeyWordsConstant.SINGLE_QUOTES)
                        .append(SQLKeyWordsConstant.PERCENT)
                        .append(cfgFlexQryParamIndexDto.getParamValue())
                        .append(SQLKeyWordsConstant.PERCENT)
                        .append(SQLKeyWordsConstant.SINGLE_QUOTES)
                        .append(SQLKeyWordsConstant.SPACE);
                break;
            default:
                columnsSql
                        .append(SQLKeyWordsConstant.SINGLE_QUOTES)
                        .append(cfgFlexQryParamIndexDto.getParamValue())
                        .append(SQLKeyWordsConstant.SINGLE_QUOTES)
                        .append(SQLKeyWordsConstant.SPACE);
                break;
        }
        // 如果是数字的话，需要将 ' 替换掉
        if (cfgFlexQryParamIndexDto.getItemtype().equals(QryFlexConstant.ITEM_TYPE_DIGIT)) {
            new StringBuffer(columnsSql.toString().replaceAll(SQLKeyWordsConstant.SINGLE_QUOTES, ""));
        }
        return columnsSql;
    }

    /**
     * @方法名称: generateColumnsGroupShow
     * @方法描述: 通过灵活查询流水号生成汇总分组语句的显示字段sql部分
     * @参数与返回说明:查询到paramKey以'__group_show'结尾的数据，拼接为聚合函数显示列
     * @算法描述: 无
     */
    public StringBuffer generateColumnsGroupShow(String qryCode) {
        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("qryCode", qryCode);
        queryMap.put("paramType", QryFlexConstant.PARAM_TYPE_GROUP_SHOW_COLUMN);
        List<CfgFlexQryParamIndexDto> cfgFlexQryParamIndexDtoList = cfgFlexQryParamMapper.selectCfgFlexQryParamIndexByParams(queryMap);
        //如果查询出的数据为空，则抛出异常
        if (cfgFlexQryParamIndexDtoList == null || cfgFlexQryParamIndexDtoList.isEmpty()) {
            throw new YuspException(CfgServiceExceptionDefEnums.E_DEF_QUERY_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_DEF_QUERY_EXCEPTION.getExceptionDesc() + "未配置汇总字段！");
        }
        String columnsSql = cfgFlexQryParamIndexDtoList.stream()
                .filter(cfgFlexQryParamIndexDto -> cfgFlexQryParamIndexDto.getParamKey().endsWith(QryFlexConstant.PARAM_KEY_GROUP_SHOW_TYPE))
                .map(cfgFlexQryParamIndexDto -> cfgFlexQryParamIndexDto.getParamValue()
                        .concat(SQLKeyWordsConstant.OPEN_PAREN)
                        .concat(cfgFlexQryParamIndexDto.getIndexCode()).concat(SQLKeyWordsConstant.DOT).concat(cfgFlexQryParamIndexDto.getColNameEn())
                        .concat(SQLKeyWordsConstant.CLOSE_PAREN)
                        .concat(SQLKeyWordsConstant.SPACE)
                        .concat(SQLKeyWordsConstant.AS)
                        .concat(cfgFlexQryParamIndexDto.getColNameEn()).concat(SQLKeyWordsConstant.UNDERLINE).concat(cfgFlexQryParamIndexDto.getParamValue())
                ).collect(Collectors.joining(SQLKeyWordsConstant.COMMA));
        return new StringBuffer(columnsSql);
    }


    /**
     * @方法名称: generateColumnsGroupShow
     * @方法描述: 通过灵活查询流水号生成汇总分组语句的汇总字段SQL
     * @参数与返回说明:查询到paramKey以'__group_by'结尾的数据，拼接为聚合函数显示列
     * @算法描述: 无
     */
    public StringBuffer generateColumnsGroupBy(String qryCode) {
        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("qryCode", qryCode);
        queryMap.put("paramKey", QryFlexConstant.PARAM_KEY_GROUP_BY_INDEX_CODE);
        List<CfgFlexQryParamIndexDto> cfgFlexQryParamIndexDtoList = cfgFlexQryParamMapper.selectCfgFlexQryParamIndexByParams(queryMap);
        //如果查询出的数据为空，则抛出异常
        if (cfgFlexQryParamIndexDtoList == null || cfgFlexQryParamIndexDtoList.isEmpty()) {
            throw new YuspException(CfgServiceExceptionDefEnums.E_DEF_QUERY_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_DEF_QUERY_EXCEPTION.getExceptionDesc() + "未配置分组字段！");
        }
        String columnsSql = cfgFlexQryParamIndexDtoList.stream()
                .map(cfgFlexQryParamIndexDto -> cfgFlexQryParamIndexDto.getParamValue()
                        //.concat(cfgFlexQryParamIndexDto.getIndexCode())
                        .concat(SQLKeyWordsConstant.DOT)
                        .concat(cfgFlexQryParamIndexDto.getColNameEn())
                        .concat(SQLKeyWordsConstant.SPACE)
                ).collect(Collectors.joining(SQLKeyWordsConstant.COMMA));
        return new StringBuffer(columnsSql);
    }
}
