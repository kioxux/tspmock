package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryDetail
 * @类描述: cfg_flex_qry_detail数据实体类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-29 21:34:31
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgFlexQryDetailDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 指标配置编码
     **/
    private String indexCode;

    /**
     * 指标英文
     **/
    private String colNameEn;

    /**
     * 指标名称
     **/
    private String colNameCn;

    /**
     * 数据源
     **/
    private String datasource;

    /**
     * 来源表
     **/
    private String tableSour;

    /**
     * 指标长度
     **/
    private String colSize;

    /**
     * 指标类型
     **/
    private String colType;

    /**
     * 是否为空
     **/
    private String isNull;

    /**
     * 是否主键
     **/
    private String primarykeyFlag;

    /**
     * 数据字典类型
     **/
    private String notes;

    /**
     * 是否可用 STD_ZB_YES_NO
     **/
    private String isEnable;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 最后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    private String oprType;

    /**
     * 控件类型
     **/
    private String itemtype;

    /**
     * 控件定义
     **/
    private String itemdefine;

    /**
     * @return PkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    /**
     * @return IndexCode
     */
    public String getIndexCode() {
        return this.indexCode;
    }

    /**
     * @param indexCode
     */
    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode == null ? null : indexCode.trim();
    }

    /**
     * @return ColNameEn
     */
    public String getColNameEn() {
        return this.colNameEn;
    }

    /**
     * @param colNameEn
     */
    public void setColNameEn(String colNameEn) {
        this.colNameEn = colNameEn == null ? null : colNameEn.trim();
    }

    /**
     * @return ColNameCn
     */
    public String getColNameCn() {
        return this.colNameCn;
    }

    /**
     * @param colNameCn
     */
    public void setColNameCn(String colNameCn) {
        this.colNameCn = colNameCn == null ? null : colNameCn.trim();
    }

    /**
     * @return Datasource
     */
    public String getDatasource() {
        return this.datasource;
    }

    /**
     * @param datasource
     */
    public void setDatasource(String datasource) {
        this.datasource = datasource == null ? null : datasource.trim();
    }

    /**
     * @return TableSour
     */
    public String getTableSour() {
        return this.tableSour;
    }

    /**
     * @param tableSour
     */
    public void setTableSour(String tableSour) {
        this.tableSour = tableSour == null ? null : tableSour.trim();
    }

    /**
     * @return ColSize
     */
    public String getColSize() {
        return this.colSize;
    }

    /**
     * @param colSize
     */
    public void setColSize(String colSize) {
        this.colSize = colSize == null ? null : colSize.trim();
    }

    /**
     * @return ColType
     */
    public String getColType() {
        return this.colType;
    }

    /**
     * @param colType
     */
    public void setColType(String colType) {
        this.colType = colType == null ? null : colType.trim();
    }

    /**
     * @return IsNull
     */
    public String getIsNull() {
        return this.isNull;
    }

    /**
     * @param isNull
     */
    public void setIsNull(String isNull) {
        this.isNull = isNull == null ? null : isNull.trim();
    }

    /**
     * @return PrimarykeyFlag
     */
    public String getPrimarykeyFlag() {
        return this.primarykeyFlag;
    }

    /**
     * @param primarykeyFlag
     */
    public void setPrimarykeyFlag(String primarykeyFlag) {
        this.primarykeyFlag = primarykeyFlag == null ? null : primarykeyFlag.trim();
    }

    /**
     * @return Notes
     */
    public String getNotes() {
        return this.notes;
    }

    /**
     * @param notes
     */
    public void setNotes(String notes) {
        this.notes = notes == null ? null : notes.trim();
    }

    /**
     * @return IsEnable
     */
    public String getIsEnable() {
        return this.isEnable;
    }

    /**
     * @param isEnable
     */
    public void setIsEnable(String isEnable) {
        this.isEnable = isEnable == null ? null : isEnable.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }

    /**
     * @return Itemtype
     */
    public String getItemtype() {
        return this.itemtype;
    }

    /**
     * @param itemtype
     */
    public void setItemtype(String itemtype) {
        this.itemtype = itemtype == null ? null : itemtype.trim();
    }

    /**
     * @return Itemdefine
     */
    public String getItemdefine() {
        return this.itemdefine;
    }

    /**
     * @param itemdefine
     */
    public void setItemdefine(String itemdefine) {
        this.itemdefine = itemdefine == null ? null : itemdefine.trim();
    }


}