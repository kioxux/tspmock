/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgAccountClassChoose;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgAccountClassChooseMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 16:37:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CfgAccountClassChooseMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CfgAccountClassChoose selectByPrimaryKey(@Param("seqNo") String seqNo);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgAccountClassChoose> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CfgAccountClassChoose record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CfgAccountClassChoose record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CfgAccountClassChoose record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CfgAccountClassChoose record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("seqNo") String seqNo);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: queryHxAccountClassByProps
     * @方法描述: 通过属性查询对应的核心科目号
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 马顺
     * @创建时间: 2021-06-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<CfgAccountClassChoose> queryHxAccountClassByProps(HashMap<String, String> paramsMap);

    /**
     * @方法名称: queryHxAccountClassByAcccountClass
     * @方法描述: 通过科目号查询对应的核心会计类别
     * @参数与返回说明:
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-06-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<CfgAccountClassChoose> queryHxAccountClassByAcccountClass(@Param("accountClass") String accountClass);
}