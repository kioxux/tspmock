/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.workbench.domain.WbPrbComm;
import cn.com.yusys.yusp.workbench.service.WbPrbCommService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbPrbCommResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 21:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/wbprbcomm")
public class WbPrbCommResource {
    @Autowired
    private WbPrbCommService wbPrbCommService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<WbPrbComm>> query() {
        QueryModel queryModel = new QueryModel();
        List<WbPrbComm> list = wbPrbCommService.selectAll(queryModel);
        return new ResultDto<List<WbPrbComm>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<WbPrbComm>> index(@RequestBody QueryModel queryModel) {
        List<WbPrbComm> list = wbPrbCommService.selectByModel(queryModel);
        return new ResultDto<List<WbPrbComm>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<WbPrbComm> show(@PathVariable("serno") String serno) {
        WbPrbComm wbPrbComm = wbPrbCommService.selectByPrimaryKey(serno);
        return new ResultDto<WbPrbComm>(wbPrbComm);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<WbPrbComm> create(@RequestBody WbPrbComm wbPrbComm) throws URISyntaxException {
        wbPrbCommService.insertSelective(wbPrbComm);
        return new ResultDto<WbPrbComm>(wbPrbComm);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody WbPrbComm wbPrbComm) throws URISyntaxException {
        int result = wbPrbCommService.updateSelective(wbPrbComm);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = wbPrbCommService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = wbPrbCommService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

}
