/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CfgServiceExceptionDefEnums;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.constant.SQLKeyWordsConstant;
import cn.com.yusys.yusp.domain.CfgFlexQryIndRel;
import cn.com.yusys.yusp.domain.CfgFlexQryIndex;
import cn.com.yusys.yusp.repository.mapper.CfgFlexQryIndRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryIndRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-30 17:23:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgFlexQryIndRelService {

    @Autowired
    private CfgFlexQryIndRelMapper cfgFlexQryIndRelMapper;
    @Autowired
    private CfgFlexQryIndexService cfgFlexQryIndexService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgFlexQryIndRel selectByPrimaryKey(String pkId) {
        return cfgFlexQryIndRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: queryByIndexCode
     * @方法描述: 根据左关联及者右关联指标配置编码查询关联关系
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CfgFlexQryIndRel> queryByIndexCode(CfgFlexQryIndRel cfgFlexQryIndRel) {
        return cfgFlexQryIndRelMapper.queryByIndexCode(cfgFlexQryIndRel);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgFlexQryIndRel> selectAll(QueryModel model) {
        List<CfgFlexQryIndRel> records = (List<CfgFlexQryIndRel>) cfgFlexQryIndRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgFlexQryIndRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgFlexQryIndRel> list = cfgFlexQryIndRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgFlexQryIndRel record) {
        return cfgFlexQryIndRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgFlexQryIndRel record) {
        return cfgFlexQryIndRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgFlexQryIndRel record) {
        return cfgFlexQryIndRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgFlexQryIndRel record) {
        return cfgFlexQryIndRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgFlexQryIndRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgFlexQryIndRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: generateColumnsShow
     * @方法描述: 通过灵活查询流水号生成显示字段SQL
     * @参数与返回说明: 查询到paramKey以'__show_index_code'结尾的数据，将 indexCode与colNameEn用.拼接，并按照,进行分隔处理为字符串
     * @算法描述: 无
     */
    public StringBuffer generateTablesRel(String qryCode) {
        StringBuffer tableRelSql = new StringBuffer();

        //查询配置的参数显示列中包含几个index_code(即配置的指标参数)
        List<CfgFlexQryIndex> cfgFlexQryIndexList = cfgFlexQryIndexService.selectCfgFlexQryIndexByQryCode(qryCode);
        //若只有一个指标，则只有一张表，无需查询指标关系配置表，直接取当前table
        if (cfgFlexQryIndexList != null && cfgFlexQryIndexList.size() == 1) {
            tableRelSql
                    .append(cfgFlexQryIndexList.get(0).getTableSour()).append(SQLKeyWordsConstant.SPACE)
                    .append(cfgFlexQryIndexList.get(0).getIndexCode()).append(SQLKeyWordsConstant.SPACE);
        } else {
            //否则查询指标关系配置表，拼接table
            List<CfgFlexQryIndRel> cfgFlexQryIndRelList = cfgFlexQryIndRelMapper.queryCfgFlexQryIndRelByQryCode(qryCode, CommonConstant.ADD_OPR);
            //如果查询出的关联关系表为空，则抛出异常
            if (cfgFlexQryIndRelList == null || cfgFlexQryIndRelList.isEmpty()) {
                throw new YuspException(CfgServiceExceptionDefEnums.E_DEF_QUERY_EXCEPTION.getExceptionCode(), CfgServiceExceptionDefEnums.E_DEF_QUERY_EXCEPTION.getExceptionDesc() + "关联灵活查询指标关系配置表未查询到有效数据！");
            }

            //不为空，循环解析拼接
            for (CfgFlexQryIndRel cfgFlexQryIndRel : cfgFlexQryIndRelList) {
                // 左表的表所有列中，所以做一次判断，只加载一次都有
                if (tableRelSql.indexOf(cfgFlexQryIndRel.getLeftTableName()) < 0) {
                    tableRelSql
                            .append(cfgFlexQryIndRel.getLeftTableName()).append(SQLKeyWordsConstant.SPACE)
                            .append(cfgFlexQryIndRel.getLeftIndexCode()).append(SQLKeyWordsConstant.SPACE)
                            .append(cfgFlexQryIndRel.getJoinWay()).append(SQLKeyWordsConstant.SPACE)
                            .append(cfgFlexQryIndRel.getRightTableName()).append(SQLKeyWordsConstant.SPACE)
                            .append(cfgFlexQryIndRel.getRightIndexCode()).append(SQLKeyWordsConstant.SPACE)
                            .append(SQLKeyWordsConstant.ON).append(SQLKeyWordsConstant.SPACE)
                            .append(cfgFlexQryIndRel.getLeftIndexCode()).append(SQLKeyWordsConstant.DOT).append(cfgFlexQryIndRel.getLeftColName())
                            .append(SQLKeyWordsConstant.EQUALS)
                            .append(cfgFlexQryIndRel.getRightIndexCode()).append(SQLKeyWordsConstant.DOT).append(cfgFlexQryIndRel.getRightColName())
                            .append(SQLKeyWordsConstant.SPACE);
                } else {
                    tableRelSql
                            .append(cfgFlexQryIndRel.getJoinWay()).append(SQLKeyWordsConstant.SPACE)
                            .append(cfgFlexQryIndRel.getRightTableName()).append(SQLKeyWordsConstant.SPACE)
                            .append(cfgFlexQryIndRel.getRightIndexCode()).append(SQLKeyWordsConstant.SPACE)
                            .append(SQLKeyWordsConstant.ON).append(SQLKeyWordsConstant.SPACE)
                            .append(cfgFlexQryIndRel.getLeftIndexCode()).append(SQLKeyWordsConstant.DOT).append(cfgFlexQryIndRel.getLeftColName())
                            .append(SQLKeyWordsConstant.EQUALS)
                            .append(cfgFlexQryIndRel.getRightIndexCode()).append(SQLKeyWordsConstant.DOT).append(cfgFlexQryIndRel.getRightColName())
                            .append(SQLKeyWordsConstant.SPACE);
                }
            }
        }

        return tableRelSql;
    }

}
