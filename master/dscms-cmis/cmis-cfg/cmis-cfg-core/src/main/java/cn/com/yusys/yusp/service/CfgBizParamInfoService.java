/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CfgServiceExceptionDefEnums;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgBizParamInfo;
import cn.com.yusys.yusp.dto.CfgBizParamInfoClientDto;
import cn.com.yusys.yusp.dto.CfgClientParamDto;
import cn.com.yusys.yusp.repository.mapper.CfgBizParamInfoMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBizParamInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2021-02-04 20:27:29
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgBizParamInfoService {

    private static final Logger log = LoggerFactory.getLogger(CfgBizParamInfoService.class);
    @Autowired
    private CfgBizParamInfoMapper cfgBizParamInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgBizParamInfo selectByPrimaryKey(String pkId) {
        return cfgBizParamInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgBizParamInfo> selectAll(QueryModel model) {
        List<CfgBizParamInfo> records = (List<CfgBizParamInfo>) cfgBizParamInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgBizParamInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgBizParamInfo> list = cfgBizParamInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgBizParamInfo record) {
        return cfgBizParamInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgBizParamInfo record) {
        return cfgBizParamInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgBizParamInfo record) {
        return cfgBizParamInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgBizParamInfo record) {
        return cfgBizParamInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgBizParamInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgBizParamInfoMapper.deleteByIds(ids);
    }

    /**
     * cfg服务对外提供参数查询接口
     *
     * @param cfgClientParamDto
     * @return
     */
    public CfgClientParamDto getCfgBizParamInfoByClient(CfgClientParamDto cfgClientParamDto) {
        String rtnCode = CfgServiceExceptionDefEnums.E_DEF_QUERY_SUCCESS.getExceptionCode();
        String rtnMsg = CfgServiceExceptionDefEnums.E_DEF_QUERY_SUCCESS.getExceptionDesc();
        try {
            log.info("获取入参信息");
            if (cfgClientParamDto == null) {
                rtnCode = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_PARAMSNULL_EXCEPTION.getExceptionCode();
                rtnMsg = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_PARAMSNULL_EXCEPTION.getExceptionDesc();
                cfgClientParamDto = new CfgClientParamDto();
                return cfgClientParamDto;
            }

            CfgBizParamInfoClientDto cfgBizParamInfoClientDto = cfgClientParamDto.getCfgBizParamInfoClientDto();
            if (cfgBizParamInfoClientDto == null) {
                rtnCode = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_PARAMSNULL_EXCEPTION.getExceptionCode();
                rtnMsg = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_PARAMSNULL_EXCEPTION.getExceptionDesc();
                return cfgClientParamDto;
            }

            String bizParamType = cfgBizParamInfoClientDto.getBizParamType();
            if (StringUtils.isBlank(bizParamType)) {
                rtnCode = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_PARAMSNULL_EXCEPTION.getExceptionCode();
                rtnMsg = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_PARAMSNULL_EXCEPTION.getExceptionDesc();
                return cfgClientParamDto;
            }

            log.info("获取参数类型为【" + bizParamType + "】的参数配置");
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("bizParamType", bizParamType);
            queryModel.addCondition("oprType", CommonConstant.ADD_OPR);
            List<CfgBizParamInfo> cfgBizParamInfoList = cfgBizParamInfoMapper.selectByModel(queryModel);
            if (CollectionUtils.isEmpty(cfgBizParamInfoList)) {
                rtnCode = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_PARAMINFONULL_EXCEPTION.getExceptionCode();
                rtnMsg = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_PARAMINFONULL_EXCEPTION.getExceptionDesc();
                return cfgClientParamDto;
            } else if (cfgBizParamInfoList.size() > 1) {
                rtnCode = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_PARAMSMORE_EXCEPTION.getExceptionCode();
                rtnMsg = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_PARAMSMORE_EXCEPTION.getExceptionDesc();
                return cfgClientParamDto;
            }

            CfgBizParamInfo cfgBizParamInfo = cfgBizParamInfoList.get(0);
            log.info("参数转化");
            BeanUtils.copyProperties(cfgBizParamInfo, cfgBizParamInfoClientDto);
            cfgClientParamDto.setCfgBizParamInfoClientDto(cfgBizParamInfoClientDto);
        } catch (YuspException e) {
            log.error("获取参数配置异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取参数配置异常！", e);
            rtnCode = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_DEF_EXCEPTION.getExceptionCode();
            rtnMsg = CfgServiceExceptionDefEnums.E_CFGPARAMINFO_DEF_EXCEPTION.getExceptionDesc() + e.getMessage();
        } finally {
            if (cfgClientParamDto == null) {
                cfgClientParamDto = new CfgClientParamDto();
            }
            cfgClientParamDto.setRtnCode(rtnCode);
            cfgClientParamDto.setRtnMsg(rtnMsg);
        }
        return cfgClientParamDto;
    }
}
