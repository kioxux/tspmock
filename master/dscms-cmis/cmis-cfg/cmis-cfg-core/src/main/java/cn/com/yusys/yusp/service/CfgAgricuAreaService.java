/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgAgricuArea;
import cn.com.yusys.yusp.repository.mapper.CfgAgricuAreaMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgAgricuAreaService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-06-15 22:26:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgAgricuAreaService {

    @Autowired
    private CfgAgricuAreaMapper cfgAgricuAreaMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgAgricuArea selectByPrimaryKey(String areaNo) {
        return cfgAgricuAreaMapper.selectByPrimaryKey(areaNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgAgricuArea> selectAll(QueryModel model) {
        List<CfgAgricuArea> records = (List<CfgAgricuArea>) cfgAgricuAreaMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgAgricuArea> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgAgricuArea> list = cfgAgricuAreaMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgAgricuArea record) {
        return cfgAgricuAreaMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgAgricuArea record) {
        return cfgAgricuAreaMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgAgricuArea record) {
        return cfgAgricuAreaMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgAgricuArea record) {
        return cfgAgricuAreaMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String areaNo) {
        return cfgAgricuAreaMapper.deleteByPrimaryKey(areaNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgAgricuAreaMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryAreaNum
     * @方法描述: 查询地区数量
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int queryAreaNum(String areaCode, String agriFlg) {
        return cfgAgricuAreaMapper.queryAreaNum(areaCode, agriFlg);
    }
}
