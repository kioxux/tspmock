/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.workbench.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.workbench.domain.WbCommFunc;
import cn.com.yusys.yusp.workbench.service.WbCommFuncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: WbCommFuncResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-07 21:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/wbcommfunc")
public class WbCommFuncResource {
    @Autowired
    private WbCommFuncService wbCommFuncService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<WbCommFunc>> query() {
        QueryModel queryModel = new QueryModel();
        List<WbCommFunc> list = wbCommFuncService.selectAll(queryModel);
        return new ResultDto<List<WbCommFunc>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<WbCommFunc>> index(@RequestBody QueryModel queryModel) {
        List<WbCommFunc> list = wbCommFuncService.selectByModel(queryModel);
        return new ResultDto<List<WbCommFunc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<WbCommFunc> show(@PathVariable("serno") String serno) {
        WbCommFunc wbCommFunc = wbCommFuncService.selectByPrimaryKey(serno);
        return new ResultDto<WbCommFunc>(wbCommFunc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述: 保存首页的菜单默认进行修改操作，修改失败则新增
     */
    @PostMapping("/create")
    protected ResultDto<WbCommFunc> create(@RequestBody WbCommFunc wbCommFunc) throws URISyntaxException {
        int result = wbCommFuncService.updateSelective(wbCommFunc);
        if (result == 0) {
            wbCommFuncService.insertSelective(wbCommFunc);
        }
        return new ResultDto<WbCommFunc>(wbCommFunc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody WbCommFunc wbCommFunc) throws URISyntaxException {
        int result = wbCommFuncService.update(wbCommFunc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = wbCommFuncService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = wbCommFuncService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
