/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgSxkdGuarDiscount;
import cn.com.yusys.yusp.service.CfgSxkdGuarDiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSxkdGuarDiscountResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-12 15:32:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgsxkdguardiscount")
public class CfgSxkdGuarDiscountResource {
    @Autowired
    private CfgSxkdGuarDiscountService cfgSxkdGuarDiscountService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgSxkdGuarDiscount>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgSxkdGuarDiscount> list = cfgSxkdGuarDiscountService.selectAll(queryModel);
        return new ResultDto<List<CfgSxkdGuarDiscount>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgSxkdGuarDiscount>> index(QueryModel queryModel) {
        List<CfgSxkdGuarDiscount> list = cfgSxkdGuarDiscountService.selectByModel(queryModel);
        return new ResultDto<List<CfgSxkdGuarDiscount>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{guarTypeCd}")
    protected ResultDto<CfgSxkdGuarDiscount> show(@PathVariable("guarTypeCd") String guarTypeCd) {
        CfgSxkdGuarDiscount cfgSxkdGuarDiscount = cfgSxkdGuarDiscountService.selectByPrimaryKey(guarTypeCd);
        return new ResultDto<CfgSxkdGuarDiscount>(cfgSxkdGuarDiscount);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgSxkdGuarDiscount> create(@RequestBody CfgSxkdGuarDiscount cfgSxkdGuarDiscount) throws URISyntaxException {
        cfgSxkdGuarDiscountService.insert(cfgSxkdGuarDiscount);
        return new ResultDto<CfgSxkdGuarDiscount>(cfgSxkdGuarDiscount);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgSxkdGuarDiscount cfgSxkdGuarDiscount) throws URISyntaxException {
        int result = cfgSxkdGuarDiscountService.update(cfgSxkdGuarDiscount);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{guarTypeCd}")
    protected ResultDto<Integer> delete(@PathVariable("guarTypeCd") String guarTypeCd) {
        int result = cfgSxkdGuarDiscountService.deleteByPrimaryKey(guarTypeCd);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgSxkdGuarDiscountService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:selectCfgSxkdGuarDiscountByGuarTypeCd
     * @函数描述:查询授信抵押物折率配置表信息
     * @参数与返回说明:
     * @创建者： zhnagliang15
     * @算法描述:
     */
    @PostMapping("/selectCfgSxkdGuarDiscountByGuarTypeCd")
    protected ResultDto<CfgSxkdGuarDiscount> selectCfgSxkdGuarDiscountByGuarTypeCd(@RequestBody String guarTypeCd) {
        CfgSxkdGuarDiscount cfgSxkdGuarDiscount = cfgSxkdGuarDiscountService.selectCfgSxkdGuarDiscountByGuarTypeCd(guarTypeCd);
        return new ResultDto<CfgSxkdGuarDiscount>(cfgSxkdGuarDiscount);
    }
}
