/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgTaskpoolRule
 * @类描述: cfg_taskpool_rule数据实体类
 * @功能描述:
 * @创建人: xuwei
 * @创建时间: 2021-04-08 20:48:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_taskpool_rule")
public class CfgTaskpoolRule extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 规则方案编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "TASKPOOL_RULE_ID")
    private String taskpoolRuleId;

    /**
     * 分配方式  STD_ZB_TP_ALLOT_WAY
     **/
    @Column(name = "ALLOT_WAY", unique = false, nullable = true, length = 5)
    private String allotWay;

    /**
     * 系统分配起始时间
     **/
    @Column(name = "SYATEM_ALLOT_START_TIME", unique = false, nullable = true, length = 20)
    private String syatemAllotStartTime;

    /**
     * 系统分配终止时间
     **/
    @Column(name = "SYATEM_ALLOT_END_TIME", unique = false, nullable = true, length = 20)
    private String syatemAllotEndTime;

    /**
     * 管理员分配起始时间
     **/
    @Column(name = "MANA_ALLOT_START_TIME", unique = false, nullable = true, length = 20)
    private String manaAllotStartTime;

    /**
     * 管理员分配终止时间
     **/
    @Column(name = "MANA_ALLOT_END_TIME", unique = false, nullable = true, length = 20)
    private String manaAllotEndTime;

    /**
     * 分配方法  STD_ZB_TP_ALLOT_MODE
     **/
    @Column(name = "ALLOT_MODE", unique = false, nullable = true, length = 5)
    private String allotMode;

    /**
     * 是否启用  STD_ZB_YES_NO
     **/
    @Column(name = "IS_BEGIN", unique = false, nullable = true, length = 5)
    private String isBegin;

    /**
     * 分配时间间隔
     **/
    @Column(name = "ALLOT_TIME_INTERVAL", unique = false, nullable = true, length = 10)
    private Integer allotTimeInterval;

    /**
     * 登记人
     **/
    @RedisCacheTranslator(redisCacheKey = "userName", refFieldName = "inputName")
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @RedisCacheTranslator(redisCacheKey = "orgName", refFieldName = "inputBrName")
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @RedisCacheTranslator(redisCacheKey = "userName", refFieldName = "updName")
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 关联岗位
     **/
    @Column(name = "CORRE_DUTY", unique = false, nullable = true, length = 20)
    private String correDuty;

    /**
     * 有权人分配所属岗位
     **/
    @Column(name = "ALLOT_PRIV_DUTY", unique = false, nullable = true, length = 20)
    private String allotPrivDuty;

    /**
     * 有权人分配所属机构
     **/
    @RedisCacheTranslator(redisCacheKey = "orgName", refFieldName = "allotPrivOrgName")
    @Column(name = "ALLOT_PRIV_ORG", unique = false, nullable = true, length = 20)
    private String allotPrivOrg;

    /**
     * 有权人分配
     **/
    @RedisCacheTranslator(redisCacheKey = "userName", refFieldName = "allotPrivName")
    @Column(name = "ALLOT_PRIV", unique = false, nullable = true, length = 300)
    private String allotPriv;

    public CfgTaskpoolRule() {
        // Not compliant
    }

    /**
     * @return taskpoolRuleId
     */
    public String getTaskpoolRuleId() {
        return this.taskpoolRuleId;
    }

    /**
     * @param taskpoolRuleId
     */
    public void setTaskpoolRuleId(String taskpoolRuleId) {
        this.taskpoolRuleId = taskpoolRuleId;
    }

    /**
     * @return allotWay
     */
    public String getAllotWay() {
        return this.allotWay;
    }

    /**
     * @param allotWay
     */
    public void setAllotWay(String allotWay) {
        this.allotWay = allotWay;
    }

    /**
     * @return syatemAllotStartTime
     */
    public String getSyatemAllotStartTime() {
        return this.syatemAllotStartTime;
    }

    /**
     * @param syatemAllotStartTime
     */
    public void setSyatemAllotStartTime(String syatemAllotStartTime) {
        this.syatemAllotStartTime = syatemAllotStartTime;
    }

    /**
     * @return syatemAllotEndTime
     */
    public String getSyatemAllotEndTime() {
        return this.syatemAllotEndTime;
    }

    /**
     * @param syatemAllotEndTime
     */
    public void setSyatemAllotEndTime(String syatemAllotEndTime) {
        this.syatemAllotEndTime = syatemAllotEndTime;
    }

    /**
     * @return manaAllotStartTime
     */
    public String getManaAllotStartTime() {
        return this.manaAllotStartTime;
    }

    /**
     * @param manaAllotStartTime
     */
    public void setManaAllotStartTime(String manaAllotStartTime) {
        this.manaAllotStartTime = manaAllotStartTime;
    }

    /**
     * @return manaAllotEndTime
     */
    public String getManaAllotEndTime() {
        return this.manaAllotEndTime;
    }

    /**
     * @param manaAllotEndTime
     */
    public void setManaAllotEndTime(String manaAllotEndTime) {
        this.manaAllotEndTime = manaAllotEndTime;
    }

    /**
     * @return allotMode
     */
    public String getAllotMode() {
        return this.allotMode;
    }

    /**
     * @param allotMode
     */
    public void setAllotMode(String allotMode) {
        this.allotMode = allotMode;
    }

    /**
     * @return isBegin
     */
    public String getIsBegin() {
        return this.isBegin;
    }

    /**
     * @param isBegin
     */
    public void setIsBegin(String isBegin) {
        this.isBegin = isBegin;
    }

    /**
     * @return allotTimeInterval
     */
    public Integer getAllotTimeInterval() {
        return this.allotTimeInterval;
    }

    /**
     * @param allotTimeInterval
     */
    public void setAllotTimeInterval(Integer allotTimeInterval) {
        this.allotTimeInterval = allotTimeInterval;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return correDuty
     */
    public String getCorreDuty() {
        return this.correDuty;
    }

    /**
     * @param correDuty
     */
    public void setCorreDuty(String correDuty) {
        this.correDuty = correDuty;
    }

    /**
     * @return allotPrivDuty
     */
    public String getAllotPrivDuty() {
        return this.allotPrivDuty;
    }

    /**
     * @param allotPrivDuty
     */
    public void setAllotPrivDuty(String allotPrivDuty) {
        this.allotPrivDuty = allotPrivDuty;
    }

    /**
     * @return allotPrivOrg
     */
    public String getAllotPrivOrg() {
        return this.allotPrivOrg;
    }

    /**
     * @param allotPrivOrg
     */
    public void setAllotPrivOrg(String allotPrivOrg) {
        this.allotPrivOrg = allotPrivOrg;
    }

    /**
     * @return allotPriv
     */
    public String getAllotPriv() {
        return this.allotPriv;
    }

    /**
     * @param allotPriv
     */
    public void setAllotPriv(String allotPriv) {
        this.allotPriv = allotPriv;
    }


}