/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdBasicinfo
 * @类描述: cfg_prd_basicinfo数据实体类
 * @功能描述:
 * @创建人: Acer
 * @创建时间: 2021-06-18 00:29:03
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_prd_basicinfo")
public class CfgPrdBasicinfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 产品编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PRD_ID")
    private String prdId;

    /**
     * 产品名称
     **/
    @Column(name = "PRD_NAME", unique = false, nullable = false, length = 80)
    private String prdName;

    /**
     * 目录层级
     **/
    @Column(name = "CATALOG_ID", unique = false, nullable = true, length = 200)
    private String catalogId;

    /**
     * 目录层级名称
     **/
    @Column(name = "CATALOG_LEVEL_NAME", unique = false, nullable = true, length = 400)
    private String catalogLevelName;

    /**
     * 产品版本号
     **/
    @Column(name = "PRD_VERSION", unique = false, nullable = true, length = 10)
    private String prdVersion;

    /**
     * 是否基础产品
     **/
    @Column(name = "IS_BASIC_PRD", unique = false, nullable = true, length = 5)
    private String isBasicPrd;

    /**
     * 基础产品编号
     **/
    @Column(name = "BASIC_PRD_ID", unique = false, nullable = true, length = 40)
    private String basicPrdId;

    /**
     * 所属法人机构
     **/
    @Column(name = "INSTU_CDE", unique = false, nullable = true, length = 10)
    private String instuCde;

    /**
     * 生效日期
     **/
    @Column(name = "START_DATE", unique = false, nullable = true, length = 10)
    private String startDate;

    /**
     * 失效日期
     **/
    @Column(name = "END_DATE", unique = false, nullable = true, length = 10)
    private String endDate;

    /**
     * 产品描述
     **/
    @Column(name = "PRD_DESCRIBE", unique = false, nullable = true, length = 400)
    private String prdDescribe;

    /**
     * 产品状态
     **/
    @Column(name = "PRD_STATUS", unique = false, nullable = true, length = 5)
    private String prdStatus;

    /**
     * 放款方式 STD_ZB_PUTOUT_TYP
     **/
    @Column(name = "DISB_WAY", unique = false, nullable = true, length = 10)
    private String disbWay;

    /**
     * 产品入口作业流编号
     **/
    @Column(name = "WFI_SIGN_ID", unique = false, nullable = true, length = 40)
    private String wfiSignId;

    /**
     * 业务规则方案编号
     **/
    @Column(name = "PLAN_ID", unique = false, nullable = true, length = 40)
    private String planId;

    /**
     * 产品模版编号
     **/
    @Column(name = "MODEL_GROUP_NO", unique = false, nullable = true, length = 40)
    private String modelGroupNo;

    /**
     * 产品类别
     **/
    @Column(name = "PRD_TYPE", unique = false, nullable = true, length = 40)
    private String prdType;

    /**
     * 贷款性质
     **/
    @Column(name = "LOAN_CHA", unique = false, nullable = true, length = 5)
    private String loanCha;

    /**
     * 所属条线
     **/
    @Column(name = "PRD_BELG_LINE", unique = false, nullable = true, length = 40)
    private String prdBelgLine;

    /**
     * 核心产品编号
     **/
    @Column(name = "CORE_PRD_ID", unique = false, nullable = true, length = 40)
    private String corePrdId;

    /**
     * 适用币种
     **/
    @Column(name = "SUIT_CUR_TYPE", unique = false, nullable = true, length = 500)
    private String suitCurType;

    /**
     * 适用调查报告类型
     **/
    @Column(name = "SUIT_INDGT_REPORT_TYPE", unique = false, nullable = true, length = 500)
    private String suitIndgtReportType;

    /**
     * 适用合同类型
     **/
    @Column(name = "SUIT_CONT_TYPE", unique = false, nullable = true, length = 500)
    private String suitContType;

    /**
     * 适用担保方式
     **/
    @Column(name = "SUIT_GUAR_MODE", unique = false, nullable = true, length = 500)
    private String suitGuarMode;

    /**
     * 适用支付方式
     **/
    @Column(name = "SUIT_PAY_MODE", unique = false, nullable = true, length = 500)
    private String suitPayMode;

    /**
     * 适用还款方式
     **/
    @Column(name = "SUIT_REPAY_MODE", unique = false, nullable = true, length = 500)
    private String suitRepayMode;

    /**
     * 是否需要线下调查
     **/
    @Column(name = "IS_STOP_OFFLINE", unique = false, nullable = true, length = 500)
    private String isStopOffline;

    /**
     * 是否允许展期
     **/
    @Column(name = "IS_ALLOW_EXT", unique = false, nullable = true, length = 5)
    private String isAllowExt;

    /**
     * 是否允许线上签约
     **/
    @Column(name = "IS_ALLOW_SIGN_ONLINE", unique = false, nullable = true, length = 5)
    private String isAllowSignOnline;

    /**
     * 是否允许线上放款
     **/
    @Column(name = "IS_ALLOW_DISB_ONLINE", unique = false, nullable = true, length = 5)
    private String isAllowDisbOnline;

    /**
     * 是否允许线上放款
     **/
    @Column(name = "ADV_REPAY_END_DATE", unique = false, nullable = true, length = 10)
    private String advRepayEndDate;

    /**
     * 备注
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 400)
    private String remark;

    /**
     * 排序
     **/
    @Column(name = "SERIAL_NUMBER", unique = false, nullable = true, length = 10)
    private java.math.BigDecimal serialNumber;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 更新人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 更新机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 更新日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgPrdBasicinfo() {
        // Not compliant
    }

    /**
     * @return prdId
     */
    public String getPrdId() {
        return this.prdId;
    }

    /**
     * @param prdId
     */
    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    /**
     * @return prdName
     */
    public String getPrdName() {
        return this.prdName;
    }

    /**
     * @param prdName
     */
    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    /**
     * @return catalogId
     */
    public String getCatalogId() {
        return this.catalogId;
    }

    /**
     * @param catalogId
     */
    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    /**
     * @return catalogLevelName
     */
    public String getCatalogLevelName() {
        return this.catalogLevelName;
    }

    /**
     * @param catalogLevelName
     */
    public void setCatalogLevelName(String catalogLevelName) {
        this.catalogLevelName = catalogLevelName;
    }

    /**
     * @return prdVersion
     */
    public String getPrdVersion() {
        return this.prdVersion;
    }

    /**
     * @param prdVersion
     */
    public void setPrdVersion(String prdVersion) {
        this.prdVersion = prdVersion;
    }

    /**
     * @return isBasicPrd
     */
    public String getIsBasicPrd() {
        return this.isBasicPrd;
    }

    /**
     * @param isBasicPrd
     */
    public void setIsBasicPrd(String isBasicPrd) {
        this.isBasicPrd = isBasicPrd;
    }

    /**
     * @return basicPrdId
     */
    public String getBasicPrdId() {
        return this.basicPrdId;
    }

    /**
     * @param basicPrdId
     */
    public void setBasicPrdId(String basicPrdId) {
        this.basicPrdId = basicPrdId;
    }

    /**
     * @return instuCde
     */
    public String getInstuCde() {
        return this.instuCde;
    }

    /**
     * @param instuCde
     */
    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    /**
     * @return startDate
     */
    public String getStartDate() {
        return this.startDate;
    }

    /**
     * @param startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return endDate
     */
    public String getEndDate() {
        return this.endDate;
    }

    /**
     * @param endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return prdDescribe
     */
    public String getPrdDescribe() {
        return this.prdDescribe;
    }

    /**
     * @param prdDescribe
     */
    public void setPrdDescribe(String prdDescribe) {
        this.prdDescribe = prdDescribe;
    }

    /**
     * @return prdStatus
     */
    public String getPrdStatus() {
        return this.prdStatus;
    }

    /**
     * @param prdStatus
     */
    public void setPrdStatus(String prdStatus) {
        this.prdStatus = prdStatus;
    }

    /**
     * @return disbWay
     */
    public String getDisbWay() {
        return this.disbWay;
    }

    /**
     * @param disbWay
     */
    public void setDisbWay(String disbWay) {
        this.disbWay = disbWay;
    }

    /**
     * @return wfiSignId
     */
    public String getWfiSignId() {
        return this.wfiSignId;
    }

    /**
     * @param wfiSignId
     */
    public void setWfiSignId(String wfiSignId) {
        this.wfiSignId = wfiSignId;
    }

    /**
     * @return planId
     */
    public String getPlanId() {
        return this.planId;
    }

    /**
     * @param planId
     */
    public void setPlanId(String planId) {
        this.planId = planId;
    }

    /**
     * @return modelGroupNo
     */
    public String getModelGroupNo() {
        return this.modelGroupNo;
    }

    /**
     * @param modelGroupNo
     */
    public void setModelGroupNo(String modelGroupNo) {
        this.modelGroupNo = modelGroupNo;
    }

    /**
     * @return prdType
     */
    public String getPrdType() {
        return this.prdType;
    }

    /**
     * @param prdType
     */
    public void setPrdType(String prdType) {
        this.prdType = prdType;
    }

    /**
     * @return loanCha
     */
    public String getLoanCha() {
        return this.loanCha;
    }

    /**
     * @param loanCha
     */
    public void setLoanCha(String loanCha) {
        this.loanCha = loanCha;
    }

    /**
     * @return prdBelgLine
     */
    public String getPrdBelgLine() {
        return this.prdBelgLine;
    }

    /**
     * @param prdBelgLine
     */
    public void setPrdBelgLine(String prdBelgLine) {
        this.prdBelgLine = prdBelgLine;
    }

    /**
     * @return corePrdId
     */
    public String getCorePrdId() {
        return this.corePrdId;
    }

    /**
     * @param corePrdId
     */
    public void setCorePrdId(String corePrdId) {
        this.corePrdId = corePrdId;
    }

    /**
     * @return suitCurType
     */
    public String getSuitCurType() {
        return this.suitCurType;
    }

    /**
     * @param suitCurType
     */
    public void setSuitCurType(String suitCurType) {
        this.suitCurType = suitCurType;
    }

    /**
     * @return suitIndgtReportType
     */
    public String getSuitIndgtReportType() {
        return this.suitIndgtReportType;
    }

    /**
     * @param suitIndgtReportType
     */
    public void setSuitIndgtReportType(String suitIndgtReportType) {
        this.suitIndgtReportType = suitIndgtReportType;
    }

    /**
     * @return suitContType
     */
    public String getSuitContType() {
        return this.suitContType;
    }

    /**
     * @param suitContType
     */
    public void setSuitContType(String suitContType) {
        this.suitContType = suitContType;
    }

    /**
     * @return suitGuarMode
     */
    public String getSuitGuarMode() {
        return this.suitGuarMode;
    }

    /**
     * @param suitGuarMode
     */
    public void setSuitGuarMode(String suitGuarMode) {
        this.suitGuarMode = suitGuarMode;
    }

    /**
     * @return suitPayMode
     */
    public String getSuitPayMode() {
        return this.suitPayMode;
    }

    /**
     * @param suitPayMode
     */
    public void setSuitPayMode(String suitPayMode) {
        this.suitPayMode = suitPayMode;
    }

    /**
     * @return suitRepayMode
     */
    public String getSuitRepayMode() {
        return this.suitRepayMode;
    }

    /**
     * @param suitRepayMode
     */
    public void setSuitRepayMode(String suitRepayMode) {
        this.suitRepayMode = suitRepayMode;
    }

    /**
     * @return isStopOffline
     */
    public String getIsStopOffline() {
        return this.isStopOffline;
    }

    /**
     * @param isStopOffline
     */
    public void setIsStopOffline(String isStopOffline) {
        this.isStopOffline = isStopOffline;
    }

    /**
     * @return isAllowExt
     */
    public String getIsAllowExt() {
        return this.isAllowExt;
    }

    /**
     * @param isAllowExt
     */
    public void setIsAllowExt(String isAllowExt) {
        this.isAllowExt = isAllowExt;
    }

    /**
     * @return isAllowSignOnline
     */
    public String getIsAllowSignOnline() {
        return this.isAllowSignOnline;
    }

    /**
     * @param isAllowSignOnline
     */
    public void setIsAllowSignOnline(String isAllowSignOnline) {
        this.isAllowSignOnline = isAllowSignOnline;
    }

    /**
     * @return isAllowDisbOnline
     */
    public String getIsAllowDisbOnline() {
        return this.isAllowDisbOnline;
    }

    /**
     * @param isAllowDisbOnline
     */
    public void setIsAllowDisbOnline(String isAllowDisbOnline) {
        this.isAllowDisbOnline = isAllowDisbOnline;
    }

    /**
     * @return advRepayEndDate
     */
    public String getAdvRepayEndDate() {
        return this.advRepayEndDate;
    }

    /**
     * @param advRepayEndDate
     */
    public void setAdvRepayEndDate(String advRepayEndDate) {
        this.advRepayEndDate = advRepayEndDate;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return serialNumber
     */
    public java.math.BigDecimal getSerialNumber() {
        return this.serialNumber;
    }

    /**
     * @param serialNumber
     */
    public void setSerialNumber(java.math.BigDecimal serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}