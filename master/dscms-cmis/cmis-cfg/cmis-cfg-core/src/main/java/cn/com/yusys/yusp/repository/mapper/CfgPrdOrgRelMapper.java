/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgPrdOrgRel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdOrgRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-07 11:27:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CfgPrdOrgRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CfgPrdOrgRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: queryCfgPrdOrgRel
     * @方法描述: 根据产品编号、适用范围、适用类型、机构编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    int queryCfgPrdOrgRel(CfgPrdOrgRel record);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgPrdOrgRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CfgPrdOrgRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CfgPrdOrgRel record);

    /**
     * 复制产品时，将老产品配置的产品适用机构联动复制保存
     *
     * @param prdId    复制后生成的新产品编号
     * @param oldPrdId 被复制的老产品编号
     * @param oprType  操作类型
     * @return
     */
    int copyCfgPrdOrgRel(@Param("prdId") String prdId, @Param("oldPrdId") String oldPrdId, @Param("oprType") String oprType);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CfgPrdOrgRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CfgPrdOrgRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 删除产品时，将产品配置的产品适用机构联动删除，此处为逻辑删除更新操作类型为“删除”，不进行物理删除
     *
     * @param prdId   产品编号
     * @param oprType 操作类型
     * @return
     */
    int deleteCfgPrdOrgRel(@Param("prdId") String prdId, @Param("oprType") String oprType);

}