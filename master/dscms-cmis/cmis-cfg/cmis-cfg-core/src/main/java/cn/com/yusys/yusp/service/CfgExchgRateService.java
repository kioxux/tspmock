/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CfgServiceExceptionDefEnums;
import cn.com.yusys.yusp.domain.CfgExchgRate;
import cn.com.yusys.yusp.dto.CfgExchgRateDto;
import cn.com.yusys.yusp.repository.mapper.CfgExchgRateMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgExchgRateService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-12 15:29:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgExchgRateService {
    private static final Logger log = LoggerFactory.getLogger(CfgExchgRateService.class);

    @Autowired
    private CfgExchgRateMapper cfgExchgRateMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgExchgRate selectByPrimaryKey(String pkId) {
        return cfgExchgRateMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgExchgRate> selectAll(QueryModel model) {
        List<CfgExchgRate> records = (List<CfgExchgRate>) cfgExchgRateMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgExchgRate> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgExchgRate> list = cfgExchgRateMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgExchgRate record) {
        return cfgExchgRateMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgExchgRate record) {
        return cfgExchgRateMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgExchgRate record) {
        return cfgExchgRateMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgExchgRate record) {
        return cfgExchgRateMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgExchgRateMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgExchgRateMapper.deleteByIds(ids);
    }


    /**
     * 通过币种查询实时汇率信息
     *
     * @param cfgExchgRateDto
     * @return
     */
    public CfgExchgRateDto queryRatioByCurType(CfgExchgRateDto cfgExchgRateDto) {
        String rtnCode = "000000";//默认为成功
        String rtnMsg = "查询成功";
        try {
            log.info("通过币种查询实时汇率信息");
            String oldCurType = cfgExchgRateDto.getOrigiCurType();
            String compCurType = cfgExchgRateDto.getCompCurType();
            log.info("通过币种查询实时汇率信息，实时币种：" + oldCurType + "，对照币种：" + compCurType);
            if (StringUtils.isEmpty(oldCurType) || StringUtils.isEmpty(compCurType)) {
                throw new YuspException(CfgServiceExceptionDefEnums.E_CFGEXCHANGERATE_PARAM_ERROR.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFGEXCHANGERATE_PARAM_ERROR.getExceptionDesc());
            }


            CfgExchgRate cfgExchgRate = new CfgExchgRate();
            BeanUtils.copyProperties(cfgExchgRateDto, cfgExchgRate);
            cfgExchgRate = cfgExchgRateMapper.selectByCfgExchgRateParam(cfgExchgRate);

            if (cfgExchgRate == null) {
                throw new YuspException(CfgServiceExceptionDefEnums.E_CFGEXCHANGERATE_NOT_EXISTS_ERROR.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFGEXCHANGERATE_NOT_EXISTS_ERROR.getExceptionDesc());
            }

            BeanUtils.copyProperties(cfgExchgRate, cfgExchgRateDto);

            log.info("通过币种查询实时汇率信息，实时币种：" + oldCurType + "，对照币种：" + compCurType + " 实时汇率为：" + cfgExchgRateDto.getMidRemit().doubleValue() + "");

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } finally {
            cfgExchgRateDto.setRtnCode(rtnCode);
            cfgExchgRateDto.setRtnMsg(rtnMsg);
        }
        return cfgExchgRateDto;
    }
}
