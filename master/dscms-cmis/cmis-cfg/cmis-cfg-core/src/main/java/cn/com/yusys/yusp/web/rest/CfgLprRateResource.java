/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgLprRate;
import cn.com.yusys.yusp.dto.CfgLprRateDto;
import cn.com.yusys.yusp.service.CfgLprRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgLprRateResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-15 18:51:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfglprrate")
public class CfgLprRateResource {
    @Autowired
    private CfgLprRateService cfgLprRateService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgLprRate>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgLprRate> list = cfgLprRateService.selectAll(queryModel);
        return new ResultDto<List<CfgLprRate>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgLprRate>> index(QueryModel queryModel) {
        List<CfgLprRate> list = cfgLprRateService.selectByModel(queryModel);
        return new ResultDto<List<CfgLprRate>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgLprRate> show(@PathVariable("pkId") String pkId) {
        CfgLprRate cfgLprRate = cfgLprRateService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgLprRate>(cfgLprRate);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgLprRate> create(@RequestBody CfgLprRate cfgLprRate) throws URISyntaxException {
        cfgLprRateService.insert(cfgLprRate);
        return new ResultDto<CfgLprRate>(cfgLprRate);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgLprRate cfgLprRate) throws URISyntaxException {
        int result = cfgLprRateService.update(cfgLprRate);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgLprRateService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgLprRateService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/15 18:55
     * @注释 增加LPR利率查询接口
     */
    @PostMapping("/selectlpr")
    protected ResultDto selectlpr(@RequestBody CfgLprRate cfgLprRate) {
        return cfgLprRateService.selectlpr(cfgLprRate);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/16 11:24
     * @注释 LPR利率单条查询
     */
    @PostMapping("/selectone")
    protected ResultDto<CfgLprRateDto> selectone(@RequestBody QueryModel queryModel) {
        return cfgLprRateService.selectone(queryModel);

    }


    /**
     * @创建人 WH
     * @创建时间 2021-9-17 17:24:41
     * @注释 根据签订日期找之后最近的lpr
     */
    @PostMapping("/selectafterlpr")
    protected ResultDto<CfgLprRateDto> selectAfterLpr(@RequestBody CfgLprRateDto cfgLprRate) {
        return cfgLprRateService.selectAfterLpr(cfgLprRate);

    }


    /**
     * @创建人 shenli
     * @创建时间 2021-9-17 17:24:37
     * @注释 根据签订日期找之前最近的lpr
     */
    @PostMapping("/selectfrontlpr")
    protected ResultDto<CfgLprRateDto> selectFrontLpr(@RequestBody CfgLprRateDto cfgLprRate) {
        return cfgLprRateService.selectFrontLpr(cfgLprRate);
    }

}
