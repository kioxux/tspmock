/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgDataFlow
 * @类描述: cfg_data_flow数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-28 17:02:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_data_flow")
public class CfgDataFlow extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 数据流编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "DATA_FLOW_ID")
    private String dataFlowId;

    /**
     * 数据流名称
     **/
    @Column(name = "DATA_FLOW_NAME", unique = false, nullable = true, length = 100)
    private String dataFlowName;

    /**
     * 源数据表
     **/
    @Column(name = "SOUR_TABLE", unique = false, nullable = true, length = 40)
    private String sourTable;

    /**
     * 汇聚表
     **/
    @Column(name = "CONV_TABLE", unique = false, nullable = true, length = 40)
    private String convTable;

    /**
     * 映射内容
     **/
    @Column(name = "MAPP_CONTENT", unique = false, nullable = true, length = 4000)
    private String mappContent;

    /**
     * 备注
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 400)
    private String remark;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 源数据表数据源
     **/
    @Column(name = "SOUR_DATASOURCE", unique = false, nullable = true, length = 40)
    private String sourDatasource;

    /**
     * 汇聚表数据源
     **/
    @Column(name = "CONV_DATASOURCE", unique = false, nullable = true, length = 40)
    private String convDatasource;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgDataFlow() {
        // Not compliant
    }

    /**
     * @return dataFlowId
     */
    public String getDataFlowId() {
        return this.dataFlowId;
    }

    /**
     * @param dataFlowId
     */
    public void setDataFlowId(String dataFlowId) {
        this.dataFlowId = dataFlowId;
    }

    /**
     * @return dataFlowName
     */
    public String getDataFlowName() {
        return this.dataFlowName;
    }

    /**
     * @param dataFlowName
     */
    public void setDataFlowName(String dataFlowName) {
        this.dataFlowName = dataFlowName;
    }

    /**
     * @return sourTable
     */
    public String getSourTable() {
        return this.sourTable;
    }

    /**
     * @param sourTable
     */
    public void setSourTable(String sourTable) {
        this.sourTable = sourTable;
    }

    /**
     * @return convTable
     */
    public String getConvTable() {
        return this.convTable;
    }

    /**
     * @param convTable
     */
    public void setConvTable(String convTable) {
        this.convTable = convTable;
    }

    /**
     * @return mappContent
     */
    public String getMappContent() {
        return this.mappContent;
    }

    /**
     * @param mappContent
     */
    public void setMappContent(String mappContent) {
        this.mappContent = mappContent;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return sourDatasource
     */
    public String getSourDatasource() {
        return this.sourDatasource;
    }

    /**
     * @param sourDatasource
     */
    public void setSourDatasource(String sourDatasource) {
        this.sourDatasource = sourDatasource;
    }

    /**
     * @return convDatasource
     */
    public String getConvDatasource() {
        return this.convDatasource;
    }

    /**
     * @param convDatasource
     */
    public void setConvDatasource(String convDatasource) {
        this.convDatasource = convDatasource;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}