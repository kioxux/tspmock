package cn.com.yusys.yusp.web.server.xdqt0007;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdqt0007.req.Xdqt0007DataReqDto;
import cn.com.yusys.yusp.server.xdqt0007.resp.Xdqt0007DataRespDto;
import cn.com.yusys.yusp.service.server.xdqt0007.Xdqt0007Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:新微贷产品信息同步
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0007:新微贷产品信息同步")
@RestController
@RequestMapping("/api/cfgqt4bsp")
public class CfgXdqt0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(CfgXdqt0007Resource.class);

    @Autowired
    private Xdqt0007Service xdqt0007Service;

    /**
     * 交易码：xdqt0007
     * 交易描述：新微贷产品信息同步
     *
     * @param xdqt0007DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("新微贷产品信息同步")
    @PostMapping("/xdqt0007")
    protected @ResponseBody
    ResultDto<Xdqt0007DataRespDto> xdqt0007(@Validated @RequestBody Xdqt0007DataReqDto xdqt0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, JSON.toJSONString(xdqt0007DataReqDto));
        Xdqt0007DataRespDto xdqt0007DataRespDto = new Xdqt0007DataRespDto();// 响应Dto:新微贷产品信息同步
        ResultDto<Xdqt0007DataRespDto> xdqt0007DataResultDto = new ResultDto<>();

        try {
            // 从xdqt0007DataReqDto获取业务值进行业务逻辑处理
            //  调用XXXXXService层开始
            xdqt0007DataRespDto = xdqt0007Service.updateXWDPrdBasicInfo(xdqt0007DataReqDto);
            //  调用XXXXXService层结束
            // TODO 封装xdqt0007DataRespDto对象开始
            // TODO 封装xdqt0007DataRespDto对象结束
            // 封装xdqt0007DataResultDto中正确的返回码和返回信息
            xdqt0007DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdqt0007DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, e.getMessage());
            xdqt0007DataResultDto.setCode(EcbEnum.ECB019999.key);
            xdqt0007DataResultDto.setMessage(EcbEnum.ECB019999.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, e.getMessage());
            // 封装xdqt0007DataResultDto中异常返回码和返回信息
            xdqt0007DataResultDto.setCode(EcbEnum.ECB019999.key);
            xdqt0007DataResultDto.setMessage(EcbEnum.ECB019999.value);
        }
        // 封装xdqt0007DataRespDto到xdqt0007DataResultDto中
        xdqt0007DataResultDto.setData(xdqt0007DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, JSON.toJSONString(xdqt0007DataResultDto));
        return xdqt0007DataResultDto;
    }
}
