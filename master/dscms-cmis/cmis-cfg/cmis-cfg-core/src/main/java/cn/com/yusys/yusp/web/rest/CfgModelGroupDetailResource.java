/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgModelGroupDetail;
import cn.com.yusys.yusp.service.CfgModelGroupDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgModelGroupDetailResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 15430
 * @创建时间: 2020-11-23 10:33:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgmodelgroupdetail")
public class CfgModelGroupDetailResource {
    @Autowired
    private CfgModelGroupDetailService cfgModelGroupDetailService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgModelGroupDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgModelGroupDetail> list = cfgModelGroupDetailService.selectAll(queryModel);
        return new ResultDto<List<CfgModelGroupDetail>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgModelGroupDetail>> index(QueryModel queryModel) {
        List<CfgModelGroupDetail> list = cfgModelGroupDetailService.selectByModel(queryModel);
        return new ResultDto<List<CfgModelGroupDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgModelGroupDetail> show(@PathVariable("pkId") String pkId) {
        CfgModelGroupDetail cfgModelGroupDetail = cfgModelGroupDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgModelGroupDetail>(cfgModelGroupDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgModelGroupDetail> create(@RequestBody CfgModelGroupDetail cfgModelGroupDetail) throws URISyntaxException {
        cfgModelGroupDetailService.insert(cfgModelGroupDetail);
        return new ResultDto<CfgModelGroupDetail>(cfgModelGroupDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgModelGroupDetail cfgModelGroupDetail) throws URISyntaxException {
        int result = cfgModelGroupDetailService.update(cfgModelGroupDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgModelGroupDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgModelGroupDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
