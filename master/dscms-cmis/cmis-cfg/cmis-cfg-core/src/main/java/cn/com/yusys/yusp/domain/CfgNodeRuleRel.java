/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgNodeRuleRel
 * @类描述: cfg_node_rule_rel数据实体类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-03 21:47:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_node_rule_rel")
public class CfgNodeRuleRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 方案编号
     **/
    @Column(name = "PLAN_ID", unique = false, nullable = false, length = 40)
    private String planId;

    /**
     * 工作流编号
     **/
    @Column(name = "WFI_SIGN_ID", unique = false, nullable = false, length = 40)
    private String wfiSignId;

    /**
     * 节点编号
     **/
    @Column(name = "NODE_ID", unique = false, nullable = false, length = 40)
    private String nodeId;

    /**
     * 规则项编号
     **/
    @Column(name = "RULE_ITEM_ID", unique = false, nullable = false, length = 40)
    private String ruleItemId;

    /**
     * 规则项名称
     **/
    @Column(name = "RULE_ITEM_NAME", unique = false, nullable = false, length = 40)
    private String ruleItemName;

    /**
     * 拦截类型 STD_ZB_NOTI_TYPE
     **/
    @Column(name = "NOTI_TYPE", unique = false, nullable = true, length = 5)
    private String notiType;

    /**
     * 是否启用 STD_ZB_YES_NO
     **/
    @Column(name = "USED_IND", unique = false, nullable = true, length = 5)
    private String usedInd;

    /**
     * 是否选中 STD_ZB_SELECT_IND
     **/
    @Column(name = "SELECT_IND", unique = false, nullable = true, length = 5)
    private String selectInd;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgNodeRuleRel() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return planId
     */
    public String getPlanId() {
        return this.planId;
    }

    /**
     * @param planId
     */
    public void setPlanId(String planId) {
        this.planId = planId;
    }

    /**
     * @return wfiSignId
     */
    public String getWfiSignId() {
        return this.wfiSignId;
    }

    /**
     * @param wfiSignId
     */
    public void setWfiSignId(String wfiSignId) {
        this.wfiSignId = wfiSignId;
    }

    /**
     * @return nodeId
     */
    public String getNodeId() {
        return this.nodeId;
    }

    /**
     * @param nodeId
     */
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * @return ruleItemId
     */
    public String getRuleItemId() {
        return this.ruleItemId;
    }

    /**
     * @param ruleItemId
     */
    public void setRuleItemId(String ruleItemId) {
        this.ruleItemId = ruleItemId;
    }

    /**
     * @param ruleItemName
     */
	/*public void setRuleItemName(String ruleItemName) {
		this.ruleItemName = ruleItemName;
	}*/

    /**
     * @return ruleItemName
     */
    public String getRuleItemName() {
        return this.ruleItemName;
    }

    /**
     * @return notiType
     */
    public String getNotiType() {
        return this.notiType;
    }

    /**
     * @param notiType
     */
    public void setNotiType(String notiType) {
        this.notiType = notiType;
    }

    /**
     * @return usedInd
     */
    public String getUsedInd() {
        return this.usedInd;
    }

    /**
     * @param usedInd
     */
    public void setUsedInd(String usedInd) {
        this.usedInd = usedInd;
    }

    /**
     * @return selectInd
     */
    public String getSelectInd() {
        return this.selectInd;
    }

    /**
     * @param selectInd
     */
    public void setSelectInd(String selectInd) {
        this.selectInd = selectInd;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

}