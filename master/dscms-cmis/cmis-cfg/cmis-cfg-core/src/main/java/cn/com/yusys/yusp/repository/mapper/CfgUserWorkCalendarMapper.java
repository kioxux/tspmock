/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.domain.CfgUserWorkCalendar;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgUserWorkCalendarMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: Admin
 * @创建时间: 2021-03-31 16:45:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CfgUserWorkCalendarMapper {

    /**
     * @方法名称: queryByMonth
     * @方法描述: 根据年月 查询当月的日志或备忘录记录.
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CfgUserWorkCalendar> queryByMonth(@Param("calendarDate") String calendarDate,
                                           @Param("loginCode") String loginCode, @Param("oprType") String oprType);
}