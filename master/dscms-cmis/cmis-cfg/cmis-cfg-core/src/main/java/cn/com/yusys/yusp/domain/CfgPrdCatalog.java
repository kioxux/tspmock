/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdCatalog
 * @类描述: cfg_prd_catalog数据实体类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2021-04-06 21:29:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_prd_catalog")
public class CfgPrdCatalog extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 目录编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "CATALOG_ID")
    private String catalogId;

    /**
     * 目录名称
     **/
    @Column(name = "CATALOG_NAME", unique = false, nullable = true, length = 80)
    private String catalogName;

    /**
     * 目录层级
     **/
    @Column(name = "CATALOG_LEVEL_ID", unique = false, nullable = true, length = 200)
    private String catalogLevelId;

    /**
     * 上级目录编码
     **/
    @Column(name = "SUP_CATALOG_ID", unique = false, nullable = true, length = 20)
    private String supCatalogId;

    /**
     * 上级目录名称
     **/
    @Column(name = "SUP_CATALOG_NAME", unique = false, nullable = true, length = 80)
    private String supCatalogName;

    /**
     * 备注
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 200)
    private String remark;

    /**
     * 序号
     **/
    @Column(name = "SERIAL_NUMBER", unique = false, nullable = true, length = 5)
    private java.math.BigDecimal serialNumber;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    @RedisCacheTranslator(redisCacheKey = "userName", refFieldName = "inputName")
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    @RedisCacheTranslator(redisCacheKey = "orgName", refFieldName = "inputBrName")
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    @RedisCacheTranslator(redisCacheKey = "userName", refFieldName = "updName")
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    @RedisCacheTranslator(redisCacheKey = "orgName", refFieldName = "updBrName")
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgPrdCatalog() {
        // Not compliant
    }

    /**
     * @return catalogId
     */
    public String getCatalogId() {
        return this.catalogId;
    }

    /**
     * @param catalogId
     */
    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    /**
     * @return catalogName
     */
    public String getCatalogName() {
        return this.catalogName;
    }

    /**
     * @param catalogName
     */
    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    /**
     * @return catalogLevelId
     */
    public String getCatalogLevelId() {
        return this.catalogLevelId;
    }

    /**
     * @param catalogLevelId
     */
    public void setCatalogLevelId(String catalogLevelId) {
        this.catalogLevelId = catalogLevelId;
    }

    /**
     * @return supCatalogId
     */
    public String getSupCatalogId() {
        return this.supCatalogId;
    }

    /**
     * @param supCatalogId
     */
    public void setSupCatalogId(String supCatalogId) {
        this.supCatalogId = supCatalogId;
    }

    /**
     * @return supCatalogName
     */
    public String getSupCatalogName() {
        return this.supCatalogName;
    }

    /**
     * @param supCatalogName
     */
    public void setSupCatalogName(String supCatalogName) {
        this.supCatalogName = supCatalogName;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return serialNumber
     */
    public java.math.BigDecimal getSerialNumber() {
        return this.serialNumber;
    }

    /**
     * @param serialNumber
     */
    public void setSerialNumber(java.math.BigDecimal serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }


}