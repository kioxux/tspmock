/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrgElecSeal
 * @类描述: cfg_org_elec_seal数据实体类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-07-12 08:57:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_org_elec_seal")
public class CfgOrgElecSeal extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 机构代码
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "ORG_CODE")
    private String orgCode;

    /**
     * 机构名称
     **/
    @Column(name = "ORG_NAME", unique = false, nullable = true, length = 100)
    private String orgName;

    /**
     * 信贷合同章编码
     **/
    @Column(name = "SEAL_CODE", unique = false, nullable = true, length = 60)
    private String sealCode;

    /**
     * 信贷合同章名称
     **/
    @Column(name = "SEAL_NAME", unique = false, nullable = true, length = 100)
    private String sealName;

    /**
     * 信贷合同章密码
     **/
    @Column(name = "SEAL_PASSWORD", unique = false, nullable = true, length = 60)
    private String sealPassword;

    /**
     * 是否启用
     **/
    @Column(name = "IS_BEGIN", unique = false, nullable = true, length = 5)
    private String isBegin;

    /**
     * 抵押合同章编码
     **/
    @Column(name = "GAUR_SEAL_CODE", unique = false, nullable = true, length = 60)
    private String gaurSealCode;

    /**
     * 抵押合同章名称
     **/
    @Column(name = "GAUR_SEAL_NAME", unique = false, nullable = true, length = 100)
    private String gaurSealName;

    /**
     * 抵押合同章密码
     **/
    @Column(name = "GAUR_SEAL_PASSWORD", unique = false, nullable = true, length = 60)
    private String gaurSealPassword;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    public CfgOrgElecSeal() {
        // Not compliant
    }

    /**
     * @return orgCode
     */
    public String getOrgCode() {
        return this.orgCode;
    }

    /**
     * @param orgCode
     */
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    /**
     * @return orgName
     */
    public String getOrgName() {
        return this.orgName;
    }

    /**
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return sealCode
     */
    public String getSealCode() {
        return this.sealCode;
    }

    /**
     * @param sealCode
     */
    public void setSealCode(String sealCode) {
        this.sealCode = sealCode;
    }

    /**
     * @return sealName
     */
    public String getSealName() {
        return this.sealName;
    }

    /**
     * @param sealName
     */
    public void setSealName(String sealName) {
        this.sealName = sealName;
    }

    /**
     * @return sealPassword
     */
    public String getSealPassword() {
        return this.sealPassword;
    }

    /**
     * @param sealPassword
     */
    public void setSealPassword(String sealPassword) {
        this.sealPassword = sealPassword;
    }

    /**
     * @return isBegin
     */
    public String getIsBegin() {
        return this.isBegin;
    }

    /**
     * @param isBegin
     */
    public void setIsBegin(String isBegin) {
        this.isBegin = isBegin;
    }

    /**
     * @return gaurSealCode
     */
    public String getGaurSealCode() {
        return this.gaurSealCode;
    }

    /**
     * @param gaurSealCode
     */
    public void setGaurSealCode(String gaurSealCode) {
        this.gaurSealCode = gaurSealCode;
    }

    /**
     * @return gaurSealName
     */
    public String getGaurSealName() {
        return this.gaurSealName;
    }

    /**
     * @param gaurSealName
     */
    public void setGaurSealName(String gaurSealName) {
        this.gaurSealName = gaurSealName;
    }

    /**
     * @return gaurSealPassword
     */
    public String getGaurSealPassword() {
        return this.gaurSealPassword;
    }

    /**
     * @param gaurSealPassword
     */
    public void setGaurSealPassword(String gaurSealPassword) {
        this.gaurSealPassword = gaurSealPassword;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }


}