/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSftp
 * @类描述: cfg_sftp数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 17:34:48
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_sftp")
public class CfgSftp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 服务器连接ip
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "HOST")
    private String host;

    /**
     * 业务场景类型标识
     **/
    @Column(name = "BIZ_SCENSE_TYPE", unique = false, nullable = true, length = 5)
    private String bizScenseType;

    /**
     * 用户名
     **/
    @Column(name = "USERNAME", unique = false, nullable = true, length = 200)
    private String username;

    /**
     * 密码
     **/
    @Column(name = "PASSWORD", unique = false, nullable = true, length = 200)
    private String password;

    /**
     * 端口号
     **/
    @Column(name = "PORT", unique = false, nullable = true, length = 200)
    private String port;

    /**
     * SFTP服务器下载目录
     **/
    @Column(name = "SERVERPATH", unique = false, nullable = true, length = 500)
    private String serverpath;

    /**
     * SFTP文件名下载后缀:xxx.zip
     **/
    @Column(name = "SERVERFILENAME", unique = false, nullable = true, length = 500)
    private String serverfilename;

    /**
     * SFTP服务器保存目录
     **/
    @Column(name = "SAVEPATH", unique = false, nullable = true, length = 500)
    private String savepath;

    /**
     * 压缩前保存文件名："ret_partner_" +xxx.txt
     **/
    @Column(name = "SAVEFILENAME", unique = false, nullable = true, length = 500)
    private String savefilename;

    /**
     * 本地获取文件目录
     **/
    @Column(name = "LOCALPATH", unique = false, nullable = true, length = 500)
    private String localpath;

    public CfgSftp() {
        // Not compliant
    }

    /**
     * @return host
     */
    public String getHost() {
        return this.host;
    }

    /**
     * @param host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return bizScenseType
     */
    public String getBizScenseType() {
        return this.bizScenseType;
    }

    /**
     * @param bizScenseType
     */
    public void setBizScenseType(String bizScenseType) {
        this.bizScenseType = bizScenseType;
    }

    /**
     * @return username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return port
     */
    public String getPort() {
        return this.port;
    }

    /**
     * @param port
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return serverpath
     */
    public String getServerpath() {
        return this.serverpath;
    }

    /**
     * @param serverpath
     */
    public void setServerpath(String serverpath) {
        this.serverpath = serverpath;
    }

    /**
     * @return serverfilename
     */
    public String getServerfilename() {
        return this.serverfilename;
    }

    /**
     * @param serverfilename
     */
    public void setServerfilename(String serverfilename) {
        this.serverfilename = serverfilename;
    }

    /**
     * @return savepath
     */
    public String getSavepath() {
        return this.savepath;
    }

    /**
     * @param savepath
     */
    public void setSavepath(String savepath) {
        this.savepath = savepath;
    }

    /**
     * @return savefilename
     */
    public String getSavefilename() {
        return this.savefilename;
    }

    /**
     * @param savefilename
     */
    public void setSavefilename(String savefilename) {
        this.savefilename = savefilename;
    }

    /**
     * @return localpath
     */
    public String getLocalpath() {
        return this.localpath;
    }

    /**
     * @param localpath
     */
    public void setLocalpath(String localpath) {
        this.localpath = localpath;
    }


}