/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgFlexQryDetail;
import cn.com.yusys.yusp.repository.mapper.CfgFlexQryDetailMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryDetailService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-28 20:43:08
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgFlexQryDetailService {
    private static final Logger log = LoggerFactory.getLogger(CfgPrdBasicinfoService.class);

    @Autowired
    private CfgFlexQryDetailMapper cfgFlexQryDetailMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgFlexQryDetail selectByPrimaryKey(String pkId) {
        return cfgFlexQryDetailMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgFlexQryDetail> selectAll(QueryModel model) {
        List<CfgFlexQryDetail> records = (List<CfgFlexQryDetail>) cfgFlexQryDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgFlexQryDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgFlexQryDetail> list = cfgFlexQryDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgFlexQryDetail record) {
        return cfgFlexQryDetailMapper.insert(record);
    }

    /**
     * @方法名称: batchInsert
     * @方法描述: 批量插入多条数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int batchInsert(List<CfgFlexQryDetail> listCfgFlexQryDetail) throws URISyntaxException {
        //数据保存条数
        int num = 0;
        int cfgFlexQryDetailNum = 0;
        //循环插入指标配置详情数据

        if (listCfgFlexQryDetail != null) {
            cfgFlexQryDetailNum = listCfgFlexQryDetail.size();
            for (int i = 0; i < listCfgFlexQryDetail.size(); i++) {
                //将map转化成domain
                CfgFlexQryDetail cfgFlexQryDetail = JSONObject.parseObject(JSON.toJSONString(listCfgFlexQryDetail.get(i)), CfgFlexQryDetail.class);
                //操作类型默认“新增”
                cfgFlexQryDetail.setOprType(CommonConstant.ADD_OPR);
                //插入数据
                insert(cfgFlexQryDetail);
                num++;
            }
        }
        log.info("***************当前指标参数详细条数【" + cfgFlexQryDetailNum + "】条,插入指标参数详细条数【" + num + "】条***************");
        return num;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgFlexQryDetail record) {
        return cfgFlexQryDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgFlexQryDetail record) {
        return cfgFlexQryDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgFlexQryDetail record) {
        return cfgFlexQryDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateByIndexCode
     * @方法描述: 根据指标编号更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByIndexCode(CfgFlexQryDetail cfgFlexQryDetail) {
        return cfgFlexQryDetailMapper.updateByIndexCode(cfgFlexQryDetail);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgFlexQryDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgFlexQryDetailMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByIndexCodeAndEN
     * @方法描述: 根据指标编号、指标英文查询指标详情
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgFlexQryDetail selectByIndexCodeAndEN(String indexCode, String colNameEn, String oprType) {
        return cfgFlexQryDetailMapper.selectByIndexCodeAndEN(indexCode, colNameEn, oprType);
    }

}
