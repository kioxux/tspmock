/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSOrgElecSeal
 * @类描述: cfg_s_org_elec_seal数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-06-10 22:30:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_s_org_elec_seal")
public class CfgSOrgElecSeal extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 机构号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "ORG_NO")
    private String orgNo;

    /**
     * 机构名称
     **/
    @Column(name = "ORG_NAME", unique = false, nullable = true, length = 80)
    private String orgName;

    /**
     * 信贷合同章编码
     **/
    @Column(name = "SEAL_CODE", unique = false, nullable = true, length = 60)
    private String sealCode;

    /**
     * 信贷合同章名称
     **/
    @Column(name = "SEAL_NAME", unique = false, nullable = true, length = 60)
    private String sealName;

    /**
     * 信贷合同章密码
     **/
    @Column(name = "SEAL_PWD", unique = false, nullable = true, length = 60)
    private String sealPwd;

    /**
     * 抵押合同章编码
     **/
    @Column(name = "SEAL_PLD_CODE", unique = false, nullable = true, length = 60)
    private String sealPldCode;

    /**
     * 抵押合同章名称
     **/
    @Column(name = "SEAL_PLD_NAME", unique = false, nullable = true, length = 60)
    private String sealPldName;

    /**
     * 抵押合同章密码
     **/
    @Column(name = "SEAL_PLD_PWD", unique = false, nullable = true, length = 60)
    private String sealPldPwd;

    /**
     * 区域发展分类
     **/
    @Column(name = "AREA_DEV_CATE_CD", unique = false, nullable = true, length = 1)
    private String areaDevCateCd;

    /**
     * 是否集中作业
     **/
    @Column(name = "IS_JZZY_ORG", unique = false, nullable = true, length = 1)
    private String isJzzyOrg;

    /**
     * 是否小贷
     **/
    @Column(name = "IS_XWD", unique = false, nullable = true, length = 1)
    private String isXwd;

    /**
     * 机构电子章是否启用
     **/
    @Column(name = "SEAL_STATUS", unique = false, nullable = true, length = 1)
    private String sealStatus;

    public CfgSOrgElecSeal() {
        // Not compliant
    }

    /**
     * @return orgNo
     */
    public String getOrgNo() {
        return this.orgNo;
    }

    /**
     * @param orgNo
     */
    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    /**
     * @return orgName
     */
    public String getOrgName() {
        return this.orgName;
    }

    /**
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return sealCode
     */
    public String getSealCode() {
        return this.sealCode;
    }

    /**
     * @param sealCode
     */
    public void setSealCode(String sealCode) {
        this.sealCode = sealCode;
    }

    /**
     * @return sealName
     */
    public String getSealName() {
        return this.sealName;
    }

    /**
     * @param sealName
     */
    public void setSealName(String sealName) {
        this.sealName = sealName;
    }

    /**
     * @return sealPwd
     */
    public String getSealPwd() {
        return this.sealPwd;
    }

    /**
     * @param sealPwd
     */
    public void setSealPwd(String sealPwd) {
        this.sealPwd = sealPwd;
    }

    /**
     * @return sealPldCode
     */
    public String getSealPldCode() {
        return this.sealPldCode;
    }

    /**
     * @param sealPldCode
     */
    public void setSealPldCode(String sealPldCode) {
        this.sealPldCode = sealPldCode;
    }

    /**
     * @return sealPldName
     */
    public String getSealPldName() {
        return this.sealPldName;
    }

    /**
     * @param sealPldName
     */
    public void setSealPldName(String sealPldName) {
        this.sealPldName = sealPldName;
    }

    /**
     * @return sealPldPwd
     */
    public String getSealPldPwd() {
        return this.sealPldPwd;
    }

    /**
     * @param sealPldPwd
     */
    public void setSealPldPwd(String sealPldPwd) {
        this.sealPldPwd = sealPldPwd;
    }

    /**
     * @return areaDevCateCd
     */
    public String getAreaDevCateCd() {
        return this.areaDevCateCd;
    }

    /**
     * @param areaDevCateCd
     */
    public void setAreaDevCateCd(String areaDevCateCd) {
        this.areaDevCateCd = areaDevCateCd;
    }

    /**
     * @return isJzzyOrg
     */
    public String getIsJzzyOrg() {
        return this.isJzzyOrg;
    }

    /**
     * @param isJzzyOrg
     */
    public void setIsJzzyOrg(String isJzzyOrg) {
        this.isJzzyOrg = isJzzyOrg;
    }

    /**
     * @return isXwd
     */
    public String getIsXwd() {
        return this.isXwd;
    }

    /**
     * @param isXwd
     */
    public void setIsXwd(String isXwd) {
        this.isXwd = isXwd;
    }

    /**
     * @return sealStatus
     */
    public String getSealStatus() {
        return this.sealStatus;
    }

    /**
     * @param sealStatus
     */
    public void setSealStatus(String sealStatus) {
        this.sealStatus = sealStatus;
    }


}