/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.CfgServiceExceptionDefEnums;
import cn.com.yusys.yusp.constant.CommonConstant;
import cn.com.yusys.yusp.domain.CfgPrdOrgRel;
import cn.com.yusys.yusp.repository.mapper.CfgPrdOrgRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdOrgRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-07 11:27:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CfgPrdOrgRelService {
    @Autowired
    private CfgPrdOrgRelMapper cfgPrdOrgRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgPrdOrgRel selectByPrimaryKey(String pkId) {
        return cfgPrdOrgRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CfgPrdOrgRel> selectAll(QueryModel model) {
        List<CfgPrdOrgRel> records = (List<CfgPrdOrgRel>) cfgPrdOrgRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CfgPrdOrgRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgPrdOrgRel> list = cfgPrdOrgRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgPrdOrgRel record) {
        return cfgPrdOrgRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgPrdOrgRel record) {
        return cfgPrdOrgRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgPrdOrgRel record) {
        return cfgPrdOrgRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgPrdOrgRel record) {
        return cfgPrdOrgRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cfgPrdOrgRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgPrdOrgRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryCfgPrdOrgRel
     * @方法描述: 新增保存之前根据新增数据进行查询是否已存在
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int queryCfgPrdOrgRel(CfgPrdOrgRel cfgPrdOrgRel) {
        int counts = cfgPrdOrgRelMapper.queryCfgPrdOrgRel(cfgPrdOrgRel);
        if (counts > 0) {
            throw new YuspException(CfgServiceExceptionDefEnums.E_CFG_PRD_ORG_REL_EXISTS_ERROR.getExceptionCode(), CfgServiceExceptionDefEnums.E_CFG_PRD_ORG_REL_EXISTS_ERROR.getExceptionDesc());
        }
        return counts;
    }

    /**
     * @方法名称:copyCfgPrdOrgRel
     * @方法描述: 复制产品时，将老产品配置的产品适用机构联动复制保存
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int copyCfgPrdOrgRel(String prdId, String oldPrdId) {
        return cfgPrdOrgRelMapper.copyCfgPrdOrgRel(prdId, oldPrdId, CommonConstant.ADD_OPR);
    }

    /**
     * @方法名称:deleteCfgPrdOrgRel
     * @方法描述: 删除产品时，将产品配置的产品适用机构联动删除，此处为逻辑删除更新操作类型为“删除”，不进行物理删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteCfgPrdOrgRel(String prdId, String oprType) {
        return cfgPrdOrgRelMapper.deleteCfgPrdOrgRel(prdId, oprType);
    }

}
