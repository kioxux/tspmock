/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgRate
 * @类描述: cfg_rate数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-03-11 11:05:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_rate")
public class CfgRate extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 业务品种
     **/
    @Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 10)
    private String bizType;

    /**
     * 币种
     **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 3)
    private String curType;

    /**
     * 基准利率项目类型
     **/
    @Column(name = "BASE_REMIT_TYPE", unique = false, nullable = true, length = 5)
    private String baseRemitType;

    /**
     * 基准年利率
     **/
    @Column(name = "BASE_RATE_Y", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal baseRateY;

    /**
     * 基准月利率
     **/
    @Column(name = "BASE_RATE_M", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal baseRateM;

    /**
     * 利率浮动上限
     **/
    @Column(name = "RATE_FLOAT_MAX", unique = false, nullable = true, length = 10)
    private java.math.BigDecimal rateFloatMax;

    /**
     * 利率浮动下限
     **/
    @Column(name = "RATE_FLOAT_MIN", unique = false, nullable = true, length = 10)
    private java.math.BigDecimal rateFloatMin;

    /**
     * 期限上限（月）
     **/
    @Column(name = "TERM_MAX", unique = false, nullable = true, length = 22)
    private java.math.BigDecimal termMax;

    /**
     * 期限下限（月）
     **/
    @Column(name = "TERM_MIN", unique = false, nullable = true, length = 22)
    private java.math.BigDecimal termMin;

    /**
     * 期限说明
     **/
    @Column(name = "TERM_DESC", unique = false, nullable = true, length = 250)
    private String termDesc;

    /**
     * 责任人
     **/
    @RedisCacheTranslator(redisCacheKey = "userName")
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 40)
    private String managerId;

    /**
     * 生效日期
     **/
    @Column(name = "INURE_DATE", unique = false, nullable = true, length = 10)
    private String inureDate;

    /**
     * 登记日期
     **/
    @Column(name = "REGI_DATE", unique = false, nullable = true, length = 20)
    private String regiDate;

    public CfgRate() {
        // Not compliant
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return bizType
     */
    public String getBizType() {
        return this.bizType;
    }

    /**
     * @param bizType
     */
    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    /**
     * @return curType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType;
    }

    /**
     * @return baseRemitType
     */
    public String getBaseRemitType() {
        return this.baseRemitType;
    }

    /**
     * @param baseRemitType
     */
    public void setBaseRemitType(String baseRemitType) {
        this.baseRemitType = baseRemitType;
    }

    /**
     * @return baseRateY
     */
    public java.math.BigDecimal getBaseRateY() {
        return this.baseRateY;
    }

    /**
     * @param baseRateY
     */
    public void setBaseRateY(java.math.BigDecimal baseRateY) {
        this.baseRateY = baseRateY;
    }

    /**
     * @return baseRateM
     */
    public java.math.BigDecimal getBaseRateM() {
        return this.baseRateM;
    }

    /**
     * @param baseRateM
     */
    public void setBaseRateM(java.math.BigDecimal baseRateM) {
        this.baseRateM = baseRateM;
    }

    /**
     * @return rateFloatMax
     */
    public java.math.BigDecimal getRateFloatMax() {
        return this.rateFloatMax;
    }

    /**
     * @param rateFloatMax
     */
    public void setRateFloatMax(java.math.BigDecimal rateFloatMax) {
        this.rateFloatMax = rateFloatMax;
    }

    /**
     * @return rateFloatMin
     */
    public java.math.BigDecimal getRateFloatMin() {
        return this.rateFloatMin;
    }

    /**
     * @param rateFloatMin
     */
    public void setRateFloatMin(java.math.BigDecimal rateFloatMin) {
        this.rateFloatMin = rateFloatMin;
    }

    /**
     * @return termMax
     */
    public java.math.BigDecimal getTermMax() {
        return this.termMax;
    }

    /**
     * @param termMax
     */
    public void setTermMax(java.math.BigDecimal termMax) {
        this.termMax = termMax;
    }

    /**
     * @return termMin
     */
    public java.math.BigDecimal getTermMin() {
        return this.termMin;
    }

    /**
     * @param termMin
     */
    public void setTermMin(java.math.BigDecimal termMin) {
        this.termMin = termMin;
    }

    /**
     * @return termDesc
     */
    public String getTermDesc() {
        return this.termDesc;
    }

    /**
     * @param termDesc
     */
    public void setTermDesc(String termDesc) {
        this.termDesc = termDesc;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return inureDate
     */
    public String getInureDate() {
        return this.inureDate;
    }

    /**
     * @param inureDate
     */
    public void setInureDate(String inureDate) {
        this.inureDate = inureDate;
    }

    /**
     * @return regiDate
     */
    public String getRegiDate() {
        return this.regiDate;
    }

    /**
     * @param regiDate
     */
    public void setRegiDate(String regiDate) {
        this.regiDate = regiDate;
    }


}