package cn.com.yusys.yusp.web.server.xdxt0012;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdxt0012.req.Xdxt0012DataReqDto;
import cn.com.yusys.yusp.server.xdxt0012.resp.Xdxt0012DataRespDto;
import cn.com.yusys.yusp.service.server.xdxt0012.Xdxt0012Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDXT0012:根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码")
@RestController
@RequestMapping("/api/cfgxt4bsp")
public class CfgXdxt0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(CfgXdxt0012Resource.class);

    @Autowired
    private Xdxt0012Service xdxt0012Service;

    /**
     * 交易码：xdxt0012
     * 交易描述：根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
     *
     * @param xdxt0012DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码")
    @PostMapping("/xdxt0012")
    protected @ResponseBody
    ResultDto<Xdxt0012DataRespDto> xdxt0012(@Validated @RequestBody Xdxt0012DataReqDto xdxt0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, JSON.toJSONString(xdxt0012DataReqDto));
        Xdxt0012DataRespDto xdxt0012DataRespDto = new Xdxt0012DataRespDto();// 响应Dto:根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
        ResultDto<Xdxt0012DataRespDto> xdxt0012DataResultDto = new ResultDto<>();
        try {
            // 从xdxt0012DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, JSON.toJSONString(xdxt0012DataReqDto));
            xdxt0012DataRespDto = xdxt0012Service.getXdxt0012(xdxt0012DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, JSON.toJSONString(xdxt0012DataReqDto));
            // 封装xdxt0012DataResultDto中正确的返回码和返回信息
            xdxt0012DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxt0012DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, e.getMessage());
            // 封装xdxt0012DataResultDto中异常返回码和返回信息
            xdxt0012DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxt0012DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxt0012DataRespDto到xdxt0012DataResultDto中
        xdxt0012DataResultDto.setData(xdxt0012DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, JSON.toJSONString(xdxt0012DataResultDto));
        return xdxt0012DataResultDto;
    }
}
