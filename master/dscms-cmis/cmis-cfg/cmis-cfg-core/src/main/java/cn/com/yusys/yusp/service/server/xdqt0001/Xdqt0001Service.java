package cn.com.yusys.yusp.service.server.xdqt0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CfgPrdBasicinfo;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CfgPrdBasicinfoMapper;
import cn.com.yusys.yusp.server.xdqt0001.resp.Xdqt0001DataRespDto;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg模块
 * @类名称:
 * @类描述: #服务类
 * @功能描述:
 * @创建人: sunbin
 * @创建时间: 2021-06-05 09:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdqt0001Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdqt0001Service.class);

    @Resource
    private CfgPrdBasicinfoMapper cfgPrdBasicinfoMapper;

    public Xdqt0001DataRespDto getCfgPrdBasicInfoByPrdId(String prdId) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value);
        Xdqt0001DataRespDto xdqt0001DataRespDto = new Xdqt0001DataRespDto();
        try {
            CfgPrdBasicinfo cfgPrdBasicinfo = cfgPrdBasicinfoMapper.selectByPrimaryKey(prdId);
            if (cfgPrdBasicinfo != null) {
                xdqt0001DataRespDto.setPrdName(cfgPrdBasicinfo.getPrdName());
            } else {
                throw BizException.error(null, "999999", "产品编号传入有误");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, e.getMessage());
            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, JSON.toJSONString(xdqt0001DataRespDto));
        return xdqt0001DataRespDto;
    }
}
