/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgHandoverSub
 * @类描述: cfg_handover_sub数据实体类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2020-11-20 13:55:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_handover_sub")
public class CfgHandoverSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "SUB_ID")
    private String subId;

    /**
     * 序列号
     **/
    @Column(name = "PK_ID", unique = false, nullable = false, length = 40)
    private String pkId;

    /**
     * 表编码
     **/
    @Column(name = "TABLE_CODE", unique = false, nullable = true, length = 30)
    private String tableCode;

    /**
     * 表名称
     **/
    @Column(name = "TABLE_NAME", unique = false, nullable = true, length = 100)
    private String tableName;

    /**
     * 执行语句
     **/
    @Column(name = "EXEC_SQL", unique = false, nullable = true, length = 800)
    private String execSql;

    /**
     * 执行顺序
     **/
    @Column(name = "EXEC_SORT", unique = false, nullable = true, length = 5)
    private java.math.BigDecimal execSort;

    /**
     * 登记人
     **/
    @Column(name = "REMARK", unique = false, nullable = true, length = 300)
    private String remark;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 更新人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 更新机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 更新日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    public CfgHandoverSub() {
        // Not compliant
    }

    /**
     * @return subId
     */
    public String getSubId() {
        return this.subId;
    }

    /**
     * @param subId
     */
    public void setSubId(String subId) {
        this.subId = subId;
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return tableCode
     */
    public String getTableCode() {
        return this.tableCode;
    }

    /**
     * @param tableCode
     */
    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    /**
     * @return tableName
     */
    public String getTableName() {
        return this.tableName;
    }

    /**
     * @param tableName
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * @return execSql
     */
    public String getExecSql() {
        return this.execSql;
    }

    /**
     * @param execSql
     */
    public void setExecSql(String execSql) {
        this.execSql = execSql;
    }

    /**
     * @return execSort
     */
    public java.math.BigDecimal getExecSort() {
        return this.execSort;
    }

    /**
     * @param execSort
     */
    public void setExecSort(java.math.BigDecimal execSort) {
        this.execSort = execSort;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }
}