/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgPrdOrgRel;
import cn.com.yusys.yusp.service.CfgPrdOrgRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgPrdOrgRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 张小铅
 * @创建时间: 2020-12-07 11:27:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgprdorgrel")
public class CfgPrdOrgRelResource {
    @Autowired
    private CfgPrdOrgRelService cfgPrdOrgRelService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgPrdOrgRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgPrdOrgRel> list = cfgPrdOrgRelService.selectAll(queryModel);
        return new ResultDto<List<CfgPrdOrgRel>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgPrdOrgRel>> index(QueryModel queryModel) {
        List<CfgPrdOrgRel> list = cfgPrdOrgRelService.selectByModel(queryModel);
        return new ResultDto<List<CfgPrdOrgRel>>(list);
    }

    /**
     * @param QueryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<CfgPrdOrgRel>> selectByModel(@RequestBody QueryModel queryModel) {
        List<CfgPrdOrgRel> list = cfgPrdOrgRelService.selectByModel(queryModel);
        return new ResultDto<List<CfgPrdOrgRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgPrdOrgRel> show(@PathVariable("pkId") String pkId) {
        CfgPrdOrgRel cfgPrdOrgRel = cfgPrdOrgRelService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgPrdOrgRel>(cfgPrdOrgRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgPrdOrgRel> create(@RequestBody CfgPrdOrgRel cfgPrdOrgRel) throws URISyntaxException {
        cfgPrdOrgRelService.insert(cfgPrdOrgRel);
        return new ResultDto<CfgPrdOrgRel>(cfgPrdOrgRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgPrdOrgRel cfgPrdOrgRel) throws URISyntaxException {
        int result = cfgPrdOrgRelService.update(cfgPrdOrgRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgPrdOrgRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgPrdOrgRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:query
     * @函数描述:新增前查询已存在的机构
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCfgPrdOrgRel")
    protected ResultDto<Integer> queryCfgPrdOrgRel(@RequestBody CfgPrdOrgRel cfgPrdOrgRel) throws URISyntaxException {
        int result = cfgPrdOrgRelService.queryCfgPrdOrgRel(cfgPrdOrgRel);
        return new ResultDto<Integer>(result);
    }


}
