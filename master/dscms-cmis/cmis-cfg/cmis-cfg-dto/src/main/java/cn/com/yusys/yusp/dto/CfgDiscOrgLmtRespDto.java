package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgDiscOrgLmt
 * @类描述: cfg_disc_org_lmt数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 17:38:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgDiscOrgLmtRespDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 申请年月 **/
	private String appDate;
	
	/** 机构代码 **/
	private String orgCode;
	
	/** 机构名称 **/
	private String orgName;
	
	/** 申请额度 **/
	private java.math.BigDecimal appAmt;
	
	/** 审批额度 **/
	private java.math.BigDecimal apprAmt;
	
	/** 状态 **/
	private String status;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}

	public String getAppDate() {
		return appDate;
	}

	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}

	/**
	 * @param orgCode
	 */
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode == null ? null : orgCode.trim();
	}
	
    /**
     * @return OrgCode
     */	
	public String getOrgCode() {
		return this.orgCode;
	}
	
	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName == null ? null : orgName.trim();
	}
	
    /**
     * @return OrgName
     */	
	public String getOrgName() {
		return this.orgName;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return AppAmt
     */	
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param apprAmt
	 */
	public void setApprAmt(java.math.BigDecimal apprAmt) {
		this.apprAmt = apprAmt;
	}
	
    /**
     * @return ApprAmt
     */	
	public java.math.BigDecimal getApprAmt() {
		return this.apprAmt;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}


}