package cn.com.yusys.yusp.server.xdsx0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Data：客户准入级别同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0009DataReqDto implements Serializable {
    @JsonProperty(value = "mapsetList")
    private java.util.List<MapsetList> mapsetList;

    public List<MapsetList> getMapsetList() {
        return mapsetList;
    }

    public void setMapsetList(List<MapsetList> mapsetList) {
        this.mapsetList = mapsetList;
    }

    @Override
    public String toString() {
        return "Xdsx0009DataReqDto{" +
                "mapsetList=" + mapsetList +
                '}';
    }
}
