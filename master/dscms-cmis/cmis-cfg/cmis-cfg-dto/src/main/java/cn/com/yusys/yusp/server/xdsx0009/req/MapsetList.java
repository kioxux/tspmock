package cn.com.yusys.yusp.server.xdsx0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：客户准入级别同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class MapsetList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "evalModel")
    private String evalModel;//评级模型
    @JsonProperty(value = "cusEvalAdmitLine")
    private BigDecimal cusEvalAdmitLine;//客户评级准入线
    @JsonProperty(value = "syncDate")
    private String syncDate;//同步日期

    public String getEvalModel() {
        return evalModel;
    }

    public void setEvalModel(String evalModel) {
        this.evalModel = evalModel;
    }

    public BigDecimal getCusEvalAdmitLine() {
        return cusEvalAdmitLine;
    }

    public void setCusEvalAdmitLine(BigDecimal cusEvalAdmitLine) {
        this.cusEvalAdmitLine = cusEvalAdmitLine;
    }

    public String getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(String syncDate) {
        this.syncDate = syncDate;
    }

    @Override
    public String toString() {
        return "MapsetList{" +
                "evalModel='" + evalModel + '\'' +
                ", cusEvalAdmitLine=" + cusEvalAdmitLine +
                ", syncDate='" + syncDate + '\'' +
                '}';
    }
}
