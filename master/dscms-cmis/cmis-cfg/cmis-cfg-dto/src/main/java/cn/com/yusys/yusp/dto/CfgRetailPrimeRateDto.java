/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dto;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgRetailPrimeRate
 * @类描述: cfg_retail_prime_rate数据实体类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:26:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgRetailPrimeRateDto implements Serializable{
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	private String pkId;

	/** 产品名称 **/
	private String prdName;

	/** 产品代码 **/
	private String prdId;

	/** 机构代码 **/
	private String orgCode;

	/** 机构名称 **/
	private String orgName;

	/** 岗位代码 **/
	private String dutyCode;

	/** 岗位名称 **/
	private String dutyName;

	/** 报价利率 **/
	private java.math.BigDecimal offerRate;

	/** 利率下调上限 **/
	private Integer rateMax;

	/** 利率下调下限 **/
	private Integer rateMin;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最后修改人 **/
	private String updId;

	/** 最后修改机构 **/
	private String updBrId;

	/** 最后修改日期 **/
	private String updDate;

	/** 创建时间 **/
	private Date createTime;

	/** 修改时间 **/
	private Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param orgCode
	 */
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

    /**
     * @return orgCode
     */
	public String getOrgCode() {
		return this.orgCode;
	}

	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

    /**
     * @return orgName
     */
	public String getOrgName() {
		return this.orgName;
	}

	/**
	 * @param dutyCode
	 */
	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}

    /**
     * @return dutyCode
     */
	public String getDutyCode() {
		return this.dutyCode;
	}

	/**
	 * @param dutyName
	 */
	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}

    /**
     * @return dutyName
     */
	public String getDutyName() {
		return this.dutyName;
	}

	/**
	 * @param offerRate
	 */
	public void setOfferRate(java.math.BigDecimal offerRate) {
		this.offerRate = offerRate;
	}

    /**
     * @return offerRate
     */
	public java.math.BigDecimal getOfferRate() {
		return this.offerRate;
	}

	/**
	 * @param rateMax
	 */
	public void setRateMax(Integer rateMax) {
		this.rateMax = rateMax;
	}

    /**
     * @return rateMax
     */
	public Integer getRateMax() {
		return this.rateMax;
	}

	/**
	 * @param rateMin
	 */
	public void setRateMin(Integer rateMin) {
		this.rateMin = rateMin;
	}

    /**
     * @return rateMin
     */
	public Integer getRateMin() {
		return this.rateMin;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}