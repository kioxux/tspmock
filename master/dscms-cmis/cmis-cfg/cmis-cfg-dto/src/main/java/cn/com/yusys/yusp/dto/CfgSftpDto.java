package cn.com.yusys.yusp.dto;

import javax.annotation.Generated;
import java.io.Serializable;

public class CfgSftpDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 服务器连接ip **/
    private String host;

    /** 业务场景类型标识 **/
    private String bizScenseType;

    /** 用户名 **/
    private String username;

    /** 密码 **/
    private String password;

    /** 端口号 **/
    private String port;

    /** SFTP服务器下载目录 **/
    private String serverpath;

    /** SFTP文件名下载后缀:xxx.zip **/
    private String serverfilename;

    /** SFTP服务器保存目录 **/
    private String savepath;

    /** 压缩前保存文件名："ret_partner_" +xxx.txt **/
    private String savefilename;

    /** 本地获取文件目录 **/
    private String localpath;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getBizScenseType() {
        return bizScenseType;
    }

    public void setBizScenseType(String bizScenseType) {
        this.bizScenseType = bizScenseType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getServerpath() {
        return serverpath;
    }

    public void setServerpath(String serverpath) {
        this.serverpath = serverpath;
    }

    public String getServerfilename() {
        return serverfilename;
    }

    public void setServerfilename(String serverfilename) {
        this.serverfilename = serverfilename;
    }

    public String getSavepath() {
        return savepath;
    }

    public void setSavepath(String savepath) {
        this.savepath = savepath;
    }

    public String getSavefilename() {
        return savefilename;
    }

    public void setSavefilename(String savefilename) {
        this.savefilename = savefilename;
    }

    public String getLocalpath() {
        return localpath;
    }

    public void setLocalpath(String localpath) {
        this.localpath = localpath;
    }

    @Override
    public String toString() {
        return "CfgSftpDto{" +
                "host='" + host + '\'' +
                ", bizScenseType='" + bizScenseType + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", port='" + port + '\'' +
                ", serverpath='" + serverpath + '\'' +
                ", serverfilename='" + serverfilename + '\'' +
                ", savepath='" + savepath + '\'' +
                ", savefilename='" + savefilename + '\'' +
                ", localpath='" + localpath + '\'' +
                '}';
    }
}
