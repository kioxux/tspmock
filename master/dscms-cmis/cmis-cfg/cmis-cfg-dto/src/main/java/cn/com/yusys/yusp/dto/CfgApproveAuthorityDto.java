package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgApproveAuthorityDto
 * @类描述: CfgApproveAuthorityDto
 * @功能描述: 
 * @创建人: 12651
 * @创建时间: 2021-05-07 16:07:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgApproveAuthorityDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	private String pkId;

	/** 岗位号 **/
	private String dutyCode;

	/** 客户等级 **/
	private String cusLevel;

	/** 债项等级 **/
	private String debtLevel;

	/** 授权金额（元） **/
	private java.math.BigDecimal authAmt;

	/** 规则状态 **/
	private String ruleStatus;

	/** 区分规则类型  **/
	private String ruleType;

	/** 客户类型 **/
	private String cusType;

	/** 最近修改人 **/
	private String updId;

	/** 最近修改机构 **/
	private String updBrId;

	/** 最近修改日期 **/
	private String updDate;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;

	/** 操作类型 STD_ZB_OPER_TYPE **/
	private String oprType;

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getDutyCode() {
		return dutyCode;
	}

	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}

	public String getCusLevel() {
		return cusLevel;
	}

	public void setCusLevel(String cusLevel) {
		this.cusLevel = cusLevel;
	}

	public String getDebtLevel() {
		return debtLevel;
	}

	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel;
	}

	public BigDecimal getAuthAmt() {
		return authAmt;
	}

	public void setAuthAmt(BigDecimal authAmt) {
		this.authAmt = authAmt;
	}

	public String getRuleStatus() {
		return ruleStatus;
	}

	public void setRuleStatus(String ruleStatus) {
		this.ruleStatus = ruleStatus;
	}

	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public String getCusType() {
		return cusType;
	}

	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	public String getUpdId() {
		return updId;
	}

	public void setUpdId(String updId) {
		this.updId = updId;
	}

	public String getUpdBrId() {
		return updBrId;
	}

	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getOprType() {
		return oprType;
	}

	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	@Override
	public String toString() {
		return "CfgApproveAuthorityDto{" +
				"dutyCode='" + dutyCode + '\'' +
				", cusLevel='" + cusLevel + '\'' +
				", debtLevel='" + debtLevel + '\'' +
				", authAmt=" + authAmt +
				", ruleStatus='" + ruleStatus + '\'' +
				", ruleType='" + ruleType + '\'' +
				", cusType='" + cusType + '\'' +
				", updId='" + updId + '\'' +
				", updBrId='" + updBrId + '\'' +
				", updDate='" + updDate + '\'' +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				", oprType='" + oprType + '\'' +
				'}';
	}
}