package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 查询汇率信息响应dto
 * @author :xuchao
 */
public class CfgTfRateDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    /** 币种编号 **/
    private String crcycd;

    /** 开始日期 **/
    private String startDate;

    /** 时间 **/
    private String time;

    /** 结束日期 **/
    private String endTime;

    /** 结汇汇率 **/
    private String exrtst;

    /** 中文币种 **/
    private String curTypeName;

    /** 币种 **/
    private String curType;

    /** 基准价 **/
    private Integer exunit;

    /** 汇买价 **/
    private java.math.BigDecimal csbypr;

    /** 汇卖价 **/
    private java.math.BigDecimal csslpr;

    /** 钞买价 **/
    private java.math.BigDecimal exbypr;

    /** 钞卖价 **/
    private java.math.BigDecimal exslpr;

    /** 中间价 **/
    private java.math.BigDecimal middpr;

    /** WGMDPR **/
    private java.math.BigDecimal wgmdpr;

    /** 汇率 **/
    private java.math.BigDecimal rate;

    /** 登记人 **/
    private String inputId;

    /** 责任人 **/
    private String managerId;

    /** 责任机构 **/
    private String managerBrId;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getExrtst() {
        return exrtst;
    }

    public void setExrtst(String exrtst) {
        this.exrtst = exrtst;
    }

    public String getCurTypeName() {
        return curTypeName;
    }

    public void setCurTypeName(String curTypeName) {
        this.curTypeName = curTypeName;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public Integer getExunit() {
        return exunit;
    }

    public void setExunit(Integer exunit) {
        this.exunit = exunit;
    }

    public BigDecimal getCsbypr() {
        return csbypr;
    }

    public void setCsbypr(BigDecimal csbypr) {
        this.csbypr = csbypr;
    }

    public BigDecimal getCsslpr() {
        return csslpr;
    }

    public void setCsslpr(BigDecimal csslpr) {
        this.csslpr = csslpr;
    }

    public BigDecimal getExbypr() {
        return exbypr;
    }

    public void setExbypr(BigDecimal exbypr) {
        this.exbypr = exbypr;
    }

    public BigDecimal getExslpr() {
        return exslpr;
    }

    public void setExslpr(BigDecimal exslpr) {
        this.exslpr = exslpr;
    }

    public BigDecimal getMiddpr() {
        return middpr;
    }

    public void setMiddpr(BigDecimal middpr) {
        this.middpr = middpr;
    }

    public BigDecimal getWgmdpr() {
        return wgmdpr;
    }

    public void setWgmdpr(BigDecimal wgmdpr) {
        this.wgmdpr = wgmdpr;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "CfgTfRateDto{" +
                "pkId='" + pkId + '\'' +
                ", crcycd='" + crcycd + '\'' +
                ", startDate='" + startDate + '\'' +
                ", time='" + time + '\'' +
                ", endTime='" + endTime + '\'' +
                ", exrtst='" + exrtst + '\'' +
                ", curTypeName='" + curTypeName + '\'' +
                ", curType='" + curType + '\'' +
                ", exunit=" + exunit +
                ", csbypr=" + csbypr +
                ", csslpr=" + csslpr +
                ", exbypr=" + exbypr +
                ", exslpr=" + exslpr +
                ", middpr=" + middpr +
                ", wgmdpr=" + wgmdpr +
                ", rate=" + rate +
                ", inputId='" + inputId + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
