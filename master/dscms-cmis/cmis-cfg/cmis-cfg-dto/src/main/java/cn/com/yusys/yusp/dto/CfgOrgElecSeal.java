/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;

/**、
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrgElecSeal
 * @类描述: cfg_org_elec_seal数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-07-12 08:57:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgOrgElecSeal implements Serializable{
    private static final long serialVersionUID = 1L;
	
	/** 机构代码 **/
	private String orgCode;
	
	/** 机构名称 **/
	private String orgName;
	
	/** 信贷合同章编码 **/
	private String sealCode;
	
	/** 信贷合同章名称 **/
	private String sealName;
	
	/** 信贷合同章密码 **/
	private String sealPassword;
	
	/** 是否启用 **/
	private String isBegin;
	
	/** 抵押合同章编码 **/
	private String gaurSealCode;
	
	/** 抵押合同章名称 **/
	private String gaurSealName;
	
	/** 抵押合同章密码 **/
	private String gaurSealPassword;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private Date createTime;

	/** 修改时间 **/
	private Date updateTime;


	/**
	 * @param orgCode
	 */
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

    /**
     * @return orgCode
     */
	public String getOrgCode() {
		return this.orgCode;
	}

	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

    /**
     * @return orgName
     */
	public String getOrgName() {
		return this.orgName;
	}

	/**
	 * @param sealCode
	 */
	public void setSealCode(String sealCode) {
		this.sealCode = sealCode;
	}

    /**
     * @return sealCode
     */
	public String getSealCode() {
		return this.sealCode;
	}

	/**
	 * @param sealName
	 */
	public void setSealName(String sealName) {
		this.sealName = sealName;
	}

    /**
     * @return sealName
     */
	public String getSealName() {
		return this.sealName;
	}

	/**
	 * @param sealPassword
	 */
	public void setSealPassword(String sealPassword) {
		this.sealPassword = sealPassword;
	}

    /**
     * @return sealPassword
     */
	public String getSealPassword() {
		return this.sealPassword;
	}

	/**
	 * @param isBegin
	 */
	public void setIsBegin(String isBegin) {
		this.isBegin = isBegin;
	}

    /**
     * @return isBegin
     */
	public String getIsBegin() {
		return this.isBegin;
	}

	/**
	 * @param gaurSealCode
	 */
	public void setGaurSealCode(String gaurSealCode) {
		this.gaurSealCode = gaurSealCode;
	}

    /**
     * @return gaurSealCode
     */
	public String getGaurSealCode() {
		return this.gaurSealCode;
	}

	/**
	 * @param gaurSealName
	 */
	public void setGaurSealName(String gaurSealName) {
		this.gaurSealName = gaurSealName;
	}

    /**
     * @return gaurSealName
     */
	public String getGaurSealName() {
		return this.gaurSealName;
	}

	/**
	 * @param gaurSealPassword
	 */
	public void setGaurSealPassword(String gaurSealPassword) {
		this.gaurSealPassword = gaurSealPassword;
	}

    /**
     * @return gaurSealPassword
     */
	public String getGaurSealPassword() {
		return this.gaurSealPassword;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}