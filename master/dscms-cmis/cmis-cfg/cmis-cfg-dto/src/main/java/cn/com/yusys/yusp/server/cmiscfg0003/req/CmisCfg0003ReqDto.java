package cn.com.yusys.yusp.server.cmiscfg0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisCfg0003ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bankNo")           //产品编号
    private String bankNo;

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    @Override
    public String toString() {
        return "CmisCfg0003ReqDto{" +
                "bankNo='" + bankNo + '\'' +
                '}';
    }
}
