package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgCreditLevel
 * @类描述: cfg_credit_level数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 09:24:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgCreditLevelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 配置类型 **/
	private String cfgType;
	
	/** 资产类型 **/
	private String assetType;
	
	/** 信用配置类型 **/
	private String cfgCreditType;
	
	/** 信用等级 **/
	private String creditLevel;
	
	/** 抵质押率 **/
	private java.math.BigDecimal pldimnRate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 创建时间 **/
	private Date createTime;

	/** 修改时间 **/
	private Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}

    /**
     * @return PkId
     */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param cfgType
	 */
	public void setCfgType(String cfgType) {
		this.cfgType = cfgType == null ? null : cfgType.trim();
	}

    /**
     * @return CfgType
     */
	public String getCfgType() {
		return this.cfgType;
	}

	/**
	 * @param assetType
	 */
	public void setAssetType(String assetType) {
		this.assetType = assetType == null ? null : assetType.trim();
	}

    /**
     * @return AssetType
     */
	public String getAssetType() {
		return this.assetType;
	}

	/**
	 * @param cfgCreditType
	 */
	public void setCfgCreditType(String cfgCreditType) {
		this.cfgCreditType = cfgCreditType == null ? null : cfgCreditType.trim();
	}

    /**
     * @return CfgCreditType
     */
	public String getCfgCreditType() {
		return this.cfgCreditType;
	}

	/**
	 * @param creditLevel
	 */
	public void setCreditLevel(String creditLevel) {
		this.creditLevel = creditLevel == null ? null : creditLevel.trim();
	}

    /**
     * @return CreditLevel
     */
	public String getCreditLevel() {
		return this.creditLevel;
	}

	/**
	 * @param pldimnRate
	 */
	public void setPldimnRate(java.math.BigDecimal pldimnRate) {
		this.pldimnRate = pldimnRate;
	}

    /**
     * @return PldimnRate
     */
	public java.math.BigDecimal getPldimnRate() {
		return this.pldimnRate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

    /**
     * @return OprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

    /**
     * @return InputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

    /**
     * @return InputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

    /**
     * @return InputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

    /**
     * @return UpdId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

    /**
     * @return UpdBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

    /**
     * @return UpdDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}

    /**
     * @return ManagerId
     */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}

    /**
     * @return ManagerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return CreateTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return UpdateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}