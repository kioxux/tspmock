package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncConfDefFmt
 * @类描述: fnc_conf_def_fmt数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-03-01 15:11:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class FncConfDefFmtDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 报表样式编号 **/
	private String styleId;
	/** 项目编号 **/
	private String itemId;
	
	/** 顺序编号 **/
	private Integer fncConfOrder;
	
	/** 栏位 STD_ZB_FNC_COTES **/
	private String fncConfCotes;
	
	/** 行次标识 STD_ZB_FNCCONFROW **/
	private String fncConfRowFlg;
	
	/** 层次 **/
	private Integer fncConfIndent;
	
	/** 前缀 **/
	private String fncConfPrefix;
	
	/** 项目编辑方式 STD_ZB_FNCITEMEDT **/
	private String fncItemEditTyp;
	
	/** 显示数值 **/
	private java.math.BigDecimal fncConfDispAmt;
	
	/** 追加行数 **/
	private Integer fncCnfAppRow;
	
	/** 超链接 **/
	private String fncConfUrl;
	
	/** 检查公式 **/
	private String fncConfChkFrm;
	
	/** 计算公式 **/
	private String fncConfCalFrm;
	
	/** 默认现实类型 **/
	private String fncConfDispTpy;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param styleId
	 */
	public void setStyleId(String styleId) {
		this.styleId = styleId == null ? null : styleId.trim();
	}
	
    /**
     * @return StyleId
     */	
	public String getStyleId() {
		return this.styleId;
	}
	
	/**
	 * @param itemId
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId == null ? null : itemId.trim();
	}
	
    /**
     * @return ItemId
     */	
	public String getItemId() {
		return this.itemId;
	}
	
	/**
	 * @param fncConfOrder
	 */
	public void setFncConfOrder(Integer fncConfOrder) {
		this.fncConfOrder = fncConfOrder;
	}
	
    /**
     * @return FncConfOrder
     */	
	public Integer getFncConfOrder() {
		return this.fncConfOrder;
	}
	
	/**
	 * @param fncConfCotes
	 */
	public void setFncConfCotes(String fncConfCotes) {
		this.fncConfCotes = fncConfCotes == null ? null : fncConfCotes.trim();
	}
	
    /**
     * @return FncConfCotes
     */	
	public String getFncConfCotes() {
		return this.fncConfCotes;
	}
	
	/**
	 * @param fncConfRowFlg
	 */
	public void setFncConfRowFlg(String fncConfRowFlg) {
		this.fncConfRowFlg = fncConfRowFlg == null ? null : fncConfRowFlg.trim();
	}
	
    /**
     * @return FncConfRowFlg
     */	
	public String getFncConfRowFlg() {
		return this.fncConfRowFlg;
	}
	
	/**
	 * @param fncConfIndent
	 */
	public void setFncConfIndent(Integer fncConfIndent) {
		this.fncConfIndent = fncConfIndent;
	}
	
    /**
     * @return FncConfIndent
     */	
	public Integer getFncConfIndent() {
		return this.fncConfIndent;
	}
	
	/**
	 * @param fncConfPrefix
	 */
	public void setFncConfPrefix(String fncConfPrefix) {
		this.fncConfPrefix = fncConfPrefix == null ? null : fncConfPrefix.trim();
	}
	
    /**
     * @return FncConfPrefix
     */	
	public String getFncConfPrefix() {
		return this.fncConfPrefix;
	}
	
	/**
	 * @param fncItemEditTyp
	 */
	public void setFncItemEditTyp(String fncItemEditTyp) {
		this.fncItemEditTyp = fncItemEditTyp == null ? null : fncItemEditTyp.trim();
	}
	
    /**
     * @return FncItemEditTyp
     */	
	public String getFncItemEditTyp() {
		return this.fncItemEditTyp;
	}
	
	/**
	 * @param fncConfDispAmt
	 */
	public void setFncConfDispAmt(java.math.BigDecimal fncConfDispAmt) {
		this.fncConfDispAmt = fncConfDispAmt;
	}
	
    /**
     * @return FncConfDispAmt
     */	
	public java.math.BigDecimal getFncConfDispAmt() {
		return this.fncConfDispAmt;
	}
	
	/**
	 * @param fncCnfAppRow
	 */
	public void setFncCnfAppRow(Integer fncCnfAppRow) {
		this.fncCnfAppRow = fncCnfAppRow;
	}
	
    /**
     * @return FncCnfAppRow
     */	
	public Integer getFncCnfAppRow() {
		return this.fncCnfAppRow;
	}
	
	/**
	 * @param fncConfUrl
	 */
	public void setFncConfUrl(String fncConfUrl) {
		this.fncConfUrl = fncConfUrl == null ? null : fncConfUrl.trim();
	}
	
    /**
     * @return FncConfUrl
     */	
	public String getFncConfUrl() {
		return this.fncConfUrl;
	}
	
	/**
	 * @param fncConfChkFrm
	 */
	public void setFncConfChkFrm(String fncConfChkFrm) {
		this.fncConfChkFrm = fncConfChkFrm == null ? null : fncConfChkFrm.trim();
	}
	
    /**
     * @return FncConfChkFrm
     */	
	public String getFncConfChkFrm() {
		return this.fncConfChkFrm;
	}
	
	/**
	 * @param fncConfCalFrm
	 */
	public void setFncConfCalFrm(String fncConfCalFrm) {
		this.fncConfCalFrm = fncConfCalFrm == null ? null : fncConfCalFrm.trim();
	}
	
    /**
     * @return FncConfCalFrm
     */	
	public String getFncConfCalFrm() {
		return this.fncConfCalFrm;
	}
	
	/**
	 * @param fncConfDispTpy
	 */
	public void setFncConfDispTpy(String fncConfDispTpy) {
		this.fncConfDispTpy = fncConfDispTpy == null ? null : fncConfDispTpy.trim();
	}
	
    /**
     * @return FncConfDispTpy
     */	
	public String getFncConfDispTpy() {
		return this.fncConfDispTpy;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}