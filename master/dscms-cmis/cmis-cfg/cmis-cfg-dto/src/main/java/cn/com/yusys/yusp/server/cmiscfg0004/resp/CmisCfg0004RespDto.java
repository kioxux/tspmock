package cn.com.yusys.yusp.server.cmiscfg0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisCfg0004RespDto implements Serializable {

    private static final long serialVersionUID = 3183189902744959052L;

    /**
     * 总行行号
     */
    @JsonProperty(value = "superBankNo")
    private String superBankNo;
    /**
     * 总行BIC_CODE
     */
    @JsonProperty(value = "superBicCode")
    private String superBicCode;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    public String getSuperBankNo() {
        return superBankNo;
    }

    public void setSuperBankNo(String superBankNo) {
        this.superBankNo = superBankNo;
    }

    public String getSuperBicCode() {
        return superBicCode;
    }

    public void setSuperBicCode(String superBicCode) {
        this.superBicCode = superBicCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "CmisCfg0004RespDto{" +
                "superBankNo='" + superBankNo + '\'' +
                ", superBicCode='" + superBicCode + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
