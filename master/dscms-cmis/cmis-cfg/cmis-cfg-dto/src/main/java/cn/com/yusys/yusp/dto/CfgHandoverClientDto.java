package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class CfgHandoverClientDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 序列号 **/
    private String pkId;

    /** 移交方式 **/
    private String handoverWay;

    /** 移交范围 **/
    private String handoverScope;

    /** 扩展处理 **/
    private String extClass;

    /** 备注 **/
    private String remark;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 更新人 **/
    private String updId;

    /** 更新机构 **/
    private String updBrId;

    /** 更新日期 **/
    private String updDate;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getHandoverWay() {
        return handoverWay;
    }

    public void setHandoverWay(String handoverWay) {
        this.handoverWay = handoverWay;
    }

    public String getHandoverScope() {
        return handoverScope;
    }

    public void setHandoverScope(String handoverScope) {
        this.handoverScope = handoverScope;
    }

    public String getExtClass() {
        return extClass;
    }

    public void setExtClass(String extClass) {
        this.extClass = extClass;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }
}
