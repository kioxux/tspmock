package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncConfItems
 * @类描述: fnc_conf_items数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-03-01 15:11:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class FncConfItemsDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 项目编号 **/
	private String itemId;
	
	/** 项目名称 **/
	private String itemName;
	
	/** 财报类型 STD_ZB_FNC_TYP **/
	private String fncConfTyp;
	
	/** 新旧报表标志 STD_ZB_FNC_ON_TYP **/
	private String fncNoFlg;
	
	/** 单位 **/
	private String itemUnit;
	
	/** 超链接 **/
	private String url;
	
	/** 公式 **/
	private String formula;
	
	/** 映射条目ID **/
	private String defItemId;
	
	/** 备注 **/
	private String remark;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param itemId
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId == null ? null : itemId.trim();
	}
	
    /**
     * @return ItemId
     */	
	public String getItemId() {
		return this.itemId;
	}
	
	/**
	 * @param itemName
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName == null ? null : itemName.trim();
	}
	
    /**
     * @return ItemName
     */	
	public String getItemName() {
		return this.itemName;
	}
	
	/**
	 * @param fncConfTyp
	 */
	public void setFncConfTyp(String fncConfTyp) {
		this.fncConfTyp = fncConfTyp == null ? null : fncConfTyp.trim();
	}
	
    /**
     * @return FncConfTyp
     */	
	public String getFncConfTyp() {
		return this.fncConfTyp;
	}
	
	/**
	 * @param fncNoFlg
	 */
	public void setFncNoFlg(String fncNoFlg) {
		this.fncNoFlg = fncNoFlg == null ? null : fncNoFlg.trim();
	}
	
    /**
     * @return FncNoFlg
     */	
	public String getFncNoFlg() {
		return this.fncNoFlg;
	}
	
	/**
	 * @param itemUnit
	 */
	public void setItemUnit(String itemUnit) {
		this.itemUnit = itemUnit == null ? null : itemUnit.trim();
	}
	
    /**
     * @return ItemUnit
     */	
	public String getItemUnit() {
		return this.itemUnit;
	}
	
	/**
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url == null ? null : url.trim();
	}
	
    /**
     * @return Url
     */	
	public String getUrl() {
		return this.url;
	}
	
	/**
	 * @param formula
	 */
	public void setFormula(String formula) {
		this.formula = formula == null ? null : formula.trim();
	}
	
    /**
     * @return Formula
     */	
	public String getFormula() {
		return this.formula;
	}
	
	/**
	 * @param defItemId
	 */
	public void setDefItemId(String defItemId) {
		this.defItemId = defItemId == null ? null : defItemId.trim();
	}
	
    /**
     * @return DefItemId
     */	
	public String getDefItemId() {
		return this.defItemId;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}