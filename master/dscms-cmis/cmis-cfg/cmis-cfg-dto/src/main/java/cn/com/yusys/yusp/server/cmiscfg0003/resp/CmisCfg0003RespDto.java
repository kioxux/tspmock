package cn.com.yusys.yusp.server.cmiscfg0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisCfg0003RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    @JsonProperty(value = "superBankNo")
    private String superBankNo;//结果信息

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getSuperBankNo() {
        return superBankNo;
    }

    public void setSuperBankNo(String superBankNo) {
        this.superBankNo = superBankNo;
    }

    @Override
    public String toString() {
        return "CmisCfg0003RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", superBankNo='" + superBankNo + '\'' +
                '}';
    }
}
