/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgUserOftUseFunc
 * @类描述: cfg_user_oft_use_func数据实体类
 * @功能描述: 
 * @创建人: www58
 * @创建时间: 2021-01-27 16:34:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgUserOftUseFuncClientDto implements Serializable {
    private static final long serialVersionUID = 1L;
	/** 主键 **/
	private String pkId;

	/** 用户登录代码 **/
	private String loginCode;

	/** 功能ID **/
	private String funcId;

	/** 功能名称 **/
	private String funcName;

	/** 登记日期 **/
	private String inputDate;

	/** 更新日期 **/
	private String updDate;

	/** 操作类型 **/
	private String oprType;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param loginCode
	 */
	public void setLoginCode(String loginCode) {
		this.loginCode = loginCode;
	}

	/**
	 * @return loginCode
	 */
	public String getLoginCode() {
		return this.loginCode;
	}

	/**
	 * @param funcId
	 */
	public void setFuncId(String funcId) {
		this.funcId = funcId;
	}

	/**
	 * @return funcId
	 */
	public String getFuncId() {
		return this.funcId;
	}

	/**
	 * @param funcName
	 */
	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	/**
	 * @return funcName
	 */
	public String getFuncName() {
		return this.funcName;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}
}