package cn.com.yusys.yusp.server.cmiscfg0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：新增首页提醒内容 wb_msg_notice
 * add by zhangjw 20210630
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCfg0001ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;//业务流水号
    @JsonProperty(value = "messageType")
    private String messageType;//消息类型
    @JsonProperty(value = "content")
    private String content;//内容
    @JsonProperty(value = "messageStatus")
    private String messageStatus;//消息状态
    @JsonProperty(value = "pubTime")
    private String pubTime;//发布时间
    @JsonProperty(value = "receiverType")
    private String receiverType;//接收人类型
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期
    @JsonProperty(value = "updId")
    private String updId;//最近修改人
    @JsonProperty(value = "updBrId")
    private String updBrId;//最近修改机构
    @JsonProperty(value = "updDate")
    private String updDate;//最近修改日期
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//主管机构

    public String getSerno() {
        return serno;
    }

    public String getMessageType() {
        return messageType;
    }

    public String getContent() {
        return content;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public String getPubTime() {
        return pubTime;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public String getInputId() {
        return inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public void setPubTime(String pubTime) {
        this.pubTime = pubTime;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    @Override
    public String toString() {
        return "CmisCfg0001ReqDto{" +
                "serno='" + serno + '\'' +
                ", messageType='" + messageType + '\'' +
                ", content='" + content + '\'' +
                ", messageStatus='" + messageStatus + '\'' +
                ", pubTime='" + pubTime + '\'' +
                ", receiverType='" + receiverType + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                '}';
    }
}
