package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgAccountClassChoose
 * @类描述: cfg_account_class_choose数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 16:37:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgAccountClassChooseDto implements Serializable{

	private String rtnCode;
	private String rtnMsg;

	private String seqNo;
	private String publicPerson;
	private String accountClass;
	private String hxAccountClass;
	private String accountClassName;
	private String hxAccountClassName;
	private String bizTypeSub;
	private String cusType;
	private String cityVillage;
	private String factoryType;
	private String mainDeal;
	private String farmFlag;
	private String farmDirection;
	private String directionOption;
	private String guarMode;
	private String loanType;
	private String loanTerm;

	public String getRtnCode() {
		return rtnCode;
	}

	public void setRtnCode(String rtnCode) {
		this.rtnCode = rtnCode;
	}

	public String getRtnMsg() {
		return rtnMsg;
	}

	public void setRtnMsg(String rtnMsg) {
		this.rtnMsg = rtnMsg;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public String getPublicPerson() {
		return publicPerson;
	}

	public void setPublicPerson(String publicPerson) {
		this.publicPerson = publicPerson;
	}

	public String getAccountClass() {
		return accountClass;
	}

	public void setAccountClass(String accountClass) {
		this.accountClass = accountClass;
	}

	public String getHxAccountClass() {
		return hxAccountClass;
	}

	public void setHxAccountClass(String hxAccountClass) {
		this.hxAccountClass = hxAccountClass;
	}

	public String getAccountClassName() {
		return accountClassName;
	}

	public void setAccountClassName(String accountClassName) {
		this.accountClassName = accountClassName;
	}

	public String getHxAccountClassName() {
		return hxAccountClassName;
	}

	public void setHxAccountClassName(String hxAccountClassName) {
		this.hxAccountClassName = hxAccountClassName;
	}

	public String getBizTypeSub() {
		return bizTypeSub;
	}

	public void setBizTypeSub(String bizTypeSub) {
		this.bizTypeSub = bizTypeSub;
	}

	public String getCusType() {
		return cusType;
	}

	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	public String getCityVillage() {
		return cityVillage;
	}

	public void setCityVillage(String cityVillage) {
		this.cityVillage = cityVillage;
	}

	public String getFactoryType() {
		return factoryType;
	}

	public void setFactoryType(String factoryType) {
		this.factoryType = factoryType;
	}

	public String getMainDeal() {
		return mainDeal;
	}

	public void setMainDeal(String mainDeal) {
		this.mainDeal = mainDeal;
	}

	public String getFarmFlag() {
		return farmFlag;
	}

	public void setFarmFlag(String farmFlag) {
		this.farmFlag = farmFlag;
	}

	public String getFarmDirection() {
		return farmDirection;
	}

	public void setFarmDirection(String farmDirection) {
		this.farmDirection = farmDirection;
	}

	public String getDirectionOption() {
		return directionOption;
	}

	public void setDirectionOption(String directionOption) {
		this.directionOption = directionOption;
	}

	public String getGuarMode() {
		return guarMode;
	}

	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public String getLoanTerm() {
		return loanTerm;
	}

	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}

	@Override
	public String toString() {
		return "CfgAccountClassChooseDto{" +
				"publicPerson='" + publicPerson + '\'' +
				", bizTypeSub='" + bizTypeSub + '\'' +
				", cusType='" + cusType + '\'' +
				", cityVillage='" + cityVillage + '\'' +
				", factoryType='" + factoryType + '\'' +
				", mainDeal='" + mainDeal + '\'' +
				", farmFlag='" + farmFlag + '\'' +
				", farmDirection='" + farmDirection + '\'' +
				", directionOption='" + directionOption + '\'' +
				", guarMode='" + guarMode + '\'' +
				", loanType='" + loanType + '\'' +
				", loanTerm='" + loanTerm + '\'' +
				'}';
	}
}