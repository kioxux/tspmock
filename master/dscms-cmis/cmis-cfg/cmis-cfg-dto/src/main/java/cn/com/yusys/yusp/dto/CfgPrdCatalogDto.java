package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class CfgPrdCatalogDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 关联表中的PKID
     */
    private String pkId;
    /**
     * 目录编号
     **/
    private String catalogId;

    /**
     * 目录名称
     **/
    private String catalogName;

    /**
     * 目录层级
     **/
    private String catalogLevelId;

    /**
     * 上级目录编码
     **/
    private String supCatalogId;

    /**
     * 上级目录名称
     **/
    private String supCatalogName;

    /**
     * 备注
     **/
    private String remark;

    /**
     * 序号
     **/
    private java.math.BigDecimal serialNumber;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 最后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    private String oprType;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public String getCatalogLevelId() {
        return catalogLevelId;
    }

    public void setCatalogLevelId(String catalogLevelId) {
        this.catalogLevelId = catalogLevelId;
    }

    public String getSupCatalogId() {
        return supCatalogId;
    }

    public void setSupCatalogId(String supCatalogId) {
        this.supCatalogId = supCatalogId;
    }

    public String getSupCatalogName() {
        return supCatalogName;
    }

    public void setSupCatalogName(String supCatalogName) {
        this.supCatalogName = supCatalogName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(BigDecimal serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }
}
