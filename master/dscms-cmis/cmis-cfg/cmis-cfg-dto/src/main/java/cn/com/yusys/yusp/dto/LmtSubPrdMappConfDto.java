/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtSubPrdMappConf
 * @类描述: lmt_sub_prd_mapp_conf数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-02 17:08:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSubPrdMappConfDto implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** FK_PKID **/
	private String fkPkid;
	
	/** 是否顶级产品 **/
	private String isHeadPrd;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 上级产品编号 **/
	private String hPrdId;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param fkPkid
	 */
	public void setFkPkid(String fkPkid) {
		this.fkPkid = fkPkid;
	}
	
    /**
     * @return fkPkid
     */
	public String getFkPkid() {
		return this.fkPkid;
	}
	
	/**
	 * @param isHeadPrd
	 */
	public void setIsHeadPrd(String isHeadPrd) {
		this.isHeadPrd = isHeadPrd;
	}
	
    /**
     * @return isHeadPrd
     */
	public String getIsHeadPrd() {
		return this.isHeadPrd;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param hPrdId
	 */
	public void setHPrdId(String hPrdId) {
		this.hPrdId = hPrdId;
	}
	
    /**
     * @return hPrdId
     */
	public String getHPrdId() {
		return this.hPrdId;
	}


}