package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cfg-dto模块
 * @类名称: CfgWyClassRelClientDto
 * @类描述: cfg_wy_class_rel数据实体类
 * @功能描述: 
 * @创建人: shangzy
 * @创建时间: 2021-09-04 10:45:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgWyClassRelClientDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 科目号 **/
	private String subjectNo;
	
	/** 贷款用途 **/
	private String loanPurp;
	
	/** 产品代码 **/
	private String prdCode;
	
	/** 是否农业 **/
	private String isNy;
	
	/** 是否长期 **/
	private String isCq;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private Date createTime;
	
	/** 修改时间 **/
	private Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param subjectNo
	 */
	public void setSubjectNo(String subjectNo) {
		this.subjectNo = subjectNo == null ? null : subjectNo.trim();
	}
	
    /**
     * @return SubjectNo
     */	
	public String getSubjectNo() {
		return this.subjectNo;
	}
	
	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp == null ? null : loanPurp.trim();
	}
	
    /**
     * @return LoanPurp
     */	
	public String getLoanPurp() {
		return this.loanPurp;
	}
	
	/**
	 * @param prdCode
	 */
	public void setPrdCode(String prdCode) {
		this.prdCode = prdCode == null ? null : prdCode.trim();
	}
	
    /**
     * @return PrdCode
     */	
	public String getPrdCode() {
		return this.prdCode;
	}
	
	/**
	 * @param isNy
	 */
	public void setIsNy(String isNy) {
		this.isNy = isNy == null ? null : isNy.trim();
	}
	
    /**
     * @return IsNy
     */	
	public String getIsNy() {
		return this.isNy;
	}
	
	/**
	 * @param isCq
	 */
	public void setIsCq(String isCq) {
		this.isCq = isCq == null ? null : isCq.trim();
	}
	
    /**
     * @return IsCq
     */	
	public String getIsCq() {
		return this.isCq;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public Date getUpdateTime() {
		return this.updateTime;
	}


}