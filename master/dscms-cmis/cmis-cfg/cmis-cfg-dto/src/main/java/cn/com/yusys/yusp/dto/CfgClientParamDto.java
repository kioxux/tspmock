package cn.com.yusys.yusp.dto;

import java.util.Map;

/**
 * cmis-cfg对外提供参数查询接口
 */
public class CfgClientParamDto {
    //入参部分
    private CfgBizParamInfoClientDto cfgBizParamInfoClientDto;

    //出参部分
    private String rtnCode;
    private String rtnMsg;

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnCode() {
        return rtnCode;
    }

    public CfgBizParamInfoClientDto getCfgBizParamInfoClientDto() {
        return cfgBizParamInfoClientDto;
    }

    public void setCfgBizParamInfoClientDto(CfgBizParamInfoClientDto cfgBizParamInfoClientDto) {
        this.cfgBizParamInfoClientDto = cfgBizParamInfoClientDto;
    }
}
