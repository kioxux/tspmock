package cn.com.yusys.yusp.server.cmiscfg0004.req;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class CmisCfg0004ReqDto implements Serializable {
    private static final long serialVersionUID = 7279588372796493544L;

    /**
     * 行号
     */
    private String bankNo;

    /**
     * bic_code
     */
    private String bicCode;

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getBicCode() {
        return bicCode;
    }

    public void setBicCode(String bicCode) {
        this.bicCode = bicCode;
    }

    @Override
    public String toString() {
        return "CmisCfg0004ReqDto{" +
                "bankNo='" + bankNo + '\'' +
                ", bicCode='" + bicCode + '\'' +
                '}';
    }
}
