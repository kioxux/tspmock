package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/5/8 12:49 下午
 * @desc 产品树返回对象
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class CfgPrdBasicinfoTranDto implements Serializable {
    private static final long serialVersionUID = 1L;
    // 编号
    private String id;
    //中文名
    private String label;
    //目录编号
    private String catalogId;
    //是否选中
    private String check;

    private List<CfgPrdBasicinfoTranDto> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public List<CfgPrdBasicinfoTranDto> getChildren() {
        return children;
    }

    public void setChildren(List<CfgPrdBasicinfoTranDto> children) {
        this.children = children;
    }
}
