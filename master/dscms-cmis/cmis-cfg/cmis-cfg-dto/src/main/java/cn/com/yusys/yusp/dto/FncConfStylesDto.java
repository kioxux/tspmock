package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncConfStyles
 * @类描述: fnc_conf_styles数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-03-01 15:11:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class FncConfStylesDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 报表样式编号 **/
	private String styleId;
	
	/** 报表名称 **/
	private String fncName;
	
	/** 显示名称 **/
	private String fncConfDisName;
	
	/** 所属报表种类 STD_ZB_FNC_TYP **/
	private String fncConfTyp;
	
	/** 数据列数 STD_ZB_FNC_COL **/
	private String fncConfDataCol;
	
	/** 栏位 STD_ZB_FNC_COTES **/
	private String fncConfCotes;
	
	/** 新旧报表标志 STD_ZB_FNC_ON_TYP **/
	private String noInd;
	
	/** 企事业报表标志 STD_ZB_FNC_COMIND **/
	private String comInd;
	
	/** 表头左侧描述 **/
	private String headLeft;
	
	/** 表尾右侧描述 **/
	private String foodRight;
	
	/** 表尾中部描述 **/
	private String foodCenter;
	
	/** 表尾左侧描述 **/
	private String foodLeft;
	
	/** 表头右侧描述 **/
	private String headRight;
	
	/** 表头中部描述 **/
	private String headCenter;
	
	/** 第一列数据描述 **/
	private String dataDec1;
	
	/** 第二列数据描述 **/
	private String dataDec2;
	
	/** 第三列数据描述 **/
	private String dataDec3;
	
	/** 第四列数据描述 **/
	private String dataDec4;
	
	/** 第五列数据描述 **/
	private String dataDec5;
	
	/** 第六列数据描述 **/
	private String dataDec6;
	
	/** 第七列数据描述 **/
	private String dataDec7;
	
	/** 第八列数据描述 **/
	private String dataDec8;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param styleId
	 */
	public void setStyleId(String styleId) {
		this.styleId = styleId == null ? null : styleId.trim();
	}
	
    /**
     * @return StyleId
     */	
	public String getStyleId() {
		return this.styleId;
	}
	
	/**
	 * @param fncName
	 */
	public void setFncName(String fncName) {
		this.fncName = fncName == null ? null : fncName.trim();
	}
	
    /**
     * @return FncName
     */	
	public String getFncName() {
		return this.fncName;
	}
	
	/**
	 * @param fncConfDisName
	 */
	public void setFncConfDisName(String fncConfDisName) {
		this.fncConfDisName = fncConfDisName == null ? null : fncConfDisName.trim();
	}
	
    /**
     * @return FncConfDisName
     */	
	public String getFncConfDisName() {
		return this.fncConfDisName;
	}
	
	/**
	 * @param fncConfTyp
	 */
	public void setFncConfTyp(String fncConfTyp) {
		this.fncConfTyp = fncConfTyp == null ? null : fncConfTyp.trim();
	}
	
    /**
     * @return FncConfTyp
     */	
	public String getFncConfTyp() {
		return this.fncConfTyp;
	}
	
	/**
	 * @param fncConfDataCol
	 */
	public void setFncConfDataCol(String fncConfDataCol) {
		this.fncConfDataCol = fncConfDataCol == null ? null : fncConfDataCol.trim();
	}
	
    /**
     * @return FncConfDataCol
     */	
	public String getFncConfDataCol() {
		return this.fncConfDataCol;
	}
	
	/**
	 * @param fncConfCotes
	 */
	public void setFncConfCotes(String fncConfCotes) {
		this.fncConfCotes = fncConfCotes == null ? null : fncConfCotes.trim();
	}
	
    /**
     * @return FncConfCotes
     */	
	public String getFncConfCotes() {
		return this.fncConfCotes;
	}
	
	/**
	 * @param noInd
	 */
	public void setNoInd(String noInd) {
		this.noInd = noInd == null ? null : noInd.trim();
	}
	
    /**
     * @return NoInd
     */	
	public String getNoInd() {
		return this.noInd;
	}
	
	/**
	 * @param comInd
	 */
	public void setComInd(String comInd) {
		this.comInd = comInd == null ? null : comInd.trim();
	}
	
    /**
     * @return ComInd
     */	
	public String getComInd() {
		return this.comInd;
	}
	
	/**
	 * @param headLeft
	 */
	public void setHeadLeft(String headLeft) {
		this.headLeft = headLeft == null ? null : headLeft.trim();
	}
	
    /**
     * @return HeadLeft
     */	
	public String getHeadLeft() {
		return this.headLeft;
	}
	
	/**
	 * @param foodRight
	 */
	public void setFoodRight(String foodRight) {
		this.foodRight = foodRight == null ? null : foodRight.trim();
	}
	
    /**
     * @return FoodRight
     */	
	public String getFoodRight() {
		return this.foodRight;
	}
	
	/**
	 * @param foodCenter
	 */
	public void setFoodCenter(String foodCenter) {
		this.foodCenter = foodCenter == null ? null : foodCenter.trim();
	}
	
    /**
     * @return FoodCenter
     */	
	public String getFoodCenter() {
		return this.foodCenter;
	}
	
	/**
	 * @param foodLeft
	 */
	public void setFoodLeft(String foodLeft) {
		this.foodLeft = foodLeft == null ? null : foodLeft.trim();
	}
	
    /**
     * @return FoodLeft
     */	
	public String getFoodLeft() {
		return this.foodLeft;
	}
	
	/**
	 * @param headRight
	 */
	public void setHeadRight(String headRight) {
		this.headRight = headRight == null ? null : headRight.trim();
	}
	
    /**
     * @return HeadRight
     */	
	public String getHeadRight() {
		return this.headRight;
	}
	
	/**
	 * @param headCenter
	 */
	public void setHeadCenter(String headCenter) {
		this.headCenter = headCenter == null ? null : headCenter.trim();
	}
	
    /**
     * @return HeadCenter
     */	
	public String getHeadCenter() {
		return this.headCenter;
	}
	
	/**
	 * @param dataDec1
	 */
	public void setDataDec1(String dataDec1) {
		this.dataDec1 = dataDec1 == null ? null : dataDec1.trim();
	}
	
    /**
     * @return DataDec1
     */	
	public String getDataDec1() {
		return this.dataDec1;
	}
	
	/**
	 * @param dataDec2
	 */
	public void setDataDec2(String dataDec2) {
		this.dataDec2 = dataDec2 == null ? null : dataDec2.trim();
	}
	
    /**
     * @return DataDec2
     */	
	public String getDataDec2() {
		return this.dataDec2;
	}
	
	/**
	 * @param dataDec3
	 */
	public void setDataDec3(String dataDec3) {
		this.dataDec3 = dataDec3 == null ? null : dataDec3.trim();
	}
	
    /**
     * @return DataDec3
     */	
	public String getDataDec3() {
		return this.dataDec3;
	}
	
	/**
	 * @param dataDec4
	 */
	public void setDataDec4(String dataDec4) {
		this.dataDec4 = dataDec4 == null ? null : dataDec4.trim();
	}
	
    /**
     * @return DataDec4
     */	
	public String getDataDec4() {
		return this.dataDec4;
	}
	
	/**
	 * @param dataDec5
	 */
	public void setDataDec5(String dataDec5) {
		this.dataDec5 = dataDec5 == null ? null : dataDec5.trim();
	}
	
    /**
     * @return DataDec5
     */	
	public String getDataDec5() {
		return this.dataDec5;
	}
	
	/**
	 * @param dataDec6
	 */
	public void setDataDec6(String dataDec6) {
		this.dataDec6 = dataDec6 == null ? null : dataDec6.trim();
	}
	
    /**
     * @return DataDec6
     */	
	public String getDataDec6() {
		return this.dataDec6;
	}
	
	/**
	 * @param dataDec7
	 */
	public void setDataDec7(String dataDec7) {
		this.dataDec7 = dataDec7 == null ? null : dataDec7.trim();
	}
	
    /**
     * @return DataDec7
     */	
	public String getDataDec7() {
		return this.dataDec7;
	}
	
	/**
	 * @param dataDec8
	 */
	public void setDataDec8(String dataDec8) {
		this.dataDec8 = dataDec8 == null ? null : dataDec8.trim();
	}
	
    /**
     * @return DataDec8
     */	
	public String getDataDec8() {
		return this.dataDec8;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}