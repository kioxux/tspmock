package cn.com.yusys.yusp.server.xdsx0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：审批人资本占用率参数表同步
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0025DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "update_time")
    private String update_time;//同步日期
    @JsonProperty(value = "custtype")
    private String custtype;//客户类型
    @JsonProperty(value = "list")
    private java.util.List<Xdsx0025SubData> list;

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCusttype() {
        return custtype;
    }

    public void setCusttype(String custtype) {
        this.custtype = custtype;
    }

    public java.util.List<Xdsx0025SubData> getList() {
        return list;
    }

    public void setList(java.util.List<Xdsx0025SubData> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdsx0025ReqDto{" +
                "update_time='" + update_time + '\'' +
                "custtype='" + custtype + '\'' +
                "list='" + list + '\'' +
                '}';
    }
}
