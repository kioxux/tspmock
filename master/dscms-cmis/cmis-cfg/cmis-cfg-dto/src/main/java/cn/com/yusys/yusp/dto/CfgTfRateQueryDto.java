package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 查询汇率信息请求dto
 * @author :xuchao
 */
public class CfgTfRateQueryDto  implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 币种编号 **/
    private String crcycd;
    /** 币种 **/
    private String curType;

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    @Override
    public String toString() {
        return "CfgTfRateQueryDto{" +
                "crcycd='" + crcycd + '\'' +
                ", curType='" + curType + '\'' +
                '}';
    }
}
