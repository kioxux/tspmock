package cn.com.yusys.yusp.dto;

import java.util.List;
import java.util.Map;

/**
 * cfg服务提供给biz服务的接口交互dto
 * cfg_flow_data 数据流配置
 **/
public class CfgToBizFlowDataDto {
    /**入参部分**/
    private String sourceTableName;//源表表名
    private String distTableName;//目标表名
    private Map sourceMap;//源表数据集合，因为使用实体对象进行传参，cfg服务需要建立对应的实体dto，因此使用map进行传参以及出参
    private Map distMap;//目标表数据集合

    //20210123 避免list转化的问题，增加入参的list集合对象
    private List<Map> sourObjList;//入参的数据集合

    /**出参部分**/
    private String rtnCode;//接口返回码值
    private String rtnMsg;//接口返回信息
    private List<String> mappingList;//映射集合数据
    private Map returnMap;//返回的数据集合

    private List<Map> rtnObjList;//返回的数据对象集合

    /**自定义参数部分**/

    public String getSourceTableName() {
        return sourceTableName;
    }

    public void setSourceTableName(String sourceTableName) {
        this.sourceTableName = sourceTableName;
    }

    public String getDistTableName() {
        return distTableName;
    }

    public void setDistTableName(String distTableName) {
        this.distTableName = distTableName;
    }

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public List<String> getMappingList() {
        return mappingList;
    }

    public void setMappingList(List<String> mappingList) {
        this.mappingList = mappingList;
    }

    public Map getDistMap() {
        return distMap;
    }

    public void setDistMap(Map distMap) {
        this.distMap = distMap;
    }

    public Map getReturnMap() {
        return returnMap;
    }

    public void setReturnMap(Map returnMap) {
        this.returnMap = returnMap;
    }

    public Map getSourceMap() {
        return sourceMap;
    }

    public void setSourceMap(Map sourceMap) {
        this.sourceMap = sourceMap;
    }

    public List<Map> getRtnObjList() {
        return rtnObjList;
    }

    public void setRtnObjList(List<Map> rtnObjList) {
        this.rtnObjList = rtnObjList;
    }

    public List<Map> getSourObjList() {
        return sourObjList;
    }

    public void setSourObjList(List<Map> sourObjList) {
        this.sourObjList = sourObjList;
    }
}
