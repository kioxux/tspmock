package cn.com.yusys.yusp.server.xdxt0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0012DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cnname1")
    private String cnname1;//国标地区编码字典中文名
    @JsonProperty(value = "cnname2")
    private String cnname2;//国标地区编码字典中文名

    public String getCnname1() {
        return cnname1;
    }

    public void setCnname1(String cnname1) {
        this.cnname1 = cnname1;
    }

    public String getCnname2() {
        return cnname2;
    }

    public void setCnname2(String cnname2) {
        this.cnname2 = cnname2;
    }

    @Override
    public String toString() {
        return "Xdxt0012DataReqDto{" +
                "cnname1='" + cnname1 + '\'' +
                ", cnname2='" + cnname2 + '\'' +
                '}';
    }
}
