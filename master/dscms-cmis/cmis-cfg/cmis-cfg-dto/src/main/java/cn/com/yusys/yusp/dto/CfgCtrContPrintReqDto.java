package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgCtrContPrintTemp
 * @类描述: cfg_ctr_cont_print_temp数据实体类
 * @功能描述:
 * @创建人: wrw
 * @创建时间: 2021-08-16
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgCtrContPrintReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 合同类型 **/
    private String suitContType;

    /** 产品编号 **/
    private String suitPrd;

    public String getSuitContType() {
        return suitContType;
    }

    public void setSuitContType(String suitContType) {
        this.suitContType = suitContType;
    }

    public String getSuitPrd() {
        return suitPrd;
    }

    public void setSuitPrd(String suitPrd) {
        this.suitPrd = suitPrd;
    }
}
