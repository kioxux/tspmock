package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgBankInfo
 * @类描述: cfg_bank_info数据实体类
 * @功能描述:
 * @创建人: Empty
 * @创建时间: 2021-04-29 14:15:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgBankInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 行号
     **/
    private String bankNo;

    /**
     * 行名
     **/
    private String bankName;

    /**
     * 地区代码
     **/
    private String areaCode;

    /**
     * 联系电话
     **/
    private String phone;

    /**
     * 邮政编码
     **/
    private String postcode;

    /**
     * 地址
     **/
    private String addr;

    /**
     * 上级行
     **/
    private String superBankNo;

    /**
     * 责任人
     **/
    private String managerId;

    /**
     * 责任机构
     **/
    private String managerBrId;

    /**
     * 生效日期
     **/
    private String inureDate;

    /**
     * 注销日期
     **/
    private String logoutDate;

    /**
     * 状态 STD_ZB_PRD_ST
     **/
    private String bankStatus;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 最后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    private String oprType;

    /**
     * @return BankNo
     */
    public String getBankNo() {
        return this.bankNo;
    }

    /**
     * @param bankNo
     */
    public void setBankNo(String bankNo) {
        this.bankNo = bankNo == null ? null : bankNo.trim();
    }

    /**
     * @return BankName
     */
    public String getBankName() {
        return this.bankName;
    }

    /**
     * @param bankName
     */
    public void setBankName(String bankName) {
        this.bankName = bankName == null ? null : bankName.trim();
    }

    /**
     * @return AreaCode
     */
    public String getAreaCode() {
        return this.areaCode;
    }

    /**
     * @param areaCode
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    /**
     * @return Phone
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * @return Postcode
     */
    public String getPostcode() {
        return this.postcode;
    }

    /**
     * @param postcode
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode == null ? null : postcode.trim();
    }

    /**
     * @return Addr
     */
    public String getAddr() {
        return this.addr;
    }

    /**
     * @param addr
     */
    public void setAddr(String addr) {
        this.addr = addr == null ? null : addr.trim();
    }

    /**
     * @return SuperBankNo
     */
    public String getSuperBankNo() {
        return this.superBankNo;
    }

    /**
     * @param superBankNo
     */
    public void setSuperBankNo(String superBankNo) {
        this.superBankNo = superBankNo == null ? null : superBankNo.trim();
    }

    /**
     * @return ManagerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId == null ? null : managerId.trim();
    }

    /**
     * @return ManagerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId == null ? null : managerBrId.trim();
    }

    /**
     * @return InureDate
     */
    public String getInureDate() {
        return this.inureDate;
    }

    /**
     * @param inureDate
     */
    public void setInureDate(String inureDate) {
        this.inureDate = inureDate == null ? null : inureDate.trim();
    }

    /**
     * @return LogoutDate
     */
    public String getLogoutDate() {
        return this.logoutDate;
    }

    /**
     * @param logoutDate
     */
    public void setLogoutDate(String logoutDate) {
        this.logoutDate = logoutDate == null ? null : logoutDate.trim();
    }

    /**
     * @return BankStatus
     */
    public String getBankStatus() {
        return this.bankStatus;
    }

    /**
     * @param bankStatus
     */
    public void setBankStatus(String bankStatus) {
        this.bankStatus = bankStatus == null ? null : bankStatus.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }


}