package cn.com.yusys.yusp.server.cmiscfg0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CmisCfg0002ReqDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prdId")           //产品编号
    private String prdId;

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    @Override
    public String toString() {
        return "CmisLmt0042ReqDto{" +
                "prdId='" + prdId + '\'' +
                '}';
    }
}
