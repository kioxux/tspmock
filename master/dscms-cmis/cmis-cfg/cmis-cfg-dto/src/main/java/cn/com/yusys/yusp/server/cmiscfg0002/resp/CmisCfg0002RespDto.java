package cn.com.yusys.yusp.server.cmiscfg0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CmisCfg0002RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    private List<CmisCfg0002ListRespDto> cfgPrdTypePropertiesList;//产品配置列表

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<CmisCfg0002ListRespDto> getCfgPrdTypePropertiesList() {
        return cfgPrdTypePropertiesList;
    }

    public void setCfgPrdTypePropertiesList(List<CmisCfg0002ListRespDto> cfgPrdTypePropertiesList) {
        this.cfgPrdTypePropertiesList = cfgPrdTypePropertiesList;
    }

    @Override
    public String toString() {
        return "CmisLmt0042RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", cfgPrdTypePropertiesList=" + cfgPrdTypePropertiesList +
                '}';
    }
}
