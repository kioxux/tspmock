package cn.com.yusys.yusp.server.xdxt0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Data：树形字典通用列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0013DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "optList")
    private java.util.List<OptList> optList;

    public List<OptList> getOptList() {
        return optList;
    }

    public void setOptList(List<OptList> optList) {
        this.optList = optList;
    }

    @Override
    public String toString() {
        return "Xdxt0013DataRespDto{" +
                "optList=" + optList +
                '}';
    }
}
