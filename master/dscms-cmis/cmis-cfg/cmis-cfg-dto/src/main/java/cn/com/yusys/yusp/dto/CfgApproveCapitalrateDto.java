package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgApproveCapitalrateDto
 * @类描述: CfgApproveCapitalrateDto
 * @功能描述: 
 * @创建人: 12651
 * @创建时间: 2021-05-07 16:07:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgApproveCapitalrateDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 岗位号 **/
	private String dutyCode;

	/** 上限(含) **/
	private String maxVal;

	/** 下限 **/
	private String minVal;

	/** 0:无效   1：有效 **/
	private String status;

	/** 更新时间 **/
	private String updateTime;

	/** 客户类型(010:一般客户, 020:同业客户) **/
	private String cusType;

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	private String rate;

	public String getDutyCode() {
		return dutyCode;
	}

	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}

	public String getMaxVal() {
		return maxVal;
	}

	public void setMaxVal(String maxVal) {
		this.maxVal = maxVal;
	}

	public String getMinVal() {
		return minVal;
	}

	public void setMinVal(String minVal) {
		this.minVal = minVal;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCusType() {
		return cusType;
	}

	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
}