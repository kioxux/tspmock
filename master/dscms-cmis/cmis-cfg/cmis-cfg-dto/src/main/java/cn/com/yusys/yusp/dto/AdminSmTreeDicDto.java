package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: AdminSmTreeDic
 * @类描述: admin_sm_tree_dic数据实体类
 * @功能描述: 
 * @创建人: 12651
 * @创建时间: 2021-05-07 16:07:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class AdminSmTreeDicDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 通用代码 **/
	private String enName;
	
	/** 通用代码描述 **/
	private String cnName;
	
	/** 上级通用代码 **/
	private String abvenName;
	
	/** 通用代码层级路径 **/
	private String locate;
	
	/** 代码类别 **/
	private String optType;
	
	/** 代码类别描述 **/
	private String memo;
	
	/** 状态    通用类型(NORM_STS) I-无效 A-生效 **/
	private String comSts;
	
	/** 层级标识 **/
	private String levels;
	
	/** 排序字段 **/
	private Integer orderid;

	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param enName
	 */
	public void setEnName(String enName) {
		this.enName = enName == null ? null : enName.trim();
	}
	
    /**
     * @return EnName
     */	
	public String getEnName() {
		return this.enName;
	}
	
	/**
	 * @param cnName
	 */
	public void setCnName(String cnName) {
		this.cnName = cnName == null ? null : cnName.trim();
	}
	
    /**
     * @return CnName
     */	
	public String getCnName() {
		return this.cnName;
	}
	
	/**
	 * @param abvenName
	 */
	public void setAbvenName(String abvenName) {
		this.abvenName = abvenName == null ? null : abvenName.trim();
	}
	
    /**
     * @return AbvenName
     */	
	public String getAbvenName() {
		return this.abvenName;
	}
	
	/**
	 * @param locate
	 */
	public void setLocate(String locate) {
		this.locate = locate == null ? null : locate.trim();
	}
	
    /**
     * @return Locate
     */	
	public String getLocate() {
		return this.locate;
	}
	
	/**
	 * @param optType
	 */
	public void setOptType(String optType) {
		this.optType = optType == null ? null : optType.trim();
	}
	
    /**
     * @return OptType
     */	
	public String getOptType() {
		return this.optType;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}
	
    /**
     * @return Memo
     */	
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param comSts
	 */
	public void setComSts(String comSts) {
		this.comSts = comSts == null ? null : comSts.trim();
	}
	
    /**
     * @return ComSts
     */	
	public String getComSts() {
		return this.comSts;
	}
	
	/**
	 * @param levels
	 */
	public void setLevels(String levels) {
		this.levels = levels == null ? null : levels.trim();
	}
	
    /**
     * @return Levels
     */	
	public String getLevels() {
		return this.levels;
	}
	
	/**
	 * @param orderid
	 */
	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}
	
    /**
     * @return Orderid
     */	
	public Integer getOrderid() {
		return this.orderid;
	}


}