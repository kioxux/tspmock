package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrderDownload
 * @类描述: cfg_order_download数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-26 10:09:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgOrderDownloadDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 数据源(比如是biz，就在biz自动起个任务，然后接口发的一个记录给cfg) **/
	private String dataSource;
	
	/** 预约时间cron **/
	private String cronTime;
	
	/** 预约次数 下载要执行几次 **/
	private Integer orderNum;
	
	/** 下载报表的查询类 编号 或者 其他具体待定 **/
	private String downloadAction;
	
	/** 过滤条件 灵活查询的时候可以用用 **/
	private String queryCons;
	
	/** 文件名 **/
	private String fileName;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 状态 1运行中 2未运行 **/
	private String status;
	
	/** 导出类型 csv 还是  xlsx **/
	private String exportType;
	
	/** 关联水印配置id **/
	private String watermarkId;
	
	/** 关联导出权限id 控制哪几个字段可修改啥的 **/
	private String rowCfgId;
	
	/** 查询数据的形式 1 来源于灵活查询 2固定的提前写好的方法 **/
	private String orderType;
	
	/** 水印字体大小 **/
	private Short waterFontSize;
	
	/** 水印内容 1 用户 2 机构 3时间 4自定义文字 **/
	private String waterContent;
	
	/** 水印文字 **/
	private String waterWords;
	
	/** 已下载次数 **/
	private Integer downloadCount;
	
	

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param dataSource
	 */
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource == null ? null : dataSource.trim();
	}
	
    /**
     * @return DataSource
     */	
	public String getDataSource() {
		return this.dataSource;
	}
	
	/**
	 * @param cronTime
	 */
	public void setCronTime(String cronTime) {
		this.cronTime = cronTime == null ? null : cronTime.trim();
	}
	
    /**
     * @return CronTime
     */	
	public String getCronTime() {
		return this.cronTime;
	}
	
	/**
	 * @param orderNum
	 */
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	
    /**
     * @return OrderNum
     */	
	public Integer getOrderNum() {
		return this.orderNum;
	}
	
	/**
	 * @param downloadAction
	 */
	public void setDownloadAction(String downloadAction) {
		this.downloadAction = downloadAction == null ? null : downloadAction.trim();
	}
	
    /**
     * @return DownloadAction
     */	
	public String getDownloadAction() {
		return this.downloadAction;
	}
	
	/**
	 * @param queryCons
	 */
	public void setQueryCons(String queryCons) {
		this.queryCons = queryCons == null ? null : queryCons.trim();
	}
	
    /**
     * @return QueryCons
     */	
	public String getQueryCons() {
		return this.queryCons;
	}
	
	/**
	 * @param fileName
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName == null ? null : fileName.trim();
	}
	
    /**
     * @return FileName
     */	
	public String getFileName() {
		return this.fileName;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param exportType
	 */
	public void setExportType(String exportType) {
		this.exportType = exportType == null ? null : exportType.trim();
	}
	
    /**
     * @return ExportType
     */	
	public String getExportType() {
		return this.exportType;
	}
	
	/**
	 * @param watermarkId
	 */
	public void setWatermarkId(String watermarkId) {
		this.watermarkId = watermarkId == null ? null : watermarkId.trim();
	}
	
    /**
     * @return WatermarkId
     */	
	public String getWatermarkId() {
		return this.watermarkId;
	}
	
	/**
	 * @param rowCfgId
	 */
	public void setRowCfgId(String rowCfgId) {
		this.rowCfgId = rowCfgId == null ? null : rowCfgId.trim();
	}
	
    /**
     * @return RowCfgId
     */	
	public String getRowCfgId() {
		return this.rowCfgId;
	}
	
	/**
	 * @param orderType
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType == null ? null : orderType.trim();
	}
	
    /**
     * @return OrderType
     */	
	public String getOrderType() {
		return this.orderType;
	}

	public Integer getDownloadCount() {
		return downloadCount;
	}

	public void setDownloadCount(Integer downloadCount) {
		this.downloadCount = downloadCount;
	}

	public Short getWaterFontSize() {
		return waterFontSize;
	}

	public void setWaterFontSize(Short waterFontSize) {
		this.waterFontSize = waterFontSize;
	}

	public String getWaterContent() {
		return waterContent;
	}

	public void setWaterContent(String waterContent) {
		this.waterContent = waterContent;
	}

	public String getWaterWords() {
		return waterWords;
	}

	public void setWaterWords(String waterWords) {
		this.waterWords = waterWords;
	}


}