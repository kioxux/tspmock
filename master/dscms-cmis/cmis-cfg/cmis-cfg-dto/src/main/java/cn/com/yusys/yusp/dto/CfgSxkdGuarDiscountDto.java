package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSxkdGuarDiscount
 * @类描述: cfg_sxkd_guar_discount数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-10 22:30:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgSxkdGuarDiscountDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 抵押物类型 **/
	private String guarTypeCd;

	/** 抵押物名称 **/
	private String guarName;

	/** 折率 **/
	private java.math.BigDecimal discountRate;

	/** 有效标志(Y:有效, N:无效) **/
	private String usedInd;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;


	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}

	/**
	 * @return guarTypeCd
	 */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}

	/**
	 * @param guarName
	 */
	public void setGuarName(String guarName) {
		this.guarName = guarName;
	}

	/**
	 * @return guarName
	 */
	public String getGuarName() {
		return this.guarName;
	}

	/**
	 * @param discountRate
	 */
	public void setDiscountRate(java.math.BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	/**
	 * @return discountRate
	 */
	public java.math.BigDecimal getDiscountRate() {
		return this.discountRate;
	}

	/**
	 * @param usedInd
	 */
	public void setUsedInd(String usedInd) {
		this.usedInd = usedInd;
	}

	/**
	 * @return usedInd
	 */
	public String getUsedInd() {
		return this.usedInd;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

}