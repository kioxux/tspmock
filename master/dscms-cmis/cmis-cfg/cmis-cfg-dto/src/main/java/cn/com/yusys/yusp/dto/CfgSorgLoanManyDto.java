package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgSorgLoanMany
 * @类描述: cfg_sorg_loan_many数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-21 10:13:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgSorgLoanManyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String pkId;
	
	/** 管理机构号 **/
	private String managerBrNo;
	
	/** 放贷机构号 **/
	private String loanBrNo;
	
	/** 放贷机构名 **/
	private String loanBrNoName;
	
	/** 账务机构 **/
	private String finaBrNo;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param managerBrNo
	 */
	public void setManagerBrNo(String managerBrNo) {
		this.managerBrNo = managerBrNo == null ? null : managerBrNo.trim();
	}
	
    /**
     * @return ManagerBrNo
     */	
	public String getManagerBrNo() {
		return this.managerBrNo;
	}
	
	/**
	 * @param loanBrNo
	 */
	public void setLoanBrNo(String loanBrNo) {
		this.loanBrNo = loanBrNo == null ? null : loanBrNo.trim();
	}
	
    /**
     * @return LoanBrNo
     */	
	public String getLoanBrNo() {
		return this.loanBrNo;
	}
	
	/**
	 * @param loanBrNoName
	 */
	public void setLoanBrNoName(String loanBrNoName) {
		this.loanBrNoName = loanBrNoName == null ? null : loanBrNoName.trim();
	}
	
    /**
     * @return LoanBrNoName
     */	
	public String getLoanBrNoName() {
		return this.loanBrNoName;
	}
	
	/**
	 * @param finaBrNo
	 */
	public void setFinaBrNo(String finaBrNo) {
		this.finaBrNo = finaBrNo == null ? null : finaBrNo.trim();
	}
	
    /**
     * @return FinaBrNo
     */	
	public String getFinaBrNo() {
		return this.finaBrNo;
	}


}