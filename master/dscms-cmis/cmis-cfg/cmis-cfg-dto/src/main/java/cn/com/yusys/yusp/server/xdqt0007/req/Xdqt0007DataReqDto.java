package cn.com.yusys.yusp.server.xdqt0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：新微贷产品信息同步
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdqt0007DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prdCatalogFirstLevel")
    private String prdCatalogFirstLevel;//产品一级目录
    @JsonProperty(value = "prdCatalogSecondLevel")
    private String prdCatalogSecondLevel;//产品二级分类
    @JsonProperty(value = "prdCatalogThreeLevel")
    private String prdCatalogThreeLevel;//产品三级分类
    @JsonProperty(value = "prdCatalogFourLevel")
    private String prdCatalogFourLevel;//产品四级分类
	@JsonProperty(value = "prdName")
	private String prdName;//产品名称
    @JsonProperty(value = "prdBelgLine")
    private String prdBelgLine;//产品所属条线
    @JsonProperty(value = "prdType")
    private String prdType;//产品类别
    @JsonProperty(value = "loanCha")
    private String loanCha;//贷款性质
    @JsonProperty(value = "corePrdId")
    private String corePrdId;//核心产品编号
    @JsonProperty(value = "suitIndgtReportType")
    private String suitIndgtReportType;//适用调查报告类型
    @JsonProperty(value = "isStopOffline")
    private String isStopOffline;//是否需要线下调查
    @JsonProperty(value = "suitContType")
    private String suitContType;//适用合同类型
    @JsonProperty(value = "suitCurType")
    private String suitCurType;//适用币种
    @JsonProperty(value = "suitGuarMode")
    private String suitGuarMode;//担保方式
    @JsonProperty(value = "suitPayMode")
    private String suitPayMode;//支付方式
    @JsonProperty(value = "suitRepayMode")
    private String suitRepayMode;//还款方式
    @JsonProperty(value = "isAllowExt")
    private String isAllowExt;//是否允许展期
    @JsonProperty(value = "isAllowSignOnline")
    private String isAllowSignOnline;//是否允许线上签约
    @JsonProperty(value = "isAllowDisbOnline")
    private String isAllowDisbOnline;//是否允许线上放款
    @JsonProperty(value = "advRepayEndDate")
    private String advRepayEndDate;//提前还款截止日
    @JsonProperty(value = "prdStatus")
    private String prdStatus;//产品状态

	public String getPrdCatalogFirstLevel() {
		return prdCatalogFirstLevel;
	}

	public void setPrdCatalogFirstLevel(String prdCatalogFirstLevel) {
		this.prdCatalogFirstLevel = prdCatalogFirstLevel;
	}

	public String getPrdCatalogSecondLevel() {
		return prdCatalogSecondLevel;
	}

	public void setPrdCatalogSecondLevel(String prdCatalogSecondLevel) {
		this.prdCatalogSecondLevel = prdCatalogSecondLevel;
	}

	public String getPrdCatalogThreeLevel() {
		return prdCatalogThreeLevel;
	}

	public void setPrdCatalogThreeLevel(String prdCatalogThreeLevel) {
		this.prdCatalogThreeLevel = prdCatalogThreeLevel;
	}

	public String getPrdCatalogFourLevel() {
		return prdCatalogFourLevel;
	}

	public void setPrdCatalogFourLevel(String prdCatalogFourLevel) {
		this.prdCatalogFourLevel = prdCatalogFourLevel;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getPrdBelgLine() {
		return prdBelgLine;
	}

	public void setPrdBelgLine(String prdBelgLine) {
		this.prdBelgLine = prdBelgLine;
	}

	public String getPrdType() {
		return prdType;
	}

	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}

	public String getLoanCha() {
		return loanCha;
	}

	public void setLoanCha(String loanCha) {
		this.loanCha = loanCha;
	}

	public String getCorePrdId() {
		return corePrdId;
	}

	public void setCorePrdId(String corePrdId) {
		this.corePrdId = corePrdId;
	}

	public String getSuitIndgtReportType() {
		return suitIndgtReportType;
	}

	public void setSuitIndgtReportType(String suitIndgtReportType) {
		this.suitIndgtReportType = suitIndgtReportType;
	}

	public String getIsStopOffline() {
		return isStopOffline;
	}

	public void setIsStopOffline(String isStopOffline) {
		this.isStopOffline = isStopOffline;
	}

	public String getSuitContType() {
		return suitContType;
	}

	public void setSuitContType(String suitContType) {
		this.suitContType = suitContType;
	}

	public String getSuitCurType() {
		return suitCurType;
	}

	public void setSuitCurType(String suitCurType) {
		this.suitCurType = suitCurType;
	}

	public String getSuitGuarMode() {
		return suitGuarMode;
	}

	public void setSuitGuarMode(String suitGuarMode) {
		this.suitGuarMode = suitGuarMode;
	}

	public String getSuitPayMode() {
		return suitPayMode;
	}

	public void setSuitPayMode(String suitPayMode) {
		this.suitPayMode = suitPayMode;
	}

	public String getSuitRepayMode() {
		return suitRepayMode;
	}

	public void setSuitRepayMode(String suitRepayMode) {
		this.suitRepayMode = suitRepayMode;
	}

	public String getIsAllowExt() {
		return isAllowExt;
	}

	public void setIsAllowExt(String isAllowExt) {
		this.isAllowExt = isAllowExt;
	}

	public String getIsAllowSignOnline() {
		return isAllowSignOnline;
	}

	public void setIsAllowSignOnline(String isAllowSignOnline) {
		this.isAllowSignOnline = isAllowSignOnline;
	}

	public String getIsAllowDisbOnline() {
		return isAllowDisbOnline;
	}

	public void setIsAllowDisbOnline(String isAllowDisbOnline) {
		this.isAllowDisbOnline = isAllowDisbOnline;
	}

	public String getAdvRepayEndDate() {
		return advRepayEndDate;
	}

	public void setAdvRepayEndDate(String advRepayEndDate) {
		this.advRepayEndDate = advRepayEndDate;
	}

	public String getPrdStatus() {
		return prdStatus;
	}

	public void setPrdStatus(String prdStatus) {
		this.prdStatus = prdStatus;
	}

	@Override
	public String toString() {
		return "Xdqt0007DataReqDto{" +
				"prdCatalogFirstLevel='" + prdCatalogFirstLevel + '\'' +
				", prdCatalogSecondLevel='" + prdCatalogSecondLevel + '\'' +
				", prdCatalogThreeLevel='" + prdCatalogThreeLevel + '\'' +
				", prdCatalogFourLevel='" + prdCatalogFourLevel + '\'' +
				", prdName='" + prdName + '\'' +
				", prdBelgLine='" + prdBelgLine + '\'' +
				", prdType='" + prdType + '\'' +
				", loanCha='" + loanCha + '\'' +
				", corePrdId='" + corePrdId + '\'' +
				", suitIndgtReportType='" + suitIndgtReportType + '\'' +
				", isStopOffline='" + isStopOffline + '\'' +
				", suitContType='" + suitContType + '\'' +
				", suitCurType=" + suitCurType +
				", suitGuarMode='" + suitGuarMode + '\'' +
				", suitPayMode='" + suitPayMode + '\'' +
				", suitRepayMode='" + suitRepayMode + '\'' +
				", isAllowExt='" + isAllowExt + '\'' +
				", isAllowSignOnline='" + isAllowSignOnline + '\'' +
				", isAllowDisbOnline='" + isAllowDisbOnline + '\'' +
				", advRepayEndDate='" + advRepayEndDate + '\'' +
				", prdStatus='" + prdStatus + '\'' +
				'}';
	}
}
