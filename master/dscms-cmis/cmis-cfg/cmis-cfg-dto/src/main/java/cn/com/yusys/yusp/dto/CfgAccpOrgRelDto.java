package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgAccpOrgRel
 * @类描述: cfg_accp_org_rel数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-23 22:57:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgAccpOrgRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 兑付机构 **/
	private String payBrId;
	
	/** 机构名称 **/
	private String organno;

	/** 兑付机构 **/
	private String payBrName;
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param payBrId
	 */
	public void setPayBrId(String payBrId) {
		this.payBrId = payBrId == null ? null : payBrId.trim();
	}
	
    /**
     * @return PayBrId
     */	
	public String getPayBrId() {
		return this.payBrId;
	}
	
	/**
	 * @param organno
	 */
	public void setOrganno(String organno) {
		this.organno = organno == null ? null : organno.trim();
	}
	
    /**
     * @return Organno
     */	
	public String getOrganno() {
		return this.organno;
	}

	public String getPayBrName() {
		return payBrName;
	}

	public void setPayBrName(String payBrName) {
		this.payBrName = payBrName;
	}
}