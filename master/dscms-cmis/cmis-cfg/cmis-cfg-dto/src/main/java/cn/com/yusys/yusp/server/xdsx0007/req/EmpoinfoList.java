package cn.com.yusys.yusp.server.xdsx0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：授信业务授权同步、资本占用率参数表同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class EmpoinfoList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusLvl")
    private String cusLvl;//客户等级
    @JsonProperty(value = "debtLvl")
    private String debtLvl;//债项等级
    @JsonProperty(value = "authAmt")
    private String authAmt;//授权金额（元）/资本占用率

    public String getCusLvl() {
        return cusLvl;
    }

    public void setCusLvl(String cusLvl) {
        this.cusLvl = cusLvl;
    }

    public String getDebtLvl() {
        return debtLvl;
    }

    public void setDebtLvl(String debtLvl) {
        this.debtLvl = debtLvl;
    }

    public String getAuthAmt() {
        return authAmt;
    }

    public void setAuthAmt(String authAmt) {
        this.authAmt = authAmt;
    }

    @Override
    public String toString() {
        return "EmpoinfoList{" +
                "cusLvl='" + cusLvl + '\'' +
                "debtLvl='" + debtLvl + '\'' +
                "authAmt='" + authAmt + '\'' +
                '}';
    }
}
