package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgOrderDownloadRecord
 * @类描述: cfg_order_download_record数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-24 17:03:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgOrderDownloadRecordDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 关联预约配置主键 **/
	private String relPkId;
	
	/** 文件路径或者文件服务给的索引，待定  **/
	private String filePath;
	
	/** 文件数据获取时间 **/
	private String dataDate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param relPkId
	 */
	public void setRelPkId(String relPkId) {
		this.relPkId = relPkId == null ? null : relPkId.trim();
	}
	
    /**
     * @return RelPkId
     */	
	public String getRelPkId() {
		return this.relPkId;
	}
	
	/**
	 * @param filePath
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath == null ? null : filePath.trim();
	}
	
    /**
     * @return FilePath
     */	
	public String getFilePath() {
		return this.filePath;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate == null ? null : dataDate.trim();
	}
	
    /**
     * @return DataDate
     */	
	public String getDataDate() {
		return this.dataDate;
	}


}