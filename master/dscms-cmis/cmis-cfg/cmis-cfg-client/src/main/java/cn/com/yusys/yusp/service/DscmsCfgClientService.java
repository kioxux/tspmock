package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.service.impl.DscmsCfgClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 封装的接口类:贷后管理模块
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-cfg", path = "/api", fallback = DscmsCfgClientServiceImpl.class)
public interface DscmsCfgClientService {

    /**
     * 查询临时存储文件配置表
     *
     * @param queryModel
     * @return
     */
    @PostMapping("/cfggeneratetempfile/query/queryCfgFile")
    ResultDto<List<CfgGenerateTempFileDto>> queryCfgFile(@RequestBody QueryModel queryModel);

    /**
     * 查询临时存储文件配置表
     *
     * @param queryModel
     * @return
     */
    @PostMapping("/cfgsorgelecseal/query/queryCfgSeal")
    ResultDto<List<CfgSOrgElecSealDto>> queryCfgSeal(@RequestBody QueryModel queryModel);

    /**
     * 获取配置信息list
     *
     * @param queryMap
     * @return
     */
    @PostMapping("/cfgdiscorglmt/getListByQueryMap")
    ResultDto<List<CfgDiscOrgLmtRespDto>> getListByQueryMap(Map queryMap);

    @PostMapping("/cfgagricuarea/queryAreaNum")
    ResultDto<Integer> queryAreaNum(@RequestParam("areaCode") String areaCode, @RequestParam("agriFlg") String agriFlg);

    /**
     * 根据 areaCode获取注册地址
     * @param adminSmTreeDicDto
     * @return
     */
    @PostMapping("/adminsmtreedic/queryLabelPath")
    ResultDto<Map<String, String>> querySingleLabelPath(@RequestBody AdminSmTreeDicDto adminSmTreeDicDto);

    /**
     * @param cfgPrdBasicinfoDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<CfgPrdBasicinfo>
     * @author 王玉坤
     * @date 2021/9/1 22:45
     * @version 1.0.0
     * @desc  根据产品代码查询产品信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/querycfgprdinfobyprcid")
    ResultDto<CfgPrdBasicinfoDto> queryCfgprdInfoByprcId(@RequestBody CfgPrdBasicinfoDto cfgPrdBasicinfoDto);
    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/9/13 10:12
     * @version 1.0.0
     * @desc  根据树形字典项码值查询中文翻译
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/adminsmtreedic/querycfgtreebycode")
    ResultDto<AdminSmTreeDicDto> queryCfgTreeByCode(@RequestBody AdminSmTreeDicDto adminSmTreeDicDto);
}