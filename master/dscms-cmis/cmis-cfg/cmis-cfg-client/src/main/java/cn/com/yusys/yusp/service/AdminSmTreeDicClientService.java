package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.service.impl.AdminSmTreeDicClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * 封装的接口类:贷后管理模块
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-cfg", path = "/api/adminsmtreedic", fallback = AdminSmTreeDicClientServiceImpl.class)
public interface AdminSmTreeDicClientService {

    /**
     * 查询树形字典
     *
     * @param adminSmTreeDicDto
     * @return
     */
    @PostMapping("/query")
    ResultDto<List<Map<String, String>>> querySingle(@RequestBody AdminSmTreeDicDto adminSmTreeDicDto);
}