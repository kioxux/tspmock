package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.server.xdsx0007.req.Xdsx0007DataReqDto;
import cn.com.yusys.yusp.server.xdsx0007.resp.Xdsx0007DataRespDto;
import cn.com.yusys.yusp.server.xdsx0009.req.Xdsx0009DataReqDto;
import cn.com.yusys.yusp.server.xdsx0009.resp.Xdsx0009DataRespDto;
import cn.com.yusys.yusp.server.xdsx0025.req.Xdsx0025DataReqDto;
import cn.com.yusys.yusp.server.xdsx0025.resp.Xdsx0025DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsCfgSxClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类
 *
 * @author lihh
 * @version 1.0
 */
@FeignClient(name = "cmis-cfg", path = "/api", fallback = DscmsCfgSxClientServiceImpl.class)
public interface DscmsCfgSxClientService {
    /**
     * 交易码：xdsx0007
     * 交易描述：授信业务授权同步、资本占用率参数表同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cfgsx4bsp/xdsx0007")
    ResultDto<Xdsx0007DataRespDto> xdsx0007(Xdsx0007DataReqDto reqDto);


    /**
     * 交易码：xdsx0009
     * 交易描述：客户准入级别同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cfgsx4bsp/xdsx0009")
    ResultDto<Xdsx0009DataRespDto> xdsx0009(Xdsx0009DataReqDto reqDto);

    /**
     * 交易码：xdsx0025
     * 交易描述：审批人资本占用率参数表同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cfgsx4bsp/xdsx0025")
    ResultDto<Xdsx0025DataRespDto> xdsx0025(Xdsx0025DataReqDto reqDto);
}