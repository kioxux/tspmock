package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.server.cmiscfg0001.req.CmisCfg0001ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0001.resp.CmisCfg0001RespDto;
import cn.com.yusys.yusp.server.cmiscfg0002.req.CmisCfg0002ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002RespDto;
import cn.com.yusys.yusp.server.cmiscfg0003.req.CmisCfg0003ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0003.resp.CmisCfg0003RespDto;
import cn.com.yusys.yusp.server.cmiscfg0004.req.CmisCfg0004ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0004.resp.CmisCfg0004RespDto;
import cn.com.yusys.yusp.server.xdqt0001.req.Xdqt0001DataReqDto;
import cn.com.yusys.yusp.server.xdqt0001.resp.Xdqt0001DataRespDto;
import cn.com.yusys.yusp.server.xdqt0007.req.Xdqt0007DataReqDto;
import cn.com.yusys.yusp.server.xdqt0007.resp.Xdqt0007DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsCfgQtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类
 *
 * @author lihh
 * @version 1.0
 */
@FeignClient(name = "cmis-cfg", path = "/api", fallback = DscmsCfgQtClientServiceImpl.class)
public interface DscmsCfgQtClientService {

    /**
     * 交易码：xdqt0001
     * 交易描述：根据产品号查询产品名称
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cfgqt4bsp/xdqt0001")
    ResultDto<Xdqt0001DataRespDto> xdqt0001(Xdqt0001DataReqDto reqDto);

    /**
     * 交易码：xdqt0007
     * 交易描述：新微贷产品信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cfgqt4bsp/xdqt0007")
    ResultDto<Xdqt0007DataRespDto> xdqt0007(Xdqt0007DataReqDto reqDto);


    /**
     * 新增首页提醒
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cfg4inner/cmiscfg0001")
    ResultDto<CmisCfg0001RespDto> cmisCfg0001(CmisCfg0001ReqDto reqDto);

    /**
     * 根据产品编号查询产品扩展属性
     * add by zhangjw 20210714
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cfg4inner/cmiscfg0002")
    ResultDto<CmisCfg0002RespDto> cmisCfg0002(CmisCfg0002ReqDto reqDto);

    /**
     * 根据产品编号查询产品扩展属性
     * add by zhangjw 20210714
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cfg4inner/cmiscfg0003")
    ResultDto<CmisCfg0003RespDto> cmisCfg0003(CmisCfg0003ReqDto reqDto);


    /**
     * 根据分支机构获取总行行号或BICCODE
     * add by lixy 20210830
     * @param reqDto
     * @return
     */
    @PostMapping("/cfg4inner/cmiscfg0004")
    ResultDto<CmisCfg0004RespDto> cmisCfg0004(CmisCfg0004ReqDto reqDto);

}