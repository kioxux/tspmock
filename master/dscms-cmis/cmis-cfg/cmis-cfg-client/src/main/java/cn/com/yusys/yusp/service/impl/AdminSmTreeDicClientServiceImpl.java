package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.service.AdminSmTreeDicClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class AdminSmTreeDicClientServiceImpl implements AdminSmTreeDicClientService {
    private static final Logger logger = LoggerFactory.getLogger(DscmsCfgClientServiceImpl.class);

    @Override
    public ResultDto<List<Map<String, String>>> querySingle(AdminSmTreeDicDto adminSmTreeDicDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }
}