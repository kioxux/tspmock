package cn.com.yusys.yusp.service;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.server.xdxt0012.req.Xdxt0012DataReqDto;
import cn.com.yusys.yusp.server.xdxt0012.resp.Xdxt0012DataRespDto;
import cn.com.yusys.yusp.server.xdxt0013.req.Xdxt0013DataReqDto;
import cn.com.yusys.yusp.server.xdxt0013.resp.Xdxt0013DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsCfgXtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 系统管理服务接口
 *
 * @author leehuang
 * @version 1.0
 */
@FeignClient(name = "cmis-cfg", path = "/api", fallback = DscmsCfgXtClientServiceImpl.class)
public interface DscmsCfgXtClientService {
    /**
     * 交易码：xdxt0012
     * 交易描述：根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cfgxt4bsp/xdxt0012")
    ResultDto<Xdxt0012DataRespDto> xdxt0012(Xdxt0012DataReqDto reqDto);

    /**
     * 交易码：xdxt0013
     * 交易描述：树形字典通用列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cfgxt4bsp/xdxt0013")
    ResultDto<Xdxt0013DataRespDto> xdxt0013(Xdxt0013DataReqDto reqDto);
}
