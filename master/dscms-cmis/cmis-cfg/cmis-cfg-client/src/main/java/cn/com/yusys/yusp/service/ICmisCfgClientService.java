package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@FeignClient(name = "cmis-cfg", path = "/api", fallback = CmisCfgClientServiceImpl.class)
public interface ICmisCfgClientService {

    @PostMapping(value = "/cfghandover/queryCfgHandover")
    CfgHandoverClientDto queryCfgHandover(@RequestBody CfgHandoverClientDto cfgHandoverDto);

    /**
     * 数据流查询-单个对象
     *
     * @param cfgToBizFlowDataDto
     * @return
     */
    @PostMapping(value = "/cfgdataflow/queryCfgDataFlowInfo4Out")
    CfgToBizFlowDataDto queryCfgDataFlowInfo4Out(@RequestBody CfgToBizFlowDataDto cfgToBizFlowDataDto);

    /**
     * 新增用户常用功能
     *
     * @param cfgUserOftUseFuncClientDto
     * @return
     */
    @PostMapping(value = "/cfguseroftusefunc/insertUserOftUseFunc")
    ResultDto<Integer> insertUserOftUseFunc(@RequestBody CfgUserOftUseFuncClientDto cfgUserOftUseFuncClientDto);

    /**
     * 删除用户常用功能
     *
     * @param cfgUserOftUseFuncClientDto
     * @return
     */
    @PostMapping(value = "/cfguseroftusefunc/deleteUserOftUseFunc")
    int deleteUserOftUseFunc(@RequestBody CfgUserOftUseFuncClientDto cfgUserOftUseFuncClientDto);

    /**
     * 获取用户常用功能
     *
     * @param loginCode
     * @return
     */
    @PostMapping(value = "/cfguseroftusefunc/getUserOftUseFunc")
    List<CfgUserOftUseFuncClientDto> getUserOftUseFunc(@RequestBody String loginCode);

    /**
     * 数据流映射查询-列表
     *
     * @param cfgToBizFlowDataDto
     * @return
     */
    @PostMapping(value = "/cfgdataflow/queryCfgDataFlowInfo4Out4List")
    CfgToBizFlowDataDto queryCfgDataFlowInfo4Out4List(@RequestBody CfgToBizFlowDataDto cfgToBizFlowDataDto);

    /**
     * 参数配置查询
     *
     * @param cfgClientParamDto
     * @return
     */
    @PostMapping(value = "/cfgbizparaminfo/getCfgBizParamInfoByClient")
    CfgClientParamDto getCfgClientParam(@RequestBody CfgClientParamDto cfgClientParamDto);

    /**
     * 获取查询sql
     *
     * @param qryCode
     * @return
     */
    @PostMapping(value = "/cfgflexqry/generateQueryListSQLByQryCode/{qryCode}")
    ResultDto<Map> generateQueryListSQLByQryCode(@PathVariable("qryCode") String qryCode);

    /**
     * 保存预约下载记录
     *
     * @param cfgOrderDownloadRecordDto
     * @return
     */
    @PostMapping(value = "/cfgorderdownloadrecord/")
    ResultDto<CfgOrderDownloadRecordDto> insertDownloadRecord(@RequestBody CfgOrderDownloadRecordDto cfgOrderDownloadRecordDto);

    /**
     * 获取预约下载的配置
     *
     * @param pkId
     * @return
     */
    @GetMapping(value = "/cfgorderdownload/getordercfg/{pkId}")
    CfgOrderDownloadDto getOrderCfg(@PathVariable("pkId") String pkId);

    /**
     * 获取预约路径
     *
     * @param pkId
     * @return
     */
    @GetMapping(value = "/cfgorderdownloadrecord/getorderfilepath/{pkId}")
    String getOrderFilePath(@PathVariable("pkId") String pkId);


    /**
     * 获取预约下载的运行中的配置
     *
     * @param dataSource 各个调用预约下载的微服务的编号
     * @return
     */
    @GetMapping(value = "/cfgorderdownload/getordercfgbydatasource/{dataSource}")
    List<CfgOrderDownloadDto> getOrderCfgByDataSource(@PathVariable("dataSource") String dataSource);

    /**
     * 根据项目样式编号和项目编号 获取项目配置信息
     *
     * @param
     * @return
     */
    @PostMapping(value = "/fncconfdeffmt/getFncConfDefFmtByPrimaryKey")
    FncConfDefFmtDto getFncConfDefFmtByPrimaryKey(@RequestBody Map map);

    /**
     * 根据项目样式编号 获取报表样式信息
     *
     * @param map
     * @return
     */
    @PostMapping(value = "/fncconfstyles/getFncConfStylesByPrimaryKey")
    FncConfStylesDto getFncConfStylesByPrimaryKey(@RequestBody Map map);

    /**
     * 根据项目样式编号 获取报表样式信息
     *
     * @param map
     * @return
     */
    @PostMapping(value = "/fncconfitems/getFncConfItemsByPrimaryKey")
    FncConfItemsDto getFncConfItemsByPrimaryKey(@RequestBody Map map);

    /**
     * 根据样式编号去查询所有的项目列表
     *
     * @param map
     * @return
     */
    @PostMapping(value = "/fncconfitems/getItemsListByStyleId")
    List<FncConfItemsDto> getItemsListByStyleId(@RequestBody Map map);

    /**
     * 根据样式编号去查询所有的项目配置定义
     *
     * @param map
     * @return
     */
    @PostMapping(value = "/fncconfdeffmt/getFncConfDefFmtByStyleId")
    List<FncConfDefFmtDto> getFncConfDefFmtByStyleId(@RequestBody Map map);

    /**
     * 获取额度分项产品映射信息
     *
     * @param cfgPrdCatalogDtos
     * @return
     */
    @PostMapping("/cfgprdcatalog/getLmtSubPrdMappConf")
    List<LmtSubPrdMappConfDto> getLmtSubPrdMappConf(@RequestBody List<CfgPrdCatalogDto> cfgPrdCatalogDtos);

    /**
     * 查询产品树
     *
     * @param map
     * @return
     */
    @PostMapping("/cfgprdcatalog/queryPrdTree")
    ResultDto<List<CfgPrdBasicinfoTranDto>> queryPrdTree(@RequestBody Map<String, String> map);


    @PostMapping("/cfgprdbasicinfo/queryContInfoByEnName4Biz")
    ResultDto<Integer> queryContInfoByEnName4Biz(@RequestBody Map<String, String> map);

    /**
     * @方法名称: queryCfgCreditLevelByType
     * @方法描述: 根据配置类型查询信用配置信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/cfgcreditlevel/queryCfgCreditLevelByType")
    ResultDto<List<CfgCreditLevelDto>> queryCfgCreditLevelByType(@RequestBody CfgCreditLevelDto cfgCreditLevelDto);

    /**
     * 查询机构电子章配置信息
     *
     * @param orgNo
     * @return
     */
    @PostMapping("/cfgsorgelecseal/selectByOrgNo")
    ResultDto<CfgSOrgElecSealDto> selectByOrgNo(@RequestBody String orgNo);

    /**
     * 查询机构电子章配置信息（20211025）
     *
     * @param orgNo
     * @return
     */
    @PostMapping("/cfgorgelecseal/selectFromCfgByOrgNo")
    ResultDto<CfgOrgElecSeal> selectFromCfgByOrgNo(@RequestBody String orgNo);


    /**
     * 查询授信抵押物折率配置表信息
     *
     * @param guarTypeCd
     * @return
     * @创建者：zhangliang15
     */
    @PostMapping("/cfgsxkdguardiscount/selectCfgSxkdGuarDiscountByGuarTypeCd")
    ResultDto<CfgSxkdGuarDiscountDto> selectCfgSxkdGuarDiscountByGuarTypeCd(@RequestBody String guarTypeCd);

    /**
     * @方法名称: queryCfgTfRate
     * @方法描述: 查询汇率信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/cfgtfrate/querycfgtfrate")
    ResultDto<CfgTfRateDto> queryCfgTfRate(@RequestBody CfgTfRateQueryDto cfgTfRateQueryDto);

    /**
     * @方法名称: queryAllCfgTfRate
     * @方法描述: 查询所有汇率信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/cfgtfrate/queryallcfgtfrate")
    ResultDto<List<CfgTfRateDto>> queryAllCfgTfRate(@RequestBody QueryModel model);

    /**
     * @创建人 WH
     * @创建时间 2021/6/15 19:47
     * @注释 获取LPR利率
     */
    @PostMapping("/cfglprrate/selectlpr")
    ResultDto selectlpr(@RequestBody CfgLprRateDto cfgLprRateDto);

    /**
     * @创建人 WH
     * @创建时间 2021/6/15 19:47
     * @注释 获取LPR利率 条件获取
     */
    @PostMapping("/cfglprrate/selectone")
    ResultDto<CfgLprRateDto> selectone(@RequestBody QueryModel queryModel);

    /**
     * 根据产品编号查询产品信息
     *
     * @param prdId
     * @return
     * @author xuchao
     */
    @PostMapping("/cfgprdbasicinfo/queryCfgPrdBasicInfo")
    ResultDto<CfgPrdBasicinfoDto> queryCfgPrdBasicInfo(@RequestBody String prdId);

    /**
     * 上传文件
     *
     * @param paramMap
     * @return
     */
    @PostMapping("/SftpUtilResource/upload")
    ResultDto<Boolean> upload(@RequestBody Map<String, String> paramMap);

    /**
     * 根据渠道标识查询配置信息
     *
     * @param bizScenseType
     * @return
     */
    @PostMapping("/cfgsftp/queryCfgSftpByBizScenseType")
    ResultDto<CfgSftpDto> queryCfgSftpByBizScenseType(@RequestBody String bizScenseType);

    /**
     * @方法名称: queryHxAccountClassByProps
     * @方法描述: 通过属性查询对应的核心科目号
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 马顺
     * @创建时间: 2021-06-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cfgaccountclasschoose/queryHxAccountClassByProps")
    ResultDto<CfgAccountClassChooseDto> queryHxAccountClassByProps(@RequestBody CfgAccountClassChooseDto cfgAccountClassChooseDto);

    /**
     * @创建人 zxz
     * @创建时间 2021/6/21 11:24
     * @注释 入账机构查询
     */
    @PostMapping("/cfgsorgfina/selecsorgfina")
    ResultDto<List<CfgSorgFinaDto>> selecSorgFina(@RequestBody QueryModel queryModel);

    /**
     * @方法名称: queryHxAccountClassByAcccountClass
     * @方法描述: 通过科目号查询对应的核心会计类别
     * @参数与返回说明:
     * @算法描述:
     * @创建人: YY
     * @创建时间: 2021-06-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cfgaccountclasschoose/queryHxAccountClassByAcccountClass")
    ResultDto<CfgAccountClassChooseDto> queryHxAccountClassByAcccountClass(@RequestBody String accountClass);

    /**
     * @创建人 zxz
     * @创建时间 2021/6/21 11:24
     * @注释 贷款机构查询
     */
    @PostMapping("/cfgsorgloanmany/selecsorgmany")
    ResultDto<List<CfgSorgLoanManyDto>> selecSorgLoanMany(@RequestBody QueryModel queryModel);

    /**
     * @方法名称: selectbycondition
     * @方法描述:根据贷款用途，产品代码，是否农业，是否长期查询网银支用科目对应关系
     * @参数与返回说明:
     * @算法描述:
     * @创建人: YY
     * @创建时间: 2021-06-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cfgwyclassrel/selectbycondition")
    ResultDto<CfgWyClassRelClientDto> selectbycondition(@RequestBody CfgWyClassRelClientDto cfgWyClassRelClientDto);

    /**
     * @创建人 xs
     * @创建时间 2021/6/22 16:24
     * @注释 获取当前时间到月末的工作日天数
     */
    @PostMapping("/wbholidaycfg/getWorkDays")
    ResultDto<Integer> getWorkDaysByMonth(Date date);

    /**
     * @创建人 zxz
     * @创建时间 2021/6/22 16:24
     * @注释 获取银票承兑机构
     */
    @PostMapping("/cfgaccporgrel/selectaccporg")
    ResultDto<List<CfgAccpOrgRelDto>> selectAccpOrg(@RequestBody QueryModel queryModel);


    /**
     * 查询产品扩展属性
     *
     * @param typePropNo
     * @return
     * @author xll
     */
    @PostMapping("/cfgprdtypeproperties/queryCfgPrdTypePropertiesByProrNo")
    ResultDto<List<CfgPrdTypePropertiesDto>> queryCfgPrdTypePropertiesByProrNo(@RequestBody String typePropNo);

    /**
     * 根据产品目录层级catalogid查询产品列表
     *
     * @param catalogId
     * @return
     * @author xll
     */
    @PostMapping("/cfgprdbasicinfo/queryBasicInfoByCatalogId")
    ResultDto<List<CfgPrdBasicinfoDto>> queryBasicInfoByCatalogId(@RequestBody String catalogId);


    @PostMapping("/cfgprdbasicinfo/querybasicinfobycatalog")
    ResultDto<List<CfgPrdBasicinfoDto>> queryBasicInfoByCatalog(@RequestBody String cataloglevelName);

    /**
     * 根据产品类别查询所有的产品信息
     *
     * @param prdType
     * @return
     * @author xuchao
     */
    @PostMapping("/cfgprdbasicinfo/queryCfgPrdBasicInfoByPrdType")
    ResultDto<List<CfgPrdBasicinfoDto>> queryCfgPrdBasicInfoByPrdType(@RequestBody String prdType);

    /**
     * 根据区域编码查询区域地址
     *
     * @param adminSmTreeDicDto
     * @return
     * @author xll
     */
    @PostMapping("/adminsmtreedic/queryLabelPath")
    ResultDto<Map<String, String>> querySingleLabelPath(@RequestBody AdminSmTreeDicDto adminSmTreeDicDto);

    /**
     * 获取内评审批岗位
     *
     * @param cfgApproveAuthorityDto
     * @return
     */
    @PostMapping("/cfgapproveauthority/querydutybycusdebtlevel")
    ResultDto<CfgApproveAuthorityDto> queryDutyByCusDebtLevel(@RequestBody CfgApproveAuthorityDto cfgApproveAuthorityDto);

    /**
     * 获取审批人的资本占用率
     *
     * @param cfgApproveAuthorityDto
     * @return
     */
    @PostMapping("/cfgapproveauthority/queryratebycusdebtlevel")
    ResultDto<CfgApproveAuthorityDto> queryRateByCusDebtLevel(@RequestBody CfgApproveAuthorityDto cfgApproveAuthorityDto);

    /**
     * 根据占用率获取岗位
     *
     * @param cfgApproveCapitalrateDto
     * @return
     */
    @PostMapping("/cfgapprovecapitalrate/querydutybyrate")
    ResultDto<CfgApproveCapitalrateDto> queryDutyByRate(@RequestBody CfgApproveCapitalrateDto cfgApproveCapitalrateDto);


    /**
     * 查询临时存储文件配置表(提供给frpt服务)
     *
     * @param pkid
     * @return
     */
    @PostMapping("/cfggeneratetempfile/query/queryCfgFilebyPkid")
    ResultDto<List<CfgGenerateTempFileDto>> queryCfgFilebyPkid(@RequestBody String pkid);

    /**
     * 机构电子章配置表
     *
     * @param map
     * @return
     */
    @PostMapping("/cfgorgelecseal/selectbycondition")
    ResultDto<List<CfgOrgElecSeal>> selectByCondition(@RequestBody Map<String, String> map);

    /**
     * 根据产品编号获取适用报表全称
     *
     * @param
     * @return
     */
    @PostMapping("/cfgctrcontprinttemp/querysuitreportname")
    public String querySuitReportName(@RequestBody CfgCtrContPrintReqDto cfgCtrContPrintReqDto);


    /**
     * 查询零售优惠利率信息
     *
     * @param
     * @return
     */
    @PostMapping("/cfgretailprimerate/selectbyprdid")
    public ResultDto<CfgRetailPrimeRateDto> selectbyPrdId(@RequestBody String prdId);

    /**
     * @param cfgRetailPrimeRateDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.CfgRetailPrimeRateDto>
     * @author 王玉坤
     * @date 2021/9/10 15:27
     * @version 1.0.0
     * @desc 根据条件查询零售优惠利率信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cfgretailprimerate/selectretailprimeratebycondition")
    public List<CfgRetailPrimeRateDto> selectRetailPrimerateByCondition(@RequestBody CfgRetailPrimeRateDto cfgRetailPrimeRateDto);

    /**
     * 根据贷款用途，产品代码，是否农业，是否长期查询网银支用科目对应关系
     *
     * @param
     * @return
     */
    @PostMapping("/cfgwyclassrel/selectbycondition")
    public ResultDto<CfgWyClassRelClientDto> selectByCondition(@RequestBody CfgWyClassRelClientDto cfgWyClassRelClientDto);

    /**
     * 根据行号查询信息
     *
     * @param
     * @return
     */
    @PostMapping("/cfgbankinfo/selectbybankno")
    public ResultDto<CfgBankInfoDto> selectbybankno(@RequestBody String bankNo);


    /**
     * @创建人 shenli
     * @创建时间 2021-9-17 17:25:31
     * @注释 获取LPR利率 条件获取
     */
    @PostMapping("/cfglprrate/selectafterlpr")
    ResultDto<CfgLprRateDto> selectAfterLpr(@RequestBody CfgLprRateDto cfgLprRateDto);

    /**
     * @创建人 shenli
     * @创建时间 2021-9-17 17:25:31
     * @注释 获取LPR利率 条件获取
     */
    @PostMapping("/cfglprrate/selectfrontlpr")
    ResultDto<CfgLprRateDto> selectFrontLpr(@RequestBody CfgLprRateDto cfgLprRateDto);

    /**
     * @param cfgXdLoanOrgDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CfgXdLoanOrg>
     * @author 王玉坤
     * @date 2021/10/20 20:20
     * @version 1.0.0
     * @desc 根据所属机构查询小贷账务机构信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cfgxdloanorg/querycfgxdloanorg")
    ResultDto<CfgXdLoanOrgDto> queryCfgXdLoanOrg(@RequestBody CfgXdLoanOrgDto cfgXdLoanOrgDto);

}
