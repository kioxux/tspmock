package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.server.xdsx0007.req.Xdsx0007DataReqDto;
import cn.com.yusys.yusp.server.xdsx0007.resp.Xdsx0007DataRespDto;
import cn.com.yusys.yusp.server.xdsx0009.req.Xdsx0009DataReqDto;
import cn.com.yusys.yusp.server.xdsx0009.resp.Xdsx0009DataRespDto;
import cn.com.yusys.yusp.server.xdsx0025.req.Xdsx0025DataReqDto;
import cn.com.yusys.yusp.server.xdsx0025.resp.Xdsx0025DataRespDto;
import cn.com.yusys.yusp.service.DscmsCfgSxClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsCfgSxClientServiceImpl implements DscmsCfgSxClientService {
    private static final Logger logger = LoggerFactory.getLogger(DscmsCfgSxClientServiceImpl.class);

    /**
     * 交易码：xdsx0007
     * 交易描述：授信业务授权同步、资本占用率参数表同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdsx0007DataRespDto> xdsx0007(Xdsx0007DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDSX0007.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDSX0007.value));
        return null;
    }

    /**
     * 交易码：xdsx0009
     * 交易描述：客户准入级别同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdsx0009DataRespDto> xdsx0009(Xdsx0009DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDSX0009.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDSX0009.value));
        return null;
    }

    /**
     * 交易码：xdsx0025
     * 交易描述：审批人资本占用率参数表同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdsx0025DataRespDto> xdsx0025(Xdsx0025DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDSX0025.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDSX0025.value));
        return null;
    }
}
