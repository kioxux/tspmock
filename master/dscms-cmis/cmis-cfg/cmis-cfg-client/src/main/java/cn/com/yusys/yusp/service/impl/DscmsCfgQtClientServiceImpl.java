package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.server.cmiscfg0001.req.CmisCfg0001ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0001.resp.CmisCfg0001RespDto;
import cn.com.yusys.yusp.server.cmiscfg0002.req.CmisCfg0002ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002RespDto;
import cn.com.yusys.yusp.server.cmiscfg0003.req.CmisCfg0003ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0003.resp.CmisCfg0003RespDto;
import cn.com.yusys.yusp.server.cmiscfg0004.req.CmisCfg0004ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0004.resp.CmisCfg0004RespDto;
import cn.com.yusys.yusp.server.xdqt0001.req.Xdqt0001DataReqDto;
import cn.com.yusys.yusp.server.xdqt0001.resp.Xdqt0001DataRespDto;
import cn.com.yusys.yusp.server.xdqt0007.req.Xdqt0007DataReqDto;
import cn.com.yusys.yusp.server.xdqt0007.resp.Xdqt0007DataRespDto;
import cn.com.yusys.yusp.service.DscmsCfgQtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsCfgQtClientServiceImpl implements DscmsCfgQtClientService {

    private static final Logger logger = LoggerFactory.getLogger(DscmsCfgQtClientServiceImpl.class);

    /**
     * 交易码：xdqt0001
     * 交易描述：根据产品号查询产品名称
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdqt0001DataRespDto> xdqt0001(Xdqt0001DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDQT0001.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDQT0001.value));
        return null;
    }

    /**
     * 交易码：xdqt0007
     * 交易描述：新微贷产品信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdqt0007DataRespDto> xdqt0007(Xdqt0007DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDQT0007.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDQT0007.value));
        return null;
    }


    @Override
    public ResultDto<CmisCfg0001RespDto> cmisCfg0001(CmisCfg0001ReqDto reqDto) {
        logger.error("访问失败，触发熔断。", DscmsCfgEnum.TRADE_CODE_CMISCFG0001.key.concat("|").concat(DscmsCfgEnum.TRADE_CODE_CMISCFG0001.value));
        return null;
    }

    @Override
    public ResultDto<CmisCfg0002RespDto> cmisCfg0002(CmisCfg0002ReqDto reqDto) {
        logger.error("访问失败，触发熔断。", DscmsCfgEnum.TRADE_CODE_CMISCFG0002.key.concat("|").concat(DscmsCfgEnum.TRADE_CODE_CMISCFG0002.value));
        return null;
    }

    @Override
    public ResultDto<CmisCfg0003RespDto> cmisCfg0003(CmisCfg0003ReqDto reqDto) {
        logger.error("访问失败，触发熔断。", DscmsCfgEnum.TRADE_CODE_CMISCFG0003.key.concat("|").concat(DscmsCfgEnum.TRADE_CODE_CMISCFG0003.value));
        return null;
    }

    @Override
    public ResultDto<CmisCfg0004RespDto> cmisCfg0004(CmisCfg0004ReqDto reqDto) {
        logger.error("访问失败，触发熔断。", DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key.concat("|").concat(DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value));
        return null;
    }
}
