package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.server.xdxt0012.req.Xdxt0012DataReqDto;
import cn.com.yusys.yusp.server.xdxt0012.resp.Xdxt0012DataRespDto;
import cn.com.yusys.yusp.server.xdxt0013.req.Xdxt0013DataReqDto;
import cn.com.yusys.yusp.server.xdxt0013.resp.Xdxt0013DataRespDto;
import cn.com.yusys.yusp.service.DscmsCfgXtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsCfgXtClientServiceImpl implements DscmsCfgXtClientService {
    private static final Logger logger = LoggerFactory.getLogger(DscmsCfgXtClientService.class);

    /**
     * 交易码：xdxt0012
     * 交易描述：根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0012DataRespDto> xdxt0012(Xdxt0012DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDXT0012.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDXT0012.value));
        return null;
    }

    /**
     * 交易码：xdxt0013
     * 交易描述：树形字典通用列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0013DataRespDto> xdxt0013(Xdxt0013DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDXT0013.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDXT0013.value));
        return null;
    }
}
