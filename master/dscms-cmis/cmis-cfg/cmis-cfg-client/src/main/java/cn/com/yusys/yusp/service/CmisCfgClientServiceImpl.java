package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;

@Component
public class CmisCfgClientServiceImpl implements ICmisCfgClientService {
    private static final Logger logger = LoggerFactory.getLogger(CmisCfgClientServiceImpl.class);

    @Override
    public CfgHandoverClientDto queryCfgHandover(CfgHandoverClientDto cfgHandoverDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 获取单表的数据流映射，返回处理后数据集合
     *
     * @param cfgToBizFlowDataDto
     * @return
     */
    @Override
    public CfgToBizFlowDataDto queryCfgDataFlowInfo4Out(CfgToBizFlowDataDto cfgToBizFlowDataDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> insertUserOftUseFunc(CfgUserOftUseFuncClientDto cfgUserOftUseFuncClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public int deleteUserOftUseFunc(CfgUserOftUseFuncClientDto cfgUserOftUseFuncClientDto) {
        logger.error("访问失败，触发熔断。");
        return 0;
    }

    @Override
    public List<CfgUserOftUseFuncClientDto> getUserOftUseFunc(String loginCode) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }

    /**
     * 获取list的数据流映射
     *
     * @param cfgToBizFlowDataDto
     * @return
     */
    @Override
    public CfgToBizFlowDataDto queryCfgDataFlowInfo4Out4List(CfgToBizFlowDataDto cfgToBizFlowDataDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 参数配置查询
     *
     * @param cfgClientParamDto
     * @return
     */
    @Override
    public CfgClientParamDto getCfgClientParam(CfgClientParamDto cfgClientParamDto) {
        return null;
    }

    @Override
    public ResultDto<Map> generateQueryListSQLByQryCode(String qryCode) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgOrderDownloadRecordDto> insertDownloadRecord(CfgOrderDownloadRecordDto cfgOrderDownloadRecordDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public CfgOrderDownloadDto getOrderCfg(String pkId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }


    @Override
    public String getOrderFilePath(String pkId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public List<CfgOrderDownloadDto> getOrderCfgByDataSource(String dataSource) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 参数配置查询
     *
     * @param map
     * @return
     */
    @Override
    public FncConfDefFmtDto getFncConfDefFmtByPrimaryKey(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 财报样式配置查询
     *
     * @param map
     * @return
     */
    @Override
    public FncConfStylesDto getFncConfStylesByPrimaryKey(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 根据项目编号查询项目信息
     *
     * @param map
     * @return
     */

    @Override
    public FncConfItemsDto getFncConfItemsByPrimaryKey(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 根据样式编号去查询所有的项目列表
     *
     * @param map
     * @return
     */

    @Override
    public List<FncConfItemsDto> getItemsListByStyleId(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 根据样式编号去查询所有的项目列表
     *
     * @param map
     * @return
     */

    @Override
    public List<FncConfDefFmtDto> getFncConfDefFmtByStyleId(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }


    @Override
    public List<LmtSubPrdMappConfDto> getLmtSubPrdMappConf(List<CfgPrdCatalogDto> cfgPrdCatalogDtos) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgPrdBasicinfoTranDto>> queryPrdTree(Map<String, String> map) {
        return null;
    }


    /**
     * 根据配置类型查询信用配置信息
     */
    @Override
    public ResultDto<List<CfgCreditLevelDto>> queryCfgCreditLevelByType(CfgCreditLevelDto cfgCreditLevelDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgTfRateDto> queryCfgTfRate(CfgTfRateQueryDto cfgTfRateQueryDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto selectlpr(CfgLprRateDto cfgLprRateDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgLprRateDto> selectone(QueryModel queryModel) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> queryContInfoByEnName4Biz(Map<String, String> map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgSOrgElecSealDto> selectByOrgNo(String orgNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgOrgElecSeal> selectFromCfgByOrgNo(String orgNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }


    @Override
    public ResultDto<CfgSxkdGuarDiscountDto> selectCfgSxkdGuarDiscountByGuarTypeCd(String guarTypeCd) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgPrdBasicinfoDto> queryCfgPrdBasicInfo(String prdId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Boolean> upload(Map<String, String> paramMap) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgSftpDto> queryCfgSftpByBizScenseType(String bizScenseType) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    public ResultDto<CfgAccountClassChooseDto> queryHxAccountClassByProps(CfgAccountClassChooseDto cfgAccountClassChooseDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgSorgFinaDto>> selecSorgFina(QueryModel queryModel) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgAccountClassChooseDto> queryHxAccountClassByAcccountClass(String accountClass) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgSorgLoanManyDto>> selecSorgLoanMany(QueryModel queryModel) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgWyClassRelClientDto> selectbycondition(CfgWyClassRelClientDto cfgWyClassRelClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> getWorkDaysByMonth(Date date) {
        return null;
    }

    @Override
    public ResultDto<List<CfgAccpOrgRelDto>> selectAccpOrg(QueryModel queryModel) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgPrdTypePropertiesDto>> queryCfgPrdTypePropertiesByProrNo(String typePropNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgPrdBasicinfoDto>> queryBasicInfoByCatalogId(String catalogId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgPrdBasicinfoDto>> queryBasicInfoByCatalog(String cataloglevelName) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgPrdBasicinfoDto>> queryCfgPrdBasicInfoByPrdType(String prdType) {
        return null;
    }

    @Override
    public ResultDto<Map<String, String>> querySingleLabelPath(AdminSmTreeDicDto adminSmTreeDicDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgApproveAuthorityDto> queryDutyByCusDebtLevel(CfgApproveAuthorityDto cfgApproveAuthorityDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgApproveAuthorityDto> queryRateByCusDebtLevel(CfgApproveAuthorityDto cfgApproveAuthorityDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgApproveCapitalrateDto> queryDutyByRate(CfgApproveCapitalrateDto cfgApproveCapitalrateDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 查询临时存储文件配置表(提供给frpt服务)
     *
     * @param pkid
     * @return
     */
    @Override
    public ResultDto<List<CfgGenerateTempFileDto>> queryCfgFilebyPkid(String pkid) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgOrgElecSeal>> selectByCondition(Map<String, String> map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public String querySuitReportName(CfgCtrContPrintReqDto cfgCtrContPrintReqDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgRetailPrimeRateDto> selectbyPrdId(String prdId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public List<CfgRetailPrimeRateDto> selectRetailPrimerateByCondition(CfgRetailPrimeRateDto cfgRetailPrimeRateDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgWyClassRelClientDto> selectByCondition(CfgWyClassRelClientDto cfgWyClassRelClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgBankInfoDto> selectbybankno(String bankNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgLprRateDto> selectAfterLpr(@RequestBody  CfgLprRateDto cfgLprRateDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgLprRateDto> selectFrontLpr(@RequestBody  CfgLprRateDto cfgLprRateDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgXdLoanOrgDto> queryCfgXdLoanOrg(@RequestBody CfgXdLoanOrgDto cfgXdLoanOrgDto){
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgTfRateDto>> queryAllCfgTfRate(@RequestBody QueryModel model){
        logger.error("访问失败，触发熔断。");
        return null;
    }
}
