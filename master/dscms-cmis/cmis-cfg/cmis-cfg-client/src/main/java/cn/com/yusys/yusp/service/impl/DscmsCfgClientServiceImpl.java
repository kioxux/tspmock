package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@Component
public class DscmsCfgClientServiceImpl implements DscmsCfgClientService {
    private static final Logger logger = LoggerFactory.getLogger(DscmsCfgClientServiceImpl.class);

    @Override
    public ResultDto<List<CfgGenerateTempFileDto>> queryCfgFile(QueryModel queryModel) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgSOrgElecSealDto>> queryCfgSeal(QueryModel queryModel) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CfgDiscOrgLmtRespDto>> getListByQueryMap(Map queryMap) {
        logger.error("访问{}失败，触发熔断。", DscmsCfgEnum.CFG_DISC_ORG_LMT_GETLISTBYQUERYMAP.key.concat("|").concat(DscmsCfgEnum.CFG_DISC_ORG_LMT_GETLISTBYQUERYMAP.value));
        return null;
    }

    @Override
    public ResultDto<Integer> queryAreaNum(String areaCode, String agriFlg) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Map<String, String>> querySingleLabelPath(AdminSmTreeDicDto adminSmTreeDicDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CfgPrdBasicinfoDto> queryCfgprdInfoByprcId(CfgPrdBasicinfoDto cfgPrdBasicinfoDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<AdminSmTreeDicDto> queryCfgTreeByCode(AdminSmTreeDicDto adminSmTreeDicDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

}
