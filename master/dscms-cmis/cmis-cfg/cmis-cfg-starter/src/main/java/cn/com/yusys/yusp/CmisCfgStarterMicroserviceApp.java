package cn.com.yusys.yusp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

@EnableDiscoveryClient
@SpringBootApplication
@MapperScan({"cn.com.yusys.yusp.**.repository.mapper"})
@EnableFeignClients("cn.com.yusys.yusp")
@EnableTransactionManagement
public class CmisCfgStarterMicroserviceApp {
    public static void main(String[] args) {
        Environment env = SpringApplication.run(CmisCfgStarterMicroserviceApp.class, args).getEnvironment();
    }
}
