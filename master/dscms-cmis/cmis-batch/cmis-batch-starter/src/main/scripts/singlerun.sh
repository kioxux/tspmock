#!/bin/sh
if [ $# -ne "2" ]
then
	echo "please enter two parameters,include taskNo and openDay"
	exit;
fi

export taskNo=$1
export openDay=$2
export WORKDIR=/home/dscms/bin
export IP=10.85.10.211
echo "taskNo is: "  taskNo
echo "openDay is: "  openDay
echo "WORKDIR is: "  $WORKDIR
echo "IP is:" + $IP

echo "invoke singlerun4shell method begin"
result=`curl -H "Content-Type: application/json" -s -X POST "http://$IP:6008/api/batchresource/singlerun4shell?taskNo=$taskNo&openDay=$openDay"`
echo "invoke singlerun4shell method end"
echo "result is: "  $result

## 0-success 1-fail
if [ $result == 0 ];then
    exit 0
else
    exit 1
fi