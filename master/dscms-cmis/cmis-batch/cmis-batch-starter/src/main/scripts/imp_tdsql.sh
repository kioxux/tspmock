#!/bin/sh
if [ $# -ne "8" ]
then
	echo "usage:Please Enter PROXY_HOST,PROXY_PORT,USER,PASSWORD,DB_TABLE,OPEN_DAY,DATA_FILE_NAME,LOG_FILE_NAME"
fi 

export SHELL_HOME=/home/dscms/bin
export PROXY_HOST=$1
export PROXY_PORT=$2
export USER=$3
export PASSWORD=$4
export DB_TABLE=$5
export OPEN_DAY=$6
export DATA_FILE_NAME=$7
export LOG_FILE_NAME=$8

echo "IMP TDSQL PARAMS BEGIN"
echo "SHELL_HOME:"$SHELL_HOME
echo "PROXY_HOST:"$PROXY_HOST
echo "PROXY_PORT:"$PROXY_PORT
echo "USER:"$USER
echo "PASSWORD:"$PASSWORD
echo "DB_TABLE:"$DB_TABLE
echo "OPEN_DAY:"$OPEN_DAY
echo "DATA_FILE_NAME:"$DATA_FILE_NAME
echo "LOG_FILE_NAME:"$LOG_FILE_NAME
echo "IMP TDSQL PARAMS END"

echo "IMP TDSQL BEGIN"
cd $SHELL_HOME
./load_data mode1 $PROXY_HOST $PROXY_PORT $USER $PASSWORD  $DB_TABLE auto /home/dscms/batch/work/$OPEN_DAY/$DATA_FILE_NAME ' | ' ''  >  /home/dscms/batch/work/$OPEN_DAY/$LOG_FILE_NAME;
echo "IMP TDSQL END"

echo "IMP TDSQL SUCCESS"
exit
