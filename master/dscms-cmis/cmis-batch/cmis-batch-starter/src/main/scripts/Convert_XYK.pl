#!/usr/bin/perl

%fileoption = (
    'ACCOUNT' => [qw/9 9 6 3 13 13 8 8 5 5 5 15 15 15 15 15 13 8 2 1 1 1 27 1 1 80 9 40 80 8 8 15 24 15 8 15 15 8 15 300 15 2 2 200/],
    'ADDRESS' => [qw/9 9 1 3 40 40 40 10 20 200 200/],
    'CARD' => [qw/19 9 9 6 20 1 19 9 80 19 8 27 1 8 8 20 20 80 1 1 6 1 2 1 1 300 19 40 20 26 8/],
    'CONTACT' => [qw/9 9 1 80 1 20 8 80 1 30 20 20 1 200/],
    'CUSTOMER' => [qw/9 20 1 30 80 1 8 1 1 20 3 1 3 1 1 1 200 20 1 1 6 1 20 80 1 2 4 8 15 20 8 1 80 13 13 13 200/],
    'LOAN' => [qw/9 9 23 19 8 1 1 3 3 15 15 15 15 15 15 15 15 8 15 15 4 1 2 1 15 8 2 200/],
    'TXN' => [qw/9 9 19 8 6 15 3 6 4 8 15 3 23 40 13 9 15 40 4 20 20 15 200 1 11 11 2 30 28 118/],
    'DDREQ' => [qw/12 80 9 1 19 1 80 9 20 80 9 15 8/],
    'GLOUT' => [qw/12 40 9 8 11 15 1 11 15 1 3 15 1 30/],
    'S04' => [qw/36 19 3 4 80 1 15 1 9 40 1 1/],
    'CARD_MEDIA_MAP' => [qw/19 19 200/],
    'APP01_HISTORY' => [qw/20 80 20 400 3 1000 200 40 8 200/],
    'APP01_PRIMARY_INFO' => [qw/13 12 20 1 19 80 30 1 2 3 3 1 30 8 200 8 1 13 10 1 10 1 3 40 40 40 1 200 10 20 8 20 80 1 2 13 1 20 80 3 40 40 40 200 10 20 80 1 1 4 2 20 1 4 40 8 1 1 10 1 1 1 20 20 1 2 1 80 40 80 1 8 40 8 40 50/],
    'APP_MAIN' =>[qw/20 1 13 13 13 6 1 3 1 20 3 2 1000 2 1 20 20 9 1 80 20 20 1 9 15 20 20 1 1 1 20 20 1 200 8 40 8 40 200/],
    'APP01_MAIN' =>[qw/20 1 13 13 13 6 1 3 1 20 3 2 1000 2 1 20 20 9 1 80 20 20 1 9 15 20 20 1 1 1 20 20 1 200 8 40 8 40 200/],
    'TXN_LOAN' =>[qw/9 19 6 23 8 8 7 7 3 3 15 15 15 15 15 15 117/],
    'EMPLOYEE' => [qw/9 9 80 20 20 1 1 1 12 12 1 20 8 172/],
);
$spl = " | ";
$seq = "\n";

chdir '/sunline/ETL/';

$file = $ARGV[0];
$reg = $ARGV[1];
$first = $ARGV[2];
$newfile = $ARGV[3];

$nfile = "new_".$newfile;
#print "file: $file \n";

die "no config!!\n" if not exists $fileoption{$reg};

$option = join '',map {"(.{$_})"} @{$fileoption{$reg}};
#print "option: $option \n";

open F,"<$file";
open NF,">$nfile";

$i = 0;
while (<F>) {
    $i++;
    next if $i == 1 && $first eq 'Y';
    chomp;
    @arr = $_ =~ m/$option/g;
    $newline = join $spl,@arr;
    print NF "$newline",$seq;
}

close(F);
close(NF);