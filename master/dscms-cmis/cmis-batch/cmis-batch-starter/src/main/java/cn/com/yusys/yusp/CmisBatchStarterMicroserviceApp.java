package cn.com.yusys.yusp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.amqp.RabbitHealthContributorAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;
import tk.mybatis.spring.annotation.MapperScan;

@EnableDiscoveryClient
@SpringBootApplication(exclude = {BatchAutoConfiguration.class, RabbitHealthContributorAutoConfiguration.class})
@MapperScan({"cn.com.yusys.yusp.batch.repository.mapper", "cn.com.yusys.yusp.spring.batch"})
@EnableFeignClients("cn.com.yusys.yusp")
@EnableTransactionManagement
public class CmisBatchStarterMicroserviceApp {
    public static void main(String[] args) {
        SpringApplication.run(CmisBatchStarterMicroserviceApp.class, args).getEnvironment();
    }
}
