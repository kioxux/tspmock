/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.gjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGjpTfbIftMl
 * @类描述: bat_s_gjp_tfb_ift_ml数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-24 00:12:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_gjp_tfb_ift_ml")
public class BatSGjpTfbIftMl extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 业务主关联流水号   **/
	@Column(name = "SYS_MAIN_REL_NO", unique = false, nullable = true, length = 32)
	private String sysMainRelNo;
	
	/** 系统实体ID **/
	@Column(name = "SYS_ENTY_ID", unique = false, nullable = true, length = 32)
	private String sysEntyId;
	
	/** 系统删除标志 **/
	@Column(name = "SYS_DEL_FLG", unique = false, nullable = true, length = 1)
	private String sysDelFlg;
	
	/** 创建人 **/
	@Column(name = "SYS_CRT_USER", unique = false, nullable = true, length = 32)
	private String sysCrtUser;
	
	/** 系统创建日期 **/
	@Column(name = "SYS_CRT_DT", unique = false, nullable = true, length = 20)
	private String sysCrtDt;
	
	/** 系统修改用户 **/
	@Column(name = "SYS_MODIFY_USER", unique = false, nullable = true, length = 32)
	private String sysModifyUser;
	
	/** 系统修改日期 **/
	@Column(name = "SYS_MODIFY_DT", unique = false, nullable = true, length = 20)
	private String sysModifyDt;
	
	/** 业务流水号 **/
	@Column(name = "BIZ_ID", unique = false, nullable = true, length = 32)
	private String bizId;
	
	/** 入账账号 **/
	@Column(name = "ACC_NO", unique = false, nullable = true, length = 40)
	private String accNo;
	
	/** 承兑金额 **/
	@Column(name = "ACP_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal acpAmt;
	
	/** 承兑币种 **/
	@Column(name = "ACP_CCY", unique = false, nullable = true, length = 3)
	private String acpCcy;
	
	/** 承兑日期 **/
	@Column(name = "ACP_DT", unique = false, nullable = true, length = 8)
	private String acpDt;
	
	/** 承兑到期日 **/
	@Column(name = "ACP_EXPIRY_DT", unique = false, nullable = true, length = 8)
	private String acpExpiryDt;
	
	/** 预约登记标示 **/
	@Column(name = "APPM_FLG", unique = false, nullable = true, length = 1)
	private String appmFlg;
	
	/** 申请人代码 **/
	@Column(name = "APP_ID", unique = false, nullable = true, length = 32)
	private String appId;
	
	/** 申请人名称 **/
	@Column(name = "APP_NM", unique = false, nullable = true, length = 140)
	private String appNm;
	
	/** 受益行行号 **/
	@Column(name = "BEN_BK_NO", unique = false, nullable = true, length = 100)
	private String benBkNo;
	
	/** 业务归属 **/
	@Column(name = "BIZ_BR_CDE", unique = false, nullable = true, length = 8)
	private String bizBrCde;
	
	/** 买断行信息 **/
	@Column(name = "BUYER_BK_INFO", unique = false, nullable = true, length = 512)
	private String buyerBkInfo;
	
	/** 保兑行BIC  **/
	@Column(name = "CNF_BIC", unique = false, nullable = true, length = 16)
	private String cnfBic;
	
	/** 保兑行名称 **/
	@Column(name = "CNF_BK_NM", unique = false, nullable = true, length = 140)
	private String cnfBkNm;
	
	/** 收取承诺费 **/
	@Column(name = "COMMON_FLG", unique = false, nullable = true, length = 1)
	private String commonFlg;
	
	/** 收款行行号 **/
	@Column(name = "CONSN_RECVR_BK_NO", unique = false, nullable = true, length = 32)
	private String consnRecvrBkNo;
	
	/** 还款来源 **/
	@Column(name = "CONSN_REFUND_FROM", unique = false, nullable = true, length = 8)
	private String consnRefundFrom;
	
	/** 同业客户BIC  **/
	@Column(name = "CST_CHN_BIC", unique = false, nullable = true, length = 16)
	private String cstChnBic;
	
	/** 客户名称 **/
	@Column(name = "CST_NM", unique = false, nullable = true, length = 140)
	private String cstNm;
	
	/** 客户类型 **/
	@Column(name = "CST_TYPE", unique = false, nullable = true, length = 8)
	private String cstType;
	
	/** 转卖收入金额 **/
	@Column(name = "CVT_INC_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal cvtIncAmt;
	
	/** 转卖收入币种 **/
	@Column(name = "CVT_INC_CCY", unique = false, nullable = true, length = 3)
	private String cvtIncCcy;
	
	/** 到期日 **/
	@Column(name = "EXPIRY_DT", unique = false, nullable = true, length = 8)
	private String expiryDt;
	
	/** 申请业务编号 **/
	@Column(name = "FFT_APL_NO", unique = false, nullable = true, length = 32)
	private String fftAplNo;
	
	/** 交单金额 **/
	@Column(name = "FFT_BILL_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftBillAmt;
	
	/** 交单币种 **/
	@Column(name = "FFT_BILL_CCY", unique = false, nullable = true, length = 3)
	private String fftBillCcy;
	
	/** 买断债权付款期限 **/
	@Column(name = "FFT_BILL_PAY_TENOR", unique = false, nullable = true, length = 64)
	private String fftBillPayTenor;
	
	/** 买断债权种类 **/
	@Column(name = "FFT_BILL_TYPE", unique = false, nullable = true, length = 8)
	private String fftBillType;
	
	/** 买断行BIC  **/
	@Column(name = "FFT_BUYER_BIC_CDE", unique = false, nullable = true, length = 16)
	private String fftBuyerBicCde;
	
	/** 买断行名址 **/
	@Column(name = "FFT_BUYER_BK_NM", unique = false, nullable = true, length = 140)
	private String fftBuyerBkNm;
	
	/** 兑付方式 **/
	@Column(name = "FFT_CASH_TYPE", unique = false, nullable = true, length = 8)
	private String fftCashType;
	
	/** 清算场景 **/
	@Column(name = "FFT_CLR_TYPE", unique = false, nullable = true, length = 8)
	private String fftClrType;
	
	/** 宽限期类型 **/
	@Column(name = "FFT_DATE_TYPE", unique = false, nullable = true, length = 8)
	private String fftDateType;
	
	/** 福费廷业务类型 **/
	@Column(name = "FFT_DEAL_TYPE", unique = false, nullable = true, length = 8)
	private String fftDealType;
	
	/** 市场等级 **/
	@Column(name = "FFT_DEAL_TYPE1", unique = false, nullable = true, length = 8)
	private String fftDealType1;
	
	/** 业务流水号 **/
	@Column(name = "SYS_REF_NO", unique = false, nullable = true, length = 32)
	private String sysRefNo;
	
	/** 关联流水号 **/
	@Column(name = "SYS_REL_NO", unique = false, nullable = true, length = 32)
	private String sysRelNo;
	
	/** 福费廷宽限期 **/
	@Column(name = "FFT_GRACE_DAYS", unique = false, nullable = true, length = 10)
	private Integer fftGraceDays;
	
	/** 福费廷融资利息 **/
	@Column(name = "FFT_INT_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftIntAmt;
	
	/** 福费廷手续费利息金额 **/
	@Column(name = "FFT_INT_AMT5", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftIntAmt5;
	
	/** 我行利息币种 **/
	@Column(name = "FFT_INT_CCY", unique = false, nullable = true, length = 3)
	private String fftIntCcy;
	
	/** 手续费利息币种 **/
	@Column(name = "FFT_INT_CCY5", unique = false, nullable = true, length = 3)
	private String fftIntCcy5;
	
	/** 入客户帐 **/
	@Column(name = "FFT_IN_CST_FLG", unique = false, nullable = true, length = 1)
	private String fftInCstFlg;
	
	/** 福费廷融资最低收费金额 **/
	@Column(name = "FFT_MIN_AMT1", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftMinAmt1;
	
	/** 福费廷手续费率最低收费金额 **/
	@Column(name = "FFT_MIN_AMT2", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftMinAmt2;
	
	/** 手续费报价最低金额 **/
	@Column(name = "FFT_MIN_AMT3", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftMinAmt3;
	
	/** 福费廷融资最低收费币种 **/
	@Column(name = "FFT_MIN_CCY1", unique = false, nullable = true, length = 3)
	private String fftMinCcy1;
	
	/** 福费廷手续费率最低收费币种 **/
	@Column(name = "FFT_MIN_CCY2", unique = false, nullable = true, length = 3)
	private String fftMinCcy2;
	
	/** 福费廷手续费率最低币种 **/
	@Column(name = "FFT_MIN_CCY3", unique = false, nullable = true, length = 3)
	private String fftMinCcy3;
	
	/** 我行预扣费用 **/
	@Column(name = "FFT_ORD_DEDUCT_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftOrdDeductAmt;
	
	/** 我行预扣费用币种 **/
	@Column(name = "FFT_ORD_DEDUCT_CCY", unique = false, nullable = true, length = 3)
	private String fftOrdDeductCcy;
	
	/** 预计到期日 **/
	@Column(name = "FFT_ORD_DT", unique = false, nullable = true, length = 8)
	private String fftOrdDt;
	
	/** 我行原始报价格式 **/
	@Column(name = "FFT_ORGI_QUOT_BY", unique = false, nullable = true, length = 128)
	private String fftOrgiQuotBy;
	
	/** 福费廷手续费率报价格式 **/
	@Column(name = "FFT_ORGI_QUOT_BY1", unique = false, nullable = true, length = 128)
	private String fftOrgiQuotBy1;
	
	/** 付款银行 **/
	@Column(name = "FFT_PAY_BK_NM", unique = false, nullable = true, length = 140)
	private String fftPayBkNm;
	
	/** 融资本金差额金额 **/
	@Column(name = "FFT_PRI_DF_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftPriDfAmt;
	
	/** 融资本金差额币种 **/
	@Column(name = "FFT_PRI_DF_CCY", unique = false, nullable = true, length = 3)
	private String fftPriDfCcy;
	
	/** 我行报价加点数 **/
	@Column(name = "FFT_QUOT_BPS", unique = false, nullable = true, length = 10)
	private Integer fftQuotBps;
	
	/** 手续费率报价加点数 **/
	@Column(name = "FFT_QUOT_BPS1", unique = false, nullable = true, length = 10)
	private Integer fftQuotBps1;
	
	/** 加点数 **/
	@Column(name = "FFT_QUOT_BPS3", unique = false, nullable = true, length = 8)
	private String fftQuotBps3;
	
	/** 福费廷最终承诺费率 **/
	@Column(name = "FFT_QUOT_COMM", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftQuotComm;
	
	/** 承诺费率 **/
	@Column(name = "FFT_QUOT_COMM_RT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftQuotCommRt;
	
	/** 我行报价利率 **/
	@Column(name = "FFT_QUOT_INT_RT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftQuotIntRt;
	
	/** 福费廷手续费率报价利率 **/
	@Column(name = "FFT_QUOT_INT_RT1", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftQuotIntRt1;
	
	/** 利率 **/
	@Column(name = "FFT_QUOT_INT_RT3", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftQuotIntRt3;
	
	/** 我行报价利率类型 **/
	@Column(name = "FFT_QUOT_INT_RT_TYPE", unique = false, nullable = true, length = 8)
	private String fftQuotIntRtType;
	
	/** 福费廷手续费率报价利率类型 **/
	@Column(name = "FFT_QUOT_INT_RT_TYPE1", unique = false, nullable = true, length = 8)
	private String fftQuotIntRtType1;
	
	/** 利率类型 **/
	@Column(name = "FFT_QUOT_INT_RT_TYPE3", unique = false, nullable = true, length = 8)
	private String fftQuotIntRtType3;
	
	/** 我行报价有效期 **/
	@Column(name = "FFT_QUOT_TENOR_DAYS", unique = false, nullable = true, length = 8)
	private String fftQuotTenorDays;
	
	/** 手续费报价利率期限 **/
	@Column(name = "FFT_QUOT_TENOR_DAYS1", unique = false, nullable = true, length = 8)
	private String fftQuotTenorDays1;
	
	/** 期限 **/
	@Column(name = "FFT_QUOT_TENOR_DAYS3", unique = false, nullable = true, length = 8)
	private String fftQuotTenorDays3;
	
	/** 收汇金额 **/
	@Column(name = "FFT_RECVD_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftRecvdAmt;
	
	/** 收汇币种 **/
	@Column(name = "FFT_RECVD_CCY", unique = false, nullable = true, length = 3)
	private String fftRecvdCcy;
	
	/** 收汇来源 **/
	@Column(name = "FFT_RECVD_SRC", unique = false, nullable = true, length = 8)
	private String fftRecvdSrc;
	
	/** 融资关联业务编号 **/
	@Column(name = "FFT_REL_NO", unique = false, nullable = true, length = 32)
	private String fftRelNo;
	
	/** 起息日 **/
	@Column(name = "FFT_VALUE_DT", unique = false, nullable = true, length = 8)
	private String fftValueDt;
	
	/** 融资余额 **/
	@Column(name = "FIN_PRI_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal finPriBal;
	
	/** 受理渠道 **/
	@Column(name = "HD_CHANNEL", unique = false, nullable = true, length = 8)
	private String hdChannel;
	
	/** 处理类型 **/
	@Column(name = "HD_TYPE", unique = false, nullable = true, length = 8)
	private String hdType;
	
	/** 国内证福费廷操作步骤 **/
	@Column(name = "IFT_CASE_STEP", unique = false, nullable = true, length = 10)
	private Integer iftCaseStep;
	
	/** 国内证福费廷业务编号 **/
	@Column(name = "IFT_RG_NO", unique = false, nullable = true, length = 32)
	private String iftRgNo;
	
	/** 付息金额 **/
	@Column(name = "INT_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal intAmt;
	
	/** 付息币种 **/
	@Column(name = "INT_CCY", unique = false, nullable = true, length = 3)
	private String intCcy;
	
	/** 付息日期 **/
	@Column(name = "INT_DATE", unique = false, nullable = true, length = 8)
	private String intDate;
	
	/** 买入金额 **/
	@Column(name = "IN_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal inAmt;
	
	/** 买入币种 **/
	@Column(name = "IN_CUR", unique = false, nullable = true, length = 3)
	private String inCur;
	
	/** 开证行BIC  **/
	@Column(name = "ISS_BK_BIC", unique = false, nullable = true, length = 16)
	private String issBkBic;
	
	/** 开证行名称 **/
	@Column(name = "ISS_BK_NM", unique = false, nullable = true, length = 140)
	private String issBkNm;
	
	/** 给开证/保兑行信息  **/
	@Column(name = "ISS_CNF_INFO", unique = false, nullable = true, length = 2048)
	private String issCnfInfo;
	
	/** 放款净额 **/
	@Column(name = "LOAN_ACTUAL_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal loanActualAmt;
	
	/** 放款净额币种 **/
	@Column(name = "LOAN_ACTUAL_CCY", unique = false, nullable = true, length = 3)
	private String loanActualCcy;
	
	/** 放款日期 **/
	@Column(name = "LOAN_DT", unique = false, nullable = true, length = 8)
	private String loanDt;
	
	/** 组织机构代码 **/
	@Column(name = "ORG_CDE", unique = false, nullable = true, length = 32)
	private String orgCde;
	
	/** 其他资金去向 **/
	@Column(name = "OTH_FUND_WHERE", unique = false, nullable = true, length = 8)
	private String othFundWhere;
	
	/** 付款期限 **/
	@Column(name = "PAY_DAYS", unique = false, nullable = true, length = 3)
	private String payDays;
	
	/** 交单日期 **/
	@Column(name = "PRESNT_DT", unique = false, nullable = true, length = 8)
	private String presntDt;
	
	/** 汇款途径 **/
	@Column(name = "REMT_ROUTE", unique = false, nullable = true, length = 8)
	private String remtRoute;
	
	/** 收汇日期 **/
	@Column(name = "SETT_DT", unique = false, nullable = true, length = 8)
	private String settDt;
	
	/** 交易日期 **/
	@Column(name = "TX_DT", unique = false, nullable = true, length = 8)
	private String txDt;
	
	/** 扣客户帐 **/
	@Column(name = "FFT_DEDUCT_CST_FLG", unique = false, nullable = true, length = 1)
	private String fftDeductCstFlg;
	
	/** 资金来源 **/
	@Column(name = "FFT_FUND_FROM", unique = false, nullable = true, length = 8)
	private String fftFundFrom;
	
	/** 我行原始报价格式 **/
	@Column(name = "FFT_ORGI_QUOT_BY3", unique = false, nullable = true, length = 128)
	private String fftOrgiQuotBy3;
	
	/** 信贷出账号 **/
	@Column(name = "CMS_REF_NO", unique = false, nullable = true, length = 40)
	private String cmsRefNo;
	
	/** 他行手续费报价利率 **/
	@Column(name = "FFT_QUOT_INT_RT4", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftQuotIntRt4;
	
	/** 福费廷他行手续费最低收费币种 **/
	@Column(name = "FFT_MIN_CCY4", unique = false, nullable = true, length = 3)
	private String fftMinCcy4;
	
	/** 福费廷他行手续费率最低收费金额 **/
	@Column(name = "FFT_MIN_AMT4", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftMinAmt4;
	
	/** 福费廷他行手续费率报价格式 **/
	@Column(name = "FFT_ORGI_QUOT_BY4", unique = false, nullable = true, length = 128)
	private String fftOrgiQuotBy4;
	
	/** 福费廷他行融资利息币种 **/
	@Column(name = "FFT_INT_CCY1", unique = false, nullable = true, length = 3)
	private String fftIntCcy1;
	
	/** 福费廷他行融资利息金额 **/
	@Column(name = "FFT_INT_AMT1", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftIntAmt1;
	
	/** 福费廷他行手续费利息币种 **/
	@Column(name = "FFT_INT_CCY4", unique = false, nullable = true, length = 3)
	private String fftIntCcy4;
	
	/** 福费廷他行手续费利息金额 **/
	@Column(name = "FFT_INT_AMT4", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftIntAmt4;
	
	/** 他行预扣费用币种 **/
	@Column(name = "FFT_ORD_DEDUCT_CCY1", unique = false, nullable = true, length = 3)
	private String fftOrdDeductCcy1;
	
	/** 他行预扣费用 **/
	@Column(name = "FFT_ORD_DEDUCT_AMT1", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal fftOrdDeductAmt1;
	
	/** 支付买断行币种 **/
	@Column(name = "PAY_BUYER_CCY", unique = false, nullable = true, length = 3)
	private String payBuyerCcy;
	
	/** 支付买断行金额 **/
	@Column(name = "PAY_BUYER_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal payBuyerAmt;
	
	/** 开证行行号 **/
	@Column(name = "ISS_BK_NO", unique = false, nullable = true, length = 32)
	private String issBkNo;
	
	/** 议付编号 **/
	@Column(name = "BP_NO", unique = false, nullable = true, length = 32)
	private String bpNo;
	
	
	/**
	 * @param sysMainRelNo
	 */
	public void setSysMainRelNo(String sysMainRelNo) {
		this.sysMainRelNo = sysMainRelNo;
	}
	
    /**
     * @return sysMainRelNo
     */
	public String getSysMainRelNo() {
		return this.sysMainRelNo;
	}
	
	/**
	 * @param sysEntyId
	 */
	public void setSysEntyId(String sysEntyId) {
		this.sysEntyId = sysEntyId;
	}
	
    /**
     * @return sysEntyId
     */
	public String getSysEntyId() {
		return this.sysEntyId;
	}
	
	/**
	 * @param sysDelFlg
	 */
	public void setSysDelFlg(String sysDelFlg) {
		this.sysDelFlg = sysDelFlg;
	}
	
    /**
     * @return sysDelFlg
     */
	public String getSysDelFlg() {
		return this.sysDelFlg;
	}
	
	/**
	 * @param sysCrtUser
	 */
	public void setSysCrtUser(String sysCrtUser) {
		this.sysCrtUser = sysCrtUser;
	}
	
    /**
     * @return sysCrtUser
     */
	public String getSysCrtUser() {
		return this.sysCrtUser;
	}
	
	/**
	 * @param sysCrtDt
	 */
	public void setSysCrtDt(String sysCrtDt) {
		this.sysCrtDt = sysCrtDt;
	}
	
    /**
     * @return sysCrtDt
     */
	public String getSysCrtDt() {
		return this.sysCrtDt;
	}
	
	/**
	 * @param sysModifyUser
	 */
	public void setSysModifyUser(String sysModifyUser) {
		this.sysModifyUser = sysModifyUser;
	}
	
    /**
     * @return sysModifyUser
     */
	public String getSysModifyUser() {
		return this.sysModifyUser;
	}
	
	/**
	 * @param sysModifyDt
	 */
	public void setSysModifyDt(String sysModifyDt) {
		this.sysModifyDt = sysModifyDt;
	}
	
    /**
     * @return sysModifyDt
     */
	public String getSysModifyDt() {
		return this.sysModifyDt;
	}
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param bizId
	 */
	public void setBizId(String bizId) {
		this.bizId = bizId;
	}
	
    /**
     * @return bizId
     */
	public String getBizId() {
		return this.bizId;
	}
	
	/**
	 * @param accNo
	 */
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	
    /**
     * @return accNo
     */
	public String getAccNo() {
		return this.accNo;
	}
	
	/**
	 * @param acpAmt
	 */
	public void setAcpAmt(java.math.BigDecimal acpAmt) {
		this.acpAmt = acpAmt;
	}
	
    /**
     * @return acpAmt
     */
	public java.math.BigDecimal getAcpAmt() {
		return this.acpAmt;
	}
	
	/**
	 * @param acpCcy
	 */
	public void setAcpCcy(String acpCcy) {
		this.acpCcy = acpCcy;
	}
	
    /**
     * @return acpCcy
     */
	public String getAcpCcy() {
		return this.acpCcy;
	}
	
	/**
	 * @param acpDt
	 */
	public void setAcpDt(String acpDt) {
		this.acpDt = acpDt;
	}
	
    /**
     * @return acpDt
     */
	public String getAcpDt() {
		return this.acpDt;
	}
	
	/**
	 * @param acpExpiryDt
	 */
	public void setAcpExpiryDt(String acpExpiryDt) {
		this.acpExpiryDt = acpExpiryDt;
	}
	
    /**
     * @return acpExpiryDt
     */
	public String getAcpExpiryDt() {
		return this.acpExpiryDt;
	}
	
	/**
	 * @param appmFlg
	 */
	public void setAppmFlg(String appmFlg) {
		this.appmFlg = appmFlg;
	}
	
    /**
     * @return appmFlg
     */
	public String getAppmFlg() {
		return this.appmFlg;
	}
	
	/**
	 * @param appId
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
    /**
     * @return appId
     */
	public String getAppId() {
		return this.appId;
	}
	
	/**
	 * @param appNm
	 */
	public void setAppNm(String appNm) {
		this.appNm = appNm;
	}
	
    /**
     * @return appNm
     */
	public String getAppNm() {
		return this.appNm;
	}
	
	/**
	 * @param benBkNo
	 */
	public void setBenBkNo(String benBkNo) {
		this.benBkNo = benBkNo;
	}
	
    /**
     * @return benBkNo
     */
	public String getBenBkNo() {
		return this.benBkNo;
	}
	
	/**
	 * @param bizBrCde
	 */
	public void setBizBrCde(String bizBrCde) {
		this.bizBrCde = bizBrCde;
	}
	
    /**
     * @return bizBrCde
     */
	public String getBizBrCde() {
		return this.bizBrCde;
	}
	
	/**
	 * @param buyerBkInfo
	 */
	public void setBuyerBkInfo(String buyerBkInfo) {
		this.buyerBkInfo = buyerBkInfo;
	}
	
    /**
     * @return buyerBkInfo
     */
	public String getBuyerBkInfo() {
		return this.buyerBkInfo;
	}
	
	/**
	 * @param cnfBic
	 */
	public void setCnfBic(String cnfBic) {
		this.cnfBic = cnfBic;
	}
	
    /**
     * @return cnfBic
     */
	public String getCnfBic() {
		return this.cnfBic;
	}
	
	/**
	 * @param cnfBkNm
	 */
	public void setCnfBkNm(String cnfBkNm) {
		this.cnfBkNm = cnfBkNm;
	}
	
    /**
     * @return cnfBkNm
     */
	public String getCnfBkNm() {
		return this.cnfBkNm;
	}
	
	/**
	 * @param commonFlg
	 */
	public void setCommonFlg(String commonFlg) {
		this.commonFlg = commonFlg;
	}
	
    /**
     * @return commonFlg
     */
	public String getCommonFlg() {
		return this.commonFlg;
	}
	
	/**
	 * @param consnRecvrBkNo
	 */
	public void setConsnRecvrBkNo(String consnRecvrBkNo) {
		this.consnRecvrBkNo = consnRecvrBkNo;
	}
	
    /**
     * @return consnRecvrBkNo
     */
	public String getConsnRecvrBkNo() {
		return this.consnRecvrBkNo;
	}
	
	/**
	 * @param consnRefundFrom
	 */
	public void setConsnRefundFrom(String consnRefundFrom) {
		this.consnRefundFrom = consnRefundFrom;
	}
	
    /**
     * @return consnRefundFrom
     */
	public String getConsnRefundFrom() {
		return this.consnRefundFrom;
	}
	
	/**
	 * @param cstChnBic
	 */
	public void setCstChnBic(String cstChnBic) {
		this.cstChnBic = cstChnBic;
	}
	
    /**
     * @return cstChnBic
     */
	public String getCstChnBic() {
		return this.cstChnBic;
	}
	
	/**
	 * @param cstNm
	 */
	public void setCstNm(String cstNm) {
		this.cstNm = cstNm;
	}
	
    /**
     * @return cstNm
     */
	public String getCstNm() {
		return this.cstNm;
	}
	
	/**
	 * @param cstType
	 */
	public void setCstType(String cstType) {
		this.cstType = cstType;
	}
	
    /**
     * @return cstType
     */
	public String getCstType() {
		return this.cstType;
	}
	
	/**
	 * @param cvtIncAmt
	 */
	public void setCvtIncAmt(java.math.BigDecimal cvtIncAmt) {
		this.cvtIncAmt = cvtIncAmt;
	}
	
    /**
     * @return cvtIncAmt
     */
	public java.math.BigDecimal getCvtIncAmt() {
		return this.cvtIncAmt;
	}
	
	/**
	 * @param cvtIncCcy
	 */
	public void setCvtIncCcy(String cvtIncCcy) {
		this.cvtIncCcy = cvtIncCcy;
	}
	
    /**
     * @return cvtIncCcy
     */
	public String getCvtIncCcy() {
		return this.cvtIncCcy;
	}
	
	/**
	 * @param expiryDt
	 */
	public void setExpiryDt(String expiryDt) {
		this.expiryDt = expiryDt;
	}
	
    /**
     * @return expiryDt
     */
	public String getExpiryDt() {
		return this.expiryDt;
	}
	
	/**
	 * @param fftAplNo
	 */
	public void setFftAplNo(String fftAplNo) {
		this.fftAplNo = fftAplNo;
	}
	
    /**
     * @return fftAplNo
     */
	public String getFftAplNo() {
		return this.fftAplNo;
	}
	
	/**
	 * @param fftBillAmt
	 */
	public void setFftBillAmt(java.math.BigDecimal fftBillAmt) {
		this.fftBillAmt = fftBillAmt;
	}
	
    /**
     * @return fftBillAmt
     */
	public java.math.BigDecimal getFftBillAmt() {
		return this.fftBillAmt;
	}
	
	/**
	 * @param fftBillCcy
	 */
	public void setFftBillCcy(String fftBillCcy) {
		this.fftBillCcy = fftBillCcy;
	}
	
    /**
     * @return fftBillCcy
     */
	public String getFftBillCcy() {
		return this.fftBillCcy;
	}
	
	/**
	 * @param fftBillPayTenor
	 */
	public void setFftBillPayTenor(String fftBillPayTenor) {
		this.fftBillPayTenor = fftBillPayTenor;
	}
	
    /**
     * @return fftBillPayTenor
     */
	public String getFftBillPayTenor() {
		return this.fftBillPayTenor;
	}
	
	/**
	 * @param fftBillType
	 */
	public void setFftBillType(String fftBillType) {
		this.fftBillType = fftBillType;
	}
	
    /**
     * @return fftBillType
     */
	public String getFftBillType() {
		return this.fftBillType;
	}
	
	/**
	 * @param fftBuyerBicCde
	 */
	public void setFftBuyerBicCde(String fftBuyerBicCde) {
		this.fftBuyerBicCde = fftBuyerBicCde;
	}
	
    /**
     * @return fftBuyerBicCde
     */
	public String getFftBuyerBicCde() {
		return this.fftBuyerBicCde;
	}
	
	/**
	 * @param fftBuyerBkNm
	 */
	public void setFftBuyerBkNm(String fftBuyerBkNm) {
		this.fftBuyerBkNm = fftBuyerBkNm;
	}
	
    /**
     * @return fftBuyerBkNm
     */
	public String getFftBuyerBkNm() {
		return this.fftBuyerBkNm;
	}
	
	/**
	 * @param fftCashType
	 */
	public void setFftCashType(String fftCashType) {
		this.fftCashType = fftCashType;
	}
	
    /**
     * @return fftCashType
     */
	public String getFftCashType() {
		return this.fftCashType;
	}
	
	/**
	 * @param fftClrType
	 */
	public void setFftClrType(String fftClrType) {
		this.fftClrType = fftClrType;
	}
	
    /**
     * @return fftClrType
     */
	public String getFftClrType() {
		return this.fftClrType;
	}
	
	/**
	 * @param fftDateType
	 */
	public void setFftDateType(String fftDateType) {
		this.fftDateType = fftDateType;
	}
	
    /**
     * @return fftDateType
     */
	public String getFftDateType() {
		return this.fftDateType;
	}
	
	/**
	 * @param fftDealType
	 */
	public void setFftDealType(String fftDealType) {
		this.fftDealType = fftDealType;
	}
	
    /**
     * @return fftDealType
     */
	public String getFftDealType() {
		return this.fftDealType;
	}
	
	/**
	 * @param fftDealType1
	 */
	public void setFftDealType1(String fftDealType1) {
		this.fftDealType1 = fftDealType1;
	}
	
    /**
     * @return fftDealType1
     */
	public String getFftDealType1() {
		return this.fftDealType1;
	}
	
	/**
	 * @param sysRefNo
	 */
	public void setSysRefNo(String sysRefNo) {
		this.sysRefNo = sysRefNo;
	}
	
    /**
     * @return sysRefNo
     */
	public String getSysRefNo() {
		return this.sysRefNo;
	}
	
	/**
	 * @param sysRelNo
	 */
	public void setSysRelNo(String sysRelNo) {
		this.sysRelNo = sysRelNo;
	}
	
    /**
     * @return sysRelNo
     */
	public String getSysRelNo() {
		return this.sysRelNo;
	}
	
	/**
	 * @param fftGraceDays
	 */
	public void setFftGraceDays(Integer fftGraceDays) {
		this.fftGraceDays = fftGraceDays;
	}
	
    /**
     * @return fftGraceDays
     */
	public Integer getFftGraceDays() {
		return this.fftGraceDays;
	}
	
	/**
	 * @param fftIntAmt
	 */
	public void setFftIntAmt(java.math.BigDecimal fftIntAmt) {
		this.fftIntAmt = fftIntAmt;
	}
	
    /**
     * @return fftIntAmt
     */
	public java.math.BigDecimal getFftIntAmt() {
		return this.fftIntAmt;
	}
	
	/**
	 * @param fftIntAmt5
	 */
	public void setFftIntAmt5(java.math.BigDecimal fftIntAmt5) {
		this.fftIntAmt5 = fftIntAmt5;
	}
	
    /**
     * @return fftIntAmt5
     */
	public java.math.BigDecimal getFftIntAmt5() {
		return this.fftIntAmt5;
	}
	
	/**
	 * @param fftIntCcy
	 */
	public void setFftIntCcy(String fftIntCcy) {
		this.fftIntCcy = fftIntCcy;
	}
	
    /**
     * @return fftIntCcy
     */
	public String getFftIntCcy() {
		return this.fftIntCcy;
	}
	
	/**
	 * @param fftIntCcy5
	 */
	public void setFftIntCcy5(String fftIntCcy5) {
		this.fftIntCcy5 = fftIntCcy5;
	}
	
    /**
     * @return fftIntCcy5
     */
	public String getFftIntCcy5() {
		return this.fftIntCcy5;
	}
	
	/**
	 * @param fftInCstFlg
	 */
	public void setFftInCstFlg(String fftInCstFlg) {
		this.fftInCstFlg = fftInCstFlg;
	}
	
    /**
     * @return fftInCstFlg
     */
	public String getFftInCstFlg() {
		return this.fftInCstFlg;
	}
	
	/**
	 * @param fftMinAmt1
	 */
	public void setFftMinAmt1(java.math.BigDecimal fftMinAmt1) {
		this.fftMinAmt1 = fftMinAmt1;
	}
	
    /**
     * @return fftMinAmt1
     */
	public java.math.BigDecimal getFftMinAmt1() {
		return this.fftMinAmt1;
	}
	
	/**
	 * @param fftMinAmt2
	 */
	public void setFftMinAmt2(java.math.BigDecimal fftMinAmt2) {
		this.fftMinAmt2 = fftMinAmt2;
	}
	
    /**
     * @return fftMinAmt2
     */
	public java.math.BigDecimal getFftMinAmt2() {
		return this.fftMinAmt2;
	}
	
	/**
	 * @param fftMinAmt3
	 */
	public void setFftMinAmt3(java.math.BigDecimal fftMinAmt3) {
		this.fftMinAmt3 = fftMinAmt3;
	}
	
    /**
     * @return fftMinAmt3
     */
	public java.math.BigDecimal getFftMinAmt3() {
		return this.fftMinAmt3;
	}
	
	/**
	 * @param fftMinCcy1
	 */
	public void setFftMinCcy1(String fftMinCcy1) {
		this.fftMinCcy1 = fftMinCcy1;
	}
	
    /**
     * @return fftMinCcy1
     */
	public String getFftMinCcy1() {
		return this.fftMinCcy1;
	}
	
	/**
	 * @param fftMinCcy2
	 */
	public void setFftMinCcy2(String fftMinCcy2) {
		this.fftMinCcy2 = fftMinCcy2;
	}
	
    /**
     * @return fftMinCcy2
     */
	public String getFftMinCcy2() {
		return this.fftMinCcy2;
	}
	
	/**
	 * @param fftMinCcy3
	 */
	public void setFftMinCcy3(String fftMinCcy3) {
		this.fftMinCcy3 = fftMinCcy3;
	}
	
    /**
     * @return fftMinCcy3
     */
	public String getFftMinCcy3() {
		return this.fftMinCcy3;
	}
	
	/**
	 * @param fftOrdDeductAmt
	 */
	public void setFftOrdDeductAmt(java.math.BigDecimal fftOrdDeductAmt) {
		this.fftOrdDeductAmt = fftOrdDeductAmt;
	}
	
    /**
     * @return fftOrdDeductAmt
     */
	public java.math.BigDecimal getFftOrdDeductAmt() {
		return this.fftOrdDeductAmt;
	}
	
	/**
	 * @param fftOrdDeductCcy
	 */
	public void setFftOrdDeductCcy(String fftOrdDeductCcy) {
		this.fftOrdDeductCcy = fftOrdDeductCcy;
	}
	
    /**
     * @return fftOrdDeductCcy
     */
	public String getFftOrdDeductCcy() {
		return this.fftOrdDeductCcy;
	}
	
	/**
	 * @param fftOrdDt
	 */
	public void setFftOrdDt(String fftOrdDt) {
		this.fftOrdDt = fftOrdDt;
	}
	
    /**
     * @return fftOrdDt
     */
	public String getFftOrdDt() {
		return this.fftOrdDt;
	}
	
	/**
	 * @param fftOrgiQuotBy
	 */
	public void setFftOrgiQuotBy(String fftOrgiQuotBy) {
		this.fftOrgiQuotBy = fftOrgiQuotBy;
	}
	
    /**
     * @return fftOrgiQuotBy
     */
	public String getFftOrgiQuotBy() {
		return this.fftOrgiQuotBy;
	}
	
	/**
	 * @param fftOrgiQuotBy1
	 */
	public void setFftOrgiQuotBy1(String fftOrgiQuotBy1) {
		this.fftOrgiQuotBy1 = fftOrgiQuotBy1;
	}
	
    /**
     * @return fftOrgiQuotBy1
     */
	public String getFftOrgiQuotBy1() {
		return this.fftOrgiQuotBy1;
	}
	
	/**
	 * @param fftPayBkNm
	 */
	public void setFftPayBkNm(String fftPayBkNm) {
		this.fftPayBkNm = fftPayBkNm;
	}
	
    /**
     * @return fftPayBkNm
     */
	public String getFftPayBkNm() {
		return this.fftPayBkNm;
	}
	
	/**
	 * @param fftPriDfAmt
	 */
	public void setFftPriDfAmt(java.math.BigDecimal fftPriDfAmt) {
		this.fftPriDfAmt = fftPriDfAmt;
	}
	
    /**
     * @return fftPriDfAmt
     */
	public java.math.BigDecimal getFftPriDfAmt() {
		return this.fftPriDfAmt;
	}
	
	/**
	 * @param fftPriDfCcy
	 */
	public void setFftPriDfCcy(String fftPriDfCcy) {
		this.fftPriDfCcy = fftPriDfCcy;
	}
	
    /**
     * @return fftPriDfCcy
     */
	public String getFftPriDfCcy() {
		return this.fftPriDfCcy;
	}
	
	/**
	 * @param fftQuotBps
	 */
	public void setFftQuotBps(Integer fftQuotBps) {
		this.fftQuotBps = fftQuotBps;
	}
	
    /**
     * @return fftQuotBps
     */
	public Integer getFftQuotBps() {
		return this.fftQuotBps;
	}
	
	/**
	 * @param fftQuotBps1
	 */
	public void setFftQuotBps1(Integer fftQuotBps1) {
		this.fftQuotBps1 = fftQuotBps1;
	}
	
    /**
     * @return fftQuotBps1
     */
	public Integer getFftQuotBps1() {
		return this.fftQuotBps1;
	}
	
	/**
	 * @param fftQuotBps3
	 */
	public void setFftQuotBps3(String fftQuotBps3) {
		this.fftQuotBps3 = fftQuotBps3;
	}
	
    /**
     * @return fftQuotBps3
     */
	public String getFftQuotBps3() {
		return this.fftQuotBps3;
	}
	
	/**
	 * @param fftQuotComm
	 */
	public void setFftQuotComm(java.math.BigDecimal fftQuotComm) {
		this.fftQuotComm = fftQuotComm;
	}
	
    /**
     * @return fftQuotComm
     */
	public java.math.BigDecimal getFftQuotComm() {
		return this.fftQuotComm;
	}
	
	/**
	 * @param fftQuotCommRt
	 */
	public void setFftQuotCommRt(java.math.BigDecimal fftQuotCommRt) {
		this.fftQuotCommRt = fftQuotCommRt;
	}
	
    /**
     * @return fftQuotCommRt
     */
	public java.math.BigDecimal getFftQuotCommRt() {
		return this.fftQuotCommRt;
	}
	
	/**
	 * @param fftQuotIntRt
	 */
	public void setFftQuotIntRt(java.math.BigDecimal fftQuotIntRt) {
		this.fftQuotIntRt = fftQuotIntRt;
	}
	
    /**
     * @return fftQuotIntRt
     */
	public java.math.BigDecimal getFftQuotIntRt() {
		return this.fftQuotIntRt;
	}
	
	/**
	 * @param fftQuotIntRt1
	 */
	public void setFftQuotIntRt1(java.math.BigDecimal fftQuotIntRt1) {
		this.fftQuotIntRt1 = fftQuotIntRt1;
	}
	
    /**
     * @return fftQuotIntRt1
     */
	public java.math.BigDecimal getFftQuotIntRt1() {
		return this.fftQuotIntRt1;
	}
	
	/**
	 * @param fftQuotIntRt3
	 */
	public void setFftQuotIntRt3(java.math.BigDecimal fftQuotIntRt3) {
		this.fftQuotIntRt3 = fftQuotIntRt3;
	}
	
    /**
     * @return fftQuotIntRt3
     */
	public java.math.BigDecimal getFftQuotIntRt3() {
		return this.fftQuotIntRt3;
	}
	
	/**
	 * @param fftQuotIntRtType
	 */
	public void setFftQuotIntRtType(String fftQuotIntRtType) {
		this.fftQuotIntRtType = fftQuotIntRtType;
	}
	
    /**
     * @return fftQuotIntRtType
     */
	public String getFftQuotIntRtType() {
		return this.fftQuotIntRtType;
	}
	
	/**
	 * @param fftQuotIntRtType1
	 */
	public void setFftQuotIntRtType1(String fftQuotIntRtType1) {
		this.fftQuotIntRtType1 = fftQuotIntRtType1;
	}
	
    /**
     * @return fftQuotIntRtType1
     */
	public String getFftQuotIntRtType1() {
		return this.fftQuotIntRtType1;
	}
	
	/**
	 * @param fftQuotIntRtType3
	 */
	public void setFftQuotIntRtType3(String fftQuotIntRtType3) {
		this.fftQuotIntRtType3 = fftQuotIntRtType3;
	}
	
    /**
     * @return fftQuotIntRtType3
     */
	public String getFftQuotIntRtType3() {
		return this.fftQuotIntRtType3;
	}
	
	/**
	 * @param fftQuotTenorDays
	 */
	public void setFftQuotTenorDays(String fftQuotTenorDays) {
		this.fftQuotTenorDays = fftQuotTenorDays;
	}
	
    /**
     * @return fftQuotTenorDays
     */
	public String getFftQuotTenorDays() {
		return this.fftQuotTenorDays;
	}
	
	/**
	 * @param fftQuotTenorDays1
	 */
	public void setFftQuotTenorDays1(String fftQuotTenorDays1) {
		this.fftQuotTenorDays1 = fftQuotTenorDays1;
	}
	
    /**
     * @return fftQuotTenorDays1
     */
	public String getFftQuotTenorDays1() {
		return this.fftQuotTenorDays1;
	}
	
	/**
	 * @param fftQuotTenorDays3
	 */
	public void setFftQuotTenorDays3(String fftQuotTenorDays3) {
		this.fftQuotTenorDays3 = fftQuotTenorDays3;
	}
	
    /**
     * @return fftQuotTenorDays3
     */
	public String getFftQuotTenorDays3() {
		return this.fftQuotTenorDays3;
	}
	
	/**
	 * @param fftRecvdAmt
	 */
	public void setFftRecvdAmt(java.math.BigDecimal fftRecvdAmt) {
		this.fftRecvdAmt = fftRecvdAmt;
	}
	
    /**
     * @return fftRecvdAmt
     */
	public java.math.BigDecimal getFftRecvdAmt() {
		return this.fftRecvdAmt;
	}
	
	/**
	 * @param fftRecvdCcy
	 */
	public void setFftRecvdCcy(String fftRecvdCcy) {
		this.fftRecvdCcy = fftRecvdCcy;
	}
	
    /**
     * @return fftRecvdCcy
     */
	public String getFftRecvdCcy() {
		return this.fftRecvdCcy;
	}
	
	/**
	 * @param fftRecvdSrc
	 */
	public void setFftRecvdSrc(String fftRecvdSrc) {
		this.fftRecvdSrc = fftRecvdSrc;
	}
	
    /**
     * @return fftRecvdSrc
     */
	public String getFftRecvdSrc() {
		return this.fftRecvdSrc;
	}
	
	/**
	 * @param fftRelNo
	 */
	public void setFftRelNo(String fftRelNo) {
		this.fftRelNo = fftRelNo;
	}
	
    /**
     * @return fftRelNo
     */
	public String getFftRelNo() {
		return this.fftRelNo;
	}
	
	/**
	 * @param fftValueDt
	 */
	public void setFftValueDt(String fftValueDt) {
		this.fftValueDt = fftValueDt;
	}
	
    /**
     * @return fftValueDt
     */
	public String getFftValueDt() {
		return this.fftValueDt;
	}
	
	/**
	 * @param finPriBal
	 */
	public void setFinPriBal(java.math.BigDecimal finPriBal) {
		this.finPriBal = finPriBal;
	}
	
    /**
     * @return finPriBal
     */
	public java.math.BigDecimal getFinPriBal() {
		return this.finPriBal;
	}
	
	/**
	 * @param hdChannel
	 */
	public void setHdChannel(String hdChannel) {
		this.hdChannel = hdChannel;
	}
	
    /**
     * @return hdChannel
     */
	public String getHdChannel() {
		return this.hdChannel;
	}
	
	/**
	 * @param hdType
	 */
	public void setHdType(String hdType) {
		this.hdType = hdType;
	}
	
    /**
     * @return hdType
     */
	public String getHdType() {
		return this.hdType;
	}
	
	/**
	 * @param iftCaseStep
	 */
	public void setIftCaseStep(Integer iftCaseStep) {
		this.iftCaseStep = iftCaseStep;
	}
	
    /**
     * @return iftCaseStep
     */
	public Integer getIftCaseStep() {
		return this.iftCaseStep;
	}
	
	/**
	 * @param iftRgNo
	 */
	public void setIftRgNo(String iftRgNo) {
		this.iftRgNo = iftRgNo;
	}
	
    /**
     * @return iftRgNo
     */
	public String getIftRgNo() {
		return this.iftRgNo;
	}
	
	/**
	 * @param intAmt
	 */
	public void setIntAmt(java.math.BigDecimal intAmt) {
		this.intAmt = intAmt;
	}
	
    /**
     * @return intAmt
     */
	public java.math.BigDecimal getIntAmt() {
		return this.intAmt;
	}
	
	/**
	 * @param intCcy
	 */
	public void setIntCcy(String intCcy) {
		this.intCcy = intCcy;
	}
	
    /**
     * @return intCcy
     */
	public String getIntCcy() {
		return this.intCcy;
	}
	
	/**
	 * @param intDate
	 */
	public void setIntDate(String intDate) {
		this.intDate = intDate;
	}
	
    /**
     * @return intDate
     */
	public String getIntDate() {
		return this.intDate;
	}
	
	/**
	 * @param inAmt
	 */
	public void setInAmt(java.math.BigDecimal inAmt) {
		this.inAmt = inAmt;
	}
	
    /**
     * @return inAmt
     */
	public java.math.BigDecimal getInAmt() {
		return this.inAmt;
	}
	
	/**
	 * @param inCur
	 */
	public void setInCur(String inCur) {
		this.inCur = inCur;
	}
	
    /**
     * @return inCur
     */
	public String getInCur() {
		return this.inCur;
	}
	
	/**
	 * @param issBkBic
	 */
	public void setIssBkBic(String issBkBic) {
		this.issBkBic = issBkBic;
	}
	
    /**
     * @return issBkBic
     */
	public String getIssBkBic() {
		return this.issBkBic;
	}
	
	/**
	 * @param issBkNm
	 */
	public void setIssBkNm(String issBkNm) {
		this.issBkNm = issBkNm;
	}
	
    /**
     * @return issBkNm
     */
	public String getIssBkNm() {
		return this.issBkNm;
	}
	
	/**
	 * @param issCnfInfo
	 */
	public void setIssCnfInfo(String issCnfInfo) {
		this.issCnfInfo = issCnfInfo;
	}
	
    /**
     * @return issCnfInfo
     */
	public String getIssCnfInfo() {
		return this.issCnfInfo;
	}
	
	/**
	 * @param loanActualAmt
	 */
	public void setLoanActualAmt(java.math.BigDecimal loanActualAmt) {
		this.loanActualAmt = loanActualAmt;
	}
	
    /**
     * @return loanActualAmt
     */
	public java.math.BigDecimal getLoanActualAmt() {
		return this.loanActualAmt;
	}
	
	/**
	 * @param loanActualCcy
	 */
	public void setLoanActualCcy(String loanActualCcy) {
		this.loanActualCcy = loanActualCcy;
	}
	
    /**
     * @return loanActualCcy
     */
	public String getLoanActualCcy() {
		return this.loanActualCcy;
	}
	
	/**
	 * @param loanDt
	 */
	public void setLoanDt(String loanDt) {
		this.loanDt = loanDt;
	}
	
    /**
     * @return loanDt
     */
	public String getLoanDt() {
		return this.loanDt;
	}
	
	/**
	 * @param orgCde
	 */
	public void setOrgCde(String orgCde) {
		this.orgCde = orgCde;
	}
	
    /**
     * @return orgCde
     */
	public String getOrgCde() {
		return this.orgCde;
	}
	
	/**
	 * @param othFundWhere
	 */
	public void setOthFundWhere(String othFundWhere) {
		this.othFundWhere = othFundWhere;
	}
	
    /**
     * @return othFundWhere
     */
	public String getOthFundWhere() {
		return this.othFundWhere;
	}
	
	/**
	 * @param payDays
	 */
	public void setPayDays(String payDays) {
		this.payDays = payDays;
	}
	
    /**
     * @return payDays
     */
	public String getPayDays() {
		return this.payDays;
	}
	
	/**
	 * @param presntDt
	 */
	public void setPresntDt(String presntDt) {
		this.presntDt = presntDt;
	}
	
    /**
     * @return presntDt
     */
	public String getPresntDt() {
		return this.presntDt;
	}
	
	/**
	 * @param remtRoute
	 */
	public void setRemtRoute(String remtRoute) {
		this.remtRoute = remtRoute;
	}
	
    /**
     * @return remtRoute
     */
	public String getRemtRoute() {
		return this.remtRoute;
	}
	
	/**
	 * @param settDt
	 */
	public void setSettDt(String settDt) {
		this.settDt = settDt;
	}
	
    /**
     * @return settDt
     */
	public String getSettDt() {
		return this.settDt;
	}
	
	/**
	 * @param txDt
	 */
	public void setTxDt(String txDt) {
		this.txDt = txDt;
	}
	
    /**
     * @return txDt
     */
	public String getTxDt() {
		return this.txDt;
	}
	
	/**
	 * @param fftDeductCstFlg
	 */
	public void setFftDeductCstFlg(String fftDeductCstFlg) {
		this.fftDeductCstFlg = fftDeductCstFlg;
	}
	
    /**
     * @return fftDeductCstFlg
     */
	public String getFftDeductCstFlg() {
		return this.fftDeductCstFlg;
	}
	
	/**
	 * @param fftFundFrom
	 */
	public void setFftFundFrom(String fftFundFrom) {
		this.fftFundFrom = fftFundFrom;
	}
	
    /**
     * @return fftFundFrom
     */
	public String getFftFundFrom() {
		return this.fftFundFrom;
	}
	
	/**
	 * @param fftOrgiQuotBy3
	 */
	public void setFftOrgiQuotBy3(String fftOrgiQuotBy3) {
		this.fftOrgiQuotBy3 = fftOrgiQuotBy3;
	}
	
    /**
     * @return fftOrgiQuotBy3
     */
	public String getFftOrgiQuotBy3() {
		return this.fftOrgiQuotBy3;
	}
	
	/**
	 * @param cmsRefNo
	 */
	public void setCmsRefNo(String cmsRefNo) {
		this.cmsRefNo = cmsRefNo;
	}
	
    /**
     * @return cmsRefNo
     */
	public String getCmsRefNo() {
		return this.cmsRefNo;
	}
	
	/**
	 * @param fftQuotIntRt4
	 */
	public void setFftQuotIntRt4(java.math.BigDecimal fftQuotIntRt4) {
		this.fftQuotIntRt4 = fftQuotIntRt4;
	}
	
    /**
     * @return fftQuotIntRt4
     */
	public java.math.BigDecimal getFftQuotIntRt4() {
		return this.fftQuotIntRt4;
	}
	
	/**
	 * @param fftMinCcy4
	 */
	public void setFftMinCcy4(String fftMinCcy4) {
		this.fftMinCcy4 = fftMinCcy4;
	}
	
    /**
     * @return fftMinCcy4
     */
	public String getFftMinCcy4() {
		return this.fftMinCcy4;
	}
	
	/**
	 * @param fftMinAmt4
	 */
	public void setFftMinAmt4(java.math.BigDecimal fftMinAmt4) {
		this.fftMinAmt4 = fftMinAmt4;
	}
	
    /**
     * @return fftMinAmt4
     */
	public java.math.BigDecimal getFftMinAmt4() {
		return this.fftMinAmt4;
	}
	
	/**
	 * @param fftOrgiQuotBy4
	 */
	public void setFftOrgiQuotBy4(String fftOrgiQuotBy4) {
		this.fftOrgiQuotBy4 = fftOrgiQuotBy4;
	}
	
    /**
     * @return fftOrgiQuotBy4
     */
	public String getFftOrgiQuotBy4() {
		return this.fftOrgiQuotBy4;
	}
	
	/**
	 * @param fftIntCcy1
	 */
	public void setFftIntCcy1(String fftIntCcy1) {
		this.fftIntCcy1 = fftIntCcy1;
	}
	
    /**
     * @return fftIntCcy1
     */
	public String getFftIntCcy1() {
		return this.fftIntCcy1;
	}
	
	/**
	 * @param fftIntAmt1
	 */
	public void setFftIntAmt1(java.math.BigDecimal fftIntAmt1) {
		this.fftIntAmt1 = fftIntAmt1;
	}
	
    /**
     * @return fftIntAmt1
     */
	public java.math.BigDecimal getFftIntAmt1() {
		return this.fftIntAmt1;
	}
	
	/**
	 * @param fftIntCcy4
	 */
	public void setFftIntCcy4(String fftIntCcy4) {
		this.fftIntCcy4 = fftIntCcy4;
	}
	
    /**
     * @return fftIntCcy4
     */
	public String getFftIntCcy4() {
		return this.fftIntCcy4;
	}
	
	/**
	 * @param fftIntAmt4
	 */
	public void setFftIntAmt4(java.math.BigDecimal fftIntAmt4) {
		this.fftIntAmt4 = fftIntAmt4;
	}
	
    /**
     * @return fftIntAmt4
     */
	public java.math.BigDecimal getFftIntAmt4() {
		return this.fftIntAmt4;
	}
	
	/**
	 * @param fftOrdDeductCcy1
	 */
	public void setFftOrdDeductCcy1(String fftOrdDeductCcy1) {
		this.fftOrdDeductCcy1 = fftOrdDeductCcy1;
	}
	
    /**
     * @return fftOrdDeductCcy1
     */
	public String getFftOrdDeductCcy1() {
		return this.fftOrdDeductCcy1;
	}
	
	/**
	 * @param fftOrdDeductAmt1
	 */
	public void setFftOrdDeductAmt1(java.math.BigDecimal fftOrdDeductAmt1) {
		this.fftOrdDeductAmt1 = fftOrdDeductAmt1;
	}
	
    /**
     * @return fftOrdDeductAmt1
     */
	public java.math.BigDecimal getFftOrdDeductAmt1() {
		return this.fftOrdDeductAmt1;
	}
	
	/**
	 * @param payBuyerCcy
	 */
	public void setPayBuyerCcy(String payBuyerCcy) {
		this.payBuyerCcy = payBuyerCcy;
	}
	
    /**
     * @return payBuyerCcy
     */
	public String getPayBuyerCcy() {
		return this.payBuyerCcy;
	}
	
	/**
	 * @param payBuyerAmt
	 */
	public void setPayBuyerAmt(java.math.BigDecimal payBuyerAmt) {
		this.payBuyerAmt = payBuyerAmt;
	}
	
    /**
     * @return payBuyerAmt
     */
	public java.math.BigDecimal getPayBuyerAmt() {
		return this.payBuyerAmt;
	}
	
	/**
	 * @param issBkNo
	 */
	public void setIssBkNo(String issBkNo) {
		this.issBkNo = issBkNo;
	}
	
    /**
     * @return issBkNo
     */
	public String getIssBkNo() {
		return this.issBkNo;
	}
	
	/**
	 * @param bpNo
	 */
	public void setBpNo(String bpNo) {
		this.bpNo = bpNo;
	}
	
    /**
     * @return bpNo
     */
	public String getBpNo() {
		return this.bpNo;
	}


}