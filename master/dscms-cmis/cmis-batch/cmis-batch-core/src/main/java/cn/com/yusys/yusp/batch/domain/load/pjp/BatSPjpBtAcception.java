/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtAcception
 * @类描述: bat_s_pjp_bt_acception数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-03 10:58:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_pjp_bt_acception")
public class BatSPjpBtAcception extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ACCEPTION_BILL_ID")
	private String acceptionBillId;
	
	/** 关联的票据信息对象主键ID **/
	@Column(name = "BILLINFO_ID", unique = false, nullable = true, length = 40)
	private String billinfoId;
	
	/** 票号 **/
	@Column(name = "S_BILL_NO", unique = false, nullable = true, length = 40)
	private String sBillNo;
	
	/** 票据种类 **/
	@Column(name = "S_BILL_TYPE", unique = false, nullable = true, length = 10)
	private String sBillType;
	
	/** 票据介质 **/
	@Column(name = "S_BILL_MEDIA", unique = false, nullable = true, length = 10)
	private String sBillMedia;
	
	/** 币种 **/
	@Column(name = "MONEY_KIND", unique = false, nullable = true, length = 3)
	private String moneyKind;
	
	/** 金额 **/
	@Column(name = "f_bill_amount", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fBillAmount;
	
	/** 到期无条件支付委托 **/
	@Column(name = "S_UNCON_PAY_COMM", unique = false, nullable = true, length = 10)
	private String sUnconPayComm;
	
	/** 交易合同编号 **/
	@Column(name = "D_TXLCTRCTNB", unique = false, nullable = true, length = 30)
	private String dTxlctrctnb;
	
	/** 发票编号 **/
	@Column(name = "D_INVCNB", unique = false, nullable = true, length = 30)
	private String dInvcnb;
	
	/** 批次号 **/
	@Column(name = "S_BATCHNB", unique = false, nullable = true, length = 10)
	private String sBatchnb;
	
	/** 提示承兑备注 **/
	@Column(name = "CLEW_ACPT_RMAK", unique = false, nullable = true, length = 800)
	private String clewAcptRmak;
	
	/** 承兑申请日期 **/
	@Column(name = "s_accptrdt", unique = false, nullable = true, length = 20)
	private String sAccptrdt;
	
	/** 出票人类别 **/
	@Column(name = "S_DRWRROLE", unique = false, nullable = true, length = 10)
	private String sDrwrrole;
	
	/** 出票人组织机构代码 **/
	@Column(name = "S_APPLYER_ORG_CODE", unique = false, nullable = true, length = 20)
	private String sApplyerOrgCode;
	
	/** 出票人账号 **/
	@Column(name = "S_ISSUER_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sIssuerAccount;
	
	/** 出票人开户行行号 **/
	@Column(name = "ED_DRWRACCTSVCR", unique = false, nullable = true, length = 40)
	private String edDrwracctsvcr;
	
	/** 出票人开户行名称 **/
	@Column(name = "S_ISSUERBANKNAME", unique = false, nullable = true, length = 180)
	private String sIssuerbankname;
	
	/** 出票人承接行行号 **/
	@Column(name = "ED_DRWRAGCYACCTSVCR", unique = false, nullable = true, length = 15)
	private String edDrwragcyacctsvcr;
	
	/** 出票人承接行名称 **/
	@Column(name = "S_DRWRAGCYNAME", unique = false, nullable = true, length = 180)
	private String sDrwragcyname;
	
	/** 出票人全称 **/
	@Column(name = "S_ISSUER_NAME", unique = false, nullable = true, length = 80)
	private String sIssuerName;
	
	/** 代理回复标识 **/
	@Column(name = "S_PRXYSGNTR", unique = false, nullable = true, length = 10)
	private String sPrxysgntr;
	
	/** 承兑回复备注 **/
	@Column(name = "ACPT_SIGN_RMAK", unique = false, nullable = true, length = 800)
	private String acptSignRmak;
	
	/** 承兑回复人类别 **/
	@Column(name = "S_ACPTSIGNROLE", unique = false, nullable = true, length = 10)
	private String sAcptsignrole;
	
	/** 承兑回复人组织机构代码 **/
	@Column(name = "S_ACCEPTORORGCODE", unique = false, nullable = true, length = 15)
	private String sAcceptororgcode;
	
	/** 承兑回复电子签名 **/
	@Column(name = "S_ELCTRNCSGNTR", unique = false, nullable = true, length = 65535)
	private String sElctrncsgntr;
	
	/** 承兑回复人账号 **/
	@Column(name = "ATER_ACCT", unique = false, nullable = true, length = 40)
	private String aterAcct;
	
	/** 承兑回复人开户行行号 **/
	@Column(name = "S_ACCEPTOR_BANK_CODE", unique = false, nullable = true, length = 40)
	private String sAcceptorBankCode;
	
	/** 承兑回复人承接行行号 **/
	@Column(name = "S_ACCPTRACCTSVCRAGCY", unique = false, nullable = true, length = 12)
	private String sAccptracctsvcragcy;
	
	/** 承兑回复人承接行名称 **/
	@Column(name = "S_AGCYSVCRNAME", unique = false, nullable = true, length = 120)
	private String sAgcysvcrname;
	
	/** 承兑人信用等级 **/
	@Column(name = "APTR_CRDT_GRADE", unique = false, nullable = true, length = 3)
	private String aptrCrdtGrade;
	
	/** 承兑人评价机构 **/
	@Column(name = "APTR_CRDT_ORGAN", unique = false, nullable = true, length = 60)
	private String aptrCrdtOrgan;
	
	/** 承兑人评级到期日 **/
	@Column(name = "aptr_crdt_date", unique = false, nullable = true, length = 20)
	private String aptrCrdtDate;
	
	/** 承兑回复日期 **/
	@Column(name = "d_accptrsgnupdt", unique = false, nullable = true, length = 20)
	private String dAccptrsgnupdt;
	
	/** 承兑回复标记 **/
	@Column(name = "S_ACCPTRSGNUPMK", unique = false, nullable = true, length = 4)
	private String sAccptrsgnupmk;
	
	/** 到期无条件支付承诺 **/
	@Column(name = "S_UCONDLPRMSMRK", unique = false, nullable = true, length = 4)
	private String sUcondlprmsmrk;
	
	/** 承兑协议号 **/
	@Column(name = "S_ACCEP_AGREE_NO", unique = false, nullable = true, length = 30)
	private String sAccepAgreeNo;
	
	/** 手续费 **/
	@Column(name = "F_CHARGE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fCharge;
	
	/** 状态标志 STD_RZ_S_STATUS_FLAG **/
	@Column(name = "S_STATUS_FLAG", unique = false, nullable = true, length = 10)
	private String sStatusFlag;
	
	/** 前置状态 **/
	@Column(name = "S_PREFIX_STATE", unique = false, nullable = true, length = 10)
	private String sPrefixState;
	
	/** 报文状态 **/
	@Column(name = "S_MSG_STATUS", unique = false, nullable = true, length = 15)
	private String sMsgStatus;
	
	/** 人行回复033报文信息 **/
	@Column(name = "S_MSG_CODE", unique = false, nullable = true, length = 800)
	private String sMsgCode;
	
	/** 流程实例ID **/
	@Column(name = "WORKFLOW_CASEID", unique = false, nullable = true, length = 10)
	private Integer workflowCaseid;
	
	/** 机构ID **/
	@Column(name = "S_BRANCH_ID", unique = false, nullable = true, length = 40)
	private String sBranchId;
	
	/** 操作员ID **/
	@Column(name = "S_OPERATOR_ID", unique = false, nullable = true, length = 40)
	private String sOperatorId;
	
	/** 对应的002报文标识号 **/
	@Column(name = "S_MSGID", unique = false, nullable = true, length = 40)
	private String sMsgid;
	
	/** 本记录创建时间 **/
	@Column(name = "S_CREATETIME", unique = false, nullable = true, length = 20)
	private String sCreatetime;
	
	/** 保证金类别 **/
	@Column(name = "S_CASHDEPOSITTYPE", unique = false, nullable = true, length = 10)
	private String sCashdeposittype;
	
	/** 保证金金额 **/
	@Column(name = "S_CASHDEPOSITISSEAMT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal sCashdepositisseamt;
	
	/** 保证金账号 **/
	@Column(name = "S_CASHDEPOSITACCTID", unique = false, nullable = true, length = 40)
	private String sCashdepositacctid;
	
	/** 保证金开户行行号 **/
	@Column(name = "S_CASHDEPOSITACCTSVCR", unique = false, nullable = true, length = 12)
	private String sCashdepositacctsvcr;
	
	/** 授信额度 **/
	@Column(name = "S_LINEOFCREDIT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal sLineofcredit;
	
	/** 手续费比例 **/
	@Column(name = "S_PDGRATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal sPdgrate;
	
	/** 保证金比例 **/
	@Column(name = "S_BAILRATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal sBailrate;
	
	/** 信贷合同流水号 **/
	@Column(name = "S_TXTRANSNUMBER", unique = false, nullable = true, length = 100)
	private String sTxtransnumber;
	
	/** 保证金账户名称 **/
	@Column(name = "S_CASHDEPOSITACCTNAME", unique = false, nullable = true, length = 300)
	private String sCashdepositacctname;
	
	/** (打印出账单)打印人 **/
	@Column(name = "S_PRINTER", unique = false, nullable = true, length = 100)
	private String sPrinter;
	
	/** (打印出账单)打印时间 **/
	@Column(name = "S_PRINTDATE", unique = false, nullable = true, length = 10)
	private String sPrintdate;
	
	/** 解付时入账账号 **/
	@Column(name = "S_RECEIVEACCT", unique = false, nullable = true, length = 40)
	private String sReceiveacct;
	
	/** 解付日期 **/
	@Column(name = "s_paydt", unique = false, nullable = true, length = 20)
	private String sPaydt;
	
	/** 解付时入账行号 **/
	@Column(name = "S_RECEIVEACCTBANKCODE", unique = false, nullable = true, length = 40)
	private String sReceiveacctbankcode;
	
	/** 解付时入账行名 **/
	@Column(name = "S_RECEIVEACCTBANKNAME", unique = false, nullable = true, length = 100)
	private String sReceiveacctbankname;
	
	/** 托收人（最后一手持票人） **/
	@Column(name = "S_OWNER", unique = false, nullable = true, length = 100)
	private String sOwner;
	
	/** 时间戳 **/
	@Column(name = "S_TIMEVERSION", unique = false, nullable = true, length = 20)
	private String sTimeversion;
	
	/** 信贷选中标记 **/
	@Column(name = "S_CHECKED", unique = false, nullable = true, length = 2)
	private String sChecked;
	
	/** 记账流水号 **/
	@Column(name = "S_JZLSH", unique = false, nullable = true, length = 50)
	private String sJzlsh;
	
	/** 记账日期 **/
	@Column(name = "s_jzrq", unique = false, nullable = true, length = 20)
	private String sJzrq;
	
	/** 承兑机构号 **/
	@Column(name = "S_CDJG", unique = false, nullable = true, length = 10)
	private String sCdjg;
	
	/** 借据号 **/
	@Column(name = "S_JJH", unique = false, nullable = true, length = 50)
	private String sJjh;
	
	/** 记账机构 **/
	@Column(name = "HX_JZJG", unique = false, nullable = true, length = 10)
	private String hxJzjg;
	
	/** 票据顺序号 **/
	@Column(name = "S_BILLSEQUENCE", unique = false, nullable = true, length = 10)
	private Integer sBillsequence;
	
	/** 手工备款标志 **/
	@Column(name = "SECTFLAG", unique = false, nullable = true, length = 2)
	private String sectflag;
	
	/** 批次表ID **/
	@Column(name = "ACCEPTION_BATCH_ID", unique = false, nullable = true, length = 40)
	private String acceptionBatchId;
	
	/** 担保方式 **/
	@Column(name = "GUARANTEE_TYPE", unique = false, nullable = true, length = 10)
	private String guaranteeType;
	
	/** 还款账号 **/
	@Column(name = "RETURN_ACCT_NO", unique = false, nullable = true, length = 40)
	private String returnAcctNo;
	
	/** 还款账号行号 **/
	@Column(name = "RETURN_ACCT_BANK_NO", unique = false, nullable = true, length = 20)
	private String returnAcctBankNo;
	
	/** 还款账号户名 **/
	@Column(name = "RETURN_ACCT_NAME", unique = false, nullable = true, length = 300)
	private String returnAcctName;
	
	/** 产品类型 **/
	@Column(name = "PRODUCT_ID", unique = false, nullable = true, length = 20)
	private String productId;
	
	/** 承兑协议号 **/
	@Column(name = "PROTOCOL_NO", unique = false, nullable = true, length = 100)
	private String protocolNo;
	
	/** 扣费标记 **/
	@Column(name = "CHARGEFLAG", unique = false, nullable = true, length = 10)
	private String chargeflag;
	
	/** 记账日期 **/
	@Column(name = "acct_date", unique = false, nullable = true, length = 20)
	private String acctDate;
	
	/** 记账流水号 **/
	@Column(name = "ACCT_FLOW_NO", unique = false, nullable = true, length = 60)
	private String acctFlowNo;
	
	/** 记账柜员号 **/
	@Column(name = "ACCT_USER_NO", unique = false, nullable = true, length = 60)
	private String acctUserNo;
	
	/** 记账柜员名 **/
	@Column(name = "ACCT_USER_NAME", unique = false, nullable = true, length = 60)
	private String acctUserName;
	
	/** 记账授权柜员号 **/
	@Column(name = "ACCT_AUTH_USER_NO", unique = false, nullable = true, length = 60)
	private String acctAuthUserNo;
	
	/** 记账授权柜员名 **/
	@Column(name = "ACCT_AUTH_USER_NAME", unique = false, nullable = true, length = 60)
	private String acctAuthUserName;
	
	/** 撤销记账柜员号 **/
	@Column(name = "CANCEL_ACCT_USER_NO", unique = false, nullable = true, length = 60)
	private String cancelAcctUserNo;
	
	/** 撤销记账柜员名 **/
	@Column(name = "CANCEL_ACCT_USER_NAME", unique = false, nullable = true, length = 60)
	private String cancelAcctUserName;
	
	/** 撤销记账授权柜员号 **/
	@Column(name = "CANCEL_ACCT_AUTH_USER_NO", unique = false, nullable = true, length = 60)
	private String cancelAcctAuthUserNo;
	
	/** 撤销记账授权柜员名 **/
	@Column(name = "CANCEL_ACCT_AUTH_USER_NAME", unique = false, nullable = true, length = 60)
	private String cancelAcctAuthUserName;
	
	/** 代理标志 **/
	@Column(name = "AGENT_FLAG", unique = false, nullable = true, length = 2)
	private String agentFlag;
	
	/** 解付日期 **/
	@Column(name = "colldate", unique = false, nullable = true, length = 20)
	private String colldate;
	
	/** 解付状态 **/
	@Column(name = "COLLSTATUS", unique = false, nullable = true, length = 20)
	private String collstatus;
	
	/** 是否打印 STD_ISI_PRINTE **/
	@Column(name = "IS_PRINT", unique = false, nullable = true, length = 10)
	private String isPrint;
	
	/** 是否挂失止付 STD_RZ_STOP_PAYMENT **/
	@Column(name = "STOP_PAYMENT", unique = false, nullable = true, length = 10)
	private String stopPayment;
	
	/** 计息方式 **/
	@Column(name = "INTEREST_MODE", unique = false, nullable = true, length = 10)
	private String interestMode;
	
	/** 敞口标志 STD_RZ_OPEN_MARK **/
	@Column(name = "OPEN_MARK", unique = false, nullable = true, length = 2)
	private String openMark;
	
	/** 撤销记账授权时间 **/
	@Column(name = "authorize_time", unique = false, nullable = true, length = 20)
	private String authorizeTime;
	
	/** 数据移植标记 STD_RZ_YZ_FLAG **/
	@Column(name = "YZ_FLAG", unique = false, nullable = true, length = 2)
	private String yzFlag;
	
	/** 挂失止付日期 **/
	@Column(name = "stop_pyament_Date", unique = false, nullable = true, length = 20)
	private String stopPyamentDate;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param acceptionBillId
	 */
	public void setAcceptionBillId(String acceptionBillId) {
		this.acceptionBillId = acceptionBillId;
	}
	
    /**
     * @return acceptionBillId
     */
	public String getAcceptionBillId() {
		return this.acceptionBillId;
	}
	
	/**
	 * @param billinfoId
	 */
	public void setBillinfoId(String billinfoId) {
		this.billinfoId = billinfoId;
	}
	
    /**
     * @return billinfoId
     */
	public String getBillinfoId() {
		return this.billinfoId;
	}
	
	/**
	 * @param sBillNo
	 */
	public void setSBillNo(String sBillNo) {
		this.sBillNo = sBillNo;
	}
	
    /**
     * @return sBillNo
     */
	public String getSBillNo() {
		return this.sBillNo;
	}
	
	/**
	 * @param sBillType
	 */
	public void setSBillType(String sBillType) {
		this.sBillType = sBillType;
	}
	
    /**
     * @return sBillType
     */
	public String getSBillType() {
		return this.sBillType;
	}
	
	/**
	 * @param sBillMedia
	 */
	public void setSBillMedia(String sBillMedia) {
		this.sBillMedia = sBillMedia;
	}
	
    /**
     * @return sBillMedia
     */
	public String getSBillMedia() {
		return this.sBillMedia;
	}
	
	/**
	 * @param moneyKind
	 */
	public void setMoneyKind(String moneyKind) {
		this.moneyKind = moneyKind;
	}
	
    /**
     * @return moneyKind
     */
	public String getMoneyKind() {
		return this.moneyKind;
	}
	
	/**
	 * @param fBillAmount
	 */
	public void setFBillAmount(java.math.BigDecimal fBillAmount) {
		this.fBillAmount = fBillAmount;
	}
	
    /**
     * @return fBillAmount
     */
	public java.math.BigDecimal getFBillAmount() {
		return this.fBillAmount;
	}
	
	/**
	 * @param sUnconPayComm
	 */
	public void setSUnconPayComm(String sUnconPayComm) {
		this.sUnconPayComm = sUnconPayComm;
	}
	
    /**
     * @return sUnconPayComm
     */
	public String getSUnconPayComm() {
		return this.sUnconPayComm;
	}
	
	/**
	 * @param dTxlctrctnb
	 */
	public void setDTxlctrctnb(String dTxlctrctnb) {
		this.dTxlctrctnb = dTxlctrctnb;
	}
	
    /**
     * @return dTxlctrctnb
     */
	public String getDTxlctrctnb() {
		return this.dTxlctrctnb;
	}
	
	/**
	 * @param dInvcnb
	 */
	public void setDInvcnb(String dInvcnb) {
		this.dInvcnb = dInvcnb;
	}
	
    /**
     * @return dInvcnb
     */
	public String getDInvcnb() {
		return this.dInvcnb;
	}
	
	/**
	 * @param sBatchnb
	 */
	public void setSBatchnb(String sBatchnb) {
		this.sBatchnb = sBatchnb;
	}
	
    /**
     * @return sBatchnb
     */
	public String getSBatchnb() {
		return this.sBatchnb;
	}
	
	/**
	 * @param clewAcptRmak
	 */
	public void setClewAcptRmak(String clewAcptRmak) {
		this.clewAcptRmak = clewAcptRmak;
	}
	
    /**
     * @return clewAcptRmak
     */
	public String getClewAcptRmak() {
		return this.clewAcptRmak;
	}
	
	/**
	 * @param sAccptrdt
	 */
	public void setSAccptrdt(String sAccptrdt) {
		this.sAccptrdt = sAccptrdt;
	}
	
    /**
     * @return sAccptrdt
     */
	public String getSAccptrdt() {
		return this.sAccptrdt;
	}
	
	/**
	 * @param sDrwrrole
	 */
	public void setSDrwrrole(String sDrwrrole) {
		this.sDrwrrole = sDrwrrole;
	}
	
    /**
     * @return sDrwrrole
     */
	public String getSDrwrrole() {
		return this.sDrwrrole;
	}
	
	/**
	 * @param sApplyerOrgCode
	 */
	public void setSApplyerOrgCode(String sApplyerOrgCode) {
		this.sApplyerOrgCode = sApplyerOrgCode;
	}
	
    /**
     * @return sApplyerOrgCode
     */
	public String getSApplyerOrgCode() {
		return this.sApplyerOrgCode;
	}
	
	/**
	 * @param sIssuerAccount
	 */
	public void setSIssuerAccount(String sIssuerAccount) {
		this.sIssuerAccount = sIssuerAccount;
	}
	
    /**
     * @return sIssuerAccount
     */
	public String getSIssuerAccount() {
		return this.sIssuerAccount;
	}
	
	/**
	 * @param edDrwracctsvcr
	 */
	public void setEdDrwracctsvcr(String edDrwracctsvcr) {
		this.edDrwracctsvcr = edDrwracctsvcr;
	}
	
    /**
     * @return edDrwracctsvcr
     */
	public String getEdDrwracctsvcr() {
		return this.edDrwracctsvcr;
	}
	
	/**
	 * @param sIssuerbankname
	 */
	public void setSIssuerbankname(String sIssuerbankname) {
		this.sIssuerbankname = sIssuerbankname;
	}
	
    /**
     * @return sIssuerbankname
     */
	public String getSIssuerbankname() {
		return this.sIssuerbankname;
	}
	
	/**
	 * @param edDrwragcyacctsvcr
	 */
	public void setEdDrwragcyacctsvcr(String edDrwragcyacctsvcr) {
		this.edDrwragcyacctsvcr = edDrwragcyacctsvcr;
	}
	
    /**
     * @return edDrwragcyacctsvcr
     */
	public String getEdDrwragcyacctsvcr() {
		return this.edDrwragcyacctsvcr;
	}
	
	/**
	 * @param sDrwragcyname
	 */
	public void setSDrwragcyname(String sDrwragcyname) {
		this.sDrwragcyname = sDrwragcyname;
	}
	
    /**
     * @return sDrwragcyname
     */
	public String getSDrwragcyname() {
		return this.sDrwragcyname;
	}
	
	/**
	 * @param sIssuerName
	 */
	public void setSIssuerName(String sIssuerName) {
		this.sIssuerName = sIssuerName;
	}
	
    /**
     * @return sIssuerName
     */
	public String getSIssuerName() {
		return this.sIssuerName;
	}
	
	/**
	 * @param sPrxysgntr
	 */
	public void setSPrxysgntr(String sPrxysgntr) {
		this.sPrxysgntr = sPrxysgntr;
	}
	
    /**
     * @return sPrxysgntr
     */
	public String getSPrxysgntr() {
		return this.sPrxysgntr;
	}
	
	/**
	 * @param acptSignRmak
	 */
	public void setAcptSignRmak(String acptSignRmak) {
		this.acptSignRmak = acptSignRmak;
	}
	
    /**
     * @return acptSignRmak
     */
	public String getAcptSignRmak() {
		return this.acptSignRmak;
	}
	
	/**
	 * @param sAcptsignrole
	 */
	public void setSAcptsignrole(String sAcptsignrole) {
		this.sAcptsignrole = sAcptsignrole;
	}
	
    /**
     * @return sAcptsignrole
     */
	public String getSAcptsignrole() {
		return this.sAcptsignrole;
	}
	
	/**
	 * @param sAcceptororgcode
	 */
	public void setSAcceptororgcode(String sAcceptororgcode) {
		this.sAcceptororgcode = sAcceptororgcode;
	}
	
    /**
     * @return sAcceptororgcode
     */
	public String getSAcceptororgcode() {
		return this.sAcceptororgcode;
	}
	
	/**
	 * @param sElctrncsgntr
	 */
	public void setSElctrncsgntr(String sElctrncsgntr) {
		this.sElctrncsgntr = sElctrncsgntr;
	}
	
    /**
     * @return sElctrncsgntr
     */
	public String getSElctrncsgntr() {
		return this.sElctrncsgntr;
	}
	
	/**
	 * @param aterAcct
	 */
	public void setAterAcct(String aterAcct) {
		this.aterAcct = aterAcct;
	}
	
    /**
     * @return aterAcct
     */
	public String getAterAcct() {
		return this.aterAcct;
	}
	
	/**
	 * @param sAcceptorBankCode
	 */
	public void setSAcceptorBankCode(String sAcceptorBankCode) {
		this.sAcceptorBankCode = sAcceptorBankCode;
	}
	
    /**
     * @return sAcceptorBankCode
     */
	public String getSAcceptorBankCode() {
		return this.sAcceptorBankCode;
	}
	
	/**
	 * @param sAccptracctsvcragcy
	 */
	public void setSAccptracctsvcragcy(String sAccptracctsvcragcy) {
		this.sAccptracctsvcragcy = sAccptracctsvcragcy;
	}
	
    /**
     * @return sAccptracctsvcragcy
     */
	public String getSAccptracctsvcragcy() {
		return this.sAccptracctsvcragcy;
	}
	
	/**
	 * @param sAgcysvcrname
	 */
	public void setSAgcysvcrname(String sAgcysvcrname) {
		this.sAgcysvcrname = sAgcysvcrname;
	}
	
    /**
     * @return sAgcysvcrname
     */
	public String getSAgcysvcrname() {
		return this.sAgcysvcrname;
	}
	
	/**
	 * @param aptrCrdtGrade
	 */
	public void setAptrCrdtGrade(String aptrCrdtGrade) {
		this.aptrCrdtGrade = aptrCrdtGrade;
	}
	
    /**
     * @return aptrCrdtGrade
     */
	public String getAptrCrdtGrade() {
		return this.aptrCrdtGrade;
	}
	
	/**
	 * @param aptrCrdtOrgan
	 */
	public void setAptrCrdtOrgan(String aptrCrdtOrgan) {
		this.aptrCrdtOrgan = aptrCrdtOrgan;
	}
	
    /**
     * @return aptrCrdtOrgan
     */
	public String getAptrCrdtOrgan() {
		return this.aptrCrdtOrgan;
	}
	
	/**
	 * @param aptrCrdtDate
	 */
	public void setAptrCrdtDate(String aptrCrdtDate) {
		this.aptrCrdtDate = aptrCrdtDate;
	}
	
    /**
     * @return aptrCrdtDate
     */
	public String getAptrCrdtDate() {
		return this.aptrCrdtDate;
	}
	
	/**
	 * @param dAccptrsgnupdt
	 */
	public void setDAccptrsgnupdt(String dAccptrsgnupdt) {
		this.dAccptrsgnupdt = dAccptrsgnupdt;
	}
	
    /**
     * @return dAccptrsgnupdt
     */
	public String getDAccptrsgnupdt() {
		return this.dAccptrsgnupdt;
	}
	
	/**
	 * @param sAccptrsgnupmk
	 */
	public void setSAccptrsgnupmk(String sAccptrsgnupmk) {
		this.sAccptrsgnupmk = sAccptrsgnupmk;
	}
	
    /**
     * @return sAccptrsgnupmk
     */
	public String getSAccptrsgnupmk() {
		return this.sAccptrsgnupmk;
	}
	
	/**
	 * @param sUcondlprmsmrk
	 */
	public void setSUcondlprmsmrk(String sUcondlprmsmrk) {
		this.sUcondlprmsmrk = sUcondlprmsmrk;
	}
	
    /**
     * @return sUcondlprmsmrk
     */
	public String getSUcondlprmsmrk() {
		return this.sUcondlprmsmrk;
	}
	
	/**
	 * @param sAccepAgreeNo
	 */
	public void setSAccepAgreeNo(String sAccepAgreeNo) {
		this.sAccepAgreeNo = sAccepAgreeNo;
	}
	
    /**
     * @return sAccepAgreeNo
     */
	public String getSAccepAgreeNo() {
		return this.sAccepAgreeNo;
	}
	
	/**
	 * @param fCharge
	 */
	public void setFCharge(java.math.BigDecimal fCharge) {
		this.fCharge = fCharge;
	}
	
    /**
     * @return fCharge
     */
	public java.math.BigDecimal getFCharge() {
		return this.fCharge;
	}
	
	/**
	 * @param sStatusFlag
	 */
	public void setSStatusFlag(String sStatusFlag) {
		this.sStatusFlag = sStatusFlag;
	}
	
    /**
     * @return sStatusFlag
     */
	public String getSStatusFlag() {
		return this.sStatusFlag;
	}
	
	/**
	 * @param sPrefixState
	 */
	public void setSPrefixState(String sPrefixState) {
		this.sPrefixState = sPrefixState;
	}
	
    /**
     * @return sPrefixState
     */
	public String getSPrefixState() {
		return this.sPrefixState;
	}
	
	/**
	 * @param sMsgStatus
	 */
	public void setSMsgStatus(String sMsgStatus) {
		this.sMsgStatus = sMsgStatus;
	}
	
    /**
     * @return sMsgStatus
     */
	public String getSMsgStatus() {
		return this.sMsgStatus;
	}
	
	/**
	 * @param sMsgCode
	 */
	public void setSMsgCode(String sMsgCode) {
		this.sMsgCode = sMsgCode;
	}
	
    /**
     * @return sMsgCode
     */
	public String getSMsgCode() {
		return this.sMsgCode;
	}
	
	/**
	 * @param workflowCaseid
	 */
	public void setWorkflowCaseid(Integer workflowCaseid) {
		this.workflowCaseid = workflowCaseid;
	}
	
    /**
     * @return workflowCaseid
     */
	public Integer getWorkflowCaseid() {
		return this.workflowCaseid;
	}
	
	/**
	 * @param sBranchId
	 */
	public void setSBranchId(String sBranchId) {
		this.sBranchId = sBranchId;
	}
	
    /**
     * @return sBranchId
     */
	public String getSBranchId() {
		return this.sBranchId;
	}
	
	/**
	 * @param sOperatorId
	 */
	public void setSOperatorId(String sOperatorId) {
		this.sOperatorId = sOperatorId;
	}
	
    /**
     * @return sOperatorId
     */
	public String getSOperatorId() {
		return this.sOperatorId;
	}
	
	/**
	 * @param sMsgid
	 */
	public void setSMsgid(String sMsgid) {
		this.sMsgid = sMsgid;
	}
	
    /**
     * @return sMsgid
     */
	public String getSMsgid() {
		return this.sMsgid;
	}
	
	/**
	 * @param sCreatetime
	 */
	public void setSCreatetime(String sCreatetime) {
		this.sCreatetime = sCreatetime;
	}
	
    /**
     * @return sCreatetime
     */
	public String getSCreatetime() {
		return this.sCreatetime;
	}
	
	/**
	 * @param sCashdeposittype
	 */
	public void setSCashdeposittype(String sCashdeposittype) {
		this.sCashdeposittype = sCashdeposittype;
	}
	
    /**
     * @return sCashdeposittype
     */
	public String getSCashdeposittype() {
		return this.sCashdeposittype;
	}
	
	/**
	 * @param sCashdepositisseamt
	 */
	public void setSCashdepositisseamt(java.math.BigDecimal sCashdepositisseamt) {
		this.sCashdepositisseamt = sCashdepositisseamt;
	}
	
    /**
     * @return sCashdepositisseamt
     */
	public java.math.BigDecimal getSCashdepositisseamt() {
		return this.sCashdepositisseamt;
	}
	
	/**
	 * @param sCashdepositacctid
	 */
	public void setSCashdepositacctid(String sCashdepositacctid) {
		this.sCashdepositacctid = sCashdepositacctid;
	}
	
    /**
     * @return sCashdepositacctid
     */
	public String getSCashdepositacctid() {
		return this.sCashdepositacctid;
	}
	
	/**
	 * @param sCashdepositacctsvcr
	 */
	public void setSCashdepositacctsvcr(String sCashdepositacctsvcr) {
		this.sCashdepositacctsvcr = sCashdepositacctsvcr;
	}
	
    /**
     * @return sCashdepositacctsvcr
     */
	public String getSCashdepositacctsvcr() {
		return this.sCashdepositacctsvcr;
	}
	
	/**
	 * @param sLineofcredit
	 */
	public void setSLineofcredit(java.math.BigDecimal sLineofcredit) {
		this.sLineofcredit = sLineofcredit;
	}
	
    /**
     * @return sLineofcredit
     */
	public java.math.BigDecimal getSLineofcredit() {
		return this.sLineofcredit;
	}
	
	/**
	 * @param sPdgrate
	 */
	public void setSPdgrate(java.math.BigDecimal sPdgrate) {
		this.sPdgrate = sPdgrate;
	}
	
    /**
     * @return sPdgrate
     */
	public java.math.BigDecimal getSPdgrate() {
		return this.sPdgrate;
	}
	
	/**
	 * @param sBailrate
	 */
	public void setSBailrate(java.math.BigDecimal sBailrate) {
		this.sBailrate = sBailrate;
	}
	
    /**
     * @return sBailrate
     */
	public java.math.BigDecimal getSBailrate() {
		return this.sBailrate;
	}
	
	/**
	 * @param sTxtransnumber
	 */
	public void setSTxtransnumber(String sTxtransnumber) {
		this.sTxtransnumber = sTxtransnumber;
	}
	
    /**
     * @return sTxtransnumber
     */
	public String getSTxtransnumber() {
		return this.sTxtransnumber;
	}
	
	/**
	 * @param sCashdepositacctname
	 */
	public void setSCashdepositacctname(String sCashdepositacctname) {
		this.sCashdepositacctname = sCashdepositacctname;
	}
	
    /**
     * @return sCashdepositacctname
     */
	public String getSCashdepositacctname() {
		return this.sCashdepositacctname;
	}
	
	/**
	 * @param sPrinter
	 */
	public void setSPrinter(String sPrinter) {
		this.sPrinter = sPrinter;
	}
	
    /**
     * @return sPrinter
     */
	public String getSPrinter() {
		return this.sPrinter;
	}
	
	/**
	 * @param sPrintdate
	 */
	public void setSPrintdate(String sPrintdate) {
		this.sPrintdate = sPrintdate;
	}
	
    /**
     * @return sPrintdate
     */
	public String getSPrintdate() {
		return this.sPrintdate;
	}
	
	/**
	 * @param sReceiveacct
	 */
	public void setSReceiveacct(String sReceiveacct) {
		this.sReceiveacct = sReceiveacct;
	}
	
    /**
     * @return sReceiveacct
     */
	public String getSReceiveacct() {
		return this.sReceiveacct;
	}
	
	/**
	 * @param sPaydt
	 */
	public void setSPaydt(String sPaydt) {
		this.sPaydt = sPaydt;
	}
	
    /**
     * @return sPaydt
     */
	public String getSPaydt() {
		return this.sPaydt;
	}
	
	/**
	 * @param sReceiveacctbankcode
	 */
	public void setSReceiveacctbankcode(String sReceiveacctbankcode) {
		this.sReceiveacctbankcode = sReceiveacctbankcode;
	}
	
    /**
     * @return sReceiveacctbankcode
     */
	public String getSReceiveacctbankcode() {
		return this.sReceiveacctbankcode;
	}
	
	/**
	 * @param sReceiveacctbankname
	 */
	public void setSReceiveacctbankname(String sReceiveacctbankname) {
		this.sReceiveacctbankname = sReceiveacctbankname;
	}
	
    /**
     * @return sReceiveacctbankname
     */
	public String getSReceiveacctbankname() {
		return this.sReceiveacctbankname;
	}
	
	/**
	 * @param sOwner
	 */
	public void setSOwner(String sOwner) {
		this.sOwner = sOwner;
	}
	
    /**
     * @return sOwner
     */
	public String getSOwner() {
		return this.sOwner;
	}
	
	/**
	 * @param sTimeversion
	 */
	public void setSTimeversion(String sTimeversion) {
		this.sTimeversion = sTimeversion;
	}
	
    /**
     * @return sTimeversion
     */
	public String getSTimeversion() {
		return this.sTimeversion;
	}
	
	/**
	 * @param sChecked
	 */
	public void setSChecked(String sChecked) {
		this.sChecked = sChecked;
	}
	
    /**
     * @return sChecked
     */
	public String getSChecked() {
		return this.sChecked;
	}
	
	/**
	 * @param sJzlsh
	 */
	public void setSJzlsh(String sJzlsh) {
		this.sJzlsh = sJzlsh;
	}
	
    /**
     * @return sJzlsh
     */
	public String getSJzlsh() {
		return this.sJzlsh;
	}
	
	/**
	 * @param sJzrq
	 */
	public void setSJzrq(String sJzrq) {
		this.sJzrq = sJzrq;
	}
	
    /**
     * @return sJzrq
     */
	public String getSJzrq() {
		return this.sJzrq;
	}
	
	/**
	 * @param sCdjg
	 */
	public void setSCdjg(String sCdjg) {
		this.sCdjg = sCdjg;
	}
	
    /**
     * @return sCdjg
     */
	public String getSCdjg() {
		return this.sCdjg;
	}
	
	/**
	 * @param sJjh
	 */
	public void setSJjh(String sJjh) {
		this.sJjh = sJjh;
	}
	
    /**
     * @return sJjh
     */
	public String getSJjh() {
		return this.sJjh;
	}
	
	/**
	 * @param hxJzjg
	 */
	public void setHxJzjg(String hxJzjg) {
		this.hxJzjg = hxJzjg;
	}
	
    /**
     * @return hxJzjg
     */
	public String getHxJzjg() {
		return this.hxJzjg;
	}
	
	/**
	 * @param sBillsequence
	 */
	public void setSBillsequence(Integer sBillsequence) {
		this.sBillsequence = sBillsequence;
	}
	
    /**
     * @return sBillsequence
     */
	public Integer getSBillsequence() {
		return this.sBillsequence;
	}
	
	/**
	 * @param sectflag
	 */
	public void setSectflag(String sectflag) {
		this.sectflag = sectflag;
	}
	
    /**
     * @return sectflag
     */
	public String getSectflag() {
		return this.sectflag;
	}
	
	/**
	 * @param acceptionBatchId
	 */
	public void setAcceptionBatchId(String acceptionBatchId) {
		this.acceptionBatchId = acceptionBatchId;
	}
	
    /**
     * @return acceptionBatchId
     */
	public String getAcceptionBatchId() {
		return this.acceptionBatchId;
	}
	
	/**
	 * @param guaranteeType
	 */
	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}
	
    /**
     * @return guaranteeType
     */
	public String getGuaranteeType() {
		return this.guaranteeType;
	}
	
	/**
	 * @param returnAcctNo
	 */
	public void setReturnAcctNo(String returnAcctNo) {
		this.returnAcctNo = returnAcctNo;
	}
	
    /**
     * @return returnAcctNo
     */
	public String getReturnAcctNo() {
		return this.returnAcctNo;
	}
	
	/**
	 * @param returnAcctBankNo
	 */
	public void setReturnAcctBankNo(String returnAcctBankNo) {
		this.returnAcctBankNo = returnAcctBankNo;
	}
	
    /**
     * @return returnAcctBankNo
     */
	public String getReturnAcctBankNo() {
		return this.returnAcctBankNo;
	}
	
	/**
	 * @param returnAcctName
	 */
	public void setReturnAcctName(String returnAcctName) {
		this.returnAcctName = returnAcctName;
	}
	
    /**
     * @return returnAcctName
     */
	public String getReturnAcctName() {
		return this.returnAcctName;
	}
	
	/**
	 * @param productId
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
    /**
     * @return productId
     */
	public String getProductId() {
		return this.productId;
	}
	
	/**
	 * @param protocolNo
	 */
	public void setProtocolNo(String protocolNo) {
		this.protocolNo = protocolNo;
	}
	
    /**
     * @return protocolNo
     */
	public String getProtocolNo() {
		return this.protocolNo;
	}
	
	/**
	 * @param chargeflag
	 */
	public void setChargeflag(String chargeflag) {
		this.chargeflag = chargeflag;
	}
	
    /**
     * @return chargeflag
     */
	public String getChargeflag() {
		return this.chargeflag;
	}
	
	/**
	 * @param acctDate
	 */
	public void setAcctDate(String acctDate) {
		this.acctDate = acctDate;
	}
	
    /**
     * @return acctDate
     */
	public String getAcctDate() {
		return this.acctDate;
	}
	
	/**
	 * @param acctFlowNo
	 */
	public void setAcctFlowNo(String acctFlowNo) {
		this.acctFlowNo = acctFlowNo;
	}
	
    /**
     * @return acctFlowNo
     */
	public String getAcctFlowNo() {
		return this.acctFlowNo;
	}
	
	/**
	 * @param acctUserNo
	 */
	public void setAcctUserNo(String acctUserNo) {
		this.acctUserNo = acctUserNo;
	}
	
    /**
     * @return acctUserNo
     */
	public String getAcctUserNo() {
		return this.acctUserNo;
	}
	
	/**
	 * @param acctUserName
	 */
	public void setAcctUserName(String acctUserName) {
		this.acctUserName = acctUserName;
	}
	
    /**
     * @return acctUserName
     */
	public String getAcctUserName() {
		return this.acctUserName;
	}
	
	/**
	 * @param acctAuthUserNo
	 */
	public void setAcctAuthUserNo(String acctAuthUserNo) {
		this.acctAuthUserNo = acctAuthUserNo;
	}
	
    /**
     * @return acctAuthUserNo
     */
	public String getAcctAuthUserNo() {
		return this.acctAuthUserNo;
	}
	
	/**
	 * @param acctAuthUserName
	 */
	public void setAcctAuthUserName(String acctAuthUserName) {
		this.acctAuthUserName = acctAuthUserName;
	}
	
    /**
     * @return acctAuthUserName
     */
	public String getAcctAuthUserName() {
		return this.acctAuthUserName;
	}
	
	/**
	 * @param cancelAcctUserNo
	 */
	public void setCancelAcctUserNo(String cancelAcctUserNo) {
		this.cancelAcctUserNo = cancelAcctUserNo;
	}
	
    /**
     * @return cancelAcctUserNo
     */
	public String getCancelAcctUserNo() {
		return this.cancelAcctUserNo;
	}
	
	/**
	 * @param cancelAcctUserName
	 */
	public void setCancelAcctUserName(String cancelAcctUserName) {
		this.cancelAcctUserName = cancelAcctUserName;
	}
	
    /**
     * @return cancelAcctUserName
     */
	public String getCancelAcctUserName() {
		return this.cancelAcctUserName;
	}
	
	/**
	 * @param cancelAcctAuthUserNo
	 */
	public void setCancelAcctAuthUserNo(String cancelAcctAuthUserNo) {
		this.cancelAcctAuthUserNo = cancelAcctAuthUserNo;
	}
	
    /**
     * @return cancelAcctAuthUserNo
     */
	public String getCancelAcctAuthUserNo() {
		return this.cancelAcctAuthUserNo;
	}
	
	/**
	 * @param cancelAcctAuthUserName
	 */
	public void setCancelAcctAuthUserName(String cancelAcctAuthUserName) {
		this.cancelAcctAuthUserName = cancelAcctAuthUserName;
	}
	
    /**
     * @return cancelAcctAuthUserName
     */
	public String getCancelAcctAuthUserName() {
		return this.cancelAcctAuthUserName;
	}
	
	/**
	 * @param agentFlag
	 */
	public void setAgentFlag(String agentFlag) {
		this.agentFlag = agentFlag;
	}
	
    /**
     * @return agentFlag
     */
	public String getAgentFlag() {
		return this.agentFlag;
	}
	
	/**
	 * @param colldate
	 */
	public void setColldate(String colldate) {
		this.colldate = colldate;
	}
	
    /**
     * @return colldate
     */
	public String getColldate() {
		return this.colldate;
	}
	
	/**
	 * @param collstatus
	 */
	public void setCollstatus(String collstatus) {
		this.collstatus = collstatus;
	}
	
    /**
     * @return collstatus
     */
	public String getCollstatus() {
		return this.collstatus;
	}
	
	/**
	 * @param isPrint
	 */
	public void setIsPrint(String isPrint) {
		this.isPrint = isPrint;
	}
	
    /**
     * @return isPrint
     */
	public String getIsPrint() {
		return this.isPrint;
	}
	
	/**
	 * @param stopPayment
	 */
	public void setStopPayment(String stopPayment) {
		this.stopPayment = stopPayment;
	}
	
    /**
     * @return stopPayment
     */
	public String getStopPayment() {
		return this.stopPayment;
	}
	
	/**
	 * @param interestMode
	 */
	public void setInterestMode(String interestMode) {
		this.interestMode = interestMode;
	}
	
    /**
     * @return interestMode
     */
	public String getInterestMode() {
		return this.interestMode;
	}
	
	/**
	 * @param openMark
	 */
	public void setOpenMark(String openMark) {
		this.openMark = openMark;
	}
	
    /**
     * @return openMark
     */
	public String getOpenMark() {
		return this.openMark;
	}
	
	/**
	 * @param authorizeTime
	 */
	public void setAuthorizeTime(String authorizeTime) {
		this.authorizeTime = authorizeTime;
	}
	
    /**
     * @return authorizeTime
     */
	public String getAuthorizeTime() {
		return this.authorizeTime;
	}
	
	/**
	 * @param yzFlag
	 */
	public void setYzFlag(String yzFlag) {
		this.yzFlag = yzFlag;
	}
	
    /**
     * @return yzFlag
     */
	public String getYzFlag() {
		return this.yzFlag;
	}
	
	/**
	 * @param stopPyamentDate
	 */
	public void setStopPyamentDate(String stopPyamentDate) {
		this.stopPyamentDate = stopPyamentDate;
	}
	
    /**
     * @return stopPyamentDate
     */
	public String getStopPyamentDate() {
		return this.stopPyamentDate;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}