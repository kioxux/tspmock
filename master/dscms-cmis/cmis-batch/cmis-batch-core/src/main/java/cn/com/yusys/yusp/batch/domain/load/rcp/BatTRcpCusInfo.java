/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpCusInfo
 * @类描述: bat_t_rcp_cus_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:04:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_rcp_cus_info")
public class BatTRcpCusInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 客户号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "cust_id")
	private String custId;
	
	/** 核心客户编号 **/
	@Column(name = "cust_id_core", unique = false, nullable = true, length = 16)
	private String custIdCore;
	
	/** 客户名称 **/
	@Column(name = "cust_name", unique = false, nullable = true, length = 60)
	private String custName;
	
	/** 客户类型 **/
	@Column(name = "cust_type", unique = false, nullable = true, length = 10)
	private String custType;
	
	/** 发证机关 **/
	@Column(name = "cert_org", unique = false, nullable = true, length = 200)
	private String certOrg;
	
	/** 证件类型 10 身份证 **/
	@Column(name = "cert_type", unique = false, nullable = true, length = 20)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "cert_code", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 性别 **/
	@Column(name = "sex", unique = false, nullable = true, length = 10)
	private String sex;
	
	/** 出生日期 **/
	@Column(name = "birthday", unique = false, nullable = true, length = 10)
	private String birthday;
	
	/** 证件有效期 **/
	@Column(name = "id_due_date", unique = false, nullable = true, length = 10)
	private String idDueDate;
	
	/** 教育水平 **/
	@Column(name = "ifeduc", unique = false, nullable = true, length = 20)
	private String ifeduc;
	
	/** 婚姻状态 **/
	@Column(name = "ifwedd", unique = false, nullable = true, length = 20)
	private String ifwedd;
	
	/** 现居住地 **/
	@Column(name = "siaddr", unique = false, nullable = true, length = 300)
	private String siaddr;
	
	/** 固定电话 **/
	@Column(name = "sitelp", unique = false, nullable = true, length = 20)
	private String sitelp;
	
	/** 移动电话 **/
	@Column(name = "ifmobl", unique = false, nullable = true, length = 20)
	private String ifmobl;
	
	/** 居住状态 **/
	@Column(name = "live_status", unique = false, nullable = true, length = 20)
	private String liveStatus;
	
	/** 居住地址邮政编码 **/
	@Column(name = "sizpcd", unique = false, nullable = true, length = 60)
	private String sizpcd;
	
	/** 联系地址 **/
	@Column(name = "addr", unique = false, nullable = true, length = 300)
	private String addr;
	
	/** 联系地址邮政编码 **/
	@Column(name = "zpcd", unique = false, nullable = true, length = 60)
	private String zpcd;
	
	/** 电子邮箱 **/
	@Column(name = "ifmail", unique = false, nullable = true, length = 60)
	private String ifmail;
	
	/** 其他联系人姓名 **/
	@Column(name = "related_person_name", unique = false, nullable = true, length = 60)
	private String relatedPersonName;
	
	/** 其他联系人身份证号 **/
	@Column(name = "related_person_id_no", unique = false, nullable = true, length = 20)
	private String relatedPersonIdNo;
	
	/** 其他联系人与申请人的关系 **/
	@Column(name = "related_person_rel", unique = false, nullable = true, length = 10)
	private String relatedPersonRel;
	
	/** 其他联系人联系电话 **/
	@Column(name = "related_person_telp", unique = false, nullable = true, length = 20)
	private String relatedPersonTelp;
	
	/** 配偶姓名 **/
	@Column(name = "spouse_name", unique = false, nullable = true, length = 60)
	private String spouseName;
	
	/** 配偶证件号码 **/
	@Column(name = "spouse_code", unique = false, nullable = true, length = 20)
	private String spouseCode;
	
	/** 配偶证件类型 **/
	@Column(name = "spouse_type", unique = false, nullable = true, length = 20)
	private String spouseType;
	
	/** 是否有我行个人账户 **/
	@Column(name = "spdb_pacc_flag", unique = false, nullable = true, length = 10)
	private String spdbPaccFlag;
	
	/** 是否已重置密码 **/
	@Column(name = "reset_flag", unique = false, nullable = true, length = 10)
	private String resetFlag;
	
	/** 所属机构 **/
	@Column(name = "biz_org_id", unique = false, nullable = true, length = 10)
	private String bizOrgId;
	
	/** 录入时间 **/
	@Column(name = "input_time", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最近更新时间 **/
	@Column(name = "last_update_time", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	/** 配偶手机号码 **/
	@Column(name = "spouse_phone", unique = false, nullable = true, length = 20)
	private String spousePhone;
	
	/** 职务 **/
	@Column(name = "indiv_com_job_ttl", unique = false, nullable = true, length = 100)
	private String indivComJobTtl;
	
	/** 职业 **/
	@Column(name = "indiv_occ", unique = false, nullable = true, length = 60)
	private String indivOcc;
	
	/** 是否农户 **/
	@Column(name = "agri_flg", unique = false, nullable = true, length = 40)
	private String agriFlg;
	
	/** 工作单位 **/
	@Column(name = "indiv_com_name", unique = false, nullable = true, length = 100)
	private String indivComName;
	
	/** 单位所属行业 **/
	@Column(name = "indiv_com_fld", unique = false, nullable = true, length = 100)
	private String indivComFld;
	
	/** 行业类别 **/
	@Column(name = "std_zx_employmet", unique = false, nullable = true, length = 20)
	private String stdZxEmploymet;
	
	/** 个人年收入 **/
	@Column(name = "indiv_ann_incm", unique = false, nullable = true, length = 15)
	private String indivAnnIncm;
	
	/** 单位所属行业名称 **/
	@Column(name = "indiv_com_fld_name", unique = false, nullable = true, length = 200)
	private String indivComFldName;
	
	/** 单位地址-市民贷 **/
	@Column(name = "indiv_com_addr", unique = false, nullable = true, length = 200)
	private String indivComAddr;
	
	/** 单位电话-市民贷 **/
	@Column(name = "indiv_com_phn", unique = false, nullable = true, length = 20)
	private String indivComPhn;
	
	/** 单位性质-市民贷 **/
	@Column(name = "indiv_com_typ", unique = false, nullable = true, length = 20)
	private String indivComTyp;
	
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custIdCore
	 */
	public void setCustIdCore(String custIdCore) {
		this.custIdCore = custIdCore;
	}
	
    /**
     * @return custIdCore
     */
	public String getCustIdCore() {
		return this.custIdCore;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param custType
	 */
	public void setCustType(String custType) {
		this.custType = custType;
	}
	
    /**
     * @return custType
     */
	public String getCustType() {
		return this.custType;
	}
	
	/**
	 * @param certOrg
	 */
	public void setCertOrg(String certOrg) {
		this.certOrg = certOrg;
	}
	
    /**
     * @return certOrg
     */
	public String getCertOrg() {
		return this.certOrg;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
    /**
     * @return sex
     */
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param birthday
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
    /**
     * @return birthday
     */
	public String getBirthday() {
		return this.birthday;
	}
	
	/**
	 * @param idDueDate
	 */
	public void setIdDueDate(String idDueDate) {
		this.idDueDate = idDueDate;
	}
	
    /**
     * @return idDueDate
     */
	public String getIdDueDate() {
		return this.idDueDate;
	}
	
	/**
	 * @param ifeduc
	 */
	public void setIfeduc(String ifeduc) {
		this.ifeduc = ifeduc;
	}
	
    /**
     * @return ifeduc
     */
	public String getIfeduc() {
		return this.ifeduc;
	}
	
	/**
	 * @param ifwedd
	 */
	public void setIfwedd(String ifwedd) {
		this.ifwedd = ifwedd;
	}
	
    /**
     * @return ifwedd
     */
	public String getIfwedd() {
		return this.ifwedd;
	}
	
	/**
	 * @param siaddr
	 */
	public void setSiaddr(String siaddr) {
		this.siaddr = siaddr;
	}
	
    /**
     * @return siaddr
     */
	public String getSiaddr() {
		return this.siaddr;
	}
	
	/**
	 * @param sitelp
	 */
	public void setSitelp(String sitelp) {
		this.sitelp = sitelp;
	}
	
    /**
     * @return sitelp
     */
	public String getSitelp() {
		return this.sitelp;
	}
	
	/**
	 * @param ifmobl
	 */
	public void setIfmobl(String ifmobl) {
		this.ifmobl = ifmobl;
	}
	
    /**
     * @return ifmobl
     */
	public String getIfmobl() {
		return this.ifmobl;
	}
	
	/**
	 * @param liveStatus
	 */
	public void setLiveStatus(String liveStatus) {
		this.liveStatus = liveStatus;
	}
	
    /**
     * @return liveStatus
     */
	public String getLiveStatus() {
		return this.liveStatus;
	}
	
	/**
	 * @param sizpcd
	 */
	public void setSizpcd(String sizpcd) {
		this.sizpcd = sizpcd;
	}
	
    /**
     * @return sizpcd
     */
	public String getSizpcd() {
		return this.sizpcd;
	}
	
	/**
	 * @param addr
	 */
	public void setAddr(String addr) {
		this.addr = addr;
	}
	
    /**
     * @return addr
     */
	public String getAddr() {
		return this.addr;
	}
	
	/**
	 * @param zpcd
	 */
	public void setZpcd(String zpcd) {
		this.zpcd = zpcd;
	}
	
    /**
     * @return zpcd
     */
	public String getZpcd() {
		return this.zpcd;
	}
	
	/**
	 * @param ifmail
	 */
	public void setIfmail(String ifmail) {
		this.ifmail = ifmail;
	}
	
    /**
     * @return ifmail
     */
	public String getIfmail() {
		return this.ifmail;
	}
	
	/**
	 * @param relatedPersonName
	 */
	public void setRelatedPersonName(String relatedPersonName) {
		this.relatedPersonName = relatedPersonName;
	}
	
    /**
     * @return relatedPersonName
     */
	public String getRelatedPersonName() {
		return this.relatedPersonName;
	}
	
	/**
	 * @param relatedPersonIdNo
	 */
	public void setRelatedPersonIdNo(String relatedPersonIdNo) {
		this.relatedPersonIdNo = relatedPersonIdNo;
	}
	
    /**
     * @return relatedPersonIdNo
     */
	public String getRelatedPersonIdNo() {
		return this.relatedPersonIdNo;
	}
	
	/**
	 * @param relatedPersonRel
	 */
	public void setRelatedPersonRel(String relatedPersonRel) {
		this.relatedPersonRel = relatedPersonRel;
	}
	
    /**
     * @return relatedPersonRel
     */
	public String getRelatedPersonRel() {
		return this.relatedPersonRel;
	}
	
	/**
	 * @param relatedPersonTelp
	 */
	public void setRelatedPersonTelp(String relatedPersonTelp) {
		this.relatedPersonTelp = relatedPersonTelp;
	}
	
    /**
     * @return relatedPersonTelp
     */
	public String getRelatedPersonTelp() {
		return this.relatedPersonTelp;
	}
	
	/**
	 * @param spouseName
	 */
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}
	
    /**
     * @return spouseName
     */
	public String getSpouseName() {
		return this.spouseName;
	}
	
	/**
	 * @param spouseCode
	 */
	public void setSpouseCode(String spouseCode) {
		this.spouseCode = spouseCode;
	}
	
    /**
     * @return spouseCode
     */
	public String getSpouseCode() {
		return this.spouseCode;
	}
	
	/**
	 * @param spouseType
	 */
	public void setSpouseType(String spouseType) {
		this.spouseType = spouseType;
	}
	
    /**
     * @return spouseType
     */
	public String getSpouseType() {
		return this.spouseType;
	}
	
	/**
	 * @param spdbPaccFlag
	 */
	public void setSpdbPaccFlag(String spdbPaccFlag) {
		this.spdbPaccFlag = spdbPaccFlag;
	}
	
    /**
     * @return spdbPaccFlag
     */
	public String getSpdbPaccFlag() {
		return this.spdbPaccFlag;
	}
	
	/**
	 * @param resetFlag
	 */
	public void setResetFlag(String resetFlag) {
		this.resetFlag = resetFlag;
	}
	
    /**
     * @return resetFlag
     */
	public String getResetFlag() {
		return this.resetFlag;
	}
	
	/**
	 * @param bizOrgId
	 */
	public void setBizOrgId(String bizOrgId) {
		this.bizOrgId = bizOrgId;
	}
	
    /**
     * @return bizOrgId
     */
	public String getBizOrgId() {
		return this.bizOrgId;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}
	
	/**
	 * @param spousePhone
	 */
	public void setSpousePhone(String spousePhone) {
		this.spousePhone = spousePhone;
	}
	
    /**
     * @return spousePhone
     */
	public String getSpousePhone() {
		return this.spousePhone;
	}
	
	/**
	 * @param indivComJobTtl
	 */
	public void setIndivComJobTtl(String indivComJobTtl) {
		this.indivComJobTtl = indivComJobTtl;
	}
	
    /**
     * @return indivComJobTtl
     */
	public String getIndivComJobTtl() {
		return this.indivComJobTtl;
	}
	
	/**
	 * @param indivOcc
	 */
	public void setIndivOcc(String indivOcc) {
		this.indivOcc = indivOcc;
	}
	
    /**
     * @return indivOcc
     */
	public String getIndivOcc() {
		return this.indivOcc;
	}
	
	/**
	 * @param agriFlg
	 */
	public void setAgriFlg(String agriFlg) {
		this.agriFlg = agriFlg;
	}
	
    /**
     * @return agriFlg
     */
	public String getAgriFlg() {
		return this.agriFlg;
	}
	
	/**
	 * @param indivComName
	 */
	public void setIndivComName(String indivComName) {
		this.indivComName = indivComName;
	}
	
    /**
     * @return indivComName
     */
	public String getIndivComName() {
		return this.indivComName;
	}
	
	/**
	 * @param indivComFld
	 */
	public void setIndivComFld(String indivComFld) {
		this.indivComFld = indivComFld;
	}
	
    /**
     * @return indivComFld
     */
	public String getIndivComFld() {
		return this.indivComFld;
	}
	
	/**
	 * @param stdZxEmploymet
	 */
	public void setStdZxEmploymet(String stdZxEmploymet) {
		this.stdZxEmploymet = stdZxEmploymet;
	}
	
    /**
     * @return stdZxEmploymet
     */
	public String getStdZxEmploymet() {
		return this.stdZxEmploymet;
	}

	public String getIndivAnnIncm() {
		return indivAnnIncm;
	}

	public void setIndivAnnIncm(String indivAnnIncm) {
		this.indivAnnIncm = indivAnnIncm;
	}

	/**
	 * @param indivComFldName
	 */
	public void setIndivComFldName(String indivComFldName) {
		this.indivComFldName = indivComFldName;
	}
	
    /**
     * @return indivComFldName
     */
	public String getIndivComFldName() {
		return this.indivComFldName;
	}
	
	/**
	 * @param indivComAddr
	 */
	public void setIndivComAddr(String indivComAddr) {
		this.indivComAddr = indivComAddr;
	}
	
    /**
     * @return indivComAddr
     */
	public String getIndivComAddr() {
		return this.indivComAddr;
	}
	
	/**
	 * @param indivComPhn
	 */
	public void setIndivComPhn(String indivComPhn) {
		this.indivComPhn = indivComPhn;
	}
	
    /**
     * @return indivComPhn
     */
	public String getIndivComPhn() {
		return this.indivComPhn;
	}
	
	/**
	 * @param indivComTyp
	 */
	public void setIndivComTyp(String indivComTyp) {
		this.indivComTyp = indivComTyp;
	}
	
    /**
     * @return indivComTyp
     */
	public String getIndivComTyp() {
		return this.indivComTyp;
	}


}