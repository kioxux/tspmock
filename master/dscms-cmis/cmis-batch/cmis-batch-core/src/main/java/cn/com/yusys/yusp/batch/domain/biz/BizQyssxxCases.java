/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.biz;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BizQyssxxCases
 * @类描述: biz_qyssxx_cases数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "biz_qyssxx_cases")
public class BizQyssxxCases extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "APP_NO")
	private String appNo;
	
	/** 案件大类 ：civil 民事案件 ； criminal 刑事案件；administrative 行政案件；implement 执行案件；bankrupt 强制清算与破产案件 **/
	@Id
	@Column(name = "CASE_TYPE")
	private String caseType;
	
	/** 案件序号 **/
	@Id
	@Column(name = "SERIAL_NUMBER")
	private String serialNumber;
	
	/** 查询类型   0：查询自然人， 1：查询组织机构 **/
	@Column(name = "QUERY_TYPE", unique = false, nullable = true, length = 4)
	private String queryType;
	
	/** 身份证号/组织机构代码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 30)
	private String certCode;
	
	/** 姓名/企业名称 **/
	@Column(name = "CUST_NAME", unique = false, nullable = true, length = 40)
	private String custName;
	
	/** 案件类型 **/
	@Column(name = "N_AJLX", unique = false, nullable = true, length = 40)
	private String nAjlx;
	
	/** 案号 **/
	@Column(name = "C_AH", unique = false, nullable = true, length = 80)
	private String cAh;
	
	/** 原审案号 **/
	@Column(name = "C_AH_YS", unique = false, nullable = true, length = 80)
	private String cAhYs;
	
	/** 后续案号 **/
	@Column(name = "C_AH_HX", unique = false, nullable = true, length = 80)
	private String cAhHx;
	
	/** 案件标识 **/
	@Column(name = "N_AJBS", unique = false, nullable = true, length = 80)
	private String nAjbs;
	
	/** 经办法院 **/
	@Column(name = "N_JBFY", unique = false, nullable = true, length = 40)
	private String nJbfy;
	
	/** 法院所属层级 **/
	@Column(name = "N_JBFY_CJ", unique = false, nullable = true, length = 40)
	private String nJbfyCj;
	
	/** 案件进展阶段 **/
	@Column(name = "N_AJJZJD", unique = false, nullable = true, length = 40)
	private String nAjjzjd;
	
	/** 审理程序 **/
	@Column(name = "N_SLCX", unique = false, nullable = true, length = 20)
	private String nSlcx;
	
	/** 所属地域 **/
	@Column(name = "C_SSDY", unique = false, nullable = true, length = 80)
	private String cSsdy;
	
	/** 立案时间 **/
	@Column(name = "D_LARQ", unique = false, nullable = true, length = 40)
	private String dLarq;
	
	/** 立案案由 **/
	@Column(name = "N_LAAY", unique = false, nullable = true, length = 200)
	private String nLaay;
	
	/** 立案标的金额 **/
	@Column(name = "N_QSBDJE", unique = false, nullable = true, length = 40)
	private String nQsbdje;
	
	/** 立案标的金额估计 **/
	@Column(name = "N_QSBDJE_GJ", unique = false, nullable = true, length = 40)
	private String nQsbdjeGj;
	
	/** 结案时间 **/
	@Column(name = "D_JARQ", unique = false, nullable = true, length = 40)
	private String dJarq;
	
	/** 结案案由 **/
	@Column(name = "N_JAAY", unique = false, nullable = true, length = 200)
	private String nJaay;
	
	/** 结案标的金额 **/
	@Column(name = "N_JABDJE", unique = false, nullable = true, length = 40)
	private String nJabdje;
	
	/** 结案标的金额估计 **/
	@Column(name = "N_JABDJE_GJ", unique = false, nullable = true, length = 40)
	private String nJabdjeGj;
	
	/** 结案方式 **/
	@Column(name = "N_JAFS", unique = false, nullable = true, length = 80)
	private String nJafs;
	
	/** 诉讼地位 **/
	@Column(name = "N_SSDW", unique = false, nullable = true, length = 80)
	private String nSsdw;
	
	/** 一审诉讼地位 **/
	@Column(name = "N_SSDW_YS", unique = false, nullable = true, length = 80)
	private String nSsdwYs;
	
	/** 相关案件号 **/
	@Column(name = "C_GKWS_GLAH", unique = false, nullable = true, length = 800)
	private String cGkwsGlah;
	
	/** 当事人 **/
	@Column(name = "C_GKWS_DSR", unique = false, nullable = true, length = 4000)
	private String cGkwsDsr;
	
	/** 判决结果 **/
	@Column(name = "C_GKWS_PJJG", unique = false, nullable = true, length = 4000)
	private String cGkwsPjjg;
	
	/** 犯罪金额 **/
	@Column(name = "N_FZJE", unique = false, nullable = true, length = 40)
	private String nFzje;
	
	/** 被请求赔偿金额 **/
	@Column(name = "N_BQQPCJE", unique = false, nullable = true, length = 40)
	private String nBqqpcje;
	
	/** 财产刑执行金额 **/
	@Column(name = "N_CCXZXJE", unique = false, nullable = true, length = 40)
	private String nCcxzxje;
	
	/** 财产刑执行金额估计 **/
	@Column(name = "N_CCXZXJE_GJ", unique = false, nullable = true, length = 40)
	private String nCcxzxjeGj;
	
	/** 判处赔偿金额 **/
	@Column(name = "N_PCPCJE", unique = false, nullable = true, length = 40)
	private String nPcpcje;
	
	/** 判处赔偿金额估计 **/
	@Column(name = "N_PCPCJE_GJ", unique = false, nullable = true, length = 40)
	private String nPcpcjeGj;
	
	/** 录入日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "LAST_UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	
	/**
	 * @param appNo
	 */
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	
    /**
     * @return appNo
     */
	public String getAppNo() {
		return this.appNo;
	}
	
	/**
	 * @param queryType
	 */
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	
    /**
     * @return queryType
     */
	public String getQueryType() {
		return this.queryType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param caseType
	 */
	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}
	
    /**
     * @return caseType
     */
	public String getCaseType() {
		return this.caseType;
	}
	
	/**
	 * @param serialNumber
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
    /**
     * @return serialNumber
     */
	public String getSerialNumber() {
		return this.serialNumber;
	}
	
	/**
	 * @param nAjlx
	 */
	public void setNAjlx(String nAjlx) {
		this.nAjlx = nAjlx;
	}
	
    /**
     * @return nAjlx
     */
	public String getNAjlx() {
		return this.nAjlx;
	}
	
	/**
	 * @param cAh
	 */
	public void setCAh(String cAh) {
		this.cAh = cAh;
	}
	
    /**
     * @return cAh
     */
	public String getCAh() {
		return this.cAh;
	}
	
	/**
	 * @param cAhYs
	 */
	public void setCAhYs(String cAhYs) {
		this.cAhYs = cAhYs;
	}
	
    /**
     * @return cAhYs
     */
	public String getCAhYs() {
		return this.cAhYs;
	}
	
	/**
	 * @param cAhHx
	 */
	public void setCAhHx(String cAhHx) {
		this.cAhHx = cAhHx;
	}
	
    /**
     * @return cAhHx
     */
	public String getCAhHx() {
		return this.cAhHx;
	}
	
	/**
	 * @param nAjbs
	 */
	public void setNAjbs(String nAjbs) {
		this.nAjbs = nAjbs;
	}
	
    /**
     * @return nAjbs
     */
	public String getNAjbs() {
		return this.nAjbs;
	}
	
	/**
	 * @param nJbfy
	 */
	public void setNJbfy(String nJbfy) {
		this.nJbfy = nJbfy;
	}
	
    /**
     * @return nJbfy
     */
	public String getNJbfy() {
		return this.nJbfy;
	}
	
	/**
	 * @param nJbfyCj
	 */
	public void setNJbfyCj(String nJbfyCj) {
		this.nJbfyCj = nJbfyCj;
	}
	
    /**
     * @return nJbfyCj
     */
	public String getNJbfyCj() {
		return this.nJbfyCj;
	}
	
	/**
	 * @param nAjjzjd
	 */
	public void setNAjjzjd(String nAjjzjd) {
		this.nAjjzjd = nAjjzjd;
	}
	
    /**
     * @return nAjjzjd
     */
	public String getNAjjzjd() {
		return this.nAjjzjd;
	}
	
	/**
	 * @param nSlcx
	 */
	public void setNSlcx(String nSlcx) {
		this.nSlcx = nSlcx;
	}
	
    /**
     * @return nSlcx
     */
	public String getNSlcx() {
		return this.nSlcx;
	}
	
	/**
	 * @param cSsdy
	 */
	public void setCSsdy(String cSsdy) {
		this.cSsdy = cSsdy;
	}
	
    /**
     * @return cSsdy
     */
	public String getCSsdy() {
		return this.cSsdy;
	}
	
	/**
	 * @param dLarq
	 */
	public void setDLarq(String dLarq) {
		this.dLarq = dLarq;
	}
	
    /**
     * @return dLarq
     */
	public String getDLarq() {
		return this.dLarq;
	}
	
	/**
	 * @param nLaay
	 */
	public void setNLaay(String nLaay) {
		this.nLaay = nLaay;
	}
	
    /**
     * @return nLaay
     */
	public String getNLaay() {
		return this.nLaay;
	}
	
	/**
	 * @param nQsbdje
	 */
	public void setNQsbdje(String nQsbdje) {
		this.nQsbdje = nQsbdje;
	}
	
    /**
     * @return nQsbdje
     */
	public String getNQsbdje() {
		return this.nQsbdje;
	}
	
	/**
	 * @param nQsbdjeGj
	 */
	public void setNQsbdjeGj(String nQsbdjeGj) {
		this.nQsbdjeGj = nQsbdjeGj;
	}
	
    /**
     * @return nQsbdjeGj
     */
	public String getNQsbdjeGj() {
		return this.nQsbdjeGj;
	}
	
	/**
	 * @param dJarq
	 */
	public void setDJarq(String dJarq) {
		this.dJarq = dJarq;
	}
	
    /**
     * @return dJarq
     */
	public String getDJarq() {
		return this.dJarq;
	}
	
	/**
	 * @param nJaay
	 */
	public void setNJaay(String nJaay) {
		this.nJaay = nJaay;
	}
	
    /**
     * @return nJaay
     */
	public String getNJaay() {
		return this.nJaay;
	}
	
	/**
	 * @param nJabdje
	 */
	public void setNJabdje(String nJabdje) {
		this.nJabdje = nJabdje;
	}
	
    /**
     * @return nJabdje
     */
	public String getNJabdje() {
		return this.nJabdje;
	}
	
	/**
	 * @param nJabdjeGj
	 */
	public void setNJabdjeGj(String nJabdjeGj) {
		this.nJabdjeGj = nJabdjeGj;
	}
	
    /**
     * @return nJabdjeGj
     */
	public String getNJabdjeGj() {
		return this.nJabdjeGj;
	}
	
	/**
	 * @param nJafs
	 */
	public void setNJafs(String nJafs) {
		this.nJafs = nJafs;
	}
	
    /**
     * @return nJafs
     */
	public String getNJafs() {
		return this.nJafs;
	}
	
	/**
	 * @param nSsdw
	 */
	public void setNSsdw(String nSsdw) {
		this.nSsdw = nSsdw;
	}
	
    /**
     * @return nSsdw
     */
	public String getNSsdw() {
		return this.nSsdw;
	}
	
	/**
	 * @param nSsdwYs
	 */
	public void setNSsdwYs(String nSsdwYs) {
		this.nSsdwYs = nSsdwYs;
	}
	
    /**
     * @return nSsdwYs
     */
	public String getNSsdwYs() {
		return this.nSsdwYs;
	}
	
	/**
	 * @param cGkwsGlah
	 */
	public void setCGkwsGlah(String cGkwsGlah) {
		this.cGkwsGlah = cGkwsGlah;
	}
	
    /**
     * @return cGkwsGlah
     */
	public String getCGkwsGlah() {
		return this.cGkwsGlah;
	}
	
	/**
	 * @param cGkwsDsr
	 */
	public void setCGkwsDsr(String cGkwsDsr) {
		this.cGkwsDsr = cGkwsDsr;
	}
	
    /**
     * @return cGkwsDsr
     */
	public String getCGkwsDsr() {
		return this.cGkwsDsr;
	}
	
	/**
	 * @param cGkwsPjjg
	 */
	public void setCGkwsPjjg(String cGkwsPjjg) {
		this.cGkwsPjjg = cGkwsPjjg;
	}
	
    /**
     * @return cGkwsPjjg
     */
	public String getCGkwsPjjg() {
		return this.cGkwsPjjg;
	}
	
	/**
	 * @param nFzje
	 */
	public void setNFzje(String nFzje) {
		this.nFzje = nFzje;
	}
	
    /**
     * @return nFzje
     */
	public String getNFzje() {
		return this.nFzje;
	}
	
	/**
	 * @param nBqqpcje
	 */
	public void setNBqqpcje(String nBqqpcje) {
		this.nBqqpcje = nBqqpcje;
	}
	
    /**
     * @return nBqqpcje
     */
	public String getNBqqpcje() {
		return this.nBqqpcje;
	}
	
	/**
	 * @param nCcxzxje
	 */
	public void setNCcxzxje(String nCcxzxje) {
		this.nCcxzxje = nCcxzxje;
	}
	
    /**
     * @return nCcxzxje
     */
	public String getNCcxzxje() {
		return this.nCcxzxje;
	}
	
	/**
	 * @param nCcxzxjeGj
	 */
	public void setNCcxzxjeGj(String nCcxzxjeGj) {
		this.nCcxzxjeGj = nCcxzxjeGj;
	}
	
    /**
     * @return nCcxzxjeGj
     */
	public String getNCcxzxjeGj() {
		return this.nCcxzxjeGj;
	}
	
	/**
	 * @param nPcpcje
	 */
	public void setNPcpcje(String nPcpcje) {
		this.nPcpcje = nPcpcje;
	}
	
    /**
     * @return nPcpcje
     */
	public String getNPcpcje() {
		return this.nPcpcje;
	}
	
	/**
	 * @param nPcpcjeGj
	 */
	public void setNPcpcjeGj(String nPcpcjeGj) {
		this.nPcpcjeGj = nPcpcjeGj;
	}
	
    /**
     * @return nPcpcjeGj
     */
	public String getNPcpcjeGj() {
		return this.nPcpcjeGj;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}


}