/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.transform.cfg;

import cn.com.yusys.yusp.batch.domain.transform.cfg.CfgPspMonFreParam;
import cn.com.yusys.yusp.batch.service.transform.cfg.CfgPspMonFreParamService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: CfgPspMonFreParamResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-22 20:25:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgpspmonfreparam")
public class CfgPspMonFreParamResource {
    @Autowired
    private CfgPspMonFreParamService cfgPspMonFreParamService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgPspMonFreParam>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgPspMonFreParam> list = cfgPspMonFreParamService.selectAll(queryModel);
        return new ResultDto<List<CfgPspMonFreParam>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgPspMonFreParam>> index(QueryModel queryModel) {
        List<CfgPspMonFreParam> list = cfgPspMonFreParamService.selectByModel(queryModel);
        return new ResultDto<List<CfgPspMonFreParam>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgPspMonFreParam> show(@PathVariable("pkId") String pkId) {
        CfgPspMonFreParam cfgPspMonFreParam = cfgPspMonFreParamService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgPspMonFreParam>(cfgPspMonFreParam);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgPspMonFreParam> create(@RequestBody CfgPspMonFreParam cfgPspMonFreParam) throws URISyntaxException {
        cfgPspMonFreParamService.insert(cfgPspMonFreParam);
        return new ResultDto<CfgPspMonFreParam>(cfgPspMonFreParam);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgPspMonFreParam cfgPspMonFreParam) throws URISyntaxException {
        int result = cfgPspMonFreParamService.update(cfgPspMonFreParam);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgPspMonFreParamService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgPspMonFreParamService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
