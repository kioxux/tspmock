/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpMsLoanDetailTemp
 * @类描述: bat_t_rcp_ms_loan_detail_temp数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:04:51
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_rcp_ms_loan_detail_temp")
public class BatTRcpMsLoanDetailTemp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务日期
     **/
    @Id
    @Column(name = "biz_date", unique = false, nullable = false, length = 10)
    private String bizDate;
    /**
     * 客户编号
     **/
    @Id
    @Column(name = "uuid")
    private String uuid;
    /**
     * 合同号
     **/
    @Column(name = "contr_nbr", unique = false, nullable = false, length = 30)
    private String contrNbr;
    /**
     * 贷款编号
     **/
    @Id
    @Column(name = "ref_nbr")
    private String refNbr;

    /**
     * 币种
     **/
    @Column(name = "currency", unique = false, nullable = false, length = 3)
    private String currency;

    /**
     * 贷款状态
     **/
    @Column(name = "txn_status", unique = false, nullable = false, length = 2)
    private String txnStatus;

    /**
     * 放款日期
     **/
    @Column(name = "txn_date", unique = false, nullable = false, length = 8)
    private String txnDate;

    /**
     * 放款金额
     **/
    @Column(name = "txn_amt", unique = false, nullable = false, length = 17)
    private java.math.BigDecimal txnAmt;

    /**
     * 放款总期数
     **/
    @Column(name = "init_term", unique = false, nullable = false, length = 10)
    private Integer initTerm;

    /**
     * 当前期数
     **/
    @Column(name = "curr_term", unique = false, nullable = false, length = 10)
    private Integer currTerm;

    /**
     * 逾期天数
     **/
    @Column(name = "overdue_days", unique = false, nullable = false, length = 10)
    private Integer overdueDays;

    /**
     * 逾期起始日
     **/
    @Column(name = "overdue_date", unique = false, nullable = true, length = 10)
    private String overdueDate;

    /**
     * 贷款本金余额
     **/
    @Column(name = "principal_bal", unique = false, nullable = false, length = 17)
    private java.math.BigDecimal principalBal;

    /**
     * 贷款利息余额
     **/
    @Column(name = "interest_bal", unique = false, nullable = false, length = 17)
    private java.math.BigDecimal interestBal;

    /**
     * 逾期贷款本金余额
     **/
    @Column(name = "overdue_prin", unique = false, nullable = false, length = 17)
    private java.math.BigDecimal overduePrin;

    /**
     * 逾期利息
     **/
    @Column(name = "overdue_int", unique = false, nullable = false, length = 17)
    private java.math.BigDecimal overdueInt;

    /**
     * 罚息余额
     **/
    @Column(name = "penalty_bal", unique = false, nullable = false, length = 17)
    private java.math.BigDecimal penaltyBal;

    /**
     * 利率类型
     **/
    @Column(name = "int_type", unique = false, nullable = false, length = 6)
    private String intType;

    /**
     * 计息标志
     **/
    @Column(name = "int_flag", unique = false, nullable = false, length = 1)
    private String intFlag;

    /**
     * 贷款入帐账号
     **/
    @Column(name = "card_no", unique = false, nullable = false, length = 64)
    private String cardNo;

    /**
     * 贷款担保方式
     **/
    @Column(name = "guarantee_flag", unique = false, nullable = false, length = 4)
    private String guaranteeFlag;

    /**
     * 还款方式
     **/
    @Column(name = "pmt_type", unique = false, nullable = false, length = 1)
    private String pmtType;

    /**
     * 借款利率
     **/
    @Column(name = "interest_rate", unique = false, nullable = false, length = 17)
    private java.math.BigDecimal interestRate;

    /**
     * 罚息利率
     **/
    @Column(name = "penalty_rate", unique = false, nullable = false, length = 17)
    private java.math.BigDecimal penaltyRate;

    /**
     * 借款利率类型
     **/
    @Column(name = "int_rate_type", unique = false, nullable = false, length = 1)
    private String intRateType;

    /**
     * 罚息利率类型
     **/
    @Column(name = "pen_rate_type", unique = false, nullable = false, length = 1)
    private String penRateType;

    /**
     * 保单号
     **/
    @Column(name = "policy_number", unique = false, nullable = true, length = 60)
    private String policyNumber;

    /**
     * 承贷比
     **/
    @Column(name = "loan_ratio", unique = false, nullable = false, length = 6)
    private String loanRatio;

    /**
     * 合作方编码
     **/
    @Column(name = "coop_id", unique = false, nullable = false, length = 61)
    private String coopId;

    /**
     * 银行实收比例
     **/
    @Column(name = "bank_scale", unique = false, nullable = false, length = 17)
    private java.math.BigDecimal bankScale;

    /**
     * 录入日期
     **/
    @Column(name = "input_date", unique = false, nullable = true, length = 10)
    private String inputDate;

    /**
     * 录入时间
     **/
    @Column(name = "input_time", unique = false, nullable = true, length = 20)
    private String inputTime;

    /**
     * 最后更新日期
     **/
    @Column(name = "last_update_date", unique = false, nullable = true, length = 10)
    private String lastUpdateDate;

    /**
     * 最后更新时间
     **/
    @Column(name = "last_update_time", unique = false, nullable = true, length = 20)
    private String lastUpdateTime;

    /**
     * 本期计提利息
     **/
    @Column(name = "accu_interest", unique = false, nullable = false, length = 17)
    private java.math.BigDecimal accuInterest;

    /**
     * 计提罚息
     **/
    @Column(name = "accu_penalty", unique = false, nullable = true, length = 17)
    private java.math.BigDecimal accuPenalty;


    /**
     * @param bizDate
     */
    public void setBizDate(String bizDate) {
        this.bizDate = bizDate;
    }

    /**
     * @return bizDate
     */
    public String getBizDate() {
        return this.bizDate;
    }

    /**
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return uuid
     */
    public String getUuid() {
        return this.uuid;
    }

    /**
     * @param contrNbr
     */
    public void setContrNbr(String contrNbr) {
        this.contrNbr = contrNbr;
    }

    /**
     * @return contrNbr
     */
    public String getContrNbr() {
        return this.contrNbr;
    }

    /**
     * @param refNbr
     */
    public void setRefNbr(String refNbr) {
        this.refNbr = refNbr;
    }

    /**
     * @return refNbr
     */
    public String getRefNbr() {
        return this.refNbr;
    }

    /**
     * @param currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return currency
     */
    public String getCurrency() {
        return this.currency;
    }

    /**
     * @param txnStatus
     */
    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    /**
     * @return txnStatus
     */
    public String getTxnStatus() {
        return this.txnStatus;
    }

    /**
     * @param txnDate
     */
    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    /**
     * @return txnDate
     */
    public String getTxnDate() {
        return this.txnDate;
    }

    /**
     * @param txnAmt
     */
    public void setTxnAmt(java.math.BigDecimal txnAmt) {
        this.txnAmt = txnAmt;
    }

    /**
     * @return txnAmt
     */
    public java.math.BigDecimal getTxnAmt() {
        return this.txnAmt;
    }

    /**
     * @param initTerm
     */
    public void setInitTerm(Integer initTerm) {
        this.initTerm = initTerm;
    }

    /**
     * @return initTerm
     */
    public Integer getInitTerm() {
        return this.initTerm;
    }

    /**
     * @param currTerm
     */
    public void setCurrTerm(Integer currTerm) {
        this.currTerm = currTerm;
    }

    /**
     * @return currTerm
     */
    public Integer getCurrTerm() {
        return this.currTerm;
    }

    /**
     * @param overdueDays
     */
    public void setOverdueDays(Integer overdueDays) {
        this.overdueDays = overdueDays;
    }

    /**
     * @return overdueDays
     */
    public Integer getOverdueDays() {
        return this.overdueDays;
    }

    /**
     * @param overdueDate
     */
    public void setOverdueDate(String overdueDate) {
        this.overdueDate = overdueDate;
    }

    /**
     * @return overdueDate
     */
    public String getOverdueDate() {
        return this.overdueDate;
    }

    /**
     * @param principalBal
     */
    public void setPrincipalBal(java.math.BigDecimal principalBal) {
        this.principalBal = principalBal;
    }

    /**
     * @return principalBal
     */
    public java.math.BigDecimal getPrincipalBal() {
        return this.principalBal;
    }

    /**
     * @param interestBal
     */
    public void setInterestBal(java.math.BigDecimal interestBal) {
        this.interestBal = interestBal;
    }

    /**
     * @return interestBal
     */
    public java.math.BigDecimal getInterestBal() {
        return this.interestBal;
    }

    /**
     * @param overduePrin
     */
    public void setOverduePrin(java.math.BigDecimal overduePrin) {
        this.overduePrin = overduePrin;
    }

    /**
     * @return overduePrin
     */
    public java.math.BigDecimal getOverduePrin() {
        return this.overduePrin;
    }

    /**
     * @param overdueInt
     */
    public void setOverdueInt(java.math.BigDecimal overdueInt) {
        this.overdueInt = overdueInt;
    }

    /**
     * @return overdueInt
     */
    public java.math.BigDecimal getOverdueInt() {
        return this.overdueInt;
    }

    /**
     * @param penaltyBal
     */
    public void setPenaltyBal(java.math.BigDecimal penaltyBal) {
        this.penaltyBal = penaltyBal;
    }

    /**
     * @return penaltyBal
     */
    public java.math.BigDecimal getPenaltyBal() {
        return this.penaltyBal;
    }

    /**
     * @param intType
     */
    public void setIntType(String intType) {
        this.intType = intType;
    }

    /**
     * @return intType
     */
    public String getIntType() {
        return this.intType;
    }

    /**
     * @param intFlag
     */
    public void setIntFlag(String intFlag) {
        this.intFlag = intFlag;
    }

    /**
     * @return intFlag
     */
    public String getIntFlag() {
        return this.intFlag;
    }

    /**
     * @param cardNo
     */
    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    /**
     * @return cardNo
     */
    public String getCardNo() {
        return this.cardNo;
    }

    /**
     * @param guaranteeFlag
     */
    public void setGuaranteeFlag(String guaranteeFlag) {
        this.guaranteeFlag = guaranteeFlag;
    }

    /**
     * @return guaranteeFlag
     */
    public String getGuaranteeFlag() {
        return this.guaranteeFlag;
    }

    /**
     * @param pmtType
     */
    public void setPmtType(String pmtType) {
        this.pmtType = pmtType;
    }

    /**
     * @return pmtType
     */
    public String getPmtType() {
        return this.pmtType;
    }

    /**
     * @param interestRate
     */
    public void setInterestRate(java.math.BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    /**
     * @return interestRate
     */
    public java.math.BigDecimal getInterestRate() {
        return this.interestRate;
    }

    /**
     * @param penaltyRate
     */
    public void setPenaltyRate(java.math.BigDecimal penaltyRate) {
        this.penaltyRate = penaltyRate;
    }

    /**
     * @return penaltyRate
     */
    public java.math.BigDecimal getPenaltyRate() {
        return this.penaltyRate;
    }

    /**
     * @param intRateType
     */
    public void setIntRateType(String intRateType) {
        this.intRateType = intRateType;
    }

    /**
     * @return intRateType
     */
    public String getIntRateType() {
        return this.intRateType;
    }

    /**
     * @param penRateType
     */
    public void setPenRateType(String penRateType) {
        this.penRateType = penRateType;
    }

    /**
     * @return penRateType
     */
    public String getPenRateType() {
        return this.penRateType;
    }

    /**
     * @param policyNumber
     */
    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    /**
     * @return policyNumber
     */
    public String getPolicyNumber() {
        return this.policyNumber;
    }

    /**
     * @param loanRatio
     */
    public void setLoanRatio(String loanRatio) {
        this.loanRatio = loanRatio;
    }

    /**
     * @return loanRatio
     */
    public String getLoanRatio() {
        return this.loanRatio;
    }

    /**
     * @param coopId
     */
    public void setCoopId(String coopId) {
        this.coopId = coopId;
    }

    /**
     * @return coopId
     */
    public String getCoopId() {
        return this.coopId;
    }

    /**
     * @param bankScale
     */
    public void setBankScale(java.math.BigDecimal bankScale) {
        this.bankScale = bankScale;
    }

    /**
     * @return bankScale
     */
    public java.math.BigDecimal getBankScale() {
        return this.bankScale;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param inputTime
     */
    public void setInputTime(String inputTime) {
        this.inputTime = inputTime;
    }

    /**
     * @return inputTime
     */
    public String getInputTime() {
        return this.inputTime;
    }

    /**
     * @param lastUpdateDate
     */
    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    /**
     * @return lastUpdateDate
     */
    public String getLastUpdateDate() {
        return this.lastUpdateDate;
    }

    /**
     * @param lastUpdateTime
     */
    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    /**
     * @return lastUpdateTime
     */
    public String getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    /**
     * @param accuInterest
     */
    public void setAccuInterest(java.math.BigDecimal accuInterest) {
        this.accuInterest = accuInterest;
    }

    /**
     * @return accuInterest
     */
    public java.math.BigDecimal getAccuInterest() {
        return this.accuInterest;
    }

    /**
     * @param accuPenalty
     */
    public void setAccuPenalty(java.math.BigDecimal accuPenalty) {
        this.accuPenalty = accuPenalty;
    }

    /**
     * @return accuPenalty
     */
    public java.math.BigDecimal getAccuPenalty() {
        return this.accuPenalty;
    }


}