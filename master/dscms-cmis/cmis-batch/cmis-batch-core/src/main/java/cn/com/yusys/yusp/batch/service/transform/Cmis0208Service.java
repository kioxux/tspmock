package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0208Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0208</br>
 * 任务名称：加工任务-额度处理-台账占用合同关系处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0208Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0208Service.class);
    @Autowired
    private Cmis0208Mapper cmis0208Mapper;

    /**
     * 更新额度占用关系07，对应SQL为
     *
     * @param openDay
     */
    public void cmis0208UpdateLmtContRel(String openDay) {

        logger.info("处理贷款占用合同关系表处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4A = cmis0208Mapper.insertLmtContAccRel4A(openDay);
        logger.info("处理贷款占用合同关系表处理结束,返回参数为:[{}]", insertLmtContAccRel4A);

        logger.info("银承合同产生垫款插入开始,请求参数为:[{}]", openDay);
        int insertTdcarYchtdk = cmis0208Mapper.insertTdcarYchtdk(openDay);
        logger.info("银承合同产生垫款插入理结束,返回参数为:[{}]", insertTdcarYchtdk);

        logger.info("最高额授信协议下银承产生垫款插入开始,请求参数为:[{}]", openDay);
        int insertTdcarZgeYcdk = cmis0208Mapper.insertTdcarZgeYcdk(openDay);
        logger.info("最高额授信协议下银承产生垫款插入结束,返回参数为:[{}]", insertTdcarZgeYcdk);

        logger.info("处理台账占用最高额授信协议的关系表处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4B = cmis0208Mapper.insertLmtContAccRel4B(openDay);
        logger.info("处理台账占用最高额授信协议的关系表处理结束,返回参数为:[{}]", insertLmtContAccRel4B);

        logger.info("处理银承台账占用最高额授信协议的关系表处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4Yc = cmis0208Mapper.insertLmtContAccRel4Yc(openDay);
        logger.info("处理银承台账占用最高额授信协议的关系表处理结束,返回参数为:[{}]", insertLmtContAccRel4Yc);

        logger.info("处理保函台账占用最高额授信协议的关系表处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4Bh = cmis0208Mapper.insertLmtContAccRel4Bh(openDay);
        logger.info("处理保函台账占用最高额授信协议的关系表处理结束,返回参数为:[{}]", insertLmtContAccRel4Bh);

        logger.info("处理信用证台账占用最高额授信协议的关系表处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4Xyz = cmis0208Mapper.insertLmtContAccRel4Xyz(openDay);
        logger.info("处理信用证台账占用最高额授信协议的关系表处理结束,返回参数为:[{}]", insertLmtContAccRel4Xyz);

        logger.info("处理贴现台账占用最高额授信协议的关系表处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4Txtz = cmis0208Mapper.insertLmtContAccRel4Txtz(openDay);
        logger.info("处理贴现台账占用最高额授信协议的关系表处理结束,返回参数为:[{}]", insertLmtContAccRel4Txtz);


        logger.info("处理委托贷款占用合同关系表处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4C = cmis0208Mapper.insertLmtContAccRel4C(openDay);
        logger.info("处理委托贷款占用合同关系表处理结束,返回参数为:[{}]", insertLmtContAccRel4C);


        logger.info("处理银承占用合同关系表处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4D = cmis0208Mapper.insertLmtContAccRel4D(openDay);
        logger.info("处理银承占用合同关系表处理结束,返回参数为:[{}]", insertLmtContAccRel4D);

        // 20211102  缺少的代码进行补充
        logger.info("处理保函占用合同关系表处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4F = cmis0208Mapper.insertLmtContAccRel4F(openDay);
        logger.info("处理保函占用合同关系表处理结束,返回参数为:[{}]", insertLmtContAccRel4F);


        logger.info("处理信用证占用合同关系处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4G = cmis0208Mapper.insertLmtContAccRel4G(openDay);
        logger.info("处理信用证占用合同关系处理结束,返回参数为:[{}]", insertLmtContAccRel4G);


        logger.info("处理贴现占用合同关系处理开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4H = cmis0208Mapper.insertLmtContAccRel4H(openDay);
        logger.info("处理贴现占用合同关系处理结束,返回参数为:[{}]", insertLmtContAccRel4H);

        logger.info("贷款占用资产池协议处理开始,请求参数为:[{}]", openDay);
        int insertLmtTdcar4Dkzyzcc = cmis0208Mapper.insertLmtTdcar4Dkzyzcc(openDay);
        logger.info("贷款占用资产池协议处理结束,返回参数为:[{}]", insertLmtTdcar4Dkzyzcc);

        logger.info("银承占用资产池协议处理开始,请求参数为:[{}]", openDay);
        int insertLmtTdcar4Yczyzcc = cmis0208Mapper.insertLmtTdcar4Yczyzcc(openDay);
        logger.info("银承占用资产池协议处理结束,返回参数为:[{}]", insertLmtTdcar4Yczyzcc);
    }
}
