/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.transform.gjp;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: TmpGjpSecurityAmt
 * @类描述: tmp_gjp_security_amt数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 10:37:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_gjp_security_amt")
public class TmpGjpSecurityAmt extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 国结主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 系统业务编号 **/
	@Column(name = "SYS_REF_NO", unique = false, nullable = true, length = 32)
	private String sysRefNo;
	
	/** 系统实体编号 **/
	@Column(name = "SYS_ENTY_ID", unique = false, nullable = true, length = 32)
	private String sysEntyId;
	
	/** 系统删除标识 STD_SYS_DEL_FLG **/
	@Column(name = "SYS_DEL_FLG", unique = false, nullable = true, length = 1)
	private String sysDelFlg;
	
	/** 国结创建人 **/
	@Column(name = "SYS_CRT_USER", unique = false, nullable = true, length = 32)
	private String sysCrtUser;
	
	/** 国结创建日期 **/
	@Column(name = "SYS_CRT_DT", unique = false, nullable = true, length = 20)
	private String sysCrtDt;
	
	/** 借据号 **/
	@Column(name = "IOU_NO", unique = false, nullable = true, length = 64)
	private String iouNo;
	
	/** 国结合同号 **/
	@Column(name = "CONTR_NO", unique = false, nullable = true, length = 64)
	private String contrNo;
	
	/** 国结客户号 **/
	@Column(name = "KERNEL_NO", unique = false, nullable = true, length = 64)
	private String kernelNo;
	
	/** 国结客户名称 **/
	@Column(name = "CUST_NM", unique = false, nullable = true, length = 64)
	private String custNm;
	
	/** 信用证号/保函编号 **/
	@Column(name = "SYS_REL_NO", unique = false, nullable = true, length = 64)
	private String sysRelNo;
	
	/** 业务币种 **/
	@Column(name = "CCY", unique = false, nullable = true, length = 3)
	private String ccy;
	
	/** 业务金额 **/
	@Column(name = "GJP_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal gjpAmt;
	
	/** 业务余额 **/
	@Column(name = "PAY_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal payBal;
	
	/** 国结保证金金额 **/
	@Column(name = "MGN_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal mgnAmt;
	
	/** 国结汇率 **/
	@Column(name = "MMS_RATE", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal mmsRate;
	
	/** 保证金编号 **/
	@Column(name = "MGN_NO", unique = false, nullable = true, length = 64)
	private String mgnNo;
	
	/** 发送时间 **/
	@Column(name = "SEND_DT", unique = false, nullable = true, length = 20)
	private String sendDt;
	
	/** 返回码 **/
	@Column(name = "RESP_CODE", unique = false, nullable = true, length = 8)
	private String respCode;
	
	/** 返回信息 **/
	@Column(name = "RESP_INFO", unique = false, nullable = true, length = 256)
	private String respInfo;
	
	/** 保证金操作标志 **/
	@Column(name = "OP_FLG", unique = false, nullable = true, length = 16)
	private String opFlg;
	
	/** 国结创建日期 **/
	@Column(name = "CREAT_DT", unique = false, nullable = true, length = 20)
	private String creatDt;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param sysRefNo
	 */
	public void setSysRefNo(String sysRefNo) {
		this.sysRefNo = sysRefNo;
	}
	
    /**
     * @return sysRefNo
     */
	public String getSysRefNo() {
		return this.sysRefNo;
	}
	
	/**
	 * @param sysEntyId
	 */
	public void setSysEntyId(String sysEntyId) {
		this.sysEntyId = sysEntyId;
	}
	
    /**
     * @return sysEntyId
     */
	public String getSysEntyId() {
		return this.sysEntyId;
	}
	
	/**
	 * @param sysDelFlg
	 */
	public void setSysDelFlg(String sysDelFlg) {
		this.sysDelFlg = sysDelFlg;
	}
	
    /**
     * @return sysDelFlg
     */
	public String getSysDelFlg() {
		return this.sysDelFlg;
	}
	
	/**
	 * @param sysCrtUser
	 */
	public void setSysCrtUser(String sysCrtUser) {
		this.sysCrtUser = sysCrtUser;
	}
	
    /**
     * @return sysCrtUser
     */
	public String getSysCrtUser() {
		return this.sysCrtUser;
	}
	
	/**
	 * @param sysCrtDt
	 */
	public void setSysCrtDt(String sysCrtDt) {
		this.sysCrtDt = sysCrtDt;
	}
	
    /**
     * @return sysCrtDt
     */
	public String getSysCrtDt() {
		return this.sysCrtDt;
	}
	
	/**
	 * @param iouNo
	 */
	public void setIouNo(String iouNo) {
		this.iouNo = iouNo;
	}
	
    /**
     * @return iouNo
     */
	public String getIouNo() {
		return this.iouNo;
	}
	
	/**
	 * @param contrNo
	 */
	public void setContrNo(String contrNo) {
		this.contrNo = contrNo;
	}
	
    /**
     * @return contrNo
     */
	public String getContrNo() {
		return this.contrNo;
	}
	
	/**
	 * @param kernelNo
	 */
	public void setKernelNo(String kernelNo) {
		this.kernelNo = kernelNo;
	}
	
    /**
     * @return kernelNo
     */
	public String getKernelNo() {
		return this.kernelNo;
	}
	
	/**
	 * @param custNm
	 */
	public void setCustNm(String custNm) {
		this.custNm = custNm;
	}
	
    /**
     * @return custNm
     */
	public String getCustNm() {
		return this.custNm;
	}
	
	/**
	 * @param sysRelNo
	 */
	public void setSysRelNo(String sysRelNo) {
		this.sysRelNo = sysRelNo;
	}
	
    /**
     * @return sysRelNo
     */
	public String getSysRelNo() {
		return this.sysRelNo;
	}
	
	/**
	 * @param ccy
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	
    /**
     * @return ccy
     */
	public String getCcy() {
		return this.ccy;
	}
	
	/**
	 * @param gjpAmt
	 */
	public void setGjpAmt(java.math.BigDecimal gjpAmt) {
		this.gjpAmt = gjpAmt;
	}
	
    /**
     * @return gjpAmt
     */
	public java.math.BigDecimal getGjpAmt() {
		return this.gjpAmt;
	}
	
	/**
	 * @param payBal
	 */
	public void setPayBal(java.math.BigDecimal payBal) {
		this.payBal = payBal;
	}
	
    /**
     * @return payBal
     */
	public java.math.BigDecimal getPayBal() {
		return this.payBal;
	}
	
	/**
	 * @param mgnAmt
	 */
	public void setMgnAmt(java.math.BigDecimal mgnAmt) {
		this.mgnAmt = mgnAmt;
	}
	
    /**
     * @return mgnAmt
     */
	public java.math.BigDecimal getMgnAmt() {
		return this.mgnAmt;
	}
	
	/**
	 * @param mmsRate
	 */
	public void setMmsRate(java.math.BigDecimal mmsRate) {
		this.mmsRate = mmsRate;
	}
	
    /**
     * @return mmsRate
     */
	public java.math.BigDecimal getMmsRate() {
		return this.mmsRate;
	}
	
	/**
	 * @param mgnNo
	 */
	public void setMgnNo(String mgnNo) {
		this.mgnNo = mgnNo;
	}
	
    /**
     * @return mgnNo
     */
	public String getMgnNo() {
		return this.mgnNo;
	}
	
	/**
	 * @param sendDt
	 */
	public void setSendDt(String sendDt) {
		this.sendDt = sendDt;
	}
	
    /**
     * @return sendDt
     */
	public String getSendDt() {
		return this.sendDt;
	}
	
	/**
	 * @param respCode
	 */
	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}
	
    /**
     * @return respCode
     */
	public String getRespCode() {
		return this.respCode;
	}
	
	/**
	 * @param respInfo
	 */
	public void setRespInfo(String respInfo) {
		this.respInfo = respInfo;
	}
	
    /**
     * @return respInfo
     */
	public String getRespInfo() {
		return this.respInfo;
	}
	
	/**
	 * @param opFlg
	 */
	public void setOpFlg(String opFlg) {
		this.opFlg = opFlg;
	}
	
    /**
     * @return opFlg
     */
	public String getOpFlg() {
		return this.opFlg;
	}
	
	/**
	 * @param creatDt
	 */
	public void setCreatDt(String creatDt) {
		this.creatDt = creatDt;
	}
	
    /**
     * @return creatDt
     */
	public String getCreatDt() {
		return this.creatDt;
	}


}