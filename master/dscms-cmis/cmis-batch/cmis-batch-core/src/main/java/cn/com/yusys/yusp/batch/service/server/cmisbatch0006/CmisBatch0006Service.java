package cn.com.yusys.yusp.batch.service.server.cmisbatch0006;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006RespDto;
import cn.com.yusys.yusp.batch.service.transform.Cmis0417Service;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.CmisPspClientService;
import com.alibaba.fastjson.JSON;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理Service:不定期检查任务信息生成
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class CmisBatch0006Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0006Service.class);

    @Autowired
    private Cmis0417Service cmis0417Service;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    //psp_debit_info
    @Autowired
    private CmisPspClientService cmisPspClientService;

    /**
     * 交易码：cmisbatch0006
     * 交易描述：不定期检查任务信息生成
     *
     * @param cmisbatch0006ReqDto
     * @return
     * @throws Exception
     */
//    @Async
//    @Transactional(rollbackFor = {BizException.class, Exception.class})
//    public Cmisbatch0006RespDto cmisBatch0006(Cmisbatch0006ReqDto cmisbatch0006ReqDto) throws Exception {
//        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, JSON.toJSONString(cmisbatch0006ReqDto));
//        Cmisbatch0006RespDto cmisbatch0006RespDto = new Cmisbatch0006RespDto();//
//        try {
//            String openDay = stringRedisTemplate.opsForValue().get("openDay");
//            Map map = new HashMap<>();
//            map.put("openDay", openDay);
//            if (Strings.isNotEmpty(cmisbatch0006ReqDto.getTaskNo())) {
//                map.put("taskNo", cmisbatch0006ReqDto.getTaskNo());
//            }
//            logger.info("调用加工任务-贷后管理-定期检查子表数据生成批量任务开始");
//            cmis0417Service.cmis0417InsertPspDebitInfo(map); // 插入定期检查借据信息
//            cmis0417Service.cmis0417InsertPspCheckRst(map); // 插入贷后检查结果表
//            cmis0417Service.cmis0417InsertPspGuarntrList(map);// 插入定期检查保证人检查
//            cmis0417Service.cmis0417InsertPspFinSituCheck(map);// 插入定期检查财务状况检查
//            cmis0417Service.cmis0417UpdatePspFinSituCheck(map);// 更新定期检查财务状况检查
//            cmis0417Service.cmis0417InsertPspImplementOpt(map);// 插入定期检查贷后管理建议落实情况
//            cmis0417Service.cmis0417InsertPspPldimnList(map);// 插入定期检查贷后管理建议落实情况
//            cmis0417Service.cmis0417InsertPspOperStatusCheck(map);// 插入定期检查经营状况检查（通用）
//            cmis0417Service.cmis0417InsertPspCusFinAnaly(map);// 插入定期检查融资情况分析
//            cmis0417Service.cmis0417InsertPspWarningInfoList(map);// 插入定期检查预警信号表
//            cmis0417Service.cmis0417InsertPspBankFinSitu(map);// 插入定期检查正常类本行融资情况
//            cmis0417Service.cmis0417InsertPspGuarCheck(map);// 插入定期检查正常类对外担保分析
//            cmis0417Service.cmis0417InsertPspRelCorp(map);// 插入对公定期检查不良类关联企业表
//            cmis0417Service.cmis0417InsertPspOperStatusCheckProperty(map);// 插入对公定期检查经营性物业贷款检查
//            cmis0417Service.cmis0417InsertPspOperStatusCheckRealpro(map);// 插入对公定期检查经营状况检查（房地产开发贷）
//            cmis0417Service.cmis0417InsertPspOperStatusCheckArch(map);// 插入对公定期检查经营状况检查（建筑业）
//            cmis0417Service.cmis0417InsertPspOperStatusCheckManufacture(map);// 插入对公定期检查经营状况检查（制造业）
//            cmis0417Service.cmis0417InsertPspCusBaseCase(map);// 插入对公、个人客户基本信息分析表
//            cmis0417Service.cmis0417InsertPspCapCusBaseCase(map);// 插入资金业务客户基本信息分析表
//            logger.info("调用加工任务-贷后管理-定期检查子表数据生成批量任务结束");
//            cmisbatch0006RespDto.setOpFlag(CmisCommonConstants.OP_FLAG_S);
//            cmisbatch0006RespDto.setOpMessage(CmisCommonConstants.OP_MSG_S);
//        } catch (Exception e) {
//            cmisbatch0006RespDto.setOpFlag(CmisCommonConstants.OP_FLAG_F);
//            cmisbatch0006RespDto.setOpMessage(CmisCommonConstants.OP_MSG_F);
//            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, e.getMessage());
//            throw new Exception(EpbEnum.EPB099999.value);
//        }
//        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value);
//
//        return cmisbatch0006RespDto;
//    }

    /**
     * 交易码：cmisBatch0006Ext
     * 交易描述：不定期检查任务信息生成
     *
     * @param cmisbatch0006ReqDto
     * @return
     * @throws Exception
     */
    @Async
    //@Transactional(rollbackFor = {BizException.class, Exception.class})
    public Cmisbatch0006RespDto cmisBatch0006Ext(Cmisbatch0006ReqDto cmisbatch0006ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, JSON.toJSONString(cmisbatch0006ReqDto));
        Cmisbatch0006RespDto cmisbatch0006RespDto = new Cmisbatch0006RespDto();//
        Map map = new HashMap<>();
        try {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            map.put("openDay", openDay);
            String taskNo = "";
            if (Strings.isNotEmpty(cmisbatch0006ReqDto.getTaskNo())) {
                taskNo = cmisbatch0006ReqDto.getTaskNo();
                map.put("taskNo", cmisbatch0006ReqDto.getTaskNo());
            }
            logger.info("调用加工任务-贷后管理-定期检查子表数据生成批量任务开始");
            /**
             * 1、先根据检查类型判断，插入对应的数据
             * 2、再根据报告类型区分正常瑕疵与不良各种特殊数据
             */
            String checkType = "";
            if (StringUtils.nonBlank(cmisbatch0006ReqDto.getCheckType())) {
                checkType = cmisbatch0006ReqDto.getCheckType();
                map.put("checkType", checkType);
            }
            // 获取客户号
            if (StringUtils.nonBlank(cmisbatch0006ReqDto.getCusId())) {
                map.put("cusId", cmisbatch0006ReqDto.getCusId());
            }
            // 报告类型
            String rptType = "";
            if (StringUtils.nonBlank(cmisbatch0006ReqDto.getRptType())) {
                rptType = cmisbatch0006ReqDto.getRptType();
            }


            // 因加了psp_task_list临时表，手工发起的需插入tmp_psp_task_list
            cmis0417Service.cmis0417InsertPspTaskList(map);
            // 投资业务
            if ("36".equals(checkType) || "37".equals(checkType) || "38".equals(checkType)) {
                cmis0417Service.cmis0417InsertPspCapCusBaseCase(map);// 插入资金业务客户基本信息分析表
            } else {
                cmis0417Service.cmis0417InsertPspDebitInfo(map); // 插入定期检查借据信息
                // 判断检查类型
                // 对公不定期检查 和个人经营性不定期检查
                if ("31".equals(checkType) || "32".equals(checkType)) {
                    // 融资情况分析对外担保分析
                    cmis0417Service.cmis0417InsertPspCusFinAnaly(map);// 插入定期检查融资情况分析
                    // 非不良类
                    if (!"3".equals(rptType)) {
                        cmis0417Service.cmis0417InsertPspBankFinSitu(map);// 插入定期检查正常类本行融资情况
                        cmis0417Service.cmis0417InsertPspGuarCheck(map);// 插入定期检查正常类对外担保分析
                        // 预警信号分析
                        cmis0417Service.cmis0417InsertPspWarningInfoList(map);// 插入定期检查预警信号表
                        // 经营状况分析 todo 可以通过融资情况判断选用哪种检查
                        cmis0417Service.cmis0417InsertPspOperStatusCheck(map);// 插入定期检查经营状况检查（通用）
                        cmis0417Service.cmis0417InsertPspOperStatusCheckProperty(map);// 插入对公定期检查经营性物业贷款检查
                        cmis0417Service.cmis0417InsertPspOperStatusCheckRealpro(map);// 插入对公定期检查经营状况检查（房地产开发贷）
                        cmis0417Service.cmis0417InsertPspOperStatusCheckArch(map);// 插入对公定期检查经营状况检查（建筑业）
                        cmis0417Service.cmis0417InsertPspOperStatusCheckManufacture(map);// 插入对公定期检查经营状况检查（制造业）
                        // 财务状况检查
                        cmis0417Service.cmis0417InsertPspFinSituCheck(map);// 插入定期检查财务状况检查
                        cmis0417Service.cmis0417UpdatePspFinSituCheck(map);// 更新定期检查财务状况检查
                        //授信时贷后管理建议落实情况
                        cmis0417Service.cmis0417InsertPspImplementOpt(map);// 插入定期检查贷后管理建议落实情况
                    } else if ("31".equals(checkType)) {// 对公不良
                        cmis0417Service.cmis0417InsertPspRelCorp(map);// 插入对公定期检查不良类关联企业表
                    }
                    // 担保状况检查
                    cmis0417Service.cmis0417InsertPspGuarntrList(map);// 插入定期检查保证人检查
                    cmis0417Service.cmis0417InsertPspPldimnList(map);// 插入抵质押物检查
                    // 个人消费性不良类
                } else if ("33".equals(checkType) && "3".equals(rptType)) {
                    // 担保状况检查
                    cmis0417Service.cmis0417InsertPspGuarntrList(map);// 插入定期检查保证人检查
                    cmis0417Service.cmis0417InsertPspPldimnList(map);// 插入抵质押物检查
                    // 小微经营性
                } else if ("34".equals(checkType)) {
                    //预警信号分析
                    cmis0417Service.cmis0417InsertPspWarningInfoList(map);// 插入定期检查预警信号表
                    // 小微消费性
                } else if ("35".equals(checkType)) {
                    cmis0417Service.cmis0417InsertPspWarningInfoList(map);// 插入定期检查预警信号表
                    // 投资业务
                }

                cmis0417Service.cmis0417InsertPspCusBaseCase(map);// 插入对公、个人客户基本信息分析表
                //检查结果及意见
                cmis0417Service.cmis0417InsertPspCheckRst(map); // 插入贷后检查结果表
                /**
                 * 征信数据插入 todo
                 */
                if ("31".equals(checkType) || "32".equals(checkType)) {
                    logger.info("调用征信，插入征信信息---开始");
                    cmisPspClientService.createCreditCus(taskNo);
                    logger.info("调用征信，插入征信信息---结束");
                }
            }
            // 日间手工发起，将借据信息插入原表
            cmis0417Service.cmis0417InsertPspDebitInfoFromTmp(map);
            logger.info("调用加工任务-贷后管理-定期检查子表数据生成批量任务结束");
            map.put("taskStatus","2");
            cmisbatch0006RespDto.setOpFlag(CmisCommonConstants.OP_FLAG_S);
            cmisbatch0006RespDto.setOpMessage(CmisCommonConstants.OP_MSG_S);
        } catch (Exception e) {
            map.put("taskStatus","3");
            cmisbatch0006RespDto.setOpFlag(CmisCommonConstants.OP_FLAG_F);
            cmisbatch0006RespDto.setOpMessage(CmisCommonConstants.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }finally {
            try {
                cmis0417Service.cmis0417ChangePspTaskList(map);
                logger.info("调用加工任务-贷后管理-不定期检查子表数据生成异常,更新该笔任务生成状态[{}]",map.get("taskStatus"));
            }catch (Exception e){
                logger.error(e.getMessage(),e);
                throw new RuntimeException();
            }
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value);
        return cmisbatch0006RespDto;
    }
}