package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0203Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0203</br>
 * 任务名称：加工任务-额度处理-表内占用敞口余额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0203Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0203Service.class);
    @Autowired
    private TableUtilsService tableUtilsService;

    @Autowired
    private Cmis0203Mapper cmis0203Mapper;

    /**
     * 更新额度占用关系02，对应SQL为CMIS0203-表内占用敞口余额.sql
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0203UpdateLmtContRel(String openDay) {
        logger.info("更新合同主表-将未关联额度的联合贷合同开始,请求参数为:[{}]", openDay);
        int updateCtrLoanCont = cmis0203Mapper.updateCtrLoanCont(openDay);
        logger.info("更新合同主表-将未关联额度的联合贷合同结束,返回参数为:[{}]", updateCtrLoanCont);

        logger.info("deleteTempCtrLoanCont0203清空删除合同主表0203开始,请求参数为:[{}]", openDay);
        // cmis0203Mapper.deleteTempCtrLoanCont0203();// cmis_biz.temp_ctr_loan_cont_0203
        tableUtilsService.renameCreateDropTable("cmis_biz", "temp_ctr_loan_cont_0203");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTempCtrLoanCont0203清空删除合同主表0203结束");

        logger.info("插入除合同主表0203开始,请求参数为:[{}]", openDay);
        int insertTempCtrLoanCont0203 = cmis0203Mapper.insertTempCtrLoanCont0203(openDay);
        logger.info("插入除合同主表0203结束,返回参数为:[{}]", insertTempCtrLoanCont0203);


        logger.info("插入批复额度分项基础信息-将未生成额度的合同生成额度分项信息开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfo = cmis0203Mapper.insertApprLmtSubBasicInfo(openDay);
        logger.info("插入批复额度分项基础信息-将未生成额度的合同生成额度分项信息结束,返回参数为:[{}]", insertApprLmtSubBasicInfo);


        logger.info("插入批复主信息-将未生成额度的合同生成额度主信息开始,请求参数为:[{}]", openDay);
        int insertApprStrMtableInfo = cmis0203Mapper.insertApprStrMtableInfo(openDay);
        logger.info("插入批复主信息-将未生成额度的合同生成额度主信息结束,返回参数为:[{}]", insertApprStrMtableInfo);
    }
}
