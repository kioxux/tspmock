package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0115Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0115</br>
 * 任务名称：加工任务-业务处理-风险预警处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0115Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0115Service.class);

    @Autowired
    private Cmis0115Mapper cmis0115Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 清空和插入风险预警业务表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0115TruncateAndInsertBatBizRiskSign(String openDay) {
        logger.info("重命名创建和删除临风险预警业务表开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","bat_biz_risk_sign");
        logger.info("重命名创建和删除临风险预警业务表结束");
        logger.info("插入风险预警业务表开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskSign = cmis0115Mapper.insertBatBizRiskSign(openDay);
        logger.info("插入风险预警业务表结束,返回参数为:[{}]", insertBatBizRiskSign);
        tableUtilsService.analyzeTable("cmis_biz","bat_biz_risk_sign");//分析表
    }
}
