package cn.com.yusys.yusp.batch.job.bak;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bak.BakD0025Service;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepLmtEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKD0025</br>
 * 任务名称：批前备份日表任务-备份担保合同表[GRT_GUAR_CONT]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class BakD0025Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(BakD0025Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private BakD0025Service bakD0025Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job bakD0025Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_JOB.key, JobStepLmtEnum.BAKD0025_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job bakD0025Job = this.jobBuilderFactory.get(JobStepLmtEnum.BAKD0025_JOB.key)
                .start(bakD0025UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(bakD0025CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                //.next(bakD0025DeleteCurrentStep(WILL_BE_INJECTED)) // 删除当天的数据
                .next(bakD0025InsertCurrentStep(WILL_BE_INJECTED)) // 备份当天的数据
                .next(bakD0025UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return bakD0025Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0025UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_UPDATE_TASK010_STEP.key, JobStepLmtEnum.BAKD0025_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0025UpdateTask010Step = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0025_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BAKD0025_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_UPDATE_TASK010_STEP.key, JobStepLmtEnum.BAKD0025_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0025UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0025CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_CHECK_REL_STEP.key, JobStepLmtEnum.BAKD0025_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0025CheckRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0025_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BAKD0025_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_CHECK_REL_STEP.key, JobStepLmtEnum.BAKD0025_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_CHECK_REL_STEP.key, JobStepLmtEnum.BAKD0025_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return bakD0025CheckRelStep;
    }

    /**
     * 备份当天的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0025InsertCurrentStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_INSERT_CURRENT_STEP.key, JobStepLmtEnum.BAKD0025_INSERT_CURRENT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0025InsertCurrentStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0025_INSERT_CURRENT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0025Service.bakD0025DeleteCurrent(openDay);//删除当天的数据
                    bakD0025Service.bakD0025InsertCurrent(openDay);//备份当天的数据
                    bakD0025Service.checkBakDEqualsOriginal(openDay);//校验备份表和原表数据是否一致
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_INSERT_CURRENT_STEP.key, JobStepLmtEnum.BAKD0025_INSERT_CURRENT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0025InsertCurrentStep;
    }

    /**
     * 删除当天的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0025DeleteCurrentStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_DELETE_CURRENT_STEP.key, JobStepLmtEnum.BAKD0025_DELETE_CURRENT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0025DeleteCurrentStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0025_DELETE_CURRENT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0025Service.bakD0025DeleteCurrent(openDay);//删除当天的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_DELETE_CURRENT_STEP.key, JobStepLmtEnum.BAKD0025_DELETE_CURRENT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0025DeleteCurrentStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0025UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_UPDATE_TASK100_STEP.key, JobStepLmtEnum.BAKD0025_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0025UpdateTask100Step = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0025_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BAKD0025_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_UPDATE_TASK100_STEP.key, JobStepLmtEnum.BAKD0025_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0025UpdateTask100Step;
    }

}
