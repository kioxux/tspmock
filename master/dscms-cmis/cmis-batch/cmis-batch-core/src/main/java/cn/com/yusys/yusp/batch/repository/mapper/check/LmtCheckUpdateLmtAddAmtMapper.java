/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.check;

import org.apache.ibatis.annotations.Param;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateLmtAddAmtMapper
 * @类描述: #Dao类
 * @功能描述: 数据更新额度校正-计算分项授信总额累加
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCheckUpdateLmtAddAmtMapper {


    /**
     * 1、更新授信总额累加为0
     */
    int updateLmtAddAmtTo0(@Param("cusId") String cusId);

    /**
     * 2、加工授信总额累加
     */
    int insertLmtAddAmt1(@Param("cusId") String cusId);

    int insertLmtAddAmt2(@Param("cusId") String cusId);

    int insertLmtAddAmt3(@Param("cusId") String cusId);

    int insertLmtAddAmt4(@Param("cusId") String cusId);

    int truncateLmtAccDetails01();

    int insertTmpAccAddDetails(@Param("cusId") String cusId);

    int truncateTmpLmtAmt01();
    int insertLmtAddCase01(@Param("cusId") String cusId);
    int insertLmtAddCase02(@Param("cusId") String cusId);
    int insertLmtAddCase03(@Param("cusId") String cusId);
    int insertLmtAddCase04(@Param("cusId") String cusId);

    int updateLmtAddAmtCase05(@Param("cusId") String cusId);

    int truncateTmpLmtAmt02();

    int insertLmtAddOneSubTmp(@Param("cusId") String cusId);
    int updateLmtAddOneSub(@Param("cusId") String cusId);
}