/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTCoreKlnlDkkhmx
 * @类描述: bat_t_core_klnl_dkkhmx数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_core_klnl_dkkhmx")
public class BatTCoreKlnlDkkhmx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 法人代码 **/
	@Id
	@Column(name = "farendma")
	private String farendma;
	
	/** 明细编号 **/
	@Id
	@Column(name = "mingxibh")
	private String mingxibh;
	
	/** 贷款借据号 **/
	@Id
	@Column(name = "dkjiejuh")
	private String dkjiejuh;
	
	/** 贷款账号 **/
	@Column(name = "dkzhangh", unique = false, nullable = false, length = 40)
	private String dkzhangh;
	
	/** 货币代号 STD_HUOBDHAO **/
	@Column(name = "huobdhao", unique = false, nullable = true, length = 3)
	private String huobdhao;
	
	/** 营业机构 **/
	@Column(name = "yngyjigo", unique = false, nullable = true, length = 12)
	private String yngyjigo;
	
	/** 交易日期 **/
	@Column(name = "jiaoyirq", unique = false, nullable = false, length = 8)
	private String jiaoyirq;
	
	/** 贷款入账账号 **/
	@Column(name = "dkrzhzhh", unique = false, nullable = true, length = 35)
	private String dkrzhzhh;
	
	/** 贷款入账账号子序号 **/
	@Column(name = "dkrzhzxh", unique = false, nullable = true, length = 8)
	private String dkrzhzxh;
	
	/** 放款金额 **/
	@Column(name = "fkjineee", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal fkjineee;
	
	/** 放款资金处理方式 STD_FKZJCLFS **/
	@Column(name = "fkzjclfs", unique = false, nullable = true, length = 1)
	private String fkzjclfs;
	
	/** 待销账序号 **/
	@Column(name = "daixzhxh", unique = false, nullable = true, length = 500)
	private String daixzhxh;
	
	/** 冻结编号 **/
	@Column(name = "djiebhao", unique = false, nullable = true, length = 500)
	private String djiebhao;
	
	/** 正常本金 **/
	@Column(name = "zhchbjin", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal zhchbjin;
	
	/** 逾期本金 **/
	@Column(name = "yuqibjin", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yuqibjin;
	
	/** 呆滞本金 **/
	@Column(name = "dzhibjin", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal dzhibjin;
	
	/** 呆账本金 **/
	@Column(name = "daizbjin", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal daizbjin;
	
	/** 资金来源 STD_ZIJNLAIY **/
	@Column(name = "zijnlaiy", unique = false, nullable = true, length = 1)
	private String zijnlaiy;
	
	/** 还款账号 **/
	@Column(name = "huankzhh", unique = false, nullable = true, length = 35)
	private String huankzhh;
	
	/** 还款账号子序号 **/
	@Column(name = "hkzhhzxh", unique = false, nullable = true, length = 8)
	private String hkzhhzxh;
	
	/** 可用金额 **/
	@Column(name = "keyongje", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal keyongje;
	
	/** 还款总额 **/
	@Column(name = "hkzongee", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal hkzongee;
	
	/** 还款状态 STD_HUANKZHT **/
	@Column(name = "huankzht", unique = false, nullable = true, length = 1)
	private String huankzht;
	
	/** 归还本金 **/
	@Column(name = "ghbenjin", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghbenjin;
	
	/** 归还核销本金 **/
	@Column(name = "ghhxbenj", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghhxbenj;
	
	/** 归还应收应计利息 **/
	@Column(name = "ghysyjlx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghysyjlx;
	
	/** 归还催收应计利息 **/
	@Column(name = "ghcsyjlx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghcsyjlx;
	
	/** 归还应收欠息 **/
	@Column(name = "ghynshqx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghynshqx;
	
	/** 归还催收欠息 **/
	@Column(name = "ghcushqx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghcushqx;
	
	/** 归还应收应计罚息 **/
	@Column(name = "ghysyjfx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghysyjfx;
	
	/** 归还催收应计罚息 **/
	@Column(name = "ghcsyjfx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghcsyjfx;
	
	/** 归还应收罚息 **/
	@Column(name = "ghynshfx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghynshfx;
	
	/** 归还催收罚息 **/
	@Column(name = "ghcushfx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghcushfx;
	
	/** 归还应计复息 **/
	@Column(name = "ghyjfuxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghyjfuxi;
	
	/** 归还复息 **/
	@Column(name = "ghfxfuxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghfxfuxi;
	
	/** 归还核销利息 **/
	@Column(name = "ghhxlixi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghhxlixi;
	
	/** 归还已核销本金利息 **/
	@Column(name = "ghyhxbjl", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghyhxbjl;
	
	/** 归还罚金 **/
	@Column(name = "ghfajinn", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghfajinn;
	
	/** 归还费用 **/
	@Column(name = "ghfeiyin", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ghfeiyin;
	
	/** 交易机构 **/
	@Column(name = "jiaoyijg", unique = false, nullable = true, length = 12)
	private String jiaoyijg;
	
	/** 交易柜员 **/
	@Column(name = "jiaoyigy", unique = false, nullable = true, length = 8)
	private String jiaoyigy;
	
	/** 交易流水 **/
	@Column(name = "jiaoyils", unique = false, nullable = false, length = 32)
	private String jiaoyils;
	
	/** 交易事件 **/
	@Column(name = "jiaoyisj", unique = false, nullable = true, length = 10)
	private String jiaoyisj;
	
	/** 事件说明 **/
	@Column(name = "shjshuom", unique = false, nullable = true, length = 80)
	private String shjshuom;
	
	/** 交易码 **/
	@Column(name = "jiaoyima", unique = false, nullable = true, length = 20)
	private String jiaoyima;
	
	/** 渠道号 **/
	@Column(name = "qudaohao", unique = false, nullable = true, length = 7)
	private String qudaohao;
	
	/** 摘要 **/
	@Column(name = "zhaiyoms", unique = false, nullable = true, length = 300)
	private String zhaiyoms;
	
	/** 贷款归还方式 **/
	@Column(name = "daikghfs", unique = false, nullable = true, length = 2)
	private String daikghfs;
	
	/** 还款备注 **/
	@Column(name = "hkbeizhu", unique = false, nullable = true, length = 65535)
	private String hkbeizhu;
	
	/** 分行标识 **/
	@Column(name = "fenhbios", unique = false, nullable = false, length = 4)
	private String fenhbios;
	
	/** 维护柜员 **/
	@Column(name = "WEIHGUIY", unique = false, nullable = true, length = 8)
	private String weihguiy;
	
	/** 维护机构 **/
	@Column(name = "weihjigo", unique = false, nullable = false, length = 12)
	private String weihjigo;
	
	/** 维护日期 **/
	@Column(name = "weihriqi", unique = false, nullable = false, length = 8)
	private String weihriqi;
	
	/** 维护时间 **/
	@Column(name = "weihshij", unique = false, nullable = true, length = 9)
	private String weihshij;
	
	/** 时间戳 **/
	@Column(name = "shijchuo", unique = false, nullable = false, length = 19)
	private Long shijchuo;
	
	/** 记录状态 STD_JILUZTAI **/
	@Column(name = "jiluztai", unique = false, nullable = false, length = 1)
	private String jiluztai;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param mingxibh
	 */
	public void setMingxibh(String mingxibh) {
		this.mingxibh = mingxibh;
	}
	
    /**
     * @return mingxibh
     */
	public String getMingxibh() {
		return this.mingxibh;
	}
	
	/**
	 * @param dkzhangh
	 */
	public void setDkzhangh(String dkzhangh) {
		this.dkzhangh = dkzhangh;
	}
	
    /**
     * @return dkzhangh
     */
	public String getDkzhangh() {
		return this.dkzhangh;
	}
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param huobdhao
	 */
	public void setHuobdhao(String huobdhao) {
		this.huobdhao = huobdhao;
	}
	
    /**
     * @return huobdhao
     */
	public String getHuobdhao() {
		return this.huobdhao;
	}
	
	/**
	 * @param yngyjigo
	 */
	public void setYngyjigo(String yngyjigo) {
		this.yngyjigo = yngyjigo;
	}
	
    /**
     * @return yngyjigo
     */
	public String getYngyjigo() {
		return this.yngyjigo;
	}
	
	/**
	 * @param jiaoyirq
	 */
	public void setJiaoyirq(String jiaoyirq) {
		this.jiaoyirq = jiaoyirq;
	}
	
    /**
     * @return jiaoyirq
     */
	public String getJiaoyirq() {
		return this.jiaoyirq;
	}
	
	/**
	 * @param dkrzhzhh
	 */
	public void setDkrzhzhh(String dkrzhzhh) {
		this.dkrzhzhh = dkrzhzhh;
	}
	
    /**
     * @return dkrzhzhh
     */
	public String getDkrzhzhh() {
		return this.dkrzhzhh;
	}
	
	/**
	 * @param dkrzhzxh
	 */
	public void setDkrzhzxh(String dkrzhzxh) {
		this.dkrzhzxh = dkrzhzxh;
	}
	
    /**
     * @return dkrzhzxh
     */
	public String getDkrzhzxh() {
		return this.dkrzhzxh;
	}
	
	/**
	 * @param fkjineee
	 */
	public void setFkjineee(java.math.BigDecimal fkjineee) {
		this.fkjineee = fkjineee;
	}
	
    /**
     * @return fkjineee
     */
	public java.math.BigDecimal getFkjineee() {
		return this.fkjineee;
	}
	
	/**
	 * @param fkzjclfs
	 */
	public void setFkzjclfs(String fkzjclfs) {
		this.fkzjclfs = fkzjclfs;
	}
	
    /**
     * @return fkzjclfs
     */
	public String getFkzjclfs() {
		return this.fkzjclfs;
	}
	
	/**
	 * @param daixzhxh
	 */
	public void setDaixzhxh(String daixzhxh) {
		this.daixzhxh = daixzhxh;
	}
	
    /**
     * @return daixzhxh
     */
	public String getDaixzhxh() {
		return this.daixzhxh;
	}
	
	/**
	 * @param djiebhao
	 */
	public void setDjiebhao(String djiebhao) {
		this.djiebhao = djiebhao;
	}
	
    /**
     * @return djiebhao
     */
	public String getDjiebhao() {
		return this.djiebhao;
	}
	
	/**
	 * @param zhchbjin
	 */
	public void setZhchbjin(java.math.BigDecimal zhchbjin) {
		this.zhchbjin = zhchbjin;
	}
	
    /**
     * @return zhchbjin
     */
	public java.math.BigDecimal getZhchbjin() {
		return this.zhchbjin;
	}
	
	/**
	 * @param yuqibjin
	 */
	public void setYuqibjin(java.math.BigDecimal yuqibjin) {
		this.yuqibjin = yuqibjin;
	}
	
    /**
     * @return yuqibjin
     */
	public java.math.BigDecimal getYuqibjin() {
		return this.yuqibjin;
	}
	
	/**
	 * @param dzhibjin
	 */
	public void setDzhibjin(java.math.BigDecimal dzhibjin) {
		this.dzhibjin = dzhibjin;
	}
	
    /**
     * @return dzhibjin
     */
	public java.math.BigDecimal getDzhibjin() {
		return this.dzhibjin;
	}
	
	/**
	 * @param daizbjin
	 */
	public void setDaizbjin(java.math.BigDecimal daizbjin) {
		this.daizbjin = daizbjin;
	}
	
    /**
     * @return daizbjin
     */
	public java.math.BigDecimal getDaizbjin() {
		return this.daizbjin;
	}
	
	/**
	 * @param zijnlaiy
	 */
	public void setZijnlaiy(String zijnlaiy) {
		this.zijnlaiy = zijnlaiy;
	}
	
    /**
     * @return zijnlaiy
     */
	public String getZijnlaiy() {
		return this.zijnlaiy;
	}
	
	/**
	 * @param huankzhh
	 */
	public void setHuankzhh(String huankzhh) {
		this.huankzhh = huankzhh;
	}
	
    /**
     * @return huankzhh
     */
	public String getHuankzhh() {
		return this.huankzhh;
	}
	
	/**
	 * @param hkzhhzxh
	 */
	public void setHkzhhzxh(String hkzhhzxh) {
		this.hkzhhzxh = hkzhhzxh;
	}
	
    /**
     * @return hkzhhzxh
     */
	public String getHkzhhzxh() {
		return this.hkzhhzxh;
	}
	
	/**
	 * @param keyongje
	 */
	public void setKeyongje(java.math.BigDecimal keyongje) {
		this.keyongje = keyongje;
	}
	
    /**
     * @return keyongje
     */
	public java.math.BigDecimal getKeyongje() {
		return this.keyongje;
	}
	
	/**
	 * @param hkzongee
	 */
	public void setHkzongee(java.math.BigDecimal hkzongee) {
		this.hkzongee = hkzongee;
	}
	
    /**
     * @return hkzongee
     */
	public java.math.BigDecimal getHkzongee() {
		return this.hkzongee;
	}
	
	/**
	 * @param huankzht
	 */
	public void setHuankzht(String huankzht) {
		this.huankzht = huankzht;
	}
	
    /**
     * @return huankzht
     */
	public String getHuankzht() {
		return this.huankzht;
	}
	
	/**
	 * @param ghbenjin
	 */
	public void setGhbenjin(java.math.BigDecimal ghbenjin) {
		this.ghbenjin = ghbenjin;
	}
	
    /**
     * @return ghbenjin
     */
	public java.math.BigDecimal getGhbenjin() {
		return this.ghbenjin;
	}
	
	/**
	 * @param ghhxbenj
	 */
	public void setGhhxbenj(java.math.BigDecimal ghhxbenj) {
		this.ghhxbenj = ghhxbenj;
	}
	
    /**
     * @return ghhxbenj
     */
	public java.math.BigDecimal getGhhxbenj() {
		return this.ghhxbenj;
	}
	
	/**
	 * @param ghysyjlx
	 */
	public void setGhysyjlx(java.math.BigDecimal ghysyjlx) {
		this.ghysyjlx = ghysyjlx;
	}
	
    /**
     * @return ghysyjlx
     */
	public java.math.BigDecimal getGhysyjlx() {
		return this.ghysyjlx;
	}
	
	/**
	 * @param ghcsyjlx
	 */
	public void setGhcsyjlx(java.math.BigDecimal ghcsyjlx) {
		this.ghcsyjlx = ghcsyjlx;
	}
	
    /**
     * @return ghcsyjlx
     */
	public java.math.BigDecimal getGhcsyjlx() {
		return this.ghcsyjlx;
	}
	
	/**
	 * @param ghynshqx
	 */
	public void setGhynshqx(java.math.BigDecimal ghynshqx) {
		this.ghynshqx = ghynshqx;
	}
	
    /**
     * @return ghynshqx
     */
	public java.math.BigDecimal getGhynshqx() {
		return this.ghynshqx;
	}
	
	/**
	 * @param ghcushqx
	 */
	public void setGhcushqx(java.math.BigDecimal ghcushqx) {
		this.ghcushqx = ghcushqx;
	}
	
    /**
     * @return ghcushqx
     */
	public java.math.BigDecimal getGhcushqx() {
		return this.ghcushqx;
	}
	
	/**
	 * @param ghysyjfx
	 */
	public void setGhysyjfx(java.math.BigDecimal ghysyjfx) {
		this.ghysyjfx = ghysyjfx;
	}
	
    /**
     * @return ghysyjfx
     */
	public java.math.BigDecimal getGhysyjfx() {
		return this.ghysyjfx;
	}
	
	/**
	 * @param ghcsyjfx
	 */
	public void setGhcsyjfx(java.math.BigDecimal ghcsyjfx) {
		this.ghcsyjfx = ghcsyjfx;
	}
	
    /**
     * @return ghcsyjfx
     */
	public java.math.BigDecimal getGhcsyjfx() {
		return this.ghcsyjfx;
	}
	
	/**
	 * @param ghynshfx
	 */
	public void setGhynshfx(java.math.BigDecimal ghynshfx) {
		this.ghynshfx = ghynshfx;
	}
	
    /**
     * @return ghynshfx
     */
	public java.math.BigDecimal getGhynshfx() {
		return this.ghynshfx;
	}
	
	/**
	 * @param ghcushfx
	 */
	public void setGhcushfx(java.math.BigDecimal ghcushfx) {
		this.ghcushfx = ghcushfx;
	}
	
    /**
     * @return ghcushfx
     */
	public java.math.BigDecimal getGhcushfx() {
		return this.ghcushfx;
	}
	
	/**
	 * @param ghyjfuxi
	 */
	public void setGhyjfuxi(java.math.BigDecimal ghyjfuxi) {
		this.ghyjfuxi = ghyjfuxi;
	}
	
    /**
     * @return ghyjfuxi
     */
	public java.math.BigDecimal getGhyjfuxi() {
		return this.ghyjfuxi;
	}
	
	/**
	 * @param ghfxfuxi
	 */
	public void setGhfxfuxi(java.math.BigDecimal ghfxfuxi) {
		this.ghfxfuxi = ghfxfuxi;
	}
	
    /**
     * @return ghfxfuxi
     */
	public java.math.BigDecimal getGhfxfuxi() {
		return this.ghfxfuxi;
	}
	
	/**
	 * @param ghhxlixi
	 */
	public void setGhhxlixi(java.math.BigDecimal ghhxlixi) {
		this.ghhxlixi = ghhxlixi;
	}
	
    /**
     * @return ghhxlixi
     */
	public java.math.BigDecimal getGhhxlixi() {
		return this.ghhxlixi;
	}
	
	/**
	 * @param ghyhxbjl
	 */
	public void setGhyhxbjl(java.math.BigDecimal ghyhxbjl) {
		this.ghyhxbjl = ghyhxbjl;
	}
	
    /**
     * @return ghyhxbjl
     */
	public java.math.BigDecimal getGhyhxbjl() {
		return this.ghyhxbjl;
	}
	
	/**
	 * @param ghfajinn
	 */
	public void setGhfajinn(java.math.BigDecimal ghfajinn) {
		this.ghfajinn = ghfajinn;
	}
	
    /**
     * @return ghfajinn
     */
	public java.math.BigDecimal getGhfajinn() {
		return this.ghfajinn;
	}
	
	/**
	 * @param ghfeiyin
	 */
	public void setGhfeiyin(java.math.BigDecimal ghfeiyin) {
		this.ghfeiyin = ghfeiyin;
	}
	
    /**
     * @return ghfeiyin
     */
	public java.math.BigDecimal getGhfeiyin() {
		return this.ghfeiyin;
	}
	
	/**
	 * @param jiaoyijg
	 */
	public void setJiaoyijg(String jiaoyijg) {
		this.jiaoyijg = jiaoyijg;
	}
	
    /**
     * @return jiaoyijg
     */
	public String getJiaoyijg() {
		return this.jiaoyijg;
	}
	
	/**
	 * @param jiaoyigy
	 */
	public void setJiaoyigy(String jiaoyigy) {
		this.jiaoyigy = jiaoyigy;
	}
	
    /**
     * @return jiaoyigy
     */
	public String getJiaoyigy() {
		return this.jiaoyigy;
	}
	
	/**
	 * @param jiaoyils
	 */
	public void setJiaoyils(String jiaoyils) {
		this.jiaoyils = jiaoyils;
	}
	
    /**
     * @return jiaoyils
     */
	public String getJiaoyils() {
		return this.jiaoyils;
	}
	
	/**
	 * @param jiaoyisj
	 */
	public void setJiaoyisj(String jiaoyisj) {
		this.jiaoyisj = jiaoyisj;
	}
	
    /**
     * @return jiaoyisj
     */
	public String getJiaoyisj() {
		return this.jiaoyisj;
	}
	
	/**
	 * @param shjshuom
	 */
	public void setShjshuom(String shjshuom) {
		this.shjshuom = shjshuom;
	}
	
    /**
     * @return shjshuom
     */
	public String getShjshuom() {
		return this.shjshuom;
	}
	
	/**
	 * @param jiaoyima
	 */
	public void setJiaoyima(String jiaoyima) {
		this.jiaoyima = jiaoyima;
	}
	
    /**
     * @return jiaoyima
     */
	public String getJiaoyima() {
		return this.jiaoyima;
	}
	
	/**
	 * @param qudaohao
	 */
	public void setQudaohao(String qudaohao) {
		this.qudaohao = qudaohao;
	}
	
    /**
     * @return qudaohao
     */
	public String getQudaohao() {
		return this.qudaohao;
	}
	
	/**
	 * @param zhaiyoms
	 */
	public void setZhaiyoms(String zhaiyoms) {
		this.zhaiyoms = zhaiyoms;
	}
	
    /**
     * @return zhaiyoms
     */
	public String getZhaiyoms() {
		return this.zhaiyoms;
	}
	
	/**
	 * @param daikghfs
	 */
	public void setDaikghfs(String daikghfs) {
		this.daikghfs = daikghfs;
	}
	
    /**
     * @return daikghfs
     */
	public String getDaikghfs() {
		return this.daikghfs;
	}
	
	/**
	 * @param hkbeizhu
	 */
	public void setHkbeizhu(String hkbeizhu) {
		this.hkbeizhu = hkbeizhu;
	}
	
    /**
     * @return hkbeizhu
     */
	public String getHkbeizhu() {
		return this.hkbeizhu;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}


}