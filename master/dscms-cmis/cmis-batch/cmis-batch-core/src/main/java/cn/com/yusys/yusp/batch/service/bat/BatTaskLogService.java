/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskLog;
import cn.com.yusys.yusp.batch.repository.mapper.bat.BatTaskLogMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTaskLogService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 16:29:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTaskLogService {
    private static final Logger logger = LoggerFactory.getLogger(BatTaskLogService.class);

    @Autowired
    private BatTaskLogMapper batTaskLogMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatTaskLog selectByPrimaryKey(String taskDate, String taskNo) {
        return batTaskLogMapper.selectByPrimaryKey(taskDate, taskNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatTaskLog> selectAll(QueryModel model) {
        List<BatTaskLog> records = (List<BatTaskLog>) batTaskLogMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatTaskLog> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTaskLog> list = batTaskLogMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatTaskLog record) {
        record.setInputDate(DateUtils.getCurrDateStr()); // 登记日期
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setCreateTime(DateUtils.getCurrTimestamp());// 创建时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskLogMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatTaskLog record) {
        record.setInputDate(DateUtils.getCurrDateStr()); // 登记日期
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setCreateTime(DateUtils.getCurrTimestamp());// 创建时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskLogMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatTaskLog record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskLogMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatTaskLog record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskLogMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String taskDate, String taskNo) {
        return batTaskLogMapper.deleteByPrimaryKey(taskDate, taskNo);
    }

    /**
     * 将任务运行管理数据保存到任务运行日志中
     *
     * @param openDay
     * @return
     */
    public int insertByOpenDay(String openDay) {
        logger.info("将任务运行管理数据保存到任务运行日志开始,请求参数为:[{}]", openDay);
        int insertByOpenDay = batTaskLogMapper.insertByOpenDay(openDay);
        logger.info("将任务运行管理数据保存到任务运行日志结束,响应参数为:[{}]", insertByOpenDay);
        return insertByOpenDay;
    }
}
