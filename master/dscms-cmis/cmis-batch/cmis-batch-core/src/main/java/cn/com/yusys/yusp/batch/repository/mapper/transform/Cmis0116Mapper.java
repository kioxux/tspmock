package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0116</br>
 * 任务名称：加工任务-业务处理-信用卡额度同步  </br>
 *
 * @author 徐超
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0116Mapper {

    /**
     * 清除批复分项信息-清除所有信用卡额度
     *
     * @param openDay
     */
    void deleteApprStrMtableInfo01(@Param("openDay") String openDay);


    /**
     * 清除批复主信息-清除所有信用卡额度
     *
     * @param openDay
     */
    void deleteApprLmtSubBasicInfo01(@Param("openDay") String openDay);

    /**
     * 更新通联推送的文件日期为10位日期
     *
     * @param openDay
     */
    int updateBatSDjkDataAccount(@Param("openDay") String openDay);

    /**
     * 插入批复分项信息对应账户信息文件
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo01(@Param("openDay") String openDay);

    /**
     * 更新批复分项信息对应账户信息文件
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo01(@Param("openDay") String openDay);


    /**
     * 插入批复主信息对应账户信息文件
     *
     * @param openDay
     * @return
     */
    int insertApprStrMtableInfo01(@Param("openDay") String openDay);

    /**
     * 插入批复分项信息对应大额分期文件
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo02(@Param("openDay") String openDay);


    /**
     * 插入分项占用关系
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel(@Param("openDay") String openDay);

    /**
     * 删除合同占用关系信息
     *
     * @param openDay
     * @return
     */
    int deleteContAccRel(@Param("openDay") String openDay);


    /**
     * 插入合同占用关系信息
     *
     * @param openDay
     * @return
     */
    int insertContAccRel(@Param("openDay") String openDay);

    /**
     * truncateLmtContRel01- 22010201	专项消费分期 分项占用关系信息 占用总余额（折人民币）更新
     *
     * @param openDay
     */
    void truncateLmtContRel01(@Param("openDay") String openDay);

    /**
     * insertLmtContRel01- 22010201	专项消费分期 分项占用关系信息 占用总余额（折人民币）更新
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel01(@Param("openDay") String openDay);

    /**
     * 22010201	专项消费分期 分项占用关系信息 占用总余额（折人民币）更新
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel01(@Param("openDay") String openDay);

    /**
     * 22010201	专项消费分期 分项占用关系信息 占用敞口余额（折人民币） 更新
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel02(@Param("openDay") String openDay);

    /**
     * truncateApprLmtSubBasicInfo02A-22010201	专项消费分期 批复额度分项基础信息更新占用额
     *
     * @param openDay
     */
    void truncateApprLmtSubBasicInfo02A(@Param("openDay") String openDay);

    /**
     * insertApprLmtSubBasicInfo02A-22010201	专项消费分期 批复额度分项基础信息更新占用额
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo02A(@Param("openDay") String openDay);

    /**
     * updateApprLmtSubBasicInfo02A-22010201	专项消费分期 批复额度分项基础信息更新占用额
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo02A(@Param("openDay") String openDay);

    /**
     * 更新额度分项的 敞口已用总额
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo03(@Param("openDay") String openDay);


    /**
     * truncateApprLmtSubBasicInfo04-更新额度分项的LOAN_BALANCE贷款余额
     *
     * @param openDay
     */
    void truncateApprLmtSubBasicInfo04(@Param("openDay") String openDay);

    /**
     * insertApprLmtSubBasicInfo04-更新额度分项的LOAN_BALANCE贷款余额
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo04(@Param("openDay") String openDay);

    /**
     * updateApprLmtSubBasicInfo04-更新额度分项的LOAN_BALANCE贷款余额
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo04(@Param("openDay") String openDay);


    /**
     * truncateApprLmtSubBasicInfo05-更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额
     *
     * @param openDay
     */
    void truncateApprLmtSubBasicInfo05(@Param("openDay") String openDay);

    /**
     * insertApprLmtSubBasicInfo05-更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo05(@Param("openDay") String openDay);

    /**
     * updateApprLmtSubBasicInfo05-更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo05(@Param("openDay") String openDay);


    /**
     * updateApprLmtSubBasicInfo06-更新22010201	专项消费分期 AVL_OUTSTND_AMT可出账金额
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo06(@Param("openDay") String openDay);

    /**
     * deleteApprStrMtableInfo02-删除授信批复台账表 22010201	专项消费分期
     *
     * @param openDay
     */
    void deleteApprStrMtableInfo02(@Param("openDay") String openDay);

    /**
     * insertApprStrMtableInfo02-重新插入批复主信息 22010201	专项消费分期
     *
     * @param openDay
     * @return
     */
    int insertApprStrMtableInfo02(@Param("openDay") String openDay);

}
