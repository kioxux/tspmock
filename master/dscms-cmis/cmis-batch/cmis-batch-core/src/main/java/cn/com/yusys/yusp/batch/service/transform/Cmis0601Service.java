package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0601Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0601</br>
 * 任务名称：加工任务-业务处理-生成短信  </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0601Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0601Service.class);

    @Autowired
    private Cmis0601Mapper cmis0601Mapper;

    /**
     * 插入短信通知表输入表
     *
     * @param openDay
     */
    public void cmis0601InsertSmsManageInfo01(String openDay) {
        logger.info("场景为日终生成，发送对象为客户手机，全行范围内的按揭贷款,还款方式为等额本息或者等额本金。 提前5天发送短信提醒   ,插入短信通知表输入表开始,请求参数为:[{}]", openDay);
        int insertSmsManageInfo01 = cmis0601Mapper.insertSmsManageInfo01(openDay);
        logger.info("场景为日终生成，发送对象为客户手机，全行范围内的按揭贷款,还款方式为等额本息或者等额本金。 提前5天发送短信提醒 ,插入短信通知表输入表结束,返回参数为:[{}]", insertSmsManageInfo01);
    }

    /**
     * 插入短信通知表输入表
     *
     * @param openDay
     */
    public void cmis0601InsertSmsManageInfo02(String openDay) {
        logger.info("场景为日终批量生成,发送对象为客户手机 ，  非按揭   消费类贷款（不包含小贷），[按226比例还款][按月还息,按年还本][按期付息到期还本][利随本清]的贷款到期前5,15,30天分别给客户  开始,请求参数为:[{}]", openDay);
        int insertSmsManageInfo02 = cmis0601Mapper.insertSmsManageInfo02(openDay);
        logger.info("场景为日终批量生成,发送对象为客户手机 ， 非按揭   消费类贷款（不包含小贷），[按226比例还款][按月还息,按年还本][按期付息到期还本][利随本清]的贷款到期前5,15,30天分别给客户   结束,返回参数为:[{}]", insertSmsManageInfo02);
    }


    /**
     * 插入短信通知表输入表
     *
     * @param openDay
     */
    public void cmis0601InsertSmsManageInfo03(String openDay) {
        logger.info("场景为日终批量生成,发送对象为客户手机， 非按揭   小微经营性小微消费性 小贷提前5天15天发送短信提醒  开始,请求参数为:[{}]", openDay);
        int insertSmsManageInfo03 = cmis0601Mapper.insertSmsManageInfo03(openDay);
        logger.info("场景为日终批量生成,发送对象为客户手机， 非按揭   小微经营性小微消费性 小贷提前5天15天发送短信提醒  结束,返回参数为:[{}]", insertSmsManageInfo03);
    }


    /**
     * 插入短信通知表输入表
     *
     * @param openDay
     */
    public void cmis0601InsertSmsManageInfo04(String openDay) {
        logger.info("场景为日终批量生成,发送对象为客户手机， 非按揭   个人经营性贷款（不包含小贷），到期提醒  每期还款日及贷款到期日前5天、15天  开始,请求参数为:[{}]", openDay);
        int insertSmsManageInfo04 = cmis0601Mapper.insertSmsManageInfo04(openDay);
        logger.info("场景为日终批量生成,发送对象为客户手机， 非按揭   个人经营性贷款（不包含小贷），到期提醒  每期还款日及贷款到期日前5天、15天  结束,返回参数为:[{}]", insertSmsManageInfo04);


    }


    /**
     * 插入短信通知表输入表
     *
     * @param openDay
     */
    public void cmis0601InsertSmsManageInfo05(String openDay) {
        logger.info("场景为日终批量生成,发送对象为客户经理手机， 逾期情况：      全行范围内的按揭贷款,还款方式为等额本息或者等额本金。  逾期后一天； 每月25日至月末每天  开始,请求参数为:[{}]", openDay);
        int insertSmsManageInfo05 = cmis0601Mapper.insertSmsManageInfo05(openDay);
        logger.info("场景为日终批量生成,发送对象为客户经理手机， 逾期情况：      全行范围内的按揭贷款,还款方式为等额本息或者等额本金。  逾期后一天； 每月25日至月末每天   结束,返回参数为:[{}]", insertSmsManageInfo05);

    }


    /**
     * 插入短信通知表输入表
     *
     * @param openDay
     */
    public void cmis0601InsertSmsManageInfoAccLoan(String openDay) {
        logger.info("场景为日终批量生成,发送对象为客户经理手机，发生逾期 ：  小贷逾期催收     非按揭 贷款到期日   逾期后一天； 每月25日至月末每天 开始,请求参数为:[{}]", openDay);
        int insertSmsManageInfoAccLoan = cmis0601Mapper.insertSmsManageInfoAccLoan(openDay);
        logger.info("场景为日终批量生成,发送对象为客户经理手机，发生逾期 ：  小贷逾期催收     非按揭 贷款到期日   逾期后一天； 每月25日至月末每天 结束,返回参数为:[{}]", insertSmsManageInfoAccLoan);


    }


    /**
     * 插入短信通知表输入表
     *
     * @param openDay
     */
    public void cmis0601InsertSmsManageInfoLmtReplyAcc(String openDay) {
        logger.info("场景为日终批量生成,发送对象为客户经理手机， 发生逾期 ：  非按揭 贷款到期日  消费类贷款（不包含小贷），[按226比例还款][按月还息,按年还本][按期付息到期还本][利随本清]的贷款到期 逾期后一天； 每月25日至月末每天 开始,请求参数为:[{}]", openDay);
        int insertSmsManageInfoLmtReplyAcc = cmis0601Mapper.insertSmsManageInfoLmtReplyAcc(openDay);
        logger.info("场景为日终批量生成,发送对象为客户经理手机，发生逾期 ：  非按揭 贷款到期日  消费类贷款（不包含小贷），[按226比例还款][按月还息,按年还本][按期付息到期还本][利随本清]的贷款到期 逾期后一天； 每月25日至月末每天   结束,返回参数为:[{}]", insertSmsManageInfoLmtReplyAcc);

        // 个人经营性贷款（不包含小贷）-逾期提示	逾期后一天；每月25日至月末每天
        logger.info("场景为日终批量生成,发送对象为客户手机， 发生逾期 ： 非按揭 贷款到期日   个人经营性贷款（不包含小贷），逾期提示 逾期后一天； 每月25日至月末每天   开始,请求参数为:[{}]", openDay);
        int insertSmsManageInfo08 = cmis0601Mapper.insertSmsManageInfo08(openDay);
        logger.info("场景为日终批量生成,发送对象为客户手机，  发生逾期 ： 非按揭 贷款到期日   个人经营性贷款（不包含小贷），逾期提示 逾期后一天； 每月25日至月末每天   结束,返回参数为:[{}]", insertSmsManageInfo08);

        // 贷款到期前30天提醒客户经理
        logger.info("场景为日终批量生成,发送对象为客户经理手机，客户经理消息提醒 贷款到期前30天  开始,请求参数为:[{}]", openDay);
        int insertSmsManageInfo09 = cmis0601Mapper.insertSmsManageInfo09(openDay);
        logger.info("场景为日终批量生成,发送对象为客户经理手机，客户经理消息提醒   贷款到期前30天   结束,返回参数为:[{}]", insertSmsManageInfo09);

        // 授信批复到期前45天提醒客户经理
        logger.info("场景为日终批量生成,发送对象为客户经理手机，授信批复到期，发送规则是授信批复到期前45天   开始,请求参数为:[{}]", openDay);
        int insertSmsManageInfo10 = cmis0601Mapper.insertSmsManageInfo10(openDay);
        logger.info("场景为日终批量生成,发送对象为客户经理手机，授信批复到期，发送规则是授信批复到期前45天   结束,返回参数为:[{}]", insertSmsManageInfo10);

    }

}
