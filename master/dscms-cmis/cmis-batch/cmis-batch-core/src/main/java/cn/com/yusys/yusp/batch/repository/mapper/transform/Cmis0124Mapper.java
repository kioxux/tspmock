package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0124</br>
 * 任务名称：加工任务-业务处理--授信每年复审提醒 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0124Mapper {
    /**
     * 插入消息提示表 ： 授信每年复审提醒
     *
     * @param openDay
     * @return
     */
//    int insertWbMsgNotice(@Param("openDay") String openDay);

    /**
     * 更新批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo(@Param("openDay") String openDay);

    /**
     * 清空当日单一客户授信复审申请表
     *
     * @param openDay
     * @return
     */
    int truncateTmpLmtAppPer(@Param("openDay") String openDay);


    /**
     * 插入当日单一客户授信复审申请表
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAppPer(@Param("openDay") String openDay);

    /**
     * 清空当日集团客户授信复审申请表
     *
     * @param openDay
     * @return
     */
    int truncateTmpLmtAppCorp(@Param("openDay") String openDay);

    /**
     * 插入当日集团客户授信复审申请表
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAppCorp(@Param("openDay") String openDay);

    /**
     * 根据返回结果查询集团授信申请表
     *
     * @param grpSerno
     * @return
     */
    int selectLmtGrpApp(@Param("grpSerno") String grpSerno);
}