package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS020A</br>
 * 任务名称：加工任务-额度处理-担保公司占用关系处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis020AMapper {
    /**
     * 清空加工占用授信表
     */
    void truncateTmpLmtAmt();

    /**
     * 取贷款占用授信的关系表占用余 额复制给担保公司占用关系 ，如果担保合同金额比占用额小，则取担保合同金额
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt(@Param("openDay") String openDay);

    /**
     * 更新占用总余额、占用敞口余额
     *
     * @param openDay
     * @return
     */
    int updateTmpLmtAmt(@Param("openDay") String openDay);
}
