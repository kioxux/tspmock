package cn.com.yusys.yusp.batch.service.bak;

import cn.com.yusys.yusp.batch.repository.mapper.bak.BakMD028Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKMD028</br>
 * 任务名称：批后备份月表日表任务-备份白名单额度信息历史[LMT_WHITE_INFO_HISTORY]  </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
@Service
@Transactional
public class BakMD028Service {
    private static final Logger logger = LoggerFactory.getLogger(BakMD028Service.class);

    @Autowired
    private BakMD028Mapper bakMD028Mapper;

    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 重命名创建和删除当天的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void bakMD028DeleteDLmtWhiteInfoHistory(String openDay) {
        logger.info("计算当天日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -4); // 4天前日期

        logger.info("重命名创建和删除当天的[白名单额度信息历史[TMP_DAF_LMT_WHITE_INFO_HISTORY]]数据开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_lmt","TMP_DAF_LMT_WHITE_INFO_HISTORY");
        logger.info("重命名创建和删除当天的[白名单额度信息历史[TMP_DAF_LMT_WHITE_INFO_HISTORY]]数据结束");
//        logger.info("计算当天日期结束,返回参数为:[{}]", openDay);
//        logger.info("查询待删除当天的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据总次数开始,请求参数为:[{}]", openDay);
//        int queryMD028DeleteOpenDayDLmtWhiteInfoHistoryCounts = bakMD028Mapper.queryMD028DeleteOpenDayDLmtWhiteInfoHistoryCounts(openDay);
//        // 由于TDSQL不支持ceiling函数，此处在java中处理
//        BigDecimal times = new BigDecimal(queryMD028DeleteOpenDayDLmtWhiteInfoHistoryCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
//        queryMD028DeleteOpenDayDLmtWhiteInfoHistoryCounts = times.intValue();
//        logger.info("查询待删除当天的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据总次数结束,返回参数为:[{}]", queryMD028DeleteOpenDayDLmtWhiteInfoHistoryCounts);
//        // 删除当天的数据
//        for (int i = 0; i < queryMD028DeleteOpenDayDLmtWhiteInfoHistoryCounts; i++) {
//            logger.info("第[" + i + "]次删除当天的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据开始,请求参数为:[{}]", openDay);
//            int bakMD028DeleteDLmtWhiteInfoHistory = bakMD028Mapper.bakMD028DeleteDLmtWhiteInfoHistory(openDay);
//            logger.info("第[" + i + "]次删除当天的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据结束,返回参数为:[{}]", bakMD028DeleteDLmtWhiteInfoHistory);
//        }
    }


    /**
     * 删除当月的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据
     *
     * @param openDay
     */
    public void bakMD028DeleteMLmtWhiteInfoHistory(String openDay) {
        logger.info("查询当月的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据开始,请求参数为:[{}]", openDay);
        int queryMD0028DeleteDLmtWhiteInfoHistory = bakMD028Mapper.queryMD0028DeleteDLmtWhiteInfoHistory(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(queryMD0028DeleteDLmtWhiteInfoHistory).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        queryMD0028DeleteDLmtWhiteInfoHistory = times.intValue();
        logger.info("查询当月的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据结束,返回参数为:[{}]", queryMD0028DeleteDLmtWhiteInfoHistory);
        // 删除当月的数据
        for (int i = 0; i < queryMD0028DeleteDLmtWhiteInfoHistory; i++) {
            logger.info("第[" + i + "]次删除当月的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据开始,请求参数为:[{}]", openDay);
            int bakMD028DeleteMLmtWhiteInfoHistory = bakMD028Mapper.bakMD028DeleteMLmtWhiteInfoHistory(openDay);
            logger.info("第[" + i + "]次删除当月的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据结束,返回参数为:[{}]", bakMD028DeleteMLmtWhiteInfoHistory);
        }
    }

    /**
     * 备份当天的数据
     *
     * @param openDay
     */
    public void bakMD028InsertD(String openDay) {
        // 备份当天的数据
        logger.info("备份当天的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakMD028Mapper.insertD(openDay);
        logger.info("备份当天的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据结束,返回参数为:[{}]", insertCurrent);
    }

    /**
     * 备份当月的数据
     *
     * @param openDay
     */
    public void bakMD028InsertM(String openDay) {
        logger.info("备份当月的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakMD028Mapper.insertM(openDay);
        logger.info("备份当月的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据结束,返回参数为:[{}]", insertCurrent);
    }

    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakMEqualsOriginal(String openDay) {
        String nextOpenDay = DateUtils.addDay(openDay, "yyyy-MM-dd", 1); // 切日后营业日期
        logger.info("切日后营业日期为:[{}]", nextOpenDay);
        Date nextOpenDayDate = DateUtils.parseDateByDef(nextOpenDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
        int day = DateUtils.getMonthDay(nextOpenDayDate);// 获取当月中的天
        logger.info("获取当月中的天为:[{}]", day);

        if (day == 1) {
            logger.info("查询当月备份的[分项占用关系信息[TMP_M_LMT_CONT_REL]]数据总条数开始,请求参数为:[{}]", openDay);
            int bakMCounts = bakMD028Mapper.queryBakMCounts(openDay);
            logger.info("查询当月备份的[分项占用关系信息[TMP_M_LMT_CONT_REL]]数据总条数结束,返回参数为:[{}]", bakMCounts);

            logger.info("查询当月的[分项占用关系信息[LMT_CONT_REL]]原表数据总条数开始,请求参数为:[{}]", openDay);
            int originalCounts = bakMD028Mapper.queryOriginalCounts(openDay);
            logger.info("查询当月的[分项占用关系信息[LMT_CONT_REL]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

            if (bakMCounts != originalCounts) {
                logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakMCounts, originalCounts);
                throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakMCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
            } else {
                logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakMCounts, originalCounts);
            }
        }
    }


    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakDEqualsOriginal(String openDay) {
        logger.info("查询当天备份的[白名单额度信息历史[TMP_DAF_LMT_WHITE_INFO_HISTORY]]数据总条数开始,请求参数为:[{}]", openDay);
        int bakDCounts = tableUtilsService.coutBakDafTableLines("cmis_lmt", "TMP_DAF_LMT_WHITE_INFO_HISTORY");
        logger.info("查询当天备份的[白名单额度信息历史[TMP_DAF_LMT_WHITE_INFO_HISTORY]]数据总条数结束,返回参数为:[{}]", bakDCounts);

        logger.info("查询当天的[白名单额度信息历史 [LMT_WHITE_INFO_HISTORY]]原表数据总条数开始,请求参数为:[{}]", openDay);
        int originalCounts = tableUtilsService.countTableLines("cmis_lmt", "LMT_WHITE_INFO_HISTORY");
        logger.info("查询当天的[白名单额度信息历史 [LMT_WHITE_INFO_HISTORY]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

        if (bakDCounts != originalCounts) {
            logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakDCounts, originalCounts);
            throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakDCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
        } else {
            logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakDCounts, originalCounts);
        }
    }
}
