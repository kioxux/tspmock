/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.gjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGjpTfbDldeDlIssMl
 * @类描述: bat_s_gjp_tfb_dlde_dl_iss_ml数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-03 22:11:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_gjp_tfb_dlde_dl_iss_ml")
public class BatSGjpTfbDldeDlIssMl extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 系统删除标识 **/
	@Column(name = "SYS_DEL_FLG", unique = false, nullable = false, length = 1)
	private String sysDelFlg;
	
	/** 创建人 **/
	@Column(name = "SYS_CRT_USER", unique = false, nullable = true, length = 32)
	private String sysCrtUser;
	
	/** 创建日期 **/
	@Column(name = "SYS_CRT_DT", unique = false, nullable = true, length = 20)
	private String sysCrtDt;
	
	/** 修改人编号 **/
	@Column(name = "SYS_MODIFY_USER", unique = false, nullable = true, length = 32)
	private String sysModifyUser;
	
	/** 系统实体编号 **/
	@Column(name = "SYS_ENTY_ID", unique = false, nullable = true, length = 32)
	private String sysEntyId;
	
	/** 修改日期 **/
	@Column(name = "SYS_MODIFY_DT", unique = false, nullable = true, length = 20)
	private String sysModifyDt;
	
	/** 到单金额总额 **/
	@Column(name = "AB_AMT_TTL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal abAmtTtl;
	
	/** 到单余额 **/
	@Column(name = "AB_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal abBal;
	
	/** 到单标识 **/
	@Column(name = "AB_FLG", unique = false, nullable = true, length = 1)
	private String abFlg;
	
	/** 到单次数 **/
	@Column(name = "AB_TIMES", unique = false, nullable = true, length = 10)
	private Integer abTimes;
	
	/** 通知行地址 **/
	@Column(name = "ADV_BK_ADDR", unique = false, nullable = true, length = 140)
	private String advBkAddr;
	
	/** 通知行BIC **/
	@Column(name = "ADV_BK_BIC", unique = false, nullable = true, length = 16)
	private String advBkBic;
	
	/** 通知行ID **/
	@Column(name = "ADV_BK_ID", unique = false, nullable = true, length = 16)
	private String advBkId;
	
	/** 通知行名称 **/
	@Column(name = "ADV_BK_NM", unique = false, nullable = true, length = 140)
	private String advBkNm;
	
	/** 通知行名址 **/
	@Column(name = "ADV_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String advBkNmAddr;
	
	/** 通知行号 **/
	@Column(name = "ADV_BK_NO", unique = false, nullable = true, length = 32)
	private String advBkNo;
	
	/** 通知行经由ID **/
	@Column(name = "ADV_BK_THR_ID", unique = false, nullable = true, length = 16)
	private String advBkThrId;
	
	/** 通知行经由名址 **/
	@Column(name = "ADV_BK_THR_NM_ADDR", unique = false, nullable = true, length = 140)
	private String advBkThrNmAddr;
	
	/** 空运编号 **/
	@Column(name = "AIR_NO", unique = false, nullable = true, length = 32)
	private String airNo;
	
	/** 修改客户 **/
	@Column(name = "AMD_CUST", unique = false, nullable = true, length = 140)
	private String amdCust;
	
	/** 修改标识 **/
	@Column(name = "AMD_FLG", unique = false, nullable = true, length = 1)
	private String amdFlg;
	
	/** 修改次数 **/
	@Column(name = "AMD_TIMES", unique = false, nullable = true, length = 10)
	private Integer amdTimes;
	
	/** 修改次数总数 **/
	@Column(name = "AMD_TIMES_TTL", unique = false, nullable = true, length = 10)
	private Integer amdTimesTtl;
	
	/** 修改类型 **/
	@Column(name = "AMD_TYPE", unique = false, nullable = true, length = 8)
	private String amdType;
	
	/** 申请人账户 **/
	@Column(name = "APP_ACC", unique = false, nullable = true, length = 40)
	private String appAcc;
	
	/** 申请人地址 **/
	@Column(name = "APP_ADDR", unique = false, nullable = true, length = 140)
	private String appAddr;
	
	/** 申请行地址 **/
	@Column(name = "APP_BK_ADDR", unique = false, nullable = true, length = 140)
	private String appBkAddr;
	
	/** 申请行BIC **/
	@Column(name = "APP_BK_BIC", unique = false, nullable = true, length = 16)
	private String appBkBic;
	
	/** 申请行名称 **/
	@Column(name = "APP_BK_NM", unique = false, nullable = true, length = 140)
	private String appBkNm;
	
	/** 申请行名址 **/
	@Column(name = "APP_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String appBkNmAddr;
	
	/** 申请人代码 **/
	@Column(name = "APP_ID", unique = false, nullable = true, length = 32)
	private String appId;
	
	/** 申请人名称 **/
	@Column(name = "APP_NM", unique = false, nullable = true, length = 140)
	private String appNm;
	
	/** 申请人名址 **/
	@Column(name = "APP_NM_ADDR", unique = false, nullable = true, length = 200)
	private String appNmAddr;
	
	/** 申请人邮编 **/
	@Column(name = "APP_POSTCODE", unique = false, nullable = true, length = 32)
	private String appPostcode;
	
	/** 在即期远期标识 **/
	@Column(name = "AT_SIGHT_USANCE_FLG", unique = false, nullable = true, length = 1)
	private String atSightUsanceFlg;
	
	/** 兑付/有效行标识 **/
	@Column(name = "AVL_BK_FLG", unique = false, nullable = true, length = 1)
	private String avlBkFlg;
	
	/** 兑付/有效行ID **/
	@Column(name = "AVL_BK_ID", unique = false, nullable = true, length = 16)
	private String avlBkId;
	
	/** 兑付/有效行名址 **/
	@Column(name = "AVL_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String avlBkNmAddr;
	
	/** 兑付/有效行类型 **/
	@Column(name = "AVL_BK_TYPE", unique = false, nullable = true, length = 8)
	private String avlBkType;
	
	/** 兑付方法 **/
	@Column(name = "AVL_BY", unique = false, nullable = true, length = 8)
	private String avlBy;
	
	/** 兑付/有效类型 **/
	@Column(name = "AVL_TYPE", unique = false, nullable = true, length = 128)
	private String avlType;
	
	/** 提单背书标识 **/
	@Column(name = "BD_FLG", unique = false, nullable = true, length = 1)
	private String bdFlg;
	
	/** 提单背书笔数 **/
	@Column(name = "BD_TIMES", unique = false, nullable = true, length = 10)
	private Integer bdTimes;
	
	/** 受益人账户 **/
	@Column(name = "BEN_ACC", unique = false, nullable = true, length = 40)
	private String benAcc;
	
	/** 受益人地址 **/
	@Column(name = "BEN_ADDR", unique = false, nullable = true, length = 140)
	private String benAddr;
	
	/** 受益行地址 **/
	@Column(name = "BEN_BK_ADDR", unique = false, nullable = true, length = 140)
	private String benBkAddr;
	
	/** 收款行(58a) **/
	@Column(name = "BEN_BK_BIC", unique = false, nullable = true, length = 16)
	private String benBkBic;
	
	/** 受益行名称 **/
	@Column(name = "BEN_BK_NM", unique = false, nullable = true, length = 140)
	private String benBkNm;
	
	/** 收款行名址 **/
	@Column(name = "BEN_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String benBkNmAddr;
	
	/** 受益人所在国别/地区 **/
	@Column(name = "BEN_CNT", unique = false, nullable = true, length = 3)
	private String benCnt;
	
	/** 国别/地区 **/
	@Column(name = "BEN_CNT_CDE", unique = false, nullable = true, length = 3)
	private String benCntCde;
	
	/** 受益人ID **/
	@Column(name = "BEN_ID", unique = false, nullable = true, length = 32)
	private String benId;
	
	/** 受益人名称 **/
	@Column(name = "BEN_NM", unique = false, nullable = true, length = 140)
	private String benNm;
	
	/** 受益人名址 **/
	@Column(name = "BEN_NM_ADDR", unique = false, nullable = true, length = 140)
	private String benNmAddr;
	
	/** 受益人邮编 **/
	@Column(name = "BEN_POSTCODE", unique = false, nullable = true, length = 32)
	private String benPostcode;
	
	/** 业务归属 **/
	@Column(name = "BIZ_BR_CDE", unique = false, nullable = true, length = 8)
	private String bizBrCde;
	
	/** 撤销日期 **/
	@Column(name = "CAC_DT", unique = false, nullable = true, length = 8)
	private String cacDt;
	
	/** 电提不符点通知标识 **/
	@Column(name = "CD_ADV_FLG", unique = false, nullable = true, length = 1)
	private String cdAdvFlg;
	
	/** 清算标识 **/
	@Column(name = "CLS_FLG", unique = false, nullable = true, length = 8)
	private String clsFlg;
	
	/** 信贷金额 **/
	@Column(name = "CMS_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal cmsAmt;
	
	/** 信贷申请人名称 **/
	@Column(name = "CMS_APP_NM", unique = false, nullable = true, length = 64)
	private String cmsAppNm;
	
	/** 信贷系统业务类型 **/
	@Column(name = "CMS_BIZ_TYPE", unique = false, nullable = true, length = 10)
	private String cmsBizType;
	
	/** 信贷币种 **/
	@Column(name = "CMS_CCY", unique = false, nullable = true, length = 3)
	private String cmsCcy;
	
	/** 信贷系统合同号 **/
	@Column(name = "CMS_CNTRCT_NO", unique = false, nullable = true, length = 32)
	private String cmsCntrctNo;
	
	/** 信贷系统结束日期 **/
	@Column(name = "CMS_END_DT", unique = false, nullable = true, length = 8)
	private String cmsEndDt;
	
	/** 信贷系统编号 **/
	@Column(name = "CMS_NO", unique = false, nullable = true, length = 32)
	private String cmsNo;
	
	/** 信贷开始日期 **/
	@Column(name = "CMS_START_DT", unique = false, nullable = true, length = 8)
	private String cmsStartDt;
	
	/** 保兑/确认指示 **/
	@Column(name = "CNF_INS", unique = false, nullable = true, length = 8)
	private String cnfIns;
	
	/** 合同金额 **/
	@Column(name = "CNTRCT_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal cntrctAmt;
	
	/** 合同币种 **/
	@Column(name = "CNTRCT_CCY", unique = false, nullable = true, length = 3)
	private String cntrctCcy;
	
	/** 合同号 **/
	@Column(name = "CNTRCT_NO", unique = false, nullable = true, length = 100)
	private String cntrctNo;
	
	/** 跨境人民币标识 **/
	@Column(name = "CROSS_RMB_FLG", unique = false, nullable = true, length = 1)
	private String crossRmbFlg;
	
	/** 延迟支付条款 **/
	@Column(name = "DEF_PAY_CLAUSE", unique = false, nullable = true, length = 256)
	private String defPayClause;
	
	/** 保证金金额 **/
	@Column(name = "DEPOSIT_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal depositAmt;
	
	/** 保证金币种 **/
	@Column(name = "DEPOSIT_CCY", unique = false, nullable = true, length = 3)
	private String depositCcy;
	
	/** 保证金比例 **/
	@Column(name = "DEPOSIT_PCT", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal depositPct;
	
	/** 开立方式 **/
	@Column(name = "DL_ISS_TYPE", unique = false, nullable = true, length = 8)
	private String dlIssType;
	
	/** 文档副本标识 **/
	@Column(name = "DOC_COPY_FLG", unique = false, nullable = true, length = 1)
	private String docCopyFlg;
	
	/** 汇票金额比例 **/
	@Column(name = "DRAFT_AMT_PCT", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal draftAmtPct;
	
	/** 汇票描述 **/
	@Column(name = "DRAFT_DESC", unique = false, nullable = true, length = 256)
	private String draftDesc;
	
	/** 缮制汇票 **/
	@Column(name = "DRAFT_FLG", unique = false, nullable = true, length = 1)
	private String draftFlg;
	
	/** 汇票类型 **/
	@Column(name = "DRAFT_TYPE", unique = false, nullable = true, length = 8)
	private String draftType;
	
	/** 受票人BIC **/
	@Column(name = "DWE_BIC", unique = false, nullable = true, length = 16)
	private String dweBic;
	
	/** 付款人名址 **/
	@Column(name = "DWE_NM_ADDR", unique = false, nullable = true, length = 140)
	private String dweNmAddr;
	
	/** 传真 **/
	@Column(name = "FAX", unique = false, nullable = true, length = 64)
	private String fax;
	
	/** 货物名称 **/
	@Column(name = "GDS", unique = false, nullable = true, length = 128)
	private String gds;
	
	/** H.S.CODE **/
	@Column(name = "GDS_ID", unique = false, nullable = true, length = 128)
	private String gdsId;
	
	/** 开立行地址 **/
	@Column(name = "ISS_BK_ADDR", unique = false, nullable = true, length = 140)
	private String issBkAddr;
	
	/** 开立行BIC **/
	@Column(name = "ISS_BK_BIC", unique = false, nullable = true, length = 16)
	private String issBkBic;
	
	/** 开立行名称 **/
	@Column(name = "ISS_BK_NM", unique = false, nullable = true, length = 140)
	private String issBkNm;
	
	/** 开立行名址 **/
	@Column(name = "ISS_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String issBkNmAddr;
	
	/** 开立行编号 **/
	@Column(name = "ISS_BK_NO", unique = false, nullable = true, length = 32)
	private String issBkNo;
	
	/** 开立Swift 72域 **/
	@Column(name = "ISS_SWF_72", unique = false, nullable = true, length = 256)
	private String issSwf72;
	
	/** 核心号码 **/
	@Column(name = "KERNEL_NO", unique = false, nullable = true, length = 32)
	private String kernelNo;
	
	/** 客户组织机构代码 **/
	@Column(name = "KERNEL_ORG_CODE", unique = false, nullable = true, length = 32)
	private String kernelOrgCode;
	
	/** 最近到单标识 **/
	@Column(name = "LAST_AB_FLG", unique = false, nullable = true, length = 1)
	private String lastAbFlg;
	
	/** 最近装船、运输日期 **/
	@Column(name = "LAST_SHPMT_DT", unique = false, nullable = true, length = 8)
	private String lastShpmtDt;
	
	/** 信用证号 **/
	@Column(name = "LC_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lcAmt;
	
	/** 信用证金额通知 **/
	@Column(name = "LC_AMT_ADT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lcAmtAdt;
	
	/** 信用证金额最大值 **/
	@Column(name = "LC_AMT_MAX", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lcAmtMax;
	
	/** 信用证金额 **/
	@Column(name = "LC_CCY", unique = false, nullable = true, length = 3)
	private String lcCcy;
	
	/** 信用证闭卷/关闭日期 **/
	@Column(name = "LC_CLS_DT", unique = false, nullable = true, length = 8)
	private String lcClsDt;
	
	/** 信用证复制保兑/确认 **/
	@Column(name = "LC_COPY_CNF", unique = false, nullable = true, length = 1)
	private String lcCopyCnf;
	
	/** 信用证日期 **/
	@Column(name = "LC_DT", unique = false, nullable = true, length = 8)
	private String lcDt;
	
	/** 信用证过期日期 **/
	@Column(name = "LC_EXPIRY_DT", unique = false, nullable = true, length = 8)
	private String lcExpiryDt;
	
	/** 信用证有效地点 **/
	@Column(name = "LC_EXPIRY_PLACE", unique = false, nullable = true, length = 256)
	private String lcExpiryPlace;
	
	/** 信用证标识 **/
	@Column(name = "LC_FLG", unique = false, nullable = true, length = 8)
	private String lcFlg;
	
	/** 信用证编号 **/
	@Column(name = "LC_NO", unique = false, nullable = true, length = 32)
	private String lcNo;
	
	/** 是FALSE密押关系 **/
	@Column(name = "LC_SIGNK_FLG", unique = false, nullable = true, length = 1)
	private String lcSignkFlg;
	
	/** 信用证类型 **/
	@Column(name = "LC_TYPE", unique = false, nullable = true, length = 8)
	private String lcType;
	
	/** 快递编号 **/
	@Column(name = "MAIL_NO", unique = false, nullable = true, length = 32)
	private String mailNo;
	
	/** 快邮方式 **/
	@Column(name = "MAIL_TYPE", unique = false, nullable = true, length = 8)
	private String mailType;
	
	/** 保证金比例 **/
	@Column(name = "MGN_PCT", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal mgnPct;
	
	/** 混合付款条款 **/
	@Column(name = "MIX_PAY_CLAUSE", unique = false, nullable = true, length = 256)
	private String mixPayClause;
	
	/** 下浮溢短装 **/
	@Column(name = "NEG_TLRNC", unique = false, nullable = true, length = 8)
	private java.math.BigDecimal negTlrnc;
	
	/** 他行BIC **/
	@Column(name = "OTH_BK_BIC", unique = false, nullable = true, length = 32)
	private String othBkBic;
	
	/** 他行ID **/
	@Column(name = "OTH_BK_ID", unique = false, nullable = true, length = 16)
	private String othBkId;
	
	/** 他行信用证号码 **/
	@Column(name = "OTH_BK_LC_NO", unique = false, nullable = true, length = 32)
	private String othBkLcNo;
	
	/** 他行名址 **/
	@Column(name = "OTH_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String othBkNmAddr;
	
	/** 其他费用 **/
	@Column(name = "OTH_FEE", unique = false, nullable = true, length = 256)
	private String othFee;
	
	/** 付款总金额 **/
	@Column(name = "PAY_AMT_TTL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal payAmtTtl;
	
	/** 信用证余额 **/
	@Column(name = "PAY_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal payBal;
	
	/** 卸货港 **/
	@Column(name = "PORT_DISCHG", unique = false, nullable = true, length = 128)
	private String portDischg;
	
	/** 起运港 **/
	@Column(name = "PORT_LOAD", unique = false, nullable = true, length = 128)
	private String portLoad;
	
	/** 上浮溢短装 **/
	@Column(name = "POS_TLRNC", unique = false, nullable = true, length = 8)
	private java.math.BigDecimal posTlrnc;
	
	/** 交单天数 **/
	@Column(name = "PRESNT_DAYS", unique = false, nullable = true, length = 32)
	private String presntDays;
	
	/** 产品名称 **/
	@Column(name = "PROD_NM", unique = false, nullable = true, length = 8)
	private String prodNm;
	
	/** 撤销确认标识 **/
	@Column(name = "RCAN_FLG", unique = false, nullable = true, length = 8)
	private String rcanFlg;
	
	/** 偿付行账户 **/
	@Column(name = "REIM_BK_ACC", unique = false, nullable = true, length = 40)
	private String reimBkAcc;
	
	/** 偿付行地址 **/
	@Column(name = "REIM_BK_ADDR", unique = false, nullable = true, length = 140)
	private String reimBkAddr;
	
	/** 偿付行BIC **/
	@Column(name = "REIM_BK_BIC", unique = false, nullable = true, length = 16)
	private String reimBkBic;
	
	/** 偿付行费用方式 **/
	@Column(name = "REIM_BK_FEE_BY", unique = false, nullable = true, length = 8)
	private String reimBkFeeBy;
	
	/** 偿付行标识 **/
	@Column(name = "REIM_BK_FLG", unique = false, nullable = true, length = 1)
	private String reimBkFlg;
	
	/** 偿付行ID **/
	@Column(name = "REIM_BK_ID", unique = false, nullable = true, length = 16)
	private String reimBkId;
	
	/** 偿付行名称 **/
	@Column(name = "REIM_BK_NM", unique = false, nullable = true, length = 140)
	private String reimBkNm;
	
	/** 偿付行名址 **/
	@Column(name = "REIM_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String reimBkNmAddr;
	
	/** 偿付行号 **/
	@Column(name = "REIM_BK_NO", unique = false, nullable = true, length = 32)
	private String reimBkNo;
	
	/** 偿付SWIFT72域 **/
	@Column(name = "REIM_SWF_72", unique = false, nullable = true, length = 256)
	private String reimSwf72;
	
	/** 特殊经济区域企业标识 **/
	@Column(name = "SA_FLG", unique = false, nullable = true, length = 1)
	private String saFlg;
	
	/** 提货担保总金额 **/
	@Column(name = "SG_AMT_TTL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal sgAmtTtl;
	
	/** 提货担保闭卷/关闭标识 **/
	@Column(name = "SG_CLS_FLG", unique = false, nullable = true, length = 1)
	private String sgClsFlg;
	
	/** 提货担保次数 **/
	@Column(name = "SG_TIMES", unique = false, nullable = true, length = 10)
	private Integer sgTimes;
	
	/** 装船天数 **/
	@Column(name = "SHPMT_DAYS", unique = false, nullable = true, length = 10)
	private Integer shpmtDays;
	
	/** 装船开立日期 **/
	@Column(name = "SHPMT_DT_ISS", unique = false, nullable = true, length = 390)
	private String shpmtDtIss;
	
	/** 附言(72) **/
	@Column(name = "SWF_72", unique = false, nullable = true, length = 1024)
	private String swf72;
	
	/** 付款期限 **/
	@Column(name = "TENOR_DAYS", unique = false, nullable = true, length = 10)
	private Integer tenorDays;
	
	/** 付款期限 **/
	@Column(name = "TENOR_TYPE", unique = false, nullable = true, length = 128)
	private String tenorType;
	
	/** 溢短装点标识 **/
	@Column(name = "TLRNC_POINT_FLG", unique = false, nullable = true, length = 1)
	private String tlrncPointFlg;
	
	/** 交易属性 **/
	@Column(name = "TRADE_PROP", unique = false, nullable = true, length = 8)
	private String tradeProp;
	
	/** 远期即期标识 **/
	@Column(name = "USANCE_AT_SIGHT_FLG", unique = false, nullable = true, length = 1)
	private String usanceAtSightFlg;
	
	/** 远期即期点 **/
	@Column(name = "USANCE_AT_SIGHT_POINT", unique = false, nullable = true, length = 10)
	private Integer usanceAtSightPoint;
	
	/** 证件号码 **/
	@Column(name = "ID_CER_NO", unique = false, nullable = true, length = 32)
	private String idCerNo;
	
	/** 证件类型 **/
	@Column(name = "ID_CER_TYPE", unique = false, nullable = true, length = 8)
	private String idCerType;
	
	/** 系统业务编号 **/
	@Column(name = "SYS_REF_NO", unique = false, nullable = true, length = 32)
	private String sysRefNo;
	
	/** 关联业务编号 **/
	@Column(name = "SYS_REL_NO", unique = false, nullable = true, length = 32)
	private String sysRelNo;
	
	/** 主关联业务编号 **/
	@Column(name = "SYS_MAIN_REL_NO", unique = false, nullable = true, length = 32)
	private String sysMainRelNo;
	
	/** FORM_OF_LC **/
	@Column(name = "FORM_OF_LC", unique = false, nullable = true, length = 8)
	private String formOfLc;
	
	/** CLS_TYPE **/
	@Column(name = "CLS_TYPE", unique = false, nullable = true, length = 8)
	private String clsType;
	
	/** 转递行BIC **/
	@Column(name = "TRF_BK_BIC", unique = false, nullable = true, length = 32)
	private String trfBkBic;
	
	/** TRF_BK_ADDR **/
	@Column(name = "TRF_BK_ADDR", unique = false, nullable = true, length = 256)
	private String trfBkAddr;
	
	/** 可转让标识 **/
	@Column(name = "CAC_FLG", unique = false, nullable = true, length = 8)
	private String cacFlg;
	
	/** 是否保兑 **/
	@Column(name = "CNF_IS_OR_NOT", unique = false, nullable = true, length = 32)
	private String cnfIsOrNot;
	
	/** CNF_BIC **/
	@Column(name = "CNF_BIC", unique = false, nullable = true, length = 16)
	private String cnfBic;
	
	/** 保兑行名称 **/
	@Column(name = "CNF_BK_NM", unique = false, nullable = true, length = 140)
	private String cnfBkNm;
	
	/** TRF_BK **/
	@Column(name = "TRF_BK", unique = false, nullable = true, length = 32)
	private String trfBk;
	
	/** 转让行行号 **/
	@Column(name = "TRF_BK_NO", unique = false, nullable = true, length = 32)
	private String trfBkNo;
	
	/** BP_IS_OR_NOT **/
	@Column(name = "BP_IS_OR_NOT", unique = false, nullable = true, length = 32)
	private String bpIsOrNot;
	
	/** CLS_TIMES **/
	@Column(name = "CLS_TIMES", unique = false, nullable = true, length = 10)
	private Integer clsTimes;
	
	/** 是否代理他行 **/
	@Column(name = "AGT_FLG", unique = false, nullable = true, length = 1)
	private String agtFlg;
	
	/** 是否我行开证 **/
	@Column(name = "LC_ISS_SELF_FLG", unique = false, nullable = true, length = 1)
	private String lcIssSelfFlg;
	
	/** 委托行行号 **/
	@Column(name = "CONSN_BK_NO", unique = false, nullable = true, length = 32)
	private String consnBkNo;
	
	/** 委托行名称 **/
	@Column(name = "CONSN_BK_NM", unique = false, nullable = true, length = 140)
	private String consnBkNm;
	
	/** 通知我行机构号 **/
	@Column(name = "ADV_ORG", unique = false, nullable = true, length = 32)
	private String advOrg;
	
	/** 保兑行行号 **/
	@Column(name = "CNF_NO", unique = false, nullable = true, length = 32)
	private String cnfNo;
	
	/** 快邮公司 **/
	@Column(name = "MAIL_CO_1", unique = false, nullable = true, length = 8)
	private String mailCo1;
	
	/** 一次寄单编号 **/
	@Column(name = "MAIL_NO_1", unique = false, nullable = true, length = 32)
	private String mailNo1;
	
	/** 即远期标识 **/
	@Column(name = "USANCE_AMD_TYPE", unique = false, nullable = true, length = 8)
	private String usanceAmdType;
	
	/** 影像附件标识 **/
	@Column(name = "CNTRCT_IMG_FLG", unique = false, nullable = true, length = 8)
	private String cntrctImgFlg;
	
	/** 印押核符 **/
	@Column(name = "SIGNK_FLG", unique = false, nullable = true, length = 1)
	private String signkFlg;
	
	/** 我行不予通知 **/
	@Column(name = "NO_ADV_FLG", unique = false, nullable = true, length = 1)
	private String noAdvFlg;
	
	/** 有效地点 **/
	@Column(name = "EXPIRY_PLACE", unique = false, nullable = true, length = 32)
	private String expiryPlace;
	
	/** 期限类型 **/
	@Column(name = "TRAN_TYPE", unique = false, nullable = true, length = 140)
	private String tranType;
	
	/** 系统内开证 **/
	@Column(name = "INNER_FLG", unique = false, nullable = true, length = 1)
	private String innerFlg;
	
	/** 代开行行号 **/
	@Column(name = "OTH_BK_NO", unique = false, nullable = true, length = 32)
	private String othBkNo;
	
	/** 代开行名称 **/
	@Column(name = "OTH_BK_NM", unique = false, nullable = true, length = 140)
	private String othBkNm;
	
	/** 申请人账号开户行行号 **/
	@Column(name = "APP_BK_NO", unique = false, nullable = true, length = 140)
	private String appBkNo;
	
	/** 申请人银行邮编 **/
	@Column(name = "APP_BK_POSTCODE", unique = false, nullable = true, length = 32)
	private String appBkPostcode;
	
	/** 申请人银行电话 **/
	@Column(name = "APP_BK_TEL", unique = false, nullable = true, length = 64)
	private String appBkTel;
	
	/** 开证行电话 **/
	@Column(name = "ISS_BK_TEL", unique = false, nullable = true, length = 64)
	private String issBkTel;
	
	/** 开证行邮编 **/
	@Column(name = "ISS_BK_POSTCODE", unique = false, nullable = true, length = 32)
	private String issBkPostcode;
	
	/** 通知行电话 **/
	@Column(name = "ADV_BK_TEL", unique = false, nullable = true, length = 64)
	private String advBkTel;
	
	/** 通知行邮编 **/
	@Column(name = "ADV_BK_POSTCODE", unique = false, nullable = true, length = 32)
	private String advBkPostcode;
	
	/** 转让行名称 **/
	@Column(name = "TRF_BK_NM", unique = false, nullable = true, length = 140)
	private String trfBkNm;
	
	/** 保兑行名址 **/
	@Column(name = "CNF_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String cnfBkNmAddr;
	
	/** 保兑行BIC **/
	@Column(name = "CNF_BK_BIC", unique = false, nullable = true, length = 16)
	private String cnfBkBic;
	
	/** 申请人电话 **/
	@Column(name = "APP_TEL", unique = false, nullable = true, length = 64)
	private String appTel;
	
	/** 受益人电话 **/
	@Column(name = "BEN_TEL", unique = false, nullable = true, length = 64)
	private String benTel;
	
	/** 受益行行号 **/
	@Column(name = "BEN_BK_NO", unique = false, nullable = true, length = 100)
	private String benBkNo;
	
	/** 业务类型 **/
	@Column(name = "TRADE_TYPE", unique = false, nullable = true, length = 128)
	private String tradeType;
	
	/** 服务提供地点 **/
	@Column(name = "SER_PLACE", unique = false, nullable = true, length = 128)
	private String serPlace;
	
	/** 分批装运条款(43P) **/
	@Column(name = "PARTL_SHPMT_FLG", unique = false, nullable = true, length = 256)
	private String partlShpmtFlg;
	
	/** 分期装运/提供服务约定 **/
	@Column(name = "SHPMT_PARTL_DESC", unique = false, nullable = true, length = 64)
	private String shpmtPartlDesc;
	
	/** 其他影像附件标识 **/
	@Column(name = "OTH_IMG_FLG", unique = false, nullable = true, length = 8)
	private String othImgFlg;
	
	/** 运输方式 **/
	@Column(name = "SHPMT_TYPE", unique = false, nullable = true, length = 64)
	private String shpmtType;
	
	/** 分批装运 **/
	@Column(name = "SHPMT_PARTL_FLG", unique = false, nullable = true, length = 4)
	private String shpmtPartlFlg;
	
	/** 允许转运 **/
	@Column(name = "SHPMT_TRM_FLG", unique = false, nullable = true, length = 4)
	private String shpmtTrmFlg;
	
	/** 其他条款 **/
	@Column(name = "OTH_CLAUSE", unique = false, nullable = true, length = 1024)
	private String othClause;
	
	/** 付款指示 **/
	@Column(name = "PAY_INS", unique = false, nullable = true, length = 1024)
	private String payIns;
	
	/** 注销/闭卷类型 **/
	@Column(name = "CLS_BY", unique = false, nullable = true, length = 256)
	private String clsBy;
	
	/** 注销/闭卷原因 **/
	@Column(name = "CLS_CAUSE", unique = false, nullable = true, length = 2048)
	private String clsCause;
	
	/** 通知编号 **/
	@Column(name = "ADV_NO", unique = false, nullable = true, length = 32)
	private String advNo;
	
	/** 货物运输或交货方式/服务方式 **/
	@Column(name = "SER_BY", unique = false, nullable = true, length = 20)
	private String serBy;
	
	/** 付款方式 **/
	@Column(name = "PAY_BY", unique = false, nullable = true, length = 8)
	private String payBy;
	
	/** 交单日期 **/
	@Column(name = "PRESNT_DESCP", unique = false, nullable = true, length = 32)
	private String presntDescp;
	
	/** 修改（更正）总次数 **/
	@Column(name = "ALL_AMD_TIMES", unique = false, nullable = true, length = 10)
	private Integer allAmdTimes;
	
	/** 修改后信用证金额 **/
	@Column(name = "AMD_LC_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal amdLcAmt;
	
	/** KERNEL_TYPE_CODE **/
	@Column(name = "KERNEL_TYPE_CODE", unique = false, nullable = true, length = 32)
	private String kernelTypeCode;
	
	
	/**
	 * @param sysDelFlg
	 */
	public void setSysDelFlg(String sysDelFlg) {
		this.sysDelFlg = sysDelFlg;
	}
	
    /**
     * @return sysDelFlg
     */
	public String getSysDelFlg() {
		return this.sysDelFlg;
	}
	
	/**
	 * @param sysCrtUser
	 */
	public void setSysCrtUser(String sysCrtUser) {
		this.sysCrtUser = sysCrtUser;
	}
	
    /**
     * @return sysCrtUser
     */
	public String getSysCrtUser() {
		return this.sysCrtUser;
	}
	
	/**
	 * @param sysCrtDt
	 */
	public void setSysCrtDt(String sysCrtDt) {
		this.sysCrtDt = sysCrtDt;
	}
	
    /**
     * @return sysCrtDt
     */
	public String getSysCrtDt() {
		return this.sysCrtDt;
	}
	
	/**
	 * @param sysModifyUser
	 */
	public void setSysModifyUser(String sysModifyUser) {
		this.sysModifyUser = sysModifyUser;
	}
	
    /**
     * @return sysModifyUser
     */
	public String getSysModifyUser() {
		return this.sysModifyUser;
	}
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param sysEntyId
	 */
	public void setSysEntyId(String sysEntyId) {
		this.sysEntyId = sysEntyId;
	}
	
    /**
     * @return sysEntyId
     */
	public String getSysEntyId() {
		return this.sysEntyId;
	}
	
	/**
	 * @param sysModifyDt
	 */
	public void setSysModifyDt(String sysModifyDt) {
		this.sysModifyDt = sysModifyDt;
	}
	
    /**
     * @return sysModifyDt
     */
	public String getSysModifyDt() {
		return this.sysModifyDt;
	}
	
	/**
	 * @param abAmtTtl
	 */
	public void setAbAmtTtl(java.math.BigDecimal abAmtTtl) {
		this.abAmtTtl = abAmtTtl;
	}
	
    /**
     * @return abAmtTtl
     */
	public java.math.BigDecimal getAbAmtTtl() {
		return this.abAmtTtl;
	}
	
	/**
	 * @param abBal
	 */
	public void setAbBal(java.math.BigDecimal abBal) {
		this.abBal = abBal;
	}
	
    /**
     * @return abBal
     */
	public java.math.BigDecimal getAbBal() {
		return this.abBal;
	}
	
	/**
	 * @param abFlg
	 */
	public void setAbFlg(String abFlg) {
		this.abFlg = abFlg;
	}
	
    /**
     * @return abFlg
     */
	public String getAbFlg() {
		return this.abFlg;
	}
	
	/**
	 * @param abTimes
	 */
	public void setAbTimes(Integer abTimes) {
		this.abTimes = abTimes;
	}
	
    /**
     * @return abTimes
     */
	public Integer getAbTimes() {
		return this.abTimes;
	}
	
	/**
	 * @param advBkAddr
	 */
	public void setAdvBkAddr(String advBkAddr) {
		this.advBkAddr = advBkAddr;
	}
	
    /**
     * @return advBkAddr
     */
	public String getAdvBkAddr() {
		return this.advBkAddr;
	}
	
	/**
	 * @param advBkBic
	 */
	public void setAdvBkBic(String advBkBic) {
		this.advBkBic = advBkBic;
	}
	
    /**
     * @return advBkBic
     */
	public String getAdvBkBic() {
		return this.advBkBic;
	}
	
	/**
	 * @param advBkId
	 */
	public void setAdvBkId(String advBkId) {
		this.advBkId = advBkId;
	}
	
    /**
     * @return advBkId
     */
	public String getAdvBkId() {
		return this.advBkId;
	}
	
	/**
	 * @param advBkNm
	 */
	public void setAdvBkNm(String advBkNm) {
		this.advBkNm = advBkNm;
	}
	
    /**
     * @return advBkNm
     */
	public String getAdvBkNm() {
		return this.advBkNm;
	}
	
	/**
	 * @param advBkNmAddr
	 */
	public void setAdvBkNmAddr(String advBkNmAddr) {
		this.advBkNmAddr = advBkNmAddr;
	}
	
    /**
     * @return advBkNmAddr
     */
	public String getAdvBkNmAddr() {
		return this.advBkNmAddr;
	}
	
	/**
	 * @param advBkNo
	 */
	public void setAdvBkNo(String advBkNo) {
		this.advBkNo = advBkNo;
	}
	
    /**
     * @return advBkNo
     */
	public String getAdvBkNo() {
		return this.advBkNo;
	}
	
	/**
	 * @param advBkThrId
	 */
	public void setAdvBkThrId(String advBkThrId) {
		this.advBkThrId = advBkThrId;
	}
	
    /**
     * @return advBkThrId
     */
	public String getAdvBkThrId() {
		return this.advBkThrId;
	}
	
	/**
	 * @param advBkThrNmAddr
	 */
	public void setAdvBkThrNmAddr(String advBkThrNmAddr) {
		this.advBkThrNmAddr = advBkThrNmAddr;
	}
	
    /**
     * @return advBkThrNmAddr
     */
	public String getAdvBkThrNmAddr() {
		return this.advBkThrNmAddr;
	}
	
	/**
	 * @param airNo
	 */
	public void setAirNo(String airNo) {
		this.airNo = airNo;
	}
	
    /**
     * @return airNo
     */
	public String getAirNo() {
		return this.airNo;
	}
	
	/**
	 * @param amdCust
	 */
	public void setAmdCust(String amdCust) {
		this.amdCust = amdCust;
	}
	
    /**
     * @return amdCust
     */
	public String getAmdCust() {
		return this.amdCust;
	}
	
	/**
	 * @param amdFlg
	 */
	public void setAmdFlg(String amdFlg) {
		this.amdFlg = amdFlg;
	}
	
    /**
     * @return amdFlg
     */
	public String getAmdFlg() {
		return this.amdFlg;
	}
	
	/**
	 * @param amdTimes
	 */
	public void setAmdTimes(Integer amdTimes) {
		this.amdTimes = amdTimes;
	}
	
    /**
     * @return amdTimes
     */
	public Integer getAmdTimes() {
		return this.amdTimes;
	}
	
	/**
	 * @param amdTimesTtl
	 */
	public void setAmdTimesTtl(Integer amdTimesTtl) {
		this.amdTimesTtl = amdTimesTtl;
	}
	
    /**
     * @return amdTimesTtl
     */
	public Integer getAmdTimesTtl() {
		return this.amdTimesTtl;
	}
	
	/**
	 * @param amdType
	 */
	public void setAmdType(String amdType) {
		this.amdType = amdType;
	}
	
    /**
     * @return amdType
     */
	public String getAmdType() {
		return this.amdType;
	}
	
	/**
	 * @param appAcc
	 */
	public void setAppAcc(String appAcc) {
		this.appAcc = appAcc;
	}
	
    /**
     * @return appAcc
     */
	public String getAppAcc() {
		return this.appAcc;
	}
	
	/**
	 * @param appAddr
	 */
	public void setAppAddr(String appAddr) {
		this.appAddr = appAddr;
	}
	
    /**
     * @return appAddr
     */
	public String getAppAddr() {
		return this.appAddr;
	}
	
	/**
	 * @param appBkAddr
	 */
	public void setAppBkAddr(String appBkAddr) {
		this.appBkAddr = appBkAddr;
	}
	
    /**
     * @return appBkAddr
     */
	public String getAppBkAddr() {
		return this.appBkAddr;
	}
	
	/**
	 * @param appBkBic
	 */
	public void setAppBkBic(String appBkBic) {
		this.appBkBic = appBkBic;
	}
	
    /**
     * @return appBkBic
     */
	public String getAppBkBic() {
		return this.appBkBic;
	}
	
	/**
	 * @param appBkNm
	 */
	public void setAppBkNm(String appBkNm) {
		this.appBkNm = appBkNm;
	}
	
    /**
     * @return appBkNm
     */
	public String getAppBkNm() {
		return this.appBkNm;
	}
	
	/**
	 * @param appBkNmAddr
	 */
	public void setAppBkNmAddr(String appBkNmAddr) {
		this.appBkNmAddr = appBkNmAddr;
	}
	
    /**
     * @return appBkNmAddr
     */
	public String getAppBkNmAddr() {
		return this.appBkNmAddr;
	}
	
	/**
	 * @param appId
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
    /**
     * @return appId
     */
	public String getAppId() {
		return this.appId;
	}
	
	/**
	 * @param appNm
	 */
	public void setAppNm(String appNm) {
		this.appNm = appNm;
	}
	
    /**
     * @return appNm
     */
	public String getAppNm() {
		return this.appNm;
	}
	
	/**
	 * @param appNmAddr
	 */
	public void setAppNmAddr(String appNmAddr) {
		this.appNmAddr = appNmAddr;
	}
	
    /**
     * @return appNmAddr
     */
	public String getAppNmAddr() {
		return this.appNmAddr;
	}
	
	/**
	 * @param appPostcode
	 */
	public void setAppPostcode(String appPostcode) {
		this.appPostcode = appPostcode;
	}
	
    /**
     * @return appPostcode
     */
	public String getAppPostcode() {
		return this.appPostcode;
	}
	
	/**
	 * @param atSightUsanceFlg
	 */
	public void setAtSightUsanceFlg(String atSightUsanceFlg) {
		this.atSightUsanceFlg = atSightUsanceFlg;
	}
	
    /**
     * @return atSightUsanceFlg
     */
	public String getAtSightUsanceFlg() {
		return this.atSightUsanceFlg;
	}
	
	/**
	 * @param avlBkFlg
	 */
	public void setAvlBkFlg(String avlBkFlg) {
		this.avlBkFlg = avlBkFlg;
	}
	
    /**
     * @return avlBkFlg
     */
	public String getAvlBkFlg() {
		return this.avlBkFlg;
	}
	
	/**
	 * @param avlBkId
	 */
	public void setAvlBkId(String avlBkId) {
		this.avlBkId = avlBkId;
	}
	
    /**
     * @return avlBkId
     */
	public String getAvlBkId() {
		return this.avlBkId;
	}
	
	/**
	 * @param avlBkNmAddr
	 */
	public void setAvlBkNmAddr(String avlBkNmAddr) {
		this.avlBkNmAddr = avlBkNmAddr;
	}
	
    /**
     * @return avlBkNmAddr
     */
	public String getAvlBkNmAddr() {
		return this.avlBkNmAddr;
	}
	
	/**
	 * @param avlBkType
	 */
	public void setAvlBkType(String avlBkType) {
		this.avlBkType = avlBkType;
	}
	
    /**
     * @return avlBkType
     */
	public String getAvlBkType() {
		return this.avlBkType;
	}
	
	/**
	 * @param avlBy
	 */
	public void setAvlBy(String avlBy) {
		this.avlBy = avlBy;
	}
	
    /**
     * @return avlBy
     */
	public String getAvlBy() {
		return this.avlBy;
	}
	
	/**
	 * @param avlType
	 */
	public void setAvlType(String avlType) {
		this.avlType = avlType;
	}
	
    /**
     * @return avlType
     */
	public String getAvlType() {
		return this.avlType;
	}
	
	/**
	 * @param bdFlg
	 */
	public void setBdFlg(String bdFlg) {
		this.bdFlg = bdFlg;
	}
	
    /**
     * @return bdFlg
     */
	public String getBdFlg() {
		return this.bdFlg;
	}
	
	/**
	 * @param bdTimes
	 */
	public void setBdTimes(Integer bdTimes) {
		this.bdTimes = bdTimes;
	}
	
    /**
     * @return bdTimes
     */
	public Integer getBdTimes() {
		return this.bdTimes;
	}
	
	/**
	 * @param benAcc
	 */
	public void setBenAcc(String benAcc) {
		this.benAcc = benAcc;
	}
	
    /**
     * @return benAcc
     */
	public String getBenAcc() {
		return this.benAcc;
	}
	
	/**
	 * @param benAddr
	 */
	public void setBenAddr(String benAddr) {
		this.benAddr = benAddr;
	}
	
    /**
     * @return benAddr
     */
	public String getBenAddr() {
		return this.benAddr;
	}
	
	/**
	 * @param benBkAddr
	 */
	public void setBenBkAddr(String benBkAddr) {
		this.benBkAddr = benBkAddr;
	}
	
    /**
     * @return benBkAddr
     */
	public String getBenBkAddr() {
		return this.benBkAddr;
	}
	
	/**
	 * @param benBkBic
	 */
	public void setBenBkBic(String benBkBic) {
		this.benBkBic = benBkBic;
	}
	
    /**
     * @return benBkBic
     */
	public String getBenBkBic() {
		return this.benBkBic;
	}
	
	/**
	 * @param benBkNm
	 */
	public void setBenBkNm(String benBkNm) {
		this.benBkNm = benBkNm;
	}
	
    /**
     * @return benBkNm
     */
	public String getBenBkNm() {
		return this.benBkNm;
	}
	
	/**
	 * @param benBkNmAddr
	 */
	public void setBenBkNmAddr(String benBkNmAddr) {
		this.benBkNmAddr = benBkNmAddr;
	}
	
    /**
     * @return benBkNmAddr
     */
	public String getBenBkNmAddr() {
		return this.benBkNmAddr;
	}
	
	/**
	 * @param benCnt
	 */
	public void setBenCnt(String benCnt) {
		this.benCnt = benCnt;
	}
	
    /**
     * @return benCnt
     */
	public String getBenCnt() {
		return this.benCnt;
	}
	
	/**
	 * @param benCntCde
	 */
	public void setBenCntCde(String benCntCde) {
		this.benCntCde = benCntCde;
	}
	
    /**
     * @return benCntCde
     */
	public String getBenCntCde() {
		return this.benCntCde;
	}
	
	/**
	 * @param benId
	 */
	public void setBenId(String benId) {
		this.benId = benId;
	}
	
    /**
     * @return benId
     */
	public String getBenId() {
		return this.benId;
	}
	
	/**
	 * @param benNm
	 */
	public void setBenNm(String benNm) {
		this.benNm = benNm;
	}
	
    /**
     * @return benNm
     */
	public String getBenNm() {
		return this.benNm;
	}
	
	/**
	 * @param benNmAddr
	 */
	public void setBenNmAddr(String benNmAddr) {
		this.benNmAddr = benNmAddr;
	}
	
    /**
     * @return benNmAddr
     */
	public String getBenNmAddr() {
		return this.benNmAddr;
	}
	
	/**
	 * @param benPostcode
	 */
	public void setBenPostcode(String benPostcode) {
		this.benPostcode = benPostcode;
	}
	
    /**
     * @return benPostcode
     */
	public String getBenPostcode() {
		return this.benPostcode;
	}
	
	/**
	 * @param bizBrCde
	 */
	public void setBizBrCde(String bizBrCde) {
		this.bizBrCde = bizBrCde;
	}
	
    /**
     * @return bizBrCde
     */
	public String getBizBrCde() {
		return this.bizBrCde;
	}
	
	/**
	 * @param cacDt
	 */
	public void setCacDt(String cacDt) {
		this.cacDt = cacDt;
	}
	
    /**
     * @return cacDt
     */
	public String getCacDt() {
		return this.cacDt;
	}
	
	/**
	 * @param cdAdvFlg
	 */
	public void setCdAdvFlg(String cdAdvFlg) {
		this.cdAdvFlg = cdAdvFlg;
	}
	
    /**
     * @return cdAdvFlg
     */
	public String getCdAdvFlg() {
		return this.cdAdvFlg;
	}
	
	/**
	 * @param clsFlg
	 */
	public void setClsFlg(String clsFlg) {
		this.clsFlg = clsFlg;
	}
	
    /**
     * @return clsFlg
     */
	public String getClsFlg() {
		return this.clsFlg;
	}
	
	/**
	 * @param cmsAmt
	 */
	public void setCmsAmt(java.math.BigDecimal cmsAmt) {
		this.cmsAmt = cmsAmt;
	}
	
    /**
     * @return cmsAmt
     */
	public java.math.BigDecimal getCmsAmt() {
		return this.cmsAmt;
	}
	
	/**
	 * @param cmsAppNm
	 */
	public void setCmsAppNm(String cmsAppNm) {
		this.cmsAppNm = cmsAppNm;
	}
	
    /**
     * @return cmsAppNm
     */
	public String getCmsAppNm() {
		return this.cmsAppNm;
	}
	
	/**
	 * @param cmsBizType
	 */
	public void setCmsBizType(String cmsBizType) {
		this.cmsBizType = cmsBizType;
	}
	
    /**
     * @return cmsBizType
     */
	public String getCmsBizType() {
		return this.cmsBizType;
	}
	
	/**
	 * @param cmsCcy
	 */
	public void setCmsCcy(String cmsCcy) {
		this.cmsCcy = cmsCcy;
	}
	
    /**
     * @return cmsCcy
     */
	public String getCmsCcy() {
		return this.cmsCcy;
	}
	
	/**
	 * @param cmsCntrctNo
	 */
	public void setCmsCntrctNo(String cmsCntrctNo) {
		this.cmsCntrctNo = cmsCntrctNo;
	}
	
    /**
     * @return cmsCntrctNo
     */
	public String getCmsCntrctNo() {
		return this.cmsCntrctNo;
	}
	
	/**
	 * @param cmsEndDt
	 */
	public void setCmsEndDt(String cmsEndDt) {
		this.cmsEndDt = cmsEndDt;
	}
	
    /**
     * @return cmsEndDt
     */
	public String getCmsEndDt() {
		return this.cmsEndDt;
	}
	
	/**
	 * @param cmsNo
	 */
	public void setCmsNo(String cmsNo) {
		this.cmsNo = cmsNo;
	}
	
    /**
     * @return cmsNo
     */
	public String getCmsNo() {
		return this.cmsNo;
	}
	
	/**
	 * @param cmsStartDt
	 */
	public void setCmsStartDt(String cmsStartDt) {
		this.cmsStartDt = cmsStartDt;
	}
	
    /**
     * @return cmsStartDt
     */
	public String getCmsStartDt() {
		return this.cmsStartDt;
	}
	
	/**
	 * @param cnfIns
	 */
	public void setCnfIns(String cnfIns) {
		this.cnfIns = cnfIns;
	}
	
    /**
     * @return cnfIns
     */
	public String getCnfIns() {
		return this.cnfIns;
	}
	
	/**
	 * @param cntrctAmt
	 */
	public void setCntrctAmt(java.math.BigDecimal cntrctAmt) {
		this.cntrctAmt = cntrctAmt;
	}
	
    /**
     * @return cntrctAmt
     */
	public java.math.BigDecimal getCntrctAmt() {
		return this.cntrctAmt;
	}
	
	/**
	 * @param cntrctCcy
	 */
	public void setCntrctCcy(String cntrctCcy) {
		this.cntrctCcy = cntrctCcy;
	}
	
    /**
     * @return cntrctCcy
     */
	public String getCntrctCcy() {
		return this.cntrctCcy;
	}
	
	/**
	 * @param cntrctNo
	 */
	public void setCntrctNo(String cntrctNo) {
		this.cntrctNo = cntrctNo;
	}
	
    /**
     * @return cntrctNo
     */
	public String getCntrctNo() {
		return this.cntrctNo;
	}
	
	/**
	 * @param crossRmbFlg
	 */
	public void setCrossRmbFlg(String crossRmbFlg) {
		this.crossRmbFlg = crossRmbFlg;
	}
	
    /**
     * @return crossRmbFlg
     */
	public String getCrossRmbFlg() {
		return this.crossRmbFlg;
	}
	
	/**
	 * @param defPayClause
	 */
	public void setDefPayClause(String defPayClause) {
		this.defPayClause = defPayClause;
	}
	
    /**
     * @return defPayClause
     */
	public String getDefPayClause() {
		return this.defPayClause;
	}
	
	/**
	 * @param depositAmt
	 */
	public void setDepositAmt(java.math.BigDecimal depositAmt) {
		this.depositAmt = depositAmt;
	}
	
    /**
     * @return depositAmt
     */
	public java.math.BigDecimal getDepositAmt() {
		return this.depositAmt;
	}
	
	/**
	 * @param depositCcy
	 */
	public void setDepositCcy(String depositCcy) {
		this.depositCcy = depositCcy;
	}
	
    /**
     * @return depositCcy
     */
	public String getDepositCcy() {
		return this.depositCcy;
	}
	
	/**
	 * @param depositPct
	 */
	public void setDepositPct(java.math.BigDecimal depositPct) {
		this.depositPct = depositPct;
	}
	
    /**
     * @return depositPct
     */
	public java.math.BigDecimal getDepositPct() {
		return this.depositPct;
	}
	
	/**
	 * @param dlIssType
	 */
	public void setDlIssType(String dlIssType) {
		this.dlIssType = dlIssType;
	}
	
    /**
     * @return dlIssType
     */
	public String getDlIssType() {
		return this.dlIssType;
	}
	
	/**
	 * @param docCopyFlg
	 */
	public void setDocCopyFlg(String docCopyFlg) {
		this.docCopyFlg = docCopyFlg;
	}
	
    /**
     * @return docCopyFlg
     */
	public String getDocCopyFlg() {
		return this.docCopyFlg;
	}
	
	/**
	 * @param draftAmtPct
	 */
	public void setDraftAmtPct(java.math.BigDecimal draftAmtPct) {
		this.draftAmtPct = draftAmtPct;
	}
	
    /**
     * @return draftAmtPct
     */
	public java.math.BigDecimal getDraftAmtPct() {
		return this.draftAmtPct;
	}
	
	/**
	 * @param draftDesc
	 */
	public void setDraftDesc(String draftDesc) {
		this.draftDesc = draftDesc;
	}
	
    /**
     * @return draftDesc
     */
	public String getDraftDesc() {
		return this.draftDesc;
	}
	
	/**
	 * @param draftFlg
	 */
	public void setDraftFlg(String draftFlg) {
		this.draftFlg = draftFlg;
	}
	
    /**
     * @return draftFlg
     */
	public String getDraftFlg() {
		return this.draftFlg;
	}
	
	/**
	 * @param draftType
	 */
	public void setDraftType(String draftType) {
		this.draftType = draftType;
	}
	
    /**
     * @return draftType
     */
	public String getDraftType() {
		return this.draftType;
	}
	
	/**
	 * @param dweBic
	 */
	public void setDweBic(String dweBic) {
		this.dweBic = dweBic;
	}
	
    /**
     * @return dweBic
     */
	public String getDweBic() {
		return this.dweBic;
	}
	
	/**
	 * @param dweNmAddr
	 */
	public void setDweNmAddr(String dweNmAddr) {
		this.dweNmAddr = dweNmAddr;
	}
	
    /**
     * @return dweNmAddr
     */
	public String getDweNmAddr() {
		return this.dweNmAddr;
	}
	
	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	
    /**
     * @return fax
     */
	public String getFax() {
		return this.fax;
	}
	
	/**
	 * @param gds
	 */
	public void setGds(String gds) {
		this.gds = gds;
	}
	
    /**
     * @return gds
     */
	public String getGds() {
		return this.gds;
	}
	
	/**
	 * @param gdsId
	 */
	public void setGdsId(String gdsId) {
		this.gdsId = gdsId;
	}
	
    /**
     * @return gdsId
     */
	public String getGdsId() {
		return this.gdsId;
	}
	
	/**
	 * @param issBkAddr
	 */
	public void setIssBkAddr(String issBkAddr) {
		this.issBkAddr = issBkAddr;
	}
	
    /**
     * @return issBkAddr
     */
	public String getIssBkAddr() {
		return this.issBkAddr;
	}
	
	/**
	 * @param issBkBic
	 */
	public void setIssBkBic(String issBkBic) {
		this.issBkBic = issBkBic;
	}
	
    /**
     * @return issBkBic
     */
	public String getIssBkBic() {
		return this.issBkBic;
	}
	
	/**
	 * @param issBkNm
	 */
	public void setIssBkNm(String issBkNm) {
		this.issBkNm = issBkNm;
	}
	
    /**
     * @return issBkNm
     */
	public String getIssBkNm() {
		return this.issBkNm;
	}
	
	/**
	 * @param issBkNmAddr
	 */
	public void setIssBkNmAddr(String issBkNmAddr) {
		this.issBkNmAddr = issBkNmAddr;
	}
	
    /**
     * @return issBkNmAddr
     */
	public String getIssBkNmAddr() {
		return this.issBkNmAddr;
	}
	
	/**
	 * @param issBkNo
	 */
	public void setIssBkNo(String issBkNo) {
		this.issBkNo = issBkNo;
	}
	
    /**
     * @return issBkNo
     */
	public String getIssBkNo() {
		return this.issBkNo;
	}
	
	/**
	 * @param issSwf72
	 */
	public void setIssSwf72(String issSwf72) {
		this.issSwf72 = issSwf72;
	}
	
    /**
     * @return issSwf72
     */
	public String getIssSwf72() {
		return this.issSwf72;
	}
	
	/**
	 * @param kernelNo
	 */
	public void setKernelNo(String kernelNo) {
		this.kernelNo = kernelNo;
	}
	
    /**
     * @return kernelNo
     */
	public String getKernelNo() {
		return this.kernelNo;
	}
	
	/**
	 * @param kernelOrgCode
	 */
	public void setKernelOrgCode(String kernelOrgCode) {
		this.kernelOrgCode = kernelOrgCode;
	}
	
    /**
     * @return kernelOrgCode
     */
	public String getKernelOrgCode() {
		return this.kernelOrgCode;
	}
	
	/**
	 * @param lastAbFlg
	 */
	public void setLastAbFlg(String lastAbFlg) {
		this.lastAbFlg = lastAbFlg;
	}
	
    /**
     * @return lastAbFlg
     */
	public String getLastAbFlg() {
		return this.lastAbFlg;
	}
	
	/**
	 * @param lastShpmtDt
	 */
	public void setLastShpmtDt(String lastShpmtDt) {
		this.lastShpmtDt = lastShpmtDt;
	}
	
    /**
     * @return lastShpmtDt
     */
	public String getLastShpmtDt() {
		return this.lastShpmtDt;
	}
	
	/**
	 * @param lcAmt
	 */
	public void setLcAmt(java.math.BigDecimal lcAmt) {
		this.lcAmt = lcAmt;
	}
	
    /**
     * @return lcAmt
     */
	public java.math.BigDecimal getLcAmt() {
		return this.lcAmt;
	}
	
	/**
	 * @param lcAmtAdt
	 */
	public void setLcAmtAdt(java.math.BigDecimal lcAmtAdt) {
		this.lcAmtAdt = lcAmtAdt;
	}
	
    /**
     * @return lcAmtAdt
     */
	public java.math.BigDecimal getLcAmtAdt() {
		return this.lcAmtAdt;
	}
	
	/**
	 * @param lcAmtMax
	 */
	public void setLcAmtMax(java.math.BigDecimal lcAmtMax) {
		this.lcAmtMax = lcAmtMax;
	}
	
    /**
     * @return lcAmtMax
     */
	public java.math.BigDecimal getLcAmtMax() {
		return this.lcAmtMax;
	}
	
	/**
	 * @param lcCcy
	 */
	public void setLcCcy(String lcCcy) {
		this.lcCcy = lcCcy;
	}
	
    /**
     * @return lcCcy
     */
	public String getLcCcy() {
		return this.lcCcy;
	}
	
	/**
	 * @param lcClsDt
	 */
	public void setLcClsDt(String lcClsDt) {
		this.lcClsDt = lcClsDt;
	}
	
    /**
     * @return lcClsDt
     */
	public String getLcClsDt() {
		return this.lcClsDt;
	}
	
	/**
	 * @param lcCopyCnf
	 */
	public void setLcCopyCnf(String lcCopyCnf) {
		this.lcCopyCnf = lcCopyCnf;
	}
	
    /**
     * @return lcCopyCnf
     */
	public String getLcCopyCnf() {
		return this.lcCopyCnf;
	}
	
	/**
	 * @param lcDt
	 */
	public void setLcDt(String lcDt) {
		this.lcDt = lcDt;
	}
	
    /**
     * @return lcDt
     */
	public String getLcDt() {
		return this.lcDt;
	}
	
	/**
	 * @param lcExpiryDt
	 */
	public void setLcExpiryDt(String lcExpiryDt) {
		this.lcExpiryDt = lcExpiryDt;
	}
	
    /**
     * @return lcExpiryDt
     */
	public String getLcExpiryDt() {
		return this.lcExpiryDt;
	}
	
	/**
	 * @param lcExpiryPlace
	 */
	public void setLcExpiryPlace(String lcExpiryPlace) {
		this.lcExpiryPlace = lcExpiryPlace;
	}
	
    /**
     * @return lcExpiryPlace
     */
	public String getLcExpiryPlace() {
		return this.lcExpiryPlace;
	}
	
	/**
	 * @param lcFlg
	 */
	public void setLcFlg(String lcFlg) {
		this.lcFlg = lcFlg;
	}
	
    /**
     * @return lcFlg
     */
	public String getLcFlg() {
		return this.lcFlg;
	}
	
	/**
	 * @param lcNo
	 */
	public void setLcNo(String lcNo) {
		this.lcNo = lcNo;
	}
	
    /**
     * @return lcNo
     */
	public String getLcNo() {
		return this.lcNo;
	}
	
	/**
	 * @param lcSignkFlg
	 */
	public void setLcSignkFlg(String lcSignkFlg) {
		this.lcSignkFlg = lcSignkFlg;
	}
	
    /**
     * @return lcSignkFlg
     */
	public String getLcSignkFlg() {
		return this.lcSignkFlg;
	}
	
	/**
	 * @param lcType
	 */
	public void setLcType(String lcType) {
		this.lcType = lcType;
	}
	
    /**
     * @return lcType
     */
	public String getLcType() {
		return this.lcType;
	}
	
	/**
	 * @param mailNo
	 */
	public void setMailNo(String mailNo) {
		this.mailNo = mailNo;
	}
	
    /**
     * @return mailNo
     */
	public String getMailNo() {
		return this.mailNo;
	}
	
	/**
	 * @param mailType
	 */
	public void setMailType(String mailType) {
		this.mailType = mailType;
	}
	
    /**
     * @return mailType
     */
	public String getMailType() {
		return this.mailType;
	}
	
	/**
	 * @param mgnPct
	 */
	public void setMgnPct(java.math.BigDecimal mgnPct) {
		this.mgnPct = mgnPct;
	}
	
    /**
     * @return mgnPct
     */
	public java.math.BigDecimal getMgnPct() {
		return this.mgnPct;
	}
	
	/**
	 * @param mixPayClause
	 */
	public void setMixPayClause(String mixPayClause) {
		this.mixPayClause = mixPayClause;
	}
	
    /**
     * @return mixPayClause
     */
	public String getMixPayClause() {
		return this.mixPayClause;
	}
	
	/**
	 * @param negTlrnc
	 */
	public void setNegTlrnc(java.math.BigDecimal negTlrnc) {
		this.negTlrnc = negTlrnc;
	}
	
    /**
     * @return negTlrnc
     */
	public java.math.BigDecimal getNegTlrnc() {
		return this.negTlrnc;
	}
	
	/**
	 * @param othBkBic
	 */
	public void setOthBkBic(String othBkBic) {
		this.othBkBic = othBkBic;
	}
	
    /**
     * @return othBkBic
     */
	public String getOthBkBic() {
		return this.othBkBic;
	}
	
	/**
	 * @param othBkId
	 */
	public void setOthBkId(String othBkId) {
		this.othBkId = othBkId;
	}
	
    /**
     * @return othBkId
     */
	public String getOthBkId() {
		return this.othBkId;
	}
	
	/**
	 * @param othBkLcNo
	 */
	public void setOthBkLcNo(String othBkLcNo) {
		this.othBkLcNo = othBkLcNo;
	}
	
    /**
     * @return othBkLcNo
     */
	public String getOthBkLcNo() {
		return this.othBkLcNo;
	}
	
	/**
	 * @param othBkNmAddr
	 */
	public void setOthBkNmAddr(String othBkNmAddr) {
		this.othBkNmAddr = othBkNmAddr;
	}
	
    /**
     * @return othBkNmAddr
     */
	public String getOthBkNmAddr() {
		return this.othBkNmAddr;
	}
	
	/**
	 * @param othFee
	 */
	public void setOthFee(String othFee) {
		this.othFee = othFee;
	}
	
    /**
     * @return othFee
     */
	public String getOthFee() {
		return this.othFee;
	}
	
	/**
	 * @param payAmtTtl
	 */
	public void setPayAmtTtl(java.math.BigDecimal payAmtTtl) {
		this.payAmtTtl = payAmtTtl;
	}
	
    /**
     * @return payAmtTtl
     */
	public java.math.BigDecimal getPayAmtTtl() {
		return this.payAmtTtl;
	}
	
	/**
	 * @param payBal
	 */
	public void setPayBal(java.math.BigDecimal payBal) {
		this.payBal = payBal;
	}
	
    /**
     * @return payBal
     */
	public java.math.BigDecimal getPayBal() {
		return this.payBal;
	}
	
	/**
	 * @param portDischg
	 */
	public void setPortDischg(String portDischg) {
		this.portDischg = portDischg;
	}
	
    /**
     * @return portDischg
     */
	public String getPortDischg() {
		return this.portDischg;
	}
	
	/**
	 * @param portLoad
	 */
	public void setPortLoad(String portLoad) {
		this.portLoad = portLoad;
	}
	
    /**
     * @return portLoad
     */
	public String getPortLoad() {
		return this.portLoad;
	}
	
	/**
	 * @param posTlrnc
	 */
	public void setPosTlrnc(java.math.BigDecimal posTlrnc) {
		this.posTlrnc = posTlrnc;
	}
	
    /**
     * @return posTlrnc
     */
	public java.math.BigDecimal getPosTlrnc() {
		return this.posTlrnc;
	}
	
	/**
	 * @param presntDays
	 */
	public void setPresntDays(String presntDays) {
		this.presntDays = presntDays;
	}
	
    /**
     * @return presntDays
     */
	public String getPresntDays() {
		return this.presntDays;
	}
	
	/**
	 * @param prodNm
	 */
	public void setProdNm(String prodNm) {
		this.prodNm = prodNm;
	}
	
    /**
     * @return prodNm
     */
	public String getProdNm() {
		return this.prodNm;
	}
	
	/**
	 * @param rcanFlg
	 */
	public void setRcanFlg(String rcanFlg) {
		this.rcanFlg = rcanFlg;
	}
	
    /**
     * @return rcanFlg
     */
	public String getRcanFlg() {
		return this.rcanFlg;
	}
	
	/**
	 * @param reimBkAcc
	 */
	public void setReimBkAcc(String reimBkAcc) {
		this.reimBkAcc = reimBkAcc;
	}
	
    /**
     * @return reimBkAcc
     */
	public String getReimBkAcc() {
		return this.reimBkAcc;
	}
	
	/**
	 * @param reimBkAddr
	 */
	public void setReimBkAddr(String reimBkAddr) {
		this.reimBkAddr = reimBkAddr;
	}
	
    /**
     * @return reimBkAddr
     */
	public String getReimBkAddr() {
		return this.reimBkAddr;
	}
	
	/**
	 * @param reimBkBic
	 */
	public void setReimBkBic(String reimBkBic) {
		this.reimBkBic = reimBkBic;
	}
	
    /**
     * @return reimBkBic
     */
	public String getReimBkBic() {
		return this.reimBkBic;
	}
	
	/**
	 * @param reimBkFeeBy
	 */
	public void setReimBkFeeBy(String reimBkFeeBy) {
		this.reimBkFeeBy = reimBkFeeBy;
	}
	
    /**
     * @return reimBkFeeBy
     */
	public String getReimBkFeeBy() {
		return this.reimBkFeeBy;
	}
	
	/**
	 * @param reimBkFlg
	 */
	public void setReimBkFlg(String reimBkFlg) {
		this.reimBkFlg = reimBkFlg;
	}
	
    /**
     * @return reimBkFlg
     */
	public String getReimBkFlg() {
		return this.reimBkFlg;
	}
	
	/**
	 * @param reimBkId
	 */
	public void setReimBkId(String reimBkId) {
		this.reimBkId = reimBkId;
	}
	
    /**
     * @return reimBkId
     */
	public String getReimBkId() {
		return this.reimBkId;
	}
	
	/**
	 * @param reimBkNm
	 */
	public void setReimBkNm(String reimBkNm) {
		this.reimBkNm = reimBkNm;
	}
	
    /**
     * @return reimBkNm
     */
	public String getReimBkNm() {
		return this.reimBkNm;
	}
	
	/**
	 * @param reimBkNmAddr
	 */
	public void setReimBkNmAddr(String reimBkNmAddr) {
		this.reimBkNmAddr = reimBkNmAddr;
	}
	
    /**
     * @return reimBkNmAddr
     */
	public String getReimBkNmAddr() {
		return this.reimBkNmAddr;
	}
	
	/**
	 * @param reimBkNo
	 */
	public void setReimBkNo(String reimBkNo) {
		this.reimBkNo = reimBkNo;
	}
	
    /**
     * @return reimBkNo
     */
	public String getReimBkNo() {
		return this.reimBkNo;
	}
	
	/**
	 * @param reimSwf72
	 */
	public void setReimSwf72(String reimSwf72) {
		this.reimSwf72 = reimSwf72;
	}
	
    /**
     * @return reimSwf72
     */
	public String getReimSwf72() {
		return this.reimSwf72;
	}
	
	/**
	 * @param saFlg
	 */
	public void setSaFlg(String saFlg) {
		this.saFlg = saFlg;
	}
	
    /**
     * @return saFlg
     */
	public String getSaFlg() {
		return this.saFlg;
	}
	
	/**
	 * @param sgAmtTtl
	 */
	public void setSgAmtTtl(java.math.BigDecimal sgAmtTtl) {
		this.sgAmtTtl = sgAmtTtl;
	}
	
    /**
     * @return sgAmtTtl
     */
	public java.math.BigDecimal getSgAmtTtl() {
		return this.sgAmtTtl;
	}
	
	/**
	 * @param sgClsFlg
	 */
	public void setSgClsFlg(String sgClsFlg) {
		this.sgClsFlg = sgClsFlg;
	}
	
    /**
     * @return sgClsFlg
     */
	public String getSgClsFlg() {
		return this.sgClsFlg;
	}
	
	/**
	 * @param sgTimes
	 */
	public void setSgTimes(Integer sgTimes) {
		this.sgTimes = sgTimes;
	}
	
    /**
     * @return sgTimes
     */
	public Integer getSgTimes() {
		return this.sgTimes;
	}
	
	/**
	 * @param shpmtDays
	 */
	public void setShpmtDays(Integer shpmtDays) {
		this.shpmtDays = shpmtDays;
	}
	
    /**
     * @return shpmtDays
     */
	public Integer getShpmtDays() {
		return this.shpmtDays;
	}
	
	/**
	 * @param shpmtDtIss
	 */
	public void setShpmtDtIss(String shpmtDtIss) {
		this.shpmtDtIss = shpmtDtIss;
	}
	
    /**
     * @return shpmtDtIss
     */
	public String getShpmtDtIss() {
		return this.shpmtDtIss;
	}
	
	/**
	 * @param swf72
	 */
	public void setSwf72(String swf72) {
		this.swf72 = swf72;
	}
	
    /**
     * @return swf72
     */
	public String getSwf72() {
		return this.swf72;
	}
	
	/**
	 * @param tenorDays
	 */
	public void setTenorDays(Integer tenorDays) {
		this.tenorDays = tenorDays;
	}
	
    /**
     * @return tenorDays
     */
	public Integer getTenorDays() {
		return this.tenorDays;
	}
	
	/**
	 * @param tenorType
	 */
	public void setTenorType(String tenorType) {
		this.tenorType = tenorType;
	}
	
    /**
     * @return tenorType
     */
	public String getTenorType() {
		return this.tenorType;
	}
	
	/**
	 * @param tlrncPointFlg
	 */
	public void setTlrncPointFlg(String tlrncPointFlg) {
		this.tlrncPointFlg = tlrncPointFlg;
	}
	
    /**
     * @return tlrncPointFlg
     */
	public String getTlrncPointFlg() {
		return this.tlrncPointFlg;
	}
	
	/**
	 * @param tradeProp
	 */
	public void setTradeProp(String tradeProp) {
		this.tradeProp = tradeProp;
	}
	
    /**
     * @return tradeProp
     */
	public String getTradeProp() {
		return this.tradeProp;
	}
	
	/**
	 * @param usanceAtSightFlg
	 */
	public void setUsanceAtSightFlg(String usanceAtSightFlg) {
		this.usanceAtSightFlg = usanceAtSightFlg;
	}
	
    /**
     * @return usanceAtSightFlg
     */
	public String getUsanceAtSightFlg() {
		return this.usanceAtSightFlg;
	}
	
	/**
	 * @param usanceAtSightPoint
	 */
	public void setUsanceAtSightPoint(Integer usanceAtSightPoint) {
		this.usanceAtSightPoint = usanceAtSightPoint;
	}
	
    /**
     * @return usanceAtSightPoint
     */
	public Integer getUsanceAtSightPoint() {
		return this.usanceAtSightPoint;
	}
	
	/**
	 * @param idCerNo
	 */
	public void setIdCerNo(String idCerNo) {
		this.idCerNo = idCerNo;
	}
	
    /**
     * @return idCerNo
     */
	public String getIdCerNo() {
		return this.idCerNo;
	}
	
	/**
	 * @param idCerType
	 */
	public void setIdCerType(String idCerType) {
		this.idCerType = idCerType;
	}
	
    /**
     * @return idCerType
     */
	public String getIdCerType() {
		return this.idCerType;
	}
	
	/**
	 * @param sysRefNo
	 */
	public void setSysRefNo(String sysRefNo) {
		this.sysRefNo = sysRefNo;
	}
	
    /**
     * @return sysRefNo
     */
	public String getSysRefNo() {
		return this.sysRefNo;
	}
	
	/**
	 * @param sysRelNo
	 */
	public void setSysRelNo(String sysRelNo) {
		this.sysRelNo = sysRelNo;
	}
	
    /**
     * @return sysRelNo
     */
	public String getSysRelNo() {
		return this.sysRelNo;
	}
	
	/**
	 * @param sysMainRelNo
	 */
	public void setSysMainRelNo(String sysMainRelNo) {
		this.sysMainRelNo = sysMainRelNo;
	}
	
    /**
     * @return sysMainRelNo
     */
	public String getSysMainRelNo() {
		return this.sysMainRelNo;
	}
	
	/**
	 * @param formOfLc
	 */
	public void setFormOfLc(String formOfLc) {
		this.formOfLc = formOfLc;
	}
	
    /**
     * @return formOfLc
     */
	public String getFormOfLc() {
		return this.formOfLc;
	}
	
	/**
	 * @param clsType
	 */
	public void setClsType(String clsType) {
		this.clsType = clsType;
	}
	
    /**
     * @return clsType
     */
	public String getClsType() {
		return this.clsType;
	}
	
	/**
	 * @param trfBkBic
	 */
	public void setTrfBkBic(String trfBkBic) {
		this.trfBkBic = trfBkBic;
	}
	
    /**
     * @return trfBkBic
     */
	public String getTrfBkBic() {
		return this.trfBkBic;
	}
	
	/**
	 * @param trfBkAddr
	 */
	public void setTrfBkAddr(String trfBkAddr) {
		this.trfBkAddr = trfBkAddr;
	}
	
    /**
     * @return trfBkAddr
     */
	public String getTrfBkAddr() {
		return this.trfBkAddr;
	}
	
	/**
	 * @param cacFlg
	 */
	public void setCacFlg(String cacFlg) {
		this.cacFlg = cacFlg;
	}
	
    /**
     * @return cacFlg
     */
	public String getCacFlg() {
		return this.cacFlg;
	}
	
	/**
	 * @param cnfIsOrNot
	 */
	public void setCnfIsOrNot(String cnfIsOrNot) {
		this.cnfIsOrNot = cnfIsOrNot;
	}
	
    /**
     * @return cnfIsOrNot
     */
	public String getCnfIsOrNot() {
		return this.cnfIsOrNot;
	}
	
	/**
	 * @param cnfBic
	 */
	public void setCnfBic(String cnfBic) {
		this.cnfBic = cnfBic;
	}
	
    /**
     * @return cnfBic
     */
	public String getCnfBic() {
		return this.cnfBic;
	}
	
	/**
	 * @param cnfBkNm
	 */
	public void setCnfBkNm(String cnfBkNm) {
		this.cnfBkNm = cnfBkNm;
	}
	
    /**
     * @return cnfBkNm
     */
	public String getCnfBkNm() {
		return this.cnfBkNm;
	}
	
	/**
	 * @param trfBk
	 */
	public void setTrfBk(String trfBk) {
		this.trfBk = trfBk;
	}
	
    /**
     * @return trfBk
     */
	public String getTrfBk() {
		return this.trfBk;
	}
	
	/**
	 * @param trfBkNo
	 */
	public void setTrfBkNo(String trfBkNo) {
		this.trfBkNo = trfBkNo;
	}
	
    /**
     * @return trfBkNo
     */
	public String getTrfBkNo() {
		return this.trfBkNo;
	}
	
	/**
	 * @param bpIsOrNot
	 */
	public void setBpIsOrNot(String bpIsOrNot) {
		this.bpIsOrNot = bpIsOrNot;
	}
	
    /**
     * @return bpIsOrNot
     */
	public String getBpIsOrNot() {
		return this.bpIsOrNot;
	}
	
	/**
	 * @param clsTimes
	 */
	public void setClsTimes(Integer clsTimes) {
		this.clsTimes = clsTimes;
	}
	
    /**
     * @return clsTimes
     */
	public Integer getClsTimes() {
		return this.clsTimes;
	}
	
	/**
	 * @param agtFlg
	 */
	public void setAgtFlg(String agtFlg) {
		this.agtFlg = agtFlg;
	}
	
    /**
     * @return agtFlg
     */
	public String getAgtFlg() {
		return this.agtFlg;
	}
	
	/**
	 * @param lcIssSelfFlg
	 */
	public void setLcIssSelfFlg(String lcIssSelfFlg) {
		this.lcIssSelfFlg = lcIssSelfFlg;
	}
	
    /**
     * @return lcIssSelfFlg
     */
	public String getLcIssSelfFlg() {
		return this.lcIssSelfFlg;
	}
	
	/**
	 * @param consnBkNo
	 */
	public void setConsnBkNo(String consnBkNo) {
		this.consnBkNo = consnBkNo;
	}
	
    /**
     * @return consnBkNo
     */
	public String getConsnBkNo() {
		return this.consnBkNo;
	}
	
	/**
	 * @param consnBkNm
	 */
	public void setConsnBkNm(String consnBkNm) {
		this.consnBkNm = consnBkNm;
	}
	
    /**
     * @return consnBkNm
     */
	public String getConsnBkNm() {
		return this.consnBkNm;
	}
	
	/**
	 * @param advOrg
	 */
	public void setAdvOrg(String advOrg) {
		this.advOrg = advOrg;
	}
	
    /**
     * @return advOrg
     */
	public String getAdvOrg() {
		return this.advOrg;
	}
	
	/**
	 * @param cnfNo
	 */
	public void setCnfNo(String cnfNo) {
		this.cnfNo = cnfNo;
	}
	
    /**
     * @return cnfNo
     */
	public String getCnfNo() {
		return this.cnfNo;
	}
	
	/**
	 * @param mailCo1
	 */
	public void setMailCo1(String mailCo1) {
		this.mailCo1 = mailCo1;
	}
	
    /**
     * @return mailCo1
     */
	public String getMailCo1() {
		return this.mailCo1;
	}
	
	/**
	 * @param mailNo1
	 */
	public void setMailNo1(String mailNo1) {
		this.mailNo1 = mailNo1;
	}
	
    /**
     * @return mailNo1
     */
	public String getMailNo1() {
		return this.mailNo1;
	}
	
	/**
	 * @param usanceAmdType
	 */
	public void setUsanceAmdType(String usanceAmdType) {
		this.usanceAmdType = usanceAmdType;
	}
	
    /**
     * @return usanceAmdType
     */
	public String getUsanceAmdType() {
		return this.usanceAmdType;
	}
	
	/**
	 * @param cntrctImgFlg
	 */
	public void setCntrctImgFlg(String cntrctImgFlg) {
		this.cntrctImgFlg = cntrctImgFlg;
	}
	
    /**
     * @return cntrctImgFlg
     */
	public String getCntrctImgFlg() {
		return this.cntrctImgFlg;
	}
	
	/**
	 * @param signkFlg
	 */
	public void setSignkFlg(String signkFlg) {
		this.signkFlg = signkFlg;
	}
	
    /**
     * @return signkFlg
     */
	public String getSignkFlg() {
		return this.signkFlg;
	}
	
	/**
	 * @param noAdvFlg
	 */
	public void setNoAdvFlg(String noAdvFlg) {
		this.noAdvFlg = noAdvFlg;
	}
	
    /**
     * @return noAdvFlg
     */
	public String getNoAdvFlg() {
		return this.noAdvFlg;
	}
	
	/**
	 * @param expiryPlace
	 */
	public void setExpiryPlace(String expiryPlace) {
		this.expiryPlace = expiryPlace;
	}
	
    /**
     * @return expiryPlace
     */
	public String getExpiryPlace() {
		return this.expiryPlace;
	}
	
	/**
	 * @param tranType
	 */
	public void setTranType(String tranType) {
		this.tranType = tranType;
	}
	
    /**
     * @return tranType
     */
	public String getTranType() {
		return this.tranType;
	}
	
	/**
	 * @param innerFlg
	 */
	public void setInnerFlg(String innerFlg) {
		this.innerFlg = innerFlg;
	}
	
    /**
     * @return innerFlg
     */
	public String getInnerFlg() {
		return this.innerFlg;
	}
	
	/**
	 * @param othBkNo
	 */
	public void setOthBkNo(String othBkNo) {
		this.othBkNo = othBkNo;
	}
	
    /**
     * @return othBkNo
     */
	public String getOthBkNo() {
		return this.othBkNo;
	}
	
	/**
	 * @param othBkNm
	 */
	public void setOthBkNm(String othBkNm) {
		this.othBkNm = othBkNm;
	}
	
    /**
     * @return othBkNm
     */
	public String getOthBkNm() {
		return this.othBkNm;
	}
	
	/**
	 * @param appBkNo
	 */
	public void setAppBkNo(String appBkNo) {
		this.appBkNo = appBkNo;
	}
	
    /**
     * @return appBkNo
     */
	public String getAppBkNo() {
		return this.appBkNo;
	}
	
	/**
	 * @param appBkPostcode
	 */
	public void setAppBkPostcode(String appBkPostcode) {
		this.appBkPostcode = appBkPostcode;
	}
	
    /**
     * @return appBkPostcode
     */
	public String getAppBkPostcode() {
		return this.appBkPostcode;
	}
	
	/**
	 * @param appBkTel
	 */
	public void setAppBkTel(String appBkTel) {
		this.appBkTel = appBkTel;
	}
	
    /**
     * @return appBkTel
     */
	public String getAppBkTel() {
		return this.appBkTel;
	}
	
	/**
	 * @param issBkTel
	 */
	public void setIssBkTel(String issBkTel) {
		this.issBkTel = issBkTel;
	}
	
    /**
     * @return issBkTel
     */
	public String getIssBkTel() {
		return this.issBkTel;
	}
	
	/**
	 * @param issBkPostcode
	 */
	public void setIssBkPostcode(String issBkPostcode) {
		this.issBkPostcode = issBkPostcode;
	}
	
    /**
     * @return issBkPostcode
     */
	public String getIssBkPostcode() {
		return this.issBkPostcode;
	}
	
	/**
	 * @param advBkTel
	 */
	public void setAdvBkTel(String advBkTel) {
		this.advBkTel = advBkTel;
	}
	
    /**
     * @return advBkTel
     */
	public String getAdvBkTel() {
		return this.advBkTel;
	}
	
	/**
	 * @param advBkPostcode
	 */
	public void setAdvBkPostcode(String advBkPostcode) {
		this.advBkPostcode = advBkPostcode;
	}
	
    /**
     * @return advBkPostcode
     */
	public String getAdvBkPostcode() {
		return this.advBkPostcode;
	}
	
	/**
	 * @param trfBkNm
	 */
	public void setTrfBkNm(String trfBkNm) {
		this.trfBkNm = trfBkNm;
	}
	
    /**
     * @return trfBkNm
     */
	public String getTrfBkNm() {
		return this.trfBkNm;
	}
	
	/**
	 * @param cnfBkNmAddr
	 */
	public void setCnfBkNmAddr(String cnfBkNmAddr) {
		this.cnfBkNmAddr = cnfBkNmAddr;
	}
	
    /**
     * @return cnfBkNmAddr
     */
	public String getCnfBkNmAddr() {
		return this.cnfBkNmAddr;
	}
	
	/**
	 * @param cnfBkBic
	 */
	public void setCnfBkBic(String cnfBkBic) {
		this.cnfBkBic = cnfBkBic;
	}
	
    /**
     * @return cnfBkBic
     */
	public String getCnfBkBic() {
		return this.cnfBkBic;
	}
	
	/**
	 * @param appTel
	 */
	public void setAppTel(String appTel) {
		this.appTel = appTel;
	}
	
    /**
     * @return appTel
     */
	public String getAppTel() {
		return this.appTel;
	}
	
	/**
	 * @param benTel
	 */
	public void setBenTel(String benTel) {
		this.benTel = benTel;
	}
	
    /**
     * @return benTel
     */
	public String getBenTel() {
		return this.benTel;
	}
	
	/**
	 * @param benBkNo
	 */
	public void setBenBkNo(String benBkNo) {
		this.benBkNo = benBkNo;
	}
	
    /**
     * @return benBkNo
     */
	public String getBenBkNo() {
		return this.benBkNo;
	}
	
	/**
	 * @param tradeType
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	
    /**
     * @return tradeType
     */
	public String getTradeType() {
		return this.tradeType;
	}
	
	/**
	 * @param serPlace
	 */
	public void setSerPlace(String serPlace) {
		this.serPlace = serPlace;
	}
	
    /**
     * @return serPlace
     */
	public String getSerPlace() {
		return this.serPlace;
	}
	
	/**
	 * @param partlShpmtFlg
	 */
	public void setPartlShpmtFlg(String partlShpmtFlg) {
		this.partlShpmtFlg = partlShpmtFlg;
	}
	
    /**
     * @return partlShpmtFlg
     */
	public String getPartlShpmtFlg() {
		return this.partlShpmtFlg;
	}
	
	/**
	 * @param shpmtPartlDesc
	 */
	public void setShpmtPartlDesc(String shpmtPartlDesc) {
		this.shpmtPartlDesc = shpmtPartlDesc;
	}
	
    /**
     * @return shpmtPartlDesc
     */
	public String getShpmtPartlDesc() {
		return this.shpmtPartlDesc;
	}
	
	/**
	 * @param othImgFlg
	 */
	public void setOthImgFlg(String othImgFlg) {
		this.othImgFlg = othImgFlg;
	}
	
    /**
     * @return othImgFlg
     */
	public String getOthImgFlg() {
		return this.othImgFlg;
	}
	
	/**
	 * @param shpmtType
	 */
	public void setShpmtType(String shpmtType) {
		this.shpmtType = shpmtType;
	}
	
    /**
     * @return shpmtType
     */
	public String getShpmtType() {
		return this.shpmtType;
	}
	
	/**
	 * @param shpmtPartlFlg
	 */
	public void setShpmtPartlFlg(String shpmtPartlFlg) {
		this.shpmtPartlFlg = shpmtPartlFlg;
	}
	
    /**
     * @return shpmtPartlFlg
     */
	public String getShpmtPartlFlg() {
		return this.shpmtPartlFlg;
	}
	
	/**
	 * @param shpmtTrmFlg
	 */
	public void setShpmtTrmFlg(String shpmtTrmFlg) {
		this.shpmtTrmFlg = shpmtTrmFlg;
	}
	
    /**
     * @return shpmtTrmFlg
     */
	public String getShpmtTrmFlg() {
		return this.shpmtTrmFlg;
	}
	
	/**
	 * @param othClause
	 */
	public void setOthClause(String othClause) {
		this.othClause = othClause;
	}
	
    /**
     * @return othClause
     */
	public String getOthClause() {
		return this.othClause;
	}
	
	/**
	 * @param payIns
	 */
	public void setPayIns(String payIns) {
		this.payIns = payIns;
	}
	
    /**
     * @return payIns
     */
	public String getPayIns() {
		return this.payIns;
	}
	
	/**
	 * @param clsBy
	 */
	public void setClsBy(String clsBy) {
		this.clsBy = clsBy;
	}
	
    /**
     * @return clsBy
     */
	public String getClsBy() {
		return this.clsBy;
	}
	
	/**
	 * @param clsCause
	 */
	public void setClsCause(String clsCause) {
		this.clsCause = clsCause;
	}
	
    /**
     * @return clsCause
     */
	public String getClsCause() {
		return this.clsCause;
	}
	
	/**
	 * @param advNo
	 */
	public void setAdvNo(String advNo) {
		this.advNo = advNo;
	}
	
    /**
     * @return advNo
     */
	public String getAdvNo() {
		return this.advNo;
	}
	
	/**
	 * @param serBy
	 */
	public void setSerBy(String serBy) {
		this.serBy = serBy;
	}
	
    /**
     * @return serBy
     */
	public String getSerBy() {
		return this.serBy;
	}
	
	/**
	 * @param payBy
	 */
	public void setPayBy(String payBy) {
		this.payBy = payBy;
	}
	
    /**
     * @return payBy
     */
	public String getPayBy() {
		return this.payBy;
	}
	
	/**
	 * @param presntDescp
	 */
	public void setPresntDescp(String presntDescp) {
		this.presntDescp = presntDescp;
	}
	
    /**
     * @return presntDescp
     */
	public String getPresntDescp() {
		return this.presntDescp;
	}
	
	/**
	 * @param allAmdTimes
	 */
	public void setAllAmdTimes(Integer allAmdTimes) {
		this.allAmdTimes = allAmdTimes;
	}
	
    /**
     * @return allAmdTimes
     */
	public Integer getAllAmdTimes() {
		return this.allAmdTimes;
	}
	
	/**
	 * @param amdLcAmt
	 */
	public void setAmdLcAmt(java.math.BigDecimal amdLcAmt) {
		this.amdLcAmt = amdLcAmt;
	}
	
    /**
     * @return amdLcAmt
     */
	public java.math.BigDecimal getAmdLcAmt() {
		return this.amdLcAmt;
	}
	
	/**
	 * @param kernelTypeCode
	 */
	public void setKernelTypeCode(String kernelTypeCode) {
		this.kernelTypeCode = kernelTypeCode;
	}
	
    /**
     * @return kernelTypeCode
     */
	public String getKernelTypeCode() {
		return this.kernelTypeCode;
	}


}