package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0421Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0421</br>
 * 任务名称：加工任务-贷后管理-公司客户风险分类  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0421Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0421Service.class);

    @Autowired
    private Cmis0421Mapper cmis0421Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 插入公司客户风险分类
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0421InsertPspRisk001(String openDay) {
        logger.info("truncatetmpupdatedate042fl 1、加工任务-贷后管理-删除临时表 参数为 :[{}]", openDay);
        // int truncatetmpupdatedate042fl = cmis0421Mapper.truncatetmpupdatedate042fl(openDay);// cmis_psp.tmp_update_date
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_update_date");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("truncatetmpupdatedate042fl 1、加工任务-贷后管理-删除临时表");

        logger.info("1、加工任务-贷后管理-插入对公贷款客户 参数为 :[{}]", openDay);
        int insertruncatetmpupdatedate0421dk = cmis0421Mapper.insertruncatetmpupdatedate0421dk(openDay);
        logger.info("1、加工任务-贷后管理-插入对公贷款客户,返回参数为:[{}]", insertruncatetmpupdatedate0421dk);

        logger.info("1、加工任务-贷后管理-插入对公委托贷款客户 参数为 :[{}]", openDay);
        int insertruncatetmpupdatedate0421wt = cmis0421Mapper.insertruncatetmpupdatedate0421wt(openDay);
        logger.info("1、加工任务-贷后管理-插入对公委托贷款客户,返回参数为:[{}]", insertruncatetmpupdatedate0421wt);

        logger.info("1、加工任务-贷后管理-插入对公银承客户 参数为 :[{}]", openDay);
        int insertruncatetmpupdatedate0421yc = cmis0421Mapper.insertruncatetmpupdatedate0421yc(openDay);
        logger.info("1、加工任务-贷后管理-插入对公银承客户,返回参数为:[{}]", insertruncatetmpupdatedate0421yc);

        logger.info("1、加工任务-贷后管理-插入对公保函客户 参数为 :[{}]", openDay);
        int insertruncatetmpupdatedate0421bh = cmis0421Mapper.insertruncatetmpupdatedate0421bh(openDay);
        logger.info("1、加工任务-贷后管理-插入对公保函客户,返回参数为:[{}]", insertruncatetmpupdatedate0421bh);

        logger.info("1、加工任务-贷后管理-插入信用证客户 参数为 :[{}]", openDay);
        int insertruncatetmpupdatedate0421xyz = cmis0421Mapper.insertruncatetmpupdatedate0421xyz(openDay);
        logger.info("1、加工任务-贷后管理-插入信用证客户,返回参数为:[{}]", insertruncatetmpupdatedate0421xyz);

        logger.info("1、加工任务-贷后管理-插入贴现客户 参数为 :[{}]", openDay);
        int insertruncatetmpupdatedate0421tx = cmis0421Mapper.insertruncatetmpupdatedate0421tx(openDay);
        logger.info("1、加工任务-贷后管理-插入贴现客户,返回参数为:[{}]", insertruncatetmpupdatedate0421tx);

        logger.info("1、加工任务-贷后管理-公司客户风险分类 参数为 :[{}]", openDay);
        int insertPspRisk01 = cmis0421Mapper.insertPspRisk01(openDay);
        logger.info("1、加工任务-贷后管理-公司客户风险分类,返回参数为:[{}]", insertPspRisk01);
    }

    /**
     * 插入6个月之内首次风险分类任务生成
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0421InsertPspRisk002(String openDay) {
//        logger.info("truncatetmpupdatedate04212fl清理临时表2  开始 参数为 :[{}]", openDay);
//        // int truncatetmpupdatedate04212fl = cmis0421Mapper.truncatetmpupdatedate04212fl(openDay);// cmis_psp.tmp_update_data2
//        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_update_data2");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
//        logger.info("truncatetmpupdatedate04212fl清理临时表2 结束");

//        logger.info(" 贷款存在分类结果的客户  开始 参数为 :[{}]", openDay);
//        int inserttmpupdatedata2 = cmis0421Mapper.inserttmpupdatedata2(openDay);
//        logger.info(" 贷款存在分类结果的客户 结束 ,返回参数为:[{}]", inserttmpupdatedata2);

//        logger.info("truncatetmpupdatedate04213fl清理临时表  开始 参数为 :[{}]", openDay);
//        // int truncatetmpupdatedate04213fl = cmis0421Mapper.truncatetmpupdatedate04213fl(openDay);// cmis_psp.tmp_update_data3
//        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_update_data3");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
//        logger.info("truncatetmpupdatedate04213fl清理临时表 结束");
//
//        logger.info("任务待发起的客户 开始 参数为 :[{}]", openDay);
//        int inserttmpupdatedata3 = cmis0421Mapper.inserttmpupdatedata3(openDay);
//        logger.info("任务待发起的客户 结束 ,返回参数为:[{}]", inserttmpupdatedata3);

        logger.info("2、客户如果台账分类结果为空 则业务发放 的同时由系统同步生成一笔新发放 业务的分类任务 参数为 :[{}]", openDay);
        int insertPspRisk02 = cmis0421Mapper.insertPspRisk02(openDay);
        logger.info("2、客户如果台账分类结果为空 ，则业务发放 的同时由系统同步生成一笔新发放 业务的分类任务 ,返回参数为:[{}]", insertPspRisk02);
    }
}
