/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.gjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGjpTfbComBizinfo
 * @类描述: bat_s_gjp_tfb_com_bizinfo数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-24 00:13:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_gjp_tfb_com_bizinfo")
public class BatSGjpTfbComBizinfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 业务流水号 **/
	@Column(name = "SYS_REF_NO", unique = false, nullable = true, length = 32)
	private String sysRefNo;
	
	/** 系统实体编号 **/
	@Column(name = "SYS_ENTY_ID", unique = false, nullable = true, length = 32)
	private String sysEntyId;
	
	/** 系统删除标志 **/
	@Column(name = "SYS_DEL_FLG", unique = false, nullable = true, length = 1)
	private String sysDelFlg;
	
	/** 创建人 **/
	@Column(name = "SYS_CRT_USER", unique = false, nullable = true, length = 32)
	private String sysCrtUser;
	
	/** 系统创建日期时间 **/
	@Column(name = "SYS_CRT_DT", unique = false, nullable = true, length = 20)
	private String sysCrtDt;
	
	/** 最近修改用户 **/
	@Column(name = "SYS_MODIFY_USER", unique = false, nullable = true, length = 32)
	private String sysModifyUser;
	
	/** 最近修改日期时间 **/
	@Column(name = "SYS_MODIFY_DT", unique = false, nullable = true, length = 20)
	private String sysModifyDt;
	
	/** 业务所属 **/
	@Column(name = "BIZ_ORG", unique = false, nullable = true, length = 6)
	private String bizOrg;
	
	/** 客户经理 **/
	@Column(name = "CST_MGR", unique = false, nullable = true, length = 50)
	private String cstMgr;
	
	/** 客户经理号码 **/
	@Column(name = "CST_MGR_NO", unique = false, nullable = true, length = 32)
	private String cstMgrNo;
	
	/** 客户编号 **/
	@Column(name = "CUST_ID", unique = false, nullable = true, length = 32)
	private String custId;
	
	/** 客户名称 **/
	@Column(name = "CUST_NAME", unique = false, nullable = true, length = 280)
	private String custName;
	
	/** 核心证件号码 **/
	@Column(name = "KERNEL_CRED_NO", unique = false, nullable = true, length = 32)
	private String kernelCredNo;
	
	/** 客户组织机构代码 **/
	@Column(name = "KERNEL_ORG_CODE", unique = false, nullable = true, length = 32)
	private String kernelOrgCode;
	
	/** 机构号 **/
	@Column(name = "ORG_CODE", unique = false, nullable = true, length = 32)
	private String orgCode;
	
	/** 机构名称 **/
	@Column(name = "ORG_NAME", unique = false, nullable = true, length = 256)
	private String orgName;
	
	/** 交易日期 **/
	@Column(name = "TX_DT", unique = false, nullable = true, length = 8)
	private String txDt;
	
	/** 机构号 **/
	@Column(name = "TX_ORG", unique = false, nullable = true, length = 32)
	private String txOrg;
	
	/** VIP标识 **/
	@Column(name = "VIP_FLG", unique = false, nullable = true, length = 2)
	private String vipFlg;
	
	/** 监管信息 **/
	@Column(name = "FIMS_INFO", unique = false, nullable = true, length = 128)
	private String fimsInfo;
	
	/** 协议方类别 **/
	@Column(name = "CUST_TYPE", unique = false, nullable = true, length = 8)
	private String custType;
	
	/** 核心编号 **/
	@Column(name = "KERNEL_NO", unique = false, nullable = true, length = 32)
	private String kernelNo;
	
	
	/**
	 * @param sysRefNo
	 */
	public void setSysRefNo(String sysRefNo) {
		this.sysRefNo = sysRefNo;
	}
	
    /**
     * @return sysRefNo
     */
	public String getSysRefNo() {
		return this.sysRefNo;
	}
	
	/**
	 * @param sysEntyId
	 */
	public void setSysEntyId(String sysEntyId) {
		this.sysEntyId = sysEntyId;
	}
	
    /**
     * @return sysEntyId
     */
	public String getSysEntyId() {
		return this.sysEntyId;
	}
	
	/**
	 * @param sysDelFlg
	 */
	public void setSysDelFlg(String sysDelFlg) {
		this.sysDelFlg = sysDelFlg;
	}
	
    /**
     * @return sysDelFlg
     */
	public String getSysDelFlg() {
		return this.sysDelFlg;
	}
	
	/**
	 * @param sysCrtUser
	 */
	public void setSysCrtUser(String sysCrtUser) {
		this.sysCrtUser = sysCrtUser;
	}
	
    /**
     * @return sysCrtUser
     */
	public String getSysCrtUser() {
		return this.sysCrtUser;
	}
	
	/**
	 * @param sysCrtDt
	 */
	public void setSysCrtDt(String sysCrtDt) {
		this.sysCrtDt = sysCrtDt;
	}
	
    /**
     * @return sysCrtDt
     */
	public String getSysCrtDt() {
		return this.sysCrtDt;
	}
	
	/**
	 * @param sysModifyUser
	 */
	public void setSysModifyUser(String sysModifyUser) {
		this.sysModifyUser = sysModifyUser;
	}
	
    /**
     * @return sysModifyUser
     */
	public String getSysModifyUser() {
		return this.sysModifyUser;
	}
	
	/**
	 * @param sysModifyDt
	 */
	public void setSysModifyDt(String sysModifyDt) {
		this.sysModifyDt = sysModifyDt;
	}
	
    /**
     * @return sysModifyDt
     */
	public String getSysModifyDt() {
		return this.sysModifyDt;
	}
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param bizOrg
	 */
	public void setBizOrg(String bizOrg) {
		this.bizOrg = bizOrg;
	}
	
    /**
     * @return bizOrg
     */
	public String getBizOrg() {
		return this.bizOrg;
	}
	
	/**
	 * @param cstMgr
	 */
	public void setCstMgr(String cstMgr) {
		this.cstMgr = cstMgr;
	}
	
    /**
     * @return cstMgr
     */
	public String getCstMgr() {
		return this.cstMgr;
	}
	
	/**
	 * @param cstMgrNo
	 */
	public void setCstMgrNo(String cstMgrNo) {
		this.cstMgrNo = cstMgrNo;
	}
	
    /**
     * @return cstMgrNo
     */
	public String getCstMgrNo() {
		return this.cstMgrNo;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param kernelCredNo
	 */
	public void setKernelCredNo(String kernelCredNo) {
		this.kernelCredNo = kernelCredNo;
	}
	
    /**
     * @return kernelCredNo
     */
	public String getKernelCredNo() {
		return this.kernelCredNo;
	}
	
	/**
	 * @param kernelOrgCode
	 */
	public void setKernelOrgCode(String kernelOrgCode) {
		this.kernelOrgCode = kernelOrgCode;
	}
	
    /**
     * @return kernelOrgCode
     */
	public String getKernelOrgCode() {
		return this.kernelOrgCode;
	}
	
	/**
	 * @param orgCode
	 */
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	
    /**
     * @return orgCode
     */
	public String getOrgCode() {
		return this.orgCode;
	}
	
	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
    /**
     * @return orgName
     */
	public String getOrgName() {
		return this.orgName;
	}
	
	/**
	 * @param txDt
	 */
	public void setTxDt(String txDt) {
		this.txDt = txDt;
	}
	
    /**
     * @return txDt
     */
	public String getTxDt() {
		return this.txDt;
	}
	
	/**
	 * @param txOrg
	 */
	public void setTxOrg(String txOrg) {
		this.txOrg = txOrg;
	}
	
    /**
     * @return txOrg
     */
	public String getTxOrg() {
		return this.txOrg;
	}
	
	/**
	 * @param vipFlg
	 */
	public void setVipFlg(String vipFlg) {
		this.vipFlg = vipFlg;
	}
	
    /**
     * @return vipFlg
     */
	public String getVipFlg() {
		return this.vipFlg;
	}
	
	/**
	 * @param fimsInfo
	 */
	public void setFimsInfo(String fimsInfo) {
		this.fimsInfo = fimsInfo;
	}
	
    /**
     * @return fimsInfo
     */
	public String getFimsInfo() {
		return this.fimsInfo;
	}
	
	/**
	 * @param custType
	 */
	public void setCustType(String custType) {
		this.custType = custType;
	}
	
    /**
     * @return custType
     */
	public String getCustType() {
		return this.custType;
	}
	
	/**
	 * @param kernelNo
	 */
	public void setKernelNo(String kernelNo) {
		this.kernelNo = kernelNo;
	}
	
    /**
     * @return kernelNo
     */
	public String getKernelNo() {
		return this.kernelNo;
	}


}