/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.ypp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSYppTInfContManaRights
 * @类描述: bat_s_ypp_t_inf_cont_mana_rights数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 17:10:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ypp_t_inf_cont_mana_rights")
public class BatSYppTInfContManaRights extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 押品编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "GUAR_NO")
	private String guarNo;
	
	/** 土地证（不动产权证号） **/
	@Column(name = "LAND_NO", unique = false, nullable = true, length = 450)
	private String landNo;
	
	/** 土地承包经营权权证号 **/
	@Column(name = "LAND_CERT_NO", unique = false, nullable = true, length = 450)
	private String landCertNo;
	
	/** 土地承包经营权抵押登记证号 **/
	@Column(name = "LAND_REG_CERT_NO", unique = false, nullable = true, length = 450)
	private String landRegCertNo;
	
	/** 土地承包经营权登记机关 **/
	@Column(name = "LAND_REG_CERT_OFFICE", unique = false, nullable = true, length = 450)
	private String landRegCertOffice;
	
	/** 土地承包经营权取得日期 **/
	@Column(name = "LAND_CERT_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String landCertBeginDate;
	
	/** 土地承包经营权到期日期 **/
	@Column(name = "LAND_CERT_END_DATE", unique = false, nullable = true, length = 10)
	private String landCertEndDate;
	
	/** 土地承包经营权面积 **/
	@Column(name = "LAND_CERT_AREA", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal landCertArea;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 土地承包经营权位置 **/
	@Column(name = "LAND_CERT_LOCATION", unique = false, nullable = true, length = 4000)
	private String landCertLocation;
	
	/** 买卖合同编号 **/
	@Column(name = "BUSINESS_CONTRACT_NO", unique = false, nullable = true, length = 1500)
	private String businessContractNo;
	
	/** 购买日期 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 购买价格 **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 10)
	private String curType;
	
	/** 押品使用情况 **/
	@Column(name = "T_USAGE", unique = false, nullable = true, length = 10)
	private String tUsage;
	
	/** 备注(限1000个汉字) **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 1000)
	private String remark;
	
	/** 土地使用年限 **/
	@Column(name = "LAND_USE_YEAR", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal landUseYear;
	
	/** 土地使用权性质 **/
	@Column(name = "LAND_USE_QUAL", unique = false, nullable = true, length = 10)
	private String landUseQual;
	
	/** 剩余使用年限 **/
	@Column(name = "LAND_REMAINDER_YEAR", unique = false, nullable = true, length = 10)
	private String landRemainderYear;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 街道/村镇/路名 **/
	@Column(name = "STREET", unique = false, nullable = true, length = 10)
	private String street;
	
	/** 担保分类代码 **/
	@Column(name = "GUAR_TYPE_CD", unique = false, nullable = true, length = 9)
	private String guarTypeCd;
	
	/** 土地用途（慧押押） **/
	@Column(name = "LAND_USAGE", unique = false, nullable = true, length = 9)
	private String landUsage;
	
	/** 实时查封状态 **/
	@Column(name = "SS_CF_STATUS", unique = false, nullable = true, length = 5)
	private String ssCfStatus;
	
	/** 查封时间 **/
	@Column(name = "CF_DATE", unique = false, nullable = true, length = 10)
	private String cfDate;
	
	/** 最高可抵押顺位 **/
	@Column(name = "MAX_YP_STATUS", unique = false, nullable = true, length = 5)
	private String maxYpStatus;
	
	/** 房屋用途（在线抵押使用） **/
	@Column(name = "HOUSE_USAGE", unique = false, nullable = true, length = 9)
	private String houseUsage;
	
	/** 房屋面积（在线抵押使用） **/
	@Column(name = "HOUSE_AREA", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal houseArea;
	
	/** 不动产单元号 **/
	@Column(name = "BDCDYH_NO", unique = false, nullable = true, length = 100)
	private String bdcdyhNo;
	
	/** 状态 **/
	@Column(name = "INSPECT_STATUS", unique = false, nullable = true, length = 2)
	private String inspectStatus;
	
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo;
	}
	
    /**
     * @return landNo
     */
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landCertNo
	 */
	public void setLandCertNo(String landCertNo) {
		this.landCertNo = landCertNo;
	}
	
    /**
     * @return landCertNo
     */
	public String getLandCertNo() {
		return this.landCertNo;
	}
	
	/**
	 * @param landRegCertNo
	 */
	public void setLandRegCertNo(String landRegCertNo) {
		this.landRegCertNo = landRegCertNo;
	}
	
    /**
     * @return landRegCertNo
     */
	public String getLandRegCertNo() {
		return this.landRegCertNo;
	}
	
	/**
	 * @param landRegCertOffice
	 */
	public void setLandRegCertOffice(String landRegCertOffice) {
		this.landRegCertOffice = landRegCertOffice;
	}
	
    /**
     * @return landRegCertOffice
     */
	public String getLandRegCertOffice() {
		return this.landRegCertOffice;
	}
	
	/**
	 * @param landCertBeginDate
	 */
	public void setLandCertBeginDate(String landCertBeginDate) {
		this.landCertBeginDate = landCertBeginDate;
	}
	
    /**
     * @return landCertBeginDate
     */
	public String getLandCertBeginDate() {
		return this.landCertBeginDate;
	}
	
	/**
	 * @param landCertEndDate
	 */
	public void setLandCertEndDate(String landCertEndDate) {
		this.landCertEndDate = landCertEndDate;
	}
	
    /**
     * @return landCertEndDate
     */
	public String getLandCertEndDate() {
		return this.landCertEndDate;
	}
	
	/**
	 * @param landCertArea
	 */
	public void setLandCertArea(java.math.BigDecimal landCertArea) {
		this.landCertArea = landCertArea;
	}
	
    /**
     * @return landCertArea
     */
	public java.math.BigDecimal getLandCertArea() {
		return this.landCertArea;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param landCertLocation
	 */
	public void setLandCertLocation(String landCertLocation) {
		this.landCertLocation = landCertLocation;
	}
	
    /**
     * @return landCertLocation
     */
	public String getLandCertLocation() {
		return this.landCertLocation;
	}
	
	/**
	 * @param businessContractNo
	 */
	public void setBusinessContractNo(String businessContractNo) {
		this.businessContractNo = businessContractNo;
	}
	
    /**
     * @return businessContractNo
     */
	public String getBusinessContractNo() {
		return this.businessContractNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param tUsage
	 */
	public void setTUsage(String tUsage) {
		this.tUsage = tUsage;
	}
	
    /**
     * @return tUsage
     */
	public String getTUsage() {
		return this.tUsage;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param landUseYear
	 */
	public void setLandUseYear(java.math.BigDecimal landUseYear) {
		this.landUseYear = landUseYear;
	}
	
    /**
     * @return landUseYear
     */
	public java.math.BigDecimal getLandUseYear() {
		return this.landUseYear;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual;
	}
	
    /**
     * @return landUseQual
     */
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landRemainderYear
	 */
	public void setLandRemainderYear(String landRemainderYear) {
		this.landRemainderYear = landRemainderYear;
	}
	
    /**
     * @return landRemainderYear
     */
	public String getLandRemainderYear() {
		return this.landRemainderYear;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}
	
    /**
     * @return guarTypeCd
     */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}
	
	/**
	 * @param landUsage
	 */
	public void setLandUsage(String landUsage) {
		this.landUsage = landUsage;
	}
	
    /**
     * @return landUsage
     */
	public String getLandUsage() {
		return this.landUsage;
	}
	
	/**
	 * @param ssCfStatus
	 */
	public void setSsCfStatus(String ssCfStatus) {
		this.ssCfStatus = ssCfStatus;
	}
	
    /**
     * @return ssCfStatus
     */
	public String getSsCfStatus() {
		return this.ssCfStatus;
	}
	
	/**
	 * @param cfDate
	 */
	public void setCfDate(String cfDate) {
		this.cfDate = cfDate;
	}
	
    /**
     * @return cfDate
     */
	public String getCfDate() {
		return this.cfDate;
	}
	
	/**
	 * @param maxYpStatus
	 */
	public void setMaxYpStatus(String maxYpStatus) {
		this.maxYpStatus = maxYpStatus;
	}
	
    /**
     * @return maxYpStatus
     */
	public String getMaxYpStatus() {
		return this.maxYpStatus;
	}
	
	/**
	 * @param houseUsage
	 */
	public void setHouseUsage(String houseUsage) {
		this.houseUsage = houseUsage;
	}
	
    /**
     * @return houseUsage
     */
	public String getHouseUsage() {
		return this.houseUsage;
	}
	
	/**
	 * @param houseArea
	 */
	public void setHouseArea(java.math.BigDecimal houseArea) {
		this.houseArea = houseArea;
	}
	
    /**
     * @return houseArea
     */
	public java.math.BigDecimal getHouseArea() {
		return this.houseArea;
	}
	
	/**
	 * @param bdcdyhNo
	 */
	public void setBdcdyhNo(String bdcdyhNo) {
		this.bdcdyhNo = bdcdyhNo;
	}
	
    /**
     * @return bdcdyhNo
     */
	public String getBdcdyhNo() {
		return this.bdcdyhNo;
	}
	
	/**
	 * @param inspectStatus
	 */
	public void setInspectStatus(String inspectStatus) {
		this.inspectStatus = inspectStatus;
	}
	
    /**
     * @return inspectStatus
     */
	public String getInspectStatus() {
		return this.inspectStatus;
	}


}