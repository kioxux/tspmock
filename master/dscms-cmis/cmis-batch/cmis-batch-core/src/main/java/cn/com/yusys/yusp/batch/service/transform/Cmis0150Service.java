package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0150Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0150</br>
 * 任务名称：加工任务-业务处理-网金数据初始化 </br>
 *
 * @author sunzhe
 * @version 1.0
 * @since 2021年10月05日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0150Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0150Service.class);

    @Autowired
    private Cmis0150Mapper cmis0150Mapper;

    /**
     * 插入贷款合同表
     *
     * @param openDay
     */
    public void cmis0150InsertWjCtrLoanCont01(String openDay) {
        logger.info("插入贷款合同表开始,请求参数为:[{}]", openDay);
        int cmis0150InsertWjCtrLoanCont01 = cmis0150Mapper.cmis0150InsertWjCtrLoanCont01(openDay);
        logger.info("插入贷款合同表结束,返回参数为:[{}]", cmis0150InsertWjCtrLoanCont01);

    }

    /**
     * 插入贷款台账表
     *
     * @param openDay
     */
    public void cmis0150InsertWjAccLoan02(String openDay) {
        logger.info("插入贷款台账表开始,请求参数为:[{}]", openDay);
        int cmis0150InsertWjAccLoan02 = cmis0150Mapper.cmis0150InsertWjAccLoan02(openDay);
        logger.info("插入贷款台账表结束,返回参数为:[{}]", cmis0150InsertWjAccLoan02);

    }

    /**
     * 更新贷款台账表
     *
     * @param openDay
     */
    public void cmis0150InsertCtrLoanCont(String openDay) {
        logger.info("清空临时表-清空网金五级分类数据 开始");
        cmis0150Mapper.truncateTmpRcpAntSlLoanCusRfc05();
        logger.info("清空临时表-清空网金五级分类数据 结束");

        logger.info("插入网金五级分类数据开始,请求参数为:[{}]", openDay);
        int insertTmpRcpAntSlLoanCusRfc06 = cmis0150Mapper.insertTmpRcpAntSlLoanCusRfc06(openDay);
        logger.info("插入网金五级分类数据结束,返回参数为:[{}]", insertTmpRcpAntSlLoanCusRfc06);

        logger.info("更新网金台账五级分类数据开始,请求参数为:[{}]", openDay);
        int updateAccLoanWj07 = cmis0150Mapper.updateAccLoanWj07(openDay);
        logger.info("更新网金台账五级分类数据结束,返回参数为:[{}]", updateAccLoanWj07);

        logger.info("更新网金核销台账数据- 更新核销状态 蚂蚁一期 开始,请求参数为:[{}]", openDay);
        int updateAccLoanWj08 = cmis0150Mapper.updateAccLoanWj08(openDay);
        logger.info("更新网金核销台账数据- 更新核销状态 蚂蚁一期 结束,返回参数为:[{}]", updateAccLoanWj08);

        logger.info("更新网金核销台账数据- 更新核销状态 蚂蚁二期 开始,请求参数为:[{}]", openDay);
        int updateAccLoanWj09 = cmis0150Mapper.updateAccLoanWj09(openDay);
        logger.info("更新网金核销台账数据- 更新核销状态 蚂蚁二期 结束,返回参数为:[{}]", updateAccLoanWj09);

    }
}
