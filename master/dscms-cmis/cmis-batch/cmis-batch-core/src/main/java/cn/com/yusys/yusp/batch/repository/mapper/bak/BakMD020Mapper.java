package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKMD020</br>
 * 任务名称：批后备份月表日表任务-备份最高额授信协议[CTR_HIGH_AMT_AGR_CONT]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakMD020Mapper {
    /**
     * 查询待删除当天的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryMD020DeleteOpenDayCtrHighAmtAgrContCounts(@Param("openDay") String openDay);


    /**
     * 查询待删除当月的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据
     *
     * @param openDay
     */
    int queryMD020DeleteCtrHighAmtAgrContCont(@Param("openDay") String openDay);

    /**
     * 删除当天的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据
     *
     * @param openDay
     */
    int bakMD020DeleteDCtrHighAmtAgrCont(@Param("openDay") String openDay);

    /**
     * 删除当月的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据
     *
     * @param openDay
     */
    int bakMD020DeleteMCtrHighAmtAgrCont(@Param("openDay") String openDay);

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertD(@Param("openDay") String openDay);

    /**
     * 备份当月的数据
     *
     * @param openDay
     * @return
     */
    int insertM(@Param("openDay") String openDay);

    /**
     * 查询当天备份的[委托贷款合同详情[ctr_high_amt_agr_cont]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryOriginalCounts(@Param("openDay") String openDay);


    /**
     * 查询当月备份的[委托贷款合同详情[TMP_M_ctr_high_amt_agr_cont]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryBakMCounts(@Param("openDay") String openDay);
}
