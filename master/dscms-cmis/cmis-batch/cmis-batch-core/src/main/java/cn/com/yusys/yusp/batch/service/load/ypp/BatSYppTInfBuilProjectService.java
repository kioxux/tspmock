/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.ypp;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.ypp.BatSYppTInfBuilProject;
import cn.com.yusys.yusp.batch.repository.mapper.load.ypp.BatSYppTInfBuilProjectMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSYppTInfBuilProjectService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 17:10:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSYppTInfBuilProjectService {
    private static final Logger logger = LoggerFactory.getLogger(BatSYppTInfBuilProjectService.class);
    @Autowired
    private BatSYppTInfBuilProjectMapper batSYppTInfBuilProjectMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSYppTInfBuilProject selectByPrimaryKey(String guarNo) {
        return batSYppTInfBuilProjectMapper.selectByPrimaryKey(guarNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSYppTInfBuilProject> selectAll(QueryModel model) {
        List<BatSYppTInfBuilProject> records = (List<BatSYppTInfBuilProject>) batSYppTInfBuilProjectMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSYppTInfBuilProject> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSYppTInfBuilProject> list = batSYppTInfBuilProjectMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSYppTInfBuilProject record) {
        return batSYppTInfBuilProjectMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSYppTInfBuilProject record) {
        return batSYppTInfBuilProjectMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSYppTInfBuilProject record) {
        return batSYppTInfBuilProjectMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSYppTInfBuilProject record) {
        return batSYppTInfBuilProjectMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String guarNo) {
        return batSYppTInfBuilProjectMapper.deleteByPrimaryKey(guarNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSYppTInfBuilProjectMapper.deleteByIds(ids);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建押品缓释系统-在建工程表[bat_s_ypp_t_inf_buil_project]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_ypp_t_inf_buil_project");
        logger.info("重建【YPP00008】押品缓释系统-在建工程表[bat_s_ypp_t_inf_buil_project]结束");
        return 0;
    }
}
