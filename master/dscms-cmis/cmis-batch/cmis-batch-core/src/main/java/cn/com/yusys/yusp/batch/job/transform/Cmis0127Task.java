package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0127Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0127</br>
 * 任务名称：加工任务-业务处理-非零内评数据更新 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0127Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0127Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0127Service cmis0127Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job cmis0127Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_JOB.key, JobStepEnum.CMIS0127_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0127Job = this.jobBuilderFactory.get(JobStepEnum.CMIS0127_JOB.key)
                .start(cmis0127UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0127CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(cmis0127UpdateMajorGradeInfoStep(WILL_BE_INJECTED)) // 更新专业贷款最终认定等级
                .next(cmis0127UpdateCtrLoanContStep(WILL_BE_INJECTED)) // 更新贷款合同
                .next(cmis0127UpdateCtrAccpContStep(WILL_BE_INJECTED)) // 更新银承合同
                .next(cmis0127UpdateCtrDiscContStep(WILL_BE_INJECTED)) // 更新贴现合同
                .next(cmis0127UpdateCtrCvrgContStep(WILL_BE_INJECTED)) // 更新保函合同
                .next(cmis0127UpdateCtrTfLocContStep(WILL_BE_INJECTED)) // 更新信用证合同
                .next(cmis0127UpdateAccLoanStep(WILL_BE_INJECTED)) // 更新贷款台账
                .next(cmis0127UpdateAccAccpDrftSubStep(WILL_BE_INJECTED)) // 更新银承台账明细
                .next(cmis0127UpdateAccDiscStep(WILL_BE_INJECTED)) // 更新贴现台账
                .next(cmis0127UpdateAccCvrsStep(WILL_BE_INJECTED)) // 更新保函台账
                .next(cmis0127UpdateAccTfLocStep(WILL_BE_INJECTED)) // 更新信用证台账
                .next(cmis0127UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0127Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0127_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0127_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0127_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_CHECK_REL_STEP.key, JobStepEnum.CMIS0127_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127CheckRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0127_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0127_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0127_CHECK_REL_STEP.key, JobStepEnum.CMIS0127_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0127_CHECK_REL_STEP.key, JobStepEnum.CMIS0127_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0127CheckRelStep;
    }

    /**
     * 更新专业贷款最终认定等级
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateMajorGradeInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_INSERT_MAJOR_GRADE_INFO_STEP.key, JobStepEnum.CMIS0127_INSERT_MAJOR_GRADE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateMajorGradeInfoStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0127_INSERT_MAJOR_GRADE_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateMajorGradeInfo(openDay);//更新专业贷款最终认定等级
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_INSERT_MAJOR_GRADE_INFO_STEP.key, JobStepEnum.CMIS0127_INSERT_MAJOR_GRADE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateMajorGradeInfoStep;
    }

    /**
     * 更新贷款合同
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateCtrLoanContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_CTR_LOAN_CONT_STEP.key, JobStepEnum.CMIS0127_UPDATE_CTR_LOAN_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateCtrLoanContStep= this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_CTR_LOAN_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateCtrLoanCont(openDay);//更新贷款合同
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_CTR_LOAN_CONT_STEP.key, JobStepEnum.CMIS0127_UPDATE_CTR_LOAN_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateCtrLoanContStep;
    }

    /**
     * 更新银承合同
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateCtrAccpContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_CTR_ACCP_CONT_STEP.key, JobStepEnum.CMIS0127_UPDATE_CTR_ACCP_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateCtrAccpContStep= this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_CTR_ACCP_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateCtrAccpCont(openDay);//更新贷款合同
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_CTR_ACCP_CONT_STEP.key, JobStepEnum.CMIS0127_UPDATE_CTR_ACCP_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateCtrAccpContStep;
    }


    /**
     * 更新贴现合同
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateCtrDiscContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_CTR_DISC_CONT_STEP.key, JobStepEnum.CMIS0127_UPDATE_CTR_DISC_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateCtrDiscContStep= this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_CTR_DISC_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateCtrDiscCont(openDay);//更新贴现合同
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_CTR_DISC_CONT_STEP.key, JobStepEnum.CMIS0127_UPDATE_CTR_DISC_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateCtrDiscContStep;
    }

    /**
     * 更新保函合同
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateCtrCvrgContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_CTR_CVRG_CONT_STEP.key, JobStepEnum.CMIS0127_UPDATE_CTR_CVRG_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateCtrCvrgContStep= this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_CTR_CVRG_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateCtrCvrgCont(openDay);//更新保函合同
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_CTR_CVRG_CONT_STEP.key, JobStepEnum.CMIS0127_UPDATE_CTR_CVRG_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateCtrCvrgContStep;
    }

    /**
     * 更新信用证合同
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateCtrTfLocContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_CTR_TF_LOC_CONT_STEP.key, JobStepEnum.CMIS0127_UPDATE_CTR_TF_LOC_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateCtrTfLocContStep= this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_CTR_TF_LOC_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateCtrTfLocCont(openDay);//更新信用证合同
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_CTR_TF_LOC_CONT_STEP.key, JobStepEnum.CMIS0127_UPDATE_CTR_TF_LOC_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateCtrTfLocContStep;
    }

    /**
     * 更新贷款台账
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateAccLoanStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_ACC_LOAN_STEP.key, JobStepEnum.CMIS0127_UPDATE_ACC_LOAN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateAccLoanStep= this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_ACC_LOAN_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateAccLoan(openDay);//更新贷款台账
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_ACC_LOAN_STEP.key, JobStepEnum.CMIS0127_UPDATE_ACC_LOAN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateAccLoanStep;
    }

    /**
     * 更新银承台账明细
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateAccAccpDrftSubStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_ACC_ACCP_DRFT_SUB_STEP.key, JobStepEnum.CMIS0127_UPDATE_ACC_ACCP_DRFT_SUB_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateAccAccpDrftSubStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_ACC_ACCP_DRFT_SUB_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateAccAccpDrftSub(openDay);//更新银承台账明细
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_ACC_ACCP_DRFT_SUB_STEP.key, JobStepEnum.CMIS0127_UPDATE_ACC_ACCP_DRFT_SUB_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateAccAccpDrftSubStep;
    }

    /**
     * 更新贴现台账
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateAccDiscStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_ACC_DISC_STEP.key, JobStepEnum.CMIS0127_UPDATE_ACC_DISC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateAccDiscStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_ACC_DISC_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateAccDisc(openDay);//更新贴现台账
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_ACC_DISC_STEP.key, JobStepEnum.CMIS0127_UPDATE_ACC_DISC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateAccDiscStep;
    }

    /**
     * 更新保函台账
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateAccCvrsStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_ACC_CVRS_STEP.key, JobStepEnum.CMIS0127_UPDATE_ACC_CVRS_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateAccCvrsStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_ACC_CVRS_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateAccCvrs(openDay);//更新保函台账
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_ACC_CVRS_STEP.key, JobStepEnum.CMIS0127_UPDATE_ACC_CVRS_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateAccCvrsStep;
    }

    /**
     * 更新信用证台账
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateAccTfLocStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_ACC_TF_LOC_STEP.key, JobStepEnum.CMIS0127_UPDATE_ACC_TF_LOC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateAccTfLocStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_ACC_TF_LOC_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0127Service.cmis0127UpdateAccTfLoc(openDay);//更新信用证台账
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_ACC_TF_LOC_STEP.key, JobStepEnum.CMIS0127_UPDATE_ACC_TF_LOC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateAccTfLocStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0127UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0127_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0127UpdateTask100Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0127_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0127_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0127_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0127_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0127UpdateTask100Step;
    }


}
