/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.djk;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpRptFiveClassifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.djk.BatTDjkDataLoan;
import cn.com.yusys.yusp.batch.repository.mapper.load.djk.BatTDjkDataLoanMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTDjkDataLoanService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-07 10:57:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTDjkDataLoanService {
    private static final Logger logger = LoggerFactory.getLogger(BatSRcpRptFiveClassifyService.class);
    @Autowired
    private TableUtilsService tableUtilsService;
    @Autowired
    private BatTDjkDataLoanMapper batTDjkDataLoanMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTDjkDataLoan selectByPrimaryKey(String loanId, String acctNo) {
        return batTDjkDataLoanMapper.selectByPrimaryKey(loanId, acctNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTDjkDataLoan> selectAll(QueryModel model) {
        List<BatTDjkDataLoan> records = (List<BatTDjkDataLoan>) batTDjkDataLoanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTDjkDataLoan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTDjkDataLoan> list = batTDjkDataLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTDjkDataLoan record) {
        return batTDjkDataLoanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTDjkDataLoan record) {
        return batTDjkDataLoanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTDjkDataLoan record) {
        return batTDjkDataLoanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTDjkDataLoan record) {
        return batTDjkDataLoanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String loanId, String acctNo) {
        return batTDjkDataLoanMapper.deleteByPrimaryKey(loanId, acctNo);
    }
    /**
     * 清空落地表
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void truncateTTable() {
        // 重建相关表
        logger.info("重建分期信息文件[bat_t_djk_data_loan]开始,请求参数为:[{}]");
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_t_djk_data_loan");
        logger.info("重建分期信息文件[bat_t_djk_data_loan]结束");
    }

}
