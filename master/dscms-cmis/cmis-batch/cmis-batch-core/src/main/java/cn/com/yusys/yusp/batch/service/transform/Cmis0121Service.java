package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0121Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0121</br>
 * 任务名称：加工任务-业务处理-零售智能风控马上金融 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0121Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0121Service.class);

    @Autowired
    private Cmis0121Mapper cmis0121Mapper;

    /**
     * 插入贷款合同表
     *
     * @param openDay
     */
    public void cmis0121InsertCtrLoanCont(String openDay) {
        logger.info("插入贷款合同表开始,请求参数为:[{}]", openDay);
        int insertCtrLoanCont = cmis0121Mapper.insertCtrLoanCont(openDay);
        logger.info("插入贷款合同表结束,返回参数为:[{}]", insertCtrLoanCont);
    }

    /**
     * 更新贷款合同表
     *
     * @param openDay
     */
    public void cmis0121UpdateCtrLoanCont(String openDay) {
//        logger.info("清空 临时表-马上金融贷款放款明细和客户信息关系表 开始,请求参数为:[{}] ", openDay);
//        int truncateTmpRcpMsLoanCus = cmis0121Mapper.truncateTmpRcpMsLoanCus(openDay);
//        logger.info("清空 临时表-马上金融贷款放款明细和客户信息关系表 结束,返回参数为:[{}] ", truncateTmpRcpMsLoanCus);
//
//        logger.info("插入 临时表-马上金融贷款放款明细和客户信息关系表 开始,请求参数为:[{}] ", openDay);
//        int insertTmpRcpMsLoanCus = cmis0121Mapper.insertTmpRcpMsLoanCus(openDay);
//        logger.info("插入 临时表-马上金融贷款放款明细和客户信息关系表 结束,返回参数为:[{}] ", insertTmpRcpMsLoanCus);
//
//        logger.info("更新贷款合同表开始,请求参数为:[{}]", openDay);
//        int updateCtrLoanCont = cmis0121Mapper.updateCtrLoanCont(openDay);
//        logger.info("更新贷款合同表结束,返回参数为:[{}]", updateCtrLoanCont);
    }

    /**
     * 插入贷款台账表
     *
     * @param openDay
     */
    public void cmis0121InsertAccLoan(String openDay) {
        logger.info("插入贷款台账表开始,请求参数为:[{}]", openDay);
        int insertAccLoan = cmis0121Mapper.insertAccLoan(openDay);
        logger.info("插入贷款台账表结束,返回参数为:[{}]", insertAccLoan);
    }

    /**
     * 更新贷款台账表
     *
     * @param openDay
     */
    public void cmis0121UpdateAccLoan(String openDay) {
        logger.info("更新贷款台账表开始,请求参数为:[{}]", openDay);
        int updateAccLoan = cmis0121Mapper.updateAccLoan(openDay);
        logger.info("更新贷款台账表结束,返回参数为:[{}]", updateAccLoan);
    }
}
