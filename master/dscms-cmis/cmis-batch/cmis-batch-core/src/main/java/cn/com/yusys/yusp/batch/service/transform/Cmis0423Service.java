package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0423Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0423</br>
 * 任务名称：加工任务-贷后管理-个人消费性风险分类  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0423Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0423Service.class);

    @Autowired
    private Cmis0423Mapper cmis0423Mapper;

    /**
     * 插入贷后检查任务表和风险分类借据信息
     *
     * @param openDay
     */
    public void cmis0423InsertPspRisk(String openDay) {
        logger.info(" a) 个人消费贷款风险分类任务");
        logger.info("插入贷后分类任务表开始,请求参数为:[{}]", openDay);
        int insertPspRisk01 = cmis0423Mapper.insertPspRisk01(openDay);
        logger.info("插入贷后分类任务表结束,返回参数为:[{}]", insertPspRisk01);

    }
}
