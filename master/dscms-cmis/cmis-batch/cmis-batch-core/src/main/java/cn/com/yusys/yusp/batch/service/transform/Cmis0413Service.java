package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0413Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-贷后管理-个人消费贷款定期检查 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0413Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0413Service.class);

    @Autowired
    private Cmis0413Mapper cmis0413Mapper;

    /**
     * 插入贷后检查任务表
     * @param openDay
     */
    public void cmis0413InsertPspTaskList(String openDay) {
        logger.info("非按揭类贷款中非分期付款类（非等额本金、非等额本息 ）的180天生成一次定期贷后检查任务开始,请求参数为:[{}]", openDay);
        int insertTownTask1 = cmis0413Mapper.insertTownTask1(openDay);
        logger.info("非按揭类贷款中非分期付款类（非等额本金、非等额本息 ）的180天生成一次定期贷后检查任务结束,返回参数为:[{}]", insertTownTask1);

        logger.info("村镇 非按揭类贷款中分期还款（ 等额本金 等额本息 ）的贷款，按时还款，不产生贷后；只要有逾期，就生成贷后开始,请求参数为:[{}]", openDay);
        int insertTownTask2 = cmis0413Mapper.insertTownTask2(openDay);
        logger.info("村镇 非按揭类贷款中分期还款（ 等额本金 等额本息 ）的贷款，按时还款，不产生贷后；只要有逾期，就生成贷后结束,返回参数为:[{}]", insertTownTask2);


        logger.info("1、个人消费贷款定期检查 不良 类贷款客户风险分类后三类 ,插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList01 = cmis0413Mapper.insertPspTaskList01(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList01);

        logger.info("2、房屋按揭类 贷款 借据出现逾期欠息 ,插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList02 = cmis0413Mapper.insertPspTaskList02(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList02);

        logger.info("3 、房屋按揭类 期房已过交房日期,仍未办理抵押插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList03 = cmis0413Mapper.insertPspTaskList03(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList03);

        logger.info("4、零售非按揭类 线下个人消费贷款 上月整月产生借据逾期欠息,插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList04 = cmis0413Mapper.insertPspTaskList04(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList04);

        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList05 = cmis0413Mapper.insertPspTaskList05(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList05);
    }
}
