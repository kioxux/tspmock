package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0104Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-业务处理- </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0104Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0104Service.class);

    @Autowired
    private Cmis0104Mapper cmis0104Mapper;

    /**
     * 插入贴现台账,同步贴现台账状态
     *
     * @param openDay
     */
    public void cmis0104InsertUpdateAccDisc(String openDay) {
        logger.info("插入贴现台账开始,请求参数为:[{}]", openDay);
        int insertAccDisc = cmis0104Mapper.insertAccDisc(openDay);
        logger.info("插入贴现台账结束,返回参数为:[{}]", insertAccDisc);
        logger.info("同步贴现台账状态开始,请求参数为:[{}]", openDay);
        int updateAccDisc = cmis0104Mapper.updateAccDisc(openDay);
        logger.info("同步贴现台账状态结束,返回参数为:[{}]", updateAccDisc);
    }
}
