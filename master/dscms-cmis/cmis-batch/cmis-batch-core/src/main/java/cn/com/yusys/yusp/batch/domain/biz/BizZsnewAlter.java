/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.biz;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BizZsnewAlter
 * @类描述: biz_zsnew_alter数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "biz_zsnew_alter")
public class BizZsnewAlter extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "APP_NO")
	private String appNo;
	
	/** 顺序号 **/
	@Id
	@Column(name = "SEQ_NUM")
	private String seqNum;
	
	/** 变更日期 **/
	@Column(name = "ALTDATE", unique = false, nullable = true, length = 2550)
	private String altdate;
	
	/** 变更事项 **/
	@Column(name = "ALTITEM", unique = false, nullable = true, length = 2550)
	private String altitem;
	
	/** 变更后内容 **/
	@Column(name = "ALTAF", unique = false, nullable = true, length = 2550)
	private String altaf;
	
	/** 变更前内容 **/
	@Column(name = "ALTBE", unique = false, nullable = true, length = 2550)
	private String altbe;
	
	/** 录入日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "LAST_UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	
	/**
	 * @param appNo
	 */
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	
    /**
     * @return appNo
     */
	public String getAppNo() {
		return this.appNo;
	}
	
	/**
	 * @param seqNum
	 */
	public void setSeqNum(String seqNum) {
		this.seqNum = seqNum;
	}
	
    /**
     * @return seqNum
     */
	public String getSeqNum() {
		return this.seqNum;
	}
	
	/**
	 * @param altdate
	 */
	public void setAltdate(String altdate) {
		this.altdate = altdate;
	}
	
    /**
     * @return altdate
     */
	public String getAltdate() {
		return this.altdate;
	}
	
	/**
	 * @param altitem
	 */
	public void setAltitem(String altitem) {
		this.altitem = altitem;
	}
	
    /**
     * @return altitem
     */
	public String getAltitem() {
		return this.altitem;
	}
	
	/**
	 * @param altaf
	 */
	public void setAltaf(String altaf) {
		this.altaf = altaf;
	}
	
    /**
     * @return altaf
     */
	public String getAltaf() {
		return this.altaf;
	}
	
	/**
	 * @param altbe
	 */
	public void setAltbe(String altbe) {
		this.altbe = altbe;
	}
	
    /**
     * @return altbe
     */
	public String getAltbe() {
		return this.altbe;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}


}