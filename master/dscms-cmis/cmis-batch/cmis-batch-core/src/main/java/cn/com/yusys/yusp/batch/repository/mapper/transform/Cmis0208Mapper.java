package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0208</br>
 * 任务名称：加工任务-额度处理-台账占用合同关系处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0208Mapper {
    /**
     * 处理贷款占用合同关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4A(@Param("openDay") String openDay);

    /**
     * 银承合同产生 垫款 插入 tmp_deal_cont_acc_rel
     *
     * @param openDay
     * @return
     */
    int insertTdcarYchtdk(@Param("openDay") String openDay);

    /**
     * insertTdcarZgeYcdk -  最高额授信协议下银承产生垫款 插入 tmp_deal_cont_acc_rel
     *
     * @param openDay
     * @return
     */
    int insertTdcarZgeYcdk(@Param("openDay") String openDay);


    /**
     * 处理台账占用最高额授信协议的关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4B(@Param("openDay") String openDay);

    /**
     * 处理银承台账占用最高额授信协议的关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4Yc(@Param("openDay") String openDay);

    /**
     * 处理保函台账占用最高额授信协议的关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4Bh(@Param("openDay") String openDay);

    /**
     * 处理信用证台账占用最高额授信协议的关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4Xyz(@Param("openDay") String openDay);

    /**
     * 处理贴现台账占用最高额授信协议的关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4Txtz(@Param("openDay") String openDay);

    /**
     * 处理委托贷款占用合同关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4C(@Param("openDay") String openDay);

    /**
     * 处理银承占用合同关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4D(@Param("openDay") String openDay);


    /**
     * 处理保函占用合同关系
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4F(@Param("openDay") String openDay);


    /**
     * 处理信用证占用合同关系
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4G(@Param("openDay") String openDay);

    /**
     * 处理贴现占用合同关系
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4H(@Param("openDay") String openDay);

    /**
     * 贷款占用资产池协议
     *
     * @param openDay
     * @return
     */
    int insertLmtTdcar4Dkzyzcc(@Param("openDay") String openDay);

    /**
     * 银承占用资产池协议
     *
     * @param openDay
     * @return
     */
    int insertLmtTdcar4Yczyzcc(@Param("openDay") String openDay);
}
