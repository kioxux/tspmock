package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0103Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import com.alibaba.fastjson.JSON;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0103</br>
 * 任务名称：加工任务-业务处理-银承台账处理 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0103Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0103Service.class);

    @Autowired
    private Cmis0103Mapper cmis0103Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 重命名创建和删除和初始化国结临时表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0103TruncateAndInsertTmpPjp(String openDay) {
        logger.info("重命名创建和删除临时表-综合票据系统-历史表-承兑批次表开始");
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_bat_s_pjp_bt_acception_batch");
        logger.info("重命名创建和删除临时表-综合票据系统-历史表-承兑批次表结束");

        logger.info("票据表中同一笔票据存在正常与作废，去掉作废  开始");
        int deletebtAcception = cmis0103Mapper.deletebtAcception( );
        logger.info("票据表中同一笔票据存在正常与作废，去掉作废  结束,返回参数为:[{}]", deletebtAcception);

        logger.info("插入临时表-综合票据系统-历史表-承兑批次表开始");
        int insertTmpPjpBtCcceptionBatch = cmis0103Mapper.insertTmpPjpBtCcceptionBatch(openDay);
        logger.info("插入临时表-综合票据系统-历史表-承兑批次表结束,返回参数为:[{}]", insertTmpPjpBtCcceptionBatch);

        logger.info("重命名创建和删除票据加工临时表开始");
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_pjg_drafacc");
        logger.info("重命名创建和删除票据加工临时表结束");
        String nextOpenDay = DateUtils.addDay(openDay, "yyyy-MM-dd", 1); // 切日后营业日期
        logger.info("切日后营业日期为:[{}]", nextOpenDay);
        Map paramMap = new HashMap<String, String>();
        paramMap.put("nextOpenDay", nextOpenDay);// 切日后营业日期
        paramMap.put("openDay", openDay);//切日前营业日期
        logger.info("插入票据加工临时表开始,请求参数为:[{}]", JSON.toJSONString(paramMap));
        int insertTmpPjp = cmis0103Mapper.insertTmpPjp(paramMap);//Job中openDay的传参是切日前营业日期
        logger.info("插入票据加工临时表结束,返回参数为:[{}]", insertTmpPjp);
    }

    /**
     * 插入和更新银承台账
     *
     * @param openDay
     * @return
     */
    public int cmis0103InsertUpdateAccCvrs(String openDay) {
        logger.info("更新票据台账表，更新原始敞口金额开始,请求参数为:[{}]", openDay);
        int updateAccAccp01 = cmis0103Mapper.updateAccAccp01(openDay);
        logger.info("更新票据台账表，更新原始敞口金额结束,返回参数为:[{}]", updateAccAccp01);

        return 0;
    }

    /**
     * 插入更新票据明细
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0103InsertUpdateAccAccpDrftSub(String openDay) {
        logger.info("重命名创建和删除综合票据系统-临时表关联大库表-票据加工临时表开始");
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_pjg_drafacc_edratf");
        logger.info("重命名创建和删除综合票据系统-临时表关联大库表-票据加工临时表结束");
        logger.info("插入综合票据系统-临时表关联大库表-票据加工临时表开始");
        int insertTmpPjpEdratf = cmis0103Mapper.insertTmpPjpEdratf(openDay);
        tableUtilsService.analyzeTable("cmis_batch","tmp_pjg_drafacc_edratf");//分析表

        logger.info("插入综合票据系统-临时表关联大库表-票据加工临时表结束,返回参数为:[{}]", insertTmpPjpEdratf);
        logger.info("重命名创建和删除临时表-银承台账开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_acc_accp_cmis_0103");
        logger.info("重命名创建和删除临时表-银承台账结束");
//        logger.info("插入临时表-银承台账开始");
//        int insertTmpAccAccp = cmis0103Mapper.insertTmpAccAccp(openDay);
//        tableUtilsService.analyzeTable("cmis_biz","tmp_acc_accp_cmis_0103");//分析表
//
//        logger.info("插入临时表-银承台账结束,返回参数为:[{}]", insertTmpAccAccp);
        logger.info("重命名创建和删除综合票据系统-临时表关联大库表-票据加工临时表2开始");
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_pjg_drafacc_edratf_cmis0103");
        logger.info("重命名创建和删除综合票据系统-临时表关联大库表-票据加工临时表2结束");
        logger.info("插入综合票据系统-临时表关联大库表-票据加工临时表2开始");
        int insertTmpPjgDrafaccEdratf = cmis0103Mapper.insertTmpPjgDrafaccEdratf(openDay);
        logger.info("插入综合票据系统-临时表关联大库表-票据加工临时表2结束,返回参数为:[{}]", insertTmpPjgDrafaccEdratf);
        tableUtilsService.analyzeTable("cmis_batch","tmp_pjg_drafacc_edratf_cmis0103");//分析表

        logger.info("重命名创建和删除临时表-批复额度分项基础信息开始");
        tableUtilsService.renameCreateDropTable("cmis_lmt","tmp_appr_lmt_sub_basic_info_cmis0103");
        logger.info("重命名创建和删除临时表-批复额度分项基础信息结束");
        logger.info("插入临时表-批复额度分项基础信息开始");
        int insertTmpApprLmtSub = cmis0103Mapper.insertTmpApprLmtSub(openDay);
        logger.info("插入临时表-批复额度分项基础信息结束,返回参数为:[{}]", insertTmpApprLmtSub);
        tableUtilsService.analyzeTable("cmis_lmt","tmp_appr_lmt_sub_basic_info_cmis0103");//分析表

        logger.info("插入银承台账票据明细开始,请求参数为:[{}]", openDay);
        int insertAccAccpDrftSub = cmis0103Mapper.insertAccAccpDrftSub(openDay);
        logger.info("插入银承台账票据明细结束,返回参数为:[{}]", insertAccAccpDrftSub);

        logger.info("更新  银承票据明细 保证金 、状态开始,请求参数为:[{}]", openDay);
        int updateAccAccpDrftSub01 = cmis0103Mapper.updateAccAccpDrftSub01(openDay);
        logger.info("更新  银承票据明细 保证金 、状态结束,返回参数为:[{}]", updateAccAccpDrftSub01);

        logger.info("重命名创建和删除临时表银承台账2开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_acc_accp_cmis_2_0103");
        logger.info("重命名创建和删除临时表银承台账2结束");

        logger.info("插入-临时表银承台账2开始,请求参数为:[{}]", openDay);
        int insertTmpAccAccpCmis2 = cmis0103Mapper.insertTmpAccAccpCmis2(openDay);
        logger.info("插入-临时表银承台账2结束,返回参数为:[{}]", insertTmpAccAccpCmis2);
        tableUtilsService.analyzeTable("cmis_biz","tmp_acc_accp_cmis_2_0103");//分析表

        logger.info("重命名创建和删除-综合票据系统-临时表-银承台账处理开始");
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_pjp_drafacc_accp");
        logger.info("重命名创建和删除-综合票据系统-临时表-银承台账处理结束");

        logger.info("插入-综合票据系统-临时表-银承台账处理 开始,请求参数为:[{}]", openDay);
        int insertTmpPjpDrafaccAccp4A = cmis0103Mapper.insertTmpPjpDrafaccAccp4A(openDay);
        logger.info("插入-综合票据系统-临时表-银承台账处理 结束,返回参数为:[{}]", insertTmpPjpDrafaccAccp4A);

        logger.info("更新银承台账4A开始,请求参数为:[{}]", openDay);
        int updateAccAccp4A = cmis0103Mapper.updateAccAccp4A(openDay);
        logger.info("更新银承台账4A结束,返回参数为:[{}]", updateAccAccp4A);

        logger.info("重命名创建和删除银承台账处理开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_pjp_drafacc_accp");
        logger.info("重命名创建和删除银承台账处理处理 结束");

        logger.info("重命名创建和删除临时表银承台账3开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_acc_accp_cmis_3_0103");
        logger.info("重命名创建和删除临时表银承台账3结束");

        logger.info("插入-临时表银承台账3开始,请求参数为:[{}]", openDay);
        int insertTmpAccAccpCmis3 = cmis0103Mapper.insertTmpAccAccpCmis3(openDay);
        logger.info("插入-临时表银承台账3结束,返回参数为:[{}]", insertTmpAccAccpCmis3);
        tableUtilsService.analyzeTable("cmis_biz","tmp_acc_accp_cmis_3_0103");//分析表

        logger.info("插入 临时表-银承台账处理 开始,请求参数为:[{}]", openDay);
        int insertTmpPjpDrafaccAccp = cmis0103Mapper.insertTmpPjpDrafaccAccp(openDay);
        logger.info("插入 临时表-银承台账处理 结束,返回参数为:[{}]", insertTmpPjpDrafaccAccp);
        tableUtilsService.analyzeTable("cmis_batch","tmp_pjp_drafacc_accp");//分析表

        logger.info("更新票据台账表，更新  银承敞口金额、保证金开始,请求参数为:[{}]", openDay);
        int updateAccAccp02 = cmis0103Mapper.updateAccAccp02(openDay);
        logger.info("更新票据台账表，更新  银承敞口金额、保证金结束,返回参数为:[{}]", updateAccAccp02);

        logger.info("更新银承台账B开始,请求参数为:[{}]", openDay);
        int updateAccAccp4B = cmis0103Mapper.updateAccAccp4B(openDay);
        logger.info("更更新银承台账B结束,返回参数为:[{}]", updateAccAccp4B);

        logger.info("重命名创建和删除根据票据明细的状态处理银承台账开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_biz_acc_accp1");
        logger.info("重命名创建和删除根据票据明细的状态处理银承台账结束");

        logger.info("插入根据票据明细的状态处理银承台账开始,请求参数为:[{}]", openDay);
        int insertTmpBizAccAccp1 = cmis0103Mapper.insertTmpBizAccAccp1(openDay);
        logger.info("插入根据票据明细的状态处理银承台账结束,返回参数为:[{}]", insertTmpBizAccAccp1);
        tableUtilsService.analyzeTable("cmis_batch","tmp_biz_acc_accp1");//分析表

        logger.info("重命名创建和删除根据票据明细的状态处理银承台账开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_biz_acc_accp2");
        logger.info("重命名创建和删除根据票据明细的状态处理银承台账结束");

        logger.info("插入根据票据明细的状态处理银承台账开始,请求参数为:[{}]", openDay);
        int insertTmpBizAccAccp2 = cmis0103Mapper.insertTmpBizAccAccp2(openDay);
        logger.info("插入根据票据明细的状态处理银承台账结束,返回参数为:[{}]", insertTmpBizAccAccp2);
        tableUtilsService.analyzeTable("cmis_batch","tmp_biz_acc_accp2");//分析表

        logger.info("重命名创建和删除 临时表-根据票据明细的状态处理银承台账 开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_biz_acc_accp");
        logger.info("重命名创建和删除 临时表-根据票据明细的状态处理银承台账 结束");

        logger.info("插入 临时表-根据票据明细的状态处理银承台账 开始,请求参数为:[{}]", openDay);
        int insertTmpBizAccAccp = cmis0103Mapper.insertTmpBizAccAccp(openDay);
        logger.info("插入 临时表-根据票据明细的状态处理银承台账 结束,返回参数为:[{}]", insertTmpBizAccAccp);
        tableUtilsService.analyzeTable("cmis_batch","tmp_biz_acc_accp");//分析表

        logger.info("依据票据明细的状态更新银承台账的状态开始,请求参数为:[{}]", openDay);
        int updateAccAccpDrftSub02 = cmis0103Mapper.updateAccAccpDrftSub02(openDay);
        logger.info("依据票据明细的状态更新银承台账的状态结束,返回参数为:[{}]", updateAccAccpDrftSub02);

        logger.info("重命名创建和删除临时表银承台账票据明细-核心银承编号开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_acc_accp_drft_sub_cbn_cmis0103");
        logger.info("重命名创建和删除临时表银承台账票据明细-核心银承编号结束");
//        logger.info("插入临时表银承台账票据明细-核心银承编号开始");
//        int insertTmpAccAccpSub = cmis0103Mapper.insertTmpAccAccpSub(openDay);
//        logger.info("插入临时表银承台账票据明细-核心银承编号结束,返回参数为:[{}]", insertTmpAccAccpSub);
//        tableUtilsService.analyzeTable("cmis_biz","tmp_acc_accp_drft_sub_cbn_cmis0103");//分析表
//
//        logger.info("不存在的票据明细中的票据直接作废 开始,请求参数为:[{}]", openDay);
//        int updateAccAccpAccStatus01 = cmis0103Mapper.updateAccAccpAccStatus01(openDay);
//        logger.info("不存在的票据明细中的票据直接作废 结束,返回参数为:[{}]", updateAccAccpAccStatus01);

        logger.info("到期垫款 开始,请求参数为:[{}]", openDay);
        int updateAccAccp03 = cmis0103Mapper.updateAccAccp03(openDay);
        logger.info("到期垫款结束,返回参数为:[{}]", updateAccAccp03);

        logger.info("垫款结清开始,请求参数为:[{}]", openDay);
        int updateAccAccp04 = cmis0103Mapper.updateAccAccp04(openDay);
        logger.info("垫款结清结束,返回参数为:[{}]", updateAccAccp04);



        logger.info("删除票据汇总临时表开始,请求参数为:[{}]", openDay);
        int deleteTmpAccCalculate = cmis0103Mapper.deleteTmpAccCalculate(openDay);
        logger.info("删除票据汇总临时表结束,返回参数为:[{}]", deleteTmpAccCalculate);


        logger.info("插入票据汇总临时表 开始,请求参数为:[{}]", openDay);
        int insertTmpAccCalculate = cmis0103Mapper.insertTmpAccCalculate(openDay);
        logger.info("插入票据汇总临时表 结束,返回参数为:[{}]", insertTmpAccCalculate);
    }
}
