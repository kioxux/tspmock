package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKMD025</br>
 * 任务名称：批后备份月表日表任务-备份担保合同表[GRT_GUAR_CONT]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakMD025Mapper {
    /**
     * 查询待删除当天的[担保合同表[GRT_GUAR_CONT]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryMD025DeleteOpenDayGrtGuarContCounts(@Param("openDay") String openDay);


    /**
     * 查询待删除当月的[担保合同表[GRT_GUAR_CONT]]数据
     *
     * @param openDay
     */
    int queryMD0025DeleteGrtGuarContCont(@Param("openDay") String openDay);

    /**
     * 删除当天的[担保合同表[GRT_GUAR_CONT]]数据
     *
     * @param openDay
     */
    int bakMD025DeleteDGrtGuarCont(@Param("openDay") String openDay);

    /**
     * 删除当月的[担保合同表[GRT_GUAR_CONT]]数据
     *
     * @param openDay
     */
    int bakMD025DeleteMGrtGuarCont(@Param("openDay") String openDay);

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertD(@Param("openDay") String openDay);

    /**
     * 备份当月的数据
     *
     * @param openDay
     * @return
     */
    int insertM(@Param("openDay") String openDay);

    /**
     * 查询当天备份的[担保合同表[GRT_GUAR_CONT]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryOriginalCounts(@Param("openDay") String openDay);


    /**
     * 查询当月备份的[担保合同表[TMP_M_GRT_GUAR_CONT]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryBakMCounts(@Param("openDay") String openDay);

}
