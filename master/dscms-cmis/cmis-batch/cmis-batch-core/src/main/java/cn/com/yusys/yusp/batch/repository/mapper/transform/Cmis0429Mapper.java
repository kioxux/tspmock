package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0429</br>
 * 任务名称：加工任务-贷后管理-生成自动化贷后个人名单  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月18日 下午11:56:54
 */
public interface Cmis0429Mapper {

    /**
     * 插入个人预跑批白名单月表
     *
     * @param openDay
     * @return
     */
    int insertAutoPspWhiteInfoPer01Month(@Param("openDay") String openDay);

    /**
     * 删除个人预跑批白名单
     *
     * @param openDay
     * @return
     */
    int deleteAutoPspWhiteInfoPer(@Param("openDay") String openDay);

    /**
     * 插入个人预跑批白名单
     *
     * @param openDay
     * @return
     */
    int insertAutoPspWhiteInfoPer01(@Param("openDay") String openDay);

    /**
     * 删除 非专业担保公司 担保
     *
     * @param openDay
     * @return
     */
    int deleteautoPspWhiteInfoPerNotGuarCom(@Param("openDay") String openDay);



    /**
     * 插入实际控制人用于征信校验
     *
     * @param openDay
     * @return
     */
    int insertAutoPspWhiteInfoPer02(@Param("openDay") String openDay);

    /**
     *  6个月之内存在逾期欠息则删除此自动化名单
     *
     * @param openDay
     * @return
     */
    int deletePspWhiteInfoCorpOverdue(@Param("openDay") String openDay);

    /**
     * 不存在三个月之内的个人征信需要到人行查询征信报告
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer(@Param("openDay") String openDay);
}
