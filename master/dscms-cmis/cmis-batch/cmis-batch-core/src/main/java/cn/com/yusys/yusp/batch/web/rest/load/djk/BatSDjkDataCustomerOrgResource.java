/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.djk;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.djk.BatSDjkDataCustomerOrg;
import cn.com.yusys.yusp.batch.service.load.djk.BatSDjkDataCustomerOrgService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSDjkDataCustomerOrgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-29 22:44:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsdjkdatacustomerorg")
public class BatSDjkDataCustomerOrgResource {
    @Autowired
    private BatSDjkDataCustomerOrgService batSDjkDataCustomerOrgService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSDjkDataCustomerOrg>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSDjkDataCustomerOrg> list = batSDjkDataCustomerOrgService.selectAll(queryModel);
        return new ResultDto<List<BatSDjkDataCustomerOrg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSDjkDataCustomerOrg>> index(QueryModel queryModel) {
        List<BatSDjkDataCustomerOrg> list = batSDjkDataCustomerOrgService.selectByModel(queryModel);
        return new ResultDto<List<BatSDjkDataCustomerOrg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{custId}")
    protected ResultDto<BatSDjkDataCustomerOrg> show(@PathVariable("custId") String custId) {
        BatSDjkDataCustomerOrg batSDjkDataCustomerOrg = batSDjkDataCustomerOrgService.selectByPrimaryKey(custId);
        return new ResultDto<BatSDjkDataCustomerOrg>(batSDjkDataCustomerOrg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSDjkDataCustomerOrg> create(@RequestBody BatSDjkDataCustomerOrg batSDjkDataCustomerOrg) throws URISyntaxException {
        batSDjkDataCustomerOrgService.insert(batSDjkDataCustomerOrg);
        return new ResultDto<BatSDjkDataCustomerOrg>(batSDjkDataCustomerOrg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSDjkDataCustomerOrg batSDjkDataCustomerOrg) throws URISyntaxException {
        int result = batSDjkDataCustomerOrgService.update(batSDjkDataCustomerOrg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{custId}")
    protected ResultDto<Integer> delete(@PathVariable("custId") String custId) {
        int result = batSDjkDataCustomerOrgService.deleteByPrimaryKey(custId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSDjkDataCustomerOrgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
