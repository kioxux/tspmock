package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0101Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0101</br>
 * 任务名称：加工任务-业务处理-贷款台账处理 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0101Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0101Service.class);

    @Autowired
    private Cmis0101Mapper cmis0101Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 垫款处理，更新 贷款台账 表中字段
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0101InsertUpdateAccLoan01(String openDay) {
//        logger.info("重命名创建和删除银承台账表0101开始,请求参数为:[{}]", openDay);
//        tableUtilsService.renameCreateDropTable("cmis_biz","temp_acc_accp_0101");
//        logger.info("重命名创建和删除银承台账表0101结束");
//        logger.info("插入银承台账表0101处理开始,请求参数为:[{}]" , openDay);
//        int insertTempAccAccp0101 = cmis0101Mapper.insertTempAccAccp0101(openDay);
//        logger.info("插入银承台账表0101处理结束,返回参数为:[{}]", insertTempAccAccp0101);

        logger.info("垫款处理开始,请求参数为:[{}]"  , openDay);
        int cmis0101InsertAccLoan = cmis0101Mapper.cmis0101InsertAccLoan(openDay);
        logger.info("垫款处理结束,返回参数为:[{}]", cmis0101InsertAccLoan);

        logger.info("更新 台账状态 、借据起始日、到期日、借据余额 、结清日期开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan01 = cmis0101Mapper.cmis0101UpdateAccLoan01(openDay);
        logger.info("更新 台账状态 、借据起始日、到期日、借据余额 、结清日期结束,返回参数为:[{}]", cmis0101UpdateAccLoan01);

        logger.info("更新  人民币金额 汇率开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan01A = cmis0101Mapper.cmis0101UpdateAccLoan01A(openDay);
        logger.info("更新  人民币金额 汇率结束,返回参数为:[{}]", cmis0101UpdateAccLoan01A);

        logger.info("更新 贷款台账人民币余额补充开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan02A = cmis0101Mapper.cmis0101UpdateAccLoan02A(openDay);
        logger.info("更新 贷款台账人民币余额补充结束,返回参数为:[{}]", cmis0101UpdateAccLoan02A);

        logger.info("更新 借据结清更新信贷台账状态开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan02 = cmis0101Mapper.cmis0101UpdateAccLoan02(openDay);
        logger.info("更新 借据结清更新信贷台账状态结束,返回参数为:[{}]", cmis0101UpdateAccLoan02);

        logger.info("更新 借据核销更新信贷台账状态开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan03A = cmis0101Mapper.cmis0101UpdateAccLoan03A(openDay);
        logger.info("更新 借据核销更新信贷台账状态结束,返回参数为:[{}]", cmis0101UpdateAccLoan03A);

        logger.info("更新欠息、罚息 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan03 = cmis0101Mapper.cmis0101UpdateAccLoan03(openDay);
        logger.info("更新 欠息、罚息 结束,返回参数为:[{}]", cmis0101UpdateAccLoan03);

        logger.info("更新还款方式开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan04 = cmis0101Mapper.cmis0101UpdateAccLoan04(openDay);
        logger.info("更新还款方式结束,返回参数为:[{}]", cmis0101UpdateAccLoan04);

        logger.info("同步利率调整方式开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan05 = cmis0101Mapper.cmis0101UpdateAccLoan05(openDay);
        logger.info("同步利率调整方式结束,返回参数为:[{}]", cmis0101UpdateAccLoan05);

        logger.info("更新利率调整类型开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan05Tmp2 = cmis0101Mapper.cmis0101UpdateAccLoan05Tmp2(openDay);
        logger.info("更新利率调整类型结束,返回参数为:[{}]", cmis0101UpdateAccLoan05Tmp2);

        logger.info("重命名创建和删除核心利率信息临时表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_core_rate_info");
        logger.info("重命名创建和删除核心利率信息临时表结束");

        logger.info("插入数据到核心利率信息临时表开始,请求参数为:[{}]", openDay);
        int cmis0101InsertAccLoan06 = cmis0101Mapper.cmis0101InsertAccLoan06(openDay);
        logger.info("插入数据到核心利率信息临时表结束,返回参数为:[{}]", cmis0101InsertAccLoan06);

        logger.info("更新逾期利率开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan06 = cmis0101Mapper.cmis0101UpdateAccLoan06(openDay);
        logger.info("更新逾期利率结束,返回参数为:[{}]", cmis0101UpdateAccLoan06);

        logger.info("更新逾期罚息浮动比开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan07 = cmis0101Mapper.cmis0101UpdateAccLoan07(openDay);
        logger.info("更新逾期罚息浮动比结束,返回参数为:[{}]", cmis0101UpdateAccLoan07);

        logger.info("同步执行利率开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan08 = cmis0101Mapper.cmis0101UpdateAccLoan08(openDay);
        logger.info("同步执行利率结束,返回参数为:[{}]", cmis0101UpdateAccLoan08);

        logger.info("先将台账状态为正常的利息逾期天数字段 更新为0，再由后面的SQL根据核心数据更新该字段开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan09 = cmis0101Mapper.cmis0101UpdateAccLoan09(openDay);
        logger.info("先将台账状态为正常的利息逾期天数字段 更新为0，再由后面的SQL根据核心数据更新该字段结束,返回参数为:[{}]", cmis0101UpdateAccLoan09);
    }

    /**
     * 垫款处理，更新 贷款台账 表中字段
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0101InsertUpdateAccLoan02(String openDay) {
        logger.info("重命名创建和删除数据到贷款借据号和最大期供逾期天数关系表 开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_core_dkzhqg_dkjcsx");
        logger.info("重命名创建和删除数据到贷款借据号和最大期供逾期天数关系表 结束");

        logger.info("插入数据到贷款借据号和最大期供逾期天数关系表 开始,请求参数为:[{}]", openDay);
        int cmis0101InsertTmp = cmis0101Mapper.cmis0101InsertTmp(openDay);
        logger.info("插入数据到贷款借据号和最大期供逾期天数关系表 结束,返回参数为:[{}]", cmis0101InsertTmp);

        logger.info("更新信贷逾期天数为 期供逾期天数 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan10 = cmis0101Mapper.cmis0101UpdateAccLoan10(openDay);
        logger.info("更新信贷逾期天数为 期供逾期天数 结束,返回参数为:[{}]", cmis0101UpdateAccLoan10);
    }

    /**
     * 垫款处理，更新 贷款台账 表中字段
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0101InsertUpdateAccLoan03(String openDay) {
        logger.info("清空合同与申请加工临时表 开始,请求参数为:[{}]", openDay);
        cmis0101Mapper.cmis0101TruncateTmp01();
        logger.info("清空合同与申请加工临时表 结束");

        logger.info("插入数据到合同与申请加工临时表开始,请求参数为:[{}]", openDay);
        int cmis0101InsertAccLoan01 = cmis0101Mapper.cmis0101InsertAccLoan01(openDay);
        logger.info("插入数据到合同与申请加工临时表结束,返回参数为:[{}]", cmis0101InsertAccLoan01);

        logger.info("更新合同申请信息表中合同号为空的数据开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan11 = cmis0101Mapper.cmis0101UpdateAccLoan11(openDay);
        logger.info("更新合同申请信息表中合同号为空的数据结束,返回参数为:[{}]", cmis0101UpdateAccLoan11);

        logger.info("更新合同主表里合同汇率为空的数据开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan12 = cmis0101Mapper.cmis0101UpdateAccLoan12(openDay);
        logger.info("更新合同主表里合同汇率为空的数据结束,返回参数为:[{}]", cmis0101UpdateAccLoan12);

        logger.info("更新合同申请信息表中申请汇率为空的数据开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan13 = cmis0101Mapper.cmis0101UpdateAccLoan13(openDay);
        logger.info("更新合同申请信息表中申请汇率为空的数据结束,返回参数为:[{}]", cmis0101UpdateAccLoan13);

        logger.info("更新复利利率开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan14 = cmis0101Mapper.cmis0101UpdateAccLoan14(openDay);
        logger.info("更新复利利率结束,返回参数为:[{}]", cmis0101UpdateAccLoan14);

        logger.info("更新复利浮动比开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan15 = cmis0101Mapper.cmis0101UpdateAccLoan15(openDay);
        logger.info("更新复利浮动比结束,返回参数为:[{}]", cmis0101UpdateAccLoan15);

        logger.info("更新正常本金和逾期本金开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccLoan16 = cmis0101Mapper.cmis0101UpdateAccLoan16(openDay);
        logger.info("更新正常本金和逾期本金结束,返回参数为:[{}]", cmis0101UpdateAccLoan16);

        logger.info("更新台账状态 、借据起始日、到期日、借据余额 、结清日期开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan01 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan01(openDay);
        logger.info("更新台账状态 、借据起始日、到期日、借据余额 、结清日期结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan01);

        logger.info("更新台账折人民币金额、汇率开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan01A = cmis0101Mapper.cmis0101UpdateAccEntrustLoan01A(openDay);
        logger.info("更新台账折人民币金额、汇率结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan01A);

        logger.info("贷款台账人民币余额补充更新开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan02A = cmis0101Mapper.cmis0101UpdateAccEntrustLoan02A(openDay);
        logger.info("贷款台账人民币余额补充更新结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan02A);

        logger.info("借据结清更新信贷台账状态开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan02 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan02(openDay);
        logger.info("借据结清更新信贷台账状态结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan02);

        logger.info("借据核销更新信贷台账状态开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan03A = cmis0101Mapper.cmis0101UpdateAccEntrustLoan03A(openDay);
        logger.info("借据核销更新信贷台账状态结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan03A);

        logger.info("欠息、罚息开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan03 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan03(openDay);
        logger.info("欠息、罚息结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan03);

        logger.info("更新还款方式开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan04 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan04(openDay);
        logger.info("更新还款方式结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan04);

        logger.info("同步利率调整方式开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan05Mode = cmis0101Mapper.cmis0101UpdateAccEntrustLoan05Mode(openDay);
        logger.info("同步利率调整方式结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan05Mode);

        logger.info("同步利率调整选项 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan05Type = cmis0101Mapper.cmis0101UpdateAccEntrustLoan05Type(openDay);
        logger.info("同步利率调整选项 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan05Type);

        logger.info("更新逾期利率 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan06 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan06(openDay);
        logger.info("更新逾期利率 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan06);

        logger.info("更新逾期罚息浮动比 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan07 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan07(openDay);
        logger.info("更新逾期罚息浮动比 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan07);

        logger.info("同步执行利率 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan08 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan08(openDay);
        logger.info("同步执行利率 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan08);

        logger.info("更新信贷台账状态正常的逾期天数为0 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan09 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan09(openDay);
        logger.info("更新信贷台账状态正常的逾期天数为0 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan09);
    }

    /**
     * 垫款处理，更新 贷款台账 表中字段
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0101InsertUpdateAccLoan04(String openDay) {
        logger.info("重命名创建和删除临时表-贷款借据号和最大期供逾期天数关系表 开始");
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_core_dkzhqg_dkjcsx");
        logger.info("重命名创建和删除临时表-贷款借据号和最大期供逾期天数关系表 结束");

        logger.info("加工临时表-贷款借据号和最大期供逾期天数关系表 开始,请求参数为:[{}]", openDay);
        int insertEntrustTmpCoreDkzhqgDkjcsx = cmis0101Mapper.insertEntrustTmpCoreDkzhqgDkjcsx(openDay);
        logger.info("加工临时表-贷款借据号和最大期供逾期天数关系表 结束,请求参数为:[{}]", insertEntrustTmpCoreDkzhqgDkjcsx);

        logger.info("更新信贷逾期天数为期供逾期天数 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan10 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan10(openDay);
        logger.info("更新信贷逾期天数为期供逾期天数 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan10);
    }

    /**
     * 垫款处理，更新 贷款台账 表中字段
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0101InsertUpdateAccLoan05(String openDay) {
        logger.info("删除合同与申请加工临时表 开始");
        cmis0101Mapper.deleteEntrustTmpIqpCtr();
        logger.info("删除合同与申请加工临时表 结束");

        logger.info("插入数据到合同与申请加工临时表 开始,请求参数为:[{}]", openDay);
        int cmis0101InsertAccEntrustLoan01 = cmis0101Mapper.cmis0101InsertAccEntrustLoan01(openDay);
        logger.info("插入数据到合同与申请加工临时表 结束,请求参数为:[{}]", cmis0101InsertAccEntrustLoan01);

        logger.info("更新合同申请信息表中合同号为空的数据 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan11 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan11(openDay);
        logger.info("更新合同申请信息表中合同号为空的数据 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan11);

//        logger.info("更新合同主表里合同汇率为空的数据 开始,请求参数为:[{}]", openDay);
//        int cmis0101UpdateAccEntrustLoan12 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan12(openDay);
//        logger.info("更新合同主表里合同汇率为空的数据 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan12);

        logger.info("更新合同申请信息表中申请汇率为空的数据 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan13 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan13(openDay);
        logger.info("更新合同申请信息表中申请汇率为空的数据 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan13);

        logger.info("更新复利利率 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan14 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan14(openDay);
        logger.info("更新复利利率 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan14);

        logger.info("更新复利浮动比 开始,请求参数为:[{}]", openDay);
        int cmis0101UpdateAccEntrustLoan15 = cmis0101Mapper.cmis0101UpdateAccEntrustLoan15(openDay);
        logger.info("更新复利浮动比 结束,请求参数为:[{}]", cmis0101UpdateAccEntrustLoan15);
    }

}
