package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0426</br>
 * 任务名称：加工任务-业务处理-风险任务自动分类  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0426Mapper {



    /**
     * 更新贷款台账表[逾期达到61天及以上，且当前分类状态是正常、关注，客户名下所有消费贷款自动认定为次级]
     *
     * @param openDay
     * @return
     */
    int updateAccLoan01(@Param("openDay") String openDay);

    /**
     * 更新贷款台账表[逾期达到31天～60天，且当前分类状态是正常，客户名下所有消费贷款自动认定为关注]
     *
     * @param openDay
     * @return
     */
    int updateAccLoan02(@Param("openDay") String openDay);

    /**
     * 更新贷款台账表[公司客户的低风险业务由系统自动认定为正常 1]
     *
     * @param openDay
     * @return
     */
    int updateAccLoan03(@Param("openDay") String openDay);

    /**
     * 更新贷款台账表[个人低风险业务由系统自动认定为正常]
     *
     * @param openDay
     * @return
     */
    int updateAccLoan04(@Param("openDay") String openDay);

    /**
     * 清空 临时表-贷款台账6个月内有最新有效分类结果信息
     *
     * @return
     */
   // int truncateTmpAccLoan180dCus();

    /**
     * 插入 临时表-贷款台账6个月内有最新有效分类结果信息
     *
     * @param lastOpenDay
     * @return
     */
   // int insertTmpAccLoan180dCus(@Param("lastOpenDay") String lastOpenDay);

    /**
     * 更新贷款台账表[客户名下存在6个月内有最新有效分类结果，则系统直接按照当前有效分类结果进行认定]
     *
     * @param lastOpenDay
     * @return
     */
 //   int updateAccLoan05(@Param("lastOpenDay") String lastOpenDay);

    /**
     * 客户名下 没有存量的有效贷款，则默认正常
     *
     * @param openDay
     * @return
     */
    int updateAccLoanTenClass(@Param("openDay") String openDay);

    /**
     * 客户名下 没有存量的有效贷款，则默认正常
     *
     * @param openDay
     * @return
     */
    int updateAccLoanFiveClass(@Param("openDay") String openDay);
}
