package cn.com.yusys.yusp.spring.batch.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

/**
 * SpringBatch启动配置类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午11:56:54
 */
@Configuration
@EnableBatchProcessing
public class CommonBatchConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(CommonBatchConfiguration.class);

    @Bean
    public SimpleJobLauncher customJobLauncher(DataSource dataSource, PlatformTransactionManager transactionManager) throws Exception {
        JobRepositoryFactoryBean jobRepositoryFactoryBean = new JobRepositoryFactoryBean();
        SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        jobRepositoryFactoryBean.setDataSource(dataSource);
        jobRepositoryFactoryBean.setTransactionManager(transactionManager);
        jobRepositoryFactoryBean.setIsolationLevelForCreate("ISOLATION_READ_COMMITTED");
        jobRepositoryFactoryBean.setDatabaseType("mysql");
        JobRepository jobRepository = jobRepositoryFactoryBean.getObject();
        simpleJobLauncher.setJobRepository(jobRepository);
        try {
            simpleJobLauncher.afterPropertiesSet();
            logger.info("创建customJobLauncher成功");
        } catch (Exception e) {
            logger.error("创建customJobLauncher异常,异常信息为:[{}]", e.getMessage());
        }
        return simpleJobLauncher;
    }

}
