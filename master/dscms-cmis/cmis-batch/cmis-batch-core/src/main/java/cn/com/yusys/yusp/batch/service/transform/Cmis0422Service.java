package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0422Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0422</br>
 * 任务名称：加工任务-贷后管理-个人经营性风险分类  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0422Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0422Service.class);

    @Autowired
    private Cmis0422Mapper cmis0422Mapper;

    /**
     * 插入贷后检查任务表和风险分类借据信息
     *
     * @param openDay
     */
    public void cmis0422InsertPspRisk(String openDay) {
        logger.info(" 1.系统在每季季末月（即3 月、6 月、9 月、12月）的20 日当天生成；");
        logger.info(" 2. 任务完成日期 ：3月31、6月30日，9月30日，12月31日；");
        logger.info(" 3. 客户授信金额（不含低风险）>50万，按照大额跑批矩阵进行分类：客户授信金额（不含低风险）<=50万,按照小额跑批矩阵进行分类；");
        logger.info(" 4. 跑批矩阵的分类结果与上次分类结果不一致，生成风险分类任务；");
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspRisk01 = cmis0422Mapper.insertPspRisk01(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspRisk01);

    }
}
