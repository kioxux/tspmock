package cn.com.yusys.yusp.batch.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Shell相关的工具类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月26日 下午11:56:54
 */
public class ShellUtils {
    private static final Logger logger = LoggerFactory.getLogger(ShellUtils.class);
    private static final int THREAD_SLEEP_TIME = 10;
    private static final int DEFAULT_WAIT_TIME = 20 * 60 * 1000;

    /**
     * 运行Shell脚本
     *
     * @param cmd
     * @throws IOException
     */
    public static void runShellByProcessBuilder(String cmd) {
        String[] command = new String[]{"/bin/sh", "-c", cmd};
        StackTraceElement stackTraceElement = null;//异常堆栈信息，方便查找问题
        try {
            logger.info("运行Shell脚本开始,请求参数为[{}]", cmd);
            ProcessBuilder processBuilder = new ProcessBuilder(command);
            processBuilder.redirectErrorStream(true);// 将错误输出流转移到标准输出流中
            Process process = processBuilder.start();
            String dataMsg = reader(process.getInputStream());
            int resp = process.waitFor();
            logger.info("运行Shell脚本结束，返回对象为[{}]", resp);
        } catch (Exception e) {
            logger.error("运行Shell脚本异常,异常信息为:[{}]", e.getMessage());
            stackTraceElement = e.getStackTrace()[0];
            logger.error("运行Shell脚本异常,异常堆栈信息为:[文件名为{},行号为{}]", stackTraceElement.getFileName(), stackTraceElement.getLineNumber());
        }
    }

    /**
     * 数据读取操作
     *
     * @param inputStream
     * @return
     */
    private static String reader(InputStream inputStream) {
        StringBuilder dataMsg = new StringBuilder();
        StackTraceElement stackTraceElement = null;//异常堆栈信息，方便查找问题
        try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                logger.info("数据读取部分信息为:[{}]", line);
                dataMsg.append(line);
            }
        } catch (IOException e) {
            logger.error("数据读取操作异常,异常信息为:[{}]", e.getMessage());
            stackTraceElement = e.getStackTrace()[0];
            logger.error("数据读取操作异常,异常堆栈信息为:[文件名为{},行号为{}]", stackTraceElement.getFileName(), stackTraceElement.getLineNumber());
        }
        return dataMsg.toString();
    }

}
