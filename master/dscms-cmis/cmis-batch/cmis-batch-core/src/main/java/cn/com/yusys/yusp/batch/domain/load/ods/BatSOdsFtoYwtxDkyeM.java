/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.ods;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSOdsFtoYwtxDkyeM
 * @类描述: bat_s_ods_fto_ywtx_dkye_m数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-25 16:35:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ods_fto_ywtx_dkye_m")
public class BatSOdsFtoYwtxDkyeM extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 日期 **/
	@Id
	@Column(name = "ACCTDT")
	private String acctdt;
	
	/** 机构号 **/
	@Id
	@Column(name = "BRCHNO")
	private String brchno;
	
	/** 条线 **/
	@Id
	@Column(name = "STLINE")
	private String stline;
	
	/** 贷款余额 **/
	@Id
	@Column(name = "LOANBL")
	private java.math.BigDecimal loanbl;
	
	
	/**
	 * @param acctdt
	 */
	public void setAcctdt(String acctdt) {
		this.acctdt = acctdt;
	}
	
    /**
     * @return acctdt
     */
	public String getAcctdt() {
		return this.acctdt;
	}
	
	/**
	 * @param brchno
	 */
	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}
	
    /**
     * @return brchno
     */
	public String getBrchno() {
		return this.brchno;
	}
	
	/**
	 * @param stline
	 */
	public void setStline(String stline) {
		this.stline = stline;
	}
	
    /**
     * @return stline
     */
	public String getStline() {
		return this.stline;
	}
	
	/**
	 * @param loanbl
	 */
	public void setLoanbl(java.math.BigDecimal loanbl) {
		this.loanbl = loanbl;
	}
	
    /**
     * @return loanbl
     */
	public java.math.BigDecimal getLoanbl() {
		return this.loanbl;
	}


}