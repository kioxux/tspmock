package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0212</br>
 * 任务名称：加工任务-额度处理-合作方额度处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0212Mapper {
    /**
     * 更新批复额度分项基础信息
     *
     * @return
     */
    void truncateApprLmtSubBasicInfo04A();

    /**
     * 删除分项占用关系信息加工表
     */
    void truncateTmpDealLmtContRel0212();//add20211014

    /**
     * 插入分项占用关系信息加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpDealLmtContRel0212(@Param("openDay") String openDay);//add20211014

    /**
     * 更新批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo04A(@Param("openDay") String openDay);

    /**
     * 更新批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo04A(@Param("openDay") String openDay);

    /**
     * 清理关系表
     *
     * @return
     */
    void truncateApprLmtSubBasicInfo04B();

    /**
     * 删除合同占用关系信息加工表
     *
     * @return
     */
    void truncateTmpDealContAccRel0212();//add20211014

    /**
     * 插入合同占用关系信息加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpDealContAccRel0212(@Param("openDay") String openDay);//add20211014

    /**
     * 用信余额汇总 合作方分项下关联的合同关联的台账余额
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo04B(@Param("openDay") String openDay);

    /**
     * 用信余额汇总 合作方分项下关联的合同关联的台账余额
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo04B(@Param("openDay") String openDay);


}
