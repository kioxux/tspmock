package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0108</br>
 * 任务名称：加工任务-业务处理-业务合同处理 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0108Mapper {

    /**
     * 插入贷款台账信息表
     */
    int insertTempAccLoan0108(@Param("openDay") String openDay);//addby cjz 20211017

    /**
     * 贷款全部结清时，贷款协议注销
     *
     * @param openDay
     * @return
     */
    int updateCtrLoanCont(@Param("openDay") String openDay);

    /**
     * 贷款全部结清时，贷款协议注销
     *
     * @param openDay
     * @return
     */
    int updateCtrAccpCont(@Param("openDay") String openDay);

    /**
     * 贴现台账全部结清时，贴现协议注销
     *
     * @param openDay
     * @return
     */
    int updateCtrDiscCont(@Param("openDay") String openDay);

    /**
     * 保函台账全部结清时，保函协议注销
     *
     * @param openDay
     * @return
     */
    int updateCtrCvrgCont(@Param("openDay") String openDay);

    /**
     * 信用证台账全部结清时，信用证协议注销
     *
     * @param openDay
     * @return
     */
    int updateCtrTfLocCont(@Param("openDay") String openDay);

    /**
     * 委托贷款台账全部结清时，委托贷款协议注销
     *
     * @param openDay
     * @return
     */
    int updateCtrEntrustLoanCont(@Param("openDay") String openDay);


    /**
     * 贷款合同项下无台账，到期自动注销
     *
     * @param openDay
     * @return
     */
    int updateCtrLoanContByNoLoan(@Param("openDay") String openDay);

    /**
     * 银票合同项下无台账，到期自动注销
     *
     * @param openDay
     * @return
     */
    int updateCtrAccpContByNoAccp(@Param("openDay") String openDay);

    /**
     * 贴现合同项下无台账，到期自动注销
     *
     * @param openDay
     * @return
     */
    int updateCtrAccpContByNoDisc(@Param("openDay") String openDay);

    /**
     * 一般贴现合同没有到期日，若一般贴现合同，项下存在贴现的票，且票据都已结清，则认为合同已注销
     *
     * @param openDay
     * @return
     */
    int updateCtrDiscCont01(@Param("openDay") String openDay);

    /**
     * 保函合同项下无台账，到期自动注销
     *
     * @param openDay
     * @return
     */
    int updateCtrAccpContByNoCvrg(@Param("openDay") String openDay);

    /**
     * 信用证合同项下无台账，到期自动注销
     *
     * @param openDay
     * @return
     */
    int updateCtrAccpContByNoTfLoc(@Param("openDay") String openDay);

    /**
     * 委托贷款合同项下无台账，到期自动注销
     *
     * @param openDay
     * @return
     */
    int updateCtrAccpContByNoEntrustLoan(@Param("openDay") String openDay);

    /**
     * 更新合作方案台账信息中合作方案状态
     *
     * @param openDay
     * @return
     */
    int updateCoopPlanAccInfo(@Param("openDay") String openDay);

    /**
     * 更新合作方协议台账信息中协议状态
     *
     * @param openDay
     * @return
     */
    int updateCoopPartnerAgrAccInfo(@Param("openDay") String openDay);


    /**
     * 加工临时表-加工担保合同与借款合同关联关系
     *
     * @param openDay
     * @return
     */
    int insertTmpGrtGuarBizRstAtstus(@Param("openDay") String openDay);

    /**
     * 更新最高额担保合同状态
     *
     * @param openDay
     * @return
     */
    int updateGrtGuarContByGuarContState(@Param("openDay") String openDay);


    /**
     * 一般担保合同自动注销：与主合同关联的一般担保合同在主合同注销时自动注销
     *
     * @param openDay
     * @return
     */
    int updateGrtGuarContByGuarContState2(@Param("openDay") String openDay);

    /**
     * 最高额授信协议到期注销
     *
     * @param openDay
     * @return
     */
    int updateCtrHighAmtAgrCont(@Param("openDay") String openDay);

    /**
     * 更新iqp_loan_app的approve_status='997'
     *
     * @param openDay
     * @return
     */
    int updateIqpLoanApp01(@Param("openDay") String openDay);

    /**
     * 更新IQP_HIGH_AMT_AGR_APP的approve_status='997'
     *
     * @param openDay
     * @return
     */
    int updateIqpHighAmtAgrApp02(@Param("openDay") String openDay);

    /**
     * 更新IQP_ENTRUST_LOAN_APP的approve_status='997'
     *
     * @param openDay
     * @return
     */
    int updateIqpEntrustLoanApp03(@Param("openDay") String openDay);

}
