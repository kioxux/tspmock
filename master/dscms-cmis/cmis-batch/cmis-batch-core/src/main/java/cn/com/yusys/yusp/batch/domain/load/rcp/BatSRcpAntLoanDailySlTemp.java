/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpAntLoanDailySlTemp
 * @类描述: bat_s_rcp_ant_loan_daily_sl_temp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-10 14:48:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_rcp_ant_loan_daily_sl_temp")
public class BatSRcpAntLoanDailySlTemp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 借呗平台贷款合同号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CONTRACT_NO")
	private String contractNo;
	
	/** 会计日期，格式：YYYYMMdd **/
	@Column(name = "SETTLE_DATE", unique = false, nullable = true, length = 8)
	private String settleDate;
	
	/** 合约状态，正常NORMAL,逾期OVD,结清CLEAR  **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 10)
	private String status;
	
	/** 结清日期，格式：yyyyMMdd **/
	@Column(name = "CLEAR_DATE", unique = false, nullable = true, length = 8)
	private String clearDate;
	
	/** 五级分类标识，正常1，关注2，次级3，可疑4，损失5 **/
	@Column(name = "ASSET_CLASS", unique = false, nullable = true, length = 2)
	private String assetClass;
	
	/** 应计非应计标识，应计0，非应计1 **/
	@Column(name = "ACCRUED_STATUS", unique = false, nullable = true, length = 2)
	private String accruedStatus;
	
	/** 下一还款日期，格式：yyyyMMdd **/
	@Column(name = "NEXT_REPAY_DATE", unique = false, nullable = true, length = 8)
	private String nextRepayDate;
	
	/** 未结清期数 **/
	@Column(name = "UNCLEAR_TERMS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal unclearTerms;
	
	/** 逾期期次数 **/
	@Column(name = "OVD_TERMS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ovdTerms;
	
	/** 本金逾期天数 **/
	@Column(name = "PRIN_OVD_DAYS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal prinOvdDays;
	
	/** 利息逾期天数 **/
	@Column(name = "INT_OVD_DAYS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal intOvdDays;
	
	/** 正常本金余额（单位分），汇总所有正常期次的本金余额 **/
	@Column(name = "PRIN_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal prinBal;
	
	/** 逾期本金余额（单位分），汇总所有逾期期次的本金余额 **/
	@Column(name = "OVD_PRIN_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ovdPrinBal;
	
	/** 正常利息余额（单位分），汇总所有正常期次的利息余额 **/
	@Column(name = "INT_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal intBal;
	
	/** 逾期利息余额（单位分），汇总所有逾期期次的利息余额 **/
	@Column(name = "OVD_INT_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ovdIntBal;
	
	/** 逾期本金罚息余额（单位分），汇总所有期次的逾本罚余额 **/
	@Column(name = "OVD_PRIN_PNLT_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ovdPrinPnltBal;
	
	/** 逾期利息罚息余额（单位分），汇总所有期次的逾利罚余额 **/
	@Column(name = "OVD_INT_PNLT_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ovdIntPnltBal;
	
	/** 核销标识，已核销为Y，否则为N **/
	@Column(name = "WRITE_OFF", unique = false, nullable = true, length = 2)
	private String writeOff;
	
	/** 录入日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 数据文件 **/
	@Column(name = "RCP_DATA_DATE", unique = false, nullable = true, length = 10)
	private String rcpDataDate;
	
	/** 行政区划代码 **/
	@Column(name = "REGION_CODE", unique = false, nullable = true, length = 8)
	private String regionCode;
	
	/** 业务类型 **/
	@Column(name = "BSN_TYPE", unique = false, nullable = true, length = 8)
	private String bsnType;
	
	/** 扩展信息,使用类json串格式存储，key1:value1;key2:value2|value3。如果没有值value给null **/
	@Column(name = "ext_info", unique = false, nullable = true, length = 1024)
	private String extInfo;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param settleDate
	 */
	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}
	
    /**
     * @return settleDate
     */
	public String getSettleDate() {
		return this.settleDate;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param clearDate
	 */
	public void setClearDate(String clearDate) {
		this.clearDate = clearDate;
	}
	
    /**
     * @return clearDate
     */
	public String getClearDate() {
		return this.clearDate;
	}
	
	/**
	 * @param assetClass
	 */
	public void setAssetClass(String assetClass) {
		this.assetClass = assetClass;
	}
	
    /**
     * @return assetClass
     */
	public String getAssetClass() {
		return this.assetClass;
	}
	
	/**
	 * @param accruedStatus
	 */
	public void setAccruedStatus(String accruedStatus) {
		this.accruedStatus = accruedStatus;
	}
	
    /**
     * @return accruedStatus
     */
	public String getAccruedStatus() {
		return this.accruedStatus;
	}
	
	/**
	 * @param nextRepayDate
	 */
	public void setNextRepayDate(String nextRepayDate) {
		this.nextRepayDate = nextRepayDate;
	}
	
    /**
     * @return nextRepayDate
     */
	public String getNextRepayDate() {
		return this.nextRepayDate;
	}
	
	/**
	 * @param unclearTerms
	 */
	public void setUnclearTerms(java.math.BigDecimal unclearTerms) {
		this.unclearTerms = unclearTerms;
	}
	
    /**
     * @return unclearTerms
     */
	public java.math.BigDecimal getUnclearTerms() {
		return this.unclearTerms;
	}
	
	/**
	 * @param ovdTerms
	 */
	public void setOvdTerms(java.math.BigDecimal ovdTerms) {
		this.ovdTerms = ovdTerms;
	}
	
    /**
     * @return ovdTerms
     */
	public java.math.BigDecimal getOvdTerms() {
		return this.ovdTerms;
	}
	
	/**
	 * @param prinOvdDays
	 */
	public void setPrinOvdDays(java.math.BigDecimal prinOvdDays) {
		this.prinOvdDays = prinOvdDays;
	}
	
    /**
     * @return prinOvdDays
     */
	public java.math.BigDecimal getPrinOvdDays() {
		return this.prinOvdDays;
	}
	
	/**
	 * @param intOvdDays
	 */
	public void setIntOvdDays(java.math.BigDecimal intOvdDays) {
		this.intOvdDays = intOvdDays;
	}
	
    /**
     * @return intOvdDays
     */
	public java.math.BigDecimal getIntOvdDays() {
		return this.intOvdDays;
	}
	
	/**
	 * @param prinBal
	 */
	public void setPrinBal(java.math.BigDecimal prinBal) {
		this.prinBal = prinBal;
	}
	
    /**
     * @return prinBal
     */
	public java.math.BigDecimal getPrinBal() {
		return this.prinBal;
	}
	
	/**
	 * @param ovdPrinBal
	 */
	public void setOvdPrinBal(java.math.BigDecimal ovdPrinBal) {
		this.ovdPrinBal = ovdPrinBal;
	}
	
    /**
     * @return ovdPrinBal
     */
	public java.math.BigDecimal getOvdPrinBal() {
		return this.ovdPrinBal;
	}
	
	/**
	 * @param intBal
	 */
	public void setIntBal(java.math.BigDecimal intBal) {
		this.intBal = intBal;
	}
	
    /**
     * @return intBal
     */
	public java.math.BigDecimal getIntBal() {
		return this.intBal;
	}
	
	/**
	 * @param ovdIntBal
	 */
	public void setOvdIntBal(java.math.BigDecimal ovdIntBal) {
		this.ovdIntBal = ovdIntBal;
	}
	
    /**
     * @return ovdIntBal
     */
	public java.math.BigDecimal getOvdIntBal() {
		return this.ovdIntBal;
	}
	
	/**
	 * @param ovdPrinPnltBal
	 */
	public void setOvdPrinPnltBal(java.math.BigDecimal ovdPrinPnltBal) {
		this.ovdPrinPnltBal = ovdPrinPnltBal;
	}
	
    /**
     * @return ovdPrinPnltBal
     */
	public java.math.BigDecimal getOvdPrinPnltBal() {
		return this.ovdPrinPnltBal;
	}
	
	/**
	 * @param ovdIntPnltBal
	 */
	public void setOvdIntPnltBal(java.math.BigDecimal ovdIntPnltBal) {
		this.ovdIntPnltBal = ovdIntPnltBal;
	}
	
    /**
     * @return ovdIntPnltBal
     */
	public java.math.BigDecimal getOvdIntPnltBal() {
		return this.ovdIntPnltBal;
	}
	
	/**
	 * @param writeOff
	 */
	public void setWriteOff(String writeOff) {
		this.writeOff = writeOff;
	}
	
    /**
     * @return writeOff
     */
	public String getWriteOff() {
		return this.writeOff;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param rcpDataDate
	 */
	public void setRcpDataDate(String rcpDataDate) {
		this.rcpDataDate = rcpDataDate;
	}
	
    /**
     * @return rcpDataDate
     */
	public String getRcpDataDate() {
		return this.rcpDataDate;
	}
	
	/**
	 * @param regionCode
	 */
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	
    /**
     * @return regionCode
     */
	public String getRegionCode() {
		return this.regionCode;
	}
	
	/**
	 * @param bsnType
	 */
	public void setBsnType(String bsnType) {
		this.bsnType = bsnType;
	}
	
    /**
     * @return bsnType
     */
	public String getBsnType() {
		return this.bsnType;
	}
	
	/**
	 * @param extInfo
	 */
	public void setExtInfo(String extInfo) {
		this.extInfo = extInfo;
	}
	
    /**
     * @return extInfo
     */
	public String getExtInfo() {
		return this.extInfo;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}