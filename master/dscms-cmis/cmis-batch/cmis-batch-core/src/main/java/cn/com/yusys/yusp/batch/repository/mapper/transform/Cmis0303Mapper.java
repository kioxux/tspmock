package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0303</br>
 * 任务名称：增享贷名单  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0303Mapper {



    /**
     *  申请时间超过30天，办理状态  02	  待办、 07	失效、 04	拒绝 、01	日终准入拒绝、   00	初始化 删除掉重新跑批
     *
     * @param openDay
     * @return
     */
    int deleteCusLstZxdByStatus(@Param("openDay") String openDay);
    /**
     * 插入临时表_贷款台账信息表_客户号
     *
     * @param openDay
     * @return
     */
    int insertTmpAccLoanCusId(@Param("openDay") String openDay);


    /**
     * 插入临时表担保合同和合同的关系
     *
     * @param openDay
     * @return
     */
    int insertTmpGrtGuarContRel(@Param("openDay") String openDay);

    /**
     * 插入临时表-担保合同关联信息表
     *
     * @param openDay
     * @return
     */
    int insertTmpGrtGuarBizRel(@Param("openDay") String openDay);
    /**
     * 插入临时表_配偶客户信息表
     *
     * @param openDay
     * @return
     */
    void deleteTmpGrtGuarBizRel(@Param("openDay") String openDay);


    /**
     * 删除押品统一编号重复临时表_配偶客户信息表开始
     *
     * @param openDay
     * @return
     */
    int insertTmpCusIndivContact(@Param("openDay") String openDay);



    /**
     * 插入临时表-授信调查批复表
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtCrdReplyInfo(@Param("openDay") String openDay);


    /**
     * 插入临时表-增享贷名单信息
     *
     * @param openDay
     * @return
     */
    int insertTmpCusLstZxd(@Param("openDay") String openDay);



    /**
     * 插入临时表-系统用户表
     *
     * @param openDay
     * @return
     */
    int insertTmpSmUser(@Param("openDay") String openDay);



    /**
     * 增享贷名单信息
     *
     * @param lastOpenDay
     * @return
     */
    int insertCusLstZxd01(@Param("lastOpenDay") String lastOpenDay);

    int insertCusLstZxd0(@Param("openDay") String openDay);
    /**
     * 押品 自有 ，非自有的删除
     *
     * @param lastOpenDay
     * @return
     */
    int deleteCusLstZxdForOwner(@Param("openDay") String openDay);

    /**
     * 借款人是否存在逾期
     *
     * @param openDay
     * @return
     */
    int insertCusLstZxd02(@Param("openDay") String openDay);

    int truncateTable1(@Param("openDay") String openDay);
    int truncateTable2(@Param("openDay") String openDay);
    int insertTmpLmtCustZxd1(@Param("openDay") String openDay);
    int insertTmpLmtCustZxd2(@Param("openDay") String openDay);
    /**
     * 借款人借据汇总BILL_NO_ALL
     *
     * @param openDay
     * @return
     */
    int insertCusLstZxd03(@Param("openDay") String openDay);

    /**
     * 清理配偶是否逾期临时表
     * @param openDay
     * @return
     */
    int truncateTable3(@Param("openDay") String openDay);

    /**
     * 加工是否逾期临时表
     * @param openDay
     * @return
     */
    int insertTmp3(@Param("openDay") String openDay);
    /**
     *配偶是否存在逾期
     *
     * @param openDay
     * @return
     */
    int insertCusLstZxd04(@Param("openDay") String openDay);

    /**
     * 配偶借据汇总
     *
     * @param openDay
     * @return
     */
    int insertCusLstZxd05(@Param("openDay") String openDay);

    int truncateTmpBatKlnbDkzhqg(@Param("openDay") String openDay);
    int insertTmpBatKlnbDkzhqg(@Param("openDay") String openDay);
    int truncateTable4(@Param("openDay") String openDay);
    int insertTmp4(@Param("openDay") String openDay);
    /**
     * 增享贷名单内的借据已还款期数
     *
     * @param openDay
     * @return
     */
    int insertCusLstZxd06(@Param("openDay") String openDay);

}
