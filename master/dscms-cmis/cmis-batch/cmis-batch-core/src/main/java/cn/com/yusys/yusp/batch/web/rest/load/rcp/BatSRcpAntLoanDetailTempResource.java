/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.rcp;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpAntLoanDetailTemp;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpAntLoanDetailTempService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpAntLoanDetailTempResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:02:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsrcpantloandetailtemp")
public class BatSRcpAntLoanDetailTempResource {
    @Autowired
    private BatSRcpAntLoanDetailTempService batSRcpAntLoanDetailTempService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSRcpAntLoanDetailTemp>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSRcpAntLoanDetailTemp> list = batSRcpAntLoanDetailTempService.selectAll(queryModel);
        return new ResultDto<List<BatSRcpAntLoanDetailTemp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSRcpAntLoanDetailTemp>> index(QueryModel queryModel) {
        List<BatSRcpAntLoanDetailTemp> list = batSRcpAntLoanDetailTempService.selectByModel(queryModel);
        return new ResultDto<List<BatSRcpAntLoanDetailTemp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{contractNo}")
    protected ResultDto<BatSRcpAntLoanDetailTemp> show(@PathVariable("contractNo") String contractNo) {
        BatSRcpAntLoanDetailTemp batSRcpAntLoanDetailTemp = batSRcpAntLoanDetailTempService.selectByPrimaryKey(contractNo);
        return new ResultDto<BatSRcpAntLoanDetailTemp>(batSRcpAntLoanDetailTemp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSRcpAntLoanDetailTemp> create(@RequestBody BatSRcpAntLoanDetailTemp batSRcpAntLoanDetailTemp) throws URISyntaxException {
        batSRcpAntLoanDetailTempService.insert(batSRcpAntLoanDetailTemp);
        return new ResultDto<BatSRcpAntLoanDetailTemp>(batSRcpAntLoanDetailTemp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSRcpAntLoanDetailTemp batSRcpAntLoanDetailTemp) throws URISyntaxException {
        int result = batSRcpAntLoanDetailTempService.update(batSRcpAntLoanDetailTemp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{contractNo}")
    protected ResultDto<Integer> delete(@PathVariable("contractNo") String contractNo) {
        int result = batSRcpAntLoanDetailTempService.deleteByPrimaryKey(contractNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSRcpAntLoanDetailTempService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
