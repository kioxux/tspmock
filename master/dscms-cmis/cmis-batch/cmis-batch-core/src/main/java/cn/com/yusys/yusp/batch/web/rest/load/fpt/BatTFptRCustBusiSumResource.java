/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.fpt;

import cn.com.yusys.yusp.batch.domain.load.fpt.BatTFptRCustBusiSum;
import cn.com.yusys.yusp.batch.service.load.fpt.BatTFptRCustBusiSumService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTFptRCustBusiSumResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-12 10:11:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battfptrcustbusisum")
public class BatTFptRCustBusiSumResource {
    @Autowired
    private BatTFptRCustBusiSumService batTFptRCustBusiSumService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTFptRCustBusiSum>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTFptRCustBusiSum> list = batTFptRCustBusiSumService.selectAll(queryModel);
        return new ResultDto<List<BatTFptRCustBusiSum>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTFptRCustBusiSum>> index(QueryModel queryModel) {
        List<BatTFptRCustBusiSum> list = batTFptRCustBusiSumService.selectByModel(queryModel);
        return new ResultDto<List<BatTFptRCustBusiSum>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{custId}")
    protected ResultDto<BatTFptRCustBusiSum> show(@PathVariable("custId") String custId) {
        BatTFptRCustBusiSum batTFptRCustBusiSum = batTFptRCustBusiSumService.selectByPrimaryKey(custId);
        return new ResultDto<BatTFptRCustBusiSum>(batTFptRCustBusiSum);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTFptRCustBusiSum> create(@RequestBody BatTFptRCustBusiSum batTFptRCustBusiSum) throws URISyntaxException {
        batTFptRCustBusiSumService.insert(batTFptRCustBusiSum);
        return new ResultDto<BatTFptRCustBusiSum>(batTFptRCustBusiSum);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTFptRCustBusiSum batTFptRCustBusiSum) throws URISyntaxException {
        int result = batTFptRCustBusiSumService.update(batTFptRCustBusiSum);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{custId}")
    protected ResultDto<Integer> delete(@PathVariable("custId") String custId) {
        int result = batTFptRCustBusiSumService.deleteByPrimaryKey(custId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batTFptRCustBusiSumService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
