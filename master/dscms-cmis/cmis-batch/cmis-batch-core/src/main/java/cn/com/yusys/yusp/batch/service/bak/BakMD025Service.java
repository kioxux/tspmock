package cn.com.yusys.yusp.batch.service.bak;

import cn.com.yusys.yusp.batch.repository.mapper.bak.BakMD025Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKMD025</br>
 * 任务名称：批后备份月表日表任务-备份担保合同表[GRT_GUAR_CONT]  </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
@Service
@Transactional
public class BakMD025Service {
    private static final Logger logger = LoggerFactory.getLogger(BakMD025Service.class);

    @Autowired
    private BakMD025Mapper bakMD025Mapper;

    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 重命名创建和删除当天的[担保合同表[GRT_GUAR_CONT]]数据
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void bakMD025DeleteDGrtGuarCont(String openDay) {

        logger.info("重命名创建和删除当天的[担保合同表[TMP_DAF_GRT_GUAR_CONT]]数据开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","TMP_DAF_GRT_GUAR_CONT");
        logger.info("重命名创建和删除当天的[担保合同表[TMP_DAF_GRT_GUAR_CONT]]数据结束");

//        logger.info("查询待删除当天的[担保合同表[GRT_GUAR_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
//        int queryMD025DeleteOpenDayGrtGuarContCounts = bakMD025Mapper.queryMD025DeleteOpenDayGrtGuarContCounts(openDay);
//        // 由于TDSQL不支持ceiling函数，此处在java中处理
//        BigDecimal times = new BigDecimal(queryMD025DeleteOpenDayGrtGuarContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
//        queryMD025DeleteOpenDayGrtGuarContCounts = times.intValue();
//        logger.info("查询待删除当天的[担保合同表[GRT_GUAR_CONT]]数据总次数结束,返回参数为:[{}]", queryMD025DeleteOpenDayGrtGuarContCounts);
//        // 删除当天的数据
//        for (int i = 0; i < queryMD025DeleteOpenDayGrtGuarContCounts; i++) {
//            logger.info("第[" + i + "]次删除当天的[担保合同表[GRT_GUAR_CONT]]数据开始,请求参数为:[{}]", openDay);
//            int bakMD025DeleteDGrtGuarCont = bakMD025Mapper.bakMD025DeleteDGrtGuarCont(openDay);
//            logger.info("第[" + i + "]次删除当天的[担保合同表[GRT_GUAR_CONT]]数据结束,返回参数为:[{}]", bakMD025DeleteDGrtGuarCont);
//        }
    }

    /**
     * 删除当月的[担保合同表[GRT_GUAR_CONT]]数据
     *
     * @param openDay
     */
    public void bakMD025DeleteMGrtGuarCont(String openDay) {
        logger.info("查询当月的[担保合同表[GRT_GUAR_CONT]]数据开始,请求参数为:[{}]", openDay);
        int queryMD0025DeleteGrtGuarContCont = bakMD025Mapper.queryMD0025DeleteGrtGuarContCont(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(queryMD0025DeleteGrtGuarContCont).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        queryMD0025DeleteGrtGuarContCont = times.intValue();
        logger.info("查询当月的[担保合同表[GRT_GUAR_CONT]]数据结束,返回参数为:[{}]", queryMD0025DeleteGrtGuarContCont);
        // 删除当月的数据
        for (int i = 0; i < queryMD0025DeleteGrtGuarContCont; i++) {
            logger.info("第[" + i + "]次删除当月的[担保合同表[GRT_GUAR_CONT]]数据开始,请求参数为:[{}]", openDay);
            int bakMD025DeleteMGrtGuarCont = bakMD025Mapper.bakMD025DeleteMGrtGuarCont(openDay);
            logger.info("第[" + i + "]次删除当月的[担保合同表[GRT_GUAR_CONT]]数据结束,返回参数为:[{}]", bakMD025DeleteMGrtGuarCont);
        }
    }

    /**
     * 备份当天的数据
     *
     * @param openDay
     */
    public void bakMD025InsertD(String openDay) {
        // 备份当天的数据
        logger.info("备份当天的[担保合同表[GRT_GUAR_CONT]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakMD025Mapper.insertD(openDay);
        logger.info("备份当天的[担保合同表[GRT_GUAR_CONT]]数据结束,返回参数为:[{}]", insertCurrent);
    }

    /**
     * 备份当月的数据
     *
     * @param openDay
     */
    public void bakMD025InsertM(String openDay) {
        logger.info("备份当月的[担保合同表[GRT_GUAR_CONT]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakMD025Mapper.insertM(openDay);
        logger.info("备份当月的[担保合同表[GRT_GUAR_CONT]]数据结束,返回参数为:[{}]", insertCurrent);
    }


    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakMEqualsOriginal(String openDay) {
        String nextOpenDay = DateUtils.addDay(openDay, "yyyy-MM-dd", 1); // 切日后营业日期
        logger.info("切日后营业日期为:[{}]", nextOpenDay);
        Date nextOpenDayDate = DateUtils.parseDateByDef(nextOpenDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
        int day = DateUtils.getMonthDay(nextOpenDayDate);// 获取当月中的天
        logger.info("获取当月中的天为:[{}]", day);

        if (day == 1) {
            logger.info("查询当月备份的[担保合同表[TMP_M_GRT_GUAR_CONT]]数据总条数开始,请求参数为:[{}]", openDay);
            int bakMCounts = bakMD025Mapper.queryBakMCounts(openDay);
            logger.info("查询当月备份的[担保合同表[TMP_M_GRT_GUAR_CONT]]数据总条数结束,返回参数为:[{}]", bakMCounts);

            logger.info("查询当月的[担保合同表[GRT_GUAR_CONT]]原表数据总条数开始,请求参数为:[{}]", openDay);
            int originalCounts = bakMD025Mapper.queryOriginalCounts(openDay);
            logger.info("查询当月的[担保合同表[GRT_GUAR_CONT]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

            if (bakMCounts != originalCounts) {
                logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakMCounts, originalCounts);
                throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakMCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
            } else {
                logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakMCounts, originalCounts);
            }
        }
    }


    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakDEqualsOriginal(String openDay) {
        logger.info("查询当天备份的[权证台账[TMP_DAF_GRT_GUAR_CONT]]数据总条数开始,请求参数为:[{}]", openDay);
        int bakDCounts = tableUtilsService.coutBakDafTableLines("cmis_biz", "TMP_DAF_GRT_GUAR_CONT");
        logger.info("查询当天备份的[权证台账[TMP_DAF_GRT_GUAR_CONT]]数据总条数结束,返回参数为:[{}]", bakDCounts);

        logger.info("查询当天的[权证台账 [GRT_GUAR_CONT]]原表数据总条数开始,请求参数为:[{}]", openDay);
        int originalCounts = tableUtilsService.countTableLines("cmis_biz", "GRT_GUAR_CONT");
        logger.info("查询当天的[权证台账 [GRT_GUAR_CONT]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

        if (bakDCounts != originalCounts) {
            logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakDCounts, originalCounts);
            throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakDCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
        } else {
            logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakDCounts, originalCounts);
        }
    }
}
