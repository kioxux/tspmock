package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0425Mapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Objects;


/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0425</br>
 * 任务名称：加工任务-贷后管理-风险分类子表数据生成</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0425Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0425Service.class);

    @Resource
    private Cmis0425Mapper cmis0425Mapper;

    /**
     * 插入风险分类借据信息[对公专项贷款风险分类借据信息]
     *
     * @param map
     */
    public void cmis0425InsertRiskDebitInfo01(Map map) {
        logger.info("插入风险分类借据信息[对公专项贷款风险分类借据信息]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfo01 = cmis0425Mapper.insertRiskDebitInfo01(map);
        logger.info("插入风险分类借据信息[对公专项贷款风险分类借据信息]结束,返回参数为:[{}]", insertRiskDebitInfo01);


        logger.info("插入风险分类借据信息[委托贷款借据 ]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfowtdk = cmis0425Mapper.insertRiskDebitInfowtdk(map);
        logger.info("插入风险分类借据信息[委托贷款借据 ]结束,返回参数为:[{}]", insertRiskDebitInfowtdk);

        logger.info("插入风险分类借据信息[风险分类银承借据 ]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfoyc = cmis0425Mapper.insertRiskDebitInfoyc(map);
        logger.info("插入风险分类借据信息[风险分类银承借据]结束,返回参数为:[{}]", insertRiskDebitInfoyc);

        logger.info("插入风险分类借据信息[风险分类保函借据 ]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfobh = cmis0425Mapper.insertRiskDebitInfobh(map);
        logger.info("插入风险分类借据信息[风险分类保函借据]结束,返回参数为:[{}]", insertRiskDebitInfobh);

        logger.info("插入风险分类借据信息[风险分类信用证借据 ]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfoxyz = cmis0425Mapper.insertRiskDebitInfoxyz(map);
        logger.info("插入风险分类借据信息[ 风险分类信用证借据 ]结束,返回参数为:[{}]", insertRiskDebitInfoxyz);


        logger.info("插入风险分类借据信息[风险分类贴现借据 ]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfotx = cmis0425Mapper.insertRiskDebitInfotx(map);
        logger.info("插入风险分类借据信息[ 风险分类贴现借据 ]结束,返回参数为:[{}]", insertRiskDebitInfotx);



    }

    /**
     * 插入风险分类借据信息[个人经营性分类借据大额跑批矩阵]
     *
     * @param map
     */
    public void cmis0425InsertRiskDebitInfo02(Map map) {
        logger.info("插入风险分类借据信息[1)先执行大额： 个人经营性分类借据大额跑批矩阵]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfo02 = cmis0425Mapper.insertRiskDebitInfo02(map);
        logger.info("插入风险分类借据信息[1)先执行大额： 个人经营性分类借据大额跑批矩阵]结束,返回参数为:[{}]", insertRiskDebitInfo02);
    }


    /**
     * 插入风险分类借据信息[个人经营性分类借据小额跑批矩阵]
     *
     * @param map
     */
    public void cmis0425InsertRiskDebitInfo03(Map map) {
        logger.info("插入风险分类借据信息[2)再执行小额： 个人经营性分类借据小额跑批矩阵]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfo03 = cmis0425Mapper.insertRiskDebitInfo03(map);
        logger.info("插入风险分类借据信息[2)再执行小额： 个人经营性分类借据小额跑批矩阵]结束,返回参数为:[{}]", insertRiskDebitInfo03);
    }

    /**
     * 插入风险分类借据信息[个人消费分类借据纯线上贷跑批矩阵]
     *
     * @param map
     */
    public void cmis0425InsertRiskDebitInfo04(Map map) {
        logger.info("插入风险分类借据信息[个人消费分类借据纯线上贷跑批矩阵]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfo04 = cmis0425Mapper.insertRiskDebitInfo04(map);
        logger.info("插入风险分类借据信息[个人消费分类借据纯线上贷跑批矩阵]结束,返回参数为:[{}]", insertRiskDebitInfo04);
    }

    /**
     * 插入风险分类借据信息[个人消费分类借据大额跑批矩阵]
     *
     * @param map
     */
    public void cmis0425InsertRiskDebitInfo05(Map map) {
        logger.info("插入风险分类借据信息[个人消费分类借据大额跑批矩阵]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfo05 = cmis0425Mapper.insertRiskDebitInfo05(map);
        logger.info("插入风险分类借据信息[个人消费分类借据大额跑批矩阵]结束,返回参数为:[{}]", insertRiskDebitInfo05);
    }

    /**
     * 插入风险分类借据信息[个人消费分类借据小额跑批矩阵]
     *
     * @param map
     */
    public void cmis0425InsertRiskDebitInfo06(Map map) {
        logger.info("插入风险分类借据信息[个人消费分类借据小额跑批矩阵]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfo06 = cmis0425Mapper.insertRiskDebitInfo06(map);
        logger.info("插入风险分类借据信息[个人消费分类借据小额跑批矩阵]结束,返回参数为:[{}]", insertRiskDebitInfo06);
    }

    /**
     * 插入风险分类借据信息[个人消费分类借据住房按揭与汽车]
     *
     * @param map
     */
    public void cmis0425InsertRiskDebitInfo07(Map map) {
        logger.info("插入风险分类借据信息[个人消费分类借据住房按揭与汽车]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfo07 = cmis0425Mapper.insertRiskDebitInfo07(map);
        logger.info("插入风险分类借据信息[个人消费分类借据住房按揭与汽车]结束,返回参数为:[{}]", insertRiskDebitInfo07);

        logger.info("删除借据重复数据开始,请求参数为:[{}]", JSON.toJSONString(map));
        int deleteRiskDebitInfoTmp = cmis0425Mapper.deleteRiskDebitInfoTmp(map);
        logger.info("删除借据重复数据结束,返回参数为:[{}]", deleteRiskDebitInfoTmp);
    }

    /**
     * 插入 风险分类担保合同分析
     *
     * @param map
     */
    public void cmis0425InsertRiskGuarContAnaly(Map map) {
        logger.info("插入风险分类担保合同分析开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskGuarContAnaly = cmis0425Mapper.insertRiskGuarContAnaly(map);
        logger.info("插入风险分类担保合同分析结束,返回参数为:[{}]", insertRiskGuarContAnaly);
    }

    /**
     * 插入 风险分类保证人检查
     *
     * @param map
     */
    public void cmis0425InsertRiskGuarntrList(Map map) {
        logger.info("插入风险分类保证人检查开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskGuarntrList = cmis0425Mapper.insertRiskGuarntrList(map);
        logger.info("插入风险分类保证人检查结束,返回参数为:[{}]", insertRiskGuarntrList);
    }

    /**
     * 插入 风险分类抵质押物检查
     *
     * @param map
     */
    public void cmis0425InsertRiskPldimnList(Map map) {
        logger.info("插入风险分类抵质押物检查开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskPldimnList = cmis0425Mapper.insertRiskPldimnList(map);
        logger.info("插入风险分类抵质押物检查结束,返回参数为:[{}]", insertRiskPldimnList);
    }

    /**
     * 插入 企业财务情况
     *
     * @param map
     */
    public void cmis0425InsertRiskFinReportAnaly(Map map) {
        int truncateRiskFin2 = 0;
        logger.info("清理客户最新财报时间开始,请求参数为:[{}]", JSON.toJSONString(map));
        if (null != map.get("cusId") && !"".equals(map.get("cusId"))){
            // 日间删除
            truncateRiskFin2 = cmis0425Mapper.truncateRiskFin2(map);
        }else{
            // 日终删除
            truncateRiskFin2 = cmis0425Mapper.truncateRiskFin(map);
        }
        logger.info("清理客户最新财报时间结束,返回参数为:[{}]", truncateRiskFin2);
        logger.info("插入客户最新财报时间开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskFin = cmis0425Mapper.insertRiskFin(map);
        logger.info("插入客户最新财报时间结束,返回参数为:[{}]", insertRiskFin);

        logger.info("插入企业财务情况-损益表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskFinReportAnaly01 = cmis0425Mapper.insertRiskFinReportAnaly01(map);
        logger.info("插入企业财务情况-损益表结束,返回参数为:[{}]", insertRiskFinReportAnaly01);

        logger.info("插入企业财务情况-资产负债表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskFinReportAnaly02 = cmis0425Mapper.insertRiskFinReportAnaly02(map);
        logger.info("插入企业财务情况-资产负债表结束,返回参数为:[{}]", insertRiskFinReportAnaly02);

        logger.info("插入企业财务情况-现金流量表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskFinReportAnaly03 = cmis0425Mapper.insertRiskFinReportAnaly03(map);
        logger.info("插入企业财务情况-现金流量表结束,返回参数为:[{}]", insertRiskFinReportAnaly03);

        logger.info("插入企业财务情况-财务指标表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskFinReportAnaly04 = cmis0425Mapper.insertRiskFinReportAnaly04(map);
        logger.info("插入企业财务情况-财务指标表结束,返回参数为:[{}]", insertRiskFinReportAnaly04);
    }

    /**
     * 插入 影响偿还因素分析
     *
     * @param map
     */
    public void cmis0425InsertRiskRepayAnaly(Map map) {
        logger.info("插入影响偿还因素分析开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskRepayAnaly = cmis0425Mapper.insertRiskRepayAnaly(map);
        logger.info("插入影响偿还因素分析结束,返回参数为:[{}]", insertRiskRepayAnaly);
    }

    /**
     * 更新 风险分类任务表
     *
     * @param openDay
     */
    public void cmis0425UpdateRiskTaskList(String openDay) {
        logger.info("更新风险分类任务表开始,请求参数为:[{}]", openDay);
        int updateRiskTaskList = cmis0425Mapper.updateRiskTaskList(openDay);
        logger.info("更新风险分类任务表结束,返回参数为:[{}]", updateRiskTaskList);

        logger.info("清空临时表开始,请求参数为:[{}]", openDay);
       cmis0425Mapper.truncatetmpupdate();
        logger.info("清空临时表结束");

        logger.info("个人消费分类借据纯线上贷跑批矩阵自动分类数据插入开始,请求参数为:[{}]", openDay);
        int inserttmpupdateOnlineLoan = cmis0425Mapper.inserttmpupdateOnlineLoan(openDay);
        logger.info("个人消费分类借据纯线上贷跑批矩阵自动分类数据插入结束,返回参数为:[{}]", inserttmpupdateOnlineLoan);

        logger.info("个人消费分类借据纯线上贷跑批矩阵自动分类数据更新到贷款台账开始,请求参数为:[{}]", openDay);
        int updatetmpupdateOnlineLoan = cmis0425Mapper.updatetmpupdateOnlineLoan(openDay);
        logger.info("个人消费分类借据纯线上贷跑批矩阵自动分类数据更新到贷款台账结束,返回参数为:[{}]", updatetmpupdateOnlineLoan);

        logger.info("个人消费分类借据纯线上贷跑批矩阵自动分类数据更新为通过开始,请求参数为:[{}]", openDay);
        int updateOnlineLoanTask = cmis0425Mapper.updateOnlineLoanTask(openDay);
        logger.info("个人消费分类借据纯线上贷跑批矩阵自动分类数据更新为通过结束,返回参数为:[{}]", updateOnlineLoanTask);

    }

    /**
     * 插入 风险分类任务临时主表
     * @param map
     */
    public void cmis0425InsertTmpRiskTaskList(Map map) {


        logger.info("删除风险分类借据信息临时表  日间手工发起分类使用 开始,请求参数为:[{}]", JSON.toJSONString(map));
        int deletTmpPspTaskList = cmis0425Mapper.deletTmpPspTaskList(map);
        logger.info("删除风险分类借据信息临时表 日间手工发起分类使用  结束,返回参数为:[{}]", deletTmpPspTaskList);


        logger.info("删除 风险借据信息临时表 日间手工发起分类使用 开始,请求参数为:[{}]", JSON.toJSONString(map));
        int deletTmpRiskDebitInfo = cmis0425Mapper.deletTmpRiskDebitInfo(map);
        logger.info("删除 风险借据信息临时表 日间手工发起分类使用 结束,返回参数为:[{}]", deletTmpRiskDebitInfo);


        logger.info("插入风险分类任务临时主表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertTmpRiskTaskList = cmis0425Mapper.insertTmpRiskTaskList(map);
        logger.info("插入风险分类任务临时主表结束,返回参数为:[{}]", insertTmpRiskTaskList);
    }

    /**
     * 日间风险分类临时借据信息回插
     * @param map
     */
    public void insertRiskDebitInfoFromTmp(Map map) {
        logger.info("风险分类临时借据信息回插开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertRiskDebitInfo = cmis0425Mapper.insertRiskDebitInfoFromTmp(map);
        logger.info("风险分类临时借据信息回插结束,返回参数为:[{}]", insertRiskDebitInfo);

    }

    /**
     * 更新风险分类子表任务生成状态
     * @author jijian_yx
     * @date 2021/9/30 1:45
     **/
    public void cmis0425ChangeRiskTaskListTaskStatus(Map map) {
        logger.info("更新风险分类子表任务生成状态,请求参数为:[{}]", JSON.toJSONString(map));
        int updateRiskTaskListTaskStatus = cmis0425Mapper.updateRiskTaskListTaskStatus(map);
        logger.info("更新风险分类子表任务生成状态,返回参数为:[{}]", updateRiskTaskListTaskStatus);
    }
}
