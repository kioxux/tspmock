package cn.com.yusys.yusp.batch.repository.mapper.server.cmisbatch0008;

import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 接口处理Mapper:任务加急初始化数据
 *
 * @author xuchao
 * @version 1.0
 */
public interface CmisBatch0008Mapper {

    /**
     * 初始化员工数据
     */
    void truncateTaskUrgentDetailInfo();

    /**
     * 进入任务加急统计表
     * @return
     */
    int insertTaskUrgentDetailInfo(@Param("dataDate")String dataDate);

    /**
     * 计算本次新增笔数
     * 本次新增笔数=基数*优先分配比例。
     * 基数=“更新周期”时间范围内所有配置的“业务类型”数据之和
     * 合同申请 ： iqp_high_amt_agr_app、iqp_loan_app、iqp_disc_app、iqp_tf_loc_app、iqp_cvrg_app、iqp_accp_app、iqp_entrust_loan_app
     * @return
     */
    int updateTaskUrgentDetailInfo01(@Param("dataDate")String dataDate);

    /**
     * 总优先笔数：历史结余笔数+本次新增笔数。
     * @return
     */
    int updateTaskUrgentDetailInfo02(@Param("dataDate")String dataDate);

    /**
     * 当前剩余笔数等于总优先笔数
     * @return
     */
    int updateTaskUrgentDetailInfo03(@Param("dataDate")String dataDate);

    /**
     * 更新任务加急表（更新历史结余）历史结余=上期剩余
     * @return
     */
    int updateTaskUrgentDetailInfo04(@Param("dataDate")String dataDate);
}
