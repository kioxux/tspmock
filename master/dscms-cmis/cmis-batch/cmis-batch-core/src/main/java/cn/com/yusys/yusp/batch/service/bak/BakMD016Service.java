package cn.com.yusys.yusp.batch.service.bak;

import cn.com.yusys.yusp.batch.repository.mapper.bak.BakMD016Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKMD016</br>
 * 任务名称：批后备份月表日表任务-备份保函协议详情[CTR_CVRG_CONT]  </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
@Service
@Transactional
public class BakMD016Service {
    private static final Logger logger = LoggerFactory.getLogger(BakMD016Service.class);

    @Autowired
    private BakMD016Mapper bakMD016Mapper;

    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 重命名创建和删除当天的[保函协议详情[CTR_CVRG_CONT]]数据
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void bakMD016DeleteDCtrCvrgCont(String openDay) {
        logger.info("重命名创建和删除当天的[保函协议详情[TMP_DAF_CTR_CVRG_CONT]]数据开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","TMP_DAF_CTR_CVRG_CONT");
        logger.info("重命名创建和删除当天的[保函协议详情[TMP_DAF_CTR_CVRG_CONT]]数据结束");
//
//        logger.info("计算当天日期开始,请求参数为:[{}]", openDay);
//        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -4); // 4天前日期
//        logger.info("计算当天日期结束,返回参数为:[{}]", openDay);
//        logger.info("查询待删除当天的[保函协议详情[CTR_CVRG_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
//        int queryMD016DeleteOpenDayCtrCvrgContCounts = bakMD016Mapper.queryMD016DeleteOpenDayCtrCvrgContCounts(openDay);
//        // 由于TDSQL不支持ceiling函数，此处在java中处理
//        BigDecimal times = new BigDecimal(queryMD016DeleteOpenDayCtrCvrgContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
//        queryMD016DeleteOpenDayCtrCvrgContCounts = times.intValue();
//        logger.info("查询待删除当天的[保函协议详情[CTR_CVRG_CONT]]数据总次数结束,返回参数为:[{}]", queryMD016DeleteOpenDayCtrCvrgContCounts);
//        // 删除当天的数据
//        for (int i = 0; i < queryMD016DeleteOpenDayCtrCvrgContCounts; i++) {
//            logger.info("第[" + i + "]次删除当天的[保函协议详情[CTR_CVRG_CONT]]数据开始,请求参数为:[{}]", openDay);
//            int bakMD016DeleteDCtrCvrgCont = bakMD016Mapper.bakMD016DeleteDCtrCvrgCont(openDay);
//            logger.info("第[" + i + "]次删除当天的[保函协议详情[CTR_CVRG_CONT]]数据结束,返回参数为:[{}]", bakMD016DeleteDCtrCvrgCont);
//        }
    }

    /**
     * 删除当月的[保函协议详情[CTR_CVRG_CONT]]数据
     *
     * @param openDay
     */
    public void bakMD016DeleteMCtrCvrgCont(String openDay) {
        logger.info("查询当月的[保函协议详情[CTR_CVRG_CONT]]数据开始,请求参数为:[{}]", openDay);
        int queryMD016DeleteCtrCvrgCont = bakMD016Mapper.queryMD016DeleteCtrCvrgCont(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(queryMD016DeleteCtrCvrgCont).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        queryMD016DeleteCtrCvrgCont = times.intValue();
        logger.info("查询当月的[保函协议详情[CTR_CVRG_CONT]]数据结束,返回参数为:[{}]", queryMD016DeleteCtrCvrgCont);
        // 删除当月的数据
        for (int i = 0; i < queryMD016DeleteCtrCvrgCont; i++) {
            logger.info("第[" + i + "]次删除当月的[保函协议详情[CTR_CVRG_CONT]]数据开始,请求参数为:[{}]", openDay);
            int bakMD016DeleteMCtrCvrgCont = bakMD016Mapper.bakMD016DeleteMCtrCvrgCont(openDay);
            logger.info("第[" + i + "]次删除当月的[保函协议详情[CTR_CVRG_CONT]]数据结束,返回参数为:[{}]", bakMD016DeleteMCtrCvrgCont);
        }
    }

    /**
     * 备份当天的数据
     *
     * @param openDay
     */
    public void bakMD016InsertD(String openDay) {
        // 备份当天的数据
        logger.info("备份当天的[保函协议详情[CTR_CVRG_CONT]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakMD016Mapper.insertD(openDay);
        logger.info("备份当天的[保函协议详情[CTR_CVRG_CONT]]数据结束,返回参数为:[{}]", insertCurrent);
    }

    /**
     * 备份当月的数据
     *
     * @param openDay
     */
    public void bakMD016InsertM(String openDay) {
        logger.info("备份当月的[保函协议详情[CTR_CVRG_CONT]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakMD016Mapper.insertM(openDay);
        logger.info("备份当月的[保函协议详情[CTR_CVRG_CONT]]数据结束,返回参数为:[{}]", insertCurrent);
    }

    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakMEqualsOriginal(String openDay) {
        String nextOpenDay = DateUtils.addDay(openDay, "yyyy-MM-dd", 1); // 切日后营业日期
        logger.info("切日后营业日期为:[{}]", nextOpenDay);
        Date nextOpenDayDate = DateUtils.parseDateByDef(nextOpenDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
        int day = DateUtils.getMonthDay(nextOpenDayDate);// 获取当月中的天
        logger.info("获取当月中的天为:[{}]", day);

        if (day == 1) {
            logger.info("查询当月备份的[保函协议详情 [TMP_M_CTR_CVRG_CONT]]数据总条数开始,请求参数为:[{}]", openDay);
            int bakMCounts = bakMD016Mapper.queryBakMCounts(openDay);
            logger.info("查询当月备份的[保函协议详情 [TMP_M_CTR_CVRG_CONT]]数据总条数结束,返回参数为:[{}]", bakMCounts);

            logger.info("查询当月的[保函协议详情 [CTR_CVRG_CONT]]原表数据总条数开始,请求参数为:[{}]", openDay);
            int originalCounts = bakMD016Mapper.queryOriginalCounts(openDay);
            logger.info("查询当月的[保函协议详情 [CTR_CVRG_CONT]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

            if (bakMCounts != originalCounts) {
                logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakMCounts, originalCounts);
                throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakMCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
            } else {
                logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakMCounts, originalCounts);
            }
        }
    }


    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakDEqualsOriginal(String openDay) {
        logger.info("查询当天备份的[保函协议详情[TMP_DAF_CTR_CVRG_CONT]]数据总条数开始,请求参数为:[{}]", openDay);
        int bakDCounts = tableUtilsService.coutBakDafTableLines("cmis_biz", "TMP_DAF_CTR_CVRG_CONT");
        logger.info("查询当天备份的[保函协议详情[TMP_DAF_CTR_CVRG_CONT]]数据总条数结束,返回参数为:[{}]", bakDCounts);

        logger.info("查询当天的[保函协议详情 [CTR_CVRG_CONT]]原表数据总条数开始,请求参数为:[{}]", openDay);
        int originalCounts = tableUtilsService.countTableLines("cmis_biz", "CTR_CVRG_CONT");
        logger.info("查询当天的[保函协议详情 [CTR_CVRG_CONT]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

        if (bakDCounts != originalCounts) {
            logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakDCounts, originalCounts);
            throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakDCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
        } else {
            logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakDCounts, originalCounts);
        }
    }
}
