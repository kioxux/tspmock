package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0204</br>
 * 任务名称：加工任务-额度处理-表外占用总余额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0204Mapper {
    /**
     * 更新额度占用关系
     *
     * @return
     */
    void truncateLmtContRel07();

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel07(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel07(@Param("openDay") String openDay);


    /**
     * 更新额度占用关系
     *
     * @return
     */
    void truncateLmtContRel09();

    /**
     * 银承占用子授信分项
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel09AYchtsq(@Param("openDay") String openDay);

    /**
     *  银承占用子授信分项
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel09AYchtxq(@Param("openDay") String openDay);

    /**
     *  保函协议申请详情占用子授信分项
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel09ABhxysq(@Param("openDay") String openDay);

    /**
     * 保函协议详情占用子授信分项
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel09ABhxyxq(@Param("openDay") String openDay);

    /**
     * 贴现协议申请详情占用子授信分项
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel09ATxxysqxq(@Param("openDay") String openDay);

    /**
     *  贴现协议详情占用子授信分项
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel09ATxxyxq(@Param("openDay") String openDay);

    /**
     * 开证合同申请详情占用子授信分项
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel09AKzhtsqxq(@Param("openDay") String openDay);

    /**
     * 开证合同详情占用子授信分项
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel09AKzhtxq(@Param("openDay") String openDay);

    /**
     * 根据授信占用临时表表数据 更新 授信占用关系表
     *
     * @param openDay
     * @return
     */
    int updateTdlcrTla(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系中的占用总敞口余额
     *
     * @param openDay
     * @return
     */
    int updateTdlcr01(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel09(@Param("openDay") String openDay);

}
