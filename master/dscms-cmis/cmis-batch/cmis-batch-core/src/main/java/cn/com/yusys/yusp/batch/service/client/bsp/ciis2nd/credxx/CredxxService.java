package cn.com.yusys.yusp.batch.service.client.bsp.ciis2nd.credxx;

import cn.com.yusys.yusp.batch.repository.mapper.timed.BatchTimedTask0001Mapper;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.Dscms2Ciis2ndClientService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：线下查询接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CredxxService {
    private static final Logger logger = LoggerFactory.getLogger(CredxxService.class);

    // 1）注入：BSP封装调用二代征信系统的接口处理类（credxx）
    @Autowired
    private Dscms2Ciis2ndClientService dscms2Ciis2ndClientService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private BatchTimedTask0001Mapper batchTimedTask0001Mapper;

    /**
     * 业务逻辑处理方法：线下查询接口
     *
     * @param credxxReqDto
     * @return
     */
    @Transactional
    public CredxxRespDto credxx(CredxxReqDto credxxReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value, JSON.toJSONString(credxxReqDto));
        ResultDto<CredxxRespDto> credxxResultDto = dscms2Ciis2ndClientService.credxx(credxxReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value, JSON.toJSONString(credxxResultDto));
        String credxxCode = Optional.ofNullable(credxxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String credxxMeesage = Optional.ofNullable(credxxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CredxxRespDto credxxRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, credxxResultDto.getCode())) {
            //  获取相关的值并解析
            credxxRespDto = credxxResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, credxxCode, credxxMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value);
        return credxxRespDto;
    }

    /**
     * 业务逻辑处理方法：线下查询接口
     *
     * @param credxxReqDto
     * @return
     */
    @Transactional
    public ResultDto<CredxxRespDto> credxxNew(CredxxReqDto credxxReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value, JSON.toJSONString(credxxReqDto));
        ResultDto<CredxxRespDto> credxxResultDto = dscms2Ciis2ndClientService.credxx(credxxReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value, JSON.toJSONString(credxxResultDto));
        return credxxResultDto;
    }

    /**
     * 根据查询结果来组装[请求Dto：线下查询接口]
     *
     * @param needQueryCrereportMap
     * @return
     */
    public CredxxReqDto buildByNeedQueryCrereportMap(Map<String, String> needQueryCrereportMap) {
        logger.info("根据查询结果来组装[请求Dto：线下查询接口]开始,请求参数为:[{}]", JSON.toJSONString(needQueryCrereportMap));
        CredxxReqDto credxxReqDto = new CredxxReqDto();
        String cusId = needQueryCrereportMap.get("cusId");
        String cusName = needQueryCrereportMap.get("cusName");
        String certType = needQueryCrereportMap.get("certType");
        String certCode = needQueryCrereportMap.get("certCode");
        String managerId = needQueryCrereportMap.get("managerId");
        String brchno = needQueryCrereportMap.get("managerBrId");

        // TODO prcscd	处理码	否	varchar(40)	是	接口交易码区分交易credwd
        logger.info("根据客户ID:[{}]查询征信授权书信息表开始", cusId);
        Map<String, String> creditAuthbookInfoMap = batchTimedTask0001Mapper.selectCreditAuthbookInfo(cusId);
        logger.info("根据客户ID:[{}]查询征信授权书信息表结束，响应参数为:[{}]", JSON.toJSONString(creditAuthbookInfoMap));
        if (CollectionUtils.isEmpty(creditAuthbookInfoMap)) {
            credxxReqDto = null;
        } else {
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, "getbylogincode", "通过账号获取用户信息", JSON.toJSONString(managerId));
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, "getbylogincode", "通过账号获取用户信息", JSON.toJSONString(resultDto));
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            String auditReason = creditAuthbookInfoMap.get("authbookContent");// 授权书内容
            String creditDocId = creditAuthbookInfoMap.get("authbookNo");// 授权书编号
            credxxReqDto.setAuditReason(auditReason); //授权条款
            credxxReqDto.setCreditDocId(creditDocId);//授权影像编
            // 20211109 增加根据报告类型 0 个人 1 企业获取查询原因的判断 开始
            String creditType = toConverCreditType(certType);//报告类型 0 个人 1 企业
            logger.info("报告类型 0 个人 1 企业为:[{}]", creditType);
            credxxReqDto.setCreditType(creditType);//报告类型 0 个人 1 企业
            credxxReqDto.setCustomName(cusName);//客户名称
            credxxReqDto.setCertificateType(toConverCertType(certType));//证件类型
            credxxReqDto.setCertificateNum(certCode);//证件号
            String queryReason = StringUtils.EMPTY;
            logger.info("根据报告类型 0 个人 1 企业为:[{}]获取查询原因开始", creditType);
            if (Objects.equals(creditType, "0")) {// 0 个人
                queryReason = "01";// 贷后（在保）管理
            } else { // 1 企业
                queryReason = "03";// 贷后（在保）管理
            }
            logger.info("根据报告类型 0 个人 1 企业为:[{}]获取查询原因结束，对应的查询原因为:[{}]", creditType, queryReason);
            credxxReqDto.setQueryReason(queryReason);//查询原因，贷后（在保）管理
            // 20211109 增加根据报告类型 0 个人 1 企业获取查询原因的判断 结束
            credxxReqDto.setCreateUserName(adminSmUserDto.getUserName());//客户经理名称
            credxxReqDto.setCreateUserIdCard(adminSmUserDto.getCertNo());//客户经理身份证号
            credxxReqDto.setCreateUserId(managerId);//客户经理工号
            credxxReqDto.setBrchno(brchno);// 部门号
            credxxReqDto.setReportType("H");//信用报告返回格式H ：html X:xml J:json
            //1、若该值为负数,则查询该值绝对值内的本地报告，不查询征信中心;
            //2、若该值为0，则强制查询征信中心；
            //3、若该值为正数，则查询该值内的本地报告，本地无报告则查询征信中心
            credxxReqDto.setQueryType("1");//信用报告复用策略
            //记录发生查询请求的具体业务线，业务线需要在前台进行数据字典维护。
            // 00~09为系统保留，不要使用。10~99为对接系统使用。
            credxxReqDto.setBusinessLine("101");//产品业务线
            credxxReqDto.setSysCode("1");//系统来源
            //总行：朱真尧 东海：邵雯
            if (brchno.startsWith("81")) {
                credxxReqDto.setApprovalName("邵雯");//审批人姓名
                credxxReqDto.setApprovalIdCard("32072219960820002X");//审批人身份证号
            } else {
                credxxReqDto.setApprovalName("朱真尧");//审批人姓名
                credxxReqDto.setApprovalIdCard("320582199401122613");//审批人身份证号
            }
            //取营业日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");//营业日期
            //授权结束日期=授权日期+30天
            String expireDate = DateUtils.addDay(openDay, "yyyy-MM-dd", 18250);//和业务沟通，贷后增加50年
            credxxReqDto.setArchiveCreateDate(openDay);//授权书签订日期

            credxxReqDto.setArchiveExpireDate(expireDate);//授权结束日期
            credxxReqDto.setBorrowPersonRelation("001");//与主借款人关系 TODO ys  001 暂时
            credxxReqDto.setBorrowPersonRelationName("");//主借款人名称
            credxxReqDto.setBorrowPersonRelationNumber("");//主借款人证件号
            logger.info("根据查询结果来组装[请求Dto：线下查询接口]结束,响应参数为:[{}]", JSON.toJSONString(credxxReqDto));
        }
        return credxxReqDto;
    }

    /**
     * 根据证件类型获取报告类型
     *
     * @param certType
     * @return
     */
    private String toConverCreditType(String certType) {
        String creditType = "";
        if ("C".equals(certType)) { //户口簿
            creditType = "0";
        } else if ("B".equals(certType)) {//护照
            creditType = "0";
        } else if ("D".equals(certType)) {//港澳居民来往内地通行证
            creditType = "0";
        } else if ("E".equals(certType)) {//台湾同胞来往内地通行证
            creditType = "0";
        } else if ("12".equals(certType)) { // 外国人居留证
            creditType = "0";
        } else if ("Y".equals(certType)) {//警官证
            creditType = "0";
        } else if ("13".equals(certType)) {//香港身份证
            creditType = "0";
        } else if ("14".equals(certType)) {//澳门身份证
            creditType = "0";
        } else if ("15".equals(certType)) {//台湾身份证
            creditType = "0";
        } else if ("16".equals(certType)) {//其他证件
            creditType = "0";
        } else if ("A".equals(certType)) {//居民身份证及其他以公民身份证号为标识的证件
            creditType = "0";
        } else if ("11".equals(certType)) {//军人身份证件
            creditType = "0";
        } else if ("06".equals(certType)) {//工商注册号
            creditType = "1";
        } else if ("01".equals(certType)) {//机关和事业单位登记号
            creditType = "1";
        } else if ("02".equals(certType)) {//社会团体登记号
            creditType = "1";
        } else if ("03".equals(certType)) {//民办非企业登记号
            creditType = "1";
        } else if ("04".equals(certType)) {//基金会登记号
            creditType = "1";
        } else if ("05".equals(certType)) {//宗教证书登记号
            creditType = "1";
        } else if ("P2".equals(certType)) {//中征码
            creditType = "1";
        } else if ("R".equals(certType)) {//统一社会信用代码
            creditType = "1";
        } else if ("Q".equals(certType) || "M".equals(certType)) {//Q-组织机构代码  M-营业执照
            creditType = "1";
        } else if ("07".equals(certType)) {//纳税人识别号（国税）
            creditType = "1";
        } else if ("08".equals(certType)) {//纳税人识别号（地税）
            creditType = "1";
        }
        return creditType;
    }


    /*
     * 证件映射 信贷--->征信
     * @param xdbz 信贷证件码值
     * @return zxbz 征信证件码值
     */
    public static String toConverCertType(String xdbz) {
        String zxbz = "";
        if ("C".equals(xdbz)) { //户口簿
            zxbz = "1";
        } else if ("B".equals(xdbz)) {//护照
            zxbz = "2";
        } else if ("D".equals(xdbz)) {//港澳居民来往内地通行证
            zxbz = "5";
        } else if ("E".equals(xdbz)) {//台湾同胞来往内地通行证
            zxbz = "6";
        } else if ("12".equals(xdbz)) { // 外国人居留证
            zxbz = "8";
        } else if ("Y".equals(xdbz)) {//警官证
            zxbz = "9";
        } else if ("13".equals(xdbz)) {//香港身份证
            zxbz = "A";
        } else if ("14".equals(xdbz)) {//澳门身份证
            zxbz = "B";
        } else if ("15".equals(xdbz)) {//台湾身份证
            zxbz = "C";
        } else if ("16".equals(xdbz)) {//其他证件
            zxbz = "X";
        } else if ("A".equals(xdbz)) {//居民身份证及其他以公民身份证号为标识的证件
            zxbz = "10";
        } else if ("11".equals(xdbz)) {//军人身份证件
            zxbz = "20";
        } else if ("06".equals(xdbz)) {//工商注册号
            zxbz = "01";
        } else if ("01".equals(xdbz)) {//机关和事业单位登记号
            zxbz = "02";
        } else if ("02".equals(xdbz)) {//社会团体登记号
            zxbz = "03";
        } else if ("03".equals(xdbz)) {//民办非企业登记号
            zxbz = "04";
        } else if ("04".equals(xdbz)) {//基金会登记号
            zxbz = "05";
        } else if ("05".equals(xdbz)) {//宗教证书登记号
            zxbz = "06";
        } else if ("P2".equals(xdbz)) {//中征码
            zxbz = "10";
        } else if ("R".equals(xdbz)) {//统一社会信用代码
            zxbz = "20";
        } else if ("Q".equals(xdbz)) {//组织机构代码
            zxbz = "30";
        } else if ("07".equals(xdbz)) {//纳税人识别号（国税）
            zxbz = "41";
        } else if ("08".equals(xdbz)) {//纳税人识别号（地税）
            zxbz = "42";
        } else {
            zxbz = xdbz;//未匹配到的证件类型
        }
        return zxbz;
    }
}
