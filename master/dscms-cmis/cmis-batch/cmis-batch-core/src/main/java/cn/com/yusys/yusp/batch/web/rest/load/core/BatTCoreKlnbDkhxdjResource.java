/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatTCoreKlnbDkhxdj;
import cn.com.yusys.yusp.batch.service.load.core.BatTCoreKlnbDkhxdjService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatTCoreKlnbDkhxdjResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 23:46:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battcoreklnbdkhxdj")
public class BatTCoreKlnbDkhxdjResource {
    @Autowired
    private BatTCoreKlnbDkhxdjService batTCoreKlnbDkhxdjService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTCoreKlnbDkhxdj>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTCoreKlnbDkhxdj> list = batTCoreKlnbDkhxdjService.selectAll(queryModel);
        return new ResultDto<List<BatTCoreKlnbDkhxdj>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTCoreKlnbDkhxdj>> index(QueryModel queryModel) {
        List<BatTCoreKlnbDkhxdj> list = batTCoreKlnbDkhxdjService.selectByModel(queryModel);
        return new ResultDto<List<BatTCoreKlnbDkhxdj>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTCoreKlnbDkhxdj> create(@RequestBody BatTCoreKlnbDkhxdj batTCoreKlnbDkhxdj) throws URISyntaxException {
        batTCoreKlnbDkhxdjService.insert(batTCoreKlnbDkhxdj);
        return new ResultDto<BatTCoreKlnbDkhxdj>(batTCoreKlnbDkhxdj);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTCoreKlnbDkhxdj batTCoreKlnbDkhxdj) throws URISyntaxException {
        int result = batTCoreKlnbDkhxdjService.update(batTCoreKlnbDkhxdj);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String farendma, String dkjiejuh, String jiaoyils, String jiaoyirq) {
        int result = batTCoreKlnbDkhxdjService.deleteByPrimaryKey(farendma, dkjiejuh, jiaoyils, jiaoyirq);
        return new ResultDto<Integer>(result);
    }

}
