/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.rcp;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpAntWriteOffSl;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpAntWriteOffSlService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpAntWriteOffSlResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-10 14:48:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsrcpantwriteoffsl")
public class BatSRcpAntWriteOffSlResource {
    @Autowired
    private BatSRcpAntWriteOffSlService batSRcpAntWriteOffSlService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSRcpAntWriteOffSl>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSRcpAntWriteOffSl> list = batSRcpAntWriteOffSlService.selectAll(queryModel);
        return new ResultDto<List<BatSRcpAntWriteOffSl>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSRcpAntWriteOffSl>> index(QueryModel queryModel) {
        List<BatSRcpAntWriteOffSl> list = batSRcpAntWriteOffSlService.selectByModel(queryModel);
        return new ResultDto<List<BatSRcpAntWriteOffSl>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{contractNo}")
    protected ResultDto<BatSRcpAntWriteOffSl> show(@PathVariable("contractNo") String contractNo) {
        BatSRcpAntWriteOffSl batSRcpAntWriteOffSl = batSRcpAntWriteOffSlService.selectByPrimaryKey(contractNo);
        return new ResultDto<BatSRcpAntWriteOffSl>(batSRcpAntWriteOffSl);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSRcpAntWriteOffSl> create(@RequestBody BatSRcpAntWriteOffSl batSRcpAntWriteOffSl) throws URISyntaxException {
        batSRcpAntWriteOffSlService.insert(batSRcpAntWriteOffSl);
        return new ResultDto<BatSRcpAntWriteOffSl>(batSRcpAntWriteOffSl);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSRcpAntWriteOffSl batSRcpAntWriteOffSl) throws URISyntaxException {
        int result = batSRcpAntWriteOffSlService.update(batSRcpAntWriteOffSl);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{contractNo}")
    protected ResultDto<Integer> delete(@PathVariable("contractNo") String contractNo) {
        int result = batSRcpAntWriteOffSlService.deleteByPrimaryKey(contractNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSRcpAntWriteOffSlService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
