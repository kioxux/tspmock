package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0215Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0215</br>
 * 任务名称：加工任务-额度处理-</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0215Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0215Service.class);
    @Autowired
    private Cmis0215Mapper cmis0215Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * CMIS0215-计算授信总额累加
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0215UpdateTdldLmtAmtInfo(String openDay) {

        logger.info("更新批复额度分项基础信息加工表中字段,请求参数为:[{}]", openDay);
        int updateTdldLmtAmtAdd01 = cmis0215Mapper.updateTdldLmtAmtAdd01(openDay);
        logger.info("更新批复额度分项基础信息加工表中字段 ,返回参数为:[{}]", updateTdldLmtAmtAdd01);

        logger.info("删除批复额度分项基础信息加工表,请求参数为:[{}]", openDay);
        // cmis0215Mapper.truncateTmpLmtAmt01();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("删除批复额度分项基础信息加工表");


        logger.info("插入批复额度分项基础信息加工表,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt = cmis0215Mapper.insertTmpLmtAmt(openDay);
        logger.info("插入批复额度分项基础信息加工表 ,返回参数为:[{}]", insertTmpLmtAmt);


        logger.info("更新批复额度分项基础信息加工表中字段,请求参数为:[{}]", openDay);
        int updateTdldLmtAmtAdd02 = cmis0215Mapper.updateTdldLmtAmtAdd02(openDay);
        logger.info("更新批复额度分项基础信息加工表中字段 ,返回参数为:[{}]", updateTdldLmtAmtAdd02);


        logger.info("truncateTmpAccAdddetails删除加工台账低风险总额累加明细,请求参数为:[{}]", openDay);
        // cmis0215Mapper.truncateTmpAccAdddetails();// cmis_lmt.tmp_acc_adddetails
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_acc_adddetails");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpAccAdddetails删除加工台账低风险总额累加明细");


        logger.info("插入台账低风险总额累加明细,请求参数为:[{}]", openDay);
        int insertTmpAccAdddetails = cmis0215Mapper.insertTmpAccAdddetails(openDay);
        logger.info("插入台账低风险总额累加明细 ,返回参数为:[{}]", insertTmpAccAdddetails);


        logger.info("truncateTmpLmtAmt02删除加工占用授信表02,请求参数为:[{}]", openDay);
        // cmis0215Mapper.truncateTmpLmtAmt02();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpLmtAmt02删除加工占用授信表02");


        logger.info("插入情况一类型数据01,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt01 = cmis0215Mapper.insertTmpLmtAmt01(openDay);
        logger.info("插入情况一类型数据01 ,返回参数为:[{}]", insertTmpLmtAmt01);


        logger.info("插入情况一类型数据02,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt02 = cmis0215Mapper.insertTmpLmtAmt02(openDay);
        logger.info("插入情况一类型数据02 ,返回参数为:[{}]", insertTmpLmtAmt02);


        logger.info("插入情况一类型数据03,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt03 = cmis0215Mapper.insertTmpLmtAmt03(openDay);
        logger.info("插入情况一类型数据03 ,返回参数为:[{}]", insertTmpLmtAmt03);


        logger.info("插入情况一类型数据04,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt04 = cmis0215Mapper.insertTmpLmtAmt04(openDay);
        logger.info("插入情况一类型数据04 ,返回参数为:[{}]", insertTmpLmtAmt04);


        logger.info("更新批复额度分项基础信息加工表中字段 03,请求参数为:[{}]", openDay);
        int updateTdldLmtAmtAdd03 = cmis0215Mapper.updateTdldLmtAmtAdd03(openDay);
        logger.info("更新批复额度分项基础信息加工表中字段 03 ,返回参数为:[{}]", updateTdldLmtAmtAdd03);

        logger.info("deleteTmpLmtAmt删除新信贷-临时表-加工占用授信表开始");
        // cmis0215Mapper.deleteTmpLmtAmt();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTmpLmtAmt删除新信贷-临时表-加工占用授信表结束");

        logger.info("插入新信贷-临时表-加工占用授信表开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt05 = cmis0215Mapper.insertTmpLmtAmt05(openDay);
        logger.info("插入新信贷-临时表-加工占用授信表结束,返回参数为:[{}]", insertTmpLmtAmt05);

        logger.info("更新批复额度分项基础信息加工表开始,请求参数为:[{}]", openDay);
        int updateTmpDealLmtDetails = cmis0215Mapper.updateTmpDealLmtDetails(openDay);
        logger.info("更新批复额度分项基础信息加工表结束,返回参数为:[{}]", updateTmpDealLmtDetails);

    }
}
