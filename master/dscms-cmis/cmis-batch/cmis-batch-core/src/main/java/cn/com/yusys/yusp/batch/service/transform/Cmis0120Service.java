package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0120Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0120</br>
 * 任务名称：加工任务-业务处理-零售智能风控蚂蚁借呗 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2120年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0120Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0120Service.class);

    @Autowired
    private Cmis0120Mapper cmis0120Mapper;

    /**
     * 插入贷款合同表
     * @param openDay
     */
    public void cmis0120InsertCtrLoanCont(String openDay) {
        logger.info("插入贷款合同表开始,请求参数为:[{}]", openDay);
        int insertCtrLoanCont = cmis0120Mapper.insertCtrLoanCont(openDay);
        logger.info("插入贷款合同表结束,返回参数为:[{}]", insertCtrLoanCont);
    }

    /**
     * 插入贷款台账表
     * @param openDay
     */
    public void cmis0120InsertAccLoan(String openDay) {
        logger.info("插入贷款台账表开始,请求参数为:[{}]", openDay);
        int insertAccLoan = cmis0120Mapper.insertAccLoan(openDay);
        logger.info("插入贷款台账表结束,返回参数为:[{}]", insertAccLoan);
    }

    /**
     * 更新贷款台账表
     * @param openDay
     */
    public void cmis0120UpdateAccLoan(String openDay) {
        logger.info("更新贷款台账表[来源表为蚂蚁借呗日终（合约）信息落地表]开始,请求参数为:[{}]", openDay);
        int updateAccLoan03 = cmis0120Mapper.updateAccLoan03(openDay);
        logger.info("更新贷款台账表[来源表为蚂蚁借呗日终（合约）信息落地表]结束,返回参数为:[{}]", updateAccLoan03);
    }
}
