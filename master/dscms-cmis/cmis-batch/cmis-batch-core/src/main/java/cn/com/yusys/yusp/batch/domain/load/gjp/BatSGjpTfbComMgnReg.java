/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.gjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGjpTfbComMgnReg
 * @类描述: bat_s_gjp_tfb_com_mgn_reg数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-11-07 15:51:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_gjp_tfb_com_mgn_reg")
public class BatSGjpTfbComMgnReg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 系统实体编号 **/
	@Column(name = "SYS_ENTY_ID", unique = false, nullable = true, length = 32)
	private String sysEntyId;
	
	/** 系统删除标识 **/
	@Column(name = "SYS_DEL_FLG", unique = false, nullable = true, length = 1)
	private String sysDelFlg;
	
	/** 创建人 **/
	@Column(name = "SYS_CRT_USER", unique = false, nullable = true, length = 32)
	private String sysCrtUser;
	
	/** 创建日期 **/
	@Column(name = "SYS_CRT_DT", unique = false, nullable = true, length = 20)
	private String sysCrtDt;
	
	/** 修改人编号 **/
	@Column(name = "SYS_MODIFY_USER", unique = false, nullable = true, length = 32)
	private String sysModifyUser;
	
	/** 修改日期 **/
	@Column(name = "SYS_MODIFY_DT", unique = false, nullable = true, length = 20)
	private String sysModifyDt;
	
	/** 组织机构代码 **/
	@Column(name = "ORG_CDE", unique = false, nullable = true, length = 32)
	private String orgCde;
	
	/** 业务参考号 **/
	@Column(name = "REF_NO", unique = false, nullable = true, length = 32)
	private String refNo;
	
	/** 交易类型 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 32)
	private String bizType;
	
	/** 客户代码 **/
	@Column(name = "CST_ID", unique = false, nullable = true, length = 32)
	private String cstId;
	
	/** 业务原币币种 **/
	@Column(name = "TRADE_CUR", unique = false, nullable = true, length = 3)
	private String tradeCur;
	
	/** 业务原币金额 **/
	@Column(name = "TRADE_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal tradeAmt;
	
	/** 操作标志 **/
	@Column(name = "OP_FLAG", unique = false, nullable = true, length = 2)
	private String opFlag;
	
	/** 信贷出账号 **/
	@Column(name = "CMS_REF_NO", unique = false, nullable = true, length = 40)
	private String cmsRefNo;
	
	/** 产品代码 **/
	@Column(name = "PRODUCT_ID", unique = false, nullable = true, length = 3)
	private String productId;
	
	/** 产品编号 **/
	@Column(name = "PRODUCT_CDE", unique = false, nullable = true, length = 18)
	private String productCde;
	
	/** 产品组代码 **/
	@Column(name = "PRODUCT_GROUP_ID", unique = false, nullable = true, length = 3)
	private String productGroupId;
	
	/** 产品组编号 **/
	@Column(name = "PRODUCT_GROUP_CDE", unique = false, nullable = true, length = 18)
	private String productGroupCde;
	
	/** 保证金比例 **/
	@Column(name = "MGN_PCT", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal mgnPct;
	
	/** 操作日期 **/
	@Column(name = "MGN_OP_DT", unique = false, nullable = true, length = 8)
	private String mgnOpDt;
	
	/** 系统业务编号 **/
	@Column(name = "SYS_REF_NO", unique = false, nullable = true, length = 32)
	private String sysRefNo;
	
	/** 业务编号 **/
	@Column(name = "BIZ_ID", unique = false, nullable = true, length = 32)
	private String bizId;
	
	/** 业务日期 **/
	@Column(name = "BIZ_DT", unique = false, nullable = true, length = 8)
	private String bizDt;
	
	/** 信贷牌价 **/
	@Column(name = "MMS_RATE", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal mmsRate;
	
	/** 金额 **/
	@Column(name = "MGN_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal mgnAmt;
	
	/** 标志 **/
	@Column(name = "SAME_BIZ_FLAG", unique = false, nullable = true, length = 1)
	private String sameBizFlag;
	
	/** 代码 **/
	@Column(name = "BIC_CODE", unique = false, nullable = true, length = 16)
	private String bicCode;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param sysEntyId
	 */
	public void setSysEntyId(String sysEntyId) {
		this.sysEntyId = sysEntyId;
	}
	
    /**
     * @return sysEntyId
     */
	public String getSysEntyId() {
		return this.sysEntyId;
	}
	
	/**
	 * @param sysDelFlg
	 */
	public void setSysDelFlg(String sysDelFlg) {
		this.sysDelFlg = sysDelFlg;
	}
	
    /**
     * @return sysDelFlg
     */
	public String getSysDelFlg() {
		return this.sysDelFlg;
	}
	
	/**
	 * @param sysCrtUser
	 */
	public void setSysCrtUser(String sysCrtUser) {
		this.sysCrtUser = sysCrtUser;
	}
	
    /**
     * @return sysCrtUser
     */
	public String getSysCrtUser() {
		return this.sysCrtUser;
	}
	
	/**
	 * @param sysCrtDt
	 */
	public void setSysCrtDt(String sysCrtDt) {
		this.sysCrtDt = sysCrtDt;
	}
	
    /**
     * @return sysCrtDt
     */
	public String getSysCrtDt() {
		return this.sysCrtDt;
	}
	
	/**
	 * @param sysModifyUser
	 */
	public void setSysModifyUser(String sysModifyUser) {
		this.sysModifyUser = sysModifyUser;
	}
	
    /**
     * @return sysModifyUser
     */
	public String getSysModifyUser() {
		return this.sysModifyUser;
	}
	
	/**
	 * @param sysModifyDt
	 */
	public void setSysModifyDt(String sysModifyDt) {
		this.sysModifyDt = sysModifyDt;
	}
	
    /**
     * @return sysModifyDt
     */
	public String getSysModifyDt() {
		return this.sysModifyDt;
	}
	
	/**
	 * @param orgCde
	 */
	public void setOrgCde(String orgCde) {
		this.orgCde = orgCde;
	}
	
    /**
     * @return orgCde
     */
	public String getOrgCde() {
		return this.orgCde;
	}
	
	/**
	 * @param refNo
	 */
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
    /**
     * @return refNo
     */
	public String getRefNo() {
		return this.refNo;
	}
	
	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
    /**
     * @return bizType
     */
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param cstId
	 */
	public void setCstId(String cstId) {
		this.cstId = cstId;
	}
	
    /**
     * @return cstId
     */
	public String getCstId() {
		return this.cstId;
	}
	
	/**
	 * @param tradeCur
	 */
	public void setTradeCur(String tradeCur) {
		this.tradeCur = tradeCur;
	}
	
    /**
     * @return tradeCur
     */
	public String getTradeCur() {
		return this.tradeCur;
	}
	
	/**
	 * @param tradeAmt
	 */
	public void setTradeAmt(java.math.BigDecimal tradeAmt) {
		this.tradeAmt = tradeAmt;
	}
	
    /**
     * @return tradeAmt
     */
	public java.math.BigDecimal getTradeAmt() {
		return this.tradeAmt;
	}
	
	/**
	 * @param opFlag
	 */
	public void setOpFlag(String opFlag) {
		this.opFlag = opFlag;
	}
	
    /**
     * @return opFlag
     */
	public String getOpFlag() {
		return this.opFlag;
	}
	
	/**
	 * @param cmsRefNo
	 */
	public void setCmsRefNo(String cmsRefNo) {
		this.cmsRefNo = cmsRefNo;
	}
	
    /**
     * @return cmsRefNo
     */
	public String getCmsRefNo() {
		return this.cmsRefNo;
	}
	
	/**
	 * @param productId
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
    /**
     * @return productId
     */
	public String getProductId() {
		return this.productId;
	}
	
	/**
	 * @param productCde
	 */
	public void setProductCde(String productCde) {
		this.productCde = productCde;
	}
	
    /**
     * @return productCde
     */
	public String getProductCde() {
		return this.productCde;
	}
	
	/**
	 * @param productGroupId
	 */
	public void setProductGroupId(String productGroupId) {
		this.productGroupId = productGroupId;
	}
	
    /**
     * @return productGroupId
     */
	public String getProductGroupId() {
		return this.productGroupId;
	}
	
	/**
	 * @param productGroupCde
	 */
	public void setProductGroupCde(String productGroupCde) {
		this.productGroupCde = productGroupCde;
	}
	
    /**
     * @return productGroupCde
     */
	public String getProductGroupCde() {
		return this.productGroupCde;
	}
	
	/**
	 * @param mgnPct
	 */
	public void setMgnPct(java.math.BigDecimal mgnPct) {
		this.mgnPct = mgnPct;
	}
	
    /**
     * @return mgnPct
     */
	public java.math.BigDecimal getMgnPct() {
		return this.mgnPct;
	}
	
	/**
	 * @param mgnOpDt
	 */
	public void setMgnOpDt(String mgnOpDt) {
		this.mgnOpDt = mgnOpDt;
	}
	
    /**
     * @return mgnOpDt
     */
	public String getMgnOpDt() {
		return this.mgnOpDt;
	}
	
	/**
	 * @param sysRefNo
	 */
	public void setSysRefNo(String sysRefNo) {
		this.sysRefNo = sysRefNo;
	}
	
    /**
     * @return sysRefNo
     */
	public String getSysRefNo() {
		return this.sysRefNo;
	}
	
	/**
	 * @param bizId
	 */
	public void setBizId(String bizId) {
		this.bizId = bizId;
	}
	
    /**
     * @return bizId
     */
	public String getBizId() {
		return this.bizId;
	}
	
	/**
	 * @param bizDt
	 */
	public void setBizDt(String bizDt) {
		this.bizDt = bizDt;
	}
	
    /**
     * @return bizDt
     */
	public String getBizDt() {
		return this.bizDt;
	}
	
	/**
	 * @param mmsRate
	 */
	public void setMmsRate(java.math.BigDecimal mmsRate) {
		this.mmsRate = mmsRate;
	}
	
    /**
     * @return mmsRate
     */
	public java.math.BigDecimal getMmsRate() {
		return this.mmsRate;
	}
	
	/**
	 * @param mgnAmt
	 */
	public void setMgnAmt(java.math.BigDecimal mgnAmt) {
		this.mgnAmt = mgnAmt;
	}
	
    /**
     * @return mgnAmt
     */
	public java.math.BigDecimal getMgnAmt() {
		return this.mgnAmt;
	}
	
	/**
	 * @param sameBizFlag
	 */
	public void setSameBizFlag(String sameBizFlag) {
		this.sameBizFlag = sameBizFlag;
	}
	
    /**
     * @return sameBizFlag
     */
	public String getSameBizFlag() {
		return this.sameBizFlag;
	}
	
	/**
	 * @param bicCode
	 */
	public void setBicCode(String bicCode) {
		this.bicCode = bicCode;
	}
	
    /**
     * @return bicCode
     */
	public String getBicCode() {
		return this.bicCode;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}