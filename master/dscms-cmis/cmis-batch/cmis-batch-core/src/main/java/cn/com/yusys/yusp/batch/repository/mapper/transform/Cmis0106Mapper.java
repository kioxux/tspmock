package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0105</br>
 * 任务名称：加工任务-业务处理-节假日  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0106Mapper {

    /**
     * 插入节假日配置
     * @param openDay
     * @return
     */
    int insertWbHolidayCfg(@Param("openDay") String openDay);

    /**
     * 更新节假日配置
     * @param openDay
     * @return
     */
    int updateWbHolidayCfg(@Param("openDay") String openDay);
}
