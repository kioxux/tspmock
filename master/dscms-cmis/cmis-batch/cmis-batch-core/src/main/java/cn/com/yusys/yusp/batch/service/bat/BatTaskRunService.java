/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.repository.mapper.bat.BatTaskRunMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTaskRunService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 16:29:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTaskRunService {
    private static final Logger logger = LoggerFactory.getLogger(BatTaskRunService.class);
    @Autowired
    private BatTaskRunMapper batTaskRunMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatTaskRun selectByPrimaryKey(String taskDate, String taskNo) {
        return batTaskRunMapper.selectByPrimaryKey(taskDate, taskNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatTaskRun> selectAll(QueryModel model) {
        List<BatTaskRun> records = (List<BatTaskRun>) batTaskRunMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatTaskRun> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTaskRun> list = batTaskRunMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatTaskRun record) {
        record.setInputDate(DateUtils.getCurrDateStr()); // 登记日期
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setCreateTime(DateUtils.getCurrTimestamp());// 创建时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskRunMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatTaskRun record) {
        record.setInputDate(DateUtils.getCurrDateStr()); // 登记日期
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setCreateTime(DateUtils.getCurrTimestamp());// 创建时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskRunMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatTaskRun record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskRunMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatTaskRun record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskRunMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String taskDate, String taskNo) {
        return batTaskRunMapper.deleteByPrimaryKey(taskDate, taskNo);
    }

    /**
     * 根据营业日期将任务运行日志中数据删除
     *
     * @param openDay
     * @return
     */
    public int deleteByOpenDay(String openDay) {
        logger.info("根据营业日期将任务运行日志中数据删除开始,请求参数为[{}]", openDay);
        int deleteByOpenDay = batTaskRunMapper.deleteByOpenDay(openDay);
        logger.info("根据营业日期将任务运行日志中数据删除结束,响应参数为[{}]", deleteByOpenDay);
        return deleteByOpenDay;
    }

    /**
     * @方法名称: queryNonSuccessTaskStatus
     * @方法描述: 查询任务状态不为执行成功的列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<BatTaskRun> queryNonSuccessTaskStatus(QueryModel model) {
        List<BatTaskRun> list = batTaskRunMapper.queryNonSuccessTaskStatus(model);
        return list;
    }

    /**
     * 查询待执行的任务
     *
     * @param btrlQm
     * @return
     */
    public List<BatTaskRun> selectPendingTask(QueryModel btrlQm) {
        List<BatTaskRun> list = batTaskRunMapper.selectPendingTask(btrlQm);
        return list;
    }

    /**
     * 根据优先级别[日终一阶段(加载外围系统文件)]查询任务信息
     *
     * @param queryMap
     * @return
     */
    public Map selectPriFlag01TaskInfo(Map queryMap) {
        return batTaskRunMapper.selectPriFlag01TaskInfo(queryMap);
    }

    /**
     * 根据优先级别[日终二阶段(台账和额度等加工任务)]查询任务信息
     *
     * @return
     */
    public Map selectPriFlag02TaskInfo(Map queryMap) {
        return batTaskRunMapper.selectPriFlag02TaskInfo(queryMap);
    }


    /**
     * 根据优先级别[日终三阶段(贷后管理风险分类)]查询任务信息
     *
     * @return
     */
    public Map selectPriFlag03TaskInfo(Map queryMap) {
        return batTaskRunMapper.selectPriFlag03TaskInfo(queryMap);
    }


    /**
     * 根据任务日期和任务级别查询bat_task_run中任务状态为执行失败的列表
     *
     * @param failedQueryModel
     */
    public List<BatTaskRun> selectFailedTaskList(QueryModel failedQueryModel) {
        List<BatTaskRun> list = batTaskRunMapper.selectFailedTaskList(failedQueryModel);
        return list;
    }
}
