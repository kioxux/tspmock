package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0101Mapper;
import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0416Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-贷后管理-投后定期检查 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0416Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0416Service.class);

    @Autowired
    private Cmis0416Mapper cmis0416Mapper;

    /**
     * 插入贷后检查任务表
     * @param openDay
     */
    public void cmis0416InsertPspTaskList(String openDay) {

        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList01 = cmis0416Mapper.insertPspTaskList01(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList01);

        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList02 = cmis0416Mapper.insertPspTaskList02(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList02);

        logger.info("插入同业申请表开始,请求参数为:[{}]", openDay);
        int insertIntbankAdmit = cmis0416Mapper.insertIntbankAdmit(openDay);
        logger.info("插入同业申请表结束,返回参数为:[{}]", insertIntbankAdmit);
    }
}
