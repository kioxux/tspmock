/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKcdbHkdjxx
 * @类描述: bat_s_core_kcdb_hkdjxx数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_core_kcdb_hkdjxx")
public class BatSCoreKcdbHkdjxx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
    /**
     * 法人代码
     **/
    @Id
    @Column(name = "farendma")
    private String farendma;
    /**
     * 老卡号
     **/
    @Id
    @Column(name = "LAOKAHAO")
    private String laokahao;

    /**
     * 老卡产品代码
     **/
    @Column(name = "LAOKCPDM", unique = false, nullable = true, length = 10)
    private String laokcpdm;
    /**
     * 新卡号
     **/
    @Id
    @Column(name = "XINKAHAO")
    private String xinkahao;
    /**
     * 新卡产品代码
     **/
    @Column(name = "XINKCPDM", unique = false, nullable = true, length = 10)
    private String xinkcpdm;
    /**
     * 换卡日期
     **/
    @Column(name = "HUANKARQ", unique = false, nullable = true, length = 8)
    private String huankarq;

    /**
     * 换卡机构
     **/
    @Column(name = "HUANKAJG", unique = false, nullable = true, length = 12)
    private String huankajg;

    /**
     * 换卡柜员
     **/
    @Column(name = "HUANKAGY", unique = false, nullable = true, length = 8)
    private String huankagy;
    /**
     * 换卡原因
     **/
    @Column(name = "HUANKAYY", unique = false, nullable = true, length = 1)
    private String huankayy;

    /**
     * 交易流水
     **/
    @Column(name = "JIOYLIUS", unique = false, nullable = true, length = 32)
    private String jioylius;

    /**
     * 领卡日期
     **/
    @Column(name = "LINGKARQ", unique = false, nullable = true, length = 8)
    private String lingkarq;

    /**
     * 备注信息
     **/
    @Column(name = "BEIZHUXX", unique = false, nullable = true, length = 1500)
    private String beizhuxx;

    /**
     * 序号值
     **/
    @Id
    @Column(name = "XUHAOZHI")
    private Long xuhaozhi;

    /**
     * 客户号
     **/
    @Column(name = "KEHUHAOO", unique = false, nullable = true, length = 16)
    private String kehuhaoo;

    /**
     * 采集处理号
     **/
    @Column(name = "CAIJICLH", unique = false, nullable = true, length = 32)
    private String caijiclh;

    /**
     * 分行标识
     **/
    @Column(name = "FENHBIOS", unique = false, nullable = false, length = 4)
    private String fenhbios;

    /**
     * 维护柜员
     **/
    @Column(name = "WEIHGUIY", unique = false, nullable = false, length = 8)
    private String weihguiy;

    /**
     * 维护机构
     **/
    @Column(name = "weihjigo", unique = false, nullable = false, length = 12)
    private String weihjigo;

    /**
     * 维护日期
     **/
    @Column(name = "WEIHRIQI", unique = false, nullable = false, length = 8)
    private String weihriqi;

    /**
     * 维护时间
     **/
    @Column(name = "WEIHSHIJ", unique = false, nullable = true, length = 9)
    private String weihshij;

    /**
     * 时间戳
     **/
    @Column(name = "SHIJCHUO", unique = false, nullable = false, length = 19)
    private Long shijchuo;

    /**
     * 记录状态 STD_JILUZTAI
     **/
    @Column(name = "JILUZTAI", unique = false, nullable = false, length = 1)
    private String jiluztai;
	
	/** data_date **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param laokahao
	 */
	public void setLaokahao(String laokahao) {
		this.laokahao = laokahao;
	}
	
    /**
     * @return laokahao
     */
	public String getLaokahao() {
		return this.laokahao;
	}
	
	/**
	 * @param laokcpdm
	 */
	public void setLaokcpdm(String laokcpdm) {
		this.laokcpdm = laokcpdm;
	}
	
    /**
     * @return laokcpdm
     */
	public String getLaokcpdm() {
		return this.laokcpdm;
	}
	
	/**
	 * @param xinkahao
	 */
	public void setXinkahao(String xinkahao) {
		this.xinkahao = xinkahao;
	}
	
    /**
     * @return xinkahao
     */
	public String getXinkahao() {
		return this.xinkahao;
	}
	
	/**
	 * @param xinkcpdm
	 */
	public void setXinkcpdm(String xinkcpdm) {
		this.xinkcpdm = xinkcpdm;
	}
	
    /**
     * @return xinkcpdm
     */
	public String getXinkcpdm() {
		return this.xinkcpdm;
	}
	
	/**
	 * @param huankarq
	 */
	public void setHuankarq(String huankarq) {
		this.huankarq = huankarq;
	}
	
    /**
     * @return huankarq
     */
	public String getHuankarq() {
		return this.huankarq;
	}
	
	/**
	 * @param huankajg
	 */
	public void setHuankajg(String huankajg) {
		this.huankajg = huankajg;
	}
	
    /**
     * @return huankajg
     */
	public String getHuankajg() {
		return this.huankajg;
	}
	
	/**
	 * @param huankagy
	 */
	public void setHuankagy(String huankagy) {
		this.huankagy = huankagy;
	}
	
    /**
     * @return huankagy
     */
	public String getHuankagy() {
		return this.huankagy;
	}
	
	/**
	 * @param huankayy
	 */
	public void setHuankayy(String huankayy) {
		this.huankayy = huankayy;
	}
	
    /**
     * @return huankayy
     */
	public String getHuankayy() {
		return this.huankayy;
	}
	
	/**
	 * @param jioylius
	 */
	public void setJioylius(String jioylius) {
		this.jioylius = jioylius;
	}
	
    /**
     * @return jioylius
     */
	public String getJioylius() {
		return this.jioylius;
	}
	
	/**
	 * @param lingkarq
	 */
	public void setLingkarq(String lingkarq) {
		this.lingkarq = lingkarq;
	}
	
    /**
     * @return lingkarq
     */
	public String getLingkarq() {
		return this.lingkarq;
	}
	
	/**
	 * @param beizhuxx
	 */
	public void setBeizhuxx(String beizhuxx) {
		this.beizhuxx = beizhuxx;
	}
	
    /**
     * @return beizhuxx
     */
	public String getBeizhuxx() {
		return this.beizhuxx;
	}
	
	/**
	 * @param xuhaozhi
	 */
	public void setXuhaozhi(Long xuhaozhi) {
		this.xuhaozhi = xuhaozhi;
	}
	
    /**
     * @return xuhaozhi
     */
	public Long getXuhaozhi() {
		return this.xuhaozhi;
	}
	
	/**
	 * @param kehuhaoo
	 */
	public void setKehuhaoo(String kehuhaoo) {
		this.kehuhaoo = kehuhaoo;
	}
	
    /**
     * @return kehuhaoo
     */
	public String getKehuhaoo() {
		return this.kehuhaoo;
	}
	
	/**
	 * @param caijiclh
	 */
	public void setCaijiclh(String caijiclh) {
		this.caijiclh = caijiclh;
	}
	
    /**
     * @return caijiclh
     */
	public String getCaijiclh() {
		return this.caijiclh;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}