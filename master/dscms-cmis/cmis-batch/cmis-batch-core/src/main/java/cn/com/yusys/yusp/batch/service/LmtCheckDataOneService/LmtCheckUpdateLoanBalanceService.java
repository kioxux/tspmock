/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.LmtCheckDataOneService;

import cn.com.yusys.yusp.batch.repository.mapper.check.LmtCheckUpdateLoanBalanceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckInsertContAccRelService
 * @类描述: #服务类
 * @功能描述: 数据更新额度校正-计算分项用信金额
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCheckUpdateLoanBalanceService {

    private static final Logger logger = LoggerFactory.getLogger(LmtCheckUpdateLoanBalanceService.class);

    @Autowired
    private LmtCheckUpdateLoanBalanceMapper lmtCheckUpdateLoanBalanceMapper;

    public void checkOutLmtCheckUpdateLoanBalance(String cusId){
        logger.info("8.LmtCheckUpdateLoanBalance数据更新额度校正-计算分项用信金额 --------start");
        /** 1、计算分项的用信金额 **/
        calculateSubLmtAmt(cusId) ;

        /** 2、一级分项用信余额汇总至二级分项,二级分项项下的用信台账余额之和 + 分项项下关联的非最高额授信协议的合同下的用信台账余额之和 + 分项项下关联的用信台账余额之和 **/
        collect1LevelSubLmtAmtTo2LevelSub(cusId) ;
        logger.info("8.LmtCheckUpdateLoanBalance数据更新额度校正-计算分项用信金额 --------end");
    }
    /**
     * 1、计算分项的用信金额
     */
    public int calculateSubLmtAmt(String cusId){
        logger.info("1、计算分项的用信金额 --------");
        lmtCheckUpdateLoanBalanceMapper.calculateSubLmtAmt1(cusId);
        lmtCheckUpdateLoanBalanceMapper.calculateSubLmtAmt2(cusId);
        lmtCheckUpdateLoanBalanceMapper.calculateSubLmtAmt3(cusId);
        return lmtCheckUpdateLoanBalanceMapper.calculateSubLmtAmt4(cusId);
    }

    /**
     * 2、一级分项用信余额汇总至二级分项
     *  二级分项项下的用信台账余额之和 + 分项项下关联的非最高额授信协议的合同下的用信台账余额之和 + 分项项下关联的用信台账余额之和
     */
    public int collect1LevelSubLmtAmtTo2LevelSub(String cusId){
        logger.info("2、一级分项用信余额汇总至二级分项 二级分项项下的用信台账余额之和 + 分项项下关联的非最高额授信协议的合同下的用信台账余额之和 + 分项项下关联的用信台账余额之和 --------");
        lmtCheckUpdateLoanBalanceMapper.collect1LevelSubLmtAmtTo2LevelSub1(cusId);
        lmtCheckUpdateLoanBalanceMapper.collect1LevelSubLmtAmtTo2LevelSub2(cusId);
        lmtCheckUpdateLoanBalanceMapper.collect1LevelSubLmtAmtTo2LevelSub3(cusId);
        return lmtCheckUpdateLoanBalanceMapper.collect1LevelSubLmtAmtTo2LevelSub4(cusId);
    }

}
