package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0114Mapper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0114</br>
 * 任务名称：加工任务-业务处理-lpr利率同步  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0114Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0114Service.class);

    @Autowired
    private Cmis0114Mapper cmis0114Mapper;

    /**
     * 插入LPR配置表
     *
     * @param openDay
     */
    public void cmis0114InsertCfgLprRate(String openDay) {

        logger.info("清空LPR配置表开始,请求参数为:[{}]", openDay);
        int truncatecfgLprRate = cmis0114Mapper.truncatecfgLprRate(openDay);
        logger.info("清空LPR配置表结束,返回参数为:[{}]", truncatecfgLprRate);


        logger.info("插入LPR配置表开始,请求参数为:[{}]", openDay);
        int insertCfgLprRate = cmis0114Mapper.insertCfgLprRate(openDay);
        logger.info("插入LPR配置表结束,返回参数为:[{}]", insertCfgLprRate);


        logger.info(" 将lpr利率表的生效日期减一 开始,请求参数为:[{}]", openDay);
        int updateCfgLprRate = cmis0114Mapper.updateCfgLprRate(openDay);
        logger.info(" 将lpr利率表的生效日期减一  结束,返回参数为:[{}]", updateCfgLprRate);

    }
}
