package cn.com.yusys.yusp.batch.service.extract.core;

import cn.com.yusys.yusp.batch.repository.mapper.extract.core.Core0101Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CORE0101</br>
 * 任务名称：文件处理任务-核心系统-营改增信息[bat_core_t_ygz]</br>
 *
 * @author chenchao
 * @version 1.0
 * @since 2021年7月6日 下午11:56:54
 */
@Service
@Transactional
public class Core0101Service {
    private static final Logger logger = LoggerFactory.getLogger(Core0101Service.class);

    @Autowired
    private Core0101Mapper core0101Mapper;

    /**
     * 清空和加工到营改增信息
     *
     * @param openDay
     */
    public void core0101InsertYgz(String openDay) {
        logger.info("清空营改增信息开始");
        core0101Mapper.truncateYgz();
        logger.info("清空营改增信息结束");

        logger.info("加工到营改增信息开始，请求参数:[{}]", openDay);
        int insertYgz = core0101Mapper.insertYgz(openDay);
        logger.info("加工到营改增信息开始，返回参数:[{}]", insertYgz);
    }
}
