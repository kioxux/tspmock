package cn.com.yusys.yusp.batch.job.common;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.SimpleBinaryBufferedReaderFactory;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DefaultFieldSetFactory;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * ODS卸数平台文件(S表)公共读取数据
 *
 * @param <T>
 * @date 2020-01-17 15:49
 */
public class OdsSCommonFileItemReader<T> extends FlatFileItemReader<T> {
    public OdsSCommonFileItemReader() {
    }

    public OdsSCommonFileItemReader(Class clz, String filepath, String openDay, String delimiter, String Encoding) {
        this.init(clz, filepath, openDay, delimiter, Encoding, 0);
    }

    /**
     * 将文件中内容转换成对象
     *
     * @param clz       待加载文件对应的实体类对象
     * @param filepath  文件路径
     * @param openDay   营业日期
     * @param delimiter 分隔符
     * @param encoding  文件编码
     * @param skipNum   忽略行数
     */
    public void init(Class<T> clz, String filepath, String openDay, String delimiter, String encoding, int skipNum) {
        //设置源文件路经
        FileSystemResource fileSystemResource = new FileSystemResource(new File(filepath));
        setResource(fileSystemResource);
        setEncoding(encoding);
        //LineMapper给定当前行和与其关联的行号，映射器应返回结果域对象
        DefaultLineMapper defaultLineMapper = new DefaultLineMapper();

        //将输入行转换为a的抽象
        DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
        delimitedLineTokenizer.setFieldSetFactory(new DefaultFieldSetFactory());
        //设置分隔符
        delimitedLineTokenizer.setDelimiter(delimiter);
        // 部分文件最后一个字段为空，spring batch 默认不处理该字段
        delimitedLineTokenizer.setStrict(false);
        defaultLineMapper.setLineTokenizer(delimitedLineTokenizer);
        //读取设置字段
        Field[] fields = clz.getDeclaredFields();
        List<String> list = new ArrayList<>();
        for (Field field : fields) {
            if (!Modifier.isStatic(field.getModifiers()) && !Objects.equals("dataDate", field.getName())) {
                list.add(field.getName());
            }
        }
        delimitedLineTokenizer.setNames(list.toArray(new String[0]));

        BeanWrapperFieldSetMapper fieldSetMapper = new BeanWrapperFieldSetMapper();
        fieldSetMapper.setTargetType(clz);
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);
        setLineMapper(defaultLineMapper);
        setLinesToSkip(skipNum);
        SimpleBinaryBufferedReaderFactory simpleBinaryBufferedReaderFactory = new SimpleBinaryBufferedReaderFactory();
        simpleBinaryBufferedReaderFactory.setLineEnding("\n");
        setBufferedReaderFactory(simpleBinaryBufferedReaderFactory);
    }

}
