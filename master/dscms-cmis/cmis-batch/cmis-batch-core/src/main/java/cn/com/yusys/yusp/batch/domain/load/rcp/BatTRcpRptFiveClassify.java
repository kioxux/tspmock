/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpRptFiveClassify
 * @类描述: bat_t_rcp_rpt_five_classify数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:04:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_rcp_rpt_five_classify")
public class BatTRcpRptFiveClassify extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "seral_no")
	private String seralNo;
	
	/** 流程审批状态 **/
	@Column(name = "wf_appr_sts", unique = false, nullable = true, length = 3)
	private String wfApprSts;
	
	/** 分类级别(新) **/
	@Column(name = "classify_level_new", unique = false, nullable = true, length = 2)
	private String classifyLevelNew;
	
	/** 借据编号 **/
	@Column(name = "bill_no", unique = false, nullable = true, length = 100)
	private String billNo;
	
	/** 客户姓名 **/
	@Column(name = "cust_name", unique = false, nullable = true, length = 60)
	private String custName;
	
	/** 证件类型 10 身份证 **/
	@Column(name = "cert_type", unique = false, nullable = true, length = 2)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "cert_code", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 分类级别 **/
	@Column(name = "classify_level", unique = false, nullable = true, length = 2)
	private String classifyLevel;
	
	/** 逾期天数 **/
	@Column(name = "overdue_day", unique = false, nullable = true, length = 10)
	private Integer overdueDay;
	
	/** 贷款余额 **/
	@Column(name = "loan_bal", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal loanBal;
	
	/** 贷款金额 **/
	@Column(name = "loan_amt", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款状态 **/
	@Column(name = "cont_status", unique = false, nullable = true, length = 3)
	private String contStatus;
	
	/** 数据日期 **/
	@Column(name = "rcp_data_date", unique = false, nullable = true, length = 10)
	private String rcpDataDate;
	
	/** 产品代码 **/
	@Column(name = "prd_code", unique = false, nullable = true, length = 12)
	private String prdCode;
	
	
	/**
	 * @param seralNo
	 */
	public void setSeralNo(String seralNo) {
		this.seralNo = seralNo;
	}
	
    /**
     * @return seralNo
     */
	public String getSeralNo() {
		return this.seralNo;
	}
	
	/**
	 * @param wfApprSts
	 */
	public void setWfApprSts(String wfApprSts) {
		this.wfApprSts = wfApprSts;
	}
	
    /**
     * @return wfApprSts
     */
	public String getWfApprSts() {
		return this.wfApprSts;
	}
	
	/**
	 * @param classifyLevelNew
	 */
	public void setClassifyLevelNew(String classifyLevelNew) {
		this.classifyLevelNew = classifyLevelNew;
	}
	
    /**
     * @return classifyLevelNew
     */
	public String getClassifyLevelNew() {
		return this.classifyLevelNew;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param classifyLevel
	 */
	public void setClassifyLevel(String classifyLevel) {
		this.classifyLevel = classifyLevel;
	}
	
    /**
     * @return classifyLevel
     */
	public String getClassifyLevel() {
		return this.classifyLevel;
	}
	
	/**
	 * @param overdueDay
	 */
	public void setOverdueDay(Integer overdueDay) {
		this.overdueDay = overdueDay;
	}
	
    /**
     * @return overdueDay
     */
	public Integer getOverdueDay() {
		return this.overdueDay;
	}
	
	/**
	 * @param loanBal
	 */
	public void setLoanBal(java.math.BigDecimal loanBal) {
		this.loanBal = loanBal;
	}
	
    /**
     * @return loanBal
     */
	public java.math.BigDecimal getLoanBal() {
		return this.loanBal;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param contStatus
	 */
	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}
	
    /**
     * @return contStatus
     */
	public String getContStatus() {
		return this.contStatus;
	}
	
	/**
	 * @param rcpDataDate
	 */
	public void setRcpDataDate(String rcpDataDate) {
		this.rcpDataDate = rcpDataDate;
	}
	
    /**
     * @return rcpDataDate
     */
	public String getRcpDataDate() {
		return this.rcpDataDate;
	}
	
	/**
	 * @param prdCode
	 */
	public void setPrdCode(String prdCode) {
		this.prdCode = prdCode;
	}
	
    /**
     * @return prdCode
     */
	public String getPrdCode() {
		return this.prdCode;
	}


}