/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.pjp;

import cn.com.yusys.yusp.batch.domain.load.pjp.BatSPjpBtDiscount;
import cn.com.yusys.yusp.batch.service.load.pjp.BatSPjpBtDiscountService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtDiscountResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-02 08:25:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batspjpbtdiscount")
public class BatSPjpBtDiscountResource {
    @Autowired
    private BatSPjpBtDiscountService batSPjpBtDiscountService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSPjpBtDiscount>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSPjpBtDiscount> list = batSPjpBtDiscountService.selectAll(queryModel);
        return new ResultDto<List<BatSPjpBtDiscount>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSPjpBtDiscount>> index(QueryModel queryModel) {
        List<BatSPjpBtDiscount> list = batSPjpBtDiscountService.selectByModel(queryModel);
        return new ResultDto<List<BatSPjpBtDiscount>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{discBillId}")
    protected ResultDto<BatSPjpBtDiscount> show(@PathVariable("discBillId") String discBillId) {
        BatSPjpBtDiscount batSPjpBtDiscount = batSPjpBtDiscountService.selectByPrimaryKey(discBillId);
        return new ResultDto<BatSPjpBtDiscount>(batSPjpBtDiscount);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSPjpBtDiscount> create(@RequestBody BatSPjpBtDiscount batSPjpBtDiscount) throws URISyntaxException {
        batSPjpBtDiscountService.insert(batSPjpBtDiscount);
        return new ResultDto<BatSPjpBtDiscount>(batSPjpBtDiscount);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSPjpBtDiscount batSPjpBtDiscount) throws URISyntaxException {
        int result = batSPjpBtDiscountService.update(batSPjpBtDiscount);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{discBillId}")
    protected ResultDto<Integer> delete(@PathVariable("discBillId") String discBillId) {
        int result = batSPjpBtDiscountService.deleteByPrimaryKey(discBillId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSPjpBtDiscountService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
