package cn.com.yusys.yusp.batch.repository.mapper.timed;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 业务逻辑Dao类：</br>
 * 定时任务处理类:自动化贷后跑批白名单  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月20日 下午11:56:54
 */
public interface BatchTimedTask0001Mapper {
    /**
     * 根据对公是否需要查询人行征信 查询对公自动化贷后跑批白名单
     *
     * @param queryMap
     * @return
     */
    List<Map<String, String>> selectNeedQueryCrereport(Map<String, String> queryMap);



    /**
     * 更新对公自动化贷后跑批白名单中小安涉诉标志
     *
     * @param updateCrereportMap
     * @return
     */
    int updateCrereportXACaseFlagMap(Map<String, String> updateCrereportMap);


    /**
     * 更新对公自动化贷后跑批白名单中报告编号
     *
     * @param updateCrereportMap
     * @return
     */
    int updateCrereportMap(Map<String, String> updateCrereportMap);

    /**
     * 查询对公自动化贷后跑批白名单所有数据
     *
     * @return
     */
    List<Map<String, String>> selectCorpList();

    /**
     * 根据个人或者实际控制人是否需要查询人行征信查询个人自动化贷后跑批白名单
     *
     * @param queryPerMap
     * @return
     */
    List<Map<String, String>> selectPerNeedQueryCrereport(Map<String, String> queryPerMap);

    /**
     * 更新个人自动化贷后跑批白名单中报告编号
     *
     * @param updateCrereportMap
     * @return
     */
    int updatePerCrereportMap(Map<String, String> updateCrereportMap);

    /**
     * 查询个人自动化贷后跑批白名单所有数据
     *
     * @return
     */
    List<Map<String, String>> selectPerList();

    /**
     * 根据客户ID查询征信授权书信息表
     * @param cusId
     * @return
     */
    Map<String, String> selectCreditAuthbookInfo(@Param("cusId")String cusId);

    /**
     * 登记对公自动化贷后跑批白名单调用征信二代返回的错误信息
     *
     * @param updateCrereportMap
     * @return
     */
    int updateCrereportErrMsgMap(Map<String, String> updateCrereportMap);

    /**
     * 登记对公自动化贷后跑批白名单调用外围系统错误信息
     *
     * @param updateCrereportMap
     * @return
     */
    int updateCrereportErrMsgConcatMap(Map<String, String> updateCrereportMap);

    /**
     * 登记个人自动化贷后跑批白名单调用征信二代返回的错误信息
     *
     * @param updateCrereportMap
     * @return
     */
    int updatePerCrereporErrMsgMap(Map<String, String> updateCrereportMap);

    /**
     * 登记个人自动化贷后跑批白名单调用外围系统错误信息
     *
     * @param updateCrereportMap
     * @return
     */
    int updatePerCrereporErrMsgConcatMap(Map<String, String> updateCrereportMap);

    /**
     * 置空登记的对公自动化贷后跑批白名单报错的信息
     *
     * @param
     * @return
     */
    int updateErrMsgByAutoPspWhiteInfoCorp(Map<String, String> updateCrereportMap);

    /**
     * 置空登记的个人自动化贷后跑批白名单报错信息
     *
     * @param
     * @return
     */
    int updateErrMsgByAutoPspWhiteInfoPer(Map<String, String> updateCrereportMap);
}
