/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.djk;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.djk.BatSDjkDataLoan;
import cn.com.yusys.yusp.batch.repository.mapper.load.djk.BatSDjkDataLoanMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSDjkDataLoanService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-07 10:58:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSDjkDataLoanService {
    private static final Logger logger = LoggerFactory.getLogger(BatSDjkDataLoanService.class);
    @Autowired
    private BatSDjkDataLoanMapper batSDjkDataLoanMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSDjkDataLoan selectByPrimaryKey(String loanId, String acctNo) {
        return batSDjkDataLoanMapper.selectByPrimaryKey(loanId, acctNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSDjkDataLoan> selectAll(QueryModel model) {
        List<BatSDjkDataLoan> records = (List<BatSDjkDataLoan>) batSDjkDataLoanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSDjkDataLoan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSDjkDataLoan> list = batSDjkDataLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSDjkDataLoan record) {
        return batSDjkDataLoanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSDjkDataLoan record) {
        return batSDjkDataLoanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSDjkDataLoan record) {
        return batSDjkDataLoanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSDjkDataLoan record) {
        return batSDjkDataLoanMapper.updateByPrimaryKeySelective(record);
    }
    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建分期信息文件开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_djk_data_loan");
        logger.info("重建分期信息文件结束");
        return 0;
    }
    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String loanId, String acctNo) {
        return batSDjkDataLoanMapper.deleteByPrimaryKey(loanId, acctNo);
    }

    /**
     * 将T表数据merge到S表
     *
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        // logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]开始,请求参数为:[{}]", openDay);
        // int updateSResult = batSDjkDataLoanMapper.updateDataDateByOpenDay(openDay);
        // logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]结束,返回参数为:[{}]", updateSResult);
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        logger.info("更新S表中[数值类型字段为Decimal类型值]开始,请求参数为:[{}]", openDay);
        int updateSResultField = batSDjkDataLoanMapper.updateSResultField(openDay);
        logger.info("更新S表中[数值类型字段为Decimal类型值]结束,返回参数为:[{}]", updateSResultField);
    }
}
