package cn.com.yusys.yusp.batch.util;

import cn.com.yusys.yusp.commons.util.StringUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Objects;

/**
 * 贷记卡文件解析工具类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public class DjkFileParseUtils {
    public static void main(String[] args) throws IOException {
        String filePath = "D:\\Backup\\test.txt";
        File file = new File(filePath);
        InputStream is = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(is, Charset.forName("GBK")));
        String line = StringUtils.EMPTY;
        while ((line = br.readLine()) != null) {
//            读取设置字段为:[acctNo],长度为:[9],范围为:[1-9]
//            读取设置字段为:[custId],长度为:[9],范围为:[10-18]
//            读取设置字段为:[creditLimit],长度为:[13],范围为:[28-40]
//            读取设置字段为:[qualGraceBal],长度为:[15],范围为:[145-159]
//            读取设置字段为:[setupDate],长度为:[8],范围为:[173-180]
//            读取设置字段为:[bankCustomerId],长度为:[20],范围为:[875-894]
            String acctNo = DjkFileParseUtils.substringByBytes(line, 0, 8);
            String custId = DjkFileParseUtils.substringByBytes(line, 9, 9);
            String creditLimit = DjkFileParseUtils.substringByBytes(line, 27, 13);
            String qualGraceBal = DjkFileParseUtils.substringByBytes(line, 144, 15);
            String setupDate = DjkFileParseUtils.substringByBytes(line, 172, 8);
            String bankCustomerId = DjkFileParseUtils.substringByBytes(line, 874, 20);
            System.out.println(acctNo + "--> " + acctNo.length());
            System.out.println(custId + "--> " + custId.length());
            System.out.println(creditLimit + "--> " + creditLimit.length());
            System.out.println(qualGraceBal + "--> " + qualGraceBal.length());
            System.out.println(setupDate + "--> " + setupDate.length());
            System.out.println(bankCustomerId + "--> " + bankCustomerId.length());
        }
    }

    public static String substringByBytes(String original, int start, int offset) {
        if (Objects.isNull(original) || Objects.equals(original, ""))
            return original;
        if (offset < 0)
            return original;
        if (start < 0)
            start = 0;
        StringBuffer buff = new StringBuffer();
        try {
            if (start > getStringBytesLengths(original)) {
                return null;
            }
            int length = 0;
            char ch;
            for (int i = 0; i < original.toCharArray().length; i++) {
                ch = original.charAt(i);
                if (start == 0) {
                    length += String.valueOf(ch).getBytes("GBK").length;
                    if (length <= offset) {
                        buff.append(ch);
                    } else {
                        break;
                    }
                } else {
                    length += String.valueOf(ch).getBytes("GBK").length;
                    if (length >= start && length <= start + offset) {
                        buff.append(ch);
                    }
                    if (length > start + offset) {
                        break;
                    }
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new String(buff);
    }

    public static int getStringBytesLengths(String args) throws UnsupportedEncodingException {
        return args != null && args != "" ? args.getBytes("GBK").length : 0;
    }
}
