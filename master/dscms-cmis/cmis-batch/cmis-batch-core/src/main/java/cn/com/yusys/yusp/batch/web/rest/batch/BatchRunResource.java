package cn.com.yusys.yusp.batch.web.rest.batch;

import cn.com.yusys.yusp.batch.dto.batch.ParallelRunReqDto;
import cn.com.yusys.yusp.batch.dto.batch.ParallelRunRespDto;
import cn.com.yusys.yusp.batch.dto.batch.SingleRunReqDto;
import cn.com.yusys.yusp.batch.dto.batch.SingleRunRespDto;
import cn.com.yusys.yusp.batch.util.HttpCallerUtil;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/batchrunresource")
public class BatchRunResource {

    private static final Logger logger = LoggerFactory.getLogger(BatchRunResource.class);

    @PostMapping("/runFpt")
    public @ResponseBody
    ParallelRunRespDto runFpt() throws Exception {
        ParallelRunReqDto parallelRunReqDto0102 = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDto0102s = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto fpt00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fpt00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fpt00002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fpt00002").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fpt00003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fpt00003").with(SingleRunReqDto::setOpenDay, openDay).build();

        singleRunReqDto0102s.add(fpt00001);
        singleRunReqDto0102s.add(fpt00002);
        singleRunReqDto0102s.add(fpt00003);

        parallelRunReqDto0102.setSingleRunReqDtos(singleRunReqDto0102s);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto0102));
        java.util.List<SingleRunRespDto> singleRunRespDto0102s = singleRunReqDto0102s.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto0102 = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDto0102 = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto0102;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto0102 = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDto0102s).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto0102));
        return parallelRunRespDto0102;
    }

    @PostMapping("/runCore")
    public @ResponseBody
    ParallelRunRespDto runCore() throws Exception {
        ParallelRunReqDto parallelRunReqDto = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDtos = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto core0001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0002").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0003").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0004 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0004").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0005 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0005").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0006 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0006").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0007 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0007").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0008 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0008").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0009 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0009").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0010 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0010").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0011 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0011").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0012 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0012").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0013 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0013").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0014 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0014").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0015 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0015").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto core0016 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "core0016").with(SingleRunReqDto::setOpenDay, openDay).build();

        singleRunReqDtos.add(core0009);
        singleRunReqDtos.add(core0007);
        singleRunReqDtos.add(core0010);
        singleRunReqDtos.add(core0013);

        singleRunReqDtos.add(core0001);
        singleRunReqDtos.add(core0002);
        singleRunReqDtos.add(core0003);
        singleRunReqDtos.add(core0004);
        singleRunReqDtos.add(core0005);
        singleRunReqDtos.add(core0006);
        singleRunReqDtos.add(core0008);
        singleRunReqDtos.add(core0011);
        singleRunReqDtos.add(core0012);
        singleRunReqDtos.add(core0014);
        singleRunReqDtos.add(core0015);
        singleRunReqDtos.add(core0016);
        parallelRunReqDto.setSingleRunReqDtos(singleRunReqDtos);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto));
        java.util.List<SingleRunRespDto> singleRunRespDtos = singleRunReqDtos.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDto = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtos).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto));
        return parallelRunRespDto;
    }

    @PostMapping("/runGjp")
    public @ResponseBody
    ParallelRunRespDto runGjp() throws Exception {
        ParallelRunReqDto parallelRunReqDtoGjp = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDtoGjp = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto gjp00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gjp00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto gjp00002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gjp00002").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto gjp00003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gjp00003").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto gjp00004 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gjp00004").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto gjp00005 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gjp00005").with(SingleRunReqDto::setOpenDay, openDay).build();

        singleRunReqDtoGjp.add(gjp00001);
        singleRunReqDtoGjp.add(gjp00002);
        singleRunReqDtoGjp.add(gjp00003);
        singleRunReqDtoGjp.add(gjp00004);
        singleRunReqDtoGjp.add(gjp00005);

        SingleRunReqDto gjp00006 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gjp00006").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto gjp00007 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gjp00007").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto gjp00008 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gjp00008").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto gjp00009 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gjp00009").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto gjp00010 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gjp00010").with(SingleRunReqDto::setOpenDay, openDay).build();

        singleRunReqDtoGjp.add(gjp00006);
        singleRunReqDtoGjp.add(gjp00007);
        singleRunReqDtoGjp.add(gjp00008);
        singleRunReqDtoGjp.add(gjp00009);
        singleRunReqDtoGjp.add(gjp00010);
        parallelRunReqDtoGjp.setSingleRunReqDtos(singleRunReqDtoGjp);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDtoGjp));
        java.util.List<SingleRunRespDto> singleRunRespDtoGjps = singleRunReqDtoGjp.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDtoGjp = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDtoGjp = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDtoGjp;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDtoGjp = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtoGjps).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDtoGjp));
        return parallelRunRespDtoGjp;
    }

    @PostMapping("/runPjp")
    public @ResponseBody
    ParallelRunRespDto runPjp() throws Exception {
        ParallelRunReqDto parallelRunReqDtoPjp = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDtoPjps = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto pjp00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto pjp00002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00002").with(SingleRunReqDto::setOpenDay, openDay).build();

        SingleRunReqDto pjp00003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00003").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto pjp00004 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00004").with(SingleRunReqDto::setOpenDay, openDay).build();

        SingleRunReqDto pjp00005 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00005").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto pjp00006 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00006").with(SingleRunReqDto::setOpenDay, openDay).build();

        SingleRunReqDto pjp00007 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00007").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto pjp00008 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00008").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto pjp00009 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00009").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto pjp00010 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00010").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto pjp00011 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "pjp00011").with(SingleRunReqDto::setOpenDay, openDay).build();

        singleRunReqDtoPjps.add(pjp00003);
        singleRunReqDtoPjps.add(pjp00001);
        singleRunReqDtoPjps.add(pjp00002);
        singleRunReqDtoPjps.add(pjp00004);
        singleRunReqDtoPjps.add(pjp00005);
        singleRunReqDtoPjps.add(pjp00006);
        singleRunReqDtoPjps.add(pjp00007);
        singleRunReqDtoPjps.add(pjp00008);
        singleRunReqDtoPjps.add(pjp00009);
        singleRunReqDtoPjps.add(pjp00010);
        singleRunReqDtoPjps.add(pjp00011);

        parallelRunReqDtoPjp.setSingleRunReqDtos(singleRunReqDtoPjps);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDtoPjp));
        java.util.List<SingleRunRespDto> singleRunRespDtoPjps = singleRunReqDtoPjps.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDtoPjp = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDtoPjp = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDtoPjp;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDtoPjp = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtoPjps).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDtoPjp));
        return parallelRunRespDtoPjp;
    }


    @PostMapping("/runYpp")
    public @ResponseBody
    ParallelRunRespDto runYpp() throws Exception {
        ParallelRunReqDto parallelRunReqDto01 = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDto01s = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期

        SingleRunReqDto ypp00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ypp00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ypp00002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ypp00002").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ypp00003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ypp00003").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ypp00004 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ypp00004").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ypp00005 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ypp00005").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ypp00006 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ypp00006").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ypp00007 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ypp00007").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ypp00008 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ypp00008").with(SingleRunReqDto::setOpenDay, openDay).build();

        // singleRunReqDto01s.add(ypp00001);
        singleRunReqDto01s.add(ypp00002);
        singleRunReqDto01s.add(ypp00003);
        singleRunReqDto01s.add(ypp00004);
        singleRunReqDto01s.add(ypp00005);
        singleRunReqDto01s.add(ypp00006);
        singleRunReqDto01s.add(ypp00007);
        singleRunReqDto01s.add(ypp00008);
        parallelRunReqDto01.setSingleRunReqDtos(singleRunReqDto01s);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto01));
        java.util.List<SingleRunRespDto> singleRunRespDto01s = singleRunReqDto01s.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto01 = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDto01 = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto01;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto01 = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDto01s).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto01));
        return parallelRunRespDto01;
    }


    @PostMapping("/runRlp01")
    public @ResponseBody
    ParallelRunRespDto runRlp01() throws Exception {
        ParallelRunReqDto parallelRunReqDto01 = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDto01s = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期

        SingleRunReqDto rlp00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rlp00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDto01s.add(rlp00001);
        parallelRunReqDto01.setSingleRunReqDtos(singleRunReqDto01s);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto01));
        java.util.List<SingleRunRespDto> singleRunRespDto01s = singleRunReqDto01s.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto01 = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDto01 = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto01;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto01 = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDto01s).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto01));
        return parallelRunRespDto01;
    }

    @PostMapping("/runSbs0102")
    public @ResponseBody
    ParallelRunRespDto runSbs0102() throws Exception {
        ParallelRunReqDto parallelRunReqDto0102 = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDto0102s = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto sbs00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "sbs00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto sbs00002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "sbs00002").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDto0102s.add(sbs00001);
        singleRunReqDto0102s.add(sbs00002);
        parallelRunReqDto0102.setSingleRunReqDtos(singleRunReqDto0102s);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto0102));
        java.util.List<SingleRunRespDto> singleRunRespDto0102s = singleRunReqDto0102s.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto0102 = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDto0102 = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto0102;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto0102 = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDto0102s).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto0102));
        return parallelRunRespDto0102;
    }


    @PostMapping("/runRis")
    public @ResponseBody
    ParallelRunRespDto runRis() throws Exception {
        ParallelRunReqDto parallelRunReqDtoRis = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDtoRisList = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto ris00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ris00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ris00002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ris00002").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDtoRisList.add(ris00001);
        singleRunReqDtoRisList.add(ris00002);
        parallelRunReqDtoRis.setSingleRunReqDtos(singleRunReqDtoRisList);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDtoRis));
        java.util.List<SingleRunRespDto> singleRunRespDtoRisList = singleRunReqDtoRisList.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDtoRis = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDtoRis = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDtoRis;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDtoRis = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtoRisList).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDtoRis));
        return parallelRunRespDtoRis;
    }


    @PostMapping("/runOds")
    public @ResponseBody
    ParallelRunRespDto runOds() throws Exception {
        ParallelRunReqDto parallelRunReqDtoOds = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDtoOdsList = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto ods00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ods00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ods00002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ods00002").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ods00003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ods00003").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto ods00004 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "ods00004").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDtoOdsList.add(ods00001);
        singleRunReqDtoOdsList.add(ods00002);
        singleRunReqDtoOdsList.add(ods00003);
        singleRunReqDtoOdsList.add(ods00004);
        parallelRunReqDtoOds.setSingleRunReqDtos(singleRunReqDtoOdsList);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDtoOds));
        java.util.List<SingleRunRespDto> singleRunRespDtoOdsList = singleRunReqDtoOdsList.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDtoOds = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDtoOds = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDtoOds;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDtoRis = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtoOdsList).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDtoRis));
        return parallelRunRespDtoRis;
    }

    /**
     * 批前备份日表任务
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/runBakD")
    public @ResponseBody
    ParallelRunRespDto runBakD() throws Exception {
        ParallelRunReqDto parallelRunReqDto = new ParallelRunReqDto();
        java.util.List<SingleRunReqDto> singleRunReqDtos = new ArrayList<>();
        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto bakD0000 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0000").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0002").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0003").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0004 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0004").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0005 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0005").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0006 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0006").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0007 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0007").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0008 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0008").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0009 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0009").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0010 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0010").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0011 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0011").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0012 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0012").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0013 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0013").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0014 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0014").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0015 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0015").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0016 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0016").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0017 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0017").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0018 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0018").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0019 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0019").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0020 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0020").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0021 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0021").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0022 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0022").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0023 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0023").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0024 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0024").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0025 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0025").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0026 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0026").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0027 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0027").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0028 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0028").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0029 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0029").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0030 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0030").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakD0031 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakD0031").with(SingleRunReqDto::setOpenDay, openDay).build();

        singleRunReqDtos.add(bakD0000);
        singleRunReqDtos.add(bakD0002);
        singleRunReqDtos.add(bakD0006);
        singleRunReqDtos.add(bakD0010);
        singleRunReqDtos.add(bakD0011);
        singleRunReqDtos.add(bakD0022);
        singleRunReqDtos.add(bakD0026);
        singleRunReqDtos.add(bakD0004);

        singleRunReqDtos.add(bakD0001);
        singleRunReqDtos.add(bakD0003);
        singleRunReqDtos.add(bakD0005);
        singleRunReqDtos.add(bakD0007);
        singleRunReqDtos.add(bakD0008);
        singleRunReqDtos.add(bakD0009);
        singleRunReqDtos.add(bakD0012);
        singleRunReqDtos.add(bakD0013);
        singleRunReqDtos.add(bakD0014);
        singleRunReqDtos.add(bakD0015);
        singleRunReqDtos.add(bakD0016);
        singleRunReqDtos.add(bakD0017);
        singleRunReqDtos.add(bakD0018);
        singleRunReqDtos.add(bakD0019);
        singleRunReqDtos.add(bakD0020);
        singleRunReqDtos.add(bakD0021);

        singleRunReqDtos.add(bakD0023);
        singleRunReqDtos.add(bakD0024);
        singleRunReqDtos.add(bakD0025);

        singleRunReqDtos.add(bakD0027);
        singleRunReqDtos.add(bakD0028);

        singleRunReqDtos.add(bakD0029);
        singleRunReqDtos.add(bakD0030);
        singleRunReqDtos.add(bakD0031);

        parallelRunReqDto.setSingleRunReqDtos(singleRunReqDtos);
        logger.info("处理[批前备份日表任务]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto));

        java.util.List<SingleRunRespDto> singleRunRespDtos = singleRunReqDtos.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[批前备份日表任务]逻辑前，睡眠5秒开始");
                        TimeUnit.SECONDS.sleep(5);
                        logger.info("处理[批前备份日表任务]逻辑前，睡眠5秒结束");
                        singleRunRespDto = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtos).build();
        logger.info("处理[批前备份日表任务]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto));
        return parallelRunRespDto;
    }


    @PostMapping("/runFls010203")
    public @ResponseBody
    ParallelRunRespDto runFls010203() throws Exception {
        ParallelRunReqDto parallelRunReqDto010203 = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDto010203s = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto fls00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls00002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls00002").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls00003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls00003").with(SingleRunReqDto::setOpenDay, openDay).build();

        singleRunReqDto010203s.add(fls00001);
        singleRunReqDto010203s.add(fls00002);
        singleRunReqDto010203s.add(fls00003);
        parallelRunReqDto010203.setSingleRunReqDtos(singleRunReqDto010203s);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto010203));
        java.util.List<SingleRunRespDto> singleRunRespDto010203s = singleRunReqDto010203s.parallelStream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto010203 = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDto010203 = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto010203;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto010203 = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDto010203s).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto010203));
        return parallelRunRespDto010203;
    }

    @PostMapping("/runFls01")
    public @ResponseBody
    ParallelRunRespDto runFls01() throws Exception {
        ParallelRunReqDto parallelRunReqDto = new ParallelRunReqDto();
        java.util.List<SingleRunReqDto> singleRunReqDtos = new ArrayList<>();
        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto fls01001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01002").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01003").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01004 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01004").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01005 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01005").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01006 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01006").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01007 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01007").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01008 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01008").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01009 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01009").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01010 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01010").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01011 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01011").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01012 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01012").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01013 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01013").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01014 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01014").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01015 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01015").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01016 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01016").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01017 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01017").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01018 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01018").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01019 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01019").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01020 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01020").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01021 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01021").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01022 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01022").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto fls01023 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "fls01023").with(SingleRunReqDto::setOpenDay, openDay).build();


        singleRunReqDtos.add(fls01001);
        singleRunReqDtos.add(fls01002);
        singleRunReqDtos.add(fls01003);
        singleRunReqDtos.add(fls01004);
        singleRunReqDtos.add(fls01005);
        singleRunReqDtos.add(fls01006);
        singleRunReqDtos.add(fls01007);
        singleRunReqDtos.add(fls01008);
        singleRunReqDtos.add(fls01009);
        singleRunReqDtos.add(fls01010);
        singleRunReqDtos.add(fls01011);
        singleRunReqDtos.add(fls01012);
        singleRunReqDtos.add(fls01013);
        singleRunReqDtos.add(fls01014);
        singleRunReqDtos.add(fls01015);
        singleRunReqDtos.add(fls01016);
        singleRunReqDtos.add(fls01017);
        singleRunReqDtos.add(fls01018);
        singleRunReqDtos.add(fls01019);
        singleRunReqDtos.add(fls01020);
        singleRunReqDtos.add(fls01021);
        singleRunReqDtos.add(fls01022);
        singleRunReqDtos.add(fls01023);


        parallelRunReqDto.setSingleRunReqDtos(singleRunReqDtos);
        logger.info("处理[文件处理任务-非零内评]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto));

        java.util.List<SingleRunRespDto> singleRunRespDtos = singleRunReqDtos.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务-非零内评]逻辑前，睡眠5秒开始");
                        TimeUnit.SECONDS.sleep(20);
                        logger.info("处理[文件处理任务-非零内评]逻辑前，睡眠5秒结束");
                        singleRunRespDto = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtos).build();
        logger.info("处理[文件处理任务-非零内评]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto));
        return parallelRunRespDto;
    }

    @PostMapping("/runCmis01")
    public @ResponseBody
    ParallelRunRespDto runCmis01() throws Exception {
        ParallelRunReqDto parallelRunReqDto = new ParallelRunReqDto();
        java.util.List<SingleRunReqDto> singleRunReqDtos = new ArrayList<>();
        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto cmis0101 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0101").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0102 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0102").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0103 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0103").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0104 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0104").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0105 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0105").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0106 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0106").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0107 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0107").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0108 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0108").with(SingleRunReqDto::setOpenDay, openDay).build();

        SingleRunReqDto cmis0110 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0110").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0111 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0111").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0112 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0112").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0113 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0113").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0114 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0114").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0115 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0115").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0116 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0116").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0117 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0117").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0118 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0118").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0119 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0119").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0122 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0122").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0123 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0123").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0124 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0124").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0125 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0125").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0126 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0126").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0127 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0127").with(SingleRunReqDto::setOpenDay, openDay).build();

        singleRunReqDtos.add(cmis0107);
        singleRunReqDtos.add(cmis0101);
        singleRunReqDtos.add(cmis0102);
        singleRunReqDtos.add(cmis0103);
        singleRunReqDtos.add(cmis0104);
        singleRunReqDtos.add(cmis0105);
        singleRunReqDtos.add(cmis0106);
        singleRunReqDtos.add(cmis0108);
        singleRunReqDtos.add(cmis0110);
        singleRunReqDtos.add(cmis0111);
        singleRunReqDtos.add(cmis0112);
        singleRunReqDtos.add(cmis0113);
        singleRunReqDtos.add(cmis0114);
//        singleRunReqDtos.add(cmis0115);
//        singleRunReqDtos.add(cmis0116);
//        singleRunReqDtos.add(cmis0117);
//        singleRunReqDtos.add(cmis0118);
//        singleRunReqDtos.add(cmis0119);
//        singleRunReqDtos.add(cmis0122);
//        singleRunReqDtos.add(cmis0123);


        parallelRunReqDto.setSingleRunReqDtos(singleRunReqDtos);
        logger.info("处理[加工任务-业务处理]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto));

        java.util.List<SingleRunRespDto> singleRunRespDtos = singleRunReqDtos.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[加工任务-业务处理]逻辑前，睡眠5秒开始");
                        TimeUnit.SECONDS.sleep(5);
                        logger.info("处理[加工任务-业务处理]逻辑前，睡眠5秒结束");
                        singleRunRespDto = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtos).build();
        logger.info("处理[加工任务-业务处理]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto));
        return parallelRunRespDto;
    }


    @PostMapping("/runCmisEd")
    public @ResponseBody
    ParallelRunRespDto runCmisEd() throws Exception {
        ParallelRunReqDto parallelRunReqDto = new ParallelRunReqDto();
        java.util.List<SingleRunReqDto> singleRunReqDtos = new ArrayList<>();
        String openDay = "2021-10-20";//任务日期，即营业日期

        SingleRunReqDto cmis0109 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0109").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0203 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0203").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis020C = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis020C").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis020D = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis020D").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis020E = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis020E").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis020F = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis020F").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis020G = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis020G").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis020H = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis020H").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0200 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0200").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis020B = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis020B").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0202 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0202").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0204 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0204").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0205 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0205").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0206 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0206").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0207 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0207").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0209 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0209").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis020A = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis020A").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0210 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0210").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0211 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0211").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0212 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0212").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0213 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0213").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0214 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0214").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0215 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0215").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0220 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0220").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0221 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0221").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0222 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0222").with(SingleRunReqDto::setOpenDay, openDay).build();


//        SingleRunReqDto cmis0201 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0201").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0208 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0208").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0223 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0223").with(SingleRunReqDto::setOpenDay, openDay).build();

        singleRunReqDtos.add(cmis0109);
        singleRunReqDtos.add(cmis0203);
        singleRunReqDtos.add(cmis020C);
        singleRunReqDtos.add(cmis020D);
        singleRunReqDtos.add(cmis020E);
        singleRunReqDtos.add(cmis020F);
        singleRunReqDtos.add(cmis020G);
        singleRunReqDtos.add(cmis020H);

        singleRunReqDtos.add(cmis0200);
        singleRunReqDtos.add(cmis020B);
        singleRunReqDtos.add(cmis0202);
        singleRunReqDtos.add(cmis0204);
        singleRunReqDtos.add(cmis0205);
        singleRunReqDtos.add(cmis0206);
        singleRunReqDtos.add(cmis0207);
        singleRunReqDtos.add(cmis0209);
        singleRunReqDtos.add(cmis020A);
        singleRunReqDtos.add(cmis0210);
        singleRunReqDtos.add(cmis0211);
        singleRunReqDtos.add(cmis0212);
        singleRunReqDtos.add(cmis0213);
        singleRunReqDtos.add(cmis0214);
        singleRunReqDtos.add(cmis0215);
        singleRunReqDtos.add(cmis0220);
        singleRunReqDtos.add(cmis0221);
        singleRunReqDtos.add(cmis0222);

//        singleRunReqDtos.add(cmis0201);
//        singleRunReqDtos.add(cmis0208);
//        singleRunReqDtos.add(cmis0223);
        parallelRunReqDto.setSingleRunReqDtos(singleRunReqDtos);
        logger.info("处理[加工任务-业务处理]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto));

        java.util.List<SingleRunRespDto> singleRunRespDtos = singleRunReqDtos.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[加工任务-业务处理]逻辑前，睡眠5秒开始");
                        TimeUnit.SECONDS.sleep(5);
                        logger.info("处理[加工任务-业务处理]逻辑前，睡眠5秒结束");
                        singleRunRespDto = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtos).build();
        logger.info("处理[加工任务-业务处理]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto));
        return parallelRunRespDto;
    }

    @PostMapping("/runCmis0120To21")
    public @ResponseBody
    ParallelRunRespDto runCmis0120To21() throws Exception {
        ParallelRunReqDto parallelRunReqDto = new ParallelRunReqDto();
        java.util.List<SingleRunReqDto> singleRunReqDtos = new ArrayList<>();
        String openDay = "2021-08-29";//任务日期，即营业日期
//        SingleRunReqDto cmis0120 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0120").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0121 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0121").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0128 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0128").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0129 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0129").with(SingleRunReqDto::setOpenDay, openDay).build();
//
//        singleRunReqDtos.add(cmis0120);
//        singleRunReqDtos.add(cmis0121);
//        singleRunReqDtos.add(cmis0128);
//        singleRunReqDtos.add(cmis0129);


        SingleRunReqDto cmis0602 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0602").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDtos.add(cmis0602);

        parallelRunReqDto.setSingleRunReqDtos(singleRunReqDtos);
        logger.info("处理[加工任务-业务处理]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto));

        java.util.List<SingleRunRespDto> singleRunRespDtos = singleRunReqDtos.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto = new SingleRunRespDto();
                    String url = "http://10.28.206.245:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[加工任务-业务处理]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[加工任务-业务处理]逻辑前，睡眠7秒结束");
                        singleRunRespDto = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtos).build();
        logger.info("处理[加工任务-业务处理]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto));
        return parallelRunRespDto;
    }

    @PostMapping("/runCmis04To05To06")
    public @ResponseBody
    ParallelRunRespDto runCmis04To05To06() throws Exception {
        ParallelRunReqDto parallelRunReqDto = new ParallelRunReqDto();
        java.util.List<SingleRunReqDto> singleRunReqDtos = new ArrayList<>();
        String openDay = "2022-01-01";//任务日期，即营业日期
//        SingleRunReqDto cmis0421 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0421").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0422 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0422").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0423 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0423").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0424 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0424").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0425 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0425").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0426 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0426").with(SingleRunReqDto::setOpenDay, openDay).build();

//        SingleRunReqDto cmis0400 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0400").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0401 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0401").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0402 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0402").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0403 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0403").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0404 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0404").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0405 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0405").with(SingleRunReqDto::setOpenDay, openDay).build();
//
//        SingleRunReqDto cmis0410 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0410").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0411 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0411").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0412 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0412").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0413 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0413").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0414 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0414").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0415 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0415").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0416 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0416").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis0417 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0417").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417A = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417A").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417B = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417B").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417C = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417C").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417D = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417D").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417E = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417E").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417F = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417F").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417G = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417G").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417H = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417H").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417I = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417I").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417J = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417J").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417K = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417K").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417L = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417L").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417M = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417M").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417N = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417N").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417O = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417O").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417P = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417P").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417Q = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417Q").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis417R = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis417R").with(SingleRunReqDto::setOpenDay, openDay).build();

        SingleRunReqDto cmis0418 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0418").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418A = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418A").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418B = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418B").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418C = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418C").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418D = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418D").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418E = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418E").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418F = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418F").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418G = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418G").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418H = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418H").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418I = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418I").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418J = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418J").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418K = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418K").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418L = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418L").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418M = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418M").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418N = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418N").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418O = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418O").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418P = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418P").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418Q = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418Q").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418R = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418R").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto cmis418S = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis418S").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0427 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0427").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0501 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0501").with(SingleRunReqDto::setOpenDay, openDay).build();
//        SingleRunReqDto cmis0601 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "cmis0601").with(SingleRunReqDto::setOpenDay, openDay).build();

//        singleRunReqDtos.add(cmis0421);
//        singleRunReqDtos.add(cmis0422);
//        singleRunReqDtos.add(cmis0423);
//        singleRunReqDtos.add(cmis0424);
//        singleRunReqDtos.add(cmis0425);
//        singleRunReqDtos.add(cmis0426);

//        singleRunReqDtos.add(cmis0400);
//        singleRunReqDtos.add(cmis0401);
//        singleRunReqDtos.add(cmis0402);
//        singleRunReqDtos.add(cmis0403);
//        singleRunReqDtos.add(cmis0404);
//        singleRunReqDtos.add(cmis0405);
//
//        singleRunReqDtos.add(cmis0410);
//        singleRunReqDtos.add(cmis0411);
//        singleRunReqDtos.add(cmis0412);
//        singleRunReqDtos.add(cmis0413);
//        singleRunReqDtos.add(cmis0414);
//        singleRunReqDtos.add(cmis0415);
//        singleRunReqDtos.add(cmis0416);
        singleRunReqDtos.add(cmis417A);
        singleRunReqDtos.add(cmis0417);
        singleRunReqDtos.add(cmis417B);
        singleRunReqDtos.add(cmis417C);
        singleRunReqDtos.add(cmis417D);
        singleRunReqDtos.add(cmis417E);
        singleRunReqDtos.add(cmis417F);
        singleRunReqDtos.add(cmis417G);
        singleRunReqDtos.add(cmis417H);
        singleRunReqDtos.add(cmis417I);
        singleRunReqDtos.add(cmis417J);
        singleRunReqDtos.add(cmis417K);
        singleRunReqDtos.add(cmis417L);
        singleRunReqDtos.add(cmis417M);
        singleRunReqDtos.add(cmis417N);
        singleRunReqDtos.add(cmis417O);
        singleRunReqDtos.add(cmis417P);
        singleRunReqDtos.add(cmis417Q);
        singleRunReqDtos.add(cmis417R);

        singleRunReqDtos.add(cmis0418);
        singleRunReqDtos.add(cmis418A);
        singleRunReqDtos.add(cmis418B);
        singleRunReqDtos.add(cmis418C);
        singleRunReqDtos.add(cmis418D);
        singleRunReqDtos.add(cmis418E);
        singleRunReqDtos.add(cmis418F);
        singleRunReqDtos.add(cmis418G);
        singleRunReqDtos.add(cmis418H);
        singleRunReqDtos.add(cmis418I);
        singleRunReqDtos.add(cmis418J);
        singleRunReqDtos.add(cmis418K);
        singleRunReqDtos.add(cmis418L);
        singleRunReqDtos.add(cmis418M);
        singleRunReqDtos.add(cmis418N);
        singleRunReqDtos.add(cmis418O);
        singleRunReqDtos.add(cmis418P);
        singleRunReqDtos.add(cmis418Q);
        singleRunReqDtos.add(cmis418R);
        singleRunReqDtos.add(cmis418S);
        // singleRunReqDtos.add(cmis0427);
//        singleRunReqDtos.add(cmis0501);
//        singleRunReqDtos.add(cmis0601);

        parallelRunReqDto.setSingleRunReqDtos(singleRunReqDtos);
        logger.info("处理[加工任务-贷后管理-系统管理-批量管理]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto));

        java.util.List<SingleRunRespDto> singleRunRespDtos = singleRunReqDtos.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[加工任务-贷后管理-系统管理-批量管理]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[加工任务-贷后管理-系统管理-批量管理]逻辑前，睡眠7秒结束");
                        singleRunRespDto = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtos).build();
        logger.info("处理[加工任务-贷后管理-系统管理-批量管理]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto));
        return parallelRunRespDto;
    }

    /**
     * 批后备份月表日表任务
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/runBakMD")
    public @ResponseBody
    ParallelRunRespDto runBakMD() throws Exception {
        ParallelRunReqDto parallelRunReqDto = new ParallelRunReqDto();
        java.util.List<SingleRunReqDto> singleRunReqDtos = new ArrayList<>();
        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto bakMD001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD002").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD003").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD004 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD004").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD005 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD005").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD006 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD006").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD007 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD007").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD008 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD008").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD009 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD009").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD010 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD010").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD011 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD011").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD012 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD012").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD013 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD013").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD014 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD014").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD015 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD015").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD016 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD016").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD017 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD017").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD018 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD018").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD019 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD019").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD020 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD020").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD021 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD021").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD022 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD022").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD023 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD023").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD024 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD024").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD025 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD025").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD026 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD026").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD027 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD027").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto bakMD028 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "bakMD028").with(SingleRunReqDto::setOpenDay, openDay).build();

        singleRunReqDtos.add(bakMD001);
        singleRunReqDtos.add(bakMD002);
        singleRunReqDtos.add(bakMD003);
        singleRunReqDtos.add(bakMD004);
        singleRunReqDtos.add(bakMD005);
        singleRunReqDtos.add(bakMD006);
        singleRunReqDtos.add(bakMD007);
        singleRunReqDtos.add(bakMD008);
        singleRunReqDtos.add(bakMD009);
        singleRunReqDtos.add(bakMD010);
        singleRunReqDtos.add(bakMD011);
        singleRunReqDtos.add(bakMD012);
        singleRunReqDtos.add(bakMD013);
        singleRunReqDtos.add(bakMD014);
        singleRunReqDtos.add(bakMD015);
        singleRunReqDtos.add(bakMD016);
        singleRunReqDtos.add(bakMD017);
        singleRunReqDtos.add(bakMD018);
        singleRunReqDtos.add(bakMD019);
        singleRunReqDtos.add(bakMD020);
        singleRunReqDtos.add(bakMD021);
        singleRunReqDtos.add(bakMD022);
        singleRunReqDtos.add(bakMD023);
        singleRunReqDtos.add(bakMD024);
        singleRunReqDtos.add(bakMD025);
        singleRunReqDtos.add(bakMD026);
        singleRunReqDtos.add(bakMD027);
        singleRunReqDtos.add(bakMD028);

        parallelRunReqDto.setSingleRunReqDtos(singleRunReqDtos);
        logger.info("处理[批后备份月表日表任务]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto));

        java.util.List<SingleRunRespDto> singleRunRespDtos = singleRunReqDtos.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto = new SingleRunRespDto();
                    String url = "http://10.85.10.211:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[批后备份月表日表任务]逻辑前，睡眠5秒开始");
                        TimeUnit.SECONDS.sleep(5);
                        logger.info("处理[批后备份月表日表任务]逻辑前，睡眠5秒结束");
                        singleRunRespDto = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtos).build();
        logger.info("处理[批后备份月表日表任务]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto));
        return parallelRunRespDto;
    }

    @PostMapping("/runRcp")
    public @ResponseBody
    ParallelRunRespDto runRcp() throws Exception {
        ParallelRunReqDto parallelRunReqDtoRcp = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDtoRcps = new ArrayList<>();

        String openDay = "2021-10-09";//任务日期，即营业日期
        SingleRunReqDto rcp00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00002").with(SingleRunReqDto::setOpenDay, openDay).build();
        parallelRunReqDtoRcp.setSingleRunReqDtos(singleRunReqDtoRcps);
        SingleRunReqDto rcp00003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00003").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00004 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00004").with(SingleRunReqDto::setOpenDay, openDay).build();
        parallelRunReqDtoRcp.setSingleRunReqDtos(singleRunReqDtoRcps);
        SingleRunReqDto rcp00005 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00005").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00006 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00006").with(SingleRunReqDto::setOpenDay, openDay).build();
        parallelRunReqDtoRcp.setSingleRunReqDtos(singleRunReqDtoRcps);
        SingleRunReqDto rcp00007 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00007").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00008 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00008").with(SingleRunReqDto::setOpenDay, openDay).build();
        parallelRunReqDtoRcp.setSingleRunReqDtos(singleRunReqDtoRcps);
        SingleRunReqDto rcp00009 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00009").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00010 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00010").with(SingleRunReqDto::setOpenDay, openDay).build();
        parallelRunReqDtoRcp.setSingleRunReqDtos(singleRunReqDtoRcps);
        SingleRunReqDto rcp00011 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00011").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00012 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00012").with(SingleRunReqDto::setOpenDay, openDay).build();
        parallelRunReqDtoRcp.setSingleRunReqDtos(singleRunReqDtoRcps);
        SingleRunReqDto rcp00013 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00013").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00014 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00014").with(SingleRunReqDto::setOpenDay, openDay).build();
        parallelRunReqDtoRcp.setSingleRunReqDtos(singleRunReqDtoRcps);
        SingleRunReqDto rcp00015 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00015").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00016 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00016").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00017 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00017").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00018 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00018").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00019 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00019").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00020 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00020").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00021 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00021").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00022 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00022").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00023 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00023").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00024 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00024").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto rcp00025 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "rcp00025").with(SingleRunReqDto::setOpenDay, openDay).build();

//        singleRunReqDtoRcps.add(rcp00001);
//        singleRunReqDtoRcps.add(rcp00002);
//        singleRunReqDtoRcps.add(rcp00003);
//        singleRunReqDtoRcps.add(rcp00004);
//        singleRunReqDtoRcps.add(rcp00005);
//        singleRunReqDtoRcps.add(rcp00006);
//        singleRunReqDtoRcps.add(rcp00007);
//        singleRunReqDtoRcps.add(rcp00008);
//        singleRunReqDtoRcps.add(rcp00009);
//        singleRunReqDtoRcps.add(rcp00010);
//        singleRunReqDtoRcps.add(rcp00011);
//        singleRunReqDtoRcps.add(rcp00012);
//        singleRunReqDtoRcps.add(rcp00013);
//        singleRunReqDtoRcps.add(rcp00014);
//        singleRunReqDtoRcps.add(rcp00015);
//        singleRunReqDtoRcps.add(rcp00016);
//        singleRunReqDtoRcps.add(rcp00017);
//        singleRunReqDtoRcps.add(rcp00018);
//        singleRunReqDtoRcps.add(rcp00019);
//        singleRunReqDtoRcps.add(rcp00020);
//        singleRunReqDtoRcps.add(rcp00021);
//        singleRunReqDtoRcps.add(rcp00022);
//        singleRunReqDtoRcps.add(rcp00023);


        singleRunReqDtoRcps.add(rcp00013);
        singleRunReqDtoRcps.add(rcp00008);
        singleRunReqDtoRcps.add(rcp00019);
        singleRunReqDtoRcps.add(rcp00006);
        singleRunReqDtoRcps.add(rcp00011);
        singleRunReqDtoRcps.add(rcp00001);
        singleRunReqDtoRcps.add(rcp00002);
        singleRunReqDtoRcps.add(rcp00003);
        singleRunReqDtoRcps.add(rcp00004);
        singleRunReqDtoRcps.add(rcp00005);
        singleRunReqDtoRcps.add(rcp00007);
        singleRunReqDtoRcps.add(rcp00009);
        singleRunReqDtoRcps.add(rcp00010);
        singleRunReqDtoRcps.add(rcp00012);
        singleRunReqDtoRcps.add(rcp00014);
        singleRunReqDtoRcps.add(rcp00015);
        singleRunReqDtoRcps.add(rcp00016);
        singleRunReqDtoRcps.add(rcp00017);
        singleRunReqDtoRcps.add(rcp00018);
        singleRunReqDtoRcps.add(rcp00020);
        singleRunReqDtoRcps.add(rcp00021);
        singleRunReqDtoRcps.add(rcp00022);
        singleRunReqDtoRcps.add(rcp00023);
        singleRunReqDtoRcps.add(rcp00024);
        singleRunReqDtoRcps.add(rcp00025);

        parallelRunReqDtoRcp.setSingleRunReqDtos(singleRunReqDtoRcps);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDtoRcp));
        java.util.List<SingleRunRespDto> singleRunRespDtoRcps = singleRunReqDtoRcps.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDtoRcp = new SingleRunRespDto();
                    String url = "http://10.85.10.159:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDtoRcp = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDtoRcp;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDtoRcp = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtoRcps).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDtoRcp));
        return parallelRunRespDtoRcp;
    }

    @PostMapping("/runC2d")
    public @ResponseBody
    ParallelRunRespDto runC2d() throws Exception {
        ParallelRunReqDto parallelRunReqDtoC2d = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDtoC2ds = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期
        SingleRunReqDto c2d00001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00001").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto c2d00002 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00002").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDtoC2ds.add(c2d00001);
        singleRunReqDtoC2ds.add(c2d00002);
        parallelRunReqDtoC2d.setSingleRunReqDtos(singleRunReqDtoC2ds);
        SingleRunReqDto c2d00003 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00003").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto c2d00004 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00004").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDtoC2ds.add(c2d00003);
        singleRunReqDtoC2ds.add(c2d00004);
        parallelRunReqDtoC2d.setSingleRunReqDtos(singleRunReqDtoC2ds);
        SingleRunReqDto c2d00005 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00005").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto c2d00006 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00006").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDtoC2ds.add(c2d00005);
        singleRunReqDtoC2ds.add(c2d00006);
        parallelRunReqDtoC2d.setSingleRunReqDtos(singleRunReqDtoC2ds);
        SingleRunReqDto c2d00007 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00007").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto c2d00008 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00008").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDtoC2ds.add(c2d00007);
        singleRunReqDtoC2ds.add(c2d00008);
        parallelRunReqDtoC2d.setSingleRunReqDtos(singleRunReqDtoC2ds);
        SingleRunReqDto c2d00009 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00009").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto c2d00010 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00010").with(SingleRunReqDto::setOpenDay, openDay).build();
        SingleRunReqDto c2d00011 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "c2d00011").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDtoC2ds.add(c2d00009);
        singleRunReqDtoC2ds.add(c2d00010);
        singleRunReqDtoC2ds.add(c2d00011);
        parallelRunReqDtoC2d.setSingleRunReqDtos(singleRunReqDtoC2ds);

        parallelRunReqDtoC2d.setSingleRunReqDtos(singleRunReqDtoC2ds);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDtoC2d));
        java.util.List<SingleRunRespDto> singleRunRespDtoC2ds = singleRunReqDtoC2ds.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDtoC2d = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDtoC2d = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDtoC2d;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDtoC2d = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtoC2ds).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDtoC2d));
        return parallelRunRespDtoC2d;
    }


    @PostMapping("/runGaps")
    public @ResponseBody
    ParallelRunRespDto runGaps() throws Exception {
        ParallelRunReqDto parallelRunReqDtoC2d = new ParallelRunReqDto();

        java.util.List<SingleRunReqDto> singleRunReqDtoGaps = new ArrayList<>();

        String openDay = "2021-09-30";//任务日期，即营业日期

        SingleRunReqDto gaps0001 = GenericBuilder.of(SingleRunReqDto::new).with(SingleRunReqDto::setTaskNo, "gaps0001").with(SingleRunReqDto::setOpenDay, openDay).build();
        singleRunReqDtoGaps.add(gaps0001);

        parallelRunReqDtoC2d.setSingleRunReqDtos(singleRunReqDtoGaps);

        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDtoC2d));
        java.util.List<SingleRunRespDto> singleRunRespDtoC2ds = singleRunReqDtoGaps.stream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDtoGaps = new SingleRunRespDto();
                    String url = "http://10.28.110.49:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒开始");
                        TimeUnit.SECONDS.sleep(7);
                        logger.info("处理[文件处理任务]逻辑前，睡眠7秒结束");
                        singleRunRespDtoGaps = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException | InterruptedException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                        Thread.currentThread().interrupt();
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDtoGaps;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDtoGaps = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtoC2ds).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDtoGaps));
        return parallelRunRespDtoGaps;
    }

}
