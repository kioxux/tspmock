/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatTCoreKlnlDkqgmx;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatTCoreKlnlDkqgmxMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatTCoreKlnlDkqgmxService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 23:46:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTCoreKlnlDkqgmxService {

    @Autowired
    private BatTCoreKlnlDkqgmxMapper batTCoreKlnlDkqgmxMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTCoreKlnlDkqgmx selectByPrimaryKey(String farendma, String dkjiejuh, String benqqish, Long benqizqs, Long mingxixh) {
        return batTCoreKlnlDkqgmxMapper.selectByPrimaryKey(farendma, dkjiejuh, benqqish, benqizqs, mingxixh);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTCoreKlnlDkqgmx> selectAll(QueryModel model) {
        List<BatTCoreKlnlDkqgmx> records = (List<BatTCoreKlnlDkqgmx>) batTCoreKlnlDkqgmxMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTCoreKlnlDkqgmx> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTCoreKlnlDkqgmx> list = batTCoreKlnlDkqgmxMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTCoreKlnlDkqgmx record) {
        return batTCoreKlnlDkqgmxMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTCoreKlnlDkqgmx record) {
        return batTCoreKlnlDkqgmxMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTCoreKlnlDkqgmx record) {
        return batTCoreKlnlDkqgmxMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTCoreKlnlDkqgmx record) {
        return batTCoreKlnlDkqgmxMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String farendma, String dkjiejuh, String benqqish, Long benqizqs, Long mingxixh) {
        return batTCoreKlnlDkqgmxMapper.deleteByPrimaryKey(farendma, dkjiejuh, benqqish, benqizqs, mingxixh);
    }

    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTCoreKlnlDkqgmxMapper.truncateTTable();
    }
}
