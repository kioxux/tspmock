package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0209Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepLmtEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0209</br>
 * 任务名称：加工任务-额度处理-同业交易对手额度占用</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0209Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0209Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0209Service cmis0209Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息


    @Bean
    public Job cmis0209Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_JOB.key, JobStepLmtEnum.CMIS0209_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0209Job = this.jobBuilderFactory.get(JobStepLmtEnum.CMIS0209_JOB.key)
                .start(cmis0209UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0209CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(cmis0209UpdateLmtContRelStep(WILL_BE_INJECTED))//更新额度占用关系
                .next(cmis0209UpdateLmtContRelYpzyStep(WILL_BE_INJECTED))//更新额度占用关系-银票质押
                .next(cmis0209UpdateLmtContRelZccypzyStep(WILL_BE_INJECTED))//更新额度占用关系-资产池银票质押入池
                .next(cmis0209UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0209Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0209UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_UPDATE_TASK010_STEP.key, JobStepLmtEnum.CMIS0209_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0209UpdateTask010Step = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0209_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0209_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_UPDATE_TASK010_STEP.key, JobStepLmtEnum.CMIS0209_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0209UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0209CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0209_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0209CheckRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0209_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0209_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0209_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0209_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0209CheckRelStep;
    }

    /**
     * 更新额度占用关系
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0209UpdateLmtContRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_STEP.key, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0209UpdateLmtContRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0209Service.cmis0209UpdateLmtContRel(openDay);//更新额度占用关系
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_STEP.key, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0209UpdateLmtContRelStep;
    }

    /**
     * 更新额度占用关系-银票质押
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0209UpdateLmtContRelYpzyStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_YPZY_STEP.key, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_YPZY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0209UpdateLmtContRelYpzyStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_YPZY_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0209Service.cmis0209UpdateLmtContRelYpzy(openDay);//更新额度占用关系-银票质押
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_YPZY_STEP.key, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_YPZY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0209UpdateLmtContRelYpzyStep;
    }

    /**
     * 更新额度占用关系-资产池银票质押入池
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0209UpdateLmtContRelZccypzyStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_ZCCYPZY_STEP.key, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_ZCCYPZY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0209UpdateLmtContRelZccypzyStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_ZCCYPZY_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0209Service.cmis0209UpdateLmtContRelZccypzy(openDay);//更新额度占用关系-资产池银票质押入池
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_ZCCYPZY_STEP.key, JobStepLmtEnum.CMIS0209_UPDATE_LMT_CONT_REL_ZCCYPZY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0209UpdateLmtContRelZccypzyStep;
    }


    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0209UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_UPDATE_TASK100_STEP.key, JobStepLmtEnum.CMIS0209_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0209UpdateTask100Step = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0209_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0209_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0209_UPDATE_TASK100_STEP.key, JobStepLmtEnum.CMIS0209_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0209UpdateTask100Step;
    }


}
