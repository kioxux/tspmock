package cn.com.yusys.yusp.batch.service.server.cmisbatch0009;

import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKlnbDkzhqg;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009Resp;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009Resp;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatSCoreKlnbDkzhqgMapper;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CmisBatch0009Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0009Service.class);
    @Autowired
    private BatSCoreKlnbDkzhqgMapper batSCoreKlnbDkzhqgMapper;// 核心系统-历史表-贷款账户期供表

    /**
     * 交易码：cmisbatch0009
     * 交易描述：查询[贷款账户期供表]信息
     *
     * @param cmisbatch0009ReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Cmisbatch0009RespDto cmisBatch0009(Cmisbatch0009ReqDto cmisbatch0009ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value);
        Cmisbatch0009RespDto cmisbatch0009RespDto = new Cmisbatch0009RespDto();// 响应Dto:调度运行管理信息查詢
        try {
            //请求字段
            String dkjiejuh = cmisbatch0009ReqDto.getDkjiejuh();//贷款借据号
            String benqqish = cmisbatch0009ReqDto.getBenqqish();//本期期数
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("dkjiejuh", dkjiejuh);//贷款借据号
            queryModel.addCondition("benqqish", benqqish);//本期期数
            // 查询
            List<BatSCoreKlnbDkzhqg>  batSCoreKlnbDkzhqgs = batSCoreKlnbDkzhqgMapper.selectByModel(queryModel);
            List<Cmisbatch0009Resp> cmisbatch0009List = new ArrayList<>();
            if (CollectionUtils.nonEmpty(batSCoreKlnbDkzhqgs)) {
                cmisbatch0009List = batSCoreKlnbDkzhqgs.stream().map(batSCoreKlnbDkzhqg -> {
                    Cmisbatch0009Resp cmisbatch0009Resp = new Cmisbatch0009Resp();
                    BeanUtils.copyProperties(batSCoreKlnbDkzhqg, cmisbatch0009Resp);
                    return cmisbatch0009Resp;
                }).collect(Collectors.toList());
            }
            cmisbatch0009RespDto.setCmisbatch0009List(cmisbatch0009List);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value);
        return cmisbatch0009RespDto;
    }
}
