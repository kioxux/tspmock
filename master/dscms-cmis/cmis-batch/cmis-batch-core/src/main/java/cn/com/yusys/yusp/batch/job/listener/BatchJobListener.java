package cn.com.yusys.yusp.batch.job.listener;


import cn.com.yusys.yusp.batch.config.BatchBeanFactory;
import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 公共job监听器
 */
@Component
public class BatchJobListener implements JobExecutionListener {
    private static final Logger logger = LoggerFactory.getLogger(BatchJobListener.class);


    @Override
    public void beforeJob(JobExecution jobExecution) {
        logger.info(jobExecution.getJobInstance().getJobName() + "开始");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        String jobName = jobExecution.getJobInstance().getJobName();// Job名称
        String exitCode = jobExecution.getExitStatus().getExitCode();//退出编码
        String exitDescription = jobExecution.getExitStatus().getExitDescription();//退出描述
        logger.info("Job名称为:[{}],退出编码为:[{}],退出描述为:[{}]的处理逻辑开始", jobName, exitCode, exitDescription);

        if (!jobName.equals(JobStepEnum.BATEND_JOB.key)) {
            logger.info(jobExecution.getJobInstance().getJobName() + "结束,更新任务运行管理中逻辑开始");
            BatControlRunService batControlRunService = BatchBeanFactory.getBean("batControlRunService");//调度运行管理
            BatTaskRunService batTaskRunService = BatchBeanFactory.getBean("batTaskRunService");// 任务运行管理

            QueryModel queryModel = new QueryModel();
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(queryModel));
            List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunList));

            String taskDate = batControlRunList.get(0).getLastOpenDay();// 任务日期
            String taskNo = StringUtils.EMPTY;// 任务编号
            // 针对batEndJob单独处理
            if (jobName.equals(JobStepEnum.BATEND_JOB.key)) {
                taskNo = TaskEnum.BATEND_TASK.key;
            } else {
                taskNo = jobName.substring(0, 8).toUpperCase();
            }
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, taskNo);
            BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(taskDate, taskNo);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
            if ("COMPLETED".equals(exitCode)) {
                logger.info("任务日期:【" + taskDate + "】,任务编号:【" + taskNo + "】,任务名称:【" + batTaskRun.getTaskName() + "】执行成功");
                batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                batTaskRun.setErrorCode(StringUtils.EMPTY);// 批量任务执行异常
                batTaskRun.setErrorInfo(StringUtils.EMPTY);
                batTaskRun.setTaskEndTime(DateUtils.getCurrDateTimeStr());// 任务结束时间
                batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
            } else {
                String errorInfo = "任务日期:【" + taskDate + "】,任务编号:【" + taskNo + "】,任务名称:【" + batTaskRun.getTaskName() + "】执行失败，异常信息为：【" + exitDescription + "】";
                batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_101.key);// 任务状态 STD_TASK_TYPE, 执行失败
                batTaskRun.setErrorCode(EchEnum.ECH089999.key);// 批量任务执行异常
                if (errorInfo.length() > 500) {
                    errorInfo = errorInfo.substring(0, 499);
                }
                batTaskRun.setErrorInfo(errorInfo);
                batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                int updateTaskStatus101 = batTaskRunService.updateSelective(batTaskRun);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus101));
            }
            logger.info(jobExecution.getJobInstance().getJobName() + "结束,更新任务运行管理中逻辑结束");
        }
        logger.info("Job名称为:[{}],退出编码为:[{}],退出描述为:[{}]的处理逻辑结束", jobName, exitCode, exitDescription);
    }

}
