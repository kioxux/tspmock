package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0125</br>
 * 任务名称：加工任务-业务处理-资金同业批复状态 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0125Mapper {

    /**
     * 更新同业机构准入台账表
     * @param openDay
     * @return
     */
    int updateIntbankOrgAdmitAcc(@Param("openDay") String openDay);

    /**
     * 更新同业机构准入批复表
     * @param openDay
     * @return
     */
    int updateIntbankOrgAdmitReply(@Param("openDay") String openDay);

    /**
     * 更新同业授信台账表
     * @param openDay
     * @return
     */
    int updateLmtIntbankAcc(@Param("openDay") String openDay);

    /**
     * 更新同业授信批复表
     * @param openDay
     * @return
     */
    int updateLmtIntbankReply(@Param("openDay") String openDay);

    /**
     * 更新单笔投资授信台账表
     * @param openDay
     * @return
     */
    int updateLmtSigInvestAcc(@Param("openDay") String openDay);

    /**
     * 更新底层授信额度台账表
     * @param openDay
     * @return
     */
    int updateLmtSigInvestBasicLmtAcc(@Param("openDay") String openDay);
}
