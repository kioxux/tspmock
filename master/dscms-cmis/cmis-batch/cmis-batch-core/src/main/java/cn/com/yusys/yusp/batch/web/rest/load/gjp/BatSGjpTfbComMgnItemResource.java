/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.gjp;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.gjp.BatSGjpTfbComMgnItem;
import cn.com.yusys.yusp.batch.service.load.gjp.BatSGjpTfbComMgnItemService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGjpTfbComMgnItemResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-11-15 16:53:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsgjptfbcommgnitem")
public class BatSGjpTfbComMgnItemResource {
    @Autowired
    private BatSGjpTfbComMgnItemService batSGjpTfbComMgnItemService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSGjpTfbComMgnItem>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSGjpTfbComMgnItem> list = batSGjpTfbComMgnItemService.selectAll(queryModel);
        return new ResultDto<List<BatSGjpTfbComMgnItem>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSGjpTfbComMgnItem>> index(QueryModel queryModel) {
        List<BatSGjpTfbComMgnItem> list = batSGjpTfbComMgnItemService.selectByModel(queryModel);
        return new ResultDto<List<BatSGjpTfbComMgnItem>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{id}")
    protected ResultDto<BatSGjpTfbComMgnItem> show(@PathVariable("id") String id) {
        BatSGjpTfbComMgnItem batSGjpTfbComMgnItem = batSGjpTfbComMgnItemService.selectByPrimaryKey(id);
        return new ResultDto<BatSGjpTfbComMgnItem>(batSGjpTfbComMgnItem);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSGjpTfbComMgnItem> create(@RequestBody BatSGjpTfbComMgnItem batSGjpTfbComMgnItem) throws URISyntaxException {
        batSGjpTfbComMgnItemService.insert(batSGjpTfbComMgnItem);
        return new ResultDto<BatSGjpTfbComMgnItem>(batSGjpTfbComMgnItem);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSGjpTfbComMgnItem batSGjpTfbComMgnItem) throws URISyntaxException {
        int result = batSGjpTfbComMgnItemService.update(batSGjpTfbComMgnItem);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{id}")
    protected ResultDto<Integer> delete(@PathVariable("id") String id) {
        int result = batSGjpTfbComMgnItemService.deleteByPrimaryKey(id);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSGjpTfbComMgnItemService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
