/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.repository.mapper.bat.BatControlRunMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: yusp-batch-core模块
 * @类名称: BatControlRunService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-09 17:12:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatControlRunService {
    private static final Logger logger = LoggerFactory.getLogger(BatControlRunService.class);
    @Autowired
    private BatControlRunMapper batControlRunMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatControlRun selectByPrimaryKey(String openDay) {
        return batControlRunMapper.selectByPrimaryKey(openDay);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatControlRun> selectAll(QueryModel model) {

        List<BatControlRun> records = (List<BatControlRun>) batControlRunMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatControlRun> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatControlRun> list = batControlRunMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatControlRun record) {
        record.setInputDate(DateUtils.getCurrDateStr()); // 登记日期
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setCreateTime(DateUtils.getCurrTimestamp());// 创建时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batControlRunMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatControlRun record) {
        record.setInputDate(DateUtils.getCurrDateStr()); // 登记日期
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setCreateTime(DateUtils.getCurrTimestamp());// 创建时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batControlRunMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatControlRun record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batControlRunMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatControlRun record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batControlRunMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String openDay) {
        return batControlRunMapper.deleteByPrimaryKey(openDay);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batControlRunMapper.deleteByIds(ids);
    }


}
