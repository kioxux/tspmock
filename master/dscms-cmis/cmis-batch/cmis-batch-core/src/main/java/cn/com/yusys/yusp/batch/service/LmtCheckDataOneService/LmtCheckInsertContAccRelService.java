/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.LmtCheckDataOneService;

import cn.com.yusys.yusp.batch.repository.mapper.check.LmtCheckInsertContAccRelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckInsertContAccRelService
 * @类描述: #服务类
 * @功能描述: 数据插入额度校正-台账临时表
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCheckInsertContAccRelService {
    private static final Logger logger = LoggerFactory.getLogger(LmtCheckInsertContAccRelService.class);

    @Autowired
    private LmtCheckInsertContAccRelMapper lmtCheckInsertContAccRelMapper;


    public void checkOutLmtCheckInsertContAccRel(String cusId){
        logger.info("1.LmtCheckInsertContAccRel数据插入额度校正-台账临时表------------start");

        /** 1.1 插入贷款台账 **/
        insertLoanOccRecord(cusId) ;

        /** 1.2、贷款出账申请 **/
        checkInsertDkczApply(cusId) ;

        /** 1.3、委托贷款 **/
        checkInsertWtdkRecord(cusId) ;

        /** 1.4、委托贷款出账申请 **/
        checkInsertWtdkczRecord(cusId) ;

        /** 1.5、银票台账 **/
        checkInsertYptzRecord(cusId) ;

        /** 1.6、银票台账出账申请 **/
        checkInsertYptzczRecord(cusId) ;

        /** 1.7、保函台账 **/
        checkInsertBhtzRecord(cusId) ;

        /** 1.8、信用证台账 **/
        checkInsertXyztzRecord(cusId) ;

        /** 1.9、贴现台账 **/
        checkInsertTxtzRecord(cusId) ;

        logger.info("1.LmtCheckInsertContAccRel数据插入额度校正-台账临时表------------start");
    }


    /**
     * 1.1 插入贷款台账
     * @param cusId
     * @return
     */
    public int insertLoanOccRecord(String cusId){
        logger.info("1.1 插入贷款台账------------start");
        return lmtCheckInsertContAccRelMapper.insertLoanOccRecord(cusId);
    }

    /**
     * 1.2、贷款出账申请
     * @param cusId
     * @return
     */
    public int checkInsertDkczApply(String cusId){
        logger.info("1.2、贷款出账申请------------start");
        return lmtCheckInsertContAccRelMapper.insertDkczApply(cusId);
    }

    /**
     * 1.3、委托贷款
     * @param cusId
     * @return
     */
    public int checkInsertWtdkRecord(String cusId){
        logger.info("1.3、委托贷款------------start");
        return lmtCheckInsertContAccRelMapper.insertWtdkRecord(cusId);
    }

    /**
     * 1.4、委托贷款出账申请
     * @param cusId
     * @return
     */
    public int checkInsertWtdkczRecord(String cusId){
        logger.info("1.4、委托贷款出账申请------------start");
        return lmtCheckInsertContAccRelMapper.insertWtdkczRecord(cusId);
    }

    /**
     * 1.5、银票台账
     * @param cusId
     * @return
     */
    public int checkInsertYptzRecord(String cusId){
        logger.info("1.5、银票台账------------start");
        return lmtCheckInsertContAccRelMapper.insertYptzRecord(cusId);
    }

    /**
     * 1.6、银票台账出账申请
     * @param cusId
     * @return
     */
    public int checkInsertYptzczRecord(String cusId){
        logger.info("1.6、银票台账出账申请------------start");
        return lmtCheckInsertContAccRelMapper.insertYptzczRecord(cusId);
    }

    /**
     * 1.7、保函台账
     * @param cusId
     * @return
     */
    public int checkInsertBhtzRecord(String cusId){
        logger.info("1.7、保函台账------------start");
        return lmtCheckInsertContAccRelMapper.insertBhtzRecord(cusId);
    }

    /**
     * 1.8、信用证台账
     * @param cusId
     * @return
     */
    public int checkInsertXyztzRecord(String cusId){
        logger.info("1.8、信用证台账------------start");
        return lmtCheckInsertContAccRelMapper.insertXyztzRecord(cusId);
    }

    /**
     * 1.9、贴现台账
     * @param cusId
     * @return
     */
    public int checkInsertTxtzRecord(String cusId){
        logger.info("1.9、贴现台账------------start");
        return lmtCheckInsertContAccRelMapper.insertTxtzRecord(cusId);
    }
}
