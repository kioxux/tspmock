package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-贷后管理-小微经营性定期检查 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0414Mapper {

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList01(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList02(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList03(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList04(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList05(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList06(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList07(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList08(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList09(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList10(@Param("openDay") String openDay);

    /**
     * 更新贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int updatePspTaskList(@Param("openDay") String openDay);
}
