package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0427</br>
 * 任务名称：加工任务-贷后管理-贷后催收任务  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
public interface Cmis0427Mapper {

    /**
     * 删除当天重复的数据
     */
    void deletePlaBcmTaskInfo01();

    /**
     * 分类为正常 关注  小微条线非不良催收任务生成
     *
     * @param openDay
     * @return
     */
    int insertPlaBcmTaskInfo01(@Param("openDay") String openDay);

    /**
     * 分类为正常 关注  非小微条线 非不良贷款 催收任务生成
     *
     * @param openDay
     * @return
     */
    int insertPlaBcmTaskInfo02(@Param("openDay") String openDay);

    /**
     * 不良贷款  小微条线 催收任务生成
     *
     * @param openDay
     * @return
     */
    int insertPlaBcmTaskInfo03(@Param("openDay") String openDay);

    /**
     * 分类为不良类   非小微条线 催收任务生成
     *
     * @param openDay
     * @return
     */
    int insertPlaBcmTaskInfo04(@Param("openDay") String openDay);

    /**
     * 删除当天借据重复的数据
     */
    void deletePlaBcmTaskInfo02();

    /**
     * 催收关联业务表 催收业务 借据  小微
     *
     * @param openDay
     * @return
     */
    int insertPlaBcmRel01(@Param("openDay") String openDay);

    /**
     * 催收业务 借据 非小微
     *
     * @param openDay
     * @return
     */
    int insertPlaBcmRel02(@Param("openDay") String openDay);
}
