package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0230</br>
 * 任务名称：加工任务-额度处理-全行贷款额度分配更新 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0230Mapper {

    /**
     * 1.分支机构额度管控备份至历史表
     *
     * @param openDay
     * @return
     */
    int insertManaOrgLmtHis(@Param("openDay") String openDay);

    /**
     * 2.业务条线额度管控备份至历史表
     *
     * @param openDay
     * @return
     */
    int insertManaBusiLmtHis(@Param("openDay") String openDay);

    /**
     * 全行贷款额度分配：日终月末最后一天日终，更新[分支机构额度管控表]当月可净新增对公贷款投放金额 初始化为10亿
     *
     * @param nextOpenDay
     * @return
     */
    int updateManaOrgLmt(@Param("nextOpenDay") String nextOpenDay);

    /**
     * 全行贷款额度分配:月末获取大数据平台推送机构上月末对公贷款余额 更新至 分支机构额度管控表
     *
     * @param nextOpenDay
     * @return
     */
    int updateMolBsofdm(@Param("nextOpenDay") String nextOpenDay);

    /**
     * 全行贷款额度分配:每日 获取的大数据平台推送机构上日对公贷款余额 更新至 分支机构额度管控表
     *
     * @param nextOpenDay
     * @return
     */
    int updateMolBsofdd(@Param("nextOpenDay") String nextOpenDay);

    /**
     * 全行贷款额度分配：月末获取大数据平台推送机构条线上月末贷款余额 更新至 业务条线额度管控表
     *
     * @param nextOpenDay
     * @return
     */
    int updateMblBsofydm(@Param("nextOpenDay") String nextOpenDay);

    /**
     * 全行贷款额度分配：每日 获取的大数据平台推送机构上日机构条线上月末贷款余额 更新至 业务条线额度管控表
     *
     * @param nextOpenDay
     * @return
     */
    int updateMblBsofydd(@Param("nextOpenDay") String nextOpenDay);

    /**
     * 全行贷款额度分配：日终月末最后一天日终，更新[业务条线额度管控表]当月可净新增对公贷款投放金额 初始化为10亿
     *
     * @param nextOpenDay
     * @return
     */
    int updateManaBusiLmt(@Param("nextOpenDay") String nextOpenDay);

    /**
     * 贴现限额：日终月末最后一天更新状态为失效
     *
     * @param paramMap
     * @return
     */
    int updateLmtDiscOrg(Map paramMap);
    /**
     * 大额风险暴露客户类型，两边码值不对应，进行转换
     *
     * @param openDay
     * @return
     */
    int updateDefxKhlx01(String openDay);
    /**
     * 大额风险暴露客户类型，两边码值不对应，进行转换
     *
     * @param openDay
     * @return
     */
    int updateDefxKhlx02(String openDay);
    /**
     * 大额风险暴露客户类型，两边码值不对应，进行转换
     *
     * @param openDay
     * @return
     */
    int updateDefxKhlx03(String openDay);
    /**
     * 大额风险暴露客户类型，两边码值不对应，进行转换
     *
     * @param openDay
     * @return
     */
    int updateDefxKhlx04(String openDay);
    /**
     * 大额风险暴露客户类型，两边码值不对应，进行转换
     *
     * @param openDay
     * @return
     */
    int updateDefxKhlxHz01(String openDay);
    /**
     * 大额风险暴露客户类型，两边码值不对应，进行转换
     *
     * @param openDay
     * @return
     */
    int updateDefxKhlxHz02(String openDay);
    /**
     * 大额风险暴露客户类型，两边码值不对应，进行转换
     *
     * @param openDay
     * @return
     */
    int updateDefxKhlxHz03(String openDay);
    /**
     * 大额风险暴露客户类型，两边码值不对应，进行转换
     *
     * @param openDay
     * @return
     */
    int updateDefxKhlxHz04(String openDay);

    /**
     * 大额风险暴露-客户维度风险暴露汇总：查询15个月以前的数据
     *
     * @param openDay
     * @return
     */
    int queryDmRisKhFxJgbxJk15M(@Param("openDay") String openDay);

    /**
     * 大额风险暴露-客户维度风险暴露汇总：清理15个月以前的数据
     *
     * @param openDay
     * @return
     */
    int deleteDmRisKhFxJgbxJk15M(@Param("openDay") String openDay);

    /**
     * 大额风险暴露-风险暴露指标配置历史表：查询15个月以前的数据
     *
     * @param openDay
     * @return
     */
    int queryDeRiskIndexCfgHis15M(@Param("openDay") String openDay);

    /**
     * 大额风险暴露-风险暴露指标配置历史表：清理15个月以前的数据
     *
     * @param openDay
     * @return
     */
    int deleteDeRiskIndexCfgHis15M(@Param("openDay") String openDay);

    /**
     * 大额风险暴露：大额风险暴露的配置表备份至历史表
     *
     * @param openDay
     * @return
     */
    int insertDeRiskIndexCfgHis(@Param("openDay") String openDay);


    /**
     * 清空额度库中集团客户与成员关系表
     *
     * @param openDay
     * @return
     */
    int deleteLmtCusGrpMemberRel(@Param("openDay") String openDay);

    /**
     * 插入额度库中集团客户与成员关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtCusGrpMemberRel(@Param("openDay") String openDay);


    /**
     * 清空额度库中集团客户信息表
     *
     * @param openDay
     * @return
     */
    int deleteLmtCusGrp(@Param("openDay") String openDay);


    /**
     * 插入额度库中集团客户信息表
     *
     * @param openDay
     * @return
     */
    int insertLmtCusGrp(@Param("openDay") String openDay);


    /**
     * 清空批复主信息及批复分项信息表A
     *
     * @param openDay
     * @return
     */
    int deleteTmpTdcarAlA(@Param("openDay") String openDay);


    /**
     * 插入批复主信息及批复分项信息表A
     *
     * @param openDay
     * @return
     */
    int insertTmpTdcarAlA(@Param("openDay") String openDay);

    /**
     * 将最新的集团客户编号同步至批复主信息及批复分项信息表中 A
     *
     * @param openDay
     * @return
     */
    int updateTmpTdcarAlA(@Param("openDay") String openDay);


    /**
     * 清空批复主信息及批复分项信息表B
     *
     * @param openDay
     * @return
     */
    int deleteTmpTdcarAlB(@Param("openDay") String openDay);


    /**
     * 插入批复主信息及批复分项信息表B
     *
     * @param openDay
     * @return
     */
    int insertTmpTdcarAlB(@Param("openDay") String openDay);

    /**
     * 将最新的集团客户编号同步至批复主信息及批复分项信息表中 B
     *
     * @param openDay
     * @return
     */
    int updateTmpTdcarAlB(@Param("openDay") String openDay);
}
