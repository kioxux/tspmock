package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0420</br>
 * 任务名称：加工任务-贷后管理-清理贷后临时表数据  </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月14日 下午7:56:32
 */
public interface Cmis0420Mapper {
    /**
     * 贷后管理-清理贷后临时表-风险分类任务表临时表数据
     * @param openDay
     * @return
     */
    int truncateTmpRiskTaskList(@Param("openDay") String openDay);

    /**
     * 贷后管理-清理贷后临时表-风险分类借据信息临时表数据
     * @param openDay
     * @return
     */
    int truncateTmpRiskDebitInfo(@Param("openDay") String openDay);

    /**
     * 贷后管理-清理贷后临时表-清理担保合同分析临时表数据
     * @param openDay
     * @return
     */
    int truncateTmpRiskGuarContAnaly(@Param("openDay") String openDay);

    /**
     * 贷后管理-清理贷后临时表-清理风险分类保证人检查临时表数据
     * @param openDay
     * @return
     */
    int truncateTmpRiskGuarntrList(@Param("openDay") String openDay);

    /**
     * 贷后管理-清理贷后临时表-清理风险分类抵质押物检查临时表数据
     * @param openDay
     * @return
     */
    int truncateTmpRiskPldimnList(@Param("openDay") String openDay);

    /**
     * 贷后管理-清理贷后临时表-清理影响偿还因素分析临时表数据
     * @param openDay
     * @return
     */
    int truncateTmpRiskRepayAnaly(@Param("openDay") String openDay);
}
