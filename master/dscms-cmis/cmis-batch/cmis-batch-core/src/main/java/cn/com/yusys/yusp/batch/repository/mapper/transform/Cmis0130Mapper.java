package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0130</br>
 * 任务名称：加工任务-业务处理-同行业排行处理 </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月10日 下午01:04:44
 */
public interface Cmis0130Mapper {


    /**
     * 插入临时表-同行业营业收入排行客户号
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtReplyAcc(@Param("openDay") String openDay);


    /**
     * 插入单一授信同行业营业收入排行表
     *
     * @param openDay
     * @return
     */
    int insertInduBsinsIncoTop(@Param("openDay") String openDay);

    /**
     * 插入集团授信同行业营业收入排行表
     *
     * @param openDay
     * @return
     */
    int insertInduBsinsIncoTopGrp(@Param("openDay") String openDay);


    /**
     * 插入临时表同行业营业收入排行表
     *
     * @param openDay
     * @return
     */
    int insertTmpInduBsinsIncoTop(@Param("openDay") String openDay);


    /**
     * 插入新信贷-临时表-同行业营业收入排行的总资产年期末数
     *
     * @param openDay
     * @return
     */
    int insertTmpIbitStatEndAmtY1(@Param("openDay") String openDay);


    /**
     * 插入新信贷-临时表-同行业营业收入排行的净资产年期末数
     *
     * @param openDay
     * @return
     */
    int insertTmpIbitStatEndAmtY2(@Param("openDay") String openDay);


    /**
     * 插入新信贷-临时表-同行业营业收入排行的营业收入年期末数
     *
     * @param openDay
     * @return
     */
    int insertTmpIbitStatEndAmtY3(@Param("openDay") String openDay);


    /**
     * 插入新信贷-临时表-同行业营业收入排行的净利润年期末数
     *
     * @param openDay
     * @return
     */
    int insertTmpIbitStatEndAmtY4(@Param("openDay") String openDay);


    /**
     * 插入新信贷-临时表-同行业营业收入排行的资产负债率年期末数
     *
     * @param openDay
     * @return
     */
    int insertTmpIbitStatEndAmtY5(@Param("openDay") String openDay);


    /**
     * 插入新信贷-临时表-同行业营业收入排行的营业融资总额年期末数
     *
     * @param openDay
     * @return
     */
    int insertTmpIbitStatEndAmtY6(@Param("openDay") String openDay);

    /**
     * 更新同行业营业收入排行表总资产字段
     *
     * @param openDay
     * @return
     */
    int updateTotalRessetAmt(@Param("openDay") String openDay);

    /**
     * 更新同行业营业收入排行表净资产字段
     *
     * @param openDay
     * @return
     */
    int updateTotalPureAmt(@Param("openDay") String openDay);

    /**
     * 更新同行业营业收入排行表营业收入字段
     *
     * @param openDay
     * @return
     */
    int updateBsinsIncome(@Param("openDay") String openDay);

    /**
     * 更新同行业营业收入排行表净利润字段
     *
     * @param openDay
     * @return
     */
    int updateProfit(@Param("openDay") String openDay);

    /**
     * 更新同行业营业收入排行表资产负债率字段
     *
     * @param openDay
     * @return
     */
    int updateAssetDebtRate(@Param("openDay") String openDay);

    /**
     * 更新同行业营业收入排行表营业融资总额字段
     *
     * @param openDay
     * @return
     */
    int updateTotalOperFinance(@Param("openDay") String openDay);

    /**
     * 更新同行业营业收入排行表销贷比字段
     *
     * @param openDay
     * @return
     */
    int updateLoanSoldRate(@Param("openDay") String openDay);
}
