/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.check;

import org.apache.ibatis.annotations.Param;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: WbMsgNoticeMapper
 * @类描述: #Dao类
 * @功能描述: 数据插入额度校正-台账临时表
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCheckInsertContAccRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertLoanOccRecord(@Param("cusId") String cusId);


    /**
     * 1.2、贷款出账申请
     */
    int insertDkczApply(@Param("cusId") String cusId);


    /**
     * 1.3、委托贷款
     */
    int insertWtdkRecord(@Param("cusId") String cusId);

    /**
     * 1.4、委托贷款出账申请
     */
    int insertWtdkczRecord(@Param("cusId") String cusId);

    /**
     * 1.5、银票台账
     */
    int insertYptzRecord(@Param("cusId") String cusId);

    /**
     * 1.6、银票台账出账申请
     */
    int insertYptzczRecord(@Param("cusId") String cusId);

    /**
     * 1.7、保函台账
     */
    int insertBhtzRecord(@Param("cusId") String cusId);

    /**
     * 1.8、信用证台账
     * @param cusId
     * @return
     */
    int insertXyztzRecord(@Param("cusId") String cusId);

    /**
     * 1.9、贴现台账
     */
    int insertTxtzRecord(@Param("cusId") String cusId);
}