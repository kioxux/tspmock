/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.ods;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.ods.BatSOdsFtoDgdkyeM;
import cn.com.yusys.yusp.batch.service.load.ods.BatSOdsFtoDgdkyeMService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSOdsFtoDgdkyeMResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-25 16:35:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsodsftodgdkyem")
public class BatSOdsFtoDgdkyeMResource {
    @Autowired
    private BatSOdsFtoDgdkyeMService batSOdsFtoDgdkyeMService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSOdsFtoDgdkyeM>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSOdsFtoDgdkyeM> list = batSOdsFtoDgdkyeMService.selectAll(queryModel);
        return new ResultDto<List<BatSOdsFtoDgdkyeM>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSOdsFtoDgdkyeM>> index(QueryModel queryModel) {
        List<BatSOdsFtoDgdkyeM> list = batSOdsFtoDgdkyeMService.selectByModel(queryModel);
        return new ResultDto<List<BatSOdsFtoDgdkyeM>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSOdsFtoDgdkyeM> create(@RequestBody BatSOdsFtoDgdkyeM batSOdsFtoDgdkyeM) throws URISyntaxException {
        batSOdsFtoDgdkyeMService.insert(batSOdsFtoDgdkyeM);
        return new ResultDto<BatSOdsFtoDgdkyeM>(batSOdsFtoDgdkyeM);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSOdsFtoDgdkyeM batSOdsFtoDgdkyeM) throws URISyntaxException {
        int result = batSOdsFtoDgdkyeMService.update(batSOdsFtoDgdkyeM);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String acctdt, String brchno, java.math.BigDecimal loanbl) {
        int result = batSOdsFtoDgdkyeMService.deleteByPrimaryKey(acctdt, brchno, loanbl);
        return new ResultDto<Integer>(result);
    }

}
