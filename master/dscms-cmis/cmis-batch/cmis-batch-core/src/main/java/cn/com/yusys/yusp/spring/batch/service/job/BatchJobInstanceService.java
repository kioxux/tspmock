/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.service.job;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.spring.batch.domain.job.BatchJobInstance;
import cn.com.yusys.yusp.spring.batch.repository.mapper.job.BatchJobInstanceMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobInstanceService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatchJobInstanceService {

    @Autowired
    private BatchJobInstanceMapper batchJobInstanceMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatchJobInstance selectByPrimaryKey(Long jobInstanceId) {
        return batchJobInstanceMapper.selectByPrimaryKey(jobInstanceId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatchJobInstance> selectAll(QueryModel model) {
        List<BatchJobInstance> records = (List<BatchJobInstance>) batchJobInstanceMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatchJobInstance> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatchJobInstance> list = batchJobInstanceMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatchJobInstance record) {
        return batchJobInstanceMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatchJobInstance record) {
        return batchJobInstanceMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatchJobInstance record) {
        return batchJobInstanceMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatchJobInstance record) {
        return batchJobInstanceMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(Long jobInstanceId) {
        return batchJobInstanceMapper.deleteByPrimaryKey(jobInstanceId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batchJobInstanceMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:getbatchinfo
     * @函数描述:查询作业信息
     * @参数与返回说明:
     * @算法描述:
     */
    public List<Map<String, Object>> getBatchInfo(QueryModel queryModel) {
        PageMethod.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> batchInfos = batchJobInstanceMapper.getBatchInfo(queryModel);
        PageHelper.clearPage();
        return batchInfos;
    }
}
