/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.ypp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSYppTInfBuilProject
 * @类描述: bat_s_ypp_t_inf_buil_project数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 17:10:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ypp_t_inf_buil_project")
public class BatSYppTInfBuilProject extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 押品编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "GUAR_NO")
	private String guarNo;
	
	/** 土地证（不动产权证号） **/
	@Column(name = "LAND_NO", unique = false, nullable = true, length = 450)
	private String landNo;
	
	/** 土地使用权性质 **/
	@Column(name = "LAND_USE_QUAL", unique = false, nullable = true, length = 6)
	private String landUseQual;
	
	/** 土地使用权取得方式 **/
	@Column(name = "LAND_USE_WAY", unique = false, nullable = true, length = 6)
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	@Column(name = "LAND_USE_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	@Column(name = "LAND_USE_END_DATE", unique = false, nullable = true, length = 10)
	private String landUseEndDate;
	
	/** 土地使用年限 **/
	@Column(name = "LAND_USE_YEARS", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal landUseYears;
	
	/** 土地用途 **/
	@Column(name = "LAND_PURP", unique = false, nullable = true, length = 2)
	private String landPurp;
	
	/** 土地说明 **/
	@Column(name = "LAND_EXPLAIN", unique = false, nullable = true, length = 400)
	private String landExplain;
	
	/** 工程项目名称 **/
	@Column(name = "PROJECT_NAME", unique = false, nullable = true, length = 450)
	private String projectName;
	
	/** 建设用地规划许可证号 **/
	@Column(name = "LAND_LICENCE", unique = false, nullable = true, length = 100)
	private String landLicence;
	
	/** 建设工程规划许可证号 **/
	@Column(name = "LAYOUT_LICENCE", unique = false, nullable = true, length = 100)
	private String layoutLicence;
	
	/** 施工许可证号 **/
	@Column(name = "CONS_LICENCE", unique = false, nullable = true, length = 100)
	private String consLicence;
	
	/** 工程启动日期 **/
	@Column(name = "PROJ_START_DATE", unique = false, nullable = true, length = 10)
	private String projStartDate;
	
	/** 工程竣工预计交付日期 **/
	@Column(name = "PLAN_FINISH_DATE", unique = false, nullable = true, length = 10)
	private String planFinishDate;
	
	/** 工程预计总造价 **/
	@Column(name = "PROJ_PLAN_ACCNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal projPlanAccnt;
	
	/** 工程契约合同编号 **/
	@Column(name = "BUSINESS_CONTRACT_NO", unique = false, nullable = true, length = 100)
	private String businessContractNo;
	
	/** 合同签订日期 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 合同签订金额 **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 建筑面积 **/
	@Column(name = "BUILD_AREA", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal buildArea;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 详细地址 **/
	@Column(name = "DETAILS_ADDRESS", unique = false, nullable = true, length = 300)
	private String detailsAddress;
	
	/** 工程实际启动日期 **/
	@Column(name = "PRO_PRS_DATE", unique = false, nullable = true, length = 10)
	private String proPrsDate;
	
	/** 工程竣工交付日期 **/
	@Column(name = "PREDICT_FINISH_DATE", unique = false, nullable = true, length = 10)
	private String predictFinishDate;
	
	/** 所得税率 **/
	@Column(name = "GET_TAX", unique = false, nullable = true, length = 6)
	private java.math.BigDecimal getTax;
	
	/** 在建工程类型 **/
	@Column(name = "BUILT_PROJECT_TYPE", unique = false, nullable = true, length = 10)
	private String builtProjectType;
	
	/** 备注(限1000个汉字) **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 1000)
	private String remark;
	
	/** 建设商（房产商） **/
	@Column(name = "BUILER", unique = false, nullable = true, length = 100)
	private String builer;
	
	/** 是否欠工程款 **/
	@Column(name = "IF_ARREARAGE", unique = false, nullable = true, length = 10)
	private String ifArrearage;
	
	/** 欠工程款金额 **/
	@Column(name = "ARREARAGE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal arrearageAmt;
	
	/** 四证是否齐全 **/
	@Column(name = "LAND_FOUR", unique = false, nullable = true, length = 10)
	private String landFour;
	
	/** 街道/村镇/路名 **/
	@Column(name = "STREET", unique = false, nullable = true, length = 100)
	private String street;
	
	/** 土地面积 **/
	@Column(name = "LAND_AREA", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal landArea;
	
	/** 土地剩余使用年限 **/
	@Column(name = "LAND_NO_USE_YEARS", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal landNoUseYears;
	
	/** 土地用途（慧押押） **/
	@Column(name = "LAND_USAGE", unique = false, nullable = true, length = 9)
	private String landUsage;
	
	/** 实时查封状态 **/
	@Column(name = "SS_CF_STATUS", unique = false, nullable = true, length = 5)
	private String ssCfStatus;
	
	/** 查封时间 **/
	@Column(name = "CF_DATE", unique = false, nullable = true, length = 10)
	private String cfDate;
	
	/** 最高可抵押顺位 **/
	@Column(name = "MAX_YP_STATUS", unique = false, nullable = true, length = 5)
	private String maxYpStatus;
	
	/** 房屋用途（在线抵押使用） **/
	@Column(name = "HOUSE_USAGE", unique = false, nullable = true, length = 9)
	private String houseUsage;
	
	/** 不动产单元号 **/
	@Column(name = "BDCDYH_NO", unique = false, nullable = true, length = 100)
	private String bdcdyhNo;
	
	/** 状态 **/
	@Column(name = "INSPECT_STATUS", unique = false, nullable = true, length = 2)
	private String inspectStatus;
	
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo;
	}
	
    /**
     * @return landNo
     */
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual;
	}
	
    /**
     * @return landUseQual
     */
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay;
	}
	
    /**
     * @return landUseWay
     */
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate;
	}
	
    /**
     * @return landUseBeginDate
     */
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate;
	}
	
    /**
     * @return landUseEndDate
     */
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(java.math.BigDecimal landUseYears) {
		this.landUseYears = landUseYears;
	}
	
    /**
     * @return landUseYears
     */
	public java.math.BigDecimal getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp;
	}
	
    /**
     * @return landPurp
     */
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain;
	}
	
    /**
     * @return landExplain
     */
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param projectName
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
    /**
     * @return projectName
     */
	public String getProjectName() {
		return this.projectName;
	}
	
	/**
	 * @param landLicence
	 */
	public void setLandLicence(String landLicence) {
		this.landLicence = landLicence;
	}
	
    /**
     * @return landLicence
     */
	public String getLandLicence() {
		return this.landLicence;
	}
	
	/**
	 * @param layoutLicence
	 */
	public void setLayoutLicence(String layoutLicence) {
		this.layoutLicence = layoutLicence;
	}
	
    /**
     * @return layoutLicence
     */
	public String getLayoutLicence() {
		return this.layoutLicence;
	}
	
	/**
	 * @param consLicence
	 */
	public void setConsLicence(String consLicence) {
		this.consLicence = consLicence;
	}
	
    /**
     * @return consLicence
     */
	public String getConsLicence() {
		return this.consLicence;
	}
	
	/**
	 * @param projStartDate
	 */
	public void setProjStartDate(String projStartDate) {
		this.projStartDate = projStartDate;
	}
	
    /**
     * @return projStartDate
     */
	public String getProjStartDate() {
		return this.projStartDate;
	}
	
	/**
	 * @param planFinishDate
	 */
	public void setPlanFinishDate(String planFinishDate) {
		this.planFinishDate = planFinishDate;
	}
	
    /**
     * @return planFinishDate
     */
	public String getPlanFinishDate() {
		return this.planFinishDate;
	}
	
	/**
	 * @param projPlanAccnt
	 */
	public void setProjPlanAccnt(java.math.BigDecimal projPlanAccnt) {
		this.projPlanAccnt = projPlanAccnt;
	}
	
    /**
     * @return projPlanAccnt
     */
	public java.math.BigDecimal getProjPlanAccnt() {
		return this.projPlanAccnt;
	}
	
	/**
	 * @param businessContractNo
	 */
	public void setBusinessContractNo(String businessContractNo) {
		this.businessContractNo = businessContractNo;
	}
	
    /**
     * @return businessContractNo
     */
	public String getBusinessContractNo() {
		return this.businessContractNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(java.math.BigDecimal buildArea) {
		this.buildArea = buildArea;
	}
	
    /**
     * @return buildArea
     */
	public java.math.BigDecimal getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param detailsAddress
	 */
	public void setDetailsAddress(String detailsAddress) {
		this.detailsAddress = detailsAddress;
	}
	
    /**
     * @return detailsAddress
     */
	public String getDetailsAddress() {
		return this.detailsAddress;
	}
	
	/**
	 * @param proPrsDate
	 */
	public void setProPrsDate(String proPrsDate) {
		this.proPrsDate = proPrsDate;
	}
	
    /**
     * @return proPrsDate
     */
	public String getProPrsDate() {
		return this.proPrsDate;
	}
	
	/**
	 * @param predictFinishDate
	 */
	public void setPredictFinishDate(String predictFinishDate) {
		this.predictFinishDate = predictFinishDate;
	}
	
    /**
     * @return predictFinishDate
     */
	public String getPredictFinishDate() {
		return this.predictFinishDate;
	}
	
	/**
	 * @param getTax
	 */
	public void setGetTax(java.math.BigDecimal getTax) {
		this.getTax = getTax;
	}
	
    /**
     * @return getTax
     */
	public java.math.BigDecimal getGetTax() {
		return this.getTax;
	}
	
	/**
	 * @param builtProjectType
	 */
	public void setBuiltProjectType(String builtProjectType) {
		this.builtProjectType = builtProjectType;
	}
	
    /**
     * @return builtProjectType
     */
	public String getBuiltProjectType() {
		return this.builtProjectType;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param builer
	 */
	public void setBuiler(String builer) {
		this.builer = builer;
	}
	
    /**
     * @return builer
     */
	public String getBuiler() {
		return this.builer;
	}
	
	/**
	 * @param ifArrearage
	 */
	public void setIfArrearage(String ifArrearage) {
		this.ifArrearage = ifArrearage;
	}
	
    /**
     * @return ifArrearage
     */
	public String getIfArrearage() {
		return this.ifArrearage;
	}
	
	/**
	 * @param arrearageAmt
	 */
	public void setArrearageAmt(java.math.BigDecimal arrearageAmt) {
		this.arrearageAmt = arrearageAmt;
	}
	
    /**
     * @return arrearageAmt
     */
	public java.math.BigDecimal getArrearageAmt() {
		return this.arrearageAmt;
	}
	
	/**
	 * @param landFour
	 */
	public void setLandFour(String landFour) {
		this.landFour = landFour;
	}
	
    /**
     * @return landFour
     */
	public String getLandFour() {
		return this.landFour;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param landArea
	 */
	public void setLandArea(java.math.BigDecimal landArea) {
		this.landArea = landArea;
	}
	
    /**
     * @return landArea
     */
	public java.math.BigDecimal getLandArea() {
		return this.landArea;
	}
	
	/**
	 * @param landNoUseYears
	 */
	public void setLandNoUseYears(java.math.BigDecimal landNoUseYears) {
		this.landNoUseYears = landNoUseYears;
	}
	
    /**
     * @return landNoUseYears
     */
	public java.math.BigDecimal getLandNoUseYears() {
		return this.landNoUseYears;
	}
	
	/**
	 * @param landUsage
	 */
	public void setLandUsage(String landUsage) {
		this.landUsage = landUsage;
	}
	
    /**
     * @return landUsage
     */
	public String getLandUsage() {
		return this.landUsage;
	}
	
	/**
	 * @param ssCfStatus
	 */
	public void setSsCfStatus(String ssCfStatus) {
		this.ssCfStatus = ssCfStatus;
	}
	
    /**
     * @return ssCfStatus
     */
	public String getSsCfStatus() {
		return this.ssCfStatus;
	}
	
	/**
	 * @param cfDate
	 */
	public void setCfDate(String cfDate) {
		this.cfDate = cfDate;
	}
	
    /**
     * @return cfDate
     */
	public String getCfDate() {
		return this.cfDate;
	}
	
	/**
	 * @param maxYpStatus
	 */
	public void setMaxYpStatus(String maxYpStatus) {
		this.maxYpStatus = maxYpStatus;
	}
	
    /**
     * @return maxYpStatus
     */
	public String getMaxYpStatus() {
		return this.maxYpStatus;
	}
	
	/**
	 * @param houseUsage
	 */
	public void setHouseUsage(String houseUsage) {
		this.houseUsage = houseUsage;
	}
	
    /**
     * @return houseUsage
     */
	public String getHouseUsage() {
		return this.houseUsage;
	}
	
	/**
	 * @param bdcdyhNo
	 */
	public void setBdcdyhNo(String bdcdyhNo) {
		this.bdcdyhNo = bdcdyhNo;
	}
	
    /**
     * @return bdcdyhNo
     */
	public String getBdcdyhNo() {
		return this.bdcdyhNo;
	}
	
	/**
	 * @param inspectStatus
	 */
	public void setInspectStatus(String inspectStatus) {
		this.inspectStatus = inspectStatus;
	}
	
    /**
     * @return inspectStatus
     */
	public String getInspectStatus() {
		return this.inspectStatus;
	}


}