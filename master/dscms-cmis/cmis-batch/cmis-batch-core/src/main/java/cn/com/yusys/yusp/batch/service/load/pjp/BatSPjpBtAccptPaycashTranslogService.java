/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.pjp;

import cn.com.yusys.yusp.batch.domain.load.pjp.BatSPjpBtAccptPaycashTranslog;
import cn.com.yusys.yusp.batch.repository.mapper.load.pjp.BatSPjpBtAccptPaycashTranslogMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtAccptPaycashTranslogService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-03 10:58:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSPjpBtAccptPaycashTranslogService {
    private static final Logger logger = LoggerFactory.getLogger(BatSPjpBtAccptPaycashTranslogService.class);
    @Autowired
    private BatSPjpBtAccptPaycashTranslogMapper batSPjpBtAccptPaycashTranslogMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSPjpBtAccptPaycashTranslog selectByPrimaryKey(String id) {
        return batSPjpBtAccptPaycashTranslogMapper.selectByPrimaryKey(id);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSPjpBtAccptPaycashTranslog> selectAll(QueryModel model) {
        List<BatSPjpBtAccptPaycashTranslog> records = (List<BatSPjpBtAccptPaycashTranslog>) batSPjpBtAccptPaycashTranslogMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSPjpBtAccptPaycashTranslog> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSPjpBtAccptPaycashTranslog> list = batSPjpBtAccptPaycashTranslogMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSPjpBtAccptPaycashTranslog record) {
        return batSPjpBtAccptPaycashTranslogMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSPjpBtAccptPaycashTranslog record) {
        return batSPjpBtAccptPaycashTranslogMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSPjpBtAccptPaycashTranslog record) {
        return batSPjpBtAccptPaycashTranslogMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSPjpBtAccptPaycashTranslog record) {
        return batSPjpBtAccptPaycashTranslogMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String id) {
        return batSPjpBtAccptPaycashTranslogMapper.deleteByPrimaryKey(id);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSPjpBtAccptPaycashTranslogMapper.deleteByIds(ids);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建保证金记录表[bat_s_pjp_bt_accpt_paycash_translog]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_pjp_bt_accpt_paycash_translog");
        logger.info("重建【PJP00004】保证金记录表[bat_s_pjp_bt_accpt_paycash_translog]结束");
        return 0;
    }


    /**
     * 将T表数据merge到S表
     *
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        // logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]开始,请求参数为:[{}]", openDay);
        // int updateSResult = batSPjpBtAccptPaycashTranslogMapper.updateDataDateByOpenDay(openDay);
        // logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]结束,返回参数为:[{}]", updateSResult);
        logger.info("更新S表中[字典项转换:转出账号币种]开始,请求参数为:[{}]", openDay);
        int updateOutAcctCurrency = batSPjpBtAccptPaycashTranslogMapper.updateOutAcctCurrency(openDay);
        logger.info("更新S表中[字典项转换:转出账号币种]]结束,返回参数为:[{}]", updateOutAcctCurrency);
    }
}
