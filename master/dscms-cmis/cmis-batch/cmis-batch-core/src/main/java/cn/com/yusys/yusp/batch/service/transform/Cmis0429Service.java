package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0428Mapper;
import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0429Mapper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0429</br>
 * 任务名称：加工任务-贷后管理-生成自动化贷后个人名单  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月18日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0429Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0429Service.class);

    @Autowired
    private Cmis0429Mapper cmis0429Mapper;

    /**
     * 插入个人自动化贷后跑批白名单
     *
     * @param openDay
     */
    public void insertAutoPspWhiteInfoPer(String openDay) {


        logger.info("27号 插入个人预跑批白名单月表 开始,请求参数为:[{}]", openDay);
        int insertAutoPspWhiteInfoPer01Month = cmis0429Mapper.insertAutoPspWhiteInfoPer01Month(openDay);
        logger.info("27号 插入个人预跑批白名单月表 结束,响应参数为:[{}]", insertAutoPspWhiteInfoPer01Month);


        logger.info("27号删除个人预跑批白名单开始,请求参数为:[{}]", openDay);
        int deleteAutoPspWhiteInfoPer = cmis0429Mapper.deleteAutoPspWhiteInfoPer(openDay);
        logger.info("27号删除个人预跑批白名单结束,响应参数为:[{}]", deleteAutoPspWhiteInfoPer);

        logger.info("27号插入个人预跑批白名单开始,请求参数为:[{}]", openDay);
        int insertAutoPspWhiteInfoPer01 = cmis0429Mapper.insertAutoPspWhiteInfoPer01(openDay);
        logger.info("27号插入个人预跑批白名单结束,响应参数为:[{}]", insertAutoPspWhiteInfoPer01);


        logger.info("27号删除 非专业担保公司 担保 开始,请求参数为:[{}]", openDay);
        int deleteautoPspWhiteInfoPerNotGuarCom = cmis0429Mapper.deleteautoPspWhiteInfoPerNotGuarCom(openDay);
        logger.info("27号删除 非专业担保公司 担保 结束,返回参数为:[{}]", deleteautoPspWhiteInfoPerNotGuarCom);



        logger.info("27号插入实际控制人用于征信校验开始,请求参数为:[{}]", openDay);
        int insertAutoPspWhiteInfoPer02 = cmis0429Mapper.insertAutoPspWhiteInfoPer02(openDay);
        logger.info("27号插入实际控制人用于征信校验结束,响应参数为:[{}]", insertAutoPspWhiteInfoPer02);




        logger.info("6个月之内存在逾期欠息则删除此自动化名单开始,请求参数为:[{}]", openDay);
        int deletePspWhiteInfoCorpOverdue = cmis0429Mapper.deletePspWhiteInfoCorpOverdue(openDay);
        logger.info("6个月之内存在逾期欠息则删除此自动化名单结束,响应参数为:[{}]", deletePspWhiteInfoCorpOverdue);


        logger.info("27号不存在三个月之内的个人征信需要到人行查询征信报告开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer = cmis0429Mapper.updateAutoPspWhiteInfoPer(openDay);
        logger.info("27号不存在三个月之内的个人征信需要到人行查询征信报告结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer);
    }
}
