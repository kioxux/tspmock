package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0400</br>
 * 任务名称：加工任务-贷后管理-首次检查[清理贷后首次检查临时表数据]  </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月17日 上午00:56:54
 */
public interface Cmis0400Mapper {

    /**
     * 插入贷后检查任务表
     * @param openDay
     * @return
     */
    int truncateTable(@Param("openDay") String openDay);

    int truncateTable01(@Param("openDay") String openDay);
    int truncateTable21(@Param("openDay") String openDay);
    int truncateTable22(@Param("openDay") String openDay);

    int truncateTLMPTL(Map map);
    int deleteTLMPTL(Map map);
    int insertTLMPTL(Map map);
    int insertTmp(Map map);
    //int insertTmp2(Map map);
}
