/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.ypp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSYppTGuarBaseInfo
 * @类描述: bat_s_ypp_t_guar_base_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 17:10:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ypp_t_guar_base_info")
public class BatSYppTGuarBaseInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 押品编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "GUAR_NO")
	private String guarNo;
	
	/** 担保分类代码 **/
	@Column(name = "GUAR_TYPE_CD", unique = false, nullable = false, length = 9)
	private String guarTypeCd;
	
	/** 押品状态 **/
	@Column(name = "GUAR_STATE", unique = false, nullable = true, length = 10)
	private String guarState;
	
	/** 押品所在业务阶段 **/
	@Column(name = "GUAR_BUSISTATE", unique = false, nullable = true, length = 20)
	private String guarBusistate;
	
	/** 我行确认价值 **/
	@Column(name = "EVAL_AMT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal evalAmt;
	
	/** 确认价值币种 **/
	@Column(name = "EVAL_CURRENCY", unique = false, nullable = true, length = 10)
	private String evalCurrency;
	
	/** 占用额度 **/
	@Column(name = "USE_AMT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal useAmt;
	
	/** 剩余额度 **/
	@Column(name = "REMAIN_AMT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal remainAmt;
	
	/** 所有权人编号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 50)
	private String guarCusId;
	
	/** 所有权人名称 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 300)
	private String guarCusName;
	
	/** 押品所有人类型 **/
	@Column(name = "GUAR_CUS_TYPE", unique = false, nullable = true, length = 3)
	private String guarCusType;
	
	/** 押品所有人证件类型 **/
	@Column(name = "GUAR_CERT_TYPE", unique = false, nullable = true, length = 10)
	private String guarCertType;
	
	/** 押品所有人证件号码 **/
	@Column(name = "GUAR_CERT_CODE", unique = false, nullable = true, length = 40)
	private String guarCertCode;
	
	/** 是否共有财产 **/
	@Column(name = "COMMON_ASSETS_IND", unique = false, nullable = true, length = 10)
	private String commonAssetsInd;
	
	/** 押品所有人所占份额 **/
	@Column(name = "OCCUPYOFOWNER", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal occupyofowner;
	
	/** 是否需要办理保险 **/
	@Column(name = "INSURANCE_IND", unique = false, nullable = true, length = 4)
	private String insuranceInd;
	
	/** 是否强制创建 **/
	@Column(name = "FORCECREATE_IND", unique = false, nullable = true, length = 10)
	private String forcecreateInd;
	
	/** 强制创建理由 **/
	@Column(name = "FORCECREATE_REASON", unique = false, nullable = true, length = 4000)
	private String forcecreateReason;
	
	/** 押品是否已出租 **/
	@Column(name = "HIRE_IND", unique = false, nullable = true, length = 10)
	private String hireInd;
	
	/** 租赁起始日 **/
	@Column(name = "HIRE_START_DATE", unique = false, nullable = true, length = 10)
	private String hireStartDate;
	
	/** 租赁到期日 **/
	@Column(name = "HIRE_END_DATE", unique = false, nullable = true, length = 10)
	private String hireEndDate;
	
	/** 抵质押品法律有效性 **/
	@Column(name = "LAW_AVILABLE", unique = false, nullable = true, length = 10)
	private String lawAvilable;
	
	/** 抵质押品价值波动性 **/
	@Column(name = "VALUE_WAVE", unique = false, nullable = true, length = 10)
	private String valueWave;
	
	/** 抵质押品处置便利性 **/
	@Column(name = "EASYSEAL_STATE", unique = false, nullable = true, length = 10)
	private String easysealState;
	
	/** 抵质押品变现能力 **/
	@Column(name = "SALE_STATE", unique = false, nullable = true, length = 10)
	private String saleState;
	
	/** 保险办理状态 **/
	@Column(name = "INSUR_STATE", unique = false, nullable = true, length = 10)
	private String insurState;
	
	/** 创建时间 **/
	@Column(name = "GUAR_CREATE_DATE", unique = false, nullable = true, length = 10)
	private String guarCreateDate;
	
	/** 最后更新时间 **/
	@Column(name = "GUAR_LASTUPDATE_DATE", unique = false, nullable = true, length = 10)
	private String guarLastupdateDate;
	
	/** 创建人 **/
	@Column(name = "CREATE_USERID", unique = false, nullable = true, length = 100)
	private String createUserid;
	
	/** 创建机构 **/
	@Column(name = "CREATE_ORGID", unique = false, nullable = true, length = 40)
	private String createOrgid;
	
	/** 最后修改人 **/
	@Column(name = "LASTMODIFY_USERID", unique = false, nullable = true, length = 100)
	private String lastmodifyUserid;
	
	/** 最后修改人机构 **/
	@Column(name = "LASTMODIFY_ORGID", unique = false, nullable = true, length = 40)
	private String lastmodifyOrgid;
	
	/** 关键字段1 **/
	@Column(name = "HINGE_FIELD_01", unique = false, nullable = true, length = 1000)
	private String hingeField01;
	
	/** 关键字段2 **/
	@Column(name = "HINGE_FIELD_02", unique = false, nullable = true, length = 1000)
	private String hingeField02;
	
	/** 他行是否已设定担保权 **/
	@Column(name = "OTHER_BACK_GUAR_IND", unique = false, nullable = true, length = 10)
	private String otherBackGuarInd;
	
	/** 我行担保权优先受偿权顺序 **/
	@Column(name = "MYBACK_GUAR_FIRST_SEQ", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal mybackGuarFirstSeq;
	
	/** 权证完备状态（是否全部已落实） **/
	@Column(name = "CERTI_COMPLETE_STATUS", unique = false, nullable = true, length = 10)
	private String certiCompleteStatus;
	
	/** 我行确认日期 **/
	@Column(name = "EVAL_DATE", unique = false, nullable = true, length = 10)
	private String evalDate;
	
	/** 保证是否已落实 **/
	@Column(name = "GUAR_IMPLEMENTED_IND", unique = false, nullable = true, length = 10)
	private String guarImplementedInd;
	
	/** 原押品编号 **/
	@Column(name = "OLD_GUAR_NO", unique = false, nullable = true, length = 32)
	private String oldGuarNo;
	
	/** 备注信息 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 2000)
	private String remark;
	
	/** 押品登记办理状态 **/
	@Column(name = "REG_STATE", unique = false, nullable = true, length = 10)
	private String regState;
	
	/** 备用字段 **/
	@Column(name = "IS_REAL", unique = false, nullable = true, length = 10)
	private String isReal;
	
	/** 押品初次创建时间 **/
	@Column(name = "GUAR_FIRST_CREATE_TIME", unique = false, nullable = true, length = 10)
	private String guarFirstCreateTime;
	
	/** 人工认定结果 **/
	@Column(name = "MANUAL_AFFIRM_RS", unique = false, nullable = true, length = 10)
	private String manualAffirmRs;
	
	/** 是否权属清晰 **/
	@Column(name = "IS_OWNERSHIP_CLEAR", unique = false, nullable = true, length = 10)
	private String isOwnershipClear;
	
	/** 法定优先受偿款 **/
	@Column(name = "LEGAL_PRI_PAYMENT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal legalPriPayment;
	
	/** 押品所有人贷款卡号(抵押)  或 出质人贷款卡号(质押) **/
	@Column(name = "ASSURE_CARD_NO", unique = false, nullable = true, length = 40)
	private String assureCardNo;
	
	/** 担保权生效方式 **/
	@Column(name = "DEF_EFFECT_TYPE", unique = false, nullable = true, length = 10)
	private String defEffectType;
	
	/** 已设定担保他行名称 **/
	@Column(name = "OTHER_BACK_GUAR_NAME", unique = false, nullable = true, length = 100)
	private String otherBackGuarName;
	
	/** 管户人 **/
	@Column(name = "ACCOUNT_MANAGER", unique = false, nullable = true, length = 10)
	private String accountManager;
	
	/** 是否实质正相关 **/
	@Column(name = "RELATION_INT", unique = false, nullable = true, length = 10)
	private String relationInt;
	
	/** 创建系统 **/
	@Column(name = "CREATE_SYS", unique = false, nullable = true, length = 10)
	private String createSys;
	
	/** 修改原因 **/
	@Column(name = "MODIFY_REMARK", unique = false, nullable = true, length = 2000)
	private String modifyRemark;
	
	/** 是否需要抵质押合同公正 **/
	@Column(name = "CONTRACT_JUSTICE_IND", unique = false, nullable = true, length = 10)
	private String contractJusticeInd;
	
	/** 抵质押物与借款人相关性 **/
	@Column(name = "GUAR_BORROWER_RELA", unique = false, nullable = true, length = 10)
	private String guarBorrowerRela;
	
	/** 查封便利性 **/
	@Column(name = "SHUT_DOWN_CONV", unique = false, nullable = true, length = 10)
	private String shutDownConv;
	
	/** 法律有效性 **/
	@Column(name = "LEGAL_VALIDITY", unique = false, nullable = true, length = 10)
	private String legalValidity;
	
	/** 抵质押品通用性 **/
	@Column(name = "GUAR_UNIVERSALITY", unique = false, nullable = true, length = 10)
	private String guarUniversality;
	
	/** 抵质押品变现能力 **/
	@Column(name = "GUAR_ABILITY", unique = false, nullable = true, length = 10)
	private String guarAbility;
	
	/** 价格波动性 **/
	@Column(name = "PRICE_VOLATILITY", unique = false, nullable = true, length = 10)
	private String priceVolatility;
	
	/** 抵质押品名称 **/
	@Column(name = "GUAR_NAME", unique = false, nullable = true, length = 500)
	private String guarName;
	
	/** 是否抵债资产 **/
	@Column(name = "IF_DEAL", unique = false, nullable = true, length = 2)
	private String ifDeal;
	
	/** 购入价格（用于信贷存量数据移植，页面不展示） **/
	@Column(name = "BUY_AMT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal buyAmt;
	
	/** 权属状况 **/
	@Column(name = "SHIP_STATUS", unique = false, nullable = true, length = 10)
	private String shipStatus;
	
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}
	
    /**
     * @return guarTypeCd
     */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}
	
	/**
	 * @param guarState
	 */
	public void setGuarState(String guarState) {
		this.guarState = guarState;
	}
	
    /**
     * @return guarState
     */
	public String getGuarState() {
		return this.guarState;
	}
	
	/**
	 * @param guarBusistate
	 */
	public void setGuarBusistate(String guarBusistate) {
		this.guarBusistate = guarBusistate;
	}
	
    /**
     * @return guarBusistate
     */
	public String getGuarBusistate() {
		return this.guarBusistate;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return evalAmt
     */
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param evalCurrency
	 */
	public void setEvalCurrency(String evalCurrency) {
		this.evalCurrency = evalCurrency;
	}
	
    /**
     * @return evalCurrency
     */
	public String getEvalCurrency() {
		return this.evalCurrency;
	}
	
	/**
	 * @param useAmt
	 */
	public void setUseAmt(java.math.BigDecimal useAmt) {
		this.useAmt = useAmt;
	}
	
    /**
     * @return useAmt
     */
	public java.math.BigDecimal getUseAmt() {
		return this.useAmt;
	}
	
	/**
	 * @param remainAmt
	 */
	public void setRemainAmt(java.math.BigDecimal remainAmt) {
		this.remainAmt = remainAmt;
	}
	
    /**
     * @return remainAmt
     */
	public java.math.BigDecimal getRemainAmt() {
		return this.remainAmt;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}
	
    /**
     * @return guarCusId
     */
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}
	
    /**
     * @return guarCusName
     */
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param guarCusType
	 */
	public void setGuarCusType(String guarCusType) {
		this.guarCusType = guarCusType;
	}
	
    /**
     * @return guarCusType
     */
	public String getGuarCusType() {
		return this.guarCusType;
	}
	
	/**
	 * @param guarCertType
	 */
	public void setGuarCertType(String guarCertType) {
		this.guarCertType = guarCertType;
	}
	
    /**
     * @return guarCertType
     */
	public String getGuarCertType() {
		return this.guarCertType;
	}
	
	/**
	 * @param guarCertCode
	 */
	public void setGuarCertCode(String guarCertCode) {
		this.guarCertCode = guarCertCode;
	}
	
    /**
     * @return guarCertCode
     */
	public String getGuarCertCode() {
		return this.guarCertCode;
	}
	
	/**
	 * @param commonAssetsInd
	 */
	public void setCommonAssetsInd(String commonAssetsInd) {
		this.commonAssetsInd = commonAssetsInd;
	}
	
    /**
     * @return commonAssetsInd
     */
	public String getCommonAssetsInd() {
		return this.commonAssetsInd;
	}
	
	/**
	 * @param occupyofowner
	 */
	public void setOccupyofowner(java.math.BigDecimal occupyofowner) {
		this.occupyofowner = occupyofowner;
	}
	
    /**
     * @return occupyofowner
     */
	public java.math.BigDecimal getOccupyofowner() {
		return this.occupyofowner;
	}
	
	/**
	 * @param insuranceInd
	 */
	public void setInsuranceInd(String insuranceInd) {
		this.insuranceInd = insuranceInd;
	}
	
    /**
     * @return insuranceInd
     */
	public String getInsuranceInd() {
		return this.insuranceInd;
	}
	
	/**
	 * @param forcecreateInd
	 */
	public void setForcecreateInd(String forcecreateInd) {
		this.forcecreateInd = forcecreateInd;
	}
	
    /**
     * @return forcecreateInd
     */
	public String getForcecreateInd() {
		return this.forcecreateInd;
	}
	
	/**
	 * @param forcecreateReason
	 */
	public void setForcecreateReason(String forcecreateReason) {
		this.forcecreateReason = forcecreateReason;
	}
	
    /**
     * @return forcecreateReason
     */
	public String getForcecreateReason() {
		return this.forcecreateReason;
	}
	
	/**
	 * @param hireInd
	 */
	public void setHireInd(String hireInd) {
		this.hireInd = hireInd;
	}
	
    /**
     * @return hireInd
     */
	public String getHireInd() {
		return this.hireInd;
	}
	
	/**
	 * @param hireStartDate
	 */
	public void setHireStartDate(String hireStartDate) {
		this.hireStartDate = hireStartDate;
	}
	
    /**
     * @return hireStartDate
     */
	public String getHireStartDate() {
		return this.hireStartDate;
	}
	
	/**
	 * @param hireEndDate
	 */
	public void setHireEndDate(String hireEndDate) {
		this.hireEndDate = hireEndDate;
	}
	
    /**
     * @return hireEndDate
     */
	public String getHireEndDate() {
		return this.hireEndDate;
	}
	
	/**
	 * @param lawAvilable
	 */
	public void setLawAvilable(String lawAvilable) {
		this.lawAvilable = lawAvilable;
	}
	
    /**
     * @return lawAvilable
     */
	public String getLawAvilable() {
		return this.lawAvilable;
	}
	
	/**
	 * @param valueWave
	 */
	public void setValueWave(String valueWave) {
		this.valueWave = valueWave;
	}
	
    /**
     * @return valueWave
     */
	public String getValueWave() {
		return this.valueWave;
	}
	
	/**
	 * @param easysealState
	 */
	public void setEasysealState(String easysealState) {
		this.easysealState = easysealState;
	}
	
    /**
     * @return easysealState
     */
	public String getEasysealState() {
		return this.easysealState;
	}
	
	/**
	 * @param saleState
	 */
	public void setSaleState(String saleState) {
		this.saleState = saleState;
	}
	
    /**
     * @return saleState
     */
	public String getSaleState() {
		return this.saleState;
	}
	
	/**
	 * @param insurState
	 */
	public void setInsurState(String insurState) {
		this.insurState = insurState;
	}
	
    /**
     * @return insurState
     */
	public String getInsurState() {
		return this.insurState;
	}
	
	/**
	 * @param guarCreateDate
	 */
	public void setGuarCreateDate(String guarCreateDate) {
		this.guarCreateDate = guarCreateDate;
	}
	
    /**
     * @return guarCreateDate
     */
	public String getGuarCreateDate() {
		return this.guarCreateDate;
	}
	
	/**
	 * @param guarLastupdateDate
	 */
	public void setGuarLastupdateDate(String guarLastupdateDate) {
		this.guarLastupdateDate = guarLastupdateDate;
	}
	
    /**
     * @return guarLastupdateDate
     */
	public String getGuarLastupdateDate() {
		return this.guarLastupdateDate;
	}
	
	/**
	 * @param createUserid
	 */
	public void setCreateUserid(String createUserid) {
		this.createUserid = createUserid;
	}
	
    /**
     * @return createUserid
     */
	public String getCreateUserid() {
		return this.createUserid;
	}
	
	/**
	 * @param createOrgid
	 */
	public void setCreateOrgid(String createOrgid) {
		this.createOrgid = createOrgid;
	}
	
    /**
     * @return createOrgid
     */
	public String getCreateOrgid() {
		return this.createOrgid;
	}
	
	/**
	 * @param lastmodifyUserid
	 */
	public void setLastmodifyUserid(String lastmodifyUserid) {
		this.lastmodifyUserid = lastmodifyUserid;
	}
	
    /**
     * @return lastmodifyUserid
     */
	public String getLastmodifyUserid() {
		return this.lastmodifyUserid;
	}
	
	/**
	 * @param lastmodifyOrgid
	 */
	public void setLastmodifyOrgid(String lastmodifyOrgid) {
		this.lastmodifyOrgid = lastmodifyOrgid;
	}
	
    /**
     * @return lastmodifyOrgid
     */
	public String getLastmodifyOrgid() {
		return this.lastmodifyOrgid;
	}
	
	/**
	 * @param hingeField01
	 */
	public void setHingeField01(String hingeField01) {
		this.hingeField01 = hingeField01;
	}
	
    /**
     * @return hingeField01
     */
	public String getHingeField01() {
		return this.hingeField01;
	}
	
	/**
	 * @param hingeField02
	 */
	public void setHingeField02(String hingeField02) {
		this.hingeField02 = hingeField02;
	}
	
    /**
     * @return hingeField02
     */
	public String getHingeField02() {
		return this.hingeField02;
	}
	
	/**
	 * @param otherBackGuarInd
	 */
	public void setOtherBackGuarInd(String otherBackGuarInd) {
		this.otherBackGuarInd = otherBackGuarInd;
	}
	
    /**
     * @return otherBackGuarInd
     */
	public String getOtherBackGuarInd() {
		return this.otherBackGuarInd;
	}
	
	/**
	 * @param mybackGuarFirstSeq
	 */
	public void setMybackGuarFirstSeq(java.math.BigDecimal mybackGuarFirstSeq) {
		this.mybackGuarFirstSeq = mybackGuarFirstSeq;
	}
	
    /**
     * @return mybackGuarFirstSeq
     */
	public java.math.BigDecimal getMybackGuarFirstSeq() {
		return this.mybackGuarFirstSeq;
	}
	
	/**
	 * @param certiCompleteStatus
	 */
	public void setCertiCompleteStatus(String certiCompleteStatus) {
		this.certiCompleteStatus = certiCompleteStatus;
	}
	
    /**
     * @return certiCompleteStatus
     */
	public String getCertiCompleteStatus() {
		return this.certiCompleteStatus;
	}
	
	/**
	 * @param evalDate
	 */
	public void setEvalDate(String evalDate) {
		this.evalDate = evalDate;
	}
	
    /**
     * @return evalDate
     */
	public String getEvalDate() {
		return this.evalDate;
	}
	
	/**
	 * @param guarImplementedInd
	 */
	public void setGuarImplementedInd(String guarImplementedInd) {
		this.guarImplementedInd = guarImplementedInd;
	}
	
    /**
     * @return guarImplementedInd
     */
	public String getGuarImplementedInd() {
		return this.guarImplementedInd;
	}
	
	/**
	 * @param oldGuarNo
	 */
	public void setOldGuarNo(String oldGuarNo) {
		this.oldGuarNo = oldGuarNo;
	}
	
    /**
     * @return oldGuarNo
     */
	public String getOldGuarNo() {
		return this.oldGuarNo;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param regState
	 */
	public void setRegState(String regState) {
		this.regState = regState;
	}
	
    /**
     * @return regState
     */
	public String getRegState() {
		return this.regState;
	}
	
	/**
	 * @param isReal
	 */
	public void setIsReal(String isReal) {
		this.isReal = isReal;
	}
	
    /**
     * @return isReal
     */
	public String getIsReal() {
		return this.isReal;
	}
	
	/**
	 * @param guarFirstCreateTime
	 */
	public void setGuarFirstCreateTime(String guarFirstCreateTime) {
		this.guarFirstCreateTime = guarFirstCreateTime;
	}
	
    /**
     * @return guarFirstCreateTime
     */
	public String getGuarFirstCreateTime() {
		return this.guarFirstCreateTime;
	}
	
	/**
	 * @param manualAffirmRs
	 */
	public void setManualAffirmRs(String manualAffirmRs) {
		this.manualAffirmRs = manualAffirmRs;
	}
	
    /**
     * @return manualAffirmRs
     */
	public String getManualAffirmRs() {
		return this.manualAffirmRs;
	}
	
	/**
	 * @param isOwnershipClear
	 */
	public void setIsOwnershipClear(String isOwnershipClear) {
		this.isOwnershipClear = isOwnershipClear;
	}
	
    /**
     * @return isOwnershipClear
     */
	public String getIsOwnershipClear() {
		return this.isOwnershipClear;
	}
	
	/**
	 * @param legalPriPayment
	 */
	public void setLegalPriPayment(java.math.BigDecimal legalPriPayment) {
		this.legalPriPayment = legalPriPayment;
	}
	
    /**
     * @return legalPriPayment
     */
	public java.math.BigDecimal getLegalPriPayment() {
		return this.legalPriPayment;
	}
	
	/**
	 * @param assureCardNo
	 */
	public void setAssureCardNo(String assureCardNo) {
		this.assureCardNo = assureCardNo;
	}
	
    /**
     * @return assureCardNo
     */
	public String getAssureCardNo() {
		return this.assureCardNo;
	}
	
	/**
	 * @param defEffectType
	 */
	public void setDefEffectType(String defEffectType) {
		this.defEffectType = defEffectType;
	}
	
    /**
     * @return defEffectType
     */
	public String getDefEffectType() {
		return this.defEffectType;
	}
	
	/**
	 * @param otherBackGuarName
	 */
	public void setOtherBackGuarName(String otherBackGuarName) {
		this.otherBackGuarName = otherBackGuarName;
	}
	
    /**
     * @return otherBackGuarName
     */
	public String getOtherBackGuarName() {
		return this.otherBackGuarName;
	}
	
	/**
	 * @param accountManager
	 */
	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}
	
    /**
     * @return accountManager
     */
	public String getAccountManager() {
		return this.accountManager;
	}
	
	/**
	 * @param relationInt
	 */
	public void setRelationInt(String relationInt) {
		this.relationInt = relationInt;
	}
	
    /**
     * @return relationInt
     */
	public String getRelationInt() {
		return this.relationInt;
	}
	
	/**
	 * @param createSys
	 */
	public void setCreateSys(String createSys) {
		this.createSys = createSys;
	}
	
    /**
     * @return createSys
     */
	public String getCreateSys() {
		return this.createSys;
	}
	
	/**
	 * @param modifyRemark
	 */
	public void setModifyRemark(String modifyRemark) {
		this.modifyRemark = modifyRemark;
	}
	
    /**
     * @return modifyRemark
     */
	public String getModifyRemark() {
		return this.modifyRemark;
	}
	
	/**
	 * @param contractJusticeInd
	 */
	public void setContractJusticeInd(String contractJusticeInd) {
		this.contractJusticeInd = contractJusticeInd;
	}
	
    /**
     * @return contractJusticeInd
     */
	public String getContractJusticeInd() {
		return this.contractJusticeInd;
	}
	
	/**
	 * @param guarBorrowerRela
	 */
	public void setGuarBorrowerRela(String guarBorrowerRela) {
		this.guarBorrowerRela = guarBorrowerRela;
	}
	
    /**
     * @return guarBorrowerRela
     */
	public String getGuarBorrowerRela() {
		return this.guarBorrowerRela;
	}
	
	/**
	 * @param shutDownConv
	 */
	public void setShutDownConv(String shutDownConv) {
		this.shutDownConv = shutDownConv;
	}
	
    /**
     * @return shutDownConv
     */
	public String getShutDownConv() {
		return this.shutDownConv;
	}
	
	/**
	 * @param legalValidity
	 */
	public void setLegalValidity(String legalValidity) {
		this.legalValidity = legalValidity;
	}
	
    /**
     * @return legalValidity
     */
	public String getLegalValidity() {
		return this.legalValidity;
	}
	
	/**
	 * @param guarUniversality
	 */
	public void setGuarUniversality(String guarUniversality) {
		this.guarUniversality = guarUniversality;
	}
	
    /**
     * @return guarUniversality
     */
	public String getGuarUniversality() {
		return this.guarUniversality;
	}
	
	/**
	 * @param guarAbility
	 */
	public void setGuarAbility(String guarAbility) {
		this.guarAbility = guarAbility;
	}
	
    /**
     * @return guarAbility
     */
	public String getGuarAbility() {
		return this.guarAbility;
	}
	
	/**
	 * @param priceVolatility
	 */
	public void setPriceVolatility(String priceVolatility) {
		this.priceVolatility = priceVolatility;
	}
	
    /**
     * @return priceVolatility
     */
	public String getPriceVolatility() {
		return this.priceVolatility;
	}
	
	/**
	 * @param guarName
	 */
	public void setGuarName(String guarName) {
		this.guarName = guarName;
	}
	
    /**
     * @return guarName
     */
	public String getGuarName() {
		return this.guarName;
	}
	
	/**
	 * @param ifDeal
	 */
	public void setIfDeal(String ifDeal) {
		this.ifDeal = ifDeal;
	}
	
    /**
     * @return ifDeal
     */
	public String getIfDeal() {
		return this.ifDeal;
	}
	
	/**
	 * @param buyAmt
	 */
	public void setBuyAmt(java.math.BigDecimal buyAmt) {
		this.buyAmt = buyAmt;
	}
	
    /**
     * @return buyAmt
     */
	public java.math.BigDecimal getBuyAmt() {
		return this.buyAmt;
	}
	
	/**
	 * @param shipStatus
	 */
	public void setShipStatus(String shipStatus) {
		this.shipStatus = shipStatus;
	}
	
    /**
     * @return shipStatus
     */
	public String getShipStatus() {
		return this.shipStatus;
	}


}