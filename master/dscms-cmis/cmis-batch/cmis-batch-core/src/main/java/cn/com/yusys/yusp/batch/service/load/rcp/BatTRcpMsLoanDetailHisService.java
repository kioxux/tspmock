/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.rcp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatTRcpMsLoanDetailHis;
import cn.com.yusys.yusp.batch.repository.mapper.load.rcp.BatTRcpMsLoanDetailHisMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpMsLoanDetailHisService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:04:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTRcpMsLoanDetailHisService {

    @Autowired
    private BatTRcpMsLoanDetailHisMapper batTRcpMsLoanDetailHisMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTRcpMsLoanDetailHis selectByPrimaryKey(String uuid, String refNbr) {
        return batTRcpMsLoanDetailHisMapper.selectByPrimaryKey(uuid, refNbr);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTRcpMsLoanDetailHis> selectAll(QueryModel model) {
        List<BatTRcpMsLoanDetailHis> records = (List<BatTRcpMsLoanDetailHis>) batTRcpMsLoanDetailHisMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTRcpMsLoanDetailHis> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTRcpMsLoanDetailHis> list = batTRcpMsLoanDetailHisMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTRcpMsLoanDetailHis record) {
        return batTRcpMsLoanDetailHisMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTRcpMsLoanDetailHis record) {
        return batTRcpMsLoanDetailHisMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTRcpMsLoanDetailHis record) {
        return batTRcpMsLoanDetailHisMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTRcpMsLoanDetailHis record) {
        return batTRcpMsLoanDetailHisMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String uuid, String refNbr) {
        return batTRcpMsLoanDetailHisMapper.deleteByPrimaryKey(uuid, refNbr);
    }

    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTRcpMsLoanDetailHisMapper.truncateTTable();
    }
}
