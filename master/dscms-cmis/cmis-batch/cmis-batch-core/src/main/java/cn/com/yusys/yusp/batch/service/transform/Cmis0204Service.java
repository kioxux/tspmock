package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0204Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0204</br>
 * 任务名称：加工任务-额度处理-表外占用总余额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0204Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0204Service.class);
    @Autowired
    private Cmis0204Mapper cmis0204Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新额度占用关系03，对应SQL为CMIS0204-表外占用总余额.sql
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0204UpdateLmtContRel(String openDay) {
        logger.info("truncateLmtContRel07更新额度占用关系开始,请求参数为:[{}]", openDay);
        // cmis0204Mapper.truncateLmtContRel07();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateLmtContRel07更新额度占用关系结束");


        logger.info("表外业务  BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 用信最高额（未到期）：本合同项下最高可用信金额/(1-保证金比例)（不管是否项下台账是否存在结清，不恢复额度）,请求参数为:[{}]", openDay);
        int insertLmtContRel07 = cmis0204Mapper.insertLmtContRel07(openDay);
        logger.info("表外业务  BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 用信最高额（未到期）：本合同项下最高可用信金额/(1-保证金比例)（不管是否项下台账是否存在结清，不恢复额度）结束,返回参数为:[{}]", insertLmtContRel07);

        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额开始,请求参数为:[{}]", openDay);
        int updateLmtContRel07 = cmis0204Mapper.updateLmtContRel07(openDay);
        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额结束,返回参数为:[{}]", updateLmtContRel07);


        logger.info("更新额度占用关系开始,请求参数为:[{}]", openDay);
        // cmis0204Mapper.truncateLmtContRel09();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_cop");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("更新额度占用关系结束");


        logger.info("银承占用子授信分项开始,请求参数为:[{}]", openDay);
        int insertLmtContRel09AYchtsq = cmis0204Mapper.insertLmtContRel09AYchtsq(openDay);
        logger.info("银承占用子授信分项结束,返回参数为:[{}]", insertLmtContRel09AYchtsq);

        logger.info("银承占用子授信分项开始,请求参数为:[{}]", openDay);
        int insertLmtContRel09AYchtxq = cmis0204Mapper.insertLmtContRel09AYchtxq(openDay);
        logger.info("银承占用子授信分项结束,返回参数为:[{}]", insertLmtContRel09AYchtxq);

        logger.info("保函协议申请详情占用子授信分项开始,请求参数为:[{}]", openDay);
        int insertLmtContRel09ABhxysq = cmis0204Mapper.insertLmtContRel09ABhxysq(openDay);
        logger.info("保函协议申请详情占用子授信分项结束,返回参数为:[{}]", insertLmtContRel09ABhxysq);

        logger.info("保函协议详情占用子授信分项开始,请求参数为:[{}]", openDay);
        int insertLmtContRel09ABhxyxq = cmis0204Mapper.insertLmtContRel09ABhxyxq(openDay);
        logger.info("保函协议详情占用子授信分项结束,返回参数为:[{}]", insertLmtContRel09ABhxyxq);

        logger.info("贴现协议申请详情占用子授信分项开始,请求参数为:[{}]", openDay);
        int insertLmtContRel09ATxxysqxq = cmis0204Mapper.insertLmtContRel09ATxxysqxq(openDay);
        logger.info("贴现协议申请详情占用子授信分项结束,返回参数为:[{}]", insertLmtContRel09ATxxysqxq);

        logger.info("贴现协议详情占用子授信分项开始,请求参数为:[{}]", openDay);
        int insertLmtContRel09ATxxyxq = cmis0204Mapper.insertLmtContRel09ATxxyxq(openDay);
        logger.info("贴现协议详情占用子授信分项结束,返回参数为:[{}]", insertLmtContRel09ATxxyxq);

        logger.info("开证合同申请详情占用子授信分项开始,请求参数为:[{}]", openDay);
        int insertLmtContRel09AKzhtsqxq = cmis0204Mapper.insertLmtContRel09AKzhtsqxq(openDay);
        logger.info("开证合同申请详情占用子授信分项结束,返回参数为:[{}]", insertLmtContRel09AKzhtsqxq);

        logger.info("开证合同详情占用子授信分项开始,请求参数为:[{}]", openDay);
        int insertLmtContRel09AKzhtxq = cmis0204Mapper.insertLmtContRel09AKzhtxq(openDay);
        logger.info("开证合同详情占用子授信分项结束,返回参数为:[{}]", insertLmtContRel09AKzhtxq);


        logger.info("根据授信占用临时表表数据 更新 授信占用关系表开始,请求参数为:[{}]", openDay);
        int updateTdlcrTla = cmis0204Mapper.updateTdlcrTla(openDay);
        logger.info("根据授信占用临时表表数据 更新 授信占用关系表结束,返回参数为:[{}]", updateTdlcrTla);

        logger.info("更新额度占用关系中的占用总敞口余额开始,请求参数为:[{}]", openDay);
        int updateTdlcr01 = cmis0204Mapper.updateTdlcr01(openDay);
        logger.info("更新额度占用关系中的占用总敞口余额结束,返回参数为:[{}]", updateTdlcr01);

        logger.info("根据授信占用临时表表数据 更新 授信占用关系表开始,请求参数为:[{}]", openDay);
        int updateLmtContRel09 = cmis0204Mapper.updateLmtContRel09(openDay);
        logger.info("根据授信占用临时表表数据 更新 授信占用关系表结束,返回参数为:[{}]", updateLmtContRel09);
    }
}
