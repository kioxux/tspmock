package cn.com.yusys.yusp.batch.repository.mapper.server.cmisbatch0005;

import java.util.Map;

/**
 * 接口处理Mapper:更新客户移交相关表
 *
 * @author leehuang
 * @version 1.0
 */
public interface CmisBatch0005Mapper {
    /**
     * 根据传入的值更新相关表中对应的值
     *
     * @param updateMap
     * @return
     */
    int updateManagerInfoByMap(Map updateMap);
}
