/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.ypp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSYppTInfLivingRoom
 * @类描述: bat_s_ypp_t_inf_living_room数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 17:10:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ypp_t_inf_living_room")
public class BatSYppTInfLivingRoom extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 押品编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "GUAR_NO")
	private String guarNo;
	
	/** 现房/期房标识 **/
	@Column(name = "READY_OR_PERIOD_HOUSE", unique = false, nullable = true, length = 50)
	private String readyOrPeriodHouse;
	
	/** 预购商品房预告登记证明号 **/
	@Column(name = "PURCHSE_HOUSE_NO", unique = false, nullable = true, length = 75)
	private String purchseHouseNo;
	
	/** 预购商品房抵押权预告登记证明号 **/
	@Column(name = "PURCHSE_HOUSE_REG_NO", unique = false, nullable = true, length = 75)
	private String purchseHouseRegNo;
	
	/** 预售许可证编号 **/
	@Column(name = "PRESELL_PERMIT_NO", unique = false, nullable = true, length = 75)
	private String presellPermitNo;
	
	/** 预售许可证有效期 **/
	@Column(name = "PRESELL_PERMIT_VAL_DATE", unique = false, nullable = true, length = 10)
	private String presellPermitValDate;
	
	/** 预计房屋交付时间 **/
	@Column(name = "P_GET_DATE", unique = false, nullable = true, length = 10)
	private String pGetDate;
	
	/** 预计预抵押凭证取得时间 **/
	@Column(name = "P_CREDIT_GET_DATE", unique = false, nullable = true, length = 10)
	private String pCreditGetDate;
	
	/** 预计产权证办理时间 **/
	@Column(name = "P_DO_DATE", unique = false, nullable = true, length = 10)
	private String pDoDate;
	
	/** 预计他项权证取得时间 **/
	@Column(name = "P_OTHER_DATE", unique = false, nullable = true, length = 10)
	private String pOtherDate;
	
	/** 一手/二手标识 **/
	@Column(name = "IS_USED", unique = false, nullable = true, length = 10)
	private String isUsed;
	
	/** 是否两证合一 **/
	@Column(name = "TWOCARD2ONE_IND", unique = false, nullable = true, length = 10)
	private String twocard2oneInd;
	
	/** 房产证（不动产权证）号 **/
	@Column(name = "HOUSE_LAND_NO", unique = false, nullable = true, length = 200)
	private String houseLandNo;
	
	/** 销售许可证编号 **/
	@Column(name = "MARKET_PERMIT_NO", unique = false, nullable = true, length = 75)
	private String marketPermitNo;
	
	/** 该产证所属房产是否全部抵押 **/
	@Column(name = "HOUSE_ALL_PLEDGE_IND", unique = false, nullable = true, length = 10)
	private String houseAllPledgeInd;
	
	/** 部分抵押房产的部位描述 **/
	@Column(name = "PART_REG_POSITION_DESC", unique = false, nullable = true, length = 600)
	private String partRegPositionDesc;
	
	/** 房地产买卖合同编号 **/
	@Column(name = "BUSINESS_HOUSE_NO", unique = false, nullable = true, length = 400)
	private String businessHouseNo;
	
	/** 购买日期 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 购买价格（元） **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 销售备案 **/
	@Column(name = "SALE_BAK", unique = false, nullable = true, length = 10)
	private String saleBak;
	
	/** 是否本次申请所购房产 **/
	@Column(name = "APPLY_FOR_HOUSE", unique = false, nullable = true, length = 10)
	private String applyForHouse;
	
	/** 抵押住房是否权属人唯一住所 **/
	@Column(name = "HOUSE_OWNERSHIP_IND", unique = false, nullable = true, length = 10)
	private String houseOwnershipInd;
	
	/** 建筑面积(㎡) **/
	@Column(name = "BUILD_AREA", unique = false, nullable = true, length = 30)
	private java.math.BigDecimal buildArea;
	
	/** 建成年份 **/
	@Column(name = "ACTIVATE_YEARS", unique = false, nullable = true, length = 10)
	private String activateYears;
	
	/** 房屋产权期限信息（年） **/
	@Column(name = "HOUSE_PR_DESC", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal housePrDesc;
	
	/** 楼龄 **/
	@Column(name = "FLOOR_AGE", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal floorAge;
	
	/** 套型 **/
	@Column(name = "F_STY", unique = false, nullable = true, length = 10)
	private String fSty;
	
	/** 朝向 **/
	@Column(name = "ORIENTATIONS", unique = false, nullable = true, length = 10)
	private String orientations;
	
	/** 地面构造 **/
	@Column(name = "GROUND_STRUCTURE", unique = false, nullable = true, length = 10)
	private String groundStructure;
	
	/** 屋顶构造 **/
	@Column(name = "ROOF_STRUCTURE", unique = false, nullable = true, length = 10)
	private String roofStructure;
	
	/** 房屋状态 **/
	@Column(name = "HOUSE_STA", unique = false, nullable = true, length = 10)
	private String houseSta;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 街道/村镇/路名 **/
	@Column(name = "STREET", unique = false, nullable = true, length = 300)
	private String street;
	
	/** 门牌号/弄号 **/
	@Column(name = "HOUSE_NO", unique = false, nullable = true, length = 300)
	private String houseNo;
	
	/** 楼号 **/
	@Column(name = "BUILDING_ROOM_NUM", unique = false, nullable = true, length = 300)
	private String buildingRoomNum;
	
	/** 房产证（不动产权证）地址/期房预售合同地址 **/
	@Column(name = "POC_ADDR", unique = false, nullable = true, length = 600)
	private String pocAddr;
	
	/** 楼盘（社区）名称 **/
	@Column(name = "COMMUNITY_NAME", unique = false, nullable = true, length = 600)
	private String communityName;
	
	/** 层次（标的所在楼层） **/
	@Column(name = "BDLC", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal bdlc;
	
	/** 层数（标的所在楼高） **/
	@Column(name = "BDGD", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal bdgd;
	
	/** 房地产所在地段情况 **/
	@Column(name = "HOUSE_PLACE_INFO", unique = false, nullable = true, length = 10)
	private String housePlaceInfo;
	
	/** 周边环境 **/
	@Column(name = "ARR_ENV", unique = false, nullable = true, length = 10)
	private String arrEnv;
	
	/** 建筑物说明 **/
	@Column(name = "BUILD_DESC", unique = false, nullable = true, length = 1000)
	private String buildDesc;
	
	/** 土地证号 **/
	@Column(name = "LAND_NO", unique = false, nullable = true, length = 450)
	private String landNo;
	
	/** 土地使用权性质 **/
	@Column(name = "LAND_USE_QUAL", unique = false, nullable = true, length = 10)
	private String landUseQual;
	
	/** 土地使用权使用年限起始日期 **/
	@Column(name = "LAND_USE_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	@Column(name = "LAND_USE_END_DATE", unique = false, nullable = true, length = 10)
	private String landUseEndDate;
	
	/** 土地使用年限 **/
	@Column(name = "LAND_USE_YEARS", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal landUseYears;
	
	/** 土地用途 **/
	@Column(name = "LAND_PURP", unique = false, nullable = true, length = 10)
	private String landPurp;
	
	/** 土地说明 **/
	@Column(name = "LAND_EXPLAIN", unique = false, nullable = true, length = 4000)
	private String landExplain;
	
	/** 公共配套 **/
	@Column(name = "PUBLIC_FACILITIES", unique = false, nullable = true, length = 300)
	private String publicFacilities;
	
	/** 楼层情况 **/
	@Column(name = "FLOOR", unique = false, nullable = true, length = 300)
	private String floor;
	
	/** 装修状况 **/
	@Column(name = "DECORATION", unique = false, nullable = true, length = 300)
	private String decoration;
	
	/** 平面布局 **/
	@Column(name = "PLANE_LAYOUT", unique = false, nullable = true, length = 300)
	private String planeLayout;
	
	/** 通风采光 **/
	@Column(name = "VENTILATION_AND_LIGHTING", unique = false, nullable = true, length = 300)
	private String ventilationAndLighting;
	
	/** 临街状况 **/
	@Column(name = "STREET_SITUATION", unique = false, nullable = true, length = 300)
	private String streetSituation;
	
	/** 地面构造其他文本输入 **/
	@Column(name = "GROUND_STRUCTURE_OTHER", unique = false, nullable = true, length = 300)
	private String groundStructureOther;
	
	/** 屋顶构造其他文本输入 **/
	@Column(name = "ROOF_STRUCTURE_OTHER", unique = false, nullable = true, length = 300)
	private String roofStructureOther;
	
	/** 土地用途其他文本输入 **/
	@Column(name = "LAND_PURP_OTHER", unique = false, nullable = true, length = 300)
	private String landPurpOther;
	
	/** 房屋结构（QBGZ_TYPE） **/
	@Column(name = "HOUSE_STRUCTURE", unique = false, nullable = true, length = 10)
	private String houseStructure;
	
	/** 是否有独立产权证 **/
	@Column(name = "IS_OWN_TITLE_DEED", unique = false, nullable = true, length = 10)
	private String isOwnTitleDeed;
	
	/** 车库/车位类型 **/
	@Column(name = "CARPORT_TYPE", unique = false, nullable = true, length = 10)
	private String carportType;
	
	/** 室号 **/
	@Column(name = "ROOM_NUM", unique = false, nullable = true, length = 100)
	private String roomNum;
	
	/** 原房产证（不动产权证）号 **/
	@Column(name = "OLD_HOUSE_LAND_NO", unique = false, nullable = true, length = 100)
	private String oldHouseLandNo;
	
	/** 土地使用权取得方式 **/
	@Column(name = "LAND_USE_WAY", unique = false, nullable = true, length = 150)
	private String landUseWay;
	
	/** 竣工日期 **/
	@Column(name = "COMPLETE_DATE", unique = false, nullable = true, length = 10)
	private String completeDate;
	
	/** 房产使用情况 **/
	@Column(name = "T_USAGE", unique = false, nullable = true, length = 10)
	private String tUsage;
	
	/** 土地面积(㎡) **/
	@Column(name = "LAND_AREA", unique = false, nullable = true, length = 30)
	private java.math.BigDecimal landArea;
	
	/** 房产商名称 **/
	@Column(name = "HOUSE_BOSS", unique = false, nullable = true, length = 300)
	private String houseBoss;
	
	/** 剩余使用年限 **/
	@Column(name = "HOUSE_REMAINDER_YEAR", unique = false, nullable = true, length = 10)
	private String houseRemainderYear;
	
	/** 物业情况 **/
	@Column(name = "PROPERTY", unique = false, nullable = true, length = 10)
	private String property;
	
	/** 所属地段 **/
	@Column(name = "AREA_LOCATION", unique = false, nullable = true, length = 10)
	private String areaLocation;
	
	/** 担保分类代码 **/
	@Column(name = "GUAR_TYPE_CD", unique = false, nullable = true, length = 9)
	private String guarTypeCd;
	
	/** 房屋用途 **/
	@Column(name = "HOUSE_USAGE", unique = false, nullable = true, length = 9)
	private String houseUsage;
	
	/** 土地用途（慧押押） **/
	@Column(name = "LAND_USAGE", unique = false, nullable = true, length = 9)
	private String landUsage;
	
	/** 实时查封状态 **/
	@Column(name = "SS_CF_STATUS", unique = false, nullable = true, length = 5)
	private String ssCfStatus;
	
	/** 查封时间 **/
	@Column(name = "CF_DATE", unique = false, nullable = true, length = 10)
	private String cfDate;
	
	/** 最高可抵押顺位 **/
	@Column(name = "MAX_YP_STATUS", unique = false, nullable = true, length = 5)
	private String maxYpStatus;
	
	/** 不动产单元号(苏州在线抵押使用) **/
	@Column(name = "BDCDYH_NO", unique = false, nullable = true, length = 100)
	private String bdcdyhNo;
	
	/** 是否包含车库(苏州在线抵押使用) **/
	@Column(name = "IS_CARPORT", unique = false, nullable = true, length = 5)
	private String isCarport;
	
	/** 是否包含阁楼(苏州在线抵押使用) **/
	@Column(name = "IS_ATTIC", unique = false, nullable = true, length = 5)
	private String isAttic;
	
	/** 状态 **/
	@Column(name = "INSPECT_STATUS", unique = false, nullable = true, length = 2)
	private String inspectStatus;
	
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param readyOrPeriodHouse
	 */
	public void setReadyOrPeriodHouse(String readyOrPeriodHouse) {
		this.readyOrPeriodHouse = readyOrPeriodHouse;
	}
	
    /**
     * @return readyOrPeriodHouse
     */
	public String getReadyOrPeriodHouse() {
		return this.readyOrPeriodHouse;
	}
	
	/**
	 * @param purchseHouseNo
	 */
	public void setPurchseHouseNo(String purchseHouseNo) {
		this.purchseHouseNo = purchseHouseNo;
	}
	
    /**
     * @return purchseHouseNo
     */
	public String getPurchseHouseNo() {
		return this.purchseHouseNo;
	}
	
	/**
	 * @param purchseHouseRegNo
	 */
	public void setPurchseHouseRegNo(String purchseHouseRegNo) {
		this.purchseHouseRegNo = purchseHouseRegNo;
	}
	
    /**
     * @return purchseHouseRegNo
     */
	public String getPurchseHouseRegNo() {
		return this.purchseHouseRegNo;
	}
	
	/**
	 * @param presellPermitNo
	 */
	public void setPresellPermitNo(String presellPermitNo) {
		this.presellPermitNo = presellPermitNo;
	}
	
    /**
     * @return presellPermitNo
     */
	public String getPresellPermitNo() {
		return this.presellPermitNo;
	}
	
	/**
	 * @param presellPermitValDate
	 */
	public void setPresellPermitValDate(String presellPermitValDate) {
		this.presellPermitValDate = presellPermitValDate;
	}
	
    /**
     * @return presellPermitValDate
     */
	public String getPresellPermitValDate() {
		return this.presellPermitValDate;
	}
	
	/**
	 * @param pGetDate
	 */
	public void setPGetDate(String pGetDate) {
		this.pGetDate = pGetDate;
	}
	
    /**
     * @return pGetDate
     */
	public String getPGetDate() {
		return this.pGetDate;
	}
	
	/**
	 * @param pCreditGetDate
	 */
	public void setPCreditGetDate(String pCreditGetDate) {
		this.pCreditGetDate = pCreditGetDate;
	}
	
    /**
     * @return pCreditGetDate
     */
	public String getPCreditGetDate() {
		return this.pCreditGetDate;
	}
	
	/**
	 * @param pDoDate
	 */
	public void setPDoDate(String pDoDate) {
		this.pDoDate = pDoDate;
	}
	
    /**
     * @return pDoDate
     */
	public String getPDoDate() {
		return this.pDoDate;
	}
	
	/**
	 * @param pOtherDate
	 */
	public void setPOtherDate(String pOtherDate) {
		this.pOtherDate = pOtherDate;
	}
	
    /**
     * @return pOtherDate
     */
	public String getPOtherDate() {
		return this.pOtherDate;
	}
	
	/**
	 * @param isUsed
	 */
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed;
	}
	
    /**
     * @return isUsed
     */
	public String getIsUsed() {
		return this.isUsed;
	}
	
	/**
	 * @param twocard2oneInd
	 */
	public void setTwocard2oneInd(String twocard2oneInd) {
		this.twocard2oneInd = twocard2oneInd;
	}
	
    /**
     * @return twocard2oneInd
     */
	public String getTwocard2oneInd() {
		return this.twocard2oneInd;
	}
	
	/**
	 * @param houseLandNo
	 */
	public void setHouseLandNo(String houseLandNo) {
		this.houseLandNo = houseLandNo;
	}
	
    /**
     * @return houseLandNo
     */
	public String getHouseLandNo() {
		return this.houseLandNo;
	}
	
	/**
	 * @param marketPermitNo
	 */
	public void setMarketPermitNo(String marketPermitNo) {
		this.marketPermitNo = marketPermitNo;
	}
	
    /**
     * @return marketPermitNo
     */
	public String getMarketPermitNo() {
		return this.marketPermitNo;
	}
	
	/**
	 * @param houseAllPledgeInd
	 */
	public void setHouseAllPledgeInd(String houseAllPledgeInd) {
		this.houseAllPledgeInd = houseAllPledgeInd;
	}
	
    /**
     * @return houseAllPledgeInd
     */
	public String getHouseAllPledgeInd() {
		return this.houseAllPledgeInd;
	}
	
	/**
	 * @param partRegPositionDesc
	 */
	public void setPartRegPositionDesc(String partRegPositionDesc) {
		this.partRegPositionDesc = partRegPositionDesc;
	}
	
    /**
     * @return partRegPositionDesc
     */
	public String getPartRegPositionDesc() {
		return this.partRegPositionDesc;
	}
	
	/**
	 * @param businessHouseNo
	 */
	public void setBusinessHouseNo(String businessHouseNo) {
		this.businessHouseNo = businessHouseNo;
	}
	
    /**
     * @return businessHouseNo
     */
	public String getBusinessHouseNo() {
		return this.businessHouseNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param saleBak
	 */
	public void setSaleBak(String saleBak) {
		this.saleBak = saleBak;
	}
	
    /**
     * @return saleBak
     */
	public String getSaleBak() {
		return this.saleBak;
	}
	
	/**
	 * @param applyForHouse
	 */
	public void setApplyForHouse(String applyForHouse) {
		this.applyForHouse = applyForHouse;
	}
	
    /**
     * @return applyForHouse
     */
	public String getApplyForHouse() {
		return this.applyForHouse;
	}
	
	/**
	 * @param houseOwnershipInd
	 */
	public void setHouseOwnershipInd(String houseOwnershipInd) {
		this.houseOwnershipInd = houseOwnershipInd;
	}
	
    /**
     * @return houseOwnershipInd
     */
	public String getHouseOwnershipInd() {
		return this.houseOwnershipInd;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(java.math.BigDecimal buildArea) {
		this.buildArea = buildArea;
	}
	
    /**
     * @return buildArea
     */
	public java.math.BigDecimal getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param activateYears
	 */
	public void setActivateYears(String activateYears) {
		this.activateYears = activateYears;
	}
	
    /**
     * @return activateYears
     */
	public String getActivateYears() {
		return this.activateYears;
	}
	
	/**
	 * @param housePrDesc
	 */
	public void setHousePrDesc(java.math.BigDecimal housePrDesc) {
		this.housePrDesc = housePrDesc;
	}
	
    /**
     * @return housePrDesc
     */
	public java.math.BigDecimal getHousePrDesc() {
		return this.housePrDesc;
	}
	
	/**
	 * @param floorAge
	 */
	public void setFloorAge(java.math.BigDecimal floorAge) {
		this.floorAge = floorAge;
	}
	
    /**
     * @return floorAge
     */
	public java.math.BigDecimal getFloorAge() {
		return this.floorAge;
	}
	
	/**
	 * @param fSty
	 */
	public void setFSty(String fSty) {
		this.fSty = fSty;
	}
	
    /**
     * @return fSty
     */
	public String getFSty() {
		return this.fSty;
	}
	
	/**
	 * @param orientations
	 */
	public void setOrientations(String orientations) {
		this.orientations = orientations;
	}
	
    /**
     * @return orientations
     */
	public String getOrientations() {
		return this.orientations;
	}
	
	/**
	 * @param groundStructure
	 */
	public void setGroundStructure(String groundStructure) {
		this.groundStructure = groundStructure;
	}
	
    /**
     * @return groundStructure
     */
	public String getGroundStructure() {
		return this.groundStructure;
	}
	
	/**
	 * @param roofStructure
	 */
	public void setRoofStructure(String roofStructure) {
		this.roofStructure = roofStructure;
	}
	
    /**
     * @return roofStructure
     */
	public String getRoofStructure() {
		return this.roofStructure;
	}
	
	/**
	 * @param houseSta
	 */
	public void setHouseSta(String houseSta) {
		this.houseSta = houseSta;
	}
	
    /**
     * @return houseSta
     */
	public String getHouseSta() {
		return this.houseSta;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param houseNo
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}
	
    /**
     * @return houseNo
     */
	public String getHouseNo() {
		return this.houseNo;
	}
	
	/**
	 * @param buildingRoomNum
	 */
	public void setBuildingRoomNum(String buildingRoomNum) {
		this.buildingRoomNum = buildingRoomNum;
	}
	
    /**
     * @return buildingRoomNum
     */
	public String getBuildingRoomNum() {
		return this.buildingRoomNum;
	}
	
	/**
	 * @param pocAddr
	 */
	public void setPocAddr(String pocAddr) {
		this.pocAddr = pocAddr;
	}
	
    /**
     * @return pocAddr
     */
	public String getPocAddr() {
		return this.pocAddr;
	}
	
	/**
	 * @param communityName
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}
	
    /**
     * @return communityName
     */
	public String getCommunityName() {
		return this.communityName;
	}
	
	/**
	 * @param bdlc
	 */
	public void setBdlc(java.math.BigDecimal bdlc) {
		this.bdlc = bdlc;
	}
	
    /**
     * @return bdlc
     */
	public java.math.BigDecimal getBdlc() {
		return this.bdlc;
	}
	
	/**
	 * @param bdgd
	 */
	public void setBdgd(java.math.BigDecimal bdgd) {
		this.bdgd = bdgd;
	}
	
    /**
     * @return bdgd
     */
	public java.math.BigDecimal getBdgd() {
		return this.bdgd;
	}
	
	/**
	 * @param housePlaceInfo
	 */
	public void setHousePlaceInfo(String housePlaceInfo) {
		this.housePlaceInfo = housePlaceInfo;
	}
	
    /**
     * @return housePlaceInfo
     */
	public String getHousePlaceInfo() {
		return this.housePlaceInfo;
	}
	
	/**
	 * @param arrEnv
	 */
	public void setArrEnv(String arrEnv) {
		this.arrEnv = arrEnv;
	}
	
    /**
     * @return arrEnv
     */
	public String getArrEnv() {
		return this.arrEnv;
	}
	
	/**
	 * @param buildDesc
	 */
	public void setBuildDesc(String buildDesc) {
		this.buildDesc = buildDesc;
	}
	
    /**
     * @return buildDesc
     */
	public String getBuildDesc() {
		return this.buildDesc;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo;
	}
	
    /**
     * @return landNo
     */
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual;
	}
	
    /**
     * @return landUseQual
     */
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate;
	}
	
    /**
     * @return landUseBeginDate
     */
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate;
	}
	
    /**
     * @return landUseEndDate
     */
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(java.math.BigDecimal landUseYears) {
		this.landUseYears = landUseYears;
	}
	
    /**
     * @return landUseYears
     */
	public java.math.BigDecimal getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp;
	}
	
    /**
     * @return landPurp
     */
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain;
	}
	
    /**
     * @return landExplain
     */
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param publicFacilities
	 */
	public void setPublicFacilities(String publicFacilities) {
		this.publicFacilities = publicFacilities;
	}
	
    /**
     * @return publicFacilities
     */
	public String getPublicFacilities() {
		return this.publicFacilities;
	}
	
	/**
	 * @param floor
	 */
	public void setFloor(String floor) {
		this.floor = floor;
	}
	
    /**
     * @return floor
     */
	public String getFloor() {
		return this.floor;
	}
	
	/**
	 * @param decoration
	 */
	public void setDecoration(String decoration) {
		this.decoration = decoration;
	}
	
    /**
     * @return decoration
     */
	public String getDecoration() {
		return this.decoration;
	}
	
	/**
	 * @param planeLayout
	 */
	public void setPlaneLayout(String planeLayout) {
		this.planeLayout = planeLayout;
	}
	
    /**
     * @return planeLayout
     */
	public String getPlaneLayout() {
		return this.planeLayout;
	}
	
	/**
	 * @param ventilationAndLighting
	 */
	public void setVentilationAndLighting(String ventilationAndLighting) {
		this.ventilationAndLighting = ventilationAndLighting;
	}
	
    /**
     * @return ventilationAndLighting
     */
	public String getVentilationAndLighting() {
		return this.ventilationAndLighting;
	}
	
	/**
	 * @param streetSituation
	 */
	public void setStreetSituation(String streetSituation) {
		this.streetSituation = streetSituation;
	}
	
    /**
     * @return streetSituation
     */
	public String getStreetSituation() {
		return this.streetSituation;
	}
	
	/**
	 * @param groundStructureOther
	 */
	public void setGroundStructureOther(String groundStructureOther) {
		this.groundStructureOther = groundStructureOther;
	}
	
    /**
     * @return groundStructureOther
     */
	public String getGroundStructureOther() {
		return this.groundStructureOther;
	}
	
	/**
	 * @param roofStructureOther
	 */
	public void setRoofStructureOther(String roofStructureOther) {
		this.roofStructureOther = roofStructureOther;
	}
	
    /**
     * @return roofStructureOther
     */
	public String getRoofStructureOther() {
		return this.roofStructureOther;
	}
	
	/**
	 * @param landPurpOther
	 */
	public void setLandPurpOther(String landPurpOther) {
		this.landPurpOther = landPurpOther;
	}
	
    /**
     * @return landPurpOther
     */
	public String getLandPurpOther() {
		return this.landPurpOther;
	}
	
	/**
	 * @param houseStructure
	 */
	public void setHouseStructure(String houseStructure) {
		this.houseStructure = houseStructure;
	}
	
    /**
     * @return houseStructure
     */
	public String getHouseStructure() {
		return this.houseStructure;
	}
	
	/**
	 * @param isOwnTitleDeed
	 */
	public void setIsOwnTitleDeed(String isOwnTitleDeed) {
		this.isOwnTitleDeed = isOwnTitleDeed;
	}
	
    /**
     * @return isOwnTitleDeed
     */
	public String getIsOwnTitleDeed() {
		return this.isOwnTitleDeed;
	}
	
	/**
	 * @param carportType
	 */
	public void setCarportType(String carportType) {
		this.carportType = carportType;
	}
	
    /**
     * @return carportType
     */
	public String getCarportType() {
		return this.carportType;
	}
	
	/**
	 * @param roomNum
	 */
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}
	
    /**
     * @return roomNum
     */
	public String getRoomNum() {
		return this.roomNum;
	}
	
	/**
	 * @param oldHouseLandNo
	 */
	public void setOldHouseLandNo(String oldHouseLandNo) {
		this.oldHouseLandNo = oldHouseLandNo;
	}
	
    /**
     * @return oldHouseLandNo
     */
	public String getOldHouseLandNo() {
		return this.oldHouseLandNo;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay;
	}
	
    /**
     * @return landUseWay
     */
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param completeDate
	 */
	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate;
	}
	
    /**
     * @return completeDate
     */
	public String getCompleteDate() {
		return this.completeDate;
	}
	
	/**
	 * @param tUsage
	 */
	public void setTUsage(String tUsage) {
		this.tUsage = tUsage;
	}
	
    /**
     * @return tUsage
     */
	public String getTUsage() {
		return this.tUsage;
	}
	
	/**
	 * @param landArea
	 */
	public void setLandArea(java.math.BigDecimal landArea) {
		this.landArea = landArea;
	}
	
    /**
     * @return landArea
     */
	public java.math.BigDecimal getLandArea() {
		return this.landArea;
	}
	
	/**
	 * @param houseBoss
	 */
	public void setHouseBoss(String houseBoss) {
		this.houseBoss = houseBoss;
	}
	
    /**
     * @return houseBoss
     */
	public String getHouseBoss() {
		return this.houseBoss;
	}
	
	/**
	 * @param houseRemainderYear
	 */
	public void setHouseRemainderYear(String houseRemainderYear) {
		this.houseRemainderYear = houseRemainderYear;
	}
	
    /**
     * @return houseRemainderYear
     */
	public String getHouseRemainderYear() {
		return this.houseRemainderYear;
	}
	
	/**
	 * @param property
	 */
	public void setProperty(String property) {
		this.property = property;
	}
	
    /**
     * @return property
     */
	public String getProperty() {
		return this.property;
	}
	
	/**
	 * @param areaLocation
	 */
	public void setAreaLocation(String areaLocation) {
		this.areaLocation = areaLocation;
	}
	
    /**
     * @return areaLocation
     */
	public String getAreaLocation() {
		return this.areaLocation;
	}
	
	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}
	
    /**
     * @return guarTypeCd
     */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}
	
	/**
	 * @param houseUsage
	 */
	public void setHouseUsage(String houseUsage) {
		this.houseUsage = houseUsage;
	}
	
    /**
     * @return houseUsage
     */
	public String getHouseUsage() {
		return this.houseUsage;
	}
	
	/**
	 * @param landUsage
	 */
	public void setLandUsage(String landUsage) {
		this.landUsage = landUsage;
	}
	
    /**
     * @return landUsage
     */
	public String getLandUsage() {
		return this.landUsage;
	}
	
	/**
	 * @param ssCfStatus
	 */
	public void setSsCfStatus(String ssCfStatus) {
		this.ssCfStatus = ssCfStatus;
	}
	
    /**
     * @return ssCfStatus
     */
	public String getSsCfStatus() {
		return this.ssCfStatus;
	}
	
	/**
	 * @param cfDate
	 */
	public void setCfDate(String cfDate) {
		this.cfDate = cfDate;
	}
	
    /**
     * @return cfDate
     */
	public String getCfDate() {
		return this.cfDate;
	}
	
	/**
	 * @param maxYpStatus
	 */
	public void setMaxYpStatus(String maxYpStatus) {
		this.maxYpStatus = maxYpStatus;
	}
	
    /**
     * @return maxYpStatus
     */
	public String getMaxYpStatus() {
		return this.maxYpStatus;
	}
	
	/**
	 * @param bdcdyhNo
	 */
	public void setBdcdyhNo(String bdcdyhNo) {
		this.bdcdyhNo = bdcdyhNo;
	}
	
    /**
     * @return bdcdyhNo
     */
	public String getBdcdyhNo() {
		return this.bdcdyhNo;
	}
	
	/**
	 * @param isCarport
	 */
	public void setIsCarport(String isCarport) {
		this.isCarport = isCarport;
	}
	
    /**
     * @return isCarport
     */
	public String getIsCarport() {
		return this.isCarport;
	}
	
	/**
	 * @param isAttic
	 */
	public void setIsAttic(String isAttic) {
		this.isAttic = isAttic;
	}
	
    /**
     * @return isAttic
     */
	public String getIsAttic() {
		return this.isAttic;
	}
	
	/**
	 * @param inspectStatus
	 */
	public void setInspectStatus(String inspectStatus) {
		this.inspectStatus = inspectStatus;
	}
	
    /**
     * @return inspectStatus
     */
	public String getInspectStatus() {
		return this.inspectStatus;
	}


}