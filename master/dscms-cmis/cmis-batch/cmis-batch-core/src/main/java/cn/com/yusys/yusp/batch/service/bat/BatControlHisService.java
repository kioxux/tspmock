/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatControlHis;
import cn.com.yusys.yusp.batch.repository.mapper.bat.BatControlHisMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: yusp-batch-core模块
 * @类名称: BatControlHisService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-09 17:19:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatControlHisService {

    @Autowired
    private BatControlHisMapper batControlHisMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatControlHis selectByPrimaryKey(String openDay) {
        return batControlHisMapper.selectByPrimaryKey(openDay);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatControlHis> selectAll(QueryModel model) {
        List<BatControlHis> records = (List<BatControlHis>) batControlHisMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatControlHis> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatControlHis> list = batControlHisMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatControlHis record) {
        record.setInputDate(DateUtils.getCurrDateStr()); // 登记日期
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setCreateTime(DateUtils.getCurrTimestamp());// 创建时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batControlHisMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatControlHis record) {
        record.setInputDate(DateUtils.getCurrDateStr()); // 登记日期
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setCreateTime(DateUtils.getCurrTimestamp());// 创建时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batControlHisMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatControlHis record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batControlHisMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatControlHis record) {
        return batControlHisMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String openDay) {
        return batControlHisMapper.deleteByPrimaryKey(openDay);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batControlHisMapper.deleteByIds(ids);
    }
}
