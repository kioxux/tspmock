package cn.com.yusys.yusp.batch.repository.mapper.extract.core;

/**
 * 批量任务处理类：</br>
 * 任务编号：CORE0101</br>
 * 任务名称：文件处理任务-核心系统-营改增信息[bat_core_t_ygz]</br>
 *
 * @author chenchao
 * @version 1.0
 * @since 2021年7月6日 下午11:56:54
 */
public interface Core0101Mapper {
    /**
     * 清空营改增信息
     */
    void truncateYgz();

    /**
     * 加工到营改增信息
     *
     * @param openDay
     * @return
     */
    int insertYgz(String openDay);
}
