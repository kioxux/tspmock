/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTPjpTDepartment
 * @类描述: bat_t_pjp_t_department数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-03 10:58:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_pjp_t_department")
public class BatTPjpTDepartment extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 名称 **/
	@Column(name = "D_NAME", unique = false, nullable = true, length = 100)
	private String dName;
	
	/** 描述 **/
	@Column(name = "D_DESC", unique = false, nullable = true, length = 300)
	private String dDesc;
	
	/** 序号 **/
	@Column(name = "D_ORDER", unique = false, nullable = true, length = 10)
	private Integer dOrder;
	
	/** 上层机构ID **/
	@Column(name = "D_PID", unique = false, nullable = true, length = 50)
	private String dPid;
	
	/** 状态 STD_RZ_D_STATUS **/
	@Column(name = "D_STATUS", unique = false, nullable = true, length = 10)
	private String dStatus;
	
	/** 机构级别(层级 根为1 依次2、3) **/
	@Column(name = "D_LEVEL", unique = false, nullable = true, length = 10)
	private String dLevel;
	
	/** ID层级字串儿(ID层级字串儿，用/分隔) **/
	@Column(name = "D_TREECODE", unique = false, nullable = true, length = 100)
	private String dTreecode;
	
	/** 是否机构 STD_RZ_D_ISORG **/
	@Column(name = "D_ISORG", unique = false, nullable = true, length = 10)
	private String dIsorg;
	
	/** 大额支付系统行号 **/
	@Column(name = "D_BANKNUMBER", unique = false, nullable = true, length = 12)
	private String dBanknumber;
	
	/** 组织机构代码 **/
	@Column(name = "D_ORGCODE", unique = false, nullable = true, length = 10)
	private String dOrgcode;
	
	/** 系统内部行号 **/
	@Column(name = "D_INNERBANKCODE", unique = false, nullable = true, length = 100)
	private String dInnerbankcode;
	
	/** 核算中心网点号 **/
	@Column(name = "D_AUDITBANKCODE", unique = false, nullable = true, length = 20)
	private String dAuditbankcode;
	
	/** 是否本机构(供汇总信息用 默认值:是) STD_YES_NO **/
	@Column(name = "IS_BRANCH", unique = false, nullable = true, length = 2)
	private String isBranch;
	
	/** 库存机构ID **/
	@Column(name = "D_STOCKDEPT", unique = false, nullable = true, length = 50)
	private String dStockdept;
	
	/** 地址 **/
	@Column(name = "ADDRESS", unique = false, nullable = true, length = 300)
	private String address;
	
	/** 【废弃】承兑状态(用于承兑签发时回显承兑行名称) 

STD_RZ_ACPTDEPT **/
	@Column(name = "ACPTDEPT", unique = false, nullable = true, length = 2)
	private String acptdept;
	
	/** 贴现库存机构ID **/
	@Column(name = "D_DISCOUNTDTOCKDEPT", unique = false, nullable = true, length = 50)
	private String dDiscountdtockdept;
	
	/** 托收账号，只有总行或者分行才需要维护 **/
	@Column(name = "COLLECTIONACCOUNT", unique = false, nullable = true, length = 40)
	private String collectionaccount;
	
	/** 邮编 **/
	@Column(name = "AREACODE", unique = false, nullable = true, length = 20)
	private String areacode;
	
	/** 内部使用层级编码 **/
	@Column(name = "LEVEL_CODE", unique = false, nullable = true, length = 80)
	private String levelCode;
	
	/** 是否主业务机构 **/
	@Column(name = "MAINBRA_FLG", unique = false, nullable = true, length = 2)
	private String mainbraFlg;
	
	/** 自动记账柜员号 **/
	@Column(name = "AUTO_USER_NO", unique = false, nullable = true, length = 40)
	private String autoUserNo;
	
	/** 电子记账柜员号 **/
	@Column(name = "ELC_USER_NO", unique = false, nullable = true, length = 40)
	private String elcUserNo;
	
	/** 电话 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 40)
	private String phone;
	
	/** 机构内部账户 **/
	@Column(name = "INNER_ACCT_NO", unique = false, nullable = true, length = 60)
	private String innerAcctNo;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param dName
	 */
	public void setDName(String dName) {
		this.dName = dName;
	}
	
    /**
     * @return dName
     */
	public String getDName() {
		return this.dName;
	}
	
	/**
	 * @param dDesc
	 */
	public void setDDesc(String dDesc) {
		this.dDesc = dDesc;
	}
	
    /**
     * @return dDesc
     */
	public String getDDesc() {
		return this.dDesc;
	}
	
	/**
	 * @param dOrder
	 */
	public void setDOrder(Integer dOrder) {
		this.dOrder = dOrder;
	}
	
    /**
     * @return dOrder
     */
	public Integer getDOrder() {
		return this.dOrder;
	}
	
	/**
	 * @param dPid
	 */
	public void setDPid(String dPid) {
		this.dPid = dPid;
	}
	
    /**
     * @return dPid
     */
	public String getDPid() {
		return this.dPid;
	}
	
	/**
	 * @param dStatus
	 */
	public void setDStatus(String dStatus) {
		this.dStatus = dStatus;
	}
	
    /**
     * @return dStatus
     */
	public String getDStatus() {
		return this.dStatus;
	}
	
	/**
	 * @param dLevel
	 */
	public void setDLevel(String dLevel) {
		this.dLevel = dLevel;
	}
	
    /**
     * @return dLevel
     */
	public String getDLevel() {
		return this.dLevel;
	}
	
	/**
	 * @param dTreecode
	 */
	public void setDTreecode(String dTreecode) {
		this.dTreecode = dTreecode;
	}
	
    /**
     * @return dTreecode
     */
	public String getDTreecode() {
		return this.dTreecode;
	}
	
	/**
	 * @param dIsorg
	 */
	public void setDIsorg(String dIsorg) {
		this.dIsorg = dIsorg;
	}
	
    /**
     * @return dIsorg
     */
	public String getDIsorg() {
		return this.dIsorg;
	}
	
	/**
	 * @param dBanknumber
	 */
	public void setDBanknumber(String dBanknumber) {
		this.dBanknumber = dBanknumber;
	}
	
    /**
     * @return dBanknumber
     */
	public String getDBanknumber() {
		return this.dBanknumber;
	}
	
	/**
	 * @param dOrgcode
	 */
	public void setDOrgcode(String dOrgcode) {
		this.dOrgcode = dOrgcode;
	}
	
    /**
     * @return dOrgcode
     */
	public String getDOrgcode() {
		return this.dOrgcode;
	}
	
	/**
	 * @param dInnerbankcode
	 */
	public void setDInnerbankcode(String dInnerbankcode) {
		this.dInnerbankcode = dInnerbankcode;
	}
	
    /**
     * @return dInnerbankcode
     */
	public String getDInnerbankcode() {
		return this.dInnerbankcode;
	}
	
	/**
	 * @param dAuditbankcode
	 */
	public void setDAuditbankcode(String dAuditbankcode) {
		this.dAuditbankcode = dAuditbankcode;
	}
	
    /**
     * @return dAuditbankcode
     */
	public String getDAuditbankcode() {
		return this.dAuditbankcode;
	}
	
	/**
	 * @param isBranch
	 */
	public void setIsBranch(String isBranch) {
		this.isBranch = isBranch;
	}
	
    /**
     * @return isBranch
     */
	public String getIsBranch() {
		return this.isBranch;
	}
	
	/**
	 * @param dStockdept
	 */
	public void setDStockdept(String dStockdept) {
		this.dStockdept = dStockdept;
	}
	
    /**
     * @return dStockdept
     */
	public String getDStockdept() {
		return this.dStockdept;
	}
	
	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
    /**
     * @return address
     */
	public String getAddress() {
		return this.address;
	}
	
	/**
	 * @param acptdept
	 */
	public void setAcptdept(String acptdept) {
		this.acptdept = acptdept;
	}
	
    /**
     * @return acptdept
     */
	public String getAcptdept() {
		return this.acptdept;
	}
	
	/**
	 * @param dDiscountdtockdept
	 */
	public void setDDiscountdtockdept(String dDiscountdtockdept) {
		this.dDiscountdtockdept = dDiscountdtockdept;
	}
	
    /**
     * @return dDiscountdtockdept
     */
	public String getDDiscountdtockdept() {
		return this.dDiscountdtockdept;
	}
	
	/**
	 * @param collectionaccount
	 */
	public void setCollectionaccount(String collectionaccount) {
		this.collectionaccount = collectionaccount;
	}
	
    /**
     * @return collectionaccount
     */
	public String getCollectionaccount() {
		return this.collectionaccount;
	}
	
	/**
	 * @param areacode
	 */
	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}
	
    /**
     * @return areacode
     */
	public String getAreacode() {
		return this.areacode;
	}
	
	/**
	 * @param levelCode
	 */
	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}
	
    /**
     * @return levelCode
     */
	public String getLevelCode() {
		return this.levelCode;
	}
	
	/**
	 * @param mainbraFlg
	 */
	public void setMainbraFlg(String mainbraFlg) {
		this.mainbraFlg = mainbraFlg;
	}
	
    /**
     * @return mainbraFlg
     */
	public String getMainbraFlg() {
		return this.mainbraFlg;
	}
	
	/**
	 * @param autoUserNo
	 */
	public void setAutoUserNo(String autoUserNo) {
		this.autoUserNo = autoUserNo;
	}
	
    /**
     * @return autoUserNo
     */
	public String getAutoUserNo() {
		return this.autoUserNo;
	}
	
	/**
	 * @param elcUserNo
	 */
	public void setElcUserNo(String elcUserNo) {
		this.elcUserNo = elcUserNo;
	}
	
    /**
     * @return elcUserNo
     */
	public String getElcUserNo() {
		return this.elcUserNo;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param innerAcctNo
	 */
	public void setInnerAcctNo(String innerAcctNo) {
		this.innerAcctNo = innerAcctNo;
	}
	
    /**
     * @return innerAcctNo
     */
	public String getInnerAcctNo() {
		return this.innerAcctNo;
	}


}