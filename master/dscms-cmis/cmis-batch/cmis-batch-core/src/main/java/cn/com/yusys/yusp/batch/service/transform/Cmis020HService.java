package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0200Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS020H</br>
 * 任务名称：加工任务-额度处理-批前备份台账临时表</br>
 * 重要备注：此任务从CMIS0200中拆分出来，该任务不单独建立Mapper，直接复用原有的业务Mapper</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis020HService {
    private static final Logger logger = LoggerFactory.getLogger(Cmis020HService.class);
    @Autowired
    private Cmis0200Mapper cmis0200Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 批前备份台账临时表-银承台账
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis020HDeleteInsertTmpBalanceCompareAccYc(String openDay) {
        logger.info("清空临时表-银承台账表开始");
        // cmis0200Mapper.truncateTmpAccAccp(); //  cmis_biz.tmp_acc_accp
        tableUtilsService.renameCreateDropTable("cmis_biz", "tmp_acc_accp");// 20211107 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("清空分项占用关系信息加工表结束");
        logger.info("清空临时表-银承台账表结束");

        logger.info("插入临时表-银承台账表开始，请求参数:[{}]", openDay);
        int insertTmpAccAccp = cmis0200Mapper.insertTmpAccAccp(openDay);
        logger.info("插入临时表-银承台账表结束，返回参数:[{}]", insertTmpAccAccp);


        logger.info("删除备份日表-银承台账表0200开始");
        // cmis0200Mapper.deleteTmpDAccApp0200();//  cmis_biz.tmp_d_acc_accp_0200
        tableUtilsService.renameCreateDropTable("cmis_biz", "tmp_d_acc_accp_0200");// 20211107 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("删除备份日表-银承台账表0200结束");

        logger.info("插入备份日表-银承台账表0200开始，请求参数:[{}]", openDay);
        int insertTmpDAccApp0200 = cmis0200Mapper.insertTmpDAccApp0200(openDay);
        logger.info("插入备份日表-银承台账表0200结束，返回参数:[{}]", insertTmpDAccApp0200);

    }


    /**
     * 批前备份台账临时表-贴现台账
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis020HDeleteInsertTmpBalanceCompareAccTx(String openDay) {
        logger.info("清空临时表-贴现台账表开始");
        // cmis0200Mapper.truncateTmpAccDisc();//  cmis_biz.tmp_acc_disc
        tableUtilsService.renameCreateDropTable("cmis_biz", "tmp_acc_disc");// 20211107 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("清空临时表-贴现台账表结束");

        logger.info("插入临时表-贴现台账表开始，请求参数:[{}]", openDay);
        int insertTmpAccDisc = cmis0200Mapper.insertTmpAccDisc(openDay);
        logger.info("插入临时表-贴现台账表结束，返回参数:[{}]", insertTmpAccDisc);

        logger.info("删除备份日表_贴现台账0200开始");
        // cmis0200Mapper.deleteTmpDAccDisc0200();//  cmis_biz.tmp_d_acc_disc_0200
        tableUtilsService.renameCreateDropTable("cmis_biz", "tmp_d_acc_disc_0200");// 20211107 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("删除备份日表_贴现台账0200结束");

        logger.info("插入备份日表_贴现台账0200开始，请求参数:[{}]", openDay);
        int insertTmpDAccDisc0200 = cmis0200Mapper.insertTmpDAccDisc0200(openDay);
        logger.info("插入备份日表_贴现台账0200结束，返回参数:[{}]", insertTmpDAccDisc0200);
    }

    /**
     * 批前备份台账临时表-开证台账
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis020HDeleteInsertTmpBalanceCompareAccXyz(String openDay) {
        logger.info("清空临时表-开证台账表开始");
        // cmis0200Mapper.truncateTmpAccTfLoc();//  cmis_biz.TMP_ACC_TF_LOC
        tableUtilsService.renameCreateDropTable("cmis_biz", "TMP_ACC_TF_LOC");// 20211107 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("清空临时表-开证台账表结束");

        logger.info("插入临时表-开证台账表开始，请求参数:[{}]", openDay);
        int insertTmpAccTfLoc = cmis0200Mapper.insertTmpAccTfLoc(openDay);
        logger.info("插入临时表-开证台账表结束，返回参数:[{}]", insertTmpAccTfLoc);
    }


    /**
     * 批前备份台账临时表-保函台账
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis020HDeleteInsertTmpBalanceCompareAccBh(String openDay) {
        logger.info("清空临时表-保函台账开始");
        // cmis0200Mapper.truncateTmpAccCvrs();//  cmis_biz.TMP_ACC_CVRS
        tableUtilsService.renameCreateDropTable("cmis_biz", "TMP_ACC_CVRS");// 20211107 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("清空临时表-保函台账结束");

        logger.info("插入临时表-保函台账开始，请求参数:[{}]", openDay);
        int insertTmpAccCvrs = cmis0200Mapper.insertTmpAccCvrs(openDay);
        logger.info("插入临时表-保函台账结束，返回参数:[{}]", insertTmpAccCvrs);
    }
}
