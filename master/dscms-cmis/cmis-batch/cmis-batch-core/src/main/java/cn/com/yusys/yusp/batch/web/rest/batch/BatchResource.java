package cn.com.yusys.yusp.batch.web.rest.batch;

import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.dto.batch.ParallelRunReqDto;
import cn.com.yusys.yusp.batch.dto.batch.ParallelRunRespDto;
import cn.com.yusys.yusp.batch.dto.batch.SingleRunReqDto;
import cn.com.yusys.yusp.batch.dto.batch.SingleRunRespDto;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskCfgService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.util.HttpCallerUtil;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/batchresource")
public class BatchResource {
    private static final Logger logger = LoggerFactory.getLogger(BatchResource.class);

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private SimpleJobLauncher customJobLauncher;
    @Autowired
    private BatControlRunService batControlRunService;
    @Autowired
    private BatTaskRunService batTaskRunService;
    @Autowired
    private BatTaskCfgService batTaskCfgService;
    @Autowired
    private BatTaskRelService batTaskRelService;


    /**
     * 运行单个任务
     *
     * @param singleRunReqDto 请求Dto：运行单个任务
     * @return
     */
    @PostMapping("/singlerun")
    public @ResponseBody
    SingleRunRespDto singlerun(@RequestBody SingleRunReqDto singleRunReqDto) {
        int result = 0;// 返回结果,0-成功,1-失败
        String msg = "调用成功";// 返回消息
        String taskNo = singleRunReqDto.getTaskNo();//任务编号
        String openDay = singleRunReqDto.getOpenDay();//营业日期
        if (Objects.nonNull(taskNo) && Objects.nonNull(openDay)) {
            // 最早开始时间检查

            // 最晚开始时间检查

            // 依赖关系检查

            // 互斥关系检查
            String jobName = taskNo.concat("Job");
            try {
                JobExecution execution = null;
                if (!Objects.equals("batStart", taskNo)) {
                    // 查询任务运行信息
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, "任务日期为:[" + openDay + "],任务编号为:[" + taskNo + "]");
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, taskNo.toUpperCase());
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));

                    execution = customJobLauncher.run((Job) applicationContext.getBean(taskNo + "Job"), createJobParams(openDay, taskNo, false));

                    if ("COMPLETED".equals(execution.getExitStatus().getExitCode())) {
                        result = 0;
                        logger.info("任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行成功");
                        batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                        int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    } else {
                        result = 1;
                        msg = "任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行异常";
                        batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_101.key);// 任务状态 STD_TASK_TYPE, 执行失败
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                        int updateTaskStatus101 = batTaskRunService.updateSelective(batTaskRun);
                        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus101));
                        logger.error(msg);
                    }
                } else {
                    execution = customJobLauncher.run((Job) applicationContext.getBean(taskNo + "Job"), createJobParams(openDay, taskNo, false));
                }
            } catch (BizException e) {
                msg = "任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行异常,异常信息为:" + e.getMessage();
                logger.error(msg);
                // 查询任务运行信息
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, "任务日期为:[" + openDay + "],任务编号为:[" + taskNo + "]");
                BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, taskNo.toUpperCase());
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));

                batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_101.key);// 任务状态 STD_TASK_TYPE, 执行失败

                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                int updateTaskStatus101 = batTaskRunService.updateSelective(batTaskRun);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus101));
            } catch (Exception e) {
                msg = "任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行异常,异常信息为:" + e.getMessage();
                logger.error(msg);
                // 查询任务运行信息
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, "任务日期为:[" + openDay + "],任务编号为:[" + taskNo + "]");
                BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, taskNo.toUpperCase());
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));

                batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_101.key);// 任务状态 STD_TASK_TYPE, 执行失败

                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                int updateTaskStatus101 = batTaskRunService.updateSelective(batTaskRun);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus101));
            }
        } else {
            logger.error("传入参数小于一个，启动失败！！");
            result = 1;
        }
        SingleRunRespDto singleRunRespDto = GenericBuilder.of(SingleRunRespDto::new)
                .with(SingleRunRespDto::setTaskNo, taskNo)
                .with(SingleRunRespDto::setOpenDay, openDay)
                .with(SingleRunRespDto::setResult, result)
                .with(SingleRunRespDto::setMsg, msg)
                .build();
        return singleRunRespDto;
    }

    /**
     * 单个任务运行
     *
     * @param singleRunReqDto
     * @return
     */
    @PostMapping("/singlerun4use")
    public int singlerun4use(@RequestBody SingleRunReqDto singleRunReqDto) {
        int result = 0;// 返回结果,0-成功,1-失败
        String msg = "";// 返回消息
        String taskNo = singleRunReqDto.getTaskNo();//任务编号
        String openDay = singleRunReqDto.getOpenDay();//营业日期
        if (Objects.nonNull(taskNo) && Objects.nonNull(openDay)) {
            // 最早开始时间检查

            // 最晚开始时间检查

            // 依赖关系检查

            // 互斥关系检查
            String jobName = taskNo.concat("Job");
            try {
                JobExecution execution = null;
                if (!Objects.equals("batStart", taskNo)) {
                    // 查询任务运行信息
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, "任务日期为:[" + openDay + "],任务编号为:[" + taskNo + "]");
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, taskNo.toUpperCase());
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));

                    execution = customJobLauncher.run((Job) applicationContext.getBean(taskNo + "Job"), createJobParams(openDay, taskNo, false));

                    if ("COMPLETED".equals(execution.getExitStatus().getExitCode())) {
                        result = 0;
                        logger.info("任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行成功");
                        batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                        int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    } else {
                        result = 1;
                        msg = "任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行异常";
                        batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_101.key);// 任务状态 STD_TASK_TYPE, 执行失败
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                        int updateTaskStatus101 = batTaskRunService.updateSelective(batTaskRun);
                        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus101));
                        logger.error(msg);
                    }
                } else {
                    execution = customJobLauncher.run((Job) applicationContext.getBean(taskNo + "Job"), createJobParams(openDay, taskNo, false));
                }
            } catch (BizException e) {
                msg = "任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行异常,异常信息为:" + e.getMessage();
                logger.error(msg);
                // 查询任务运行信息
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, "任务日期为:[" + openDay + "],任务编号为:[" + taskNo + "]");
                BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, taskNo.toUpperCase());
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));

                batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_101.key);// 任务状态 STD_TASK_TYPE, 执行失败

                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                int updateTaskStatus101 = batTaskRunService.updateSelective(batTaskRun);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus101));
            } catch (Exception e) {
                msg = "任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行异常,异常信息为:" + e.getMessage();
                logger.error(msg);
                // 查询任务运行信息
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, "任务日期为:[" + openDay + "],任务编号为:[" + taskNo + "]");
                BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, taskNo.toUpperCase());
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));

                batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_101.key);// 任务状态 STD_TASK_TYPE, 执行失败

                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                int updateTaskStatus101 = batTaskRunService.updateSelective(batTaskRun);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus101));
            }
        } else {
            logger.error("传入参数小于一个，启动失败！！");
            result = 1;
        }
        return result;
    }

    /**
     * 单个任务运行
     *
     * @param taskNo  任务编号
     * @param openDay 营业日期
     * @return
     */
    @PostMapping("/singlerun4shell")
    public int singlerun4shell(String taskNo, String openDay) {
        int result = 0;// 返回结果,0-成功,1-失败
        String msg = "";// 返回消息
        if (Objects.nonNull(taskNo) && Objects.nonNull(openDay)) {
            String jobName = taskNo.concat("Job");

            try {
                JobExecution execution = null;
                if (!Objects.equals("batStart", taskNo)) {
                    execution = customJobLauncher.run((Job) applicationContext.getBean(taskNo + "Job"), createJobParams(openDay, taskNo, false));
                    if ("COMPLETED".equals(execution.getExitStatus().getExitCode())) {
                        result = 0;
                        logger.info("任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行成功");
                    } else {
                        result = 1;
                        msg = "任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行异常";
                        logger.error(msg);
                    }
                } else {
                    execution = customJobLauncher.run((Job) applicationContext.getBean(taskNo + "Job"), createJobParams(openDay, taskNo, false));
                }
            } catch (BizException e) {
                msg = "任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行异常,异常信息为:" + e.getMessage();
                logger.error(msg);
            } catch (Exception e) {
                msg = "任务日期:【" + openDay + "】,任务编号:【" + taskNo + "】,任务名称:【" + jobName + "】执行异常,异常信息为:" + e.getMessage();
                logger.error(msg);
            }
        } else {
            logger.error("传入参数小于一个，启动失败！！");
            result = 1;
        }
        return result;
    }

    /**
     * 多个任务并发运行
     *
     * @param parallelRunReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/parallelrun")
    public @ResponseBody
    ParallelRunRespDto parallelRun(@Validated @RequestBody ParallelRunReqDto parallelRunReqDto) throws Exception {
        logger.info("处理[多个任务并发运行]逻辑开始,请求参数为:[{}]", JSON.toJSONString(parallelRunReqDto));
        java.util.List<SingleRunReqDto> singleRunReqDtos = parallelRunReqDto.getSingleRunReqDtos();

        java.util.List<SingleRunRespDto> singleRunRespDtos = singleRunReqDtos.parallelStream().map(
                singleRunReqDto -> {
                    SingleRunRespDto singleRunRespDto = new SingleRunRespDto();
                    String url = "http://10.85.10.211:6008/api/batchresource/singlerun";
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]开始");
                    try {
                        singleRunRespDto = HttpCallerUtil.post(url, singleRunReqDto);
                    } catch (BizException e) {
                        logger.error("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]异常信息为:[" + e.getMessage() + "]");
                    }
                    logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]结束");
                    return singleRunRespDto;
                }
        ).collect(Collectors.toList());

        ParallelRunRespDto parallelRunRespDto = GenericBuilder.of(ParallelRunRespDto::new).with(ParallelRunRespDto::setSingleRunRespDtos, singleRunRespDtos).build();
        logger.info("处理[多个任务并发运行]逻辑结束,响应参数为:[{}]", JSON.toJSONString(parallelRunRespDto));
        return parallelRunRespDto;
    }

    /**
     * 并发启动开始任务和任务级别为1的任务
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/parallelRunByPriFlag01")
    public @ResponseBody int parallelRunByPriFlag01() throws Exception {
        int result = 0;// 0成功，1失败
        // 查询批量运行表，获取任务日期，任务日期等于营业日期
        QueryModel queryModel = new QueryModel();
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(queryModel));
        List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunList));
        if (CollectionUtils.isEmpty(batControlRunList)) {
            // 调度运行管理(BAT_CONTROL_RUN)未配置
            logger.error(TradeLogConstants.BATCH_STEP_EXCEPTION_PREFIX_LOGGER, JobStepEnum.BATSTART_RUN_STEP.key, JobStepEnum.BATSTART_RUN_STEP.value, EchEnum.ECH080008.value);
            throw BizException.error(null, EchEnum.ECH080008.key, EchEnum.ECH080008.value);
        }
        String taskDate = batControlRunList.get(0).getOpenDay();//营业日期

        // 运行 batStart任务
        JobExecution execution = null;
        String taskNo = "batStart";
        // 查询任务运行信息
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, "任务日期为:[" + taskDate + "],任务编号为:[" + taskNo + "]");

        execution = customJobLauncher.run((Job) applicationContext.getBean(taskNo + "Job"), createJobParams(taskDate, taskNo, false));

        if ("COMPLETED".equals(execution.getExitStatus().getExitCode())) {
            BatTaskRun batStartTask = batTaskRunService.selectByPrimaryKey(taskDate, taskNo.toUpperCase());
            logger.info("任务日期:【" + taskDate + "】,任务编号:【" + taskNo + "】,任务名称:【" + batStartTask.getTaskName() + "】执行成功");
            batStartTask.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batStartTask));
            int updateTaskStatus100 = batTaskRunService.updateSelective(batStartTask);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
        } else {
            result = 1;
            return result;
        }

        // 线程池初始化
        logger.info("线程池初始化开始");
        int waitTime = 60;//等待时间
        int corePoolSize = 150;//核心线程数
        int queueCapacity = 300;//等待队列长度
        int maxPoolSize = 150;//线程最大数量
        ThreadPoolTaskExecutor pendingTaskExecutor = new ThreadPoolTaskExecutor();
        pendingTaskExecutor.setCorePoolSize(corePoolSize);
        pendingTaskExecutor.setQueueCapacity(queueCapacity);
        pendingTaskExecutor.setMaxPoolSize(maxPoolSize);
        pendingTaskExecutor.setThreadNamePrefix("ParallelRunByPriFlag01");
        pendingTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        pendingTaskExecutor.initialize();
        logger.info("线程池初始化结束");
        try {
            // 每次循环任务运行清单
            String loopRunningTaskList = StringUtils.EMPTY;
            // 待运行任务标志，当所有任务处理完成，跳出循环
            boolean flag = true;
            while (flag) {
                // 查询待运行的任务列表
                QueryModel btrlQm = new QueryModel();
                btrlQm.addCondition("taskDate", taskDate);
                btrlQm.addCondition("priFlag", BatEnums.PRI_FLAG_01.key); // 日终主任务(非零内评之前)
                btrlQm.addCondition("taskStatus", BatEnums.TASK_STATUS_000.key);
                logger.info("查询待运行的任务列表,请求参数为[{}]", JSON.toJSONString(btrlQm));
                java.util.List<BatTaskRun> pendingTaskList = batTaskRunService.selectPendingTask(btrlQm);
                logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATSTART_RUN_STEP.key, JobStepEnum.BATSTART_RUN_STEP.value, "查询待运行的任务列表数量为:[" + pendingTaskList.size() + "]");
                // 每次循环前，循环任务运行清单置空
                loopRunningTaskList = StringUtils.EMPTY;
                for (BatTaskRun pendingTask : pendingTaskList) {
                    // 将运行中的任务查到运行任务清单中
                    loopRunningTaskList += pendingTask.getTaskNo() + ",";
                    pendingTaskExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            // Job都是小写,除了bak之外
                            String pendingTaskNo = StringUtils.EMPTY;
                            if (pendingTask.getTaskNo().contains("BAK")) {
                                pendingTaskNo = pendingTask.getTaskNo().replaceAll("BAK", "bak");
                            } else {
                                pendingTaskNo = pendingTask.getTaskNo().toLowerCase();
                            }
                            String pendingTaskName = pendingTask.getTaskName();
                            try {
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]开始调用开始");
                                customJobLauncher.run((Job) applicationContext.getBean(pendingTaskNo + "Job"), createJobParams(taskDate, pendingTaskNo, false));
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用完成");
                            } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                                logger.error("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]");
                            }
                        }
                    });
                }
                logger.info("本次循环触发运行的任务列表为:[{}]", loopRunningTaskList);
                // 判断是否没有待发起的任务
                java.util.List<BatTaskRun> pendingTaskExistsList = batTaskRunService.selectPendingTask(btrlQm);
                if (CollectionUtils.isEmpty(pendingTaskExistsList)) {
                    logger.info("没有待发起的任务,退出循环");
                    break;
                }
                logger.info("本次循环循环结束,线程休眠[" + waitTime + "]秒开始");
                TimeUnit.SECONDS.sleep(waitTime);
                logger.info("本次循环循环结束,线程休眠[" + waitTime + "]秒结束");
            }
        } catch (Exception e) {
            logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
        }
        return 0;
    }


    /**
     * 并发启动任务级别为2的任务
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/parallelRunByPriFlag02")
    public @ResponseBody int parallelRunByPriFlag02() throws Exception {
        int result = 0;// 0成功，1失败
        // 查询批量运行表，获取任务日期，任务日期等于营业日期
        QueryModel queryModel = new QueryModel();
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(queryModel));
        List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunList));
        if (CollectionUtils.isEmpty(batControlRunList)) {
            // 调度运行管理(BAT_CONTROL_RUN)未配置
            logger.error(TradeLogConstants.BATCH_STEP_EXCEPTION_PREFIX_LOGGER, JobStepEnum.BATSTART_RUN_STEP.key, JobStepEnum.BATSTART_RUN_STEP.value, EchEnum.ECH080008.value);
            throw BizException.error(null, EchEnum.ECH080008.key, EchEnum.ECH080008.value);
        }
        String taskDate = batControlRunList.get(0).getOpenDay();//营业日期

        // 线程池初始化
        logger.info("线程池初始化开始");
        int waitTime = 60;//等待时间
        int corePoolSize = 100;//核心线程数
        int queueCapacity = 300;//等待队列长度
        int maxPoolSize = 100;//线程最大数量
        ThreadPoolTaskExecutor pendingTaskExecutor = new ThreadPoolTaskExecutor();
        pendingTaskExecutor.setCorePoolSize(corePoolSize);
        pendingTaskExecutor.setQueueCapacity(queueCapacity);
        pendingTaskExecutor.setMaxPoolSize(maxPoolSize);
        pendingTaskExecutor.setThreadNamePrefix("ParallelRunByPriFlag02");
        pendingTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        pendingTaskExecutor.initialize();
        logger.info("线程池初始化结束");
        try {
            // 每次循环任务运行清单
            String loopRunningTaskList = StringUtils.EMPTY;
            // 待运行任务标志，当所有任务处理完成，跳出循环
            boolean flag = true;
            while (flag) {
                // 查询待运行的任务列表
                QueryModel btrlQm = new QueryModel();
                btrlQm.addCondition("taskDate", taskDate);
                btrlQm.addCondition("priFlag", BatEnums.PRI_FLAG_02.key);// 日终主任务(非零内评之后)
                btrlQm.addCondition("taskStatus", BatEnums.TASK_STATUS_000.key);
                logger.info("查询待运行的任务列表,请求参数为[{}]", JSON.toJSONString(btrlQm));
                java.util.List<BatTaskRun> pendingTaskList = batTaskRunService.selectPendingTask(btrlQm);
                logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATSTART_RUN_STEP.key, JobStepEnum.BATSTART_RUN_STEP.value, "查询待运行的任务列表数量为:[" + pendingTaskList.size() + "]");
                // 每次循环前，循环任务运行清单置空
                loopRunningTaskList = StringUtils.EMPTY;
                for (BatTaskRun pendingTask : pendingTaskList) {
                    // 将运行中的任务查到运行任务清单中
                    loopRunningTaskList += pendingTask.getTaskNo() + ",";
                    pendingTaskExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            // Job都是小写,除了bak之外
                            String pendingTaskNo = StringUtils.EMPTY;
                            if (pendingTask.getTaskNo().contains("BAK")) {
                                pendingTaskNo = pendingTask.getTaskNo().replaceAll("BAK", "bak");
                            } else {
                                pendingTaskNo = pendingTask.getTaskNo().toLowerCase();
                            }
                            String pendingTaskName = pendingTask.getTaskName();
                            try {
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]开始调用开始");
                                customJobLauncher.run((Job) applicationContext.getBean(pendingTaskNo + "Job"), createJobParams(taskDate, pendingTaskNo, false));
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用完成");
                            } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                                logger.error("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]");
                            }
                        }
                    });
                }
                logger.info("本次循环触发运行的任务列表为:[{}]", loopRunningTaskList);
                // 判断是否没有待发起的任务
                java.util.List<BatTaskRun> pendingTaskExistsList = batTaskRunService.selectPendingTask(btrlQm);
                if (CollectionUtils.isEmpty(pendingTaskExistsList)) {
                    logger.info("没有待发起的任务,退出循环");
                    break;
                }
                logger.info("本次循环循环结束,线程休眠[" + waitTime + "]秒开始");
                TimeUnit.SECONDS.sleep(waitTime);
                logger.info("本次循环循环结束,线程休眠[" + waitTime + "]秒结束");
            }
        } catch (Exception e) {
            logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
        }
        return 0;
    }

    /**
     * 生成 JobParameter
     *
     * @param openDay 跑批日期
     * @param restart 是否需要重跑 需要则加时间戳
     * @return
     */
    private static JobParameters createJobParams(String openDay, String taskNo, boolean... restart) {
        JobParametersBuilder builder = new JobParametersBuilder().addString("openDay", openDay).addString("taskNo", taskNo);
        for (boolean arg : restart) {
            if (arg) {
                builder.addString("date", DateUtils.getCurrTimestamp().toString());
            }
        }
        return builder.toJobParameters();
    }
}
