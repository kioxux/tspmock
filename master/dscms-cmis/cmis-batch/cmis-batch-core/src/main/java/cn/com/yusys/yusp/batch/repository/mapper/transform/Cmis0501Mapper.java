package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0501</br>
 * 任务名称：加工任务-业务处理-更新用户管理表  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0501Mapper {

    /**
     * 更新系统用户表[离岗时,更新用户表电话、邮件、生日、状态、姓名、证件号码]
     *
     * @param openDay
     * @return
     */
    int truncateForAdminSmUse(@Param("openDay") String openDay);


    /**
     * 更新系统用户表[离岗时,更新用户表电话、邮件、生日、状态、姓名、证件号码]
     *
     * @param openDay
     * @return
     */
    int insertAdminSmUser01(@Param("openDay") String openDay);

    /**
     * 更新系统用户表[在岗时  更新用户表电话、邮件、生日、状态、姓名、证件号码]
     *
     * @param openDay
     * @return
     */
    int updateAdminSmUser02(@Param("openDay") String openDay);
}
