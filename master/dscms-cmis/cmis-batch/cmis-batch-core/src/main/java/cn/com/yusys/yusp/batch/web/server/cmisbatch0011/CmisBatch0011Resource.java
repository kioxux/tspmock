package cn.com.yusys.yusp.batch.web.server.cmisbatch0011;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.Cmisbatch0011ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.Cmisbatch0011RespDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0011.CmisBatch0011Service;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:提供日终调度平台查询批量任务状态
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CMISBATCH0011:提供日终调度平台查询批量任务状态")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0011Resource.class);

    @Autowired
    private CmisBatch0011Service cmisBatch0011Service;

    /**
     * 交易码：cmisbatch0011
     * 交易描述：提供日终调度平台查询批量任务状态
     *
     * @param cmisbatch0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提供日终调度平台查询批量任务状态")
    @PostMapping("/cmisbatch0011")
    protected @ResponseBody
    ResultDto<Cmisbatch0011RespDto> cmisbatch0011(@Validated @RequestBody Cmisbatch0011ReqDto cmisbatch0011ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, JSON.toJSONString(cmisbatch0011ReqDto));
        Cmisbatch0011RespDto cmisbatch0011RespDto = new Cmisbatch0011RespDto();// 响应Dto:提供日终调度平台查询批量任务状态
        ResultDto<Cmisbatch0011RespDto> cmisbatch0011ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0011ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, JSON.toJSONString(cmisbatch0011ReqDto));
            cmisbatch0011RespDto = cmisBatch0011Service.cmisBatch0011(cmisbatch0011ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, JSON.toJSONString(cmisbatch0011RespDto));
            // 封装cmisbatch0011ResultDto中正确的返回码和返回信息
            cmisbatch0011ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0011ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, e.getMessage());
            // 封装cmisbatch0011ResultDto中异常返回码和返回信息
            cmisbatch0011ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0011ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0011RespDto到cmisbatch0011ResultDto中
        cmisbatch0011ResultDto.setData(cmisbatch0011RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, JSON.toJSONString(cmisbatch0011ResultDto));
        return cmisbatch0011ResultDto;
    }
}
