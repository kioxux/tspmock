/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.domain.job;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobExecutionParams
 * @类描述: batch_job_execution_params数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "batch_job_execution_params")
public class BatchJobExecutionParams extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 作业执行器ID **/
	@Column(name = "JOB_EXECUTION_ID", unique = false, nullable = false, length = 19)
	private Long jobExecutionId;
	
	/** 存储值类型的字符串表示形式 **/
	@Column(name = "TYPE_CD", unique = false, nullable = false, length = 6)
	private String typeCd;
	
	/** 参数键 **/
	@Column(name = "KEY_NAME", unique = false, nullable = false, length = 100)
	private String keyName;
	
	/** 字符串类型参数值 **/
	@Column(name = "STRING_VAL", unique = false, nullable = true, length = 250)
	private String stringVal;
	
	/** 日期类型参数值 **/
	@Column(name = "DATE_VAL", unique = false, nullable = true, length = 19)
	private java.util.Date dateVal;
	
	/** 长类型参数值 **/
	@Column(name = "LONG_VAL", unique = false, nullable = true, length = 19)
	private Long longVal;
	
	/** 双倍类型参数值 **/
	@Column(name = "DOUBLE_VAL", unique = false, nullable = true, length = 22)
	private Double doubleVal;
	
	/** 指示参数是否有助于相关身份的标志JobInstance **/
	@Column(name = "IDENTIFYING", unique = false, nullable = false, length = 1)
	private String identifying;
	
	
	/**
	 * @param jobExecutionId
	 */
	public void setJobExecutionId(Long jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}
	
    /**
     * @return jobExecutionId
     */
	public Long getJobExecutionId() {
		return this.jobExecutionId;
	}
	
	/**
	 * @param typeCd
	 */
	public void setTypeCd(String typeCd) {
		this.typeCd = typeCd;
	}
	
    /**
     * @return typeCd
     */
	public String getTypeCd() {
		return this.typeCd;
	}
	
	/**
	 * @param keyName
	 */
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	
    /**
     * @return keyName
     */
	public String getKeyName() {
		return this.keyName;
	}
	
	/**
	 * @param stringVal
	 */
	public void setStringVal(String stringVal) {
		this.stringVal = stringVal;
	}
	
    /**
     * @return stringVal
     */
	public String getStringVal() {
		return this.stringVal;
	}
	
	/**
	 * @param dateVal
	 */
	public void setDateVal(java.util.Date dateVal) {
		this.dateVal = dateVal;
	}
	
    /**
     * @return dateVal
     */
	public java.util.Date getDateVal() {
		return this.dateVal;
	}
	
	/**
	 * @param longVal
	 */
	public void setLongVal(Long longVal) {
		this.longVal = longVal;
	}
	
    /**
     * @return longVal
     */
	public Long getLongVal() {
		return this.longVal;
	}
	
	/**
	 * @param doubleVal
	 */
	public void setDoubleVal(Double doubleVal) {
		this.doubleVal = doubleVal;
	}
	
    /**
     * @return doubleVal
     */
	public Double getDoubleVal() {
		return this.doubleVal;
	}
	
	/**
	 * @param identifying
	 */
	public void setIdentifying(String identifying) {
		this.identifying = identifying;
	}
	
    /**
     * @return identifying
     */
	public String getIdentifying() {
		return this.identifying;
	}


}