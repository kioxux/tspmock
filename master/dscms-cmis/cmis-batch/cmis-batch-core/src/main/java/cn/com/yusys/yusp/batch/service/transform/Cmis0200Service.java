package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0200Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0200</br>
 * 任务名称：加工任务-额度处理-批前台账比对和将额度相关表备份到额度相关临时表</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0200Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0200Service.class);
    @Autowired
    private Cmis0200Mapper cmis0200Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 清空分项占用关系信息加工表和插入分项占用关系信息加工表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpDealLmtContRel(String openDay) {
        logger.info("清空分项占用关系信息加工表开始");
        // cmis0200Mapper.deleteTmpDealLmtContRel();// cmis_lmt.tmp_deal_lmt_cont_rel
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_deal_lmt_cont_rel");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空分项占用关系信息加工表结束");

        logger.info("插入分项占用关系信息加工表开始，请求参数:[{}]", openDay);
        int insertTmpDealLmtContRel = cmis0200Mapper.insertTmpDealLmtContRel(openDay);
        logger.info("插入分项占用关系信息加工表结束，返回参数:[{}]", insertTmpDealLmtContRel);
    }

    /**
     * 清空合同占用关系信息加工表和插入合同占用关系信息加工表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpDealContAccRel(String openDay) {
        logger.info("清空合同占用关系信息加工表开始");
        // cmis0200Mapper.deleteTmpDealContAccRel();// cmis_lmt.tmp_deal_cont_acc_rel
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_deal_cont_acc_rel");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空合同占用关系信息加工表结束");

        logger.info("插入合同占用关系信息加工表开始，请求参数:[{}]", openDay);
        int insertTmpDealContAccRel = cmis0200Mapper.insertTmpDealContAccRel(openDay);
        logger.info("插入合同占用关系信息加工表结束，返回参数:[{}]", insertTmpDealContAccRel);
    }


    /**
     * 清空批复额度分项基础信息加工表和插入批复额度分项基础信息加工表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpDealLmtDetails(String openDay) {
        logger.info("清空批复额度分项基础信息加工表开始");
        // cmis0200Mapper.deleteTmpDealLmtDetails();// cmis_lmt.tmp_deal_lmt_details
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_deal_lmt_details");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空批复额度分项基础信息加工表结束");

        logger.info("插入批复额度分项基础信息加工表开始，请求参数:[{}]", openDay);
        int insertTmpDealLmtDetails = cmis0200Mapper.insertTmpDealLmtDetails(openDay);
        logger.info("插入批复额度分项基础信息加工表结束，返回参数:[{}]", insertTmpDealLmtDetails);
    }

    /**
     * 清空白名单额度信息表加工表和插入白名单额度信息表加工表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpDealLmtWhiteInfo(String openDay) {
        logger.info("清空白名单额度信息表加工表开始");
        // cmis0200Mapper.deleteTmpDealLmtWhiteInfo();// cmis_lmt.tmp_deal_lmt_white_info
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_deal_lmt_white_info");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空白名单额度信息表加工表结束");

        logger.info("插入白名单额度信息表加工表开始，请求参数:[{}]", openDay);
        int insertTmpDealLmtWhiteInfo = cmis0200Mapper.insertTmpDealLmtWhiteInfo(openDay);
        logger.info("插入白名单额度信息表加工表结束，返回参数:[{}]", insertTmpDealLmtWhiteInfo);
    }

    /**
     * 清空合作方授信分项信息加工表和插入合作方授信分项信息加工表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpDealApprCoopSubInfo(String openDay) {
        logger.info("清空合作方授信分项信息加工表开始");
        // cmis0200Mapper.deleteTmpDealApprCoopSubInfo();// cmis_lmt.tmp_deal_appr_coop_sub_info
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_deal_appr_coop_sub_info");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空合作方授信分项信息加工表结束");

        logger.info("插入白名单额度信息表加工表开始，请求参数:[{}]", openDay);
        int insertTmpDealApprCoopSubInfo = cmis0200Mapper.insertTmpDealApprCoopSubInfo(openDay);
        logger.info("插入白名单额度信息表加工表结束，返回参数:[{}]", insertTmpDealApprCoopSubInfo);
    }

    /**
     * 清空余额比对差异台账和插入贷款台账余额比对表生成
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpBalanceCompareAccDk(String openDay) {
        //   logger.info("清空临时表-贷款台账表开始");
        //   cmis0200Mapper.truncateTmpAccLoan();
        //   logger.info("清空临时表-贷款台账表结束");

        // 重建相关表
        logger.info("重建临时表-贷款台账表[TMP_ACC_LOAN]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz", "TMP_ACC_LOAN");
        logger.info("重建加工占用授信表[TMP_ACC_LOAN]结束");

        logger.info("插入临时表-贷款台账表开始，请求参数:[{}]", openDay);
        int insertTmpAccLoan = cmis0200Mapper.insertTmpAccLoan(openDay);
        logger.info("插入临时表-贷款台账表结束，返回参数:[{}]", insertTmpAccLoan);


        // 重建相关表
        logger.info("重建临时表-贷款台账表[tmp_d_acc_loan_0200]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz", "tmp_d_acc_loan_0200");
        logger.info("重建临时表-贷款台账表[tmp_d_acc_loan_0200]结束");

        //   logger.info("删除备份日表_贷款台账信息表0200开始");
        //   cmis0200Mapper.deleteTmpDaccLoan0200();
        //   logger.info("删除备份日表_贷款台账信息表0200结束");

        logger.info("插入插入备份日表_贷款台账信息表0200开始，请求参数:[{}]", openDay);
        int insertTmpDaccLoan0200 = cmis0200Mapper.insertTmpDaccLoan0200(openDay);
        logger.info("插入插入备份日表_贷款台账信息表0200结束，返回参数:[{}]", insertTmpDaccLoan0200);

        logger.info("清空余额比对差异台账开始");
        cmis0200Mapper.truncateTmpBalanceCompareAccDk();
        logger.info("清空余额比对差异台账结束");

        logger.info("插入贷款台账余额比对表生成之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "ctr_loan_cont");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_loan_0200");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_loan_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入贷款台账余额比对表生成之前分析相关的表结束");

        logger.info("插入贷款台账余额比对表生成开始，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccDk = cmis0200Mapper.insertTmpBalanceCompareAccDk(openDay);
        logger.info("插入贷款台账余额比对表生成结束，返回参数:[{}]", insertTmpBalanceCompareAccDk);

        logger.info("插入贷款台账余额比对表（资产池协议）生成之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "tmp_acc_loan");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_aspl_details");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_loan_0200");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_aspl_details");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入贷款台账余额比对表（资产池协议）生成之前分析相关的表结束");

        logger.info("插入贷款台账余额比对表生成开始（资产池协议），请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccDk2 = cmis0200Mapper.insertTmpBalanceCompareAccDk2(openDay);
        logger.info("插入贷款台账余额比对表生成结束（资产池协议），返回参数:[{}]", insertTmpBalanceCompareAccDk2);

        logger.info("插入贷款台账余额比对表生成开始（最高额授信协议下），请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccDk3 = cmis0200Mapper.insertTmpBalanceCompareAccDk3(openDay);
        logger.info("插入贷款台账余额比对表生成结束（最高额授信协议下），返回参数:[{}]", insertTmpBalanceCompareAccDk3);
    }

    /**
     * 清空余额比对差异台账和插入垫款台账余额比对表生成
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpBalanceCompareAccWtdk(String openDay) {

        logger.info("清空临时表-贷款台账表开始");
        cmis0200Mapper.truncateTmpBalanceCompareAccYcht();
        logger.info("清空临时表-贷款台账表结束");

        logger.info("插入临时表-垫款台账余额比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "tmp_acc_loan");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_accp_cont");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_loan_0200");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_accp_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入临时表-垫款台账余额比对表之前分析相关的表结束");

        logger.info("插入临时表-垫款台账余额比对表生成，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccYcht = cmis0200Mapper.insertTmpBalanceCompareAccYcht(openDay);
        logger.info("插入临时表-垫款台账余额比对表生成，返回参数:[{}]", insertTmpBalanceCompareAccYcht);

        logger.info("清空临时表-贷款台账表开始");
        cmis0200Mapper.truncateTmpBalanceCompareAccWtdk();
        logger.info("清空临时表-贷款台账表结束");

        logger.info("插入临时表-垫款台账余额比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "acc_entrust_loan");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_entrust_loan_cont");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_entrust_loan");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_entrust_loan_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入临时表-垫款台账余额比对表之前分析相关的表结束");

        logger.info("插入临时表-委托贷款台账余额比对表生成，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccWtdk = cmis0200Mapper.insertTmpBalanceCompareAccWtdk(openDay);
        logger.info("插入临时表-委托贷款台账余额比对表生成，返回参数:[{}]", insertTmpBalanceCompareAccWtdk);

    }

    /**
     * 清空余额比对差异台账和插入银承台账余额比对表生成
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpBalanceCompareAccYc(String openDay) {
        logger.info("清空余额比对差异台账开始");
        cmis0200Mapper.truncateTmpBalanceCompareAccYc();
        logger.info("清空余额比对差异台账结束");

        logger.info("插入银承台账余额比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "tmp_acc_accp");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_accp_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_accp_0200");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_accp_cont");
        logger.info("插入银承台账余额比对表之前分析相关的表结束");

        logger.info("插入银承台账余额比对表生成开始，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccYc = cmis0200Mapper.insertTmpBalanceCompareAccYc(openDay);
        logger.info("插入银承台账余额比对表生成结束，返回参数:[{}]", insertTmpBalanceCompareAccYc);

        logger.info("插入银承台账余额比对表（资产池协议）之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "tmp_acc_accp");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_aspl_details");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_accp_0200");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_aspl_details");
        logger.info("插入银承台账余额比对表（资产池协议）之前分析相关的表结束");

        logger.info("插入银承台账余额比对表生成开始（资产池协议），请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccYc2 = cmis0200Mapper.insertTmpBalanceCompareAccYc2(openDay);
        logger.info("插入银承台账余额比对表生成结束（资产池协议），返回参数:[{}]", insertTmpBalanceCompareAccYc2);

        logger.info("插入银承台账余额比对表（最高额授信协议）之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "tmp_acc_accp");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_high_amt_agr_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_accp_0200");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_high_amt_agr_cont");
        logger.info("插入银承台账余额比对表（最高额授信协议）之前分析相关的表结束");

        logger.info("插入银承台账余额比对表生成开始（最高额授信协议），请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccYc3 = cmis0200Mapper.insertTmpBalanceCompareAccYc3(openDay);
        logger.info("插入银承台账余额比对表生成结束（最高额授信协议），返回参数:[{}]", insertTmpBalanceCompareAccYc3);
    }

    /**
     * 清空余额比对差异台账和插入贴现台账余额比对表生成
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpBalanceCompareAccTx(String openDay) {
        logger.info("清空余额比对差异台账开始");
        cmis0200Mapper.truncateTmpBalanceCompareAccTx();
        logger.info("清空余额比对差异台账结束");

        logger.info("插入贴现台账余额比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "tmp_acc_accp");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_high_amt_agr_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_accp_0200");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_high_amt_agr_cont");
        logger.info("插入贴现台账余额比对表之前分析相关的表结束");

        logger.info("插入贴现台账余额比对表生成开始，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccTx = cmis0200Mapper.insertTmpBalanceCompareAccTx(openDay);
        logger.info("插入贴现台账余额比对表生成结束，返回参数:[{}]", insertTmpBalanceCompareAccTx);
    }

    /**
     * 清空余额比对差异台账和插入信用证台账余额比对表生成
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpBalanceCompareAccXyz(String openDay) {

        logger.info("清空余额比对差异台账开始");
        cmis0200Mapper.truncateTmpBalanceCompareAccXyz();
        logger.info("清空余额比对差异台账结束");

        logger.info("插入信用证台账余额比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "tmp_acc_tf_loc");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_tf_loc_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_tf_loc");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_tf_loc_cont");
        logger.info("插入信用证台账余额比对表之前分析相关的表结束");

        logger.info("插入信用证台账余额比对表生成开始，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccXyz = cmis0200Mapper.insertTmpBalanceCompareAccXyz(openDay);
        logger.info("插入信用证台账余额比对表生成结束，返回参数:[{}]", insertTmpBalanceCompareAccXyz);

        logger.info("插入信用证台账余额比对表（最高额授信协议）之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "tmp_acc_tf_loc");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_high_amt_agr_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_tf_loc");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_high_amt_agr_cont");
        logger.info("插入信用证台账余额比对表（最高额授信协议）之前分析相关的表结束");

        logger.info("插入信用证台账余额比对表生成开始（最高额授信协议），请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccXyz2 = cmis0200Mapper.insertTmpBalanceCompareAccXyz2(openDay);
        logger.info("插入信用证台账余额比对表生成结束（最高额授信协议），返回参数:[{}]", insertTmpBalanceCompareAccXyz2);
    }

    /**
     * 清空余额比对差异台账和插入保函台账余额比对表生成
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpBalanceCompareAccBh(String openDay) {
        logger.info("清空余额比对差异台账开始");
        cmis0200Mapper.truncateTmpBalanceCompareAccBh();
        logger.info("清空余额比对差异台账结束");

        logger.info("插入保函台账余额比对表（最高额授信协议）之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "tmp_acc_cvrs");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_cvrg_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_cvrs");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_cvrg_cont");
        logger.info("插入保函台账余额比对表（最高额授信协议）之前分析相关的表结束");

        logger.info("插入保函台账余额比对表生成开始，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccBh = cmis0200Mapper.insertTmpBalanceCompareAccBh(openDay);
        logger.info("插入保函台账余额比对表生成结束，返回参数:[{}]", insertTmpBalanceCompareAccBh);

        logger.info("插入保函台账余额比对表（最高额授信协议）之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "tmp_acc_cvrs");
        tableUtilsService.analyzeTable("cmis_biz", "ctr_high_amt_agr_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_acc_cvrs");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_high_amt_agr_cont");
        logger.info("插入保函台账余额比对表（最高额授信协议）之前分析相关的表结束");

        logger.info("插入保函台账余额比对表生成开始（最高额授信协议），请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccBh2 = cmis0200Mapper.insertTmpBalanceCompareAccBh2(openDay);
        logger.info("插入保函台账余额比对表生成结束（最高额授信协议），返回参数:[{}]", insertTmpBalanceCompareAccBh2);
    }

    /**
     * 清空余额比对差异台账和插入代开保函余额比对表生成
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpBalanceCompareAccDkbh(String openDay) {
        logger.info("清空余额比对差异台账开始");
        cmis0200Mapper.truncateTmpBalanceCompareAccDkBh();
        logger.info("清空余额比对差异台账结束");

        logger.info("插入代开保函余额比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_batch", "tmp_gjp_lmt_cvrg_rzkz");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_batch", "tmp_d_lmt_cvrg_rzkz");
        logger.info("插入代开保函余额比对表之前分析相关的表结束");

        logger.info("插入代开保函余额比对表生成开始，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccDkbh = cmis0200Mapper.insertTmpBalanceCompareAccDkbh(openDay);
        logger.info("插入代开保函余额比对表生成结束，返回参数:[{}]", insertTmpBalanceCompareAccDkbh);
    }

    /**
     * 清空余额比对差异台账和插入代开信用证余额比对表生成
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpBalanceCompareAccDkxyz(String openDay) {
        logger.info("清空余额比对差异台账开始");
        cmis0200Mapper.truncateTmpBalanceCompareAccDkxyz();
        logger.info("清空余额比对差异台账结束");

        logger.info("插入代开信用证余额比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_batch", "tmp_gjp_lmt_peer_rzkz");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_batch", "tmp_d_lmt_peer_rzkz");
        logger.info("插入代开信用证余额比对表之前分析相关的表结束");

        logger.info("插入代开信用证余额比对表生成开始，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccDkxyz = cmis0200Mapper.insertTmpBalanceCompareAccDkxyz(openDay);
        logger.info("插入代开信用证余额比对表生成结束，返回参数:[{}]", insertTmpBalanceCompareAccDkxyz);
    }

    /**
     * 清空余额比对差异台账和插入福费廷余额比对表生成
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200DeleteInsertTmpBalanceCompareAccFftz(String openDay) {
        logger.info("清空余额比对差异台账开始");
        cmis0200Mapper.truncateTmpBalanceCompareAccFft();
        logger.info("清空余额比对差异台账结束");

        logger.info("插入福费廷余额比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_batch", "tmp_gjp_lmt_fft_rzkz");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        tableUtilsService.analyzeTable("cmis_batch", "tmp_d_lmt_peer_rzkz");
        logger.info("插入福费廷余额比对表之前分析相关的表结束");

        logger.info("插入福费廷余额比对表生成开始，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccFft = cmis0200Mapper.insertTmpBalanceCompareAccFft(openDay);
        logger.info("插入福费廷余额比对表生成结束，返回参数:[{}]", insertTmpBalanceCompareAccFft);
    }


    /**
     * 清空余额比对差异台账
     *
     * @param openDay
     */
    public void cmis0200TruncateTmpBalanceCompareAccDkhtdq(String openDay) {
        logger.info("清空余额比对差异台账  tmp_balance_compare_acc，请求参数:[{}]", openDay);
        cmis0200Mapper.truncateTmpBalanceCompareAccDkhtdq(openDay);
        logger.info("清空余额比对差异台账  tmp_balance_compare_acc，返回参数:[{}]");
    }

    /**
     * 贷款合同到期注销
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200InsertTmpBalanceCompareAccFhtdqzx(String openDay) {
        logger.info("插入贷款合同到期注销比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "ctr_loan_cont");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_loan_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入贷款合同到期注销比对表之前分析相关的表结束");

        logger.info("贷款合同到期注销开始，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccFhtdqzx = cmis0200Mapper.insertTmpBalanceCompareAccFhtdqzx(openDay);
        logger.info("贷款合同到期注销结束，返回参数:[{}]", insertTmpBalanceCompareAccFhtdqzx);
    }


    /**
     * 清空余额比对差异台账
     *
     * @param openDay
     */
    public void cmis0200TruncateTmpBalanceCompareAccWtdkhtdq(String openDay) {
        logger.info("清空余额比对差异台账 tmp_balance_compare_acc，请求参数:[{}]", openDay);
        cmis0200Mapper.truncateTmpBalanceCompareAccWtdkhtdq(openDay);
        logger.info("清空余额比对差异台账 tmp_balance_compare_acc，返回参数:[{}]");
    }

    /**
     * 委托贷款合同到期注销
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200InsertTmpBalanceCompareAccFhtdqzxB(String openDay) {
        logger.info("插入[委托贷款合同到期注销]比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "ctr_entrust_loan_cont");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_entrust_loan_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入[委托贷款合同到期注销]比对表之前分析相关的表结束");

        logger.info("委托贷款合同到期注销，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccFhtdqzxB = cmis0200Mapper.insertTmpBalanceCompareAccFhtdqzxB(openDay);
        logger.info("委托贷款合同到期注销，返回参数:[{}]", insertTmpBalanceCompareAccFhtdqzxB);
    }


    /**
     * 清空余额比对差异台账
     *
     * @param openDay
     */
    public void cmis0200TruncateTmpBalanceCompareAccYchtdq(String openDay) {
        logger.info("  tmp_balance_compare_acc，请求参数:[{}]", openDay);
        cmis0200Mapper.truncateTmpBalanceCompareAccYchtdq(openDay);
        logger.info("  tmp_balance_compare_acc，返回参数:[{}]");
    }

    /**
     * C
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200InsertTmpBalanceCompareAccFhtdqzxC(String openDay) {
        logger.info("插入[银承合同到期注销]比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "ctr_accp_cont");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_accp_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入[银承合同到期注销]比对表之前分析相关的表结束");

        logger.info("银承合同到期注销 插入tmp_balance_compare_acc，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccFhtdqzxC = cmis0200Mapper.insertTmpBalanceCompareAccFhtdqzxC(openDay);
        logger.info("银承合同到期注销 插入tmp_balance_compare_acc，返回参数:[{}]", insertTmpBalanceCompareAccFhtdqzxC);
    }


    /**
     * 清空余额比对差异台账
     *
     * @param openDay
     */
    public void cmis0200TruncateTmpBalanceCompareAccTxhtdq(String openDay) {
        logger.info("  tmp_balance_compare_acc，请求参数:[{}]", openDay);
        cmis0200Mapper.truncateTmpBalanceCompareAccTxhtdq(openDay);
        logger.info("  tmp_balance_compare_acc，返回参数:[{}]");
    }

    /**
     * D
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200InsertTmpBalanceCompareAccFhtdqzxD(String openDay) {
        logger.info("插入[贴现合同到期注销]比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "ctr_disc_cont");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_disc_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入[贴现合同到期注销]比对表之前分析相关的表结束");

        logger.info("贴现合同到期注销 插入tmp_balance_compare_acc D-，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccFhtdqzxD = cmis0200Mapper.insertTmpBalanceCompareAccFhtdqzxD(openDay);
        logger.info("贴现合同到期注销 插入tmp_balance_compare_acc D-，返回参数:[{}]", insertTmpBalanceCompareAccFhtdqzxD);
    }


    /**
     * 清空余额比对差异台账
     *
     * @param openDay
     */
    public void cmis0200TruncateTmpBalanceCompareAccXyzhtdq(String openDay) {
        logger.info("清空余额比对差异台账  tmp_balance_compare_acc，请求参数:[{}]", openDay);
        cmis0200Mapper.truncateTmpBalanceCompareAccXyzhtdq(openDay);
        logger.info("清空余额比对差异台账  tmp_balance_compare_acc，返回参数:[{}]");
    }

    /**
     * E
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200InsertTmpBalanceCompareAccFhtdqzxE(String openDay) {
        logger.info("插入[贴现合同到期注销]比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "ctr_tf_loc_cont");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_tf_loc_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入[贴现合同到期注销]比对表之前分析相关的表结束");

        logger.info("贴现合同到期注销 信用证合同到期注销 插入tmp_balance_compare_acc E，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccFhtdqzxE = cmis0200Mapper.insertTmpBalanceCompareAccFhtdqzxE(openDay);
        logger.info("贴现合同到期注销 信用证合同到期注销 插入tmp_balance_compare_acc E，返回参数:[{}]", insertTmpBalanceCompareAccFhtdqzxE);
    }


    /**
     * 清空余额比对差异台账
     *
     * @param openDay
     */
    public void cmis0200TruncateTmpBalanceCompareAccBhhtdq(String openDay) {
        logger.info("清空余额比对差异台账  tmp_balance_compare_acc，请求参数:[{}]", openDay);
        cmis0200Mapper.truncateTmpBalanceCompareAccBhhtdq(openDay);
        logger.info("清空余额比对差异台账  tmp_balance_compare_acc，返回参数:[{}]");
    }

    /**
     * 保函合同到期 注销 插入tmp_balance_compare_acc F
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200InsertTmpBalanceCompareAccFhtdqzxF(String openDay) {
        logger.info("插入[贴现合同到期注销 保函合同到期 注销 ]比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "ctr_cvrg_cont");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_cvrg_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入[贴现合同到期注销 保函合同到期 注销 ]比对表之前分析相关的表结束");

        logger.info("贴现合同到期注销 保函合同到期 注销 插入tmp_balance_compare_acc F，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccFhtdqzxF = cmis0200Mapper.insertTmpBalanceCompareAccFhtdqzxF(openDay);
        logger.info("贴现合同到期注销 保函合同到期 注销 插入tmp_balance_compare_acc F，返回参数:[{}]", insertTmpBalanceCompareAccFhtdqzxF);
    }


    /**
     * 清空余额比对差异台账
     *
     * @param openDay
     */
    public void cmis0200TruncateTmpBalanceCompareAccZgehtdq(String openDay) {
        logger.info("清空余额比对差异台账  tmp_balance_compare_acc，请求参数:[{}]", openDay);
        cmis0200Mapper.truncateTmpBalanceCompareAccZgehtdq(openDay);
        logger.info("清空余额比对差异台账  tmp_balance_compare_acc，返回参数:[{}]");
    }


    /**
     * 最高额授信协议到期注销 插入tmp_balance_compare_acc G
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200InsertTmpBalanceCompareAccFhtdqzxG(String openDay) {
        logger.info("插入[贴现合同到期注销 最高额授信协议到期注销]比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "ctr_high_amt_agr_cont");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_high_amt_agr_cont");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入[贴现合同到期注销 最高额授信协议到期注销]比对表之前分析相关的表结束");

        logger.info("贴现合同到期注销 最高额授信协议到期注销 插入tmp_balance_compare_acc G-，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccFhtdqzxG = cmis0200Mapper.insertTmpBalanceCompareAccFhtdqzxG(openDay);
        logger.info("贴现合同到期注销 最高额授信协议到期注销 插入tmp_balance_compare_acc G-，返回参数:[{}]", insertTmpBalanceCompareAccFhtdqzxG);
    }

    /**
     * 资产池协议到期注销 清空tmp_balance_compare_acc
     *
     * @param openDay
     */
    public void cmis0200DeleteTmpBalanceCompareAcc(String openDay) {
        logger.info("贴现合同到期注销 资产池协议到期注销 清空tmp_balance_compare_acc，请求参数:[{}]", openDay);
        cmis0200Mapper.deleteTmpBalanceCompareAcc(openDay);
        logger.info("贴现合同到期注销 资产池协议到期注销 清空tmp_balance_compare_acc，返回参数:[{}]");
    }


    /**
     * H
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0200InsertTmpBalanceCompareAccFhtdqzxH(String openDay) {
        logger.info("插入[贴现合同到期注销]比对表之前分析相关的表开始，请求参数:[{}]", openDay);
        tableUtilsService.analyzeTable("cmis_biz", "ctr_aspl_details");
        tableUtilsService.analyzeTable("cmis_biz", "tmp_d_ctr_aspl_details");
        tableUtilsService.analyzeTable("cmis_lmt", "lmt_cont_rel");
        logger.info("插入[贴现合同到期注销]比对表之前分析相关的表结束");

        logger.info("贴现合同到期注销 插入tmp_balance_compare_acc H，请求参数:[{}]", openDay);
        int insertTmpBalanceCompareAccFhtdqzxH = cmis0200Mapper.insertTmpBalanceCompareAccFhtdqzxH(openDay);
        logger.info("贴现合同到期注销 插入tmp_balance_compare_acc H，返回参数:[{}]", insertTmpBalanceCompareAccFhtdqzxH);
    }

}
