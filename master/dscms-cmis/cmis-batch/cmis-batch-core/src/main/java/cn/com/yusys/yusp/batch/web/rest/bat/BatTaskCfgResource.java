/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskCfg;
import cn.com.yusys.yusp.batch.service.bat.BatTaskCfgService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTaskCfgResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-15 20:15:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battaskcfg")
public class BatTaskCfgResource {
    @Autowired
    private BatTaskCfgService batTaskCfgService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTaskCfg>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTaskCfg> list = batTaskCfgService.selectAll(queryModel);
        return new ResultDto<List<BatTaskCfg>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTaskCfg>> index(QueryModel queryModel) {
        List<BatTaskCfg> list = batTaskCfgService.selectByModel(queryModel);
        return new ResultDto<List<BatTaskCfg>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatTaskCfg>> query(@RequestBody QueryModel queryModel) {
        List<BatTaskCfg> list = batTaskCfgService.selectByModel(queryModel);
        return new ResultDto<List<BatTaskCfg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{taskNo}")
    protected ResultDto<BatTaskCfg> show(@PathVariable("taskNo") String taskNo) {
        BatTaskCfg batTaskCfg = batTaskCfgService.selectByPrimaryKey(taskNo);
        return new ResultDto<BatTaskCfg>(batTaskCfg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTaskCfg> create(@RequestBody BatTaskCfg batTaskCfg) throws URISyntaxException {
        batTaskCfgService.insert(batTaskCfg);
        return new ResultDto<BatTaskCfg>(batTaskCfg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTaskCfg batTaskCfg) throws URISyntaxException {
        int result = batTaskCfgService.update(batTaskCfg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{taskNo}")
    protected ResultDto<Integer> delete(@PathVariable("taskNo") String taskNo) {
        int result = batTaskCfgService.deleteByPrimaryKey(taskNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batTaskCfgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
