/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.biz;

import cn.com.yusys.yusp.batch.domain.biz.BizZsnewBasic;
import cn.com.yusys.yusp.batch.repository.mapper.biz.BizZsnewBasicMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.BASIC;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BizZsnewBasicService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BizZsnewBasicService {
    private static final Logger logger = LoggerFactory.getLogger(BizZsnewBasicService.class);
    @Autowired
    private BizZsnewBasicMapper bizZsnewBasicMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BizZsnewBasic selectByPrimaryKey(String appNo, String seqNum) {
        return bizZsnewBasicMapper.selectByPrimaryKey(appNo, seqNum);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BizZsnewBasic> selectAll(QueryModel model) {
        List<BizZsnewBasic> records = (List<BizZsnewBasic>) bizZsnewBasicMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BizZsnewBasic> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BizZsnewBasic> list = bizZsnewBasicMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BizZsnewBasic record) {
        return bizZsnewBasicMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BizZsnewBasic record) {
        return bizZsnewBasicMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BizZsnewBasic record) {
        return bizZsnewBasicMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BizZsnewBasic record) {
        return bizZsnewBasicMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String appNo, String seqNum) {
        return bizZsnewBasicMapper.deleteByPrimaryKey(appNo, seqNum);
    }

    /**
     * 根据查询结果来组装[企业照面信息]
     *
     * @param basic
     * @param corpMap
     * @return
     */
    public BizZsnewBasic buildByBasic(BASIC basic, Map<String, String> corpMap) {
        logger.info("根据查询结果来组装[企业照面信息]开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        BizZsnewBasic bizZsnewBasic = new BizZsnewBasic();
        String autoSerno = corpMap.get("autoSerno");
        String cusId = corpMap.get("cusId");
        String cusName = corpMap.get("cusName");
        String certType = corpMap.get("certType");
        String certCode = corpMap.get("certCode");


        bizZsnewBasic.setAppNo(autoSerno);
        bizZsnewBasic.setPspSerno(autoSerno);
        bizZsnewBasic.setSeqNum(UUID.randomUUID().toString().substring(0, 24));
        bizZsnewBasic.setCreditcode(basic.getCREDITCODE());//统一信用代码
        bizZsnewBasic.setCandate(basic.getCANDATE());// 注销日期
        bizZsnewBasic.setAncheyear(basic.getANCHEYEAR());//最后年检年度
        bizZsnewBasic.setDom(basic.getDOM());//住址
        bizZsnewBasic.setEntstatus(basic.getENTSTATUS());//经营状态
        bizZsnewBasic.setEntname(basic.getENTNAME());//企业名称
        bizZsnewBasic.setApprdate(basic.getAPPRDATE());//核准日期
        bizZsnewBasic.setEnttypecode(basic.getENTTYPECODE());//企业(机构)类型编码
        bizZsnewBasic.setEntnameOld(basic.getENTNAME_OLD());//曾用名
        String abuitem = basic.getABUITEM();
        if (Objects.nonNull(basic.getABUITEM()) && basic.getABUITEM().length() > 400) {
            abuitem = basic.getABUITEM().substring(0, 399);
        }
        bizZsnewBasic.setAbuitem(abuitem);//许可经营项目
        bizZsnewBasic.setIndustryconame(basic.getINDUSTRYPHYNAME());//国际经济代码名称
        bizZsnewBasic.setRegno(basic.getREGNO());   //注册号
        bizZsnewBasic.setRegcapcur(basic.getREGCAPCUR());//注册资本币种
        bizZsnewBasic.setRegcap(basic.getREGCAP());// 注册资本（万元）
        bizZsnewBasic.setRegorg(basic.getREGORG());//登记机关
        bizZsnewBasic.setZsopscope(basic.getZSOPSCOPE());//经营业务范围
        bizZsnewBasic.setRevdate(basic.getREVDATE());//吊销日期
        bizZsnewBasic.setRegorgcode(basic.getREGORGCODE());//注册地址行政编号
        bizZsnewBasic.setFrname(basic.getFRNAME());//法定代表人/负责人/执行事务合伙人
        bizZsnewBasic.setEsdate(basic.getESDATE());//成立日期
        bizZsnewBasic.setEnttype(basic.getENTTYPE());//企业类型
        bizZsnewBasic.setOpfrom(basic.getOPFROM());//经营期限自
        bizZsnewBasic.setOrgcodes(basic.getORGCODES());//组织机构代码
        bizZsnewBasic.setOpto(basic.getOPTO());//经营期限至
        bizZsnewBasic.setCandate(basic.getCANDATE());//注销日期
        bizZsnewBasic.setInputTime(DateUtils.getCurrDateTimeStr());
        bizZsnewBasic.setLastUpdateDate(DateUtils.getCurrDateStr());
        bizZsnewBasic.setLastUpdateTime(DateUtils.getCurrDateTimeStr());
        logger.info("根据查询结果来组装[企业照面信息]结束,响应参数为:[{}]", JSON.toJSONString(bizZsnewBasic));
        return bizZsnewBasic;
    }
}
