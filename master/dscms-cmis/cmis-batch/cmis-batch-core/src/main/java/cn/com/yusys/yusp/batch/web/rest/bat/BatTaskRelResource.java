/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRel;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTaskRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-15 14:17:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battaskrel")
public class BatTaskRelResource {
    @Autowired
    private BatTaskRelService batTaskRelService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTaskRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTaskRel> list = batTaskRelService.selectAll(queryModel);
        return new ResultDto<List<BatTaskRel>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTaskRel>> index(QueryModel queryModel) {
        List<BatTaskRel> list = batTaskRelService.selectByModel(queryModel);
        return new ResultDto<List<BatTaskRel>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatTaskRel>> query(@RequestBody QueryModel queryModel) {
        List<BatTaskRel> list = batTaskRelService.selectByModel(queryModel);
        return new ResultDto<List<BatTaskRel>>(list);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTaskRel> create(@RequestBody BatTaskRel batTaskRel) throws URISyntaxException {
        batTaskRelService.insert(batTaskRel);
        return new ResultDto<BatTaskRel>(batTaskRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTaskRel batTaskRel) throws URISyntaxException {
        int result = batTaskRelService.update(batTaskRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String relNo, String relTaskNo) {
        int result = batTaskRelService.deleteByPrimaryKey(relNo, relTaskNo);
        return new ResultDto<Integer>(result);
    }

}
