/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.znsd;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSZnsdBcCusGbList
 * @类描述: bat_s_znsd_bc_cus_gb_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-28 14:24:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_znsd_bc_cus_gb_list")
public class BatSZnsdBcCusGbList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 客户名称  **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 20)
	private String cusName;
	
	/** 客户证件号 **/
	@Column(name = "CERT_NO", unique = false, nullable = true, length = 20)
	private String certNo;
	
	/** 客户经理编号 **/
	@Column(name = "CUS_MGR_NO", unique = false, nullable = true, length = 20)
	private String cusMgrNo;
	
	/** 客户经理名称 **/
	@Column(name = "CUS_MGR_NAME", unique = false, nullable = true, length = 20)
	private String cusMgrName;
	
	/** 申请金额 **/
	@Column(name = "APPLY_AMOUNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal applyAmount;
	
	/** 担保方式编码 **/
	@Column(name = "GUAR_WAY_CODE", unique = false, nullable = true, length = 10)
	private String guarWayCode;
	
	/** 担保方式名称 **/
	@Column(name = "GUAR_WAY_NAME", unique = false, nullable = true, length = 20)
	private String guarWayName;
	
	/** 业务类型 **/
	@Column(name = "BUS_TYPE", unique = false, nullable = true, length = 2)
	private String busType;
	
	/** 产品编码 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 20)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 50)
	private String prdName;
	
	/** 风险分类 **/
	@Column(name = "RISK_CLASSIFY", unique = false, nullable = true, length = 10)
	private String riskClassify;
	
	/** 原因 **/
	@Column(name = "REASON", unique = false, nullable = true, length = 4000)
	private String reason;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 2)
	private String status;
	
	/** 调查报告流水号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 20)
	private String surveySerno;
	
	/** 插入时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 20)
	private String createTime;
	
	/** 更新时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String updateTime;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}
	
    /**
     * @return certNo
     */
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param cusMgrNo
	 */
	public void setCusMgrNo(String cusMgrNo) {
		this.cusMgrNo = cusMgrNo;
	}
	
    /**
     * @return cusMgrNo
     */
	public String getCusMgrNo() {
		return this.cusMgrNo;
	}
	
	/**
	 * @param cusMgrName
	 */
	public void setCusMgrName(String cusMgrName) {
		this.cusMgrName = cusMgrName;
	}
	
    /**
     * @return cusMgrName
     */
	public String getCusMgrName() {
		return this.cusMgrName;
	}
	
	/**
	 * @param applyAmount
	 */
	public void setApplyAmount(java.math.BigDecimal applyAmount) {
		this.applyAmount = applyAmount;
	}
	
    /**
     * @return applyAmount
     */
	public java.math.BigDecimal getApplyAmount() {
		return this.applyAmount;
	}
	
	/**
	 * @param guarWayCode
	 */
	public void setGuarWayCode(String guarWayCode) {
		this.guarWayCode = guarWayCode;
	}
	
    /**
     * @return guarWayCode
     */
	public String getGuarWayCode() {
		return this.guarWayCode;
	}
	
	/**
	 * @param guarWayName
	 */
	public void setGuarWayName(String guarWayName) {
		this.guarWayName = guarWayName;
	}
	
    /**
     * @return guarWayName
     */
	public String getGuarWayName() {
		return this.guarWayName;
	}
	
	/**
	 * @param busType
	 */
	public void setBusType(String busType) {
		this.busType = busType;
	}
	
    /**
     * @return busType
     */
	public String getBusType() {
		return this.busType;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param riskClassify
	 */
	public void setRiskClassify(String riskClassify) {
		this.riskClassify = riskClassify;
	}
	
    /**
     * @return riskClassify
     */
	public String getRiskClassify() {
		return this.riskClassify;
	}
	
	/**
	 * @param reason
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	
    /**
     * @return reason
     */
	public String getReason() {
		return this.reason;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public String getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public String getUpdateTime() {
		return this.updateTime;
	}


}