package cn.com.yusys.yusp.batch.job.load.core;

import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.batch.util.FileUtils;
import cn.com.yusys.yusp.batch.util.ShellUtils;
import cn.com.yusys.yusp.constants.BatConstance;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import cn.com.yusys.yusp.batch.domain.bat.BatTaskCfg;
import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.domain.bat.BatTransferCfg;
import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKlnbDkjcsx;
import cn.com.yusys.yusp.batch.domain.load.core.BatTCoreKlnbDkjcsx;
import cn.com.yusys.yusp.batch.job.common.OdsCommonFileItemReader;
import cn.com.yusys.yusp.batch.job.common.OdsSCommonFileItemReader;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatSCoreKlnbDkjcsxMapper;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatTCoreKlnbDkjcsxMapper;
import cn.com.yusys.yusp.batch.service.bat.BatTaskCfgService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.bat.BatTransferCfgService;
import cn.com.yusys.yusp.batch.service.load.core.BatSCoreKlnbDkjcsxService;
import cn.com.yusys.yusp.batch.service.load.core.BatTCoreKlnbDkjcsxService;
import cn.com.yusys.yusp.batch.util.DataLoadUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import com.alibaba.fastjson.JSON;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CORE0006</br>
 * 任务名称：文件处理任务-核心系统-贷款账户基础表[core_klnb_dkjcsx]</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Core0006Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Core0006Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Value("${batch.task.throttle-limit:96}")
    public int throttleLimit;
    @Value("${batch.proxyHost:10.28.111.113}")
    public String proxyHost;
    @Value("${batch.proxyPort:15770}")
    public String proxyPort;
    @Value("${batch.user:dba}")
    public String user;
    @Value("${batch.password:123456}")
    public String password;
    @Autowired
    @Qualifier("batchTaskPoolExecutor")
    private ThreadPoolTaskExecutor batchTaskExecutor;
    @Autowired
    private BatTCoreKlnbDkjcsxService tService;//T表
    @Autowired
    private BatSCoreKlnbDkjcsxService sService;//S表
    @Autowired
    private BatTransferCfgService batTransferCfgService;//文件传输配置信息
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskCfgService batTaskCfgService;//任务配置信息
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息
    @Autowired
    private TableUtilsService tableUtilsService;

    @Bean
    public Job core0006Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_JOB.key, JobStepEnum.CORE0006_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job core0006Job = this.jobBuilderFactory.get(JobStepEnum.CORE0006_JOB.key)
                .start(core0006UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(core0006CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(core0006CheckOkFileStep(WILL_BE_INJECTED))//检查文件是否已经生成
                .next(core0006FetchFileStep(WILL_BE_INJECTED))//获取文件到本地目录
                // .next(core0006TruncateAndLoadTStep(WILL_BE_INJECTED))//清空落地表和将数据从文件插入落地表
                // .next(core0006LoadTStep())//将数据从文件插入落地表
                .next(core0006DeleteAndLoadSStep(WILL_BE_INJECTED))//删除S表当天数据和将数据从文件插入S表
                // .next(core0006LoadSStep())//将数据从文件插入S表
                // .next(core0006MergeT2SStep(WILL_BE_INJECTED))//将T表数据merge到S表
                .next(core0006UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return core0006Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step core0006UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_UPDATE_TASK010_STEP.key, JobStepEnum.CORE0006_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step core0006UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.CORE0006_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CORE0006_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CORE0006_UPDATE_TASK010_STEP.key, JobStepEnum.CORE0006_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return core0006UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step core0006CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_CHECK_REL_STEP.key, JobStepEnum.CORE0006_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step core0006CheckRelStep = this.stepBuilderFactory.get(JobStepEnum.CORE0006_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CORE0006_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CORE0006_CHECK_REL_STEP.key, JobStepEnum.CORE0006_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CORE0006_CHECK_REL_STEP.key, JobStepEnum.CORE0006_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return core0006CheckRelStep;
    }

    /**
     * 将T表数据merge到S表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step core0006MergeT2SStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_MERGE_T2S_STEP.key, JobStepEnum.CORE0006_MERGE_T2S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step core0006MergeT2SStep = this.stepBuilderFactory.get(JobStepEnum.CORE0006_MERGE_T2S_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    sService.mergeT2S(openDay);
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CORE0006_MERGE_T2S_STEP.key, JobStepEnum.CORE0006_MERGE_T2S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();

        return core0006MergeT2SStep;
    }

    /**
     * 将数据从文件插入S表
     *
     * @return
     */
    @Bean
    @JobScope
    public Step core0006LoadSStep() {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_LOAD_S_STEP.key, JobStepEnum.CORE0006_LOAD_S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        logger.info("最大使用线程池数量为[{}]", throttleLimit);
        Step core0006LoadSStep = this.stepBuilderFactory.get(JobStepEnum.CORE0006_LOAD_S_STEP.key)
                .<BatSCoreKlnbDkjcsx, BatSCoreKlnbDkjcsx>chunk(BatConstance.CHUNK_SIZE)
                .reader(batSCoreKlnbDkjcsxReader(WILL_BE_INJECTED))
                .writer(list -> {
                    // 获取线程批量Session
                    SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH);
                    // 获取Mapper
                    BatSCoreKlnbDkjcsxMapper mapper = sqlSession.getMapper(BatSCoreKlnbDkjcsxMapper.class);
                    list.forEach(sInfo -> {
                        mapper.insert(sInfo);
                    });
                    logger.info(TradeLogConstants.BATCH_STEP_RESULT_PREFIX_LOGGER, JobStepEnum.CORE0006_LOAD_S_STEP.key, JobStepEnum.CORE0006_LOAD_S_STEP.value, list.size());
                    // 推送更新
                    sqlSession.flushStatements();
                    // 关闭线程批量Session
                    sqlSession.close();
                    TimeUnit.MILLISECONDS.sleep(500);// sleep 500毫秒(0.5秒)
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CORE0006_LOAD_S_STEP.key, JobStepEnum.CORE0006_LOAD_S_STEP.value, "关闭线程批量Session后该线程休眠500毫秒");
                })
                .taskExecutor(batchTaskExecutor)
                .throttleLimit(throttleLimit)
                .build();
        return core0006LoadSStep;
    }


    /**
     * 删除S表当天数据和将数据从文件插入S表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step core0006DeleteAndLoadSStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step core0006DeleteAndLoadSStep = this.stepBuilderFactory.get(JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    int result = sService.deleteByOpenDay(openDay);
                    logger.info(TradeLogConstants.BATCH_STEP_RESULT_PREFIX_LOGGER, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.value, result);
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CORE0006_TASK.key);
                    String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
                    String dataFileName = batTTabName.concat(".txt");//数据文件名
                    String batSTabName = "bat_s_core_klnb_dkjcsx";//数据文件S表名
                    String logFileName = batSTabName.concat(".log");//日志文件名
                    logger.info("load_data中相关参数值为proxy_host is[{}], proxy_port is[{}], user is[{}], password is[{}]", proxyHost, proxyPort, user, password);
                    String filePathDate = DateUtils.formatDate(openDayDate, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd
                    // String cmd = "imp_tdsql.sh 10.28.111.113 15770 dba 123456 cmis_batch.bat_t_core_klnl_dkqgmx 20210702 bat_t_core_klnl_dkqgmx.txt  bat_t_core_klnl_dkqgmx.log";
                    String cmd = "imp_tdsql.sh".concat(" ")
                            .concat(proxyHost).concat(" ")// IP地址
                            .concat(proxyPort).concat(" ")// 端口
                            .concat(user).concat(" ") //用户名
                            .concat(password).concat(" ")//密码
                            .concat("cmis_batch.").concat(batSTabName).concat(" ")//导入的表名
                            .concat(filePathDate).concat(" ")//文件路径
                            .concat(dataFileName).concat(" ")// 文件名称
                            .concat(logFileName);// 日志名称
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.value, "待执行命令为:[" + cmd + "]");
                    ShellUtils.runShellByProcessBuilder(cmd);

                    // 检查转换后的文件中的数据条数和数据库中的数据条数是否相等
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.value, "检查转换后的文件中的数据条数和数据库中的数据条数是否相等开始");
                    String dbName = "cmis_batch";
                    String tableName = "bat_s_core_klnb_dkjcsx";
                    Integer countTableLines = tableUtilsService.countTableLines(dbName, tableName);
                    DataLoadUtils dataLoadUtils = new DataLoadUtils();
                    boolean flag = dataLoadUtils.checkFileLinesEqualsDb(batTaskRun, countTableLines);
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.value, "检查转换后的文件中的数据条数和数据库中的数据条数是否相等结束,返回标志为:[" + flag + "]");

                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.CORE0006_DELETE_AND_LOAD_S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();

        return core0006DeleteAndLoadSStep;
    }

    /**
     * 将数据从文件插入落地表
     *
     * @return
     */
    @Bean
    @JobScope
    public Step core0006LoadTStep() {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_LOAD_T_STEP.key, JobStepEnum.CORE0006_LOAD_T_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        logger.info("最大使用线程池数量为[{}]", throttleLimit);
        Step core0006LoadTStep = this.stepBuilderFactory.get(JobStepEnum.CORE0006_LOAD_T_STEP.key)
                .<BatTCoreKlnbDkjcsx, BatTCoreKlnbDkjcsx>chunk(BatConstance.CHUNK_SIZE)
                .reader(batTCoreKlnbDkjcsxReader(WILL_BE_INJECTED))
                .writer(list -> {
                    // 获取线程批量Session
                    SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH);
                    // 获取Mapper
                    BatTCoreKlnbDkjcsxMapper mapper = sqlSession.getMapper(BatTCoreKlnbDkjcsxMapper.class);
                    list.forEach(tInfo -> {
                        mapper.insert(tInfo);
                    });
                    logger.info(TradeLogConstants.BATCH_STEP_RESULT_PREFIX_LOGGER, JobStepEnum.CORE0006_LOAD_T_STEP.key, JobStepEnum.CORE0006_LOAD_T_STEP.value, list.size());
                    // 推送更新
                    sqlSession.flushStatements();
                    // 关闭线程批量Session
                    sqlSession.close();
                    TimeUnit.MILLISECONDS.sleep(500);// sleep 500毫秒(0.5秒)
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CORE0006_LOAD_T_STEP.key, JobStepEnum.CORE0006_LOAD_T_STEP.value, "关闭线程批量Session后该线程休眠500毫秒");
                })
                .taskExecutor(batchTaskExecutor)
                .throttleLimit(throttleLimit)
                .build();

        return core0006LoadTStep;
    }


    /**
     * 清空落地表和将数据从文件插入落地表
     *
     * @return
     */
    @Bean
    @JobScope
    public Step core0006TruncateAndLoadTStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_TRUNCATE_AND_LOAD_T_STEP.key, JobStepEnum.CORE0006_TRUNCATE_AND_LOAD_T_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step core0006TruncateAndLoadTStep = this.stepBuilderFactory.get(JobStepEnum.CORE0006_TRUNCATE_AND_LOAD_T_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    tService.truncateTTable();

                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CORE0006_TASK.key);
                    String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
                    String dataFileName = batTTabName.concat(".txt");//数据文件名
                    String logFileName = batTTabName.concat(".log");//日志文件名
                    logger.info("load_data中相关参数值为proxy_host is[{}], proxy_port is[{}], user is[{}], password is[{}]", proxyHost, proxyPort, user, password);
                    Date openDayDate = DateUtils.parseDateByDef(openDay);
                    String filePathDate = DateUtils.formatDate(openDayDate, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd
                    // String cmd = "imp_tdsql.sh 10.28.111.113 15770 dba 123456 cmis_batch.bat_t_core_klnl_dkqgmx 20210702 bat_t_core_klnl_dkqgmx.txt  bat_t_core_klnl_dkqgmx.log";
                    String cmd = "imp_tdsql.sh".concat(" ")
                            .concat(proxyHost).concat(" ")// IP地址
                            .concat(proxyPort).concat(" ")// 端口
                            .concat(user).concat(" ") //用户名
                            .concat(password).concat(" ")//密码
                            .concat("cmis_batch.").concat(batTTabName).concat(" ")//导入的表名
                            .concat(filePathDate).concat(" ")//文件路径
                            .concat(dataFileName).concat(" ")// 文件名称
                            .concat(logFileName);// 日志名称
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CORE0006_TRUNCATE_AND_LOAD_T_STEP.key, JobStepEnum.CORE0006_TRUNCATE_AND_LOAD_T_STEP.value, "待执行命令为:[" + cmd + "]");
                    ShellUtils.runShellByProcessBuilder(cmd);
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CORE0006_TRUNCATE_AND_LOAD_T_STEP.key, JobStepEnum.CORE0006_TRUNCATE_AND_LOAD_T_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return core0006TruncateAndLoadTStep;
    }

    /**
     * 获取文件到本地目录
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step core0006FetchFileStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_FETCH_FILE_STEP.key, JobStepEnum.CORE0006_FETCH_FILE_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step core0006FetchFileStep = this.stepBuilderFactory.get(JobStepEnum.CORE0006_FETCH_FILE_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CORE0006_TASK.key);
                    BatTransferCfg batTransferCfg = batTransferCfgService.selectByPrimaryKey(TaskEnum.CORE0006_TASK.key);
                    BatTaskCfg batTaskCfg = batTaskCfgService.selectByPrimaryKey(TaskEnum.CORE0006_TASK.key);
                    DataLoadUtils dataLoadUtils = new DataLoadUtils();
                    dataLoadUtils.downloadTaskFiles(batTransferCfg, batTaskRun);
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CORE0006_FETCH_FILE_STEP.key, JobStepEnum.CORE0006_FETCH_FILE_STEP.value, "删除3日前文件开始");
                    FileUtils.delete3DaysBeforeFiles(openDay, batTransferCfg, batTaskRun);
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CORE0006_FETCH_FILE_STEP.key, JobStepEnum.CORE0006_FETCH_FILE_STEP.value, "删除3日前文件结束");
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CORE0006_FETCH_FILE_STEP.key, JobStepEnum.CORE0006_FETCH_FILE_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();

        return core0006FetchFileStep;
    }

    /**
     * 检查文件是否已经生成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step core0006CheckOkFileStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_CHECK_OK_FILE_STEP.key, JobStepEnum.CORE0006_CHECK_OK_FILE_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step core0006CheckOkFileStep = this.stepBuilderFactory.get(JobStepEnum.CORE0006_CHECK_OK_FILE_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CORE0006_TASK.key);
                    BatTransferCfg batTransferCfg = batTransferCfgService.selectByPrimaryKey(TaskEnum.CORE0006_TASK.key);

                    DataLoadUtils dataLoadUtils = new DataLoadUtils();
                    //  检查远程目录信号文件是否存在
                    boolean remoteFileFlag = dataLoadUtils.checkOdsSign(batTransferCfg, batTaskRun);
                    if (remoteFileFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CORE0006_CHECK_OK_FILE_STEP.key, JobStepEnum.CORE0006_CHECK_OK_FILE_STEP.value, "检查文件未生成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CORE0006_CHECK_OK_FILE_STEP.key, JobStepEnum.CORE0006_CHECK_OK_FILE_STEP.value, "检查文件未生成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return core0006CheckOkFileStep;
    }

    /**
     * 将文件解析到数据库(S表)中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @StepScope
    public FlatFileItemReader<? extends BatSCoreKlnbDkjcsx> batSCoreKlnbDkjcsxReader(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_READER_S_STEP.key, JobStepEnum.CORE0006_READER_S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CORE0006_TASK.key);
        String delimiter = batTaskRun.getFileSplit();//文件分隔符
        String workDir = batTaskRun.getWorkDir();//工作目录
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String workFileName = batTTabName.concat(".txt");
        String filePath = workDir + workFileName;
        FlatFileItemReader sIitemReader = new OdsSCommonFileItemReader<>(BatSCoreKlnbDkjcsx.class, filePath, openDay, delimiter, BatConstance.ENCODING_UTF8);
        logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CORE0006_READER_S_STEP.key, JobStepEnum.CORE0006_READER_S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return sIitemReader;
    }

    /**
     * 将文件解析到数据库(T表)中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @StepScope
    public FlatFileItemReader<? extends BatTCoreKlnbDkjcsx> batTCoreKlnbDkjcsxReader(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_READER_T_STEP.key, JobStepEnum.CORE0006_READER_T_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CORE0006_TASK.key);
        String delimiter = batTaskRun.getFileSplit();//文件分隔符
        String workDir = batTaskRun.getWorkDir();//工作目录
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String workFileName = batTTabName.concat(".txt");
        String filePath = workDir + workFileName;
        FlatFileItemReader tItemReader = new OdsCommonFileItemReader<>(BatTCoreKlnbDkjcsx.class, filePath, openDay, delimiter, BatConstance.ENCODING_UTF8);
        logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CORE0006_READER_T_STEP.key, JobStepEnum.CORE0006_READER_T_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return tItemReader;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step core0006UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CORE0006_UPDATE_TASK100_STEP.key, JobStepEnum.CORE0006_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step core0006UpdateTask100Step = this.stepBuilderFactory.get(JobStepEnum.CORE0006_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CORE0006_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CORE0006_UPDATE_TASK100_STEP.key, JobStepEnum.CORE0006_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return core0006UpdateTask100Step;
    }
}
