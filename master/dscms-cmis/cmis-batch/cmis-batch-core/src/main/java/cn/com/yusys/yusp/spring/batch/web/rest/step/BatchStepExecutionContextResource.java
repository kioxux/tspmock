/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.web.rest.step;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.spring.batch.domain.step.BatchStepExecutionContext;
import cn.com.yusys.yusp.spring.batch.service.step.BatchStepExecutionContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchStepExecutionContextResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:02:36
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batchstepexecutioncontext")
public class BatchStepExecutionContextResource {
    @Autowired
    private BatchStepExecutionContextService batchStepExecutionContextService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatchStepExecutionContext>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatchStepExecutionContext> list = batchStepExecutionContextService.selectAll(queryModel);
        return new ResultDto<List<BatchStepExecutionContext>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatchStepExecutionContext>> index(QueryModel queryModel) {
        List<BatchStepExecutionContext> list = batchStepExecutionContextService.selectByModel(queryModel);
        return new ResultDto<List<BatchStepExecutionContext>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatchStepExecutionContext>> query(@RequestBody QueryModel queryModel) {
        List<BatchStepExecutionContext> list = batchStepExecutionContextService.selectByModel(queryModel);
        return new ResultDto<List<BatchStepExecutionContext>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{stepExecutionId}")
    protected ResultDto<BatchStepExecutionContext> show(@PathVariable("stepExecutionId") Long stepExecutionId) {
        BatchStepExecutionContext batchStepExecutionContext = batchStepExecutionContextService.selectByPrimaryKey(stepExecutionId);
        return new ResultDto<BatchStepExecutionContext>(batchStepExecutionContext);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatchStepExecutionContext> create(@RequestBody BatchStepExecutionContext batchStepExecutionContext) throws URISyntaxException {
        batchStepExecutionContextService.insert(batchStepExecutionContext);
        return new ResultDto<BatchStepExecutionContext>(batchStepExecutionContext);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatchStepExecutionContext batchStepExecutionContext) throws URISyntaxException {
        int result = batchStepExecutionContextService.update(batchStepExecutionContext);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{stepExecutionId}")
    protected ResultDto<Integer> delete(@PathVariable("stepExecutionId") Long stepExecutionId) {
        int result = batchStepExecutionContextService.deleteByPrimaryKey(stepExecutionId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batchStepExecutionContextService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
