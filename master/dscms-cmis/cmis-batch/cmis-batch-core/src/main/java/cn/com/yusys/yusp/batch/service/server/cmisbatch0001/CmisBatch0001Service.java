package cn.com.yusys.yusp.batch.service.server.cmisbatch0001;

import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001RespDto;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CmisBatch0001Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0001Service.class);

    @Autowired
    private BatControlRunService batControlRunService;

    /**
     * 交易码：cmisbatch0001
     * 交易描述：调度运行管理信息查詢
     *
     * @param cmisbatch0001ReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Cmisbatch0001RespDto cmisBatch0001(Cmisbatch0001ReqDto cmisbatch0001ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value);
        Cmisbatch0001RespDto cmisbatch0001RespDto = new Cmisbatch0001RespDto();// 响应Dto:调度运行管理信息查詢
        try {
            //请求字段
            String openDay = cmisbatch0001ReqDto.getOpenDay();//营业日期,T日
            BatControlRun batControlRun = batControlRunService.selectByPrimaryKey(openDay);
            BeanUtils.copyProperties(batControlRun, cmisbatch0001RespDto);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value);
        return cmisbatch0001RespDto;
    }
}
