/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.biz;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BizSxbzxr
 * @类描述: biz_sxbzxr数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "biz_sxbzxr")
public class BizSxbzxr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "APP_NO")
	private String appNo;
	
	/** 姓名 **/
	@Id
	@Column(name = "NAME")
	private String name;
	
	/** 0：查询自然人， 1：查询组织机构 **/
	@Id
	@Column(name = "TYPE")
	private String type;
	
	/** 失信被执行编号 **/
	@Id
	@Column(name = "RECORD_NUMBER")
	private String recordNumber;
	
	/** 贷后流水号 **/
	@Column(name = "PSP_SERNO", unique = false, nullable = true, length = 40)
	private String pspSerno;
	
	/** 性别 **/
	@Column(name = "XB", unique = false, nullable = true, length = 10)
	private String xb;
	
	/** 组织机构代码 **/
	@Column(name = "ZZJGDM", unique = false, nullable = true, length = 100)
	private String zzjgdm;
	
	/** 企业法人 **/
	@Column(name = "QYFR", unique = false, nullable = true, length = 50)
	private String qyfr;
	
	/** 执行法院 **/
	@Column(name = "ZXFY", unique = false, nullable = true, length = 100)
	private String zxfy;
	
	/** 省份 **/
	@Column(name = "SF", unique = false, nullable = true, length = 20)
	private String sf;
	
	/** 执行依据文号 **/
	@Column(name = "ZXYJWH", unique = false, nullable = true, length = 200)
	private String zxyjwh;
	
	/** 立案时间（日期） **/
	@Column(name = "LARQ", unique = false, nullable = true, length = 20)
	private String larq;
	
	/** 案号 **/
	@Column(name = "AH", unique = false, nullable = true, length = 100)
	private String ah;
	
	/** 出执行依据单位 **/
	@Column(name = "ZXYJDW", unique = false, nullable = true, length = 200)
	private String zxyjdw;
	
	/** 生效法律文书确定的义务 **/
	@Column(name = "YW", unique = false, nullable = true, length = 2000)
	private String yw;
	
	/** 被执行人的履行情况 **/
	@Column(name = "LXQK", unique = false, nullable = true, length = 200)
	private String lxqk;
	
	/** 失信被执行人行为具体情形 **/
	@Column(name = "XWQX", unique = false, nullable = true, length = 200)
	private String xwqx;
	
	/** 发布时间（日期） **/
	@Column(name = "FBRQ", unique = false, nullable = true, length = 20)
	private String fbrq;
	
	/** 外部数据接口请求欣方接口是否成功 **/
	@Column(name = "CODE", unique = false, nullable = true, length = 39)
	private String code;
	
	/** 状态成功码200 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 40)
	private String status;
	
	/** 返回码说明 **/
	@Column(name = "MSG", unique = false, nullable = true, length = 40)
	private String msg;
	
	/** 获取类型 **/
	@Column(name = "GET_TYPE", unique = false, nullable = true, length = 39)
	private String getType;
	
	/** 返回信息说明 **/
	@Column(name = "MESSAGE", unique = false, nullable = true, length = 100)
	private String message;
	
	/** 柜员号 **/
	@Column(name = "USERID", unique = false, nullable = true, length = 30)
	private String userid;
	
	/** 身份证号/组织机构代码 **/
	@Column(name = "ID", unique = false, nullable = true, length = 30)
	private String id;
	
	/** 响应码 **/
	@Column(name = "ERORCD", unique = false, nullable = true, length = 100)
	private String erorcd;
	
	/** 响应信息 **/
	@Column(name = "ERORTX", unique = false, nullable = true, length = 100)
	private String erortx;
	
	/** 录入日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "LAST_UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	
	/**
	 * @param appNo
	 */
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	
    /**
     * @return appNo
     */
	public String getAppNo() {
		return this.appNo;
	}
	
	/**
	 * @param pspSerno
	 */
	public void setPspSerno(String pspSerno) {
		this.pspSerno = pspSerno;
	}
	
    /**
     * @return pspSerno
     */
	public String getPspSerno() {
		return this.pspSerno;
	}
	
	/**
	 * @param xb
	 */
	public void setXb(String xb) {
		this.xb = xb;
	}
	
    /**
     * @return xb
     */
	public String getXb() {
		return this.xb;
	}
	
	/**
	 * @param zzjgdm
	 */
	public void setZzjgdm(String zzjgdm) {
		this.zzjgdm = zzjgdm;
	}
	
    /**
     * @return zzjgdm
     */
	public String getZzjgdm() {
		return this.zzjgdm;
	}
	
	/**
	 * @param qyfr
	 */
	public void setQyfr(String qyfr) {
		this.qyfr = qyfr;
	}
	
    /**
     * @return qyfr
     */
	public String getQyfr() {
		return this.qyfr;
	}
	
	/**
	 * @param zxfy
	 */
	public void setZxfy(String zxfy) {
		this.zxfy = zxfy;
	}
	
    /**
     * @return zxfy
     */
	public String getZxfy() {
		return this.zxfy;
	}
	
	/**
	 * @param sf
	 */
	public void setSf(String sf) {
		this.sf = sf;
	}
	
    /**
     * @return sf
     */
	public String getSf() {
		return this.sf;
	}
	
	/**
	 * @param zxyjwh
	 */
	public void setZxyjwh(String zxyjwh) {
		this.zxyjwh = zxyjwh;
	}
	
    /**
     * @return zxyjwh
     */
	public String getZxyjwh() {
		return this.zxyjwh;
	}
	
	/**
	 * @param larq
	 */
	public void setLarq(String larq) {
		this.larq = larq;
	}
	
    /**
     * @return larq
     */
	public String getLarq() {
		return this.larq;
	}
	
	/**
	 * @param ah
	 */
	public void setAh(String ah) {
		this.ah = ah;
	}
	
    /**
     * @return ah
     */
	public String getAh() {
		return this.ah;
	}
	
	/**
	 * @param zxyjdw
	 */
	public void setZxyjdw(String zxyjdw) {
		this.zxyjdw = zxyjdw;
	}
	
    /**
     * @return zxyjdw
     */
	public String getZxyjdw() {
		return this.zxyjdw;
	}
	
	/**
	 * @param yw
	 */
	public void setYw(String yw) {
		this.yw = yw;
	}
	
    /**
     * @return yw
     */
	public String getYw() {
		return this.yw;
	}
	
	/**
	 * @param lxqk
	 */
	public void setLxqk(String lxqk) {
		this.lxqk = lxqk;
	}
	
    /**
     * @return lxqk
     */
	public String getLxqk() {
		return this.lxqk;
	}
	
	/**
	 * @param xwqx
	 */
	public void setXwqx(String xwqx) {
		this.xwqx = xwqx;
	}
	
    /**
     * @return xwqx
     */
	public String getXwqx() {
		return this.xwqx;
	}
	
	/**
	 * @param fbrq
	 */
	public void setFbrq(String fbrq) {
		this.fbrq = fbrq;
	}
	
    /**
     * @return fbrq
     */
	public String getFbrq() {
		return this.fbrq;
	}
	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
    /**
     * @return name
     */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
    /**
     * @return code
     */
	public String getCode() {
		return this.code;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
    /**
     * @return msg
     */
	public String getMsg() {
		return this.msg;
	}
	
	/**
	 * @param getType
	 */
	public void setGetType(String getType) {
		this.getType = getType;
	}
	
    /**
     * @return getType
     */
	public String getGetType() {
		return this.getType;
	}
	
	/**
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
    /**
     * @return message
     */
	public String getMessage() {
		return this.message;
	}
	
	/**
	 * @param userid
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
    /**
     * @return userid
     */
	public String getUserid() {
		return this.userid;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
    /**
     * @return type
     */
	public String getType() {
		return this.type;
	}
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param recordNumber
	 */
	public void setRecordNumber(String recordNumber) {
		this.recordNumber = recordNumber;
	}
	
    /**
     * @return recordNumber
     */
	public String getRecordNumber() {
		return this.recordNumber;
	}
	
	/**
	 * @param erorcd
	 */
	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}
	
    /**
     * @return erorcd
     */
	public String getErorcd() {
		return this.erorcd;
	}
	
	/**
	 * @param erortx
	 */
	public void setErortx(String erortx) {
		this.erortx = erortx;
	}
	
    /**
     * @return erortx
     */
	public String getErortx() {
		return this.erortx;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}


}