/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.sms;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: SmsManageInfoHis
 * @类描述: sms_manage_info_his数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:42:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "sms_manage_info_his")
public class SmsManageInfoHis extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键，UUID **/
	@Id
	@Column(name = "SMS_ID")
	private String smsId;
	
	/** 任务日期 **/
	@Id
	@Column(name = "TASK_DATE")
	private String taskDate;
	
	/** 接收短信的手机号，多个号码用英文逗号(,)分割，最多不能超过100个号码。 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;
	
	/** 个人联系人组id,多个组用英文逗号(,)分割，平台将会取指定组下所有联系人作为接受号码发送。 **/
	@Column(name = "GROUP_ID", unique = false, nullable = true, length = 250)
	private String groupId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 还款日中年 **/
	@Column(name = "REPAY_DATE_YY", unique = false, nullable = true, length = 8)
	private String repayDateYy;
	
	/** 还款日中月 **/
	@Column(name = "REPAY_DATE_MM", unique = false, nullable = true, length = 8)
	private String repayDateMm;
	
	/** 还款日中日 **/
	@Column(name = "REPAY_DATE_DD", unique = false, nullable = true, length = 8)
	private String repayDateDd;
	
	/** 归还本息 **/
	@Column(name = "REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repayAmt;
	
	/** 营业日期中年 **/
	@Column(name = "CURRRENT_YEAR", unique = false, nullable = true, length = 8)
	private String currrentYear;
	
	/** 营业日期中月 **/
	@Column(name = "CURRRENT_MONTH", unique = false, nullable = true, length = 8)
	private String currrentMonth;
	
	/** 营业日期中日 **/
	@Column(name = "CURRENT_DAY", unique = false, nullable = true, length = 8)
	private String currentDay;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 贷款逾期金额 **/
	@Column(name = "OVER_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overTotalAmt;
	
	/** 短信内容。 **/
	@Column(name = "SMS_CONTENT", unique = false, nullable = true, length = 1000)
	private String smsContent;
	
	/** 下发扩展子码，可为空，不为空时必须是数字；手机端接收到的短信中，显示源号码的末几位，前提是网关要支持扩展码发送 **/
	@Column(name = "SUB_CODE", unique = false, nullable = true, length = 20)
	private String subCode;
	
	/** 发送账户，由平台管理员提供 **/
	@Column(name = "USER_ID", unique = false, nullable = true, length = 20)
	private String userId;
	
	/** 短信类型，由平台管理员提供 **/
	@Column(name = "SMS_TYPE", unique = false, nullable = true, length = 10)
	private String smsType;
	
	/** 记录入库时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 短信发送时间，如果时间大于当前时间，则该记录为定时短信，只有当该时间大于等于系统时间时，才会进行下发 **/
	@Column(name = "SEND_TIME", unique = false, nullable = true, length = 20)
	private String sendTime;
	
	/** 短信状态，0-待发送，1-已发送 **/
	@Column(name = "SMS_SEND_STATE", unique = false, nullable = true, length = 1)
	private String smsSendState;
	
	/** 短信平台返回的信息，r:000为成功，以p:开头的均为失败 **/
	@Column(name = "SUBSTAT", unique = false, nullable = true, length = 10)
	private String substat;
	
	/** 短信标识，0-实时，1-批量 **/
	@Column(name = "SMS_FLAG", unique = false, nullable = true, length = 1)
	private String smsFlag;
	
	/** 是否有效 **/
	@Column(name = "VALID_FLAG", unique = false, nullable = true, length = 1)
	private String validFlag;
	
	
	/**
	 * @param smsId
	 */
	public void setSmsId(String smsId) {
		this.smsId = smsId;
	}
	
    /**
     * @return smsId
     */
	public String getSmsId() {
		return this.smsId;
	}
	
	/**
	 * @param taskDate
	 */
	public void setTaskDate(String taskDate) {
		this.taskDate = taskDate;
	}
	
    /**
     * @return taskDate
     */
	public String getTaskDate() {
		return this.taskDate;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param groupId
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
    /**
     * @return groupId
     */
	public String getGroupId() {
		return this.groupId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param repayDateYy
	 */
	public void setRepayDateYy(String repayDateYy) {
		this.repayDateYy = repayDateYy;
	}
	
    /**
     * @return repayDateYy
     */
	public String getRepayDateYy() {
		return this.repayDateYy;
	}
	
	/**
	 * @param repayDateMm
	 */
	public void setRepayDateMm(String repayDateMm) {
		this.repayDateMm = repayDateMm;
	}
	
    /**
     * @return repayDateMm
     */
	public String getRepayDateMm() {
		return this.repayDateMm;
	}
	
	/**
	 * @param repayDateDd
	 */
	public void setRepayDateDd(String repayDateDd) {
		this.repayDateDd = repayDateDd;
	}
	
    /**
     * @return repayDateDd
     */
	public String getRepayDateDd() {
		return this.repayDateDd;
	}
	
	/**
	 * @param repayAmt
	 */
	public void setRepayAmt(java.math.BigDecimal repayAmt) {
		this.repayAmt = repayAmt;
	}
	
    /**
     * @return repayAmt
     */
	public java.math.BigDecimal getRepayAmt() {
		return this.repayAmt;
	}
	
	/**
	 * @param currrentYear
	 */
	public void setCurrrentYear(String currrentYear) {
		this.currrentYear = currrentYear;
	}
	
    /**
     * @return currrentYear
     */
	public String getCurrrentYear() {
		return this.currrentYear;
	}
	
	/**
	 * @param currrentMonth
	 */
	public void setCurrrentMonth(String currrentMonth) {
		this.currrentMonth = currrentMonth;
	}
	
    /**
     * @return currrentMonth
     */
	public String getCurrrentMonth() {
		return this.currrentMonth;
	}
	
	/**
	 * @param currentDay
	 */
	public void setCurrentDay(String currentDay) {
		this.currentDay = currentDay;
	}
	
    /**
     * @return currentDay
     */
	public String getCurrentDay() {
		return this.currentDay;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param overTotalAmt
	 */
	public void setOverTotalAmt(java.math.BigDecimal overTotalAmt) {
		this.overTotalAmt = overTotalAmt;
	}
	
    /**
     * @return overTotalAmt
     */
	public java.math.BigDecimal getOverTotalAmt() {
		return this.overTotalAmt;
	}
	
	/**
	 * @param smsContent
	 */
	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}
	
    /**
     * @return smsContent
     */
	public String getSmsContent() {
		return this.smsContent;
	}
	
	/**
	 * @param subCode
	 */
	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}
	
    /**
     * @return subCode
     */
	public String getSubCode() {
		return this.subCode;
	}
	
	/**
	 * @param userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
    /**
     * @return userId
     */
	public String getUserId() {
		return this.userId;
	}
	
	/**
	 * @param smsType
	 */
	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}
	
    /**
     * @return smsType
     */
	public String getSmsType() {
		return this.smsType;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param sendTime
	 */
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
	
    /**
     * @return sendTime
     */
	public String getSendTime() {
		return this.sendTime;
	}
	
	/**
	 * @param smsSendState
	 */
	public void setSmsSendState(String smsSendState) {
		this.smsSendState = smsSendState;
	}
	
    /**
     * @return smsSendState
     */
	public String getSmsSendState() {
		return this.smsSendState;
	}
	
	/**
	 * @param substat
	 */
	public void setSubstat(String substat) {
		this.substat = substat;
	}
	
    /**
     * @return substat
     */
	public String getSubstat() {
		return this.substat;
	}
	
	/**
	 * @param smsFlag
	 */
	public void setSmsFlag(String smsFlag) {
		this.smsFlag = smsFlag;
	}
	
    /**
     * @return smsFlag
     */
	public String getSmsFlag() {
		return this.smsFlag;
	}
	
	/**
	 * @param validFlag
	 */
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}
	
    /**
     * @return validFlag
     */
	public String getValidFlag() {
		return this.validFlag;
	}


}