/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.rcp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpSnRepayPlanFileTemp;
import cn.com.yusys.yusp.batch.repository.mapper.load.rcp.BatSRcpSnRepayPlanFileTempMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpSnRepayPlanFileTempService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 15:42:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSRcpSnRepayPlanFileTempService {

    @Autowired
    private BatSRcpSnRepayPlanFileTempMapper batSRcpSnRepayPlanFileTempMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSRcpSnRepayPlanFileTemp selectByPrimaryKey(String prdCode, String contractNo, String period) {
        return batSRcpSnRepayPlanFileTempMapper.selectByPrimaryKey(prdCode, contractNo, period);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSRcpSnRepayPlanFileTemp> selectAll(QueryModel model) {
        List<BatSRcpSnRepayPlanFileTemp> records = (List<BatSRcpSnRepayPlanFileTemp>) batSRcpSnRepayPlanFileTempMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSRcpSnRepayPlanFileTemp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSRcpSnRepayPlanFileTemp> list = batSRcpSnRepayPlanFileTempMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSRcpSnRepayPlanFileTemp record) {
        return batSRcpSnRepayPlanFileTempMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSRcpSnRepayPlanFileTemp record) {
        return batSRcpSnRepayPlanFileTempMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSRcpSnRepayPlanFileTemp record) {
        return batSRcpSnRepayPlanFileTempMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSRcpSnRepayPlanFileTemp record) {
        return batSRcpSnRepayPlanFileTempMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String prdCode, String contractNo, String period) {
        return batSRcpSnRepayPlanFileTempMapper.deleteByPrimaryKey(prdCode, contractNo, period);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    public int deleteByOpenDay(String openDay) {
        return batSRcpSnRepayPlanFileTempMapper.deleteByOpenDay(openDay);
    }
}
