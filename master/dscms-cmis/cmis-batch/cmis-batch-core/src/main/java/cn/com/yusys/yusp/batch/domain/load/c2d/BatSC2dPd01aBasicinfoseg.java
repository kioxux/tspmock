/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.c2d;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dPd01aBasicinfoseg
 * @类描述: bat_s_c2d_pd01a_basicinfoseg数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:02:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_c2d_pd01a_basicinfoseg")
public class BatSC2dPd01aBasicinfoseg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** id **/
	@Id
	@Column(name = "ID")
	private String id;
	
	/** 信用报id **/
	@Id
	@Column(name = "REPORT_ID")
	private String reportId;
	
	/** 账户编号 **/
	@Column(name = "ACCOUNT_NUM", unique = false, nullable = true, length = 5)
	private String accountNum;
	
	/** 账户类型 **/
	@Column(name = "ACCOUNT_TYPE", unique = false, nullable = true, length = 60)
	private String accountType;
	
	/** 业务管理机构类型 **/
	@Column(name = "ORG_TYPE", unique = false, nullable = true, length = 60)
	private String orgType;
	
	/** 业务管理机构代码 **/
	@Column(name = "ORG_CODE", unique = false, nullable = true, length = 60)
	private String orgCode;
	
	/** 账户标识 **/
	@Column(name = "ACCOUNT_ID", unique = false, nullable = true, length = 64)
	private String accountId;
	
	/** 授信协议编号 **/
	@Column(name = "AGREEMENT_NUM", unique = false, nullable = true, length = 5)
	private String agreementNum;
	
	/** 业务种类 **/
	@Column(name = "BUSINESS_TYPE", unique = false, nullable = true, length = 60)
	private String businessType;
	
	/** 开立日期 **/
	@Column(name = "ISSUANCE_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date issuanceDate;
	
	/** 币种 **/
	@Column(name = "CURRENCY", unique = false, nullable = true, length = 60)
	private String currency;
	
	/** 借款金额 **/
	@Column(name = "LOAN_AMOUNT", unique = false, nullable = true, length = 22)
	private String loanAmount;
	
	/** 账户授信额度 **/
	@Column(name = "ACCOUNT_QUOTA", unique = false, nullable = true, length = 22)
	private String accountQuota;
	
	/** 共享授信额度 **/
	@Column(name = "SHARE_QUOTA", unique = false, nullable = true, length = 22)
	private String shareQuota;
	
	/** 到期日期 **/
	@Column(name = "DUE_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date dueDate;
	
	/** 还款方式 **/
	@Column(name = "REPAY_METHOD", unique = false, nullable = true, length = 60)
	private String repayMethod;
	
	/** 还款频率 **/
	@Column(name = "REPAY_RATE", unique = false, nullable = true, length = 60)
	private String repayRate;
	
	/** 还款期数 **/
	@Column(name = "REPAY_PERIODS", unique = false, nullable = true, length = 8)
	private String repayPeriods;
	
	/** 担保方式 **/
	@Column(name = "GUARANTEE_MODE", unique = false, nullable = true, length = 60)
	private String guaranteeMode;
	
	/** 贷款发放形式 **/
	@Column(name = "LOANISSU_METHOD", unique = false, nullable = true, length = 60)
	private String loanissuMethod;
	
	/** 共同借款标志 **/
	@Column(name = "LOAN_FLAG", unique = false, nullable = true, length = 60)
	private String loanFlag;
	
	/** 债券转移时的还款状态 **/
	@Column(name = "REPAY_STATUS", unique = false, nullable = true, length = 60)
	private String repayStatus;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param reportId
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	
    /**
     * @return reportId
     */
	public String getReportId() {
		return this.reportId;
	}
	
	/**
	 * @param accountNum
	 */
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}
	
    /**
     * @return accountNum
     */
	public String getAccountNum() {
		return this.accountNum;
	}
	
	/**
	 * @param accountType
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
    /**
     * @return accountType
     */
	public String getAccountType() {
		return this.accountType;
	}
	
	/**
	 * @param orgType
	 */
	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}
	
    /**
     * @return orgType
     */
	public String getOrgType() {
		return this.orgType;
	}
	
	/**
	 * @param orgCode
	 */
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	
    /**
     * @return orgCode
     */
	public String getOrgCode() {
		return this.orgCode;
	}
	
	/**
	 * @param accountId
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
    /**
     * @return accountId
     */
	public String getAccountId() {
		return this.accountId;
	}
	
	/**
	 * @param agreementNum
	 */
	public void setAgreementNum(String agreementNum) {
		this.agreementNum = agreementNum;
	}
	
    /**
     * @return agreementNum
     */
	public String getAgreementNum() {
		return this.agreementNum;
	}
	
	/**
	 * @param businessType
	 */
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	
    /**
     * @return businessType
     */
	public String getBusinessType() {
		return this.businessType;
	}
	
	/**
	 * @param issuanceDate
	 */
	public void setIssuanceDate(java.util.Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}
	
    /**
     * @return issuanceDate
     */
	public java.util.Date getIssuanceDate() {
		return this.issuanceDate;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    /**
     * @return currency
     */
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param loanAmount
	 */
	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}
	
    /**
     * @return loanAmount
     */
	public String getLoanAmount() {
		return this.loanAmount;
	}
	
	/**
	 * @param accountQuota
	 */
	public void setAccountQuota(String accountQuota) {
		this.accountQuota = accountQuota;
	}
	
    /**
     * @return accountQuota
     */
	public String getAccountQuota() {
		return this.accountQuota;
	}
	
	/**
	 * @param shareQuota
	 */
	public void setShareQuota(String shareQuota) {
		this.shareQuota = shareQuota;
	}
	
    /**
     * @return shareQuota
     */
	public String getShareQuota() {
		return this.shareQuota;
	}
	
	/**
	 * @param dueDate
	 */
	public void setDueDate(java.util.Date dueDate) {
		this.dueDate = dueDate;
	}
	
    /**
     * @return dueDate
     */
	public java.util.Date getDueDate() {
		return this.dueDate;
	}
	
	/**
	 * @param repayMethod
	 */
	public void setRepayMethod(String repayMethod) {
		this.repayMethod = repayMethod;
	}
	
    /**
     * @return repayMethod
     */
	public String getRepayMethod() {
		return this.repayMethod;
	}
	
	/**
	 * @param repayRate
	 */
	public void setRepayRate(String repayRate) {
		this.repayRate = repayRate;
	}
	
    /**
     * @return repayRate
     */
	public String getRepayRate() {
		return this.repayRate;
	}
	
	/**
	 * @param repayPeriods
	 */
	public void setRepayPeriods(String repayPeriods) {
		this.repayPeriods = repayPeriods;
	}
	
    /**
     * @return repayPeriods
     */
	public String getRepayPeriods() {
		return this.repayPeriods;
	}
	
	/**
	 * @param guaranteeMode
	 */
	public void setGuaranteeMode(String guaranteeMode) {
		this.guaranteeMode = guaranteeMode;
	}
	
    /**
     * @return guaranteeMode
     */
	public String getGuaranteeMode() {
		return this.guaranteeMode;
	}
	
	/**
	 * @param loanissuMethod
	 */
	public void setLoanissuMethod(String loanissuMethod) {
		this.loanissuMethod = loanissuMethod;
	}
	
    /**
     * @return loanissuMethod
     */
	public String getLoanissuMethod() {
		return this.loanissuMethod;
	}
	
	/**
	 * @param loanFlag
	 */
	public void setLoanFlag(String loanFlag) {
		this.loanFlag = loanFlag;
	}
	
    /**
     * @return loanFlag
     */
	public String getLoanFlag() {
		return this.loanFlag;
	}
	
	/**
	 * @param repayStatus
	 */
	public void setRepayStatus(String repayStatus) {
		this.repayStatus = repayStatus;
	}
	
    /**
     * @return repayStatus
     */
	public String getRepayStatus() {
		return this.repayStatus;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}