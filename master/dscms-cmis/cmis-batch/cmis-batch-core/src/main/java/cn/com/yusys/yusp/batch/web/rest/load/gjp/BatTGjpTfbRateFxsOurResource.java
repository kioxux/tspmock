/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.gjp;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.gjp.BatTGjpTfbRateFxsOur;
import cn.com.yusys.yusp.batch.service.load.gjp.BatTGjpTfbRateFxsOurService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTGjpTfbRateFxsOurResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:40:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battgjptfbratefxsour")
public class BatTGjpTfbRateFxsOurResource {
    @Autowired
    private BatTGjpTfbRateFxsOurService batTGjpTfbRateFxsOurService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTGjpTfbRateFxsOur>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTGjpTfbRateFxsOur> list = batTGjpTfbRateFxsOurService.selectAll(queryModel);
        return new ResultDto<List<BatTGjpTfbRateFxsOur>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTGjpTfbRateFxsOur>> index(QueryModel queryModel) {
        List<BatTGjpTfbRateFxsOur> list = batTGjpTfbRateFxsOurService.selectByModel(queryModel);
        return new ResultDto<List<BatTGjpTfbRateFxsOur>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{id}")
    protected ResultDto<BatTGjpTfbRateFxsOur> show(@PathVariable("id") String id) {
        BatTGjpTfbRateFxsOur batTGjpTfbRateFxsOur = batTGjpTfbRateFxsOurService.selectByPrimaryKey(id);
        return new ResultDto<BatTGjpTfbRateFxsOur>(batTGjpTfbRateFxsOur);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTGjpTfbRateFxsOur> create(@RequestBody BatTGjpTfbRateFxsOur batTGjpTfbRateFxsOur) throws URISyntaxException {
        batTGjpTfbRateFxsOurService.insert(batTGjpTfbRateFxsOur);
        return new ResultDto<BatTGjpTfbRateFxsOur>(batTGjpTfbRateFxsOur);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTGjpTfbRateFxsOur batTGjpTfbRateFxsOur) throws URISyntaxException {
        int result = batTGjpTfbRateFxsOurService.update(batTGjpTfbRateFxsOur);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{id}")
    protected ResultDto<Integer> delete(@PathVariable("id") String id) {
        int result = batTGjpTfbRateFxsOurService.deleteByPrimaryKey(id);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batTGjpTfbRateFxsOurService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
