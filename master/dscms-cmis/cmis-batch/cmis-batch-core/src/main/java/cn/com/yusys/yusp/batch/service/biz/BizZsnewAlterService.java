/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.biz;

import cn.com.yusys.yusp.batch.domain.biz.BizZsnewAlter;
import cn.com.yusys.yusp.batch.repository.mapper.biz.BizZsnewAlterMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ALTER;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BizZsnewAlterService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BizZsnewAlterService {
    private static final Logger logger = LoggerFactory.getLogger(BizZsnewAlterService.class);
    @Autowired
    private BizZsnewAlterMapper bizZsnewAlterMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BizZsnewAlter selectByPrimaryKey(String appNo, String seqNum) {
        return bizZsnewAlterMapper.selectByPrimaryKey(appNo, seqNum);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BizZsnewAlter> selectAll(QueryModel model) {
        List<BizZsnewAlter> records = (List<BizZsnewAlter>) bizZsnewAlterMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BizZsnewAlter> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BizZsnewAlter> list = bizZsnewAlterMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BizZsnewAlter record) {
        return bizZsnewAlterMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BizZsnewAlter record) {
        return bizZsnewAlterMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BizZsnewAlter record) {
        return bizZsnewAlterMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BizZsnewAlter record) {
        return bizZsnewAlterMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String appNo, String seqNum) {
        return bizZsnewAlterMapper.deleteByPrimaryKey(appNo, seqNum);
    }

    /**
     * 根据查询结果来组装[企业历史变更信息]
     *
     * @param zsnewAlter
     * @param corpMap
     * @return
     */
    public BizZsnewAlter buildByZsnewAlter(ALTER zsnewAlter, Map<String, String> corpMap) {
        logger.info("根据查询结果来组装[企业历史变更信息]开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        BizZsnewAlter bizZsnewAlter = new BizZsnewAlter();
        String autoSerno = corpMap.get("autoSerno");
        String cusId = corpMap.get("cusId");
        String cusName = corpMap.get("cusName");
        String certType = corpMap.get("certType");
        String certCode = corpMap.get("certCode");

        bizZsnewAlter.setAppNo(autoSerno);
        bizZsnewAlter.setSeqNum(UUID.randomUUID().toString().substring(0, 24));
        bizZsnewAlter.setAltdate(zsnewAlter.getALTDATE());//变更日期
        String altitem = zsnewAlter.getALTITEM();
        if (Objects.nonNull(zsnewAlter.getALTITEM()) && zsnewAlter.getALTITEM().length() > 2550) {
            altitem = zsnewAlter.getALTITEM().substring(0, 2449);
        }
        bizZsnewAlter.setAltitem(altitem);//变更事项

        String altaf = zsnewAlter.getALTAF();
        if (Objects.nonNull(zsnewAlter.getALTAF()) && zsnewAlter.getALTAF().length() > 2550) {
            altaf = zsnewAlter.getALTAF().substring(0, 2449);
        }
        bizZsnewAlter.setAltaf(altaf);//变更后内容

        String altbe = zsnewAlter.getALTBE();
        if (Objects.nonNull(zsnewAlter.getALTBE()) && zsnewAlter.getALTBE().length() > 2550) {
            altbe = zsnewAlter.getALTBE().substring(0, 2449);
        }
        bizZsnewAlter.setAltbe(altbe);//变更前内容
        bizZsnewAlter.setInputTime(DateUtils.getCurrDateTimeStr());
        bizZsnewAlter.setLastUpdateDate(DateUtils.getCurrDateStr());
        bizZsnewAlter.setLastUpdateTime(DateUtils.getCurrDateTimeStr());
        logger.info("根据查询结果来组装[企业历史变更信息]结束,响应参数为:[{}]", JSON.toJSONString(bizZsnewAlter));
        return bizZsnewAlter;
    }
}
