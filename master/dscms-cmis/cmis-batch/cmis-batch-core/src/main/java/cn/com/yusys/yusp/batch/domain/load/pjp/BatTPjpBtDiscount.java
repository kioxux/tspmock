/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTPjpBtDiscount
 * @类描述: bat_t_pjp_bt_discount数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-03 10:58:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_pjp_bt_discount")
public class BatTPjpBtDiscount extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键id **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "DISC_BILL_ID")
	private String discBillId;
	
	/** 机构id **/
	@Column(name = "S_BRANCH_ID", unique = false, nullable = true, length = 40)
	private String sBranchId;
	
	/** 票据id **/
	@Column(name = "BILLINFO_ID", unique = false, nullable = true, length = 40)
	private String billinfoId;
	
	/** 票号 **/
	@Column(name = "S_BILL_NO", unique = false, nullable = true, length = 40)
	private String sBillNo;
	
	/** 票据类型 **/
	@Column(name = "S_BILL_TYPE", unique = false, nullable = true, length = 10)
	private String sBillType;
	
	/** 出票日 **/
	@Column(name = "D_ISSUE_DT", unique = false, nullable = true, length = 10)
	private String dIssueDt;
	
	/** 到期日 **/
	@Column(name = "D_DUE_DT", unique = false, nullable = true, length = 10)
	private String dDueDt;
	
	/** 出票人名称 **/
	@Column(name = "S_ISSUER_NAME", unique = false, nullable = true, length = 80)
	private String sIssuerName;
	
	/** 承兑人名称 **/
	@Column(name = "S_ACCEPTOR", unique = false, nullable = true, length = 80)
	private String sAcceptor;
	
	/** 计息天数 **/
	@Column(name = "F_INT_DAYS", unique = false, nullable = true, length = 10)
	private Integer fIntDays;
	
	/** 是否同城 **/
	@Column(name = "S_IF_SAME_CITY", unique = false, nullable = true, length = 10)
	private String sIfSameCity;
	
	/** 票面金额 **/
	@Column(name = "F_BILL_AMOUNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fBillAmount;
	
	/** 应付利息 **/
	@Column(name = "F_INT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fInt;
	
	/** 实付金额 **/
	@Column(name = "F_PAYMENT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fPayment;

    /**
     * 票据介质
     **/
    @Column(name = "S_BILL_MEDIA", unique = false, nullable = true, length = 100)
    private String sBillMedia;
	
	/** 清算方式 **/
	@Column(name = "S_CLEAR_WAY", unique = false, nullable = true, length = 10)
	private String sClearWay;
	
	/** 利率 **/
	@Column(name = "F_RATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fRate;
	
	/** 利率类型 **/
	@Column(name = "S_RATE_TYPE", unique = false, nullable = true, length = 10)
	private String sRateType;

    /**
     * 回购截止日
     **/
    @Column(name = "D_REDE_END_DT", unique = false, nullable = true, length = 60)
    private String dRedeEndDt;

    /**
     * 回购开放日
     **/
    @Column(name = "D_REDE_START_DT", unique = false, nullable = true, length = 60)
    private String dRedeStartDt;
	
	/** 状态 **/
	@Column(name = "S_BILL_STATUS", unique = false, nullable = true, length = 10)
	private String sBillStatus;
	
	/** 产品ID **/
	@Column(name = "PRODUCT_ID", unique = false, nullable = true, length = 40)
	private String productId;
	
	/** 批次ID **/
	@Column(name = "DISC_BATCH_ID", unique = false, nullable = true, length = 40)
	private String discBatchId;
	
	/** 报文状态 **/
	@Column(name = "S_MSG_STATUS", unique = false, nullable = true, length = 15)
    private String sMsgStatus;

    /**
     * 贴现申请日
     **/
    @Column(name = "DISC_BATCH_DT", unique = false, nullable = true, length = 60)
	private String discBatchDt;
	
	/** 贴现种类 **/
	@Column(name = "DISC_TYPE", unique = false, nullable = true, length = 40)
	private String discType;
	
	/** 不得转让标记 **/
	@Column(name = "TRANS_SIGN", unique = false, nullable = true, length = 40)
	private String transSign;
	
	/** 入账帐号 **/
	@Column(name = "IN_ACCOUNT_NUM", unique = false, nullable = true, length = 40)
	private String inAccountNum;
	
	/** 入账行号 **/
	@Column(name = "IN_ACCOUNT_BANK_NUM", unique = false, nullable = true, length = 40)
	private String inAccountBankNum;
	
	/** 贴入人账号 **/
	@Column(name = "DISC_IN_ACCOUNT", unique = false, nullable = true, length = 40)
	private String discInAccount;
	
	/** 贴入人开户行行号 **/
	@Column(name = "DISC_IN_OPN", unique = false, nullable = true, length = 40)
	private String discInOpn;
	
	/** 付息方式 **/
	@Column(name = "S_INT_PAYWAY", unique = false, nullable = true, length = 10)
	private String sIntPayway;
	
	/** 报文错误码以及错误信息 **/
	@Column(name = "S_MSG_CODE", unique = false, nullable = true, length = 800)
	private String sMsgCode;
	
	/** 买方实付金额 **/
	@Column(name = "F_BUY_MENT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fBuyMent;
	
	/** 贴出方名称 **/
	@Column(name = "S_OUT_NAME", unique = false, nullable = true, length = 40)
	private String sOutName;
	
	/** 贴出方组织机构代码 **/
	@Column(name = "S_OUT_ORGCODE", unique = false, nullable = true, length = 40)
	private String sOutOrgcode;
	
	/** 贴出人账号 **/
	@Column(name = "S_OUT_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sOutAccount;
	
	/** 贴出方行号 **/
	@Column(name = "S_OUT_BANKNUM", unique = false, nullable = true, length = 40)
	private String sOutBanknum;
	
	/** 贴入方名称 **/
	@Column(name = "S_INT_NAME", unique = false, nullable = true, length = 40)
	private String sIntName;
	
	/** 贴入方组织机构代码 **/
	@Column(name = "S_INT_ORGCODE", unique = false, nullable = true, length = 40)
	private String sIntOrgcode;
	
	/** 贴入方行号 **/
	@Column(name = "S_INT_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sIntAccount;
	
	/** 大额行号 **/
	@Column(name = "S_INT_BANKNUM", unique = false, nullable = true, length = 40)
	private String sIntBanknum;
	
	/** 贴现实付金额 **/
	@Column(name = "F_BUY_PAYMENT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fBuyPayment;
	
	/** 付息比例 **/
	@Column(name = "EN_SC", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal enSc;
	
	/** 贴入人角色 **/
	@Column(name = "DSCNT_ROLE", unique = false, nullable = true, length = 40)
	private String dscntRole;
	
	/** 签收备注(签收031报文) **/
	@Column(name = "DISC_OUT_REMARK", unique = false, nullable = true, length = 800)
	private String discOutRemark;
	
	/** 承兑行行号 **/
	@Column(name = "S_AGCYSVCR", unique = false, nullable = true, length = 12)
	private String sAgcysvcr;
	
	/** 承兑行名称 **/
	@Column(name = "S_AGCYSVCRNAME", unique = false, nullable = true, length = 120)
	private String sAgcysvcrname;
	
	/** 出票人账号 **/
	@Column(name = "S_ISSUER_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sIssuerAccount;
	
	/** 出票人行名 **/
	@Column(name = "S_ISSUER_BANK_NAME", unique = false, nullable = true, length = 40)
	private String sIssuerBankName;
	
	/** 出票人行号 **/
	@Column(name = "S_ISSUER_BANK_CODE", unique = false, nullable = true, length = 40)
    private String sIssuerBankCode;

    /**
     * 回购式贴现赎回签收日
     **/
    @Column(name = "RE_SIGN_DT", unique = false, nullable = true, length = 60)
	private String reSignDt;
	
	/** 申请人名称是否一致标识 **/
	@Column(name = "APP_ORGCODE_YORN", unique = false, nullable = true, length = 10)
	private String appOrgcodeYorn;
	
	/** 贴现申请备注 **/
	@Column(name = "DISC_SELL_REMARK", unique = false, nullable = true, length = 800)
	private String discSellRemark;
	
	/** 发票总金额 **/
	@Column(name = "INVOICE_AMOUNT_SUM", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal invoiceAmountSum;
	
	/** 合同总金额 **/
	@Column(name = "CONTRACT_AMOUNT_SUM", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal contractAmountSum;
	
	/** 贴现赎回利率 **/
	@Column(name = "F_PAY_RATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fPayRate;
	
	/** 贴现类型(保贴、直贴) **/
	@Column(name = "S_DISC_CLASS", unique = false, nullable = true, length = 40)
	private String sDiscClass;
	
	/** 贴现赎回状态(如果是回购式贴现，该字段用来

标识是否已发回购式贴现赎回申请) **/
	@Column(name = "DISC_SALE_STATUS", unique = false, nullable = true, length = 40)
	private String discSaleStatus;
	
	/** 贴入人角色 **/
	@Column(name = "DISCOUNT_IN_ROLE", unique = false, nullable = true, length = 40)
	private String discountInRole;
	
	/** 贴入人承接行行号 **/
	@Column(name = "DISCOUNT_IN_AGCYSVCR", unique = false, nullable = true, length = 40)
	private String discountInAgcysvcr;
	
	/** 交易合同编号 **/
	@Column(name = "S_SWAP_CONT_NUM", unique = false, nullable = true, length = 40)
	private String sSwapContNum;
	
	/** 发票号码 **/
	@Column(name = "S_VOICE_NUM", unique = false, nullable = true, length = 40)
	private String sVoiceNum;
	
	/** 库存状态 **/
	@Column(name = "STORAGE_STS", unique = false, nullable = true, length = 10)
	private String storageSts;
	
	/** 操作员id **/
	@Column(name = "S_OPERATOR_ID", unique = false, nullable = true, length = 40)
	private String sOperatorId;
	
	/** 贴现收益((利率-成本利率)*计息天数) **/
	@Column(name = "DISCINCOME", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal discincome;

    /**
     * 时间撮-业务明细修改时间标识
     **/
    @Column(name = "TIMEVERSION", unique = false, nullable = true, length = 100)
	private String timeversion;
	
	/** 获得电子签名 **/
	@Column(name = "DISCIN_ELECTRON_SIGN", unique = false, nullable = true, length = 2000)
	private String discinElectronSign;
	
	/** 借据号 **/
	@Column(name = "S_JJH", unique = false, nullable = true, length = 50)
	private String sJjh;
	
	/** 贴现协议号 **/
	@Column(name = "S_DISCOUNTAGREENO", unique = false, nullable = true, length = 50)
    private String sDiscountagreeno;

    /**
     * 计息到期日
     **/
    @Column(name = "GALE_DT", unique = false, nullable = true, length = 60)
	private String galeDt;
	
	/** 业务结清 标记 00未结清；01已结清 **/
	@Column(name = "BUSI_END_FLAG", unique = false, nullable = true, length = 10)
	private String busiEndFlag;
	
	/** 结清类型 **/
	@Column(name = "BUSI_END_TYPE", unique = false, nullable = true, length = 10)
	private String busiEndType;
	
	/** 结清日期 **/
	@Column(name = "BUSI_END_DATE", unique = false, nullable = true, length = 20)
	private String busiEndDate;
	
	/** 记账日期 **/
	@Column(name = "ACCT_DATE", unique = false, nullable = true, length = 20)
	private String acctDate;
	
	/** 记账流水号 **/
	@Column(name = "ACCT_FLOW_NO", unique = false, nullable = true, length = 60)
	private String acctFlowNo;
	
	/** 记账柜员号 **/
	@Column(name = "ACCT_USER_NO", unique = false, nullable = true, length = 60)
	private String acctUserNo;
	
	/** 记账柜员名 **/
	@Column(name = "ACCT_USER_NAME", unique = false, nullable = true, length = 60)
	private String acctUserName;
	
	/** 记账授权柜员号 **/
	@Column(name = "ACCT_AUTH_USER_NO", unique = false, nullable = true, length = 60)
	private String acctAuthUserNo;
	
	/** 记账授权柜员名 **/
	@Column(name = "ACCT_AUTH_USER_NAME", unique = false, nullable = true, length = 60)
	private String acctAuthUserName;
	
	/** 撤销记账柜员号 **/
	@Column(name = "CANCEL_ACCT_USER_NO", unique = false, nullable = true, length = 60)
	private String cancelAcctUserNo;
	
	/** 撤销记账柜员名 **/
	@Column(name = "CANCEL_ACCT_USER_NAME", unique = false, nullable = true, length = 60)
	private String cancelAcctUserName;
	
	/** 撤销记账授权柜员号 **/
	@Column(name = "CANCEL_ACCT_AUTH_USER_NO", unique = false, nullable = true, length = 60)
	private String cancelAcctAuthUserNo;
	
	/** 撤销记账授权柜员名 **/
	@Column(name = "CANCEL_ACCT_AUTH_USER_NAME", unique = false, nullable = true, length = 60)
	private String cancelAcctAuthUserName;
	
	/** 代理行业务标记 STD_RZ_AGENT_FLAG **/
	@Column(name = "AGENT_FLAG", unique = false, nullable = true, length = 2)
	private String agentFlag;
	
	/** 承兑行类别 **/
	@Column(name = "ACCPT_BANK_SORT", unique = false, nullable = true, length = 10)
	private String accptBankSort;
	
	/** 贴现成本利率 **/
	@Column(name = "DISCOUNT_COST_RATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal discountCostRate;
	
	/** 移存标记 **/
	@Column(name = "MOVE_FLAG", unique = false, nullable = true, length = 10)
	private String moveFlag;
	
	/** 移出机构号 **/
	@Column(name = "MOVE_BRANCH_NO", unique = false, nullable = true, length = 30)
	private String moveBranchNo;
	
	/** 计息调整天数 **/
	@Column(name = "IMPROVE_DAYS", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal improveDays;
	
	/** 数据移植标记 STD_RZ_YZ_FLAG **/
	@Column(name = "YZ_FLAG", unique = false, nullable = true, length = 2)
	private String yzFlag;
	
	/** 核心返回时间 **/
	@Column(name = "CORE_TRANDT", unique = false, nullable = true, length = 40)
	private String coreTrandt;
	
	/** 核心返回流水 **/
	@Column(name = "CORE_TRANSQ", unique = false, nullable = true, length = 100)
	private String coreTransq;
	
	/** 信贷贴现记账成功标记 **/
	@Column(name = "IF_CREDIT_FLAG", unique = false, nullable = true, length = 4)
	private String ifCreditFlag;
	
	
	/**
	 * @param discBillId
	 */
	public void setDiscBillId(String discBillId) {
		this.discBillId = discBillId;
	}
	
    /**
     * @return discBillId
     */
	public String getDiscBillId() {
		return this.discBillId;
	}
	
	/**
	 * @param sBranchId
	 */
	public void setSBranchId(String sBranchId) {
		this.sBranchId = sBranchId;
	}
	
    /**
     * @return sBranchId
     */
	public String getSBranchId() {
		return this.sBranchId;
	}
	
	/**
	 * @param billinfoId
	 */
	public void setBillinfoId(String billinfoId) {
		this.billinfoId = billinfoId;
	}
	
    /**
     * @return billinfoId
     */
	public String getBillinfoId() {
		return this.billinfoId;
	}
	
	/**
	 * @param sBillNo
	 */
	public void setSBillNo(String sBillNo) {
		this.sBillNo = sBillNo;
	}
	
    /**
     * @return sBillNo
     */
	public String getSBillNo() {
		return this.sBillNo;
	}
	
	/**
	 * @param sBillType
	 */
	public void setSBillType(String sBillType) {
		this.sBillType = sBillType;
	}
	
    /**
     * @return sBillType
     */
	public String getSBillType() {
		return this.sBillType;
	}
	
	/**
	 * @param dIssueDt
	 */
	public void setDIssueDt(String dIssueDt) {
		this.dIssueDt = dIssueDt;
	}
	
    /**
     * @return dIssueDt
     */
	public String getDIssueDt() {
		return this.dIssueDt;
	}
	
	/**
	 * @param dDueDt
	 */
	public void setDDueDt(String dDueDt) {
		this.dDueDt = dDueDt;
	}
	
    /**
     * @return dDueDt
     */
	public String getDDueDt() {
		return this.dDueDt;
	}
	
	/**
	 * @param sIssuerName
	 */
	public void setSIssuerName(String sIssuerName) {
		this.sIssuerName = sIssuerName;
	}
	
    /**
     * @return sIssuerName
     */
	public String getSIssuerName() {
		return this.sIssuerName;
	}
	
	/**
	 * @param sAcceptor
	 */
	public void setSAcceptor(String sAcceptor) {
		this.sAcceptor = sAcceptor;
	}
	
    /**
     * @return sAcceptor
     */
	public String getSAcceptor() {
		return this.sAcceptor;
	}
	
	/**
	 * @param fIntDays
	 */
	public void setFIntDays(Integer fIntDays) {
		this.fIntDays = fIntDays;
	}
	
    /**
     * @return fIntDays
     */
	public Integer getFIntDays() {
		return this.fIntDays;
	}
	
	/**
	 * @param sIfSameCity
	 */
	public void setSIfSameCity(String sIfSameCity) {
		this.sIfSameCity = sIfSameCity;
	}
	
    /**
     * @return sIfSameCity
     */
	public String getSIfSameCity() {
		return this.sIfSameCity;
	}
	
	/**
	 * @param fBillAmount
	 */
	public void setFBillAmount(java.math.BigDecimal fBillAmount) {
		this.fBillAmount = fBillAmount;
	}
	
    /**
     * @return fBillAmount
     */
	public java.math.BigDecimal getFBillAmount() {
		return this.fBillAmount;
	}
	
	/**
	 * @param fInt
	 */
	public void setFInt(java.math.BigDecimal fInt) {
		this.fInt = fInt;
	}
	
    /**
     * @return fInt
     */
	public java.math.BigDecimal getFInt() {
		return this.fInt;
	}
	
	/**
	 * @param fPayment
	 */
	public void setFPayment(java.math.BigDecimal fPayment) {
		this.fPayment = fPayment;
	}
	
    /**
     * @return fPayment
     */
	public java.math.BigDecimal getFPayment() {
		return this.fPayment;
	}
	
	/**
	 * @param sBillMedia
	 */
	public void setSBillMedia(String sBillMedia) {
		this.sBillMedia = sBillMedia;
	}
	
    /**
     * @return sBillMedia
     */
	public String getSBillMedia() {
		return this.sBillMedia;
	}
	
	/**
	 * @param sClearWay
	 */
	public void setSClearWay(String sClearWay) {
		this.sClearWay = sClearWay;
	}
	
    /**
     * @return sClearWay
     */
	public String getSClearWay() {
		return this.sClearWay;
	}
	
	/**
	 * @param fRate
	 */
	public void setFRate(java.math.BigDecimal fRate) {
		this.fRate = fRate;
	}
	
    /**
     * @return fRate
     */
	public java.math.BigDecimal getFRate() {
		return this.fRate;
	}
	
	/**
	 * @param sRateType
	 */
	public void setSRateType(String sRateType) {
		this.sRateType = sRateType;
	}
	
    /**
     * @return sRateType
     */
	public String getSRateType() {
		return this.sRateType;
	}
	
	/**
	 * @param dRedeEndDt
	 */
	public void setDRedeEndDt(String dRedeEndDt) {
		this.dRedeEndDt = dRedeEndDt;
	}
	
    /**
     * @return dRedeEndDt
     */
	public String getDRedeEndDt() {
		return this.dRedeEndDt;
	}
	
	/**
	 * @param dRedeStartDt
	 */
	public void setDRedeStartDt(String dRedeStartDt) {
		this.dRedeStartDt = dRedeStartDt;
	}
	
    /**
     * @return dRedeStartDt
     */
	public String getDRedeStartDt() {
		return this.dRedeStartDt;
	}
	
	/**
	 * @param sBillStatus
	 */
	public void setSBillStatus(String sBillStatus) {
		this.sBillStatus = sBillStatus;
	}
	
    /**
     * @return sBillStatus
     */
	public String getSBillStatus() {
		return this.sBillStatus;
	}
	
	/**
	 * @param productId
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
    /**
     * @return productId
     */
	public String getProductId() {
		return this.productId;
	}
	
	/**
	 * @param discBatchId
	 */
	public void setDiscBatchId(String discBatchId) {
		this.discBatchId = discBatchId;
	}
	
    /**
     * @return discBatchId
     */
	public String getDiscBatchId() {
		return this.discBatchId;
	}
	
	/**
	 * @param sMsgStatus
	 */
	public void setSMsgStatus(String sMsgStatus) {
		this.sMsgStatus = sMsgStatus;
	}
	
    /**
     * @return sMsgStatus
     */
	public String getSMsgStatus() {
		return this.sMsgStatus;
	}
	
	/**
	 * @param discBatchDt
	 */
	public void setDiscBatchDt(String discBatchDt) {
		this.discBatchDt = discBatchDt;
	}
	
    /**
     * @return discBatchDt
     */
	public String getDiscBatchDt() {
		return this.discBatchDt;
	}
	
	/**
	 * @param discType
	 */
	public void setDiscType(String discType) {
		this.discType = discType;
	}
	
    /**
     * @return discType
     */
	public String getDiscType() {
		return this.discType;
	}
	
	/**
	 * @param transSign
	 */
	public void setTransSign(String transSign) {
		this.transSign = transSign;
	}
	
    /**
     * @return transSign
     */
	public String getTransSign() {
		return this.transSign;
	}
	
	/**
	 * @param inAccountNum
	 */
	public void setInAccountNum(String inAccountNum) {
		this.inAccountNum = inAccountNum;
	}
	
    /**
     * @return inAccountNum
     */
	public String getInAccountNum() {
		return this.inAccountNum;
	}
	
	/**
	 * @param inAccountBankNum
	 */
	public void setInAccountBankNum(String inAccountBankNum) {
		this.inAccountBankNum = inAccountBankNum;
	}
	
    /**
     * @return inAccountBankNum
     */
	public String getInAccountBankNum() {
		return this.inAccountBankNum;
	}
	
	/**
	 * @param discInAccount
	 */
	public void setDiscInAccount(String discInAccount) {
		this.discInAccount = discInAccount;
	}
	
    /**
     * @return discInAccount
     */
	public String getDiscInAccount() {
		return this.discInAccount;
	}
	
	/**
	 * @param discInOpn
	 */
	public void setDiscInOpn(String discInOpn) {
		this.discInOpn = discInOpn;
	}
	
    /**
     * @return discInOpn
     */
	public String getDiscInOpn() {
		return this.discInOpn;
	}
	
	/**
	 * @param sIntPayway
	 */
	public void setSIntPayway(String sIntPayway) {
		this.sIntPayway = sIntPayway;
	}
	
    /**
     * @return sIntPayway
     */
	public String getSIntPayway() {
		return this.sIntPayway;
	}
	
	/**
	 * @param sMsgCode
	 */
	public void setSMsgCode(String sMsgCode) {
		this.sMsgCode = sMsgCode;
	}
	
    /**
     * @return sMsgCode
     */
	public String getSMsgCode() {
		return this.sMsgCode;
	}
	
	/**
	 * @param fBuyMent
	 */
	public void setFBuyMent(java.math.BigDecimal fBuyMent) {
		this.fBuyMent = fBuyMent;
	}
	
    /**
     * @return fBuyMent
     */
	public java.math.BigDecimal getFBuyMent() {
		return this.fBuyMent;
	}
	
	/**
	 * @param sOutName
	 */
	public void setSOutName(String sOutName) {
		this.sOutName = sOutName;
	}
	
    /**
     * @return sOutName
     */
	public String getSOutName() {
		return this.sOutName;
	}
	
	/**
	 * @param sOutOrgcode
	 */
	public void setSOutOrgcode(String sOutOrgcode) {
		this.sOutOrgcode = sOutOrgcode;
	}
	
    /**
     * @return sOutOrgcode
     */
	public String getSOutOrgcode() {
		return this.sOutOrgcode;
	}
	
	/**
	 * @param sOutAccount
	 */
	public void setSOutAccount(String sOutAccount) {
		this.sOutAccount = sOutAccount;
	}
	
    /**
     * @return sOutAccount
     */
	public String getSOutAccount() {
		return this.sOutAccount;
	}
	
	/**
	 * @param sOutBanknum
	 */
	public void setSOutBanknum(String sOutBanknum) {
		this.sOutBanknum = sOutBanknum;
	}
	
    /**
     * @return sOutBanknum
     */
	public String getSOutBanknum() {
		return this.sOutBanknum;
	}
	
	/**
	 * @param sIntName
	 */
	public void setSIntName(String sIntName) {
		this.sIntName = sIntName;
	}
	
    /**
     * @return sIntName
     */
	public String getSIntName() {
		return this.sIntName;
	}
	
	/**
	 * @param sIntOrgcode
	 */
	public void setSIntOrgcode(String sIntOrgcode) {
		this.sIntOrgcode = sIntOrgcode;
	}
	
    /**
     * @return sIntOrgcode
     */
	public String getSIntOrgcode() {
		return this.sIntOrgcode;
	}
	
	/**
	 * @param sIntAccount
	 */
	public void setSIntAccount(String sIntAccount) {
		this.sIntAccount = sIntAccount;
	}
	
    /**
     * @return sIntAccount
     */
	public String getSIntAccount() {
		return this.sIntAccount;
	}
	
	/**
	 * @param sIntBanknum
	 */
	public void setSIntBanknum(String sIntBanknum) {
		this.sIntBanknum = sIntBanknum;
	}
	
    /**
     * @return sIntBanknum
     */
	public String getSIntBanknum() {
		return this.sIntBanknum;
	}
	
	/**
	 * @param fBuyPayment
	 */
	public void setFBuyPayment(java.math.BigDecimal fBuyPayment) {
		this.fBuyPayment = fBuyPayment;
	}
	
    /**
     * @return fBuyPayment
     */
	public java.math.BigDecimal getFBuyPayment() {
		return this.fBuyPayment;
	}
	
	/**
	 * @param enSc
	 */
	public void setEnSc(java.math.BigDecimal enSc) {
		this.enSc = enSc;
	}
	
    /**
     * @return enSc
     */
	public java.math.BigDecimal getEnSc() {
		return this.enSc;
	}
	
	/**
	 * @param dscntRole
	 */
	public void setDscntRole(String dscntRole) {
		this.dscntRole = dscntRole;
	}
	
    /**
     * @return dscntRole
     */
	public String getDscntRole() {
		return this.dscntRole;
	}
	
	/**
	 * @param discOutRemark
	 */
	public void setDiscOutRemark(String discOutRemark) {
		this.discOutRemark = discOutRemark;
	}
	
    /**
     * @return discOutRemark
     */
	public String getDiscOutRemark() {
		return this.discOutRemark;
	}
	
	/**
	 * @param sAgcysvcr
	 */
	public void setSAgcysvcr(String sAgcysvcr) {
		this.sAgcysvcr = sAgcysvcr;
	}
	
    /**
     * @return sAgcysvcr
     */
	public String getSAgcysvcr() {
		return this.sAgcysvcr;
	}
	
	/**
	 * @param sAgcysvcrname
	 */
	public void setSAgcysvcrname(String sAgcysvcrname) {
		this.sAgcysvcrname = sAgcysvcrname;
	}
	
    /**
     * @return sAgcysvcrname
     */
	public String getSAgcysvcrname() {
		return this.sAgcysvcrname;
	}
	
	/**
	 * @param sIssuerAccount
	 */
	public void setSIssuerAccount(String sIssuerAccount) {
		this.sIssuerAccount = sIssuerAccount;
	}
	
    /**
     * @return sIssuerAccount
     */
	public String getSIssuerAccount() {
		return this.sIssuerAccount;
	}
	
	/**
	 * @param sIssuerBankName
	 */
	public void setSIssuerBankName(String sIssuerBankName) {
		this.sIssuerBankName = sIssuerBankName;
	}
	
    /**
     * @return sIssuerBankName
     */
	public String getSIssuerBankName() {
		return this.sIssuerBankName;
	}
	
	/**
	 * @param sIssuerBankCode
	 */
	public void setSIssuerBankCode(String sIssuerBankCode) {
		this.sIssuerBankCode = sIssuerBankCode;
	}
	
    /**
     * @return sIssuerBankCode
     */
	public String getSIssuerBankCode() {
		return this.sIssuerBankCode;
	}
	
	/**
	 * @param reSignDt
	 */
	public void setReSignDt(String reSignDt) {
		this.reSignDt = reSignDt;
	}
	
    /**
     * @return reSignDt
     */
	public String getReSignDt() {
		return this.reSignDt;
	}
	
	/**
	 * @param appOrgcodeYorn
	 */
	public void setAppOrgcodeYorn(String appOrgcodeYorn) {
		this.appOrgcodeYorn = appOrgcodeYorn;
	}
	
    /**
     * @return appOrgcodeYorn
     */
	public String getAppOrgcodeYorn() {
		return this.appOrgcodeYorn;
	}
	
	/**
	 * @param discSellRemark
	 */
	public void setDiscSellRemark(String discSellRemark) {
		this.discSellRemark = discSellRemark;
	}
	
    /**
     * @return discSellRemark
     */
	public String getDiscSellRemark() {
		return this.discSellRemark;
	}
	
	/**
	 * @param invoiceAmountSum
	 */
	public void setInvoiceAmountSum(java.math.BigDecimal invoiceAmountSum) {
		this.invoiceAmountSum = invoiceAmountSum;
	}
	
    /**
     * @return invoiceAmountSum
     */
	public java.math.BigDecimal getInvoiceAmountSum() {
		return this.invoiceAmountSum;
	}
	
	/**
	 * @param contractAmountSum
	 */
	public void setContractAmountSum(java.math.BigDecimal contractAmountSum) {
		this.contractAmountSum = contractAmountSum;
	}
	
    /**
     * @return contractAmountSum
     */
	public java.math.BigDecimal getContractAmountSum() {
		return this.contractAmountSum;
	}
	
	/**
	 * @param fPayRate
	 */
	public void setFPayRate(java.math.BigDecimal fPayRate) {
		this.fPayRate = fPayRate;
	}
	
    /**
     * @return fPayRate
     */
	public java.math.BigDecimal getFPayRate() {
		return this.fPayRate;
	}
	
	/**
	 * @param sDiscClass
	 */
	public void setSDiscClass(String sDiscClass) {
		this.sDiscClass = sDiscClass;
	}
	
    /**
     * @return sDiscClass
     */
	public String getSDiscClass() {
		return this.sDiscClass;
	}
	
	/**
	 * @param discSaleStatus
	 */
	public void setDiscSaleStatus(String discSaleStatus) {
		this.discSaleStatus = discSaleStatus;
	}
	
    /**
     * @return discSaleStatus
     */
	public String getDiscSaleStatus() {
		return this.discSaleStatus;
	}
	
	/**
	 * @param discountInRole
	 */
	public void setDiscountInRole(String discountInRole) {
		this.discountInRole = discountInRole;
	}
	
    /**
     * @return discountInRole
     */
	public String getDiscountInRole() {
		return this.discountInRole;
	}
	
	/**
	 * @param discountInAgcysvcr
	 */
	public void setDiscountInAgcysvcr(String discountInAgcysvcr) {
		this.discountInAgcysvcr = discountInAgcysvcr;
	}
	
    /**
     * @return discountInAgcysvcr
     */
	public String getDiscountInAgcysvcr() {
		return this.discountInAgcysvcr;
	}
	
	/**
	 * @param sSwapContNum
	 */
	public void setSSwapContNum(String sSwapContNum) {
		this.sSwapContNum = sSwapContNum;
	}
	
    /**
     * @return sSwapContNum
     */
	public String getSSwapContNum() {
		return this.sSwapContNum;
	}
	
	/**
	 * @param sVoiceNum
	 */
	public void setSVoiceNum(String sVoiceNum) {
		this.sVoiceNum = sVoiceNum;
	}
	
    /**
     * @return sVoiceNum
     */
	public String getSVoiceNum() {
		return this.sVoiceNum;
	}
	
	/**
	 * @param storageSts
	 */
	public void setStorageSts(String storageSts) {
		this.storageSts = storageSts;
	}
	
    /**
     * @return storageSts
     */
	public String getStorageSts() {
		return this.storageSts;
	}
	
	/**
	 * @param sOperatorId
	 */
	public void setSOperatorId(String sOperatorId) {
		this.sOperatorId = sOperatorId;
	}
	
    /**
     * @return sOperatorId
     */
	public String getSOperatorId() {
		return this.sOperatorId;
	}
	
	/**
	 * @param discincome
	 */
	public void setDiscincome(java.math.BigDecimal discincome) {
		this.discincome = discincome;
	}
	
    /**
     * @return discincome
     */
	public java.math.BigDecimal getDiscincome() {
		return this.discincome;
	}
	
	/**
	 * @param timeversion
	 */
	public void setTimeversion(String timeversion) {
		this.timeversion = timeversion;
	}
	
    /**
     * @return timeversion
     */
	public String getTimeversion() {
		return this.timeversion;
	}
	
	/**
	 * @param discinElectronSign
	 */
	public void setDiscinElectronSign(String discinElectronSign) {
		this.discinElectronSign = discinElectronSign;
	}
	
    /**
     * @return discinElectronSign
     */
	public String getDiscinElectronSign() {
		return this.discinElectronSign;
	}
	
	/**
	 * @param sJjh
	 */
	public void setSJjh(String sJjh) {
		this.sJjh = sJjh;
	}
	
    /**
     * @return sJjh
     */
	public String getSJjh() {
		return this.sJjh;
	}
	
	/**
	 * @param sDiscountagreeno
	 */
	public void setSDiscountagreeno(String sDiscountagreeno) {
		this.sDiscountagreeno = sDiscountagreeno;
	}
	
    /**
     * @return sDiscountagreeno
     */
	public String getSDiscountagreeno() {
		return this.sDiscountagreeno;
	}
	
	/**
	 * @param galeDt
	 */
	public void setGaleDt(String galeDt) {
		this.galeDt = galeDt;
	}
	
    /**
     * @return galeDt
     */
	public String getGaleDt() {
		return this.galeDt;
	}
	
	/**
	 * @param busiEndFlag
	 */
	public void setBusiEndFlag(String busiEndFlag) {
		this.busiEndFlag = busiEndFlag;
	}
	
    /**
     * @return busiEndFlag
     */
	public String getBusiEndFlag() {
		return this.busiEndFlag;
	}
	
	/**
	 * @param busiEndType
	 */
	public void setBusiEndType(String busiEndType) {
		this.busiEndType = busiEndType;
	}
	
    /**
     * @return busiEndType
     */
	public String getBusiEndType() {
		return this.busiEndType;
	}
	
	/**
	 * @param busiEndDate
	 */
	public void setBusiEndDate(String busiEndDate) {
		this.busiEndDate = busiEndDate;
	}
	
    /**
     * @return busiEndDate
     */
	public String getBusiEndDate() {
		return this.busiEndDate;
	}
	
	/**
	 * @param acctDate
	 */
	public void setAcctDate(String acctDate) {
		this.acctDate = acctDate;
	}
	
    /**
     * @return acctDate
     */
	public String getAcctDate() {
		return this.acctDate;
	}
	
	/**
	 * @param acctFlowNo
	 */
	public void setAcctFlowNo(String acctFlowNo) {
		this.acctFlowNo = acctFlowNo;
	}
	
    /**
     * @return acctFlowNo
     */
	public String getAcctFlowNo() {
		return this.acctFlowNo;
	}
	
	/**
	 * @param acctUserNo
	 */
	public void setAcctUserNo(String acctUserNo) {
		this.acctUserNo = acctUserNo;
	}
	
    /**
     * @return acctUserNo
     */
	public String getAcctUserNo() {
		return this.acctUserNo;
	}
	
	/**
	 * @param acctUserName
	 */
	public void setAcctUserName(String acctUserName) {
		this.acctUserName = acctUserName;
	}
	
    /**
     * @return acctUserName
     */
	public String getAcctUserName() {
		return this.acctUserName;
	}
	
	/**
	 * @param acctAuthUserNo
	 */
	public void setAcctAuthUserNo(String acctAuthUserNo) {
		this.acctAuthUserNo = acctAuthUserNo;
	}
	
    /**
     * @return acctAuthUserNo
     */
	public String getAcctAuthUserNo() {
		return this.acctAuthUserNo;
	}
	
	/**
	 * @param acctAuthUserName
	 */
	public void setAcctAuthUserName(String acctAuthUserName) {
		this.acctAuthUserName = acctAuthUserName;
	}
	
    /**
     * @return acctAuthUserName
     */
	public String getAcctAuthUserName() {
		return this.acctAuthUserName;
	}
	
	/**
	 * @param cancelAcctUserNo
	 */
	public void setCancelAcctUserNo(String cancelAcctUserNo) {
		this.cancelAcctUserNo = cancelAcctUserNo;
	}
	
    /**
     * @return cancelAcctUserNo
     */
	public String getCancelAcctUserNo() {
		return this.cancelAcctUserNo;
	}
	
	/**
	 * @param cancelAcctUserName
	 */
	public void setCancelAcctUserName(String cancelAcctUserName) {
		this.cancelAcctUserName = cancelAcctUserName;
	}
	
    /**
     * @return cancelAcctUserName
     */
	public String getCancelAcctUserName() {
		return this.cancelAcctUserName;
	}
	
	/**
	 * @param cancelAcctAuthUserNo
	 */
	public void setCancelAcctAuthUserNo(String cancelAcctAuthUserNo) {
		this.cancelAcctAuthUserNo = cancelAcctAuthUserNo;
	}
	
    /**
     * @return cancelAcctAuthUserNo
     */
	public String getCancelAcctAuthUserNo() {
		return this.cancelAcctAuthUserNo;
	}
	
	/**
	 * @param cancelAcctAuthUserName
	 */
	public void setCancelAcctAuthUserName(String cancelAcctAuthUserName) {
		this.cancelAcctAuthUserName = cancelAcctAuthUserName;
	}
	
    /**
     * @return cancelAcctAuthUserName
     */
	public String getCancelAcctAuthUserName() {
		return this.cancelAcctAuthUserName;
	}
	
	/**
	 * @param agentFlag
	 */
	public void setAgentFlag(String agentFlag) {
		this.agentFlag = agentFlag;
	}
	
    /**
     * @return agentFlag
     */
	public String getAgentFlag() {
		return this.agentFlag;
	}
	
	/**
	 * @param accptBankSort
	 */
	public void setAccptBankSort(String accptBankSort) {
		this.accptBankSort = accptBankSort;
	}
	
    /**
     * @return accptBankSort
     */
	public String getAccptBankSort() {
		return this.accptBankSort;
	}
	
	/**
	 * @param discountCostRate
	 */
	public void setDiscountCostRate(java.math.BigDecimal discountCostRate) {
		this.discountCostRate = discountCostRate;
	}
	
    /**
     * @return discountCostRate
     */
	public java.math.BigDecimal getDiscountCostRate() {
		return this.discountCostRate;
	}
	
	/**
	 * @param moveFlag
	 */
	public void setMoveFlag(String moveFlag) {
		this.moveFlag = moveFlag;
	}
	
    /**
     * @return moveFlag
     */
	public String getMoveFlag() {
		return this.moveFlag;
	}
	
	/**
	 * @param moveBranchNo
	 */
	public void setMoveBranchNo(String moveBranchNo) {
		this.moveBranchNo = moveBranchNo;
	}
	
    /**
     * @return moveBranchNo
     */
	public String getMoveBranchNo() {
		return this.moveBranchNo;
	}
	
	/**
	 * @param improveDays
	 */
	public void setImproveDays(java.math.BigDecimal improveDays) {
		this.improveDays = improveDays;
	}
	
    /**
     * @return improveDays
     */
	public java.math.BigDecimal getImproveDays() {
		return this.improveDays;
	}
	
	/**
	 * @param yzFlag
	 */
	public void setYzFlag(String yzFlag) {
		this.yzFlag = yzFlag;
	}
	
    /**
     * @return yzFlag
     */
	public String getYzFlag() {
		return this.yzFlag;
	}
	
	/**
	 * @param coreTrandt
	 */
	public void setCoreTrandt(String coreTrandt) {
		this.coreTrandt = coreTrandt;
	}
	
    /**
     * @return coreTrandt
     */
	public String getCoreTrandt() {
		return this.coreTrandt;
	}
	
	/**
	 * @param coreTransq
	 */
	public void setCoreTransq(String coreTransq) {
		this.coreTransq = coreTransq;
	}
	
    /**
     * @return coreTransq
     */
	public String getCoreTransq() {
		return this.coreTransq;
	}
	
	/**
	 * @param ifCreditFlag
	 */
	public void setIfCreditFlag(String ifCreditFlag) {
		this.ifCreditFlag = ifCreditFlag;
	}
	
    /**
     * @return ifCreditFlag
     */
	public String getIfCreditFlag() {
		return this.ifCreditFlag;
	}


}