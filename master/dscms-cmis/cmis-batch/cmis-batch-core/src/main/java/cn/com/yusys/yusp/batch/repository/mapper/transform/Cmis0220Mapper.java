package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0220</br>
 * 任务名称：加工任务-额度处理-批后额度比对结果处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0220Mapper {
    /**
     * 清空批复额度分项额度比对表
     */
    void truncateTmpCompareLmtDetails();

    /**
     * 插入授信分项额度比对表
     *
     * @param openDay
     * @return
     */
    int insertTmpCompareLmtDetails(@Param("openDay") String openDay);

    /**
     * 清空分项占用关系信息比对表
     */
    void truncateTmpCompareLmtContRel();

    /**
     * 插入分项占用关系信息比对表
     *
     * @param openDay
     * @return
     */
    int insertTmpCompareLmtContRel(@Param("openDay") String openDay);

    /**
     * 清空合同占用关系额度比对表
     */
    void truncateTmpCompareContAccRel();

    /**
     * 插入合同占用关系额度比对表
     *
     * @param openDay
     * @return
     */
    int insertTmpCompareContAccRel(@Param("openDay") String openDay);

    /**
     * 清空白名单额度信息表加工表
     */
    void truncateTmpCompareLmtWhiteInfo();

    /**
     * 插入白名单额度信息表加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpCompareLmtWhiteInfo(@Param("openDay") String openDay);


    /**
     * 清空合作方授信分项信息额度比对表
     */
    void truncateTmpCompareApprCoopSubInfo();

    /**
     * 插入合作方授信分项信息额度比对表
     *
     * @param openDay
     * @return
     */
    int insertTmpCompareApprCoopSubInfo(@Param("openDay") String openDay);
}
