/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.sbs;

import cn.com.yusys.yusp.batch.domain.load.sbs.BatSSbsAvailableZjg;
import cn.com.yusys.yusp.batch.service.load.sbs.BatSSbsAvailableZjgService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSSbsAvailableZjgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 16:20:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batssbsavailablezjg")
public class BatSSbsAvailableZjgResource {
    @Autowired
    private BatSSbsAvailableZjgService batSSbsAvailableZjgService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSSbsAvailableZjg>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSSbsAvailableZjg> list = batSSbsAvailableZjgService.selectAll(queryModel);
        return new ResultDto<List<BatSSbsAvailableZjg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSSbsAvailableZjg>> index(QueryModel queryModel) {
        List<BatSSbsAvailableZjg> list = batSSbsAvailableZjgService.selectByModel(queryModel);
        return new ResultDto<List<BatSSbsAvailableZjg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<BatSSbsAvailableZjg> show(@PathVariable("pkId") String pkId) {
        BatSSbsAvailableZjg batSSbsAvailableZjg = batSSbsAvailableZjgService.selectByPrimaryKey(pkId);
        return new ResultDto<BatSSbsAvailableZjg>(batSSbsAvailableZjg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSSbsAvailableZjg> create(@RequestBody BatSSbsAvailableZjg batSSbsAvailableZjg) throws URISyntaxException {
        batSSbsAvailableZjgService.insert(batSSbsAvailableZjg);
        return new ResultDto<BatSSbsAvailableZjg>(batSSbsAvailableZjg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSSbsAvailableZjg batSSbsAvailableZjg) throws URISyntaxException {
        int result = batSSbsAvailableZjgService.update(batSSbsAvailableZjg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = batSSbsAvailableZjgService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSSbsAvailableZjgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
