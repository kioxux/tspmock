package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0701</br>
 * 任务名称：加工任务-配置管理-更新行名行号对照表  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月18日 下午11:56:54
 */
public interface Cmis0701Mapper {

    /**
     * 更新行名行号对照表
     *
     * @param openDay
     * @return
     */
    int updateCfgBankInfo(@Param("openDay") String openDay);

    /**
     * 插入行名行号对照表
     *
     * @param openDay
     * @return
     */
    int insertCfgBankInfo(@Param("openDay") String openDay);
}
