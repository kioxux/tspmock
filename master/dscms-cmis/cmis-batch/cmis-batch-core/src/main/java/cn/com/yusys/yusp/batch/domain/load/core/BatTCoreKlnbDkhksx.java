/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTCoreKlnbDkhksx
 * @类描述: bat_t_core_klnb_dkhksx数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_core_klnb_dkhksx")
public class BatTCoreKlnbDkhksx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 法人代码 **/
	@Id
	@Column(name = "farendma")
	private String farendma;
	
	/** 贷款借据号 **/
	@Id
	@Column(name = "dkjiejuh")
	private String dkjiejuh;
	
	/** 期供生成方式 STD_QIGSCFSH **/
	@Column(name = "qigscfsh", unique = false, nullable = true, length = 1)
	private String qigscfsh;
	
	/** 期供利息类型 STD_QGLXLEIX **/
	@Column(name = "qglxleix", unique = false, nullable = true, length = 1)
	private String qglxleix;
	
	/** 还款方式 STD_HUANKFSH **/
	@Column(name = "huankfsh", unique = false, nullable = true, length = 5)
	private String huankfsh;
	
	/** 贸融ABS贷款类型 STD_ABSDKULX **/
	@Column(name = "absdkulx", unique = false, nullable = true, length = 1)
	private String absdkulx;
	
	/** 等额处理规则 STD_DECHLIGZ **/
	@Column(name = "dechligz", unique = false, nullable = true, length = 1)
	private String dechligz;
	
	/** 累进首段期数 **/
	@Column(name = "ljsxqish", unique = false, nullable = true, length = 19)
	private Long ljsxqish;
	
	/** 累进值 **/
	@Column(name = "leijinzh", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal leijinzh;
	
	/** 累进区间期数 **/
	@Column(name = "leijqjsh", unique = false, nullable = true, length = 19)
	private Long leijqjsh;
	
	/** 还款周期 **/
	@Column(name = "hkzhouqi", unique = false, nullable = true, length = 8)
	private String hkzhouqi;
	
	/** 多频率还款标志 STD_YESORNO **/
	@Column(name = "duophkbz", unique = false, nullable = true, length = 1)
	private String duophkbz;
	
	/** 还本周期 **/
	@Column(name = "huanbzhq", unique = false, nullable = true, length = 8)
	private String huanbzhq;
	
	/** 逾期还款周期 **/
	@Column(name = "yuqhkzhq", unique = false, nullable = true, length = 8)
	private String yuqhkzhq;
	
	/** 顺延利息 **/
	@Column(name = "shunylix", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal shunylix;
	
	/** 还款期限 **/
	@Column(name = "hkqixian", unique = false, nullable = true, length = 20)
	private String hkqixian;
	
	/** 保留金额 **/
	@Column(name = "baoliuje", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal baoliuje;
	
	/** 每期还款总额 **/
	@Column(name = "meiqhkze", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal meiqhkze;
	
	/** 每期还本金额 **/
	@Column(name = "meiqhbje", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal meiqhbje;
	
	/** 每期还本比例 **/
	@Column(name = "mqhbbili", unique = false, nullable = true, length = 6)
	private java.math.BigDecimal mqhbbili;
	
	/** 本金分段 STD_BENJINFD **/
	@Column(name = "benjinfd", unique = false, nullable = true, length = 1)
	private String benjinfd;
	
	/** 当前本金段数 **/
	@Column(name = "danqbjds", unique = false, nullable = true, length = 19)
	private Long danqbjds;
	
	/** 定制还款计划 STD_YESORNO **/
	@Column(name = "dzhhkjih", unique = false, nullable = true, length = 1)
	private String dzhhkjih;
	
	/** 总期数 **/
	@Column(name = "zongqish", unique = false, nullable = true, length = 19)
	private Long zongqish;
	
	/** 本期期数 **/
	@Column(name = "benqqish", unique = false, nullable = true, length = 19)
	private Long benqqish;
	
	/** 本期子期数 **/
	@Column(name = "benqizqs", unique = false, nullable = true, length = 19)
	private Long benqizqs;
	
	/** 计息总期数 **/
	@Column(name = "jixizqsh", unique = false, nullable = true, length = 19)
	private Long jixizqsh;
	
	/** 计息期数 **/
	@Column(name = "jixiqish", unique = false, nullable = true, length = 19)
	private Long jixiqish;
	
	/** 计息本金 **/
	@Column(name = "jixibenj", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal jixibenj;
	
	/** 还款顺序编号 **/
	@Column(name = "hkshxubh", unique = false, nullable = true, length = 30)
	private String hkshxubh;
	
	/** 内部借据扣款方式 STD_NBJJKKFS **/
	@Column(name = "nbjjkkfs", unique = false, nullable = true, length = 1)
	private String nbjjkkfs;
	
	/** 允许提前还款 STD_YXTSFKBZ **/
	@Column(name = "yunxtqhk", unique = false, nullable = true, length = 1)
	private String yunxtqhk;
	
	/** 提前还款并调整计划方式 STD_TQHKTZFS **/
	@Column(name = "tqhktzfs", unique = false, nullable = true, length = 1)
	private String tqhktzfs;
	
	/** 提前还款还息方式 STD_TQHKHXFS **/
	@Column(name = "tqhkhxfs", unique = false, nullable = true, length = 1)
	private String tqhkhxfs;
	
	/** 提前还款是否还贴息 STD_YESORNO **/
	@Column(name = "tqhktiex", unique = false, nullable = true, length = 1)
	private String tqhktiex;
	
	/** 期限变更调整计划 STD_TZJH **/
	@Column(name = "qxbgtzjh", unique = false, nullable = true, length = 1)
	private String qxbgtzjh;
	
	/** 利率变更调整计划 STD_TZJH **/
	@Column(name = "llbgtzjh", unique = false, nullable = true, length = 1)
	private String llbgtzjh;
	
	/** 多次放款调整计划 STD_TZJH **/
	@Column(name = "dcfktzjh", unique = false, nullable = true, length = 1)
	private String dcfktzjh;
	
	/** 提前还款调整计划 STD_TZJH **/
	@Column(name = "tqhktzjh", unique = false, nullable = true, length = 1)
	private String tqhktzjh;
	
	/** 自动扣款标志 STD_YESORNO **/
	@Column(name = "zdkoukbz", unique = false, nullable = true, length = 1)
	private String zdkoukbz;
	
	/** 自动结清贷款标志 STD_YESORNO **/
	@Column(name = "zdjqdkbz", unique = false, nullable = true, length = 1)
	private String zdjqdkbz;
	
	/** 指定文件批量扣款标识 STD_YESORNO **/
	@Column(name = "zdplkkbz", unique = false, nullable = true, length = 1)
	private String zdplkkbz;
	
	/** 多还款账户标志 STD_YESORNO **/
	@Column(name = "dhkzhhbz", unique = false, nullable = true, length = 1)
	private String dhkzhhbz;
	
	/** 还款账号 **/
	@Column(name = "huankzhh", unique = false, nullable = true, length = 35)
	private String huankzhh;
	
	/** 还款账号子序号 **/
	@Column(name = "hkzhhzxh", unique = false, nullable = true, length = 8)
	private String hkzhhzxh;
	
	/** 签约循环贷款标志 STD_YESORNO **/
	@Column(name = "qyxhdkbz", unique = false, nullable = true, length = 1)
	private String qyxhdkbz;
	
	/** 循环贷款签约账号 **/
	@Column(name = "xhdkqyzh", unique = false, nullable = true, length = 35)
	private String xhdkqyzh;
	
	/** 循环贷款签约账号子序号 **/
	@Column(name = "xhdkzhxh", unique = false, nullable = true, length = 8)
	private String xhdkzhxh;
	
	/** 提前出账标志 STD_YESORNO **/
	@Column(name = "tqchzhbz", unique = false, nullable = true, length = 1)
	private String tqchzhbz;
	
	/** 提前出账天数 **/
	@Column(name = "tqchzhts", unique = false, nullable = true, length = 19)
	private Long tqchzhts;
	
	/** 提前出账利息 **/
	@Column(name = "tqchzhlx", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal tqchzhlx;
	
	/** 结息方式 STD_JIEXIFSH **/
	@Column(name = "jiexifsh", unique = false, nullable = true, length = 1)
	private String jiexifsh;
	
	/** 欠款还款时段 STD_QKHKPLSD **/
	@Column(name = "qkhkplsd", unique = false, nullable = true, length = 1)
	private String qkhkplsd;
	
	/** 允许调整还款方式 STD_YESORNO **/
	@Column(name = "tiaozhkf", unique = false, nullable = true, length = 1)
	private String tiaozhkf;
	
	/** 首次还款日确定模式 STD_SCIHKRBZ **/
	@Column(name = "scihkrbz", unique = false, nullable = true, length = 1)
	private String scihkrbz;
	
	/** 末期还款方式 STD_MQIHKFSH **/
	@Column(name = "mqihkfsh", unique = false, nullable = true, length = 1)
	private String mqihkfsh;
	
	/** 允许缩期 STD_YESORNO **/
	@Column(name = "yunxsuoq", unique = false, nullable = true, length = 1)
	private String yunxsuoq;
	
	/** 缩期次数 **/
	@Column(name = "suoqcish", unique = false, nullable = true, length = 19)
	private Long suoqcish;
	
	/** 不足额扣款方式 STD_BZUEKKFS **/
	@Column(name = "bzuekkfs", unique = false, nullable = true, length = 1)
	private String bzuekkfs;
	
	/** 逾期不足额扣款方式 STD_BZUEKKFS **/
	@Column(name = "yqbzkkfs", unique = false, nullable = true, length = 1)
	private String yqbzkkfs;
	
	/** 欠息时是否减少额度 STD_YESORNO **/
	@Column(name = "qxsfjsed", unique = false, nullable = true, length = 1)
	private String qxsfjsed;
	
	/** 节假日利息顺延标志 STD_YESORNO **/
	@Column(name = "jrlxsybz", unique = false, nullable = true, length = 1)
	private String jrlxsybz;
	
	/** 允许现金还款 STD_YESORNO **/
	@Column(name = "xianjhku", unique = false, nullable = true, length = 1)
	private String xianjhku;
	
	/** 允许行内同名账户还款 STD_YESORNO **/
	@Column(name = "hntmihku", unique = false, nullable = true, length = 1)
	private String hntmihku;
	
	/** 允许行内非同名帐户还款 STD_YESORNO **/
	@Column(name = "hnftmhku", unique = false, nullable = true, length = 1)
	private String hnftmhku;
	
	/** 允许内部账户还款 STD_YESORNO **/
	@Column(name = "nbuzhhku", unique = false, nullable = true, length = 1)
	private String nbuzhhku;
	
	/** 返还合格性编号 **/
	@Column(name = "fanhhgbh", unique = false, nullable = true, length = 30)
	private String fanhhgbh;
	
	/** 返还方式 STD_FANHUAFS **/
	@Column(name = "fanhuafs", unique = false, nullable = true, length = 1)
	private String fanhuafs;
	
	/** 返还频率 **/
	@Column(name = "fanhpilv", unique = false, nullable = true, length = 8)
	private String fanhpilv;
	
	/** 返还计算方式 STD_FANHJSFS **/
	@Column(name = "fanhjsfs", unique = false, nullable = true, length = 1)
	private String fanhjsfs;
	
	/** 返还金额或比例 **/
	@Column(name = "fanhjebl", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal fanhjebl;
	
	/** 提前还款锁定期 **/
	@Column(name = "tiqhksdq", unique = false, nullable = true, length = 19)
	private Long tiqhksdq;
	
	/** 本年提前还款次数累计 **/
	@Column(name = "bntqhkcs", unique = false, nullable = true, length = 19)
	private Long bntqhkcs;
	
	/** 期限内提前还款次数累计 **/
	@Column(name = "qxtqhkcs", unique = false, nullable = true, length = 19)
	private Long qxtqhkcs;
	
	/** 每年可提前还款最大次数 **/
	@Column(name = "mntqhkcs", unique = false, nullable = true, length = 19)
	private Long mntqhkcs;
	
	/** 期限内提前还款最大次数 **/
	@Column(name = "cqtqhkcs", unique = false, nullable = true, length = 19)
	private Long cqtqhkcs;
	
	/** 提前还款最低金额规则 STD_TQHKZDFS **/
	@Column(name = "tqhkzdfs", unique = false, nullable = true, length = 1)
	private String tqhkzdfs;
	
	/** 提前还款最低金额取值 **/
	@Column(name = "tqhkzdje", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal tqhkzdje;
	
	/** 提前还款最高金额规则 STD_TQHKZDFS **/
	@Column(name = "tqhkzgfs", unique = false, nullable = true, length = 1)
	private String tqhkzgfs;
	
	/** 提前还款最高金额取值 **/
	@Column(name = "tqhkzgje", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal tqhkzgje;
	
	/** 是否允许部分还款 STD_YESORNO **/
	@Column(name = "yxbufhuk", unique = false, nullable = true, length = 1)
	private String yxbufhuk;
	
	/** 部分还款最低金额规则 STD_TQHKZDFS **/
	@Column(name = "bhzdjefs", unique = false, nullable = true, length = 1)
	private String bhzdjefs;
	
	/** 设置部分还款可调整限额 STD_YESORNO **/
	@Column(name = "bfhkktxe", unique = false, nullable = true, length = 1)
	private String bfhkktxe;
	
	/** 部分还款可调整限额规则 STD_TQHKZDFS **/
	@Column(name = "bfhkktje", unique = false, nullable = true, length = 1)
	private String bfhkktje;
	
	/** 部分还款最低金额或比例 **/
	@Column(name = "bfhkzdje", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal bfhkzdje;
	
	/** 是否有宽限期 STD_YESORNO **/
	@Column(name = "sfyxkuxq", unique = false, nullable = true, length = 1)
	private String sfyxkuxq;
	
	/** 宽限期天数 **/
	@Column(name = "kuanxqts", unique = false, nullable = true, length = 19)
	private Long kuanxqts;
	
	/** 本金宽限期天数 **/
	@Column(name = "bjkuanxq", unique = false, nullable = true, length = 19)
	private Long bjkuanxq;
	
	/** 利息宽限期天数 **/
	@Column(name = "lxkuanxq", unique = false, nullable = true, length = 19)
	private Long lxkuanxq;
	
	/** 宽限期最大次数 **/
	@Column(name = "kxqzdcsh", unique = false, nullable = true, length = 19)
	private Long kxqzdcsh;
	
	/** 宽限期计息方式 STD_KXQJIXGZ **/
	@Column(name = "kxqjixgz", unique = false, nullable = true, length = 1)
	private String kxqjixgz;
	
	/** 宽限期后计息规则 STD_KXQHJXGZ **/
	@Column(name = "kxqhjxgz", unique = false, nullable = true, length = 1)
	private String kxqhjxgz;
	
	/** 宽限期收息规则 STD_KXQSHXGZ **/
	@Column(name = "kxqshxgz", unique = false, nullable = true, length = 1)
	private String kxqshxgz;
	
	/** 宽限期转逾期规则 STD_KXQZYQGZ **/
	@Column(name = "kxqzyqgz", unique = false, nullable = true, length = 1)
	private String kxqzyqgz;
	
	/** 宽限期节假日规则 STD_KXQJJRGZ **/
	@Column(name = "kxqjjrgz", unique = false, nullable = true, length = 1)
	private String kxqjjrgz;
	
	/** 还款汇率牌价类型 STD_HKHLPJLX **/
	@Column(name = "hkhlpjlx", unique = false, nullable = true, length = 1)
	private String hkhlpjlx;
	
	/** 还款折算汇率 **/
	@Column(name = "hkzshuil", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal hkzshuil;
	
	/** 汇率种类 STD_HUILZLEI **/
	@Column(name = "huilzlei", unique = false, nullable = true, length = 1)
	private String huilzlei;
	
	/** 上次还款日 **/
	@Column(name = "schkriqi", unique = false, nullable = true, length = 8)
	private String schkriqi;
	
	/** 下一次还款日 **/
	@Column(name = "xycihkrq", unique = false, nullable = true, length = 8)
	private String xycihkrq;
	
	/** 核销贷款自动扣款标志 STD_YESORNO **/
	@Column(name = "hxzdkkbz", unique = false, nullable = true, length = 1)
	private String hxzdkkbz;
	
	/** 逾期自动追缴标志 STD_YESORNO **/
	@Column(name = "yqzdzjbz", unique = false, nullable = true, length = 1)
	private String yqzdzjbz;
	
	/** 日初扣款标志 STD_YESORNO **/
	@Column(name = "rchukkbz", unique = false, nullable = true, length = 1)
	private String rchukkbz;
	
	/** 分行标识 **/
	@Column(name = "fenhbios", unique = false, nullable = false, length = 4)
	private String fenhbios;
	
	/** 维护柜员 **/
	@Column(name = "WEIHGUIY", unique = false, nullable = true, length = 8)
	private String weihguiy;
	
	/** 维护机构 **/
	@Column(name = "weihjigo", unique = false, nullable = false, length = 12)
	private String weihjigo;
	
	/** 维护日期 **/
	@Column(name = "weihriqi", unique = false, nullable = false, length = 8)
	private String weihriqi;
	
	/** 维护时间 **/
	@Column(name = "weihshij", unique = false, nullable = true, length = 9)
	private String weihshij;
	
	/** 时间戳 **/
	@Column(name = "shijchuo", unique = false, nullable = false, length = 19)
	private Long shijchuo;
	
	/** 记录状态 STD_JILUZTAI **/
	@Column(name = "jiluztai", unique = false, nullable = false, length = 1)
	private String jiluztai;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param qigscfsh
	 */
	public void setQigscfsh(String qigscfsh) {
		this.qigscfsh = qigscfsh;
	}
	
    /**
     * @return qigscfsh
     */
	public String getQigscfsh() {
		return this.qigscfsh;
	}
	
	/**
	 * @param qglxleix
	 */
	public void setQglxleix(String qglxleix) {
		this.qglxleix = qglxleix;
	}
	
    /**
     * @return qglxleix
     */
	public String getQglxleix() {
		return this.qglxleix;
	}
	
	/**
	 * @param huankfsh
	 */
	public void setHuankfsh(String huankfsh) {
		this.huankfsh = huankfsh;
	}
	
    /**
     * @return huankfsh
     */
	public String getHuankfsh() {
		return this.huankfsh;
	}
	
	/**
	 * @param absdkulx
	 */
	public void setAbsdkulx(String absdkulx) {
		this.absdkulx = absdkulx;
	}
	
    /**
     * @return absdkulx
     */
	public String getAbsdkulx() {
		return this.absdkulx;
	}
	
	/**
	 * @param dechligz
	 */
	public void setDechligz(String dechligz) {
		this.dechligz = dechligz;
	}
	
    /**
     * @return dechligz
     */
	public String getDechligz() {
		return this.dechligz;
	}
	
	/**
	 * @param ljsxqish
	 */
	public void setLjsxqish(Long ljsxqish) {
		this.ljsxqish = ljsxqish;
	}
	
    /**
     * @return ljsxqish
     */
	public Long getLjsxqish() {
		return this.ljsxqish;
	}
	
	/**
	 * @param leijinzh
	 */
	public void setLeijinzh(java.math.BigDecimal leijinzh) {
		this.leijinzh = leijinzh;
	}
	
    /**
     * @return leijinzh
     */
	public java.math.BigDecimal getLeijinzh() {
		return this.leijinzh;
	}
	
	/**
	 * @param leijqjsh
	 */
	public void setLeijqjsh(Long leijqjsh) {
		this.leijqjsh = leijqjsh;
	}
	
    /**
     * @return leijqjsh
     */
	public Long getLeijqjsh() {
		return this.leijqjsh;
	}
	
	/**
	 * @param hkzhouqi
	 */
	public void setHkzhouqi(String hkzhouqi) {
		this.hkzhouqi = hkzhouqi;
	}
	
    /**
     * @return hkzhouqi
     */
	public String getHkzhouqi() {
		return this.hkzhouqi;
	}
	
	/**
	 * @param duophkbz
	 */
	public void setDuophkbz(String duophkbz) {
		this.duophkbz = duophkbz;
	}
	
    /**
     * @return duophkbz
     */
	public String getDuophkbz() {
		return this.duophkbz;
	}
	
	/**
	 * @param huanbzhq
	 */
	public void setHuanbzhq(String huanbzhq) {
		this.huanbzhq = huanbzhq;
	}
	
    /**
     * @return huanbzhq
     */
	public String getHuanbzhq() {
		return this.huanbzhq;
	}
	
	/**
	 * @param yuqhkzhq
	 */
	public void setYuqhkzhq(String yuqhkzhq) {
		this.yuqhkzhq = yuqhkzhq;
	}
	
    /**
     * @return yuqhkzhq
     */
	public String getYuqhkzhq() {
		return this.yuqhkzhq;
	}
	
	/**
	 * @param shunylix
	 */
	public void setShunylix(java.math.BigDecimal shunylix) {
		this.shunylix = shunylix;
	}
	
    /**
     * @return shunylix
     */
	public java.math.BigDecimal getShunylix() {
		return this.shunylix;
	}
	
	/**
	 * @param hkqixian
	 */
	public void setHkqixian(String hkqixian) {
		this.hkqixian = hkqixian;
	}
	
    /**
     * @return hkqixian
     */
	public String getHkqixian() {
		return this.hkqixian;
	}
	
	/**
	 * @param baoliuje
	 */
	public void setBaoliuje(java.math.BigDecimal baoliuje) {
		this.baoliuje = baoliuje;
	}
	
    /**
     * @return baoliuje
     */
	public java.math.BigDecimal getBaoliuje() {
		return this.baoliuje;
	}
	
	/**
	 * @param meiqhkze
	 */
	public void setMeiqhkze(java.math.BigDecimal meiqhkze) {
		this.meiqhkze = meiqhkze;
	}
	
    /**
     * @return meiqhkze
     */
	public java.math.BigDecimal getMeiqhkze() {
		return this.meiqhkze;
	}
	
	/**
	 * @param meiqhbje
	 */
	public void setMeiqhbje(java.math.BigDecimal meiqhbje) {
		this.meiqhbje = meiqhbje;
	}
	
    /**
     * @return meiqhbje
     */
	public java.math.BigDecimal getMeiqhbje() {
		return this.meiqhbje;
	}
	
	/**
	 * @param mqhbbili
	 */
	public void setMqhbbili(java.math.BigDecimal mqhbbili) {
		this.mqhbbili = mqhbbili;
	}
	
    /**
     * @return mqhbbili
     */
	public java.math.BigDecimal getMqhbbili() {
		return this.mqhbbili;
	}
	
	/**
	 * @param benjinfd
	 */
	public void setBenjinfd(String benjinfd) {
		this.benjinfd = benjinfd;
	}
	
    /**
     * @return benjinfd
     */
	public String getBenjinfd() {
		return this.benjinfd;
	}
	
	/**
	 * @param danqbjds
	 */
	public void setDanqbjds(Long danqbjds) {
		this.danqbjds = danqbjds;
	}
	
    /**
     * @return danqbjds
     */
	public Long getDanqbjds() {
		return this.danqbjds;
	}
	
	/**
	 * @param dzhhkjih
	 */
	public void setDzhhkjih(String dzhhkjih) {
		this.dzhhkjih = dzhhkjih;
	}
	
    /**
     * @return dzhhkjih
     */
	public String getDzhhkjih() {
		return this.dzhhkjih;
	}
	
	/**
	 * @param zongqish
	 */
	public void setZongqish(Long zongqish) {
		this.zongqish = zongqish;
	}
	
    /**
     * @return zongqish
     */
	public Long getZongqish() {
		return this.zongqish;
	}
	
	/**
	 * @param benqqish
	 */
	public void setBenqqish(Long benqqish) {
		this.benqqish = benqqish;
	}
	
    /**
     * @return benqqish
     */
	public Long getBenqqish() {
		return this.benqqish;
	}
	
	/**
	 * @param benqizqs
	 */
	public void setBenqizqs(Long benqizqs) {
		this.benqizqs = benqizqs;
	}
	
    /**
     * @return benqizqs
     */
	public Long getBenqizqs() {
		return this.benqizqs;
	}
	
	/**
	 * @param jixizqsh
	 */
	public void setJixizqsh(Long jixizqsh) {
		this.jixizqsh = jixizqsh;
	}
	
    /**
     * @return jixizqsh
     */
	public Long getJixizqsh() {
		return this.jixizqsh;
	}
	
	/**
	 * @param jixiqish
	 */
	public void setJixiqish(Long jixiqish) {
		this.jixiqish = jixiqish;
	}
	
    /**
     * @return jixiqish
     */
	public Long getJixiqish() {
		return this.jixiqish;
	}
	
	/**
	 * @param jixibenj
	 */
	public void setJixibenj(java.math.BigDecimal jixibenj) {
		this.jixibenj = jixibenj;
	}
	
    /**
     * @return jixibenj
     */
	public java.math.BigDecimal getJixibenj() {
		return this.jixibenj;
	}
	
	/**
	 * @param hkshxubh
	 */
	public void setHkshxubh(String hkshxubh) {
		this.hkshxubh = hkshxubh;
	}
	
    /**
     * @return hkshxubh
     */
	public String getHkshxubh() {
		return this.hkshxubh;
	}
	
	/**
	 * @param nbjjkkfs
	 */
	public void setNbjjkkfs(String nbjjkkfs) {
		this.nbjjkkfs = nbjjkkfs;
	}
	
    /**
     * @return nbjjkkfs
     */
	public String getNbjjkkfs() {
		return this.nbjjkkfs;
	}
	
	/**
	 * @param yunxtqhk
	 */
	public void setYunxtqhk(String yunxtqhk) {
		this.yunxtqhk = yunxtqhk;
	}
	
    /**
     * @return yunxtqhk
     */
	public String getYunxtqhk() {
		return this.yunxtqhk;
	}
	
	/**
	 * @param tqhktzfs
	 */
	public void setTqhktzfs(String tqhktzfs) {
		this.tqhktzfs = tqhktzfs;
	}
	
    /**
     * @return tqhktzfs
     */
	public String getTqhktzfs() {
		return this.tqhktzfs;
	}
	
	/**
	 * @param tqhkhxfs
	 */
	public void setTqhkhxfs(String tqhkhxfs) {
		this.tqhkhxfs = tqhkhxfs;
	}
	
    /**
     * @return tqhkhxfs
     */
	public String getTqhkhxfs() {
		return this.tqhkhxfs;
	}
	
	/**
	 * @param tqhktiex
	 */
	public void setTqhktiex(String tqhktiex) {
		this.tqhktiex = tqhktiex;
	}
	
    /**
     * @return tqhktiex
     */
	public String getTqhktiex() {
		return this.tqhktiex;
	}
	
	/**
	 * @param qxbgtzjh
	 */
	public void setQxbgtzjh(String qxbgtzjh) {
		this.qxbgtzjh = qxbgtzjh;
	}
	
    /**
     * @return qxbgtzjh
     */
	public String getQxbgtzjh() {
		return this.qxbgtzjh;
	}
	
	/**
	 * @param llbgtzjh
	 */
	public void setLlbgtzjh(String llbgtzjh) {
		this.llbgtzjh = llbgtzjh;
	}
	
    /**
     * @return llbgtzjh
     */
	public String getLlbgtzjh() {
		return this.llbgtzjh;
	}
	
	/**
	 * @param dcfktzjh
	 */
	public void setDcfktzjh(String dcfktzjh) {
		this.dcfktzjh = dcfktzjh;
	}
	
    /**
     * @return dcfktzjh
     */
	public String getDcfktzjh() {
		return this.dcfktzjh;
	}
	
	/**
	 * @param tqhktzjh
	 */
	public void setTqhktzjh(String tqhktzjh) {
		this.tqhktzjh = tqhktzjh;
	}
	
    /**
     * @return tqhktzjh
     */
	public String getTqhktzjh() {
		return this.tqhktzjh;
	}
	
	/**
	 * @param zdkoukbz
	 */
	public void setZdkoukbz(String zdkoukbz) {
		this.zdkoukbz = zdkoukbz;
	}
	
    /**
     * @return zdkoukbz
     */
	public String getZdkoukbz() {
		return this.zdkoukbz;
	}
	
	/**
	 * @param zdjqdkbz
	 */
	public void setZdjqdkbz(String zdjqdkbz) {
		this.zdjqdkbz = zdjqdkbz;
	}
	
    /**
     * @return zdjqdkbz
     */
	public String getZdjqdkbz() {
		return this.zdjqdkbz;
	}
	
	/**
	 * @param zdplkkbz
	 */
	public void setZdplkkbz(String zdplkkbz) {
		this.zdplkkbz = zdplkkbz;
	}
	
    /**
     * @return zdplkkbz
     */
	public String getZdplkkbz() {
		return this.zdplkkbz;
	}
	
	/**
	 * @param dhkzhhbz
	 */
	public void setDhkzhhbz(String dhkzhhbz) {
		this.dhkzhhbz = dhkzhhbz;
	}
	
    /**
     * @return dhkzhhbz
     */
	public String getDhkzhhbz() {
		return this.dhkzhhbz;
	}
	
	/**
	 * @param huankzhh
	 */
	public void setHuankzhh(String huankzhh) {
		this.huankzhh = huankzhh;
	}
	
    /**
     * @return huankzhh
     */
	public String getHuankzhh() {
		return this.huankzhh;
	}
	
	/**
	 * @param hkzhhzxh
	 */
	public void setHkzhhzxh(String hkzhhzxh) {
		this.hkzhhzxh = hkzhhzxh;
	}
	
    /**
     * @return hkzhhzxh
     */
	public String getHkzhhzxh() {
		return this.hkzhhzxh;
	}
	
	/**
	 * @param qyxhdkbz
	 */
	public void setQyxhdkbz(String qyxhdkbz) {
		this.qyxhdkbz = qyxhdkbz;
	}
	
    /**
     * @return qyxhdkbz
     */
	public String getQyxhdkbz() {
		return this.qyxhdkbz;
	}
	
	/**
	 * @param xhdkqyzh
	 */
	public void setXhdkqyzh(String xhdkqyzh) {
		this.xhdkqyzh = xhdkqyzh;
	}
	
    /**
     * @return xhdkqyzh
     */
	public String getXhdkqyzh() {
		return this.xhdkqyzh;
	}
	
	/**
	 * @param xhdkzhxh
	 */
	public void setXhdkzhxh(String xhdkzhxh) {
		this.xhdkzhxh = xhdkzhxh;
	}
	
    /**
     * @return xhdkzhxh
     */
	public String getXhdkzhxh() {
		return this.xhdkzhxh;
	}
	
	/**
	 * @param tqchzhbz
	 */
	public void setTqchzhbz(String tqchzhbz) {
		this.tqchzhbz = tqchzhbz;
	}
	
    /**
     * @return tqchzhbz
     */
	public String getTqchzhbz() {
		return this.tqchzhbz;
	}
	
	/**
	 * @param tqchzhts
	 */
	public void setTqchzhts(Long tqchzhts) {
		this.tqchzhts = tqchzhts;
	}
	
    /**
     * @return tqchzhts
     */
	public Long getTqchzhts() {
		return this.tqchzhts;
	}
	
	/**
	 * @param tqchzhlx
	 */
	public void setTqchzhlx(java.math.BigDecimal tqchzhlx) {
		this.tqchzhlx = tqchzhlx;
	}
	
    /**
     * @return tqchzhlx
     */
	public java.math.BigDecimal getTqchzhlx() {
		return this.tqchzhlx;
	}
	
	/**
	 * @param jiexifsh
	 */
	public void setJiexifsh(String jiexifsh) {
		this.jiexifsh = jiexifsh;
	}
	
    /**
     * @return jiexifsh
     */
	public String getJiexifsh() {
		return this.jiexifsh;
	}
	
	/**
	 * @param qkhkplsd
	 */
	public void setQkhkplsd(String qkhkplsd) {
		this.qkhkplsd = qkhkplsd;
	}
	
    /**
     * @return qkhkplsd
     */
	public String getQkhkplsd() {
		return this.qkhkplsd;
	}
	
	/**
	 * @param tiaozhkf
	 */
	public void setTiaozhkf(String tiaozhkf) {
		this.tiaozhkf = tiaozhkf;
	}
	
    /**
     * @return tiaozhkf
     */
	public String getTiaozhkf() {
		return this.tiaozhkf;
	}
	
	/**
	 * @param scihkrbz
	 */
	public void setScihkrbz(String scihkrbz) {
		this.scihkrbz = scihkrbz;
	}
	
    /**
     * @return scihkrbz
     */
	public String getScihkrbz() {
		return this.scihkrbz;
	}
	
	/**
	 * @param mqihkfsh
	 */
	public void setMqihkfsh(String mqihkfsh) {
		this.mqihkfsh = mqihkfsh;
	}
	
    /**
     * @return mqihkfsh
     */
	public String getMqihkfsh() {
		return this.mqihkfsh;
	}
	
	/**
	 * @param yunxsuoq
	 */
	public void setYunxsuoq(String yunxsuoq) {
		this.yunxsuoq = yunxsuoq;
	}
	
    /**
     * @return yunxsuoq
     */
	public String getYunxsuoq() {
		return this.yunxsuoq;
	}
	
	/**
	 * @param suoqcish
	 */
	public void setSuoqcish(Long suoqcish) {
		this.suoqcish = suoqcish;
	}
	
    /**
     * @return suoqcish
     */
	public Long getSuoqcish() {
		return this.suoqcish;
	}
	
	/**
	 * @param bzuekkfs
	 */
	public void setBzuekkfs(String bzuekkfs) {
		this.bzuekkfs = bzuekkfs;
	}
	
    /**
     * @return bzuekkfs
     */
	public String getBzuekkfs() {
		return this.bzuekkfs;
	}
	
	/**
	 * @param yqbzkkfs
	 */
	public void setYqbzkkfs(String yqbzkkfs) {
		this.yqbzkkfs = yqbzkkfs;
	}
	
    /**
     * @return yqbzkkfs
     */
	public String getYqbzkkfs() {
		return this.yqbzkkfs;
	}
	
	/**
	 * @param qxsfjsed
	 */
	public void setQxsfjsed(String qxsfjsed) {
		this.qxsfjsed = qxsfjsed;
	}
	
    /**
     * @return qxsfjsed
     */
	public String getQxsfjsed() {
		return this.qxsfjsed;
	}
	
	/**
	 * @param jrlxsybz
	 */
	public void setJrlxsybz(String jrlxsybz) {
		this.jrlxsybz = jrlxsybz;
	}
	
    /**
     * @return jrlxsybz
     */
	public String getJrlxsybz() {
		return this.jrlxsybz;
	}
	
	/**
	 * @param xianjhku
	 */
	public void setXianjhku(String xianjhku) {
		this.xianjhku = xianjhku;
	}
	
    /**
     * @return xianjhku
     */
	public String getXianjhku() {
		return this.xianjhku;
	}
	
	/**
	 * @param hntmihku
	 */
	public void setHntmihku(String hntmihku) {
		this.hntmihku = hntmihku;
	}
	
    /**
     * @return hntmihku
     */
	public String getHntmihku() {
		return this.hntmihku;
	}
	
	/**
	 * @param hnftmhku
	 */
	public void setHnftmhku(String hnftmhku) {
		this.hnftmhku = hnftmhku;
	}
	
    /**
     * @return hnftmhku
     */
	public String getHnftmhku() {
		return this.hnftmhku;
	}
	
	/**
	 * @param nbuzhhku
	 */
	public void setNbuzhhku(String nbuzhhku) {
		this.nbuzhhku = nbuzhhku;
	}
	
    /**
     * @return nbuzhhku
     */
	public String getNbuzhhku() {
		return this.nbuzhhku;
	}
	
	/**
	 * @param fanhhgbh
	 */
	public void setFanhhgbh(String fanhhgbh) {
		this.fanhhgbh = fanhhgbh;
	}
	
    /**
     * @return fanhhgbh
     */
	public String getFanhhgbh() {
		return this.fanhhgbh;
	}
	
	/**
	 * @param fanhuafs
	 */
	public void setFanhuafs(String fanhuafs) {
		this.fanhuafs = fanhuafs;
	}
	
    /**
     * @return fanhuafs
     */
	public String getFanhuafs() {
		return this.fanhuafs;
	}
	
	/**
	 * @param fanhpilv
	 */
	public void setFanhpilv(String fanhpilv) {
		this.fanhpilv = fanhpilv;
	}
	
    /**
     * @return fanhpilv
     */
	public String getFanhpilv() {
		return this.fanhpilv;
	}
	
	/**
	 * @param fanhjsfs
	 */
	public void setFanhjsfs(String fanhjsfs) {
		this.fanhjsfs = fanhjsfs;
	}
	
    /**
     * @return fanhjsfs
     */
	public String getFanhjsfs() {
		return this.fanhjsfs;
	}
	
	/**
	 * @param fanhjebl
	 */
	public void setFanhjebl(java.math.BigDecimal fanhjebl) {
		this.fanhjebl = fanhjebl;
	}
	
    /**
     * @return fanhjebl
     */
	public java.math.BigDecimal getFanhjebl() {
		return this.fanhjebl;
	}
	
	/**
	 * @param tiqhksdq
	 */
	public void setTiqhksdq(Long tiqhksdq) {
		this.tiqhksdq = tiqhksdq;
	}
	
    /**
     * @return tiqhksdq
     */
	public Long getTiqhksdq() {
		return this.tiqhksdq;
	}
	
	/**
	 * @param bntqhkcs
	 */
	public void setBntqhkcs(Long bntqhkcs) {
		this.bntqhkcs = bntqhkcs;
	}
	
    /**
     * @return bntqhkcs
     */
	public Long getBntqhkcs() {
		return this.bntqhkcs;
	}
	
	/**
	 * @param qxtqhkcs
	 */
	public void setQxtqhkcs(Long qxtqhkcs) {
		this.qxtqhkcs = qxtqhkcs;
	}
	
    /**
     * @return qxtqhkcs
     */
	public Long getQxtqhkcs() {
		return this.qxtqhkcs;
	}
	
	/**
	 * @param mntqhkcs
	 */
	public void setMntqhkcs(Long mntqhkcs) {
		this.mntqhkcs = mntqhkcs;
	}
	
    /**
     * @return mntqhkcs
     */
	public Long getMntqhkcs() {
		return this.mntqhkcs;
	}
	
	/**
	 * @param cqtqhkcs
	 */
	public void setCqtqhkcs(Long cqtqhkcs) {
		this.cqtqhkcs = cqtqhkcs;
	}
	
    /**
     * @return cqtqhkcs
     */
	public Long getCqtqhkcs() {
		return this.cqtqhkcs;
	}
	
	/**
	 * @param tqhkzdfs
	 */
	public void setTqhkzdfs(String tqhkzdfs) {
		this.tqhkzdfs = tqhkzdfs;
	}
	
    /**
     * @return tqhkzdfs
     */
	public String getTqhkzdfs() {
		return this.tqhkzdfs;
	}
	
	/**
	 * @param tqhkzdje
	 */
	public void setTqhkzdje(java.math.BigDecimal tqhkzdje) {
		this.tqhkzdje = tqhkzdje;
	}
	
    /**
     * @return tqhkzdje
     */
	public java.math.BigDecimal getTqhkzdje() {
		return this.tqhkzdje;
	}
	
	/**
	 * @param tqhkzgfs
	 */
	public void setTqhkzgfs(String tqhkzgfs) {
		this.tqhkzgfs = tqhkzgfs;
	}
	
    /**
     * @return tqhkzgfs
     */
	public String getTqhkzgfs() {
		return this.tqhkzgfs;
	}
	
	/**
	 * @param tqhkzgje
	 */
	public void setTqhkzgje(java.math.BigDecimal tqhkzgje) {
		this.tqhkzgje = tqhkzgje;
	}
	
    /**
     * @return tqhkzgje
     */
	public java.math.BigDecimal getTqhkzgje() {
		return this.tqhkzgje;
	}
	
	/**
	 * @param yxbufhuk
	 */
	public void setYxbufhuk(String yxbufhuk) {
		this.yxbufhuk = yxbufhuk;
	}
	
    /**
     * @return yxbufhuk
     */
	public String getYxbufhuk() {
		return this.yxbufhuk;
	}
	
	/**
	 * @param bhzdjefs
	 */
	public void setBhzdjefs(String bhzdjefs) {
		this.bhzdjefs = bhzdjefs;
	}
	
    /**
     * @return bhzdjefs
     */
	public String getBhzdjefs() {
		return this.bhzdjefs;
	}
	
	/**
	 * @param bfhkktxe
	 */
	public void setBfhkktxe(String bfhkktxe) {
		this.bfhkktxe = bfhkktxe;
	}
	
    /**
     * @return bfhkktxe
     */
	public String getBfhkktxe() {
		return this.bfhkktxe;
	}
	
	/**
	 * @param bfhkktje
	 */
	public void setBfhkktje(String bfhkktje) {
		this.bfhkktje = bfhkktje;
	}
	
    /**
     * @return bfhkktje
     */
	public String getBfhkktje() {
		return this.bfhkktje;
	}
	
	/**
	 * @param bfhkzdje
	 */
	public void setBfhkzdje(java.math.BigDecimal bfhkzdje) {
		this.bfhkzdje = bfhkzdje;
	}
	
    /**
     * @return bfhkzdje
     */
	public java.math.BigDecimal getBfhkzdje() {
		return this.bfhkzdje;
	}
	
	/**
	 * @param sfyxkuxq
	 */
	public void setSfyxkuxq(String sfyxkuxq) {
		this.sfyxkuxq = sfyxkuxq;
	}
	
    /**
     * @return sfyxkuxq
     */
	public String getSfyxkuxq() {
		return this.sfyxkuxq;
	}
	
	/**
	 * @param kuanxqts
	 */
	public void setKuanxqts(Long kuanxqts) {
		this.kuanxqts = kuanxqts;
	}
	
    /**
     * @return kuanxqts
     */
	public Long getKuanxqts() {
		return this.kuanxqts;
	}
	
	/**
	 * @param bjkuanxq
	 */
	public void setBjkuanxq(Long bjkuanxq) {
		this.bjkuanxq = bjkuanxq;
	}
	
    /**
     * @return bjkuanxq
     */
	public Long getBjkuanxq() {
		return this.bjkuanxq;
	}
	
	/**
	 * @param lxkuanxq
	 */
	public void setLxkuanxq(Long lxkuanxq) {
		this.lxkuanxq = lxkuanxq;
	}
	
    /**
     * @return lxkuanxq
     */
	public Long getLxkuanxq() {
		return this.lxkuanxq;
	}
	
	/**
	 * @param kxqzdcsh
	 */
	public void setKxqzdcsh(Long kxqzdcsh) {
		this.kxqzdcsh = kxqzdcsh;
	}
	
    /**
     * @return kxqzdcsh
     */
	public Long getKxqzdcsh() {
		return this.kxqzdcsh;
	}
	
	/**
	 * @param kxqjixgz
	 */
	public void setKxqjixgz(String kxqjixgz) {
		this.kxqjixgz = kxqjixgz;
	}
	
    /**
     * @return kxqjixgz
     */
	public String getKxqjixgz() {
		return this.kxqjixgz;
	}
	
	/**
	 * @param kxqhjxgz
	 */
	public void setKxqhjxgz(String kxqhjxgz) {
		this.kxqhjxgz = kxqhjxgz;
	}
	
    /**
     * @return kxqhjxgz
     */
	public String getKxqhjxgz() {
		return this.kxqhjxgz;
	}
	
	/**
	 * @param kxqshxgz
	 */
	public void setKxqshxgz(String kxqshxgz) {
		this.kxqshxgz = kxqshxgz;
	}
	
    /**
     * @return kxqshxgz
     */
	public String getKxqshxgz() {
		return this.kxqshxgz;
	}
	
	/**
	 * @param kxqzyqgz
	 */
	public void setKxqzyqgz(String kxqzyqgz) {
		this.kxqzyqgz = kxqzyqgz;
	}
	
    /**
     * @return kxqzyqgz
     */
	public String getKxqzyqgz() {
		return this.kxqzyqgz;
	}
	
	/**
	 * @param kxqjjrgz
	 */
	public void setKxqjjrgz(String kxqjjrgz) {
		this.kxqjjrgz = kxqjjrgz;
	}
	
    /**
     * @return kxqjjrgz
     */
	public String getKxqjjrgz() {
		return this.kxqjjrgz;
	}
	
	/**
	 * @param hkhlpjlx
	 */
	public void setHkhlpjlx(String hkhlpjlx) {
		this.hkhlpjlx = hkhlpjlx;
	}
	
    /**
     * @return hkhlpjlx
     */
	public String getHkhlpjlx() {
		return this.hkhlpjlx;
	}
	
	/**
	 * @param hkzshuil
	 */
	public void setHkzshuil(java.math.BigDecimal hkzshuil) {
		this.hkzshuil = hkzshuil;
	}
	
    /**
     * @return hkzshuil
     */
	public java.math.BigDecimal getHkzshuil() {
		return this.hkzshuil;
	}
	
	/**
	 * @param huilzlei
	 */
	public void setHuilzlei(String huilzlei) {
		this.huilzlei = huilzlei;
	}
	
    /**
     * @return huilzlei
     */
	public String getHuilzlei() {
		return this.huilzlei;
	}
	
	/**
	 * @param schkriqi
	 */
	public void setSchkriqi(String schkriqi) {
		this.schkriqi = schkriqi;
	}
	
    /**
     * @return schkriqi
     */
	public String getSchkriqi() {
		return this.schkriqi;
	}
	
	/**
	 * @param xycihkrq
	 */
	public void setXycihkrq(String xycihkrq) {
		this.xycihkrq = xycihkrq;
	}
	
    /**
     * @return xycihkrq
     */
	public String getXycihkrq() {
		return this.xycihkrq;
	}
	
	/**
	 * @param hxzdkkbz
	 */
	public void setHxzdkkbz(String hxzdkkbz) {
		this.hxzdkkbz = hxzdkkbz;
	}
	
    /**
     * @return hxzdkkbz
     */
	public String getHxzdkkbz() {
		return this.hxzdkkbz;
	}
	
	/**
	 * @param yqzdzjbz
	 */
	public void setYqzdzjbz(String yqzdzjbz) {
		this.yqzdzjbz = yqzdzjbz;
	}
	
    /**
     * @return yqzdzjbz
     */
	public String getYqzdzjbz() {
		return this.yqzdzjbz;
	}
	
	/**
	 * @param rchukkbz
	 */
	public void setRchukkbz(String rchukkbz) {
		this.rchukkbz = rchukkbz;
	}
	
    /**
     * @return rchukkbz
     */
	public String getRchukkbz() {
		return this.rchukkbz;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}


}