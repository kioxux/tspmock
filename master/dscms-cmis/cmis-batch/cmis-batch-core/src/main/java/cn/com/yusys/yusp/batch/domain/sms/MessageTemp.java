package cn.com.yusys.yusp.batch.domain.sms;


import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: MessageTemp
 * @类描述: Message_Temp数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:42:29
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "message_temp")
public class MessageTemp implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 消息类型
     */
    @Id
    @Column(name = "MESSAGE_TYPE")
    private String messageType;

    /**
     * 适用渠道类型[system,email,mobile]
     */
    @Id
    @Column(name = "CHANNEL_TYPE")
    private String channelType;

    /**
     * 异常重发次数
     */
    @Column(name = "SEND_NUM")
    private Integer sendNum;

    /**
     * 模板内容
     */
    @Column(name = "TEMPLATE_CONTENT")
    private String templateContent;

    /**
     * 邮件/系统消息标题
     */
    @Column(name = "EMAIL_TITLE")
    private String emailTitle;

    /**
     * 发送开始时间
     */
    @Column(name = "TIME_START")
    private String timeStart;

    /**
     * 发送结束时间
     */
    @Column(name = "TIME_END")
    private String timeEnd;

    /**
     * 是否固定时间发送
     */
    @Column(name = "IS_TIME")
    private String isTime;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public Integer getSendNum() {
        return sendNum;
    }

    public void setSendNum(Integer sendNum) {
        this.sendNum = sendNum;
    }

    public String getTemplateContent() {
        return templateContent;
    }

    public void setTemplateContent(String templateContent) {
        this.templateContent = templateContent;
    }

    public String getEmailTitle() {
        return emailTitle;
    }

    public void setEmailTitle(String emailTitle) {
        this.emailTitle = emailTitle;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getIsTime() {
        return isTime;
    }

    public void setIsTime(String isTime) {
        this.isTime = isTime;
    }
}
