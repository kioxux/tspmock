package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0150</br>
 * 任务名称：加工任务-业务处理-网金数据初始化 </br>
 *
 * @author sunzhe
 * @version 1.0
 * @since 2021年10月05日 下午9:56:54
 */
public interface Cmis0150Mapper {
    /**
     * 插入数据到合同表
     *
     * @param openDay
     * @return
     */
    int cmis0150InsertWjCtrLoanCont01(@Param("openDay") String openDay);


    /**
     * 插入数据到台账表
     *
     * @param openDay
     * @return
     */
    int cmis0150InsertWjAccLoan02(@Param("openDay") String openDay);


    /**
     * 清空临时表-清空网金五级分类数据
     */
    void truncateTmpRcpAntSlLoanCusRfc05();

    /**
     * 插入网金五级分类数据
     *
     * @param openDay
     * @return
     */
    int insertTmpRcpAntSlLoanCusRfc06(@Param("openDay") String openDay);

    /**
     * 更新网金台账五级分类数据
     *
     * @param openDay
     * @return
     */
    int updateAccLoanWj07(@Param("openDay") String openDay);

    /**
     * 更新网金核销台账数据- 更新核销状态 蚂蚁一期
     *
     * @param openDay
     * @return
     */
    int updateAccLoanWj08(@Param("openDay") String openDay);

    /**
     * 更新网金核销台账数据- 更新核销状态 蚂蚁二期
     *
     * @param openDay
     * @return
     */
    int updateAccLoanWj09(@Param("openDay") String openDay);

}
