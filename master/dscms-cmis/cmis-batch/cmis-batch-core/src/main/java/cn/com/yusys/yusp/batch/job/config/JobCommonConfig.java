package cn.com.yusys.yusp.batch.job.config;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;

/*
 * 公共Job配置类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年2月20日 上午9:56:54
 */
public class JobCommonConfig {
    @Autowired
    protected JobBuilderFactory jobBuilderFactory;

    @Autowired
    protected StepBuilderFactory stepBuilderFactory;

    @Autowired
    protected SqlSessionTemplate sqlSessionTemplate;

}
