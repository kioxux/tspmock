package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0122Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0122</br>
 * 任务名称：加工任务-业务处理-诉讼时效提醒 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0122Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0122Service.class);

    @Autowired
    private Cmis0122Mapper cmis0122Mapper;


    /**
     * 插入风险提示表
     * @param openDay
     */
    public void cmis0122InsertWbMsgNotice(String openDay) {
        logger.info("插入风险提示表,距离诉讼时效到期日前12个月 、6个月 3个月提醒开始,请求参数为:[{}]", openDay);
        int insertWbMsgNotice01 = cmis0122Mapper.insertWbMsgNotice01(openDay);
        logger.info("插入风险提示表,距离诉讼时效到期日前12个月 、6个月 3个月提醒结束,返回参数为:[{}]", insertWbMsgNotice01);

        logger.info("插入风险提示表开始,距离抵债资产处置时效到期日前6个月 、4个月、1个月开始,请求参数为:[{}]", openDay);
        int insertWbMsgNotice02 = cmis0122Mapper.insertWbMsgNotice02(openDay);
        logger.info("插入风险提示表结束,距离抵债资产处置时效到期日前6个月 、4个月、1个月结束,返回参数为:[{}]", insertWbMsgNotice02);

        logger.info("插入风险提示表开始,单户核销  核销后2年（24个月）开始提醒，提醒相关人员进行核销后事项处理开始,请求参数为:[{}]", openDay);
        int insertWbMsgNotice03 = cmis0122Mapper.insertWbMsgNotice03(openDay);
        logger.info("插入风险提示表结束,单户核销  核销后2年（24个月）开始提醒，提醒相关人员进行核销后事项处理结束,返回参数为:[{}]", insertWbMsgNotice03);

        logger.info("插入风险提示表开始,批量核销  核销后2年（24个月）开始提醒，提醒相关人员进行核销后事项处理开始,请求参数为:[{}]", openDay);
        int insertWbMsgNotice04 = cmis0122Mapper.insertWbMsgNotice04(openDay);
        logger.info("插入风险提示表结束,批量核销  核销后2年（24个月）开始提醒，提醒相关人员进行核销后事项处理结束,返回参数为:[{}]", insertWbMsgNotice04);

        logger.info("插入风险提示表开始,信用卡核销  核销后2年（24个月）开始提醒，提醒相关人员进行核销后事项处理开始,请求参数为:[{}]", openDay);
        int insertWbMsgNotice05 = cmis0122Mapper.insertWbMsgNotice05(openDay);
        logger.info("插入风险提示表结束,信用卡核销  核销后2年（24个月）开始提醒，提醒相关人员进行核销后事项处理结束,返回参数为:[{}]", insertWbMsgNotice05);
    }
}
