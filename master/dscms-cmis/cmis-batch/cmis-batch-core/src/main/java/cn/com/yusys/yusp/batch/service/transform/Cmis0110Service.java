package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0110Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0110</br>
 * 任务名称：加工任务-业务处理-资产池协议处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0110Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0110Service.class);

    @Autowired
    private Cmis0110Mapper cmis0110Mapper;

    /**
     * 自动注销-银承借据和贷款借据
     *
     * @param openDay
     */
    public void cmis0110UpdateCtrAsplDetails(String openDay) {
        logger.info("自动注销-银承借据和贷款借据开始,请求参数为:[{}]", openDay);
        int updateCtrAsplDetails01 = cmis0110Mapper.updateCtrAsplDetails01(openDay);
        logger.info("自动注销-银承借据和贷款借据结束,返回参数为:[{}]", updateCtrAsplDetails01);
    }
}
