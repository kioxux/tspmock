package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0124Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0124</br>
 * 任务名称：加工任务-业务处理-授信每年复审提醒 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0124Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0124Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0124Service cmis0124Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job cmis0124Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0124_JOB.key, JobStepEnum.CMIS0124_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0124Job = this.jobBuilderFactory.get(JobStepEnum.CMIS0124_JOB.key)
                .start(cmis0124UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0124CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
//                复审任务暂时不跑
//                .next(cmis0124InsertWbMsgNoticeStep(WILL_BE_INJECTED)) // 插入风险提示表
//                .next(cmis0124UpdateApprLmtSubBasicInfoStep(WILL_BE_INJECTED)) // 更新批复额度分项基础信息
//                .next(cmis0124QueryApprLmtSubBasicInfoStep(WILL_BE_INJECTED))// 查询批复额度分项基础信息和调用单一客户授信复审信接口
//                .next(cmis0124QueryApprLmtSubBasicInfoGrpStep(WILL_BE_INJECTED))//查询批复额度分项基础信息和调用集团客户授信复审信接口
                .next(cmis0124UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0124Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0124UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0124_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0124_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0124UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0124_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0124_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0124_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0124_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0124UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0124CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0124_CHECK_REL_STEP.key, JobStepEnum.CMIS0124_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0124CheckRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0124_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0124_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0124_CHECK_REL_STEP.key, JobStepEnum.CMIS0124_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0124_CHECK_REL_STEP.key, JobStepEnum.CMIS0124_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0124CheckRelStep;
    }

    /**
     * 插入风险提示表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0124InsertWbMsgNoticeStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0124_INSERT_WB_MSG_NOTICE_STEP.key, JobStepEnum.CMIS0124_INSERT_WB_MSG_NOTICE_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0124InsertWbMsgNoticeStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0124_INSERT_WB_MSG_NOTICE_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0124Service.cmis0124InsertWbMsgNotice(openDay);//插入风险提示表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0124_INSERT_WB_MSG_NOTICE_STEP.key, JobStepEnum.CMIS0124_INSERT_WB_MSG_NOTICE_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0124InsertWbMsgNoticeStep;
    }

    /**
     * 更新批复额度分项基础信息
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0124UpdateApprLmtSubBasicInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0124_UPDATE_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepEnum.CMIS0124_UPDATE_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0124UpdateApprLmtSubBasicInfoStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0124_UPDATE_APPR_LMT_SUB_BASIC_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0124Service.cmis0124UpdateApprLmtSubBasicInfo(openDay);//更新批复额度分项基础信息
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0124_UPDATE_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepEnum.CMIS0124_UPDATE_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0124UpdateApprLmtSubBasicInfoStep;
    }

    /**
     * 授信每年复审提醒-查询批复额度分项基础信息和调用单一客户授信复审信接口
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0124QueryApprLmtSubBasicInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0124_QUERY_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepEnum.CMIS0124_QUERY_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0124QueryApprLmtSubBasicInfoStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0124_QUERY_APPR_LMT_SUB_BASIC_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0124Service.cmis0124QueryApprLmtSubBasicInfo(openDay);//授信每年复审提醒-查询批复额度分项基础信息和调用单一客户授信复审信接口
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0124_QUERY_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepEnum.CMIS0124_QUERY_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0124QueryApprLmtSubBasicInfoStep;
    }

    /**
     * 授信每年复审提醒-查询批复额度分项基础信息和调用集团客户授信复审信接口
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0124QueryApprLmtSubBasicInfoGrpStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0124_QUERY_APPR_LMT_SUB_BASIC_INFO_GRP_STEP.key, JobStepEnum.CMIS0124_QUERY_APPR_LMT_SUB_BASIC_INFO_GRP_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0124QueryApprLmtSubBasicInfoGrpStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0124_QUERY_APPR_LMT_SUB_BASIC_INFO_GRP_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0124Service.cmis0124QueryApprLmtSubBasicInfoGrp(openDay);//授信每年复审提醒-查询批复额度分项基础信息和调用集团客户授信复审信接口
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0124_QUERY_APPR_LMT_SUB_BASIC_INFO_GRP_STEP.key, JobStepEnum.CMIS0124_QUERY_APPR_LMT_SUB_BASIC_INFO_GRP_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0124QueryApprLmtSubBasicInfoGrpStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0124UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0124_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0124_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0124UpdateTask100Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0124_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0124_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0124_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0124_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0124UpdateTask100Step;
    }


}
