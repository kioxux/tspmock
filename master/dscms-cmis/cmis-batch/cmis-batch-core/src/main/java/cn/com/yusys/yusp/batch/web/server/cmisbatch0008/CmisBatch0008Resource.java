package cn.com.yusys.yusp.batch.web.server.cmisbatch0008;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0008.Cmisbatch0008ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0008.Cmisbatch0008RespDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0008.CmisBatch0008Service;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:任务加急初始化数据
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "CMISBATCH0008:任务加急初始化数据")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0008Resource.class);

    @Autowired
    private CmisBatch0008Service cmisBatch0008Service;

    /**
     * 交易码：cmisbatch0008
     * 交易描述：任务加急初始化数据
     *
     * @param cmisbatch0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("任务加急初始化数据")
    @PostMapping("/cmisbatch0008")
    protected @ResponseBody
    ResultDto<Cmisbatch0008RespDto> cmisbatch0008(@Validated @RequestBody Cmisbatch0008ReqDto cmisbatch0008ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0008.key, DscmsEnum.TRADE_CODE_CMISBATCH0008.value, JSON.toJSONString(cmisbatch0008ReqDto));
        Cmisbatch0008RespDto cmisbatch0008RespDto = new Cmisbatch0008RespDto();// 响应Dto:任务加急初始化数据
        ResultDto<Cmisbatch0008RespDto> cmisbatch0008ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0008ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0008.key, DscmsEnum.TRADE_CODE_CMISBATCH0008.value, JSON.toJSONString(cmisbatch0008ReqDto));
            cmisbatch0008RespDto = cmisBatch0008Service.cmisBatch0008(cmisbatch0008ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0008.key, DscmsEnum.TRADE_CODE_CMISBATCH0008.value, JSON.toJSONString(cmisbatch0008RespDto));
            // 封装cmisbatch0008ResultDto中正确的返回码和返回信息
            cmisbatch0008ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0008ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0008.key, DscmsEnum.TRADE_CODE_CMISBATCH0008.value, e.getMessage());
            // 封装cmisbatch0008ResultDto中异常返回码和返回信息
            cmisbatch0008ResultDto.setCode(e.getErrorCode());
            cmisbatch0008ResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0008.key, DscmsEnum.TRADE_CODE_CMISBATCH0008.value, e.getMessage());
            // 封装cmisbatch0008ResultDto中异常返回码和返回信息
            cmisbatch0008ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0008ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0008RespDto到cmisbatch0008ResultDto中
        cmisbatch0008ResultDto.setData(cmisbatch0008RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0008.key, DscmsEnum.TRADE_CODE_CMISBATCH0008.value, JSON.toJSONString(cmisbatch0008ResultDto));
        return cmisbatch0008ResultDto;
    }
}
