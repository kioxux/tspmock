/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpMsRepayDetailFileHis
 * @类描述: bat_t_rcp_ms_repay_detail_file_his数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:04:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_rcp_ms_repay_detail_file_his")
public class BatTRcpMsRepayDetailFileHis extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务日期 **/
	@Id
	@Column(name = "BIZ_DATE")
	private String bizDate;
	
	/** 合同号 **/
	@Id
	@Column(name = "CONTR_NBR")
	private String contrNbr;
	
	/** 流水号 **/
	@Id
	@Column(name = "REPAY_REF_NBR")
	private String repayRefNbr;
	
	/** 贷款编号 **/
	@Column(name = "REF_NBR", unique = false, nullable = false, length = 72)
	private String refNbr;
	
	/** 还款日期 **/
	@Column(name = "REPAY_DATE", unique = false, nullable = false, length = 32)
	private String repayDate;
	
	/** 本期应还本金 **/
	@Column(name = "PRINCIPAL_DUE", unique = false, nullable = false, length = 17)
	private java.math.BigDecimal principalDue;
	
	/** 本期实还本金金额 **/
	@Column(name = "PRINCIPAL_AMT", unique = false, nullable = false, length = 17)
	private java.math.BigDecimal principalAmt;
	
	/** 本期应还利息 **/
	@Column(name = "INTEREST_DUE", unique = false, nullable = false, length = 17)
	private java.math.BigDecimal interestDue;
	
	/** 本期实还利息金额 **/
	@Column(name = "INTEREST_AMT", unique = false, nullable = false, length = 17)
	private java.math.BigDecimal interestAmt;
	
	/** 本期应还罚息金额 **/
	@Column(name = "PENALTY_DUE", unique = false, nullable = false, length = 17)
	private java.math.BigDecimal penaltyDue;
	
	/** 本期实还罚息金额 **/
	@Column(name = "PENALTY_AMT", unique = false, nullable = false, length = 17)
	private java.math.BigDecimal penaltyAmt;
	
	/** 还款期数 **/
	@Column(name = "REPAY_TERM", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal repayTerm;
	
	/** "还款类型01、按期还款02、提前还款03、逾期还款04、非应计还款" **/
	@Column(name = "REPAY_TYPE", unique = false, nullable = false, length = 12)
	private String repayType;
	
	/** 还款银行卡号 **/
	@Column(name = "REPAY_CARD_NO", unique = false, nullable = true, length = 64)
	private String repayCardNo;
	
	/** 录入日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "LAST_UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	/** 本期应还利息*58% **/
	@Column(name = "INTEREST_DUE_ZJG", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal interestDueZjg;
	
	/** 本期实还利息金额*58% **/
	@Column(name = "INTEREST_AMT_ZJG", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal interestAmtZjg;
	
	/** 本期应还罚息金额*58% **/
	@Column(name = "PENALTY_DUE_ZJG", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal penaltyDueZjg;
	
	/** 本期实还罚息金额*58% **/
	@Column(name = "PENALTY_AMT_ZJG", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal penaltyAmtZjg;
	
	
	/**
	 * @param bizDate
	 */
	public void setBizDate(String bizDate) {
		this.bizDate = bizDate;
	}
	
    /**
     * @return bizDate
     */
	public String getBizDate() {
		return this.bizDate;
	}
	
	/**
	 * @param contrNbr
	 */
	public void setContrNbr(String contrNbr) {
		this.contrNbr = contrNbr;
	}
	
    /**
     * @return contrNbr
     */
	public String getContrNbr() {
		return this.contrNbr;
	}
	
	/**
	 * @param refNbr
	 */
	public void setRefNbr(String refNbr) {
		this.refNbr = refNbr;
	}
	
    /**
     * @return refNbr
     */
	public String getRefNbr() {
		return this.refNbr;
	}
	
	/**
	 * @param repayDate
	 */
	public void setRepayDate(String repayDate) {
		this.repayDate = repayDate;
	}
	
    /**
     * @return repayDate
     */
	public String getRepayDate() {
		return this.repayDate;
	}
	
	/**
	 * @param repayRefNbr
	 */
	public void setRepayRefNbr(String repayRefNbr) {
		this.repayRefNbr = repayRefNbr;
	}
	
    /**
     * @return repayRefNbr
     */
	public String getRepayRefNbr() {
		return this.repayRefNbr;
	}
	
	/**
	 * @param principalDue
	 */
	public void setPrincipalDue(java.math.BigDecimal principalDue) {
		this.principalDue = principalDue;
	}
	
    /**
     * @return principalDue
     */
	public java.math.BigDecimal getPrincipalDue() {
		return this.principalDue;
	}
	
	/**
	 * @param principalAmt
	 */
	public void setPrincipalAmt(java.math.BigDecimal principalAmt) {
		this.principalAmt = principalAmt;
	}
	
    /**
     * @return principalAmt
     */
	public java.math.BigDecimal getPrincipalAmt() {
		return this.principalAmt;
	}
	
	/**
	 * @param interestDue
	 */
	public void setInterestDue(java.math.BigDecimal interestDue) {
		this.interestDue = interestDue;
	}
	
    /**
     * @return interestDue
     */
	public java.math.BigDecimal getInterestDue() {
		return this.interestDue;
	}
	
	/**
	 * @param interestAmt
	 */
	public void setInterestAmt(java.math.BigDecimal interestAmt) {
		this.interestAmt = interestAmt;
	}
	
    /**
     * @return interestAmt
     */
	public java.math.BigDecimal getInterestAmt() {
		return this.interestAmt;
	}
	
	/**
	 * @param penaltyDue
	 */
	public void setPenaltyDue(java.math.BigDecimal penaltyDue) {
		this.penaltyDue = penaltyDue;
	}
	
    /**
     * @return penaltyDue
     */
	public java.math.BigDecimal getPenaltyDue() {
		return this.penaltyDue;
	}
	
	/**
	 * @param penaltyAmt
	 */
	public void setPenaltyAmt(java.math.BigDecimal penaltyAmt) {
		this.penaltyAmt = penaltyAmt;
	}
	
    /**
     * @return penaltyAmt
     */
	public java.math.BigDecimal getPenaltyAmt() {
		return this.penaltyAmt;
	}
	
	/**
	 * @param repayTerm
	 */
	public void setRepayTerm(java.math.BigDecimal repayTerm) {
		this.repayTerm = repayTerm;
	}
	
    /**
     * @return repayTerm
     */
	public java.math.BigDecimal getRepayTerm() {
		return this.repayTerm;
	}
	
	/**
	 * @param repayType
	 */
	public void setRepayType(String repayType) {
		this.repayType = repayType;
	}
	
    /**
     * @return repayType
     */
	public String getRepayType() {
		return this.repayType;
	}
	
	/**
	 * @param repayCardNo
	 */
	public void setRepayCardNo(String repayCardNo) {
		this.repayCardNo = repayCardNo;
	}
	
    /**
     * @return repayCardNo
     */
	public String getRepayCardNo() {
		return this.repayCardNo;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}
	
	/**
	 * @param interestDueZjg
	 */
	public void setInterestDueZjg(java.math.BigDecimal interestDueZjg) {
		this.interestDueZjg = interestDueZjg;
	}
	
    /**
     * @return interestDueZjg
     */
	public java.math.BigDecimal getInterestDueZjg() {
		return this.interestDueZjg;
	}
	
	/**
	 * @param interestAmtZjg
	 */
	public void setInterestAmtZjg(java.math.BigDecimal interestAmtZjg) {
		this.interestAmtZjg = interestAmtZjg;
	}
	
    /**
     * @return interestAmtZjg
     */
	public java.math.BigDecimal getInterestAmtZjg() {
		return this.interestAmtZjg;
	}
	
	/**
	 * @param penaltyDueZjg
	 */
	public void setPenaltyDueZjg(java.math.BigDecimal penaltyDueZjg) {
		this.penaltyDueZjg = penaltyDueZjg;
	}
	
    /**
     * @return penaltyDueZjg
     */
	public java.math.BigDecimal getPenaltyDueZjg() {
		return this.penaltyDueZjg;
	}
	
	/**
	 * @param penaltyAmtZjg
	 */
	public void setPenaltyAmtZjg(java.math.BigDecimal penaltyAmtZjg) {
		this.penaltyAmtZjg = penaltyAmtZjg;
	}
	
    /**
     * @return penaltyAmtZjg
     */
	public java.math.BigDecimal getPenaltyAmtZjg() {
		return this.penaltyAmtZjg;
	}


}