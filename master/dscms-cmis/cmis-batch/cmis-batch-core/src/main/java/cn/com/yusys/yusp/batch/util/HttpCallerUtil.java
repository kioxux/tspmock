package cn.com.yusys.yusp.batch.util;

import cn.com.yusys.yusp.batch.dto.batch.SingleRunReqDto;
import cn.com.yusys.yusp.batch.dto.batch.SingleRunRespDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.SpringContextUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

/**
 * HTTP调用工具类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月21日 下午11:56:54
 */
public class HttpCallerUtil {
    private static final Logger logger = LoggerFactory.getLogger(HttpCallerUtil.class);

    public static <T> SingleRunRespDto post(String url, SingleRunReqDto singleRunReqDto) throws BizException {
        // 组装报文头和报文体
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = MediaType.parseMediaType("application/json;charset=UTF-8");
        headers.setContentType(mediaType);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(singleRunReqDto), headers);
        // 调用
        RestTemplate restTemplate = SpringContextUtils.getBean("restTemplate");
        ParameterizedTypeReference<SingleRunRespDto> typeReference = new ParameterizedTypeReference<SingleRunRespDto>() {
        };
        logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]调用开始");
        ResponseEntity<SingleRunRespDto> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, typeReference);
        logger.info("任务日期为:[" + singleRunReqDto.getOpenDay() + "],任务编号为:[" + singleRunReqDto.getTaskNo() + "]调用结束");
        // 判断返回情况
        if (Objects.equals(responseEntity.getStatusCode(), HttpStatus.OK)) {
            return responseEntity.getBody();
        } else {
            throw BizException.error(null, null, "调用[" + singleRunReqDto.getTaskNo() + "]失败");
        }
    }
}
