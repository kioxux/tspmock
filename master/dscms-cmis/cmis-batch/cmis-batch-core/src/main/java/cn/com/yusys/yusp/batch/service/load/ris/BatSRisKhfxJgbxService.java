/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.ris;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpRptFiveClassifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.ris.BatSRisKhfxJgbx;
import cn.com.yusys.yusp.batch.repository.mapper.load.ris.BatSRisKhfxJgbxMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRisKhfxJgbxService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-25 16:59:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSRisKhfxJgbxService {
    private static final Logger logger = LoggerFactory.getLogger(BatSRcpRptFiveClassifyService.class);
    @Autowired
    private TableUtilsService tableUtilsService;
    @Autowired
    private BatSRisKhfxJgbxMapper batSRisKhfxJgbxMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSRisKhfxJgbx selectByPrimaryKey(String dataDt, String custTypeId, String custId, String custName) {
        return batSRisKhfxJgbxMapper.selectByPrimaryKey(dataDt, custTypeId, custId, custName);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSRisKhfxJgbx> selectAll(QueryModel model) {
        List<BatSRisKhfxJgbx> records = (List<BatSRisKhfxJgbx>) batSRisKhfxJgbxMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSRisKhfxJgbx> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSRisKhfxJgbx> list = batSRisKhfxJgbxMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSRisKhfxJgbx record) {
        return batSRisKhfxJgbxMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSRisKhfxJgbx record) {
        return batSRisKhfxJgbxMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSRisKhfxJgbx record) {
        return batSRisKhfxJgbxMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSRisKhfxJgbx record) {
        return batSRisKhfxJgbxMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dataDt, String custTypeId, String custId, String custName) {
        return batSRisKhfxJgbxMapper.deleteByPrimaryKey(dataDt, custTypeId, custId, custName);
    }
    /**
     * 删除S表当天数据
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建客户维度风险暴露[bat_s_ris_khfx_jgbx]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_ris_khfx_jgbx");
        logger.info("重建客户维度风险暴露[bat_s_ris_khfx_jgbx]结束");
        return 0;
    }

    /**
     * 删除业务表当天数据
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteBizByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建客户维度风险暴露[lmt_ris_khfx_jgbx]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_lmt", "lmt_ris_khfx_jgbx");
        logger.info("重建客户维度风险暴露[lmt_ris_khfx_jgbx]结束");
        return 0;
    }
}
