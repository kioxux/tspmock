/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.biz;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.biz.TmpLmtAppPer;
import cn.com.yusys.yusp.batch.service.biz.TmpLmtAppPerService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: TmpLmtAppPerResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-24 11:12:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/tmplmtappper")
public class TmpLmtAppPerResource {
    @Autowired
    private TmpLmtAppPerService tmpLmtAppPerService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<TmpLmtAppPer>> query() {
        QueryModel queryModel = new QueryModel();
        List<TmpLmtAppPer> list = tmpLmtAppPerService.selectAll(queryModel);
        return new ResultDto<List<TmpLmtAppPer>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<TmpLmtAppPer>> index(QueryModel queryModel) {
        List<TmpLmtAppPer> list = tmpLmtAppPerService.selectByModel(queryModel);
        return new ResultDto<List<TmpLmtAppPer>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<TmpLmtAppPer> create(@RequestBody TmpLmtAppPer tmpLmtAppPer) throws URISyntaxException {
        tmpLmtAppPerService.insert(tmpLmtAppPer);
        return new ResultDto<TmpLmtAppPer>(tmpLmtAppPer);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody TmpLmtAppPer tmpLmtAppPer) throws URISyntaxException {
        int result = tmpLmtAppPerService.update(tmpLmtAppPer);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String serno, String taskDate) {
        int result = tmpLmtAppPerService.deleteByPrimaryKey(pkId, serno, taskDate);
        return new ResultDto<Integer>(result);
    }

}
