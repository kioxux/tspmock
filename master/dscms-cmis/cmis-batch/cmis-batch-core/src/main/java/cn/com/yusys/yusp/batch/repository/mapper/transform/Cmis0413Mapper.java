package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0413</br>
 * 任务名称：加工任务-贷后管理-个人消费贷款定期检查  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0413Mapper {

    /**
     * 非按揭类贷款中非分期付款类（非等额本金、非等额本息 ）的180天生成一次定期贷后检查任务
     *
     * @param openDay
     * @return
     */
    int insertTownTask1(@Param("openDay") String openDay);

    /**
     * 村镇 非按揭类贷款中分期还款（ 等额本金 等额本息 ）的贷款，按时还款，不产生贷后；只要有逾期，就生成贷后
     *
     * @param openDay
     * @return
     */
    int insertTownTask2(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList01(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList02(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList03(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList04(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList05(@Param("openDay") String openDay);
}
