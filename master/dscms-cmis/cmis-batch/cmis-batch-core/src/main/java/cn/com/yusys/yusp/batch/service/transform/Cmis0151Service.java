package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0151Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0151</br>
 * 任务名称：加工任务-业务处理-网金数据初始化到历史表 </br>
 *
 * @author sunzhe
 * @version 1.0
 * @since 2021年10月05日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0151Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0151Service.class);

    @Autowired
    private Cmis0151Mapper cmis0151Mapper;


    /**
     * 插入贷款合同历史表
     *
     * @param openDay
     */
    public void cmis0151InsertWjCtrLoanContHist03(String openDay) {
        logger.info("插入贷款合同历史表开始,请求参数为:[{}]", openDay);
        int cmis0151InsertWjCtrLoanContHist03 = cmis0151Mapper.cmis0151InsertWjCtrLoanContHist03(openDay);
        logger.info("插入贷款合同历史表结束,返回参数为:[{}]", cmis0151InsertWjCtrLoanContHist03);

    }

    /**
     * 插入贷款台账历史表
     *
     * @param openDay
     */
    public void cmis0151InsertWjAccLoanHist04(String openDay) {
        logger.info("插入贷款台账历史表开始,请求参数为:[{}]", openDay);
        int cmis0151InsertWjAccLoanHist04 = cmis0151Mapper.cmis0151InsertWjAccLoanHist04(openDay);
        logger.info("插入贷款台账历史表结束,返回参数为:[{}]", cmis0151InsertWjAccLoanHist04);

    }


}
