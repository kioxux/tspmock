/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.web.rest.job;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.spring.batch.domain.job.BatchJobSeq;
import cn.com.yusys.yusp.spring.batch.service.job.BatchJobSeqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobSeqResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batchjobseq")
public class BatchJobSeqResource {
    @Autowired
    private BatchJobSeqService batchJobSeqService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatchJobSeq>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatchJobSeq> list = batchJobSeqService.selectAll(queryModel);
        return new ResultDto<List<BatchJobSeq>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatchJobSeq>> index(QueryModel queryModel) {
        List<BatchJobSeq> list = batchJobSeqService.selectByModel(queryModel);
        return new ResultDto<List<BatchJobSeq>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatchJobSeq>> query(@RequestBody QueryModel queryModel) {
        List<BatchJobSeq> list = batchJobSeqService.selectByModel(queryModel);
        return new ResultDto<List<BatchJobSeq>>(list);
    }

}
