/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.LmtCheckDataOneService;

import cn.com.yusys.yusp.batch.repository.mapper.check.LmtCheckUpdatePvpOutAmtMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: 数据更新额度校正-计算分项可出账金额
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCheckUpdatePvpOutAmtService {

    private static final Logger logger = LoggerFactory.getLogger(LmtCheckUpdatePvpOutAmtService.class);

    @Autowired
    private LmtCheckUpdatePvpOutAmtMapper lmtCheckUpdatePvpOutAmtMapper;

    public void checkOutLmtCheckUpdatePvpOutAmt(String cusId){
        logger.info("9.LmtCheckUpdatePvpOutAmt数据更新额度校正-计算分项可出账金额 --------start");
        /** 1、计算分项已出账金额、可出账金额 **/
        calculatePvpOutAmtAlreadyAndCanOutAmt(cusId) ;

        /** 2、二级分项已出账汇总至一级分项 **/
        collectPvpOutAmt2LevelSubOutAmtTo1LevelSub(cusId) ;
        logger.info("9.LmtCheckUpdatePvpOutAmt数据更新额度校正-计算分项可出账金额 --------start");
    }

    /**
     * 1、计算分项已出账金额、可出账金额
     */
    public int calculatePvpOutAmtAlreadyAndCanOutAmt(String cusId){
        logger.info("1、计算分项已出账金额、可出账金额 -------- ");
        lmtCheckUpdatePvpOutAmtMapper.calculatePvpOutAmtAlreadyAndCanOutAmt1(cusId);
        lmtCheckUpdatePvpOutAmtMapper.calculatePvpOutAmtAlreadyAndCanOutAmt2(cusId);
        lmtCheckUpdatePvpOutAmtMapper.calculatePvpOutAmtAlreadyAndCanOutAmt3(cusId);
        return lmtCheckUpdatePvpOutAmtMapper.calculatePvpOutAmtAlreadyAndCanOutAmt4(cusId);
    }

    /**
     * 2、二级分项已出账汇总至一级分项
     */
    public int collectPvpOutAmt2LevelSubOutAmtTo1LevelSub(String cusId){
        logger.info("2、二级分项已出账汇总至一级分项 -------- ");
        lmtCheckUpdatePvpOutAmtMapper.collectPvpOutAmt2LevelSubOutAmtTo1LevelSub1(cusId);
        lmtCheckUpdatePvpOutAmtMapper.collectPvpOutAmt2LevelSubOutAmtTo1LevelSub2(cusId);
        lmtCheckUpdatePvpOutAmtMapper.collectPvpOutAmt2LevelSubOutAmtTo1LevelSub3(cusId);
        return lmtCheckUpdatePvpOutAmtMapper.collectPvpOutAmt2LevelSubOutAmtTo1LevelSub4(cusId);
    }

}
