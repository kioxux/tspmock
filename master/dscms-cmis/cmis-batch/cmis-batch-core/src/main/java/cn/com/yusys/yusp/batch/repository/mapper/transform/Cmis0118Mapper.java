package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0118</br>
 * 任务名称：加工任务-业务处理-信用卡额度同步  </br>
 *
 * @author 徐超
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0118Mapper {

    /**
     * 更新外币利率定价申请信息表
     * @param openDay
     * @return
     */
    int updateOtherForRateApp(@Param("openDay") String openDay);

    /**
     * 更新 保证金存款特惠利率客户名单
     * @param openDay
     * @return
     */
    int updateOtherBailDepPreferRateAppCusList(@Param("openDay") String openDay);

    /**
     * 更新 银票手续费优惠客户名单
     * @param openDay
     * @return
     */
    int updateOtherAccpPerferFeeAppCusList(@Param("openDay")String openDay);
}
