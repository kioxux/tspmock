/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.web.rest.step;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.spring.batch.domain.step.BatchStepExecution;
import cn.com.yusys.yusp.spring.batch.service.step.BatchStepExecutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchStepExecutionResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:02:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batchstepexecution")
public class BatchStepExecutionResource {
    @Autowired
    private BatchStepExecutionService batchStepExecutionService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatchStepExecution>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatchStepExecution> list = batchStepExecutionService.selectAll(queryModel);
        return new ResultDto<List<BatchStepExecution>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatchStepExecution>> index(QueryModel queryModel) {
        List<BatchStepExecution> list = batchStepExecutionService.selectByModel(queryModel);
        return new ResultDto<List<BatchStepExecution>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatchStepExecution>> query(@RequestBody QueryModel queryModel) {
        List<BatchStepExecution> list = batchStepExecutionService.selectByModel(queryModel);
        return new ResultDto<List<BatchStepExecution>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{stepExecutionId}")
    protected ResultDto<BatchStepExecution> show(@PathVariable("stepExecutionId") Long stepExecutionId) {
        BatchStepExecution batchStepExecution = batchStepExecutionService.selectByPrimaryKey(stepExecutionId);
        return new ResultDto<BatchStepExecution>(batchStepExecution);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatchStepExecution> create(@RequestBody BatchStepExecution batchStepExecution) throws URISyntaxException {
        batchStepExecutionService.insert(batchStepExecution);
        return new ResultDto<BatchStepExecution>(batchStepExecution);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatchStepExecution batchStepExecution) throws URISyntaxException {
        int result = batchStepExecutionService.update(batchStepExecution);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{stepExecutionId}")
    protected ResultDto<Integer> delete(@PathVariable("stepExecutionId") Long stepExecutionId) {
        int result = batchStepExecutionService.deleteByPrimaryKey(stepExecutionId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batchStepExecutionService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
