/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpAntRepayLoanDetailTemp
 * @类描述: bat_s_rcp_ant_repay_loan_detail_temp数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:02:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_rcp_ant_repay_loan_detail_temp")
public class BatSRcpAntRepayLoanDetailTemp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 借呗平台贷款合同号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CONTRACT_NO")
	private String contractNo;
	
	/** 还款流水号，用于合约明细和分期明细关联 **/
	@Id
	@Column(name = "SEQ_NO", unique = false, nullable = true, length = 64)
	private String seqNo;
	
	/** 平台服务费收费单号，用来和网商银行账户资金流水进行对账 **/
	@Column(name = "FEE_NO", unique = false, nullable = true, length = 64)
	private String feeNo;
	
	/** 还款提现单号，用来和网商银行账户资金流水进行对账 **/
	@Column(name = "WITHDRAW_NO", unique = false, nullable = true, length = 64)
	private String withdrawNo;
	
	/** 还款类型，01、主动还款；02、批量扣款；03、动账通知还款；04、线下还款；05、代偿 **/
	@Column(name = "REPAY_TYPE", unique = false, nullable = true, length = 8)
	private String repayType;
	
	/** 还款日期，格式：yyyy-MM-dd HH:mm:ss **/
	@Column(name = "REPAY_DATE", unique = false, nullable = true, length = 20)
	private String repayDate;
	
	/** 本次还款前的应收未收正常本金余额（单位分） **/
	@Column(name = "CURR_PRIN_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currPrinBal;
	
	/** 本次还款前的应收未收逾期本金余额（单位分） **/
	@Column(name = "CURR_OVD_PRIN_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currOvdPrinBal;
	
	/** 本次还款前的应收未收正常利息余额（单位分），仅包括已结利息 **/
	@Column(name = "CURR_INT_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currIntBal;
	
	/** 本次还款前的应收未收逾期利息余额（单位分） **/
	@Column(name = "CURR_OVD_INT_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currOvdIntBal;
	
	/** 本次还款前的应收未收逾期本金罚息余额（单位分） **/
	@Column(name = "CURR_OVD_PRIN_PNLT_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currOvdPrinPnltBal;
	
	/** 本次还款前的应收未收逾期利息罚息余额（单位分） **/
	@Column(name = "CURR_OVD_INT_PNLT_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currOvdIntPnltBal;
	
	/** 总金额（单位分） **/
	@Column(name = "REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repayAmt;
	
	/** 本次实还正常本金金额（单位分） **/
	@Column(name = "PAID_PRIN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidPrinAmt;
	
	/** 本次实还逾期本金金额（单位分） **/
	@Column(name = "PAID_OVD_PRIN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidOvdPrinAmt;
	
	/** 本次实还正常利息金额（单位分） **/
	@Column(name = "PAID_INT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidIntAmt;
	
	/** 本次实还逾期利息金额（单位分） **/
	@Column(name = "PAID_OVD_INT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidOvdIntAmt;
	
	/** 本次实还逾期本金罚息金额（单位分） **/
	@Column(name = "PAID_OVD_PRIN_PNLT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidOvdPrinPnltAmt;
	
	/** 本次实还逾期利息罚息金额（单位分） **/
	@Column(name = "PAID_OVD_INT_PNLT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidOvdIntPnltAmt;
	
	/** 本次还款对应的平台服务费金额（单位分） **/
	@Column(name = "FEE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal feeAmt;
	
	/** 录入日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 数据文件 **/
	@Column(name = "rcp_DATA_DATE", unique = false, nullable = true, length = 10)
	private String rcpDATADATE;
	
	/** 应计非应计标识，应计0，非应计1 **/
	@Column(name = "ACCRUED_STATUS", unique = false, nullable = true, length = 2)
	private String accruedStatus;
	
	/** 核销标识，已核销为Y，否则为N **/
	@Column(name = "WRITE_OFF", unique = false, nullable = true, length = 2)
	private String writeOff;
	
	/** 行政区划代码 **/
	@Column(name = "REGION_CODE", unique = false, nullable = true, length = 8)
	private String regionCode;
	
	/** 业务类型 **/
	@Column(name = "BSN_TYPE", unique = false, nullable = true, length = 8)
	private String bsnType;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param seqNo
	 */
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	
    /**
     * @return seqNo
     */
	public String getSeqNo() {
		return this.seqNo;
	}
	
	/**
	 * @param feeNo
	 */
	public void setFeeNo(String feeNo) {
		this.feeNo = feeNo;
	}
	
    /**
     * @return feeNo
     */
	public String getFeeNo() {
		return this.feeNo;
	}
	
	/**
	 * @param withdrawNo
	 */
	public void setWithdrawNo(String withdrawNo) {
		this.withdrawNo = withdrawNo;
	}
	
    /**
     * @return withdrawNo
     */
	public String getWithdrawNo() {
		return this.withdrawNo;
	}
	
	/**
	 * @param repayType
	 */
	public void setRepayType(String repayType) {
		this.repayType = repayType;
	}
	
    /**
     * @return repayType
     */
	public String getRepayType() {
		return this.repayType;
	}
	
	/**
	 * @param repayDate
	 */
	public void setRepayDate(String repayDate) {
		this.repayDate = repayDate;
	}
	
    /**
     * @return repayDate
     */
	public String getRepayDate() {
		return this.repayDate;
	}
	
	/**
	 * @param currPrinBal
	 */
	public void setCurrPrinBal(java.math.BigDecimal currPrinBal) {
		this.currPrinBal = currPrinBal;
	}
	
    /**
     * @return currPrinBal
     */
	public java.math.BigDecimal getCurrPrinBal() {
		return this.currPrinBal;
	}
	
	/**
	 * @param currOvdPrinBal
	 */
	public void setCurrOvdPrinBal(java.math.BigDecimal currOvdPrinBal) {
		this.currOvdPrinBal = currOvdPrinBal;
	}
	
    /**
     * @return currOvdPrinBal
     */
	public java.math.BigDecimal getCurrOvdPrinBal() {
		return this.currOvdPrinBal;
	}
	
	/**
	 * @param currIntBal
	 */
	public void setCurrIntBal(java.math.BigDecimal currIntBal) {
		this.currIntBal = currIntBal;
	}
	
    /**
     * @return currIntBal
     */
	public java.math.BigDecimal getCurrIntBal() {
		return this.currIntBal;
	}
	
	/**
	 * @param currOvdIntBal
	 */
	public void setCurrOvdIntBal(java.math.BigDecimal currOvdIntBal) {
		this.currOvdIntBal = currOvdIntBal;
	}
	
    /**
     * @return currOvdIntBal
     */
	public java.math.BigDecimal getCurrOvdIntBal() {
		return this.currOvdIntBal;
	}
	
	/**
	 * @param currOvdPrinPnltBal
	 */
	public void setCurrOvdPrinPnltBal(java.math.BigDecimal currOvdPrinPnltBal) {
		this.currOvdPrinPnltBal = currOvdPrinPnltBal;
	}
	
    /**
     * @return currOvdPrinPnltBal
     */
	public java.math.BigDecimal getCurrOvdPrinPnltBal() {
		return this.currOvdPrinPnltBal;
	}
	
	/**
	 * @param currOvdIntPnltBal
	 */
	public void setCurrOvdIntPnltBal(java.math.BigDecimal currOvdIntPnltBal) {
		this.currOvdIntPnltBal = currOvdIntPnltBal;
	}
	
    /**
     * @return currOvdIntPnltBal
     */
	public java.math.BigDecimal getCurrOvdIntPnltBal() {
		return this.currOvdIntPnltBal;
	}
	
	/**
	 * @param repayAmt
	 */
	public void setRepayAmt(java.math.BigDecimal repayAmt) {
		this.repayAmt = repayAmt;
	}
	
    /**
     * @return repayAmt
     */
	public java.math.BigDecimal getRepayAmt() {
		return this.repayAmt;
	}
	
	/**
	 * @param paidPrinAmt
	 */
	public void setPaidPrinAmt(java.math.BigDecimal paidPrinAmt) {
		this.paidPrinAmt = paidPrinAmt;
	}
	
    /**
     * @return paidPrinAmt
     */
	public java.math.BigDecimal getPaidPrinAmt() {
		return this.paidPrinAmt;
	}
	
	/**
	 * @param paidOvdPrinAmt
	 */
	public void setPaidOvdPrinAmt(java.math.BigDecimal paidOvdPrinAmt) {
		this.paidOvdPrinAmt = paidOvdPrinAmt;
	}
	
    /**
     * @return paidOvdPrinAmt
     */
	public java.math.BigDecimal getPaidOvdPrinAmt() {
		return this.paidOvdPrinAmt;
	}
	
	/**
	 * @param paidIntAmt
	 */
	public void setPaidIntAmt(java.math.BigDecimal paidIntAmt) {
		this.paidIntAmt = paidIntAmt;
	}
	
    /**
     * @return paidIntAmt
     */
	public java.math.BigDecimal getPaidIntAmt() {
		return this.paidIntAmt;
	}
	
	/**
	 * @param paidOvdIntAmt
	 */
	public void setPaidOvdIntAmt(java.math.BigDecimal paidOvdIntAmt) {
		this.paidOvdIntAmt = paidOvdIntAmt;
	}
	
    /**
     * @return paidOvdIntAmt
     */
	public java.math.BigDecimal getPaidOvdIntAmt() {
		return this.paidOvdIntAmt;
	}
	
	/**
	 * @param paidOvdPrinPnltAmt
	 */
	public void setPaidOvdPrinPnltAmt(java.math.BigDecimal paidOvdPrinPnltAmt) {
		this.paidOvdPrinPnltAmt = paidOvdPrinPnltAmt;
	}
	
    /**
     * @return paidOvdPrinPnltAmt
     */
	public java.math.BigDecimal getPaidOvdPrinPnltAmt() {
		return this.paidOvdPrinPnltAmt;
	}
	
	/**
	 * @param paidOvdIntPnltAmt
	 */
	public void setPaidOvdIntPnltAmt(java.math.BigDecimal paidOvdIntPnltAmt) {
		this.paidOvdIntPnltAmt = paidOvdIntPnltAmt;
	}
	
    /**
     * @return paidOvdIntPnltAmt
     */
	public java.math.BigDecimal getPaidOvdIntPnltAmt() {
		return this.paidOvdIntPnltAmt;
	}
	
	/**
	 * @param feeAmt
	 */
	public void setFeeAmt(java.math.BigDecimal feeAmt) {
		this.feeAmt = feeAmt;
	}
	
    /**
     * @return feeAmt
     */
	public java.math.BigDecimal getFeeAmt() {
		return this.feeAmt;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param rcpDATADATE
	 */
	public void setRcpDATADATE(String rcpDATADATE) {
		this.rcpDATADATE = rcpDATADATE;
	}
	
    /**
     * @return rcpDATADATE
     */
	public String getRcpDATADATE() {
		return this.rcpDATADATE;
	}
	
	/**
	 * @param accruedStatus
	 */
	public void setAccruedStatus(String accruedStatus) {
		this.accruedStatus = accruedStatus;
	}
	
    /**
     * @return accruedStatus
     */
	public String getAccruedStatus() {
		return this.accruedStatus;
	}
	
	/**
	 * @param writeOff
	 */
	public void setWriteOff(String writeOff) {
		this.writeOff = writeOff;
	}
	
    /**
     * @return writeOff
     */
	public String getWriteOff() {
		return this.writeOff;
	}
	
	/**
	 * @param regionCode
	 */
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	
    /**
     * @return regionCode
     */
	public String getRegionCode() {
		return this.regionCode;
	}
	
	/**
	 * @param bsnType
	 */
	public void setBsnType(String bsnType) {
		this.bsnType = bsnType;
	}
	
    /**
     * @return bsnType
     */
	public String getBsnType() {
		return this.bsnType;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}