/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.core;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.core.BatTCoreKappJiejrb;
import cn.com.yusys.yusp.batch.service.load.core.BatTCoreKappJiejrbService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTCoreKappJiejrbResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battcorekappjiejrb")
public class BatTCoreKappJiejrbResource {
    @Autowired
    private BatTCoreKappJiejrbService batTCoreKappJiejrbService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTCoreKappJiejrb>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTCoreKappJiejrb> list = batTCoreKappJiejrbService.selectAll(queryModel);
        return new ResultDto<List<BatTCoreKappJiejrb>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTCoreKappJiejrb>> index(QueryModel queryModel) {
        List<BatTCoreKappJiejrb> list = batTCoreKappJiejrbService.selectByModel(queryModel);
        return new ResultDto<List<BatTCoreKappJiejrb>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTCoreKappJiejrb> create(@RequestBody BatTCoreKappJiejrb batTCoreKappJiejrb) throws URISyntaxException {
        batTCoreKappJiejrbService.insert(batTCoreKappJiejrb);
        return new ResultDto<BatTCoreKappJiejrb>(batTCoreKappJiejrb);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTCoreKappJiejrb batTCoreKappJiejrb) throws URISyntaxException {
        int result = batTCoreKappJiejrbService.update(batTCoreKappJiejrb);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String farendma, String guobiedm, String fenhdaim, String huobdaih, String duigduis, String jiejiari) {
        int result = batTCoreKappJiejrbService.deleteByPrimaryKey(farendma, guobiedm, fenhdaim, huobdaih, duigduis, jiejiari);
        return new ResultDto<Integer>(result);
    }

}
