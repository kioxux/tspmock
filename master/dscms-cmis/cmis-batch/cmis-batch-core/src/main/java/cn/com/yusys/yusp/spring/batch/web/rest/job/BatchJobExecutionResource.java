/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.web.rest.job;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.spring.batch.domain.job.BatchJobExecution;
import cn.com.yusys.yusp.spring.batch.service.job.BatchJobExecutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobExecutionResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batchjobexecution")
public class BatchJobExecutionResource {
    @Autowired
    private BatchJobExecutionService batchJobExecutionService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatchJobExecution>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatchJobExecution> list = batchJobExecutionService.selectAll(queryModel);
        return new ResultDto<List<BatchJobExecution>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatchJobExecution>> index(QueryModel queryModel) {
        List<BatchJobExecution> list = batchJobExecutionService.selectByModel(queryModel);
        return new ResultDto<List<BatchJobExecution>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatchJobExecution>> query(@RequestBody QueryModel queryModel) {
        List<BatchJobExecution> list = batchJobExecutionService.selectByModel(queryModel);
        return new ResultDto<List<BatchJobExecution>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{jobExecutionId}")
    protected ResultDto<BatchJobExecution> show(@PathVariable("jobExecutionId") Long jobExecutionId) {
        BatchJobExecution batchJobExecution = batchJobExecutionService.selectByPrimaryKey(jobExecutionId);
        return new ResultDto<BatchJobExecution>(batchJobExecution);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatchJobExecution> create(@RequestBody BatchJobExecution batchJobExecution) throws URISyntaxException {
        batchJobExecutionService.insert(batchJobExecution);
        return new ResultDto<BatchJobExecution>(batchJobExecution);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatchJobExecution batchJobExecution) throws URISyntaxException {
        int result = batchJobExecutionService.update(batchJobExecution);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{jobExecutionId}")
    protected ResultDto<Integer> delete(@PathVariable("jobExecutionId") Long jobExecutionId) {
        int result = batchJobExecutionService.deleteByPrimaryKey(jobExecutionId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batchJobExecutionService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
