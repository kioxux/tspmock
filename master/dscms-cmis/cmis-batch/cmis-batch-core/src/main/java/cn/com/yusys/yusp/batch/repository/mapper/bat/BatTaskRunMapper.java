/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTaskRunMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 16:29:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface BatTaskRunMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    BatTaskRun selectByPrimaryKey(@Param("taskDate") String taskDate, @Param("taskNo") String taskNo);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<BatTaskRun> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(BatTaskRun record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(BatTaskRun record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(BatTaskRun record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(BatTaskRun record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("taskDate") String taskDate, @Param("taskNo") String taskNo);

    /**
     * 根据营业日期将任务运行日志中数据删除
     *
     * @param openDay
     * @return
     */
    int deleteByOpenDay(@Param("openDay") String openDay);

    /**
     * 查询任务状态不为执行成功的列表
     *
     * @param model
     * @return
     */
    List<BatTaskRun> queryNonSuccessTaskStatus(QueryModel model);

    /**
     * 查询待执行的任务
     *
     * @param btrlQm
     * @return
     */
    List<BatTaskRun> selectPendingTask(QueryModel btrlQm);

    /**
     * 根据优先级别[日终一阶段(加载外围系统文件)]查询任务信息
     *
     * @param queryMap
     * @return
     */
    Map selectPriFlag01TaskInfo(Map queryMap);

    /**
     * 根据优先级别[日终二阶段(台账和额度等加工任务)]查询任务信息
     *
     * @param queryMap
     * @return
     */
    Map selectPriFlag02TaskInfo(Map queryMap);

    /**
     * 根据优先级别[日终三阶段(贷后管理风险分类)]查询任务信息
     *
     * @param queryMap
     * @return
     */
    Map selectPriFlag03TaskInfo(Map queryMap);

    /**
     * 根据任务日期和任务级别查询bat_task_run中任务状态为执行失败的列表
     *
     * @param failedQueryModel
     */
    List<BatTaskRun> selectFailedTaskList(QueryModel failedQueryModel);
}