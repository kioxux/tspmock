package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0123</br>
 * 任务名称：加工任务-业务处理-资金同业授信客户准入名单年审提醒 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0123Mapper {

    /**
     * 插入消息提示表 ：资金同业客户 准入名单年审即将到期
     * @param openDay
     * @return
     */
    int insertWbMsgNotice(@Param("openDay") String openDay);

    /**
     * 插入消息提示表 ：	资金同业客户准入名单年审任务生成
     * @param openDay
     * @return
     */
    int  insertIntbankOrgAdmitApp(@Param("openDay") String openDay);


    /**
     * updateInbankOrgAdmitAcc 更新同业机构准入台账表
     * @param openDay
     * @return
     */
    int updateInbankOrgAdmitAcc(@Param("openDay") String openDay);
}
