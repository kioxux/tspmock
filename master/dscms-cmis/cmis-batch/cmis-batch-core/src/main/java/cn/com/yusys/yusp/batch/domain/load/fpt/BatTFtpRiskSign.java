/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.fpt;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatTFtpRiskSign
 * @类描述: bat_t_ftp_risk_sign数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-09 17:17:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_ftp_risk_sign")
public class BatTFtpRiskSign extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 预警信号ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "risk_id")
	private String riskId;
	
	/** 规则编号 **/
	@Column(name = "risk_num", unique = false, nullable = true, length = 20)
	private String riskNum;
	
	/** 客户编号 **/
	@Column(name = "cust_id", unique = false, nullable = true, length = 32)
	private String custId;
	
	/** 客户名称 **/
	@Column(name = "cust_name", unique = false, nullable = true, length = 80)
	private String custName;
	
	/** 客户类型 **/
	@Column(name = "cust_type", unique = false, nullable = true, length = 10)
	private String custType;
	
	/** 预警大类 **/
	@Column(name = "risk_big", unique = false, nullable = true, length = 32)
	private String riskBig;
	
	/** 预警子项 **/
	@Column(name = "risk_sub", unique = false, nullable = true, length = 2000)
	private String riskSub;
	
	/** 预警条件 **/
	@Column(name = "risk_reasion", unique = false, nullable = true, length = 2000)
	private String riskReasion;
	
	/** 预警输出描述 **/
	@Column(name = "risk_out_reasion", unique = false, nullable = true, length = 4000)
	private String riskOutReasion;
	
	/** 信号名称 **/
	@Column(name = "sign_name", unique = false, nullable = true, length = 300)
	private String signName;
	
	/** 预警日期 **/
	@Column(name = "risk_date", unique = false, nullable = true, length = 12)
	private String riskDate;
	
	/** 信号等级 **/
	@Column(name = "risk_lvl", unique = false, nullable = true, length = 4)
	private String riskLvl;
	
	/** 信号来源 **/
	@Column(name = "sign_source", unique = false, nullable = true, length = 4)
	private String signSource;
	
	/** 预警类型 **/
	@Column(name = "risk_type", unique = false, nullable = true, length = 8)
	private String riskType;
	
	/** 预警状态 **/
	@Column(name = "risk_status", unique = false, nullable = true, length = 20)
	private String riskStatus;
	
	/** 客户经理 **/
	@Column(name = "main_mgr", unique = false, nullable = true, length = 20)
	private String mainMgr;
	
	/** 主管机构 **/
	@Column(name = "main_br_id", unique = false, nullable = true, length = 20)
	private String mainBrId;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 12)
	private String inputDate;
	
	/** 处置期限 **/
	@Column(name = "deal_term_day", unique = false, nullable = true, length = 10)
	private Integer dealTermDay;
	
	/** 预警单审阅状态 **/
	@Column(name = "risk_read_state", unique = false, nullable = true, length = 2)
	private String riskReadState;
	
	/** 是否重新打开 **/
	@Column(name = "is_reopen", unique = false, nullable = true, length = 2)
	private String isReopen;
	
	/** 是否从提醒类变成预警类 STD_IS_YES_NO **/
	@Column(name = "is_changed", unique = false, nullable = true, length = 2)
	private String isChanged;
	
	/** 是否采取措施 STD_IS_YES_NO **/
	@Column(name = "is_deal", unique = false, nullable = true, length = 2)
	private String isDeal;
	
	/** 采取处置措施 **/
	@Column(name = "deal_measure", unique = false, nullable = true, length = 40)
	private String dealMeasure;
	
	/** 风险排查结果 **/
	@Column(name = "invest_result", unique = false, nullable = true, length = 2000)
	private String investResult;
	
	/** 拟采取措施 **/
	@Column(name = "take_measure", unique = false, nullable = true, length = 2000)
	private String takeMeasure;
	
	/** 授信额度 **/
	@Column(name = "credit_balance", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal creditBalance;
	
	/** 贷款余额 **/
	@Column(name = "used_balance", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal usedBalance;
	
	/** 不良贷款余额 **/
	@Column(name = "bad_balance", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal badBalance;
	
	/** 对外担保户数 **/
	@Column(name = "out_guaranty_no", unique = false, nullable = true, length = 10)
	private Integer outGuarantyNo;
	
	/** 担保贷款余额 **/
	@Column(name = "guaranty_balance", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal guarantyBalance;
	
	/** 不良担保贷款余额 **/
	@Column(name = "bad_guaranty_balance", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal badGuarantyBalance;
	
	/** 预警分值 **/
	@Column(name = "risk_score", unique = false, nullable = true, length = 10)
	private Integer riskScore;
	
	/** 发布流水号 **/
	@Column(name = "pub_serno", unique = false, nullable = true, length = 32)
	private String pubSerno;
	
	/** 行业分类 **/
	@Column(name = "indstry", unique = false, nullable = true, length = 15)
	private String indstry;
	
	/** 是否持续跟踪 STD_IS_YES_NO **/
	@Column(name = "is_follow", unique = false, nullable = true, length = 1)
	private String isFollow;
	
	/** 流程状态 **/
	@Column(name = "wf_appr_sts", unique = false, nullable = true, length = 3)
	private String wfApprSts;
	
	/** 跟踪周期 **/
	@Column(name = "follow_date", unique = false, nullable = true, length = 10)
	private Integer followDate;
	
	/** 业务编号 **/
	@Column(name = "biz_no", unique = false, nullable = true, length = 4000)
	private String bizNo;
	
	/** 风险分类 **/
	@Column(name = "risk_class", unique = false, nullable = true, length = 10)
	private String riskClass;
	
	/** 是否规则校验 STD_IS_YES_NO **/
	@Column(name = "is_check", unique = false, nullable = true, length = 2)
	private String isCheck;
	
	/** 借据号 **/
	@Column(name = "bill_no", unique = false, nullable = true, length = 60)
	private String billNo;
	
	/** 推送客户经理 **/
	@Column(name = "deal_mngr", unique = false, nullable = true, length = 20)
	private String dealMngr;
	
	/** 推送机构 **/
	@Column(name = "deal_org", unique = false, nullable = true, length = 20)
	private String dealOrg;
	
	/** 通知客户经理 **/
	@Column(name = "oth_mngr", unique = false, nullable = true, length = 100)
	private String othMngr;
	
	
	/**
	 * @param riskId
	 */
	public void setRiskId(String riskId) {
		this.riskId = riskId;
	}
	
    /**
     * @return riskId
     */
	public String getRiskId() {
		return this.riskId;
	}
	
	/**
	 * @param riskNum
	 */
	public void setRiskNum(String riskNum) {
		this.riskNum = riskNum;
	}
	
    /**
     * @return riskNum
     */
	public String getRiskNum() {
		return this.riskNum;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param custType
	 */
	public void setCustType(String custType) {
		this.custType = custType;
	}
	
    /**
     * @return custType
     */
	public String getCustType() {
		return this.custType;
	}
	
	/**
	 * @param riskBig
	 */
	public void setRiskBig(String riskBig) {
		this.riskBig = riskBig;
	}
	
    /**
     * @return riskBig
     */
	public String getRiskBig() {
		return this.riskBig;
	}
	
	/**
	 * @param riskSub
	 */
	public void setRiskSub(String riskSub) {
		this.riskSub = riskSub;
	}
	
    /**
     * @return riskSub
     */
	public String getRiskSub() {
		return this.riskSub;
	}
	
	/**
	 * @param riskReasion
	 */
	public void setRiskReasion(String riskReasion) {
		this.riskReasion = riskReasion;
	}
	
    /**
     * @return riskReasion
     */
	public String getRiskReasion() {
		return this.riskReasion;
	}
	
	/**
	 * @param riskOutReasion
	 */
	public void setRiskOutReasion(String riskOutReasion) {
		this.riskOutReasion = riskOutReasion;
	}
	
    /**
     * @return riskOutReasion
     */
	public String getRiskOutReasion() {
		return this.riskOutReasion;
	}
	
	/**
	 * @param signName
	 */
	public void setSignName(String signName) {
		this.signName = signName;
	}
	
    /**
     * @return signName
     */
	public String getSignName() {
		return this.signName;
	}
	
	/**
	 * @param riskDate
	 */
	public void setRiskDate(String riskDate) {
		this.riskDate = riskDate;
	}
	
    /**
     * @return riskDate
     */
	public String getRiskDate() {
		return this.riskDate;
	}
	
	/**
	 * @param riskLvl
	 */
	public void setRiskLvl(String riskLvl) {
		this.riskLvl = riskLvl;
	}
	
    /**
     * @return riskLvl
     */
	public String getRiskLvl() {
		return this.riskLvl;
	}
	
	/**
	 * @param signSource
	 */
	public void setSignSource(String signSource) {
		this.signSource = signSource;
	}
	
    /**
     * @return signSource
     */
	public String getSignSource() {
		return this.signSource;
	}
	
	/**
	 * @param riskType
	 */
	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	
    /**
     * @return riskType
     */
	public String getRiskType() {
		return this.riskType;
	}
	
	/**
	 * @param riskStatus
	 */
	public void setRiskStatus(String riskStatus) {
		this.riskStatus = riskStatus;
	}
	
    /**
     * @return riskStatus
     */
	public String getRiskStatus() {
		return this.riskStatus;
	}
	
	/**
	 * @param mainMgr
	 */
	public void setMainMgr(String mainMgr) {
		this.mainMgr = mainMgr;
	}
	
    /**
     * @return mainMgr
     */
	public String getMainMgr() {
		return this.mainMgr;
	}
	
	/**
	 * @param mainBrId
	 */
	public void setMainBrId(String mainBrId) {
		this.mainBrId = mainBrId;
	}
	
    /**
     * @return mainBrId
     */
	public String getMainBrId() {
		return this.mainBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param dealTermDay
	 */
	public void setDealTermDay(Integer dealTermDay) {
		this.dealTermDay = dealTermDay;
	}
	
    /**
     * @return dealTermDay
     */
	public Integer getDealTermDay() {
		return this.dealTermDay;
	}
	
	/**
	 * @param riskReadState
	 */
	public void setRiskReadState(String riskReadState) {
		this.riskReadState = riskReadState;
	}
	
    /**
     * @return riskReadState
     */
	public String getRiskReadState() {
		return this.riskReadState;
	}
	
	/**
	 * @param isReopen
	 */
	public void setIsReopen(String isReopen) {
		this.isReopen = isReopen;
	}
	
    /**
     * @return isReopen
     */
	public String getIsReopen() {
		return this.isReopen;
	}
	
	/**
	 * @param isChanged
	 */
	public void setIsChanged(String isChanged) {
		this.isChanged = isChanged;
	}
	
    /**
     * @return isChanged
     */
	public String getIsChanged() {
		return this.isChanged;
	}
	
	/**
	 * @param isDeal
	 */
	public void setIsDeal(String isDeal) {
		this.isDeal = isDeal;
	}
	
    /**
     * @return isDeal
     */
	public String getIsDeal() {
		return this.isDeal;
	}
	
	/**
	 * @param dealMeasure
	 */
	public void setDealMeasure(String dealMeasure) {
		this.dealMeasure = dealMeasure;
	}
	
    /**
     * @return dealMeasure
     */
	public String getDealMeasure() {
		return this.dealMeasure;
	}
	
	/**
	 * @param investResult
	 */
	public void setInvestResult(String investResult) {
		this.investResult = investResult;
	}
	
    /**
     * @return investResult
     */
	public String getInvestResult() {
		return this.investResult;
	}
	
	/**
	 * @param takeMeasure
	 */
	public void setTakeMeasure(String takeMeasure) {
		this.takeMeasure = takeMeasure;
	}
	
    /**
     * @return takeMeasure
     */
	public String getTakeMeasure() {
		return this.takeMeasure;
	}
	
	/**
	 * @param creditBalance
	 */
	public void setCreditBalance(java.math.BigDecimal creditBalance) {
		this.creditBalance = creditBalance;
	}
	
    /**
     * @return creditBalance
     */
	public java.math.BigDecimal getCreditBalance() {
		return this.creditBalance;
	}
	
	/**
	 * @param usedBalance
	 */
	public void setUsedBalance(java.math.BigDecimal usedBalance) {
		this.usedBalance = usedBalance;
	}
	
    /**
     * @return usedBalance
     */
	public java.math.BigDecimal getUsedBalance() {
		return this.usedBalance;
	}
	
	/**
	 * @param badBalance
	 */
	public void setBadBalance(java.math.BigDecimal badBalance) {
		this.badBalance = badBalance;
	}
	
    /**
     * @return badBalance
     */
	public java.math.BigDecimal getBadBalance() {
		return this.badBalance;
	}
	
	/**
	 * @param outGuarantyNo
	 */
	public void setOutGuarantyNo(Integer outGuarantyNo) {
		this.outGuarantyNo = outGuarantyNo;
	}
	
    /**
     * @return outGuarantyNo
     */
	public Integer getOutGuarantyNo() {
		return this.outGuarantyNo;
	}
	
	/**
	 * @param guarantyBalance
	 */
	public void setGuarantyBalance(java.math.BigDecimal guarantyBalance) {
		this.guarantyBalance = guarantyBalance;
	}
	
    /**
     * @return guarantyBalance
     */
	public java.math.BigDecimal getGuarantyBalance() {
		return this.guarantyBalance;
	}
	
	/**
	 * @param badGuarantyBalance
	 */
	public void setBadGuarantyBalance(java.math.BigDecimal badGuarantyBalance) {
		this.badGuarantyBalance = badGuarantyBalance;
	}
	
    /**
     * @return badGuarantyBalance
     */
	public java.math.BigDecimal getBadGuarantyBalance() {
		return this.badGuarantyBalance;
	}
	
	/**
	 * @param riskScore
	 */
	public void setRiskScore(Integer riskScore) {
		this.riskScore = riskScore;
	}
	
    /**
     * @return riskScore
     */
	public Integer getRiskScore() {
		return this.riskScore;
	}
	
	/**
	 * @param pubSerno
	 */
	public void setPubSerno(String pubSerno) {
		this.pubSerno = pubSerno;
	}
	
    /**
     * @return pubSerno
     */
	public String getPubSerno() {
		return this.pubSerno;
	}
	
	/**
	 * @param indstry
	 */
	public void setIndstry(String indstry) {
		this.indstry = indstry;
	}
	
    /**
     * @return indstry
     */
	public String getIndstry() {
		return this.indstry;
	}
	
	/**
	 * @param isFollow
	 */
	public void setIsFollow(String isFollow) {
		this.isFollow = isFollow;
	}
	
    /**
     * @return isFollow
     */
	public String getIsFollow() {
		return this.isFollow;
	}
	
	/**
	 * @param wfApprSts
	 */
	public void setWfApprSts(String wfApprSts) {
		this.wfApprSts = wfApprSts;
	}
	
    /**
     * @return wfApprSts
     */
	public String getWfApprSts() {
		return this.wfApprSts;
	}
	
	/**
	 * @param followDate
	 */
	public void setFollowDate(Integer followDate) {
		this.followDate = followDate;
	}
	
    /**
     * @return followDate
     */
	public Integer getFollowDate() {
		return this.followDate;
	}
	
	/**
	 * @param bizNo
	 */
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	
    /**
     * @return bizNo
     */
	public String getBizNo() {
		return this.bizNo;
	}
	
	/**
	 * @param riskClass
	 */
	public void setRiskClass(String riskClass) {
		this.riskClass = riskClass;
	}
	
    /**
     * @return riskClass
     */
	public String getRiskClass() {
		return this.riskClass;
	}
	
	/**
	 * @param isCheck
	 */
	public void setIsCheck(String isCheck) {
		this.isCheck = isCheck;
	}
	
    /**
     * @return isCheck
     */
	public String getIsCheck() {
		return this.isCheck;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param dealMngr
	 */
	public void setDealMngr(String dealMngr) {
		this.dealMngr = dealMngr;
	}
	
    /**
     * @return dealMngr
     */
	public String getDealMngr() {
		return this.dealMngr;
	}
	
	/**
	 * @param dealOrg
	 */
	public void setDealOrg(String dealOrg) {
		this.dealOrg = dealOrg;
	}
	
    /**
     * @return dealOrg
     */
	public String getDealOrg() {
		return this.dealOrg;
	}
	
	/**
	 * @param othMngr
	 */
	public void setOthMngr(String othMngr) {
		this.othMngr = othMngr;
	}
	
    /**
     * @return othMngr
     */
	public String getOthMngr() {
		return this.othMngr;
	}


}