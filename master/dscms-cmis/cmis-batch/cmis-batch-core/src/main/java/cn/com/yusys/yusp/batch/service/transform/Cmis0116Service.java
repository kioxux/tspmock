package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0116Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0116</br>
 * 任务名称：加工任务-业务处理-信用卡额度同步  </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0116Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0116Service.class);

    @Autowired
    private Cmis0116Mapper cmis0116Mapper;

    /**
     * 清除 批复分项信息
     *
     * @param openDay
     */
    public void cmis0016DeleteApprStrMtableInfo(String openDay) {
        logger.info("清除批复分项信息-清除所有信用卡额度开始，请求参数：[{}]", openDay);
        cmis0116Mapper.deleteApprStrMtableInfo01(openDay);
        logger.info("清除批复分项信息-清除所有信用卡额度结束");

    }

    /**
     * 清除 批复主信息
     *
     * @param openDay
     */
    public void cmis0016DeleteApprLmtSubBasicInfo01(String openDay) {
        logger.info("清除批复主信息-清除所有信用卡额度开始，请求参数：[{}]", openDay);
        cmis0116Mapper.deleteApprLmtSubBasicInfo01(openDay);
        logger.info("清除批复主信息-清除所有信用卡额度结束");

    }

    public void cmis0016UpdateBatSDjkDataAccount(String openDay) {

        logger.info("更新通联推送的文件日期为10位日期开始，请求参数：[{}]", openDay);
        int updateBatSDjkDataAccount = cmis0116Mapper.updateBatSDjkDataAccount(openDay);
        logger.info("更新通联推送的文件日期为10位日期结束，响应参数：[{}]", updateBatSDjkDataAccount);
    }

    /**
     * 插入批复分项信息
     *
     * @param openDay
     */
    public void cmis0016InsertApprLmtSubBasicInfo(String openDay) {
        logger.info("插入批复分项信息对应账户信息文件开始，请求参数：[{}]", openDay);
        int insertApprLmtSubBasicInfo01 = cmis0116Mapper.insertApprLmtSubBasicInfo01(openDay);
        logger.info("插入批复分项信息对应账户信息文件结束，响应参数：[{}]", insertApprLmtSubBasicInfo01);

    }

    /**
     * 更新批复分项信息
     *
     * @param openDay
     */
    public void cmis0016UpdateApprLmtSubBasicInfo(String openDay) {
        logger.info("更新批复分项信息对应账户信息文件开始，请求参数：[{}]", openDay);
        int updateApprLmtSubBasicInfo01 = cmis0116Mapper.updateApprLmtSubBasicInfo01(openDay);
        logger.info("更新批复分项信息对应账户信息文件结束，响应参数：[{}]", updateApprLmtSubBasicInfo01);

    }

    /**
     * 插入批复主信息
     *
     * @param openDay
     */
    public void cmis0016InsertApprStrMtableInfo01(String openDay) {
        logger.info("插入批复主信息对应账户信息文件开始，请求参数：[{}]", openDay);
        int insertApprStrMtableInfo01 = cmis0116Mapper.insertApprStrMtableInfo01(openDay);
        logger.info("插入批复主信息对应账户信息文件结束，响应参数：[{}]", insertApprStrMtableInfo01);

    }


    /**
     * 插入批复分项信息，新增额度分项  对应大额分期文件
     *
     * @param openDay
     */
    public void cmis0016InsertApprLmtSubBasicInfo02(String openDay) {
        logger.info("插入批复分项信息对应大额分期文件，请求参数：[{}]", openDay);
        int insertApprLmtSubBasicInfo02 = cmis0116Mapper.insertApprLmtSubBasicInfo02(openDay);
        logger.info("插入批复主信息对应对应大额分期结束，响应参数：[{}]", insertApprLmtSubBasicInfo02);

    }

    /**
     * 插入分项占用关系，复核大额分期合同数据，是否都存在分项占用关系，如不存在，则新增；
     *
     * @param openDay
     */
    public void cmis0016InsertLmtContRel(String openDay) {
        logger.info("插入分项占用关系开始，请求参数：[{}]", openDay);
        int insertLmtContRel = cmis0116Mapper.insertLmtContRel(openDay);
        logger.info("插入分项占用关系结束，响应参数：[{}]", insertLmtContRel);
    }

    /**
     * 删除合同占用关系信息
     *
     * @param openDay
     */
    public void cmis0016DeleteContAccRel(String openDay) {
        logger.info("删除合同占用关系信息开始，请求参数：[{}]", openDay);
        int deleteContAccRel = cmis0116Mapper.deleteContAccRel(openDay);
        logger.info("删除合同占用关系信息结束，响应参数：[{}]", deleteContAccRel);
    }


    /**
     * 插入合同占用关系信息，复核大额分期通联推送台账数据，是否存在台账占用关系，如不存在，则新增，如存在则更新余额；
     *
     * @param openDay
     */
    public void cmis0016InsertContAccRel(String openDay) {
        logger.info("插入合同占用关系信息开始，请求参数：[{}]", openDay);
        int insertContAccRel = cmis0116Mapper.insertContAccRel(openDay);
        logger.info("插入合同占用关系信息结束，响应参数：[{}]", insertContAccRel);
    }


    /**
     * 22010201	专项消费分期 分项占用关系信息 占用总余额（折人民币）更新
     *
     * @param openDay
     */
    public void cmis0016LmtContRel(String openDay) {
        logger.info("删除合同占用关系信息开始，请求参数：[{}]", openDay);
        cmis0116Mapper.truncateLmtContRel01(openDay);
        logger.info("删除合同占用关系信息结束");

        logger.info("22010201专项消费分期 分项占用关系信息 占用总余额（折人民币）更新开始，请求参数：[{}]", openDay);
        int insertLmtContRel01 = cmis0116Mapper.insertLmtContRel01(openDay);
        logger.info("22010201专项消费分期 分项占用关系信息 占用总余额（折人民币）更新结束，响应参数：[{}]", insertLmtContRel01);

        logger.info("22010201专项消费分期 分项占用关系信息 占用总余额（折人民币）更新开始，请求参数：[{}]", openDay);
        int updateLmtContRel01 = cmis0116Mapper.updateLmtContRel01(openDay);
        logger.info("22010201专项消费分期 分项占用关系信息 占用总余额（折人民币）更新结束，响应参数：[{}]", updateLmtContRel01);

        logger.info("22010201专项消费分期 分项占用关系信息 占用敞口余额（折人民币） 更新 开始，请求参数：[{}]", openDay);
        int updateLmtContRel02 = cmis0116Mapper.updateLmtContRel02(openDay);
        logger.info("22010201专项消费分期 分项占用关系信息 占用敞口余额（折人民币） 结束，响应参数：[{}]", updateLmtContRel02);

    }


    /**
     * 22010201 专项消费分期 批复额度分项基础信息更新占用额
     *
     * @param openDay
     */
    public void cmis0016ApprLmtSubBasicInfo02A(String openDay) {
        logger.info("22010201 专项消费分期 批复额度分项基础信息更新占用额开始，请求参数：[{}]", openDay);
        cmis0116Mapper.truncateApprLmtSubBasicInfo02A(openDay);
        logger.info("22010201 专项消费分期 批复额度分项基础信息更新占用额结束");

        logger.info("22010201专项消费分期 分项占用关系信息 占用总余额（折人民币）更新开始，请求参数：[{}]", openDay);
        int insertApprLmtSubBasicInfo02A = cmis0116Mapper.insertApprLmtSubBasicInfo02A(openDay);
        logger.info("22010201专项消费分期 分项占用关系信息 占用总余额（折人民币）更新结束，响应参数：[{}]", insertApprLmtSubBasicInfo02A);

        logger.info("22010201专项消费分期 分项占用关系信息 占用总余额（折人民币）更新开始，请求参数：[{}]", openDay);
        int updateApprLmtSubBasicInfo02A = cmis0116Mapper.updateApprLmtSubBasicInfo02A(openDay);
        logger.info("22010201专项消费分期 分项占用关系信息 占用总余额（折人民币）更新结束，响应参数：[{}]", updateApprLmtSubBasicInfo02A);
    }

    /**
     * 22010201专项消费分期 更新额度分项的 敞口已用总额更新
     *
     * @param openDay
     */
    public void updateApprLmtSubBasicInfo03(String openDay) {
        logger.info("22010201专项消费分期 更新额度分项的 敞口已用总额更新开始，请求参数：[{}]", openDay);
        int updateApprLmtSubBasicInfo03 = cmis0116Mapper.updateApprLmtSubBasicInfo03(openDay);
        logger.info(" 22010201专项消费分期 更新额度分项的 敞口已用总额更新结束，响应参数：[{}]", updateApprLmtSubBasicInfo03);
    }


    /**
     * 更新额度分项的LOAN_BALANCE贷款余额
     *
     * @param openDay
     */
    public void cmis0016ApprLmtSubBasicInfo04(String openDay) {
        logger.info("更新额度分项的LOAN_BALANCE贷款余额开始，请求参数：[{}]", openDay);
        cmis0116Mapper.truncateApprLmtSubBasicInfo04(openDay);
        logger.info("更新额度分项的LOAN_BALANCE贷款余额结束");

        logger.info("更新额度分项的LOAN_BALANCE贷款余额开始，请求参数：[{}]", openDay);
        int insertApprLmtSubBasicInfo04 = cmis0116Mapper.insertApprLmtSubBasicInfo04(openDay);
        logger.info("更新额度分项的LOAN_BALANCE贷款余额结束，响应参数：[{}]", insertApprLmtSubBasicInfo04);

        logger.info("更新额度分项的LOAN_BALANCE贷款余额开始，请求参数：[{}]", openDay);
        int updateApprLmtSubBasicInfo04 = cmis0116Mapper.updateApprLmtSubBasicInfo04(openDay);
        logger.info("更新额度分项的LOAN_BALANCE贷款余额结束，响应参数：[{}]", updateApprLmtSubBasicInfo04);
    }

    /**
     * 更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额
     *
     * @param openDay
     */
    public void cmis0016ApprLmtSubBasicInfo05(String openDay) {
        logger.info("更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额开始，请求参数：[{}]", openDay);
        cmis0116Mapper.truncateApprLmtSubBasicInfo05(openDay);
        logger.info("更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额结束");

        logger.info("更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额开始，请求参数：[{}]", openDay);
        int insertApprLmtSubBasicInfo05 = cmis0116Mapper.insertApprLmtSubBasicInfo05(openDay);
        logger.info("更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额结束，响应参数：[{}]", insertApprLmtSubBasicInfo05);

        logger.info("更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额开始，请求参数：[{}]", openDay);
        int updateApprLmtSubBasicInfo05 = cmis0116Mapper.updateApprLmtSubBasicInfo05(openDay);
        logger.info("更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额结束，响应参数：[{}]", updateApprLmtSubBasicInfo05);
    }

    /**
     * 更新AVL_OUTSTND_AMT可出账金额
     *
     * @param openDay
     */
    public void cmis0016ApprLmtSubBasicInfo06(String openDay) {
        logger.info("更新AVL_OUTSTND_AMT可出账金额开始，请求参数：[{}]", openDay);
        int updateApprLmtSubBasicInfo06 = cmis0116Mapper.updateApprLmtSubBasicInfo06(openDay);
        logger.info("更新AVL_OUTSTND_AMT可出账金额结束，响应参数：[{}]", updateApprLmtSubBasicInfo06);
    }

    /**
     * 授信批复台账表 22010201	专项消费分期
     *
     * @param openDay
     */
    public void cmis0116ApprStrMtableInfo02(String openDay) {
        logger.info("更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额开始，请求参数：[{}]", openDay);
        cmis0116Mapper.deleteApprStrMtableInfo02(openDay);
        logger.info("更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额结束");

        logger.info("重新插入批复主信息 22010201 专项消费分期开始，请求参数：[{}]", openDay);
        int insertApprStrMtableInfo02 = cmis0116Mapper.insertApprStrMtableInfo02(openDay);
        logger.info("重新插入批复主信息 22010201 专项消费分期结束，响应参数：[{}]", insertApprStrMtableInfo02);
    }


}
