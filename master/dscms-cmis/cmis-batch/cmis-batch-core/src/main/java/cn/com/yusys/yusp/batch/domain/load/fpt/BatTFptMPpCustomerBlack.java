/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.fpt;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTFptMPpCustomerBlack
 * @类描述: bat_t_fpt_m_pp_customer_black数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 10:18:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_fpt_m_pp_customer_black")
public class BatTFptMPpCustomerBlack extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 客户号 **/
	@Id
	@Column(name = "CUST_NO")
	private String custNo;
	
	/** 客户名称 **/
	@Id
	@Column(name = "CUST_NAME")
	private String custName;
	
	/** 黑名单描述 **/
	@Id
	@Column(name = "REMARK")
	private String remark;
	
	/** 下发日期 **/
	@Id
	@Column(name = "INPUT_DATE")
	private String inputDate;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 32)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_NO", unique = false, nullable = true, length = 32)
	private String certNo;
	
	/** 黑名单等级 **/
	@Column(name = "LEVEL_TYPE", unique = false, nullable = true, length = 10)
	private String levelType;
	
	/** 黑名单类型(01黑名单 02灰名单 03信用卡黑名单) **/
	@Column(name = "LIST_TYPE", unique = false, nullable = true, length = 10)
	private String listType;
	
	/** 流程编号 **/
	@Column(name = "DEAL_ID", unique = false, nullable = true, length = 32)
	private String dealId;
	
	/** 流程状态 **/
	@Column(name = "WF_APPR_STS", unique = false, nullable = true, length = 10)
	private String wfApprSts;
	
	/** 解除原因 **/
	@Column(name = "REMOVE_REASON", unique = false, nullable = true, length = 1000)
	private String removeReason;
	
	/** 解除时间 **/
	@Column(name = "SHUT_DATE", unique = false, nullable = true, length = 20)
	private String shutDate;
	
	/** 当前逾期金额 **/
	@Column(name = "CURRENT_BALANCE", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal currentBalance;
	
	/** 信用卡号 **/
	@Column(name = "CARD_NO", unique = false, nullable = true, length = 64)
	private String cardNo;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param custNo
	 */
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	
    /**
     * @return custNo
     */
	public String getCustNo() {
		return this.custNo;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}
	
    /**
     * @return certNo
     */
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param levelType
	 */
	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}
	
    /**
     * @return levelType
     */
	public String getLevelType() {
		return this.levelType;
	}
	
	/**
	 * @param listType
	 */
	public void setListType(String listType) {
		this.listType = listType;
	}
	
    /**
     * @return listType
     */
	public String getListType() {
		return this.listType;
	}
	
	/**
	 * @param dealId
	 */
	public void setDealId(String dealId) {
		this.dealId = dealId;
	}
	
    /**
     * @return dealId
     */
	public String getDealId() {
		return this.dealId;
	}
	
	/**
	 * @param wfApprSts
	 */
	public void setWfApprSts(String wfApprSts) {
		this.wfApprSts = wfApprSts;
	}
	
    /**
     * @return wfApprSts
     */
	public String getWfApprSts() {
		return this.wfApprSts;
	}
	
	/**
	 * @param removeReason
	 */
	public void setRemoveReason(String removeReason) {
		this.removeReason = removeReason;
	}
	
    /**
     * @return removeReason
     */
	public String getRemoveReason() {
		return this.removeReason;
	}
	
	/**
	 * @param shutDate
	 */
	public void setShutDate(String shutDate) {
		this.shutDate = shutDate;
	}
	
    /**
     * @return shutDate
     */
	public String getShutDate() {
		return this.shutDate;
	}
	
	/**
	 * @param currentBalance
	 */
	public void setCurrentBalance(java.math.BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}
	
    /**
     * @return currentBalance
     */
	public java.math.BigDecimal getCurrentBalance() {
		return this.currentBalance;
	}
	
	/**
	 * @param cardNo
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
    /**
     * @return cardNo
     */
	public String getCardNo() {
		return this.cardNo;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}