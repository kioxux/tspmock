package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0209Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0209</br>
 * 任务名称：加工任务-额度处理-同业交易对手额度占用</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0209Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0209Service.class);
    @Autowired
    private Cmis0209Mapper cmis0209Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新额度占用关系
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0209UpdateLmtContRel(String openDay) {
        logger.info("truncateTmpLmtAmt1清空加工占用授信表开始,请求参数为:[{}]", openDay);
        // cmis0209Mapper.truncateTmpLmtAmt1();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpLmtAmt1清空加工占用授信表结束");

        logger.info("代开保函 处理规则 本客户下 的 保函 金额 与余额处理开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt1A = cmis0209Mapper.insertTmpLmtAmt1A(openDay);
        logger.info("代开保函 处理规则 本客户下 的 保函 金额 与余额处理结束,返回参数为:[{}]", insertTmpLmtAmt1A);

        logger.info("代开信用证 处理规则 本客户下 的 保函 金额 与余额处理开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt1B = cmis0209Mapper.insertTmpLmtAmt1B(openDay);
        logger.info("代开信用证 处理规则 本客户下 的 保函 金额 与余额处理结束,返回参数为:[{}]", insertTmpLmtAmt1B);

        logger.info(" 福费廷（二级市场） 处理规则    本客户下 的 保函 金额 与余额处理开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt1C = cmis0209Mapper.insertTmpLmtAmt1C(openDay);
        logger.info(" 福费廷（二级市场） 处理规则    本客户下 的 保函 金额 与余额处理结束,返回参数为:[{}]", insertTmpLmtAmt1C);

        logger.info("更新额度占用关系处理开始,请求参数为:[{}]", openDay);
        int updateTmpLmtAmt1 = cmis0209Mapper.updateTmpLmtAmt1(openDay);
        logger.info("更新额度占用关系处理结束,返回参数为:[{}]", updateTmpLmtAmt1);


        logger.info("truncateTmpLmtAmt2清空加工占用授信表开始,请求参数为:[{}]", openDay);
        // cmis0209Mapper.truncateTmpLmtAmt2();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpLmtAmt2清空加工占用授信表结束");

        logger.info("代开银票 处理规则  占用同业额度处理开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt2 = cmis0209Mapper.insertTmpLmtAmt2(openDay);
        logger.info("代开银票 处理规则  占用同业额度处理结束,返回参数为:[{}]", insertTmpLmtAmt2);

        logger.info("更新额度占用关系处理开始,请求参数为:[{}]", openDay);
        int updateTmpLmtAmt2 = cmis0209Mapper.updateTmpLmtAmt2(openDay);
        logger.info("更新额度占用关系处理结束,返回参数为:[{}]", updateTmpLmtAmt2);


        logger.info("truncateTmpLmtAmt3清空加工占用授信表开始,请求参数为:[{}]", openDay);
        // cmis0209Mapper.truncateTmpLmtAmt3();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpLmtAmt3清空加工占用授信表结束");

        logger.info("代开银票  处理规则  占用同业额度处理开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt3 = cmis0209Mapper.insertTmpLmtAmt3(openDay);
        logger.info("代开银票  处理规则  占用同业额度处理结束,返回参数为:[{}]", insertTmpLmtAmt3);

        logger.info("更新额度占用关系处理开始,请求参数为:[{}]", openDay);
        int updateTmpLmtAmt3 = cmis0209Mapper.updateTmpLmtAmt3(openDay);
        logger.info("更新额度占用关系处理结束,返回参数为:[{}]", updateTmpLmtAmt3);


        logger.info("truncateTmpLmtAmt4清空加工占用授信表开始,请求参数为:[{}]", openDay);
        // cmis0209Mapper.truncateTmpLmtAmt4();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpLmtAmt4清空加工占用授信表结束");

        logger.info("银票贴现占用 承兑行白名单额度lmt_white_info开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt4 = cmis0209Mapper.insertTmpLmtAmt4(openDay);
        logger.info("银票贴现占用 承兑行白名单额度lmt_white_info处理结束,返回参数为:[{}]", insertTmpLmtAmt4);

        logger.info("更新额度占用关系处理开始,请求参数为:[{}]", openDay);
        int updateTmpLmtAmt4 = cmis0209Mapper.updateTmpLmtAmt4(openDay);
        logger.info("更新额度占用关系处理结束,返回参数为:[{}]", updateTmpLmtAmt4);

        logger.info("truncateTmpLmtAmt5清空加工占用授信表开始,请求参数为:[{}]", openDay);
        // cmis0209Mapper.truncateTmpLmtAmt5();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpLmtAmt5清空加工占用授信表结束");

        logger.info("商票贴现占用 商票保贴额度开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt5 = cmis0209Mapper.insertTmpLmtAmt5(openDay);
        logger.info("商票贴现占用 商票保贴额度处理结束,返回参数为:[{}]", insertTmpLmtAmt5);

        logger.info("更新额度占用关系处理开始,请求参数为:[{}]", openDay);
        int updateTmpLmtAmt5 = cmis0209Mapper.updateTmpLmtAmt5(openDay);
        logger.info("更新额度占用关系处理结束,返回参数为:[{}]", updateTmpLmtAmt5);
    }


    /**
     * 更新额度占用关系-银票质押
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0209UpdateLmtContRelYpzy(String openDay) {
        logger.info("truncateTmpLmtAmtYpzy清空加工占用授信表开始,请求参数为:[{}]", openDay);
        // cmis0209Mapper.truncateTmpLmtAmtYpzy();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpLmtAmtYpzy清空加工占用授信表结束");

        logger.info("银票质押占用承兑行白名单额度 占用余额更新开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtYpzy = cmis0209Mapper.insertTmpLmtAmtYpzy(openDay);
        logger.info("银票质押占用承兑行白名单额度 占用余额更新结束,返回参数为:[{}]", insertTmpLmtAmtYpzy);

        logger.info("清空授信其他更新表开始,请求参数为:[{}]", openDay);
        // cmis0209Mapper.truncateTmpLmtAmtOther();// cmis_batch.tmp_lmt_other
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_other");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空授信其他更新表结束");

        logger.info("插入授信其他更新表开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtOther = cmis0209Mapper.insertTmpLmtAmtOther(openDay);
        logger.info("插入授信其他更新表结束,返回参数为:[{}]", insertTmpLmtAmtOther);

        logger.info("更新额度占用关系处理开始,请求参数为:[{}]", openDay);
        int updateTmpLmtAmtYpzy = cmis0209Mapper.updateTmpLmtAmtYpzy(openDay);
        logger.info("更新额度占用关系处理结束,返回参数为:[{}]", updateTmpLmtAmtYpzy);
    }


    /**
     * 更新额度占用关系-资产池银票质押入池
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0209UpdateLmtContRelZccypzy(String openDay) {
        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        // cmis0209Mapper.truncateTmpLmtAmtZccypzy();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空加工占用授信表结束");

        logger.info("资产池银票质押入池占用承兑行白名单额度 占用余额更新开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtZccypzy = cmis0209Mapper.insertTmpLmtAmtZccypzy(openDay);
        logger.info("资产池银票质押入池占用承兑行白名单额度 占用余额更新处理结束,返回参数为:[{}]", insertTmpLmtAmtZccypzy);

        logger.info("清空授信其他更新表开始,请求参数为:[{}]", openDay);
        // cmis0209Mapper.truncateTmpLmtAmtOtherZccypzy();// cmis_batch.tmp_lmt_other
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_other");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空授信其他更新表结束");

        logger.info("插入授信其他更新表开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtOtherZccypzy = cmis0209Mapper.insertTmpLmtAmtOtherZccypzy(openDay);
        logger.info("插入授信其他更新表结束,返回参数为:[{}]", insertTmpLmtAmtOtherZccypzy);

        logger.info("更新额度占用关系处理开始,请求参数为:[{}]", openDay);
        int updateTmpLmtAmtZccypzy = cmis0209Mapper.updateTmpLmtAmtZccypzy(openDay);
        logger.info("更新额度占用关系处理结束,返回参数为:[{}]", updateTmpLmtAmtZccypzy);

        // 同步zjw代码 修改BUG转贴现未更新余额：20211023-00022 开始
        logger.info("清空临时表-更新转贴现占同业客户额度开始,请求参数为:[{}]", openDay);
        // cmis0209Mapper.truncateTmpLmtCop01();// cmis_batch.tmp_lmt_cop
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_cop");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空临时表-更新转贴现占同业客户额度结束");

        logger.info("插入临时表-更新转贴现占同业客户额度 开始,请求参数为:[{}]", openDay);
        int insertTmpLmtCop01 = cmis0209Mapper.insertTmpLmtCop01(openDay);
        logger.info("插入临时表-更新转贴现占同业客户额度 结束,返回参数为:[{}]", insertTmpLmtCop01);

        logger.info("更新转贴现占同业客户额度开始,请求参数为:[{}]", openDay);
        int updateTmpLmtCop01 = cmis0209Mapper.updateTmpLmtCop01(openDay);
        logger.info("更新转贴现占同业客户额度结束,返回参数为:[{}]", updateTmpLmtCop01);
        // 同步zjw代码 修改BUG转贴现未更新余额：20211023-00022 结束
    }
}
