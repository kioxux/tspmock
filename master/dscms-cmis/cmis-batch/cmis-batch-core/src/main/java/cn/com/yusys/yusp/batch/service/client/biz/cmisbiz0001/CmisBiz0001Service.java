package cn.com.yusys.yusp.batch.service.client.biz.cmisbiz0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisbiz0001.req.CmisBiz0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0001.resp.CmisBiz0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisBizClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：单一客户授信复审
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CmisBiz0001Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBiz0001Service.class);

    // 1）注入：封装的接口类:业务服务模块
    @Autowired
    private CmisBizClientService cmisBizClientService;

    /**
     * 业务逻辑处理方法：单一客户授信复审
     *
     * @param cmisBiz0001ReqDto
     * @return
     */
    @Transactional
    public CmisBiz0001RespDto cmisbiz0001(CmisBiz0001ReqDto cmisBiz0001ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0001.key, DscmsEnum.TRADE_CODE_CMISBIZ0001.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0001.key, DscmsEnum.TRADE_CODE_CMISBIZ0001.value, JSON.toJSONString(cmisBiz0001ReqDto));
        ResultDto<CmisBiz0001RespDto> cmisBiz0001ResultDto = cmisBizClientService.cmisBiz0001(cmisBiz0001ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0001.key, DscmsEnum.TRADE_CODE_CMISBIZ0001.value, JSON.toJSONString(cmisBiz0001ResultDto));

        String cmisBiz0001Code = Optional.ofNullable(cmisBiz0001ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisBiz0001Meesage = Optional.ofNullable(cmisBiz0001ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisBiz0001RespDto cmisbiz0001RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisBiz0001ResultDto.getCode())) {
            //  获取相关的值并解析
            cmisbiz0001RespDto = cmisBiz0001ResultDto.getData(); //此处只调用，不做异常抛出
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0001.key, DscmsEnum.TRADE_CODE_CMISBIZ0001.value);
        return cmisbiz0001RespDto;
    }
}
