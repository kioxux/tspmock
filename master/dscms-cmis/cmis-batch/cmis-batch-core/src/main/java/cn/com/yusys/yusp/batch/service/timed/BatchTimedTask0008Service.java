package cn.com.yusys.yusp.batch.service.timed;

import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.domain.sms.MessageTemp;
import cn.com.yusys.yusp.batch.domain.sms.SmsManageInfo;
import cn.com.yusys.yusp.batch.domain.sms.SmsManageInfoHis;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.batch.service.sms.SmsManageInfoHisService;
import cn.com.yusys.yusp.batch.service.sms.SmsManageInfoService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0602Service;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import cn.com.yusys.yusp.service.Dscms2DxptClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * 定时任务处理类:运行定时任务-消息提醒任务相关批量任务  </br>
 *
 * @author ys
 * @version 1.0
 * @since 2021年11月08日 下午11:56:54
 */
@Service
public class BatchTimedTask0008Service {
    private static final Logger logger = LoggerFactory.getLogger(BatchTimedTask0008Service.class);

    @Autowired
    private BatControlRunService batControlRunService;
    @Autowired
    private Cmis0602Service cmis0602Service;
    @Autowired
    private SmsManageInfoService smsManageInfoService;
    @Autowired
    private SmsManageInfoHisService smsManageInfoHisService;
    @Autowired
    Dscms2DxptClientService dscms2DxptClientService;

    /**
     * 运行定时任务-消息提醒任务相关批量任务
     *
     * @return
     */
    public int timedtask0008() {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0008.key, BatEnums.BATCH_TIMED_TASK0008.value);
        try {
            // 查询批量运行表，获取任务日期，任务日期等于营业日期
            QueryModel queryModel = new QueryModel();
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(queryModel));
            List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunList));
            if (CollectionUtils.isEmpty(batControlRunList)) {
                // 调度运行管理(BAT_CONTROL_RUN)未配置
                logger.error(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0008.key, BatEnums.BATCH_TIMED_TASK0008.value, EchEnum.ECH080008.value);
                throw BizException.error(null, EchEnum.ECH080008.key, EchEnum.ECH080008.value);
            }
//            // 校验运行状态
//            if (!Objects.equals(batControlRunList.get(0).getControlStatus(), BatEnums.CONTROL_STATUS_010.key)) {
//                // 调度运行管理(BAT_CONTROL_RUN)中状态不是运行中，请及时检查!
//                logger.error(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0008.key, BatEnums.BATCH_TIMED_TASK0008.value, EchEnum.ECH080016.value);
//                throw BizException.error(null, EchEnum.ECH080016.key, EchEnum.ECH080016.value);
//            }

            String openDay = batControlRunList.get(0).getOpenDay();//营业日期
            this.smsManageInfoC0001(openDay);
            this.smsManageInfoC0002(openDay);
            this.smsManageInfoC0003(openDay);
            this.smsManageInfoC0004(openDay);
            this.smsManageInfoC0005(openDay);
            this.smsManageInfoC0006(openDay);
            this.smsManageInfoC0007(openDay);
            this.smsManageInfoC0008(openDay);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0008.key, BatEnums.BATCH_TIMED_TASK0008.value, e.getMessage());
//            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0008.key, BatEnums.BATCH_TIMED_TASK0008.value, e.getMessage());
//            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0008.key, BatEnums.BATCH_TIMED_TASK0008.value);
        return 0;
    }

    public void smsManageInfoC0001(String openDay) {
        String messageType = "MSG_PL_C_0001"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人

        // 短信类型，由平台管理员提供,dx-短信 sgdx-寿光短信 dhdx-东海短信
        this.commonQuerySmsManageInfo(openDay, messageType, "dx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "sgdx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "dhdx", receivedUserType);
    }

    public void smsManageInfoC0002(String openDay) {
        String messageType = "MSG_PL_C_0002"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人

        // 短信类型，由平台管理员提供,dx-短信 sgdx-寿光短信 dhdx-东海短信
        this.commonQuerySmsManageInfo(openDay, messageType, "dx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "sgdx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "dhdx", receivedUserType);
    }

    public void smsManageInfoC0003(String openDay) {
        String messageType = "MSG_PL_C_0003"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人

        // 短信类型，由平台管理员提供,dx-短信 sgdx-寿光短信 dhdx-东海短信
        this.commonQuerySmsManageInfo(openDay, messageType, "dx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "sgdx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "dhdx", receivedUserType);
    }

    public void smsManageInfoC0004(String openDay) {
        String messageType = "MSG_PL_C_0004"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人

        // 短信类型，由平台管理员提供,dx-短信 sgdx-寿光短信 dhdx-东海短信
        this.commonQuerySmsManageInfo(openDay, messageType, "dx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "sgdx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "dhdx", receivedUserType);
    }

    public void smsManageInfoC0005(String openDay) {
        String messageType = "MSG_PL_C_0005"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人

        // 短信类型，由平台管理员提供,dx-短信 sgdx-寿光短信 dhdx-东海短信
        this.commonQuerySmsManageInfo(openDay, messageType, "dx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "sgdx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "dhdx", receivedUserType);
    }

    public void smsManageInfoC0006(String openDay) {
        String messageType = "MSG_PL_C_0006"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人

        // 短信类型，由平台管理员提供,dx-短信 sgdx-寿光短信 dhdx-东海短信
        this.commonQuerySmsManageInfo(openDay, messageType, "dx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "sgdx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "dhdx", receivedUserType);
    }

    public void smsManageInfoC0007(String openDay) {
        String messageType = "MSG_PL_C_0007"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人

        // 短信类型，由平台管理员提供,dx-短信 sgdx-寿光短信 dhdx-东海短信
        this.commonQuerySmsManageInfo(openDay, messageType, "dx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "sgdx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "dhdx", receivedUserType);
    }

    public void smsManageInfoC0008(String openDay) {
        String messageType = "MSG_PL_C_0008"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人

        // 短信类型，由平台管理员提供,dx-短信 sgdx-寿光短信 dhdx-东海短信
        this.commonQuerySmsManageInfo(openDay, messageType, "dx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "sgdx", receivedUserType);
        this.commonQuerySmsManageInfo(openDay, messageType, "dhdx", receivedUserType);
    }

    /**
     * 短信提醒客户公共方法-短信
     *
     * @param openDay
     * @param messageType
     * @param smsType dx-短信 sgdx-寿光短信 dhdx-东海短信
     * @param receivedUserType
     */
    public void commonQuerySmsManageInfo(String openDay, String messageType, String smsType, String receivedUserType) {
        QueryModel model = new QueryModel();
        model.addCondition("subCode", messageType);
        model.addCondition("smsType", smsType); // 短信类型，由平台管理员提供,dx-短信 sgdx-寿光短信 dhdx-东海短信
        logger.info("查询短信通知表输入表开始,请求参数为:[{}]、模板类型[{}]", openDay, messageType);
        List<SmsManageInfo> smsManageInfoList = smsManageInfoService.selectAll(model);
        logger.info("查询短信通知表输入表结束,响应参数为:[{}]", smsManageInfoList.size());
        // 没有要处理的短信直接返回
        if (CollectionUtils.isEmpty(smsManageInfoList)) {
            return;
        }
        logger.info("查询短信模板表开始,请求参数为模板类型:[{}]", messageType);
        MessageTemp messageTemp = smsManageInfoService.selectMessageTempByPrimaryKey(messageType, "mobile");
        logger.info("查询短信模板表结束,响应参数为:[{}]", messageTemp.toString());
        List<SmsManageInfo> sendSmsManageInfoList = new ArrayList<>();

        if (smsManageInfoList.size() <= 300) {
            this.commonmsManSageInfo(openDay, messageTemp, smsManageInfoList);
        } else {
            int countNum = 0;
            for (SmsManageInfo smsManageInfo : smsManageInfoList) {
                countNum++;
                sendSmsManageInfoList.add(smsManageInfo);
                if (countNum > 299) {
                    this.commonmsManSageInfo(openDay, messageTemp, sendSmsManageInfoList);
                    sendSmsManageInfoList.clear();
                    countNum = 0;
                    break;
                }
            }
        }
    }

    /**
     * 短信提醒客户公共方法
     *
     * @param openDay
     * @param messageTemp
     * @param smsManageInfoList
     */
    @Transactional
    public void commonmsManSageInfo(String openDay, MessageTemp messageTemp, List<SmsManageInfo> smsManageInfoList) {
        DecimalFormat decimalFormat = new DecimalFormat("###.00");
        ArrayList<SenddxReqList> senddxReqLists = new ArrayList<>();
        List<SmsManageInfo> sendSmsManageInfoList = new ArrayList<>();
        for (SmsManageInfo smsManageInfo : smsManageInfoList) {
            String smsId = smsManageInfo.getSmsId(); // 主键
            String cusName = smsManageInfo.getCusName();
            String prdName = smsManageInfo.getPrdName();
            String repayDateYy = smsManageInfo.getRepayDateYy();
            String repayDateMm = smsManageInfo.getRepayDateMm();
            String repayDateDd = smsManageInfo.getRepayDateDd();
            BigDecimal repayAmt = Optional.ofNullable(smsManageInfo.getRepayAmt()).orElse(new BigDecimal(0));
            String currrentYear = smsManageInfo.getCurrrentYear();
            String currrentMonth = smsManageInfo.getCurrrentMonth();
            String currentDay = smsManageInfo.getCurrentDay();
            BigDecimal overToatalAmt = Optional.ofNullable(smsManageInfo.getOverTotalAmt()).orElse(new BigDecimal(0));
            String smsContent = smsManageInfo.getSmsContent(); // 借据号
            String phone = smsManageInfo.getPhone(); // 接收短信的手机号
            if ("".equals(phone) || null == phone) {
                logger.info("手机号码为空不处理直接跳过，借据号:[{}]", smsContent);
                this.updateSmsManageInfo(openDay, smsManageInfo);
                continue;
            }
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", cusName);
            paramMap.put("prdName", prdName);
            paramMap.put("smsContent", "****" + smsContent.substring(smsContent.length() - 4)); // 借据号
            paramMap.put("repayDateYy", repayDateYy);
            paramMap.put("repayDateMm", repayDateMm);
            paramMap.put("repayDateDd", repayDateDd);
            paramMap.put("repayAmt", decimalFormat.format(repayAmt)); // 不支持BigDecimal类型
            paramMap.put("currrentYear", currrentYear);
            paramMap.put("currrentMonth", currrentMonth);
            paramMap.put("currentDay", currentDay);
            paramMap.put("overToatalAmt", decimalFormat.format(overToatalAmt)); // 不支持BigDecimal类型

            String template = this.parseParam(messageTemp.getTemplateContent(), paramMap);
            SenddxReqList senddxReqList = new SenddxReqList();
            senddxReqList.setMobile(phone);
            senddxReqList.setSmstxt(template);
            senddxReqLists.add(senddxReqList);
            sendSmsManageInfoList.add(smsManageInfo);
        }

        if (CollectionUtils.nonEmpty(sendSmsManageInfoList)) {
            SenddxReqDto senddxReqDto = new SenddxReqDto();
            try {
                logger.info("批量发送手机短信消息前一个批次中短信类型[dx:短信 sgdx:寿光短信 dhdx:东海短信]为:[{}]", sendSmsManageInfoList.get(0).getSmsType());
                senddxReqDto.setInfopt(sendSmsManageInfoList.get(0).getSmsType());
                senddxReqDto.setSenddxReqList(senddxReqLists);
                logger.info("批量发送手机短信消息前一个批次的数量为:[{}]", sendSmsManageInfoList.size());
                if (this.senddx(senddxReqDto)) {
                    for (SmsManageInfo smsManageInfo : sendSmsManageInfoList) {
                        smsManageInfo.setSmsSendState("1"); // 1-已发送
                        this.updateSmsManageInfo(openDay, smsManageInfo);
                    }
                } else {
                    for (SmsManageInfo smsManageInfo : sendSmsManageInfoList) {
                        smsManageInfo.setSmsSendState("2"); // 2-失败
                        this.updateSmsManageInfo(openDay, smsManageInfo);
                    }
                }
            } catch (Exception e) {
                logger.error("发布手机短信消息前组装请求对象失败,异常信息为:[{}] ", e.getMessage());
            }
        }
    }


    /**
     * 更新短信通知表
     *
     * @param openDay
     * @param smsManageInfo
     */
    public void updateSmsManageInfo(String openDay, SmsManageInfo smsManageInfo) {
        SmsManageInfoHis smsManageInfoHis = new SmsManageInfoHis();
        BeanUtils.copyProperties(smsManageInfo, smsManageInfoHis);
        smsManageInfoHis.setTaskDate(openDay); // 任务日期
        smsManageInfoHis.setSendTime(DateUtils.getCurrDateTimeStr());// 短信发送时间
        logger.info("将短信通知表输入表中值备份到短信通知历史表中开始,请求参数为:[{}]", JSON.toJSONString(smsManageInfoHis));
        int smsHisInsertResult = smsManageInfoHisService.insertSelective(smsManageInfoHis);
        logger.info("将短信通知表输入表中值备份到短信通知历史表中结束,响应参数为:[{}]", JSON.toJSONString(smsHisInsertResult));

        logger.info("将短信通知表输入表中值删除开始,请求参数为:[{}]", JSON.toJSONString(smsManageInfo.getSmsId()));
        int smsDeleteResult = smsManageInfoService.deleteByPrimaryKey(smsManageInfo.getSmsId());
        logger.info("将短信通知表输入表中值删除结束,响应参数为:[{}]", JSON.toJSONString(smsDeleteResult));
    }

    /**
     * 发布消息
     *
     * @param senddxReqDto
     */
    public boolean senddx(SenddxReqDto senddxReqDto) {
        boolean resultFlag = false;
        // 调用BSP服务接口，发送消息至短信平台
        try {
            logger.info("发布手机短信消息,短信/微信发送批量接口请求对象为:[{}]", JSON.toJSONString(senddxReqDto, SerializerFeature.WriteMapNullValue));
            ResultDto<SenddxRespDto> result = dscms2DxptClientService.senddx(senddxReqDto);
            logger.info("发布手机短信消息,短信/微信发送批量接口响应对象为:[{}]", JSON.toJSONString(result, SerializerFeature.WriteMapNullValue));

            if ("0".equals(result.getCode())) {
                logger.info("发送短信消息: 短信消息发送成功");
                resultFlag = true;
            } else {
                logger.error("发送短信消息: 短信消息发送失败, {}", result.getMessage());
            }
        } catch (Exception e) {
            logger.error("发送短信消息: 短信消息发送失败, ", e);
        }
        return resultFlag;
    }

    /**
     * 解析消息模板
     *
     * @param template 模板字符串
     * @param params   模板参数
     * @return 解析后的模板内容
     */
    public static String parseParam(String template, Map<String, String> params) {
        for (String key : params.keySet()) {
            String keyTemplate = "${" + key + "}";
            if (template.contains(keyTemplate)) {
                template = template.replace(keyTemplate, params.get(key));
            }
        }
        return template;
    }

}
