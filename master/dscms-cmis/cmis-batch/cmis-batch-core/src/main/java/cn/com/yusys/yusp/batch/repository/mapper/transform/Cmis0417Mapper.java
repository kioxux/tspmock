package cn.com.yusys.yusp.batch.repository.mapper.transform;

import java.util.Map;


/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0417</br>
 * 任务名称：加工任务-贷后管理-定期检查子表数据生成 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0417Mapper {


    /**
     * 删除临时表
     *
     * @param map
     * @return
     */
    int deletetmpupdatedata4manage(Map map);
    /**
     * 插入临时表 获取任务中最新的责任人
     *
     * @param map
     * @return
     */
    int insertTmpUpdateData4(Map map);

    /**
     * 更新任务表   任务中最新的责任人
     *
     * @param map
     * @return
     */
    int updatepsptasklistmanagerid(Map map);


    /**
     * 插入贷后检查结果表
     *
     * @param map
     * @return
     */
    int insertPspCheckRst(Map map);

    /**
     * 插入定期检查借据信息
     *
     * @param map
     * @return
     */
    int insertPspDebitInfo01(Map map);

    /**
     * 插入定期检查借据信息
     *
     * @param map
     * @return
     */
    int insertPspDebitInfo02(Map map);

    /**
     * 委托贷款贷后检查借据信息
     *
     * @param map
     * @return
     */
    int insertpspDebitInfowtdk(Map map);
    /**
     * 贷后检查银承借据
     *
     * @param map
     * @return
     */
    int insertpspDebitInfoyc(Map map);
    /**
     * 贷后检查保函借据
     *
     * @param map
     * @return
     */
    int insertpspDebitInfobh(Map map);

    /**
     *贷后检查信用证借据
     *
     * @param map
     * @return
     */
    int insertpspDebitInfoxyz(Map map);

    /**
     *贷后检查贴现借据
     *
     * @param map
     * @return
     */
    int insertpspDebitInfotx(Map map);



    /**
     * 插入定期检查保证人检查
     *
     * @param map
     * @return
     */
    int insertPspGuarntrList(Map map);

    /**
     * 清理客户最新财报时间
     *
     * @param map
     * @return
     */
    int truncatePspFin(Map map);

    int truncatePspFin2(Map map);

    /**
     * 插入客户最新财报时间
     *
     * @param map
     * @return
     */
    int insertPspFin(Map map);

    /**
     * 插入定期检查财务状况检查
     *
     * @param map
     * @return
     */
    int insertPspFinSituCheck01(Map map);

    /**
     * 插入定期检查财务状况检查
     *
     * @param map
     * @return
     */
    int insertPspFinSituCheck02(Map map);

    /**
     * 插入定期检查财务状况检查
     *
     * @param map
     * @return
     */
    int insertPspFinSituCheck03(Map map);

    /**
     * 插入定期检查财务状况检查
     *
     * @param map
     * @return
     */
    int insertPspFinSituCheck04(Map map);

    /**
     * 插入定期检查财务状况检查
     *
     * @param map
     * @return
     */
    int updatePspFinSituCheck(Map map);

    /**
     * 插入定期检查贷后管理建议落实情况
     *
     * @param map
     * @return
     */
    int insertPspImplementOpt(Map map);

    /**
     * 插入定期检查抵质押物检查
     *
     * @param map
     * @return
     */
    int insertPspPldimnList(Map map);

    /**
     * 插入定期检查经营状况检查（通用）
     *
     * @param map
     * @return
     */
    int insertPspOperStatusCheck(Map map);

    /**
     * 插入定期检查融资情况分析
     *
     * @param map
     * @return
     */
    int insertPspCusFinAnaly(Map map);

    /**
     * 定期检查预警信号表
     *
     * @param map
     * @return
     */
    int insertPspWarningInfoList(Map map);

    /**
     * 插入定期检查正常类本行融资情况
     *
     * @param map
     * @return
     */
    int insertPspBankFinSitu(Map map);

    /**
     * 插入定期检查正常类对外担保分析
     *
     * @param map
     * @return
     */
    int insertPspGuarCheck(Map map);

    /**
     * 插入对公定期检查不良类关联企业表
     *
     * @param map
     * @return
     */
    int insertPspRelCorp(Map map);

    /**
     * 删除正常分类的 数据
     *
     * @param map
     * @return
     */
//    int deletepspRelCorp(Map map);

    /**
     * 插入对公定期检查经营性物业贷款检查
     *
     * @param map
     * @return
     */
    int insertPspOperStatusCheckProperty(Map map);

    /**
     * 插入对公定期检查经营状况检查（房地产开发贷）
     *
     * @param map
     * @return
     */
    int insertPspOperStatusCheckRealpro(Map map);

    /**
     * 对公定期检查经营状况检查（建筑业）
     *
     * @param map
     * @return
     */
    int insertPspOperStatusCheckArch(Map map);

    /**
     * 插入对公定期检查经营状况检查（制造业）
     *
     * @param map
     * @return
     */
    int insertPspOperStatusCheckManufacture(Map map);

    /**
     * 插入对公客户基本信息分析表
     *
     * @param map
     * @return
     */
    int insertPspCusBaseCaseDG(Map map);

    /**
     * 插入个人客户基本信息分析表
     *
     * @param map
     * @return
     */
    int insertPspCusBaseCaseGR(Map map);

    /**
     * 更新对公、个人客户基本信息分析表[房地产开发贷检查 ,如果存在则更新 IS_REALPRO 为是]
     *
     * @param map
     * @return
     */
    int updatePspCusBaseCase01(Map map);

    /**
     * 更新对公、个人客户基本信息分析表[经营性物业贷检查,如果存在则更新 IS_PROPERTY 为是]
     *
     * @param map
     * @return
     */
    int updatePspCusBaseCase02(Map map);

    /**
     * 更新对公、个人客户基本信息分析表[IS_FIXED固定资产贷款、项目贷款检查,如果存在则更新 IS_PROPERTY 为是]
     *
     * @param map
     * @return
     */
    int updatePspCusBaseCase03(Map map);

    /**
     * 清空客户的分类状态加工表
     *
     * @param map
     * @return
     */
    int updatePspCusBaseCase04A(Map map);

    /**
     * 查询待删除[客户的分类状态加工表|psp_cus_base_case_risk]数据总次数
     *
     * @param map
     * @return
     */
    int batPcbcrCounts(Map map);

    /**
     * 删除[客户的分类状态加工表|psp_cus_base_case_risk]数据
     *
     * @param map
     * @return
     */
    int deleteBatPcbcr(Map map);

    /**
     * 插入客户的分类状态加工表
     *
     * @param map
     * @return
     */
    int updatePspCusBaseCase04B(Map map);

    /**
     * 清理[更新检查中证件号，手机号临时表] 日终调用
     *
     * @param map
     * @return
     */
    int truncatePspCusBase(Map map);

    /**
     * 清理[更新检查中证件号，手机号临时表]  日间接口调用
     *
     * @param map
     * @return
     */
    int truncatePspCusBase2(Map map);

    int insertPspCusBase(Map map);

    int updatePCBCertCode(Map map);

    /**
     * 更新对公、个人客户基本信息分析表[更新贷后客户的分类状态]
     *
     * @param map
     * @return
     */
    int updatePspCusBaseCase04(Map map);

    /**
     * 插入投后定期检查存量授信情况
     *
     * @param map
     * @return
     */
    int insertPspLmtSitu(Map map);

    /**
     * 插入投后定期检查债券池用信业务情况
     *
     * @param map
     * @return
     */
    int insertPspBondSitu(Map map);

    /**
     * 插入资金业务客户基本信息分析表
     *
     * @param map
     * @return
     */
    int insertPspCapCusBaseCase01(Map map);

    /**
     * 自动检查自动完成
     *
     * @param map
     * @return
     */
    int updatepspTaskListApproveStatus(Map map);

    /**
     * 修改贷后检查任务状态
     *
     * @param map
     * @return
     */
    int updatepspTaskListTaskStatus(Map map);

    /**
     * 更新报告类型为不良类
     *
     * @param map
     * @return
     */
    int updateTaskTaskListRptType3(Map map);

    /**
     * 更新报告类型为瑕疵类
     *
     * @param map
     * @return
     */
    int updateTaskTaskListRptType2(Map map);
    /**
     * 手工发起的检查需插入tmp_psp_task_list之前删除 当前客户
     *
     * @param map
     * @return
     */
    int deleteTmpPspTaskList(Map map);

    /**
     * 手工发起的检查需插入tmp_psp_debit_info之前删除 当前客户
     *
     * @param map
     * @return
     */
    int deletetmpPpspDebitInfo(Map map);




    /**
     * 手工发起的检查需插入tmp_psp_task_list，避免子表查数据查不到
     *
     * @param map
     * @return
     */
    int insertTmpPspTaskList(Map map);

    /**
     * 日间手工发起，将借据信息插入原表
     *
     * @param map
     * @return
     */
    int insertPspDebitInfoFromTmp(Map map);
}
