/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.bat;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTransferCfg
 * @类描述: bat_transfer_cfg数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-13 15:40:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_transfer_cfg")
public class BatTransferCfg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 任务编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "TASK_NO")
    private String taskNo;
    /**
     * 任务名称
     **/
    @Column(name = "TASK_NAME", unique = false, nullable = true, length = 100)
    private String taskName;
    /**
     * FTP服务器IP任务配置表中“FILE_EXCHANGE_TYPE文件交互方式”为“01FTP客户端”时必填；否则为空。
     **/
    @Column(name = "FTP_IP", unique = false, nullable = true, length = 20)
    private String ftpIp;

    /**
     * FTP服务器端口任务配置表中“FILE_EXCHANGE_TYPE文件交互方式”为“01FTP客户端”时必填；否则为空。
     **/
    @Column(name = "FTP_PORT", unique = false, nullable = true, length = 6)
    private String ftpPort;

    /**
     * FTP服务器用户名任务配置表中“FILE_EXCHANGE_TYPE文件交互方式”为“01FTP客户端”时必填；否则为空。
     **/
    @Column(name = "FTP_USERNAME", unique = false, nullable = true, length = 50)
    private String ftpUsername;

    /**
     * FTP服务器密码任务配置表中“FILE_EXCHANGE_TYPE文件交互方式”为“01FTP客户端”时必填；否则为空。
     **/
    @Column(name = "FTP_USERPSW", unique = false, nullable = true, length = 40)
    private String ftpUserpsw;

    /**
     * TFTP服务器编号任务配置表中“FILE_EXCHANGE_TYPE文件交互方式”为“02TFTP客户端”、“11服务端”时必填；否则为空。
     **/
    @Column(name = "TFTP_HOSTNO", unique = false, nullable = true, length = 2)
    private String tftpHostno;

    /**
     * TFTP路由标识任务配置表中“FILE_EXCHANGE_TYPE文件交互方式”为“02TFTP客户端”、“11服务端”时必填；否则为空。
     **/
    @Column(name = "TFTP_ROUTENO", unique = false, nullable = true, length = 10)
    private String tftpRouteno;

    /**
     * 数据库类型
     **/
    @Column(name = "DB_TYPE", unique = false, nullable = true, length = 20)
    private String dbType;

    /**
     * 数据库IP
     **/
    @Column(name = "DB_IP", unique = false, nullable = true, length = 20)
    private String dbIp;

    /**
     * 数据库端口
     **/
    @Column(name = "DB_PORT", unique = false, nullable = true, length = 6)
    private String dbPort;

    /**
     * 数据库实例
     **/
    @Column(name = "DB_INSTANCE", unique = false, nullable = true, length = 10)
    private String dbInstance;

    /**
     * 数据库用户名
     **/
    @Column(name = "DB_USERNAME", unique = false, nullable = true, length = 50)
    private String dbUsername;

    /**
     * 数据库密码
     **/
    @Column(name = "DB_USERPSW", unique = false, nullable = true, length = 40)
    private String dbUserpsw;

    /**
     * 工作目录必填，全路径，需要用文件日期替换路径中的日期
     **/
    @Column(name = "WORK_DIR", unique = false, nullable = true, length = 100)
    private String workDir;

    /**
     * 本地目录必填，全路径，需要用文件日期替换路径中的日期
     **/
    @Column(name = "LOCAL_DIR", unique = false, nullable = true, length = 100)
    private String localDir;

    /**
     * 远程目录任务配置表中的文件交互方式为“01FTP客户端”、“02TFTP客户端”时必填，否则为空。
     **/
    @Column(name = "REMOTE_DIR", unique = false, nullable = true, length = 100)
    private String remoteDir;
    /**
     * 远程信号目录
     **/
    @Column(name = "REMOTE_SIGNAL_DIR", unique = false, nullable = true, length = 100)
    private String remoteSignalDir;
    /**
     * 备注
     **/
    @Column(name = "REMARKS", unique = false, nullable = true, length = 250)
    private String remarks;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记时间
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 操作类型
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date updateTime;


    /**
     * @param taskNo
     */
    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    /**
     * @return taskNo
     */
    public String getTaskNo() {
        return this.taskNo;
    }

    /**
     * @return ftpIp
     */
    public String getFtpIp() {
        return this.ftpIp;
    }

    /**
     * @param ftpIp
     */
    public void setFtpIp(String ftpIp) {
        this.ftpIp = ftpIp;
    }

    /**
     * @return ftpPort
     */
    public String getFtpPort() {
        return this.ftpPort;
    }

    /**
     * @param ftpPort
     */
    public void setFtpPort(String ftpPort) {
        this.ftpPort = ftpPort;
    }

    /**
     * @return ftpUsername
     */
    public String getFtpUsername() {
        return this.ftpUsername;
    }

    /**
     * @param ftpUsername
     */
    public void setFtpUsername(String ftpUsername) {
        this.ftpUsername = ftpUsername;
    }

    /**
     * @return ftpUserpsw
     */
    public String getFtpUserpsw() {
        return this.ftpUserpsw;
    }

    /**
     * @param ftpUserpsw
     */
    public void setFtpUserpsw(String ftpUserpsw) {
        this.ftpUserpsw = ftpUserpsw;
    }

    /**
     * @return tftpHostno
     */
    public String getTftpHostno() {
        return this.tftpHostno;
    }

    /**
     * @param tftpHostno
     */
    public void setTftpHostno(String tftpHostno) {
        this.tftpHostno = tftpHostno;
    }

    /**
     * @return tftpRouteno
     */
    public String getTftpRouteno() {
        return this.tftpRouteno;
    }

    /**
     * @param tftpRouteno
     */
    public void setTftpRouteno(String tftpRouteno) {
        this.tftpRouteno = tftpRouteno;
    }

    /**
     * @return dbType
     */
    public String getDbType() {
        return this.dbType;
    }

    /**
     * @param dbType
     */
    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    /**
     * @return dbIp
     */
    public String getDbIp() {
        return this.dbIp;
    }

    /**
     * @param dbIp
     */
    public void setDbIp(String dbIp) {
        this.dbIp = dbIp;
    }

    /**
     * @return dbPort
     */
    public String getDbPort() {
        return this.dbPort;
    }

    /**
     * @param dbPort
     */
    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }

    /**
     * @return dbInstance
     */
    public String getDbInstance() {
        return this.dbInstance;
    }

    /**
     * @param dbInstance
     */
    public void setDbInstance(String dbInstance) {
        this.dbInstance = dbInstance;
    }

    /**
     * @return dbUsername
     */
    public String getDbUsername() {
        return this.dbUsername;
    }

    /**
     * @param dbUsername
     */
    public void setDbUsername(String dbUsername) {
        this.dbUsername = dbUsername;
    }

    /**
     * @return dbUserpsw
     */
    public String getDbUserpsw() {
        return this.dbUserpsw;
    }

    /**
     * @param dbUserpsw
     */
    public void setDbUserpsw(String dbUserpsw) {
        this.dbUserpsw = dbUserpsw;
    }

    /**
     * @param workDir
     */
    public void setWorkDir(String workDir) {
        this.workDir = workDir;
    }

    /**
     * @return workDir
     */
    public String getWorkDir() {
        return this.workDir;
    }

    /**
     * @param localDir
     */
    public void setLocalDir(String localDir) {
        this.localDir = localDir;
    }

    /**
     * @return localDir
     */
    public String getLocalDir() {
        return this.localDir;
    }

    /**
     * @param remoteDir
     */
    public void setRemoteDir(String remoteDir) {
        this.remoteDir = remoteDir;
    }

    /**
     * @return remoteDir
     */
    public String getRemoteDir() {
        return this.remoteDir;
    }

    public String getRemoteSignalDir() {
        return remoteSignalDir;
    }

    public void setRemoteSignalDir(String remoteSignalDir) {
        this.remoteSignalDir = remoteSignalDir;
    }

    /**
     * @param remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return remarks
     */
    public String getRemarks() {
        return this.remarks;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @return taskName
     */
    public String getTaskName() {
        return this.taskName;
    }

    /**
     * @param taskName
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }


}