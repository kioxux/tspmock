/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.sbs;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSSbsAvailableZjg
 * @类描述: bat_s_sbs_available_zjg数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 20:10:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_sbs_available_zjg")
public class BatSSbsAvailableZjg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 记录ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 授信主体ECIF_ID **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 授信分项额度ID **/
	@Column(name = "APPR_SUB_SERNO", unique = false, nullable = true, length = 30)
	private String apprSubSerno;
	
	/** 资产编号(如有) **/
	@Column(name = "ASSET_NO", unique = false, nullable = true, length = 30)
	private String assetNo;
	
	/** 授信分项额度总额度 **/
	@Column(name = "AVL_AMT", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal avlAmt;
	
	/** 分项额度当前占额(已用金额) **/
	@Column(name = "OUTSTAND_AMT", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal outstandAmt;
	
	/** 日终日 **/
	@Column(name = "BAT_DATE", unique = false, nullable = true, length = 20)
	private String batDate;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param apprSubSerno
	 */
	public void setApprSubSerno(String apprSubSerno) {
		this.apprSubSerno = apprSubSerno;
	}
	
    /**
     * @return apprSubSerno
     */
	public String getApprSubSerno() {
		return this.apprSubSerno;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	
    /**
     * @return assetNo
     */
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param avlAmt
	 */
	public void setAvlAmt(java.math.BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}
	
    /**
     * @return avlAmt
     */
	public java.math.BigDecimal getAvlAmt() {
		return this.avlAmt;
	}
	
	/**
	 * @param outstandAmt
	 */
	public void setOutstandAmt(java.math.BigDecimal outstandAmt) {
		this.outstandAmt = outstandAmt;
	}
	
    /**
     * @return outstandAmt
     */
	public java.math.BigDecimal getOutstandAmt() {
		return this.outstandAmt;
	}
	
	/**
	 * @param batDate
	 */
	public void setBatDate(String batDate) {
		this.batDate = batDate;
	}
	
    /**
     * @return batDate
     */
	public String getBatDate() {
		return this.batDate;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}