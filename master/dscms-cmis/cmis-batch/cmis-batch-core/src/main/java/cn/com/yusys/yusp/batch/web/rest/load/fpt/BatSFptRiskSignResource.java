/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.fpt;

import cn.com.yusys.yusp.batch.domain.load.fpt.BatSFptRiskSign;
import cn.com.yusys.yusp.batch.service.load.fpt.BatSFptRiskSignService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSFptRiskSignResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-12 10:11:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsfptrisksign")
public class BatSFptRiskSignResource {
    @Autowired
    private BatSFptRiskSignService batSFptRiskSignService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSFptRiskSign>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSFptRiskSign> list = batSFptRiskSignService.selectAll(queryModel);
        return new ResultDto<List<BatSFptRiskSign>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSFptRiskSign>> index(QueryModel queryModel) {
        List<BatSFptRiskSign> list = batSFptRiskSignService.selectByModel(queryModel);
        return new ResultDto<List<BatSFptRiskSign>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{riskId}")
    protected ResultDto<BatSFptRiskSign> show(@PathVariable("riskId") String riskId) {
        BatSFptRiskSign batSFptRiskSign = batSFptRiskSignService.selectByPrimaryKey(riskId);
        return new ResultDto<BatSFptRiskSign>(batSFptRiskSign);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSFptRiskSign> create(@RequestBody BatSFptRiskSign batSFptRiskSign) throws URISyntaxException {
        batSFptRiskSignService.insert(batSFptRiskSign);
        return new ResultDto<BatSFptRiskSign>(batSFptRiskSign);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSFptRiskSign batSFptRiskSign) throws URISyntaxException {
        int result = batSFptRiskSignService.update(batSFptRiskSign);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{riskId}")
    protected ResultDto<Integer> delete(@PathVariable("riskId") String riskId) {
        int result = batSFptRiskSignService.deleteByPrimaryKey(riskId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSFptRiskSignService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
