package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0201</br>
 * 任务名称：加工任务-额度处理-额度关系占用</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0201Mapper {
    /**
     * 清空额度库中集团客户与成员关系表
     *
     * @param openDay
     * @return
     */
    int deleteLmtCusGrpMemberRel(@Param("openDay") String openDay);

    /**
     * 插入额度库中集团客户与成员关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtCusGrpMemberRel(@Param("openDay") String openDay);


    /**
     * 清空额度库中集团客户信息表
     *
     * @param openDay
     * @return
     */
    int deleteLmtCusGrp(@Param("openDay") String openDay);


    /**
     * 插入额度库中集团客户信息表
     *
     * @param openDay
     * @return
     */
    int insertLmtCusGrp(@Param("openDay") String openDay);

    /**
     * 清空合同与申请加工表
     *
     * @param openDay
     * @return
     */
    int deleteTmpIqpCtr(@Param("openDay") String openDay);

    /**
     * 插入合同与申请加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpIqpCtr(@Param("openDay") String openDay);

    /**
     * 更新合同与申请加工表
     *
     * @param openDay
     * @return
     */
    int updateTmpIqpCtr(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[最高额授信协议]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelA1(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[1.2最高额授信协议 贷款 占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelA2(@Param("openDay") String openDay);

    /**
     * insertTdcarZgexyYcdk-最高额授信协议下银承产生垫款
     *
     * @param openDay
     * @return
     */
    int insertTdcarZgexyYcdk(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[1.3最高额授信协议 银票占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelA3(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[1.4最高额授信协议 保函占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelA4(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[1.5最高额授信协议 贴现 占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelA5(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[1.6 最高额授信协议 信用证 占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelA6(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[贷款]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelB(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[银承]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelC(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[保函]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelD(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[贴现]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelE(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[信用证]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelF(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[贷款占用 合作方]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelG(@Param("openDay") String openDay);

    /**
     * 3.2A对公使用担保合同占用关系占用专业担保公司额度
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelA(@Param("openDay") String openDay);

    /**
     * 3.2A对公使用担保合同占用关系占用专业担保公司额度
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelA2(@Param("openDay") String openDay);

    /**
     * 3.2B 合作方在合同签订占额 [集群贷以及对公业务条线贷款和个人经营性贷款]
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelB(@Param("openDay") String openDay);

    /**
     * 3.3A  银承占用合作方(专业担保公司）cop_guar_rel
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelC(@Param("openDay") String openDay);

    /**
     * 3.3B 银承占用合作方(非专业担保公司）lmt_cont_rel
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelD(@Param("openDay") String openDay);

    /**
     * 3.4保函占用合作方(专业担保公司）cop_guar_rel
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelE(@Param("openDay") String openDay);

    /**
     * 3.5信用证占用合作方(专业担保公司）cop_guar_rel
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelF(@Param("openDay") String openDay);

    /**
     * 代开保函占对手行同业额度
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelG(@Param("openDay") String openDay);

    /**
     * 4.2 代开信用证占对手行同业额度
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelH(@Param("openDay") String openDay);

    /**
     * 4.3 福费廷占承兑行同业额度
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelI(@Param("openDay") String openDay);

    /**
     * 4.4 代开银票占 中国银行同业额度 （ 占客户额度在2.2）  8400494253 中国银行
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelJ(@Param("openDay") String openDay);

    /**
     * 4.5 银票贴现占承兑行白名单额度（占用客户额度在2.4）
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelK(@Param("openDay") String openDay);

    /**
     * 4.6 商票贴现 占商票保贴额度（占用客户额度在2.4）
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelL(@Param("openDay") String openDay);

    /**
     * 4.7  转贴现
     *
     * @param openDay
     * @return
     */
    int insertCopGuarRelM(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[贷款合同占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelCtrA(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[银承合同占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelCtrC(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[保函合同占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelCtrD(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[贴现合同占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelCtrE(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[信用证合同占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelCtrF(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[资产池协议占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelCtrG(@Param("openDay") String openDay);

    /**
     * 插入额度占用关系[委托贷款合同占用子授信分项]
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelCtrH(@Param("openDay") String openDay);

    /**
     * 银票质押入池占承兑行白名单额度
     *
     * @param openDay
     * @return
     */
    int insertTmpDealLmtContRelA(@Param("openDay") String openDay);

    /**
     * 银票质押入池占承兑行白名单额度
     *
     * @param openDay
     * @return
     */
    int insertTmpDealLmtContRelB(@Param("openDay") String openDay);

    /**
     * 福费廷占同业客户额度
     *
     * @param openDay
     * @return
     */
    int insertIntoLmtContRelFFT(@Param("openDay") String openDay);

    /**
     * 代开保函占同业客户额度（迁移）
     *
     * @param openDay
     * @return
     */
    int insertIntoLmtContRelDKBH(@Param("openDay") String openDay);
}
