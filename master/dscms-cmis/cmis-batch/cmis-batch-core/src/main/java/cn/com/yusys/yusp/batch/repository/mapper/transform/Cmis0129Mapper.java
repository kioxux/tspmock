package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0129</br>
 * 任务名称：加工任务-业务处理-零售智能风控苏宁联合贷 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0129Mapper {
    /**
     * 插入贷款合同表
     *
     * @param openDay
     * @return
     */
    int insertCtrLoanCont(@Param("openDay") String openDay);


    /**
     * 插入贷款台账表
     *
     * @param openDay
     * @return
     */
    int insertAccLoan(@Param("openDay") String openDay);


    /**
     * 更新贷款台账表
     *
     * @param openDay
     * @return
     */
    int updateAccLoan03(@Param("openDay") String openDay);

  

    /**
     * 插入网金五级分类数据
     *
     * @param openDay
     * @return
     */
    int insertTmpRcpAntSlLoanCusRfc(@Param("openDay") String openDay);

    /**
     * 更新网金台账五级分类数据
     *
     * @param openDay
     * @return
     */
    int updateAccLoanWj(@Param("openDay") String openDay);

    /**
     * 更新网金核销台账数据- 更新核销状态 蚂蚁一期
     *
     * @param openDay
     * @return
     */
    int updateAccLoanWj08(@Param("openDay") String openDay);


    /**
     * 更新网金核销台账数据- 更新核销状态 蚂蚁二期
     *
     * @param openDay
     * @return
     */
    int updateAccLoanWj09(@Param("openDay") String openDay);

//    /**
//     *  网金贷款全部结清时，网金贷款协议注销
//     *
//     * @param openDay
//     * @return
//     */
//    int updateCtrLoanContWj(@Param("openDay") String openDay);


//    /**
//     * 网金贷款合同项下无台账，到期自动注销
//     *
//     * @param openDay
//     * @return
//     */
//    int updateCtrLoanContWjByNoLoan(@Param("openDay") String openDay);

}
