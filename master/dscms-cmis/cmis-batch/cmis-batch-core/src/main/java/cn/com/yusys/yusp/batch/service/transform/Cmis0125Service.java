package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0125Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0125</br>
 * 任务名称：加工任务-业务处理-资金同业批复状态 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0125Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0125Service.class);

    @Autowired
    private Cmis0125Mapper cmis0125Mapper;


    /**
     * 更新同业机构准入台账表
     * @param openDay
     */
    public void cmis0125UpdateIntbankOrgAdmitAcc(String openDay) {
        logger.info("更新同业机构准入台账表开始,请求参数为:[{}]", openDay);
        int updateIntbankOrgAdmitAcc = cmis0125Mapper.updateIntbankOrgAdmitAcc(openDay);
        logger.info("更新同业机构准入台账表结束,返回参数为:[{}]", updateIntbankOrgAdmitAcc);
    }

    /**
     * 更新同业机构准入批复表
     * @param openDay
     */
    public void cmis0125UpdateIntbankOrgAdmitReply(String openDay) {
        logger.info("更新同业机构准入批复表开始,请求参数为:[{}]", openDay);
        int updateIntbankOrgAdmitReply = cmis0125Mapper.updateIntbankOrgAdmitReply(openDay);
        logger.info("更新同业机构准入批复表结束,返回参数为:[{}]", updateIntbankOrgAdmitReply);
    }

    /**
     * 更新同业授信台账表
     * @param openDay
     */
    public void cmis0125UpdateLmtIntbankAcc(String openDay) {
        logger.info("更新同业授信台账表开始,请求参数为:[{}]", openDay);
        int updateLmtIntbankAcc = cmis0125Mapper.updateLmtIntbankAcc(openDay);
        logger.info("更新同业授信台账表结束,返回参数为:[{}]", updateLmtIntbankAcc);
    }

    /**
     * 更新同业授信批复表
     * @param openDay
     */
    public void cmis0125UpdateLmtIntbankReply(String openDay) {
        logger.info("更新同业授信批复表开始,请求参数为:[{}]", openDay);
        int updateLmtIntbankReply = cmis0125Mapper.updateLmtIntbankReply(openDay);
        logger.info("更新同业授信批复表结束,返回参数为:[{}]", updateLmtIntbankReply);
    }

    /**
     * 更新单笔投资授信台账表
     * @param openDay
     */
    public void cmis0125UpdateLmtSigInvestAcc(String openDay) {
        logger.info("更新单笔投资授信台账表开始,请求参数为:[{}]", openDay);
        int updateLmtSigInvestAcc = cmis0125Mapper.updateLmtSigInvestAcc(openDay);
        logger.info("更新单笔投资授信台账表结束,返回参数为:[{}]", updateLmtSigInvestAcc);
    }

    /**
     * 更新底层授信额度台账表
     * @param openDay
     */
    public void cmis0125UpdateLmtSigInvestBasicLmtAcc(String openDay) {
        logger.info("更新底层授信额度台账表开始,请求参数为:[{}]", openDay);
        int updateLmtSigInvestBasicLmtAcc = cmis0125Mapper.updateLmtSigInvestBasicLmtAcc(openDay);
        logger.info("更新底层授信额度台账表结束,返回参数为:[{}]", updateLmtSigInvestBasicLmtAcc);
    }
}
