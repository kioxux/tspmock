/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtDiscountBatch
 * @类描述: bat_s_pjp_bt_discount_batch数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-03 10:58:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_pjp_bt_discount_batch")
public class BatSPjpBtDiscountBatch extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键id **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "DISC_BATCH_ID")
	private String discBatchId;
	
	/** 产品id **/
	@Column(name = "S_PROD_ID", unique = false, nullable = true, length = 40)
	private String sProdId;
	
	/** 操作员id **/
	@Column(name = "S_OPERATOR_ID", unique = false, nullable = true, length = 40)
	private String sOperatorId;
	
	/** 客户名称 **/
	@Column(name = "S_CUST_NAME", unique = false, nullable = true, length = 80)
	private String sCustName;
	
	/** 客户账号 **/
	@Column(name = "S_CUST_ACCOUNT", unique = false, nullable = true, length = 80)
	private String sCustAccount;
	
	/** 客户组织机构代码号 **/
	@Column(name = "S_CUST_ORG_CODE", unique = false, nullable = true, length = 20)
	private String sCustOrgCode;
	
	/** 客户开户行 **/
	@Column(name = "S_CUST_BANK", unique = false, nullable = true, length = 80)
	private String sCustBank;
	
	/** 票据状态 **/
	@Column(name = "S_BILL_STATUS", unique = false, nullable = true, length = 10)
	private String sBillStatus;
	
	/** 付息方式 **/
	@Column(name = "S_INT_PAYWAY", unique = false, nullable = true, length = 10)
	private String sIntPayway;
	
	/** 买方付息人名称 **/
	@Column(name = "S_INTPAYER_NAME", unique = false, nullable = true, length = 80)
	private String sIntpayerName;
	
	/** 买方付息人组织机构代码号 **/
	@Column(name = "S_INTPAYER_ORG_CODE", unique = false, nullable = true, length = 20)
	private String sIntpayerOrgCode;
	
	/** 买方付息人账号 **/
	@Column(name = "S_INTPAYER_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sIntpayerAccount;
	
	/** 买方付息人开户行 **/
	@Column(name = "S_INTPAYER_BANK", unique = false, nullable = true, length = 80)
	private String sIntpayerBank;
	
	/** 客户经理id **/
	@Column(name = "S_CUST_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String sCustManagerId;

    /**
     * 申请日
     **/
    @Column(name = "D_APPLI_DT", unique = false, nullable = true, length = 60)
    private String dAppliDt;
	
	/** 工作流程ID **/
	@Column(name = "WORKFLOW_CASEID", unique = false, nullable = true, length = 10)
	private Integer workflowCaseid;
	
	/** 票据类型 **/
	@Column(name = "S_BILL_TYPE", unique = false, nullable = true, length = 10)
	private String sBillType;
	
	/** 子流程ID **/
	@Column(name = "WORKFLOW_SUB_CASEID", unique = false, nullable = true, length = 10)
	private Integer workflowSubCaseid;
	
	/** 应付利息总和 **/
	@Column(name = "F_INT_SUM", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fIntSum;
	
	/** 大额行号 **/
	@Column(name = "S_MBFEBANKCODE", unique = false, nullable = true, length = 40)
	private String sMbfebankcode;
	
	/** 批次号 **/
	@Column(name = "S_BATCH_NO", unique = false, nullable = true, length = 40)
	private String sBatchNo;
	
	/** 贴现类型 **/
	@Column(name = "S_DISC_CLASS", unique = false, nullable = true, length = 40)
	private String sDiscClass;
	
	/** 追索标识 **/
	@Column(name = "S_TRO_SIGN", unique = false, nullable = true, length = 40)
	private String sTroSign;
	
	/** 票据介质 **/
	@Column(name = "S_BILL_MEDIA", unique = false, nullable = true, length = 40)
	private String sBillMedia;
	
	/** 入账帐号 **/
	@Column(name = "S_INACC_NUM", unique = false, nullable = true, length = 40)
	private String sInaccNum;
	
	/** 入账行号 **/
	@Column(name = "S_INACC_BANKNUM", unique = false, nullable = true, length = 40)
	private String sInaccBanknum;
	
	/** 贴现种类 **/
	@Column(name = "S_DISC_TYPE", unique = false, nullable = true, length = 40)
	private String sDiscType;
	
	/** 清算方式 **/
	@Column(name = "S_CLEAR_WAY", unique = false, nullable = true, length = 40)
	private String sClearWay;
	
	/** 付息比例 **/
	@Column(name = "EN_SC", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal enSc;
	
	/** 客户经理编号 **/
	@Column(name = "S_NUMBER_ID", unique = false, nullable = true, length = 40)
	private String sNumberId;

    /**
     * 贴现日
     **/
    @Column(name = "D_BUY_DT", unique = false, nullable = true, length = 60)
    private String dBuyDt;
	
	/** 付息人账号 **/
	@Column(name = "S_PAYEEACCOUNT", unique = false, nullable = true, length = 40)
	private String sPayeeaccount;
	
	/** 付息人名称 **/
	@Column(name = "S_PAYEENAME", unique = false, nullable = true, length = 40)
	private String sPayeename;
	
	/** 是否代理 **/
	@Column(name = "S_ISPROXY", unique = false, nullable = true, length = 40)
	private String sIsproxy;
	
	/** 是否包买 **/
	@Column(name = "S_ISBUY", unique = false, nullable = true, length = 40)
	private String sIsbuy;
	
	/** 代理人名称 **/
	@Column(name = "PROXYNAME", unique = false, nullable = true, length = 80)
	private String proxyname;
	
	/** 代理人组织机构代码号 **/
	@Column(name = "PROXYORGCODE", unique = false, nullable = true, length = 80)
	private String proxyorgcode;
	
	/** 代扣账号 **/
	@Column(name = "PROXYACCOUNT", unique = false, nullable = true, length = 40)
	private String proxyaccount;
	
	/** 代扣金额 **/
	@Column(name = "PROXYAMOUNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal proxyamount;
	
	/** 利率类型 **/
	@Column(name = "S_RATE_TYPE", unique = false, nullable = true, length = 40)
	private String sRateType;
	
	/** 利率 **/
	@Column(name = "F_RATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fRate;
	
	/** 出库入库状态 **/
	@Column(name = "INSTORAGE_STS", unique = false, nullable = true, length = 20)
	private String instorageSts;
	
	/** 行内序号 **/
	@Column(name = "SEQUENCE_ID", unique = false, nullable = true, length = 40)
	private String sequenceId;
	
	/** 批次中所有票据票面总金额 **/
	@Column(name = "F_BILL_AMOUNT_SUM", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fBillAmountSum;
	
	/** 批次中所有票据总笔数 **/
	@Column(name = "F_BILL_SUM", unique = false, nullable = true, length = 10)
	private Integer fBillSum;
	
	/** 批次中所有票据总笔实付金额贴入方计算 **/
	@Column(name = "F_PAY__AMOUNT_SUM", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fPayAmountSum;
	
	/** 批次中所有票据总笔实付金额贴出方计算 **/
	@Column(name = "F_BUY_PAY_AMOUNT_SUM", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fBuyPayAmountSum;
	
	/** 信贷协议流水号 **/
	@Column(name = "TXTRANSNUMBER", unique = false, nullable = true, length = 20)
	private String txtransnumber;
	
	/** 贴现总收益 **/
	@Column(name = "INCOMETOTAL", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal incometotal;

    /**
     * 时间撮-业务明细修改时间标识
     **/
    @Column(name = "TIMEVERSION", unique = false, nullable = true, length = 100)
    private String timeversion;
	
	/** 机构id(操作员所在机构id) **/
	@Column(name = "S_BRANCH_ID", unique = false, nullable = true, length = 40)
	private String sBranchId;
	
	/** 代理行业务标记 **/
	@Column(name = "AGENT_FLAG", unique = false, nullable = true, length = 2)
	private String agentFlag;
	
	/** 行业投向分类代码 **/
	@Column(name = "CLASSIFICATION", unique = false, nullable = true, length = 30)
	private String classification;
	
	/** 行业投向分类名称 **/
	@Column(name = "CLASSIFICATION_NAME", unique = false, nullable = true, length = 200)
	private String classificationName;
	
	/** 是否跨行贴现 STD_YESORNO **/
	@Column(name = "ISNOT_INTERBANK", unique = false, nullable = true, length = 2)
	private String isnotInterbank;
	
	/** 客户经理名 **/
	@Column(name = "S_CUST_MANAGER_NAME", unique = false, nullable = true, length = 150)
	private String sCustManagerName;
	
	/** 贴现成本利率 **/
	@Column(name = "DISCOUNT_COST_RATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal discountCostRate;
	
	/** 数据移植标记 STD_RZ_YZ_FLAG **/
	@Column(name = "YZ_FLAG", unique = false, nullable = true, length = 2)
	private String yzFlag;
	
	/** 贴现客户ID **/
	@Column(name = "CUST_ID", unique = false, nullable = true, length = 50)
	private String custId;
	
	/** 贴现客户号 **/
	@Column(name = "CUST_NO", unique = false, nullable = true, length = 40)
	private String custNo;
	
	/** 复核岗ID **/
	@Column(name = "CHECK_OPERATOR_ID", unique = false, nullable = true, length = 40)
	private String checkOperatorId;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param discBatchId
	 */
	public void setDiscBatchId(String discBatchId) {
		this.discBatchId = discBatchId;
	}
	
    /**
     * @return discBatchId
     */
	public String getDiscBatchId() {
		return this.discBatchId;
	}
	
	/**
	 * @param sProdId
	 */
	public void setSProdId(String sProdId) {
		this.sProdId = sProdId;
	}
	
    /**
     * @return sProdId
     */
	public String getSProdId() {
		return this.sProdId;
	}
	
	/**
	 * @param sOperatorId
	 */
	public void setSOperatorId(String sOperatorId) {
		this.sOperatorId = sOperatorId;
	}
	
    /**
     * @return sOperatorId
     */
	public String getSOperatorId() {
		return this.sOperatorId;
	}
	
	/**
	 * @param sCustName
	 */
	public void setSCustName(String sCustName) {
		this.sCustName = sCustName;
	}
	
    /**
     * @return sCustName
     */
	public String getSCustName() {
		return this.sCustName;
	}
	
	/**
	 * @param sCustAccount
	 */
	public void setSCustAccount(String sCustAccount) {
		this.sCustAccount = sCustAccount;
	}
	
    /**
     * @return sCustAccount
     */
	public String getSCustAccount() {
		return this.sCustAccount;
	}
	
	/**
	 * @param sCustOrgCode
	 */
	public void setSCustOrgCode(String sCustOrgCode) {
		this.sCustOrgCode = sCustOrgCode;
	}
	
    /**
     * @return sCustOrgCode
     */
	public String getSCustOrgCode() {
		return this.sCustOrgCode;
	}
	
	/**
	 * @param sCustBank
	 */
	public void setSCustBank(String sCustBank) {
		this.sCustBank = sCustBank;
	}
	
    /**
     * @return sCustBank
     */
	public String getSCustBank() {
		return this.sCustBank;
	}
	
	/**
	 * @param sBillStatus
	 */
	public void setSBillStatus(String sBillStatus) {
		this.sBillStatus = sBillStatus;
	}
	
    /**
     * @return sBillStatus
     */
	public String getSBillStatus() {
		return this.sBillStatus;
	}
	
	/**
	 * @param sIntPayway
	 */
	public void setSIntPayway(String sIntPayway) {
		this.sIntPayway = sIntPayway;
	}
	
    /**
     * @return sIntPayway
     */
	public String getSIntPayway() {
		return this.sIntPayway;
	}
	
	/**
	 * @param sIntpayerName
	 */
	public void setSIntpayerName(String sIntpayerName) {
		this.sIntpayerName = sIntpayerName;
	}
	
    /**
     * @return sIntpayerName
     */
	public String getSIntpayerName() {
		return this.sIntpayerName;
	}
	
	/**
	 * @param sIntpayerOrgCode
	 */
	public void setSIntpayerOrgCode(String sIntpayerOrgCode) {
		this.sIntpayerOrgCode = sIntpayerOrgCode;
	}
	
    /**
     * @return sIntpayerOrgCode
     */
	public String getSIntpayerOrgCode() {
		return this.sIntpayerOrgCode;
	}
	
	/**
	 * @param sIntpayerAccount
	 */
	public void setSIntpayerAccount(String sIntpayerAccount) {
		this.sIntpayerAccount = sIntpayerAccount;
	}
	
    /**
     * @return sIntpayerAccount
     */
	public String getSIntpayerAccount() {
		return this.sIntpayerAccount;
	}
	
	/**
	 * @param sIntpayerBank
	 */
	public void setSIntpayerBank(String sIntpayerBank) {
		this.sIntpayerBank = sIntpayerBank;
	}
	
    /**
     * @return sIntpayerBank
     */
	public String getSIntpayerBank() {
		return this.sIntpayerBank;
	}
	
	/**
	 * @param sCustManagerId
	 */
	public void setSCustManagerId(String sCustManagerId) {
		this.sCustManagerId = sCustManagerId;
	}
	
    /**
     * @return sCustManagerId
     */
	public String getSCustManagerId() {
		return this.sCustManagerId;
	}
	
	/**
	 * @param dAppliDt
	 */
	public void setDAppliDt(String dAppliDt) {
		this.dAppliDt = dAppliDt;
	}
	
    /**
     * @return dAppliDt
     */
	public String getDAppliDt() {
		return this.dAppliDt;
	}
	
	/**
	 * @param workflowCaseid
	 */
	public void setWorkflowCaseid(Integer workflowCaseid) {
		this.workflowCaseid = workflowCaseid;
	}
	
    /**
     * @return workflowCaseid
     */
	public Integer getWorkflowCaseid() {
		return this.workflowCaseid;
	}
	
	/**
	 * @param sBillType
	 */
	public void setSBillType(String sBillType) {
		this.sBillType = sBillType;
	}
	
    /**
     * @return sBillType
     */
	public String getSBillType() {
		return this.sBillType;
	}
	
	/**
	 * @param workflowSubCaseid
	 */
	public void setWorkflowSubCaseid(Integer workflowSubCaseid) {
		this.workflowSubCaseid = workflowSubCaseid;
	}
	
    /**
     * @return workflowSubCaseid
     */
	public Integer getWorkflowSubCaseid() {
		return this.workflowSubCaseid;
	}
	
	/**
	 * @param fIntSum
	 */
	public void setFIntSum(java.math.BigDecimal fIntSum) {
		this.fIntSum = fIntSum;
	}
	
    /**
     * @return fIntSum
     */
	public java.math.BigDecimal getFIntSum() {
		return this.fIntSum;
	}
	
	/**
	 * @param sMbfebankcode
	 */
	public void setSMbfebankcode(String sMbfebankcode) {
		this.sMbfebankcode = sMbfebankcode;
	}
	
    /**
     * @return sMbfebankcode
     */
	public String getSMbfebankcode() {
		return this.sMbfebankcode;
	}
	
	/**
	 * @param sBatchNo
	 */
	public void setSBatchNo(String sBatchNo) {
		this.sBatchNo = sBatchNo;
	}
	
    /**
     * @return sBatchNo
     */
	public String getSBatchNo() {
		return this.sBatchNo;
	}
	
	/**
	 * @param sDiscClass
	 */
	public void setSDiscClass(String sDiscClass) {
		this.sDiscClass = sDiscClass;
	}
	
    /**
     * @return sDiscClass
     */
	public String getSDiscClass() {
		return this.sDiscClass;
	}
	
	/**
	 * @param sTroSign
	 */
	public void setSTroSign(String sTroSign) {
		this.sTroSign = sTroSign;
	}
	
    /**
     * @return sTroSign
     */
	public String getSTroSign() {
		return this.sTroSign;
	}
	
	/**
	 * @param sBillMedia
	 */
	public void setSBillMedia(String sBillMedia) {
		this.sBillMedia = sBillMedia;
	}
	
    /**
     * @return sBillMedia
     */
	public String getSBillMedia() {
		return this.sBillMedia;
	}
	
	/**
	 * @param sInaccNum
	 */
	public void setSInaccNum(String sInaccNum) {
		this.sInaccNum = sInaccNum;
	}
	
    /**
     * @return sInaccNum
     */
	public String getSInaccNum() {
		return this.sInaccNum;
	}
	
	/**
	 * @param sInaccBanknum
	 */
	public void setSInaccBanknum(String sInaccBanknum) {
		this.sInaccBanknum = sInaccBanknum;
	}
	
    /**
     * @return sInaccBanknum
     */
	public String getSInaccBanknum() {
		return this.sInaccBanknum;
	}
	
	/**
	 * @param sDiscType
	 */
	public void setSDiscType(String sDiscType) {
		this.sDiscType = sDiscType;
	}
	
    /**
     * @return sDiscType
     */
	public String getSDiscType() {
		return this.sDiscType;
	}
	
	/**
	 * @param sClearWay
	 */
	public void setSClearWay(String sClearWay) {
		this.sClearWay = sClearWay;
	}
	
    /**
     * @return sClearWay
     */
	public String getSClearWay() {
		return this.sClearWay;
	}
	
	/**
	 * @param enSc
	 */
	public void setEnSc(java.math.BigDecimal enSc) {
		this.enSc = enSc;
	}
	
    /**
     * @return enSc
     */
	public java.math.BigDecimal getEnSc() {
		return this.enSc;
	}
	
	/**
	 * @param sNumberId
	 */
	public void setSNumberId(String sNumberId) {
		this.sNumberId = sNumberId;
	}
	
    /**
     * @return sNumberId
     */
	public String getSNumberId() {
		return this.sNumberId;
	}
	
	/**
	 * @param dBuyDt
	 */
	public void setDBuyDt(String dBuyDt) {
		this.dBuyDt = dBuyDt;
	}
	
    /**
     * @return dBuyDt
     */
	public String getDBuyDt() {
		return this.dBuyDt;
	}
	
	/**
	 * @param sPayeeaccount
	 */
	public void setSPayeeaccount(String sPayeeaccount) {
		this.sPayeeaccount = sPayeeaccount;
	}
	
    /**
     * @return sPayeeaccount
     */
	public String getSPayeeaccount() {
		return this.sPayeeaccount;
	}
	
	/**
	 * @param sPayeename
	 */
	public void setSPayeename(String sPayeename) {
		this.sPayeename = sPayeename;
	}
	
    /**
     * @return sPayeename
     */
	public String getSPayeename() {
		return this.sPayeename;
	}
	
	/**
	 * @param sIsproxy
	 */
	public void setSIsproxy(String sIsproxy) {
		this.sIsproxy = sIsproxy;
	}
	
    /**
     * @return sIsproxy
     */
	public String getSIsproxy() {
		return this.sIsproxy;
	}
	
	/**
	 * @param sIsbuy
	 */
	public void setSIsbuy(String sIsbuy) {
		this.sIsbuy = sIsbuy;
	}
	
    /**
     * @return sIsbuy
     */
	public String getSIsbuy() {
		return this.sIsbuy;
	}
	
	/**
	 * @param proxyname
	 */
	public void setProxyname(String proxyname) {
		this.proxyname = proxyname;
	}
	
    /**
     * @return proxyname
     */
	public String getProxyname() {
		return this.proxyname;
	}
	
	/**
	 * @param proxyorgcode
	 */
	public void setProxyorgcode(String proxyorgcode) {
		this.proxyorgcode = proxyorgcode;
	}
	
    /**
     * @return proxyorgcode
     */
	public String getProxyorgcode() {
		return this.proxyorgcode;
	}
	
	/**
	 * @param proxyaccount
	 */
	public void setProxyaccount(String proxyaccount) {
		this.proxyaccount = proxyaccount;
	}
	
    /**
     * @return proxyaccount
     */
	public String getProxyaccount() {
		return this.proxyaccount;
	}
	
	/**
	 * @param proxyamount
	 */
	public void setProxyamount(java.math.BigDecimal proxyamount) {
		this.proxyamount = proxyamount;
	}
	
    /**
     * @return proxyamount
     */
	public java.math.BigDecimal getProxyamount() {
		return this.proxyamount;
	}
	
	/**
	 * @param sRateType
	 */
	public void setSRateType(String sRateType) {
		this.sRateType = sRateType;
	}
	
    /**
     * @return sRateType
     */
	public String getSRateType() {
		return this.sRateType;
	}
	
	/**
	 * @param fRate
	 */
	public void setFRate(java.math.BigDecimal fRate) {
		this.fRate = fRate;
	}
	
    /**
     * @return fRate
     */
	public java.math.BigDecimal getFRate() {
		return this.fRate;
	}
	
	/**
	 * @param instorageSts
	 */
	public void setInstorageSts(String instorageSts) {
		this.instorageSts = instorageSts;
	}
	
    /**
     * @return instorageSts
     */
	public String getInstorageSts() {
		return this.instorageSts;
	}
	
	/**
	 * @param sequenceId
	 */
	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}
	
    /**
     * @return sequenceId
     */
	public String getSequenceId() {
		return this.sequenceId;
	}
	
	/**
	 * @param fBillAmountSum
	 */
	public void setFBillAmountSum(java.math.BigDecimal fBillAmountSum) {
		this.fBillAmountSum = fBillAmountSum;
	}
	
    /**
     * @return fBillAmountSum
     */
	public java.math.BigDecimal getFBillAmountSum() {
		return this.fBillAmountSum;
	}
	
	/**
	 * @param fBillSum
	 */
	public void setFBillSum(Integer fBillSum) {
		this.fBillSum = fBillSum;
	}
	
    /**
     * @return fBillSum
     */
	public Integer getFBillSum() {
		return this.fBillSum;
	}
	
	/**
	 * @param fPayAmountSum
	 */
	public void setFPayAmountSum(java.math.BigDecimal fPayAmountSum) {
		this.fPayAmountSum = fPayAmountSum;
	}
	
    /**
     * @return fPayAmountSum
     */
	public java.math.BigDecimal getFPayAmountSum() {
		return this.fPayAmountSum;
	}
	
	/**
	 * @param fBuyPayAmountSum
	 */
	public void setFBuyPayAmountSum(java.math.BigDecimal fBuyPayAmountSum) {
		this.fBuyPayAmountSum = fBuyPayAmountSum;
	}
	
    /**
     * @return fBuyPayAmountSum
     */
	public java.math.BigDecimal getFBuyPayAmountSum() {
		return this.fBuyPayAmountSum;
	}
	
	/**
	 * @param txtransnumber
	 */
	public void setTxtransnumber(String txtransnumber) {
		this.txtransnumber = txtransnumber;
	}
	
    /**
     * @return txtransnumber
     */
	public String getTxtransnumber() {
		return this.txtransnumber;
	}
	
	/**
	 * @param incometotal
	 */
	public void setIncometotal(java.math.BigDecimal incometotal) {
		this.incometotal = incometotal;
	}
	
    /**
     * @return incometotal
     */
	public java.math.BigDecimal getIncometotal() {
		return this.incometotal;
	}
	
	/**
	 * @param timeversion
	 */
	public void setTimeversion(String timeversion) {
		this.timeversion = timeversion;
	}
	
    /**
     * @return timeversion
     */
	public String getTimeversion() {
		return this.timeversion;
	}
	
	/**
	 * @param sBranchId
	 */
	public void setSBranchId(String sBranchId) {
		this.sBranchId = sBranchId;
	}
	
    /**
     * @return sBranchId
     */
	public String getSBranchId() {
		return this.sBranchId;
	}
	
	/**
	 * @param agentFlag
	 */
	public void setAgentFlag(String agentFlag) {
		this.agentFlag = agentFlag;
	}
	
    /**
     * @return agentFlag
     */
	public String getAgentFlag() {
		return this.agentFlag;
	}
	
	/**
	 * @param classification
	 */
	public void setClassification(String classification) {
		this.classification = classification;
	}
	
    /**
     * @return classification
     */
	public String getClassification() {
		return this.classification;
	}
	
	/**
	 * @param classificationName
	 */
	public void setClassificationName(String classificationName) {
		this.classificationName = classificationName;
	}
	
    /**
     * @return classificationName
     */
	public String getClassificationName() {
		return this.classificationName;
	}
	
	/**
	 * @param isnotInterbank
	 */
	public void setIsnotInterbank(String isnotInterbank) {
		this.isnotInterbank = isnotInterbank;
	}
	
    /**
     * @return isnotInterbank
     */
	public String getIsnotInterbank() {
		return this.isnotInterbank;
	}
	
	/**
	 * @param sCustManagerName
	 */
	public void setSCustManagerName(String sCustManagerName) {
		this.sCustManagerName = sCustManagerName;
	}
	
    /**
     * @return sCustManagerName
     */
	public String getSCustManagerName() {
		return this.sCustManagerName;
	}
	
	/**
	 * @param discountCostRate
	 */
	public void setDiscountCostRate(java.math.BigDecimal discountCostRate) {
		this.discountCostRate = discountCostRate;
	}
	
    /**
     * @return discountCostRate
     */
	public java.math.BigDecimal getDiscountCostRate() {
		return this.discountCostRate;
	}
	
	/**
	 * @param yzFlag
	 */
	public void setYzFlag(String yzFlag) {
		this.yzFlag = yzFlag;
	}
	
    /**
     * @return yzFlag
     */
	public String getYzFlag() {
		return this.yzFlag;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custNo
	 */
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	
    /**
     * @return custNo
     */
	public String getCustNo() {
		return this.custNo;
	}
	
	/**
	 * @param checkOperatorId
	 */
	public void setCheckOperatorId(String checkOperatorId) {
		this.checkOperatorId = checkOperatorId;
	}
	
    /**
     * @return checkOperatorId
     */
	public String getCheckOperatorId() {
		return this.checkOperatorId;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}