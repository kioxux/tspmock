/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.LmtCheckDataOneService;

import cn.com.yusys.yusp.batch.repository.mapper.check.LmtCheckUpdateOutStandAmtBWMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateOutStandAmtBWService
 * @类描述: #服务类
 * @功能描述: 数据更新额度校正-合同临时表-表外占用总余额与敞口余额
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCheckUpdateOutStandAmtBWService {

    private static final Logger logger = LoggerFactory.getLogger(LmtCheckUpdateOutStandAmtBWService.class);

    @Autowired
    private LmtCheckUpdateOutStandAmtBWMapper lmtCheckUpdateOutStandAmtBWMapper;

    public void checkOutLmtCheckUpdateOutStandAmtBW(String cusId){
        logger.info("6.LmtCheckUpdateOutStandAmtBW数据更新额度校正-合同临时表-表外占用总余额与敞口余额 --------start");

        /** 1、最高额（未到期）：占用总余额、占用敞口余额； 银票、保函、信用证、贴现 **/
        insertOutStandAmtBWZge(cusId) ;

        /** 2、一般合同（未到期）：占用总余额、占用敞口余额； 银票、保函、信用证、贴现 **/
        insertOutStandAmtBWYbht(cusId) ;

        /** 3、更新低风险担保方式占用敞口金额和敞口余额为0 **/
        updateOutStandAmtBWDfxdb(cusId) ;

        /** 4、银票贴现占用敞口金额为0 **/
        updateOutStandAmtBWYptx(cusId) ;
        logger.info("6.LmtCheckUpdateOutStandAmtBW数据更新额度校正-合同临时表-表外占用总余额与敞口余额 --------end");
    }

    /**
     * 1、最高额（未到期）：占用总余额、占用敞口余额； 银票、保函、信用证、贴现
     */
    public int insertOutStandAmtBWZge(String cusId){
        logger.info("1、最高额（未到期）：占用总余额、占用敞口余额； 银票、保函、信用证、贴现 --------");
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWZge1(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWZge2(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWZge3(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWZge4(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWZge5(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWZge6(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWZge7(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWZge8(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWZge9(cusId);
        return lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWZge10(cusId);
    }

    /**
     * 2、一般合同（未到期）：占用总余额、占用敞口余额； 银票、保函、信用证、贴现
     */
    public int insertOutStandAmtBWYbht(String cusId){
        logger.info("2、一般合同（未到期）：占用总余额、占用敞口余额； 银票、保函、信用证、贴现 --------");
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWYbht1(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWYbht2(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWYbht3(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWYbht4(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWYbht5(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWYbht6(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWYbht7(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWYbht8(cusId);
        lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWYbht9(cusId);
        return lmtCheckUpdateOutStandAmtBWMapper.insertOutStandAmtBWYbht10(cusId);
    }

    /**
     * 3、更新低风险担保方式占用敞口金额和敞口余额为0
     */
    public int updateOutStandAmtBWDfxdb(String cusId){
        logger.info("3、更新低风险担保方式占用敞口金额和敞口余额为0 --------");
        return lmtCheckUpdateOutStandAmtBWMapper.updateOutStandAmtBWDfxdb(cusId);
    }

    /**
     * 4、银票贴现占用敞口金额为0
     */
    public int updateOutStandAmtBWYptx(String cusId){
        logger.info("4、银票贴现占用敞口金额为0 --------");
        return lmtCheckUpdateOutStandAmtBWMapper.updateOutStandAmtBWYptx(cusId);
    }

}
