package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0414Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-贷后管理-小微经营性定期检查 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0414Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0414Service.class);

    @Resource
    private Cmis0414Mapper cmis0414Mapper;

    /**
     * 插入贷后检查任务表01
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList01(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info("1、小微经营性 满足（五金分类正常 、无逾期欠息、单一担保方式（抵押类200万、保证100万、信用 50万） 、无查封、无预警、  自动完成分类任务");
        int insertPspTaskList01 = cmis0414Mapper.insertPspTaskList01(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList01);

    }

    /**
     * 插入贷后检查任务表01
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList02(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info("2、 小微经营性 满足（五金分类正常 、无 逾期欠息、单一担保方式（抵押类200万、保证100万、信用 50万） 、无查封、预警为 黄或橙  要求完成天数为30");
        int insertPspTaskList02 = cmis0414Mapper.insertPspTaskList02(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList02);
    }


    /**
     * 插入贷后检查任务表01
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList03(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info("3、 小微经营性 满足（五金分类正常 、无 逾期欠息、单一担保方式（抵押类200万、保证100万、信用 50万） 、无  查封、预警为  红黑   要求完成天数为30");
        int insertPspTaskList03 = cmis0414Mapper.insertPspTaskList03(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList03);
    }

    /**
     * 插入贷后检查任务表01
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList04(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info("4、 小微经营性 满足（五金分类正常 、无 逾期欠息、单一担保方式（抵押类200万、保证100万、信用 50万） 、有  查封、预警为   黄橙   要求完成天数为30");
        int insertPspTaskList04 = cmis0414Mapper.insertPspTaskList04(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList04);

    }

    /**
     * 插入贷后检查任务表01
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList05(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info("5、 小微经营性 满足（五金分类正常 、无 逾期欠息、单一担保方式（抵押类200万、保证100万、信用 50万） 、有  查封、预警为   红黑   要求完成天数为30");
        int insertPspTaskList05 = cmis0414Mapper.insertPspTaskList05(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList05);


    }

    /**
     * 插入贷后检查任务表01
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList06(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info("6、 小微经营性 满足（五金分类正常 、无 逾期欠息、不符合 单一担保方式（抵押类200万、保证100万、信用 50万）  要求完成天数为90");
        int insertPspTaskList06 = cmis0414Mapper.insertPspTaskList06(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList06);
    }

    /**
     * 插入贷后检查任务表01
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList07(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info("7、 小微经营性 满足（五金分类正常 、 有 逾期欠息   要求完成天数为75）");
        int insertPspTaskList07 = cmis0414Mapper.insertPspTaskList07(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList07);
    }

    /**
     * 插入贷后检查任务表01
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList08(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info("8、 小微经营性 满足（五金分类关注、    要求完成天数为75）");
        int insertPspTaskList08 = cmis0414Mapper.insertPspTaskList08(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList08);
    }

    /**
     * 插入贷后检查任务表09
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList09(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info(" 9、 小微经营性 满足（五金分类次级以后 、    要求完成天数为60 )");
        int insertPspTaskList09 = cmis0414Mapper.insertPspTaskList09(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList09);

    }

    /**
     * 插入贷后检查任务表10
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList10(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info(" 10、  其他情况   要求完成天数为30");
        int insertPspTaskList10 = cmis0414Mapper.insertPspTaskList10(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList10);
    }

    /**
     * 插入贷后检查任务表11
     *
     * @param openDay
     */
    public void cmis0414InsertPspTaskList11(String openDay) {
        logger.info("更新贷后检查任务表开始,请求参数为:[{}]", openDay);
        logger.info("11 现在往前10个月内未生成人工检查任务,只有自动检查任务，则更改为人工检查");
        int updatePspTaskList = cmis0414Mapper.updatePspTaskList(openDay);
        logger.info("更新贷后检查任务表结束,返回参数为:[{}]", updatePspTaskList);
    }
}
