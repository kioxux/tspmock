/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.biz;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.biz.TmpLmtAppCorp;
import cn.com.yusys.yusp.batch.repository.mapper.biz.TmpLmtAppCorpMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: TmpLmtAppCorpService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-27 01:41:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class TmpLmtAppCorpService {

    @Autowired
    private TmpLmtAppCorpMapper tmpLmtAppCorpMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public TmpLmtAppCorp selectByPrimaryKey(String pkId, String grpSerno, String taskDate) {
        return tmpLmtAppCorpMapper.selectByPrimaryKey(pkId, grpSerno, taskDate);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<TmpLmtAppCorp> selectAll(QueryModel model) {
        List<TmpLmtAppCorp> records = (List<TmpLmtAppCorp>) tmpLmtAppCorpMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<TmpLmtAppCorp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<TmpLmtAppCorp> list = tmpLmtAppCorpMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(TmpLmtAppCorp record) {
        return tmpLmtAppCorpMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(TmpLmtAppCorp record) {
        return tmpLmtAppCorpMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(TmpLmtAppCorp record) {
        return tmpLmtAppCorpMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(TmpLmtAppCorp record) {
        return tmpLmtAppCorpMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String grpSerno, String taskDate) {
        return tmpLmtAppCorpMapper.deleteByPrimaryKey(pkId, grpSerno, taskDate);
    }


    /**
     * 查询调用集团客户授信复审信息
     *
     * @param queryModel
     * @return
     */
    @Transactional(readOnly = true)
    public List<TmpLmtAppCorp> queryApprLmtSubBasicInfoGrp(QueryModel queryModel) {
        List<TmpLmtAppCorp> records = (List<TmpLmtAppCorp>) tmpLmtAppCorpMapper.queryApprLmtSubBasicInfoGrp(queryModel);
        return records;
    }
}
