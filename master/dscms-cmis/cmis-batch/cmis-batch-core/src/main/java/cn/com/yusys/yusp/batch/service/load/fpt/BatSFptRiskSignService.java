/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.fpt;

import cn.com.yusys.yusp.batch.domain.load.fpt.BatSFptRiskSign;
import cn.com.yusys.yusp.batch.repository.mapper.load.fpt.BatSFptRiskSignMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSFptRiskSignService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-12 10:11:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSFptRiskSignService {
    private static final Logger logger = LoggerFactory.getLogger(BatSFptRCustBusiSumService.class);
    @Autowired
    private BatSFptRiskSignMapper batSFptRiskSignMapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatSFptRiskSign selectByPrimaryKey(String riskId) {
        return batSFptRiskSignMapper.selectByPrimaryKey(riskId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatSFptRiskSign> selectAll(QueryModel model) {
        List<BatSFptRiskSign> records = (List<BatSFptRiskSign>) batSFptRiskSignMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatSFptRiskSign> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSFptRiskSign> list = batSFptRiskSignMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatSFptRiskSign record) {
        return batSFptRiskSignMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatSFptRiskSign record) {
        return batSFptRiskSignMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatSFptRiskSign record) {
        return batSFptRiskSignMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatSFptRiskSign record) {
        return batSFptRiskSignMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String riskId) {
        return batSFptRiskSignMapper.deleteByPrimaryKey(riskId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSFptRiskSignMapper.deleteByIds(ids);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建预警信号处置表[bat_s_fpt_risk_sign]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_fpt_risk_sign");
        logger.info("重建【FPT00001】预警信号处置表[bat_s_fpt_risk_sign]结束");
        return 0;
    }

    /**
     * 删除Biz表当天数据</br>
     * 20211029:经沟通此方法标注为过时
     *
     * @param openDay
     * @return
     */
    @Deprecated
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteBizByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建风险预警系统-预警信号处置表[biz_fpt_risk_sign]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz", "biz_fpt_risk_sign");
        logger.info("重建风险预警系统-预警信号处置表[biz_fpt_risk_sign]结束");
        return 0;
    }


    /**
     * 将T表数据merge到S表
     *
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]开始,请求参数为:[{}]", openDay);
        int updateSResult = batSFptRiskSignMapper.updateDataDateByOpenDay(openDay);
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]结束,返回参数为:[{}]", updateSResult);
    }

    /**
     * 插入Biz表当天数据</br>
     * 20211029:经沟通此方法标注为过时
     *
     * @param openDay
     */
    @Deprecated
    public void insertBizByOpenDay(String openDay) {
        logger.info("插入Biz表当天数据开始,请求参数为:[{}]", openDay);
        int insertBizByOpenDay = batSFptRiskSignMapper.insertBizByOpenDay(openDay);
        logger.info("插入Biz表当天数据结束,返回参数为:[{}]", insertBizByOpenDay);
    }
}
