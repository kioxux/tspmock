/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKlnaDkzhzb;
import cn.com.yusys.yusp.batch.service.load.core.BatSCoreKlnaDkzhzbService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatSCoreKlnaDkzhzbResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 23:46:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batscoreklnadkzhzb")
public class BatSCoreKlnaDkzhzbResource {
    @Autowired
    private BatSCoreKlnaDkzhzbService batSCoreKlnaDkzhzbService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSCoreKlnaDkzhzb>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSCoreKlnaDkzhzb> list = batSCoreKlnaDkzhzbService.selectAll(queryModel);
        return new ResultDto<List<BatSCoreKlnaDkzhzb>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSCoreKlnaDkzhzb>> index(QueryModel queryModel) {
        List<BatSCoreKlnaDkzhzb> list = batSCoreKlnaDkzhzbService.selectByModel(queryModel);
        return new ResultDto<List<BatSCoreKlnaDkzhzb>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSCoreKlnaDkzhzb> create(@RequestBody BatSCoreKlnaDkzhzb batSCoreKlnaDkzhzb) throws URISyntaxException {
        batSCoreKlnaDkzhzbService.insert(batSCoreKlnaDkzhzb);
        return new ResultDto<BatSCoreKlnaDkzhzb>(batSCoreKlnaDkzhzb);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSCoreKlnaDkzhzb batSCoreKlnaDkzhzb) throws URISyntaxException {
        int result = batSCoreKlnaDkzhzbService.update(batSCoreKlnaDkzhzb);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String farendma, String dkjiejuh) {
        int result = batSCoreKlnaDkzhzbService.deleteByPrimaryKey(farendma, dkjiejuh);
        return new ResultDto<Integer>(result);
    }

}
