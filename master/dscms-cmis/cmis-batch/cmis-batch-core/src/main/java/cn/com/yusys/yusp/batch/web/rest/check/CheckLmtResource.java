/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.check;

import cn.com.yusys.yusp.batch.service.LmtCheckDataOneService.*;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: CheckLmtResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/checkLmt")
public class CheckLmtResource {
    @Autowired
    private LmtCheckInsertContAccRelService lmtCheckInsertContAccRelService;

    @Autowired
    private LmtCheckUpdateContAccRelService lmtCheckUpdateContAccRelService;

    @Autowired
    private LmtCheckInsertLmtContRelService lmtCheckInsertLmtContRelService;

    @Autowired
    private LmtCheckUpdateLmtContRelService lmtCheckUpdateLmtContRelService;

    @Autowired
    private LmtCheckUpdateOutStandAmtBNService lmtCheckUpdateOutStandAmtBNService;

    @Autowired
    private LmtCheckUpdateOutStandAmtBWService lmtCheckUpdateOutStandAmtBWService;

    @Autowired
    private LmtCheckUpdateLmtSubUseService lmtCheckUpdateLmtSubUseService;

    @Autowired
    private LmtCheckUpdateLoanBalanceService lmtCheckUpdateLoanBalanceService;

    @Autowired
    private LmtCheckUpdatePvpOutAmtService lmtCheckUpdatePvpOutAmtService;

    @Autowired
    private LmtCheckUpdateLmtAddAmtService lmtCheckUpdateLmtAddAmtService;

    //------------------------ 1.LmtCheckInsertContAccRel数据插入额度校正-台账临时表 开始-------------------------
    /**
     * 1.1 插入贷款台账
     */
    @PostMapping("/checklmtdataone/{cusId}")
    protected ResultDto<Integer> checkOutLmtInfo(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        /** 1.LmtCheckInsertContAccRel数据插入额度校正-台账临时表 **/
        lmtCheckInsertContAccRelService.checkOutLmtCheckInsertContAccRel(cusId) ;

        /** 2.LmtCheckUpdateContAccRel数据更新额度校正-台账临时表 **/
        lmtCheckUpdateContAccRelService.checkOutLmtCheckUpdateContAccRel(cusId) ;

        /** 3.LmtCheckInsertLmtContRel数据插入额度校正-合同临时表 **/
        lmtCheckInsertLmtContRelService.checkOutLmtCheckInsertLmtContRel(cusId) ;

        /** 4.LmtCheckUpdateLmtContRel数据更新额度校正-合同临时表-更新占用总金额与敞口金额 **/
        lmtCheckUpdateLmtContRelService.checkOutLmtCheckUpdateLmtContRel(cusId) ;

        /** 5.LmtCheckUpdateOutStandAmtBN数据更新额度校正-合同临时表-表内占用总余额与敞口余额 **/
        lmtCheckUpdateOutStandAmtBNService.checkOutLmtCheckUpdateOutStandAmtBN(cusId) ;

        /** 6.LmtCheckUpdateOutStandAmtBW数据更新额度校正-合同临时表-表外占用总余额与敞口余额 **/
        lmtCheckUpdateOutStandAmtBWService.checkOutLmtCheckUpdateOutStandAmtBW(cusId) ;

        /** 7.LmtCheckUpdateLmtSubUse数据更新额度校正-额度占用处理 **/
        lmtCheckUpdateLmtSubUseService.checkOutLmtCheckUpdateLmtSubUse(cusId) ;

        /** 8.LmtCheckUpdateLoanBalance数据更新额度校正-计算分项用信金额 **/
        lmtCheckUpdateLoanBalanceService.checkOutLmtCheckUpdateLoanBalance(cusId) ;

        /** 9.LmtCheckUpdatePvpOutAmt数据更新额度校正-计算分项可出账金额 **/
        lmtCheckUpdatePvpOutAmtService.checkOutLmtCheckUpdatePvpOutAmt(cusId) ;

        /** 10.LmtCheckUpdateLmtAddAmt数据更新额度校正-计算分项授信总额累加 **/
        lmtCheckUpdateLmtAddAmtService.checkOutLmtCheckUpdateLmtAddAmt(cusId) ;

        return new ResultDto<Integer>(Integer.valueOf(1));
    }

    /**
     * 1.1、贷款出账申请
     */
    @PostMapping("/insertloanoccrecord/{cusId}")
    protected ResultDto<Integer> insertLoanOccRecord(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertContAccRelService.insertLoanOccRecord(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 1.2、贷款出账申请
     */
    @PostMapping("/checkinsertdkczapply/{cusId}")
    protected ResultDto<Integer> checkInsertDkczApply(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertContAccRelService.checkInsertDkczApply(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 1.3、委托贷款
     */
    @PostMapping("/checkinsertwtdkrecord/{cusId}")
    protected ResultDto<Integer> checkInsertWtdkRecord(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertContAccRelService.checkInsertWtdkRecord(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 1.4、委托贷款出账申请
     */
    @PostMapping("/checkinsertwtdkczrecord/{cusId}")
    protected ResultDto<Integer> checkInsertWtdkczRecord(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertContAccRelService.checkInsertWtdkczRecord(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 1.5、银票台账
     */
    @PostMapping("/checkinsertyptzrecord/{cusId}")
    protected ResultDto<Integer> checkInsertYptzRecord(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertContAccRelService.checkInsertYptzRecord(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 1.6、银票台账出账申请
     */
    @PostMapping("/checkinsertyptzczrecord/{cusId}")
    protected ResultDto<Integer> checkInsertYptzczRecord(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertContAccRelService.checkInsertYptzczRecord(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 1.8、信用证台账
     */
    @PostMapping("/checkinsertxyztzrecord/{cusId}")
    protected ResultDto<Integer> checkInsertXyztzRecord(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertContAccRelService.checkInsertXyztzRecord(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 1.9、贴现台账
     */
    @PostMapping("/checkinserttxtzrecord/{cusId}")
    protected ResultDto<Integer> checkInsertTxtzRecord(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertContAccRelService.checkInsertTxtzRecord(cusId);
        return new ResultDto<Integer>(result);
    }
    //------------------------ 1.LmtCheckInsertContAccRel数据插入额度校正-台账临时表 结束-------------------------
    /*----------------------*/
    //------------------------ 2.LmtCheckUpdateContAccRel数据更新额度校正-台账临时表 开始-------------------------
    /**
     * 2.1、贷款台账：金额及余额、及状态更新
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatedktz/{cusId}")
    protected ResultDto<Integer> checkupdate_dktz(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.checkupdate_dktz(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 2.2、贷款出账申请：金额及余额、及状态更新
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatedkczsq/{cusId}")
    protected ResultDto<Integer> update_dkczsq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.update_dkczsq(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 2.3、委托贷款台账：金额及余额、及状态更新
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatewtdktz/{cusId}")
    protected ResultDto<Integer> update_wtdktz(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.update_wtdktz(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 2.4、委托贷款出账申请：金额及余额、及状态更新
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatewtdkczsq/{cusId}")
    protected ResultDto<Integer> update_wtdkczsq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.update_wtdkczsq(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 2.5、银票台账：金额及余额、及状态更新
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updateyptz/{cusId}")
    protected ResultDto<Integer> update_yptz(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.update_yptz(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 2.6、银票台账出账申请：金额及余额、及状态更新
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updateyptzczsq/{cusId}")
    protected ResultDto<Integer> update_yptzczsq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.update_yptzczsq(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 2.7、保函台账：金额及余额、及状态更新
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatebhtz/{cusId}")
    protected ResultDto<Integer> update_bhtz(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.update_bhtz(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 2.8、信用证台账：金额及余额、及状态更新
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatexyztz/{cusId}")
    protected ResultDto<Integer> update_xyztz(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.update_xyztz(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 2.9、贴现台账：金额及余额、及状态更新
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatetxtz/{cusId}")
    protected ResultDto<Integer> update_txtz(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.update_txtz(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 2.10、交易业务状态如果为注销、作废，则更新占用余额和占用敞口余额为0
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updateckjyzt/{cusId}")
    protected ResultDto<Integer> update_ck_jyzt(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.update_ck_jyzt(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 2.11、若担保方式为低风险担保，则更新占用敞口为0
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updateckdbfs/{cusId}")
    protected ResultDto<Integer> update_ck_dbfs(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateContAccRelService.update_ck_dbfs(cusId);
        return new ResultDto<Integer>(result);
    }

    //------------------------ 2.LmtCheckUpdateContAccRel数据更新额度校正-台账临时表 结束-------------------------

    /*------------------------------------*/

    //------------------------ 3.LmtCheckInsertLmtContRel数据插入额度校正-合同临时表 开始-------------------------

    /**
     * 3.1、贷款合同
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanreldkht/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelDkht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelDkht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.2、贷款合同申请
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanreldkhtsq/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelDkhtsq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelDkhtsq(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.3、委托贷款合同
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelwtdkht/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelWtdkht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelWtdkht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.4、委托贷款合同申请
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelwtdkhtsq/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelWtdkhtsq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelWtdkhtsq(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.5、最高额授信协议
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelzgesxxy/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelZgesxxy(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelZgesxxy(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.6、最高额授信协议申请
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelzgesxxysq/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelZgesxxysq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelZgesxxysq(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.7、资产池协议
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelzccxy/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelZccxy(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelZccxy(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.8、资产池协议申请
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelzccxysq/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelZccxysq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelZccxysq(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.9、银票合同
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelypht/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelYpht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelYpht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.10、银票合同申请
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelyphtsq/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelYphtsq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelYphtsq(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.11、保函合同
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelbhht/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelBhht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelBhht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.12、保函合同申请
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelbhhtsq/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelBhhtsq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelBhhtsq(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.13、信用证合同
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelxyzht/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelXyzht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelXyzht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.14、信用证合同申请
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanrelxyzhtsq/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelXyzhtsq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelXyzhtsq(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.15、贴现合同
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanreltxht/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelTxht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelTxht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 3.16、贴现合同申请
     *
     * @param cusId
     * @return
     */
    @PostMapping("/insertlmtloanreltxhtsq/{cusId}")
    protected ResultDto<Integer> insertLmtLoanRelTxhtsq(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckInsertLmtContRelService.insertLmtLoanRelTxhtsq(cusId);
        return new ResultDto<Integer>(result);
    }

    //------------------------ 3.LmtCheckInsertLmtContRel数据插入额度校正-合同临时表 结束-------------------------

    //------------------------ 4.LmtCheckUpdateLmtContRel数据更新额度校正-合同临时表-更新占用总金额与敞口金额 开始-------------------------
    /**
     * 4.1、贷款合同/贷款合同申请：更新中间表业务的申请状态和审批状态
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatelmtcontreldkht/{cusId}")
    protected ResultDto<Integer> updateLmtContRelDkht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtContRelService.updateLmtContRelDkht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 4.2、委托贷款合同/委托贷款合同申请：更新中间表业务的申请状态和审批状态
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatelmtcontrelwtdk/{cusId}")
    protected ResultDto<Integer> updateLmtContRelWtdk(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtContRelService.updateLmtContRelWtdk(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 4.2、最高额授信协议/最高额授信协议申请：更新中间表业务的申请状态和审批状态
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatelmtcontrelzgesxxy/{cusId}")
    protected ResultDto<Integer> updateLmtContRelZgesxxy(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtContRelService.updateLmtContRelZgesxxy(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 4.3、资产池协议/资产池协议申请：更新中间表业务的申请状态和审批状态
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatelmtcontrelzcc/{cusId}")
    protected ResultDto<Integer> updateLmtContRelZcc(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtContRelService.updateLmtContRelZcc(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 4.4、银票合同/银票合同申请：更新中间表业务的申请状态和审批状态
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatelmtcontrelypht/{cusId}")
    protected ResultDto<Integer> updateLmtContRelYpht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtContRelService.updateLmtContRelYpht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 4.5、保函合同/保函合同申请：更新中间表业务的申请状态和审批状态
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatelmtcontrelbhht/{cusId}")
    protected ResultDto<Integer> updateLmtContRelBhht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtContRelService.updateLmtContRelBhht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 4.6、信用证合同/信用证合同申请：更新中间表业务的申请状态和审批状态
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatelmtcontrelxyzht/{cusId}")
    protected ResultDto<Integer> updateLmtContRelXyzht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtContRelService.updateLmtContRelXyzht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 4.7、贴现合同/贴现合同申请：更新中间表业务的申请状态和审批状态
     *
     * @param cusId
     * @return
     */
    @PostMapping("/updatelmtcontreltxht/{cusId}")
    protected ResultDto<Integer> updateLmtContRelTxht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtContRelService.updateLmtContRelTxht(cusId);
        return new ResultDto<Integer>(result);
    }

    //------------------------ 4.LmtCheckUpdateLmtContRel数据更新额度校正-合同临时表-更新占用总金额与敞口金额 结束-------------------------

    //------------------------ 5.LmtCheckUpdateOutStandAmtBN数据更新额度校正-合同临时表-表内占用总余额与敞口余额 开始-------------------------
    /**
     * 5.1、未到期的最高额贷款合同（及在途）占用额度  占用总余额  = 合同金额 ; 占用敞口 = 非低风险为合同金额、低风险为0
     */
    @PostMapping("/insertupdateoutstandamtbnzgedkht/{cusId}")
    protected ResultDto<Integer> insertUpdateOutStandAmtBNZgedkht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBNService.insertUpdateOutStandAmtBNZgedkht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 5.2、未到期的最高额委托贷款合同（及在途）占用额度：lmt_cont_rel表中 占用总余额 = 合同金额; 占用敞口 = 非低风险为合同金额、低风险为0
     */
    @PostMapping("/insertupdateoutstandamtbnzgewtdkht/{cusId}")
    protected ResultDto<Integer> insertUpdateOutStandAmtBNZgewtdkht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBNService.insertUpdateOutStandAmtBNZgewtdkht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 5.3、未到期的一般贷款合同（及在途）占用额度：lmt_cont_rel表中  占用总余额 = 合同金额 - 已发放贷款金额 + 已发放贷款余额; 占用敞口 = 非低风险为占用总余额、低风险为0
     */
    @PostMapping("/insertupdateoutstandamtbnybdkht/{cusId}")
    protected ResultDto<Integer> insertUpdateOutStandAmtBNYbdkht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBNService.insertUpdateOutStandAmtBNYbdkht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 5.4、未到期的一般委托贷款合同（及在途）占用额度：lmt_cont_rel表中 占用总余额 = 合同金额 - 已发放贷款金额 + 已发放贷款余额；占用敞口 = 非低风险为占用总余额、低风险为0
     */
    @PostMapping("/insertupdateoutstandamtbnybwtdkht/{cusId}")
    protected ResultDto<Integer> insertUpdateOutStandAmtBNYbwtdkht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBNService.insertUpdateOutStandAmtBNYbwtdkht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 5.5、未到期的最高额授信协议（及在途）占用额度：lmt_cont_rel表中  占用总余额 = 协议最高可用信金额；占用敞口 = 非低风险为协议最高可用信金额、低风险为0
     */
    @PostMapping("/insertupdateoutstandamtbnzgsxxy/{cusId}")
    protected ResultDto<Integer> insertUpdateOutStandAmtBNZgsxxy(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBNService.insertUpdateOutStandAmtBNZgsxxy(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 5.6、未到期的最高额资产池协议（及在途）占用额度
     */
    @PostMapping("/insertupdateoutstandamtbnzgezccxy/{cusId}")
    protected ResultDto<Integer> insertUpdateOutStandAmtBNZgezccxy(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBNService.insertUpdateOutStandAmtBNZgezccxy(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 5.7、 已到期 ：用信申请下的台账余额
     * 包含：最高额授信协议、贷款合同、银票合同、保函协议、信用证合同、银票合同、资产池合同、贴现协议
     */
    @PostMapping("/insertupdateoutstandamtbnbelongyx/{cusId}")
    protected ResultDto<Integer> insertUpdateOutStandAmtBNBelongYx(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBNService.insertUpdateOutStandAmtBNBelongYx(cusId);
        return new ResultDto<Integer>(result);
    }
    //------------------------ 5.LmtCheckUpdateOutStandAmtBN数据更新额度校正-合同临时表-表内占用总余额与敞口余额 结束-------------------------

    //------------------------ 6.数据更新额度校正-合同临时表-表外占用总余额与敞口余额 开始-------------------------

    /**
     * 6.1、最高额（未到期）：占用总余额、占用敞口余额； 银票、保函、信用证、贴现
     */
    @PostMapping("/insertoutstandamtbwzge/{cusId}")
    protected ResultDto<Integer> insertOutStandAmtBWZge(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBWService.insertOutStandAmtBWZge(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 6.2、一般合同（未到期）：占用总余额、占用敞口余额； 银票、保函、信用证、贴现
     */
    @PostMapping("/insertoutstandamtbwybht/{cusId}")
    protected ResultDto<Integer> insertOutStandAmtBWYbht(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBWService.insertOutStandAmtBWYbht(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 6.3、更新低风险担保方式占用敞口金额和敞口余额为0
     */
    @PostMapping("/updateoutstandamtbwdfxdb/{cusId}")
    protected ResultDto<Integer> updateOutStandAmtBWDfxdb(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBWService.updateOutStandAmtBWDfxdb(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 6.4、银票贴现占用敞口金额为0
     */
    @PostMapping("/updateoutstandamtbwyptx/{cusId}")
    protected ResultDto<Integer> updateOutStandAmtBWYptx(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateOutStandAmtBWService.updateOutStandAmtBWYptx(cusId);
        return new ResultDto<Integer>(result);
    }
    //------------------------ 6.数据更新额度校正-合同临时表-表外占用总余额与敞口余额 结束-------------------------

    //------------------------ 7.LmtCheckUpdateLmtSubUse数据更新额度校正-额度占用处理 开始-------------------------

    /**
     * 7.1、若合同审批状态为在途，则更新占用关系为200-生效
     */
    @PostMapping("/updatelmtsubusehtspzt/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseHtspzt(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseHtspzt(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.2、若合同项下台账还有生效状态的，则更新合同状态为生效
     */
    @PostMapping("/updatelmtsubusehtxtzsx/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseHtxtzsx(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseHtxtzsx(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.3、若合同状态为生效，则更新占用关系状态为生效
     */
    @PostMapping("/updatelmtsubusehtztsx/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseHtztsx(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseHtztsx(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.4、若合同状态为作废，则更新占用关系状态为作废
     */
    @PostMapping("/updatelmtsubusehtztzf/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseHtztzf(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseHtztzf(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.5、若合同状态为注销或中止，项下存在非作废的台账，则更新占用关系状态为 300-注销
     */
    @PostMapping("/updatelmtsubusehtztzxorzzandexistfzftz/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseHtztzxorzzAndExistFzftz(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseHtztzxorzzAndExistFzftz(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.6、若合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     */
    @PostMapping("/updatelmtsubusehtztcancelorstopandnoexistfzftz/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseHtztCancelOrStopAndNoExistFzftz(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseHtztCancelOrStopAndNoExistFzftz(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.7、合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     */
    @PostMapping("/updatelmtsubusehtztcancelorstoporactiveorend/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseHtztCancelOrStopOrActiveOrEnd(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseHtztCancelOrStopOrActiveOrEnd(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.8、将计算后的合同占用关系 更新至  正式表
     */
    @PostMapping("/updatelmtsubusecalculatetoformaltable1/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseCalculateToFormalTable1(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseCalculateToFormalTable1(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.9、将计算后的合同占用关系 更新至  正式表
     */
    @PostMapping("/updatelmtsubusecalculatetoformaltable2/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseCalculateToFormalTable2(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseCalculateToFormalTable2(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.10、将计算后的台账占用关系 更新至  正式表（台账占用额度）
     */
    @PostMapping("/updatelmtsubusetzzytoformal/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseTzzyToFormal(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseTzzyToFormal(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.11、根据占用关系更新分项 总额已用、敞口已用
     */
    @PostMapping("/updatelmtsubusezeyyandckyy/{cusId}")
    protected ResultDto<Integer> updateLmtSubUseZeyyAndCkyy(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUseZeyyAndCkyy(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 7.12、二级分项占额需要汇总至一级分项
     */
    @PostMapping("/updatelmtsubuse2levelzeto1level/{cusId}")
    protected ResultDto<Integer> updateLmtSubUse2LevelZeTo1Level(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtSubUseService.updateLmtSubUse2LevelZeTo1Level(cusId);
        return new ResultDto<Integer>(result);
    }

    //------------------------ 7.LmtCheckUpdateLmtSubUse数据更新额度校正-额度占用处理 结束-------------------------

    //------------------------ 8.LmtCheckUpdateLoanBalance数据更新额度校正-计算分项用信金额 开始-------------------------

    /**
     * 8.1、计算分项的用信金额
     */
    @PostMapping("/calculatesublmtamt/{cusId}")
    protected ResultDto<Integer> calculateSubLmtAmt(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLoanBalanceService.calculateSubLmtAmt(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 8.2、一级分项用信余额汇总至二级分项
     *  二级分项项下的用信台账余额之和 + 分项项下关联的非最高额授信协议的合同下的用信台账余额之和 + 分项项下关联的用信台账余额之和
     */
    @PostMapping("/collect1levelsublmtamtto2levelsub/{cusId}")
    protected ResultDto<Integer> collect1LevelSubLmtAmtTo2LevelSub(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLoanBalanceService.collect1LevelSubLmtAmtTo2LevelSub(cusId);
        return new ResultDto<Integer>(result);
    }
    //------------------------ 8.LmtCheckUpdateLoanBalance数据更新额度校正-计算分项用信金额 结束-------------------------

    //------------------------ 9.LmtCheckUpdatePvpOutAmt数据更新额度校正-计算分项可出账金额 开始-------------------------
    /**
     * 9.1、计算分项已出账金额、可出账金额
     */
    @PostMapping("/calculatepvpoutamtalreadyandcanoutamt/{cusId}")
    protected ResultDto<Integer> calculatePvpOutAmtAlreadyAndCanOutAmt(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdatePvpOutAmtService.calculatePvpOutAmtAlreadyAndCanOutAmt(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 9.2、二级分项已出账汇总至一级分项
     */
    @PostMapping("/collectpvpoutamt2levelsuboutamtto1levelsub/{cusId}")
    protected ResultDto<Integer> collectPvpOutAmt2LevelSubOutAmtTo1LevelSub(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdatePvpOutAmtService.collectPvpOutAmt2LevelSubOutAmtTo1LevelSub(cusId);
        return new ResultDto<Integer>(result);
    }
    //------------------------ 9.LmtCheckUpdatePvpOutAmt数据更新额度校正-计算分项可出账金额 结束-------------------------

    //------------------------ 10.LmtCheckUpdateLmtAddAmt数据更新额度校正-计算分项授信总额累加 开始-------------------------
    /**
     * 10.1、更新授信总额累加为0
     */
    @PostMapping("/updatelmtaddamtto0/")
    protected ResultDto<Integer> updateLmtAddAmtTo0(@PathVariable("cusId") String cusId) {
        int result = lmtCheckUpdateLmtAddAmtService.updateLmtAddAmtTo0(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 10.2、加工授信总额累加
     */
    @PostMapping("/insertlmtaddamt/{cusId}")
    protected ResultDto<Integer> insertLmtAddAmt(@PathVariable("cusId") String cusId) {
        checkParamIsExist("cusId",cusId);
        int result = lmtCheckUpdateLmtAddAmtService.insertLmtAddAmt(cusId);
        return new ResultDto<Integer>(result);
    }
    //------------------------ 10.LmtCheckUpdateLmtAddAmt数据更新额度校正-计算分项授信总额累加 结束-------------------------

    public void checkParamIsExist(String key,String val){
        if (StringUtils.isBlank(val) || "null".equals(val)){
            throw BizException.error(null,"9999",key+"值不能为空！");
        }
    }
}
