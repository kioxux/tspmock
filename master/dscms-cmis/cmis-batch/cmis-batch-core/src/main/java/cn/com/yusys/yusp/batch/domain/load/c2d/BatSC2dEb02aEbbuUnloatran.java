/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.c2d;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dEb02aEbbuUnloatran
 * @类描述: bat_s_c2d_eb02a_ebbu_unloatran数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:02:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_c2d_eb02a_ebbu_unloatran")
public class BatSC2dEb02aEbbuUnloatran extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** ID **/
	@Id
	@Column(name = "ID")
	private String id;
	
	/** 报告编号 **/
	@Id
	@Column(name = "REPORT_ID")
	private String reportId;
	
	/** 资产处置业务账户数 **/
	@Column(name = "ASS_DISP_BUS_ACC_NUM", unique = false, nullable = true, length = 8)
	private java.math.BigDecimal assDispBusAccNum;
	
	/** 资产处置业务余额 **/
	@Column(name = "ASS_DISP_BUS_BLANCE", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal assDispBusBlance;
	
	/** 最近一次处置日期 **/
	@Column(name = "LAST_TIME_DISP_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date lastTimeDispDate;
	
	/** 垫款业务账户数 **/
	@Column(name = "ADVANCE_BUS_ACC_NUM", unique = false, nullable = true, length = 8)
	private java.math.BigDecimal advanceBusAccNum;
	
	/** 垫款业务余额 **/
	@Column(name = "ADVANCE_BUS_BLANCE", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal advanceBusBlance;
	
	/** 垫款最近一次还款日期 **/
	@Column(name = "ADVAN_LAST_REPAY_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date advanLastRepayDate;
	
	/** 逾期总额 **/
	@Column(name = "OVERDUE_TOTAL", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal overdueTotal;
	
	/** 逾期本金 **/
	@Column(name = "OVERDUE_PRINC", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal overduePrinc;
	
	/** 逾期利息及其他 **/
	@Column(name = "OVERDUE_INTER_AND_OTHER", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal overdueInterAndOther;
	
	/** 其他借贷交易分类汇总条目数量 **/
	@Column(name = "OTHER_LOAN_TRANS_SUM_NUM", unique = false, nullable = true, length = 8)
	private java.math.BigDecimal otherLoanTransSumNum;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param reportId
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	
    /**
     * @return reportId
     */
	public String getReportId() {
		return this.reportId;
	}
	
	/**
	 * @param assDispBusAccNum
	 */
	public void setAssDispBusAccNum(java.math.BigDecimal assDispBusAccNum) {
		this.assDispBusAccNum = assDispBusAccNum;
	}
	
    /**
     * @return assDispBusAccNum
     */
	public java.math.BigDecimal getAssDispBusAccNum() {
		return this.assDispBusAccNum;
	}
	
	/**
	 * @param assDispBusBlance
	 */
	public void setAssDispBusBlance(java.math.BigDecimal assDispBusBlance) {
		this.assDispBusBlance = assDispBusBlance;
	}
	
    /**
     * @return assDispBusBlance
     */
	public java.math.BigDecimal getAssDispBusBlance() {
		return this.assDispBusBlance;
	}
	
	/**
	 * @param lastTimeDispDate
	 */
	public void setLastTimeDispDate(java.util.Date lastTimeDispDate) {
		this.lastTimeDispDate = lastTimeDispDate;
	}
	
    /**
     * @return lastTimeDispDate
     */
	public java.util.Date getLastTimeDispDate() {
		return this.lastTimeDispDate;
	}
	
	/**
	 * @param advanceBusAccNum
	 */
	public void setAdvanceBusAccNum(java.math.BigDecimal advanceBusAccNum) {
		this.advanceBusAccNum = advanceBusAccNum;
	}
	
    /**
     * @return advanceBusAccNum
     */
	public java.math.BigDecimal getAdvanceBusAccNum() {
		return this.advanceBusAccNum;
	}
	
	/**
	 * @param advanceBusBlance
	 */
	public void setAdvanceBusBlance(java.math.BigDecimal advanceBusBlance) {
		this.advanceBusBlance = advanceBusBlance;
	}
	
    /**
     * @return advanceBusBlance
     */
	public java.math.BigDecimal getAdvanceBusBlance() {
		return this.advanceBusBlance;
	}
	
	/**
	 * @param advanLastRepayDate
	 */
	public void setAdvanLastRepayDate(java.util.Date advanLastRepayDate) {
		this.advanLastRepayDate = advanLastRepayDate;
	}
	
    /**
     * @return advanLastRepayDate
     */
	public java.util.Date getAdvanLastRepayDate() {
		return this.advanLastRepayDate;
	}
	
	/**
	 * @param overdueTotal
	 */
	public void setOverdueTotal(java.math.BigDecimal overdueTotal) {
		this.overdueTotal = overdueTotal;
	}
	
    /**
     * @return overdueTotal
     */
	public java.math.BigDecimal getOverdueTotal() {
		return this.overdueTotal;
	}
	
	/**
	 * @param overduePrinc
	 */
	public void setOverduePrinc(java.math.BigDecimal overduePrinc) {
		this.overduePrinc = overduePrinc;
	}
	
    /**
     * @return overduePrinc
     */
	public java.math.BigDecimal getOverduePrinc() {
		return this.overduePrinc;
	}
	
	/**
	 * @param overdueInterAndOther
	 */
	public void setOverdueInterAndOther(java.math.BigDecimal overdueInterAndOther) {
		this.overdueInterAndOther = overdueInterAndOther;
	}
	
    /**
     * @return overdueInterAndOther
     */
	public java.math.BigDecimal getOverdueInterAndOther() {
		return this.overdueInterAndOther;
	}
	
	/**
	 * @param otherLoanTransSumNum
	 */
	public void setOtherLoanTransSumNum(java.math.BigDecimal otherLoanTransSumNum) {
		this.otherLoanTransSumNum = otherLoanTransSumNum;
	}
	
    /**
     * @return otherLoanTransSumNum
     */
	public java.math.BigDecimal getOtherLoanTransSumNum() {
		return this.otherLoanTransSumNum;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}