/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.check;

import org.apache.ibatis.annotations.Param;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateLmtContRelMapper
 * @类描述: #Dao类
 * @功能描述: 数据更新额度校正-计算分项授信总额累加
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCheckUpdateLmtContRelMapper {

    /**
     * 4.1、贷款合同/贷款合同申请：更新中间表业务的申请状态和审批状态
     */
    int updateLmtContRelDkht_1(@Param("cusId") String cusId);

    int updateLmtContRelDkht_2(@Param("cusId") String cusId);

    int updateLmtContRelDkht_3(@Param("cusId") String cusId);


    /**
     * 4.2、委托贷款合同/委托贷款合同申请：更新中间表业务的申请状态和审批状态
     */
    int updateLmtContRelWtdk_1(@Param("cusId") String cusId);

    int updateLmtContRelWtdk_2(@Param("cusId") String cusId);

    int updateLmtContRelWtdk_3(@Param("cusId") String cusId);


    /**
     * 4.2、最高额授信协议/最高额授信协议申请：更新中间表业务的申请状态和审批状态
     */
    int updateLmtContRelZgesxxy_1(@Param("cusId") String cusId);

    int updateLmtContRelZgesxxy_2(@Param("cusId") String cusId);

    int updateLmtContRelZgesxxy_3(@Param("cusId") String cusId);

    /**
     * 4.3、资产池协议/资产池协议申请：更新中间表业务的申请状态和审批状态
     */
    int updateLmtContRelZcc_1(@Param("cusId") String cusId);

    int updateLmtContRelZcc_2(@Param("cusId") String cusId);

    int updateLmtContRelZcc_3(@Param("cusId") String cusId);

    /**
     * 4.4、银票合同/银票合同申请：更新中间表业务的申请状态和审批状态
     */
    int updateLmtContRelYpht_1(@Param("cusId") String cusId);

    int updateLmtContRelYpht_2(@Param("cusId") String cusId);

    int updateLmtContRelYpht_3(@Param("cusId") String cusId);

    /**
     *  4.5、保函合同/保函合同申请：更新中间表业务的申请状态和审批状态
     */
    int updateLmtContRelBhht_1(@Param("cusId") String cusId);

    int updateLmtContRelBhht_2(@Param("cusId") String cusId);

    int updateLmtContRelBhht_3(@Param("cusId") String cusId);

    /**
     * 4.6、信用证合同/信用证合同申请：更新中间表业务的申请状态和审批状态
     */
    int updateLmtContRelXyzht_1(@Param("cusId") String cusId);

    int updateLmtContRelXyzht_2(@Param("cusId") String cusId);

    int updateLmtContRelXyzht_3(@Param("cusId") String cusId);

    /**
     * 4.7、贴现合同/贴现合同申请：更新中间表业务的申请状态和审批状态
     */
    int updateLmtContRelTxht_1(@Param("cusId") String cusId);

    int updateLmtContRelTxht_2(@Param("cusId") String cusId);

    int updateLmtContRelTxht_3(@Param("cusId") String cusId);
}