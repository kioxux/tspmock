/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.fpt;

import cn.com.yusys.yusp.batch.domain.load.fpt.BatTFptRCustBusiSum;
import cn.com.yusys.yusp.batch.repository.mapper.load.fpt.BatTFptRCustBusiSumMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTFptRCustBusiSumService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-12 10:11:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTFptRCustBusiSumService {

    @Autowired
    private BatTFptRCustBusiSumMapper batTFptRCustBusiSumMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatTFptRCustBusiSum selectByPrimaryKey(String custId) {
        return batTFptRCustBusiSumMapper.selectByPrimaryKey(custId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatTFptRCustBusiSum> selectAll(QueryModel model) {
        List<BatTFptRCustBusiSum> records = (List<BatTFptRCustBusiSum>) batTFptRCustBusiSumMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatTFptRCustBusiSum> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTFptRCustBusiSum> list = batTFptRCustBusiSumMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatTFptRCustBusiSum record) {
        return batTFptRCustBusiSumMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatTFptRCustBusiSum record) {
        return batTFptRCustBusiSumMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatTFptRCustBusiSum record) {
        return batTFptRCustBusiSumMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatTFptRCustBusiSum record) {
        return batTFptRCustBusiSumMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String custId) {
        return batTFptRCustBusiSumMapper.deleteByPrimaryKey(custId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batTFptRCustBusiSumMapper.deleteByIds(ids);
    }

    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTFptRCustBusiSumMapper.truncateTTable();
    }
}
