package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-业务处理- </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0102Mapper {
    /**
     * 清空 国结保函业务发信贷台账
     */
    void truncateTmpGjpAccCvrg();

    /**
     * 插入国结保函业务发信贷台账
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpAccCvrg(@Param("openDay") String openDay);

    /**
     * 清空国结保证金台账
     */
    void truncateTmpGjpSecurityAmt();

    /**
     * 插入国结保证金台账
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpSecurityAmt(@Param("openDay") String openDay);

    /**
     * 更新保函台账原始敞口金额
     *
     * @param openDay
     * @return
     */
    int cmis0102UpdateAccCvrs01(@Param("openDay") String openDay);

    /**
     * 更新保函台账
     *
     * @param openDay
     * @return
     */
    int cmis0102UpdateAccCvrs02(@Param("openDay") String openDay);
}
