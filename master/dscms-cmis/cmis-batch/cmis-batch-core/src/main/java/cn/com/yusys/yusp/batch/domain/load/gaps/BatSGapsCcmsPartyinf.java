/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.gaps;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGapsCcmsPartyinf
 * @类描述: bat_s_gaps_ccms_partyinf数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 14:06:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_gaps_ccms_partyinf")
public class BatSGapsCcmsPartyinf extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 参与机构行号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "prcptcd")
	private String prcptcd;
	
	/** 参与机构类别 **/
	@Column(name = "prcpttp", unique = false, nullable = true, length = 2)
	private String prcpttp;
	
	/** 行别代码 **/
	@Column(name = "bkctgrycd", unique = false, nullable = true, length = 3)
	private String bkctgrycd;
	
	/** 所属清算行行号 **/
	@Column(name = "subdrtbkcd", unique = false, nullable = true, length = 14)
	private String subdrtbkcd;
	
	/** 所属法人 **/
	@Column(name = "sublglprsn", unique = false, nullable = true, length = 35)
	private String sublglprsn;
	
	/** 本行上级参与机构 **/
	@Column(name = "hgrptcpt", unique = false, nullable = true, length = 14)
	private String hgrptcpt;
	
	/** 承接行行号 **/
	@Column(name = "barbkcd", unique = false, nullable = true, length = 255)
	private String barbkcd;
	
	/** 管辖人行行号 **/
	@Column(name = "chrgbkcd", unique = false, nullable = true, length = 14)
	private String chrgbkcd;
	
	/** 所属CCPC **/
	@Column(name = "subccpccd", unique = false, nullable = true, length = 4)
	private String subccpccd;
	
	/** 所在城市代码 **/
	@Column(name = "citycd", unique = false, nullable = true, length = 6)
	private String citycd;
	
	/** 参与机构全称 **/
	@Column(name = "ptcptnm", unique = false, nullable = true, length = 120)
	private String ptcptnm;
	
	/** 参与机构简称 **/
	@Column(name = "ptcptshrtnm", unique = false, nullable = true, length = 70)
	private String ptcptshrtnm;
	
	/** 加入业务系统标识 **/
	@Column(name = "jntxsyssgn", unique = false, nullable = true, length = 10)
	private String jntxsyssgn;
	
	/** 地址 **/
	@Column(name = "adr", unique = false, nullable = true, length = 512)
	private String adr;
	
	/** 邮编 **/
	@Column(name = "pstcd", unique = false, nullable = true, length = 6)
	private String pstcd;
	
	/** 电话 **/
	@Column(name = "tel", unique = false, nullable = true, length = 32)
	private String tel;
	
	/** 电子邮件地址 **/
	@Column(name = "email", unique = false, nullable = true, length = 120)
	private String email;
	
	/** 注销日期 **/
	@Column(name = "expdate", unique = false, nullable = true, length = 8)
	private String expdate;
	
	/** 备注 **/
	@Column(name = "rmkinf", unique = false, nullable = true, length = 210)
	private String rmkinf;
	
	/** 更新日期 **/
	@Column(name = "upddt", unique = false, nullable = true, length = 8)
	private String upddt;
	
	/** 更新时间 **/
	@Column(name = "updtm", unique = false, nullable = true, length = 8)
	private String updtm;
	
	/** 备用字段1 **/
	@Column(name = "mkinfo1", unique = false, nullable = true, length = 4)
	private String mkinfo1;
	
	/** 备用字段2 **/
	@Column(name = "mkinfo2", unique = false, nullable = true, length = 10)
	private String mkinfo2;
	
	/** 创建时间 **/
	@Column(name = "createts", unique = false, nullable = true, length = 19)
	private java.util.Date createts;
	
	/** 更新时间2 **/
	@Column(name = "updatets", unique = false, nullable = true, length = 19)
	private java.util.Date updatets;
	
	
	/**
	 * @param prcptcd
	 */
	public void setPrcptcd(String prcptcd) {
		this.prcptcd = prcptcd;
	}
	
    /**
     * @return prcptcd
     */
	public String getPrcptcd() {
		return this.prcptcd;
	}
	
	/**
	 * @param prcpttp
	 */
	public void setPrcpttp(String prcpttp) {
		this.prcpttp = prcpttp;
	}
	
    /**
     * @return prcpttp
     */
	public String getPrcpttp() {
		return this.prcpttp;
	}
	
	/**
	 * @param bkctgrycd
	 */
	public void setBkctgrycd(String bkctgrycd) {
		this.bkctgrycd = bkctgrycd;
	}
	
    /**
     * @return bkctgrycd
     */
	public String getBkctgrycd() {
		return this.bkctgrycd;
	}
	
	/**
	 * @param subdrtbkcd
	 */
	public void setSubdrtbkcd(String subdrtbkcd) {
		this.subdrtbkcd = subdrtbkcd;
	}
	
    /**
     * @return subdrtbkcd
     */
	public String getSubdrtbkcd() {
		return this.subdrtbkcd;
	}
	
	/**
	 * @param sublglprsn
	 */
	public void setSublglprsn(String sublglprsn) {
		this.sublglprsn = sublglprsn;
	}
	
    /**
     * @return sublglprsn
     */
	public String getSublglprsn() {
		return this.sublglprsn;
	}
	
	/**
	 * @param hgrptcpt
	 */
	public void setHgrptcpt(String hgrptcpt) {
		this.hgrptcpt = hgrptcpt;
	}
	
    /**
     * @return hgrptcpt
     */
	public String getHgrptcpt() {
		return this.hgrptcpt;
	}
	
	/**
	 * @param barbkcd
	 */
	public void setBarbkcd(String barbkcd) {
		this.barbkcd = barbkcd;
	}
	
    /**
     * @return barbkcd
     */
	public String getBarbkcd() {
		return this.barbkcd;
	}
	
	/**
	 * @param chrgbkcd
	 */
	public void setChrgbkcd(String chrgbkcd) {
		this.chrgbkcd = chrgbkcd;
	}
	
    /**
     * @return chrgbkcd
     */
	public String getChrgbkcd() {
		return this.chrgbkcd;
	}
	
	/**
	 * @param subccpccd
	 */
	public void setSubccpccd(String subccpccd) {
		this.subccpccd = subccpccd;
	}
	
    /**
     * @return subccpccd
     */
	public String getSubccpccd() {
		return this.subccpccd;
	}
	
	/**
	 * @param citycd
	 */
	public void setCitycd(String citycd) {
		this.citycd = citycd;
	}
	
    /**
     * @return citycd
     */
	public String getCitycd() {
		return this.citycd;
	}
	
	/**
	 * @param ptcptnm
	 */
	public void setPtcptnm(String ptcptnm) {
		this.ptcptnm = ptcptnm;
	}
	
    /**
     * @return ptcptnm
     */
	public String getPtcptnm() {
		return this.ptcptnm;
	}
	
	/**
	 * @param ptcptshrtnm
	 */
	public void setPtcptshrtnm(String ptcptshrtnm) {
		this.ptcptshrtnm = ptcptshrtnm;
	}
	
    /**
     * @return ptcptshrtnm
     */
	public String getPtcptshrtnm() {
		return this.ptcptshrtnm;
	}
	
	/**
	 * @param jntxsyssgn
	 */
	public void setJntxsyssgn(String jntxsyssgn) {
		this.jntxsyssgn = jntxsyssgn;
	}
	
    /**
     * @return jntxsyssgn
     */
	public String getJntxsyssgn() {
		return this.jntxsyssgn;
	}
	
	/**
	 * @param adr
	 */
	public void setAdr(String adr) {
		this.adr = adr;
	}
	
    /**
     * @return adr
     */
	public String getAdr() {
		return this.adr;
	}
	
	/**
	 * @param pstcd
	 */
	public void setPstcd(String pstcd) {
		this.pstcd = pstcd;
	}
	
    /**
     * @return pstcd
     */
	public String getPstcd() {
		return this.pstcd;
	}
	
	/**
	 * @param tel
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}
	
    /**
     * @return tel
     */
	public String getTel() {
		return this.tel;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
    /**
     * @return email
     */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param expdate
	 */
	public void setExpdate(String expdate) {
		this.expdate = expdate;
	}
	
    /**
     * @return expdate
     */
	public String getExpdate() {
		return this.expdate;
	}
	
	/**
	 * @param rmkinf
	 */
	public void setRmkinf(String rmkinf) {
		this.rmkinf = rmkinf;
	}
	
    /**
     * @return rmkinf
     */
	public String getRmkinf() {
		return this.rmkinf;
	}
	
	/**
	 * @param upddt
	 */
	public void setUpddt(String upddt) {
		this.upddt = upddt;
	}
	
    /**
     * @return upddt
     */
	public String getUpddt() {
		return this.upddt;
	}
	
	/**
	 * @param updtm
	 */
	public void setUpdtm(String updtm) {
		this.updtm = updtm;
	}
	
    /**
     * @return updtm
     */
	public String getUpdtm() {
		return this.updtm;
	}
	
	/**
	 * @param mkinfo1
	 */
	public void setMkinfo1(String mkinfo1) {
		this.mkinfo1 = mkinfo1;
	}
	
    /**
     * @return mkinfo1
     */
	public String getMkinfo1() {
		return this.mkinfo1;
	}
	
	/**
	 * @param mkinfo2
	 */
	public void setMkinfo2(String mkinfo2) {
		this.mkinfo2 = mkinfo2;
	}
	
    /**
     * @return mkinfo2
     */
	public String getMkinfo2() {
		return this.mkinfo2;
	}
	
	/**
	 * @param createts
	 */
	public void setCreatets(java.util.Date createts) {
		this.createts = createts;
	}
	
    /**
     * @return createts
     */
	public java.util.Date getCreatets() {
		return this.createts;
	}
	
	/**
	 * @param updatets
	 */
	public void setUpdatets(java.util.Date updatets) {
		this.updatets = updatets;
	}
	
    /**
     * @return updatets
     */
	public java.util.Date getUpdatets() {
		return this.updatets;
	}


}