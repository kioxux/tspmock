package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0151Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0151</br>
 * 任务名称：加工任务-业务处理-网金数据初始化到历史表 </br>
 *
 * @author sunzhe
 * @version 1.0
 * @since 2021年10月05日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0151Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0151Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0151Service cmis0151Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理

    @Bean
    public Job cmis0151Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0151_JOB.key, JobStepEnum.CMIS0151_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0151Job = this.jobBuilderFactory.get(JobStepEnum.CMIS0151_JOB.key)
                .start(cmis0151UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0151InsertCtrLoanCont03Step(WILL_BE_INJECTED)) // 插入和更新贷款台账和合同表03
                .next(cmis0151InsertCtrLoanCont04Step(WILL_BE_INJECTED)) // 插入和更新贷款台账和合同表04
                .next(cmis0151UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0151Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0151UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0151_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0151_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0151UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0151_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0151_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0151_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0151_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0151UpdateTask010Step;
    }

    /**
     * 插入和更新贷款台账和合同表03
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0151InsertCtrLoanCont03Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0151_INSERT_CTR_LOAN_CONT03_STEP.key, JobStepEnum.CMIS0151_INSERT_CTR_LOAN_CONT03_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0151InsertCtrLoanCont03Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0151_INSERT_CTR_LOAN_CONT03_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0151Service.cmis0151InsertWjCtrLoanContHist03(openDay);//插入贷款合同历史表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0151_INSERT_CTR_LOAN_CONT03_STEP.key, JobStepEnum.CMIS0151_INSERT_CTR_LOAN_CONT03_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0151InsertCtrLoanCont03Step;
    }

    /**
     * 插入和更新贷款台账和合同表04
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0151InsertCtrLoanCont04Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0151_INSERT_CTR_LOAN_CONT04_STEP.key, JobStepEnum.CMIS0151_INSERT_CTR_LOAN_CONT04_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0151InsertCtrLoanCont01Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0151_INSERT_CTR_LOAN_CONT04_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0151Service.cmis0151InsertWjAccLoanHist04(openDay);//插入贷款台账历史表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0151_INSERT_CTR_LOAN_CONT04_STEP.key, JobStepEnum.CMIS0151_INSERT_CTR_LOAN_CONT04_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0151InsertCtrLoanCont01Step;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0151UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0151_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0151_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0151UpdateTask100Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0151_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0151_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0151_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0151_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0151UpdateTask100Step;
    }


}
