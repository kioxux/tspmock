/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKitpKdlldm;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatSCoreKitpKdlldmMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKitpKdlldmService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSCoreKitpKdlldmService {
    private static final Logger logger = LoggerFactory.getLogger(BatSCoreKitpKdlldmService.class);
    @Autowired
    private BatSCoreKitpKdlldmMapper batSCoreKitpKdlldmMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSCoreKitpKdlldm selectByPrimaryKey(String farendma, String fenhdaim, String jigouhao, String qiciiiii, String lilvbhlx, String lilvdama, String bizhongg, Long cunqitsh, String cunqiiii, String fdzhouqi, java.math.BigDecimal lilvdanc, String shengxrq) {
        return batSCoreKitpKdlldmMapper.selectByPrimaryKey(farendma, fenhdaim, jigouhao, qiciiiii, lilvbhlx, lilvdama, bizhongg, cunqitsh, cunqiiii, fdzhouqi, lilvdanc, shengxrq);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSCoreKitpKdlldm> selectAll(QueryModel model) {
        List<BatSCoreKitpKdlldm> records = (List<BatSCoreKitpKdlldm>) batSCoreKitpKdlldmMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSCoreKitpKdlldm> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSCoreKitpKdlldm> list = batSCoreKitpKdlldmMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSCoreKitpKdlldm record) {
        return batSCoreKitpKdlldmMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSCoreKitpKdlldm record) {
        return batSCoreKitpKdlldmMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSCoreKitpKdlldm record) {
        return batSCoreKitpKdlldmMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSCoreKitpKdlldm record) {
        return batSCoreKitpKdlldmMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String farendma, String fenhdaim, String jigouhao, String qiciiiii, String lilvbhlx, String lilvdama, String bizhongg, Long cunqitsh, String cunqiiii, String fdzhouqi, java.math.BigDecimal lilvdanc, String shengxrq) {
        return batSCoreKitpKdlldmMapper.deleteByPrimaryKey(farendma, fenhdaim, jigouhao, qiciiiii, lilvbhlx, lilvdama, bizhongg, cunqitsh, cunqiiii, fdzhouqi, lilvdanc, shengxrq);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建靠档利率表[bat_s_core_kitp_kdlldm]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_core_kitp_kdlldm");
        logger.info("重建【CORE0014】靠档利率表[bat_s_core_kitp_kdlldm]结束");
        return 0;
    }


    /**
     * 将T表数据merge到S表
     *
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]开始,请求参数为:[{}]", openDay);
        int updateSResult = batSCoreKitpKdlldmMapper.updateDataDateByOpenDay(openDay);
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]结束,返回参数为:[{}]", updateSResult);
    }
}
