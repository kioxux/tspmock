/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKlnbDkkjsx
 * @类描述: bat_s_core_klnb_dkkjsx数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_core_klnb_dkkjsx")
public class BatSCoreKlnbDkkjsx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 法人代码 **/
	@Id
	@Column(name = "farendma")
	private String farendma;
	
	/** 贷款借据号 **/
	@Id
	@Column(name = "dkjiejuh")
	private String dkjiejuh;
	
	/** 按应计非应计核算 STD_YESORNO **/
	@Column(name = "yjfyjhes", unique = false, nullable = true, length = 1)
	private String yjfyjhes;
	
	/** 按一逾两呆核算 STD_YESORNO **/
	@Column(name = "yiyldhes", unique = false, nullable = true, length = 1)
	private String yiyldhes;
	
	/** 按贷款形态分科目核算 STD_YESORNO **/
	@Column(name = "dkxtkmhs", unique = false, nullable = true, length = 1)
	private String dkxtkmhs;
	
	/** 自动形态转移 STD_YESORNO **/
	@Column(name = "zidxtzhy", unique = false, nullable = true, length = 1)
	private String zidxtzhy;
	
	/** 利息转出规则 STD_LIXIZCGZ **/
	@Column(name = "lixizcgz", unique = false, nullable = true, length = 1)
	private String lixizcgz;
	
	/** 利息转回规则 STD_LIXIZHGZ **/
	@Column(name = "lixizhgz", unique = false, nullable = true, length = 1)
	private String lixizhgz;
	
	/** 核算方式 STD_HESUANFS **/
	@Column(name = "hesuanfs", unique = false, nullable = true, length = 1)
	private String hesuanfs;
	
	/** 表外核算码 **/
	@Column(name = "bwhesdma", unique = false, nullable = true, length = 30)
	private String bwhesdma;
	
	/** 业务属性1 **/
	@Column(name = "yewusx01", unique = false, nullable = true, length = 32)
	private String yewusx01;
	
	/** 业务属性描述1 **/
	@Column(name = "ywsxms01", unique = false, nullable = true, length = 80)
	private String ywsxms01;
	
	/** 业务属性2 **/
	@Column(name = "yewusx02", unique = false, nullable = true, length = 32)
	private String yewusx02;
	
	/** 业务属性描述2 **/
	@Column(name = "ywsxms02", unique = false, nullable = true, length = 80)
	private String ywsxms02;
	
	/** 业务属性3 **/
	@Column(name = "yewusx03", unique = false, nullable = true, length = 32)
	private String yewusx03;
	
	/** 业务属性描述3 **/
	@Column(name = "ywsxms03", unique = false, nullable = true, length = 80)
	private String ywsxms03;
	
	/** 业务属性4 **/
	@Column(name = "yewusx04", unique = false, nullable = true, length = 32)
	private String yewusx04;
	
	/** 业务属性描述4 **/
	@Column(name = "ywsxms04", unique = false, nullable = true, length = 80)
	private String ywsxms04;
	
	/** 业务属性5 **/
	@Column(name = "yewusx05", unique = false, nullable = true, length = 32)
	private String yewusx05;
	
	/** 业务属性描述5 **/
	@Column(name = "ywsxms05", unique = false, nullable = true, length = 80)
	private String ywsxms05;
	
	/** 业务属性6 **/
	@Column(name = "yewusx06", unique = false, nullable = true, length = 32)
	private String yewusx06;
	
	/** 业务属性描述6 **/
	@Column(name = "ywsxms06", unique = false, nullable = true, length = 80)
	private String ywsxms06;
	
	/** 业务属性7 **/
	@Column(name = "yewusx07", unique = false, nullable = true, length = 32)
	private String yewusx07;
	
	/** 业务属性描述7 **/
	@Column(name = "ywsxms07", unique = false, nullable = true, length = 80)
	private String ywsxms07;
	
	/** 业务属性8 **/
	@Column(name = "yewusx08", unique = false, nullable = true, length = 32)
	private String yewusx08;
	
	/** 业务属性描述8 **/
	@Column(name = "ywsxms08", unique = false, nullable = true, length = 80)
	private String ywsxms08;
	
	/** 业务属性9 **/
	@Column(name = "yewusx09", unique = false, nullable = true, length = 32)
	private String yewusx09;
	
	/** 业务属性描述9 **/
	@Column(name = "ywsxms09", unique = false, nullable = true, length = 80)
	private String ywsxms09;
	
	/** 业务属性10 **/
	@Column(name = "yewusx10", unique = false, nullable = true, length = 32)
	private String yewusx10;
	
	/** 业务属性描述10 **/
	@Column(name = "ywsxms10", unique = false, nullable = true, length = 80)
	private String ywsxms10;
	
	/** 分行标识 **/
	@Column(name = "fenhbios", unique = false, nullable = false, length = 4)
	private String fenhbios;
	
	/** 维护柜员 **/
	@Column(name = "WEIHGUIY", unique = false, nullable = true, length = 8)
	private String weihguiy;
	
	/** 维护机构 **/
	@Column(name = "weihjigo", unique = false, nullable = false, length = 12)
	private String weihjigo;
	
	/** 维护日期 **/
	@Column(name = "weihriqi", unique = false, nullable = false, length = 8)
	private String weihriqi;
	
	/** 维护时间 **/
	@Column(name = "weihshij", unique = false, nullable = true, length = 9)
	private String weihshij;
	
	/** 时间戳 **/
	@Column(name = "shijchuo", unique = false, nullable = false, length = 19)
	private Long shijchuo;
	
	/** 记录状态 STD_JILUZTAI **/
	@Column(name = "jiluztai", unique = false, nullable = false, length = 1)
	private String jiluztai;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param yjfyjhes
	 */
	public void setYjfyjhes(String yjfyjhes) {
		this.yjfyjhes = yjfyjhes;
	}
	
    /**
     * @return yjfyjhes
     */
	public String getYjfyjhes() {
		return this.yjfyjhes;
	}
	
	/**
	 * @param yiyldhes
	 */
	public void setYiyldhes(String yiyldhes) {
		this.yiyldhes = yiyldhes;
	}
	
    /**
     * @return yiyldhes
     */
	public String getYiyldhes() {
		return this.yiyldhes;
	}
	
	/**
	 * @param dkxtkmhs
	 */
	public void setDkxtkmhs(String dkxtkmhs) {
		this.dkxtkmhs = dkxtkmhs;
	}
	
    /**
     * @return dkxtkmhs
     */
	public String getDkxtkmhs() {
		return this.dkxtkmhs;
	}
	
	/**
	 * @param zidxtzhy
	 */
	public void setZidxtzhy(String zidxtzhy) {
		this.zidxtzhy = zidxtzhy;
	}
	
    /**
     * @return zidxtzhy
     */
	public String getZidxtzhy() {
		return this.zidxtzhy;
	}
	
	/**
	 * @param lixizcgz
	 */
	public void setLixizcgz(String lixizcgz) {
		this.lixizcgz = lixizcgz;
	}
	
    /**
     * @return lixizcgz
     */
	public String getLixizcgz() {
		return this.lixizcgz;
	}
	
	/**
	 * @param lixizhgz
	 */
	public void setLixizhgz(String lixizhgz) {
		this.lixizhgz = lixizhgz;
	}
	
    /**
     * @return lixizhgz
     */
	public String getLixizhgz() {
		return this.lixizhgz;
	}
	
	/**
	 * @param hesuanfs
	 */
	public void setHesuanfs(String hesuanfs) {
		this.hesuanfs = hesuanfs;
	}
	
    /**
     * @return hesuanfs
     */
	public String getHesuanfs() {
		return this.hesuanfs;
	}
	
	/**
	 * @param bwhesdma
	 */
	public void setBwhesdma(String bwhesdma) {
		this.bwhesdma = bwhesdma;
	}
	
    /**
     * @return bwhesdma
     */
	public String getBwhesdma() {
		return this.bwhesdma;
	}
	
	/**
	 * @param yewusx01
	 */
	public void setYewusx01(String yewusx01) {
		this.yewusx01 = yewusx01;
	}
	
    /**
     * @return yewusx01
     */
	public String getYewusx01() {
		return this.yewusx01;
	}
	
	/**
	 * @param ywsxms01
	 */
	public void setYwsxms01(String ywsxms01) {
		this.ywsxms01 = ywsxms01;
	}
	
    /**
     * @return ywsxms01
     */
	public String getYwsxms01() {
		return this.ywsxms01;
	}
	
	/**
	 * @param yewusx02
	 */
	public void setYewusx02(String yewusx02) {
		this.yewusx02 = yewusx02;
	}
	
    /**
     * @return yewusx02
     */
	public String getYewusx02() {
		return this.yewusx02;
	}
	
	/**
	 * @param ywsxms02
	 */
	public void setYwsxms02(String ywsxms02) {
		this.ywsxms02 = ywsxms02;
	}
	
    /**
     * @return ywsxms02
     */
	public String getYwsxms02() {
		return this.ywsxms02;
	}
	
	/**
	 * @param yewusx03
	 */
	public void setYewusx03(String yewusx03) {
		this.yewusx03 = yewusx03;
	}
	
    /**
     * @return yewusx03
     */
	public String getYewusx03() {
		return this.yewusx03;
	}
	
	/**
	 * @param ywsxms03
	 */
	public void setYwsxms03(String ywsxms03) {
		this.ywsxms03 = ywsxms03;
	}
	
    /**
     * @return ywsxms03
     */
	public String getYwsxms03() {
		return this.ywsxms03;
	}
	
	/**
	 * @param yewusx04
	 */
	public void setYewusx04(String yewusx04) {
		this.yewusx04 = yewusx04;
	}
	
    /**
     * @return yewusx04
     */
	public String getYewusx04() {
		return this.yewusx04;
	}
	
	/**
	 * @param ywsxms04
	 */
	public void setYwsxms04(String ywsxms04) {
		this.ywsxms04 = ywsxms04;
	}
	
    /**
     * @return ywsxms04
     */
	public String getYwsxms04() {
		return this.ywsxms04;
	}
	
	/**
	 * @param yewusx05
	 */
	public void setYewusx05(String yewusx05) {
		this.yewusx05 = yewusx05;
	}
	
    /**
     * @return yewusx05
     */
	public String getYewusx05() {
		return this.yewusx05;
	}
	
	/**
	 * @param ywsxms05
	 */
	public void setYwsxms05(String ywsxms05) {
		this.ywsxms05 = ywsxms05;
	}
	
    /**
     * @return ywsxms05
     */
	public String getYwsxms05() {
		return this.ywsxms05;
	}
	
	/**
	 * @param yewusx06
	 */
	public void setYewusx06(String yewusx06) {
		this.yewusx06 = yewusx06;
	}
	
    /**
     * @return yewusx06
     */
	public String getYewusx06() {
		return this.yewusx06;
	}
	
	/**
	 * @param ywsxms06
	 */
	public void setYwsxms06(String ywsxms06) {
		this.ywsxms06 = ywsxms06;
	}
	
    /**
     * @return ywsxms06
     */
	public String getYwsxms06() {
		return this.ywsxms06;
	}
	
	/**
	 * @param yewusx07
	 */
	public void setYewusx07(String yewusx07) {
		this.yewusx07 = yewusx07;
	}
	
    /**
     * @return yewusx07
     */
	public String getYewusx07() {
		return this.yewusx07;
	}
	
	/**
	 * @param ywsxms07
	 */
	public void setYwsxms07(String ywsxms07) {
		this.ywsxms07 = ywsxms07;
	}
	
    /**
     * @return ywsxms07
     */
	public String getYwsxms07() {
		return this.ywsxms07;
	}
	
	/**
	 * @param yewusx08
	 */
	public void setYewusx08(String yewusx08) {
		this.yewusx08 = yewusx08;
	}
	
    /**
     * @return yewusx08
     */
	public String getYewusx08() {
		return this.yewusx08;
	}
	
	/**
	 * @param ywsxms08
	 */
	public void setYwsxms08(String ywsxms08) {
		this.ywsxms08 = ywsxms08;
	}
	
    /**
     * @return ywsxms08
     */
	public String getYwsxms08() {
		return this.ywsxms08;
	}
	
	/**
	 * @param yewusx09
	 */
	public void setYewusx09(String yewusx09) {
		this.yewusx09 = yewusx09;
	}
	
    /**
     * @return yewusx09
     */
	public String getYewusx09() {
		return this.yewusx09;
	}
	
	/**
	 * @param ywsxms09
	 */
	public void setYwsxms09(String ywsxms09) {
		this.ywsxms09 = ywsxms09;
	}
	
    /**
     * @return ywsxms09
     */
	public String getYwsxms09() {
		return this.ywsxms09;
	}
	
	/**
	 * @param yewusx10
	 */
	public void setYewusx10(String yewusx10) {
		this.yewusx10 = yewusx10;
	}
	
    /**
     * @return yewusx10
     */
	public String getYewusx10() {
		return this.yewusx10;
	}
	
	/**
	 * @param ywsxms10
	 */
	public void setYwsxms10(String ywsxms10) {
		this.ywsxms10 = ywsxms10;
	}
	
    /**
     * @return ywsxms10
     */
	public String getYwsxms10() {
		return this.ywsxms10;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}