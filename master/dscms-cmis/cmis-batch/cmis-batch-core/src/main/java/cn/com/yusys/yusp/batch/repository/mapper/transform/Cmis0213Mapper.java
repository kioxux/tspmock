package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0213</br>
 * 任务名称：加工任务-额度处理-授信分项计算用信金额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0213Mapper {
    /**
     * 清理关系表
     */
    void truncateApprLmtSubBasicInfoA1();

    /**
     * 插入关系表
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfoA1(@Param("openDay") String openDay);

    /**
     * 计算普通贷款台账 垫款台账 最高额授信协议对应的台账 占用授信的 用信余额 、用信敞口余额
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfoA1(@Param("openDay") String openDay);


    /**
     * 清理关系表
     */
    void truncateApprLmtSubBasicInfoA2();

    /**
     * 插入关系表
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfoA2(@Param("openDay") String openDay);

    /**
     * 一级分项用信余额汇总至二级分项  额度分项项下关联的合同下的台账总余额、总敞口余额汇总 + 额度分项项下关联的非最高额授信协议项下的台账金额之和
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfoA2(@Param("openDay") String openDay);


    /**
     * 清空加工占用授信表[同业额度分项项下用信金额汇总]
     */
    void truncateTmpLmtAmtTy();

    /**
     * 插入加工占用授信表[同业额度分项项下用信金额汇总]
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtTy(@Param("openDay") String openDay);

    /**
     * 更新批复额度分项基础信息[同业额度分项项下用信金额汇总]
     *
     * @param openDay
     * @return
     */
    int updateTmpLmtAmtTy(@Param("openDay") String openDay);

    /**
     * 清空新信贷-临时表-加工占用授信表
     */
    void deleteTmpLmtAmt();


    /**
     * 插入新信贷-临时表-加工占用授信表
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt(@Param("openDay") String openDay);

    /**
     * 更新合作方授信分项信息加工表
     *
     * @param openDay
     * @return
     */
    int updateTmpDealApprCoopSubInfo(@Param("openDay") String openDay);
}
