package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0201Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0201</br>
 * 任务名称：加工任务-额度处理-额度关系占用</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0201Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0201Service.class);
    @Autowired
    private Cmis0201Mapper cmis0201Mapper;


    /**
     * 将客户模块的集团客户信息同步至额度模块的集团客户信息，对应SQL为CMIS0201-额度关系占用.sql
     *
     * @param openDay
     */
    public void cmis0201UpdateLmtCus(String openDay) {
        logger.info("清空额度库中集团客户与成员关系表开始,请求参数为:[{}]", openDay);
        int deleteLmtCusGrpMemberRel = cmis0201Mapper.deleteLmtCusGrpMemberRel(openDay);
        logger.info("清空额度库中集团客户与成员关系表结束,返回参数为:[{}]", deleteLmtCusGrpMemberRel);

        logger.info("插入额度库中集团客户与成员关系表开始,请求参数为:[{}]", openDay);
        int insertLmtCusGrpMemberRel = cmis0201Mapper.insertLmtCusGrpMemberRel(openDay);
        logger.info("插入额度库中集团客户与成员关系表结束,返回参数为:[{}]", insertLmtCusGrpMemberRel);

        logger.info("清空额度库中集团客户信息表开始,请求参数为:[{}]", openDay);
        int deleteLmtCusGrp = cmis0201Mapper.deleteLmtCusGrp(openDay);
        logger.info("清空额度库中集团客户信息表结束,返回参数为:[{}]", deleteLmtCusGrp);

        logger.info("插入额度库中集团客户信息表开始,请求参数为:[{}]", openDay);
        int insertLmtCusGrp = cmis0201Mapper.insertLmtCusGrp(openDay);
        logger.info("插入额度库中集团客户信息表结束,返回参数为:[{}]", insertLmtCusGrp);
    }

    /**
     * 插入额度占用关系，对应SQL为CMIS0201-额度关系占用.sql(SQLID为insertLmtContRelG(含)之前)
     *
     * @param openDay
     */
    public void cmis0201InsertLmtContRel01(String openDay) {
        logger.info("清空合同与申请加工表开始,请求参数为:[{}]", openDay);
        int deleteTmpIqpCtr = cmis0201Mapper.deleteTmpIqpCtr(openDay);
        logger.info("清空合同与申请加工表结束,返回参数为:[{}]", deleteTmpIqpCtr);

        logger.info("插入合同与申请加工表开始,请求参数为:[{}]", openDay);
        int insertTmpIqpCtr = cmis0201Mapper.insertTmpIqpCtr(openDay);
        logger.info("插入合同与申请加工表结束,返回参数为:[{}]", insertTmpIqpCtr);

        logger.info("更新合同与申请加工表开始,请求参数为:[{}]", openDay);
        int updateTmpIqpCtr = cmis0201Mapper.updateTmpIqpCtr(openDay);
        logger.info("更新合同与申请加工表结束,返回参数为:[{}]", updateTmpIqpCtr);

        logger.info("插入额度占用关系[1.1最高额授信协议 合同占用父授信分项]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelA1 = cmis0201Mapper.insertLmtContRelA1(openDay);
        logger.info("插入额度占用关系[1.1最高额授信协议 合同占用父授信分项]结束,返回参数为:[{}]", insertLmtContRelA1);

        logger.info("插入额度占用关系[1.2最高额授信协议 贷款 占用子授信分项]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelA2 = cmis0201Mapper.insertLmtContRelA2(openDay);
        logger.info("插入额度占用关系[1.2最高额授信协议 贷款 占用子授信分项]结束,返回参数为:[{}]", insertLmtContRelA2);

        logger.info("插入额度占用关系[最高额授信协议下银承产生垫款]开始,请求参数为:[{}]", openDay);
        int insertTdcarZgexyYcdk = cmis0201Mapper.insertTdcarZgexyYcdk(openDay);
        logger.info("插入额度占用关系[最高额授信协议下银承产生垫款]结束,返回参数为:[{}]", insertTdcarZgexyYcdk);

        logger.info("插入额度占用关系[1.3最高额授信协议 银票占用子授信分项]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelA3 = cmis0201Mapper.insertLmtContRelA3(openDay);
        logger.info("插入额度占用关系[1.3最高额授信协议 银票占用子授信分项]结束,返回参数为:[{}]", insertLmtContRelA3);

        logger.info("插入额度占用关系[1.4最高额授信协议 保函占用子授信分项]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelA4 = cmis0201Mapper.insertLmtContRelA4(openDay);
        logger.info("插入额度占用关系[1.4最高额授信协议 保函占用子授信分项]结束,返回参数为:[{}]", insertLmtContRelA4);

        logger.info("插入额度占用关系[1.5最高额授信协议 贴现 占用子授信分项]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelA5 = cmis0201Mapper.insertLmtContRelA5(openDay);
        logger.info("插入额度占用关系[1.5最高额授信协议 贴现 占用子授信分项]结束,返回参数为:[{}]", insertLmtContRelA5);

        logger.info("插入额度占用关系[1.6 最高额授信协议 信用证 占用子授信分项]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelA6 = cmis0201Mapper.insertLmtContRelA6(openDay);
        logger.info("插入额度占用关系[1.6 最高额授信协议 信用证 占用子授信分项]结束,返回参数为:[{}]", insertLmtContRelA6);
        // 删除 by zhangjw 20210925  开始
//        logger.info("插入额度占用关系[贷款]开始,请求参数为:[{}]", openDay);
//        int insertLmtContRelB = cmis0201Mapper.insertLmtContRelB(openDay);
//        logger.info("插入额度占用关系[贷款]结束,返回参数为:[{}]", insertLmtContRelB);
//
//        logger.info("插入额度占用关系[银承]开始,请求参数为:[{}]", openDay);
//        int insertLmtContRelC = cmis0201Mapper.insertLmtContRelC(openDay);
//        logger.info("插入额度占用关系[银承]结束,返回参数为:[{}]", insertLmtContRelC);
//
//        logger.info("插入额度占用关系[保函]开始,请求参数为:[{}]", openDay);
//        int insertLmtContRelD = cmis0201Mapper.insertLmtContRelD(openDay);
//        logger.info("插入额度占用关系[保函]结束,返回参数为:[{}]", insertLmtContRelD);
//
//        logger.info("插入额度占用关系[贴现]开始,请求参数为:[{}]", openDay);
//        int insertLmtContRelE = cmis0201Mapper.insertLmtContRelE(openDay);
//        logger.info("插入额度占用关系[贴现]结束,返回参数为:[{}]", insertLmtContRelE);
//
//        logger.info("插入额度占用关系[信用证]开始,请求参数为:[{}]", openDay);
//        int insertLmtContRelF = cmis0201Mapper.insertLmtContRelF(openDay);
//        logger.info("插入额度占用关系[信用证]结束,返回参数为:[{}]", insertLmtContRelF);
        // 删除 by zhangjw 20210925  结束

        logger.info("插入额度占用关系[贷款占用 合作方]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelG = cmis0201Mapper.insertLmtContRelG(openDay);
        logger.info("插入额度占用关系[贷款占用 合作方]结束,返回参数为:[{}]", insertLmtContRelG);
    }

    /**
     * 插入额度占用关系，对应SQL为CMIS0201-额度关系占用.sql(SQLID为insertLmtContRelG(不含)之后)
     *
     * @param openDay
     */
    public void cmis0201InsertLmtContRel02(String openDay) {
        logger.info("3.2A对公使用担保合同占用关系占用专业担保公司额度开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelA = cmis0201Mapper.insertCopGuarRelA(openDay);
        logger.info("3.2A对公使用担保合同占用关系占用专业担保公司额度结束,返回参数为:[{}]", insertCopGuarRelA);

        logger.info("3.2A对公使用担保合同占用关系占用专业担保公司额度开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelA2 = cmis0201Mapper.insertCopGuarRelA2(openDay);
        logger.info("3.2A对公使用担保合同占用关系占用专业担保公司额度结束,返回参数为:[{}]", insertCopGuarRelA2);

        logger.info("3.2B 合作方在合同签订占额 [集群贷以及对公业务条线贷款和个人经营性贷款]开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelB = cmis0201Mapper.insertCopGuarRelB(openDay);
        logger.info("3.2B 合作方在合同签订占额 [集群贷以及对公业务条线贷款和个人经营性贷款]结束,返回参数为:[{}]", insertCopGuarRelB);

        logger.info("3.3A  银承占用合作方(专业担保公司）cop_guar_rel开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelC = cmis0201Mapper.insertCopGuarRelC(openDay);
        logger.info("3.3A  银承占用合作方(专业担保公司）cop_guar_rel结束,返回参数为:[{}]", insertCopGuarRelC);

        logger.info("3.3B 银承占用合作方(非专业担保公司）lmt_cont_rel开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelD = cmis0201Mapper.insertCopGuarRelD(openDay);
        logger.info("3.3B 银承占用合作方(非专业担保公司）lmt_cont_rel结束,返回参数为:[{}]", insertCopGuarRelD);

        logger.info("3.4保函占用合作方(专业担保公司）cop_guar_rel开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelE = cmis0201Mapper.insertCopGuarRelE(openDay);
        logger.info("3.4保函占用合作方(专业担保公司）cop_guar_rel结束,返回参数为:[{}]", insertCopGuarRelE);

        logger.info("3.5信用证占用合作方(专业担保公司）cop_guar_rel开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelF = cmis0201Mapper.insertCopGuarRelF(openDay);
        logger.info("3.5信用证占用合作方(专业担保公司）cop_guar_rel结束,返回参数为:[{}]", insertCopGuarRelF);

        logger.info("代开保函占对手行同业额度开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelG = cmis0201Mapper.insertCopGuarRelG(openDay);
        logger.info("代开保函占对手行同业额度结束,返回参数为:[{}]", insertCopGuarRelG);

        logger.info("4.2 代开信用证占对手行同业额度开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelH = cmis0201Mapper.insertCopGuarRelH(openDay);
        logger.info("4.2 代开信用证占对手行同业额度结束,返回参数为:[{}]", insertCopGuarRelH);

        logger.info("4.3 福费廷占承兑行同业额度开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelI = cmis0201Mapper.insertCopGuarRelI(openDay);
        logger.info("4.3 福费廷占承兑行同业额度结束,返回参数为:[{}]", insertCopGuarRelI);

        logger.info("4.4 代开银票占 中国银行同业额度 （ 占客户额度在2.2）  8400494253 中国银行开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelJ = cmis0201Mapper.insertCopGuarRelJ(openDay);
        logger.info("4.4 代开银票占 中国银行同业额度 （ 占客户额度在2.2）  8400494253 中国银行结束,返回参数为:[{}]", insertCopGuarRelJ);

        logger.info("4.4 代开银票占 中国银行同业额度 （ 占客户额度在2.2）  8400494253 中国银行开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelK = cmis0201Mapper.insertCopGuarRelK(openDay);
        logger.info("4.4 代开银票占 中国银行同业额度 （ 占客户额度在2.2）  8400494253 中国银行结束,返回参数为:[{}]", insertCopGuarRelK);

        logger.info("4.6 商票贴现 占商票保贴额度（占用客户额度在2.4）开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelL = cmis0201Mapper.insertCopGuarRelL(openDay);
        logger.info("4.6 商票贴现 占商票保贴额度（占用客户额度在2.4）结束,返回参数为:[{}]", insertCopGuarRelL);

        logger.info("4.7  转贴现开始,请求参数为:[{}]", openDay);
        int insertCopGuarRelM = cmis0201Mapper.insertCopGuarRelM(openDay);
        logger.info("4.7  转贴现结束,返回参数为:[{}]", insertCopGuarRelM);

        logger.info("插入额度占用关系[贷款合同占用子授信分项]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelCtrA = cmis0201Mapper.insertLmtContRelCtrA(openDay);
        logger.info("插入额度占用关系[贷款合同占用子授信分项]结束,返回参数为:[{}]", insertLmtContRelCtrA);

        logger.info("插入额度占用关系[银承合同占用子授信分项]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelCtrC = cmis0201Mapper.insertLmtContRelCtrC(openDay);
        logger.info("插入额度占用关系[银承合同占用子授信分项]结束,返回参数为:[{}]", insertLmtContRelCtrC);


        logger.info("插入额度占用关系[银承合同占用子授信分项]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelCtrD = cmis0201Mapper.insertLmtContRelCtrD(openDay);
        logger.info("插入额度占用关系[银承合同占用子授信分项]结束,返回参数为:[{}]", insertLmtContRelCtrD);

        logger.info("插入额度占用关系[贴现合同占用子授信分项]开始,请求参数为:[{}]", openDay);
        int insertLmtContRelCtrE = cmis0201Mapper.insertLmtContRelCtrE(openDay);
        logger.info("插入额度占用关系[贴现合同占用子授信分项]结束,返回参数为:[{}]", insertLmtContRelCtrE);

        logger.info("插入额度占用关系[信用证合同占用子授信分项] 开始,请求参数为:[{}]", openDay);
        int insertLmtContRelCtrF = cmis0201Mapper.insertLmtContRelCtrF(openDay);
        logger.info("插入额度占用关系[信用证合同占用子授信分项] 结束,返回参数为:[{}]", insertLmtContRelCtrF);

        logger.info("插入额度占用关系[资产池协议占用子授信分项] 开始,请求参数为:[{}]", openDay);
        int insertLmtContRelCtrG = cmis0201Mapper.insertLmtContRelCtrG(openDay);
        logger.info("插入额度占用关系[资产池协议占用子授信分项] 结束,返回参数为:[{}]", insertLmtContRelCtrG);

        logger.info("插入额度占用关系[委托贷款合同占用子授信分项] 开始,请求参数为:[{}]", openDay);
        int insertLmtContRelCtrH = cmis0201Mapper.insertLmtContRelCtrH(openDay);
        logger.info("插入额度占用关系[委托贷款合同占用子授信分项] 结束,返回参数为:[{}]", insertLmtContRelCtrH);

        logger.info("银票质押入池占承兑行白名单额度 开始,请求参数为:[{}]", openDay);
        int insertTmpDealLmtContRelA = cmis0201Mapper.insertTmpDealLmtContRelA(openDay);
        logger.info("银票质押入池占承兑行白名单额度 结束,返回参数为:[{}]", insertTmpDealLmtContRelA);

        logger.info("银票质押入池占承兑行白名单额度开始,请求参数为:[{}]", openDay);
        int insertTmpDealLmtContRelB = cmis0201Mapper.insertTmpDealLmtContRelB(openDay);
        logger.info("银票质押入池占承兑行白名单额度结束,返回参数为:[{}]", insertTmpDealLmtContRelB);

        logger.info("福费廷占同业客户额度,请求参数为:[{}]", openDay);
        int insertIntoLmtContRelFFT = cmis0201Mapper.insertIntoLmtContRelFFT(openDay);
        logger.info("福费廷占同业客户额度,返回参数为:[{}]", insertIntoLmtContRelFFT);

        logger.info("代开保函占同业客户额度（迁移）,请求参数为:[{}]", openDay);
        int insertIntoLmtContRelDKBH = cmis0201Mapper.insertIntoLmtContRelDKBH(openDay);
        logger.info("代开保函占同业客户额度（迁移）,返回参数为:[{}]", insertIntoLmtContRelDKBH);
    }

}
