/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpSnBillNoTemp
 * @类描述: bat_s_rcp_sn_bill_no_temp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 15:42:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_rcp_sn_bill_no_temp")
public class BatSRcpSnBillNoTemp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 项目编码  联合贷平台分配（01001001） **/
	@Id
	@Column(name = "PRD_CODE")
	private String prdCode;
	
	/** 贷款编号  苏宁银行的借据编号 **/
	@Id
	@Column(name = "CONTRACT_NO")
	private String contractNo;
	
	/** 放款日期 牵头行账务核心登记的贷款建账日（YYYYMMDD） **/
	@Id
	@Column(name = "TXN_DATE")
	private String txnDate;
	
	/** 放款金额  单位元 **/
	@Column(name = "TXN_AMT", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal txnAmt;
	
	/** 币种  人民币 **/
	@Column(name = "CURRENCY", unique = false, nullable = true, length = 3)
	private String currency;
	
	/** 结息周期计数乘数  等额本息现金分期12期--12； **/
	@Column(name = "REST_CYCLE_MUL", unique = false, nullable = true, length = 8)
	private String restCycleMul;
	
	/** 年执行利率  0.xx（最长小数点后十位） **/
	@Column(name = "EXE_RATE_YEAR", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal exeRateYear;
	
	/** 年执行本金罚息率   0.xx（最长小数点后十位） **/
	@Column(name = "EXE_PENALTY_RATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal exePenaltyRate;
	
	/** 起息日   计息开始日期（YYYYMMDD） **/
	@Column(name = "INTEREST_START_DATE", unique = false, nullable = true, length = 10)
	private String interestStartDate;
	
	/** 到期日  （YYYYMMDD） **/
	@Column(name = "INTEREST_END_DATE", unique = false, nullable = true, length = 10)
	private String interestEndDate;
	
	/** 宽限期  日（n>=0）
当前宽限期内计息规则为不计息、贷款形态规则为不转逾期处理；
宽限期到期后以年执行本金罚息率对已到期本金从应还款日起按日累计罚息； **/
	@Column(name = "GRACE_TERM", unique = false, nullable = true, length = 10)
	private String graceTerm;
	
	/** 还款方式  3 - 等额还款
4 - 等本还款 **/
	@Column(name = "PMT_TYPE", unique = false, nullable = true, length = 1)
	private String pmtType;
	
	/** 本金逾期天数 **/
	@Column(name = "PRINCIPAL_OVERDUE_DAYS", unique = false, nullable = true, length = 10)
	private String principalOverdueDays;
	
	/** 利息逾期天数 **/
	@Column(name = "INTEREST_OVERDUE_DAYS", unique = false, nullable = true, length = 10)
	private String interestOverdueDays;
	
	/** 贷款本金余额   当前正常本金余额+当前逾期本金余额，单位元 **/
	@Column(name = "PRINCIPAL_BAL", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal principalBal;
	
	/** 正常本金余额   当前正常本金余额，单位元 **/
	@Column(name = "NORMAL_PRINCIPAL_BAL", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal normalPrincipalBal;
	
	/** 逾期本金余额   当前逾期本金余额，单位元 **/
	@Column(name = "OVERDUE_PRIN_BAL", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal overduePrinBal;
	
	/** 借据状态（正常、结清、逾期）  0-正常  1-结清   2-逾期 **/
	@Column(name = "TXN_STATUS", unique = false, nullable = true, length = 2)
	private String txnStatus;
	
	/** 结清日期 (销户日期) **/
	@Column(name = "SETTLE_DATE", unique = false, nullable = true, length = 10)
	private String settleDate;
	
	/** 核销标志  0-正常；
1-销户（结清）；
2-已核销； **/
	@Column(name = "CANCLE_FLAG", unique = false, nullable = true, length = 10)
	private String cancleFlag;
	
	/** 应计非应计标志  0-应计；
1-非应计； **/
	@Column(name = "ACCRUED_FLAG", unique = false, nullable = true, length = 10)
	private String accruedFlag;
	
	/** 本期应还金额 （当前期初始应还本金+初始应还利息-初始减免利息（暂无）） **/
	@Column(name = "CURR_PRRIOD_PAY_AMT", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal currPrriodPayAmt;
	
	/** 录入日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "LAST_UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	/** 零售智能风控数据日期 **/
	@Column(name = "RCP_DATA_DATE", unique = false, nullable = true, length = 10)
	private String rcpDataDate;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param prdCode
	 */
	public void setPrdCode(String prdCode) {
		this.prdCode = prdCode;
	}
	
    /**
     * @return prdCode
     */
	public String getPrdCode() {
		return this.prdCode;
	}
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param txnDate
	 */
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	
    /**
     * @return txnDate
     */
	public String getTxnDate() {
		return this.txnDate;
	}
	
	/**
	 * @param txnAmt
	 */
	public void setTxnAmt(java.math.BigDecimal txnAmt) {
		this.txnAmt = txnAmt;
	}
	
    /**
     * @return txnAmt
     */
	public java.math.BigDecimal getTxnAmt() {
		return this.txnAmt;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    /**
     * @return currency
     */
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param restCycleMul
	 */
	public void setRestCycleMul(String restCycleMul) {
		this.restCycleMul = restCycleMul;
	}
	
    /**
     * @return restCycleMul
     */
	public String getRestCycleMul() {
		return this.restCycleMul;
	}
	
	/**
	 * @param exeRateYear
	 */
	public void setExeRateYear(java.math.BigDecimal exeRateYear) {
		this.exeRateYear = exeRateYear;
	}
	
    /**
     * @return exeRateYear
     */
	public java.math.BigDecimal getExeRateYear() {
		return this.exeRateYear;
	}
	
	/**
	 * @param exePenaltyRate
	 */
	public void setExePenaltyRate(java.math.BigDecimal exePenaltyRate) {
		this.exePenaltyRate = exePenaltyRate;
	}
	
    /**
     * @return exePenaltyRate
     */
	public java.math.BigDecimal getExePenaltyRate() {
		return this.exePenaltyRate;
	}
	
	/**
	 * @param interestStartDate
	 */
	public void setInterestStartDate(String interestStartDate) {
		this.interestStartDate = interestStartDate;
	}
	
    /**
     * @return interestStartDate
     */
	public String getInterestStartDate() {
		return this.interestStartDate;
	}
	
	/**
	 * @param interestEndDate
	 */
	public void setInterestEndDate(String interestEndDate) {
		this.interestEndDate = interestEndDate;
	}
	
    /**
     * @return interestEndDate
     */
	public String getInterestEndDate() {
		return this.interestEndDate;
	}
	
	/**
	 * @param graceTerm
	 */
	public void setGraceTerm(String graceTerm) {
		this.graceTerm = graceTerm;
	}
	
    /**
     * @return graceTerm
     */
	public String getGraceTerm() {
		return this.graceTerm;
	}
	
	/**
	 * @param pmtType
	 */
	public void setPmtType(String pmtType) {
		this.pmtType = pmtType;
	}
	
    /**
     * @return pmtType
     */
	public String getPmtType() {
		return this.pmtType;
	}
	
	/**
	 * @param principalOverdueDays
	 */
	public void setPrincipalOverdueDays(String principalOverdueDays) {
		this.principalOverdueDays = principalOverdueDays;
	}
	
    /**
     * @return principalOverdueDays
     */
	public String getPrincipalOverdueDays() {
		return this.principalOverdueDays;
	}
	
	/**
	 * @param interestOverdueDays
	 */
	public void setInterestOverdueDays(String interestOverdueDays) {
		this.interestOverdueDays = interestOverdueDays;
	}
	
    /**
     * @return interestOverdueDays
     */
	public String getInterestOverdueDays() {
		return this.interestOverdueDays;
	}
	
	/**
	 * @param principalBal
	 */
	public void setPrincipalBal(java.math.BigDecimal principalBal) {
		this.principalBal = principalBal;
	}
	
    /**
     * @return principalBal
     */
	public java.math.BigDecimal getPrincipalBal() {
		return this.principalBal;
	}
	
	/**
	 * @param normalPrincipalBal
	 */
	public void setNormalPrincipalBal(java.math.BigDecimal normalPrincipalBal) {
		this.normalPrincipalBal = normalPrincipalBal;
	}
	
    /**
     * @return normalPrincipalBal
     */
	public java.math.BigDecimal getNormalPrincipalBal() {
		return this.normalPrincipalBal;
	}
	
	/**
	 * @param overduePrinBal
	 */
	public void setOverduePrinBal(java.math.BigDecimal overduePrinBal) {
		this.overduePrinBal = overduePrinBal;
	}
	
    /**
     * @return overduePrinBal
     */
	public java.math.BigDecimal getOverduePrinBal() {
		return this.overduePrinBal;
	}
	
	/**
	 * @param txnStatus
	 */
	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}
	
    /**
     * @return txnStatus
     */
	public String getTxnStatus() {
		return this.txnStatus;
	}
	
	/**
	 * @param settleDate
	 */
	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}
	
    /**
     * @return settleDate
     */
	public String getSettleDate() {
		return this.settleDate;
	}
	
	/**
	 * @param cancleFlag
	 */
	public void setCancleFlag(String cancleFlag) {
		this.cancleFlag = cancleFlag;
	}
	
    /**
     * @return cancleFlag
     */
	public String getCancleFlag() {
		return this.cancleFlag;
	}
	
	/**
	 * @param accruedFlag
	 */
	public void setAccruedFlag(String accruedFlag) {
		this.accruedFlag = accruedFlag;
	}
	
    /**
     * @return accruedFlag
     */
	public String getAccruedFlag() {
		return this.accruedFlag;
	}
	
	/**
	 * @param currPrriodPayAmt
	 */
	public void setCurrPrriodPayAmt(java.math.BigDecimal currPrriodPayAmt) {
		this.currPrriodPayAmt = currPrriodPayAmt;
	}
	
    /**
     * @return currPrriodPayAmt
     */
	public java.math.BigDecimal getCurrPrriodPayAmt() {
		return this.currPrriodPayAmt;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}
	
	/**
	 * @param rcpDataDate
	 */
	public void setRcpDataDate(String rcpDataDate) {
		this.rcpDataDate = rcpDataDate;
	}
	
    /**
     * @return rcpDataDate
     */
	public String getRcpDataDate() {
		return this.rcpDataDate;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}