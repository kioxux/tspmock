/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.rcp;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpAntRepayLoanDetailTemp;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpAntRepayLoanDetailTempService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpAntRepayLoanDetailTempResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:02:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsrcpantrepayloandetailtemp")
public class BatSRcpAntRepayLoanDetailTempResource {
    @Autowired
    private BatSRcpAntRepayLoanDetailTempService batSRcpAntRepayLoanDetailTempService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSRcpAntRepayLoanDetailTemp>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSRcpAntRepayLoanDetailTemp> list = batSRcpAntRepayLoanDetailTempService.selectAll(queryModel);
        return new ResultDto<List<BatSRcpAntRepayLoanDetailTemp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSRcpAntRepayLoanDetailTemp>> index(QueryModel queryModel) {
        List<BatSRcpAntRepayLoanDetailTemp> list = batSRcpAntRepayLoanDetailTempService.selectByModel(queryModel);
        return new ResultDto<List<BatSRcpAntRepayLoanDetailTemp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{contractNo}")
    protected ResultDto<BatSRcpAntRepayLoanDetailTemp> show(@PathVariable("contractNo") String contractNo) {
        BatSRcpAntRepayLoanDetailTemp batSRcpAntRepayLoanDetailTemp = batSRcpAntRepayLoanDetailTempService.selectByPrimaryKey(contractNo);
        return new ResultDto<BatSRcpAntRepayLoanDetailTemp>(batSRcpAntRepayLoanDetailTemp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSRcpAntRepayLoanDetailTemp> create(@RequestBody BatSRcpAntRepayLoanDetailTemp batSRcpAntRepayLoanDetailTemp) throws URISyntaxException {
        batSRcpAntRepayLoanDetailTempService.insert(batSRcpAntRepayLoanDetailTemp);
        return new ResultDto<BatSRcpAntRepayLoanDetailTemp>(batSRcpAntRepayLoanDetailTemp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSRcpAntRepayLoanDetailTemp batSRcpAntRepayLoanDetailTemp) throws URISyntaxException {
        int result = batSRcpAntRepayLoanDetailTempService.update(batSRcpAntRepayLoanDetailTemp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{contractNo}")
    protected ResultDto<Integer> delete(@PathVariable("contractNo") String contractNo) {
        int result = batSRcpAntRepayLoanDetailTempService.deleteByPrimaryKey(contractNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSRcpAntRepayLoanDetailTempService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
