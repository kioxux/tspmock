/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpAntLoanDetailSlTemp
 * @类描述: bat_t_rcp_ant_loan_detail_sl_temp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-10 14:48:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_rcp_ant_loan_detail_sl_temp")
public class BatTRcpAntLoanDetailSlTemp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 融资平台贷款合同号，等同于借据号，唯一标识一笔借据 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "contract_no")
	private String contractNo;
	
	/** 放款资金流水号，用来和网商银行账户资金流水进行对账 **/
	@Column(name = "fund_seq_no", unique = false, nullable = true, length = 64)
	private String fundSeqNo;
	
	/** 产品码，一期为J1010100100000000004，二期联合放贷为J1010100100000000004_2，二期直投J1010100100000000004_3 **/
	@Column(name = "prod_code", unique = false, nullable = true, length = 64)
	private String prodCode;
	
	/** 客户真实姓名 **/
	@Column(name = "name", unique = false, nullable = true, length = 128)
	private String name;
	
	/** 证件类型 **/
	@Column(name = "cert_type", unique = false, nullable = true, length = 2)
	private String certType;
	
	/** 客户证件号码，身份证统一为18位，尾数为X的为X大写 **/
	@Column(name = "cert_no", unique = false, nullable = true, length = 18)
	private String certNo;
	
	/** 贷款状态，1:成功 2:失败3:在途，在途支用明细文件中会存在2和3两种状态，放款（合约）明细文件中只存在状态1 **/
	@Column(name = "loan_status", unique = false, nullable = true, length = 2)
	private String loanStatus;
	
	/** 贷款用途，1:消费 3:旅游 4:装修 5:教育 6:医疗 **/
	@Column(name = "loan_use", unique = false, nullable = true, length = 2)
	private String loanUse;
	
	/** 贷款资金使用位置，0:境外  1:境内 **/
	@Column(name = "use_area", unique = false, nullable = true, length = 2)
	private String useArea;
	
	/** 申请支用时间，格式：yyyy-MM-dd HH:mm:ss **/
	@Column(name = "apply_date", unique = false, nullable = true, length = 20)
	private String applyDate;
	
	/** 放款日期，格式：格式：yyyy-MM-dd HH:mm:ss **/
	@Column(name = "encash_date", unique = false, nullable = true, length = 20)
	private String encashDate;
	
	/** 币种，默认为CNY **/
	@Column(name = "currency", unique = false, nullable = true, length = 3)
	private String currency;
	
	/** 放款金额（单位分） **/
	@Column(name = "encash_amt", unique = false, nullable = true, length = 10)
	private Integer encashAmt;
	
	/** 贷款起息日，格式：yyyyMMdd **/
	@Column(name = "start_date", unique = false, nullable = true, length = 8)
	private String startDate;
	
	/** 贷款到期日，格式：yyyyMMdd **/
	@Column(name = "end_date", unique = false, nullable = true, length = 8)
	private String endDate;
	
	/** 贷款期次数 **/
	@Column(name = "total_terms", unique = false, nullable = true, length = 10)
	private Integer totalTerms;
	
	/** 还款方式，1:等额本息2:等额本金3.按期付息到期还本6:到期一次还本付息 **/
	@Column(name = "repay_mode", unique = false, nullable = true, length = 8)
	private String repayMode;
	
	/** 宽限期天数 **/
	@Column(name = "grace_day", unique = false, nullable = true, length = 10)
	private Integer graceDay;
	
	/** 利率类型，F:固定利率L0:浮动利率-按日L1:浮动利率-按周L2:浮动利率-按月L3:浮动利率-按季L4:浮动利率-按半年L5:浮动利率-按年L9:浮动利率-其它 **/
	@Column(name = "rate_type", unique = false, nullable = true, length = 2)
	private String rateType;
	
	/** 贷款日利率，保留6位小数 **/
	@Column(name = "day_rate", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal dayRate;
	
	/** 本金还款频率，该笔贷款连续两次还款时间的间隔。01:日02:周03:月04:季05:半年06:年07:一次性08:不定期（贷款期限内任意时间还款）99:其他 **/
	@Column(name = "prin_repay_frequency", unique = false, nullable = true, length = 2)
	private String prinRepayFrequency;
	
	/** 利息还款频率，该笔贷款连续两次还款时间的间隔。01:日02:周03:月04:季05:半年06:年07:一次性08:不定期（贷款期限内任意时间还款）99:其他 **/
	@Column(name = "int_repay_frequency", unique = false, nullable = true, length = 2)
	private String intRepayFrequency;
	
	/** 担保类型，A 质押贷款 B抵押贷款 B01房地产抵押贷款 B99其他抵押贷款 C保证贷款 C01联保贷款 C99其他保证贷款 D信用/免担保贷款 E组合担保 Z其他 **/
	@Column(name = "guarantee_type", unique = false, nullable = true, length = 3)
	private String guaranteeType;
	
	/** 授信编号 **/
	@Column(name = "credit_no", unique = false, nullable = true, length = 64)
	private String creditNo;
	
	/** 收款帐号类型，01:银行借记卡；02:支付宝； **/
	@Column(name = "encash_acct_type", unique = false, nullable = true, length = 2)
	private String encashAcctType;
	
	/** 收款帐号 **/
	@Column(name = "encash_acct_no", unique = false, nullable = true, length = 64)
	private String encashAcctNo;
	
	/** 还款帐号类型，01:银行借记卡；02:支付宝；03:网商二类户 **/
	@Column(name = "repay_acct_type", unique = false, nullable = true, length = 2)
	private String repayAcctType;
	
	/** 还款帐号 **/
	@Column(name = "repay_acct_no", unique = false, nullable = true, length = 64)
	private String repayAcctNo;
	
	/** 贷款申请单号，和贷款合同号一一对应，根据这个单号可以到指定的合同文本文件目录下获取对应的文件 **/
	@Column(name = "apply_no", unique = false, nullable = true, length = 64)
	private String applyNo;
	
	/** 录入日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "input_time", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 零售智能风控_数据日期 **/
	@Column(name = "rcp_data_date", unique = false, nullable = true, length = 10)
	private String rcpDataDate;
	
	/** (原始数据)贷款到期日，格式：yyyyMMdd **/
	@Column(name = "orig_end_date", unique = false, nullable = true, length = 8)
	private String origEndDate;
	
	/** 行政区划代码 **/
	@Column(name = "region_code", unique = false, nullable = true, length = 8)
	private String regionCode;
	
	/** 业务类型 **/
	@Column(name = "bsn_type", unique = false, nullable = true, length = 8)
	private String bsnType;
	
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param fundSeqNo
	 */
	public void setFundSeqNo(String fundSeqNo) {
		this.fundSeqNo = fundSeqNo;
	}
	
    /**
     * @return fundSeqNo
     */
	public String getFundSeqNo() {
		return this.fundSeqNo;
	}
	
	/**
	 * @param prodCode
	 */
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	
    /**
     * @return prodCode
     */
	public String getProdCode() {
		return this.prodCode;
	}
	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
    /**
     * @return name
     */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}
	
    /**
     * @return certNo
     */
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param loanStatus
	 */
	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}
	
    /**
     * @return loanStatus
     */
	public String getLoanStatus() {
		return this.loanStatus;
	}
	
	/**
	 * @param loanUse
	 */
	public void setLoanUse(String loanUse) {
		this.loanUse = loanUse;
	}
	
    /**
     * @return loanUse
     */
	public String getLoanUse() {
		return this.loanUse;
	}
	
	/**
	 * @param useArea
	 */
	public void setUseArea(String useArea) {
		this.useArea = useArea;
	}
	
    /**
     * @return useArea
     */
	public String getUseArea() {
		return this.useArea;
	}
	
	/**
	 * @param applyDate
	 */
	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}
	
    /**
     * @return applyDate
     */
	public String getApplyDate() {
		return this.applyDate;
	}
	
	/**
	 * @param encashDate
	 */
	public void setEncashDate(String encashDate) {
		this.encashDate = encashDate;
	}
	
    /**
     * @return encashDate
     */
	public String getEncashDate() {
		return this.encashDate;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    /**
     * @return currency
     */
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param encashAmt
	 */
	public void setEncashAmt(Integer encashAmt) {
		this.encashAmt = encashAmt;
	}
	
    /**
     * @return encashAmt
     */
	public Integer getEncashAmt() {
		return this.encashAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param totalTerms
	 */
	public void setTotalTerms(Integer totalTerms) {
		this.totalTerms = totalTerms;
	}
	
    /**
     * @return totalTerms
     */
	public Integer getTotalTerms() {
		return this.totalTerms;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param graceDay
	 */
	public void setGraceDay(Integer graceDay) {
		this.graceDay = graceDay;
	}
	
    /**
     * @return graceDay
     */
	public Integer getGraceDay() {
		return this.graceDay;
	}
	
	/**
	 * @param rateType
	 */
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	
    /**
     * @return rateType
     */
	public String getRateType() {
		return this.rateType;
	}
	
	/**
	 * @param dayRate
	 */
	public void setDayRate(java.math.BigDecimal dayRate) {
		this.dayRate = dayRate;
	}
	
    /**
     * @return dayRate
     */
	public java.math.BigDecimal getDayRate() {
		return this.dayRate;
	}
	
	/**
	 * @param prinRepayFrequency
	 */
	public void setPrinRepayFrequency(String prinRepayFrequency) {
		this.prinRepayFrequency = prinRepayFrequency;
	}
	
    /**
     * @return prinRepayFrequency
     */
	public String getPrinRepayFrequency() {
		return this.prinRepayFrequency;
	}
	
	/**
	 * @param intRepayFrequency
	 */
	public void setIntRepayFrequency(String intRepayFrequency) {
		this.intRepayFrequency = intRepayFrequency;
	}
	
    /**
     * @return intRepayFrequency
     */
	public String getIntRepayFrequency() {
		return this.intRepayFrequency;
	}
	
	/**
	 * @param guaranteeType
	 */
	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}
	
    /**
     * @return guaranteeType
     */
	public String getGuaranteeType() {
		return this.guaranteeType;
	}
	
	/**
	 * @param creditNo
	 */
	public void setCreditNo(String creditNo) {
		this.creditNo = creditNo;
	}
	
    /**
     * @return creditNo
     */
	public String getCreditNo() {
		return this.creditNo;
	}
	
	/**
	 * @param encashAcctType
	 */
	public void setEncashAcctType(String encashAcctType) {
		this.encashAcctType = encashAcctType;
	}
	
    /**
     * @return encashAcctType
     */
	public String getEncashAcctType() {
		return this.encashAcctType;
	}
	
	/**
	 * @param encashAcctNo
	 */
	public void setEncashAcctNo(String encashAcctNo) {
		this.encashAcctNo = encashAcctNo;
	}
	
    /**
     * @return encashAcctNo
     */
	public String getEncashAcctNo() {
		return this.encashAcctNo;
	}
	
	/**
	 * @param repayAcctType
	 */
	public void setRepayAcctType(String repayAcctType) {
		this.repayAcctType = repayAcctType;
	}
	
    /**
     * @return repayAcctType
     */
	public String getRepayAcctType() {
		return this.repayAcctType;
	}
	
	/**
	 * @param repayAcctNo
	 */
	public void setRepayAcctNo(String repayAcctNo) {
		this.repayAcctNo = repayAcctNo;
	}
	
    /**
     * @return repayAcctNo
     */
	public String getRepayAcctNo() {
		return this.repayAcctNo;
	}
	
	/**
	 * @param applyNo
	 */
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	
    /**
     * @return applyNo
     */
	public String getApplyNo() {
		return this.applyNo;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param rcpDataDate
	 */
	public void setRcpDataDate(String rcpDataDate) {
		this.rcpDataDate = rcpDataDate;
	}
	
    /**
     * @return rcpDataDate
     */
	public String getRcpDataDate() {
		return this.rcpDataDate;
	}
	
	/**
	 * @param origEndDate
	 */
	public void setOrigEndDate(String origEndDate) {
		this.origEndDate = origEndDate;
	}
	
    /**
     * @return origEndDate
     */
	public String getOrigEndDate() {
		return this.origEndDate;
	}
	
	/**
	 * @param regionCode
	 */
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	
    /**
     * @return regionCode
     */
	public String getRegionCode() {
		return this.regionCode;
	}
	
	/**
	 * @param bsnType
	 */
	public void setBsnType(String bsnType) {
		this.bsnType = bsnType;
	}
	
    /**
     * @return bsnType
     */
	public String getBsnType() {
		return this.bsnType;
	}


}