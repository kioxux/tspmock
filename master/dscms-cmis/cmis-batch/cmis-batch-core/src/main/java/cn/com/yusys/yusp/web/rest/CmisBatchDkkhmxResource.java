package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.batch.dto.batch.CmisBatchDkkhmxDto;
import cn.com.yusys.yusp.batch.service.load.core.BatSCoreKlnlDkkhmxService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/batchdkkhmx")
public class CmisBatchDkkhmxResource {

    private static final Logger logger = LoggerFactory.getLogger(CmisBatchDkkhmxResource.class);

    @Autowired
    private BatSCoreKlnlDkkhmxService batSCoreKlnlDkkhmxService;

    /**
     * 根据借据号获取还款明细
     *
     * @author jijian_yx
     * @date 2021/10/20 22:19
     **/
    @PostMapping("/getrepaylistfrombatch")
    protected ResultDto<List<CmisBatchDkkhmxDto>> getRepayListFromBatch(@RequestBody QueryModel model) {
        List<CmisBatchDkkhmxDto> result = batSCoreKlnlDkkhmxService.getRepayListFromBatch(model);
        return new ResultDto<>(result);
    }
}
