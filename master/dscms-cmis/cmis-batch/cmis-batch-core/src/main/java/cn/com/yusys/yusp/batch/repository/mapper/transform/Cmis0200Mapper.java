package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0200</br>
 * 任务名称：加工任务-额度处理-批前台账比对和将额度相关表备份到额度相关临时表</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0200Mapper {
    /**
     * 清空分项占用关系信息加工表
     */
    void deleteTmpDealLmtContRel();

    /**
     * 插入分项占用关系信息加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpDealLmtContRel(@Param("openDay") String openDay);

    /**
     * 清空合同占用关系信息加工表
     */
    void deleteTmpDealContAccRel();

    /**
     * 插入合同占用关系信息加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpDealContAccRel(@Param("openDay") String openDay);

    /**
     * 清空批复额度分项基础信息加工表
     */
    void deleteTmpDealLmtDetails();

    /**
     * 插入批复额度分项基础信息加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpDealLmtDetails(@Param("openDay") String openDay);

    /**
     * 清空白名单额度信息表加工表
     */
    void deleteTmpDealLmtWhiteInfo();

    /**
     * 插入白名单额度信息表加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpDealLmtWhiteInfo(@Param("openDay") String openDay);

    /**
     * 清空合作方授信分项信息加工表
     */
    void deleteTmpDealApprCoopSubInfo();

    /**
     * 插入白名单额度信息表加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpDealApprCoopSubInfo(@Param("openDay") String openDay);

    /**
     * 清空临时表-贷款台账表
     */
    void truncateTmpAccLoan();

    /**
     * 插入临时表-贷款台账表
     *
     * @param openDay
     * @return
     */
    int insertTmpAccLoan(@Param("openDay") String openDay);

    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccDk();

    /**
     * 删除备份日表_贷款台账信息表0200
     */
    void deleteTmpDaccLoan0200();//addby cjz 20211018

    /**
     * 插入备份日表_贷款台账信息表0200
     */
    int insertTmpDaccLoan0200(@Param("openDay") String openDay);//addby cjz 20211018

    /**
     * 插入贷款台账余额比对表生成
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccDk(@Param("openDay") String openDay);

    /**
     * 插入贷款台账余额比对表生成（资产池协议）
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccDk2(@Param("openDay") String openDay);

    /**
     * 插入贷款台账余额比对表生成（最高额授信协议下）
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccDk3(@Param("openDay") String openDay);

    /**
     * 清空临时表-truncateTmpBalanceCompareAccYcht
     */
    void truncateTmpBalanceCompareAccYcht();


    /**
     * insertTmpBalanceCompareAccYcht-插入垫款台账余额比对表生成
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccYcht(@Param("openDay") String openDay);

    /**
     * 清空临时表-truncateTmpBalanceCompareAccWtdk
     */
    void truncateTmpBalanceCompareAccWtdk();

    /**
     * insertTmpBalanceCompareAccWtdk-   插入 委托贷款台账余额比对表生成
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccWtdk(@Param("openDay") String openDay);


    /**
     * 清空临时表-银承台账表
     */
    void truncateTmpAccAccp();

    /**
     * 插入临时表-银承台账表
     *
     * @param openDay
     * @return
     */
    int insertTmpAccAccp(@Param("openDay") String openDay);

    /**
     * 分析临时表-银承台账表
     *
     * @param openDay
     * @return
     */
    List<Map> analyzeTmpAccAccp(@Param("openDay") String openDay);

    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccYc();

    /**
     * 删除备份日表-银承台账表0200
     */
    void deleteTmpDAccApp0200();//addBy cjz 20211018

    /**
     * 插入备份日表-银承台账表0200
     */
    int insertTmpDAccApp0200(@Param("openDay") String openDay);//addBy cjz 20211018

    /**
     * 分析备份日表-银承台账表0200
     *
     * @param openDay
     * @return
     */
    List<Map> analyzeTmpDAccApp0200(@Param("openDay") String openDay);


    /**
     * 插入银承台账余额比对表生成
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccYc(@Param("openDay") String openDay);

    /**
     * 插入银承台账余额比对表生成（资产池协议）
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccYc2(@Param("openDay") String openDay);

    /**
     * 插入银承台账余额比对表生成（最高额授信协议）
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccYc3(@Param("openDay") String openDay);

    /**
     * 清空临时表-贴现台账表
     *
     * @return
     */
    void truncateTmpAccDisc();

    /**
     * 插入临时表-贴现台账表
     *
     * @param openDay
     * @return
     */
    int insertTmpAccDisc(@Param("openDay") String openDay);

    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccTx();


    /**
     * 删除备份日表_贴现台账0200
     */
    void deleteTmpDAccDisc0200();//addBy cjz 20211018

    /**
     * 插入备份日表_贴现台账0200
     */

    int insertTmpDAccDisc0200(@Param("openDay") String openDay);//addBy cjz 20211018

    /**
     * 插入贴现台账余额比对表生成
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccTx(@Param("openDay") String openDay);


    /**
     * 清空临时表-开证台账
     *
     * @return
     */
    void truncateTmpAccTfLoc();

    /**
     * 插入临时表-开证台账
     *
     * @param openDay
     * @return
     */
    int insertTmpAccTfLoc(@Param("openDay") String openDay);

    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccXyz();

    /**
     * 插入信用证台账余额比对表生成生成
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccXyz(@Param("openDay") String openDay);

    /**
     * 插入信用证台账余额比对表生成生成（最高额授信协议）
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccXyz2(@Param("openDay") String openDay);


    /**
     * 清空临时表-保函台账
     *
     * @return
     */
    void truncateTmpAccCvrs();

    /**
     * 插入临时表-保函台账
     *
     * @param openDay
     * @return
     */
    int insertTmpAccCvrs(@Param("openDay") String openDay);


    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccBh();

    /**
     * 插入保函台账余额比对表生成
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccBh(@Param("openDay") String openDay);

    /**
     * 插入保函台账余额比对表生成（最高额授信协议）
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccBh2(@Param("openDay") String openDay);

    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccDkBh();

    /**
     * 插入代开保函余额比对表生成
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccDkbh(@Param("openDay") String openDay);

    /**
     * 清空余额比对差异台账开始
     */
    void truncateTmpBalanceCompareAccDkxyz();

    /**
     * 插入代开信用证余额比对表生成
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccDkxyz(@Param("openDay") String openDay);

    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccFft();

    /**
     * 插入福费廷余额比对表生成
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccFft(@Param("openDay") String openDay);

    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccDkhtdq(@Param("openDay") String openDay);

    /**
     * 贷款合同到期注销开始
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccFhtdqzx(@Param("openDay") String openDay);


    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccWtdkhtdq(@Param("openDay") String openDay);

    /**
     * 委托贷款合同到期注销
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccFhtdqzxB(@Param("openDay") String openDay);


    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccYchtdq(@Param("openDay") String openDay);

    /**
     * 委托贷款合同到期注销
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccFhtdqzxC(@Param("openDay") String openDay);


    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccTxhtdq(@Param("openDay") String openDay);

    /**
     * 贴现合同到期注销 插入tmp_balance_compare_acc D
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccFhtdqzxD(@Param("openDay") String openDay);


    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccXyzhtdq(@Param("openDay") String openDay);

    /**
     * 信用证合同到期注销 插入tmp_balance_compare_acc E
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccFhtdqzxE(@Param("openDay") String openDay);

    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccBhhtdq(@Param("openDay") String openDay);


    /**
     * 保函合同到期 注销 插入tmp_balance_compare_acc F
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccFhtdqzxF(@Param("openDay") String openDay);


    /**
     * 清空余额比对差异台账
     */
    void truncateTmpBalanceCompareAccZgehtdq(@Param("openDay") String openDay);


    /**
     * 最高额授信协议到期注销 插入tmp_balance_compare_acc G
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccFhtdqzxG(@Param("openDay") String openDay);


    /**
     * 资产池协议到期注销 清空tmp_balance_compare_acc
     */
    void deleteTmpBalanceCompareAcc(@Param("openDay") String openDay);

    /**
     * 插入tmp_balance_compare_acc H
     *
     * @param openDay
     * @return
     */
    int insertTmpBalanceCompareAccFhtdqzxH(@Param("openDay") String openDay);


}
