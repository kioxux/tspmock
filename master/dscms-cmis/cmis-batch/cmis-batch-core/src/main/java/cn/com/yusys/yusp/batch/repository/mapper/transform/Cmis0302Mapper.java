package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0302</br>
 * 任务名称：加工任务-客户处理-名单生成  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0302Mapper {

    /**
     * 插入贷款台账表临时表(contNo)
     *
     * @param openDay
     * @return
     */
    int insertTmpAccLoanCont1(@Param("openDay") String openDay);


    /**
     * 插入贷款台账表临时表(cusId)
     *
     * @param openDay
     * @return
     */
    int insertTmpAccLoanCont2(@Param("openDay") String openDay);



    /**
     * 插入临时表-个人社会关系信息
     *
     * @param openDay
     * @return
     */
    int insertTmpCusindivSocial(@Param("openDay") String openDay);


    /**
     * 插入临时表-批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int insertTmpSApprLmtSub(@Param("openDay") String openDay);


    /**
     * 插入临时表_插入无还本续贷名单信息客户号exists
     *
     * @return
     */
    int insertTmpInsertCusLstWhbxd1(@Param("list") List<String> prdList);


    /**
     * 插入临时表_插入无还本续贷名单信息客户号notexists
     *
     * @param openDay
     * @return
     */
    int insertTmpInsertCusLstWhbxd2(@Param("openDay") String openDay);

    /**
     * 插入对公客户问题授信名单
     *
     * @param lastOpenDay
     * @return
     */
    int insertCusLstWtsx01(@Param("lastOpenDay") String lastOpenDay);

    /**
     * 插入个人客户问题授信名单
     *
     * @param lastOpenDay
     * @return
     */
    int insertCusLstWtsx02(@Param("lastOpenDay") String lastOpenDay);

    /**
     * 插入无还本续贷名单信息
     *
     * @param lastOpenDay
     * @return
     */
    int insertCusLstWhbxd(@Param("lastOpenDay") String lastOpenDay);

    /**
     * 更新问题授信名单导入原因
     *
     * @param lastOpenDay
     * @return
     */
    int updateCusLstWtsx(@Param("lastOpenDay") String lastOpenDay);


    /**
     * 期供表中 还款日超出到期日5天  大于0 不合格  去掉名单
     *
     * @param lastOpenDay
     * @return
     */
    int deleteCusLstMclWhbxd1(@Param("lastOpenDay") String lastOpenDay);


    /**
     * 期供表中 还款日比到期日大并且 在1 到4天之间 的次数大于3不合格    去掉名单
     *
     * @param openDay
     * @return
     */
    int deleteCusLstMclWhbxd2(@Param("openDay") String openDay);
    /**
     * 更新无还本续贷名单信息
     *
     * @param openDay
     * @return
     */
    int uptCusLstWhbxd(@Param("openDay") String openDay);

    /**
     * 查询产品配置信息信息
     *

     */
   // List<String> selectPrdIdList();
}
