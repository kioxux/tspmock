/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.domain.job;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobExecutionSeq
 * @类描述: batch_job_execution_seq数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "batch_job_execution_seq")
public class BatchJobExecutionSeq extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** ID **/
	@Column(name = "ID", unique = false, nullable = false, length = 19)
	private Long id;
	
	/** 索引值 **/
	@Column(name = "UNIQUE_KEY", unique = true, nullable = false, length = 1)
	private String uniqueKey;
	
	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public Long getId() {
		return this.id;
	}
	
	/**
	 * @param uniqueKey
	 */
	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}
	
    /**
     * @return uniqueKey
     */
	public String getUniqueKey() {
		return this.uniqueKey;
	}


}