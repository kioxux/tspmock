/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKlnbDkjcsx
 * @类描述: bat_s_core_klnb_dkjcsx数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_core_klnb_dkjcsx")
public class BatSCoreKlnbDkjcsx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 法人代码 **/
	@Id
	@Column(name = "farendma")
	private String farendma;
	
	/** 贷款借据号 **/
	@Id
	@Column(name = "dkjiejuh")
	private String dkjiejuh;
	
	/** 贷款对象 STD_DAIKDUIX **/
	@Column(name = "daikduix", unique = false, nullable = true, length = 1)
	private String daikduix;
	
	/** 贷款对象细分 STD_DAIKDXXF **/
	@Column(name = "daikdxxf", unique = false, nullable = true, length = 2)
	private String daikdxxf;
	
	/** 业务分类 STD_YEWUFENL **/
	@Column(name = "yewufenl", unique = false, nullable = true, length = 1)
	private String yewufenl;
	
	/** 表外产品 STD_YESORNO **/
	@Column(name = "bwchapbz", unique = false, nullable = true, length = 1)
	private String bwchapbz;
	
	/** 循环贷款 STD_YESORNO **/
	@Column(name = "xunhdaik", unique = false, nullable = true, length = 1)
	private String xunhdaik;
	
	/** 衍生贷款 STD_YESORNO **/
	@Column(name = "yansdaik", unique = false, nullable = true, length = 1)
	private String yansdaik;
	
	/** 衍生业务类型 STD_YSYWLEIX **/
	@Column(name = "ysywleix", unique = false, nullable = true, length = 1)
	private String ysywleix;
	
	/** 衍生业务编号 **/
	@Column(name = "ysywbhao", unique = false, nullable = true, length = 32)
	private String ysywbhao;
	
	/** 承诺贷款 STD_YESORNO **/
	@Column(name = "chendaik", unique = false, nullable = true, length = 1)
	private String chendaik;
	
	/** 承诺可循环标志 STD_YESORNO **/
	@Column(name = "cnkxhbzh", unique = false, nullable = true, length = 1)
	private String cnkxhbzh;
	
	/** 补贴贷款 STD_YESORNO **/
	@Column(name = "butidaik", unique = false, nullable = true, length = 1)
	private String butidaik;
	
	/** 银团贷款 STD_YESORNO **/
	@Column(name = "yintdkbz", unique = false, nullable = true, length = 1)
	private String yintdkbz;
	
	/** 银团贷款方式 STD_YINTDKFS **/
	@Column(name = "yintdkfs", unique = false, nullable = true, length = 1)
	private String yintdkfs;
	
	/** 银团类别 STD_YINTLEIB **/
	@Column(name = "yintleib", unique = false, nullable = true, length = 1)
	private String yintleib;
	
	/** 内部银团成员行类型 STD_YINTNBCY **/
	@Column(name = "yintnbcy", unique = false, nullable = true, length = 1)
	private String yintnbcy;
	
	/** 外部银团成员行类型 STD_YINTWBCY **/
	@Column(name = "yintwbcy", unique = false, nullable = true, length = 1)
	private String yintwbcy;
	
	/** 纳入忠诚度/积分计划 STD_YESORNO **/
	@Column(name = "zcjfjhbz", unique = false, nullable = true, length = 1)
	private String zcjfjhbz;
	
	/** 产品适用的监管类型 STD_JIANGULX **/
	@Column(name = "jiangulx", unique = false, nullable = true, length = 1)
	private String jiangulx;
	
	/** 产品关联的日历类型 STD_RILILEIX **/
	@Column(name = "rilileix", unique = false, nullable = true, length = 1)
	private String rilileix;
	
	/** 五级分类标志 STD_WUJIFLBZ **/
	@Column(name = "wujiflbz", unique = false, nullable = true, length = 1)
	private String wujiflbz;
	
	/** 五级分类日期 **/
	@Column(name = "wujiflrq", unique = false, nullable = true, length = 8)
	private String wujiflrq;
	
	/** 委托贷款代扣税标志 STD_YESORNO **/
	@Column(name = "wtdkshbz", unique = false, nullable = true, length = 1)
	private String wtdkshbz;
	
	/** 委托贷款代扣税税率 **/
	@Column(name = "wtdkshlv", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal wtdkshlv;
	
	/** 税票类型 STD_SHPIAOLX **/
	@Column(name = "shpiaolx", unique = false, nullable = true, length = 1)
	private String shpiaolx;
	
	/** 凭证种类 **/
	@Column(name = "pingzhzl", unique = false, nullable = true, length = 5)
	private String pingzhzl;
	
	/** 凭证号码 **/
	@Column(name = "pingzhma", unique = false, nullable = true, length = 32)
	private String pingzhma;
	
	/** 凭证序号 **/
	@Column(name = "pngzxhao", unique = false, nullable = true, length = 16)
	private String pngzxhao;
	
	/** 印花税率 **/
	@Column(name = "yinhshlv", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal yinhshlv;
	
	/** 印花税金额 **/
	@Column(name = "yinhshje", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yinhshje;
	
	/** 扣税状态 STD_KOUSHZHT **/
	@Column(name = "koushzht", unique = false, nullable = true, length = 1)
	private String koushzht;
	
	/** 客户经理 **/
	@Column(name = "khjingli", unique = false, nullable = true, length = 20)
	private String khjingli;
	
	/** 贷款管理机构 **/
	@Column(name = "dkgljgou", unique = false, nullable = true, length = 12)
	private String dkgljgou;
	
	/** 复核机构 **/
	@Column(name = "fuhejgou", unique = false, nullable = true, length = 12)
	private String fuhejgou;
	
	/** 联名贷款标志 STD_YESORNO **/
	@Column(name = "lmdkbzhi", unique = false, nullable = true, length = 1)
	private String lmdkbzhi;
	
	/** 保证人担保标志 STD_YESORNO **/
	@Column(name = "bzhrdbbz", unique = false, nullable = true, length = 1)
	private String bzhrdbbz;
	
	/** 关联抵质押物标志 STD_YESORNO **/
	@Column(name = "gldzywbz", unique = false, nullable = true, length = 1)
	private String gldzywbz;
	
	/** 账户质押标志 STD_YESORNO **/
	@Column(name = "zhhzhybz", unique = false, nullable = true, length = 1)
	private String zhhzhybz;
	
	/** 额度可撤销标志 STD_EDKCHXBZ **/
	@Column(name = "edkchxbz", unique = false, nullable = true, length = 1)
	private String edkchxbz;
	
	/** 分行标识 **/
	@Column(name = "fenhbios", unique = false, nullable = false, length = 4)
	private String fenhbios;
	
	/** 维护柜员 **/
	@Column(name = "WEIHGUIY", unique = false, nullable = true, length = 8)
	private String weihguiy;
	
	/** 维护机构 **/
	@Column(name = "weihjigo", unique = false, nullable = false, length = 12)
	private String weihjigo;
	
	/** 维护日期 **/
	@Column(name = "weihriqi", unique = false, nullable = false, length = 8)
	private String weihriqi;
	
	/** 维护时间 **/
	@Column(name = "weihshij", unique = false, nullable = true, length = 9)
	private String weihshij;
	
	/** 时间戳 **/
	@Column(name = "shijchuo", unique = false, nullable = false, length = 19)
	private Long shijchuo;
	
	/** 记录状态 STD_JILUZTAI **/
	@Column(name = "jiluztai", unique = false, nullable = false, length = 1)
	private String jiluztai;
	
	/** data_date **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param daikduix
	 */
	public void setDaikduix(String daikduix) {
		this.daikduix = daikduix;
	}
	
    /**
     * @return daikduix
     */
	public String getDaikduix() {
		return this.daikduix;
	}
	
	/**
	 * @param daikdxxf
	 */
	public void setDaikdxxf(String daikdxxf) {
		this.daikdxxf = daikdxxf;
	}
	
    /**
     * @return daikdxxf
     */
	public String getDaikdxxf() {
		return this.daikdxxf;
	}
	
	/**
	 * @param yewufenl
	 */
	public void setYewufenl(String yewufenl) {
		this.yewufenl = yewufenl;
	}
	
    /**
     * @return yewufenl
     */
	public String getYewufenl() {
		return this.yewufenl;
	}
	
	/**
	 * @param bwchapbz
	 */
	public void setBwchapbz(String bwchapbz) {
		this.bwchapbz = bwchapbz;
	}
	
    /**
     * @return bwchapbz
     */
	public String getBwchapbz() {
		return this.bwchapbz;
	}
	
	/**
	 * @param xunhdaik
	 */
	public void setXunhdaik(String xunhdaik) {
		this.xunhdaik = xunhdaik;
	}
	
    /**
     * @return xunhdaik
     */
	public String getXunhdaik() {
		return this.xunhdaik;
	}
	
	/**
	 * @param yansdaik
	 */
	public void setYansdaik(String yansdaik) {
		this.yansdaik = yansdaik;
	}
	
    /**
     * @return yansdaik
     */
	public String getYansdaik() {
		return this.yansdaik;
	}
	
	/**
	 * @param ysywleix
	 */
	public void setYsywleix(String ysywleix) {
		this.ysywleix = ysywleix;
	}
	
    /**
     * @return ysywleix
     */
	public String getYsywleix() {
		return this.ysywleix;
	}
	
	/**
	 * @param ysywbhao
	 */
	public void setYsywbhao(String ysywbhao) {
		this.ysywbhao = ysywbhao;
	}
	
    /**
     * @return ysywbhao
     */
	public String getYsywbhao() {
		return this.ysywbhao;
	}
	
	/**
	 * @param chendaik
	 */
	public void setChendaik(String chendaik) {
		this.chendaik = chendaik;
	}
	
    /**
     * @return chendaik
     */
	public String getChendaik() {
		return this.chendaik;
	}
	
	/**
	 * @param cnkxhbzh
	 */
	public void setCnkxhbzh(String cnkxhbzh) {
		this.cnkxhbzh = cnkxhbzh;
	}
	
    /**
     * @return cnkxhbzh
     */
	public String getCnkxhbzh() {
		return this.cnkxhbzh;
	}
	
	/**
	 * @param butidaik
	 */
	public void setButidaik(String butidaik) {
		this.butidaik = butidaik;
	}
	
    /**
     * @return butidaik
     */
	public String getButidaik() {
		return this.butidaik;
	}
	
	/**
	 * @param yintdkbz
	 */
	public void setYintdkbz(String yintdkbz) {
		this.yintdkbz = yintdkbz;
	}
	
    /**
     * @return yintdkbz
     */
	public String getYintdkbz() {
		return this.yintdkbz;
	}
	
	/**
	 * @param yintdkfs
	 */
	public void setYintdkfs(String yintdkfs) {
		this.yintdkfs = yintdkfs;
	}
	
    /**
     * @return yintdkfs
     */
	public String getYintdkfs() {
		return this.yintdkfs;
	}
	
	/**
	 * @param yintleib
	 */
	public void setYintleib(String yintleib) {
		this.yintleib = yintleib;
	}
	
    /**
     * @return yintleib
     */
	public String getYintleib() {
		return this.yintleib;
	}
	
	/**
	 * @param yintnbcy
	 */
	public void setYintnbcy(String yintnbcy) {
		this.yintnbcy = yintnbcy;
	}
	
    /**
     * @return yintnbcy
     */
	public String getYintnbcy() {
		return this.yintnbcy;
	}
	
	/**
	 * @param yintwbcy
	 */
	public void setYintwbcy(String yintwbcy) {
		this.yintwbcy = yintwbcy;
	}
	
    /**
     * @return yintwbcy
     */
	public String getYintwbcy() {
		return this.yintwbcy;
	}
	
	/**
	 * @param zcjfjhbz
	 */
	public void setZcjfjhbz(String zcjfjhbz) {
		this.zcjfjhbz = zcjfjhbz;
	}
	
    /**
     * @return zcjfjhbz
     */
	public String getZcjfjhbz() {
		return this.zcjfjhbz;
	}
	
	/**
	 * @param jiangulx
	 */
	public void setJiangulx(String jiangulx) {
		this.jiangulx = jiangulx;
	}
	
    /**
     * @return jiangulx
     */
	public String getJiangulx() {
		return this.jiangulx;
	}
	
	/**
	 * @param rilileix
	 */
	public void setRilileix(String rilileix) {
		this.rilileix = rilileix;
	}
	
    /**
     * @return rilileix
     */
	public String getRilileix() {
		return this.rilileix;
	}
	
	/**
	 * @param wujiflbz
	 */
	public void setWujiflbz(String wujiflbz) {
		this.wujiflbz = wujiflbz;
	}
	
    /**
     * @return wujiflbz
     */
	public String getWujiflbz() {
		return this.wujiflbz;
	}
	
	/**
	 * @param wujiflrq
	 */
	public void setWujiflrq(String wujiflrq) {
		this.wujiflrq = wujiflrq;
	}
	
    /**
     * @return wujiflrq
     */
	public String getWujiflrq() {
		return this.wujiflrq;
	}
	
	/**
	 * @param wtdkshbz
	 */
	public void setWtdkshbz(String wtdkshbz) {
		this.wtdkshbz = wtdkshbz;
	}
	
    /**
     * @return wtdkshbz
     */
	public String getWtdkshbz() {
		return this.wtdkshbz;
	}
	
	/**
	 * @param wtdkshlv
	 */
	public void setWtdkshlv(java.math.BigDecimal wtdkshlv) {
		this.wtdkshlv = wtdkshlv;
	}
	
    /**
     * @return wtdkshlv
     */
	public java.math.BigDecimal getWtdkshlv() {
		return this.wtdkshlv;
	}
	
	/**
	 * @param shpiaolx
	 */
	public void setShpiaolx(String shpiaolx) {
		this.shpiaolx = shpiaolx;
	}
	
    /**
     * @return shpiaolx
     */
	public String getShpiaolx() {
		return this.shpiaolx;
	}
	
	/**
	 * @param pingzhzl
	 */
	public void setPingzhzl(String pingzhzl) {
		this.pingzhzl = pingzhzl;
	}
	
    /**
     * @return pingzhzl
     */
	public String getPingzhzl() {
		return this.pingzhzl;
	}
	
	/**
	 * @param pingzhma
	 */
	public void setPingzhma(String pingzhma) {
		this.pingzhma = pingzhma;
	}
	
    /**
     * @return pingzhma
     */
	public String getPingzhma() {
		return this.pingzhma;
	}
	
	/**
	 * @param pngzxhao
	 */
	public void setPngzxhao(String pngzxhao) {
		this.pngzxhao = pngzxhao;
	}
	
    /**
     * @return pngzxhao
     */
	public String getPngzxhao() {
		return this.pngzxhao;
	}
	
	/**
	 * @param yinhshlv
	 */
	public void setYinhshlv(java.math.BigDecimal yinhshlv) {
		this.yinhshlv = yinhshlv;
	}
	
    /**
     * @return yinhshlv
     */
	public java.math.BigDecimal getYinhshlv() {
		return this.yinhshlv;
	}
	
	/**
	 * @param yinhshje
	 */
	public void setYinhshje(java.math.BigDecimal yinhshje) {
		this.yinhshje = yinhshje;
	}
	
    /**
     * @return yinhshje
     */
	public java.math.BigDecimal getYinhshje() {
		return this.yinhshje;
	}
	
	/**
	 * @param koushzht
	 */
	public void setKoushzht(String koushzht) {
		this.koushzht = koushzht;
	}
	
    /**
     * @return koushzht
     */
	public String getKoushzht() {
		return this.koushzht;
	}
	
	/**
	 * @param khjingli
	 */
	public void setKhjingli(String khjingli) {
		this.khjingli = khjingli;
	}
	
    /**
     * @return khjingli
     */
	public String getKhjingli() {
		return this.khjingli;
	}
	
	/**
	 * @param dkgljgou
	 */
	public void setDkgljgou(String dkgljgou) {
		this.dkgljgou = dkgljgou;
	}
	
    /**
     * @return dkgljgou
     */
	public String getDkgljgou() {
		return this.dkgljgou;
	}
	
	/**
	 * @param fuhejgou
	 */
	public void setFuhejgou(String fuhejgou) {
		this.fuhejgou = fuhejgou;
	}
	
    /**
     * @return fuhejgou
     */
	public String getFuhejgou() {
		return this.fuhejgou;
	}
	
	/**
	 * @param lmdkbzhi
	 */
	public void setLmdkbzhi(String lmdkbzhi) {
		this.lmdkbzhi = lmdkbzhi;
	}
	
    /**
     * @return lmdkbzhi
     */
	public String getLmdkbzhi() {
		return this.lmdkbzhi;
	}
	
	/**
	 * @param bzhrdbbz
	 */
	public void setBzhrdbbz(String bzhrdbbz) {
		this.bzhrdbbz = bzhrdbbz;
	}
	
    /**
     * @return bzhrdbbz
     */
	public String getBzhrdbbz() {
		return this.bzhrdbbz;
	}
	
	/**
	 * @param gldzywbz
	 */
	public void setGldzywbz(String gldzywbz) {
		this.gldzywbz = gldzywbz;
	}
	
    /**
     * @return gldzywbz
     */
	public String getGldzywbz() {
		return this.gldzywbz;
	}
	
	/**
	 * @param zhhzhybz
	 */
	public void setZhhzhybz(String zhhzhybz) {
		this.zhhzhybz = zhhzhybz;
	}
	
    /**
     * @return zhhzhybz
     */
	public String getZhhzhybz() {
		return this.zhhzhybz;
	}
	
	/**
	 * @param edkchxbz
	 */
	public void setEdkchxbz(String edkchxbz) {
		this.edkchxbz = edkchxbz;
	}
	
    /**
     * @return edkchxbz
     */
	public String getEdkchxbz() {
		return this.edkchxbz;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}