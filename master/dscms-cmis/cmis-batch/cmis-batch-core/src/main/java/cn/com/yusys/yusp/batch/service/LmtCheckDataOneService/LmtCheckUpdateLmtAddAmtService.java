/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.LmtCheckDataOneService;

import cn.com.yusys.yusp.batch.repository.mapper.check.LmtCheckUpdateLmtAddAmtMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateLmtAddAmtService
 * @类描述: #服务类
 * @功能描述: 数据更新额度校正-计算分项授信总额累加
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCheckUpdateLmtAddAmtService {

    private static final Logger logger = LoggerFactory.getLogger(LmtCheckUpdateLmtAddAmtService.class);

    @Autowired
    private LmtCheckUpdateLmtAddAmtMapper lmtCheckUpdateLmtAddAmtMapper;

    public void checkOutLmtCheckUpdateLmtAddAmt(String cusId){
        logger.info("10.LmtCheckUpdateLmtAddAmt数据更新额度校正-计算分项授信总额累加--------start");
        /** 1、更新授信总额累加为0 **/
        updateLmtAddAmtTo0(cusId) ;

        /** 1、更新授信总额累加为0 **/
        insertLmtAddAmt(cusId) ;
        logger.info("10.LmtCheckUpdateLmtAddAmt数据更新额度校正-计算分项授信总额累加--------end");
    }

    /**
     * 1、更新授信总额累加为0
     */
    public int updateLmtAddAmtTo0(String cusId){
        logger.info("1、更新授信总额累加为0--------start");
        return lmtCheckUpdateLmtAddAmtMapper.updateLmtAddAmtTo0(cusId);
    }

    /**
     * 2、加工授信总额累加
     */
    public int insertLmtAddAmt(String cusId){
        logger.info("2、加工授信总额累加--------start");
        lmtCheckUpdateLmtAddAmtMapper.insertLmtAddAmt1(cusId);
        lmtCheckUpdateLmtAddAmtMapper.insertLmtAddAmt2(cusId);
        lmtCheckUpdateLmtAddAmtMapper.insertLmtAddAmt3(cusId);
        lmtCheckUpdateLmtAddAmtMapper.insertLmtAddAmt4(cusId);

        lmtCheckUpdateLmtAddAmtMapper.truncateLmtAccDetails01();
        lmtCheckUpdateLmtAddAmtMapper.insertTmpAccAddDetails(cusId);

        lmtCheckUpdateLmtAddAmtMapper.truncateTmpLmtAmt01();
        lmtCheckUpdateLmtAddAmtMapper.insertLmtAddCase01(cusId);
        lmtCheckUpdateLmtAddAmtMapper.insertLmtAddCase02(cusId);
        lmtCheckUpdateLmtAddAmtMapper.insertLmtAddCase03(cusId);
        lmtCheckUpdateLmtAddAmtMapper.insertLmtAddCase04(cusId);

        lmtCheckUpdateLmtAddAmtMapper.updateLmtAddAmtCase05(cusId);

        lmtCheckUpdateLmtAddAmtMapper.truncateTmpLmtAmt02();
        lmtCheckUpdateLmtAddAmtMapper.insertLmtAddOneSubTmp(cusId);

        return 0;
    }
}
