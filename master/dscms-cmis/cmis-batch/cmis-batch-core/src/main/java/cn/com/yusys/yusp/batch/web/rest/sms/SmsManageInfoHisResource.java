/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.sms;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.sms.SmsManageInfoHis;
import cn.com.yusys.yusp.batch.service.sms.SmsManageInfoHisService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: SmsManageInfoHisResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:42:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/smsmanageinfohis")
public class SmsManageInfoHisResource {
    @Autowired
    private SmsManageInfoHisService smsManageInfoHisService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<SmsManageInfoHis>> query() {
        QueryModel queryModel = new QueryModel();
        List<SmsManageInfoHis> list = smsManageInfoHisService.selectAll(queryModel);
        return new ResultDto<List<SmsManageInfoHis>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<SmsManageInfoHis>> index(QueryModel queryModel) {
        List<SmsManageInfoHis> list = smsManageInfoHisService.selectByModel(queryModel);
        return new ResultDto<List<SmsManageInfoHis>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<SmsManageInfoHis> create(@RequestBody SmsManageInfoHis smsManageInfoHis) throws URISyntaxException {
        smsManageInfoHisService.insert(smsManageInfoHis);
        return new ResultDto<SmsManageInfoHis>(smsManageInfoHis);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody SmsManageInfoHis smsManageInfoHis) throws URISyntaxException {
        int result = smsManageInfoHisService.update(smsManageInfoHis);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String smsId, String taskDate) {
        int result = smsManageInfoHisService.deleteByPrimaryKey(smsId, taskDate);
        return new ResultDto<Integer>(result);
    }

}
