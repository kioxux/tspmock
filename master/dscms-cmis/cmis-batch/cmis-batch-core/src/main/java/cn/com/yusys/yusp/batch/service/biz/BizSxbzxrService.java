/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.biz;

import cn.com.yusys.yusp.batch.domain.biz.BizSxbzxr;
import cn.com.yusys.yusp.batch.repository.mapper.biz.BizSxbzxrMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp.Sxbzxr;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BizSxbzxrService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BizSxbzxrService {
    private static final Logger logger = LoggerFactory.getLogger(BizSxbzxrService.class);
    @Autowired
    private BizSxbzxrMapper bizSxbzxrMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BizSxbzxr selectByPrimaryKey(String appNo, String name, String type, String recordNumber) {
        return bizSxbzxrMapper.selectByPrimaryKey(appNo, name, type, recordNumber);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BizSxbzxr> selectAll(QueryModel model) {
        List<BizSxbzxr> records = (List<BizSxbzxr>) bizSxbzxrMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BizSxbzxr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BizSxbzxr> list = bizSxbzxrMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BizSxbzxr record) {
        return bizSxbzxrMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BizSxbzxr record) {
        return bizSxbzxrMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BizSxbzxr record) {
        return bizSxbzxrMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BizSxbzxr record) {
        return bizSxbzxrMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String appNo, String name, String type, String recordNumber) {
        return bizSxbzxrMapper.deleteByPrimaryKey(appNo, name, type, recordNumber);
    }

    /**
     * 根据[响应DTO：失信被执行人]封装失信被执行人
     *
     * @param sxbzxr
     * @param corpMap
     * @return
     */
    public BizSxbzxr buildBySxbzxr(Sxbzxr sxbzxr, Map<String, String> corpMap) {
        logger.info("根据[响应DTO：失信被执行人]封装失信被执行人开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        BizSxbzxr bizSxbzxr = GenericBuilder.of(BizSxbzxr::new)
                .with(BizSxbzxr::setAppNo, corpMap.get("autoSerno"))
                .with(BizSxbzxr::setType, "1")// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizSxbzxr::setName, corpMap.get("cusName"))//
                .with(BizSxbzxr::setPspSerno, corpMap.get("autoSerno"))//
                .build();
        BeanUtils.copyProperties(sxbzxr, bizSxbzxr);
        logger.info("根据[响应DTO：失信被执行人]封装失信被执行人结束,响应参数为:[{}]", JSON.toJSONString(bizSxbzxr));
        return bizSxbzxr;
    }
}
