/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.core;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKlnbDkfksx;
import cn.com.yusys.yusp.batch.service.load.core.BatSCoreKlnbDkfksxService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKlnbDkfksxResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-13 19:43:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batscoreklnbdkfksx")
public class BatSCoreKlnbDkfksxResource {
    @Autowired
    private BatSCoreKlnbDkfksxService batSCoreKlnbDkfksxService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSCoreKlnbDkfksx>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSCoreKlnbDkfksx> list = batSCoreKlnbDkfksxService.selectAll(queryModel);
        return new ResultDto<List<BatSCoreKlnbDkfksx>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSCoreKlnbDkfksx>> index(QueryModel queryModel) {
        List<BatSCoreKlnbDkfksx> list = batSCoreKlnbDkfksxService.selectByModel(queryModel);
        return new ResultDto<List<BatSCoreKlnbDkfksx>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSCoreKlnbDkfksx> create(@RequestBody BatSCoreKlnbDkfksx batSCoreKlnbDkfksx) throws URISyntaxException {
        batSCoreKlnbDkfksxService.insert(batSCoreKlnbDkfksx);
        return new ResultDto<BatSCoreKlnbDkfksx>(batSCoreKlnbDkfksx);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSCoreKlnbDkfksx batSCoreKlnbDkfksx) throws URISyntaxException {
        int result = batSCoreKlnbDkfksxService.update(batSCoreKlnbDkfksx);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String farendma, String dkjiejuh) {
        int result = batSCoreKlnbDkfksxService.deleteByPrimaryKey(farendma, dkjiejuh);
        return new ResultDto<Integer>(result);
    }

}
