/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKlnaDkzhsr
 * @类描述: bat_s_core_klna_dkzhsr数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_core_klna_dkzhsr")
public class BatSCoreKlnaDkzhsr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 法人代码 **/
	@Id
	@Column(name = "farendma")
	private String farendma;
	
	/** 贷款借据号 **/
	@Id
	@Column(name = "dkjiejuh")
	private String dkjiejuh;
	
	/** 贷款账号 **/
	@Column(name = "dkzhangh", unique = false, nullable = false, length = 40)
	private String dkzhangh;
	
	/** 合同编号 **/
	@Column(name = "hetongbh", unique = false, nullable = true, length = 30)
	private String hetongbh;
	
	/** 客户号 **/
	@Column(name = "kehuhaoo", unique = false, nullable = true, length = 20)
	private String kehuhaoo;
	
	/** 客户名称 **/
	@Column(name = "kehmingc", unique = false, nullable = true, length = 750)
	private String kehmingc;
	
	/** 营业机构 **/
	@Column(name = "yngyjigo", unique = false, nullable = true, length = 12)
	private String yngyjigo;
	
	/** 账务机构 **/
	@Column(name = "zhngjigo", unique = false, nullable = true, length = 12)
	private String zhngjigo;
	
	/** 产品代码 **/
	@Column(name = "chanpdma", unique = false, nullable = true, length = 10)
	private String chanpdma;
	
	/** 产品名称 **/
	@Column(name = "chanpmch", unique = false, nullable = true, length = 750)
	private String chanpmch;
	
	/** 会计类别 **/
	@Column(name = "kuaijilb", unique = false, nullable = true, length = 20)
	private String kuaijilb;
	
	/** 开户日期 **/
	@Column(name = "kaihriqi", unique = false, nullable = true, length = 8)
	private String kaihriqi;
	
	/** 起息日期 **/
	@Column(name = "qixiriqi", unique = false, nullable = true, length = 8)
	private String qixiriqi;
	
	/** 到期日期 **/
	@Column(name = "daoqriqi", unique = false, nullable = true, length = 8)
	private String daoqriqi;
	
	/** 贷款期限 **/
	@Column(name = "dkqixian", unique = false, nullable = true, length = 20)
	private String dkqixian;
	
	/** 贷款天数 **/
	@Column(name = "dktiansh", unique = false, nullable = true, length = 19)
	private Long dktiansh;
	
	/** 贷款形态(0-正常,1-逾期,2-呆滞,3-呆账) **/
	@Column(name = "daikxtai", unique = false, nullable = true, length = 1)
	private String daikxtai;
	
	/** 应计非应计状态(0-应计,1-非应计) **/
	@Column(name = "yjfyjzht", unique = false, nullable = true, length = 1)
	private String yjfyjzht;
	
	/** 贷款账户状态(0-正常,1-销户,2-已核销,3-准销户,4-录入,5-已减免) **/
	@Column(name = "dkzhhzht", unique = false, nullable = true, length = 1)
	private String dkzhhzht;
	
	/** 多笔贷款扣款顺序 **/
	@Column(name = "dbdkkksx", unique = false, nullable = true, length = 19)
	private Long dbdkkksx;
	
	/** 货币代号(01-人民币,12-英镑,13-香港元,14-美元,15-瑞士法郎,18-新加坡元,21-瑞典克郎,22-丹麦克郎,23-挪威克郎,27-日元,28-加元,29-澳大利亚元,38-欧
  元,70-南非兰特,81-澳门元,82-菲律宾比索,84-泰铢,87-新西兰元,88-韩元,90-俄罗斯卢布,36-银,34-黄金,SB-所有币种,WB-所有外币) **/
	@Column(name = "huobdhao", unique = false, nullable = true, length = 3)
	private String huobdhao;
	
	/** 合同金额 **/
	@Column(name = "hetongje", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal hetongje;
	
	/** 借据金额 **/
	@Column(name = "jiejuuje", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal jiejuuje;
	
	/** 已发放金额 **/
	@Column(name = "fafangje", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal fafangje;
	
	/** 冻结可放金额 **/
	@Column(name = "djiekfje", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal djiekfje;
	
	/** 可发放金额 **/
	@Column(name = "kffangje", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal kffangje;
	
	/** 正常本金 **/
	@Column(name = "zhchbjin", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal zhchbjin;
	
	/** 逾期本金 **/
	@Column(name = "yuqibjin", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yuqibjin;
	
	/** 呆滞本金 **/
	@Column(name = "dzhibjin", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal dzhibjin;
	
	/** 呆账本金 **/
	@Column(name = "daizbjin", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal daizbjin;
	
	/** 贷款基金 **/
	@Column(name = "dkuanjij", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal dkuanjij;
	
	/** 应收应计利息 **/
	@Column(name = "ysyjlixi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ysyjlixi;
	
	/** 催收应计利息 **/
	@Column(name = "csyjlixi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal csyjlixi;
	
	/** 应收欠息 **/
	@Column(name = "ysqianxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ysqianxi;
	
	/** 催收欠息 **/
	@Column(name = "csqianxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal csqianxi;
	
	/** 应收应计罚息 **/
	@Column(name = "ysyjfaxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ysyjfaxi;
	
	/** 催收应计罚息 **/
	@Column(name = "csyjfaxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal csyjfaxi;
	
	/** 应收罚息 **/
	@Column(name = "yshofaxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yshofaxi;
	
	/** 催收罚息 **/
	@Column(name = "cshofaxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal cshofaxi;
	
	/** 应计复息 **/
	@Column(name = "yingjifx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yingjifx;
	
	/** 复息 **/
	@Column(name = "fuxiiiii", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal fuxiiiii;
	
	/** 应计贴息 **/
	@Column(name = "yingjitx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yingjitx;
	
	/** 应收贴息 **/
	@Column(name = "yingshtx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yingshtx;
	
	/** 贴息复利 **/
	@Column(name = "tiexfuli", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal tiexfuli;
	
	/** 应收应计销项税 **/
	@Column(name = "ysyjxxsh", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ysyjxxsh;
	
	/** 催收应计销项税 **/
	@Column(name = "csyjxxsh", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal csyjxxsh;
	
	/** 销项税 **/
	@Column(name = "xxiangsh", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal xxiangsh;
	
	/** 待摊利息 **/
	@Column(name = "daitanlx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal daitanlx;
	
	/** 核销本金 **/
	@Column(name = "hexiaobj", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal hexiaobj;
	
	/** 核销利息 **/
	@Column(name = "hexiaolx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal hexiaolx;
	
	/** 利息收入 **/
	@Column(name = "lixishru", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal lixishru;
	
	/** 应收费用 **/
	@Column(name = "yingshfy", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yingshfy;
	
	/** 费用收入 **/
	@Column(name = "feiyshru", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal feiyshru;
	
	/** 应收罚金 **/
	@Column(name = "yingshfj", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yingshfj;
	
	/** 罚金收入 **/
	@Column(name = "fjinshru", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal fjinshru;
	
	/** 准备金 **/
	@Column(name = "zhunbeij", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal zhunbeij;
	
	/** 已计提贷款利息 **/
	@Column(name = "yijtdklx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yijtdklx;
	
	/** 已核销本金利息 **/
	@Column(name = "yihxbjlx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yihxbjlx;
	
	/** 最后财务交易日 **/
	@Column(name = "zhcwjyrq", unique = false, nullable = true, length = 8)
	private String zhcwjyrq;
	
	/** 可售产品代码 **/
	@Column(name = "kshchpdm", unique = false, nullable = true, length = 32)
	private String kshchpdm;
	
	/** 可售产品名称 **/
	@Column(name = "kshchpmc", unique = false, nullable = true, length = 750)
	private String kshchpmc;
	
	/** 系统渠道号 **/
	@Column(name = "xtqudhao", unique = false, nullable = true, length = 32)
	private String xtqudhao;
	
	/** 系统渠道名称 **/
	@Column(name = "xtqudmch", unique = false, nullable = true, length = 750)
	private String xtqudmch;
	
	/** 贷款出账号 **/
	@Column(name = "dkczhzhh", unique = false, nullable = true, length = 40)
	private String dkczhzhh;
	
	/** 贷款担保方式(1-抵押,2-质押,3-保证,4-信用,5-其他) **/
	@Column(name = "dkdbfshi", unique = false, nullable = true, length = 1)
	private String dkdbfshi;
	
	/** 贷款用途(1-经营,2-消费,3-其他) **/
	@Column(name = "dkyongtu", unique = false, nullable = true, length = 1)
	private String dkyongtu;
	
	/** 借据性质(0-不适用,1-贷款借据-对客,2-贷款借据-自营,3-贷款借据-托管,4-贷款借据-付款) **/
	@Column(name = "jiejuxzh", unique = false, nullable = true, length = 1)
	private String jiejuxzh;
	
	/** 资产状态(0-不适用,1-正常,2-转让,3-证券化,4-化债,5-核销) **/
	@Column(name = "ziczhtai", unique = false, nullable = true, length = 1)
	private String ziczhtai;
	
	/** 业务属性1 **/
	@Column(name = "yewusx01", unique = false, nullable = true, length = 32)
	private String yewusx01;
	
	/** 业务属性描述1 **/
	@Column(name = "ywsxms01", unique = false, nullable = true, length = 80)
	private String ywsxms01;
	
	/** 业务属性2 **/
	@Column(name = "yewusx02", unique = false, nullable = true, length = 32)
	private String yewusx02;
	
	/** 业务属性描述2 **/
	@Column(name = "ywsxms02", unique = false, nullable = true, length = 80)
	private String ywsxms02;
	
	/** 业务属性3 **/
	@Column(name = "yewusx03", unique = false, nullable = true, length = 32)
	private String yewusx03;
	
	/** 业务属性描述3 **/
	@Column(name = "ywsxms03", unique = false, nullable = true, length = 80)
	private String ywsxms03;
	
	/** 业务属性4 **/
	@Column(name = "yewusx04", unique = false, nullable = true, length = 32)
	private String yewusx04;
	
	/** 业务属性描述4 **/
	@Column(name = "ywsxms04", unique = false, nullable = true, length = 80)
	private String ywsxms04;
	
	/** 业务属性5 **/
	@Column(name = "yewusx05", unique = false, nullable = true, length = 32)
	private String yewusx05;
	
	/** 业务属性描述5 **/
	@Column(name = "ywsxms05", unique = false, nullable = true, length = 80)
	private String ywsxms05;
	
	/** 业务属性6 **/
	@Column(name = "yewusx06", unique = false, nullable = true, length = 32)
	private String yewusx06;
	
	/** 业务属性描述6 **/
	@Column(name = "ywsxms06", unique = false, nullable = true, length = 80)
	private String ywsxms06;
	
	/** 业务属性7 **/
	@Column(name = "yewusx07", unique = false, nullable = true, length = 32)
	private String yewusx07;
	
	/** 业务属性描述7 **/
	@Column(name = "ywsxms07", unique = false, nullable = true, length = 80)
	private String ywsxms07;
	
	/** 业务属性8 **/
	@Column(name = "yewusx08", unique = false, nullable = true, length = 32)
	private String yewusx08;
	
	/** 业务属性描述8 **/
	@Column(name = "ywsxms08", unique = false, nullable = true, length = 80)
	private String ywsxms08;
	
	/** 业务属性9 **/
	@Column(name = "yewusx09", unique = false, nullable = true, length = 32)
	private String yewusx09;
	
	/** 业务属性描述9 **/
	@Column(name = "ywsxms09", unique = false, nullable = true, length = 80)
	private String ywsxms09;
	
	/** 业务属性10 **/
	@Column(name = "yewusx10", unique = false, nullable = true, length = 32)
	private String yewusx10;
	
	/** 业务属性描述10 **/
	@Column(name = "ywsxms10", unique = false, nullable = true, length = 80)
	private String ywsxms10;
	
	/** 备注信息 **/
	@Column(name = "beizhuuu", unique = false, nullable = true, length = 65535)
	private String beizhuuu;
	
	/** 明细序号 **/
	@Column(name = "mingxixh", unique = false, nullable = true, length = 19)
	private Long mingxixh;
	
	/** 开户机构 **/
	@Column(name = "kaihujig", unique = false, nullable = true, length = 12)
	private String kaihujig;
	
	/** 开户柜员 **/
	@Column(name = "kaihguiy", unique = false, nullable = true, length = 8)
	private String kaihguiy;
	
	/** 销户柜员 **/
	@Column(name = "xiaohugy", unique = false, nullable = true, length = 8)
	private String xiaohugy;
	
	/** 销户日期 **/
	@Column(name = "xiaohurq", unique = false, nullable = true, length = 8)
	private String xiaohurq;
	
	/** 赎回时点利息还清标志(1-是,0-否) **/
	@Column(name = "shlxsfhq", unique = false, nullable = true, length = 1)
	private String shlxsfhq;
	
	/** 分行标识 **/
	@Column(name = "fenhbios", unique = false, nullable = false, length = 4)
	private String fenhbios;
	
	/** 维护柜员 **/
	@Column(name = "WEIHGUIY", unique = false, nullable = true, length = 8)
	private String weihguiy;
	
	/** 维护机构 **/
	@Column(name = "weihjigo", unique = false, nullable = false, length = 12)
	private String weihjigo;
	
	/** 维护日期 **/
	@Column(name = "weihriqi", unique = false, nullable = false, length = 8)
	private String weihriqi;
	
	/** 维护时间 **/
	@Column(name = "weihshij", unique = false, nullable = true, length = 9)
	private String weihshij;
	
	/** 时间戳 **/
	@Column(name = "shijchuo", unique = false, nullable = false, length = 19)
	private Long shijchuo;
	
	/** 记录状态(0-正常,1-删除) **/
	@Column(name = "jiluztai", unique = false, nullable = false, length = 1)
	private String jiluztai;
	
	/** data_date **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param dkzhangh
	 */
	public void setDkzhangh(String dkzhangh) {
		this.dkzhangh = dkzhangh;
	}
	
    /**
     * @return dkzhangh
     */
	public String getDkzhangh() {
		return this.dkzhangh;
	}
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param hetongbh
	 */
	public void setHetongbh(String hetongbh) {
		this.hetongbh = hetongbh;
	}
	
    /**
     * @return hetongbh
     */
	public String getHetongbh() {
		return this.hetongbh;
	}
	
	/**
	 * @param kehuhaoo
	 */
	public void setKehuhaoo(String kehuhaoo) {
		this.kehuhaoo = kehuhaoo;
	}
	
    /**
     * @return kehuhaoo
     */
	public String getKehuhaoo() {
		return this.kehuhaoo;
	}
	
	/**
	 * @param kehmingc
	 */
	public void setKehmingc(String kehmingc) {
		this.kehmingc = kehmingc;
	}
	
    /**
     * @return kehmingc
     */
	public String getKehmingc() {
		return this.kehmingc;
	}
	
	/**
	 * @param yngyjigo
	 */
	public void setYngyjigo(String yngyjigo) {
		this.yngyjigo = yngyjigo;
	}
	
    /**
     * @return yngyjigo
     */
	public String getYngyjigo() {
		return this.yngyjigo;
	}
	
	/**
	 * @param zhngjigo
	 */
	public void setZhngjigo(String zhngjigo) {
		this.zhngjigo = zhngjigo;
	}
	
    /**
     * @return zhngjigo
     */
	public String getZhngjigo() {
		return this.zhngjigo;
	}
	
	/**
	 * @param chanpdma
	 */
	public void setChanpdma(String chanpdma) {
		this.chanpdma = chanpdma;
	}
	
    /**
     * @return chanpdma
     */
	public String getChanpdma() {
		return this.chanpdma;
	}
	
	/**
	 * @param chanpmch
	 */
	public void setChanpmch(String chanpmch) {
		this.chanpmch = chanpmch;
	}
	
    /**
     * @return chanpmch
     */
	public String getChanpmch() {
		return this.chanpmch;
	}
	
	/**
	 * @param kuaijilb
	 */
	public void setKuaijilb(String kuaijilb) {
		this.kuaijilb = kuaijilb;
	}
	
    /**
     * @return kuaijilb
     */
	public String getKuaijilb() {
		return this.kuaijilb;
	}
	
	/**
	 * @param kaihriqi
	 */
	public void setKaihriqi(String kaihriqi) {
		this.kaihriqi = kaihriqi;
	}
	
    /**
     * @return kaihriqi
     */
	public String getKaihriqi() {
		return this.kaihriqi;
	}
	
	/**
	 * @param qixiriqi
	 */
	public void setQixiriqi(String qixiriqi) {
		this.qixiriqi = qixiriqi;
	}
	
    /**
     * @return qixiriqi
     */
	public String getQixiriqi() {
		return this.qixiriqi;
	}
	
	/**
	 * @param daoqriqi
	 */
	public void setDaoqriqi(String daoqriqi) {
		this.daoqriqi = daoqriqi;
	}
	
    /**
     * @return daoqriqi
     */
	public String getDaoqriqi() {
		return this.daoqriqi;
	}
	
	/**
	 * @param dkqixian
	 */
	public void setDkqixian(String dkqixian) {
		this.dkqixian = dkqixian;
	}
	
    /**
     * @return dkqixian
     */
	public String getDkqixian() {
		return this.dkqixian;
	}
	
	/**
	 * @param dktiansh
	 */
	public void setDktiansh(Long dktiansh) {
		this.dktiansh = dktiansh;
	}
	
    /**
     * @return dktiansh
     */
	public Long getDktiansh() {
		return this.dktiansh;
	}
	
	/**
	 * @param daikxtai
	 */
	public void setDaikxtai(String daikxtai) {
		this.daikxtai = daikxtai;
	}
	
    /**
     * @return daikxtai
     */
	public String getDaikxtai() {
		return this.daikxtai;
	}
	
	/**
	 * @param yjfyjzht
	 */
	public void setYjfyjzht(String yjfyjzht) {
		this.yjfyjzht = yjfyjzht;
	}
	
    /**
     * @return yjfyjzht
     */
	public String getYjfyjzht() {
		return this.yjfyjzht;
	}
	
	/**
	 * @param dkzhhzht
	 */
	public void setDkzhhzht(String dkzhhzht) {
		this.dkzhhzht = dkzhhzht;
	}
	
    /**
     * @return dkzhhzht
     */
	public String getDkzhhzht() {
		return this.dkzhhzht;
	}
	
	/**
	 * @param dbdkkksx
	 */
	public void setDbdkkksx(Long dbdkkksx) {
		this.dbdkkksx = dbdkkksx;
	}
	
    /**
     * @return dbdkkksx
     */
	public Long getDbdkkksx() {
		return this.dbdkkksx;
	}
	
	/**
	 * @param huobdhao
	 */
	public void setHuobdhao(String huobdhao) {
		this.huobdhao = huobdhao;
	}
	
    /**
     * @return huobdhao
     */
	public String getHuobdhao() {
		return this.huobdhao;
	}
	
	/**
	 * @param hetongje
	 */
	public void setHetongje(java.math.BigDecimal hetongje) {
		this.hetongje = hetongje;
	}
	
    /**
     * @return hetongje
     */
	public java.math.BigDecimal getHetongje() {
		return this.hetongje;
	}
	
	/**
	 * @param jiejuuje
	 */
	public void setJiejuuje(java.math.BigDecimal jiejuuje) {
		this.jiejuuje = jiejuuje;
	}
	
    /**
     * @return jiejuuje
     */
	public java.math.BigDecimal getJiejuuje() {
		return this.jiejuuje;
	}
	
	/**
	 * @param fafangje
	 */
	public void setFafangje(java.math.BigDecimal fafangje) {
		this.fafangje = fafangje;
	}
	
    /**
     * @return fafangje
     */
	public java.math.BigDecimal getFafangje() {
		return this.fafangje;
	}
	
	/**
	 * @param djiekfje
	 */
	public void setDjiekfje(java.math.BigDecimal djiekfje) {
		this.djiekfje = djiekfje;
	}
	
    /**
     * @return djiekfje
     */
	public java.math.BigDecimal getDjiekfje() {
		return this.djiekfje;
	}
	
	/**
	 * @param kffangje
	 */
	public void setKffangje(java.math.BigDecimal kffangje) {
		this.kffangje = kffangje;
	}
	
    /**
     * @return kffangje
     */
	public java.math.BigDecimal getKffangje() {
		return this.kffangje;
	}
	
	/**
	 * @param zhchbjin
	 */
	public void setZhchbjin(java.math.BigDecimal zhchbjin) {
		this.zhchbjin = zhchbjin;
	}
	
    /**
     * @return zhchbjin
     */
	public java.math.BigDecimal getZhchbjin() {
		return this.zhchbjin;
	}
	
	/**
	 * @param yuqibjin
	 */
	public void setYuqibjin(java.math.BigDecimal yuqibjin) {
		this.yuqibjin = yuqibjin;
	}
	
    /**
     * @return yuqibjin
     */
	public java.math.BigDecimal getYuqibjin() {
		return this.yuqibjin;
	}
	
	/**
	 * @param dzhibjin
	 */
	public void setDzhibjin(java.math.BigDecimal dzhibjin) {
		this.dzhibjin = dzhibjin;
	}
	
    /**
     * @return dzhibjin
     */
	public java.math.BigDecimal getDzhibjin() {
		return this.dzhibjin;
	}
	
	/**
	 * @param daizbjin
	 */
	public void setDaizbjin(java.math.BigDecimal daizbjin) {
		this.daizbjin = daizbjin;
	}
	
    /**
     * @return daizbjin
     */
	public java.math.BigDecimal getDaizbjin() {
		return this.daizbjin;
	}
	
	/**
	 * @param dkuanjij
	 */
	public void setDkuanjij(java.math.BigDecimal dkuanjij) {
		this.dkuanjij = dkuanjij;
	}
	
    /**
     * @return dkuanjij
     */
	public java.math.BigDecimal getDkuanjij() {
		return this.dkuanjij;
	}
	
	/**
	 * @param ysyjlixi
	 */
	public void setYsyjlixi(java.math.BigDecimal ysyjlixi) {
		this.ysyjlixi = ysyjlixi;
	}
	
    /**
     * @return ysyjlixi
     */
	public java.math.BigDecimal getYsyjlixi() {
		return this.ysyjlixi;
	}
	
	/**
	 * @param csyjlixi
	 */
	public void setCsyjlixi(java.math.BigDecimal csyjlixi) {
		this.csyjlixi = csyjlixi;
	}
	
    /**
     * @return csyjlixi
     */
	public java.math.BigDecimal getCsyjlixi() {
		return this.csyjlixi;
	}
	
	/**
	 * @param ysqianxi
	 */
	public void setYsqianxi(java.math.BigDecimal ysqianxi) {
		this.ysqianxi = ysqianxi;
	}
	
    /**
     * @return ysqianxi
     */
	public java.math.BigDecimal getYsqianxi() {
		return this.ysqianxi;
	}
	
	/**
	 * @param csqianxi
	 */
	public void setCsqianxi(java.math.BigDecimal csqianxi) {
		this.csqianxi = csqianxi;
	}
	
    /**
     * @return csqianxi
     */
	public java.math.BigDecimal getCsqianxi() {
		return this.csqianxi;
	}
	
	/**
	 * @param ysyjfaxi
	 */
	public void setYsyjfaxi(java.math.BigDecimal ysyjfaxi) {
		this.ysyjfaxi = ysyjfaxi;
	}
	
    /**
     * @return ysyjfaxi
     */
	public java.math.BigDecimal getYsyjfaxi() {
		return this.ysyjfaxi;
	}
	
	/**
	 * @param csyjfaxi
	 */
	public void setCsyjfaxi(java.math.BigDecimal csyjfaxi) {
		this.csyjfaxi = csyjfaxi;
	}
	
    /**
     * @return csyjfaxi
     */
	public java.math.BigDecimal getCsyjfaxi() {
		return this.csyjfaxi;
	}
	
	/**
	 * @param yshofaxi
	 */
	public void setYshofaxi(java.math.BigDecimal yshofaxi) {
		this.yshofaxi = yshofaxi;
	}
	
    /**
     * @return yshofaxi
     */
	public java.math.BigDecimal getYshofaxi() {
		return this.yshofaxi;
	}
	
	/**
	 * @param cshofaxi
	 */
	public void setCshofaxi(java.math.BigDecimal cshofaxi) {
		this.cshofaxi = cshofaxi;
	}
	
    /**
     * @return cshofaxi
     */
	public java.math.BigDecimal getCshofaxi() {
		return this.cshofaxi;
	}
	
	/**
	 * @param yingjifx
	 */
	public void setYingjifx(java.math.BigDecimal yingjifx) {
		this.yingjifx = yingjifx;
	}
	
    /**
     * @return yingjifx
     */
	public java.math.BigDecimal getYingjifx() {
		return this.yingjifx;
	}
	
	/**
	 * @param fuxiiiii
	 */
	public void setFuxiiiii(java.math.BigDecimal fuxiiiii) {
		this.fuxiiiii = fuxiiiii;
	}
	
    /**
     * @return fuxiiiii
     */
	public java.math.BigDecimal getFuxiiiii() {
		return this.fuxiiiii;
	}
	
	/**
	 * @param yingjitx
	 */
	public void setYingjitx(java.math.BigDecimal yingjitx) {
		this.yingjitx = yingjitx;
	}
	
    /**
     * @return yingjitx
     */
	public java.math.BigDecimal getYingjitx() {
		return this.yingjitx;
	}
	
	/**
	 * @param yingshtx
	 */
	public void setYingshtx(java.math.BigDecimal yingshtx) {
		this.yingshtx = yingshtx;
	}
	
    /**
     * @return yingshtx
     */
	public java.math.BigDecimal getYingshtx() {
		return this.yingshtx;
	}
	
	/**
	 * @param tiexfuli
	 */
	public void setTiexfuli(java.math.BigDecimal tiexfuli) {
		this.tiexfuli = tiexfuli;
	}
	
    /**
     * @return tiexfuli
     */
	public java.math.BigDecimal getTiexfuli() {
		return this.tiexfuli;
	}
	
	/**
	 * @param ysyjxxsh
	 */
	public void setYsyjxxsh(java.math.BigDecimal ysyjxxsh) {
		this.ysyjxxsh = ysyjxxsh;
	}
	
    /**
     * @return ysyjxxsh
     */
	public java.math.BigDecimal getYsyjxxsh() {
		return this.ysyjxxsh;
	}
	
	/**
	 * @param csyjxxsh
	 */
	public void setCsyjxxsh(java.math.BigDecimal csyjxxsh) {
		this.csyjxxsh = csyjxxsh;
	}
	
    /**
     * @return csyjxxsh
     */
	public java.math.BigDecimal getCsyjxxsh() {
		return this.csyjxxsh;
	}
	
	/**
	 * @param xxiangsh
	 */
	public void setXxiangsh(java.math.BigDecimal xxiangsh) {
		this.xxiangsh = xxiangsh;
	}
	
    /**
     * @return xxiangsh
     */
	public java.math.BigDecimal getXxiangsh() {
		return this.xxiangsh;
	}
	
	/**
	 * @param daitanlx
	 */
	public void setDaitanlx(java.math.BigDecimal daitanlx) {
		this.daitanlx = daitanlx;
	}
	
    /**
     * @return daitanlx
     */
	public java.math.BigDecimal getDaitanlx() {
		return this.daitanlx;
	}
	
	/**
	 * @param hexiaobj
	 */
	public void setHexiaobj(java.math.BigDecimal hexiaobj) {
		this.hexiaobj = hexiaobj;
	}
	
    /**
     * @return hexiaobj
     */
	public java.math.BigDecimal getHexiaobj() {
		return this.hexiaobj;
	}
	
	/**
	 * @param hexiaolx
	 */
	public void setHexiaolx(java.math.BigDecimal hexiaolx) {
		this.hexiaolx = hexiaolx;
	}
	
    /**
     * @return hexiaolx
     */
	public java.math.BigDecimal getHexiaolx() {
		return this.hexiaolx;
	}
	
	/**
	 * @param lixishru
	 */
	public void setLixishru(java.math.BigDecimal lixishru) {
		this.lixishru = lixishru;
	}
	
    /**
     * @return lixishru
     */
	public java.math.BigDecimal getLixishru() {
		return this.lixishru;
	}
	
	/**
	 * @param yingshfy
	 */
	public void setYingshfy(java.math.BigDecimal yingshfy) {
		this.yingshfy = yingshfy;
	}
	
    /**
     * @return yingshfy
     */
	public java.math.BigDecimal getYingshfy() {
		return this.yingshfy;
	}
	
	/**
	 * @param feiyshru
	 */
	public void setFeiyshru(java.math.BigDecimal feiyshru) {
		this.feiyshru = feiyshru;
	}
	
    /**
     * @return feiyshru
     */
	public java.math.BigDecimal getFeiyshru() {
		return this.feiyshru;
	}
	
	/**
	 * @param yingshfj
	 */
	public void setYingshfj(java.math.BigDecimal yingshfj) {
		this.yingshfj = yingshfj;
	}
	
    /**
     * @return yingshfj
     */
	public java.math.BigDecimal getYingshfj() {
		return this.yingshfj;
	}
	
	/**
	 * @param fjinshru
	 */
	public void setFjinshru(java.math.BigDecimal fjinshru) {
		this.fjinshru = fjinshru;
	}
	
    /**
     * @return fjinshru
     */
	public java.math.BigDecimal getFjinshru() {
		return this.fjinshru;
	}
	
	/**
	 * @param zhunbeij
	 */
	public void setZhunbeij(java.math.BigDecimal zhunbeij) {
		this.zhunbeij = zhunbeij;
	}
	
    /**
     * @return zhunbeij
     */
	public java.math.BigDecimal getZhunbeij() {
		return this.zhunbeij;
	}
	
	/**
	 * @param yijtdklx
	 */
	public void setYijtdklx(java.math.BigDecimal yijtdklx) {
		this.yijtdklx = yijtdklx;
	}
	
    /**
     * @return yijtdklx
     */
	public java.math.BigDecimal getYijtdklx() {
		return this.yijtdklx;
	}
	
	/**
	 * @param yihxbjlx
	 */
	public void setYihxbjlx(java.math.BigDecimal yihxbjlx) {
		this.yihxbjlx = yihxbjlx;
	}
	
    /**
     * @return yihxbjlx
     */
	public java.math.BigDecimal getYihxbjlx() {
		return this.yihxbjlx;
	}
	
	/**
	 * @param zhcwjyrq
	 */
	public void setZhcwjyrq(String zhcwjyrq) {
		this.zhcwjyrq = zhcwjyrq;
	}
	
    /**
     * @return zhcwjyrq
     */
	public String getZhcwjyrq() {
		return this.zhcwjyrq;
	}
	
	/**
	 * @param kshchpdm
	 */
	public void setKshchpdm(String kshchpdm) {
		this.kshchpdm = kshchpdm;
	}
	
    /**
     * @return kshchpdm
     */
	public String getKshchpdm() {
		return this.kshchpdm;
	}
	
	/**
	 * @param kshchpmc
	 */
	public void setKshchpmc(String kshchpmc) {
		this.kshchpmc = kshchpmc;
	}
	
    /**
     * @return kshchpmc
     */
	public String getKshchpmc() {
		return this.kshchpmc;
	}
	
	/**
	 * @param xtqudhao
	 */
	public void setXtqudhao(String xtqudhao) {
		this.xtqudhao = xtqudhao;
	}
	
    /**
     * @return xtqudhao
     */
	public String getXtqudhao() {
		return this.xtqudhao;
	}
	
	/**
	 * @param xtqudmch
	 */
	public void setXtqudmch(String xtqudmch) {
		this.xtqudmch = xtqudmch;
	}
	
    /**
     * @return xtqudmch
     */
	public String getXtqudmch() {
		return this.xtqudmch;
	}
	
	/**
	 * @param dkczhzhh
	 */
	public void setDkczhzhh(String dkczhzhh) {
		this.dkczhzhh = dkczhzhh;
	}
	
    /**
     * @return dkczhzhh
     */
	public String getDkczhzhh() {
		return this.dkczhzhh;
	}
	
	/**
	 * @param dkdbfshi
	 */
	public void setDkdbfshi(String dkdbfshi) {
		this.dkdbfshi = dkdbfshi;
	}
	
    /**
     * @return dkdbfshi
     */
	public String getDkdbfshi() {
		return this.dkdbfshi;
	}
	
	/**
	 * @param dkyongtu
	 */
	public void setDkyongtu(String dkyongtu) {
		this.dkyongtu = dkyongtu;
	}
	
    /**
     * @return dkyongtu
     */
	public String getDkyongtu() {
		return this.dkyongtu;
	}
	
	/**
	 * @param jiejuxzh
	 */
	public void setJiejuxzh(String jiejuxzh) {
		this.jiejuxzh = jiejuxzh;
	}
	
    /**
     * @return jiejuxzh
     */
	public String getJiejuxzh() {
		return this.jiejuxzh;
	}
	
	/**
	 * @param ziczhtai
	 */
	public void setZiczhtai(String ziczhtai) {
		this.ziczhtai = ziczhtai;
	}
	
    /**
     * @return ziczhtai
     */
	public String getZiczhtai() {
		return this.ziczhtai;
	}
	
	/**
	 * @param yewusx01
	 */
	public void setYewusx01(String yewusx01) {
		this.yewusx01 = yewusx01;
	}
	
    /**
     * @return yewusx01
     */
	public String getYewusx01() {
		return this.yewusx01;
	}
	
	/**
	 * @param ywsxms01
	 */
	public void setYwsxms01(String ywsxms01) {
		this.ywsxms01 = ywsxms01;
	}
	
    /**
     * @return ywsxms01
     */
	public String getYwsxms01() {
		return this.ywsxms01;
	}
	
	/**
	 * @param yewusx02
	 */
	public void setYewusx02(String yewusx02) {
		this.yewusx02 = yewusx02;
	}
	
    /**
     * @return yewusx02
     */
	public String getYewusx02() {
		return this.yewusx02;
	}
	
	/**
	 * @param ywsxms02
	 */
	public void setYwsxms02(String ywsxms02) {
		this.ywsxms02 = ywsxms02;
	}
	
    /**
     * @return ywsxms02
     */
	public String getYwsxms02() {
		return this.ywsxms02;
	}
	
	/**
	 * @param yewusx03
	 */
	public void setYewusx03(String yewusx03) {
		this.yewusx03 = yewusx03;
	}
	
    /**
     * @return yewusx03
     */
	public String getYewusx03() {
		return this.yewusx03;
	}
	
	/**
	 * @param ywsxms03
	 */
	public void setYwsxms03(String ywsxms03) {
		this.ywsxms03 = ywsxms03;
	}
	
    /**
     * @return ywsxms03
     */
	public String getYwsxms03() {
		return this.ywsxms03;
	}
	
	/**
	 * @param yewusx04
	 */
	public void setYewusx04(String yewusx04) {
		this.yewusx04 = yewusx04;
	}
	
    /**
     * @return yewusx04
     */
	public String getYewusx04() {
		return this.yewusx04;
	}
	
	/**
	 * @param ywsxms04
	 */
	public void setYwsxms04(String ywsxms04) {
		this.ywsxms04 = ywsxms04;
	}
	
    /**
     * @return ywsxms04
     */
	public String getYwsxms04() {
		return this.ywsxms04;
	}
	
	/**
	 * @param yewusx05
	 */
	public void setYewusx05(String yewusx05) {
		this.yewusx05 = yewusx05;
	}
	
    /**
     * @return yewusx05
     */
	public String getYewusx05() {
		return this.yewusx05;
	}
	
	/**
	 * @param ywsxms05
	 */
	public void setYwsxms05(String ywsxms05) {
		this.ywsxms05 = ywsxms05;
	}
	
    /**
     * @return ywsxms05
     */
	public String getYwsxms05() {
		return this.ywsxms05;
	}
	
	/**
	 * @param yewusx06
	 */
	public void setYewusx06(String yewusx06) {
		this.yewusx06 = yewusx06;
	}
	
    /**
     * @return yewusx06
     */
	public String getYewusx06() {
		return this.yewusx06;
	}
	
	/**
	 * @param ywsxms06
	 */
	public void setYwsxms06(String ywsxms06) {
		this.ywsxms06 = ywsxms06;
	}
	
    /**
     * @return ywsxms06
     */
	public String getYwsxms06() {
		return this.ywsxms06;
	}
	
	/**
	 * @param yewusx07
	 */
	public void setYewusx07(String yewusx07) {
		this.yewusx07 = yewusx07;
	}
	
    /**
     * @return yewusx07
     */
	public String getYewusx07() {
		return this.yewusx07;
	}
	
	/**
	 * @param ywsxms07
	 */
	public void setYwsxms07(String ywsxms07) {
		this.ywsxms07 = ywsxms07;
	}
	
    /**
     * @return ywsxms07
     */
	public String getYwsxms07() {
		return this.ywsxms07;
	}
	
	/**
	 * @param yewusx08
	 */
	public void setYewusx08(String yewusx08) {
		this.yewusx08 = yewusx08;
	}
	
    /**
     * @return yewusx08
     */
	public String getYewusx08() {
		return this.yewusx08;
	}
	
	/**
	 * @param ywsxms08
	 */
	public void setYwsxms08(String ywsxms08) {
		this.ywsxms08 = ywsxms08;
	}
	
    /**
     * @return ywsxms08
     */
	public String getYwsxms08() {
		return this.ywsxms08;
	}
	
	/**
	 * @param yewusx09
	 */
	public void setYewusx09(String yewusx09) {
		this.yewusx09 = yewusx09;
	}
	
    /**
     * @return yewusx09
     */
	public String getYewusx09() {
		return this.yewusx09;
	}
	
	/**
	 * @param ywsxms09
	 */
	public void setYwsxms09(String ywsxms09) {
		this.ywsxms09 = ywsxms09;
	}
	
    /**
     * @return ywsxms09
     */
	public String getYwsxms09() {
		return this.ywsxms09;
	}
	
	/**
	 * @param yewusx10
	 */
	public void setYewusx10(String yewusx10) {
		this.yewusx10 = yewusx10;
	}
	
    /**
     * @return yewusx10
     */
	public String getYewusx10() {
		return this.yewusx10;
	}
	
	/**
	 * @param ywsxms10
	 */
	public void setYwsxms10(String ywsxms10) {
		this.ywsxms10 = ywsxms10;
	}
	
    /**
     * @return ywsxms10
     */
	public String getYwsxms10() {
		return this.ywsxms10;
	}
	
	/**
	 * @param beizhuuu
	 */
	public void setBeizhuuu(String beizhuuu) {
		this.beizhuuu = beizhuuu;
	}
	
    /**
     * @return beizhuuu
     */
	public String getBeizhuuu() {
		return this.beizhuuu;
	}
	
	/**
	 * @param mingxixh
	 */
	public void setMingxixh(Long mingxixh) {
		this.mingxixh = mingxixh;
	}
	
    /**
     * @return mingxixh
     */
	public Long getMingxixh() {
		return this.mingxixh;
	}
	
	/**
	 * @param kaihujig
	 */
	public void setKaihujig(String kaihujig) {
		this.kaihujig = kaihujig;
	}
	
    /**
     * @return kaihujig
     */
	public String getKaihujig() {
		return this.kaihujig;
	}
	
	/**
	 * @param kaihguiy
	 */
	public void setKaihguiy(String kaihguiy) {
		this.kaihguiy = kaihguiy;
	}
	
    /**
     * @return kaihguiy
     */
	public String getKaihguiy() {
		return this.kaihguiy;
	}
	
	/**
	 * @param xiaohugy
	 */
	public void setXiaohugy(String xiaohugy) {
		this.xiaohugy = xiaohugy;
	}
	
    /**
     * @return xiaohugy
     */
	public String getXiaohugy() {
		return this.xiaohugy;
	}
	
	/**
	 * @param xiaohurq
	 */
	public void setXiaohurq(String xiaohurq) {
		this.xiaohurq = xiaohurq;
	}
	
    /**
     * @return xiaohurq
     */
	public String getXiaohurq() {
		return this.xiaohurq;
	}
	
	/**
	 * @param shlxsfhq
	 */
	public void setShlxsfhq(String shlxsfhq) {
		this.shlxsfhq = shlxsfhq;
	}
	
    /**
     * @return shlxsfhq
     */
	public String getShlxsfhq() {
		return this.shlxsfhq;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}