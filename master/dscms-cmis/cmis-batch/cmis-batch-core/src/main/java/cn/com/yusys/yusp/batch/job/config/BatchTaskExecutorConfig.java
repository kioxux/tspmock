package cn.com.yusys.yusp.batch.job.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/*
 * BatchTaskExecutor配置类
 *
 * @author tangxun
 * @version 1.0
 * @since 2020/11/4 9:15
 */
@Configuration
public class BatchTaskExecutorConfig {
    private static final Logger logger = LoggerFactory.getLogger(BatchTaskExecutorConfig.class);
    @Value("${batch.task.core-pool-size:96}")
    public int corePoolSize;
    @Value("${batch.task.max-pool-size:768}")
    public int maxPoolSize;
    @Value("${batch.task.queue-capacity:670}")
    public int queueCapacity;
    @Value("${batch.task.keep-alive-seconds:30}")
    public int keepAliveSeconds;

    @Bean(name = "batchTaskPoolExecutor")
    public ThreadPoolTaskExecutor threadPoolConfig() {
        logger.info("初始化BatchTaskExecutor配置开始");
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();// 线程池对象
        //线程最小数量
        logger.info("初始化BatchTaskExecutor配置项中线程最小数量为[{}]", corePoolSize);
        threadPoolTaskExecutor.setCorePoolSize(corePoolSize);
        //线程最大数量
        logger.info("初始化BatchTaskExecutor配置项中线程最大数量为[{}]", maxPoolSize);
        threadPoolTaskExecutor.setMaxPoolSize(maxPoolSize);
        //等待队列长度
        logger.info("初始化BatchTaskExecutor配置项中等待队列长度为[{}]", queueCapacity);
        threadPoolTaskExecutor.setQueueCapacity(queueCapacity);
        //空闲时间
        logger.info("初始化BatchTaskExecutor配置项中空闲时间为[{}]", keepAliveSeconds);
        threadPoolTaskExecutor.setKeepAliveSeconds(keepAliveSeconds);
        threadPoolTaskExecutor.setThreadNamePrefix("BatchTaskExecutor");
        threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        logger.info("初始化BatchTaskExecutor配置结束");
        return threadPoolTaskExecutor;
    }
}
