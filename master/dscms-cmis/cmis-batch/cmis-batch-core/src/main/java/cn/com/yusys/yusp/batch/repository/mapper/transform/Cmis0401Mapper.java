package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0401</br>
 * 任务名称：加工任务-业务处理-对公经营性首次检查  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0401Mapper {

    /**
     * 插入贷后检查任务表
     * @param openDay
     * @return
     */
    int insertPspTaskList(@Param("openDay") String openDay);
}
