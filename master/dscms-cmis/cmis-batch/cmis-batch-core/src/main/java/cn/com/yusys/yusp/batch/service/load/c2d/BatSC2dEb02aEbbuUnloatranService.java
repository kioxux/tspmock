/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.c2d;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.c2d.BatSC2dEb02aEbbuUnloatran;
import cn.com.yusys.yusp.batch.repository.mapper.load.c2d.BatSC2dEb02aEbbuUnloatranMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dEb02aEbbuUnloatranService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:02:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSC2dEb02aEbbuUnloatranService {
    private static final Logger logger = LoggerFactory.getLogger(BatSC2dEb02aEbbuUnloatranService.class);
    @Autowired
    private BatSC2dEb02aEbbuUnloatranMapper batSC2dEb02aEbbuUnloatranMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSC2dEb02aEbbuUnloatran selectByPrimaryKey(String id, String reportId) {
        return batSC2dEb02aEbbuUnloatranMapper.selectByPrimaryKey(id, reportId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSC2dEb02aEbbuUnloatran> selectAll(QueryModel model) {
        List<BatSC2dEb02aEbbuUnloatran> records = (List<BatSC2dEb02aEbbuUnloatran>) batSC2dEb02aEbbuUnloatranMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSC2dEb02aEbbuUnloatran> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSC2dEb02aEbbuUnloatran> list = batSC2dEb02aEbbuUnloatranMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSC2dEb02aEbbuUnloatran record) {
        return batSC2dEb02aEbbuUnloatranMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSC2dEb02aEbbuUnloatran record) {
        return batSC2dEb02aEbbuUnloatranMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSC2dEb02aEbbuUnloatran record) {
        return batSC2dEb02aEbbuUnloatranMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSC2dEb02aEbbuUnloatran record) {
        return batSC2dEb02aEbbuUnloatranMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String id, String reportId) {
        return batSC2dEb02aEbbuUnloatranMapper.deleteByPrimaryKey(id, reportId);
    }

    /**
     * 删除S表当天数据
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建二代征信系统-未结清信贷交易表[bat_s_c2d_eb02a_ebbu_unloatran]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_c2d_eb02a_ebbu_unloatran");
        logger.info("重建【C2D00005】二代征信系统-未结清信贷交易表[bat_s_c2d_eb02a_ebbu_unloatran]结束");
        return 0;
    }

    /**
     * 删除业务表数据
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteBizByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建二代征信系统-未结清信贷交易表[eb02a_ebbu_unloatran]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz", "eb02a_ebbu_unloatran");
        logger.info("重建【C2D00005】二代征信系统-未结清信贷交易表[eb02a_ebbu_unloatran]结束");
        return 0;
    }

    /**
     * 更新创建时间和修改时间
     * @param openDay
     */
    public void updateBizTime(String openDay) {
        batSC2dEb02aEbbuUnloatranMapper.updateBizTime(openDay);
    }
}
