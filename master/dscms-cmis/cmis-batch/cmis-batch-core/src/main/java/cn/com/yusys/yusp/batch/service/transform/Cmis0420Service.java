package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0420Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0420</br>
 * 任务名称：加工任务-贷后管理-清理贷后临时表数据  </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月14日 下午7:56:32
 */
@Service
@Transactional
public class Cmis0420Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0420Service.class);

    @Autowired
    private Cmis0420Mapper cmis0420Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 插入贷后检查任务表和风险分类借据信息
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0420TruncateTable(String openDay) {
        logger.info("加工任务-贷后管理-清理贷后临时表-风险分类任务表临时表数据 ,参数为 :[{}]", openDay);
        // int truncateTmpRiskTaskList = cmis0420Mapper.truncateTmpRiskTaskList(openDay);// cmis_psp.tmp_risk_task_list
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_risk_task_list");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("加工任务-贷后管理-清理贷后临时表-风险分类任务表临时表数据");

        logger.info("加工任务-贷后管理-清理贷后临时表-风险分类借据信息临时表数据 ,参数为 :[{}]", openDay);
        // int truncateTmpRiskDebitInfo = cmis0420Mapper.truncateTmpRiskDebitInfo(openDay);// cmis_psp.tmp_risk_debit_info
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_risk_debit_info");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("加工任务-贷后管理-清理贷后临时表-风险分类借据信息临时表数据");

        logger.info("加工任务-贷后管理-清理贷后临时表-清理担保合同分析临时表数据 ,参数为 :[{}]", openDay);
        // int truncateTmpRiskGuarContAnaly = cmis0420Mapper.truncateTmpRiskGuarContAnaly(openDay);// cmis_psp.tmp_risk_guar_cont_analy
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_risk_guar_cont_analy");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("加工任务-贷后管理-清理贷后临时表-清理担保合同分析临时表数据");

        logger.info("加工任务-贷后管理-清理贷后临时表-清理风险分类保证人检查临时表数据 ,参数为 :[{}]", openDay);
        // int truncateTmpRiskGuarntrList = cmis0420Mapper.truncateTmpRiskGuarntrList(openDay);// cmis_psp.tmp_risk_guarntr_list
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_risk_guarntr_list");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("加工任务-贷后管理-清理贷后临时表-清理风险分类保证人检查临时表数据");

        logger.info("加工任务-贷后管理-清理贷后临时表-清理风险分类抵质押物检查临时表数据 ,参数为 :[{}]", openDay);
        // int truncateTmpRiskPldimnList = cmis0420Mapper.truncateTmpRiskPldimnList(openDay);// cmis_psp.tmp_risk_pldimn_list
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_risk_pldimn_list");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("加工任务-贷后管理-清理贷后临时表-清理风险分类抵质押物检查临时表数据");

        logger.info("加工任务-贷后管理-清理贷后临时表-清理影响偿还因素分析临时表数据 ,参数为 :[{}]", openDay);
        // int truncateTmpRiskRepayAnaly = cmis0420Mapper.truncateTmpRiskRepayAnaly(openDay);// cmis_psp.tmp_risk_repay_analy
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_risk_repay_analy");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("加工任务-贷后管理-清理贷后临时表-清理影响偿还因素分析临时表数据");
    }
}
