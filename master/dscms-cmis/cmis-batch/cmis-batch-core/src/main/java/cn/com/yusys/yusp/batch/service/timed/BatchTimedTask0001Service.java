package cn.com.yusys.yusp.batch.service.timed;

import cn.com.yusys.yusp.batch.domain.biz.*;
import cn.com.yusys.yusp.batch.repository.mapper.timed.BatchTimedTask0001Mapper;
import cn.com.yusys.yusp.batch.service.biz.*;
import cn.com.yusys.yusp.batch.service.client.bsp.ciis2nd.credxx.CredxxService;
import cn.com.yusys.yusp.batch.service.client.bsp.outerdata.qyssxx.QyssxxService;
import cn.com.yusys.yusp.batch.service.client.bsp.outerdata.sxbzxr.SxbzxrService;
import cn.com.yusys.yusp.batch.service.client.bsp.outerdata.zsnew.ZsnewService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.req.SxbzxrReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp.SxbzxrRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 定时任务处理类:自动化贷后跑批白名单  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月20日 下午11:56:54
 */
@Service
@Transactional
public class BatchTimedTask0001Service {
    private static final Logger logger = LoggerFactory.getLogger(BatchTimedTask0001Service.class);
    @Autowired
    private CredxxService credxxService;// 查询征信
    @Autowired
    private QyssxxService qyssxxService;//查询涉诉信息
    @Autowired
    private SxbzxrService sxbzxrService;//查询失信被执行人
    @Autowired
    private ZsnewService zsnewService;// 查询工商信息
    @Autowired
    private BizQyssxxCasesService bizQyssxxCasesService;//涉诉企业涉诉信息(公开模型)案件
    @Autowired
    private BizQyssxxCountService bizQyssxxCountService;//涉诉企业涉诉信息(公开模型)统计
    @Autowired
    private BizSxbzxrService bizSxbzxrService;//失信被执行人
    @Autowired
    private BizZsnewBasicService bizZsnewBasicService;//企业照面信息
    @Autowired
    private BizZsnewAlterService bizZsnewAlterService;//企业历史变更信息
    @Autowired
    private BatchTimedTask0001Mapper batchTimedTask0001Mapper;


    /**
     * 自动化贷后跑批白名单
     *
     * @return
     */
    public int timedtask0001() throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value);
        try {
            logger.info("=======================================置空登记的自动化贷后跑批白名单报错的信息-开始=======================================");
            // 置空登记的对公自动化贷后跑批白名单报错的信息
            logger.info("置空登记的对公自动化贷后跑批白名单报错的信息-开始,请求参数为:[{}]");
            int updateErrMsgByAutoPspWhiteInfoCorp = batchTimedTask0001Mapper.updateErrMsgByAutoPspWhiteInfoCorp(new HashMap());
            logger.info("置空登记的对公自动化贷后跑批白名单报错的信息-结束,返回参数为:[{}]", updateErrMsgByAutoPspWhiteInfoCorp);
            // 置空登记的个人自动化贷后跑批白名单报错的信息
            logger.info("置空登记的个人自动化贷后跑批白名单报错的信息-开始,请求参数为:[{}]");
            int updateErrMsgByAutoPspWhiteInfoPer = batchTimedTask0001Mapper.updateErrMsgByAutoPspWhiteInfoPer(new HashMap());
            logger.info("置空登记的个人自动化贷后跑批白名单报错的信息-结束,返回参数为:[{}]", updateErrMsgByAutoPspWhiteInfoPer);

            // 查询自动化贷后对公名单
            logger.info("=======================================处理自动化贷后对公名单开始=======================================");
            Map<String, String> queryMap = new HashMap();
            queryMap.put("needQueryCrereport", BatEnums.STD_YES.key);//对公是否需要查询人行征信
            logger.info("根据对公是否需要查询人行征信[{}]查询对公自动化贷后跑批白名单开始,请求参数为:[{}]", BatEnums.STD_YES.key, JSON.toJSONString(queryMap));
            java.util.List<Map<String, String>> needQueryCrereportList = batchTimedTask0001Mapper.selectNeedQueryCrereport(queryMap);
            logger.info("根据对公是否需要查询人行征信[{}]查询对公自动化贷后跑批白名单结束,返回参数为:[{}]", BatEnums.STD_YES.key, JSON.toJSONString(needQueryCrereportList.size()));

            if (CollectionUtils.nonEmpty(needQueryCrereportList)) {
                needQueryCrereportList.stream().forEach(needQueryCrereportMap -> {
                    // 调用企业征信接口
                    CredxxReqDto credxxReqDto = credxxService.buildByNeedQueryCrereportMap(needQueryCrereportMap);
                    if (credxxReqDto != null) {
                        ResultDto<CredxxRespDto> credxxResultDto = credxxService.credxxNew(credxxReqDto);
                        String credxxCode = Optional.ofNullable(credxxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                        String credxxMeesage = Optional.ofNullable(credxxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                        CredxxRespDto credxxRespDto = null;
                        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, credxxResultDto.getCode())) {
                            //  获取相关的值并解析
                            credxxRespDto = credxxResultDto.getData();
                            String reportId = credxxRespDto.getReportId();//报告编号
                            Map<String, String> updateCrereportMap = new HashMap<>();
                            updateCrereportMap.put("cusId", needQueryCrereportMap.get("cusId"));
                            updateCrereportMap.put("needQueryCrereport", BatEnums.STD_YES.key);//对公是否需要查询人行征信
                            updateCrereportMap.put("reportId", reportId);// 报告编号
                            updateCrereportMap.put("queryCrdreportFlag", BatEnums.STD_YES.key);// 查询人行征信是否成功
                            logger.info("更新对公自动化贷后跑批白名单中报告编号开始,请求参数为:[{}]", JSON.toJSONString(updateCrereportMap));
                            int updateCrereport = batchTimedTask0001Mapper.updateCrereportMap(updateCrereportMap);
                            logger.info("更新对公自动化贷后跑批白名单中报告编号结束,返回参数为:[{}]", updateCrereport);
                        } else {
                            //  打印错误
                            logger.info("调用二代征信系统-线下查询接口报错" + credxxMeesage);
                            Map<String, String> updateCrereportMap = new HashMap<>();
                            updateCrereportMap.put("cusId", needQueryCrereportMap.get("cusId"));
                            updateCrereportMap.put("needQueryCrereport", BatEnums.STD_YES.key);//对公是否需要查询人行征信
                            updateCrereportMap.put("errorCode", credxxCode.length() > 10 ? credxxCode.substring(0, 10) : credxxCode);// 错误码
                            updateCrereportMap.put("errorInfo", credxxMeesage.length() > 500 ? credxxMeesage.substring(0, 500) : credxxMeesage);//错误信息
                            logger.info("登记对公自动化贷后跑批白名单调用征信二代返回的错误信息-开始,请求参数为:[{}]", JSON.toJSONString(updateCrereportMap));
                            int updateCrereport = batchTimedTask0001Mapper.updateCrereportErrMsgMap(updateCrereportMap);
                            logger.info("登记对公自动化贷后跑批白名单调用征信二代返回的错误信息-结束,返回参数为:[{}]", updateCrereport);
                        }
                        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value);
                    }
                });
            } else {
                logger.info("根据对公是否需要查询人行征信[{}]查询对公自动化贷后跑批白名单结果为空，不需要查询企业征信", BatEnums.STD_YES.key);
            }
            logger.info("查询对公自动化贷后跑批白名单所有数据开始");
            java.util.List<Map<String, String>> corpList = batchTimedTask0001Mapper.selectCorpList();
            logger.info("查询对公自动化贷后跑批白名单结束,返回参数为:[{}]", JSON.toJSONString(corpList.size()));

            if (CollectionUtils.nonEmpty(corpList)) {
                corpList.stream().forEach(corpMap -> {
                    String autoSerno = corpMap.get("autoSerno");
                    String cusId = corpMap.get("cusId");
                    String cusName = corpMap.get("cusName");
                    String certType = corpMap.get("certType");
                    String certCode = corpMap.get("certCode");
                    // 申请流水
                    String appNo = cusId + "-" + DateUtils.getCurrentDate(DateFormatEnum.DATETIME_COMPACT) + UUID.randomUUID().toString().substring(0, 8);
                    corpMap.put("appNo", appNo);

                    // 调用 涉诉信息查询接口
                    QyssxxReqDto qyssxxReqDto = qyssxxService.buildByResultMap(corpMap);
                    ResultDto<QyssxxRespDto> qyssxxResultDto = qyssxxService.qyssxxNew(qyssxxReqDto);
                    String qyssxxCode = Optional.ofNullable(qyssxxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String qyssxxMeesage = Optional.ofNullable(qyssxxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    QyssxxRespDto qyssxxRespDto = null;
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, qyssxxResultDto.getCode())) {


                        Map<String, String> updateCrereportMap = new HashMap<>();
                        updateCrereportMap.put("queryXaCaseFlag", BatEnums.STD_YES.key);// 查询涉诉信息查询接口 为成功
                        updateCrereportMap.put("cusId", cusId);


                        logger.info("* 更新对公自动化贷后跑批白名单中小安涉诉标志-开始,请求参数为:[{}]", JSON.toJSONString(updateCrereportMap));
                        int updateCrereportXACaseFlagMap = batchTimedTask0001Mapper.updateCrereportXACaseFlagMap(updateCrereportMap);
                        logger.info("* 更新对公自动化贷后跑批白名单中小安涉诉标志-结束,返回参数为:[{}]", updateCrereportXACaseFlagMap);


                        qyssxxRespDto = qyssxxResultDto.getData();
                        corpMap.put("queryType", "1");// 查询类型   0：查询自然人， 1：查询组织机构
                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Data qyssxxData = qyssxxRespDto.getData();
                        // 案件总数统计信息
                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Count count = qyssxxData.getCount();
                        if (Objects.nonNull(count) && null != count.getCount_total() && !"".equals(count.getCount_total())) {
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByCount(count, corpMap);
                            logger.info("对公-将案件总数统计信息插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)案件-开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByCount = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("对公-将案件总数统计信息外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)案件-结束,返回参数为:[{}]", insertByCount);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Civil civil = qyssxxData.getCivil();//	民事案件
                        if (CollectionUtils.nonEmpty(civil.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Cases> civilcases = Optional.ofNullable(civil.getCases()).orElseGet(() -> new ArrayList<>());
                            civilcases.stream().forEach(civilcase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByCivilcase(civilcase, corpMap);
                                logger.info("将民事案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByCivilcases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将民事案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByCivilcases);
                            });
                        }
                        if (Objects.nonNull(civil.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Count civilcount = civil.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByCivilcount(civilcount, corpMap);
                            logger.info("将民事案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByCivilcases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将民事案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByCivilcases);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Criminal criminal = qyssxxData.getCriminal();//	刑事案件
                        if (CollectionUtils.nonEmpty(criminal.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.criminal.Cases> criminalcases = Optional.ofNullable(criminal.getCases()).orElseGet(() -> new ArrayList<>());
                            criminalcases.stream().forEach(criminalcase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByCriminalcase(criminalcase, corpMap);
                                logger.info("将刑事案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByCriminalcases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将刑事案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByCriminalcases);
                            });
                        }
                        if (Objects.nonNull(criminal.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.criminal.Count criminalcount = criminal.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByCriminalcount(criminalcount, corpMap);
                            logger.info("将刑事案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByCriminalcases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将刑事案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByCriminalcases);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Administrative administrative = qyssxxData.getAdministrative();//	行政案件
                        if (CollectionUtils.nonEmpty(administrative.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.administrative.Cases> administrativecases = Optional.ofNullable(administrative.getCases()).orElseGet(() -> new ArrayList<>());
                            administrativecases.stream().forEach(administrativecase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByAdministrativecase(administrativecase, corpMap);
                                logger.info("将行政案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByAdministrativecases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将行政案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByAdministrativecases);
                            });
                        }
                        if (Objects.nonNull(administrative.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.administrative.Count administrativecount = administrative.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByAdministrativecount(administrativecount, corpMap);
                            logger.info("将行政案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByAdministrativecases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将行政案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByAdministrativecases);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Implement implement = qyssxxData.getImplement();//执行案件
                        if (CollectionUtils.nonEmpty(implement.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.implement.Cases> implementcases = Optional.ofNullable(implement.getCases()).orElseGet(() -> new ArrayList<>());
                            implementcases.stream().forEach(implementcase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByImplementcase(implementcase, corpMap);
                                logger.info("将执行案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByImplementcases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将执行案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByImplementcases);
                            });
                        }
                        if (Objects.nonNull(implement.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.implement.Count implementcount = implement.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByImplementcount(implementcount, corpMap);
                            logger.info("将执行案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByImplementcases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将执行案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByImplementcases);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Bankrupt bankrupt = qyssxxData.getBankrupt();//	强制清算与破产案件
                        if (CollectionUtils.nonEmpty(bankrupt.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.bankrupt.Cases> bankruptcases = Optional.ofNullable(bankrupt.getCases()).orElseGet(() -> new ArrayList<>());
                            bankruptcases.stream().forEach(bankruptcase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByBankruptcase(bankruptcase, corpMap);
                                logger.info("将强制清算与破产案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByBankruptcases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将强制清算与破产案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByBankruptcases);
                            });
                        }
                        if (Objects.nonNull(bankrupt.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.bankrupt.Count bankruptcount = bankrupt.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByBankruptcount(bankruptcount, corpMap);
                            logger.info("将强制清算与破产案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByBankruptcases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将强制清算与破产案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByBankruptcases);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Preservation preservation = qyssxxData.getPreservation();
                        if (CollectionUtils.nonEmpty(preservation.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.preservation.Cases> preservationcases = Optional.ofNullable(preservation.getCases()).orElseGet(() -> new ArrayList<>());
                            preservationcases.stream().forEach(preservationcase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByPreservationcase(preservationcase, corpMap);
                                logger.info("将Preservation案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByPreservationcases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将Preservation案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByPreservationcases);
                            });
                        }
                        if (Objects.nonNull(preservation.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.preservation.Count preservationcount = preservation.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByPreservationcount(preservationcount, corpMap);
                            logger.info("将Preservation案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByPreservationcases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将Preservation案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByPreservationcases);
                        }
                    } else {
                        //  打印错误
                        logger.info("涉诉信息查询接口报错======"+ qyssxxMeesage);
                        Map<String, String> updateCrereportMap = new HashMap<>();
                        updateCrereportMap.put("cusId", cusId);
                        updateCrereportMap.put("errorCode", qyssxxCode.length() > 10 ? qyssxxCode.substring(0, 10) : qyssxxCode);// 错误码
                        updateCrereportMap.put("errorInfo", qyssxxMeesage.length() > 500 ? qyssxxMeesage.substring(0, 500) : qyssxxMeesage);//错误信息
                        logger.info("登记对公自动化贷后跑批白名单调用涉诉信息查询返回的错误信息-开始,请求参数为:[{}]", JSON.toJSONString(updateCrereportMap));
                        int updateCrereport = batchTimedTask0001Mapper.updateCrereportErrMsgConcatMap(updateCrereportMap);
                        logger.info("登记对公自动化贷后跑批白名单调用涉诉信息查询返回的错误信息-结束,返回参数为:[{}]", updateCrereport);
                    }
                    logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value);

                    // 查询失信被执行人
                    SxbzxrReqDto sxbzxrReqDto = sxbzxrService.buildByCorpMap(corpMap);
                    ResultDto<SxbzxrRespDto> sxbzxrResultDto = sxbzxrService.sxbzxrNew(sxbzxrReqDto);
                    String sxbzxrCode = Optional.ofNullable(sxbzxrResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String sxbzxrMeesage = Optional.ofNullable(sxbzxrResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    SxbzxrRespDto sxbzxrRespDto = null;
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, sxbzxrResultDto.getCode())) {
                        //  获取相关的值并解析
                        sxbzxrRespDto = sxbzxrResultDto.getData();
                        if (Objects.nonNull(sxbzxrRespDto.getData()) && CollectionUtils.nonEmpty(sxbzxrRespDto.getData().getSxbzxr())) {
                            List<cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp.Sxbzxr> sxbzxrList = sxbzxrRespDto.getData().getSxbzxr();
                            sxbzxrList.stream().forEach(sxbzxr -> {
                                BizSxbzxr bizSxbzxr = bizSxbzxrService.buildBySxbzxr(sxbzxr, corpMap);
                                logger.info("插入到失信被执行人开始,请求参数为:[{}]", JSON.toJSONString(bizSxbzxr));
                                int insertByBizSxbzxr = bizSxbzxrService.insertSelective(bizSxbzxr);
                                logger.info("插入到失信被执行人结束,返回参数为:[{}]", insertByBizSxbzxr);
                            });
                        }
                    } else {
                        logger.info("查询失信被执行人报错======"+ sxbzxrMeesage);
                        Map<String, String> updateCrereportMap = new HashMap<>();
                        updateCrereportMap.put("cusId", cusId);
                        updateCrereportMap.put("errorCode", sxbzxrCode.length() > 10 ? sxbzxrCode.substring(0, 10) : sxbzxrCode);// 错误码
                        updateCrereportMap.put("errorInfo", sxbzxrMeesage.length() > 500 ? sxbzxrMeesage.substring(0, 500) : sxbzxrMeesage);//错误信息
                        logger.info("登记对公自动化贷后跑批白名单调用查询失信被执行人返回的错误信息-开始,请求参数为:[{}]", JSON.toJSONString(updateCrereportMap));
                        int updateCrereport = batchTimedTask0001Mapper.updateCrereportErrMsgConcatMap(updateCrereportMap);
                        logger.info("登记对公自动化贷后跑批白名单调用查询失信被执行人返回的错误信息-结束,返回参数为:[{}]", updateCrereport);
                    }
                    logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SXBZXR.key, EsbEnum.TRADE_CODE_SXBZXR.value);
                    // 查询工商信息
                    ZsnewReqDto zsnewReqDto = zsnewService.buildByCorpMap(corpMap);
                    ResultDto<ZsnewRespDto> zsnewResultDto = zsnewService.zsnew2(zsnewReqDto);
                    String zsnewCode = Optional.ofNullable(zsnewResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String zsnewMeesage = Optional.ofNullable(zsnewResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    ZsnewRespDto zsnewRespDto = null;
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, zsnewResultDto.getCode())) {
                        //  获取相关的值并解析
                        zsnewRespDto = zsnewResultDto.getData();
                        if (Objects.nonNull(zsnewRespDto.getData()) && Objects.nonNull(zsnewRespDto.getData().getENT_INFO()) && Objects.nonNull(zsnewRespDto.getData().getENT_INFO().getBASIC())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.BASIC basic = zsnewRespDto.getData().getENT_INFO().getBASIC();
                            BizZsnewBasic bizZsnewBasic = bizZsnewBasicService.buildByBasic(basic, corpMap);
                            logger.info("插入到企业照面信息开始,请求参数为:[{}]", JSON.toJSONString(bizZsnewBasic));
                            int insertByBizZsnewBasic = bizZsnewBasicService.insertSelective(bizZsnewBasic);
                            logger.info("插入到企业照面信息结束,返回参数为:[{}]", insertByBizZsnewBasic);
                        }
                        if (Objects.nonNull(zsnewRespDto.getData()) && Objects.nonNull(zsnewRespDto.getData().getENT_INFO()) && CollectionUtils.nonEmpty(zsnewRespDto.getData().getENT_INFO().getALTER())) {
                            List<cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ALTER> zsnewAlterList = zsnewRespDto.getData().getENT_INFO().getALTER();
                            zsnewAlterList.stream().forEach(zsnewAlter -> {
                                BizZsnewAlter bizZsnewAlter = bizZsnewAlterService.buildByZsnewAlter(zsnewAlter, corpMap);
                                logger.info("插入到企业历史变更信息开始,请求参数为:[{}]", JSON.toJSONString(bizZsnewAlter));
                                int insertByBizZsnewAlter = bizZsnewAlterService.insertSelective(bizZsnewAlter);
                                logger.info("插入到企业历史变更信息结束,返回参数为:[{}]", insertByBizZsnewAlter);
                            });
                        }
                    } else {
                        logger.info("查询工商信息报错=====" + zsnewMeesage);
                        Map<String, String> updateCrereportMap = new HashMap<>();
                        updateCrereportMap.put("cusId", cusId);
                        updateCrereportMap.put("errorCode", zsnewCode.length() > 10 ? zsnewCode.substring(0, 10) : zsnewCode);// 错误码
                        updateCrereportMap.put("errorInfo", zsnewMeesage.length() > 500 ? zsnewMeesage.substring(0, 500) : zsnewMeesage);//错误信息
                        logger.info("登记对公自动化贷后跑批白名单调用查询工商信息返回的错误信息-开始,请求参数为:[{}]", JSON.toJSONString(updateCrereportMap));
                        int updateCrereport = batchTimedTask0001Mapper.updateCrereportErrMsgConcatMap(updateCrereportMap);
                        logger.info("登记对公自动化贷后跑批白名单调用查询工商信息返回的错误信息-结束,返回参数为:[{}]", updateCrereport);
                    }
                    logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value);
                });
            } else {
                logger.info("查询对公自动化贷后跑批白名单所有数据为空，不需要调用外部接口");
            }
            logger.info("=======================================处理自动化贷后对公名单结束=======================================");


            logger.info("=======================================处理个人自动化贷后跑批白名单开始=======================================");
            Map<String, String> queryPerMap = new HashMap();
            queryPerMap.put("perNeedQueryCrereport", BatEnums.STD_YES.key);//个人或者实际控制人是否需要查询人行征信
            logger.info("根据个人或者实际控制人是否需要查询人行征信[{}]查询个人自动化贷后跑批白名单开始,请求参数为:[{}]", BatEnums.STD_YES.key, JSON.toJSONString(queryPerMap));
            java.util.List<Map<String, String>> perNeedQueryCrereportList = batchTimedTask0001Mapper.selectPerNeedQueryCrereport(queryPerMap);
            logger.info("根据个人或者实际控制人是否需要查询人行征信[{}]查询个人自动化贷后跑批白名单结束,返回参数为:[{}]", BatEnums.STD_YES.key, JSON.toJSONString(perNeedQueryCrereportList.size()));

            if (CollectionUtils.nonEmpty(perNeedQueryCrereportList)) {
                perNeedQueryCrereportList.stream().forEach(perNeedQueryCrereportMap -> {
                    // 调用个人征信接口
                    CredxxReqDto credxxReqDto = credxxService.buildByNeedQueryCrereportMap(perNeedQueryCrereportMap);
                    if (credxxReqDto != null) {
                        ResultDto<CredxxRespDto> credxxResultDto = credxxService.credxxNew(credxxReqDto);
                        String credxxCode = Optional.ofNullable(credxxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                        String credxxMeesage = Optional.ofNullable(credxxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                        CredxxRespDto credxxRespDto = null;
                        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, credxxResultDto.getCode())) {
                            //  获取相关的值并解析
                            credxxRespDto = credxxResultDto.getData();
                            String reportId = credxxRespDto.getReportId();//报告编号
                            Map<String, String> updateCrereportMap = new HashMap<>();
                            updateCrereportMap.put("cusId", perNeedQueryCrereportMap.get("cusId"));
                            updateCrereportMap.put("perNeedQueryCrereport", BatEnums.STD_YES.key);//对公是否需要查询人行征信
                            updateCrereportMap.put("reportId", reportId);// 报告编号
                            updateCrereportMap.put("queryCrdreportFlag", BatEnums.STD_YES.key);// 查询人行征信是否成功
                            logger.info("更新个人自动化贷后跑批白名单中报告编号开始,请求参数为:[{}]", JSON.toJSONString(updateCrereportMap));
                            int updatePerCrereport = batchTimedTask0001Mapper.updatePerCrereportMap(updateCrereportMap);
                            logger.info("更新个人自动化贷后跑批白名单中报告编号结束,返回参数为:[{}]", updatePerCrereport);
                        } else {
                            //  打印错误
                            logger.info("调用二代征信系统-线下查询接口报错" + credxxMeesage);
                            Map<String, String> updateCrereportMap = new HashMap<>();
                            updateCrereportMap.put("cusId", perNeedQueryCrereportMap.get("cusId"));
                            updateCrereportMap.put("perNeedQueryCrereport", BatEnums.STD_YES.key);//对公是否需要查询人行征信
                            updateCrereportMap.put("errorCode", credxxCode);// 错误码
                            updateCrereportMap.put("errorInfo", credxxMeesage);//错误信息
                            logger.info("登记个人自动化贷后跑批白名单调用征信二代返回的错误信息-开始,请求参数为:[{}]", JSON.toJSONString(updateCrereportMap));
                            int updateCrereport = batchTimedTask0001Mapper.updatePerCrereporErrMsgMap(updateCrereportMap);
                            logger.info("登记个人自动化贷后跑批白名单调用征信二代返回的错误信息-结束,返回参数为:[{}]", updateCrereport);
                        }
                        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value);
                    }
                });
            } else {
                logger.info("根据个人或者实际控制人是否需要查询人行征信[{}]查询个人自动化贷后跑批白名单为空，不需要查询个人征信", BatEnums.STD_YES.key);
            }

            logger.info("查询个人自动化贷后跑批白名单所有数据开始");
            java.util.List<Map<String, String>> perList = batchTimedTask0001Mapper.selectPerList();
            logger.info("查询个人自动化贷后跑批白名单结束,返回参数为:[{}]", BatEnums.STD_YES.key, JSON.toJSONString(perList.size()));
            if (CollectionUtils.nonEmpty(perList)) {
                perList.stream().forEach(perMap -> {
                    String autoSerno = perMap.get("autoSerno");
                    String cusId = perMap.get("cusId");
                    String cusName = perMap.get("cusName");
                    String certType = perMap.get("certType");
                    String certCode = perMap.get("certCode");
                    // 申请流水
                    String appNo = cusId + "-" + DateUtils.getCurrentDate(DateFormatEnum.DATETIME_COMPACT) + UUID.randomUUID().toString().substring(0, 8);
                    perMap.put("appNo", appNo);

                    // 调用 涉诉信息查询接口
                    QyssxxReqDto qyssxxReqDto = qyssxxService.buildByResultMap(perMap);
                    ResultDto<QyssxxRespDto> qyssxxResultDto = qyssxxService.qyssxxNew(qyssxxReqDto);
                    String qyssxxCode = Optional.ofNullable(qyssxxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String qyssxxMeesage = Optional.ofNullable(qyssxxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    QyssxxRespDto qyssxxRespDto = null;
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, qyssxxResultDto.getCode())) {
                        qyssxxRespDto = qyssxxResultDto.getData();
                        perMap.put("queryType", "0");// 查询类型   0：查询自然人， 1：查询组织机构
                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Data qyssxxData = qyssxxRespDto.getData();
                        // 案件总数统计信息
                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Count count = qyssxxData.getCount();
                        if (Objects.nonNull(count) && null != count.getCount_total() && !"".equals(count.getCount_total())) {
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByCount(count, perMap);
                            logger.info("个人-将案件总数统计信息插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)案件-开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByCount = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("个人-将案件总数统计信息外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)案件-结束,返回参数为:[{}]", insertByCount);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Civil civil = qyssxxData.getCivil();//	民事案件
                        if (CollectionUtils.nonEmpty(civil.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Cases> civilcases = Optional.ofNullable(civil.getCases()).orElseGet(() -> new ArrayList<>());
                            civilcases.stream().forEach(civilcase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByCivilcase(civilcase, perMap);
                                logger.info("将民事案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByCivilcases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将民事案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByCivilcases);
                            });
                        }
                        if (Objects.nonNull(civil.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Count civilcount = civil.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByCivilcount(civilcount, perMap);
                            logger.info("将民事案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByCivilcases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将民事案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByCivilcases);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Criminal criminal = qyssxxData.getCriminal();//	刑事案件
                        if (CollectionUtils.nonEmpty(criminal.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.criminal.Cases> criminalcases = Optional.ofNullable(criminal.getCases()).orElseGet(() -> new ArrayList<>());
                            criminalcases.stream().forEach(criminalcase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByCriminalcase(criminalcase, perMap);
                                logger.info("将刑事案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByCriminalcases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将刑事案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByCriminalcases);
                            });
                        }
                        if (Objects.nonNull(criminal.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.criminal.Count criminalcount = criminal.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByCriminalcount(criminalcount, perMap);
                            logger.info("将刑事案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByCriminalcases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将刑事案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByCriminalcases);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Administrative administrative = qyssxxData.getAdministrative();//	行政案件
                        if (CollectionUtils.nonEmpty(administrative.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.administrative.Cases> administrativecases = Optional.ofNullable(administrative.getCases()).orElseGet(() -> new ArrayList<>());
                            administrativecases.stream().forEach(administrativecase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByAdministrativecase(administrativecase, perMap);
                                logger.info("将行政案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByAdministrativecases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将行政案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByAdministrativecases);
                            });
                        }
                        if (Objects.nonNull(administrative.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.administrative.Count administrativecount = administrative.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByAdministrativecount(administrativecount, perMap);
                            logger.info("将行政案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByAdministrativecases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将行政案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByAdministrativecases);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Implement implement = qyssxxData.getImplement();//执行案件
                        if (CollectionUtils.nonEmpty(implement.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.implement.Cases> implementcases = Optional.ofNullable(implement.getCases()).orElseGet(() -> new ArrayList<>());
                            implementcases.stream().forEach(implementcase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByImplementcase(implementcase, perMap);
                                logger.info("将执行案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByImplementcases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将执行案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByImplementcases);
                            });
                        }
                        if (Objects.nonNull(implement.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.implement.Count implementcount = implement.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByImplementcount(implementcount, perMap);
                            logger.info("将执行案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByImplementcases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将执行案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByImplementcases);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Bankrupt bankrupt = qyssxxData.getBankrupt();//	强制清算与破产案件
                        if (CollectionUtils.nonEmpty(bankrupt.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.bankrupt.Cases> bankruptcases = Optional.ofNullable(bankrupt.getCases()).orElseGet(() -> new ArrayList<>());
                            bankruptcases.stream().forEach(bankruptcase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByBankruptcase(bankruptcase, perMap);
                                logger.info("将强制清算与破产案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByBankruptcases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将强制清算与破产案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByBankruptcases);
                            });
                        }
                        if (Objects.nonNull(bankrupt.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.bankrupt.Count bankruptcount = bankrupt.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByBankruptcount(bankruptcount, perMap);
                            logger.info("将强制清算与破产案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByBankruptcases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将强制清算与破产案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByBankruptcases);
                        }

                        cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Preservation preservation = qyssxxData.getPreservation();
                        if (CollectionUtils.nonEmpty(preservation.getCases())) {
                            java.util.List<cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.preservation.Cases> preservationcases = Optional.ofNullable(preservation.getCases()).orElseGet(() -> new ArrayList<>());
                            preservationcases.stream().forEach(preservationcase -> {
                                BizQyssxxCases bizQyssxxCases = bizQyssxxCasesService.buildByPreservationcase(preservationcase, perMap);
                                logger.info("将Preservation案件插入到涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
                                int insertByPreservationcases = bizQyssxxCasesService.insertSelective(bizQyssxxCases);
                                logger.info("将Preservation案件插入到涉诉企业涉诉信息(公开模型)案件结束,返回参数为:[{}]", insertByPreservationcases);
                            });
                        }
                        if (Objects.nonNull(preservation.getCount())) {
                            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.preservation.Count preservationcount = preservation.getCount();
                            BizQyssxxCount bizQyssxxCount = bizQyssxxCountService.buildByPreservationcount(preservationcount, perMap);
                            logger.info("将Preservation案件插入到外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
                            int insertByPreservationcases = bizQyssxxCountService.insertSelective(bizQyssxxCount);
                            logger.info("将Preservation案件插入外部数据平台-涉诉信息查询-涉诉企业涉诉信息(公开模型)统计结束,返回参数为:[{}]", insertByPreservationcases);
                        }
                    } else {
                        logger.info("查询个人-涉诉信息查询接口报错====="+ qyssxxMeesage);
                        Map<String, String> updateCrereportMap = new HashMap<>();
                        updateCrereportMap.put("cusId", cusId);
                        updateCrereportMap.put("errorCode", qyssxxCode.length() > 10 ? qyssxxCode.substring(0, 10) : qyssxxCode);// 错误码
                        updateCrereportMap.put("errorInfo", qyssxxMeesage.length() > 500 ? qyssxxMeesage.substring(0, 500) : qyssxxMeesage);//错误信息
                        logger.info("登记对公自动化贷后跑批白名单调用涉诉信息查询返回的错误信息-开始,请求参数为:[{}]", JSON.toJSONString(updateCrereportMap));
                        int updateCrereport = batchTimedTask0001Mapper.updatePerCrereporErrMsgConcatMap(updateCrereportMap);
                        logger.info("登记对公自动化贷后跑批白名单调用涉诉信息查询返回的错误信息-结束,返回参数为:[{}]", updateCrereport);
                    }

                    // 查询失信被执行人
                    SxbzxrReqDto sxbzxrReqDto = sxbzxrService.buildByCorpMap(perMap);
                    ResultDto<SxbzxrRespDto> sxbzxrResultDto = sxbzxrService.sxbzxrNew(sxbzxrReqDto);
                    String sxbzxrCode = Optional.ofNullable(sxbzxrResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String sxbzxrMeesage = Optional.ofNullable(sxbzxrResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    SxbzxrRespDto sxbzxrRespDto = null;
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, sxbzxrResultDto.getCode())) {
                        //  获取相关的值并解析
                        sxbzxrRespDto = sxbzxrResultDto.getData();
                        if (Objects.nonNull(sxbzxrRespDto.getData()) && CollectionUtils.nonEmpty(sxbzxrRespDto.getData().getSxbzxr())) {
                            List<cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp.Sxbzxr> sxbzxrList = sxbzxrRespDto.getData().getSxbzxr();
                            sxbzxrList.stream().forEach(sxbzxr -> {
                                BizSxbzxr bizSxbzxr = bizSxbzxrService.buildBySxbzxr(sxbzxr, perMap);
                                logger.info("插入到失信被执行人开始,请求参数为:[{}]", JSON.toJSONString(bizSxbzxr));
                                int insertByBizSxbzxr = bizSxbzxrService.insertSelective(bizSxbzxr);
                                logger.info("插入到失信被执行人结束,返回参数为:[{}]", insertByBizSxbzxr);
                            });
                        }
                    } else {
                        logger.info("查询个人-查询失信被执行人报错======"+ sxbzxrMeesage);
                        Map<String, String> updateCrereportMap = new HashMap<>();
                        updateCrereportMap.put("cusId", cusId);
                        updateCrereportMap.put("errorCode", sxbzxrCode.length() > 10 ? sxbzxrCode.substring(0, 10) : sxbzxrCode);// 错误码
                        updateCrereportMap.put("errorInfo", sxbzxrMeesage.length() > 500 ? sxbzxrMeesage.substring(0, 500) : sxbzxrMeesage);//错误信息
                        logger.info("登记对公自动化贷后跑批白名单调用查询失信被执行人返回的错误信息-开始,请求参数为:[{}]", JSON.toJSONString(updateCrereportMap));
                        int updateCrereport = batchTimedTask0001Mapper.updatePerCrereporErrMsgConcatMap(updateCrereportMap);
                        logger.info("登记对公自动化贷后跑批白名单调用查询失信被执行人返回的错误信息-结束,返回参数为:[{}]", updateCrereport);
                    }
                    logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SXBZXR.key, EsbEnum.TRADE_CODE_SXBZXR.value);
                });
            } else {
                logger.info("查询个人自动化贷后跑批白名单为空，不需要调用外部接口");
            }
            logger.info("=======================================处理个人自动化贷后跑批白名单结束=======================================");


        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value, e.getMessage());
//            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value);
        return 0;
    }
}
