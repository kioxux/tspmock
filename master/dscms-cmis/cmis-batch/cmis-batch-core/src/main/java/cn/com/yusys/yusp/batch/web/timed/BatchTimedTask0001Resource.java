package cn.com.yusys.yusp.batch.web.timed;

import cn.com.yusys.yusp.batch.service.timed.BatchTimedTask0001Service;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 定时任务处理类:自动化贷后跑批白名单
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "BatchTimedTask0001:自动化贷后跑批白名单")
@RestController
@RequestMapping("/api/batch4use")
public class BatchTimedTask0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BatchTimedTask0001Resource.class);
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private BatchTimedTask0001Service batchTimedTask0001Service;

    @ApiOperation("自动化贷后跑批白名单")
    @GetMapping("/timedtask0001")
    protected @ResponseBody
    ResultDto<Void> timedtask0001() throws Exception {
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        int result = 0;// 返回结果，0成功，1失败
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value, "");
        try {
            result = batchTimedTask0001Service.timedtask0001();

        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value, JSON.toJSONString(result));
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return new ResultDto<>();
    }

    @ApiOperation("自动化贷后跑批白名单")
    @PostMapping("/timedtask0001Post")
    protected @ResponseBody
    ResultDto<Void> timedtask0001Post() throws Exception {
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        int result = 0;// 返回结果，0成功，1失败
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value, "");
        try {
            result = batchTimedTask0001Service.timedtask0001();

        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value, JSON.toJSONString(result));
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0001.key, BatEnums.BATCH_TIMED_TASK0001.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return new ResultDto<>();
    }

}
