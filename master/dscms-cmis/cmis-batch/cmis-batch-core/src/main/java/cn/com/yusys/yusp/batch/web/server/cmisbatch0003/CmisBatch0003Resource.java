package cn.com.yusys.yusp.batch.web.server.cmisbatch0003;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003RespDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0003.CmisBatch0003Service;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询[贷款还款计划表]和[贷款期供交易明细]关联信息
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CMISBATCH0003:查询[贷款还款计划表]和[贷款期供交易明细]关联信息")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0003Resource.class);

    @Autowired
    private CmisBatch0003Service cmisBatch0003Service;

    /**
     * 交易码：cmisbatch0003
     * 交易描述：查询[贷款还款计划表]和[贷款期供交易明细]关联信息
     *
     * @param cmisbatch0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询[贷款还款计划表]和[贷款期供交易明细]关联信息")
    @PostMapping("/cmisbatch0003")
    protected @ResponseBody
    ResultDto<Cmisbatch0003RespDto> cmisbatch0003(@Validated @RequestBody Cmisbatch0003ReqDto cmisbatch0003ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value, JSON.toJSONString(cmisbatch0003ReqDto));
        Cmisbatch0003RespDto cmisbatch0003RespDto = new Cmisbatch0003RespDto();// 响应Dto:调度运行管理信息查詢
        ResultDto<Cmisbatch0003RespDto> cmisbatch0003ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0003ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value, JSON.toJSONString(cmisbatch0003ReqDto));
            cmisbatch0003RespDto = cmisBatch0003Service.cmisBatch0003(cmisbatch0003ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value, JSON.toJSONString(cmisbatch0003RespDto));
            // 封装cmisbatch0003ResultDto中正确的返回码和返回信息
            cmisbatch0003ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0003ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value, e.getMessage());
            // 封装cmisbatch0003ResultDto中异常返回码和返回信息
            cmisbatch0003ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0003ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0003RespDto到cmisbatch0003ResultDto中
        cmisbatch0003ResultDto.setData(cmisbatch0003RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value, JSON.toJSONString(cmisbatch0003ResultDto));
        return cmisbatch0003ResultDto;
    }
}
