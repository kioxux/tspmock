package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0111Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0111</br>
 * 任务名称：加工任务-业务处理-档案日终处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0111Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0111Service.class);

    @Autowired
    private Cmis0111Mapper cmis0111Mapper;

    /**
     * 插入消息提示表
     *
     * @param openDay
     */
    public void cmis0111InsertWbMsgNotice(String openDay) {
        logger.info("更新档案调阅申请明细表状态开始,请求参数为:[{}]", openDay);
        int uptDocReadDetailInfo = cmis0111Mapper.uptDocReadDetailInfo(openDay);
        logger.info("更新档案调阅申请明细表状态结束,返回参数为:[{}]", uptDocReadDetailInfo);

        logger.info("更新档案台账列表状态开始,请求参数为:[{}]", openDay);
        int uptDocAccList = cmis0111Mapper.uptDocAccList(openDay);
        logger.info("更新档案台账列表状态结束,返回参数为:[{}]", uptDocAccList);

        logger.info("插入消息提示表：档案归还延期提醒开始,请求参数为:[{}]", openDay);
        int insertWbMsgNotice01 = cmis0111Mapper.insertWbMsgNotice01(openDay);
        logger.info("插入消息提示表：档案归还延期提醒结束,返回参数为:[{}]", insertWbMsgNotice01);
    }


}
