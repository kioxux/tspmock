package cn.com.yusys.yusp.batch.repository.mapper.timed;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 业务逻辑Dao类：</br>
 * 定时任务处理类:营改增  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月20日 下午11:56:54
 */
public interface BatchTimedTask0005Mapper {
    /**
     * 清空表数据
     */
    void  truncateBatCoretYgz();

    /**
     * 生成营改增数据
     *
     * @return
     */
    int insertBatCoretYgz(String parm);

    /**
     * 更新营改增日志表
     *
     * @return
     */
    int insertBatCoretYgzLog(@Param("serno") String serno, @Param("taskDate") String taskDate);
    /**
     * 删除重复数据
     */
    void truncateRepeatBatCoretYgz();
}
