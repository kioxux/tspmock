package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0301Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0301</br>
 * 任务名称：加工任务-业务处理-客户信息处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0301Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0301Service.class);

    @Autowired
    private Cmis0301Mapper cmis0301Mapper;

    /**
     * 更新客户管理任务信息和客户基本信息
     *
     * @param openDay
     */
    public void cmis0301UpdateCusBase(String openDay) {
        logger.info("更新客户管理任务信息[如果临时客户5天没有维护成正式客户，则更新客户任务表的状态为已完成]开始,请求参数为:[{}]", openDay);
        int updateCusManaTask = cmis0301Mapper.updateCusManaTask(openDay);
        logger.info("更新客户管理任务信息[如果临时客户5天没有维护成正式客户，则更新客户任务表的状态为已完成]结束,返回参数为:[{}]", updateCusManaTask);

        logger.info("更新客户基本信息[如果临时客户5天没有维护成正式客户，则更新此客户的管护人管护机构为空]开始,请求参数为:[{}]", openDay);
        int updateCusBase = cmis0301Mapper.updateCusBase(openDay);
        logger.info("更新客户基本信息[如果临时客户5天没有维护成正式客户，则更新此客户的管护人管护机构为空]结束,返回参数为:[{}]", updateCusBase);

        logger.info("清空临时表-不良客户信息表 开始");
        cmis0301Mapper.truncateTmpBadCus();
        logger.info("清空临时表-不良客户信息表 结束");

        logger.info("加工临时表-不良客户信息表开始,请求参数为:[{}]", openDay);
        int insertTmpBadCus = cmis0301Mapper.insertTmpBadCus(openDay);
        logger.info("加工临时表-不良客户信息表结束,返回参数为:[{}]", insertTmpBadCus);

        logger.info("初始化对公信息表的是否不良记录开始,请求参数为:[{}]", openDay);
        int updateBadCusInitCusCorp = cmis0301Mapper.updateBadCusInitCusCorp(openDay);
        logger.info("初始化对公信息表的是否不良记录结束,返回参数为:[{}]", updateBadCusInitCusCorp);

        logger.info("更新对公信息表的是否不良记录开始,请求参数为:[{}]", openDay);
        int updateBadCusbyCusCorp = cmis0301Mapper.updateBadCusbyCusCorp(openDay);
        logger.info("更新对公信息表的是否不良记录结束,返回参数为:[{}]", updateBadCusbyCusCorp);
    }
}
