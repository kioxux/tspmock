package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0128Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0128</br>
 * 任务名称：加工任务-业务处理-零售智能风控蚂蚁改造 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2128年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0128Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0128Service.class);

    @Autowired
    private Cmis0128Mapper cmis0128Mapper;

    /**
     * 插入贷款合同表
     * @param openDay
     */
    public void cmis0128InsertCtrLoanCont(String openDay) {
        logger.info("插入贷款合同表开始,请求参数为:[{}]", openDay);
        int insertCtrLoanCont = cmis0128Mapper.insertCtrLoanCont(openDay);
        logger.info("插入贷款合同表结束,返回参数为:[{}]", insertCtrLoanCont);
    }

    /**
     * 更新贷款合同表
     * @param openDay
     */
    public void cmis0128UpdateCtrLoanCont(String openDay) {
//        logger.info("清空 临时表-蚂蚁借呗放款（合约）明细（助贷+新联营）和客户信息关系表 开始,请求参数为:[{}] ", openDay);
//        int  truncateTmpRcpAntLoanCus = cmis0128Mapper.truncateTmpRcpAntLoanCus(openDay);
//        logger.info("清空 临时表-蚂蚁借呗放款（合约）明细（助贷+新联营）和客户信息关系表 结束,返回参数为:[{}] ", truncateTmpRcpAntLoanCus);
//
//        logger.info("插入 临时表-蚂蚁借呗放款（合约）明细（助贷+新联营）和客户信息关系表 开始,请求参数为:[{}] ", openDay);
//        int  insertTmpRcpAntLoanCus = cmis0128Mapper.insertTmpRcpAntLoanCus(openDay);
//        logger.info("插入 临时表-蚂蚁借呗放款（合约）明细（助贷+新联营）和客户信息关系表 结束,返回参数为:[{}] ", insertTmpRcpAntLoanCus);
//
//        logger.info("更新贷款合同表开始,请求参数为:[{}]", openDay);
//        int updateCtrLoanCont = cmis0128Mapper.updateCtrLoanCont(openDay);
//        logger.info("更新贷款合同表结束,返回参数为:[{}]", updateCtrLoanCont);
    }

    /**
     * 插入贷款台账表
     * @param openDay
     */
    public void cmis0128InsertAccLoan(String openDay) {
        logger.info("插入贷款台账表开始,请求参数为:[{}]", openDay);
        int insertAccLoan = cmis0128Mapper.insertAccLoan(openDay);
        logger.info("插入贷款台账表结束,返回参数为:[{}]", insertAccLoan);
    }

    /**
     * 更新贷款台账表
     * @param openDay
     */
    public void cmis0128UpdateAccLoan(String openDay) {
        logger.info("更新贷款台账表[来源表为蚂蚁借呗日终（合约）信息落地表]开始,请求参数为:[{}]", openDay);
        int updateAccLoan03 = cmis0128Mapper.updateAccLoan03(openDay);
        logger.info("更新贷款台账表[来源表为蚂蚁借呗日终（合约）信息落地表]结束,返回参数为:[{}]", updateAccLoan03);
    }
}
