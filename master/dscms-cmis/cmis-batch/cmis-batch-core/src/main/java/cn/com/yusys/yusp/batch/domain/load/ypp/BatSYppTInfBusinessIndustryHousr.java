/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.ypp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSYppTInfBusinessIndustryHousr
 * @类描述: bat_s_ypp_t_inf_business_industry_housr数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 17:10:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ypp_t_inf_business_industry_housr")
public class BatSYppTInfBusinessIndustryHousr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 押品编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "GUAR_NO")
	private String guarNo;
	
	/** 一手/二手标识 **/
	@Column(name = "IS_USED", unique = false, nullable = true, length = 10)
	private String isUsed;
	
	/** 是否两证合一 **/
	@Column(name = "TWOCARD2ONE_IND", unique = false, nullable = true, length = 10)
	private String twocard2oneInd;
	
	/** 房产证（不动产权证号） **/
	@Column(name = "HOUSE_LAND_NO", unique = false, nullable = true, length = 300)
	private String houseLandNo;
	
	/** 工业房地产开发模式 **/
	@Column(name = "INDUSTRY_DECELOP_MODEL", unique = false, nullable = true, length = 10)
	private String industryDecelopModel;
	
	/** 该产证所属房产是否全部抵押 **/
	@Column(name = "HOUSE_ALL_PLEDGE_IND", unique = false, nullable = true, length = 10)
	private String houseAllPledgeInd;
	
	/** 部分抵押房产的部位描述 **/
	@Column(name = "PART_REG_POSITION_DESC", unique = false, nullable = true, length = 600)
	private String partRegPositionDesc;
	
	/** 房地产买卖合同编号 **/
	@Column(name = "BUSINESS_HOUSE_NO", unique = false, nullable = true, length = 600)
	private String businessHouseNo;
	
	/** 购买日期 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 购买价格 **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 建筑面积 **/
	@Column(name = "BUILD_AREA", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal buildArea;
	
	/** 竣工日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 建成年份 **/
	@Column(name = "ACTIVATE_YEARS", unique = false, nullable = true, length = 10)
	private String activateYears;
	
	/** 楼龄 **/
	@Column(name = "FLOOR_AGE", unique = false, nullable = true, length = 4)
	private java.math.BigDecimal floorAge;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 房产证（不动产权证）地址/期房预售合同地址 **/
	@Column(name = "POC_ADDR", unique = false, nullable = true, length = 600)
	private String pocAddr;
	
	/** 层次（标的所在楼层） **/
	@Column(name = "BDLC", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal bdlc;
	
	/** 层数（标的所在楼高） **/
	@Column(name = "BDGD", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal bdgd;
	
	/** 房地产所在地段情况 **/
	@Column(name = "HOUSE_PLACE_INFO", unique = false, nullable = true, length = 10)
	private String housePlaceInfo;
	
	/** 土地证号 **/
	@Column(name = "LAND_NO", unique = false, nullable = true, length = 450)
	private String landNo;
	
	/** 土地使用权性质 **/
	@Column(name = "LAND_USE_QUAL", unique = false, nullable = true, length = 10)
	private String landUseQual;
	
	/** 土地使用权取得方式 **/
	@Column(name = "LAND_USE_WAY", unique = false, nullable = true, length = 10)
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	@Column(name = "LAND_USE_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	@Column(name = "LAND_USE_END_DATE", unique = false, nullable = true, length = 10)
	private String landUseEndDate;
	
	/** 土地使用年限 **/
	@Column(name = "LAND_USE_YEARS", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal landUseYears;
	
	/** 土地用途 **/
	@Column(name = "LAND_PURP", unique = false, nullable = true, length = 10)
	private String landPurp;
	
	/** 土地说明 **/
	@Column(name = "LAND_EXPLAIN", unique = false, nullable = true, length = 400)
	private String landExplain;
	
	/** 门牌号/弄号 **/
	@Column(name = "HOUSE_NO", unique = false, nullable = true, length = 300)
	private String houseNo;
	
	/** 街道（村镇） **/
	@Column(name = "STREET", unique = false, nullable = true, length = 300)
	private String street;
	
	/** 土地用途其他文本输入 **/
	@Column(name = "LAND_PURP_OTHER", unique = false, nullable = true, length = 300)
	private String landPurpOther;
	
	/** 楼号 **/
	@Column(name = "BUILDING_ROOM_NUM", unique = false, nullable = true, length = 100)
	private String buildingRoomNum;
	
	/** 室号 **/
	@Column(name = "ROOM_NUM", unique = false, nullable = true, length = 100)
	private String roomNum;
	
	/** 楼盘（社区）名称 **/
	@Column(name = "COMMUNITY_NAME", unique = false, nullable = true, length = 600)
	private String communityName;
	
	/** 楼层情况 **/
	@Column(name = "FLOOR", unique = false, nullable = true, length = 100)
	private String floor;
	
	/** 房产使用情况 **/
	@Column(name = "T_USAGE", unique = false, nullable = true, length = 10)
	private String tUsage;
	
	/** 土地面积(㎡) **/
	@Column(name = "LAND_AREA", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal landArea;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 1000)
	private String remark;
	
	/** 剩余使用年限 **/
	@Column(name = "HOUSE_REMAINDER_YEAR", unique = false, nullable = true, length = 10)
	private String houseRemainderYear;
	
	/** 房屋结构 **/
	@Column(name = "HOUSE_STRUCTURE", unique = false, nullable = true, length = 10)
	private String houseStructure;
	
	/** 物业情况 **/
	@Column(name = "PROPERTY", unique = false, nullable = true, length = 10)
	private String property;
	
	/** 所属地理位置 **/
	@Column(name = "LOCATED_POSITION", unique = false, nullable = true, length = 10)
	private String locatedPosition;
	
	/** 所属地段 **/
	@Column(name = "AREA_LOCATION", unique = false, nullable = true, length = 10)
	private String areaLocation;
	
	/** 担保分类代码 **/
	@Column(name = "GUAR_TYPE_CD", unique = false, nullable = true, length = 9)
	private String guarTypeCd;
	
	/** 是否期房 **/
	@Column(name = "READY_OR_PERIOD_HOUSE", unique = false, nullable = true, length = 2)
	private String readyOrPeriodHouse;
	
	/** 房屋用途 **/
	@Column(name = "HOUSE_USAGE", unique = false, nullable = true, length = 9)
	private String houseUsage;
	
	/** 土地用途（慧押押） **/
	@Column(name = "LAND_USAGE", unique = false, nullable = true, length = 9)
	private String landUsage;
	
	/** 实时查封状态 **/
	@Column(name = "SS_CF_STATUS", unique = false, nullable = true, length = 5)
	private String ssCfStatus;
	
	/** 查封时间 **/
	@Column(name = "CF_DATE", unique = false, nullable = true, length = 10)
	private String cfDate;
	
	/** 最高可抵押顺位 **/
	@Column(name = "MAX_YP_STATUS", unique = false, nullable = true, length = 5)
	private String maxYpStatus;
	
	/** 不动产单元号 **/
	@Column(name = "BDCDYH_NO", unique = false, nullable = true, length = 100)
	private String bdcdyhNo;
	
	/** 状态 **/
	@Column(name = "INSPECT_STATUS", unique = false, nullable = true, length = 2)
	private String inspectStatus;
	
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param isUsed
	 */
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed;
	}
	
    /**
     * @return isUsed
     */
	public String getIsUsed() {
		return this.isUsed;
	}
	
	/**
	 * @param twocard2oneInd
	 */
	public void setTwocard2oneInd(String twocard2oneInd) {
		this.twocard2oneInd = twocard2oneInd;
	}
	
    /**
     * @return twocard2oneInd
     */
	public String getTwocard2oneInd() {
		return this.twocard2oneInd;
	}
	
	/**
	 * @param houseLandNo
	 */
	public void setHouseLandNo(String houseLandNo) {
		this.houseLandNo = houseLandNo;
	}
	
    /**
     * @return houseLandNo
     */
	public String getHouseLandNo() {
		return this.houseLandNo;
	}
	
	/**
	 * @param industryDecelopModel
	 */
	public void setIndustryDecelopModel(String industryDecelopModel) {
		this.industryDecelopModel = industryDecelopModel;
	}
	
    /**
     * @return industryDecelopModel
     */
	public String getIndustryDecelopModel() {
		return this.industryDecelopModel;
	}
	
	/**
	 * @param houseAllPledgeInd
	 */
	public void setHouseAllPledgeInd(String houseAllPledgeInd) {
		this.houseAllPledgeInd = houseAllPledgeInd;
	}
	
    /**
     * @return houseAllPledgeInd
     */
	public String getHouseAllPledgeInd() {
		return this.houseAllPledgeInd;
	}
	
	/**
	 * @param partRegPositionDesc
	 */
	public void setPartRegPositionDesc(String partRegPositionDesc) {
		this.partRegPositionDesc = partRegPositionDesc;
	}
	
    /**
     * @return partRegPositionDesc
     */
	public String getPartRegPositionDesc() {
		return this.partRegPositionDesc;
	}
	
	/**
	 * @param businessHouseNo
	 */
	public void setBusinessHouseNo(String businessHouseNo) {
		this.businessHouseNo = businessHouseNo;
	}
	
    /**
     * @return businessHouseNo
     */
	public String getBusinessHouseNo() {
		return this.businessHouseNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(java.math.BigDecimal buildArea) {
		this.buildArea = buildArea;
	}
	
    /**
     * @return buildArea
     */
	public java.math.BigDecimal getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param activateYears
	 */
	public void setActivateYears(String activateYears) {
		this.activateYears = activateYears;
	}
	
    /**
     * @return activateYears
     */
	public String getActivateYears() {
		return this.activateYears;
	}
	
	/**
	 * @param floorAge
	 */
	public void setFloorAge(java.math.BigDecimal floorAge) {
		this.floorAge = floorAge;
	}
	
    /**
     * @return floorAge
     */
	public java.math.BigDecimal getFloorAge() {
		return this.floorAge;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param pocAddr
	 */
	public void setPocAddr(String pocAddr) {
		this.pocAddr = pocAddr;
	}
	
    /**
     * @return pocAddr
     */
	public String getPocAddr() {
		return this.pocAddr;
	}
	
	/**
	 * @param bdlc
	 */
	public void setBdlc(java.math.BigDecimal bdlc) {
		this.bdlc = bdlc;
	}
	
    /**
     * @return bdlc
     */
	public java.math.BigDecimal getBdlc() {
		return this.bdlc;
	}
	
	/**
	 * @param bdgd
	 */
	public void setBdgd(java.math.BigDecimal bdgd) {
		this.bdgd = bdgd;
	}
	
    /**
     * @return bdgd
     */
	public java.math.BigDecimal getBdgd() {
		return this.bdgd;
	}
	
	/**
	 * @param housePlaceInfo
	 */
	public void setHousePlaceInfo(String housePlaceInfo) {
		this.housePlaceInfo = housePlaceInfo;
	}
	
    /**
     * @return housePlaceInfo
     */
	public String getHousePlaceInfo() {
		return this.housePlaceInfo;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo;
	}
	
    /**
     * @return landNo
     */
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual;
	}
	
    /**
     * @return landUseQual
     */
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay;
	}
	
    /**
     * @return landUseWay
     */
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate;
	}
	
    /**
     * @return landUseBeginDate
     */
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate;
	}
	
    /**
     * @return landUseEndDate
     */
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(java.math.BigDecimal landUseYears) {
		this.landUseYears = landUseYears;
	}
	
    /**
     * @return landUseYears
     */
	public java.math.BigDecimal getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp;
	}
	
    /**
     * @return landPurp
     */
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain;
	}
	
    /**
     * @return landExplain
     */
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param houseNo
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}
	
    /**
     * @return houseNo
     */
	public String getHouseNo() {
		return this.houseNo;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param landPurpOther
	 */
	public void setLandPurpOther(String landPurpOther) {
		this.landPurpOther = landPurpOther;
	}
	
    /**
     * @return landPurpOther
     */
	public String getLandPurpOther() {
		return this.landPurpOther;
	}
	
	/**
	 * @param buildingRoomNum
	 */
	public void setBuildingRoomNum(String buildingRoomNum) {
		this.buildingRoomNum = buildingRoomNum;
	}
	
    /**
     * @return buildingRoomNum
     */
	public String getBuildingRoomNum() {
		return this.buildingRoomNum;
	}
	
	/**
	 * @param roomNum
	 */
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}
	
    /**
     * @return roomNum
     */
	public String getRoomNum() {
		return this.roomNum;
	}
	
	/**
	 * @param communityName
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}
	
    /**
     * @return communityName
     */
	public String getCommunityName() {
		return this.communityName;
	}
	
	/**
	 * @param floor
	 */
	public void setFloor(String floor) {
		this.floor = floor;
	}
	
    /**
     * @return floor
     */
	public String getFloor() {
		return this.floor;
	}
	
	/**
	 * @param tUsage
	 */
	public void setTUsage(String tUsage) {
		this.tUsage = tUsage;
	}
	
    /**
     * @return tUsage
     */
	public String getTUsage() {
		return this.tUsage;
	}
	
	/**
	 * @param landArea
	 */
	public void setLandArea(java.math.BigDecimal landArea) {
		this.landArea = landArea;
	}
	
    /**
     * @return landArea
     */
	public java.math.BigDecimal getLandArea() {
		return this.landArea;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param houseRemainderYear
	 */
	public void setHouseRemainderYear(String houseRemainderYear) {
		this.houseRemainderYear = houseRemainderYear;
	}
	
    /**
     * @return houseRemainderYear
     */
	public String getHouseRemainderYear() {
		return this.houseRemainderYear;
	}
	
	/**
	 * @param houseStructure
	 */
	public void setHouseStructure(String houseStructure) {
		this.houseStructure = houseStructure;
	}
	
    /**
     * @return houseStructure
     */
	public String getHouseStructure() {
		return this.houseStructure;
	}
	
	/**
	 * @param property
	 */
	public void setProperty(String property) {
		this.property = property;
	}
	
    /**
     * @return property
     */
	public String getProperty() {
		return this.property;
	}
	
	/**
	 * @param locatedPosition
	 */
	public void setLocatedPosition(String locatedPosition) {
		this.locatedPosition = locatedPosition;
	}
	
    /**
     * @return locatedPosition
     */
	public String getLocatedPosition() {
		return this.locatedPosition;
	}
	
	/**
	 * @param areaLocation
	 */
	public void setAreaLocation(String areaLocation) {
		this.areaLocation = areaLocation;
	}
	
    /**
     * @return areaLocation
     */
	public String getAreaLocation() {
		return this.areaLocation;
	}
	
	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}
	
    /**
     * @return guarTypeCd
     */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}
	
	/**
	 * @param readyOrPeriodHouse
	 */
	public void setReadyOrPeriodHouse(String readyOrPeriodHouse) {
		this.readyOrPeriodHouse = readyOrPeriodHouse;
	}
	
    /**
     * @return readyOrPeriodHouse
     */
	public String getReadyOrPeriodHouse() {
		return this.readyOrPeriodHouse;
	}
	
	/**
	 * @param houseUsage
	 */
	public void setHouseUsage(String houseUsage) {
		this.houseUsage = houseUsage;
	}
	
    /**
     * @return houseUsage
     */
	public String getHouseUsage() {
		return this.houseUsage;
	}
	
	/**
	 * @param landUsage
	 */
	public void setLandUsage(String landUsage) {
		this.landUsage = landUsage;
	}
	
    /**
     * @return landUsage
     */
	public String getLandUsage() {
		return this.landUsage;
	}
	
	/**
	 * @param ssCfStatus
	 */
	public void setSsCfStatus(String ssCfStatus) {
		this.ssCfStatus = ssCfStatus;
	}
	
    /**
     * @return ssCfStatus
     */
	public String getSsCfStatus() {
		return this.ssCfStatus;
	}
	
	/**
	 * @param cfDate
	 */
	public void setCfDate(String cfDate) {
		this.cfDate = cfDate;
	}
	
    /**
     * @return cfDate
     */
	public String getCfDate() {
		return this.cfDate;
	}
	
	/**
	 * @param maxYpStatus
	 */
	public void setMaxYpStatus(String maxYpStatus) {
		this.maxYpStatus = maxYpStatus;
	}
	
    /**
     * @return maxYpStatus
     */
	public String getMaxYpStatus() {
		return this.maxYpStatus;
	}
	
	/**
	 * @param bdcdyhNo
	 */
	public void setBdcdyhNo(String bdcdyhNo) {
		this.bdcdyhNo = bdcdyhNo;
	}
	
    /**
     * @return bdcdyhNo
     */
	public String getBdcdyhNo() {
		return this.bdcdyhNo;
	}
	
	/**
	 * @param inspectStatus
	 */
	public void setInspectStatus(String inspectStatus) {
		this.inspectStatus = inspectStatus;
	}
	
    /**
     * @return inspectStatus
     */
	public String getInspectStatus() {
		return this.inspectStatus;
	}


}