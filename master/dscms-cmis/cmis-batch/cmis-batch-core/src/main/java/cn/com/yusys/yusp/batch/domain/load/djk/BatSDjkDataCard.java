/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.djk;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSDjkDataCard
 * @类描述: bat_s_djk_data_card数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:35:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_djk_data_card")
public class BatSDjkDataCard extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 卡号 **/
	@Id
	@Column(name = "LOGICAL_CARD_NO")
	private String logicalCardNo;
	
	/** 客户编号 **/
	@Id
	@Column(name = "CUST_ID")
	private String custId;
	
	/** 账户编号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 9)
	private String acctNo;
	
	/** 账户编号 **/
	@Column(name = "PRODUCT_CD", unique = false, nullable = true, length = 6)
	private String productCd;
	
	/** 产品代码 **/
	@Column(name = "APP_NO", unique = false, nullable = true, length = 20)
	private String appNo;
	
	/** 申请件编号 **/
	@Column(name = "BSC_SUPP_IND", unique = false, nullable = true, length = 1)
	private String bscSuppInd;
	
	/** 主附卡指示 **/
	@Column(name = "BSC_LOGICCARD_NO", unique = false, nullable = true, length = 19)
	private String bscLogiccardNo;
	
	/** 主卡卡号 **/
	@Column(name = "OWNING_BRANCH", unique = false, nullable = true, length = 9)
	private String owningBranch;
	
	/** 发卡网点 **/
	@Column(name = "RECOM_NAME", unique = false, nullable = true, length = 80)
	private String recomName;
	
	/** 推荐人姓名 **/
	@Column(name = "RECOM_CARD_NO", unique = false, nullable = true, length = 19)
	private String recomCardNo;
	
	/** 推荐人卡号 **/
	@Column(name = "SETUP_DATE", unique = false, nullable = true, length = 8)
	private String setupDate;
	
	/** 创建日期 **/
	@Column(name = "BLOCK_CODE", unique = false, nullable = true, length = 27)
	private String blockCode;
	
	/** 锁定码 **/
	@Column(name = "ACTIVATE_IND", unique = false, nullable = true, length = 1)
	private String activateInd;
	
	/** 是否已激活 **/
	@Column(name = "ACTIVATE_DATE", unique = false, nullable = true, length = 8)
	private String activateDate;
	
	/** 激活日期 **/
	@Column(name = "CANCEL_DATE", unique = false, nullable = true, length = 8)
	private String cancelDate;
	
	/** 销卡日期 **/
	@Column(name = "APP_SOURCE", unique = false, nullable = true, length = 20)
	private String appSource;
	
	/** 申请来源 **/
	@Column(name = "REPRESENT_NO", unique = false, nullable = true, length = 20)
	private String representNo;
	
	/** 客户经理编号 **/
	@Column(name = "REPRESENT_NAME", unique = false, nullable = true, length = 80)
	private String representName;
	
	/** 客户经理 **/
	@Column(name = "POS_PIN_VERIFY_IND", unique = false, nullable = true, length = 1)
	private String posPinVerifyInd;
	
	/** 是否消费凭密 **/
	@Column(name = "RELATIONSHIP_TO_BSC", unique = false, nullable = true, length = 1)
	private String relationshipToBsc;
	
	/** 与主卡持卡人关系 **/
	@Column(name = "CARD_EXPIRE_DATE", unique = false, nullable = true, length = 6)
	private String cardExpireDate;
	
	/** 卡片有效日期 **/
	@Column(name = "RENEW_IND", unique = false, nullable = true, length = 1)
	private String renewInd;
	
	/** 续卡标识 **/
	@Column(name = "RENEW_REJECT_CD", unique = false, nullable = true, length = 2)
	private String renewRejectCd;
	
	/** 续卡拒绝原因码 **/
	@Column(name = "FIRST_USAGE_FLAG", unique = false, nullable = true, length = 1)
	private String firstUsageFlag;
	
	/** 卡片首次用卡标志 **/
	@Column(name = "WAIVE_CARDFEE_IND", unique = false, nullable = true, length = 1)
	private String waiveCardfeeInd;
	
	/** 是否免除年费 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 300)
	private String remark;
	
	/** 备注 **/
	@Column(name = "LATEST_CARD_NO", unique = false, nullable = true, length = 19)
	private String latestCardNo;
	
	/** 最新介质卡号 **/
	@Column(name = "BARCODE", unique = false, nullable = true, length = 40)
	private String barcode;
	
	/** 申请书条码 **/
	@Column(name = "BANK_CUSTOMER_ID", unique = false, nullable = true, length = 20)
	private String bankCustomerId;
	
	/** 行内客户号 **/
	@Column(name = "EMB_NAME", unique = false, nullable = true, length = 26)
	private String embName;
	
	/** 主卡压花名 **/
	@Column(name = "LAST_STATUS_DATE", unique = false, nullable = true, length = 8)
	private String lastStatusDate;
	
	/** 卡片状态设置日期 **/
	@Column(name = "CANCEL_REASON", unique = false, nullable = true, length = 1)
	private String cancelReason;
	
	/** 销卡原因 **/
	@Column(name = "FIRST_USAGE_DATE", unique = false, nullable = true, length = 8)
	private String firstUsageDate;
	
	/** 首次用卡日期 **/
	@Column(name = "LAST_MODIFIED_DATE", unique = false, nullable = true, length = 14)
	private String lastModifiedDate;
	
	/** 最近一次维护日期 **/
	@Column(name = "CARD_FETCH_METHOD", unique = false, nullable = true, length = 1)
	private String cardFetchMethod;
	
	/** 介质卡领取方式 **/
	@Column(name = "CARD_MAILER_IND", unique = false, nullable = true, length = 1)
	private String cardMailerInd;
	
	/** 卡片寄送地址标志 **/
	@Column(name = "FETCH_BRANCH", unique = false, nullable = true, length = 9)
	private String fetchBranch;
	
	/** 领卡网点 **/
	@Column(name = "FILLER", unique = false, nullable = true, length = 72)
	private String filler;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param logicalCardNo
	 */
	public void setLogicalCardNo(String logicalCardNo) {
		this.logicalCardNo = logicalCardNo;
	}
	
    /**
     * @return logicalCardNo
     */
	public String getLogicalCardNo() {
		return this.logicalCardNo;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param productCd
	 */
	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}
	
    /**
     * @return productCd
     */
	public String getProductCd() {
		return this.productCd;
	}
	
	/**
	 * @param appNo
	 */
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	
    /**
     * @return appNo
     */
	public String getAppNo() {
		return this.appNo;
	}
	
	/**
	 * @param bscSuppInd
	 */
	public void setBscSuppInd(String bscSuppInd) {
		this.bscSuppInd = bscSuppInd;
	}
	
    /**
     * @return bscSuppInd
     */
	public String getBscSuppInd() {
		return this.bscSuppInd;
	}
	
	/**
	 * @param bscLogiccardNo
	 */
	public void setBscLogiccardNo(String bscLogiccardNo) {
		this.bscLogiccardNo = bscLogiccardNo;
	}
	
    /**
     * @return bscLogiccardNo
     */
	public String getBscLogiccardNo() {
		return this.bscLogiccardNo;
	}
	
	/**
	 * @param owningBranch
	 */
	public void setOwningBranch(String owningBranch) {
		this.owningBranch = owningBranch;
	}
	
    /**
     * @return owningBranch
     */
	public String getOwningBranch() {
		return this.owningBranch;
	}
	
	/**
	 * @param recomName
	 */
	public void setRecomName(String recomName) {
		this.recomName = recomName;
	}
	
    /**
     * @return recomName
     */
	public String getRecomName() {
		return this.recomName;
	}
	
	/**
	 * @param recomCardNo
	 */
	public void setRecomCardNo(String recomCardNo) {
		this.recomCardNo = recomCardNo;
	}
	
    /**
     * @return recomCardNo
     */
	public String getRecomCardNo() {
		return this.recomCardNo;
	}
	
	/**
	 * @param setupDate
	 */
	public void setSetupDate(String setupDate) {
		this.setupDate = setupDate;
	}
	
    /**
     * @return setupDate
     */
	public String getSetupDate() {
		return this.setupDate;
	}
	
	/**
	 * @param blockCode
	 */
	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}
	
    /**
     * @return blockCode
     */
	public String getBlockCode() {
		return this.blockCode;
	}
	
	/**
	 * @param activateInd
	 */
	public void setActivateInd(String activateInd) {
		this.activateInd = activateInd;
	}
	
    /**
     * @return activateInd
     */
	public String getActivateInd() {
		return this.activateInd;
	}
	
	/**
	 * @param activateDate
	 */
	public void setActivateDate(String activateDate) {
		this.activateDate = activateDate;
	}
	
    /**
     * @return activateDate
     */
	public String getActivateDate() {
		return this.activateDate;
	}
	
	/**
	 * @param cancelDate
	 */
	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}
	
    /**
     * @return cancelDate
     */
	public String getCancelDate() {
		return this.cancelDate;
	}
	
	/**
	 * @param appSource
	 */
	public void setAppSource(String appSource) {
		this.appSource = appSource;
	}
	
    /**
     * @return appSource
     */
	public String getAppSource() {
		return this.appSource;
	}
	
	/**
	 * @param representNo
	 */
	public void setRepresentNo(String representNo) {
		this.representNo = representNo;
	}
	
    /**
     * @return representNo
     */
	public String getRepresentNo() {
		return this.representNo;
	}
	
	/**
	 * @param representName
	 */
	public void setRepresentName(String representName) {
		this.representName = representName;
	}
	
    /**
     * @return representName
     */
	public String getRepresentName() {
		return this.representName;
	}
	
	/**
	 * @param posPinVerifyInd
	 */
	public void setPosPinVerifyInd(String posPinVerifyInd) {
		this.posPinVerifyInd = posPinVerifyInd;
	}
	
    /**
     * @return posPinVerifyInd
     */
	public String getPosPinVerifyInd() {
		return this.posPinVerifyInd;
	}
	
	/**
	 * @param relationshipToBsc
	 */
	public void setRelationshipToBsc(String relationshipToBsc) {
		this.relationshipToBsc = relationshipToBsc;
	}
	
    /**
     * @return relationshipToBsc
     */
	public String getRelationshipToBsc() {
		return this.relationshipToBsc;
	}
	
	/**
	 * @param cardExpireDate
	 */
	public void setCardExpireDate(String cardExpireDate) {
		this.cardExpireDate = cardExpireDate;
	}
	
    /**
     * @return cardExpireDate
     */
	public String getCardExpireDate() {
		return this.cardExpireDate;
	}
	
	/**
	 * @param renewInd
	 */
	public void setRenewInd(String renewInd) {
		this.renewInd = renewInd;
	}
	
    /**
     * @return renewInd
     */
	public String getRenewInd() {
		return this.renewInd;
	}
	
	/**
	 * @param renewRejectCd
	 */
	public void setRenewRejectCd(String renewRejectCd) {
		this.renewRejectCd = renewRejectCd;
	}
	
    /**
     * @return renewRejectCd
     */
	public String getRenewRejectCd() {
		return this.renewRejectCd;
	}
	
	/**
	 * @param firstUsageFlag
	 */
	public void setFirstUsageFlag(String firstUsageFlag) {
		this.firstUsageFlag = firstUsageFlag;
	}
	
    /**
     * @return firstUsageFlag
     */
	public String getFirstUsageFlag() {
		return this.firstUsageFlag;
	}
	
	/**
	 * @param waiveCardfeeInd
	 */
	public void setWaiveCardfeeInd(String waiveCardfeeInd) {
		this.waiveCardfeeInd = waiveCardfeeInd;
	}
	
    /**
     * @return waiveCardfeeInd
     */
	public String getWaiveCardfeeInd() {
		return this.waiveCardfeeInd;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param latestCardNo
	 */
	public void setLatestCardNo(String latestCardNo) {
		this.latestCardNo = latestCardNo;
	}
	
    /**
     * @return latestCardNo
     */
	public String getLatestCardNo() {
		return this.latestCardNo;
	}
	
	/**
	 * @param barcode
	 */
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	
    /**
     * @return barcode
     */
	public String getBarcode() {
		return this.barcode;
	}
	
	/**
	 * @param bankCustomerId
	 */
	public void setBankCustomerId(String bankCustomerId) {
		this.bankCustomerId = bankCustomerId;
	}
	
    /**
     * @return bankCustomerId
     */
	public String getBankCustomerId() {
		return this.bankCustomerId;
	}
	
	/**
	 * @param embName
	 */
	public void setEmbName(String embName) {
		this.embName = embName;
	}
	
    /**
     * @return embName
     */
	public String getEmbName() {
		return this.embName;
	}
	
	/**
	 * @param lastStatusDate
	 */
	public void setLastStatusDate(String lastStatusDate) {
		this.lastStatusDate = lastStatusDate;
	}
	
    /**
     * @return lastStatusDate
     */
	public String getLastStatusDate() {
		return this.lastStatusDate;
	}
	
	/**
	 * @param cancelReason
	 */
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	
    /**
     * @return cancelReason
     */
	public String getCancelReason() {
		return this.cancelReason;
	}
	
	/**
	 * @param firstUsageDate
	 */
	public void setFirstUsageDate(String firstUsageDate) {
		this.firstUsageDate = firstUsageDate;
	}
	
    /**
     * @return firstUsageDate
     */
	public String getFirstUsageDate() {
		return this.firstUsageDate;
	}
	
	/**
	 * @param lastModifiedDate
	 */
	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
    /**
     * @return lastModifiedDate
     */
	public String getLastModifiedDate() {
		return this.lastModifiedDate;
	}
	
	/**
	 * @param cardFetchMethod
	 */
	public void setCardFetchMethod(String cardFetchMethod) {
		this.cardFetchMethod = cardFetchMethod;
	}
	
    /**
     * @return cardFetchMethod
     */
	public String getCardFetchMethod() {
		return this.cardFetchMethod;
	}
	
	/**
	 * @param cardMailerInd
	 */
	public void setCardMailerInd(String cardMailerInd) {
		this.cardMailerInd = cardMailerInd;
	}
	
    /**
     * @return cardMailerInd
     */
	public String getCardMailerInd() {
		return this.cardMailerInd;
	}
	
	/**
	 * @param fetchBranch
	 */
	public void setFetchBranch(String fetchBranch) {
		this.fetchBranch = fetchBranch;
	}
	
    /**
     * @return fetchBranch
     */
	public String getFetchBranch() {
		return this.fetchBranch;
	}
	
	/**
	 * @param filler
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
    /**
     * @return filler
     */
	public String getFiller() {
		return this.filler;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}