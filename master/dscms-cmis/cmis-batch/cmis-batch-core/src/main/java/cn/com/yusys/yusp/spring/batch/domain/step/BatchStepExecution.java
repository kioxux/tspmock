/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.domain.step;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchStepExecution
 * @类描述: batch_step_execution数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:02:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "batch_step_execution")
public class BatchStepExecution extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 作业步实例ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "STEP_EXECUTION_ID")
	private Long stepExecutionId;
	
	/** 版本号 **/
	@Column(name = "VERSION", unique = false, nullable = false, length = 19)
	private Long version;
	
	/** 作业步名字 **/
	@Column(name = "STEP_NAME", unique = false, nullable = false, length = 100)
	private String stepName;
	
	/** 作业执行器ID **/
	@Column(name = "JOB_EXECUTION_ID", unique = false, nullable = false, length = 19)
	private Long jobExecutionId;
	
	/** 开始时间 **/
	@Column(name = "START_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date startTime;
	
	/** 结束时间 **/
	@Column(name = "END_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date endTime;
	
	/** 执行状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 10)
	private String status;
	
	/** 事物提交次数 **/
	@Column(name = "COMMIT_COUNT", unique = false, nullable = true, length = 19)
	private Long commitCount;
	
	/** 读数据次数 **/
	@Column(name = "READ_COUNT", unique = false, nullable = true, length = 19)
	private Long readCount;
	
	/** 过滤掉的数据次数 **/
	@Column(name = "FILTER_COUNT", unique = false, nullable = true, length = 19)
	private Long filterCount;
	
	/** 写数据次数 **/
	@Column(name = "WRITE_COUNT", unique = false, nullable = true, length = 19)
	private Long writeCount;
	
	/** 读数据跳过的次数 **/
	@Column(name = "READ_SKIP_COUNT", unique = false, nullable = true, length = 19)
	private Long readSkipCount;
	
	/** 写数据跳过的次数 **/
	@Column(name = "WRITE_SKIP_COUNT", unique = false, nullable = true, length = 19)
	private Long writeSkipCount;
	
	/** 处理数据跳过的次数 **/
	@Column(name = "PROCESS_SKIP_COUNT", unique = false, nullable = true, length = 19)
	private Long processSkipCount;
	
	/** 事物回滚次数 **/
	@Column(name = "ROLLBACK_COUNT", unique = false, nullable = true, length = 19)
	private Long rollbackCount;
	
	/** 退出编码 **/
	@Column(name = "EXIT_CODE", unique = false, nullable = true, length = 2500)
	private String exitCode;
	
	/** 退出描述 **/
	@Column(name = "EXIT_MESSAGE", unique = false, nullable = true, length = 2500)
	private String exitMessage;
	
	/** 上次更新时间 **/
	@Column(name = "LAST_UPDATED", unique = false, nullable = true, length = 19)
	private java.util.Date lastUpdated;
	
	
	/**
	 * @param stepExecutionId
	 */
	public void setStepExecutionId(Long stepExecutionId) {
		this.stepExecutionId = stepExecutionId;
	}
	
    /**
     * @return stepExecutionId
     */
	public Long getStepExecutionId() {
		return this.stepExecutionId;
	}
	
	/**
	 * @param version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}
	
    /**
     * @return version
     */
	public Long getVersion() {
		return this.version;
	}
	
	/**
	 * @param stepName
	 */
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	
    /**
     * @return stepName
     */
	public String getStepName() {
		return this.stepName;
	}
	
	/**
	 * @param jobExecutionId
	 */
	public void setJobExecutionId(Long jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}
	
    /**
     * @return jobExecutionId
     */
	public Long getJobExecutionId() {
		return this.jobExecutionId;
	}
	
	/**
	 * @param startTime
	 */
	public void setStartTime(java.util.Date startTime) {
		this.startTime = startTime;
	}
	
    /**
     * @return startTime
     */
	public java.util.Date getStartTime() {
		return this.startTime;
	}
	
	/**
	 * @param endTime
	 */
	public void setEndTime(java.util.Date endTime) {
		this.endTime = endTime;
	}
	
    /**
     * @return endTime
     */
	public java.util.Date getEndTime() {
		return this.endTime;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param commitCount
	 */
	public void setCommitCount(Long commitCount) {
		this.commitCount = commitCount;
	}
	
    /**
     * @return commitCount
     */
	public Long getCommitCount() {
		return this.commitCount;
	}
	
	/**
	 * @param readCount
	 */
	public void setReadCount(Long readCount) {
		this.readCount = readCount;
	}
	
    /**
     * @return readCount
     */
	public Long getReadCount() {
		return this.readCount;
	}
	
	/**
	 * @param filterCount
	 */
	public void setFilterCount(Long filterCount) {
		this.filterCount = filterCount;
	}
	
    /**
     * @return filterCount
     */
	public Long getFilterCount() {
		return this.filterCount;
	}
	
	/**
	 * @param writeCount
	 */
	public void setWriteCount(Long writeCount) {
		this.writeCount = writeCount;
	}
	
    /**
     * @return writeCount
     */
	public Long getWriteCount() {
		return this.writeCount;
	}
	
	/**
	 * @param readSkipCount
	 */
	public void setReadSkipCount(Long readSkipCount) {
		this.readSkipCount = readSkipCount;
	}
	
    /**
     * @return readSkipCount
     */
	public Long getReadSkipCount() {
		return this.readSkipCount;
	}
	
	/**
	 * @param writeSkipCount
	 */
	public void setWriteSkipCount(Long writeSkipCount) {
		this.writeSkipCount = writeSkipCount;
	}
	
    /**
     * @return writeSkipCount
     */
	public Long getWriteSkipCount() {
		return this.writeSkipCount;
	}
	
	/**
	 * @param processSkipCount
	 */
	public void setProcessSkipCount(Long processSkipCount) {
		this.processSkipCount = processSkipCount;
	}
	
    /**
     * @return processSkipCount
     */
	public Long getProcessSkipCount() {
		return this.processSkipCount;
	}
	
	/**
	 * @param rollbackCount
	 */
	public void setRollbackCount(Long rollbackCount) {
		this.rollbackCount = rollbackCount;
	}
	
    /**
     * @return rollbackCount
     */
	public Long getRollbackCount() {
		return this.rollbackCount;
	}
	
	/**
	 * @param exitCode
	 */
	public void setExitCode(String exitCode) {
		this.exitCode = exitCode;
	}
	
    /**
     * @return exitCode
     */
	public String getExitCode() {
		return this.exitCode;
	}
	
	/**
	 * @param exitMessage
	 */
	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}
	
    /**
     * @return exitMessage
     */
	public String getExitMessage() {
		return this.exitMessage;
	}
	
	/**
	 * @param lastUpdated
	 */
	public void setLastUpdated(java.util.Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
    /**
     * @return lastUpdated
     */
	public java.util.Date getLastUpdated() {
		return this.lastUpdated;
	}


}