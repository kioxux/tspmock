package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0410Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0410</br>
 * 任务名称：加工任务-贷后管理-定期检查[清理贷后临时表数据] </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月17日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0410Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0410Service.class);

    @Autowired
    private Cmis0410Mapper cmis0410Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * 插入监控对公客户信息表
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0410TruncateTable(String openDay) {
        // 重建相关表
        logger.info("重建定期检查借据信息表[tmp_psp_debit_info]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_psp_debit_info");
        logger.info("重建【Cmis0410】定期检查借据信息表[tmp_psp_debit_info]结束");

        // 重建相关表
        logger.info("重建客户最近一次贷后检查任务临时表[tmp_last_psp_task_list]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_last_psp_task_list");
        logger.info("重建【Cmis0410】客户最近一次贷后检查任务临时表[tmp_last_psp_task_list]结束");

        // 重建相关表
        logger.info("重建未完成的贷后检查任务临时表[tmp_undo_psp_task_list]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_undo_psp_task_list");
        logger.info("重建【Cmis0410】未完成的贷后检查任务临时表[tmp_undo_psp_task_list]结束");
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0410InsertTmp(Map map){
        // 重建相关表
        logger.info("重建贷后检查日期临时表[tmp_check_date]开始,请求参数为:[{}]");
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_check_date");
        logger.info("重建【Cmis0410】贷后检查日期临时表[tmp_check_date]结束");

        logger.info("加工贷后检查日期临时表-开始,请求参数为:[{}]", map);
        int insertTmpCheckDate = cmis0410Mapper.insertTmpCheckDate(map);
        logger.info("加工贷后检查日期临时表-结束,返回参数为:[{}]", insertTmpCheckDate);

        logger.info("加工获取同一客户最后一次检查历史任务-开始,请求参数为:[{}]", map);
        int insertTmp = cmis0410Mapper.insertTmp(map);
        logger.info("加工获取同一客户最后一次检查历史任务-结束,返回参数为:[{}]", insertTmp);

//        logger.info("加工未完成的贷后检查任务-开始,请求参数为:[{}]", map);
//        int insertTmp2 = cmis0410Mapper.insertTmp2(map);
//        logger.info("加工未完成的贷后检查任务-结束,返回参数为:[{}]", insertTmp2);
    }

}
