/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.fls;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.fls.BatTFlsAppContractInfo;
import cn.com.yusys.yusp.batch.repository.mapper.load.fls.BatTFlsAppContractInfoMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTFlsAppContractInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-06 11:09:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTFlsAppContractInfoService {

    @Autowired
    private BatTFlsAppContractInfoMapper batTFlsAppContractInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTFlsAppContractInfo selectByPrimaryKey(String contId) {
        return batTFlsAppContractInfoMapper.selectByPrimaryKey(contId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTFlsAppContractInfo> selectAll(QueryModel model) {
        List<BatTFlsAppContractInfo> records = (List<BatTFlsAppContractInfo>) batTFlsAppContractInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTFlsAppContractInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTFlsAppContractInfo> list = batTFlsAppContractInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTFlsAppContractInfo record) {
        return batTFlsAppContractInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTFlsAppContractInfo record) {
        return batTFlsAppContractInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTFlsAppContractInfo record) {
        return batTFlsAppContractInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTFlsAppContractInfo record) {
        return batTFlsAppContractInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String contId) {
        return batTFlsAppContractInfoMapper.deleteByPrimaryKey(contId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batTFlsAppContractInfoMapper.deleteByIds(ids);
    }

    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTFlsAppContractInfoMapper.truncateTTable();
    }
}
