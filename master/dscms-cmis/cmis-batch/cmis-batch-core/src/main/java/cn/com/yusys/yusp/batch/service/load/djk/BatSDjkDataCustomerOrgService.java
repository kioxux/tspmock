/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.djk;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.djk.BatSDjkDataCustomerOrg;
import cn.com.yusys.yusp.batch.repository.mapper.load.djk.BatSDjkDataCustomerOrgMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSDjkDataCustomerOrgService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-29 22:44:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSDjkDataCustomerOrgService {
    private static final Logger logger = LoggerFactory.getLogger(BatSDjkDataCustomerOrgService.class);
    @Autowired
    private BatSDjkDataCustomerOrgMapper batSDjkDataCustomerOrgMapper;
    @Autowired
    private TableUtilsService tableUtilsService;

	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSDjkDataCustomerOrg selectByPrimaryKey(String custId) {
        return batSDjkDataCustomerOrgMapper.selectByPrimaryKey(custId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSDjkDataCustomerOrg> selectAll(QueryModel model) {
        List<BatSDjkDataCustomerOrg> records = (List<BatSDjkDataCustomerOrg>) batSDjkDataCustomerOrgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSDjkDataCustomerOrg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSDjkDataCustomerOrg> list = batSDjkDataCustomerOrgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSDjkDataCustomerOrg record) {
        return batSDjkDataCustomerOrgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSDjkDataCustomerOrg record) {
        return batSDjkDataCustomerOrgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSDjkDataCustomerOrg record) {
        return batSDjkDataCustomerOrgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSDjkDataCustomerOrg record) {
        return batSDjkDataCustomerOrgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String custId) {
        return batSDjkDataCustomerOrgMapper.deleteByPrimaryKey(custId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSDjkDataCustomerOrgMapper.deleteByIds(ids);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建客户基本信息文件开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_djk_data_customer_org");
        logger.info("重建客户基本信息文件结束");
        return 0;
    }


    /**
     * 将T表数据merge到S表
     *
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        logger.info("更新S表中[数值类型字段为Decimal类型值]开始,请求参数为:[{}]", openDay);
        int updateSResultField = batSDjkDataCustomerOrgMapper.updateSResultField(openDay);
        logger.info("更新S表中[数值类型字段为Decimal类型值]结束,返回参数为:[{}]", updateSResultField);
    }


}
