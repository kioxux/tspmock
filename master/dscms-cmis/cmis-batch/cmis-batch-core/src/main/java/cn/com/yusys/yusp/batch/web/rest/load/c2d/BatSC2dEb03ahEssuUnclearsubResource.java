/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.c2d;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.c2d.BatSC2dEb03ahEssuUnclearsub;
import cn.com.yusys.yusp.batch.service.load.c2d.BatSC2dEb03ahEssuUnclearsubService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dEb03ahEssuUnclearsubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:02:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsc2deb03ahessuunclearsub")
public class BatSC2dEb03ahEssuUnclearsubResource {
    @Autowired
    private BatSC2dEb03ahEssuUnclearsubService batSC2dEb03ahEssuUnclearsubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSC2dEb03ahEssuUnclearsub>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSC2dEb03ahEssuUnclearsub> list = batSC2dEb03ahEssuUnclearsubService.selectAll(queryModel);
        return new ResultDto<List<BatSC2dEb03ahEssuUnclearsub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSC2dEb03ahEssuUnclearsub>> index(QueryModel queryModel) {
        List<BatSC2dEb03ahEssuUnclearsub> list = batSC2dEb03ahEssuUnclearsubService.selectByModel(queryModel);
        return new ResultDto<List<BatSC2dEb03ahEssuUnclearsub>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSC2dEb03ahEssuUnclearsub> create(@RequestBody BatSC2dEb03ahEssuUnclearsub batSC2dEb03ahEssuUnclearsub) throws URISyntaxException {
        batSC2dEb03ahEssuUnclearsubService.insert(batSC2dEb03ahEssuUnclearsub);
        return new ResultDto<BatSC2dEb03ahEssuUnclearsub>(batSC2dEb03ahEssuUnclearsub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSC2dEb03ahEssuUnclearsub batSC2dEb03ahEssuUnclearsub) throws URISyntaxException {
        int result = batSC2dEb03ahEssuUnclearsubService.update(batSC2dEb03ahEssuUnclearsub);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String id, String reportId, String unionId) {
        int result = batSC2dEb03ahEssuUnclearsubService.deleteByPrimaryKey(id, reportId, unionId);
        return new ResultDto<Integer>(result);
    }

}
