/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.pjp;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.pjp.BatSPjpBtRediscountSellBatch;
import cn.com.yusys.yusp.batch.repository.mapper.load.pjp.BatSPjpBtRediscountSellBatchMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtRediscountSellBatchService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-03 22:08:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSPjpBtRediscountSellBatchService {
    private static final Logger logger = LoggerFactory.getLogger(BatSPjpBtRediscountSellBatchService.class);
    @Autowired
    private BatSPjpBtRediscountSellBatchMapper batSPjpBtRediscountSellBatchMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSPjpBtRediscountSellBatch selectByPrimaryKey(String rediscSellBatchId) {
        return batSPjpBtRediscountSellBatchMapper.selectByPrimaryKey(rediscSellBatchId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSPjpBtRediscountSellBatch> selectAll(QueryModel model) {
        List<BatSPjpBtRediscountSellBatch> records = (List<BatSPjpBtRediscountSellBatch>) batSPjpBtRediscountSellBatchMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSPjpBtRediscountSellBatch> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSPjpBtRediscountSellBatch> list = batSPjpBtRediscountSellBatchMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSPjpBtRediscountSellBatch record) {
        return batSPjpBtRediscountSellBatchMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSPjpBtRediscountSellBatch record) {
        return batSPjpBtRediscountSellBatchMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSPjpBtRediscountSellBatch record) {
        return batSPjpBtRediscountSellBatchMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSPjpBtRediscountSellBatch record) {
        return batSPjpBtRediscountSellBatchMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String rediscSellBatchId) {
        return batSPjpBtRediscountSellBatchMapper.deleteByPrimaryKey(rediscSellBatchId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSPjpBtRediscountSellBatchMapper.deleteByIds(ids);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建综合票据系统-转贴现卖出批次表[bat_s_pjp_bt_rediscount_sell_batch]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_pjp_bt_rediscount_sell_batch");
        logger.info("重建【PJP00011】综合票据系统-转贴现卖出批次表[bat_s_pjp_bt_rediscount_sell_batch]结束");
        return 0;
    }
}
