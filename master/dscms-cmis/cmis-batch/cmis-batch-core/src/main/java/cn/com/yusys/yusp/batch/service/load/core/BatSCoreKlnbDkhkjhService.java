/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKlnbDkhkjh;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003RespDto;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatSCoreKlnbDkhkjhMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatSCoreKlnbDkhkjhService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 23:46:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSCoreKlnbDkhkjhService {
    private static final Logger logger = LoggerFactory.getLogger(BatSCoreKlnbDkhkjhService.class);
    @Autowired
    private BatSCoreKlnbDkhkjhMapper batSCoreKlnbDkhkjhMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSCoreKlnbDkhkjh selectByPrimaryKey(String dkjiejuh, Long benqqish, Long benqizqs) {
        return batSCoreKlnbDkhkjhMapper.selectByPrimaryKey(dkjiejuh, benqqish, benqizqs);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSCoreKlnbDkhkjh> selectAll(QueryModel model) {
        List<BatSCoreKlnbDkhkjh> records = (List<BatSCoreKlnbDkhkjh>) batSCoreKlnbDkhkjhMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSCoreKlnbDkhkjh> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSCoreKlnbDkhkjh> list = batSCoreKlnbDkhkjhMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSCoreKlnbDkhkjh record) {
        return batSCoreKlnbDkhkjhMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSCoreKlnbDkhkjh record) {
        return batSCoreKlnbDkhkjhMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSCoreKlnbDkhkjh record) {
        return batSCoreKlnbDkhkjhMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSCoreKlnbDkhkjh record) {
        return batSCoreKlnbDkhkjhMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dkjiejuh, Long benqqish, Long benqizqs) {
        return batSCoreKlnbDkhkjhMapper.deleteByPrimaryKey(dkjiejuh, benqqish, benqizqs);
    }

    /**
     * 根据贷款借据号 查询[贷款还款计划表]和[贷款期供交易明细]关联信息
     *
     * @param queryModel
     * @return
     */
    public Cmisbatch0003RespDto selectByDkjiejuh(QueryModel queryModel) {
        return batSCoreKlnbDkhkjhMapper.selectByDkjiejuh(queryModel);
    }


    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建贷款还款计划表表[bat_s_core_klnb_dkhkjh]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_core_klnb_dkhkjh");
        logger.info("重建【CORE0009】贷款还款计划表表[bat_s_core_klnb_dkhkjh]结束");
        return 0;
    }


    /**
     * 将T表数据merge到S表
     *
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]开始,请求参数为:[{}]", openDay);
        int updateSResult = batSCoreKlnbDkhkjhMapper.updateDataDateByOpenDay(openDay);
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]结束,返回参数为:[{}]", updateSResult);
    }
}
