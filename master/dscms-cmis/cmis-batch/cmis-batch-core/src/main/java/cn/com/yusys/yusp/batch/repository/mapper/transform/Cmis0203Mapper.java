package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0203</br>
 * 任务名称：加工任务-额度处理-表内占用敞口余额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0203Mapper {


    /**
     * 更新合同主表-将未关联额度的联合贷合同
     *
     * @param openDay
     * @return
     */
    int updateCtrLoanCont(@Param("openDay") String openDay);

    /**
     * 删除合同主表0203
     */
    void deleteTempCtrLoanCont0203();

    /**
     *
     * 插入除合同主表0203
     * @param openDay
     * @return
     */
    int insertTempCtrLoanCont0203(@Param("openDay") String openDay);



    /**
     * 插入批复额度分项基础信息-将未生成额度的合同生成额度分项信息
     *
     * @param openDay
     * @return
     */

    int insertApprLmtSubBasicInfo(@Param("openDay") String openDay);


    /**
     * 插入批复主信息-将未生成额度的合同生成额度主信息
     *
     * @param openDay
     * @return
     */

    int insertApprStrMtableInfo(@Param("openDay") String openDay);
}
