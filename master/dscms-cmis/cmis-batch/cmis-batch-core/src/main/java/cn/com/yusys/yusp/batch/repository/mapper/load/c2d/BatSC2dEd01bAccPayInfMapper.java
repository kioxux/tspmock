/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.load.c2d;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.batch.domain.load.c2d.BatSC2dEd01bAccPayInf;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dEd01bAccPayInfMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:02:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface BatSC2dEd01bAccPayInfMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    BatSC2dEd01bAccPayInf selectByPrimaryKey(@Param("id") String id, @Param("reportId") String reportId, @Param("unionId") String unionId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<BatSC2dEd01bAccPayInf> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(BatSC2dEd01bAccPayInf record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(BatSC2dEd01bAccPayInf record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(BatSC2dEd01bAccPayInf record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(BatSC2dEd01bAccPayInf record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("id") String id, @Param("reportId") String reportId, @Param("unionId") String unionId);

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    int deleteByOpenDay(@Param("openDay") String openDay);

    /**
     * 删除业务表数据
     *
     * @param openDay
     * @return
     */
    int deleteBizByOpenDay(@Param("openDay") String openDay);

    /**
     * 更新创建时间和修改时间
     *
     * @param openDay
     */
    void updateBizTime(@Param("openDay") String openDay);
}