/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.rcp;

import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpAntLoanDetailSlTemp;
import cn.com.yusys.yusp.batch.repository.mapper.load.rcp.BatSRcpAntLoanDetailSlTempMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpAntLoanDetailSlTempService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-10 14:48:48
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSRcpAntLoanDetailSlTempService {
    private static final Logger logger = LoggerFactory.getLogger(BatSRcpRptFiveClassifyService.class);
    @Autowired
    private BatSRcpAntLoanDetailSlTempMapper batSRcpAntLoanDetailSlTempMapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatSRcpAntLoanDetailSlTemp selectByPrimaryKey(String contractNo) {
        return batSRcpAntLoanDetailSlTempMapper.selectByPrimaryKey(contractNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatSRcpAntLoanDetailSlTemp> selectAll(QueryModel model) {
        List<BatSRcpAntLoanDetailSlTemp> records = (List<BatSRcpAntLoanDetailSlTemp>) batSRcpAntLoanDetailSlTempMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatSRcpAntLoanDetailSlTemp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSRcpAntLoanDetailSlTemp> list = batSRcpAntLoanDetailSlTempMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatSRcpAntLoanDetailSlTemp record) {
        return batSRcpAntLoanDetailSlTempMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatSRcpAntLoanDetailSlTemp record) {
        return batSRcpAntLoanDetailSlTempMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatSRcpAntLoanDetailSlTemp record) {
        return batSRcpAntLoanDetailSlTempMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatSRcpAntLoanDetailSlTemp record) {
        return batSRcpAntLoanDetailSlTempMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String contractNo) {
        return batSRcpAntLoanDetailSlTempMapper.deleteByPrimaryKey(contractNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSRcpAntLoanDetailSlTempMapper.deleteByIds(ids);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建蚂蚁借呗放款（合约）明细（助贷+新联营）临时表[bat_s_rcp_ant_loan_detail_sl_temp]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_rcp_ant_loan_detail_sl_temp");
        logger.info("重建蚂蚁借呗放款（合约）明细（助贷+新联营）临时表[bat_s_rcp_ant_loan_detail_sl_temp]结束");
        return 0;
    }
}
