/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.ris;

import cn.com.yusys.yusp.batch.domain.load.ris.BatSRisKhfxJgbxJk;
import cn.com.yusys.yusp.batch.repository.mapper.load.ris.BatSRisKhfxJgbxJkMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpRptFiveClassifyService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRisKhfxJgbxJkService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-10-25 16:59:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSRisKhfxJgbxJkService {
    private static final Logger logger = LoggerFactory.getLogger(BatSRcpRptFiveClassifyService.class);
    @Autowired
    private TableUtilsService tableUtilsService;
    @Autowired
    private BatSRisKhfxJgbxJkMapper batSRisKhfxJgbxJkMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatSRisKhfxJgbxJk selectByPrimaryKey(String dataDt, String custTypeId, String custId, String custName) {
        return batSRisKhfxJgbxJkMapper.selectByPrimaryKey(dataDt, custTypeId, custId, custName);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatSRisKhfxJgbxJk> selectAll(QueryModel model) {
        List<BatSRisKhfxJgbxJk> records = (List<BatSRisKhfxJgbxJk>) batSRisKhfxJgbxJkMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatSRisKhfxJgbxJk> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSRisKhfxJgbxJk> list = batSRisKhfxJgbxJkMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatSRisKhfxJgbxJk record) {
        return batSRisKhfxJgbxJkMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatSRisKhfxJgbxJk record) {
        return batSRisKhfxJgbxJkMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatSRisKhfxJgbxJk record) {
        return batSRisKhfxJgbxJkMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatSRisKhfxJgbxJk record) {
        return batSRisKhfxJgbxJkMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dataDt, String custTypeId, String custId, String custName) {
        return batSRisKhfxJgbxJkMapper.deleteByPrimaryKey(dataDt, custTypeId, custId, custName);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建大额风险暴露-客户维度风险暴露汇总[bat_s_ris_khfx_jgbx_jk]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_ris_khfx_jgbx_jk");
        logger.info("重建大额风险暴露-客户维度风险暴露汇总[bat_s_ris_khfx_jgbx_jk]结束");
        return 0;
    }

    /**
     * 删除业务表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteBizByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建大额风险暴露-客户维度风险暴露汇总[dm_ris_khfx_jgbx_jk]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_lmt", "dm_ris_khfx_jgbx_jk");
        logger.info("重建大额风险暴露-客户维度风险暴露汇总[dm_ris_khfx_jgbx_jk]结束");
        return 0;
    }
}
