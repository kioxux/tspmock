package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0112</br>
 * 任务名称：加工任务-业务处理-风险提醒生成 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0112Mapper {
    /**
     * 清空数据到贷款借据号和最大期供逾期天数关系表
     * @return
     */
    void cmis0112TruncateTmp();
    
    /**
     * 单一客户授信到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice01(@Param("openDay") String openDay);

    /**
     * 同业客户授信到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice02(@Param("openDay") String openDay);

    /**
     * 资金业务授信到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice03(@Param("openDay") String openDay);

    /**
     * 小微客户授信到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice04(@Param("openDay") String openDay);

    /**
     * 贷款台账授信到期
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice05(@Param("openDay") String openDay);

    /**
     * 开证台账到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice06(@Param("openDay") String openDay);

    /**
     * 银承台账到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice07(@Param("openDay") String openDay);

    /**
     * 保函台账到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice08(@Param("openDay") String openDay);

    /**
     * 贴现台账到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice09(@Param("openDay") String openDay);

    /**
     * 委托贷款台账到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice10(@Param("openDay") String openDay);

    /**
     * 贴现协议到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice11(@Param("openDay") String openDay);

    /**
     * 开证合同到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice12(@Param("openDay") String openDay);

    /**
     * 银承合同到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice13(@Param("openDay") String openDay);

    /**
     * 保函合同到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice14(@Param("openDay") String openDay);

    /**
     * 委托贷款合同到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice15(@Param("openDay") String openDay);

    /**
     * 担保合同到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice16(@Param("openDay") String openDay);

    /**
     * 最高额授信协议到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice17(@Param("openDay") String openDay);

    /**
     * 普通贷款合同、贸易融资合同、福费廷合同到期提醒
     *
     * @param openDay
     * @return
     */
    int insertBatBizRiskNotice18(@Param("openDay") String openDay);


}
