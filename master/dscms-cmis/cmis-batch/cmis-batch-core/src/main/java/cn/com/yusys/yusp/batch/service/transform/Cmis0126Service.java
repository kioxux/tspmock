package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0126Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0126</br>
 * 任务名称：加工任务-业务处理-压降任务 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0126Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0126Service.class);

    @Autowired
    private Cmis0126Mapper cmis0126Mapper;

    /**
     * 插入压降任务管理信息
     * @param openDay
     */
    public void cmis0126InsertCusLstDedkkhYjsxTask(String openDay) {
        logger.info("插入压降任务管理信息开始,请求参数为:[{}]", openDay);
        int insertCusLstDedkkhYjsxTask = cmis0126Mapper.insertCusLstDedkkhYjsxTask(openDay);
        logger.info("插入压降任务管理信息结束,返回参数为:[{}]", insertCusLstDedkkhYjsxTask);
    }
}
