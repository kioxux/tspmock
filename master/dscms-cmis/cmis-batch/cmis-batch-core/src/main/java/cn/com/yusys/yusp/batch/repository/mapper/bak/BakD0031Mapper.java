package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKD0031</br>
 * 任务名称：批前备份日表任务-备份福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]  </br>
 *
 * @author chenyong
 * @version 1.0
 * @since 2020年9月24日
 */
public interface BakD0031Mapper {

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertCurrent(@Param("openDay") String openDay);

    /**
     * 删除当天数据
     *
     * @return
     */
    void truncateCurrent();

    /**
     * 删除当天的数据
     *
     * @param openDay
     * @return
     */
    int deleteCurrent(@Param("openDay") String openDay);


    /**
     * 查询待删除当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryD0031DeleteOpenDayTmpGjpLmtFftRzkzCounts(@Param("openDay") String openDay);


    /**
     *
     * 查询原表当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据总条数
     * @param openDay
     * @return
     */
    int queryTmpGjpLmtFftRzkz(@Param("openDay") String openDay);
}
