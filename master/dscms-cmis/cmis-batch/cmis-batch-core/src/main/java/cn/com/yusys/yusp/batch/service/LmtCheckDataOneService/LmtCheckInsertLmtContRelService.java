/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.LmtCheckDataOneService;

import cn.com.yusys.yusp.batch.repository.mapper.check.LmtCheckInsertLmtContRelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckInsertLmtContRelService
 * @类描述: #服务类
 * @功能描述: 数据插入额度校正-合同临时表
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCheckInsertLmtContRelService {

    private static final Logger logger = LoggerFactory.getLogger(LmtCheckInsertLmtContRelService.class);

    @Autowired
    private LmtCheckInsertLmtContRelMapper lmtCheckInsertLmtContRelMapper;

    public void checkOutLmtCheckInsertLmtContRel(String cusId){
        logger.info("3.LmtCheckInsertLmtContRel数据插入额度校正-合同临时表-------start");

        /** 3.1、贷款合同 **/
        insertLmtLoanRelDkht(cusId) ;

        /** 3.2、贷款合同申请 **/
        insertLmtLoanRelDkhtsq(cusId) ;

        /** 3.3、委托贷款合同 **/
        insertLmtLoanRelWtdkht(cusId) ;

        /** 3.4、委托贷款合同申请 **/
        insertLmtLoanRelWtdkhtsq(cusId) ;

        /** 3.5、最高额授信协议 **/
        insertLmtLoanRelZgesxxy(cusId) ;

        /** 3.6、最高额授信协议申请 **/
        insertLmtLoanRelZgesxxysq(cusId) ;

        /** 3.7、资产池协议 **/
        insertLmtLoanRelZccxy(cusId) ;

        /** 3.8、资产池协议申请 **/
        insertLmtLoanRelZccxysq(cusId) ;

        /** 3.9、银票合同 **/
        insertLmtLoanRelYpht(cusId) ;

        /** 3.10、银票合同申请 **/
        insertLmtLoanRelYphtsq(cusId) ;

        /** 3.11、保函合同 **/
        insertLmtLoanRelBhht(cusId) ;

        /** 3.12、保函合同申请 **/
        insertLmtLoanRelBhhtsq(cusId) ;

        /** 3.13、信用证合同 **/
        insertLmtLoanRelXyzht(cusId) ;

        /** 3.14、信用证合同申请 **/
        insertLmtLoanRelXyzhtsq(cusId) ;

        /** 3.15、贴现合同 **/
        insertLmtLoanRelTxht(cusId) ;

        /** 3.16、贴现合同申请 **/
        insertLmtLoanRelTxhtsq(cusId) ;
        logger.info("3.LmtCheckInsertLmtContRel数据插入额度校正-合同临时表-------end");
    }

    /**
     * 3.1、贷款合同
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelDkht(String cusId){
        logger.info("3.1、贷款合同----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelDkht(cusId);
    }


    /**
     * 3.2、贷款合同申请
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelDkhtsq(String cusId){
        logger.info("3.2、贷款合同申请----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelDkhtsq(cusId);
    }


    /**
     * 3.3、委托贷款合同
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelWtdkht(String cusId){
        logger.info("3.3、委托贷款合同----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelWtdkht(cusId);
    }


    /**
     * 3.4、委托贷款合同申请
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelWtdkhtsq(String cusId){
        logger.info("3.4、委托贷款合同申请----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelWtdkhtsq(cusId);
    }


    /**
     * 3.5、最高额授信协议
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelZgesxxy(String cusId){
        logger.info("3.5、最高额授信协议----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelZgesxxy(cusId);
    }


    /**
     * 3.6、最高额授信协议申请
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelZgesxxysq(String cusId){
        logger.info("3.6、最高额授信协议申请----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelZgesxxysq(cusId);
    }


    /**
     * 3.7、资产池协议
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelZccxy(String cusId){
        logger.info("3.7、资产池协议----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelZccxy(cusId);
    }


    /**
     * 3.8、资产池协议申请
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelZccxysq(String cusId){
        logger.info("3.8、资产池协议申请----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelZccxysq(cusId);
    }


    /**
     * 3.9、银票合同
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelYpht(String cusId){
        logger.info("3.9、银票合同----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelYpht(cusId);
    }


    /**
     * 3.10、银票合同申请
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelYphtsq(String cusId){
        logger.info("3.10、银票合同申请----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelYphtsq(cusId);
    }


    /**
     * 3.11、保函合同
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelBhht(String cusId){
        logger.info("3.11、保函合同----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelBhht(cusId);
    }


    /**
     * 3.12、保函合同申请
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelBhhtsq(String cusId){
        logger.info("3.12、保函合同申请----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelBhhtsq(cusId);
    }


    /**
     * 3.13、信用证合同
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelXyzht(String cusId){
        logger.info("3.13、信用证合同----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelXyzht(cusId);
    }



    /**
     * 3.14、信用证合同申请
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelXyzhtsq(String cusId){
        logger.info("3.14、信用证合同申请----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelXyzhtsq(cusId);
    }


    /**
     * 3.15、贴现合同
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelTxht(String cusId){
        logger.info("3.15、贴现合同----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelTxht(cusId);
    }


    /**
     * 3.16、贴现合同申请
     * @param cusId
     * @return
     */
    public int insertLmtLoanRelTxhtsq(String cusId){
        logger.info("3.16、贴现合同申请----------------");
        return lmtCheckInsertLmtContRelMapper.insertLmtLoanRelTxhtsq(cusId);
    }

}
