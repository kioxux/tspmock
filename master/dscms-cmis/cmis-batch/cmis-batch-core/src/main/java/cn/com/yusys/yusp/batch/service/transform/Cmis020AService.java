package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis020AMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS020A</br>
 * 任务名称：加工任务-额度处理-担保公司占用关系处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis020AService {
    private static final Logger logger = LoggerFactory.getLogger(Cmis020AService.class);
    @Autowired
    private Cmis020AMapper cmis020AMapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新合作方担保公司分项占用关系信息
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis020AUpdateCopGuarRel(String openDay) {
        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        // cmis020AMapper.truncateTmpLmtAmt();// cmis_batch.tmp_lmt_cop
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_cop");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空加工占用授信表结束");

        logger.info("取贷款占用授信的关系表占用余 额复制给担保公司占用关系 ，如果担保合同金额比占用额小，则取担保合同金额开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt = cmis020AMapper.insertTmpLmtAmt(openDay);
        logger.info("取贷款占用授信的关系表占用余 额复制给担保公司占用关系 ，如果担保合同金额比占用额小，则取担保合同金额处理结束,返回参数为:[{}]", insertTmpLmtAmt);

        logger.info("更新占用总余额、占用敞口余额 处理开始,请求参数为:[{}]", openDay);
        int updateTmpLmtAmt = cmis020AMapper.updateTmpLmtAmt(openDay);
        logger.info("更新占用总余额、占用敞口余额 系处理结束,返回参数为:[{}]", updateTmpLmtAmt);
    }
}
