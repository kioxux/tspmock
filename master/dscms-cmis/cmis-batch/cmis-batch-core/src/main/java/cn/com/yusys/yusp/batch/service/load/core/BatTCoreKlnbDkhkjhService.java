/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatTCoreKlnbDkhkjh;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatTCoreKlnbDkhkjhMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatTCoreKlnbDkhkjhService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 23:46:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTCoreKlnbDkhkjhService {

    @Autowired
    private BatTCoreKlnbDkhkjhMapper batTCoreKlnbDkhkjhMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTCoreKlnbDkhkjh selectByPrimaryKey(String dkjiejuh, Long benqqish, Long benqizqs) {
        return batTCoreKlnbDkhkjhMapper.selectByPrimaryKey(dkjiejuh, benqqish, benqizqs);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTCoreKlnbDkhkjh> selectAll(QueryModel model) {
        List<BatTCoreKlnbDkhkjh> records = (List<BatTCoreKlnbDkhkjh>) batTCoreKlnbDkhkjhMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTCoreKlnbDkhkjh> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTCoreKlnbDkhkjh> list = batTCoreKlnbDkhkjhMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTCoreKlnbDkhkjh record) {
        return batTCoreKlnbDkhkjhMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTCoreKlnbDkhkjh record) {
        return batTCoreKlnbDkhkjhMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTCoreKlnbDkhkjh record) {
        return batTCoreKlnbDkhkjhMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTCoreKlnbDkhkjh record) {
        return batTCoreKlnbDkhkjhMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dkjiejuh, Long benqqish, Long benqizqs) {
        return batTCoreKlnbDkhkjhMapper.deleteByPrimaryKey(dkjiejuh, benqqish, benqizqs);
    }

    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTCoreKlnbDkhkjhMapper.truncateTTable();
    }
}
