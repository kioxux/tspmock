/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.c2d;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dEb03ahEssuUnclearsub
 * @类描述: bat_s_c2d_eb03ah_essu_unclearsub数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:02:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_c2d_eb03ah_essu_unclearsub")
public class BatSC2dEb03ahEssuUnclearsub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** ID **/
	@Id
	@Column(name = "ID")
	private String id;
	
	/** 报告编号 **/
	@Id
	@Column(name = "REPORT_ID")
	private String reportId;
	
	/** 关联ID **/
	@Id
	@Column(name = "UNION_ID")
	private String unionId;
	
	/** 业务类型 **/
	@Column(name = "BUSINESS_TYPE", unique = false, nullable = true, length = 60)
	private String businessType;
	
	/** 资产质量分类 **/
	@Column(name = "AQ_CLASS", unique = false, nullable = true, length = 60)
	private String aqClass;
	
	/** 账户数 **/
	@Column(name = "ACCOUNT_NUM", unique = false, nullable = true, length = 8)
	private String accountNum;
	
	/** 余额 **/
	@Column(name = "BALANCE", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal balance;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param reportId
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	
    /**
     * @return reportId
     */
	public String getReportId() {
		return this.reportId;
	}
	
	/**
	 * @param unionId
	 */
	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}
	
    /**
     * @return unionId
     */
	public String getUnionId() {
		return this.unionId;
	}
	
	/**
	 * @param businessType
	 */
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	
    /**
     * @return businessType
     */
	public String getBusinessType() {
		return this.businessType;
	}
	
	/**
	 * @param aqClass
	 */
	public void setAqClass(String aqClass) {
		this.aqClass = aqClass;
	}
	
    /**
     * @return aqClass
     */
	public String getAqClass() {
		return this.aqClass;
	}
	
	/**
	 * @param accountNum
	 */
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}
	
    /**
     * @return accountNum
     */
	public String getAccountNum() {
		return this.accountNum;
	}
	
	/**
	 * @param balance
	 */
	public void setBalance(java.math.BigDecimal balance) {
		this.balance = balance;
	}
	
    /**
     * @return balance
     */
	public java.math.BigDecimal getBalance() {
		return this.balance;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}