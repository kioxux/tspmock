package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0119Mapper;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0119</br>
 * 任务名称：加工任务-业务处理-任务加急 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0119Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0119Service.class);

    @Autowired
    private Cmis0119Mapper cmis0119Mapper;


    /**
     * 更新任务加急配置
     *
     * @param openDay
     */
    public void cmis0019UpdateTaskUrgentDetailInfo(String openDay) {

        logger.info("删除重复的记录开始,请求参数为:[{}]", openDay);
        int deleteTaskUrgentDetailInfo01dele = cmis0119Mapper.deleteTaskUrgentDetailInfo01dele(openDay);
        logger.info("删除重复的记录结束,返回参数为:[{}]", deleteTaskUrgentDetailInfo01dele);

        logger.info("按月生成用户 的加急记录 一次生成连续三个月的数据开始,请求参数为:[{}]", openDay);
        int insertTaskUrgentDetailInfo01A = cmis0119Mapper.insertTaskUrgentDetailInfo01A(openDay);
        logger.info("按月生成用户 的加急记录 一次生成连续三个月的数据结束,返回参数为:[{}]", insertTaskUrgentDetailInfo01A);

        logger.info("按季生成用户 的加急记录 一次生成一个季度的数据开始,请求参数为:[{}]", openDay);
        int insertTaskUrgentDetailInfo01B = cmis0119Mapper.insertTaskUrgentDetailInfo01B(openDay);
        logger.info("按季生成用户 的加急记录 一次生成一个季度的数据结束,返回参数为:[{}]", insertTaskUrgentDetailInfo01B);

        logger.info("清空临时表-任务加急笔数明细信息开始,请求参数为:[{}]", openDay);
        cmis0119Mapper.truncateTaskUrgentDetailInfo02A();
        logger.info("清空临时表-任务加急笔数明细信息结束");

        logger.info("插入临时表-任务加急笔数明细信息开始,请求参数为:[{}]", openDay);
        int insertTaskUrgentDetailInfo02A = cmis0119Mapper.insertTaskUrgentDetailInfo02A(openDay);
        logger.info("插入临时表-任务加急笔数明细信息结束,返回参数为:[{}]", insertTaskUrgentDetailInfo02A);

        logger.info("每季度末或者每月末执行   历史结余笔数等于上次 剩余笔数,只更新预生效开始,请求参数为:[{}]", openDay);
        int updateTaskUrgentDetailInfo02A = cmis0119Mapper.updateTaskUrgentDetailInfo02A(openDay);
        logger.info("每季度末或者每月末执行   历史结余笔数等于上次 剩余笔数,只更新预生效结束,返回参数为:[{}]", updateTaskUrgentDetailInfo02A);

        logger.info("清空临时表-任务加急笔数明细信息临时表开始,请求参数为:[{}]", openDay);
        cmis0119Mapper.truncateTaskUrgentDetailInfo02();
        logger.info("清空临时表-任务加急笔数明细信息临时表结束");

        logger.info("加工临时表-任务加急笔数明细信息临时表开始,请求参数为:[{}]", openDay);
        int insertTaskUrgentDetailInfo02 = cmis0119Mapper.insertTaskUrgentDetailInfo02(openDay);
        logger.info("加工临时表-任务加急笔数明细信息临时表结束,返回参数为:[{}]", insertTaskUrgentDetailInfo02);

        logger.info("每季度或者每月执行:计算本次新增笔数,更新任务加急配置开始,请求参数为:[{}]", openDay);
        int updateTaskUrgentDetailInfo02 = cmis0119Mapper.updateTaskUrgentDetailInfo02(openDay);
        logger.info("每季度或者每月执行:计算本次新增笔数,更新任务加急配置结束,返回参数为:[{}]", updateTaskUrgentDetailInfo02);

        logger.info("每季度或者每月执行:总优先笔数：历史结余笔数+本次新增笔数,更新任务加急配置开始,请求参数为:[{}]", openDay);
        int updateTaskUrgentDetailInfo03 = cmis0119Mapper.updateTaskUrgentDetailInfo03(openDay);
        logger.info("每季度或者每月执行:历史结余笔数等于上次剩余笔数,更新任务加急配置结束,返回参数为:[{}]", updateTaskUrgentDetailInfo03);

        logger.info("每季度或者每月执行:当前剩余笔数等于总优先笔数 ,更新任务加急配置开始,请求参数为:[{}]", openDay);
        int updateTaskUrgentDetailInfo04 = cmis0119Mapper.updateTaskUrgentDetailInfo04(openDay);
        logger.info("每季度或者每月执行:当前剩余笔数等于总优先笔数 ,更新任务加急配置结束,返回参数为:[{}]", updateTaskUrgentDetailInfo04);


        logger.info("预生效改为生效之前  将之前的加急记录改为失效开始,请求参数为:[{}]", openDay);
        int updateTaskUrgentDetailInfo05 = cmis0119Mapper.updateTaskUrgentDetailInfo05(openDay);
        logger.info("预生效改为生效之前  将之前的加急记录改为失效结束,返回参数为:[{}]", updateTaskUrgentDetailInfo05);


        logger.info("将当前的加急记录预生效改为生效开始,请求参数为:[{}]", openDay);
        int updateTaskUrgentDetailInfo06 = cmis0119Mapper.updateTaskUrgentDetailInfo06(openDay);
        logger.info("将当前的加急记录预生效改为生效结束,返回参数为:[{}]", updateTaskUrgentDetailInfo06);

    }

}
