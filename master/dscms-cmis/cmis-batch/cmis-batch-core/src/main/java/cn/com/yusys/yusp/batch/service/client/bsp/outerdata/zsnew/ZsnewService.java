package cn.com.yusys.yusp.batch.service.client.bsp.outerdata.zsnew;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2OuterdataClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/24 9:51
 * @desc 查询工商信息
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class ZsnewService {
    private static final Logger logger = LoggerFactory.getLogger(ZsnewService.class);
    // 1）注入：BSP封装调用外部数据平台的接口
    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

    /**
     * @param zsnewReqDto
     * @return cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto
     * @author hubp
     * @date 2021/5/24 9:55
     * @version 1.0.0
     * @desc 查询工商信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public ZsnewRespDto zsnew(ZsnewReqDto zsnewReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, JSON.toJSONString(zsnewReqDto));
        ResultDto<ZsnewRespDto> zsnewResultDto = dscms2OuterdataClientService.zsnew(zsnewReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, JSON.toJSONString(zsnewResultDto));
        String zsnewCode = Optional.ofNullable(zsnewResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String zsnewMeesage = Optional.ofNullable(zsnewResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        ZsnewRespDto zsnewRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, zsnewResultDto.getCode())) {
            //  获取相关的值并解析
            zsnewRespDto = zsnewResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, zsnewCode, zsnewMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value);
        return zsnewRespDto;
    }
    /**
     * @param zsnewReqDto
     * @return cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto
     * @author hubp
     * @date 2021/5/24 9:55
     * @version 1.0.0
     * @desc 查询工商信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public ResultDto<ZsnewRespDto> zsnew2(ZsnewReqDto zsnewReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, JSON.toJSONString(zsnewReqDto));
        ResultDto<ZsnewRespDto> zsnewResultDto = dscms2OuterdataClientService.zsnew(zsnewReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, JSON.toJSONString(zsnewResultDto));
        return zsnewResultDto;
    }

    /**
     * 根据查询结果来组装[请求Dto：企业工商信息查询]
     *
     * @param corpMap
     * @return
     */
    public ZsnewReqDto buildByCorpMap(Map<String, String> corpMap) {
        logger.info("根据查询结果来组装[请求Dto：企业工商信息查询]开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        ZsnewReqDto zsnewReqDto = new ZsnewReqDto();
        String cusId = corpMap.get("cusId");
        String cusName = corpMap.get("cusName");
        String certType = corpMap.get("certType");
        String certCode = corpMap.get("certCode");

        String creditcode = certCode;   //统一信用代码
        String entstatus = "1";    //企业经营状态，1：在营，2：非在营
        String enttype = "1";      //企业类型:1-企业 2-个体
        String id = StringUtils.EMPTY;           //中数企业ID
        String name = cusName;         //企业名称
        String orgcode = StringUtils.EMPTY;      //组织机构代码
        String regno = StringUtils.EMPTY;        //企业注册号
        String version = StringUtils.EMPTY;      //高管识别码版本号,返回高管识别码时生效

        zsnewReqDto.setCreditcode(creditcode);
        zsnewReqDto.setEntstatus(entstatus);
        zsnewReqDto.setEnttype(enttype);
        zsnewReqDto.setId(id);
        zsnewReqDto.setName(name);
        zsnewReqDto.setOrgcode(orgcode);
        zsnewReqDto.setRegno(regno);
        zsnewReqDto.setVersion(version);
        logger.info("根据查询结果来组装[请求Dto：失信被执行人查询接口]结束,响应参数为:[{}]", JSON.toJSONString(zsnewReqDto));
        return zsnewReqDto;
    }
}
