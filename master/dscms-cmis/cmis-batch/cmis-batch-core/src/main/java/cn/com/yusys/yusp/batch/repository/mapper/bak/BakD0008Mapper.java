package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKD0008</br>
 * 任务名称：批前备份日表任务-备份合作方授信台账信息[APPR_COOP_INFO]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakD0008Mapper {

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertCurrent(@Param("openDay") String openDay);

    /**
     * 删除当天的数据
     *
     * @return
     */
    void truncateCurrent();

    /**
     * 删除当天的数据
     *
     * @param openDay
     * @return
     */
    int deleteCurrent(@Param("openDay") String openDay);


    /**
     * 查询待删除当天的[合作方授信台账信息[TMP_D_APPR_COOP_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryD0008DeleteOpenDayAccCoopInfoCounts(@Param("openDay") String openDay);


    /**
     *
     * 查询当天原表的[委托贷款台账[ACC_ENTRUST_LOAN]]数据总条数
     * @param openDay
     * @return
     */
    int queryApprCoopInfoCounts(@Param("openDay") String openDay);


}
