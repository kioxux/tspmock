/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKlnbDkjxsx
 * @类描述: bat_s_core_klnb_dkjxsx数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_core_klnb_dkjxsx")
public class BatSCoreKlnbDkjxsx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 法人代码 **/
	@Id
	@Column(name = "farendma")
	private String farendma;
	
	/** 贷款借据号 **/
	@Id
	@Column(name = "dkjiejuh")
	private String dkjiejuh;
	
	/** 计息规则 STD_JIXIGUIZ **/
	@Column(name = "jixiguiz", unique = false, nullable = true, length = 2)
	private String jixiguiz;
	
	/** 零头计息规则 STD_LTJIXIGZ **/
	@Column(name = "ltjixigz", unique = false, nullable = true, length = 1)
	private String ltjixigz;
	
	/** 计息本金规则 STD_JIXIBJGZ **/
	@Column(name = "jixibjgz", unique = false, nullable = true, length = 1)
	private String jixibjgz;
	
	/** 利息计算方法 STD_LIXIJSFF **/
	@Column(name = "lixijsff", unique = false, nullable = true, length = 1)
	private String lixijsff;
	
	/** 计息头尾规则 STD_JIXITWGZ **/
	@Column(name = "jixitwgz", unique = false, nullable = true, length = 1)
	private String jixitwgz;
	
	/** 计息最小金额 **/
	@Column(name = "jixizxje", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal jixizxje;
	
	/** 计息舍入规则 STD_JIXISRGZ **/
	@Column(name = "jixisrgz", unique = false, nullable = true, length = 1)
	private String jixisrgz;
	
	/** 舍入最小单位 **/
	@Column(name = "srzxdanw", unique = false, nullable = true, length = 32)
	private String srzxdanw;
	
	/** 利率日期规则 STD_LILVRQGZ **/
	@Column(name = "lilvrqgz", unique = false, nullable = true, length = 1)
	private String lilvrqgz;
	
	/** 适用汇率类型 STD_SYHLLEEIX **/
	@Column(name = "syhlleix", unique = false, nullable = true, length = 1)
	private String syhlleix;
	
	/** 利率类型 STD_LILVLEIX **/
	@Column(name = "lilvleix", unique = false, nullable = true, length = 1)
	private String lilvleix;
	
	/** 利率期限 **/
	@Column(name = "lilvqixx", unique = false, nullable = true, length = 20)
	private String lilvqixx;
	
	/** 利率期限靠档方式 STD_LIQXKDFS **/
	@Column(name = "llqxkdfs", unique = false, nullable = true, length = 1)
	private String llqxkdfs;
	
	/** 合同利率 **/
	@Column(name = "hetongll", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal hetongll;
	
	/** 计息标志 STD_JIXIBZHI **/
	@Column(name = "jixibzhi", unique = false, nullable = true, length = 1)
	private String jixibzhi;
	
	/** 计复息标志 STD_JFXIBZHI **/
	@Column(name = "jfxibzhi", unique = false, nullable = true, length = 1)
	private String jfxibzhi;
	
	/** 复息计息标志 STD_JIXIBZHI **/
	@Column(name = "fxjxbzhi", unique = false, nullable = true, length = 1)
	private String fxjxbzhi;
	
	/** 贴息标志 STD_YESORNO **/
	@Column(name = "tiexibzh", unique = false, nullable = true, length = 1)
	private String tiexibzh;
	
	/** 利率分段 STD_YESORNO **/
	@Column(name = "lilvfend", unique = false, nullable = true, length = 1)
	private String lilvfend;
	
	/** 分段计息标志 STD_FDJIXIBZ **/
	@Column(name = "fdjixibz", unique = false, nullable = true, length = 1)
	private String fdjixibz;
	
	/** 利息资本化标志 STD_YESORNO **/
	@Column(name = "lxzbhbzh", unique = false, nullable = true, length = 1)
	private String lxzbhbzh;
	
	/** 利息资本化比例 **/
	@Column(name = "lxzbhbli", unique = false, nullable = true, length = 6)
	private java.math.BigDecimal lxzbhbli;
	
	/** 资本化截止日期 **/
	@Column(name = "zbhjzhrq", unique = false, nullable = true, length = 8)
	private String zbhjzhrq;
	
	/** 正常利率编号 **/
	@Column(name = "zclilvbh", unique = false, nullable = true, length = 20)
	private String zclilvbh;
	
	/** 年/月利率标识 STD_NYUELILV **/
	@Column(name = "nyuelilv", unique = false, nullable = true, length = 1)
	private String nyuelilv;
	
	/** 正常利率 **/
	@Column(name = "zhchlilv", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal zhchlilv;
	
	/** 利率调整方式 STD_LILVTZFS **/
	@Column(name = "lilvtzfs", unique = false, nullable = true, length = 1)
	private String lilvtzfs;
	
	/** 利率调整周期 **/
	@Column(name = "lilvtzzq", unique = false, nullable = true, length = 8)
	private String lilvtzzq;
	
	/** 利率浮动方式 STD_LILVFDFS **/
	@Column(name = "lilvfdfs", unique = false, nullable = true, length = 1)
	private String lilvfdfs;
	
	/** 利率浮动值 **/
	@Column(name = "lilvfdzh", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal lilvfdzh;
	
	/** 逾期利率参考类型 STD_YQLLCKLX **/
	@Column(name = "yqllcklx", unique = false, nullable = true, length = 1)
	private String yqllcklx;
	
	/** 逾期利率编号 **/
	@Column(name = "yuqillbh", unique = false, nullable = true, length = 20)
	private String yuqillbh;
	
	/** 逾期年月利率 STD_NYUELILV **/
	@Column(name = "yuqinyll", unique = false, nullable = true, length = 1)
	private String yuqinyll;
	
	/** 逾期利率 **/
	@Column(name = "yuqililv", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal yuqililv;
	
	/** 逾期利率调整方式 STD_YUQITZFS **/
	@Column(name = "yuqitzfs", unique = false, nullable = true, length = 1)
	private String yuqitzfs;
	
	/** 逾期利率调整周期 **/
	@Column(name = "yuqitzzq", unique = false, nullable = true, length = 8)
	private String yuqitzzq;
	
	/** 逾期罚息浮动方式 STD_YQFXFDFS **/
	@Column(name = "yqfxfdfs", unique = false, nullable = true, length = 1)
	private String yqfxfdfs;
	
	/** 逾期罚息浮动值 **/
	@Column(name = "yqfxfdzh", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal yqfxfdzh;
	
	/** 复利利率参考类型 STD_FLLLCKLX **/
	@Column(name = "flllcklx", unique = false, nullable = true, length = 1)
	private String flllcklx;
	
	/** 复利利率编号 **/
	@Column(name = "fulilvbh", unique = false, nullable = true, length = 20)
	private String fulilvbh;
	
	/** 复利利率年月标识 STD_NYUELILV **/
	@Column(name = "fulilvny", unique = false, nullable = true, length = 1)
	private String fulilvny;
	
	/** 复利利率 **/
	@Column(name = "fulililv", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal fulililv;
	
	/** 复利利率调整方式 STD_LILVTZFS **/
	@Column(name = "fulitzfs", unique = false, nullable = true, length = 1)
	private String fulitzfs;
	
	/** 复利利率调整周期 **/
	@Column(name = "fulitzzq", unique = false, nullable = true, length = 8)
	private String fulitzzq;
	
	/** 复利利率浮动方式 STD_LILVFDFS **/
	@Column(name = "fulifdfs", unique = false, nullable = true, length = 1)
	private String fulifdfs;
	
	/** 复利利率浮动值 **/
	@Column(name = "fulifdzh", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal fulifdzh;
	
	/** 贷款挪用标志 STD_YESORNO **/
	@Column(name = "dknuoybz", unique = false, nullable = true, length = 1)
	private String dknuoybz;
	
	/** 挪用利率参考类型 STD_YQLLCKLX **/
	@Column(name = "nyllcklx", unique = false, nullable = true, length = 1)
	private String nyllcklx;
	
	/** 挤占挪用利率编号 **/
	@Column(name = "jznylvbh", unique = false, nullable = true, length = 20)
	private String jznylvbh;
	
	/** 挤占挪用利率年月标识 STD_NYUELILV **/
	@Column(name = "jznylvny", unique = false, nullable = true, length = 1)
	private String jznylvny;
	
	/** 挤占挪用利率 **/
	@Column(name = "jznylilv", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal jznylilv;
	
	/** 挤占挪用利率调整方式(STD_LILVTZFS **/
	@Column(name = "jznytzfs", unique = false, nullable = true, length = 1)
	private String jznytzfs;
	
	/** 挤占挪用利率调整周期 **/
	@Column(name = "jznytzzq", unique = false, nullable = true, length = 8)
	private String jznytzzq;
	
	/** 挤占挪用利率浮动方式 STD_LILVFDFS **/
	@Column(name = "jznyfdfs", unique = false, nullable = true, length = 1)
	private String jznyfdfs;
	
	/** 挤占挪用利率浮动值 **/
	@Column(name = "jznyfdzh", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal jznyfdzh;
	
	/** 实际利率计算方式 STD_SJLLJSFS **/
	@Column(name = "sjlljsfs", unique = false, nullable = true, length = 1)
	private String sjlljsfs;
	
	/** 实际年月利率 STD_NYUELILV **/
	@Column(name = "sjnylilv", unique = false, nullable = true, length = 1)
	private String sjnylilv;
	
	/** 实际利率 **/
	@Column(name = "shijlilv", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal shijlilv;
	
	/** 实际年利率 **/
	@Column(name = "shjnlilv", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal shjnlilv;
	
	/** 计提标志 STD_YESORNO **/
	@Column(name = "jitibzhi", unique = false, nullable = true, length = 1)
	private String jitibzhi;
	
	/** 计提规则 STD_JITIGUIZ **/
	@Column(name = "jitiguiz", unique = false, nullable = true, length = 1)
	private String jitiguiz;
	
	/** 计提利率规则 STD_JITILLGZ **/
	@Column(name = "jitillgz", unique = false, nullable = true, length = 1)
	private String jitillgz;
	
	/** 计提利率 **/
	@Column(name = "jitililv", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal jitililv;
	
	/** 计提周期 **/
	@Column(name = "jitizhqi", unique = false, nullable = true, length = 8)
	private String jitizhqi;
	
	/** 下次计提日 **/
	@Column(name = "xcjtriqi", unique = false, nullable = true, length = 8)
	private String xcjtriqi;
	
	/** 损失准备计提编号 **/
	@Column(name = "sszbjtbh", unique = false, nullable = true, length = 30)
	private String sszbjtbh;
	
	/** 早起息标志 STD_YESORNO **/
	@Column(name = "zaoqixbz", unique = false, nullable = true, length = 1)
	private String zaoqixbz;
	
	/** 晚起息标志 STD_YESORNO **/
	@Column(name = "wanqixbz", unique = false, nullable = true, length = 1)
	private String wanqixbz;
	
	/** 起息天数 **/
	@Column(name = "qixitshu", unique = false, nullable = true, length = 19)
	private Long qixitshu;
	
	/** 早起息最大天数 **/
	@Column(name = "zqxizdds", unique = false, nullable = true, length = 19)
	private Long zqxizdds;
	
	/** 晚起息最大天数 **/
	@Column(name = "wqxizdds", unique = false, nullable = true, length = 19)
	private Long wqxizdds;
	
	/** 预收息方式STD_YUSHXFSH **/
	@Column(name = "yushxfsh", unique = false, nullable = true, length = 1)
	private String yushxfsh;
	
	/** 利息摊销周期 **/
	@Column(name = "lixitxzq", unique = false, nullable = true, length = 8)
	private String lixitxzq;
	
	/** 每次摊销方式 STD_MEICTXFS **/
	@Column(name = "meictxfs", unique = false, nullable = true, length = 1)
	private String meictxfs;
	
	/** 每次摊销比例 **/
	@Column(name = "meictxbl", unique = false, nullable = true, length = 6)
	private java.math.BigDecimal meictxbl;
	
	/** 预收息总额 **/
	@Column(name = "yushxize", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yushxize;
	
	/** 下次摊销日 **/
	@Column(name = "xiacitxr", unique = false, nullable = true, length = 8)
	private String xiacitxr;
	
	/** 允许以存抵贷 STD_YESORNO **/
	@Column(name = "yunxycdd", unique = false, nullable = true, length = 1)
	private String yunxycdd;
	
	/** 抵贷账号 **/
	@Column(name = "didaizhh", unique = false, nullable = true, length = 35)
	private String didaizhh;
	
	/** 抵贷账号子序号 **/
	@Column(name = "didzhzxh", unique = false, nullable = true, length = 8)
	private String didzhzxh;
	
	/** 补贴类型 STD_BUTIELEX **/
	@Column(name = "butielex", unique = false, nullable = true, length = 1)
	private String butielex;
	
	/** 补贴适用客户类型 STD_BUTISYKH **/
	@Column(name = "butisykh", unique = false, nullable = true, length = 1)
	private String butisykh;
	
	/** 补贴提供方 STD_BUTIETGF **/
	@Column(name = "butietgf", unique = false, nullable = true, length = 1)
	private String butietgf;
	
	/** 补贴贷款资金使用目的 STD_BUTIDKSY **/
	@Column(name = "butidksy", unique = false, nullable = true, length = 1)
	private String butidksy;
	
	/** 补贴金额计算方式 STD_BUTIJEJS **/
	@Column(name = "butijejs", unique = false, nullable = true, length = 1)
	private String butijejs;
	
	/** 补贴金额或比例 **/
	@Column(name = "butijine", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal butijine;
	
	/** 补贴申领时点 STD_BUTISLSD **/
	@Column(name = "butislsd", unique = false, nullable = true, length = 1)
	private String butislsd;
	
	/** 上次调息日 **/
	@Column(name = "sctxriqi", unique = false, nullable = true, length = 8)
	private String sctxriqi;
	
	/** 下次调息日 **/
	@Column(name = "xctxriqi", unique = false, nullable = true, length = 8)
	private String xctxriqi;
	
	/** 上次逾期调息日 **/
	@Column(name = "scyqtxrq", unique = false, nullable = true, length = 8)
	private String scyqtxrq;
	
	/** 下次逾期调息日 **/
	@Column(name = "xcyqtxrq", unique = false, nullable = true, length = 8)
	private String xcyqtxrq;
	
	/** 上次复利调息日 **/
	@Column(name = "scfxtxrq", unique = false, nullable = true, length = 8)
	private String scfxtxrq;
	
	/** 下次复利调息日 **/
	@Column(name = "xcfxtxrq", unique = false, nullable = true, length = 8)
	private String xcfxtxrq;
	
	/** 上次挪用调息日 **/
	@Column(name = "scnytxrq", unique = false, nullable = true, length = 8)
	private String scnytxrq;
	
	/** 下次挪用调息日 **/
	@Column(name = "xcnytxrq", unique = false, nullable = true, length = 8)
	private String xcnytxrq;
	
	/** 上次计息日 **/
	@Column(name = "scjixirq", unique = false, nullable = true, length = 8)
	private String scjixirq;
	
	/** 计息明细序号 **/
	@Column(name = "jiximxxh", unique = false, nullable = true, length = 19)
	private Long jiximxxh;
	
	/** 利率分段次月调整触发日 **/
	@Column(name = "llfdcycf", unique = false, nullable = true, length = 8)
	private String llfdcycf;
	
	/** 利率分段次月调整调整日 **/
	@Column(name = "llfdcytz", unique = false, nullable = true, length = 8)
	private String llfdcytz;
	
	/** 价税分离标志 STD_YESORNO **/
	@Column(name = "jsflbzhi", unique = false, nullable = true, length = 1)
	private String jsflbzhi;
	
	/** 税率类型 STD_SHLVLEIX **/
	@Column(name = "shlvleix", unique = false, nullable = true, length = 1)
	private String shlvleix;
	
	/** 税率代码 **/
	@Column(name = "shlvdaim", unique = false, nullable = true, length = 20)
	private String shlvdaim;
	
	/** 税率比例 **/
	@Column(name = "shlvbili", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal shlvbili;
	
	/** 分行标识 **/
	@Column(name = "fenhbios", unique = false, nullable = false, length = 4)
	private String fenhbios;
	
	/** 维护柜员 **/
	@Column(name = "WEIHGUIY", unique = false, nullable = true, length = 8)
	private String weihguiy;
	
	/** 维护机构 **/
	@Column(name = "weihjigo", unique = false, nullable = false, length = 12)
	private String weihjigo;
	
	/** 维护日期 **/
	@Column(name = "weihriqi", unique = false, nullable = false, length = 8)
	private String weihriqi;
	
	/** 维护时间 **/
	@Column(name = "weihshij", unique = false, nullable = true, length = 9)
	private String weihshij;
	
	/** 时间戳 **/
	@Column(name = "shijchuo", unique = false, nullable = false, length = 19)
	private Long shijchuo;
	
	/** 记录状态 STD_JILUZTAI **/
	@Column(name = "jiluztai", unique = false, nullable = false, length = 1)
	private String jiluztai;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param jixiguiz
	 */
	public void setJixiguiz(String jixiguiz) {
		this.jixiguiz = jixiguiz;
	}
	
    /**
     * @return jixiguiz
     */
	public String getJixiguiz() {
		return this.jixiguiz;
	}
	
	/**
	 * @param ltjixigz
	 */
	public void setLtjixigz(String ltjixigz) {
		this.ltjixigz = ltjixigz;
	}
	
    /**
     * @return ltjixigz
     */
	public String getLtjixigz() {
		return this.ltjixigz;
	}
	
	/**
	 * @param jixibjgz
	 */
	public void setJixibjgz(String jixibjgz) {
		this.jixibjgz = jixibjgz;
	}
	
    /**
     * @return jixibjgz
     */
	public String getJixibjgz() {
		return this.jixibjgz;
	}
	
	/**
	 * @param lixijsff
	 */
	public void setLixijsff(String lixijsff) {
		this.lixijsff = lixijsff;
	}
	
    /**
     * @return lixijsff
     */
	public String getLixijsff() {
		return this.lixijsff;
	}
	
	/**
	 * @param jixitwgz
	 */
	public void setJixitwgz(String jixitwgz) {
		this.jixitwgz = jixitwgz;
	}
	
    /**
     * @return jixitwgz
     */
	public String getJixitwgz() {
		return this.jixitwgz;
	}
	
	/**
	 * @param jixizxje
	 */
	public void setJixizxje(java.math.BigDecimal jixizxje) {
		this.jixizxje = jixizxje;
	}
	
    /**
     * @return jixizxje
     */
	public java.math.BigDecimal getJixizxje() {
		return this.jixizxje;
	}
	
	/**
	 * @param jixisrgz
	 */
	public void setJixisrgz(String jixisrgz) {
		this.jixisrgz = jixisrgz;
	}
	
    /**
     * @return jixisrgz
     */
	public String getJixisrgz() {
		return this.jixisrgz;
	}
	
	/**
	 * @param srzxdanw
	 */
	public void setSrzxdanw(String srzxdanw) {
		this.srzxdanw = srzxdanw;
	}
	
    /**
     * @return srzxdanw
     */
	public String getSrzxdanw() {
		return this.srzxdanw;
	}
	
	/**
	 * @param lilvrqgz
	 */
	public void setLilvrqgz(String lilvrqgz) {
		this.lilvrqgz = lilvrqgz;
	}
	
    /**
     * @return lilvrqgz
     */
	public String getLilvrqgz() {
		return this.lilvrqgz;
	}
	
	/**
	 * @param syhlleix
	 */
	public void setSyhlleix(String syhlleix) {
		this.syhlleix = syhlleix;
	}
	
    /**
     * @return syhlleix
     */
	public String getSyhlleix() {
		return this.syhlleix;
	}
	
	/**
	 * @param lilvleix
	 */
	public void setLilvleix(String lilvleix) {
		this.lilvleix = lilvleix;
	}
	
    /**
     * @return lilvleix
     */
	public String getLilvleix() {
		return this.lilvleix;
	}
	
	/**
	 * @param lilvqixx
	 */
	public void setLilvqixx(String lilvqixx) {
		this.lilvqixx = lilvqixx;
	}
	
    /**
     * @return lilvqixx
     */
	public String getLilvqixx() {
		return this.lilvqixx;
	}
	
	/**
	 * @param llqxkdfs
	 */
	public void setLlqxkdfs(String llqxkdfs) {
		this.llqxkdfs = llqxkdfs;
	}
	
    /**
     * @return llqxkdfs
     */
	public String getLlqxkdfs() {
		return this.llqxkdfs;
	}
	
	/**
	 * @param hetongll
	 */
	public void setHetongll(java.math.BigDecimal hetongll) {
		this.hetongll = hetongll;
	}
	
    /**
     * @return hetongll
     */
	public java.math.BigDecimal getHetongll() {
		return this.hetongll;
	}
	
	/**
	 * @param jixibzhi
	 */
	public void setJixibzhi(String jixibzhi) {
		this.jixibzhi = jixibzhi;
	}
	
    /**
     * @return jixibzhi
     */
	public String getJixibzhi() {
		return this.jixibzhi;
	}
	
	/**
	 * @param jfxibzhi
	 */
	public void setJfxibzhi(String jfxibzhi) {
		this.jfxibzhi = jfxibzhi;
	}
	
    /**
     * @return jfxibzhi
     */
	public String getJfxibzhi() {
		return this.jfxibzhi;
	}
	
	/**
	 * @param fxjxbzhi
	 */
	public void setFxjxbzhi(String fxjxbzhi) {
		this.fxjxbzhi = fxjxbzhi;
	}
	
    /**
     * @return fxjxbzhi
     */
	public String getFxjxbzhi() {
		return this.fxjxbzhi;
	}
	
	/**
	 * @param tiexibzh
	 */
	public void setTiexibzh(String tiexibzh) {
		this.tiexibzh = tiexibzh;
	}
	
    /**
     * @return tiexibzh
     */
	public String getTiexibzh() {
		return this.tiexibzh;
	}
	
	/**
	 * @param lilvfend
	 */
	public void setLilvfend(String lilvfend) {
		this.lilvfend = lilvfend;
	}
	
    /**
     * @return lilvfend
     */
	public String getLilvfend() {
		return this.lilvfend;
	}
	
	/**
	 * @param fdjixibz
	 */
	public void setFdjixibz(String fdjixibz) {
		this.fdjixibz = fdjixibz;
	}
	
    /**
     * @return fdjixibz
     */
	public String getFdjixibz() {
		return this.fdjixibz;
	}
	
	/**
	 * @param lxzbhbzh
	 */
	public void setLxzbhbzh(String lxzbhbzh) {
		this.lxzbhbzh = lxzbhbzh;
	}
	
    /**
     * @return lxzbhbzh
     */
	public String getLxzbhbzh() {
		return this.lxzbhbzh;
	}
	
	/**
	 * @param lxzbhbli
	 */
	public void setLxzbhbli(java.math.BigDecimal lxzbhbli) {
		this.lxzbhbli = lxzbhbli;
	}
	
    /**
     * @return lxzbhbli
     */
	public java.math.BigDecimal getLxzbhbli() {
		return this.lxzbhbli;
	}
	
	/**
	 * @param zbhjzhrq
	 */
	public void setZbhjzhrq(String zbhjzhrq) {
		this.zbhjzhrq = zbhjzhrq;
	}
	
    /**
     * @return zbhjzhrq
     */
	public String getZbhjzhrq() {
		return this.zbhjzhrq;
	}
	
	/**
	 * @param zclilvbh
	 */
	public void setZclilvbh(String zclilvbh) {
		this.zclilvbh = zclilvbh;
	}
	
    /**
     * @return zclilvbh
     */
	public String getZclilvbh() {
		return this.zclilvbh;
	}
	
	/**
	 * @param nyuelilv
	 */
	public void setNyuelilv(String nyuelilv) {
		this.nyuelilv = nyuelilv;
	}
	
    /**
     * @return nyuelilv
     */
	public String getNyuelilv() {
		return this.nyuelilv;
	}
	
	/**
	 * @param zhchlilv
	 */
	public void setZhchlilv(java.math.BigDecimal zhchlilv) {
		this.zhchlilv = zhchlilv;
	}
	
    /**
     * @return zhchlilv
     */
	public java.math.BigDecimal getZhchlilv() {
		return this.zhchlilv;
	}
	
	/**
	 * @param lilvtzfs
	 */
	public void setLilvtzfs(String lilvtzfs) {
		this.lilvtzfs = lilvtzfs;
	}
	
    /**
     * @return lilvtzfs
     */
	public String getLilvtzfs() {
		return this.lilvtzfs;
	}
	
	/**
	 * @param lilvtzzq
	 */
	public void setLilvtzzq(String lilvtzzq) {
		this.lilvtzzq = lilvtzzq;
	}
	
    /**
     * @return lilvtzzq
     */
	public String getLilvtzzq() {
		return this.lilvtzzq;
	}
	
	/**
	 * @param lilvfdfs
	 */
	public void setLilvfdfs(String lilvfdfs) {
		this.lilvfdfs = lilvfdfs;
	}
	
    /**
     * @return lilvfdfs
     */
	public String getLilvfdfs() {
		return this.lilvfdfs;
	}
	
	/**
	 * @param lilvfdzh
	 */
	public void setLilvfdzh(java.math.BigDecimal lilvfdzh) {
		this.lilvfdzh = lilvfdzh;
	}
	
    /**
     * @return lilvfdzh
     */
	public java.math.BigDecimal getLilvfdzh() {
		return this.lilvfdzh;
	}
	
	/**
	 * @param yqllcklx
	 */
	public void setYqllcklx(String yqllcklx) {
		this.yqllcklx = yqllcklx;
	}
	
    /**
     * @return yqllcklx
     */
	public String getYqllcklx() {
		return this.yqllcklx;
	}
	
	/**
	 * @param yuqillbh
	 */
	public void setYuqillbh(String yuqillbh) {
		this.yuqillbh = yuqillbh;
	}
	
    /**
     * @return yuqillbh
     */
	public String getYuqillbh() {
		return this.yuqillbh;
	}
	
	/**
	 * @param yuqinyll
	 */
	public void setYuqinyll(String yuqinyll) {
		this.yuqinyll = yuqinyll;
	}
	
    /**
     * @return yuqinyll
     */
	public String getYuqinyll() {
		return this.yuqinyll;
	}
	
	/**
	 * @param yuqililv
	 */
	public void setYuqililv(java.math.BigDecimal yuqililv) {
		this.yuqililv = yuqililv;
	}
	
    /**
     * @return yuqililv
     */
	public java.math.BigDecimal getYuqililv() {
		return this.yuqililv;
	}
	
	/**
	 * @param yuqitzfs
	 */
	public void setYuqitzfs(String yuqitzfs) {
		this.yuqitzfs = yuqitzfs;
	}
	
    /**
     * @return yuqitzfs
     */
	public String getYuqitzfs() {
		return this.yuqitzfs;
	}
	
	/**
	 * @param yuqitzzq
	 */
	public void setYuqitzzq(String yuqitzzq) {
		this.yuqitzzq = yuqitzzq;
	}
	
    /**
     * @return yuqitzzq
     */
	public String getYuqitzzq() {
		return this.yuqitzzq;
	}
	
	/**
	 * @param yqfxfdfs
	 */
	public void setYqfxfdfs(String yqfxfdfs) {
		this.yqfxfdfs = yqfxfdfs;
	}
	
    /**
     * @return yqfxfdfs
     */
	public String getYqfxfdfs() {
		return this.yqfxfdfs;
	}
	
	/**
	 * @param yqfxfdzh
	 */
	public void setYqfxfdzh(java.math.BigDecimal yqfxfdzh) {
		this.yqfxfdzh = yqfxfdzh;
	}
	
    /**
     * @return yqfxfdzh
     */
	public java.math.BigDecimal getYqfxfdzh() {
		return this.yqfxfdzh;
	}
	
	/**
	 * @param flllcklx
	 */
	public void setFlllcklx(String flllcklx) {
		this.flllcklx = flllcklx;
	}
	
    /**
     * @return flllcklx
     */
	public String getFlllcklx() {
		return this.flllcklx;
	}
	
	/**
	 * @param fulilvbh
	 */
	public void setFulilvbh(String fulilvbh) {
		this.fulilvbh = fulilvbh;
	}
	
    /**
     * @return fulilvbh
     */
	public String getFulilvbh() {
		return this.fulilvbh;
	}
	
	/**
	 * @param fulilvny
	 */
	public void setFulilvny(String fulilvny) {
		this.fulilvny = fulilvny;
	}
	
    /**
     * @return fulilvny
     */
	public String getFulilvny() {
		return this.fulilvny;
	}
	
	/**
	 * @param fulililv
	 */
	public void setFulililv(java.math.BigDecimal fulililv) {
		this.fulililv = fulililv;
	}
	
    /**
     * @return fulililv
     */
	public java.math.BigDecimal getFulililv() {
		return this.fulililv;
	}
	
	/**
	 * @param fulitzfs
	 */
	public void setFulitzfs(String fulitzfs) {
		this.fulitzfs = fulitzfs;
	}
	
    /**
     * @return fulitzfs
     */
	public String getFulitzfs() {
		return this.fulitzfs;
	}
	
	/**
	 * @param fulitzzq
	 */
	public void setFulitzzq(String fulitzzq) {
		this.fulitzzq = fulitzzq;
	}
	
    /**
     * @return fulitzzq
     */
	public String getFulitzzq() {
		return this.fulitzzq;
	}
	
	/**
	 * @param fulifdfs
	 */
	public void setFulifdfs(String fulifdfs) {
		this.fulifdfs = fulifdfs;
	}
	
    /**
     * @return fulifdfs
     */
	public String getFulifdfs() {
		return this.fulifdfs;
	}
	
	/**
	 * @param fulifdzh
	 */
	public void setFulifdzh(java.math.BigDecimal fulifdzh) {
		this.fulifdzh = fulifdzh;
	}
	
    /**
     * @return fulifdzh
     */
	public java.math.BigDecimal getFulifdzh() {
		return this.fulifdzh;
	}
	
	/**
	 * @param dknuoybz
	 */
	public void setDknuoybz(String dknuoybz) {
		this.dknuoybz = dknuoybz;
	}
	
    /**
     * @return dknuoybz
     */
	public String getDknuoybz() {
		return this.dknuoybz;
	}
	
	/**
	 * @param nyllcklx
	 */
	public void setNyllcklx(String nyllcklx) {
		this.nyllcklx = nyllcklx;
	}
	
    /**
     * @return nyllcklx
     */
	public String getNyllcklx() {
		return this.nyllcklx;
	}
	
	/**
	 * @param jznylvbh
	 */
	public void setJznylvbh(String jznylvbh) {
		this.jznylvbh = jznylvbh;
	}
	
    /**
     * @return jznylvbh
     */
	public String getJznylvbh() {
		return this.jznylvbh;
	}
	
	/**
	 * @param jznylvny
	 */
	public void setJznylvny(String jznylvny) {
		this.jznylvny = jznylvny;
	}
	
    /**
     * @return jznylvny
     */
	public String getJznylvny() {
		return this.jznylvny;
	}
	
	/**
	 * @param jznylilv
	 */
	public void setJznylilv(java.math.BigDecimal jznylilv) {
		this.jznylilv = jznylilv;
	}
	
    /**
     * @return jznylilv
     */
	public java.math.BigDecimal getJznylilv() {
		return this.jznylilv;
	}
	
	/**
	 * @param jznytzfs
	 */
	public void setJznytzfs(String jznytzfs) {
		this.jznytzfs = jznytzfs;
	}
	
    /**
     * @return jznytzfs
     */
	public String getJznytzfs() {
		return this.jznytzfs;
	}
	
	/**
	 * @param jznytzzq
	 */
	public void setJznytzzq(String jznytzzq) {
		this.jznytzzq = jznytzzq;
	}
	
    /**
     * @return jznytzzq
     */
	public String getJznytzzq() {
		return this.jznytzzq;
	}
	
	/**
	 * @param jznyfdfs
	 */
	public void setJznyfdfs(String jznyfdfs) {
		this.jznyfdfs = jznyfdfs;
	}
	
    /**
     * @return jznyfdfs
     */
	public String getJznyfdfs() {
		return this.jznyfdfs;
	}
	
	/**
	 * @param jznyfdzh
	 */
	public void setJznyfdzh(java.math.BigDecimal jznyfdzh) {
		this.jznyfdzh = jznyfdzh;
	}
	
    /**
     * @return jznyfdzh
     */
	public java.math.BigDecimal getJznyfdzh() {
		return this.jznyfdzh;
	}
	
	/**
	 * @param sjlljsfs
	 */
	public void setSjlljsfs(String sjlljsfs) {
		this.sjlljsfs = sjlljsfs;
	}
	
    /**
     * @return sjlljsfs
     */
	public String getSjlljsfs() {
		return this.sjlljsfs;
	}
	
	/**
	 * @param sjnylilv
	 */
	public void setSjnylilv(String sjnylilv) {
		this.sjnylilv = sjnylilv;
	}
	
    /**
     * @return sjnylilv
     */
	public String getSjnylilv() {
		return this.sjnylilv;
	}
	
	/**
	 * @param shijlilv
	 */
	public void setShijlilv(java.math.BigDecimal shijlilv) {
		this.shijlilv = shijlilv;
	}
	
    /**
     * @return shijlilv
     */
	public java.math.BigDecimal getShijlilv() {
		return this.shijlilv;
	}
	
	/**
	 * @param shjnlilv
	 */
	public void setShjnlilv(java.math.BigDecimal shjnlilv) {
		this.shjnlilv = shjnlilv;
	}
	
    /**
     * @return shjnlilv
     */
	public java.math.BigDecimal getShjnlilv() {
		return this.shjnlilv;
	}
	
	/**
	 * @param jitibzhi
	 */
	public void setJitibzhi(String jitibzhi) {
		this.jitibzhi = jitibzhi;
	}
	
    /**
     * @return jitibzhi
     */
	public String getJitibzhi() {
		return this.jitibzhi;
	}
	
	/**
	 * @param jitiguiz
	 */
	public void setJitiguiz(String jitiguiz) {
		this.jitiguiz = jitiguiz;
	}
	
    /**
     * @return jitiguiz
     */
	public String getJitiguiz() {
		return this.jitiguiz;
	}
	
	/**
	 * @param jitillgz
	 */
	public void setJitillgz(String jitillgz) {
		this.jitillgz = jitillgz;
	}
	
    /**
     * @return jitillgz
     */
	public String getJitillgz() {
		return this.jitillgz;
	}
	
	/**
	 * @param jitililv
	 */
	public void setJitililv(java.math.BigDecimal jitililv) {
		this.jitililv = jitililv;
	}
	
    /**
     * @return jitililv
     */
	public java.math.BigDecimal getJitililv() {
		return this.jitililv;
	}
	
	/**
	 * @param jitizhqi
	 */
	public void setJitizhqi(String jitizhqi) {
		this.jitizhqi = jitizhqi;
	}
	
    /**
     * @return jitizhqi
     */
	public String getJitizhqi() {
		return this.jitizhqi;
	}
	
	/**
	 * @param xcjtriqi
	 */
	public void setXcjtriqi(String xcjtriqi) {
		this.xcjtriqi = xcjtriqi;
	}
	
    /**
     * @return xcjtriqi
     */
	public String getXcjtriqi() {
		return this.xcjtriqi;
	}
	
	/**
	 * @param sszbjtbh
	 */
	public void setSszbjtbh(String sszbjtbh) {
		this.sszbjtbh = sszbjtbh;
	}
	
    /**
     * @return sszbjtbh
     */
	public String getSszbjtbh() {
		return this.sszbjtbh;
	}
	
	/**
	 * @param zaoqixbz
	 */
	public void setZaoqixbz(String zaoqixbz) {
		this.zaoqixbz = zaoqixbz;
	}
	
    /**
     * @return zaoqixbz
     */
	public String getZaoqixbz() {
		return this.zaoqixbz;
	}
	
	/**
	 * @param wanqixbz
	 */
	public void setWanqixbz(String wanqixbz) {
		this.wanqixbz = wanqixbz;
	}
	
    /**
     * @return wanqixbz
     */
	public String getWanqixbz() {
		return this.wanqixbz;
	}
	
	/**
	 * @param qixitshu
	 */
	public void setQixitshu(Long qixitshu) {
		this.qixitshu = qixitshu;
	}
	
    /**
     * @return qixitshu
     */
	public Long getQixitshu() {
		return this.qixitshu;
	}
	
	/**
	 * @param zqxizdds
	 */
	public void setZqxizdds(Long zqxizdds) {
		this.zqxizdds = zqxizdds;
	}
	
    /**
     * @return zqxizdds
     */
	public Long getZqxizdds() {
		return this.zqxizdds;
	}
	
	/**
	 * @param wqxizdds
	 */
	public void setWqxizdds(Long wqxizdds) {
		this.wqxizdds = wqxizdds;
	}
	
    /**
     * @return wqxizdds
     */
	public Long getWqxizdds() {
		return this.wqxizdds;
	}
	
	/**
	 * @param yushxfsh
	 */
	public void setYushxfsh(String yushxfsh) {
		this.yushxfsh = yushxfsh;
	}
	
    /**
     * @return yushxfsh
     */
	public String getYushxfsh() {
		return this.yushxfsh;
	}
	
	/**
	 * @param lixitxzq
	 */
	public void setLixitxzq(String lixitxzq) {
		this.lixitxzq = lixitxzq;
	}
	
    /**
     * @return lixitxzq
     */
	public String getLixitxzq() {
		return this.lixitxzq;
	}
	
	/**
	 * @param meictxfs
	 */
	public void setMeictxfs(String meictxfs) {
		this.meictxfs = meictxfs;
	}
	
    /**
     * @return meictxfs
     */
	public String getMeictxfs() {
		return this.meictxfs;
	}
	
	/**
	 * @param meictxbl
	 */
	public void setMeictxbl(java.math.BigDecimal meictxbl) {
		this.meictxbl = meictxbl;
	}
	
    /**
     * @return meictxbl
     */
	public java.math.BigDecimal getMeictxbl() {
		return this.meictxbl;
	}
	
	/**
	 * @param yushxize
	 */
	public void setYushxize(java.math.BigDecimal yushxize) {
		this.yushxize = yushxize;
	}
	
    /**
     * @return yushxize
     */
	public java.math.BigDecimal getYushxize() {
		return this.yushxize;
	}
	
	/**
	 * @param xiacitxr
	 */
	public void setXiacitxr(String xiacitxr) {
		this.xiacitxr = xiacitxr;
	}
	
    /**
     * @return xiacitxr
     */
	public String getXiacitxr() {
		return this.xiacitxr;
	}
	
	/**
	 * @param yunxycdd
	 */
	public void setYunxycdd(String yunxycdd) {
		this.yunxycdd = yunxycdd;
	}
	
    /**
     * @return yunxycdd
     */
	public String getYunxycdd() {
		return this.yunxycdd;
	}
	
	/**
	 * @param didaizhh
	 */
	public void setDidaizhh(String didaizhh) {
		this.didaizhh = didaizhh;
	}
	
    /**
     * @return didaizhh
     */
	public String getDidaizhh() {
		return this.didaizhh;
	}
	
	/**
	 * @param didzhzxh
	 */
	public void setDidzhzxh(String didzhzxh) {
		this.didzhzxh = didzhzxh;
	}
	
    /**
     * @return didzhzxh
     */
	public String getDidzhzxh() {
		return this.didzhzxh;
	}
	
	/**
	 * @param butielex
	 */
	public void setButielex(String butielex) {
		this.butielex = butielex;
	}
	
    /**
     * @return butielex
     */
	public String getButielex() {
		return this.butielex;
	}
	
	/**
	 * @param butisykh
	 */
	public void setButisykh(String butisykh) {
		this.butisykh = butisykh;
	}
	
    /**
     * @return butisykh
     */
	public String getButisykh() {
		return this.butisykh;
	}
	
	/**
	 * @param butietgf
	 */
	public void setButietgf(String butietgf) {
		this.butietgf = butietgf;
	}
	
    /**
     * @return butietgf
     */
	public String getButietgf() {
		return this.butietgf;
	}
	
	/**
	 * @param butidksy
	 */
	public void setButidksy(String butidksy) {
		this.butidksy = butidksy;
	}
	
    /**
     * @return butidksy
     */
	public String getButidksy() {
		return this.butidksy;
	}
	
	/**
	 * @param butijejs
	 */
	public void setButijejs(String butijejs) {
		this.butijejs = butijejs;
	}
	
    /**
     * @return butijejs
     */
	public String getButijejs() {
		return this.butijejs;
	}
	
	/**
	 * @param butijine
	 */
	public void setButijine(java.math.BigDecimal butijine) {
		this.butijine = butijine;
	}
	
    /**
     * @return butijine
     */
	public java.math.BigDecimal getButijine() {
		return this.butijine;
	}
	
	/**
	 * @param butislsd
	 */
	public void setButislsd(String butislsd) {
		this.butislsd = butislsd;
	}
	
    /**
     * @return butislsd
     */
	public String getButislsd() {
		return this.butislsd;
	}
	
	/**
	 * @param sctxriqi
	 */
	public void setSctxriqi(String sctxriqi) {
		this.sctxriqi = sctxriqi;
	}
	
    /**
     * @return sctxriqi
     */
	public String getSctxriqi() {
		return this.sctxriqi;
	}
	
	/**
	 * @param xctxriqi
	 */
	public void setXctxriqi(String xctxriqi) {
		this.xctxriqi = xctxriqi;
	}
	
    /**
     * @return xctxriqi
     */
	public String getXctxriqi() {
		return this.xctxriqi;
	}
	
	/**
	 * @param scyqtxrq
	 */
	public void setScyqtxrq(String scyqtxrq) {
		this.scyqtxrq = scyqtxrq;
	}
	
    /**
     * @return scyqtxrq
     */
	public String getScyqtxrq() {
		return this.scyqtxrq;
	}
	
	/**
	 * @param xcyqtxrq
	 */
	public void setXcyqtxrq(String xcyqtxrq) {
		this.xcyqtxrq = xcyqtxrq;
	}
	
    /**
     * @return xcyqtxrq
     */
	public String getXcyqtxrq() {
		return this.xcyqtxrq;
	}
	
	/**
	 * @param scfxtxrq
	 */
	public void setScfxtxrq(String scfxtxrq) {
		this.scfxtxrq = scfxtxrq;
	}
	
    /**
     * @return scfxtxrq
     */
	public String getScfxtxrq() {
		return this.scfxtxrq;
	}
	
	/**
	 * @param xcfxtxrq
	 */
	public void setXcfxtxrq(String xcfxtxrq) {
		this.xcfxtxrq = xcfxtxrq;
	}
	
    /**
     * @return xcfxtxrq
     */
	public String getXcfxtxrq() {
		return this.xcfxtxrq;
	}
	
	/**
	 * @param scnytxrq
	 */
	public void setScnytxrq(String scnytxrq) {
		this.scnytxrq = scnytxrq;
	}
	
    /**
     * @return scnytxrq
     */
	public String getScnytxrq() {
		return this.scnytxrq;
	}
	
	/**
	 * @param xcnytxrq
	 */
	public void setXcnytxrq(String xcnytxrq) {
		this.xcnytxrq = xcnytxrq;
	}
	
    /**
     * @return xcnytxrq
     */
	public String getXcnytxrq() {
		return this.xcnytxrq;
	}
	
	/**
	 * @param scjixirq
	 */
	public void setScjixirq(String scjixirq) {
		this.scjixirq = scjixirq;
	}
	
    /**
     * @return scjixirq
     */
	public String getScjixirq() {
		return this.scjixirq;
	}
	
	/**
	 * @param jiximxxh
	 */
	public void setJiximxxh(Long jiximxxh) {
		this.jiximxxh = jiximxxh;
	}
	
    /**
     * @return jiximxxh
     */
	public Long getJiximxxh() {
		return this.jiximxxh;
	}
	
	/**
	 * @param llfdcycf
	 */
	public void setLlfdcycf(String llfdcycf) {
		this.llfdcycf = llfdcycf;
	}
	
    /**
     * @return llfdcycf
     */
	public String getLlfdcycf() {
		return this.llfdcycf;
	}
	
	/**
	 * @param llfdcytz
	 */
	public void setLlfdcytz(String llfdcytz) {
		this.llfdcytz = llfdcytz;
	}
	
    /**
     * @return llfdcytz
     */
	public String getLlfdcytz() {
		return this.llfdcytz;
	}
	
	/**
	 * @param jsflbzhi
	 */
	public void setJsflbzhi(String jsflbzhi) {
		this.jsflbzhi = jsflbzhi;
	}
	
    /**
     * @return jsflbzhi
     */
	public String getJsflbzhi() {
		return this.jsflbzhi;
	}
	
	/**
	 * @param shlvleix
	 */
	public void setShlvleix(String shlvleix) {
		this.shlvleix = shlvleix;
	}
	
    /**
     * @return shlvleix
     */
	public String getShlvleix() {
		return this.shlvleix;
	}
	
	/**
	 * @param shlvdaim
	 */
	public void setShlvdaim(String shlvdaim) {
		this.shlvdaim = shlvdaim;
	}
	
    /**
     * @return shlvdaim
     */
	public String getShlvdaim() {
		return this.shlvdaim;
	}
	
	/**
	 * @param shlvbili
	 */
	public void setShlvbili(java.math.BigDecimal shlvbili) {
		this.shlvbili = shlvbili;
	}
	
    /**
     * @return shlvbili
     */
	public java.math.BigDecimal getShlvbili() {
		return this.shlvbili;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}