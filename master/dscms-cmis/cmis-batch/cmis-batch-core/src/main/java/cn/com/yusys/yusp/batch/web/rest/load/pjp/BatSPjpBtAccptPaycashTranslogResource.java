/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.pjp;

import cn.com.yusys.yusp.batch.domain.load.pjp.BatSPjpBtAccptPaycashTranslog;
import cn.com.yusys.yusp.batch.service.load.pjp.BatSPjpBtAccptPaycashTranslogService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtAccptPaycashTranslogResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-15 16:47:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batspjpbtaccptpaycashtranslog")
public class BatSPjpBtAccptPaycashTranslogResource {
    @Autowired
    private BatSPjpBtAccptPaycashTranslogService batSPjpBtAccptPaycashTranslogService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSPjpBtAccptPaycashTranslog>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSPjpBtAccptPaycashTranslog> list = batSPjpBtAccptPaycashTranslogService.selectAll(queryModel);
        return new ResultDto<List<BatSPjpBtAccptPaycashTranslog>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSPjpBtAccptPaycashTranslog>> index(QueryModel queryModel) {
        List<BatSPjpBtAccptPaycashTranslog> list = batSPjpBtAccptPaycashTranslogService.selectByModel(queryModel);
        return new ResultDto<List<BatSPjpBtAccptPaycashTranslog>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{id}")
    protected ResultDto<BatSPjpBtAccptPaycashTranslog> show(@PathVariable("id") String id) {
        BatSPjpBtAccptPaycashTranslog batSPjpBtAccptPaycashTranslog = batSPjpBtAccptPaycashTranslogService.selectByPrimaryKey(id);
        return new ResultDto<BatSPjpBtAccptPaycashTranslog>(batSPjpBtAccptPaycashTranslog);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSPjpBtAccptPaycashTranslog> create(@RequestBody BatSPjpBtAccptPaycashTranslog batSPjpBtAccptPaycashTranslog) throws URISyntaxException {
        batSPjpBtAccptPaycashTranslogService.insert(batSPjpBtAccptPaycashTranslog);
        return new ResultDto<BatSPjpBtAccptPaycashTranslog>(batSPjpBtAccptPaycashTranslog);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSPjpBtAccptPaycashTranslog batSPjpBtAccptPaycashTranslog) throws URISyntaxException {
        int result = batSPjpBtAccptPaycashTranslogService.update(batSPjpBtAccptPaycashTranslog);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{id}")
    protected ResultDto<Integer> delete(@PathVariable("id") String id) {
        int result = batSPjpBtAccptPaycashTranslogService.deleteByPrimaryKey(id);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSPjpBtAccptPaycashTranslogService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
