package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0129Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0129</br>
 * 任务名称：加工任务-业务处理-零售智能风控苏宁联合贷 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2128年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0129Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0129Service.class);

    @Autowired
    private Cmis0129Mapper cmis0129Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 插入贷款合同表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0129InsertCtrLoanCont(String openDay) {
        logger.info("插入贷款合同表开始,请求参数为:[{}]", openDay);
        int insertCtrLoanCont = cmis0129Mapper.insertCtrLoanCont(openDay);
        logger.info("插入贷款合同表结束,返回参数为:[{}]", insertCtrLoanCont);

        logger.info("插入贷款台账表开始,请求参数为:[{}]", openDay);
        int insertAccLoan = cmis0129Mapper.insertAccLoan(openDay);
        logger.info("插入贷款台账表结束,返回参数为:[{}]", insertAccLoan);

        logger.info("更新贷款台账表[来源表为苏宁联合贷借据信息临时表]开始,请求参数为:[{}]", openDay);
        int updateAccLoan03 = cmis0129Mapper.updateAccLoan03(openDay);
        logger.info("更新贷款台账表[来源表为苏宁联合贷借据信息临时表]结束,返回参数为:[{}]", updateAccLoan03);

        logger.info("重建临时表-清空网金五级分类数据开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_rcp_ant_sl_loan_cus_rfc");
        logger.info("重建临时表-清空网金五级分类数据结束");

        logger.info("插入网金五级分类数据开始,请求参数为:[{}]", openDay);
        int insertTmpRcpAntSlLoanCusRfc = cmis0129Mapper.insertTmpRcpAntSlLoanCusRfc(openDay);
        logger.info("插入网金五级分类数据结束,返回参数为:[{}]", insertTmpRcpAntSlLoanCusRfc);

        logger.info("更新网金台账五级分类数据开始,请求参数为:[{}]", openDay);
        int updateAccLoanWj = cmis0129Mapper.updateAccLoanWj(openDay);
        logger.info("更新网金台账五级分类数据结束,返回参数为:[{}]", updateAccLoanWj);

        logger.info("更新网金核销台账数据- 更新核销状态 蚂蚁一期 开始,请求参数为:[{}]", openDay);
        int updateAccLoanWj08 = cmis0129Mapper.updateAccLoanWj08(openDay);
        logger.info("更新网金核销台账数据- 更新核销状态 蚂蚁一期 结束,返回参数为:[{}]", updateAccLoanWj08);

        logger.info("更新网金核销台账数据- 更新核销状态 蚂蚁二期 开始,请求参数为:[{}]", openDay);
        int updateAccLoanWj09 = cmis0129Mapper.updateAccLoanWj09(openDay);
        logger.info("更新网金核销台账数据- 更新核销状态 蚂蚁二期 结束,返回参数为:[{}]", updateAccLoanWj09);

//        logger.info("网金贷款全部结清时，网金贷款协议注销开始,请求参数为:[{}]", openDay);
//        int updateCtrLoanContWj = cmis0129Mapper.updateCtrLoanContWj(openDay);
//        logger.info("网金贷款全部结清时，网金贷款协议注销结束,返回参数为:[{}]", updateCtrLoanContWj);

//        logger.info("网金贷款合同项下无台账开始, 到期自动注销开始,请求参数为:[{}]", openDay);
//        int updateCtrLoanContWjByNoLoan = cmis0129Mapper.updateCtrLoanContWjByNoLoan(openDay);
//        logger.info("网金贷款合同项下无台账结束,返回参数为:[{}]", updateCtrLoanContWjByNoLoan);
    }

    /**
     * 插入贷款台账表
     *
     * @param openDay
     */
    public void cmis0129InsertAccLoan(String openDay) {

    }

    /**
     * 更新贷款台账表
     *
     * @param openDay
     */
    public void cmis0129UpdateAccLoan(String openDay) {

    }
}
