package cn.com.yusys.yusp.batch.repository.mapper.load;

import java.util.List;
import java.util.Map;

/**
 * 获取数据库表相关工具类Mapper
 *
 * @author leehuang
 * @version 1.0
 */
public interface TableUtilsMapper {
    /**
     * 统计表的行数
     *
     * @param countMap
     * @return
     */
    int coutTableLines(Map countMap);

    /**
     * 统计批后备份表的行数
     *
     * @param bakDafMap
     * @return
     */
    int coutBakDafTableLines(Map bakDafMap);

    /**
     * 分析相关表
     *
     * @param
     * @return
     */
    List<Map> analyzeTable(Map analyzeMap);

    /**
     * 重命名相关表
     *
     * @param renameMap
     * @return
     */
    List<Map> renameTable(Map renameMap);

    /**
     * 创建相关表
     *
     * @param createMap
     * @return
     */
    List<Map> createTable(Map createMap);

    /**
     * 删除临时表
     *
     * @param dropMap
     * @return
     */
    List<Map> dropTable(Map dropMap);

    /**
     * 删除相关表(sets:allsets)
     *
     * @param dropWithSetsMap
     * @return
     */
    List<Map> dropTableWithSets(Map dropWithSetsMap);

    /**
     * 删除临时备份表
     *
     * @param dropBakMap
     * @return
     */
    List<Map> dropBakTable(Map dropBakMap);
}