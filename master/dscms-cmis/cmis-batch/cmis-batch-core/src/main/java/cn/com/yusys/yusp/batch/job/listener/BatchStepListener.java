package cn.com.yusys.yusp.batch.job.listener;


import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.stereotype.Component;

/**
 * @author admin
 * @description 公共Step监听类
 * @Create 2020-08-24 10:54
 */
@Component
public class BatchStepListener implements StepExecutionListener {

    private JobParameters parameters;

    @Override
    public void beforeStep(StepExecution stepExecution) {
        this.parameters = stepExecution.getJobParameters();
        stepExecution.getJobParameters().getString("openDay");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return null;
    }
}
