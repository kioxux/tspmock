package cn.com.yusys.yusp.batch.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

/**
 * 从ApplicationContext中获取bean对象
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月21日 下午11:56:54
 */
@Configuration
public class BatchBeanFactory implements ApplicationContextAware, DisposableBean {
    private static final Logger logger = LoggerFactory.getLogger(BatchBeanFactory.class);
    private static ApplicationContext applicationContext = null;

    /**
     * 取得存储在静态变量中的ApplicationContext
     *
     * @return
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static <T> T getBean(String beanName) {
        logger.info("从BatchBeanFactory中获取的Bean为:[{}]", beanName);
        return (T) applicationContext.getBean(beanName);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (Objects.isNull(BatchBeanFactory.applicationContext)) {
            logger.warn("BatchBeanFactory中ApplicationContext被覆盖,原有ApplicationContext为:[{}]", BatchBeanFactory.applicationContext);
        }
        BatchBeanFactory.applicationContext = applicationContext;
    }


    @Override
    public void destroy() throws Exception {
        logger.info("清除BatchBeanFactory中ApplicationContext为:[{}]", applicationContext);
        applicationContext = null;
    }
}
