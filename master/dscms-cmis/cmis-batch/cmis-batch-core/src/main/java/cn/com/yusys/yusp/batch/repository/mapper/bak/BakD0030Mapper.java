package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKD0030</br>
 * 任务名称：批前备份日表任务-备份我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]  </br>
 *
 * @author chenyong
 * @version 1.0
 * @since 2020年9月24日
 */
public interface BakD0030Mapper {

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertCurrent(@Param("openDay") String openDay);

    /**
     * 删除当天数据
     *
     * @return
     */
    void truncateCurrent();

    /**
     * 删除当天的数据
     *
     * @param openDay
     * @return
     */
    int deleteCurrent(@Param("openDay") String openDay);


    /**
     * 查询备份表当天的[我行代开他行信用证[tmp_d_lmt_peer_rzkz]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryD0030DeleteOpenDayTmpGjpLmtPeerRzkzCounts(@Param("openDay") String openDay);

    /**
     *
     * 查询原表当天的[我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]]数据总条数
     * @param openDay
     * @return
     */
    int queryTmpGjpLmtPeerRzkzCounts(@Param("openDay") String openDay);
}
