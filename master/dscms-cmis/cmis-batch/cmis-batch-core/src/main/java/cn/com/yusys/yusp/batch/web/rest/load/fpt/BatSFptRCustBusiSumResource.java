/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.fpt;

import cn.com.yusys.yusp.batch.domain.load.fpt.BatSFptRCustBusiSum;
import cn.com.yusys.yusp.batch.service.load.fpt.BatSFptRCustBusiSumService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSFptRCustBusiSumResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-12 10:11:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsfptrcustbusisum")
public class BatSFptRCustBusiSumResource {
    @Autowired
    private BatSFptRCustBusiSumService batSFptRCustBusiSumService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSFptRCustBusiSum>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSFptRCustBusiSum> list = batSFptRCustBusiSumService.selectAll(queryModel);
        return new ResultDto<List<BatSFptRCustBusiSum>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSFptRCustBusiSum>> index(QueryModel queryModel) {
        List<BatSFptRCustBusiSum> list = batSFptRCustBusiSumService.selectByModel(queryModel);
        return new ResultDto<List<BatSFptRCustBusiSum>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{custId}")
    protected ResultDto<BatSFptRCustBusiSum> show(@PathVariable("custId") String custId) {
        BatSFptRCustBusiSum batSFptRCustBusiSum = batSFptRCustBusiSumService.selectByPrimaryKey(custId);
        return new ResultDto<BatSFptRCustBusiSum>(batSFptRCustBusiSum);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSFptRCustBusiSum> create(@RequestBody BatSFptRCustBusiSum batSFptRCustBusiSum) throws URISyntaxException {
        batSFptRCustBusiSumService.insert(batSFptRCustBusiSum);
        return new ResultDto<BatSFptRCustBusiSum>(batSFptRCustBusiSum);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSFptRCustBusiSum batSFptRCustBusiSum) throws URISyntaxException {
        int result = batSFptRCustBusiSumService.update(batSFptRCustBusiSum);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{custId}")
    protected ResultDto<Integer> delete(@PathVariable("custId") String custId) {
        int result = batSFptRCustBusiSumService.deleteByPrimaryKey(custId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSFptRCustBusiSumService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
