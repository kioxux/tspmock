package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0210Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0210</br>
 * 任务名称：加工任务-额度处理-普通授信额度占用处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0210Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0210Service.class);
    @Autowired
    private Cmis0210Mapper cmis0210Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新批复额度分项基础信息，对应SQL为CMIS0210-01.sql
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0210UpdateApprLmtSubBasicInfo(String openDay) {
        logger.info("处理BIZ_STATUS交易业务状态200有效 开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A = cmis0210Mapper.updateLmtContRel0A(openDay);
        logger.info("处理BIZ_STATUS交易业务状态200有效 结束,返回参数为:[{}]", updateLmtContRel0A);

        logger.info("******1>关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效******开始******");
        logger.info("1>贷款合同表-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A1Dkht = cmis0210Mapper.updateLmtContRel0A1Dkht(openDay);
        logger.info("1>贷款合同表-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效结束,返回参数为:[{}]", updateLmtContRel0A1Dkht);

        logger.info("1>最高额授信协议-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A1Zgesxxy = cmis0210Mapper.updateLmtContRel0A1Zgesxxy(openDay);
        logger.info("1>最高额授信协议-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效结束,返回参数为:[{}]", updateLmtContRel0A1Zgesxxy);

        logger.info(" 1>委托贷款合同详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A1Wtdkhtxq = cmis0210Mapper.updateLmtContRel0A1Wtdkhtxq(openDay);
        logger.info(" 1>委托贷款合同详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效结束,返回参数为:[{}]", updateLmtContRel0A1Wtdkhtxq);

        logger.info("1>贴现协议详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A1Txxyxq = cmis0210Mapper.updateLmtContRel0A1Txxyxq(openDay);
        logger.info("1>贴现协议详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效结束,返回参数为:[{}]", updateLmtContRel0A1Txxyxq);


        logger.info("1>保函协议详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A1Bhxyxq = cmis0210Mapper.updateLmtContRel0A1Bhxyxq(openDay);
        logger.info("1>保函协议详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效结束,返回参数为:[{}]", updateLmtContRel0A1Bhxyxq);

        logger.info("1>开证合同详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A1Kzhtxq = cmis0210Mapper.updateLmtContRel0A1Kzhtxq(openDay);
        logger.info("1>开证合同详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效结束,返回参数为:[{}]", updateLmtContRel0A1Kzhtxq);

        logger.info("1>银承合同详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A1Ychtxq = cmis0210Mapper.updateLmtContRel0A1Ychtxq(openDay);
        logger.info("1>银承合同详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效结束,返回参数为:[{}]", updateLmtContRel0A1Ychtxq);

        logger.info("1>资产池协议台账表-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A1Zcc = cmis0210Mapper.updateLmtContRel0A1Zcc(openDay);
        logger.info("1>资产池协议台账表-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效结束,返回参数为:[{}]", updateLmtContRel0A1Zcc);
        logger.info("******1>关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效******结束******");


        logger.info("******1>关联合同状态处理；合同状态为作废，占用关系更新为 400-作废******开始******");
        logger.info("1> 贷款合同表-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A2Dkht = cmis0210Mapper.updateLmtContRel0A2Dkht(openDay);
        logger.info("1> 贷款合同表-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废结束,返回参数为:[{}]", updateLmtContRel0A2Dkht);

        logger.info("1> 最高额授信协议-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A2Zgesxxy = cmis0210Mapper.updateLmtContRel0A2Zgesxxy(openDay);
        logger.info("1> 最高额授信协议-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废结束,返回参数为:[{}]", updateLmtContRel0A2Zgesxxy);

        logger.info("1> 委托贷款合同详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A2Wtdkhtxq = cmis0210Mapper.updateLmtContRel0A2Wtdkhtxq(openDay);
        logger.info("1> 委托贷款合同详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废结束,返回参数为:[{}]", updateLmtContRel0A2Wtdkhtxq);

        logger.info("1> 贴现协议详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A2Txxyxq = cmis0210Mapper.updateLmtContRel0A2Txxyxq(openDay);
        logger.info("1> 贴现协议详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废结束,返回参数为:[{}]", updateLmtContRel0A2Txxyxq);

        logger.info("1> 保函协议详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A2Bhxyxq = cmis0210Mapper.updateLmtContRel0A2Bhxyxq(openDay);
        logger.info("1> 保函协议详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废结束,返回参数为:[{}]", updateLmtContRel0A2Bhxyxq);

        logger.info("1> 开证合同详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A2Kzhtxq = cmis0210Mapper.updateLmtContRel0A2Kzhtxq(openDay);
        logger.info("1> 开证合同详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废结束,返回参数为:[{}]", updateLmtContRel0A2Kzhtxq);

        logger.info("1> 银承合同详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A2Ychtxq = cmis0210Mapper.updateLmtContRel0A2Ychtxq(openDay);
        logger.info("1> 银承合同详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废结束,返回参数为:[{}]", updateLmtContRel0A2Ychtxq);

        logger.info("1> 资产池协议台账表-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A2Zcc = cmis0210Mapper.updateLmtContRel0A2Zcc(openDay);
        logger.info("1> 资产池协议台账表-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废结束,返回参数为:[{}]", updateLmtContRel0A2Zcc);
        logger.info("******1>关联合同状态处理；合同状态为作废，占用关系更新为 400-作废******结束******");


        logger.info("******1>关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销******结束******");
        logger.info("1> 贷款合同表-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A3Dkht = cmis0210Mapper.updateLmtContRel0A3Dkht(openDay);
        logger.info("1> 贷款合同表-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销结束,返回参数为:[{}]", updateLmtContRel0A3Dkht);

        logger.info("1> 最高额授信协议-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A3Zge = cmis0210Mapper.updateLmtContRel0A3Zge(openDay);
        logger.info("1> 最高额授信协议-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销结束,返回参数为:[{}]", updateLmtContRel0A3Zge);

        logger.info("1> 委托贷款合同详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A3Wtdk = cmis0210Mapper.updateLmtContRel0A3Wtdk(openDay);
        logger.info("1> 委托贷款合同详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销结束,返回参数为:[{}]", updateLmtContRel0A3Wtdk);

        logger.info("1> 贴现协议详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A3Txxx = cmis0210Mapper.updateLmtContRel0A3Txxx(openDay);
        logger.info("1> 贴现协议详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销结束,返回参数为:[{}]", updateLmtContRel0A3Txxx);

        logger.info("1> 保函协议详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A3Bhxy = cmis0210Mapper.updateLmtContRel0A3Bhxy(openDay);
        logger.info("1> 保函协议详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销结束,返回参数为:[{}]", updateLmtContRel0A3Bhxy);

        logger.info("1> 开证合同详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A3Kzht = cmis0210Mapper.updateLmtContRel0A3Kzht(openDay);
        logger.info("1> 开证合同详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销结束,返回参数为:[{}]", updateLmtContRel0A3Kzht);

        logger.info("1> 银承合同详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A3Ycht = cmis0210Mapper.updateLmtContRel0A3Ycht(openDay);
        logger.info("1> 银承合同详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销结束,返回参数为:[{}]", updateLmtContRel0A3Ycht);

        logger.info("1> 资产池协议台账表-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A3Zcc = cmis0210Mapper.updateLmtContRel0A3Zcc(openDay);
        logger.info("1> 资产池协议台账表-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销结束,返回参数为:[{}]", updateLmtContRel0A3Zcc);

        logger.info("******1>关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销******结束******");

        logger.info("******1>关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废******开始******");
        logger.info("1> 贷款合同表-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A4Dkht = cmis0210Mapper.updateLmtContRel0A4Dkht(openDay);
        logger.info("1> 贷款合同表-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废结束,返回参数为:[{}]", updateLmtContRel0A4Dkht);

        logger.info("1> 最高额授信协议-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A4Zge = cmis0210Mapper.updateLmtContRel0A4Zge(openDay);
        logger.info("1> 最高额授信协议-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废结束,返回参数为:[{}]", updateLmtContRel0A4Zge);

        logger.info("1> 委托贷款合同详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A4Wtdkht = cmis0210Mapper.updateLmtContRel0A4Wtdkht(openDay);
        logger.info("1> 委托贷款合同详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废结束,返回参数为:[{}]", updateLmtContRel0A4Wtdkht);

        logger.info("1> 贴现协议详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A4Txxy = cmis0210Mapper.updateLmtContRel0A4Txxy(openDay);
        logger.info("1> 贴现协议详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废结束,返回参数为:[{}]", updateLmtContRel0A4Txxy);

        logger.info("1> 保函协议详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A4Bhxy = cmis0210Mapper.updateLmtContRel0A4Bhxy(openDay);
        logger.info("1> 保函协议详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废结束,返回参数为:[{}]", updateLmtContRel0A4Bhxy);

        logger.info("1> 开证合同详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A4Kzht = cmis0210Mapper.updateLmtContRel0A4Kzht(openDay);
        logger.info("1> 开证合同详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废结束,返回参数为:[{}]", updateLmtContRel0A4Kzht);

        logger.info("1> 银承合同详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A4Ycht = cmis0210Mapper.updateLmtContRel0A4Ycht(openDay);
        logger.info("1> 银承合同详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废结束,返回参数为:[{}]", updateLmtContRel0A4Ycht);

        logger.info("1> 资产池协议台账表-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A4Zcc = cmis0210Mapper.updateLmtContRel0A4Zcc(openDay);
        logger.info("1> 资产池协议台账表-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废结束,返回参数为:[{}]", updateLmtContRel0A4Zcc);
        logger.info("******1>关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废******结束******");

        logger.info("******1>关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清******开始******");

        logger.info("1> 贷款合同表-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A5Dkht = cmis0210Mapper.updateLmtContRel0A5Dkht(openDay);
        logger.info("1> 贷款合同表-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清结束,返回参数为:[{}]", updateLmtContRel0A5Dkht);

        logger.info("1> 最高额授信协议-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A5Zge = cmis0210Mapper.updateLmtContRel0A5Zge(openDay);
        logger.info("1> 最高额授信协议-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清结束,返回参数为:[{}]", updateLmtContRel0A5Zge);

        logger.info("1> 委托贷款合同详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A5Wtdkht = cmis0210Mapper.updateLmtContRel0A5Wtdkht(openDay);
        logger.info("1> 委托贷款合同详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清结束,返回参数为:[{}]", updateLmtContRel0A5Wtdkht);

        logger.info("1> 贴现协议详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A5Txxy = cmis0210Mapper.updateLmtContRel0A5Txxy(openDay);
        logger.info("1> 贴现协议详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清结束,返回参数为:[{}]", updateLmtContRel0A5Txxy);

        logger.info("1> 保函协议详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A5Bhxy = cmis0210Mapper.updateLmtContRel0A5Bhxy(openDay);
        logger.info("1> 保函协议详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清结束,返回参数为:[{}]", updateLmtContRel0A5Bhxy);

        logger.info("1> 开证合同详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A5Kzht = cmis0210Mapper.updateLmtContRel0A5Kzht(openDay);
        logger.info("1> 开证合同详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清结束,返回参数为:[{}]", updateLmtContRel0A5Kzht);

        logger.info("1> 银承合同详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A5Ycht = cmis0210Mapper.updateLmtContRel0A5Ycht(openDay);
        logger.info("1> 银承合同详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清结束,返回参数为:[{}]", updateLmtContRel0A5Ycht);

        logger.info("1> 资产池协议台账表-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清开始,请求参数为:[{}]", openDay);
        int updateLmtContRel0A5Zcc = cmis0210Mapper.updateLmtContRel0A5Zcc(openDay);
        logger.info("1> 资产池协议台账表-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清结束,返回参数为:[{}]", updateLmtContRel0A5Zcc);
        logger.info("******1>关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清******结束******");

        logger.info("更新分项占用关系信息开始,请求参数为:[{}]", openDay);
        int updateLmtContRel4A = cmis0210Mapper.updateLmtContRel4A(openDay);
        logger.info("更新分项占用关系信息结束，返回参数为:[{}]", updateLmtContRel4A);


        logger.info("truncateApprLmtSubBasicInfo01清理占用关系表开始,请求参数为:[{}]", openDay);
        // cmis0210Mapper.truncateApprLmtSubBasicInfo01();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateApprLmtSubBasicInfo01清理占用关系表结束");

        logger.info("其他类型 处理额度处理开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfo01 = cmis0210Mapper.insertApprLmtSubBasicInfo01(openDay);
        logger.info("其他类型 处理额度处理结束,返回参数为:[{}]", insertApprLmtSubBasicInfo01);

        logger.info("更新批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo01 = cmis0210Mapper.updateApprLmtSubBasicInfo01(openDay);
        logger.info("更新批复额度分项基础信息结束,返回参数为:[{}]", updateApprLmtSubBasicInfo01);

        logger.info("truncateTmpDealLmtDetails清空加工占用授信表[同业额度分项项下已用金额汇总(同业票据类、同业贸融类)]开始,请求参数为:[{}]", openDay);
        // cmis0210Mapper.truncateTmpDealLmtDetails();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpDealLmtDetails清空加工占用授信表[同业额度分项项下已用金额汇总(同业票据类、同业贸融类)]结束");

        logger.info("插入加工占用授信表[同业额度分项项下已用金额汇总(同业票据类、同业贸融类)]处理开始,请求参数为:[{}]", openDay);
        int insertTmpDealLmtDetails = cmis0210Mapper.insertTmpDealLmtDetails(openDay);
        logger.info("插入加工占用授信表[同业额度分项项下已用金额汇总(同业票据类、同业贸融类)]处理结束,返回参数为:[{}]", insertTmpDealLmtDetails);

        logger.info("更新加工占用授信表[同业额度分项项下已用金额汇总(同业票据类、同业贸融类)]开始,请求参数为:[{}]", openDay);
        int updateTmpDealLmtDetails = cmis0210Mapper.updateTmpDealLmtDetails(openDay);
        logger.info("更新加工占用授信表[同业额度分项项下已用金额汇总(同业票据类、同业贸融类)]结束,返回参数为:[{}]", updateTmpDealLmtDetails);


        logger.info("truncateApprLmtSubBasicInfo02清理占用关系表开始,请求参数为:[{}]", openDay);
        // cmis0210Mapper.truncateApprLmtSubBasicInfo02();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateApprLmtSubBasicInfo02清理占用关系表结束");

        logger.info("插入关系表[一级额度分项下合同占额  + 项下二级分项下 合同占用类型的 合同占用总余额之和]开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfo02 = cmis0210Mapper.insertApprLmtSubBasicInfo02(openDay);
        logger.info("插入关系表[一级额度分项下合同占额  + 项下二级分项下 合同占用类型的 合同占用总余额之和]结束,返回参数为:[{}]", insertApprLmtSubBasicInfo02);

        logger.info("更新关系表[一级额度分项下合同占额  + 项下二级分项下 合同占用类型的 合同占用总余额之和]开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo02 = cmis0210Mapper.updateApprLmtSubBasicInfo02(openDay);
        logger.info("更新关系表[一级额度分项下合同占额  + 项下二级分项下 合同占用类型的 合同占用总余额之和]结束,返回参数为:[{}]", updateApprLmtSubBasicInfo02);

        // 20211106 zjw 修复承兑行白名单额度已用金额更新 bug 开始
        logger.info("truncateTdlwiTla清理占用关系表-承兑行白名单额度已用金额更新开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211106 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTdlwiTla清理占用关系表-承兑行白名单额度已用金额更新结束");

        logger.info("插入占用关系表-承兑行白名单额度已用金额更新 开始,请求参数为:[{}]", openDay);
        int insertTdlwiTla = cmis0210Mapper.insertTdlwiTla(openDay);
        logger.info("插入占用关系表-承兑行白名单额度已用金额更新 结束,返回参数为:[{}]", insertTdlwiTla);

        logger.info("承兑行白名单额度已用金额更新开始,请求参数为:[{}]", openDay);
        int updateTdlwiTla = cmis0210Mapper.updateTdlwiTla(openDay);
        logger.info("承兑行白名单额度已用金额更新 结束,返回参数为:[{}]", updateTdlwiTla);
        // 20211106 zjw 修复承兑行白名单额度已用金额更新 bug 结束
    }

}
