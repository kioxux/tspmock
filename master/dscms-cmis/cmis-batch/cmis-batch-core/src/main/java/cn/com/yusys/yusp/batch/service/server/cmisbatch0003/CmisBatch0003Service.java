package cn.com.yusys.yusp.batch.service.server.cmisbatch0003;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003RespDto;
import cn.com.yusys.yusp.batch.service.load.core.BatSCoreKlnbDkhkjhService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CmisBatch0003Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0003Service.class);
    @Autowired
    private BatSCoreKlnbDkhkjhService batSCoreKlnbDkhkjhService;// 核心系统-历史表-贷款还款计划表

    /**
     * 交易码：cmisbatch0003
     * 交易描述：查询[贷款还款计划表]和[贷款期供交易明细]关联信息
     *
     * @param cmisbatch0003ReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Cmisbatch0003RespDto cmisBatch0003(Cmisbatch0003ReqDto cmisbatch0003ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value);
        Cmisbatch0003RespDto cmisbatch0003RespDto = new Cmisbatch0003RespDto();// 响应Dto:调度运行管理信息查詢
        try {
            //请求字段
            String dkjiejuh = cmisbatch0003ReqDto.getDkjiejuh();//贷款借据号
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("dkjiejuh", dkjiejuh);//贷款借据号
            cmisbatch0003RespDto = batSCoreKlnbDkhkjhService.selectByDkjiejuh(queryModel);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0003.key, DscmsEnum.TRADE_CODE_CMISBATCH0003.value);
        return cmisbatch0003RespDto;
    }
}
