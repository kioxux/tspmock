/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.c2d;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.c2d.BatSC2dPd01eRepaystat60sub;
import cn.com.yusys.yusp.batch.repository.mapper.load.c2d.BatSC2dPd01eRepaystat60subMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dPd01eRepaystat60subService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:02:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSC2dPd01eRepaystat60subService {
    private static final Logger logger = LoggerFactory.getLogger(BatSC2dPd01eRepaystat60subService.class);
    @Autowired
    private BatSC2dPd01eRepaystat60subMapper batSC2dPd01eRepaystat60subMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSC2dPd01eRepaystat60sub selectByPrimaryKey(String id, String reportId) {
        return batSC2dPd01eRepaystat60subMapper.selectByPrimaryKey(id, reportId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSC2dPd01eRepaystat60sub> selectAll(QueryModel model) {
        List<BatSC2dPd01eRepaystat60sub> records = (List<BatSC2dPd01eRepaystat60sub>) batSC2dPd01eRepaystat60subMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSC2dPd01eRepaystat60sub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSC2dPd01eRepaystat60sub> list = batSC2dPd01eRepaystat60subMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSC2dPd01eRepaystat60sub record) {
        return batSC2dPd01eRepaystat60subMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSC2dPd01eRepaystat60sub record) {
        return batSC2dPd01eRepaystat60subMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSC2dPd01eRepaystat60sub record) {
        return batSC2dPd01eRepaystat60subMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSC2dPd01eRepaystat60sub record) {
        return batSC2dPd01eRepaystat60subMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String id, String reportId) {
        return batSC2dPd01eRepaystat60subMapper.deleteByPrimaryKey(id, reportId);
    }

    /**
     * 删除S表当天数据
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建最近5年内的历史表现信息表[bat_s_c2d_pd01e_repaystat60sub]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_c2d_pd01e_repaystat60sub");
        logger.info("重建【C2D00009】最近5年内的历史表现信息表[bat_s_c2d_pd01e_repaystat60sub]结束");
        return 0;
    }

    /**
     * 删除业务表数据
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteBizByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建最近5年内的历史表现信息表[pd01e_repaystat60sub]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz", "pd01e_repaystat60sub");
        logger.info("重建【C2D00009】最近5年内的历史表现信息表[pd01e_repaystat60sub]结束");
        return 0;
    }

    /**
     * 更新创建时间和修改时间
     * @param openDay
     */
    public void updateBizTime(String openDay) {
        batSC2dPd01eRepaystat60subMapper.updateBizTime(openDay);
    }
}
