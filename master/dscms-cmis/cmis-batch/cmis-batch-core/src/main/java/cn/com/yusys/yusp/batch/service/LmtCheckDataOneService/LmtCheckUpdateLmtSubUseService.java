/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.LmtCheckDataOneService;

import cn.com.yusys.yusp.batch.repository.mapper.check.LmtCheckUpdateLmtSubUseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckInsertContAccRelService
 * @类描述: #服务类
 * @功能描述: 数据更新额度校正-计算分项用信金额
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCheckUpdateLmtSubUseService {

    private static final Logger logger = LoggerFactory.getLogger(LmtCheckUpdateOutStandAmtBWService.class);

    @Autowired
    private LmtCheckUpdateLmtSubUseMapper lmtCheckUpdateLmtSubUseMapper;

    public void checkOutLmtCheckUpdateLmtSubUse(String cusId){
        logger.info("7.LmtCheckUpdateLmtSubUse数据更新额度校正-额度占用处理 --------start");
        /** 1、若合同审批状态为在途，则更新占用关系为200-生效 **/
        updateLmtSubUseHtspzt(cusId) ;

        /** 2、若合同项下台账还有生效状态的，则更新合同状态为生效 **/
        updateLmtSubUseHtxtzsx(cusId) ;

        /** 3、若合同状态为生效，则更新占用关系状态为生效 **/
        updateLmtSubUseHtztsx(cusId) ;

        /** 4、若合同状态为作废，则更新占用关系状态为作废 **/
        updateLmtSubUseHtztzf(cusId) ;

        /** 5、若合同状态为注销或中止，项下存在非作废的台账，则更新占用关系状态为 300-注销 **/
        updateLmtSubUseHtztzxorzzAndExistFzftz(cusId) ;

        /** 6、若合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废 **/
        updateLmtSubUseHtztCancelOrStopAndNoExistFzftz(cusId) ;

        /** 7、合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清 **/
        updateLmtSubUseHtztCancelOrStopOrActiveOrEnd(cusId) ;

        /** 8、将计算后的合同占用关系 更新至  正式表 **/
        updateLmtSubUseCalculateToFormalTable1(cusId) ;

        /** 9、将计算后的合同占用关系 更新至  正式表 **/
        updateLmtSubUseCalculateToFormalTable2(cusId) ;

        /** 10、将计算后的台账占用关系 更新至  正式表（台账占用额度） **/
        updateLmtSubUseTzzyToFormal(cusId) ;

        /** 11、根据占用关系更新分项 总额已用、敞口已用 **/
        updateLmtSubUseZeyyAndCkyy(cusId) ;

        /** 12、二级分项占额需要汇总至一级分项 **/
        updateLmtSubUse2LevelZeTo1Level(cusId) ;
        logger.info("7.LmtCheckUpdateLmtSubUse数据更新额度校正-额度占用处理 --------end");
    }

    /**
     * 1、若合同审批状态为在途，则更新占用关系为200-生效
     */
    public int updateLmtSubUseHtspzt(String cusId){
        logger.info("1、若合同审批状态为在途，则更新占用关系为200-生效 -------- ");
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseHtspzt(cusId);
    }

    /**
     * 2、若合同项下台账还有生效状态的，则更新合同状态为生效
     */
    public int updateLmtSubUseHtxtzsx(String cusId){
        logger.info("2、若合同项下台账还有生效状态的，则更新合同状态为生效 -------- ");
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseHtxtzsx(cusId);
    }

    /**
     * 3、若合同状态为生效，则更新占用关系状态为生效
     */
    public int updateLmtSubUseHtztsx(String cusId){
        logger.info("3、若合同状态为生效，则更新占用关系状态为生效 -------- ");
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseHtztsx(cusId);
    }

    /**
     * 4、若合同状态为作废，则更新占用关系状态为作废
     */
    public int updateLmtSubUseHtztzf(String cusId){
        logger.info("4、若合同状态为作废，则更新占用关系状态为作废 -------- ");
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseHtztzf(cusId);
    }

    /**
     * 5、若合同状态为注销或中止，项下存在非作废的台账，则更新占用关系状态为 300-注销
     */
    public int updateLmtSubUseHtztzxorzzAndExistFzftz(String cusId){
        logger.info("5、若合同状态为注销或中止，项下存在非作废的台账，则更新占用关系状态为 300-注销 --------   ");
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseHtztzxorzzAndExistFzftz(cusId);
    }

    /**
     * 6、若合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     */
    public int updateLmtSubUseHtztCancelOrStopAndNoExistFzftz(String cusId){
        logger.info("6、若合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废 -------- ");
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseHtztCancelOrStopAndNoExistFzftz(cusId);
    }

    /**
     * 7、合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     */
    public int updateLmtSubUseHtztCancelOrStopOrActiveOrEnd(String cusId){
        logger.info("7、合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清 -------- ");
        lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseHtztCancelOrStopOrActiveOrEnd1(cusId);
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseHtztCancelOrStopOrActiveOrEnd2(cusId);
    }

    /**
     * 8、将计算后的合同占用关系 更新至  正式表
     */
    public int updateLmtSubUseCalculateToFormalTable1(String cusId){
        logger.info("8、将计算后的合同占用关系 更新至  正式表-------- ");
        lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseCalculateToFormalTable1_1(cusId);
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseCalculateToFormalTable1_2(cusId);
    }

    /**
     * 9、将计算后的合同占用关系 更新至  正式表
     */
    public int updateLmtSubUseCalculateToFormalTable2(String cusId){
        logger.info("9、将计算后的合同占用关系 更新至  正式表 -------- ");
        lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseCalculateToFormalTable2_1(cusId);
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseCalculateToFormalTable2_2(cusId);
    }

    /**
     * 10、将计算后的台账占用关系 更新至  正式表（台账占用额度）
     */
    public int updateLmtSubUseTzzyToFormal(String cusId){
        logger.info("10、将计算后的台账占用关系 更新至  正式表（台账占用额度） -------- ");
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseTzzyToFormal(cusId);
    }

    /**
     * 11、根据占用关系更新分项 总额已用、敞口已用
     */
    public int updateLmtSubUseZeyyAndCkyy(String cusId){
        logger.info("11、根据占用关系更新分项 总额已用、敞口已用 -------- ");
        lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseZeyyAndCkyy1(cusId);
        lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseZeyyAndCkyy2(cusId);
        lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseZeyyAndCkyy3(cusId);
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUseZeyyAndCkyy4(cusId);
    }

    /**
     * 12、二级分项占额需要汇总至一级分项
     */
    public int updateLmtSubUse2LevelZeTo1Level(String cusId){
        logger.info("12、二级分项占额需要汇总至一级分项 --------");
        lmtCheckUpdateLmtSubUseMapper.updateLmtSubUse2LevelZeTo1Level1(cusId);
        lmtCheckUpdateLmtSubUseMapper.updateLmtSubUse2LevelZeTo1Level2(cusId);
        lmtCheckUpdateLmtSubUseMapper.updateLmtSubUse2LevelZeTo1Level3(cusId);
        return lmtCheckUpdateLmtSubUseMapper.updateLmtSubUse2LevelZeTo1Level4(cusId);
    }


}
