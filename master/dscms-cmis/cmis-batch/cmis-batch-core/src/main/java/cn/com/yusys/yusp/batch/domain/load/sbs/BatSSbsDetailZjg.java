/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.sbs;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSSbsDetailZjg
 * @类描述: bat_s_sbs_detail_zjg数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 20:10:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_sbs_detail_zjg")
public class BatSSbsDetailZjg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 记录ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 授信主体ECIF_ID **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 授信分项额度ID **/
	@Column(name = "APPR_SUB_SERNO", unique = false, nullable = true, length = 30)
	private String apprSubSerno;
	
	/** 资产编号(如有) **/
	@Column(name = "ASSET_NO", unique = false, nullable = true, length = 50)
	private String assetNo;
	
	/** 投资资产名称 **/
	@Column(name = "ASSET_NAME", unique = false, nullable = true, length = 100)
	private String assetName;
	
	/** 原交易编号 **/
	@Column(name = "ORIGI_PK_ID", unique = false, nullable = true, length = 50)
	private String origiPkId;
	
	/** 占用/恢复类型 STD_RZ_TRADE_TYPE **/
	@Column(name = "TRADE_TYPE", unique = false, nullable = true, length = 3)
	private String tradeType;
	
	/** 占用金额 **/
	@Column(name = "TRADE_AMT", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal tradeAmt;
	
	/** 交易品种编号 **/
	@Column(name = "PRD_NO", unique = false, nullable = true, length = 50)
	private String prdNo;
	
	/** 交易品种名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 100)
	private String prdName;
	
	/** 交易成交日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;
	
	/** 交易到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 日终日 **/
	@Column(name = "BAT_DATE", unique = false, nullable = true, length = 20)
	private String batDate;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param apprSubSerno
	 */
	public void setApprSubSerno(String apprSubSerno) {
		this.apprSubSerno = apprSubSerno;
	}
	
    /**
     * @return apprSubSerno
     */
	public String getApprSubSerno() {
		return this.apprSubSerno;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	
    /**
     * @return assetNo
     */
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param assetName
	 */
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	
    /**
     * @return assetName
     */
	public String getAssetName() {
		return this.assetName;
	}
	
	/**
	 * @param origiPkId
	 */
	public void setOrigiPkId(String origiPkId) {
		this.origiPkId = origiPkId;
	}
	
    /**
     * @return origiPkId
     */
	public String getOrigiPkId() {
		return this.origiPkId;
	}
	
	/**
	 * @param tradeType
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	
    /**
     * @return tradeType
     */
	public String getTradeType() {
		return this.tradeType;
	}
	
	/**
	 * @param tradeAmt
	 */
	public void setTradeAmt(java.math.BigDecimal tradeAmt) {
		this.tradeAmt = tradeAmt;
	}
	
    /**
     * @return tradeAmt
     */
	public java.math.BigDecimal getTradeAmt() {
		return this.tradeAmt;
	}
	
	/**
	 * @param prdNo
	 */
	public void setPrdNo(String prdNo) {
		this.prdNo = prdNo;
	}
	
    /**
     * @return prdNo
     */
	public String getPrdNo() {
		return this.prdNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param batDate
	 */
	public void setBatDate(String batDate) {
		this.batDate = batDate;
	}
	
    /**
     * @return batDate
     */
	public String getBatDate() {
		return this.batDate;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}