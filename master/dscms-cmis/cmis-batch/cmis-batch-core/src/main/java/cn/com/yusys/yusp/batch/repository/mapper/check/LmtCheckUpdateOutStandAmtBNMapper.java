/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.check;

import org.apache.ibatis.annotations.Param;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateOutStandAmtBNMapper
 * @类描述: #Dao类
 * @功能描述: 数据更新额度校正-合同临时表-表内占用总余额与敞口余额
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCheckUpdateOutStandAmtBNMapper {


    /**
     * 1、未到期的最高额贷款合同（及在途）占用额度  占用总余额  = 合同金额 ; 占用敞口 = 非低风险为合同金额、低风险为0
     */
    int insertUpdateOutStandAmtBNZgedkht1(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNZgedkht2(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNZgedkht3(@Param("cusId") String cusId);


    /**
     * 2、未到期的最高额委托贷款合同（及在途）占用额度：lmt_cont_rel表中 占用总余额 = 合同金额; 占用敞口 = 非低风险为合同金额、低风险为0
     */
    int insertUpdateOutStandAmtBNZgewtdkht1(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNZgewtdkht2(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNZgewtdkht3(@Param("cusId") String cusId);

    /**
     * 3、未到期的一般贷款合同（及在途）占用额度：lmt_cont_rel表中  占用总余额 = 合同金额 - 已发放贷款金额 + 已发放贷款余额; 占用敞口 = 非低风险为占用总余额、低风险为0
     */
    int insertUpdateOutStandAmtBNYbdkht1(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNYbdkht2(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNYbdkht3(@Param("cusId") String cusId);

    /**
     * 4、未到期的一般委托贷款合同（及在途）占用额度：lmt_cont_rel表中 占用总余额 = 合同金额 - 已发放贷款金额 + 已发放贷款余额；占用敞口 = 非低风险为占用总余额、低风险为0
     */
    int insertUpdateOutStandAmtBNYbwtdkht1(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNYbwtdkht2(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNYbwtdkht3(@Param("cusId") String cusId);


    /**
     * 5、未到期的最高额授信协议（及在途）占用额度：lmt_cont_rel表中  占用总余额 = 协议最高可用信金额；占用敞口 = 非低风险为协议最高可用信金额、低风险为0
     */
    int insertUpdateOutStandAmtBNZgsxxy1(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNZgsxxy2(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNZgsxxy3(@Param("cusId") String cusId);

    /**
     * 6、未到期的最高额资产池协议（及在途）占用额度
     */
    int insertUpdateOutStandAmtBNZgezccxy1(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNZgezccxy2(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNZgezccxy3(@Param("cusId") String cusId);

    /**
     * 已到期 ：用信申请下的台账余额
     * 包含：最高额授信协议、贷款合同、银票合同、保函协议、信用证合同、银票合同、资产池合同、贴现协议
     */
    int insertUpdateOutStandAmtBNBelongYx1(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNBelongYx2(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNBelongYx3(@Param("cusId") String cusId);

    int insertUpdateOutStandAmtBNBelongYx4(@Param("cusId") String cusId);

}