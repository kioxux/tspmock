package cn.com.yusys.yusp.batch.job.mid;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：MID00001</br>
 * 任务名称：中间任务-文件处理任务结束后暂停</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年10月26日 上午00:56:56
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Mid00001Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Mid00001Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job mid00001Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.MID00001_JOB.key, JobStepEnum.MID00001_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job mid00001Job = this.jobBuilderFactory.get(JobStepEnum.MID00001_JOB.key)
                .start(mid00001UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(mid00001CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .listener(new BatchJobListener())
                .build();
        return mid00001Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step mid00001UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.MID00001_UPDATE_TASK010_STEP.key, JobStepEnum.MID00001_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step mid00001UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.MID00001_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.MID00001_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.MID00001_UPDATE_TASK010_STEP.key, JobStepEnum.MID00001_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return mid00001UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step mid00001CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.MID00001_CHECK_REL_STEP.key, JobStepEnum.MID00001_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step mid00001CheckRelStep = this.stepBuilderFactory.get(JobStepEnum.MID00001_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.MID00001_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.MID00001_CHECK_REL_STEP.key, JobStepEnum.MID00001_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.MID00001_CHECK_REL_STEP.key, JobStepEnum.MID00001_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return mid00001CheckRelStep;
    }

}
