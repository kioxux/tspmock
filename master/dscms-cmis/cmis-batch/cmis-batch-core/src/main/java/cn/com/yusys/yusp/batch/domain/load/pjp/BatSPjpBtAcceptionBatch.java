/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtAcceptionBatch
 * @类描述: bat_s_pjp_bt_acception_batch数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-03 10:58:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_pjp_bt_acception_batch")
public class BatSPjpBtAcceptionBatch extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ACCEPTION_BATCH_ID")
	private String acceptionBatchId;
	
	/** 批次号 **/
	@Column(name = "S_BATCH_NO", unique = false, nullable = false, length = 40)
	private String sBatchNo;
	
	/** 票据种类 **/
	@Column(name = "S_BILL_TYPE", unique = false, nullable = true, length = 10)
	private String sBillType;
	
	/** 票据介质 **/
	@Column(name = "S_BILL_MEDIA", unique = false, nullable = true, length = 10)
	private String sBillMedia;
	
	/** 操作员id **/
	@Column(name = "S_OPERATOR_ID", unique = false, nullable = true, length = 40)
	private String sOperatorId;
	
	/** 创建时间 **/
	@Column(name = "CREATEDATE", unique = false, nullable = true, length = 20)
	private String createdate;
	
	/** 总金额 **/
	@Column(name = "TOTLEAMOUNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal totleamount;
	
	/** 总笔数 **/
	@Column(name = "TOTLEBILL", unique = false, nullable = true, length = 10)
	private Integer totlebill;
	
	/** 申请人开户行行号 **/
	@Column(name = "S_CUST_BANKNO", unique = false, nullable = true, length = 40)
	private String sCustBankno;
	
	/** 申请人开户行行名 **/
	@Column(name = "S_CUST_BANKNAME", unique = false, nullable = true, length = 150)
	private String sCustBankname;
	
	/** 申请人名称 **/
	@Column(name = "S_CUST_NAME", unique = false, nullable = true, length = 150)
	private String sCustName;
	
	/** 申请人账号 **/
	@Column(name = "S_CUST_ACCT", unique = false, nullable = true, length = 40)
	private String sCustAcct;
	
	/** 申请人组织机构代码号(客户号) **/
	@Column(name = "S_CUST_ORGCODE", unique = false, nullable = true, length = 40)
	private String sCustOrgcode;
	
	/** 机构ID **/
	@Column(name = "S_BRANCH_ID", unique = false, nullable = true, length = 300)
	private String sBranchId;
	
	/** 保证金比例，单位：% **/
	@Column(name = "S_BAILRATE", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal sBailrate;
	
	/** 承兑申请日期 **/
	@Column(name = "S_ACCPTRDT", unique = false, nullable = true, length = 20)
	private String sAccptrdt;
	
	/** 承兑协议号 **/
	@Column(name = "PROTOCOL_NO", unique = false, nullable = true, length = 100)
	private String protocolNo;
	
	/** 保证金类别 **/
	@Column(name = "S_CASHDEPOSITTYPE", unique = false, nullable = true, length = 10)
	private String sCashdeposittype;
	
	/** 保证金金额 **/
	@Column(name = "S_CASHDEPOSITISSEAMT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal sCashdepositisseamt;
	
	/** 保证金账号 **/
	@Column(name = "S_CASHDEPOSITACCTID", unique = false, nullable = true, length = 40)
	private String sCashdepositacctid;
	
	/** 保证金账户名称 **/
	@Column(name = "S_CASHDEPOSITACCTNAME", unique = false, nullable = true, length = 200)
	private String sCashdepositacctname;
	
	/** 保证金开户行行号 **/
	@Column(name = "S_CASHDEPOSITACCTSVCR", unique = false, nullable = true, length = 20)
	private String sCashdepositacctsvcr;
	
	/** 手续费 **/
	@Column(name = "F_CHARGE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fCharge;
	
	/** 手续费比例 **/
	@Column(name = "S_PDGRATE", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal sPdgrate;
	
	/** 担保方式 **/
	@Column(name = "GUARANTEE_TYPE", unique = false, nullable = true, length = 10)
	private String guaranteeType;
	
	/** 还款账号 **/
	@Column(name = "RETURN_ACCT_NO", unique = false, nullable = true, length = 40)
	private String returnAcctNo;
	
	/** 还款账号开户行行号 **/
	@Column(name = "RETURN_ACCT_BANK_NO", unique = false, nullable = true, length = 20)
	private String returnAcctBankNo;
	
	/** 还款账号户名 **/
	@Column(name = "RETURN_ACCT_NAME", unique = false, nullable = true, length = 300)
	private String returnAcctName;
	
	/** 产品类型 **/
	@Column(name = "PRODUCT_ID", unique = false, nullable = true, length = 20)
	private String productId;
	
	/** 计息方式 **/
	@Column(name = "INTEREST_MODE", unique = false, nullable = true, length = 10)
	private String interestMode;
	
	/** 敞口标志 STD_RZ_OPEN_MARK **/
	@Column(name = "OPEN_MARK", unique = false, nullable = true, length = 10)
	private String openMark;
	
	/** 核心返回的记账日期 **/
	@Column(name = "ACCT_DATE_TIME", unique = false, nullable = true, length = 20)
	private String acctDateTime;
	
	/** 核心返回的记账流水号 **/
	@Column(name = "ACCT_FLOW_NO", unique = false, nullable = true, length = 100)
	private String acctFlowNo;
	
	/** 协议金额 **/
	@Column(name = "PROTOCOL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal protocolAmt;
	
	/** 客户经理编号 **/
	@Column(name = "S_CUST_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String sCustManagerId;
	
	/** 客户经理名称 **/
	@Column(name = "S_CUST_MANAGER_NAME", unique = false, nullable = true, length = 150)
	private String sCustManagerName;
	
	/** 机构名 **/
	@Column(name = "S_ORG_NAME", unique = false, nullable = true, length = 150)
	private String sOrgName;
	
	/** 数据移植标记  STD_RZ_YZ_FLAG **/
	@Column(name = "YZ_FLAG", unique = false, nullable = true, length = 2)
	private String yzFlag;
	
	/** 合同起始日 **/
	@Column(name = "S_SIGNDATE", unique = false, nullable = true, length = 20)
	private String sSigndate;
	
	/** 合同到期日 **/
	@Column(name = "S_DUEDATE", unique = false, nullable = true, length = 20)
	private String sDuedate;
	
	/** 记账标记 **/
	@Column(name = "ACCT_FLAG", unique = false, nullable = true, length = 5)
	private String acctFlag;
	
	/** 出票人客户号 **/
	@Column(name = "CUST_NO", unique = false, nullable = true, length = 50)
	private String custNo;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param acceptionBatchId
	 */
	public void setAcceptionBatchId(String acceptionBatchId) {
		this.acceptionBatchId = acceptionBatchId;
	}
	
    /**
     * @return acceptionBatchId
     */
	public String getAcceptionBatchId() {
		return this.acceptionBatchId;
	}
	
	/**
	 * @param sBatchNo
	 */
	public void setSBatchNo(String sBatchNo) {
		this.sBatchNo = sBatchNo;
	}
	
    /**
     * @return sBatchNo
     */
	public String getSBatchNo() {
		return this.sBatchNo;
	}
	
	/**
	 * @param sBillType
	 */
	public void setSBillType(String sBillType) {
		this.sBillType = sBillType;
	}
	
    /**
     * @return sBillType
     */
	public String getSBillType() {
		return this.sBillType;
	}
	
	/**
	 * @param sBillMedia
	 */
	public void setSBillMedia(String sBillMedia) {
		this.sBillMedia = sBillMedia;
	}
	
    /**
     * @return sBillMedia
     */
	public String getSBillMedia() {
		return this.sBillMedia;
	}
	
	/**
	 * @param sOperatorId
	 */
	public void setSOperatorId(String sOperatorId) {
		this.sOperatorId = sOperatorId;
	}
	
    /**
     * @return sOperatorId
     */
	public String getSOperatorId() {
		return this.sOperatorId;
	}
	
	/**
	 * @param createdate
	 */
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	
    /**
     * @return createdate
     */
	public String getCreatedate() {
		return this.createdate;
	}
	
	/**
	 * @param totleamount
	 */
	public void setTotleamount(java.math.BigDecimal totleamount) {
		this.totleamount = totleamount;
	}
	
    /**
     * @return totleamount
     */
	public java.math.BigDecimal getTotleamount() {
		return this.totleamount;
	}
	
	/**
	 * @param totlebill
	 */
	public void setTotlebill(Integer totlebill) {
		this.totlebill = totlebill;
	}
	
    /**
     * @return totlebill
     */
	public Integer getTotlebill() {
		return this.totlebill;
	}
	
	/**
	 * @param sCustBankno
	 */
	public void setSCustBankno(String sCustBankno) {
		this.sCustBankno = sCustBankno;
	}
	
    /**
     * @return sCustBankno
     */
	public String getSCustBankno() {
		return this.sCustBankno;
	}
	
	/**
	 * @param sCustBankname
	 */
	public void setSCustBankname(String sCustBankname) {
		this.sCustBankname = sCustBankname;
	}
	
    /**
     * @return sCustBankname
     */
	public String getSCustBankname() {
		return this.sCustBankname;
	}
	
	/**
	 * @param sCustName
	 */
	public void setSCustName(String sCustName) {
		this.sCustName = sCustName;
	}
	
    /**
     * @return sCustName
     */
	public String getSCustName() {
		return this.sCustName;
	}
	
	/**
	 * @param sCustAcct
	 */
	public void setSCustAcct(String sCustAcct) {
		this.sCustAcct = sCustAcct;
	}
	
    /**
     * @return sCustAcct
     */
	public String getSCustAcct() {
		return this.sCustAcct;
	}
	
	/**
	 * @param sCustOrgcode
	 */
	public void setSCustOrgcode(String sCustOrgcode) {
		this.sCustOrgcode = sCustOrgcode;
	}
	
    /**
     * @return sCustOrgcode
     */
	public String getSCustOrgcode() {
		return this.sCustOrgcode;
	}
	
	/**
	 * @param sBranchId
	 */
	public void setSBranchId(String sBranchId) {
		this.sBranchId = sBranchId;
	}
	
    /**
     * @return sBranchId
     */
	public String getSBranchId() {
		return this.sBranchId;
	}
	
	/**
	 * @param sBailrate
	 */
	public void setSBailrate(java.math.BigDecimal sBailrate) {
		this.sBailrate = sBailrate;
	}
	
    /**
     * @return sBailrate
     */
	public java.math.BigDecimal getSBailrate() {
		return this.sBailrate;
	}
	
	/**
	 * @param sAccptrdt
	 */
	public void setSAccptrdt(String sAccptrdt) {
		this.sAccptrdt = sAccptrdt;
	}
	
    /**
     * @return sAccptrdt
     */
	public String getSAccptrdt() {
		return this.sAccptrdt;
	}
	
	/**
	 * @param protocolNo
	 */
	public void setProtocolNo(String protocolNo) {
		this.protocolNo = protocolNo;
	}
	
    /**
     * @return protocolNo
     */
	public String getProtocolNo() {
		return this.protocolNo;
	}
	
	/**
	 * @param sCashdeposittype
	 */
	public void setSCashdeposittype(String sCashdeposittype) {
		this.sCashdeposittype = sCashdeposittype;
	}
	
    /**
     * @return sCashdeposittype
     */
	public String getSCashdeposittype() {
		return this.sCashdeposittype;
	}
	
	/**
	 * @param sCashdepositisseamt
	 */
	public void setSCashdepositisseamt(java.math.BigDecimal sCashdepositisseamt) {
		this.sCashdepositisseamt = sCashdepositisseamt;
	}
	
    /**
     * @return sCashdepositisseamt
     */
	public java.math.BigDecimal getSCashdepositisseamt() {
		return this.sCashdepositisseamt;
	}
	
	/**
	 * @param sCashdepositacctid
	 */
	public void setSCashdepositacctid(String sCashdepositacctid) {
		this.sCashdepositacctid = sCashdepositacctid;
	}
	
    /**
     * @return sCashdepositacctid
     */
	public String getSCashdepositacctid() {
		return this.sCashdepositacctid;
	}
	
	/**
	 * @param sCashdepositacctname
	 */
	public void setSCashdepositacctname(String sCashdepositacctname) {
		this.sCashdepositacctname = sCashdepositacctname;
	}
	
    /**
     * @return sCashdepositacctname
     */
	public String getSCashdepositacctname() {
		return this.sCashdepositacctname;
	}
	
	/**
	 * @param sCashdepositacctsvcr
	 */
	public void setSCashdepositacctsvcr(String sCashdepositacctsvcr) {
		this.sCashdepositacctsvcr = sCashdepositacctsvcr;
	}
	
    /**
     * @return sCashdepositacctsvcr
     */
	public String getSCashdepositacctsvcr() {
		return this.sCashdepositacctsvcr;
	}
	
	/**
	 * @param fCharge
	 */
	public void setFCharge(java.math.BigDecimal fCharge) {
		this.fCharge = fCharge;
	}
	
    /**
     * @return fCharge
     */
	public java.math.BigDecimal getFCharge() {
		return this.fCharge;
	}
	
	/**
	 * @param sPdgrate
	 */
	public void setSPdgrate(java.math.BigDecimal sPdgrate) {
		this.sPdgrate = sPdgrate;
	}
	
    /**
     * @return sPdgrate
     */
	public java.math.BigDecimal getSPdgrate() {
		return this.sPdgrate;
	}
	
	/**
	 * @param guaranteeType
	 */
	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}
	
    /**
     * @return guaranteeType
     */
	public String getGuaranteeType() {
		return this.guaranteeType;
	}
	
	/**
	 * @param returnAcctNo
	 */
	public void setReturnAcctNo(String returnAcctNo) {
		this.returnAcctNo = returnAcctNo;
	}
	
    /**
     * @return returnAcctNo
     */
	public String getReturnAcctNo() {
		return this.returnAcctNo;
	}
	
	/**
	 * @param returnAcctBankNo
	 */
	public void setReturnAcctBankNo(String returnAcctBankNo) {
		this.returnAcctBankNo = returnAcctBankNo;
	}
	
    /**
     * @return returnAcctBankNo
     */
	public String getReturnAcctBankNo() {
		return this.returnAcctBankNo;
	}
	
	/**
	 * @param returnAcctName
	 */
	public void setReturnAcctName(String returnAcctName) {
		this.returnAcctName = returnAcctName;
	}
	
    /**
     * @return returnAcctName
     */
	public String getReturnAcctName() {
		return this.returnAcctName;
	}
	
	/**
	 * @param productId
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
    /**
     * @return productId
     */
	public String getProductId() {
		return this.productId;
	}
	
	/**
	 * @param interestMode
	 */
	public void setInterestMode(String interestMode) {
		this.interestMode = interestMode;
	}
	
    /**
     * @return interestMode
     */
	public String getInterestMode() {
		return this.interestMode;
	}
	
	/**
	 * @param openMark
	 */
	public void setOpenMark(String openMark) {
		this.openMark = openMark;
	}
	
    /**
     * @return openMark
     */
	public String getOpenMark() {
		return this.openMark;
	}
	
	/**
	 * @param acctDateTime
	 */
	public void setAcctDateTime(String acctDateTime) {
		this.acctDateTime = acctDateTime;
	}
	
    /**
     * @return acctDateTime
     */
	public String getAcctDateTime() {
		return this.acctDateTime;
	}
	
	/**
	 * @param acctFlowNo
	 */
	public void setAcctFlowNo(String acctFlowNo) {
		this.acctFlowNo = acctFlowNo;
	}
	
    /**
     * @return acctFlowNo
     */
	public String getAcctFlowNo() {
		return this.acctFlowNo;
	}
	
	/**
	 * @param protocolAmt
	 */
	public void setProtocolAmt(java.math.BigDecimal protocolAmt) {
		this.protocolAmt = protocolAmt;
	}
	
    /**
     * @return protocolAmt
     */
	public java.math.BigDecimal getProtocolAmt() {
		return this.protocolAmt;
	}
	
	/**
	 * @param sCustManagerId
	 */
	public void setSCustManagerId(String sCustManagerId) {
		this.sCustManagerId = sCustManagerId;
	}
	
    /**
     * @return sCustManagerId
     */
	public String getSCustManagerId() {
		return this.sCustManagerId;
	}
	
	/**
	 * @param sCustManagerName
	 */
	public void setSCustManagerName(String sCustManagerName) {
		this.sCustManagerName = sCustManagerName;
	}
	
    /**
     * @return sCustManagerName
     */
	public String getSCustManagerName() {
		return this.sCustManagerName;
	}
	
	/**
	 * @param sOrgName
	 */
	public void setSOrgName(String sOrgName) {
		this.sOrgName = sOrgName;
	}
	
    /**
     * @return sOrgName
     */
	public String getSOrgName() {
		return this.sOrgName;
	}
	
	/**
	 * @param yzFlag
	 */
	public void setYzFlag(String yzFlag) {
		this.yzFlag = yzFlag;
	}
	
    /**
     * @return yzFlag
     */
	public String getYzFlag() {
		return this.yzFlag;
	}
	
	/**
	 * @param sSigndate
	 */
	public void setSSigndate(String sSigndate) {
		this.sSigndate = sSigndate;
	}
	
    /**
     * @return sSigndate
     */
	public String getSSigndate() {
		return this.sSigndate;
	}
	
	/**
	 * @param sDuedate
	 */
	public void setSDuedate(String sDuedate) {
		this.sDuedate = sDuedate;
	}
	
    /**
     * @return sDuedate
     */
	public String getSDuedate() {
		return this.sDuedate;
	}
	
	/**
	 * @param acctFlag
	 */
	public void setAcctFlag(String acctFlag) {
		this.acctFlag = acctFlag;
	}
	
    /**
     * @return acctFlag
     */
	public String getAcctFlag() {
		return this.acctFlag;
	}
	
	/**
	 * @param custNo
	 */
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	
    /**
     * @return custNo
     */
	public String getCustNo() {
		return this.custNo;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}