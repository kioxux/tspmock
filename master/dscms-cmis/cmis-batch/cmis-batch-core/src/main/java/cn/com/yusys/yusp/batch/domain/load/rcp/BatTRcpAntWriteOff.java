/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpAntWriteOff
 * @类描述: bat_t_rcp_ant_write_off数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:04:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_rcp_ant_write_off")
public class BatTRcpAntWriteOff extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 贷款借据号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "contract_no")
	private String contractNo;
	
	/** 核销标识，已核销为Y，否则为N **/
	@Column(name = "write_off", unique = false, nullable = false, length = 2)
	private String writeOff;
	
	/** 录入日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "input_time", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param writeOff
	 */
	public void setWriteOff(String writeOff) {
		this.writeOff = writeOff;
	}
	
    /**
     * @return writeOff
     */
	public String getWriteOff() {
		return this.writeOff;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}


}