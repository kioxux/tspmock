/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.load.fpt;

import cn.com.yusys.yusp.batch.domain.load.fpt.BatSFptRCustBusiSum;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSFptRCustBusiSumMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:38:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface BatSFptRCustBusiSumMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    BatSFptRCustBusiSum selectByPrimaryKey(@Param("custId") String custId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<BatSFptRCustBusiSum> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(BatSFptRCustBusiSum record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(BatSFptRCustBusiSum record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(BatSFptRCustBusiSum record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(BatSFptRCustBusiSum record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("custId") String custId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 查询待删除S表[bat_s_fpt_r_cust_busi_sum]当天数据总次数
     *
     * @param openDay
     * @return
     */
    int deleteByOpenDayCounts(@Param("openDay") String openDay);

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    int deleteByOpenDay(@Param("openDay") String openDay);

    /**
     * 更新S表中 数据日期 DATA_DATE 为营业日期
     *
     * @param openDay
     * @return
     */
    int updateDataDateByOpenDay(@Param("openDay") String openDay);
}