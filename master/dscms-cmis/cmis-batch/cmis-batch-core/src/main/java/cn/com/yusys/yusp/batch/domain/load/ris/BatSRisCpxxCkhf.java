/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.ris;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRisCpxxCkhf
 * @类描述: bat_s_ris_cpxx_ckhf数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 16:27:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ris_cpxx_ckhf")
public class BatSRisCpxxCkhf extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 数据日期 **/
	@Id
	@Column(name = "DATA_DT")
	private String dataDt;
	
	/** 产品编号 **/
	@Id
	@Column(name = "PRODUCT_ID")
	private String productId;
	
	/** 产品名称 **/
	@Column(name = "PROD_NAME", unique = false, nullable = true, length = 100)
	private String prodName;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param productId
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
    /**
     * @return productId
     */
	public String getProductId() {
		return this.productId;
	}
	
	/**
	 * @param prodName
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
    /**
     * @return prodName
     */
	public String getProdName() {
		return this.prodName;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}