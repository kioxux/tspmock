package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0430Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0430</br>
 * 任务名称：加工任务-贷后管理-自动化贷后规则  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月18日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0430Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0430Service.class);

    @Autowired
    private Cmis0430Mapper cmis0430Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * 处理对公规则
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void dealCorp(String openDay) {
        logger.info("处理[ 对公近一年流动资金逾期次数,（>=2 不符合)]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(对公近一年流动资金逾期次数,（>=2 不符合))开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue01 = cmis0430Mapper.insertTmpAutoPspOverdue01(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(对公近一年流动资金逾期次数,（>=2 不符合))结束,响应参数为:[{}]", insertTmpAutoPspOverdue01);

        logger.info("更新对公自动化贷后跑批白名单(对公近一年流动资金逾期次数,（>=2 不符合))开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp01 = cmis0430Mapper.updateAutoPspWhiteInfoCorp01(openDay);
        logger.info("更新对公自动化贷后跑批白名单(对公近一年流动资金逾期次数,（>=2 不符合))结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp01);
        logger.info("处理[ 对公近一年流动资金逾期次数,（>=2 不符合)]结束");


        logger.info("处理[ 对公近一年连续逾期期数，（>=0 不符合)]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(对公近一年连续逾期期数，（>=0 不符合))开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue02 = cmis0430Mapper.insertTmpAutoPspOverdue02(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(对公近一年连续逾期期数，（>=0 不符合))结束,响应参数为:[{}]", insertTmpAutoPspOverdue02);

        logger.info("更新对公自动化贷后跑批白名单(对公近一年连续逾期期数，（>=0 不符合))开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp02 = cmis0430Mapper.updateAutoPspWhiteInfoCorp02(openDay);
        logger.info("更新对公自动化贷后跑批白名单(对公近一年连续逾期期数，（>=0 不符合))结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp02);
        logger.info("处理[ 对公近一年连续逾期期数，（>=0 不符合)]结束");


        logger.info("处理[ cp_loan_overdue  对公未结清贷款逾期期数]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(cp_loan_overdue  对公未结清贷款逾期期数)开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue03 = cmis0430Mapper.insertTmpAutoPspOverdue03(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(cp_loan_overdue  对公未结清贷款逾期期数)结束,响应参数为:[{}]", insertTmpAutoPspOverdue03);

        logger.info("更新对公自动化贷后跑批白名单(cp_loan_overdue  对公未结清贷款逾期期数)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp03 = cmis0430Mapper.updateAutoPspWhiteInfoCorp03(openDay);
        logger.info("更新对公自动化贷后跑批白名单(cp_loan_overdue  对公未结清贷款逾期期数)结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp03);
        logger.info("处理[ cp_loan_overdue  对公未结清贷款逾期期数]结束");

        logger.info("处理[ exist_bad_fiveclass  对公五级分类存在正常以下]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(exist_bad_fiveclass  对公五级分类存在正常以下)开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue04 = cmis0430Mapper.insertTmpAutoPspOverdue04(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(exist_bad_fiveclass  对公五级分类存在正常以下)结束,响应参数为:[{}]", insertTmpAutoPspOverdue04);

        logger.info("更新对公自动化贷后跑批白名单(exist_bad_fiveclass  对公五级分类存在正常以下)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp04 = cmis0430Mapper.updateAutoPspWhiteInfoCorp04(openDay);
        logger.info("更新对公自动化贷后跑批白名单(exist_bad_fiveclass  对公五级分类存在正常以下)结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp04);
        logger.info("处理[ exist_bad_fiveclass  对公五级分类存在正常以下]结束");

        logger.info("处理[ acc_overdue_days  台账逾期天数]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(acc_overdue_days  台账逾期天数)开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue05 = cmis0430Mapper.insertTmpAutoPspOverdue05(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(acc_overdue_days  台账逾期天数)结束,响应参数为:[{}]", insertTmpAutoPspOverdue05);

        logger.info("更新对公自动化贷后跑批白名单(acc_overdue_days  台账逾期天数)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp05 = cmis0430Mapper.updateAutoPspWhiteInfoCorp05(openDay);
        logger.info("更新对公自动化贷后跑批白名单(acc_overdue_days  台账逾期天数)结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp05);
        logger.info("处理[ acc_overdue_days  台账逾期天数]结束");

        logger.info("处理[last_year_overduect 对公近一年流动资金逾期次数,（>=2 不符合)、conti_overdue_month 对公近一年流动资金连续逾期期数，（>=0 不符合)、cp_loan_overdue  对公未结清贷款逾期期数（>=0 不符合) 、exist_bad_fiveclass  对公五级分类存在正常以下 ]开始");
        logger.info("更新对公自动化贷后跑批白名单(台账存在逾期天数)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp0A = cmis0430Mapper.updateAutoPspWhiteInfoCorp0A(openDay);
        logger.info("更新对公自动化贷后跑批白名单(台账存在逾期天数)结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp0A);
        logger.info("处理[last_year_overduect 对公近一年流动资金逾期次数,（>=2 不符合)、conti_overdue_month 对公近一年流动资金连续逾期期数，（>=0 不符合)、cp_loan_overdue  对公未结清贷款逾期期数（>=0 不符合) 、exist_bad_fiveclass  对公五级分类存在正常以下 ]结束");

        logger.info("处理[exist_high_risk_level风险预警等级红 黑]开始");
        logger.info("更新对公自动化贷后跑批白名单(exist_high_risk_level风险预警等级红 黑)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp0B = cmis0430Mapper.updateAutoPspWhiteInfoCorp0B(openDay);
        logger.info("更新对公自动化贷后跑批白名单(exist_high_risk_level风险预警等级红 黑)结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp0B);
        logger.info("处理[exist_high_risk_level风险预警等级红 黑]结束");

        logger.info("处理[exist_black_list  存在黑灰名单]开始");
        logger.info("更新对公自动化贷后跑批白名单(exist_black_list  存在黑灰名单)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp0C = cmis0430Mapper.updateAutoPspWhiteInfoCorp0C(openDay);
        logger.info("更新对公自动化贷后跑批白名单(exist_black_list  存在黑灰名单)结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp0C);
        logger.info("处理[exist_high_risk_level风险预警等级红 黑]结束");


        logger.info("处理[exist_xa_case_info  存在小安涉诉]开始");
        logger.info("更新对公自动化贷后跑批白名单(exist_xa_case_info  存在小安涉诉)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp0D = cmis0430Mapper.updateAutoPspWhiteInfoCorp0D(openDay);
        logger.info("更新对公自动化贷后跑批白名单(exist_xa_case_info  存在小安涉诉)结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp0D);
        logger.info("处理[exist_xa_case_info  存在小安涉诉]结束");

        logger.info("处理[更新失信被执行人标记]开始");
        logger.info("更新对公自动化贷后跑批白名单(更新失信被执行人标记)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp0E = cmis0430Mapper.updateAutoPspWhiteInfoCorp0E(openDay);
        logger.info("更新对公自动化贷后跑批白名单(更新失信被执行人标记)结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp0E);
        logger.info("处理[更新失信被执行人标记]结束");

        logger.info("处理[工商 营业信息  不是 在营（开业） 更新 exist_business_checkfs ]开始");
        logger.info("更新对公自动化贷后跑批白名单(工商 营业信息  不是 在营（开业） 更新 exist_business_checkfs )开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp0F = cmis0430Mapper.updateAutoPspWhiteInfoCorp0F(openDay);
        logger.info("更新对公自动化贷后跑批白名单(工商 营业信息  不是 在营（开业） 更新 exist_business_checkfs )结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp0F);
        logger.info("处理[工商 营业信息  不是 在营（开业） 更新 exist_business_checkfs ]结束");

        logger.info("处理[经营企业半年内发生过法人变更，校验不通过  更新exist_business_checkfs ]开始");
        logger.info("更新对公自动化贷后跑批白名单(经营企业半年内发生过法人变更，校验不通过  更新exist_business_checkfs )开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp0G = cmis0430Mapper.updateAutoPspWhiteInfoCorp0G(openDay);
        logger.info("更新对公自动化贷后跑批白名单(经营企业半年内发生过法人变更，校验不通过  更新exist_business_checkfs )结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp0G);
        logger.info("处理[经营企业半年内发生过法人变更，校验不通过  更新exist_business_checkfs ]结束");



        logger.info("对公自动化贷后跑批白名单存在不符合规则，更新为不通过   开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorpautostatus = cmis0430Mapper.updateAutoPspWhiteInfoCorpautostatus(openDay);
        logger.info(" 对公自动化贷后跑批白名单存在不符合规则，更新为不通过   结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorpautostatus);





        logger.info("处理[更新为通过]开始");
        logger.info("更新对公自动化贷后跑批白名单(更新为通过)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp0End = cmis0430Mapper.updateAutoPspWhiteInfoCorp0End(openDay);
        logger.info("更新对公自动化贷后跑批白名单(更新为通过)结束,响应参数为:[{}]", updateAutoPspWhiteInfoCorp0End);
        logger.info("处理[更新为通过]结束");
    }

    /**
     * 处理个人规则
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void dealPer(String openDay) {
        /***********************************个人规则开始***********************************/
        logger.info("处理[ acc_overdue_days  台账逾期天数]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(acc_overdue_days  台账逾期天数)开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue11 = cmis0430Mapper.insertTmpAutoPspOverdue11(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(acc_overdue_days  台账逾期天数)结束,响应参数为:[{}]", insertTmpAutoPspOverdue11);

        logger.info("更新个人自动化贷后跑批白名单(acc_overdue_days  台账逾期天数)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer11 = cmis0430Mapper.updateAutoPspWhiteInfoPer11(openDay);
        logger.info("更新个人自动化贷后跑批白名单(acc_overdue_days  台账逾期天数)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer11);
        logger.info("处理[ acc_overdue_days  台账逾期天数]结束");

        // 个人征信查询规则
        // 1)近一年内经营性贷款逾期次数不超过2次（含），无连续逾期、欠息记
        // ①经营性贷款近一年历史逾期次数<=2  ② 经营性贷款连续逾期次数<=1
        // 2)近一年内房贷、消费贷逾期不超过3次（含），无连续逾期、欠息记录
        // ①房贷、消费贷近1年历史逾期累计次数<=3  ②房贷、消费贷近一年连续
        // 3)未结清贷款当前无逾期、且五级分类未正常
        // ①未结清贷款（所有类型），当前无逾期 ②所有贷款，五级分类为正常
        logger.info("处理[ 经营性贷款近一年历史逾期次数<=2]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(经营性贷款近一年历史逾期次数<=2)开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue12 = cmis0430Mapper.insertTmpAutoPspOverdue12(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(经营性贷款近一年历史逾期次数<=2)结束,响应参数为:[{}]", insertTmpAutoPspOverdue12);

        logger.info("更新个人自动化贷后跑批白名单(经营性贷款近一年历史逾期次数<=2)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer12 = cmis0430Mapper.updateAutoPspWhiteInfoPer12(openDay);
        logger.info("更新个人自动化贷后跑批白名单(经营性贷款近一年历史逾期次数<=2)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer12);
        logger.info("处理[ 经营性贷款近一年历史逾期次数<=2]结束");

        logger.info("处理[ 经营性贷款连续逾期次数<=1]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(经营性贷款连续逾期次数<=1)开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue13 = cmis0430Mapper.insertTmpAutoPspOverdue13(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(经营性贷款连续逾期次数<=1)结束,响应参数为:[{}]", insertTmpAutoPspOverdue13);

        logger.info("更新个人自动化贷后跑批白名单(经营性贷款连续逾期次数<=1)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer13 = cmis0430Mapper.updateAutoPspWhiteInfoPer13(openDay);
        logger.info("更新个人自动化贷后跑批白名单(经营性贷款连续逾期次数<=1)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer13);
        logger.info("处理[ 经营性贷款连续逾期次数<=1]结束");

        logger.info("处理[近一年内房贷、消费贷逾期不超过3次（含）：房贷、消费贷近1年历史逾期累计次数<=3]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(近一年内房贷、消费贷逾期不超过3次（含）：房贷、消费贷近1年历史逾期累计次数<=3)开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue14 = cmis0430Mapper.insertTmpAutoPspOverdue14(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(近一年内房贷、消费贷逾期不超过3次（含）：房贷、消费贷近1年历史逾期累计次数<=3)结束,响应参数为:[{}]", insertTmpAutoPspOverdue14);

        logger.info("更新个人自动化贷后跑批白名单(近一年内房贷、消费贷逾期不超过3次（含）：房贷、消费贷近1年历史逾期累计次数<=3)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer14 = cmis0430Mapper.updateAutoPspWhiteInfoPer14(openDay);
        logger.info("更新个人自动化贷后跑批白名单(近一年内房贷、消费贷逾期不超过3次（含）：房贷、消费贷近1年历史逾期累计次数<=3)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer14);
        logger.info("处理[近一年内房贷、消费贷逾期不超过3次（含）：房贷、消费贷近1年历史逾期累计次数<=3]结束");

        logger.info("处理[近一年内房贷、消费贷无连续逾期、欠息记录：房贷、消费贷近一年连续逾期期数<=1 ]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(近一年内房贷、消费贷无连续逾期、欠息记录：房贷、消费贷近一年连续逾期期数<=1)开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue15 = cmis0430Mapper.insertTmpAutoPspOverdue15(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(近一年内房贷、消费贷无连续逾期、欠息记录：房贷、消费贷近一年连续逾期期数<=1)结束,响应参数为:[{}]", insertTmpAutoPspOverdue15);

        logger.info("更新个人自动化贷后跑批白名单(近一年内房贷、消费贷无连续逾期、欠息记录：房贷、消费贷近一年连续逾期期数<=1)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer15 = cmis0430Mapper.updateAutoPspWhiteInfoPer15(openDay);
        logger.info("更新个人自动化贷后跑批白名单(近一年内房贷、消费贷无连续逾期、欠息记录：房贷、消费贷近一年连续逾期期数<=1)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer15);
        logger.info("处理[近一年内房贷、消费贷无连续逾期、欠息记录：房贷、消费贷近一年连续逾期期数<=1 ]结束");


        logger.info("处理[未结清贷款（所有类型），当前无逾期]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(未结清贷款（所有类型），当前无逾期)开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue16 = cmis0430Mapper.insertTmpAutoPspOverdue16(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(未结清贷款（所有类型），当前无逾期)结束,响应参数为:[{}]", insertTmpAutoPspOverdue16);

        logger.info("更新个人自动化贷后跑批白名单(未结清贷款（所有类型），当前无逾期)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer16 = cmis0430Mapper.updateAutoPspWhiteInfoPer16(openDay);
        logger.info("更新个人自动化贷后跑批白名单(未结清贷款（所有类型），当前无逾期)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer16);
        logger.info("处理[未结清贷款（所有类型），当前无逾期]结束");

        logger.info("处理[所有贷款，五级分类为正常]开始");
        // 重建相关表
        logger.info("重建自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "TMP_AUTO_PSP_OVERDUE");
        logger.info("重建【Cmis0430】自动化贷后客户临时表[TMP_AUTO_PSP_OVERDUE]结束");

        logger.info("插入自动化贷后客户加工数据临时表中数据(所有贷款，五级分类为正常)开始,请求参数为:[{}]", openDay);
        int insertTmpAutoPspOverdue17 = cmis0430Mapper.insertTmpAutoPspOverdue17(openDay);
        logger.info("插入自动化贷后客户加工数据临时表中数据(所有贷款，五级分类为正常)结束,响应参数为:[{}]", insertTmpAutoPspOverdue17);

        logger.info("更新个人自动化贷后跑批白名单(所有贷款，五级分类为正常)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer17 = cmis0430Mapper.updateAutoPspWhiteInfoPer17(openDay);
        logger.info("更新个人自动化贷后跑批白名单(所有贷款，五级分类为正常)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer17);
        logger.info("处理[所有贷款，五级分类为正常]结束");

        logger.info("处理[AUTO_STATUS通过状态(1 通过 0 不通过)]开始");
        logger.info("更新个人自动化贷后跑批白名单(AUTO_STATUS通过状态(1 通过 0 不通过))开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer1A = cmis0430Mapper.updateAutoPspWhiteInfoPer1A(openDay);
        logger.info("更新个人自动化贷后跑批白名单(AUTO_STATUS通过状态(1 通过 0 不通过))结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer1A);
        logger.info("处理[AUTO_STATUS通过状态(1 通过 0 不通过)]结束");



        logger.info("处理[exist_high_risk_level 存在红黑预警等级 ]开始");
        logger.info("更新个人自动化贷后跑批白名单(exist_high_risk_level 存在红黑预警等级 )开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer1C = cmis0430Mapper.updateAutoPspWhiteInfoPer1C(openDay);
        logger.info("更新个人自动化贷后跑批白名单(exist_high_risk_level 存在红黑预警等级 )结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer1C);
        logger.info("处理[exist_high_risk_level 存在红黑预警等级 ]结束");

        logger.info("处理[exist_black_list  存在黑灰名单]开始");
        logger.info("更新个人自动化贷后跑批白名单(exist_black_list  存在黑灰名单)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer1D = cmis0430Mapper.updateAutoPspWhiteInfoPer1D(openDay);
        logger.info("更新个人自动化贷后跑批白名单(exist_black_list  存在黑灰名单)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer1D);
        logger.info("处理[exist_black_list  存在黑灰名单]结束");

        logger.info("处理[ exist_xa_case_info  存在小安涉诉]开始");
        logger.info("更新个人自动化贷后跑批白名单( exist_xa_case_info  存在小安涉诉)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer1E = cmis0430Mapper.updateAutoPspWhiteInfoPer1E(openDay);
        logger.info("更新个人自动化贷后跑批白名单( exist_xa_case_info  存在小安涉诉)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer1E);
        logger.info("处理[ exist_xa_case_info  存在小安涉诉]结束");

        logger.info("处理[ 发送小安 查询失信被执行人规则]开始");
        logger.info("更新个人自动化贷后跑批白名单(发送小安 查询失信被执行人规则)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer1F = cmis0430Mapper.updateAutoPspWhiteInfoPer1F(openDay);
        logger.info("更新个人自动化贷后跑批白名单(发送小安 查询失信被执行人规则)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer1F);
        logger.info("处理[发送小安 查询失信被执行人规则]结束");


        logger.info(" 更新个人自动化贷后跑批白名单不符合规则的标记为不通过  开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPerautostatus2 = cmis0430Mapper.updateAutoPspWhiteInfoPerautostatus2(openDay);
        logger.info(" 更新个人自动化贷后跑批白名单不符合规则的标记为不通过    结束,响应参数为:[{}]", updateAutoPspWhiteInfoPerautostatus2);



        logger.info("处理[实际控制人通过状态为不通过  则   对公客户的通过状态为不通过  AUTO_TYPE  个人或者实际控制人(1或者0)]开始");
        logger.info("更新个人自动化贷后跑批白名单(实际控制人通过状态为不通过则对公客户的通过状态为不通过  AUTO_TYPE  个人或者实际控制人(1或者0))开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer1B = cmis0430Mapper.updateAutoPspWhiteInfoPer1B(openDay);
        logger.info("更新个人自动化贷后跑批白名单(实际控制人通过状态为不通过则对公客户的通过状态为不通过  AUTO_TYPE  个人或者实际控制人(1或者0))结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer1B);
        logger.info("处理[实际控制人通过状态为不通过则对公客户的通过状态为不通过  AUTO_TYPE  个人或者实际控制人(1或者0)]结束");




        logger.info("处理[更新为通过]开始");
        logger.info("更新个人自动化贷后跑批白名单(更新为通过)开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoPer0End = cmis0430Mapper.updateAutoPspWhiteInfoPer0End(openDay);
        logger.info("更新个人自动化贷后跑批白名单(更新为通过)结束,响应参数为:[{}]", updateAutoPspWhiteInfoPer0End);
        logger.info("处理[更新为通过]结束");
        /***********************************个人规则结束***********************************/

        logger.info("插入自动化检查对公任务生成开始,请求参数为:[{}]", openDay);
        int insertPspTaskList01 = cmis0430Mapper.insertPspTaskList01(openDay);
        logger.info("插入自动化检查对公任务生成结束,响应参数为:[{}]", insertPspTaskList01);
        logger.info("插入自动化检查个人任务生成开始,请求参数为:[{}]", openDay);
        int insertPspTaskList02 = cmis0430Mapper.insertPspTaskList02(openDay);
        logger.info("插入自动化检查个人任务生成结束,响应参数为:[{}]", insertPspTaskList02);

        logger.info("  存在自动化贷后对公名单之内，则更新贷后检查任务为自动通过 开始,请求参数为:[{}]", openDay);
        int updatePspTaskListAutoCorp = cmis0430Mapper.updatePspTaskListAutoCorp(openDay);
        logger.info(" 存在自动化贷后对公名单之内，则更新贷后检查任务为自动通过  结束,响应参数为:[{}]", updatePspTaskListAutoCorp);

        logger.info(" 存在自动化贷后个人名单之内，则更新贷后检查任务为自动通过 开始,请求参数为:[{}]", openDay);
        int updatePspTaskListAutoPer = cmis0430Mapper.updatePspTaskListAutoPer(openDay);
        logger.info(" 存在自动化贷后个人名单之内，则更新贷后检查任务为自动通过 结束,响应参数为:[{}]", updatePspTaskListAutoPer);
    }
}
