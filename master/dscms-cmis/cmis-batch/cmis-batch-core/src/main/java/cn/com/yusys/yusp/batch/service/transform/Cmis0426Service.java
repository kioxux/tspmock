package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0426Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0426</br>
 * 任务名称：加工任务-业务处理-风险任务自动分类  </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0426Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0426Service.class);

    @Autowired
    private Cmis0426Mapper cmis0426Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新 贷款台账表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0426UpdateAccLoan(String openDay) {
        logger.info("更新贷款台账表[逾期达到61天及以上，且当前分类状态是正常、关注，客户名下所有消费贷款自动认定为次级]开始,请求参数为:[{}]", openDay);
        int updateAccLoan01 = cmis0426Mapper.updateAccLoan01(openDay);
        logger.info("更新贷款台账表[逾期达到61天及以上，且当前分类状态是正常、关注，客户名下所有消费贷款自动认定为次级]结束,返回参数为:[{}]", updateAccLoan01);

        logger.info("更新贷款台账表[逾期达到31天～60天，且当前分类状态是正常，客户名下所有消费贷款自动认定为关注]开始,请求参数为:[{}]", openDay);
        int updateAccLoan02 = cmis0426Mapper.updateAccLoan02(openDay);
        logger.info("更新贷款台账表[逾期达到31天～60天，且当前分类状态是正常，客户名下所有消费贷款自动认定为关注]结束,返回参数为:[{}]", updateAccLoan02);

        logger.info("更新贷款台账表[公司客户的低风险业务由系统自动认定为正常 1]开始,请求参数为:[{}]", openDay);
        int updateAccLoan03 = cmis0426Mapper.updateAccLoan03(openDay);
        logger.info("更新贷款台账表[公司客户的低风险业务由系统自动认定为正常 1]结束,返回参数为:[{}]", updateAccLoan03);

        logger.info("更新贷款台账表[个人低风险业务由系统自动认定为正常]开始,请求参数为:[{}]", openDay);
        int updateAccLoan04 = cmis0426Mapper.updateAccLoan04(openDay);
        logger.info("更新贷款台账表[个人低风险业务由系统自动认定为正常]结束,返回参数为:[{}]", updateAccLoan04);


//        logger.info("更新贷款台账表[客户名下存在6个月内有最新有效分类结果，则系统直接按照当前有效分类结果进行认定]开始");
//        logger.info("清空 临时表-贷款台账6个月内有最新有效分类结果信息开始");
//        // cmis0426Mapper.truncateTmpAccLoan180dCus();// cmis_batch.tmp_acc_loan_180d_cus
//        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_acc_loan_180d_cus");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
//        logger.info("清空 临时表-贷款台账6个月内有最新有效分类结果信息结束");
//
//        String lastOpenDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -1); // 上一个营业日期
//        logger.info("插入 临时表-贷款台账6个月内有最新有效分类结果信息开始,请求参数为:[{}]", lastOpenDay);
//        int insertTmpAccLoan180dCus = cmis0426Mapper.insertTmpAccLoan180dCus(lastOpenDay);
//        logger.info("插入 临时表-贷款台账6个月内有最新有效分类结果信息结束,返回参数为:[{}]", insertTmpAccLoan180dCus);
//        logger.info("更新贷款台账表[客户名下存在6个月内有最新有效分类结果，则系统直接按照当前有效分类结果进行认定]开始,请求参数为:[{}]", lastOpenDay);
//        int updateAccLoan05 = cmis0426Mapper.updateAccLoan05(lastOpenDay);
//        logger.info("更新贷款台账表[客户名下存在6个月内有最新有效分类结果，则系统直接按照当前有效分类结果进行认定]结束,返回参数为:[{}]", updateAccLoan05);
//        logger.info("更新贷款台账表[客户名下存在6个月内有最新有效分类结果，则系统直接按照当前有效分类结果进行认定]结束");
    }

    /**
     * 客户名下 没有存量的有效贷款，则默认正常
     *
     * @param openDay
     */
    public void cmis0426UpdatePspTaskList(String openDay) {

        logger.info("客户名下 没有存量的有效对公贷款，则默认正常开始,请求参数为:[{}]", openDay);
        int updateAccLoanTenClass = cmis0426Mapper.updateAccLoanTenClass(openDay);
        logger.info("客户名下 没有存量的有效贷款，则默认正常结束,返回参数为:[{}]", updateAccLoanTenClass);

        logger.info("客户名下 没有存量的有效个人贷款，则默认正常开始,请求参数为:[{}]", openDay);
        int updateAccLoanFiveClass = cmis0426Mapper.updateAccLoanFiveClass(openDay);
        logger.info("客户名下 没有存量的有效贷款，则默认正常结束,返回参数为:[{}]", updateAccLoanFiveClass);
    }
}
