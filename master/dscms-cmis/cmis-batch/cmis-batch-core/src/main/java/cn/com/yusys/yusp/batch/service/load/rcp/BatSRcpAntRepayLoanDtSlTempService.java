/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.rcp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpAntRepayLoanDtSlTemp;
import cn.com.yusys.yusp.batch.repository.mapper.load.rcp.BatSRcpAntRepayLoanDtSlTempMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpAntRepayLoanDtSlTempService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-10 14:48:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSRcpAntRepayLoanDtSlTempService {

    @Autowired
    private BatSRcpAntRepayLoanDtSlTempMapper batSRcpAntRepayLoanDtSlTempMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSRcpAntRepayLoanDtSlTemp selectByPrimaryKey(String contractNo, String seqNo) {
        return batSRcpAntRepayLoanDtSlTempMapper.selectByPrimaryKey(contractNo, seqNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSRcpAntRepayLoanDtSlTemp> selectAll(QueryModel model) {
        List<BatSRcpAntRepayLoanDtSlTemp> records = (List<BatSRcpAntRepayLoanDtSlTemp>) batSRcpAntRepayLoanDtSlTempMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSRcpAntRepayLoanDtSlTemp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSRcpAntRepayLoanDtSlTemp> list = batSRcpAntRepayLoanDtSlTempMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSRcpAntRepayLoanDtSlTemp record) {
        return batSRcpAntRepayLoanDtSlTempMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSRcpAntRepayLoanDtSlTemp record) {
        return batSRcpAntRepayLoanDtSlTempMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSRcpAntRepayLoanDtSlTemp record) {
        return batSRcpAntRepayLoanDtSlTempMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSRcpAntRepayLoanDtSlTemp record) {
        return batSRcpAntRepayLoanDtSlTempMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String contractNo, String seqNo) {
        return batSRcpAntRepayLoanDtSlTempMapper.deleteByPrimaryKey(contractNo, seqNo);
    }
    /**
     * 删除S表当天数据
     * @param openDay
     * @return
     */
    public int deleteByOpenDay(String openDay) {
        return batSRcpAntRepayLoanDtSlTempMapper.deleteByOpenDay(openDay);
    }
}
