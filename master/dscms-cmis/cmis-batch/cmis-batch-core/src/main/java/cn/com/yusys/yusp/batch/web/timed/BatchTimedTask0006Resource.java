package cn.com.yusys.yusp.batch.web.timed;

import cn.com.yusys.yusp.batch.service.timed.BatchTimedTask0006Service;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 定时任务处理类:运行定时任务-贷后管理和批后备份任务相关批量任务
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "BatchTimedTask0006:运行定时任务-贷后管理和批后备份任务相关批量任务")
@RestController
@RequestMapping("/api/batch4use")
public class BatchTimedTask0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BatchTimedTask0006Resource.class);
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private BatchTimedTask0006Service batchTimedTask0006Service;

    @ApiOperation("运行定时任务-贷后管理和批后备份任务相关批量任务")
    @GetMapping("/timedtask0006")
    protected @ResponseBody
    ResultDto<Void> timedtask0006() throws Exception {
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0006.key, BatEnums.BATCH_TIMED_TASK0006.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        int result = 0;// 返回结果，0成功，1失败
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0006.key, BatEnums.BATCH_TIMED_TASK0006.value, "");
        result = batchTimedTask0006Service.timedtask0006();
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0006.key, BatEnums.BATCH_TIMED_TASK0006.value, JSON.toJSONString(result));
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0006.key, BatEnums.BATCH_TIMED_TASK0006.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return new ResultDto<>();
    }

    @ApiOperation("运行定时任务-贷后管理和批后备份任务相关批量任务")
    @PostMapping("/timedtask0006Post")
    protected @ResponseBody
    ResultDto<Void> timedtask0006Post() throws Exception {
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0006.key, BatEnums.BATCH_TIMED_TASK0006.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        int result = 0;// 返回结果，0成功，1失败
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0006.key, BatEnums.BATCH_TIMED_TASK0006.value, "");
        result = batchTimedTask0006Service.timedtask0006();
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0006.key, BatEnums.BATCH_TIMED_TASK0006.value, JSON.toJSONString(result));
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0006.key, BatEnums.BATCH_TIMED_TASK0006.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return new ResultDto<>();
    }

}
