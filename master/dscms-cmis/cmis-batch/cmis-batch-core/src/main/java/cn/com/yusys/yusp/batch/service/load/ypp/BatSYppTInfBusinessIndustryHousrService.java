/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.ypp;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.ypp.BatSYppTInfBusinessIndustryHousr;
import cn.com.yusys.yusp.batch.repository.mapper.load.ypp.BatSYppTInfBusinessIndustryHousrMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSYppTInfBusinessIndustryHousrService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 17:10:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSYppTInfBusinessIndustryHousrService {
    private static final Logger logger = LoggerFactory.getLogger(BatSYppTInfBusinessIndustryHousrService.class);
    @Autowired
    private BatSYppTInfBusinessIndustryHousrMapper batSYppTInfBusinessIndustryHousrMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSYppTInfBusinessIndustryHousr selectByPrimaryKey(String guarNo) {
        return batSYppTInfBusinessIndustryHousrMapper.selectByPrimaryKey(guarNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSYppTInfBusinessIndustryHousr> selectAll(QueryModel model) {
        List<BatSYppTInfBusinessIndustryHousr> records = (List<BatSYppTInfBusinessIndustryHousr>) batSYppTInfBusinessIndustryHousrMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSYppTInfBusinessIndustryHousr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSYppTInfBusinessIndustryHousr> list = batSYppTInfBusinessIndustryHousrMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSYppTInfBusinessIndustryHousr record) {
        return batSYppTInfBusinessIndustryHousrMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSYppTInfBusinessIndustryHousr record) {
        return batSYppTInfBusinessIndustryHousrMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSYppTInfBusinessIndustryHousr record) {
        return batSYppTInfBusinessIndustryHousrMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSYppTInfBusinessIndustryHousr record) {
        return batSYppTInfBusinessIndustryHousrMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String guarNo) {
        return batSYppTInfBusinessIndustryHousrMapper.deleteByPrimaryKey(guarNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSYppTInfBusinessIndustryHousrMapper.deleteByIds(ids);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建商业用房和工业用房表[bat_s_ypp_t_inf_business_industry_housr]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_ypp_t_inf_business_industry_housr");
        logger.info("重建【YPP00005】商业用房和工业用房表[bat_s_ypp_t_inf_business_industry_housr]结束");
        return 0;
    }
}
