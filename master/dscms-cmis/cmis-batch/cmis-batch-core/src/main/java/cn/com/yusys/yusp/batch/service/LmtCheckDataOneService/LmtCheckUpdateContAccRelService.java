/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.LmtCheckDataOneService;

import cn.com.yusys.yusp.batch.repository.mapper.check.LmtCheckUpdateContAccRelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateContAccRelService
 * @类描述: #服务类
 * @功能描述: 数据更新额度校正-台账临时表
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCheckUpdateContAccRelService {

    private static final Logger logger = LoggerFactory.getLogger(LmtCheckUpdateContAccRelService.class);

    @Autowired
    private LmtCheckUpdateContAccRelMapper lmtCheckUpdateContAccRelMapper;

    public void checkOutLmtCheckUpdateContAccRel(String cusId){
        logger.info("2.LmtCheckUpdateContAccRel数据更新额度校正-台账临时表----------start");

        /** 2.1、贷款台账：金额及余额、及状态更新 **/
        checkupdate_dktz(cusId) ;

        /** 2.2、贷款出账申请：金额及余额、及状态更新 **/
        update_dkczsq(cusId) ;

        /** 2.3、委托贷款台账：金额及余额、及状态更新 **/
        update_wtdktz(cusId) ;

        /** 2.4、委托贷款出账申请：金额及余额、及状态更新 **/
        update_wtdkczsq(cusId) ;

        /** 2.5、银票台账：金额及余额、及状态更新 **/
        update_yptz(cusId) ;

        /** 2.6、银票台账出账申请：金额及余额、及状态更新 **/
        update_yptzczsq(cusId) ;

        /** 2.7、保函台账：金额及余额、及状态更新 **/
        update_bhtz(cusId) ;

        /** 2.8、信用证台账：金额及余额、及状态更新 **/
        update_xyztz(cusId) ;

        /** 2.9、贴现台账：金额及余额、及状态更新 **/
        update_txtz(cusId) ;

        /** 2.10、交易业务状态如果为注销、作废，则更新占用余额和占用敞口余额为0 **/
        update_ck_jyzt(cusId) ;

        /** 2.11、若担保方式为低风险担保，则更新占用敞口为0 **/
        update_ck_dbfs(cusId) ;

        logger.info("2.LmtCheckUpdateContAccRel数据更新额度校正-台账临时表----------end");
    }

    /**
     * 2.1、贷款台账：金额及余额、及状态更新
     * @param cusId
     * @return
     */
    public int checkupdate_dktz(String cusId){
        logger.info("2.1、贷款台账：金额及余额、及状态更新----------start");
        //清空中间表数据
        lmtCheckUpdateContAccRelMapper.update_yptz_1(cusId);
        //插入中间表数据
        lmtCheckUpdateContAccRelMapper.insertTmpTdcarAlData(cusId);
        //更新到中间台账表中
        return lmtCheckUpdateContAccRelMapper.update_dktz(cusId);
    }

    /**
     * 2.2、贷款出账申请：金额及余额、及状态更新
     * @param cusId
     * @return
     */
    public int update_dkczsq(String cusId){
        logger.info("2.2、贷款出账申请：金额及余额、及状态更新----------start");
        //清空中间表数据
        lmtCheckUpdateContAccRelMapper.update_yptz_1(cusId);
        lmtCheckUpdateContAccRelMapper.insert_dkczsq(cusId);
        return lmtCheckUpdateContAccRelMapper.update_dkczsq(cusId);
    }

    /**
     * 2.3、委托贷款台账：金额及余额、及状态更新
     * @param cusId
     * @return
     */
    public int update_wtdktz(String cusId){
        logger.info("2.3、委托贷款台账：金额及余额、及状态更新----------start");
        //清空中间表数据
        lmtCheckUpdateContAccRelMapper.update_yptz_1(cusId);
        lmtCheckUpdateContAccRelMapper.insert_wtdktz(cusId);
        return lmtCheckUpdateContAccRelMapper.update_wtdktz(cusId);
    }

    /**
     * 2.4、委托贷款出账申请：金额及余额、及状态更新
     * @param cusId
     * @return
     */
    public int update_wtdkczsq(String cusId){
        logger.info("2.4、委托贷款出账申请：金额及余额、及状态更新----------start");
        //清空中间表数据
        lmtCheckUpdateContAccRelMapper.update_yptz_1(cusId);
        lmtCheckUpdateContAccRelMapper.insert_wtdkczsq(cusId);
        return lmtCheckUpdateContAccRelMapper.update_wtdkczsq(cusId);
    }

    /**
     * 2.5、银票台账：金额及余额、及状态更新
     * @param cusId
     * @return
     */
    public int update_yptz(String cusId){
        logger.info("2.5、银票台账：金额及余额、及状态更新----------start");
        lmtCheckUpdateContAccRelMapper.update_yptz_1(cusId);
        lmtCheckUpdateContAccRelMapper.update_yptz_2(cusId);
        return lmtCheckUpdateContAccRelMapper.update_yptz_3(cusId);
    }

    /**
     * 2.6、银票台账出账申请：金额及余额、及状态更新
     * @param cusId
     * @return
     */
    public int update_yptzczsq(String cusId){
        logger.info("2.6、银票台账出账申请：金额及余额、及状态更新----------start");
        return lmtCheckUpdateContAccRelMapper.update_yptzczsq(cusId);
    }

    /**
     *  2.7、保函台账：金额及余额、及状态更新
     * @param cusId
     * @return
     */
    public int update_bhtz(String cusId){
        logger.info("2.7、保函台账：金额及余额、及状态更新----------start");
        return lmtCheckUpdateContAccRelMapper.update_bhtz(cusId);
    }

    /**
     * 2.8、信用证台账：金额及余额、及状态更新
     * @param cusId
     * @return
     */
    public int update_xyztz(String cusId){
        logger.info("2.8、信用证台账：金额及余额、及状态更新----------start");
        return lmtCheckUpdateContAccRelMapper.update_xyztz(cusId);
    }

    /**
     * 2.9、贴现台账：金额及余额、及状态更新
     * @param cusId
     * @return
     */
    public int update_txtz(String cusId){
        logger.info("2.9、贴现台账：金额及余额、及状态更新----------start");
        return lmtCheckUpdateContAccRelMapper.update_txtz(cusId);
    }

    /**
     * 2.10、交易业务状态如果为注销、作废，则更新占用余额和占用敞口余额为0
     * @param cusId
     * @return
     */
    public int update_ck_jyzt(String cusId){
        logger.info("2.10、交易业务状态如果为注销、作废，则更新占用余额和占用敞口余额为0----------start");
        return lmtCheckUpdateContAccRelMapper.update_ck_jyzt(cusId);
    }

    /**
     * 2.11、若担保方式为低风险担保，则更新占用敞口为0
     * @param cusId
     * @return
     */
    public int update_ck_dbfs(String cusId){
        logger.info("2.11、若担保方式为低风险担保，则更新占用敞口为0----------start");
        return lmtCheckUpdateContAccRelMapper.update_ck_dbfs(cusId);
    }
}
