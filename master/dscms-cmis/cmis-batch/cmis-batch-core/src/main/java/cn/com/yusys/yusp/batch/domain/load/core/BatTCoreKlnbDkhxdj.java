/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTCoreKlnbDkhxdj
 * @类描述: bat_t_core_klnb_dkhxdj数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_core_klnb_dkhxdj")
public class BatTCoreKlnbDkhxdj extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 法人代码
     **/
    @Id
    @Column(name = "farendma")
    private String farendma;

    /**
     * 贷款借据号
     **/
    @Id
    @Column(name = "dkjiejuh")
    private String dkjiejuh;

    /**
     * 核销来源账号
     **/
    @Column(name = "hexiaozh", unique = false, nullable = true, length = 35)
    private String hexiaozh;

    /**
     * 核销来源账号子序号
     **/
    @Column(name = "hexiaozx", unique = false, nullable = true, length = 8)
    private String hexiaozx;

    /**
     * 核销本金
     **/
    @Column(name = "hexiaobj", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal hexiaobj;

    /**
     * 核销利息
     **/
    @Column(name = "hexiaolx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal hexiaolx;

    /**
     * 自动扣款标志 STD_YESORNO
     **/
    @Column(name = "zdkoukbz", unique = false, nullable = true, length = 1)
    private String zdkoukbz;

    /**
     * 备注信息
     **/
    @Column(name = "beizhuuu", unique = false, nullable = true, length = 65535)
    private String beizhuuu;

    /**
     * 交易流水
     **/
    @Id
    @Column(name = "jiaoyils")
    private String jiaoyils;

    /**
     * 交易日期
     **/
    @Id
    @Column(name = "jiaoyirq")
    private String jiaoyirq;

    /**
     * 交易柜员
     **/
    @Column(name = "jiaoyigy", unique = false, nullable = true, length = 8)
    private String jiaoyigy;

    /**
     * 分行标识
     **/
    @Column(name = "fenhbios", unique = false, nullable = false, length = 4)
    private String fenhbios;

    /**
     * 维护柜员
     **/
    @Column(name = "WEIHGUIY", unique = false, nullable = true, length = 8)
    private String weihguiy;

    /**
     * 维护机构
     **/
    @Column(name = "weihjigo", unique = false, nullable = false, length = 12)
    private String weihjigo;

    /**
     * 维护日期
     **/
    @Column(name = "weihriqi", unique = false, nullable = false, length = 8)
    private String weihriqi;

    /**
     * 维护时间
     **/
    @Column(name = "weihshij", unique = false, nullable = true, length = 9)
    private String weihshij;

    /**
     * 时间戳
     **/
    @Column(name = "shijchuo", unique = false, nullable = false, length = 19)
    private Long shijchuo;

    /**
     * 记录状态 STD_JILUZTAI
     **/
    @Column(name = "jiluztai", unique = false, nullable = false, length = 1)
    private String jiluztai;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param hexiaozh
	 */
	public void setHexiaozh(String hexiaozh) {
		this.hexiaozh = hexiaozh;
	}
	
    /**
     * @return hexiaozh
     */
	public String getHexiaozh() {
		return this.hexiaozh;
	}
	
	/**
	 * @param hexiaozx
	 */
	public void setHexiaozx(String hexiaozx) {
		this.hexiaozx = hexiaozx;
	}
	
    /**
     * @return hexiaozx
     */
	public String getHexiaozx() {
		return this.hexiaozx;
	}
	
	/**
	 * @param hexiaobj
	 */
	public void setHexiaobj(java.math.BigDecimal hexiaobj) {
		this.hexiaobj = hexiaobj;
	}
	
    /**
     * @return hexiaobj
     */
	public java.math.BigDecimal getHexiaobj() {
		return this.hexiaobj;
	}
	
	/**
	 * @param hexiaolx
	 */
	public void setHexiaolx(java.math.BigDecimal hexiaolx) {
		this.hexiaolx = hexiaolx;
	}
	
    /**
     * @return hexiaolx
     */
	public java.math.BigDecimal getHexiaolx() {
		return this.hexiaolx;
	}
	
	/**
	 * @param zdkoukbz
	 */
	public void setZdkoukbz(String zdkoukbz) {
		this.zdkoukbz = zdkoukbz;
	}
	
    /**
     * @return zdkoukbz
     */
	public String getZdkoukbz() {
		return this.zdkoukbz;
	}
	
	/**
	 * @param beizhuuu
	 */
	public void setBeizhuuu(String beizhuuu) {
		this.beizhuuu = beizhuuu;
	}
	
    /**
     * @return beizhuuu
     */
	public String getBeizhuuu() {
		return this.beizhuuu;
	}
	
	/**
	 * @param jiaoyils
	 */
	public void setJiaoyils(String jiaoyils) {
		this.jiaoyils = jiaoyils;
	}
	
    /**
     * @return jiaoyils
     */
	public String getJiaoyils() {
		return this.jiaoyils;
	}
	
	/**
	 * @param jiaoyirq
	 */
	public void setJiaoyirq(String jiaoyirq) {
		this.jiaoyirq = jiaoyirq;
	}
	
    /**
     * @return jiaoyirq
     */
	public String getJiaoyirq() {
		return this.jiaoyirq;
	}
	
	/**
	 * @param jiaoyigy
	 */
	public void setJiaoyigy(String jiaoyigy) {
		this.jiaoyigy = jiaoyigy;
	}
	
    /**
     * @return jiaoyigy
     */
	public String getJiaoyigy() {
		return this.jiaoyigy;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}


}