/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.service.job;

import cn.com.yusys.yusp.spring.batch.domain.job.BatchJobExecutionParams;
import cn.com.yusys.yusp.spring.batch.repository.mapper.job.BatchJobExecutionParamsMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobExecutionParamsService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatchJobExecutionParamsService {

    @Autowired
    private BatchJobExecutionParamsMapper batchJobExecutionParamsMapper;
	
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatchJobExecutionParams> selectAll(QueryModel model) {
        List<BatchJobExecutionParams> records = (List<BatchJobExecutionParams>) batchJobExecutionParamsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatchJobExecutionParams> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatchJobExecutionParams> list = batchJobExecutionParamsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	

}
