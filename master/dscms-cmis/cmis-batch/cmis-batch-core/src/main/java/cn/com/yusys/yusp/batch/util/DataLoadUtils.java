package cn.com.yusys.yusp.batch.util;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.domain.bat.BatTransferCfg;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import com.jcraft.jsch.ChannelSftp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Date;
import java.util.Objects;
import java.util.Vector;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * 数据装载工具类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public class DataLoadUtils {
    private static final Logger logger = LoggerFactory.getLogger(DataLoadUtils.class);
    private final static ThreadLocal<FtpUtils> ftpUtil = new ThreadLocal<FtpUtils>();
    private final static ThreadLocal<SFTPUtil> sftpUtil = new ThreadLocal<SFTPUtil>();

    /**
     * 检查转换后的文件中的数据条数和数据库中的数据条数是否相等
     *
     * @param batTaskRun      任务运行管理
     * @param countTableLines 表中总条数
     * @throws Exception
     */
    public boolean checkFileLinesEqualsDb(BatTaskRun batTaskRun, Integer countTableLines) {
        logger.info("***************  检查转换后的文件中的数据条数和数据库中的数据条数是否相等开始    ****************** ");
        // 变量定义
        boolean flag = false; // 返回结果
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String workDir = batTaskRun.getWorkDir();//工作目录 必填，全路径，需要用文件日期替换路径中的日期
        // 工作目录文件，落地表名.txt
        String workFileName = batTTabName.concat(".txt");
        try {
            String workDataFilePath = workDir + workFileName;// 工作目录文件，落地表名.txt
            logger.info("任务日期为:[{}],任务编号为:[{}],工作目录为:[{}],工作目录文件为:[{}],工作目录文件完整路径为:[{}]", taskDate, batTaskRun.getTaskNo(), workDir, workFileName, workDataFilePath);
            Long fileLines = FileUtils.getFilesLines(workDataFilePath);
            logger.info("任务日期为:[{}],任务编号为:[{}],工作目录为:[{}],工作目录文件为:[{}],工作目录文件完整路径为:[{}],行数为:[{}]", taskDate, batTaskRun.getTaskNo(), workDir, workFileName, workDataFilePath, fileLines);
            if (countTableLines.compareTo(fileLines.intValue()) != 0) {
                logger.error("任务日期为:[{}],任务编号为:[{}],表中总条数为:[{}],文件中行数为:[{}],两者不相等，请检查！", taskDate, batTaskRun.getTaskNo(), countTableLines, fileLines);
                throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + taskDate + "],任务编号为:[" + batTaskRun.getTaskNo() + "],表中总条数为:[" + countTableLines + "],文件中行数为:[" + fileLines + "],两者不相等，请检查！");
            } else {
                logger.info("任务日期为:[{}],任务编号为:[{}],表中总条数为:[{}],文件中行数为:[{}],两者相等！", taskDate, batTaskRun.getTaskNo(), countTableLines, fileLines);
                flag = true;
            }

        } catch (BizException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error("检查转换后的文件中的数据条数和数据库中的数据条数是否相等异常," + e.getMessage(), e);
            throw BizException.error(null, EchEnum.ECH080015.key, EchEnum.ECH080015.value, e.getMessage());
        }
        logger.info("***************  检查转换后的文件中的数据条数和数据库中的数据条数是否相等完成    ****************** ");
        return flag;
    }

    /**
     * 检查信号文件是否存在
     *
     * @param batTransferCfg 文件传输配置信息
     * @param batTaskRun     任务运行管理
     * @return
     * @throws Exception
     */
    public boolean checkOdsSign(BatTransferCfg batTransferCfg, BatTaskRun batTaskRun) throws Exception {
        // 变量定义
        boolean flag = false; // 返回结果
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String fileExchangeType = batTaskRun.getFileExchangeType();//文件交互方式 STD_FILE_EXCHANGE_TYPE
        String fileDateType = batTaskRun.getFileDateType();//文件日期类型
        String dataFileName = batTaskRun.getDataFileName();// 数据文件全名
        String signalFileName = batTaskRun.getSignalFileName(); // 信号文件全名

        String remoteDir = batTaskRun.getRemoteDir();//远程目录
        String signFileFolder = batTaskRun.getRemoteSignalDir();//远程目录
        if (Objects.equals(BatEnums.FILE_EXCHANGE_TYPE_01.key, fileExchangeType)) {
            // 获取远程FTP服务端目录  Oracle:/Files/YYYYMMDD/DBTYPE/DBINSTANCE_DBIP/ -> /Files/20210414/Oracle/ORCL_10.28.124.175/ ,MySQL:/Files/YYYYMMDD/DBTYPE/DBIP_DBPORT/ -> /Files/20210421/MySQL/10.28.125.111_15515/
            logger.info("获取远程FTP服务端目录为[{}]", remoteDir);
            // 创建FTP服务器工具类实例
            FtpUtils ftpUtils = new FtpUtils(batTransferCfg);
            // 连接FTP服务器
            ftpUtils.connectServer();
            logger.info("校验远程目录为[{}]中数据文件全名为[{}]是否存在开始", remoteDir, dataFileName);
            boolean dataFileFlag = ftpUtils.checkFTPFile(remoteDir, dataFileName);
            logger.info("校验远程目录为[{}]中数据文件全名为[{}]是否存在结束,返回数据文件标志为[{}]", remoteDir, dataFileName, dataFileFlag);
            logger.info("获取远程FTP服务端信号文件目录为[{}]", signFileFolder);
            // 连接FTP服务器
            ftpUtils.connectServer();
            logger.info("校验远程目录为[{}]中信号文件全名为[{}]是否存在开始", signFileFolder, signalFileName);
            boolean singalFileFlag = ftpUtils.checkFTPFile(signFileFolder, signalFileName);
            logger.info("校验远程目录为[{}]中信号文件全名为[{}]是否存在结束,返回信号文件标志为[{}]", signFileFolder, signalFileName, singalFileFlag);
            if (dataFileFlag && singalFileFlag) {
                flag = true;
            }
        }

        return flag;
    }

    /**
     * 检查贷记卡信号文件是否存在
     *
     * @param batTransferCfg 文件传输配置信息
     * @param batTaskRun     任务运行管理
     * @return
     * @throws Exception
     */
    public boolean checkDjkSign(BatTransferCfg batTransferCfg, BatTaskRun batTaskRun) throws Exception {
        // 变量定义
        boolean flag = false; // 返回结果
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String fileExchangeType = batTaskRun.getFileExchangeType();//文件交互方式 STD_FILE_EXCHANGE_TYPE
        String fileDateType = batTaskRun.getFileDateType();//文件日期类型
        String dataFileName = batTaskRun.getDataFileName();// 数据文件全名
        String signalFileName = batTaskRun.getSignalFileName(); // 信号文件全名

        String remoteDir = batTaskRun.getRemoteDir();//远程目录
        if (Objects.equals(BatEnums.FILE_EXCHANGE_TYPE_21.key, fileExchangeType)) {
            // 获取远程FTP服务端目录  Oracle:/Files/YYYYMMDD/DBTYPE/DBINSTANCE_DBIP/ -> /Files/20210414/Oracle/ORCL_10.28.124.175/ ,MySQL:/Files/YYYYMMDD/DBTYPE/DBIP_DBPORT/ -> /Files/20210421/MySQL/10.28.125.111_15515/
            String signFileFolder = batTaskRun.getRemoteSignalDir();
            logger.info("获取远程FTP服务端目录为[{}]", remoteDir);
            SFTPUtil sftpUtil = new SFTPUtil(batTransferCfg.getFtpUsername(), batTransferCfg.getFtpUserpsw(), batTransferCfg.getFtpIp(), Integer.valueOf(batTransferCfg.getFtpPort()));
            logger.info("校验远程文件目录为[{}]中数据文件全名为[{}]是否存在开始", remoteDir, dataFileName);
            boolean dataFileFlag = sftpUtil.checkSFTPFile(remoteDir, dataFileName, true);
            logger.info("校验远程文件目录为[{}]中数据文件全名为[{}]是否存在结束,返回数据文件标志为[{}]", remoteDir, dataFileName, dataFileFlag);

            sftpUtil = new SFTPUtil(batTransferCfg.getFtpUsername(), batTransferCfg.getFtpUserpsw(), batTransferCfg.getFtpIp(), Integer.valueOf(batTransferCfg.getFtpPort()));
            logger.info("校验远程信号文件目录为[{}]中信号文件全名为[{}]是否存在开始", signFileFolder, signalFileName);
            boolean singalFileFlag = sftpUtil.checkSFTPFile(signFileFolder, signalFileName, true);
            logger.info("校验远程信号文件目录为[{}]中信号文件全名为[{}]是否存在结束,返回信号文件标志为[{}]", signFileFolder, signalFileName, singalFileFlag);
            if (dataFileFlag && singalFileFlag) {
                flag = true;
            }
        }

        return flag;
    }

    /**
     * 下载任务下所有文件至本地处理目录，包括信号文件和数据文件
     *
     * @param batTransferCfg 文件传输配置信息
     * @param batTaskRun     任务运行管理
     * @throws Exception
     */
    public void downloadTaskFiles(BatTransferCfg batTransferCfg, BatTaskRun batTaskRun) throws Exception {
        logger.info("***************           下载任务下所有文件至本地处理目录方法开始               ****************** ");
        // 变量定义
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String fileExchangeType = batTaskRun.getFileExchangeType();//文件交互方式 STD_FILE_EXCHANGE_TYPE
        String fileDateType = batTaskRun.getFileDateType();//文件日期类型
        String dataDateType = batTaskRun.getDataDateType();  // 数据日期类型
        String signType = batTaskRun.getSignalType(); // 信号类型
        String signalFileName = batTaskRun.getSignalFileName(); // 信号文件全名
        String taskType = batTaskRun.getTaskType();//任务类型
        String signalDateType = batTaskRun.getSignalDateType();//信号日期类型
        String dataFileName = batTaskRun.getDataFileName();//数据文件全名
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String workDir = batTaskRun.getWorkDir();//工作目录 必填，全路径，需要用文件日期替换路径中的日期
        String remoteDir = batTaskRun.getRemoteDir();//远程目录
        String localDir = batTaskRun.getLocalDir();//本地目录 必填，全路径，需要用文件日期替换路径中的日期

        // 工作目录文件，落地表名.txt
        String workFileName = batTTabName.concat(".txt");
        try {
            logger.info("获取远程FTP服务端目录为[{}]", remoteDir);
            logger.info("信号类型：[" + signType + "]，文件交互方式：[" + fileExchangeType + "],工作处理目录：[" + workDir + "],本地处理目录：[" + localDir + "],远程目录：[" + remoteDir + "],任务日期：[" + taskDate + "]");

            // 当“任务类型”为“01-文件到表”时，需要下载数据文件
            if (Objects.equals(BatEnums.TASK_TYPE_01.key, taskType)) {
                // 获取数据文件名称
                logger.info("获取数据文件的名称 ：[" + dataFileName + "]");
                if (Objects.equals(BatEnums.FILE_EXCHANGE_TYPE_21.key, batTaskRun.getFileExchangeType())) {
                    this.downloadFileForFileExchangeType21(dataFileName, remoteDir, localDir, batTransferCfg);
                } else if (Objects.equals(BatEnums.FILE_EXCHANGE_TYPE_01.key, batTaskRun.getFileExchangeType())) {
                    this.downloadFileForFileExchangeType01(dataFileName, remoteDir, localDir, batTransferCfg);
                }
                // 将文件拷贝到指定工作目录
                String localDataFilePath = localDir + dataFileName;
                String workDataFilePath = workDir + workFileName;// 工作目录文件，落地表名.txt

                FileUtils.copy(workDir, localDataFilePath, workDataFilePath);
                // 当“信号类型”为“11-文件信号”，不需要下载信号文件
//                if (Objects.equals(BatEnums.SIGNAL_TYPE_21.key, signType)) {
//                    this.downloadFileForFileExchangeType01(signalFileName, remoteDir, localDir, batTransferCfg);
//                    String localSignalFilePath = localDir + signalFileName;
//                    String workSignalFilePath = workDir + signalFileName;// 信号文件名称保持不变
//                    FileUtils.copy(workDir, localSignalFilePath, workSignalFilePath);
//                }
            }
        } catch (BizException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error("下载任务下所有文件至本地处理目录方法," + e.getMessage(), e);
            throw BizException.error(null, EchEnum.ECH080005.key, EchEnum.ECH080005.value, e.getMessage());
        }
        logger.info("***************           下载任务下所有文件至本地处理目录方法完成               ****************** ");
    }

    /**
     * 下载任务下所有文件至本地处理目录，包括信号文件和数据文件</br>
     * 部分系统文件需要先转码后再拷贝到工作目录，目前征信系统、二代支付、风险预警等需要此操作。
     *
     * @param batTransferCfg 文件传输配置信息
     * @param batTaskRun     任务运行管理
     * @throws Exception
     */
    public void downloadTaskFiles4C2D(BatTransferCfg batTransferCfg, BatTaskRun batTaskRun) throws Exception {
        logger.info("***************           下载任务下所有文件至本地处理目录方法开始               ****************** ");
        // 变量定义
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String fileExchangeType = batTaskRun.getFileExchangeType();//文件交互方式 STD_FILE_EXCHANGE_TYPE
        String fileDateType = batTaskRun.getFileDateType();//文件日期类型
        String dataDateType = batTaskRun.getDataDateType();  // 数据日期类型
        String signType = batTaskRun.getSignalType(); // 信号类型
        String signalFileName = batTaskRun.getSignalFileName(); // 信号文件全名
        String taskType = batTaskRun.getTaskType();//任务类型
        String signalDateType = batTaskRun.getSignalDateType();//信号日期类型
        String dataFileName = batTaskRun.getDataFileName();//数据文件全名
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String workDir = batTaskRun.getWorkDir();//工作目录 必填，全路径，需要用文件日期替换路径中的日期
        String remoteDir = batTaskRun.getRemoteDir();//远程目录
        String localDir = batTaskRun.getLocalDir();//本地目录 必填，全路径，需要用文件日期替换路径中的日期

        // 工作目录文件，落地表名.txt
        String workFileName = batTTabName.concat(".txt");
        try {
            logger.info("获取远程FTP服务端目录为[{}]", remoteDir);
            logger.info("信号类型：[" + signType + "]，文件交互方式：[" + fileExchangeType + "],工作处理目录：[" + workDir + "],本地处理目录：[" + localDir + "],远程目录：[" + remoteDir + "],任务日期：[" + taskDate + "]");

            // 当“任务类型”为“01-文件到表”时，需要下载数据文件
            if (Objects.equals(BatEnums.TASK_TYPE_01.key, taskType)) {
                // 获取数据文件名称
                logger.info("获取数据文件的名称 ：[" + dataFileName + "]");
                if (Objects.equals(BatEnums.FILE_EXCHANGE_TYPE_21.key, batTaskRun.getFileExchangeType())) {
                    this.downloadFileForFileExchangeType21(dataFileName, remoteDir, localDir, batTransferCfg);
                } else if (Objects.equals(BatEnums.FILE_EXCHANGE_TYPE_01.key, batTaskRun.getFileExchangeType())) {
                    this.downloadFileForFileExchangeType01(dataFileName, remoteDir, localDir, batTransferCfg);
                }
                // 将文件拷贝到指定工作目录
                String localDataFilePath = localDir + dataFileName;
                String workDataFilePath = workDir + workFileName;// 工作目录文件，落地表名.txt

                // 调用Shell脚本 将文件由GB2312格式转换为UTF-8格式
                DataLoadUtils dataLoadUtils = new DataLoadUtils();
                dataLoadUtils.changeCharacter4Gb2312(batTaskRun);
                // 再将文件拷贝到工作目录
                FileUtils.copy(workDir, localDataFilePath, workDataFilePath);
                // 当“信号类型”为“11-文件信号”，不需要下载信号文件
//                if (Objects.equals(BatEnums.SIGNAL_TYPE_21.key, signType)) {
//                    this.downloadFileForFileExchangeType01(signalFileName, remoteDir, localDir, batTransferCfg);
//                    String localSignalFilePath = localDir + signalFileName;
//                    String workSignalFilePath = workDir + signalFileName;// 信号文件名称保持不变
//                    FileUtils.copy(workDir, localSignalFilePath, workSignalFilePath);
//                }
            }
        } catch (BizException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error("下载任务下所有文件至本地处理目录方法," + e.getMessage(), e);
            throw BizException.error(null, EchEnum.ECH080005.key, EchEnum.ECH080005.value, e.getMessage());
        }
        logger.info("***************           下载任务下所有文件至本地处理目录方法完成               ****************** ");
    }


    /**
     * 下载任务下所有文件至本地处理目录，包括信号文件和数据文件</br>
     * 部分系统文件需要先转码后再拷贝到工作目录，目前票据系统中(PJP0003、PJP00005、PJP00008、PJP00009、GAPS0001、YPP00002、YPP00005、YPP00006)等需要此操作。
     *
     * @param batTransferCfg 文件传输配置信息
     * @param batTaskRun     任务运行管理
     * @throws Exception
     */
    public void downloadTaskFiles4Gb18030(BatTransferCfg batTransferCfg, BatTaskRun batTaskRun) throws Exception {
        logger.info("***************           下载任务下所有文件至本地处理目录方法开始               ****************** ");
        // 变量定义
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String fileExchangeType = batTaskRun.getFileExchangeType();//文件交互方式 STD_FILE_EXCHANGE_TYPE
        String fileDateType = batTaskRun.getFileDateType();//文件日期类型
        String dataDateType = batTaskRun.getDataDateType();  // 数据日期类型
        String signType = batTaskRun.getSignalType(); // 信号类型
        String signalFileName = batTaskRun.getSignalFileName(); // 信号文件全名
        String taskType = batTaskRun.getTaskType();//任务类型
        String signalDateType = batTaskRun.getSignalDateType();//信号日期类型
        String dataFileName = batTaskRun.getDataFileName();//数据文件全名
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String workDir = batTaskRun.getWorkDir();//工作目录 必填，全路径，需要用文件日期替换路径中的日期
        String remoteDir = batTaskRun.getRemoteDir();//远程目录
        String localDir = batTaskRun.getLocalDir();//本地目录 必填，全路径，需要用文件日期替换路径中的日期

        // 工作目录文件，落地表名.txt
        String workFileName = batTTabName.concat(".txt");
        try {
            logger.info("获取远程FTP服务端目录为[{}]", remoteDir);
            logger.info("信号类型：[" + signType + "]，文件交互方式：[" + fileExchangeType + "],工作处理目录：[" + workDir + "],本地处理目录：[" + localDir + "],远程目录：[" + remoteDir + "],任务日期：[" + taskDate + "]");

            // 当“任务类型”为“01-文件到表”时，需要下载数据文件
            if (Objects.equals(BatEnums.TASK_TYPE_01.key, taskType)) {
                // 获取数据文件名称
                logger.info("获取数据文件的名称 ：[" + dataFileName + "]");
                if (Objects.equals(BatEnums.FILE_EXCHANGE_TYPE_21.key, batTaskRun.getFileExchangeType())) {
                    this.downloadFileForFileExchangeType21(dataFileName, remoteDir, localDir, batTransferCfg);
                } else if (Objects.equals(BatEnums.FILE_EXCHANGE_TYPE_01.key, batTaskRun.getFileExchangeType())) {
                    this.downloadFileForFileExchangeType01(dataFileName, remoteDir, localDir, batTransferCfg);
                }
                // 将文件拷贝到指定工作目录
                String localDataFilePath = localDir + dataFileName;
                String workDataFilePath = workDir + workFileName;// 工作目录文件，落地表名.txt

                // 调用Shell脚本 将文件由GB18030格式转换为UTF-8格式
                DataLoadUtils dataLoadUtils = new DataLoadUtils();
                dataLoadUtils.changeCharacter4Gb18030(batTaskRun);
                // 再将文件拷贝到工作目录
                FileUtils.copy(workDir, localDataFilePath, workDataFilePath);
            }
        } catch (BizException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error("下载任务下所有文件至本地处理目录方法," + e.getMessage(), e);
            throw BizException.error(null, EchEnum.ECH080005.key, EchEnum.ECH080005.value, e.getMessage());
        }
        logger.info("***************           下载任务下所有文件至本地处理目录方法完成               ****************** ");
    }

    /**
     * 下载任务下所有文件至本地处理目录，包括信号文件和数据文件</br>
     * 部分系统文件需要先转码后再拷贝到工作目录，贷记卡数据单独处理
     *
     * @param batTransferCfg 文件传输配置信息
     * @param batTaskRun     任务运行管理
     * @throws Exception
     */
    public void downloadTaskFiles4Djk(BatTransferCfg batTransferCfg, BatTaskRun batTaskRun, String dataType) throws Exception {
        logger.info("***************           下载任务下所有文件至本地处理目录方法开始               ****************** ");
        // 变量定义
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String fileExchangeType = batTaskRun.getFileExchangeType();//文件交互方式 STD_FILE_EXCHANGE_TYPE
        String fileDateType = batTaskRun.getFileDateType();//文件日期类型
        String dataDateType = batTaskRun.getDataDateType();  // 数据日期类型
        String signType = batTaskRun.getSignalType(); // 信号类型
        String signalFileName = batTaskRun.getSignalFileName(); // 信号文件全名
        String taskType = batTaskRun.getTaskType();//任务类型
        String signalDateType = batTaskRun.getSignalDateType();//信号日期类型
        String dataFileName = batTaskRun.getDataFileName();//数据文件全名
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String workDir = batTaskRun.getWorkDir();//工作目录 必填，全路径，需要用文件日期替换路径中的日期
        String remoteDir = batTaskRun.getRemoteDir();//远程目录
        String localDir = batTaskRun.getLocalDir();//本地目录 必填，全路径，需要用文件日期替换路径中的日期

        // 工作目录文件，落地表名.txt
        String workFileName = batTTabName.concat(".txt");
        try {
            logger.info("获取远程FTP服务端目录为[{}]", remoteDir);
            logger.info("信号类型：[" + signType + "]，文件交互方式：[" + fileExchangeType + "],工作处理目录：[" + workDir + "],本地处理目录：[" + localDir + "],远程目录：[" + remoteDir + "],任务日期：[" + taskDate + "]");

            // 当“任务类型”为“01-文件到表”时，需要下载数据文件
            if (Objects.equals(BatEnums.TASK_TYPE_01.key, taskType)) {
                // 获取数据文件名称
                logger.info("获取数据文件的名称 ：[" + dataFileName + "]");
                if (Objects.equals(BatEnums.FILE_EXCHANGE_TYPE_21.key, batTaskRun.getFileExchangeType())) {
                    this.downloadFileForFileExchangeType21(dataFileName, remoteDir, localDir, batTransferCfg);
                } else if (Objects.equals(BatEnums.FILE_EXCHANGE_TYPE_01.key, batTaskRun.getFileExchangeType())) {
                    this.downloadFileForFileExchangeType01(dataFileName, remoteDir, localDir, batTransferCfg);
                }
                // 将文件拷贝到指定工作目录
                String localDataFilePath = localDir + dataFileName;
                String workDataFilePath = workDir + workFileName;// 工作目录文件，落地表名.txt

                DataLoadUtils dataLoadUtils = new DataLoadUtils();
                // 调用Shell脚本 将文件转换成行内格式
                dataLoadUtils.convertDjkToOds(batTaskRun, dataType);

                // 调用Shell脚本 将文件由GB2312格式转换为UTF-8格式
                dataLoadUtils.changeCharacter4Djk(batTaskRun);

                // 再将文件拷贝到工作目录
                FileUtils.copy(workDir, localDataFilePath, workDataFilePath);

            }
        } catch (BizException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error("下载任务下所有文件至本地处理目录方法," + e.getMessage(), e);
            throw BizException.error(null, EchEnum.ECH080005.key, EchEnum.ECH080005.value, e.getMessage());
        }
        logger.info("***************           下载任务下所有文件至本地处理目录方法完成               ****************** ");
    }

    /**
     * FTP下载文件,文件交互方式:01-FTP客户端；
     *
     * @param fileName       文件名称
     * @param remoteDir      远程目录
     * @param localDir       本地目录
     * @param batTransferCfg 文件传输配置
     * @throws Exception
     */
    private void downloadFileForFileExchangeType01(String fileName, String remoteDir, String localDir, BatTransferCfg batTransferCfg) throws Exception {
        logger.info("下载文件：01-FTP客户端开始");
        String localFtpClientFile = localDir + fileName; // 本地FTP客户端目录文件
        String remoteFtpServerFile = remoteDir + fileName;// 远程FTP服务端文件
        // 本地FTP客户端目录下，创建待下载文件存放的相对路径
        File file = new File(localFtpClientFile);
        File parent = file.getParentFile();
        if (parent != null && !parent.exists()) {
            parent.mkdirs();
        }
        // 获取FTP工具类实例
        FtpUtils ftpUtil = new FtpUtils(batTransferCfg);
        // 连接FTP服务器
        ftpUtil.connectServer();

        // 读取远程文件内容至"本地ftp客户端目录文件"中
        ftpUtil.download(remoteFtpServerFile, localFtpClientFile);
        // 关闭FTP服务器连接
        ftpUtil.closeConnect();
        logger.info("下载文件：01-FTP客户端完成");
    }

    /**
     * SFTP下载文件,文件交互方式:21-SFTP客户端；
     *
     * @param fileName
     * @param remoteDir
     * @param localDir
     * @param batTransferCfg
     * @throws Exception
     */
    private void downloadFileForFileExchangeType21(String fileName, String remoteDir, String localDir, BatTransferCfg batTransferCfg) throws Exception {
        logger.info("下载文件：21-SFTP客户端开始");

        // 参数准备
        if (sftpUtil.get() == null || !sftpUtil.get().isNew(batTransferCfg.getFtpUsername(), batTransferCfg.getFtpIp())) {// 检查是否已经连接
            sftpUtil.remove();
            sftpUtil.set(new SFTPUtil(batTransferCfg.getFtpUsername(), batTransferCfg.getFtpUserpsw(), batTransferCfg.getFtpIp(),
                    Integer.valueOf(batTransferCfg.getFtpPort())));
        }

        String localFtpClientFile = localDir + fileName; // 本地FTP客户端目录文件
        String remoteFtpServerFile = remoteDir + fileName;// 远程FTP服务端文件
        // 本地FTP客户端目录下，创建待下载文件存放的相对路径
        File file = new File(localFtpClientFile);
        File parent = file.getParentFile();
        if (parent != null && !parent.exists()) {
            parent.mkdirs();
        }
        Pattern pattern = Pattern.compile(fileName);
        Vector<ChannelSftp.LsEntry> files = sftpUtil.get().ls(remoteDir);
        for (ChannelSftp.LsEntry sftpFile : files) {
            if (sftpFile.getFilename().equals(fileName) || pattern.matcher(sftpFile.getFilename()).find()) {// 如果文件名相同或匹配到则下载文件
                /*
                 * 此处防止此种情况:需要下载Name_*数据文件,但是会匹配到SomeString_Name_*文件,导致数据文件无法下载
                 */
                int count = (int) pattern.splitAsStream(sftpFile.getFilename()).filter(new Predicate<String>() {// 过滤掉空字符串
                    @Override
                    public boolean test(String arg0) {
                        return !"".equals(arg0);
                    }
                }).count();
                if (count != SFTPUtil.countForStr(fileName, "[*]")) {
                    continue;
                }

                fileName = sftpFile.getFilename();
                logger.info("本地处理目录文件：【" + fileName + "】、本地SFTP客户端目录：【" + localDir + //
                        "】、本地SFTP客户端目录文件：【" + remoteFtpServerFile + "】、远程SFTP服务端文件：【" + fileName + "】");
                // 下载文件到“本地处理目录”
                sftpUtil.get().downFile(sftpUtil.get(), remoteDir, fileName, localDir, fileName, false);
                logger.info("下载文件：21-SFTP客户端完成");
                return;
            }
        }
        logger.info("下载文件：21-SFTP客户端完成");
    }

    /**
     * 调用Shell脚本 将贷记卡文件格式转换成ODS文件格式
     *
     * @param batTaskRun
     */
    public void convertDjkToOds(BatTaskRun batTaskRun, String dataType) {
        logger.info("调用Shell脚本 将贷记卡文件格式转换成ODS文件格式开始");
        // 变量定义
        String taskNo = batTaskRun.getTaskNo();// 任务编号
        String taskName = batTaskRun.getTaskName(); // 任务名称
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String dataFileName = batTaskRun.getDataFileName();//数据文件全名

        //转换之后的文件名字，默认是new_开始，比如 new_DATA_LOAN_000064163056_20210828
        String convertDataFileName = "new_".concat(dataFileName);
        Date openDayDate = DateUtils.parseDateByDef(taskDate);
        String filePathDate = DateUtils.formatDate(openDayDate, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd
        // usage:Please Enter OPEN_DAY,DATA_FILE_NAME,DATA_TYPE,CONVERT_FILE_NAME
        String cmd = "convert.sh".concat(" ")
                .concat(filePathDate).concat(" ")//文件路径
                .concat(dataFileName).concat(" ")//文件名称
                .concat(dataType).concat(" ") //文件类型
                .concat(convertDataFileName).concat(" ");
        logger.info("调用Shell脚本 将贷记卡文件格式转换成ODS文件格式,任务编号为[" + taskNo + "],任务名称为[" + taskName + "],待执行命令为:[" + cmd + "]");
        ShellUtils.runShellByProcessBuilder(cmd);
        logger.info("调用Shell脚本 将贷记卡文件格式转换成ODS文件格式结束");
        logger.info("调用Shell脚本 将贷记卡文件格式转换成ODS文件格式结束");
    }

    /**
     * 调用Shell脚本 将文件由GBK格式转换为UTF-8格式
     *
     * @param batTaskRun 任务运行管理
     */
    public void changeCharacter(BatTaskRun batTaskRun) {
        logger.info("调用Shell脚本 将文件由GBK格式转换为UTF-8格式开始");
        // 变量定义
        String taskNo = batTaskRun.getTaskNo();// 任务编号
        String taskName = batTaskRun.getTaskName(); // 任务名称
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名

        // 工作目录文件，落地表名.txt
        String dataFileName = batTTabName.concat(".txt");
        String dataUtf8FileName = batTTabName.concat("_utf8.txt");

        Date openDayDate = DateUtils.parseDateByDef(taskDate);
        String filePathDate = DateUtils.formatDate(openDayDate, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd

        // usage:Please Enter OPEN_DAY,DATA_FILE_NAME,DATA_UTF8_FILE_NAME
        String cmd = "change_character.sh".concat(" ")
                .concat(filePathDate).concat(" ")//文件路径
                .concat(dataFileName).concat(" ")//文件名称
                .concat(dataUtf8FileName).concat(" ");//UTF-8文件名称
        logger.info("调用Shell脚本 将文件由GBK格式转换为UTF-8格式,任务编号为[" + taskNo + "],任务名称为[" + taskName + "],待执行命令为:[" + cmd + "]");
        ShellUtils.runShellByProcessBuilder(cmd);
        logger.info("调用Shell脚本 将文件由GBK格式转换为UTF-8格式结束");
    }

    /**
     * 调用Shell脚本 将文件由GBK格式转换为UTF-8格式，贷记卡单独处理
     *
     * @param batTaskRun 任务运行管理
     */
    public void changeCharacter4Djk(BatTaskRun batTaskRun) {
        logger.info("调用Shell脚本 将文件由GBK格式转换为UTF-8格式，贷记卡单独处理开始");
        // 变量定义
        String taskNo = batTaskRun.getTaskNo();// 任务编号
        String taskName = batTaskRun.getTaskName(); // 任务名称
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String dataFileName = batTaskRun.getDataFileName();//数据文件全名

        // 工作目录文件，落地表名.txt
        String dataUtf8FileName = dataFileName.concat("_utf8");

        Date openDayDate = DateUtils.parseDateByDef(taskDate);
        String filePathDate = DateUtils.formatDate(openDayDate, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd

        // usage:Please Enter OPEN_DAY,DATA_FILE_NAME,DATA_UTF8_FILE_NAME
        String cmd = "change_character4djk.sh".concat(" ")
                .concat(filePathDate).concat(" ")//文件路径
                .concat(dataFileName).concat(" ")//文件名称
                .concat(dataUtf8FileName).concat(" ");//UTF-8文件名称
        logger.info("调用Shell脚本 将文件由GBK格式转换为UTF-8格式，贷记卡单独处理,任务编号为[" + taskNo + "],任务名称为[" + taskName + "],待执行命令为:[" + cmd + "]");
        ShellUtils.runShellByProcessBuilder(cmd);
        logger.info("调用Shell脚本 将文件由GBK格式转换为UTF-8格式，贷记卡单独处理结束");
    }

    /**
     * 调用Shell脚本 将文件由GB2312格式转换为UTF-8格式 </br>
     * 部分系统文件需要先转码后再拷贝到工作目录，目前征信系统、二代支付等需要此操作。
     *
     * @param batTaskRun 任务运行管理
     */
    public void changeCharacter4Gb2312(BatTaskRun batTaskRun) {
        logger.info("调用Shell脚本 将文件由GB2312格式转换为UTF-8格式开始");
        // 变量定义
        String taskNo = batTaskRun.getTaskNo();// 任务编号
        String taskName = batTaskRun.getTaskName(); // 任务名称
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String dataFileName = batTaskRun.getDataFileName();//数据文件全名
        // 工作目录文件，落地表名.txt
        // String dataFileName = batTTabName.concat(".txt");
        String dataUtf8FileName = batTTabName.concat("_utf8.txt");

        Date openDayDate = DateUtils.parseDateByDef(taskDate);
        String filePathDate = DateUtils.formatDate(openDayDate, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd

        // usage:Please Enter OPEN_DAY,DATA_FILE_NAME,DATA_UTF8_FILE_NAME
        String cmd = "change_character4gb2312.sh".concat(" ")
                .concat(filePathDate).concat(" ")//文件路径
                .concat(dataFileName).concat(" ")//文件名称
                .concat(dataUtf8FileName).concat(" ");//UTF-8文件名称
        logger.info("调用Shell脚本 将文件由GB2312格式转换为UTF-8格式,任务编号为[" + taskNo + "],任务名称为[" + taskName + "],待执行命令为:[" + cmd + "]");
        ShellUtils.runShellByProcessBuilder(cmd);
        logger.info("调用Shell脚本 将文件由GB2312格式转换为UTF-8格式结束");
    }

    /**
     * 调用Shell脚本 将文件由GB18030格式转换为UTF-8格式 </br>
     * 部分系统文件需要先转码后再拷贝到工作目录，目前票据系统中(PJP00008、PJP00009、GAPS0001)等需要此操作。
     *
     * @param batTaskRun 任务运行管理
     */
    public void changeCharacter4Gb18030(BatTaskRun batTaskRun) {
        logger.info("调用Shell脚本 将文件由GB18030格式转换为UTF-8格式开始");
        // 变量定义
        String taskNo = batTaskRun.getTaskNo();// 任务编号
        String taskName = batTaskRun.getTaskName(); // 任务名称
        String taskDate = batTaskRun.getTaskDate();//任务日期,格式yyyy-MM-dd
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String dataFileName = batTaskRun.getDataFileName();//数据文件全名
        // 工作目录文件，落地表名.txt
        // String dataFileName = batTTabName.concat(".txt");
        String dataUtf8FileName = batTTabName.concat("_utf8.txt");

        Date openDayDate = DateUtils.parseDateByDef(taskDate);
        String filePathDate = DateUtils.formatDate(openDayDate, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd

        // usage:Please Enter OPEN_DAY,DATA_FILE_NAME,DATA_UTF8_FILE_NAME
        String cmd = "change_character4gb18030.sh".concat(" ")
                .concat(filePathDate).concat(" ")//文件路径
                .concat(dataFileName).concat(" ")//文件名称
                .concat(dataUtf8FileName).concat(" ");//UTF-8文件名称
        logger.info("调用Shell脚本 将文件由GB18030格式转换为UTF-8格式,任务编号为[" + taskNo + "],任务名称为[" + taskName + "],待执行命令为:[" + cmd + "]");
        ShellUtils.runShellByProcessBuilder(cmd);
        logger.info("调用Shell脚本 将文件由GB18030格式转换为UTF-8格式结束");
    }
}
