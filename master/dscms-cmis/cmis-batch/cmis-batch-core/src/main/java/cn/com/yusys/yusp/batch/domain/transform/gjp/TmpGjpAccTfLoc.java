/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.transform.gjp;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: TmpGjpAccTfLoc
 * @类描述: tmp_gjp_acc_tf_loc数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 10:37:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_gjp_acc_tf_loc")
public class TmpGjpAccTfLoc extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 国结主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 交易ID **/
	@Column(name = "BIZ_ID", unique = false, nullable = true, length = 32)
	private String bizId;
	
	/** 国结业务编号 **/
	@Column(name = "BIZ_NO", unique = false, nullable = true, length = 32)
	private String bizNo;
	
	/** 国结交易日期 **/
	@Column(name = "OCCUR_DATE", unique = false, nullable = true, length = 8)
	private String occurDate;
	
	/** 交易CODE-国结业务交易码 **/
	@Column(name = "BIZ_CODE", unique = false, nullable = true, length = 32)
	private String bizCode;
	
	/** 国结业务类型 **/
	@Column(name = "TX_TYPE", unique = false, nullable = true, length = 2)
	private String txType;
	
	/** 操作类型 STD_OP_FLAG **/
	@Column(name = "GJP_OP_FLAG", unique = false, nullable = true, length = 2)
	private String gjpOpFlag;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 32)
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 32)
	private String contNo;
	
	/** 业务品种 042062 信贷用编码 **/
	@Column(name = "GJP_BIZ_TYPE", unique = false, nullable = true, length = 8)
	private String gjpBizType;
	
	/** 国结客户号 **/
	@Column(name = "KERNEL_NO", unique = false, nullable = true, length = 32)
	private String kernelNo;
	
	/** 国结客户名称 **/
	@Column(name = "CUST_NAME", unique = false, nullable = true, length = 280)
	private String custName;
	
	/** 国结到期日 **/
	@Column(name = "IOC_DATE", unique = false, nullable = true, length = 8)
	private String iocDate;
	
	/** 国结信用证编号 **/
	@Column(name = "IOC_NO", unique = false, nullable = true, length = 32)
	private String iocNo;
	
	/** 信用证币种 **/
	@Column(name = "TX_CCY", unique = false, nullable = true, length = 3)
	private String txCcy;
	
	/** 信用证金额 **/
	@Column(name = "APPLY_AMOUNT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal applyAmount;
	
	/** 信用证最大金额 **/
	@Column(name = "GJP_LOAN_AMOUNT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal gjpLoanAmount;
	
	/** 远期天数 **/
	@Column(name = "FAST_DAY", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal fastDay;
	
	/** 国结溢装比例 **/
	@Column(name = "SOLRT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal solrt;
	
	/** 状态 STD_ACCOUNT_STATUS **/
	@Column(name = "ACCOUNT_STATUS", unique = false, nullable = true, length = 1)
	private String accountStatus;
	
	/** 国结账务机构 **/
	@Column(name = "BIZ_BR_CDE", unique = false, nullable = true, length = 32)
	private String bizBrCde;
	
	/** 国结汇率 **/
	@Column(name = "EXCHANGE_RATE", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal exchangeRate;
	
	/** 信贷牌价汇率 **/
	@Column(name = "MMS_RATE", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal mmsRate;
	
	/** 国结创建人 **/
	@Column(name = "SYS_CRT_USER", unique = false, nullable = true, length = 32)
	private String sysCrtUser;
	
	/** 创建实体ID **/
	@Column(name = "SYS_ENTY_ID", unique = false, nullable = true, length = 32)
	private String sysEntyId;
	
	/** 国结创建日期 **/
	@Column(name = "SYS_CRT_DT", unique = false, nullable = true, length = 19)
	private java.util.Date sysCrtDt;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param bizId
	 */
	public void setBizId(String bizId) {
		this.bizId = bizId;
	}
	
    /**
     * @return bizId
     */
	public String getBizId() {
		return this.bizId;
	}
	
	/**
	 * @param bizNo
	 */
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	
    /**
     * @return bizNo
     */
	public String getBizNo() {
		return this.bizNo;
	}
	
	/**
	 * @param occurDate
	 */
	public void setOccurDate(String occurDate) {
		this.occurDate = occurDate;
	}
	
    /**
     * @return occurDate
     */
	public String getOccurDate() {
		return this.occurDate;
	}
	
	/**
	 * @param bizCode
	 */
	public void setBizCode(String bizCode) {
		this.bizCode = bizCode;
	}
	
    /**
     * @return bizCode
     */
	public String getBizCode() {
		return this.bizCode;
	}
	
	/**
	 * @param txType
	 */
	public void setTxType(String txType) {
		this.txType = txType;
	}
	
    /**
     * @return txType
     */
	public String getTxType() {
		return this.txType;
	}
	
	/**
	 * @param gjpOpFlag
	 */
	public void setGjpOpFlag(String gjpOpFlag) {
		this.gjpOpFlag = gjpOpFlag;
	}
	
    /**
     * @return gjpOpFlag
     */
	public String getGjpOpFlag() {
		return this.gjpOpFlag;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param gjpBizType
	 */
	public void setGjpBizType(String gjpBizType) {
		this.gjpBizType = gjpBizType;
	}
	
    /**
     * @return gjpBizType
     */
	public String getGjpBizType() {
		return this.gjpBizType;
	}
	
	/**
	 * @param kernelNo
	 */
	public void setKernelNo(String kernelNo) {
		this.kernelNo = kernelNo;
	}
	
    /**
     * @return kernelNo
     */
	public String getKernelNo() {
		return this.kernelNo;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param iocDate
	 */
	public void setIocDate(String iocDate) {
		this.iocDate = iocDate;
	}
	
    /**
     * @return iocDate
     */
	public String getIocDate() {
		return this.iocDate;
	}
	
	/**
	 * @param iocNo
	 */
	public void setIocNo(String iocNo) {
		this.iocNo = iocNo;
	}
	
    /**
     * @return iocNo
     */
	public String getIocNo() {
		return this.iocNo;
	}
	
	/**
	 * @param txCcy
	 */
	public void setTxCcy(String txCcy) {
		this.txCcy = txCcy;
	}
	
    /**
     * @return txCcy
     */
	public String getTxCcy() {
		return this.txCcy;
	}
	
	/**
	 * @param applyAmount
	 */
	public void setApplyAmount(java.math.BigDecimal applyAmount) {
		this.applyAmount = applyAmount;
	}
	
    /**
     * @return applyAmount
     */
	public java.math.BigDecimal getApplyAmount() {
		return this.applyAmount;
	}
	
	/**
	 * @param gjpLoanAmount
	 */
	public void setGjpLoanAmount(java.math.BigDecimal gjpLoanAmount) {
		this.gjpLoanAmount = gjpLoanAmount;
	}
	
    /**
     * @return gjpLoanAmount
     */
	public java.math.BigDecimal getGjpLoanAmount() {
		return this.gjpLoanAmount;
	}
	
	/**
	 * @param fastDay
	 */
	public void setFastDay(java.math.BigDecimal fastDay) {
		this.fastDay = fastDay;
	}
	
    /**
     * @return fastDay
     */
	public java.math.BigDecimal getFastDay() {
		return this.fastDay;
	}
	
	/**
	 * @param solrt
	 */
	public void setSolrt(java.math.BigDecimal solrt) {
		this.solrt = solrt;
	}
	
    /**
     * @return solrt
     */
	public java.math.BigDecimal getSolrt() {
		return this.solrt;
	}
	
	/**
	 * @param accountStatus
	 */
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	
    /**
     * @return accountStatus
     */
	public String getAccountStatus() {
		return this.accountStatus;
	}
	
	/**
	 * @param bizBrCde
	 */
	public void setBizBrCde(String bizBrCde) {
		this.bizBrCde = bizBrCde;
	}
	
    /**
     * @return bizBrCde
     */
	public String getBizBrCde() {
		return this.bizBrCde;
	}
	
	/**
	 * @param exchangeRate
	 */
	public void setExchangeRate(java.math.BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
    /**
     * @return exchangeRate
     */
	public java.math.BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}
	
	/**
	 * @param mmsRate
	 */
	public void setMmsRate(java.math.BigDecimal mmsRate) {
		this.mmsRate = mmsRate;
	}
	
    /**
     * @return mmsRate
     */
	public java.math.BigDecimal getMmsRate() {
		return this.mmsRate;
	}
	
	/**
	 * @param sysCrtUser
	 */
	public void setSysCrtUser(String sysCrtUser) {
		this.sysCrtUser = sysCrtUser;
	}
	
    /**
     * @return sysCrtUser
     */
	public String getSysCrtUser() {
		return this.sysCrtUser;
	}
	
	/**
	 * @param sysEntyId
	 */
	public void setSysEntyId(String sysEntyId) {
		this.sysEntyId = sysEntyId;
	}
	
    /**
     * @return sysEntyId
     */
	public String getSysEntyId() {
		return this.sysEntyId;
	}
	
	/**
	 * @param sysCrtDt
	 */
	public void setSysCrtDt(java.util.Date sysCrtDt) {
		this.sysCrtDt = sysCrtDt;
	}
	
    /**
     * @return sysCrtDt
     */
	public java.util.Date getSysCrtDt() {
		return this.sysCrtDt;
	}


}