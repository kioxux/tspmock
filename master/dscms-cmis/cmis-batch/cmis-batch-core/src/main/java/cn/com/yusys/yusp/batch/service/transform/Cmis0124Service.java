package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.domain.biz.TmpLmtAppCorp;
import cn.com.yusys.yusp.batch.domain.biz.TmpLmtAppPer;
import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0124Mapper;
import cn.com.yusys.yusp.batch.service.biz.TmpLmtAppCorpService;
import cn.com.yusys.yusp.batch.service.biz.TmpLmtAppPerService;
import cn.com.yusys.yusp.batch.service.client.biz.cmisbiz0001.CmisBiz0001Service;
import cn.com.yusys.yusp.batch.service.client.biz.cmisbiz0002.CmisBiz0002Service;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.server.cmisbiz0001.req.CmisBiz0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0001.resp.CmisBiz0001RespDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0002.req.CmisBiz0002ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0002.resp.CmisBiz0002RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0124</br>
 * 任务名称：加工任务-业务处理-授信每年复审提醒 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0124Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0124Service.class);

    @Autowired
    private Cmis0124Mapper cmis0124Mapper;
    @Autowired
    private TmpLmtAppPerService tmpLmtAppPerService;
    @Autowired
    private TmpLmtAppCorpService tmpLmtAppCorpService;
    @Autowired
    private CmisBiz0001Service cmisBiz0001Service;
    @Autowired
    private CmisBiz0002Service cmisBiz0002Service;

    /**
     * 插入消息提示表 ： 授信每年复审提醒
     *
     * @param openDay
     */
    public void cmis0124InsertWbMsgNotice(String openDay) {
//        logger.info("插入消息提示表 ：授信每年复审提醒开始,请求参数为:[{}]", openDay);
//        int insertWbMsgNotice = cmis0124Mapper.insertWbMsgNotice(openDay);
//        logger.info("插入消息提示表 ：授信每年复审提醒结束,返回参数为:[{}]", insertWbMsgNotice);
    }

    /**
     * 更新批复额度分项基础信息
     *
     * @param openDay
     */
    public void cmis0124UpdateApprLmtSubBasicInfo(String openDay) {
        logger.info("更新批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo = cmis0124Mapper.updateApprLmtSubBasicInfo(openDay);
        logger.info("更新批复额度分项基础信息结束,返回参数为:[{}]", updateApprLmtSubBasicInfo);

    }

    /**
     * 查询批复额度分项基础信息和调用单一客户授信复审接口
     *
     * @param openDay
     */
    public void cmis0124QueryApprLmtSubBasicInfo(String openDay) {
        logger.info("清空当日单一客户授信复审申请表开始,请求参数为:[{}]", openDay);
        cmis0124Mapper.truncateTmpLmtAppPer(openDay);
        logger.info("清空当日单一客户授信复审申请表结束");

        logger.info("插入当日单一客户授信复审申请表开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAppPer = cmis0124Mapper.insertTmpLmtAppPer(openDay);
        logger.info("插入当日单一客户授信复审申请表结束,返回参数为:[{}]", insertTmpLmtAppPer);

        logger.info("查询调用单一客户授信复审信息开始,请求参数为:[{}]", openDay);
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("taskDate", openDay);
        java.util.List<TmpLmtAppPer> queryApprLmtSubBasicInfoList = tmpLmtAppPerService.queryApprLmtSubBasicInfo(queryModel);
        logger.info("查询调用单一客户授信复审信息结束,返回参数为:[{}]", queryApprLmtSubBasicInfoList.size());

        logger.info("查询调用单一客户授信复审信息结束,待调用单一客户授信复审信息总笔数为:[{}]", queryApprLmtSubBasicInfoList.size());
        if (CollectionUtils.nonEmpty(queryApprLmtSubBasicInfoList)) {
            queryApprLmtSubBasicInfoList.stream().forEach(
                    queryApprLmtSubBasicInfo -> {
                        CmisBiz0001ReqDto cmisBiz0001ReqDto = new CmisBiz0001ReqDto();
                        BeanUtils.copyProperties(queryApprLmtSubBasicInfo, cmisBiz0001ReqDto);

                        logger.info("调用单一客户授信复审信息开始,请求参数为:[{}]", JSON.toJSONString(cmisBiz0001ReqDto));
                        CmisBiz0001RespDto cmisBiz0001RespDto = cmisBiz0001Service.cmisbiz0001(cmisBiz0001ReqDto);
                        logger.info("调用单一客户授信复审信息结束,返回参数为:[{}]", JSON.toJSONString(cmisBiz0001RespDto));

                        String rtnCode = cmisBiz0001RespDto.getRtnCode();// 返回码
                        String rtnMsg = cmisBiz0001RespDto.getRtnMsg();// 返回信息
                        String errorInfo = "调用单一客户授信复审信息结束，返回错误信息为:[" + rtnMsg + "]";
                        if (errorInfo.length() > 399) {
                            errorInfo = errorInfo.substring(0, 398);
                        }

                        queryApprLmtSubBasicInfo.setRetCode(rtnCode);
                        queryApprLmtSubBasicInfo.setRetMsg(errorInfo);
                        logger.info("将业务服务返回码和返回信息更新到单一客户授信复审申请表开始,请求参数为:[{}]", JSON.toJSONString(queryApprLmtSubBasicInfo));
                        int updateSelective = tmpLmtAppPerService.updateSelective(queryApprLmtSubBasicInfo);
                        logger.info("将业务服务返回码和返回信息更新到单一客户授信复审申请表结束,返回参数为:[{}]", JSON.toJSONString(updateSelective));
                    }
            );
        } else {
            logger.info("查询调用单一客户授信复审信息为空，不需要调用业务服务;");
        }
    }

    /**
     * 查询批复额度分项基础信息和调用集团客户授信复审信接口
     *
     * @param openDay
     */
    public void cmis0124QueryApprLmtSubBasicInfoGrp(String openDay) {

        logger.info("清空当日集团客户授信复审申请表开始,请求参数为:[{}]", openDay);
        cmis0124Mapper.truncateTmpLmtAppCorp(openDay);
        logger.info("清空当日集团客户授信复审申请表结束");

        logger.info("插入当日集团客户授信复审申请表开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAppCorp = cmis0124Mapper.insertTmpLmtAppCorp(openDay);
        logger.info("插入当日集团客户授信复审申请表结束,返回参数为:[{}]", insertTmpLmtAppCorp);

        logger.info("查询调用集团客户授信复审信息开始,请求参数为:[{}]", openDay);
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("taskDate", openDay);
        java.util.List<TmpLmtAppCorp> queryApprLmtSubBasicInfoGrpList = tmpLmtAppCorpService.queryApprLmtSubBasicInfoGrp(queryModel);
        logger.info("查询调用集团客户授信复审信息结束,返回参数为:[{}]", queryApprLmtSubBasicInfoGrpList.size());

        logger.info("查询调用集团客户授信复审信息结束,待调用集团客户授信复审信息总笔数为:[{}]", queryApprLmtSubBasicInfoGrpList.size());
        if (CollectionUtils.nonEmpty(queryApprLmtSubBasicInfoGrpList)) {
            queryApprLmtSubBasicInfoGrpList.stream().forEach(
                    queryApprLmtSubBasicInfoGrp -> {

                        CmisBiz0002ReqDto cmisBiz0002ReqDto = new CmisBiz0002ReqDto();
                        BeanUtils.copyProperties(queryApprLmtSubBasicInfoGrp, cmisBiz0002ReqDto);

                        logger.info("调用集团客户授信复审信息开始,请求参数为:[{}]", JSON.toJSONString(cmisBiz0002ReqDto));
                        CmisBiz0002RespDto cmisBiz0002RespDto = cmisBiz0002Service.cmisbiz0002(cmisBiz0002ReqDto);
                        logger.info("调用集团客户授信复审信息结束,返回参数为:[{}]", JSON.toJSONString(cmisBiz0002RespDto));

                        String grpSerno = cmisBiz0002RespDto.getResult();// 集团申请流水号
                        logger.info("根据返回结果查询集团授信申请表开始,请求参数为:[{}]", grpSerno);
                        int selectLmtGrpApp = cmis0124Mapper.selectLmtGrpApp(grpSerno);
                        logger.info("根据返回结果查询集团授信申请表结束,返回参数为:[{}]", selectLmtGrpApp);
                        String rtnCode = null;
                        String rtnMsg = null;
                        if (Objects.equals(0, selectLmtGrpApp)) {
                            rtnCode = EcbEnum.ECB019999.key;// 系统异常
                            rtnMsg = "根据返回结果[" + grpSerno + "]查询集团授信申请表，未能查询到结果";
                        } else {
                            rtnCode = SuccessEnum.SUCCESS.key;
                            rtnMsg = "根据返回结果[" + grpSerno + "]查询集团授信申请表，能查询到结果";
                        }
                        queryApprLmtSubBasicInfoGrp.setRetCode(rtnCode);
                        queryApprLmtSubBasicInfoGrp.setRetMsg(rtnMsg);
                        logger.info("将业务服务返回码和返回信息更新到单一客户授信复审申请表开始,请求参数为:[{}]", JSON.toJSONString(queryApprLmtSubBasicInfoGrp));
                        int updateSelective = tmpLmtAppCorpService.updateSelective(queryApprLmtSubBasicInfoGrp);
                        logger.info("将业务服务返回码和返回信息更新到单一客户授信复审申请表结束,返回参数为:[{}]", JSON.toJSONString(updateSelective));
                    }
            );
        } else {
            logger.info("查询调用集团客户授信复审信息为空，不需要调用业务服务;");
        }
    }
}
