/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.djk;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSDjkDataCustomerOrg
 * @类描述: bat_s_djk_data_customer_org数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-29 22:44:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_djk_data_customer_org")
public class BatSDjkDataCustomerOrg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 客户编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CUST_ID")
	private String custId;
	
	/** 行内客户号 **/
	@Column(name = "BANK_CUSTOMER_ID", unique = false, nullable = true, length = 20)
	private String bankCustomerId;
	
	/** 证件类型 **/
	@Column(name = "ID_TYPE", unique = false, nullable = true, length = 1)
	private String idType;
	
	/** 证件号码 **/
	@Column(name = "ID_NO", unique = false, nullable = true, length = 30)
	private String idNo;
	
	/** 姓名 **/
	@Column(name = "NAME", unique = false, nullable = true, length = 80)
	private String name;
	
	/** 性别 **/
	@Column(name = "GENDER", unique = false, nullable = true, length = 1)
	private String gender;
	
	/** 生日 **/
	@Column(name = "BIRTHDAY", unique = false, nullable = true, length = 8)
	private String birthday;
	
	/** 职业 **/
	@Column(name = "OCCUPATION", unique = false, nullable = true, length = 1)
	private String occupation;
	
	/** 职务 **/
	@Column(name = "TITLE", unique = false, nullable = true, length = 1)
	private String title;
	
	/** 本行员工号 **/
	@Column(name = "BANKMEMBER_NO", unique = false, nullable = true, length = 20)
	private String bankmemberNo;
	
	/** 国籍代码 **/
	@Column(name = "NATIONALITY", unique = false, nullable = true, length = 3)
	private String nationality;
	
	/** 是否永久居住 **/
	@Column(name = "PR_OF_COUNTRY", unique = false, nullable = true, length = 1)
	private String prOfCountry;
	
	/** 永久居住地国家代码 **/
	@Column(name = "RESIDENCY_COUNTRY_CD", unique = false, nullable = true, length = 3)
	private String residencyCountryCd;
	
	/** 婚姻状况 **/
	@Column(name = "MARITAL_STATUS", unique = false, nullable = true, length = 1)
	private String maritalStatus;
	
	/** 教育状况 **/
	@Column(name = "QUALIFICATION", unique = false, nullable = true, length = 1)
	private String qualification;
	
	/** 风险情况 **/
	@Column(name = "SOCIAL_STATUS", unique = false, nullable = true, length = 1)
	private String socialStatus;
	
	/** 发证机关所在地址 **/
	@Column(name = "ID_ISSUER_ADDRESS", unique = false, nullable = true, length = 200)
	private String idIssuerAddress;
	
	/** 家庭电话 **/
	@Column(name = "HOME_PHONE", unique = false, nullable = true, length = 20)
	private String homePhone;
	
	/** 房屋持有类型 **/
	@Column(name = "HOUSE_OWNERSHIP", unique = false, nullable = true, length = 1)
	private String houseOwnership;
	
	/** 住宅类型 **/
	@Column(name = "HOUSE_TYPE", unique = false, nullable = true, length = 1)
	private String houseType;
	
	/** 现住址居住起始年月 **/
	@Column(name = "HOME_STAND_FROM", unique = false, nullable = true, length = 6)
	private String homeStandFrom;
	
	/** 个人资产类型 **/
	@Column(name = "LIQUID_ASSET", unique = false, nullable = true, length = 1)
	private String liquidAsset;
	
	/** 移动电话 **/
	@Column(name = "MOBILE_NO", unique = false, nullable = true, length = 20)
	private String mobileNo;
	
	/** 电子邮箱 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 80)
	private String email;
	
	/** 就业状态 **/
	@Column(name = "EMP_STATUS", unique = false, nullable = true, length = 1)
	private String empStatus;
	
	/** 抚养人数 **/
	@Column(name = "NBR_OF_DEPENDENTS", unique = false, nullable = true, length = 2)
	private String nbrOfDependents;
	
	/** 语言代码 **/
	@Column(name = "LANGUAGE_IND", unique = false, nullable = true, length = 4)
	private String languageInd;
	
	/** 创建日期 **/
	@Column(name = "SETUP_DATE", unique = false, nullable = true, length = 8)
	private String setupDate;
	
	/** 社保缴存金额 **/
	@Column(name = "SOCIAL_INS_AMT", unique = false, nullable = true, length = 40)
	private String socialInsAmt;
	
	/** 驾驶证号码 **/
	@Column(name = "DRIVE_LICENSE_ID", unique = false, nullable = true, length = 20)
	private String driveLicenseId;
	
	/** 驾驶证登记日期 **/
	@Column(name = "DRIVE_LIC_REG_DATE", unique = false, nullable = true, length = 8)
	private String driveLicRegDate;
	
	/** 工作稳定性 **/
	@Column(name = "EMP_STABILITY", unique = false, nullable = true, length = 1)
	private String empStability;
	
	/** 公司名称 **/
	@Column(name = "CORP_NAME", unique = false, nullable = true, length = 80)
	private String corpName;
	
	/** 客户信用额度 **/
	@Column(name = "CUST_CREDIT_LIMIT", unique = false, nullable = true, length = 20)
	private String custCreditLimit;
	
	/** 大额分期额度 **/
	@Column(name = "LARGE_LOAN_CRLIMIT", unique = false, nullable = true, length = 20)
	private String largeLoanCrlimit;
	
	/** 循环贷款额度 **/
	@Column(name = "REVOLVE_LOAN_CRLIMIT", unique = false, nullable = true, length = 20)
	private String revolveLoanCrlimit;
	
	/** 预留问题 **/
	@Column(name = "RESERVED_QUESTION", unique = false, nullable = true, length = 40)
	private String reservedQuestion;
	
	/** 预留答案 **/
	@Column(name = "RESERVED_ANSWER", unique = false, nullable = true, length = 40)
	private String reservedAnswer;
	
	/** 任职部门 **/
	@Column(name = "DEPARTMENT", unique = false, nullable = true, length = 80)
	private String department;
	
	/** 证件到期日 **/
	@Column(name = "CERT_EXPIRE_DATE", unique = false, nullable = true, length = 8)
	private String certExpireDate;
	
	/** 最近一次维护日期 **/
	@Column(name = "LAST_MODIFIED_TIME", unique = false, nullable = true, length = 14)
	private String lastModifiedTime;
	
	/** 证件起始日 **/
	@Column(name = "ID_ISSUE_DATE", unique = false, nullable = true, length = 8)
	private String idIssueDate;
	
	/** 民族 **/
	@Column(name = "NATION", unique = false, nullable = true, length = 40)
	private String nation;
	
	/** 预留域 **/
	@Column(name = "FILLER", unique = false, nullable = true, length = 200)
	private String filler;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param bankCustomerId
	 */
	public void setBankCustomerId(String bankCustomerId) {
		this.bankCustomerId = bankCustomerId;
	}
	
    /**
     * @return bankCustomerId
     */
	public String getBankCustomerId() {
		return this.bankCustomerId;
	}
	
	/**
	 * @param idType
	 */
	public void setIdType(String idType) {
		this.idType = idType;
	}
	
    /**
     * @return idType
     */
	public String getIdType() {
		return this.idType;
	}
	
	/**
	 * @param idNo
	 */
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	
    /**
     * @return idNo
     */
	public String getIdNo() {
		return this.idNo;
	}
	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
    /**
     * @return name
     */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @param gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	
    /**
     * @return gender
     */
	public String getGender() {
		return this.gender;
	}
	
	/**
	 * @param birthday
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
    /**
     * @return birthday
     */
	public String getBirthday() {
		return this.birthday;
	}
	
	/**
	 * @param occupation
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	
    /**
     * @return occupation
     */
	public String getOccupation() {
		return this.occupation;
	}
	
	/**
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
    /**
     * @return title
     */
	public String getTitle() {
		return this.title;
	}
	
	/**
	 * @param bankmemberNo
	 */
	public void setBankmemberNo(String bankmemberNo) {
		this.bankmemberNo = bankmemberNo;
	}
	
    /**
     * @return bankmemberNo
     */
	public String getBankmemberNo() {
		return this.bankmemberNo;
	}
	
	/**
	 * @param nationality
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
    /**
     * @return nationality
     */
	public String getNationality() {
		return this.nationality;
	}
	
	/**
	 * @param prOfCountry
	 */
	public void setPrOfCountry(String prOfCountry) {
		this.prOfCountry = prOfCountry;
	}
	
    /**
     * @return prOfCountry
     */
	public String getPrOfCountry() {
		return this.prOfCountry;
	}
	
	/**
	 * @param residencyCountryCd
	 */
	public void setResidencyCountryCd(String residencyCountryCd) {
		this.residencyCountryCd = residencyCountryCd;
	}
	
    /**
     * @return residencyCountryCd
     */
	public String getResidencyCountryCd() {
		return this.residencyCountryCd;
	}
	
	/**
	 * @param maritalStatus
	 */
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	
    /**
     * @return maritalStatus
     */
	public String getMaritalStatus() {
		return this.maritalStatus;
	}
	
	/**
	 * @param qualification
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	
    /**
     * @return qualification
     */
	public String getQualification() {
		return this.qualification;
	}
	
	/**
	 * @param socialStatus
	 */
	public void setSocialStatus(String socialStatus) {
		this.socialStatus = socialStatus;
	}
	
    /**
     * @return socialStatus
     */
	public String getSocialStatus() {
		return this.socialStatus;
	}
	
	/**
	 * @param idIssuerAddress
	 */
	public void setIdIssuerAddress(String idIssuerAddress) {
		this.idIssuerAddress = idIssuerAddress;
	}
	
    /**
     * @return idIssuerAddress
     */
	public String getIdIssuerAddress() {
		return this.idIssuerAddress;
	}
	
	/**
	 * @param homePhone
	 */
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	
    /**
     * @return homePhone
     */
	public String getHomePhone() {
		return this.homePhone;
	}
	
	/**
	 * @param houseOwnership
	 */
	public void setHouseOwnership(String houseOwnership) {
		this.houseOwnership = houseOwnership;
	}
	
    /**
     * @return houseOwnership
     */
	public String getHouseOwnership() {
		return this.houseOwnership;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	
    /**
     * @return houseType
     */
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param homeStandFrom
	 */
	public void setHomeStandFrom(String homeStandFrom) {
		this.homeStandFrom = homeStandFrom;
	}
	
    /**
     * @return homeStandFrom
     */
	public String getHomeStandFrom() {
		return this.homeStandFrom;
	}
	
	/**
	 * @param liquidAsset
	 */
	public void setLiquidAsset(String liquidAsset) {
		this.liquidAsset = liquidAsset;
	}
	
    /**
     * @return liquidAsset
     */
	public String getLiquidAsset() {
		return this.liquidAsset;
	}
	
	/**
	 * @param mobileNo
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
    /**
     * @return mobileNo
     */
	public String getMobileNo() {
		return this.mobileNo;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
    /**
     * @return email
     */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param empStatus
	 */
	public void setEmpStatus(String empStatus) {
		this.empStatus = empStatus;
	}
	
    /**
     * @return empStatus
     */
	public String getEmpStatus() {
		return this.empStatus;
	}
	
	/**
	 * @param nbrOfDependents
	 */
	public void setNbrOfDependents(String nbrOfDependents) {
		this.nbrOfDependents = nbrOfDependents;
	}
	
    /**
     * @return nbrOfDependents
     */
	public String getNbrOfDependents() {
		return this.nbrOfDependents;
	}
	
	/**
	 * @param languageInd
	 */
	public void setLanguageInd(String languageInd) {
		this.languageInd = languageInd;
	}
	
    /**
     * @return languageInd
     */
	public String getLanguageInd() {
		return this.languageInd;
	}
	
	/**
	 * @param setupDate
	 */
	public void setSetupDate(String setupDate) {
		this.setupDate = setupDate;
	}
	
    /**
     * @return setupDate
     */
	public String getSetupDate() {
		return this.setupDate;
	}
	
	/**
	 * @param socialInsAmt
	 */
	public void setSocialInsAmt(String socialInsAmt) {
		this.socialInsAmt = socialInsAmt;
	}
	
    /**
     * @return socialInsAmt
     */
	public String getSocialInsAmt() {
		return this.socialInsAmt;
	}
	
	/**
	 * @param driveLicenseId
	 */
	public void setDriveLicenseId(String driveLicenseId) {
		this.driveLicenseId = driveLicenseId;
	}
	
    /**
     * @return driveLicenseId
     */
	public String getDriveLicenseId() {
		return this.driveLicenseId;
	}
	
	/**
	 * @param driveLicRegDate
	 */
	public void setDriveLicRegDate(String driveLicRegDate) {
		this.driveLicRegDate = driveLicRegDate;
	}
	
    /**
     * @return driveLicRegDate
     */
	public String getDriveLicRegDate() {
		return this.driveLicRegDate;
	}
	
	/**
	 * @param empStability
	 */
	public void setEmpStability(String empStability) {
		this.empStability = empStability;
	}
	
    /**
     * @return empStability
     */
	public String getEmpStability() {
		return this.empStability;
	}
	
	/**
	 * @param corpName
	 */
	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}
	
    /**
     * @return corpName
     */
	public String getCorpName() {
		return this.corpName;
	}
	
	/**
	 * @param custCreditLimit
	 */
	public void setCustCreditLimit(String custCreditLimit) {
		this.custCreditLimit = custCreditLimit;
	}
	
    /**
     * @return custCreditLimit
     */
	public String getCustCreditLimit() {
		return this.custCreditLimit;
	}
	
	/**
	 * @param largeLoanCrlimit
	 */
	public void setLargeLoanCrlimit(String largeLoanCrlimit) {
		this.largeLoanCrlimit = largeLoanCrlimit;
	}
	
    /**
     * @return largeLoanCrlimit
     */
	public String getLargeLoanCrlimit() {
		return this.largeLoanCrlimit;
	}
	
	/**
	 * @param revolveLoanCrlimit
	 */
	public void setRevolveLoanCrlimit(String revolveLoanCrlimit) {
		this.revolveLoanCrlimit = revolveLoanCrlimit;
	}
	
    /**
     * @return revolveLoanCrlimit
     */
	public String getRevolveLoanCrlimit() {
		return this.revolveLoanCrlimit;
	}
	
	/**
	 * @param reservedQuestion
	 */
	public void setReservedQuestion(String reservedQuestion) {
		this.reservedQuestion = reservedQuestion;
	}
	
    /**
     * @return reservedQuestion
     */
	public String getReservedQuestion() {
		return this.reservedQuestion;
	}
	
	/**
	 * @param reservedAnswer
	 */
	public void setReservedAnswer(String reservedAnswer) {
		this.reservedAnswer = reservedAnswer;
	}
	
    /**
     * @return reservedAnswer
     */
	public String getReservedAnswer() {
		return this.reservedAnswer;
	}
	
	/**
	 * @param department
	 */
	public void setDepartment(String department) {
		this.department = department;
	}
	
    /**
     * @return department
     */
	public String getDepartment() {
		return this.department;
	}
	
	/**
	 * @param certExpireDate
	 */
	public void setCertExpireDate(String certExpireDate) {
		this.certExpireDate = certExpireDate;
	}
	
    /**
     * @return certExpireDate
     */
	public String getCertExpireDate() {
		return this.certExpireDate;
	}
	
	/**
	 * @param lastModifiedTime
	 */
	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	
    /**
     * @return lastModifiedTime
     */
	public String getLastModifiedTime() {
		return this.lastModifiedTime;
	}
	
	/**
	 * @param idIssueDate
	 */
	public void setIdIssueDate(String idIssueDate) {
		this.idIssueDate = idIssueDate;
	}
	
    /**
     * @return idIssueDate
     */
	public String getIdIssueDate() {
		return this.idIssueDate;
	}
	
	/**
	 * @param nation
	 */
	public void setNation(String nation) {
		this.nation = nation;
	}
	
    /**
     * @return nation
     */
	public String getNation() {
		return this.nation;
	}
	
	/**
	 * @param filler
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
    /**
     * @return filler
     */
	public String getFiller() {
		return this.filler;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}