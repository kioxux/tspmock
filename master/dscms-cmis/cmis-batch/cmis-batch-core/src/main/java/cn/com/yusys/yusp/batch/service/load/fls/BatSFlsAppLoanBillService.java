/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.fls;

import cn.com.yusys.yusp.batch.domain.load.fls.BatSFlsAppLoanBill;
import cn.com.yusys.yusp.batch.repository.mapper.load.fls.BatSFlsAppLoanBillMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSFlsAppLoanBillService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-07-06 11:10:57
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSFlsAppLoanBillService {

    private static final Logger logger = LoggerFactory.getLogger(BatSFlsAppLoanBillService.class);
    @Autowired
    private TableUtilsService tableUtilsService;
    @Autowired
    private BatSFlsAppLoanBillMapper batSFlsAppLoanBillMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatSFlsAppLoanBill selectByPrimaryKey(String loanId) {
        return batSFlsAppLoanBillMapper.selectByPrimaryKey(loanId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatSFlsAppLoanBill> selectAll(QueryModel model) {
        List<BatSFlsAppLoanBill> records = (List<BatSFlsAppLoanBill>) batSFlsAppLoanBillMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatSFlsAppLoanBill> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSFlsAppLoanBill> list = batSFlsAppLoanBillMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatSFlsAppLoanBill record) {
        return batSFlsAppLoanBillMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatSFlsAppLoanBill record) {
        return batSFlsAppLoanBillMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatSFlsAppLoanBill record) {
        return batSFlsAppLoanBillMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatSFlsAppLoanBill record) {
        return batSFlsAppLoanBillMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String loanId) {
        return batSFlsAppLoanBillMapper.deleteByPrimaryKey(loanId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSFlsAppLoanBillMapper.deleteByIds(ids);
    }

    /**
     * 将T表数据merge到S表
     *
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]开始,请求参数为:{}", openDay);
        int updateSResult = batSFlsAppLoanBillMapper.updateDataDateByOpenDay(openDay);
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]结束,返回参数为:{}", updateSResult);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建非零内评-历史表-借据表[bat_s_fls_app_loan_bill]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_fls_app_loan_bill");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("重建非零内评-历史表-借据表[bat_s_fls_app_loan_bill]结束");
        return 0;
    }
}
