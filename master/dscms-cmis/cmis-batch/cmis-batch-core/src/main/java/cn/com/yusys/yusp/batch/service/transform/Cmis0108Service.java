package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0108Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0108</br>
 * 任务名称：加工任务-业务处理-业务合同处理 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0108Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0108Service.class);

    @Autowired
    private Cmis0108Mapper cmis0108Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新业务库中业务合同相关的表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0108UpdateCmisBizCtr(String openDay) {

        logger.info("重命名创建和删除贷款台账信息表 开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","temp_acc_loan_0108");
        logger.info("重命名创建和删除贷款台账信息表 结束");

        logger.info("插入贷款台账信息表开始,请求参数为:[{}]", openDay);
        int insertTempAccLoan0108 = cmis0108Mapper.insertTempAccLoan0108(openDay);
        logger.info("插入贷款台账信息表 结束,返回参数为:[{}]", insertTempAccLoan0108);
        tableUtilsService.analyzeTable("cmis_biz","temp_acc_loan_0108");//分析表

        logger.info("贷款全部结清时，贷款协议注销 开始,请求参数为:[{}]", openDay);
        int updateCtrLoanCont = cmis0108Mapper.updateCtrLoanCont(openDay);
        logger.info("贷款全部结清时，贷款协议注销 结束,返回参数为:[{}]", updateCtrLoanCont);

        logger.info("贷款全部结清时，贷款协议注销 开始,请求参数为:[{}]", openDay);
        int updateCtrAccpCont = cmis0108Mapper.updateCtrAccpCont(openDay);
        logger.info("贷款全部结清时，贷款协议注销 结束,返回参数为:[{}]", updateCtrAccpCont);

        logger.info("贴现台账全部结清时，贴现协议注销 开始,请求参数为:[{}]", openDay);
        int updateCtrDiscCont = cmis0108Mapper.updateCtrDiscCont(openDay);
        logger.info("贴现台账全部结清时，贴现协议注销 结束,返回参数为:[{}]", updateCtrDiscCont);

        logger.info("保函台账全部结清时，保函协议注销 开始,请求参数为:[{}]", openDay);
        int updateCtrCvrgCont = cmis0108Mapper.updateCtrCvrgCont(openDay);
        logger.info("保函台账全部结清时，保函协议注销 结束,返回参数为:[{}]", updateCtrCvrgCont);

        logger.info("信用证台账全部结清时，信用证协议注销 开始,请求参数为:[{}]", openDay);
        int updateCtrTfLocCont = cmis0108Mapper.updateCtrTfLocCont(openDay);
        logger.info("信用证台账全部结清时，信用证协议注销 结束,返回参数为:[{}]", updateCtrTfLocCont);

        logger.info("委托贷款台账全部结清时，委托贷款协议注销 开始,请求参数为:[{}]", openDay);
        int updateCtrEntrustLoanCont = cmis0108Mapper.updateCtrEntrustLoanCont(openDay);
        logger.info("贷款全部结清时，贷款协议注销 结束,返回参数为:[{}]", updateCtrEntrustLoanCont);


        logger.info("贷款合同项下无台账，到期自动注销 开始,请求参数为:[{}]", openDay);
        int updateCtrLoanContByNoLoan = cmis0108Mapper.updateCtrLoanContByNoLoan(openDay);
        logger.info("贷款合同项下无台账，到期自动注销 结束,返回参数为:[{}]", updateCtrLoanContByNoLoan);

        logger.info("银票合同项下无台账，到期自动注销 开始,请求参数为:[{}]", openDay);
        int updateCtrAccpContByNoAccp = cmis0108Mapper.updateCtrAccpContByNoAccp(openDay);
        logger.info("银票合同项下无台账，到期自动注销 结束,返回参数为:[{}]", updateCtrAccpContByNoAccp);

        logger.info("贴现合同项下无台账，到期自动注销 开始,请求参数为:[{}]", openDay);
        int updateCtrAccpContByNoDisc = cmis0108Mapper.updateCtrAccpContByNoDisc(openDay);
        logger.info("贴现合同项下无台账，到期自动注销 结束,返回参数为:[{}]", updateCtrAccpContByNoDisc);

        logger.info("一般贴现合同没有到期日，若一般贴现合同，项下存在贴现的票，且票据都已结清，则认为合同已注销 开始,请求参数为:[{}]", openDay);
        int updateCtrDiscCont01 = cmis0108Mapper.updateCtrDiscCont01(openDay);
        logger.info("一般贴现合同没有到期日，若一般贴现合同，项下存在贴现的票，且票据都已结清，则认为合同已注销结束,返回参数为:[{}]", updateCtrDiscCont01);

        logger.info("保函合同项下无台账，到期自动注销 开始,请求参数为:[{}]", openDay);
        int updateCtrAccpContByNoCvrg = cmis0108Mapper.updateCtrAccpContByNoCvrg(openDay);
        logger.info("保函合同项下无台账，到期自动注销 结束,返回参数为:[{}]", updateCtrAccpContByNoCvrg);

        logger.info("信用证合同项下无台账，到期自动注销 开始,请求参数为:[{}]", openDay);
        int updateCtrAccpContByNoTfLoc = cmis0108Mapper.updateCtrAccpContByNoTfLoc(openDay);
        logger.info("信用证合同项下无台账，到期自动注销 结束,返回参数为:[{}]", updateCtrAccpContByNoTfLoc);

        logger.info("委托贷款合同项下无台账，到期自动注销 开始,请求参数为:[{}]", openDay);
        int updateCtrAccpContByNoEntrustLoan = cmis0108Mapper.updateCtrAccpContByNoEntrustLoan(openDay);
        logger.info("委托贷款合同项下无台账，到期自动注销 结束,返回参数为:[{}]", updateCtrAccpContByNoEntrustLoan);

        logger.info("更新合作方案台账信息中合作方案状态开始,请求参数为:[{}]", openDay);
        int updateCoopPlanAccInfo = cmis0108Mapper.updateCoopPlanAccInfo(openDay);
        logger.info("更新合作方案台账信息中合作方案状态结束,返回参数为:[{}]", updateCoopPlanAccInfo);

        logger.info("更新合作方协议台账信息中协议状态开始,请求参数为:[{}]", openDay);
        int updateCoopPartnerAgrAccInfo = cmis0108Mapper.updateCoopPartnerAgrAccInfo(openDay);
        logger.info("更新合作方协议台账信息中协议状态结束,返回参数为:[{}]", updateCoopPartnerAgrAccInfo);

        logger.info("重命名创建和删除临时表-加工担保合同与借款合同关联关系 开始");
        tableUtilsService.renameCreateDropTable("cmis_batch","TMP_GRT_GUAR_BIZ_RST_ATSTUS");
        logger.info("重命名创建和删除临时表-加工担保合同与借款合同关联关系 结束");

        logger.info("加工临时表-加工担保合同与借款合同关联关系开始，请求参数:[{}]", openDay);
        int insertTmpGrtGuarBizRstAtstus = cmis0108Mapper.insertTmpGrtGuarBizRstAtstus(openDay);
        logger.info("加工临时表-加工担保合同与借款合同关联关系结束，返回参数:[{}]", insertTmpGrtGuarBizRstAtstus);
        tableUtilsService.analyzeTable("cmis_batch","TMP_GRT_GUAR_BIZ_RST_ATSTUS");//分析表

        logger.info("更新最高额担保合同状态开始,请求参数为:[{}]", openDay);
        int updateGrtGuarContByGuarContState = cmis0108Mapper.updateGrtGuarContByGuarContState(openDay);
        logger.info("更新最高额担保合同状态结束,返回参数为:[{}]", updateGrtGuarContByGuarContState);


        logger.info("更新一般担保合同状态开始,请求参数为:[{}]", openDay);
        int updateGrtGuarContByGuarContState2 = cmis0108Mapper.updateGrtGuarContByGuarContState2(openDay);
        logger.info("更新一般担保合同状态结束,返回参数为:[{}]", updateGrtGuarContByGuarContState2);

        logger.info("更新最高额授信协议到期注销,请求参数为:[{}]", openDay);
        int updateCtrHighAmtAgrCont = cmis0108Mapper.updateCtrHighAmtAgrCont(openDay);
        logger.info("更新最高额授信协议到期注销,返回参数为:[{}]", updateCtrHighAmtAgrCont);

        logger.info("更新iqp_loan_app的approve_status='997' 开始,请求参数为:[{}]", openDay);
        int updateIqpLoanApp01 = cmis0108Mapper.updateIqpLoanApp01(openDay);
        logger.info("更新iqp_loan_app的approve_status='997' 结束,返回参数为:[{}]", updateIqpLoanApp01);

        logger.info("更新IQP_HIGH_AMT_AGR_APP的approve_status='997' 开始,请求参数为:[{}]", openDay);
        int updateIqpHighAmtAgrApp02 = cmis0108Mapper.updateIqpHighAmtAgrApp02(openDay);
        logger.info("更新IQP_HIGH_AMT_AGR_APP的approve_status='997' 结束,返回参数为:[{}]", updateIqpHighAmtAgrApp02);

        logger.info("更新IQP_ENTRUST_LOAN_APP的approve_status='997' 开始,请求参数为:[{}]", openDay);
        int updateIqpEntrustLoanApp03 = cmis0108Mapper.updateIqpEntrustLoanApp03(openDay);
        logger.info("更新IQP_ENTRUST_LOAN_APP的approve_status='997' 结束,返回参数为:[{}]", updateIqpEntrustLoanApp03);

    }
}
