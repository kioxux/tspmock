package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0501Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0501</br>
 * 任务名称：加工任务-业务处理-更新用户管理表  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0501Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0501Service.class);

    @Autowired
    private Cmis0501Mapper cmis0501Mapper;

    /**
     * 更新系统用户表
     *
     * @param openDay
     */
    public void cmis0501UpdateAdminSmUser(String openDay) {




        logger.info("清理临时表 开始,请求参数为:[{}]", openDay);
        int truncateForAdminSmUse = cmis0501Mapper.truncateForAdminSmUse(openDay);
        logger.info("清理临时表 结束", truncateForAdminSmUse);


        logger.info("更新系统用户表[离岗时,更新用户表电话、邮件、生日、状态、姓名、证件号码]开始,请求参数为:[{}]", openDay);
        int updateAdminSmUser01 = cmis0501Mapper.insertAdminSmUser01(openDay);
        logger.info("更新系统用户表[离岗时,更新用户表电话、邮件、生日、状态、姓名、证件号码]结束,返回参数为:[{}]", updateAdminSmUser01);
        logger.info("更新系统用户表[在岗时  更新用户表电话、邮件、生日、状态、姓名、证件号码]开始,请求参数为:[{}]", openDay);
        int updateAdminSmUser02 = cmis0501Mapper.updateAdminSmUser02(openDay);
        logger.info("更新系统用户表[在岗时  更新用户表电话、邮件、生日、状态、姓名、证件号码]结束,返回参数为:[{}]", updateAdminSmUser02);
    }
}
