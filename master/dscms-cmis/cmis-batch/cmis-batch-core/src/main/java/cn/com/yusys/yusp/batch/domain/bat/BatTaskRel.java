/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.bat;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTaskRel
 * @类描述: bat_task_rel数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-15 14:17:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_task_rel")
public class BatTaskRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 依赖编号 **/
    @Id
    @Column(name = "REL_NO")
    private String relNo;

    /** 依赖任务编号 **/
    @Id
    @Column(name = "REL_TASK_NO")
    private String relTaskNo;

    /** 依赖任务名称 **/
    @Column(name = "REL_TASK_NAME", unique = false, nullable = true, length = 100)
    private String relTaskName;

    /** 启用标志 STD_YES_NO **/
    @Column(name = "USE_FLAG", unique = false, nullable = true, length = 1)
    private String useFlag;

    /** 备注 **/
    @Column(name = "REMARKS", unique = false, nullable = true, length = 250)
    private String remarks;

    /** 登记人 **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date createTime;

    /** 修改时间 **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date updateTime;
	
	
	/**
	 * @param relNo
	 */
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}
	
    /**
     * @return relNo
     */
	public String getRelNo() {
		return this.relNo;
	}
	
	/**
	 * @param relTaskNo
	 */
	public void setRelTaskNo(String relTaskNo) {
        this.relTaskNo = relTaskNo;
    }

    /**
     * @return relTaskNo
     */
    public String getRelTaskNo() {
        return this.relTaskNo;
    }

    /**
     * @return relTaskName
     */
    public String getRelTaskName() {
        return this.relTaskName;
    }

    /**
     * @param relTaskName
     */
    public void setRelTaskName(String relTaskName) {
        this.relTaskName = relTaskName;
    }

    /**
     * @param useFlag
     */
    public void setUseFlag(String useFlag) {
        this.useFlag = useFlag;
    }

    /**
     * @return useFlag
     */
    public String getUseFlag() {
		return this.useFlag;
	}
	
	/**
	 * @param remarks
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
    /**
     * @return remarks
     */
	public String getRemarks() {
		return this.remarks;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}