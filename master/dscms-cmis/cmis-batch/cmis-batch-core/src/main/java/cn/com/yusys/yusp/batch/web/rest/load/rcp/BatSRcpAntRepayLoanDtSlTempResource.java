/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.rcp;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpAntRepayLoanDtSlTemp;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpAntRepayLoanDtSlTempService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpAntRepayLoanDtSlTempResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-10 14:48:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsrcpantrepayloandtsltemp")
public class BatSRcpAntRepayLoanDtSlTempResource {
    @Autowired
    private BatSRcpAntRepayLoanDtSlTempService batSRcpAntRepayLoanDtSlTempService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSRcpAntRepayLoanDtSlTemp>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSRcpAntRepayLoanDtSlTemp> list = batSRcpAntRepayLoanDtSlTempService.selectAll(queryModel);
        return new ResultDto<List<BatSRcpAntRepayLoanDtSlTemp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSRcpAntRepayLoanDtSlTemp>> index(QueryModel queryModel) {
        List<BatSRcpAntRepayLoanDtSlTemp> list = batSRcpAntRepayLoanDtSlTempService.selectByModel(queryModel);
        return new ResultDto<List<BatSRcpAntRepayLoanDtSlTemp>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSRcpAntRepayLoanDtSlTemp> create(@RequestBody BatSRcpAntRepayLoanDtSlTemp batSRcpAntRepayLoanDtSlTemp) throws URISyntaxException {
        batSRcpAntRepayLoanDtSlTempService.insert(batSRcpAntRepayLoanDtSlTemp);
        return new ResultDto<BatSRcpAntRepayLoanDtSlTemp>(batSRcpAntRepayLoanDtSlTemp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSRcpAntRepayLoanDtSlTemp batSRcpAntRepayLoanDtSlTemp) throws URISyntaxException {
        int result = batSRcpAntRepayLoanDtSlTempService.update(batSRcpAntRepayLoanDtSlTemp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String contractNo, String seqNo) {
        int result = batSRcpAntRepayLoanDtSlTempService.deleteByPrimaryKey(contractNo, seqNo);
        return new ResultDto<Integer>(result);
    }

}
