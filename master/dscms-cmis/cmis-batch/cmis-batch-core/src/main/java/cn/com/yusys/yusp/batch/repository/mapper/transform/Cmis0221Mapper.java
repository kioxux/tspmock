package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0221</br>
 * 加工任务-额度处理-将额度相关临时表更新到额度相关表</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0221Mapper {
    /**
     * 清空批复额度分项额度比对
     */
    void truncateLmtTmpAlsbiTcalsbi();

    /**
     * 插入批复额度分项额度比对
     *
     * @param openDay
     * @return
     */
    int insertLmtTmpAlsbiTcalsbi(@Param("openDay") String openDay);

    /**
     * 根据批复额度分项额度比对更新批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int updateByLmtTmpAlsbiTcalsbi(@Param("openDay") String openDay);

    /**
     * 清空分项占用关系信息比对
     */
    void truncateLmtTmpLcrTclcr();

    /**
     * 插入分项占用关系信息比对
     *
     * @param openDay
     * @return
     */
    int insertLmtTmpLcrTclcr(@Param("openDay") String openDay);

    /**
     * 根据分项占用关系信息比对更新分项占用关系信息
     *
     * @param openDay
     * @return
     */
    int updateByLmtTmpLcrTclcr(@Param("openDay") String openDay);


    /**
     * 删除-新信贷-临时表-额度通用临时表-
     */
    void deleteTmpTdcarAl();//add20211022

    /**
     * 插入-新信贷-临时表-额度通用临时表-
     * @param openDay
     * @return
     */
    int insertTmpTdcarAl(@Param("openDay") String openDay);//add20211022

    /**
     * 更新分项占用关系信息
     * @param openDay
     * @return
     */
    int updateLmtContRelLmt(@Param("openDay") String openDay);//add20211022

    /**
     * ComStar新做的业务插入lmt_cont_rel
     *
     * @param openDay
     * @return
     */
    int insertComstarLmtContRel(@Param("openDay") String openDay);


    /**
     * 更细分项占用关系信息
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel(@Param("openDay") String openDay);

    /**
     * 清空合同占用关系额度比对
     */
    void truncateLmtTmpCarTccar();

    /**
     * 插入合同占用关系额度比对
     *
     * @param openDay
     * @return
     */
    int insertLmtTmpCarTccar(@Param("openDay") String openDay);

    /**
     * 根据合同占用关系额度比对更新合同占用关系信息
     *
     * @param openDay
     * @return
     */
    int updateByLmtTmpCarTccar(@Param("openDay") String openDay);

    /**
     * 插入台账占用关系额度比对（更新状态）
     *
     * @param openDay
     * @return
     */
    int insertLmtTmpCarTccar2(@Param("openDay") String openDay);

    /**
     * 根据台账占用关系额度比对更新合同占用关系信息（更新状态）
     *
     * @param openDay
     * @return
     */
    int updateByLmtTmpCarTccar2(@Param("openDay") String openDay);

    /**
     * 根据合同占用关系额度比对更新合同占用关系信息
     *
     * @param openDay
     * @return
     */
    int updateByLmtTmpCarTccar20(@Param("openDay") String openDay);


    /**
     * 清空白名单额度信息额度比对
     */
    void truncateLmtTmpLwiTclwi();

    /**
     * 插入白名单额度信息额度比对
     *
     * @param openDay
     * @return
     */
    int insertLmtTmpLwiTclwi(@Param("openDay") String openDay);

    /**
     * 根据白名单额度信息额度比对更新白名单额度信息表
     *
     * @param openDay
     * @return
     */
    int updateByLmtTmpLwiTclwi(@Param("openDay") String openDay);

    /**
     * 清空合作方授信分项信息额度比对
     */
    void truncateLmtTmpAcsiTcacsi();

    /**
     * 插入合作方授信分项信息额度比对
     *
     * @param openDay
     * @return
     */
    int insertLmtTmpAcsiTcacsi(@Param("openDay") String openDay);

    /**
     * 根据合作方授信分项信息额度比对更新合作方授信分项信息
     *
     * @param openDay
     * @return
     */
    int updateByLmtTmpAcsiTcacsi(@Param("openDay") String openDay);
}
