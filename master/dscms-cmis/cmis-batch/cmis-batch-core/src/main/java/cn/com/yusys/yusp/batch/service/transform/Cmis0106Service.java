package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0105Mapper;
import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0106Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0105</br>
 * 任务名称：加工任务-业务处理-节假日  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0106Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0106Service.class);

    @Resource
    private Cmis0106Mapper cmis0106Mapper;

    /**
     * 插入节假日配置
     * @param openDay
     */
    public void cmis0106InsertWbHolidayCfg(String openDay) {
        logger.info("插入节假日配置开始,请求参数为:{}", openDay);
        int insertWbHolidayCfg = cmis0106Mapper.insertWbHolidayCfg(openDay);
        logger.info("插入节假日配置结束,返回参数为:{}", insertWbHolidayCfg);
    }

    /**
     * 更新节假日配置
     * @param openDay
     */
    public void cmis0106UpdateWbHolidayCfg(String openDay) {
        logger.info("插入节假日配置开始,请求参数为:{}", openDay);
        int updateWbHolidayCfg = cmis0106Mapper.updateWbHolidayCfg(openDay);
        logger.info("插入节假日配置结束,返回参数为:{}", updateWbHolidayCfg);
    }
}
