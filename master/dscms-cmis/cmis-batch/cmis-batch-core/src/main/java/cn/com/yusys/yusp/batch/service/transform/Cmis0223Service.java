package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0223Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0223</br>
 * 任务名称：加工任务-额度处理-将额度相关临时表全量插入到额度正式表</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0223Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0223Service.class);
    @Autowired
    private Cmis0223Mapper cmis0223Mapper;

    /**
     * 清空和插入合同占用关系信息
     *
     * @param openDay
     */
    public void cmis0223DeleteInsertContAccRel(String openDay) {
        logger.info("清空合同占用关系信息开始,请求参数为:[{}]", openDay);
        cmis0223Mapper.truncateContAccRel();
        logger.info("清空合同占用关系信息结束");

        logger.info("插入合同占用关系信息开始,请求参数为:[{}]", openDay);
        int insertContAccRel = cmis0223Mapper.insertContAccRel(openDay);
        logger.info("插入合同占用关系信息结束,返回参数为:[{}]", insertContAccRel);

    }

    /**
     * 清空和插入分项占用关系信息
     *
     * @param openDay
     */
    public void cmis0223DeleteInsertLmtContRel(String openDay) {
        logger.info("清空分项占用关系信息开始,请求参数为:[{}]", openDay);
        cmis0223Mapper.truncateLmtContRel();
        logger.info("清空分项占用关系信息结束");

        logger.info("插入分项占用关系信息开始,请求参数为:[{}]", openDay);
        int insertLmtContRel = cmis0223Mapper.insertLmtContRel(openDay);
        logger.info("插入分项占用关系信息结束,返回参数为:[{}]", insertLmtContRel);
    }

    /**
     * 批复额度分项基础信息中金额更新
     *
     * @param openDay
     */
    public void cmis0223UpdateApprLmtSubBasicInfo(String openDay) {
        logger.info("批复额度分项基础信息中金额更新开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo = cmis0223Mapper.updateApprLmtSubBasicInfo(openDay);
        logger.info("批复额度分项基础信息中金额更新结束,返回参数为:[{}]", updateApprLmtSubBasicInfo);
    }

    /**
     * 更新合作方授信分项信息
     *
     * @param openDay
     */
    public void cmis0223UpdateApprCoopSubInfo(String openDay) {
        logger.info("合作方授信分项信息中金额更新开始,请求参数为:[{}]", openDay);
        int updateApprCoopSubInfo = cmis0223Mapper.updateApprCoopSubInfo(openDay);
        logger.info("合作方授信分项信息中金额更新结束,返回参数为:[{}]", updateApprCoopSubInfo);
    }

    /**
     * 白名单额度信息中金额更新
     *
     * @param openDay
     */
    public void cmis0223UpdateLmtWhiteInfo(String openDay) {
        logger.info("白名单额度信息中金额更新开始,请求参数为:[{}]", openDay);
        int updateLmtWhiteInfo = cmis0223Mapper.updateLmtWhiteInfo(openDay);
        logger.info("白名单额度信息中金额更新结束,返回参数为:[{}]", updateLmtWhiteInfo);
    }
}
