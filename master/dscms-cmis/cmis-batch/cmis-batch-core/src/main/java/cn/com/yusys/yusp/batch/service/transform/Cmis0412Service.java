package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0412Mapper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-业务处理-个人经营性定期检查 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0412Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0412Service.class);

    @Autowired
    private Cmis0412Mapper cmis0412Mapper;

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     */
    public void cmis0412InsertPspTaskList(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList01 = cmis0412Mapper.insertPspTaskList01(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList01);

        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList02 = cmis0412Mapper.insertPspTaskList02(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList02);

        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList03 = cmis0412Mapper.insertPspTaskList03(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList03);

        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList04 = cmis0412Mapper.insertPspTaskList04(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList04);

        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList05 = cmis0412Mapper.insertPspTaskList05(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList05);


        logger.info("删除同一天客户产生多个任务，保留这一天最早的日终产生的任务 开始,请求参数为:[{}]", openDay);
        int deletetmpPspTastListForPerCusMore = cmis0412Mapper.deletetmpPspTastListForPerCusMore(openDay);
        logger.info("删除同一天客户产生多个任务，保留这一天最早的日终产生的任务 结束,返回参数为:[{}]", deletetmpPspTastListForPerCusMore);

    }
}
