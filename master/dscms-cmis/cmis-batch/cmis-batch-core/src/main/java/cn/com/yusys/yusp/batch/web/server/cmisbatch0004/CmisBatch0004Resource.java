package cn.com.yusys.yusp.batch.web.server.cmisbatch0004;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004RespDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0004.CmisBatch0004Service;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询[贷款账户主表]和[贷款账户还款表]关联信息
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CMISBATCH0004:查询[贷款账户主表]和[贷款账户还款表]关联信息")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0004Resource.class);

    @Autowired
    private CmisBatch0004Service cmisBatch0004Service;

    /**
     * 交易码：cmisbatch0004
     * 交易描述：查询[贷款账户主表]和[贷款账户还款表]关联信息
     *
     * @param cmisbatch0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询[贷款账户主表]和[贷款账户还款表]关联信息")
    @PostMapping("/cmisbatch0004")
    protected @ResponseBody
    ResultDto<Cmisbatch0004RespDto> cmisbatch0004(@Validated @RequestBody Cmisbatch0004ReqDto cmisbatch0004ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value, JSON.toJSONString(cmisbatch0004ReqDto));
        Cmisbatch0004RespDto cmisbatch0004RespDto = new Cmisbatch0004RespDto();// 响应Dto:查询[贷款账户主表]和[贷款账户还款表]关联信息
        ResultDto<Cmisbatch0004RespDto> cmisbatch0004ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0004ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value, JSON.toJSONString(cmisbatch0004ReqDto));
            cmisbatch0004RespDto = cmisBatch0004Service.cmisBatch0004(cmisbatch0004ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value, JSON.toJSONString(cmisbatch0004RespDto));
            // 封装cmisbatch0004ResultDto中正确的返回码和返回信息
            cmisbatch0004ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0004ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value, e.getMessage());
            // 封装cmisbatch0004ResultDto中异常返回码和返回信息
            cmisbatch0004ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0004ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0004RespDto到cmisbatch0004ResultDto中
        cmisbatch0004ResultDto.setData(cmisbatch0004RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value, JSON.toJSONString(cmisbatch0004ResultDto));
        return cmisbatch0004ResultDto;
    }
}
