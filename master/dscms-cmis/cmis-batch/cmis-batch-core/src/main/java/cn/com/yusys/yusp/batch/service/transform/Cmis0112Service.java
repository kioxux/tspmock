package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0112Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0112</br>
 * 任务名称：加工任务-业务处理-风险提醒生成 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0112Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0112Service.class);

    @Autowired
    private Cmis0112Mapper cmis0112Mapper;

    /**
     * 插入风险提示表
     *
     * @param openDay
     */
    public void cmis0112InsertBatBizRiskNotice(String openDay) {
        logger.info("授信到期提醒开始");
        logger.info("清空数据到贷款借据号和最大期供逾期天数关系表 开始,请求参数为:[{}]", openDay);
        cmis0112Mapper.cmis0112TruncateTmp();
        logger.info("清空数据到贷款借据号和最大期供逾期天数关系表 结束");
        logger.info("单一客户授信到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice01 = cmis0112Mapper.insertBatBizRiskNotice01(openDay);
        logger.info("单一客户授信到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice01);
        logger.info("同业客户授信到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice02 = cmis0112Mapper.insertBatBizRiskNotice02(openDay);
        logger.info("同业客户授信到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice02);
        logger.info("资金业务授信到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice03 = cmis0112Mapper.insertBatBizRiskNotice03(openDay);
        logger.info("资金业务授信到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice03);
        logger.info("小微客户授信到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice04 = cmis0112Mapper.insertBatBizRiskNotice04(openDay);
        logger.info("小微客户授信到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice04);
        logger.info("授信到期提醒结束");

        logger.info("用信到期提醒开始");
        logger.info("贷款台账授信到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice05 = cmis0112Mapper.insertBatBizRiskNotice05(openDay);
        logger.info("贷款台账授信到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice05);
        logger.info("开证台账到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice06 = cmis0112Mapper.insertBatBizRiskNotice06(openDay);
        logger.info("开证台账到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice06);
        logger.info("银承台账到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice07 = cmis0112Mapper.insertBatBizRiskNotice07(openDay);
        logger.info("银承台账到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice07);
        logger.info("保函台账到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice08 = cmis0112Mapper.insertBatBizRiskNotice08(openDay);
        logger.info("保函台账到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice08);
        logger.info("贴现台账到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice09 = cmis0112Mapper.insertBatBizRiskNotice09(openDay);
        logger.info("贴现台账到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice09);
        logger.info("委托贷款台账到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice10 = cmis0112Mapper.insertBatBizRiskNotice10(openDay);
        logger.info("委托贷款台账到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice10);
        logger.info("用信到期提醒结束");

        logger.info("合同到期提醒开始");
        logger.info("贴现协议到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice11 = cmis0112Mapper.insertBatBizRiskNotice11(openDay);
        logger.info("贴现协议到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice11);
        logger.info("开证合同到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice12 = cmis0112Mapper.insertBatBizRiskNotice12(openDay);
        logger.info("开证合同到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice12);
        logger.info("银承合同到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice13 = cmis0112Mapper.insertBatBizRiskNotice13(openDay);
        logger.info("银承合同到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice13);
        logger.info("保函合同到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice14 = cmis0112Mapper.insertBatBizRiskNotice14(openDay);
        logger.info("保函合同到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice14);
        logger.info("委托贷款合同到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice15 = cmis0112Mapper.insertBatBizRiskNotice15(openDay);
        logger.info("委托贷款合同到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice15);
        logger.info("担保合同到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice16 = cmis0112Mapper.insertBatBizRiskNotice16(openDay);
        logger.info("担保合同到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice16);
        logger.info("最高额授信协议到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice17 = cmis0112Mapper.insertBatBizRiskNotice17(openDay);
        logger.info("最高额授信协议到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice17);
        logger.info("普通贷款合同、贸易融资合同、福费廷合同到期提醒开始,请求参数为:[{}]", openDay);
        int insertBatBizRiskNotice18 = cmis0112Mapper.insertBatBizRiskNotice18(openDay);
        logger.info("普通贷款合同、贸易融资合同、福费廷合同到期提醒结束,返回参数为:[{}]", insertBatBizRiskNotice18);
        logger.info("合同到期提醒结束");
    }
}
