/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.web.rest.job;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.spring.batch.domain.job.BatchJobExecutionParams;
import cn.com.yusys.yusp.spring.batch.domain.job.BatchJobInstance;
import cn.com.yusys.yusp.spring.batch.service.job.BatchJobExecutionParamsService;
import cn.com.yusys.yusp.spring.batch.service.job.BatchJobInstanceService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobInstanceResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batchjobinstance")
public class BatchJobInstanceResource {
    private static final Logger logger = LoggerFactory.getLogger(BatchJobInstanceResource.class);
    @Resource
    private JobLauncher jobLauncher;
    @Resource
    private ApplicationContext applicationContext;
    @Autowired
    private BatchJobInstanceService batchJobInstanceService;
    @Autowired
    private BatchJobExecutionParamsService batchJobExecutionParamsService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatchJobInstance>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatchJobInstance> list = batchJobInstanceService.selectAll(queryModel);
        return new ResultDto<List<BatchJobInstance>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatchJobInstance>> index(QueryModel queryModel) {
        List<BatchJobInstance> list = batchJobInstanceService.selectByModel(queryModel);
        return new ResultDto<List<BatchJobInstance>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<Map<String, Object>>> query(@RequestBody QueryModel queryModel) {
        List<Map<String, Object>> batchInfos = new ArrayList<>();
        ResultDto<List<Map<String, Object>>> resultDto = new ResultDto<>();
        batchInfos = batchJobInstanceService.getBatchInfo(queryModel);
        batchInfos.forEach((info -> {
            info.put("bjeStartTime", DateUtils.formatDate((Date) info.get("bjeStartTime"), "yyyy-MM-dd HH:mm:ss"));
            info.put("bjeCreateTime", DateUtils.formatDate((Date) info.get("bjeCreateTime"), "yyyy-MM-dd  HH:mm:ss"));
            info.put("bjeEndTime", DateUtils.formatDate((Date) info.get("bjeEndTime"), "yyyy-MM-dd  HH:mm:ss"));
            info.put("bseStartTime", DateUtils.formatDate((Date) info.get("bseStartTime"), "yyyy-MM-dd HH:mm:ss"));
            info.put("bseEndTime", DateUtils.formatDate((Date) info.get("bseEndTime"), "yyyy-MM-dd  HH:mm:ss"));
        }));
        resultDto = new ResultDto<>(batchInfos);
        return resultDto;
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{jobInstanceId}")
    protected ResultDto<BatchJobInstance> show(@PathVariable("jobInstanceId") Long jobInstanceId) {
        BatchJobInstance batchJobInstance = batchJobInstanceService.selectByPrimaryKey(jobInstanceId);
        return new ResultDto<BatchJobInstance>(batchJobInstance);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatchJobInstance> create(@RequestBody BatchJobInstance batchJobInstance) throws URISyntaxException {
        batchJobInstanceService.insert(batchJobInstance);
        return new ResultDto<BatchJobInstance>(batchJobInstance);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatchJobInstance batchJobInstance) throws URISyntaxException {
        int result = batchJobInstanceService.update(batchJobInstance);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{jobInstanceId}")
    protected ResultDto<Integer> delete(@PathVariable("jobInstanceId") Long jobInstanceId) {
        int result = batchJobInstanceService.deleteByPrimaryKey(jobInstanceId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batchJobInstanceService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 重新执行和续跑
     *
     * @param jobExecutionId 作业执行器ID
     * @param jobName        作业名称
     * @param flag           重新执行和续跑标志(0-重新执行,1-续跑)
     * @return
     */
    @PostMapping("/reExecute/{jobExecutionId}/{jobName}/{flag}")
    protected ResultDto<Integer> reExecute(@PathVariable("jobExecutionId") String jobExecutionId, @PathVariable("jobName") String jobName, @PathVariable("flag") String flag) {
        logger.info("重新执行和续跑开始:作业执行器ID为[{}],作业名称为[{}],重新执行和续跑标志(0-重新执行,1-续跑)为[{}]", jobExecutionId, jobName, flag);
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("jobExecutionId", jobExecutionId);
        logger.info("根据作业执行器ID为[{}]查询作业步执行参数管理开始,请求参数为:[{}]", jobExecutionId, JSON.toJSONString(queryModel));
        List<BatchJobExecutionParams> batchJobExecutionParamsList = batchJobExecutionParamsService.selectAll(queryModel);
        logger.info("根据作业执行器ID为[{}]查询作业步执行参数管理结束,返回参数为:[{}]", jobExecutionId, JSON.toJSONString(batchJobExecutionParamsList));
        Job job = (Job) applicationContext.getBean(jobName);
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        batchJobExecutionParamsList.forEach(batchJobExecutionParams -> {
            jobParametersBuilder.addString(batchJobExecutionParams.getKeyName(), batchJobExecutionParams.getStringVal());
        });
        if ("0".equals(flag)) {
            logger.info("重新执行和续跑开始:作业执行器ID为[{}],作业名称为[{}],重新执行和续跑标志(0-重新执行,1-续跑)为[{}],执行时间为:[{}]", jobExecutionId, jobName, flag, DateUtils.formatDateTimeByDef());
            jobParametersBuilder.addString("reExecuteTime", DateUtils.formatDateTimeByDef());
        }
        try {
            JobExecution jobExecution = jobLauncher.run(job, jobParametersBuilder.toJobParameters());
        } catch (Exception e) {
            logger.info("重新执行和续跑:作业执行器ID为[{}],作业名称为[{}],重新执行和续跑标志(0-重新执行,1-续跑)为[{}]的异常信息为:[{}]", jobExecutionId, jobName, flag, e.getMessage());
        }
        logger.info("重新执行和续跑结束:作业执行器ID为[{}],作业名称为[{}],重新执行和续跑标志(0-重新执行,1-续跑)为[{}]", jobExecutionId, jobName, flag);
        return new ResultDto<Integer>();
    }
}
