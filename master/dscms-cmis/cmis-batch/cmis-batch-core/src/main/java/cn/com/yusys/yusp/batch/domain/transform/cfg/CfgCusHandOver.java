/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.transform.cfg;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: CfgCusHandOver
 * @类描述: cfg_cus_hand_over数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-22 20:25:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_cus_hand_over")
public class CfgCusHandOver extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 客户移交主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 客户移交方式 STD_RZ_CUS_MOVE **/
	@Column(name = "HANDOVER_CUS_MOVE", unique = false, nullable = false, length = 3)
	private String handoverCusMove;
	
	/** 客户移交数据库中文名 **/
	@Column(name = "HANDOVER_DB_CN_NAME", unique = false, nullable = true, length = 100)
	private String handoverDbCnName;
	
	/** 客户移交数据库英文名 **/
	@Column(name = "HANDOVER_DB_EN_NAME", unique = false, nullable = true, length = 50)
	private String handoverDbEnName;
	
	/** 客户移交表中文名 **/
	@Column(name = "HANDOVER_TABLE_CN_NAME", unique = false, nullable = true, length = 100)
	private String handoverTableCnName;
	
	/** 客户移交表英文名 **/
	@Column(name = "HANDOVER_TABLE_EN_NAME", unique = false, nullable = true, length = 50)
	private String handoverTableEnName;
	
	/** 待更新管户客户经理ID **/
	@Column(name = "UPDATE_MANAGER_ID", unique = false, nullable = true, length = 20)
	private String updateManagerId;
	
	/** 待更新管户机构ID **/
	@Column(name = "UPDATE_MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String updateManagerBrId;
	
	/** 客户移交条件字段 **/
	@Column(name = "CONDITION_FIELD", unique = false, nullable = true, length = 80)
	private String conditionField;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param handoverCusMove
	 */
	public void setHandoverCusMove(String handoverCusMove) {
		this.handoverCusMove = handoverCusMove;
	}
	
    /**
     * @return handoverCusMove
     */
	public String getHandoverCusMove() {
		return this.handoverCusMove;
	}
	
	/**
	 * @param handoverDbCnName
	 */
	public void setHandoverDbCnName(String handoverDbCnName) {
		this.handoverDbCnName = handoverDbCnName;
	}
	
    /**
     * @return handoverDbCnName
     */
	public String getHandoverDbCnName() {
		return this.handoverDbCnName;
	}
	
	/**
	 * @param handoverDbEnName
	 */
	public void setHandoverDbEnName(String handoverDbEnName) {
		this.handoverDbEnName = handoverDbEnName;
	}
	
    /**
     * @return handoverDbEnName
     */
	public String getHandoverDbEnName() {
		return this.handoverDbEnName;
	}
	
	/**
	 * @param handoverTableCnName
	 */
	public void setHandoverTableCnName(String handoverTableCnName) {
		this.handoverTableCnName = handoverTableCnName;
	}
	
    /**
     * @return handoverTableCnName
     */
	public String getHandoverTableCnName() {
		return this.handoverTableCnName;
	}
	
	/**
	 * @param handoverTableEnName
	 */
	public void setHandoverTableEnName(String handoverTableEnName) {
		this.handoverTableEnName = handoverTableEnName;
	}
	
    /**
     * @return handoverTableEnName
     */
	public String getHandoverTableEnName() {
		return this.handoverTableEnName;
	}
	
	/**
	 * @param updateManagerId
	 */
	public void setUpdateManagerId(String updateManagerId) {
		this.updateManagerId = updateManagerId;
	}
	
    /**
     * @return updateManagerId
     */
	public String getUpdateManagerId() {
		return this.updateManagerId;
	}
	
	/**
	 * @param updateManagerBrId
	 */
	public void setUpdateManagerBrId(String updateManagerBrId) {
		this.updateManagerBrId = updateManagerBrId;
	}
	
    /**
     * @return updateManagerBrId
     */
	public String getUpdateManagerBrId() {
		return this.updateManagerBrId;
	}
	
	/**
	 * @param conditionField
	 */
	public void setConditionField(String conditionField) {
		this.conditionField = conditionField;
	}
	
    /**
     * @return conditionField
     */
	public String getConditionField() {
		return this.conditionField;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}