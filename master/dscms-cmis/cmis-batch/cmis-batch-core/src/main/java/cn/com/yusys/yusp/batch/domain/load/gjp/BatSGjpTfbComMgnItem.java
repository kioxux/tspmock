/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.gjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGjpTfbComMgnItem
 * @类描述: bat_s_gjp_tfb_com_mgn_item数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-11-15 16:53:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_gjp_tfb_com_mgn_item")
public class BatSGjpTfbComMgnItem extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 系统实体编号 **/
	@Column(name = "SYS_ENTY_ID", unique = false, nullable = true, length = 32)
	private String sysEntyId;
	
	/** 系统删除标识 **/
	@Column(name = "SYS_DEL_FLG", unique = false, nullable = true, length = 1)
	private String sysDelFlg;
	
	/** 创建人 **/
	@Column(name = "SYS_CRT_USER", unique = false, nullable = true, length = 32)
	private String sysCrtUser;
	
	/** 创建日期 **/
	@Column(name = "SYS_CRT_DT", unique = false, nullable = true, length = 20)
	private String sysCrtDt;
	
	/** 修改人编号 **/
	@Column(name = "SYS_MODIFY_USER", unique = false, nullable = true, length = 32)
	private String sysModifyUser;
	
	/** 修改日期 **/
	@Column(name = "SYS_MODIFY_DT", unique = false, nullable = true, length = 20)
	private String sysModifyDt;
	
	/** 主表ID **/
	@Column(name = "REG_ID", unique = false, nullable = true, length = 32)
	private String regId;
	
	/** 保证金母账户 **/
	@Column(name = "MGN_ACCT", unique = false, nullable = true, length = 32)
	private String mgnAcct;
	
	/** 保证金币种 **/
	@Column(name = "MGN_CCY", unique = false, nullable = true, length = 3)
	private String mgnCcy;
	
	/** 保证金金额 **/
	@Column(name = "MGN_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal mgnAmt;
	
	/** 起息日期 **/
	@Column(name = "VAL_DT", unique = false, nullable = true, length = 8)
	private String valDt;
	
	/** 到期日期 **/
	@Column(name = "LMT_DT", unique = false, nullable = true, length = 8)
	private String lmtDt;
	
	/** 帐户性质 **/
	@Column(name = "ACCT_TYPE", unique = false, nullable = true, length = 4)
	private String acctType;
	
	/** 是FALSE使用圈存 **/
	@Column(name = "BLOK_FLG", unique = false, nullable = true, length = 1)
	private String blokFlg;
	
	/** 圈存号 **/
	@Column(name = "BLOK_NO", unique = false, nullable = true, length = 35)
	private String blokNo;
	
	/** 信贷牌价 **/
	@Column(name = "MMS_RATE", unique = false, nullable = true, length = 14)
	private String mmsRate;
	
	/** 保证金编号 **/
	@Column(name = "MGN_NO", unique = false, nullable = true, length = 32)
	private String mgnNo;
	
	/** 是FALSE顺延 **/
	@Column(name = "MGN_AT_EXTEND", unique = false, nullable = true, length = 1)
	private String mgnAtExtend;
	
	/** 引用顺序 **/
	@Column(name = "RCP_SEQ_NO", unique = false, nullable = true, length = 10)
	private Integer rcpSeqNo;
	
	/** 系统业务编号 **/
	@Column(name = "SYS_REF_NO", unique = false, nullable = true, length = 32)
	private String sysRefNo;
	
	/** 保证金母户子账户序号 **/
	@Column(name = "MGN_SUB_ACCT", unique = false, nullable = true, length = 32)
	private String mgnSubAcct;
	
	/** 保证金子账号 **/
	@Column(name = "MGN_CUST_NO", unique = false, nullable = true, length = 32)
	private String mgnCustNo;
	
	/** 保证金子账号序号 **/
	@Column(name = "MGN_SUB_NO", unique = false, nullable = true, length = 32)
	private String mgnSubNo;
	
	/** 操作类型：03-引用??04-释放 **/
	@Column(name = "OP_FLAG", unique = false, nullable = true, length = 8)
	private String opFlag;
	
	/** 定期活期标志：01-活期02-定期 **/
	@Column(name = "MGN_RT_TYPE", unique = false, nullable = true, length = 8)
	private String mgnRtType;
	
	/** 期限01-一个月,02-三个月,03-六个月,04-一年,05-两年,06-三年,07-五年 **/
	@Column(name = "TENOR_PROP", unique = false, nullable = true, length = 8)
	private String tenorProp;
	
	/** 交易ID **/
	@Column(name = "BIZ_ID", unique = false, nullable = true, length = 32)
	private String bizId;
	
	/** 是否已经保证金开户 **/
	@Column(name = "DEPOSIT_OPEN_FLG", unique = false, nullable = true, length = 1)
	private String depositOpenFlg;
	
	/** 待清算子户 **/
	@Column(name = "ACC_NO", unique = false, nullable = true, length = 40)
	private String accNo;
	
	/** 待清算户序号 **/
	@Column(name = "SUB_ACC_NO", unique = false, nullable = true, length = 40)
	private String subAccNo;
	
	/** 保证金开户核心返回的流水号 **/
	@Column(name = "SER_SEQ_NO", unique = false, nullable = true, length = 32)
	private String serSeqNo;
	
	/** 本息转入帐号 **/
	@Column(name = "PRI_INTR_ACCTNO", unique = false, nullable = true, length = 40)
	private String priIntrAcctno;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param sysEntyId
	 */
	public void setSysEntyId(String sysEntyId) {
		this.sysEntyId = sysEntyId;
	}
	
    /**
     * @return sysEntyId
     */
	public String getSysEntyId() {
		return this.sysEntyId;
	}
	
	/**
	 * @param sysDelFlg
	 */
	public void setSysDelFlg(String sysDelFlg) {
		this.sysDelFlg = sysDelFlg;
	}
	
    /**
     * @return sysDelFlg
     */
	public String getSysDelFlg() {
		return this.sysDelFlg;
	}
	
	/**
	 * @param sysCrtUser
	 */
	public void setSysCrtUser(String sysCrtUser) {
		this.sysCrtUser = sysCrtUser;
	}
	
    /**
     * @return sysCrtUser
     */
	public String getSysCrtUser() {
		return this.sysCrtUser;
	}
	
	/**
	 * @param sysCrtDt
	 */
	public void setSysCrtDt(String sysCrtDt) {
		this.sysCrtDt = sysCrtDt;
	}
	
    /**
     * @return sysCrtDt
     */
	public String getSysCrtDt() {
		return this.sysCrtDt;
	}
	
	/**
	 * @param sysModifyUser
	 */
	public void setSysModifyUser(String sysModifyUser) {
		this.sysModifyUser = sysModifyUser;
	}
	
    /**
     * @return sysModifyUser
     */
	public String getSysModifyUser() {
		return this.sysModifyUser;
	}
	
	/**
	 * @param sysModifyDt
	 */
	public void setSysModifyDt(String sysModifyDt) {
		this.sysModifyDt = sysModifyDt;
	}
	
    /**
     * @return sysModifyDt
     */
	public String getSysModifyDt() {
		return this.sysModifyDt;
	}
	
	/**
	 * @param regId
	 */
	public void setRegId(String regId) {
		this.regId = regId;
	}
	
    /**
     * @return regId
     */
	public String getRegId() {
		return this.regId;
	}
	
	/**
	 * @param mgnAcct
	 */
	public void setMgnAcct(String mgnAcct) {
		this.mgnAcct = mgnAcct;
	}
	
    /**
     * @return mgnAcct
     */
	public String getMgnAcct() {
		return this.mgnAcct;
	}
	
	/**
	 * @param mgnCcy
	 */
	public void setMgnCcy(String mgnCcy) {
		this.mgnCcy = mgnCcy;
	}
	
    /**
     * @return mgnCcy
     */
	public String getMgnCcy() {
		return this.mgnCcy;
	}
	
	/**
	 * @param mgnAmt
	 */
	public void setMgnAmt(java.math.BigDecimal mgnAmt) {
		this.mgnAmt = mgnAmt;
	}
	
    /**
     * @return mgnAmt
     */
	public java.math.BigDecimal getMgnAmt() {
		return this.mgnAmt;
	}
	
	/**
	 * @param valDt
	 */
	public void setValDt(String valDt) {
		this.valDt = valDt;
	}
	
    /**
     * @return valDt
     */
	public String getValDt() {
		return this.valDt;
	}
	
	/**
	 * @param lmtDt
	 */
	public void setLmtDt(String lmtDt) {
		this.lmtDt = lmtDt;
	}
	
    /**
     * @return lmtDt
     */
	public String getLmtDt() {
		return this.lmtDt;
	}
	
	/**
	 * @param acctType
	 */
	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}
	
    /**
     * @return acctType
     */
	public String getAcctType() {
		return this.acctType;
	}
	
	/**
	 * @param blokFlg
	 */
	public void setBlokFlg(String blokFlg) {
		this.blokFlg = blokFlg;
	}
	
    /**
     * @return blokFlg
     */
	public String getBlokFlg() {
		return this.blokFlg;
	}
	
	/**
	 * @param blokNo
	 */
	public void setBlokNo(String blokNo) {
		this.blokNo = blokNo;
	}
	
    /**
     * @return blokNo
     */
	public String getBlokNo() {
		return this.blokNo;
	}
	
	/**
	 * @param mmsRate
	 */
	public void setMmsRate(String mmsRate) {
		this.mmsRate = mmsRate;
	}
	
    /**
     * @return mmsRate
     */
	public String getMmsRate() {
		return this.mmsRate;
	}
	
	/**
	 * @param mgnNo
	 */
	public void setMgnNo(String mgnNo) {
		this.mgnNo = mgnNo;
	}
	
    /**
     * @return mgnNo
     */
	public String getMgnNo() {
		return this.mgnNo;
	}
	
	/**
	 * @param mgnAtExtend
	 */
	public void setMgnAtExtend(String mgnAtExtend) {
		this.mgnAtExtend = mgnAtExtend;
	}
	
    /**
     * @return mgnAtExtend
     */
	public String getMgnAtExtend() {
		return this.mgnAtExtend;
	}
	
	/**
	 * @param rcpSeqNo
	 */
	public void setRcpSeqNo(Integer rcpSeqNo) {
		this.rcpSeqNo = rcpSeqNo;
	}
	
    /**
     * @return rcpSeqNo
     */
	public Integer getRcpSeqNo() {
		return this.rcpSeqNo;
	}
	
	/**
	 * @param sysRefNo
	 */
	public void setSysRefNo(String sysRefNo) {
		this.sysRefNo = sysRefNo;
	}
	
    /**
     * @return sysRefNo
     */
	public String getSysRefNo() {
		return this.sysRefNo;
	}
	
	/**
	 * @param mgnSubAcct
	 */
	public void setMgnSubAcct(String mgnSubAcct) {
		this.mgnSubAcct = mgnSubAcct;
	}
	
    /**
     * @return mgnSubAcct
     */
	public String getMgnSubAcct() {
		return this.mgnSubAcct;
	}
	
	/**
	 * @param mgnCustNo
	 */
	public void setMgnCustNo(String mgnCustNo) {
		this.mgnCustNo = mgnCustNo;
	}
	
    /**
     * @return mgnCustNo
     */
	public String getMgnCustNo() {
		return this.mgnCustNo;
	}
	
	/**
	 * @param mgnSubNo
	 */
	public void setMgnSubNo(String mgnSubNo) {
		this.mgnSubNo = mgnSubNo;
	}
	
    /**
     * @return mgnSubNo
     */
	public String getMgnSubNo() {
		return this.mgnSubNo;
	}
	
	/**
	 * @param opFlag
	 */
	public void setOpFlag(String opFlag) {
		this.opFlag = opFlag;
	}
	
    /**
     * @return opFlag
     */
	public String getOpFlag() {
		return this.opFlag;
	}
	
	/**
	 * @param mgnRtType
	 */
	public void setMgnRtType(String mgnRtType) {
		this.mgnRtType = mgnRtType;
	}
	
    /**
     * @return mgnRtType
     */
	public String getMgnRtType() {
		return this.mgnRtType;
	}
	
	/**
	 * @param tenorProp
	 */
	public void setTenorProp(String tenorProp) {
		this.tenorProp = tenorProp;
	}
	
    /**
     * @return tenorProp
     */
	public String getTenorProp() {
		return this.tenorProp;
	}
	
	/**
	 * @param bizId
	 */
	public void setBizId(String bizId) {
		this.bizId = bizId;
	}
	
    /**
     * @return bizId
     */
	public String getBizId() {
		return this.bizId;
	}
	
	/**
	 * @param depositOpenFlg
	 */
	public void setDepositOpenFlg(String depositOpenFlg) {
		this.depositOpenFlg = depositOpenFlg;
	}
	
    /**
     * @return depositOpenFlg
     */
	public String getDepositOpenFlg() {
		return this.depositOpenFlg;
	}
	
	/**
	 * @param accNo
	 */
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	
    /**
     * @return accNo
     */
	public String getAccNo() {
		return this.accNo;
	}
	
	/**
	 * @param subAccNo
	 */
	public void setSubAccNo(String subAccNo) {
		this.subAccNo = subAccNo;
	}
	
    /**
     * @return subAccNo
     */
	public String getSubAccNo() {
		return this.subAccNo;
	}
	
	/**
	 * @param serSeqNo
	 */
	public void setSerSeqNo(String serSeqNo) {
		this.serSeqNo = serSeqNo;
	}
	
    /**
     * @return serSeqNo
     */
	public String getSerSeqNo() {
		return this.serSeqNo;
	}
	
	/**
	 * @param priIntrAcctno
	 */
	public void setPriIntrAcctno(String priIntrAcctno) {
		this.priIntrAcctno = priIntrAcctno;
	}
	
    /**
     * @return priIntrAcctno
     */
	public String getPriIntrAcctno() {
		return this.priIntrAcctno;
	}


}