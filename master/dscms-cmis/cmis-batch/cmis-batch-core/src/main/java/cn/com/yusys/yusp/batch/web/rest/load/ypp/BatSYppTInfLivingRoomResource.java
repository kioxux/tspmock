/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.ypp;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.ypp.BatSYppTInfLivingRoom;
import cn.com.yusys.yusp.batch.service.load.ypp.BatSYppTInfLivingRoomService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSYppTInfLivingRoomResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 17:10:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsypptinflivingroom")
public class BatSYppTInfLivingRoomResource {
    @Autowired
    private BatSYppTInfLivingRoomService batSYppTInfLivingRoomService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSYppTInfLivingRoom>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSYppTInfLivingRoom> list = batSYppTInfLivingRoomService.selectAll(queryModel);
        return new ResultDto<List<BatSYppTInfLivingRoom>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSYppTInfLivingRoom>> index(QueryModel queryModel) {
        List<BatSYppTInfLivingRoom> list = batSYppTInfLivingRoomService.selectByModel(queryModel);
        return new ResultDto<List<BatSYppTInfLivingRoom>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{guarNo}")
    protected ResultDto<BatSYppTInfLivingRoom> show(@PathVariable("guarNo") String guarNo) {
        BatSYppTInfLivingRoom batSYppTInfLivingRoom = batSYppTInfLivingRoomService.selectByPrimaryKey(guarNo);
        return new ResultDto<BatSYppTInfLivingRoom>(batSYppTInfLivingRoom);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSYppTInfLivingRoom> create(@RequestBody BatSYppTInfLivingRoom batSYppTInfLivingRoom) throws URISyntaxException {
        batSYppTInfLivingRoomService.insert(batSYppTInfLivingRoom);
        return new ResultDto<BatSYppTInfLivingRoom>(batSYppTInfLivingRoom);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSYppTInfLivingRoom batSYppTInfLivingRoom) throws URISyntaxException {
        int result = batSYppTInfLivingRoomService.update(batSYppTInfLivingRoom);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{guarNo}")
    protected ResultDto<Integer> delete(@PathVariable("guarNo") String guarNo) {
        int result = batSYppTInfLivingRoomService.deleteByPrimaryKey(guarNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSYppTInfLivingRoomService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
