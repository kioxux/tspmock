/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.pjp;

import cn.com.yusys.yusp.batch.domain.load.pjp.BatTPjpBtDiscountBatch;
import cn.com.yusys.yusp.batch.service.load.pjp.BatTPjpBtDiscountBatchService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTPjpBtDiscountBatchResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-15 16:47:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battpjpbtdiscountbatch")
public class BatTPjpBtDiscountBatchResource {
    @Autowired
    private BatTPjpBtDiscountBatchService batTPjpBtDiscountBatchService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTPjpBtDiscountBatch>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTPjpBtDiscountBatch> list = batTPjpBtDiscountBatchService.selectAll(queryModel);
        return new ResultDto<List<BatTPjpBtDiscountBatch>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTPjpBtDiscountBatch>> index(QueryModel queryModel) {
        List<BatTPjpBtDiscountBatch> list = batTPjpBtDiscountBatchService.selectByModel(queryModel);
        return new ResultDto<List<BatTPjpBtDiscountBatch>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{discBatchId}")
    protected ResultDto<BatTPjpBtDiscountBatch> show(@PathVariable("discBatchId") String discBatchId) {
        BatTPjpBtDiscountBatch batTPjpBtDiscountBatch = batTPjpBtDiscountBatchService.selectByPrimaryKey(discBatchId);
        return new ResultDto<BatTPjpBtDiscountBatch>(batTPjpBtDiscountBatch);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTPjpBtDiscountBatch> create(@RequestBody BatTPjpBtDiscountBatch batTPjpBtDiscountBatch) throws URISyntaxException {
        batTPjpBtDiscountBatchService.insert(batTPjpBtDiscountBatch);
        return new ResultDto<BatTPjpBtDiscountBatch>(batTPjpBtDiscountBatch);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTPjpBtDiscountBatch batTPjpBtDiscountBatch) throws URISyntaxException {
        int result = batTPjpBtDiscountBatchService.update(batTPjpBtDiscountBatch);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{discBatchId}")
    protected ResultDto<Integer> delete(@PathVariable("discBatchId") String discBatchId) {
        int result = batTPjpBtDiscountBatchService.deleteByPrimaryKey(discBatchId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batTPjpBtDiscountBatchService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
