package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;


/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-贷后管理-投后定期检查 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0416Mapper {

    /**
     * 插入贷后检查任务表
     * @param openDay
     * @return
     */
    int insertPspTaskList01(@Param("openDay")String openDay);

    /**
     * 插入贷后检查任务表
     * @param openDay
     * @return
     */
    int insertPspTaskList02(@Param("openDay")String openDay);

    /**
     * 插入同业申请表
     * @param openDay
     * @return
     */
    int insertIntbankAdmit(@Param("openDay")String openDay);

    /**
     * 更新同业申请表
     * @param openDay
     * @return
     */
    int uptIntbankAdmit01(@Param("openDay")String openDay);

    /**
     * 更新同业申请表
     * @param openDay
     * @return
     */
    int uptIntbankAdmit02(@Param("openDay")String openDay);
}
