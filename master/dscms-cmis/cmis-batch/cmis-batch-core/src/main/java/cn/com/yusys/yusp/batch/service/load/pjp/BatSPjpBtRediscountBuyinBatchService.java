/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.pjp;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.pjp.BatSPjpBtRediscountBuyinBatch;
import cn.com.yusys.yusp.batch.repository.mapper.load.pjp.BatSPjpBtRediscountBuyinBatchMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtRediscountBuyinBatchService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-03 22:08:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSPjpBtRediscountBuyinBatchService {
    private static final Logger logger = LoggerFactory.getLogger(BatSPjpBtRediscountBuyinBatchService.class);
    @Autowired
    private BatSPjpBtRediscountBuyinBatchMapper batSPjpBtRediscountBuyinBatchMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSPjpBtRediscountBuyinBatch selectByPrimaryKey(String rediscBuyBatchId) {
        return batSPjpBtRediscountBuyinBatchMapper.selectByPrimaryKey(rediscBuyBatchId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSPjpBtRediscountBuyinBatch> selectAll(QueryModel model) {
        List<BatSPjpBtRediscountBuyinBatch> records = (List<BatSPjpBtRediscountBuyinBatch>) batSPjpBtRediscountBuyinBatchMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSPjpBtRediscountBuyinBatch> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSPjpBtRediscountBuyinBatch> list = batSPjpBtRediscountBuyinBatchMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSPjpBtRediscountBuyinBatch record) {
        return batSPjpBtRediscountBuyinBatchMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSPjpBtRediscountBuyinBatch record) {
        return batSPjpBtRediscountBuyinBatchMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSPjpBtRediscountBuyinBatch record) {
        return batSPjpBtRediscountBuyinBatchMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSPjpBtRediscountBuyinBatch record) {
        return batSPjpBtRediscountBuyinBatchMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String rediscBuyBatchId) {
        return batSPjpBtRediscountBuyinBatchMapper.deleteByPrimaryKey(rediscBuyBatchId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSPjpBtRediscountBuyinBatchMapper.deleteByIds(ids);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建综合票据系统-转贴现买入批次表[bat_s_pjp_bt_rediscount_buyin_batch]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_pjp_bt_rediscount_buyin_batch");
        logger.info("重建【PJP00010】综合票据系统-转贴现买入批次表[bat_s_pjp_bt_rediscount_buyin_batch]结束");
        return 0;
    }
}
