/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtRediscountSell
 * @类描述: bat_s_pjp_bt_rediscount_sell数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-03 22:08:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_pjp_bt_rediscount_sell")
public class BatSPjpBtRediscountSell extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "REDISC_SELL_BILL_ID")
	private String rediscSellBillId;
	
	/** 批次ID **/
	@Column(name = "REDISC_SELL_BATCH_ID", unique = false, nullable = true, length = 40)
	private String rediscSellBatchId;
	
	/** 大票对象ID **/
	@Column(name = "BILLINFO_ID", unique = false, nullable = true, length = 40)
	private String billinfoId;
	
	/** 票据号码 **/
	@Column(name = "S_BILL_NO", unique = false, nullable = true, length = 40)
	private String sBillNo;
	
	/** 出票日 **/
	@Column(name = "D_ISSUE_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dIssueDt;
	
	/** 到期日 **/
	@Column(name = "D_DUE_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dDueDt;
	
	/** 出票人名称 **/
	@Column(name = "S_ISSUER_NAME", unique = false, nullable = true, length = 80)
	private String sIssuerName;
	
	/** 出票人开户行行号 **/
	@Column(name = "S_ISSUER_BANK_CODE", unique = false, nullable = true, length = 40)
	private String sIssuerBankCode;
	
	/** 出票人帐号 **/
	@Column(name = "S_ISSUER_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sIssuerAccount;
	
	/** 出票人开户行名称 **/
	@Column(name = "S_ISSUER_BANK_NAME", unique = false, nullable = true, length = 80)
	private String sIssuerBankName;
	
	/** 票面金额 **/
	@Column(name = "F_BILL_AMOUNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fBillAmount;
	
	/** 承兑人 **/
	@Column(name = "S_ACCEPTOR", unique = false, nullable = true, length = 80)
	private String sAcceptor;
	
	/** 收款人 **/
	@Column(name = "S_PAYEE_NAME", unique = false, nullable = true, length = 80)
	private String sPayeeName;
	
	/** 收款人开户行名称 **/
	@Column(name = "S_PAYEE_BANK_NAME", unique = false, nullable = true, length = 80)
	private String sPayeeBankName;
	
	/** 收款人开户行行号 **/
	@Column(name = "S_PAYEE_BANK_CODE", unique = false, nullable = true, length = 40)
	private String sPayeeBankCode;
	
	/** 收款人帐号 **/
	@Column(name = "S_PAYEE_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sPayeeAccount;
	
	/** 票据类型 **/
	@Column(name = "S_BILL_TYPE", unique = false, nullable = true, length = 10)
	private String sBillType;
	
	/** 票据介质 **/
	@Column(name = "S_BILL_MEDIA", unique = false, nullable = true, length = 10)
	private String sBillMedia;
	
	/** 操作员ID **/
	@Column(name = "S_OPERATOR_ID", unique = false, nullable = true, length = 40)
	private String sOperatorId;
	
	/** 明细状态 **/
	@Column(name = "S_REDISCT_STATUS", unique = false, nullable = true, length = 10)
	private String sRedisctStatus;
	
	/** 操作机构ID **/
	@Column(name = "S_BRANCH_ID", unique = false, nullable = true, length = 40)
	private String sBranchId;
	
	/** 应付利息 **/
	@Column(name = "F_INT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fInt;
	
	/** 计息天数 **/
	@Column(name = "F_INT_DAYS", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal fIntDays;
	
	/** 实收金额 **/
	@Column(name = "F_PAYMENT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fPayment;
	
	/** 回复人备注 **/
	@Column(name = "REPLY_REMARK", unique = false, nullable = true, length = 800)
	private String replyRemark;
	
	/** 创建时间 **/
	@Column(name = "D_CREATE_DT", unique = false, nullable = true, length = 20)
	private String dCreateDt;
	
	/** 利率 **/
	@Column(name = "F_RATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fRate;
	
	/** 利率类型 **/
	@Column(name = "S_RATE_TYPE", unique = false, nullable = true, length = 10)
	private String sRateType;
	
	/** 回购开放日 **/
	@Column(name = "D_REDE_START_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dRedeStartDt;
	
	/** 回购截止日 **/
	@Column(name = "D_REDE_END_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dRedeEndDt;
	
	/** 产品id **/
	@Column(name = "PRODUCT_ID", unique = false, nullable = true, length = 40)
	private String productId;
	
	/** 报文状态 **/
	@Column(name = "S_MSG_STATUS", unique = false, nullable = true, length = 15)
	private String sMsgStatus;
	
	/** 报文状态码 **/
	@Column(name = "S_MSG_CODE", unique = false, nullable = true, length = 800)
	private String sMsgCode;
	
	/** 电子签名 **/
	@Column(name = "S_ELCTRN_NAME", unique = false, nullable = true, length = 2000)
	private String sElctrnName;
	
	/** 不得转让标记 **/
	@Column(name = "NOT_ATTORN_FLAG", unique = false, nullable = true, length = 10)
	private String notAttornFlag;
	
	/** 贴出人备注 **/
	@Column(name = "REDISCOUNTOUTREMARK", unique = false, nullable = true, length = 800)
	private String rediscountoutremark;
	
	/** 贴入方名称 **/
	@Column(name = "REDISCOUNTINNAME", unique = false, nullable = true, length = 150)
	private String rediscountinname;
	
	/** 贴入方大额行号 **/
	@Column(name = "REDISCOUNTINBANKCODE", unique = false, nullable = true, length = 20)
	private String rediscountinbankcode;
	
	/** 贴入方账号 **/
	@Column(name = "REDISCOUNTINACCOUNT", unique = false, nullable = true, length = 40)
	private String rediscountinaccount;
	
	/** 贴入方组织机构代码号 **/
	@Column(name = "REDISCOUNTINORGCODE", unique = false, nullable = true, length = 20)
	private String rediscountinorgcode;
	
	/** 贴出方名称 **/
	@Column(name = "REDISCOUNTOUTNAME", unique = false, nullable = true, length = 150)
	private String rediscountoutname;
	
	/** 贴出方大额行号 **/
	@Column(name = "REDISCOUNTOUTCODE", unique = false, nullable = true, length = 20)
	private String rediscountoutcode;
	
	/** 贴出方账号 **/
	@Column(name = "REDISCOUNTOUTACCOUNT", unique = false, nullable = true, length = 40)
	private String rediscountoutaccount;
	
	/** 贴出方组织机构代码号 **/
	@Column(name = "REDISCOUNTOUTORGCODE", unique = false, nullable = true, length = 20)
	private String rediscountoutorgcode;
	
	/** 转贴现种类 **/
	@Column(name = "REDISC_TYPE", unique = false, nullable = true, length = 10)
	private String rediscType;
	
	/** 清算标记 **/
	@Column(name = "CLEARING_FLAG", unique = false, nullable = true, length = 10)
	private String clearingFlag;
	
	/** 转贴现赎回利率 **/
	@Column(name = "BRPDINTRSTRATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal brpdintrstrate;
	
	/** 赎回利率利率类型 **/
	@Column(name = "SRPDRATETYPE", unique = false, nullable = true, length = 10)
	private String srpdratetype;
	
	/** 赎回实付金额 **/
	@Column(name = "BRPDPAYMENT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal brpdpayment;
	
	/** 赎回利息 **/
	@Column(name = "BRPDINT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal brpdint;
	
	/** 承接行名称 **/
	@Column(name = "S_AGCYSVCRNAME", unique = false, nullable = true, length = 120)
	private String sAgcysvcrname;
	
	/** 承接行行号 **/
	@Column(name = "S_AGCYSVCR", unique = false, nullable = true, length = 20)
	private String sAgcysvcr;
	
	/** 同城、异地 **/
	@Column(name = "S_CITYTYPE", unique = false, nullable = true, length = 10)
	private String sCitytype;
	
	/** 是否双向 **/
	@Column(name = "S_IF_BIDIRECT", unique = false, nullable = true, length = 10)
	private String sIfBidirect;
	
	/** 库存状态 **/
	@Column(name = "STORAGE_STS", unique = false, nullable = true, length = 10)
	private String storageSts;
	
	/** 加入状态 **/
	@Column(name = "ADD_STATUS", unique = false, nullable = true, length = 10)
	private String addStatus;
	
	/** 双向卖断到期日 **/
	@Column(name = "BIDIRECT_DT", unique = false, nullable = true, length = 10)
	private java.util.Date bidirectDt;
	
	/** 回购到期日 **/
	@Column(name = "RETURN_GOU_DT", unique = false, nullable = true, length = 10)
	private java.util.Date returnGouDt;
	
	/** 双买明细id  或者  买入返售 到期返售时 存 买入ID或卖出回购时 存原买入清单ID **/
	@Column(name = "BIDIRECT_BUY_BILL_ID", unique = false, nullable = true, length = 40)
	private String bidirectBuyBillId;
	
	/** 贴出方类别 **/
	@Column(name = "REDISCOUNTOUTROLE", unique = false, nullable = true, length = 10)
	private String rediscountoutrole;
	
	/** 撤销报文状态 **/
	@Column(name = "S_CANCELMSGSTATUS", unique = false, nullable = true, length = 15)
	private String sCancelmsgstatus;
	
	/** 撤销报文状态说明（032报文状态说明） **/
	@Column(name = "S_CANCELMSGCODE", unique = false, nullable = true, length = 800)
	private String sCancelmsgcode;
	
	/** 支付交易序号 **/
	@Column(name = "S_TRF_ID", unique = false, nullable = true, length = 40)
	private String sTrfId;
	
	/** 票据持有行名称 **/
	@Column(name = "S_OWNER_BILL_NAME", unique = false, nullable = true, length = 40)
	private String sOwnerBillName;
	
	/** 票据持有行大额支付号 **/
	@Column(name = "S_MBFE_BANK_CODE", unique = false, nullable = true, length = 40)
	private String sMbfeBankCode;
	
	/** 票据前手 **/
	@Column(name = "S_PREHAND", unique = false, nullable = true, length = 80)
	private String sPrehand;
	
	/** D_INT_END_DT **/
	@Column(name = "D_INT_END_DT", unique = false, nullable = true, length = 20)
	private String dIntEndDt;
	
	/** 申请日期 **/
	@Column(name = "REDISCOUNTAPPLYDT", unique = false, nullable = true, length = 10)
	private java.util.Date rediscountapplydt;
	
	/** 票据来源-贴现或转贴现 **/
	@Column(name = "SC_PRODUCT_ID", unique = false, nullable = true, length = 10)
	private String scProductId;
	
	/** SIFPOSTPONE **/
	@Column(name = "SIFPOSTPONE", unique = false, nullable = true, length = 10)
	private String sifpostpone;
	
	/** POSTPONEDAYS **/
	@Column(name = "POSTPONEDAYS", unique = false, nullable = true, length = 10)
	private String postponedays;
	
	/** POSTPONEDAYSSAMECITY **/
	@Column(name = "POSTPONEDAYSSAMECITY", unique = false, nullable = true, length = 10)
	private String postponedayssamecity;
	
	/** 记账流水号（没用） **/
	@Column(name = "S_JZLSH", unique = false, nullable = true, length = 50)
	private String sJzlsh;
	
	/** 核算流水号（没用） **/
	@Column(name = "S_HSLSH", unique = false, nullable = true, length = 50)
	private String sHslsh;
	
	/** 记账状态（没用） **/
	@Column(name = "S_JZZT", unique = false, nullable = true, length = 2)
	private String sJzzt;
	
	/** 核算状态（没用） **/
	@Column(name = "S_HSZT", unique = false, nullable = true, length = 2)
	private String sHszt;
	
	/** 记账日期（没用） **/
	@Column(name = "S_JZRQ", unique = false, nullable = true, length = 10)
	private java.util.Date sJzrq;
	
	/** 是否回购状态 0=未到期 1=已到期未回购 2=已到期已回购 3=到期未回购转买断 **/
	@Column(name = "BILLDT_ZT", unique = false, nullable = true, length = 2)
	private String billdtZt;
	
	/** 银行类别-国有、外资等 **/
	@Column(name = "S_COUNTERPARTYBANKTYPE", unique = false, nullable = true, length = 5)
	private String sCounterpartybanktype;
	
	/** 卖出机构（没用） **/
	@Column(name = "S_MCJG", unique = false, nullable = true, length = 10)
	private String sMcjg;
	
	/** 买入机构（没用） **/
	@Column(name = "S_MRJG", unique = false, nullable = true, length = 10)
	private String sMrjg;
	
	/** 借据号（没用） **/
	@Column(name = "S_JJH", unique = false, nullable = true, length = 50)
	private String sJjh;
	
	/** 记账机构（没用） **/
	@Column(name = "HX_JZJG", unique = false, nullable = true, length = 10)
	private String hxJzjg;
	
	/** 票据买入方式 **/
	@Column(name = "BILLBUYMODE", unique = false, nullable = true, length = 10)
	private String billbuymode;
	
	/** 计息到期日 **/
	@Column(name = "GALE_DT", unique = false, nullable = true, length = 10)
	private java.util.Date galeDt;
	
	/** 业务结清标记00未结清 01结清 03卖出回购 04卖出回购到期买回 **/
	@Column(name = "BUSI_END_FLAG", unique = false, nullable = true, length = 10)
	private String busiEndFlag;
	
	/** 业务结清类型 **/
	@Column(name = "BUSI_END_TYPE", unique = false, nullable = true, length = 10)
	private String busiEndType;
	
	/** 业务结清日期 **/
	@Column(name = "BUSI_END_date", unique = false, nullable = true, length = 10)
	private java.util.Date bUSIENDDate;
	
	/** 记账日期 **/
	@Column(name = "ACCT_date", unique = false, nullable = true, length = 10)
	private java.util.Date aCCTDate;
	
	/** 是否系统内 **/
	@Column(name = "IF_SYSTEM_INNER", unique = false, nullable = true, length = 10)
	private String ifSystemInner;
	
	/** 记账流水号 **/
	@Column(name = "ACCT_FLOW_NO", unique = false, nullable = true, length = 60)
	private String acctFlowNo;
	
	/** 记账柜员号 **/
	@Column(name = "ACCT_USER_NO", unique = false, nullable = true, length = 60)
	private String acctUserNo;
	
	/** ACCT_USER_NAME **/
	@Column(name = "ACCT_USER_NAME", unique = false, nullable = true, length = 60)
	private String acctUserName;
	
	/** 记账授权柜员 **/
	@Column(name = "ACCT_AUTH_USER_NO", unique = false, nullable = true, length = 60)
	private String acctAuthUserNo;
	
	/** 记账授权柜员 **/
	@Column(name = "ACCT_AUTH_USER_NAME", unique = false, nullable = true, length = 60)
	private String acctAuthUserName;
	
	/** 撤销记账柜名 **/
	@Column(name = "CANCEL_ACCT_USER_NO", unique = false, nullable = true, length = 60)
	private String cancelAcctUserNo;
	
	/** 撤销记账柜名 **/
	@Column(name = "CANCEL_ACCT_USER_NAME", unique = false, nullable = true, length = 60)
	private String cancelAcctUserName;
	
	/** 撤销记账授权人 **/
	@Column(name = "CANCEL_ACCT_AUTH_USER_NO", unique = false, nullable = true, length = 60)
	private String cancelAcctAuthUserNo;
	
	/** 撤销记账授权人 **/
	@Column(name = "CANCEL_ACCT_AUTH_USER_NAME", unique = false, nullable = true, length = 60)
	private String cancelAcctAuthUserName;
	
	/** 代理行业务标记 1代理行业务 0本行业务 **/
	@Column(name = "AGENT_FLAG", unique = false, nullable = true, length = 2)
	private String agentFlag;
	
	/** 贴入机构ID **/
	@Column(name = "REDISCOUNT_IN_BRANCH_ID", unique = false, nullable = true, length = 50)
	private String rediscountInBranchId;
	
	/** 贴入机构号 **/
	@Column(name = "REDISCOUNT_IN_BRANCH_NO", unique = false, nullable = true, length = 30)
	private String rediscountInBranchNo;
	
	/** 占用额度类型 **/
	@Column(name = "CREDITTYPE", unique = false, nullable = true, length = 20)
	private String credittype;
	
	/** 设否设置额度扣减规则  00 扣减承兑行   01扣减交易对手 **/
	@Column(name = "CREDITRULE", unique = false, nullable = true, length = 10)
	private String creditrule;
	
	/** 额度扣减总行行号 **/
	@Column(name = "CREDIT_ZHBANK_CODE", unique = false, nullable = true, length = 100)
	private String creditZhbankCode;
	
	/** 额度扣减总行行名 **/
	@Column(name = "CREDIT_ZHBANK_NAME", unique = false, nullable = true, length = 200)
	private String creditZhbankName;
	
	/** 调整天数   异地+顺延节假日天数 **/
	@Column(name = "IMPROVE_DAYS", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal improveDays;
	
	/** 数据移植标记 1:移植数据  0：正常数据 **/
	@Column(name = "YZ_FLAG", unique = false, nullable = true, length = 2)
	private String yzFlag;
	
	/** 核心返回的记账日期 **/
	@Column(name = "CORE_TRANDT", unique = false, nullable = true, length = 40)
	private String coreTrandt;
	
	/** 核心返回的记账流水 **/
	@Column(name = "CORE_TRANSQ", unique = false, nullable = true, length = 100)
	private String coreTransq;
	
	/** 报价ID **/
	@Column(name = "QUOTATION_BATCH_ID", unique = false, nullable = true, length = 40)
	private String quotationBatchId;
	
	/** 实际到期日 **/
	@Column(name = "MATDT", unique = false, nullable = true, length = 10)
	private java.util.Date matdt;
	
	/** (首期)剩余期限 **/
	@Column(name = "TENORDAYS", unique = false, nullable = true, length = 10)
	private String tenordays;
	
	/** 到期剩余期限 **/
	@Column(name = "TENORDAYS2", unique = false, nullable = true, length = 10)
	private String tenordays2;
	
	/** 是否转贴现票据 **/
	@Column(name = "IS_DISC", unique = false, nullable = true, length = 10)
	private String isDisc;
	
	/** 是否异地票据 **/
	@Column(name = "IS_OTHERCITY", unique = false, nullable = true, length = 10)
	private String isOthercity;
	
	/** 是否符合政策标准 **/
	@Column(name = "IS_POLICY", unique = false, nullable = true, length = 10)
	private String isPolicy;
	
	/** 剩余未摊销金额 **/
	@Column(name = "UN_ACCT_AMT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal unAcctAmt;
	
	/** 原买入来源类型 **/
	@Column(name = "SOURCE_TYPE", unique = false, nullable = true, length = 4)
	private String sourceType;
	
	/** 原买入来源ID **/
	@Column(name = "SOURCE_ID", unique = false, nullable = true, length = 50)
	private String sourceId;
	
	
	/**
	 * @param rediscSellBillId
	 */
	public void setRediscSellBillId(String rediscSellBillId) {
		this.rediscSellBillId = rediscSellBillId;
	}
	
    /**
     * @return rediscSellBillId
     */
	public String getRediscSellBillId() {
		return this.rediscSellBillId;
	}
	
	/**
	 * @param rediscSellBatchId
	 */
	public void setRediscSellBatchId(String rediscSellBatchId) {
		this.rediscSellBatchId = rediscSellBatchId;
	}
	
    /**
     * @return rediscSellBatchId
     */
	public String getRediscSellBatchId() {
		return this.rediscSellBatchId;
	}
	
	/**
	 * @param billinfoId
	 */
	public void setBillinfoId(String billinfoId) {
		this.billinfoId = billinfoId;
	}
	
    /**
     * @return billinfoId
     */
	public String getBillinfoId() {
		return this.billinfoId;
	}
	
	/**
	 * @param sBillNo
	 */
	public void setSBillNo(String sBillNo) {
		this.sBillNo = sBillNo;
	}
	
    /**
     * @return sBillNo
     */
	public String getSBillNo() {
		return this.sBillNo;
	}
	
	/**
	 * @param dIssueDt
	 */
	public void setDIssueDt(java.util.Date dIssueDt) {
		this.dIssueDt = dIssueDt;
	}
	
    /**
     * @return dIssueDt
     */
	public java.util.Date getDIssueDt() {
		return this.dIssueDt;
	}
	
	/**
	 * @param dDueDt
	 */
	public void setDDueDt(java.util.Date dDueDt) {
		this.dDueDt = dDueDt;
	}
	
    /**
     * @return dDueDt
     */
	public java.util.Date getDDueDt() {
		return this.dDueDt;
	}
	
	/**
	 * @param sIssuerName
	 */
	public void setSIssuerName(String sIssuerName) {
		this.sIssuerName = sIssuerName;
	}
	
    /**
     * @return sIssuerName
     */
	public String getSIssuerName() {
		return this.sIssuerName;
	}
	
	/**
	 * @param sIssuerBankCode
	 */
	public void setSIssuerBankCode(String sIssuerBankCode) {
		this.sIssuerBankCode = sIssuerBankCode;
	}
	
    /**
     * @return sIssuerBankCode
     */
	public String getSIssuerBankCode() {
		return this.sIssuerBankCode;
	}
	
	/**
	 * @param sIssuerAccount
	 */
	public void setSIssuerAccount(String sIssuerAccount) {
		this.sIssuerAccount = sIssuerAccount;
	}
	
    /**
     * @return sIssuerAccount
     */
	public String getSIssuerAccount() {
		return this.sIssuerAccount;
	}
	
	/**
	 * @param sIssuerBankName
	 */
	public void setSIssuerBankName(String sIssuerBankName) {
		this.sIssuerBankName = sIssuerBankName;
	}
	
    /**
     * @return sIssuerBankName
     */
	public String getSIssuerBankName() {
		return this.sIssuerBankName;
	}
	
	/**
	 * @param fBillAmount
	 */
	public void setFBillAmount(java.math.BigDecimal fBillAmount) {
		this.fBillAmount = fBillAmount;
	}
	
    /**
     * @return fBillAmount
     */
	public java.math.BigDecimal getFBillAmount() {
		return this.fBillAmount;
	}
	
	/**
	 * @param sAcceptor
	 */
	public void setSAcceptor(String sAcceptor) {
		this.sAcceptor = sAcceptor;
	}
	
    /**
     * @return sAcceptor
     */
	public String getSAcceptor() {
		return this.sAcceptor;
	}
	
	/**
	 * @param sPayeeName
	 */
	public void setSPayeeName(String sPayeeName) {
		this.sPayeeName = sPayeeName;
	}
	
    /**
     * @return sPayeeName
     */
	public String getSPayeeName() {
		return this.sPayeeName;
	}
	
	/**
	 * @param sPayeeBankName
	 */
	public void setSPayeeBankName(String sPayeeBankName) {
		this.sPayeeBankName = sPayeeBankName;
	}
	
    /**
     * @return sPayeeBankName
     */
	public String getSPayeeBankName() {
		return this.sPayeeBankName;
	}
	
	/**
	 * @param sPayeeBankCode
	 */
	public void setSPayeeBankCode(String sPayeeBankCode) {
		this.sPayeeBankCode = sPayeeBankCode;
	}
	
    /**
     * @return sPayeeBankCode
     */
	public String getSPayeeBankCode() {
		return this.sPayeeBankCode;
	}
	
	/**
	 * @param sPayeeAccount
	 */
	public void setSPayeeAccount(String sPayeeAccount) {
		this.sPayeeAccount = sPayeeAccount;
	}
	
    /**
     * @return sPayeeAccount
     */
	public String getSPayeeAccount() {
		return this.sPayeeAccount;
	}
	
	/**
	 * @param sBillType
	 */
	public void setSBillType(String sBillType) {
		this.sBillType = sBillType;
	}
	
    /**
     * @return sBillType
     */
	public String getSBillType() {
		return this.sBillType;
	}
	
	/**
	 * @param sBillMedia
	 */
	public void setSBillMedia(String sBillMedia) {
		this.sBillMedia = sBillMedia;
	}
	
    /**
     * @return sBillMedia
     */
	public String getSBillMedia() {
		return this.sBillMedia;
	}
	
	/**
	 * @param sOperatorId
	 */
	public void setSOperatorId(String sOperatorId) {
		this.sOperatorId = sOperatorId;
	}
	
    /**
     * @return sOperatorId
     */
	public String getSOperatorId() {
		return this.sOperatorId;
	}
	
	/**
	 * @param sRedisctStatus
	 */
	public void setSRedisctStatus(String sRedisctStatus) {
		this.sRedisctStatus = sRedisctStatus;
	}
	
    /**
     * @return sRedisctStatus
     */
	public String getSRedisctStatus() {
		return this.sRedisctStatus;
	}
	
	/**
	 * @param sBranchId
	 */
	public void setSBranchId(String sBranchId) {
		this.sBranchId = sBranchId;
	}
	
    /**
     * @return sBranchId
     */
	public String getSBranchId() {
		return this.sBranchId;
	}
	
	/**
	 * @param fInt
	 */
	public void setFInt(java.math.BigDecimal fInt) {
		this.fInt = fInt;
	}
	
    /**
     * @return fInt
     */
	public java.math.BigDecimal getFInt() {
		return this.fInt;
	}
	
	/**
	 * @param fIntDays
	 */
	public void setFIntDays(java.math.BigDecimal fIntDays) {
		this.fIntDays = fIntDays;
	}
	
    /**
     * @return fIntDays
     */
	public java.math.BigDecimal getFIntDays() {
		return this.fIntDays;
	}
	
	/**
	 * @param fPayment
	 */
	public void setFPayment(java.math.BigDecimal fPayment) {
		this.fPayment = fPayment;
	}
	
    /**
     * @return fPayment
     */
	public java.math.BigDecimal getFPayment() {
		return this.fPayment;
	}
	
	/**
	 * @param replyRemark
	 */
	public void setReplyRemark(String replyRemark) {
		this.replyRemark = replyRemark;
	}
	
    /**
     * @return replyRemark
     */
	public String getReplyRemark() {
		return this.replyRemark;
	}
	
	/**
	 * @param dCreateDt
	 */
	public void setDCreateDt(String dCreateDt) {
		this.dCreateDt = dCreateDt;
	}
	
    /**
     * @return dCreateDt
     */
	public String getDCreateDt() {
		return this.dCreateDt;
	}
	
	/**
	 * @param fRate
	 */
	public void setFRate(java.math.BigDecimal fRate) {
		this.fRate = fRate;
	}
	
    /**
     * @return fRate
     */
	public java.math.BigDecimal getFRate() {
		return this.fRate;
	}
	
	/**
	 * @param sRateType
	 */
	public void setSRateType(String sRateType) {
		this.sRateType = sRateType;
	}
	
    /**
     * @return sRateType
     */
	public String getSRateType() {
		return this.sRateType;
	}
	
	/**
	 * @param dRedeStartDt
	 */
	public void setDRedeStartDt(java.util.Date dRedeStartDt) {
		this.dRedeStartDt = dRedeStartDt;
	}
	
    /**
     * @return dRedeStartDt
     */
	public java.util.Date getDRedeStartDt() {
		return this.dRedeStartDt;
	}
	
	/**
	 * @param dRedeEndDt
	 */
	public void setDRedeEndDt(java.util.Date dRedeEndDt) {
		this.dRedeEndDt = dRedeEndDt;
	}
	
    /**
     * @return dRedeEndDt
     */
	public java.util.Date getDRedeEndDt() {
		return this.dRedeEndDt;
	}
	
	/**
	 * @param productId
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
    /**
     * @return productId
     */
	public String getProductId() {
		return this.productId;
	}
	
	/**
	 * @param sMsgStatus
	 */
	public void setSMsgStatus(String sMsgStatus) {
		this.sMsgStatus = sMsgStatus;
	}
	
    /**
     * @return sMsgStatus
     */
	public String getSMsgStatus() {
		return this.sMsgStatus;
	}
	
	/**
	 * @param sMsgCode
	 */
	public void setSMsgCode(String sMsgCode) {
		this.sMsgCode = sMsgCode;
	}
	
    /**
     * @return sMsgCode
     */
	public String getSMsgCode() {
		return this.sMsgCode;
	}
	
	/**
	 * @param sElctrnName
	 */
	public void setSElctrnName(String sElctrnName) {
		this.sElctrnName = sElctrnName;
	}
	
    /**
     * @return sElctrnName
     */
	public String getSElctrnName() {
		return this.sElctrnName;
	}
	
	/**
	 * @param notAttornFlag
	 */
	public void setNotAttornFlag(String notAttornFlag) {
		this.notAttornFlag = notAttornFlag;
	}
	
    /**
     * @return notAttornFlag
     */
	public String getNotAttornFlag() {
		return this.notAttornFlag;
	}
	
	/**
	 * @param rediscountoutremark
	 */
	public void setRediscountoutremark(String rediscountoutremark) {
		this.rediscountoutremark = rediscountoutremark;
	}
	
    /**
     * @return rediscountoutremark
     */
	public String getRediscountoutremark() {
		return this.rediscountoutremark;
	}
	
	/**
	 * @param rediscountinname
	 */
	public void setRediscountinname(String rediscountinname) {
		this.rediscountinname = rediscountinname;
	}
	
    /**
     * @return rediscountinname
     */
	public String getRediscountinname() {
		return this.rediscountinname;
	}
	
	/**
	 * @param rediscountinbankcode
	 */
	public void setRediscountinbankcode(String rediscountinbankcode) {
		this.rediscountinbankcode = rediscountinbankcode;
	}
	
    /**
     * @return rediscountinbankcode
     */
	public String getRediscountinbankcode() {
		return this.rediscountinbankcode;
	}
	
	/**
	 * @param rediscountinaccount
	 */
	public void setRediscountinaccount(String rediscountinaccount) {
		this.rediscountinaccount = rediscountinaccount;
	}
	
    /**
     * @return rediscountinaccount
     */
	public String getRediscountinaccount() {
		return this.rediscountinaccount;
	}
	
	/**
	 * @param rediscountinorgcode
	 */
	public void setRediscountinorgcode(String rediscountinorgcode) {
		this.rediscountinorgcode = rediscountinorgcode;
	}
	
    /**
     * @return rediscountinorgcode
     */
	public String getRediscountinorgcode() {
		return this.rediscountinorgcode;
	}
	
	/**
	 * @param rediscountoutname
	 */
	public void setRediscountoutname(String rediscountoutname) {
		this.rediscountoutname = rediscountoutname;
	}
	
    /**
     * @return rediscountoutname
     */
	public String getRediscountoutname() {
		return this.rediscountoutname;
	}
	
	/**
	 * @param rediscountoutcode
	 */
	public void setRediscountoutcode(String rediscountoutcode) {
		this.rediscountoutcode = rediscountoutcode;
	}
	
    /**
     * @return rediscountoutcode
     */
	public String getRediscountoutcode() {
		return this.rediscountoutcode;
	}
	
	/**
	 * @param rediscountoutaccount
	 */
	public void setRediscountoutaccount(String rediscountoutaccount) {
		this.rediscountoutaccount = rediscountoutaccount;
	}
	
    /**
     * @return rediscountoutaccount
     */
	public String getRediscountoutaccount() {
		return this.rediscountoutaccount;
	}
	
	/**
	 * @param rediscountoutorgcode
	 */
	public void setRediscountoutorgcode(String rediscountoutorgcode) {
		this.rediscountoutorgcode = rediscountoutorgcode;
	}
	
    /**
     * @return rediscountoutorgcode
     */
	public String getRediscountoutorgcode() {
		return this.rediscountoutorgcode;
	}
	
	/**
	 * @param rediscType
	 */
	public void setRediscType(String rediscType) {
		this.rediscType = rediscType;
	}
	
    /**
     * @return rediscType
     */
	public String getRediscType() {
		return this.rediscType;
	}
	
	/**
	 * @param clearingFlag
	 */
	public void setClearingFlag(String clearingFlag) {
		this.clearingFlag = clearingFlag;
	}
	
    /**
     * @return clearingFlag
     */
	public String getClearingFlag() {
		return this.clearingFlag;
	}
	
	/**
	 * @param brpdintrstrate
	 */
	public void setBrpdintrstrate(java.math.BigDecimal brpdintrstrate) {
		this.brpdintrstrate = brpdintrstrate;
	}
	
    /**
     * @return brpdintrstrate
     */
	public java.math.BigDecimal getBrpdintrstrate() {
		return this.brpdintrstrate;
	}
	
	/**
	 * @param srpdratetype
	 */
	public void setSrpdratetype(String srpdratetype) {
		this.srpdratetype = srpdratetype;
	}
	
    /**
     * @return srpdratetype
     */
	public String getSrpdratetype() {
		return this.srpdratetype;
	}
	
	/**
	 * @param brpdpayment
	 */
	public void setBrpdpayment(java.math.BigDecimal brpdpayment) {
		this.brpdpayment = brpdpayment;
	}
	
    /**
     * @return brpdpayment
     */
	public java.math.BigDecimal getBrpdpayment() {
		return this.brpdpayment;
	}
	
	/**
	 * @param brpdint
	 */
	public void setBrpdint(java.math.BigDecimal brpdint) {
		this.brpdint = brpdint;
	}
	
    /**
     * @return brpdint
     */
	public java.math.BigDecimal getBrpdint() {
		return this.brpdint;
	}
	
	/**
	 * @param sAgcysvcrname
	 */
	public void setSAgcysvcrname(String sAgcysvcrname) {
		this.sAgcysvcrname = sAgcysvcrname;
	}
	
    /**
     * @return sAgcysvcrname
     */
	public String getSAgcysvcrname() {
		return this.sAgcysvcrname;
	}
	
	/**
	 * @param sAgcysvcr
	 */
	public void setSAgcysvcr(String sAgcysvcr) {
		this.sAgcysvcr = sAgcysvcr;
	}
	
    /**
     * @return sAgcysvcr
     */
	public String getSAgcysvcr() {
		return this.sAgcysvcr;
	}
	
	/**
	 * @param sCitytype
	 */
	public void setSCitytype(String sCitytype) {
		this.sCitytype = sCitytype;
	}
	
    /**
     * @return sCitytype
     */
	public String getSCitytype() {
		return this.sCitytype;
	}
	
	/**
	 * @param sIfBidirect
	 */
	public void setSIfBidirect(String sIfBidirect) {
		this.sIfBidirect = sIfBidirect;
	}
	
    /**
     * @return sIfBidirect
     */
	public String getSIfBidirect() {
		return this.sIfBidirect;
	}
	
	/**
	 * @param storageSts
	 */
	public void setStorageSts(String storageSts) {
		this.storageSts = storageSts;
	}
	
    /**
     * @return storageSts
     */
	public String getStorageSts() {
		return this.storageSts;
	}
	
	/**
	 * @param addStatus
	 */
	public void setAddStatus(String addStatus) {
		this.addStatus = addStatus;
	}
	
    /**
     * @return addStatus
     */
	public String getAddStatus() {
		return this.addStatus;
	}
	
	/**
	 * @param bidirectDt
	 */
	public void setBidirectDt(java.util.Date bidirectDt) {
		this.bidirectDt = bidirectDt;
	}
	
    /**
     * @return bidirectDt
     */
	public java.util.Date getBidirectDt() {
		return this.bidirectDt;
	}
	
	/**
	 * @param returnGouDt
	 */
	public void setReturnGouDt(java.util.Date returnGouDt) {
		this.returnGouDt = returnGouDt;
	}
	
    /**
     * @return returnGouDt
     */
	public java.util.Date getReturnGouDt() {
		return this.returnGouDt;
	}
	
	/**
	 * @param bidirectBuyBillId
	 */
	public void setBidirectBuyBillId(String bidirectBuyBillId) {
		this.bidirectBuyBillId = bidirectBuyBillId;
	}
	
    /**
     * @return bidirectBuyBillId
     */
	public String getBidirectBuyBillId() {
		return this.bidirectBuyBillId;
	}
	
	/**
	 * @param rediscountoutrole
	 */
	public void setRediscountoutrole(String rediscountoutrole) {
		this.rediscountoutrole = rediscountoutrole;
	}
	
    /**
     * @return rediscountoutrole
     */
	public String getRediscountoutrole() {
		return this.rediscountoutrole;
	}
	
	/**
	 * @param sCancelmsgstatus
	 */
	public void setSCancelmsgstatus(String sCancelmsgstatus) {
		this.sCancelmsgstatus = sCancelmsgstatus;
	}
	
    /**
     * @return sCancelmsgstatus
     */
	public String getSCancelmsgstatus() {
		return this.sCancelmsgstatus;
	}
	
	/**
	 * @param sCancelmsgcode
	 */
	public void setSCancelmsgcode(String sCancelmsgcode) {
		this.sCancelmsgcode = sCancelmsgcode;
	}
	
    /**
     * @return sCancelmsgcode
     */
	public String getSCancelmsgcode() {
		return this.sCancelmsgcode;
	}
	
	/**
	 * @param sTrfId
	 */
	public void setSTrfId(String sTrfId) {
		this.sTrfId = sTrfId;
	}
	
    /**
     * @return sTrfId
     */
	public String getSTrfId() {
		return this.sTrfId;
	}
	
	/**
	 * @param sOwnerBillName
	 */
	public void setSOwnerBillName(String sOwnerBillName) {
		this.sOwnerBillName = sOwnerBillName;
	}
	
    /**
     * @return sOwnerBillName
     */
	public String getSOwnerBillName() {
		return this.sOwnerBillName;
	}
	
	/**
	 * @param sMbfeBankCode
	 */
	public void setSMbfeBankCode(String sMbfeBankCode) {
		this.sMbfeBankCode = sMbfeBankCode;
	}
	
    /**
     * @return sMbfeBankCode
     */
	public String getSMbfeBankCode() {
		return this.sMbfeBankCode;
	}
	
	/**
	 * @param sPrehand
	 */
	public void setSPrehand(String sPrehand) {
		this.sPrehand = sPrehand;
	}
	
    /**
     * @return sPrehand
     */
	public String getSPrehand() {
		return this.sPrehand;
	}
	
	/**
	 * @param dIntEndDt
	 */
	public void setDIntEndDt(String dIntEndDt) {
		this.dIntEndDt = dIntEndDt;
	}
	
    /**
     * @return dIntEndDt
     */
	public String getDIntEndDt() {
		return this.dIntEndDt;
	}
	
	/**
	 * @param rediscountapplydt
	 */
	public void setRediscountapplydt(java.util.Date rediscountapplydt) {
		this.rediscountapplydt = rediscountapplydt;
	}
	
    /**
     * @return rediscountapplydt
     */
	public java.util.Date getRediscountapplydt() {
		return this.rediscountapplydt;
	}
	
	/**
	 * @param scProductId
	 */
	public void setScProductId(String scProductId) {
		this.scProductId = scProductId;
	}
	
    /**
     * @return scProductId
     */
	public String getScProductId() {
		return this.scProductId;
	}
	
	/**
	 * @param sifpostpone
	 */
	public void setSifpostpone(String sifpostpone) {
		this.sifpostpone = sifpostpone;
	}
	
    /**
     * @return sifpostpone
     */
	public String getSifpostpone() {
		return this.sifpostpone;
	}
	
	/**
	 * @param postponedays
	 */
	public void setPostponedays(String postponedays) {
		this.postponedays = postponedays;
	}
	
    /**
     * @return postponedays
     */
	public String getPostponedays() {
		return this.postponedays;
	}
	
	/**
	 * @param postponedayssamecity
	 */
	public void setPostponedayssamecity(String postponedayssamecity) {
		this.postponedayssamecity = postponedayssamecity;
	}
	
    /**
     * @return postponedayssamecity
     */
	public String getPostponedayssamecity() {
		return this.postponedayssamecity;
	}
	
	/**
	 * @param sJzlsh
	 */
	public void setSJzlsh(String sJzlsh) {
		this.sJzlsh = sJzlsh;
	}
	
    /**
     * @return sJzlsh
     */
	public String getSJzlsh() {
		return this.sJzlsh;
	}
	
	/**
	 * @param sHslsh
	 */
	public void setSHslsh(String sHslsh) {
		this.sHslsh = sHslsh;
	}
	
    /**
     * @return sHslsh
     */
	public String getSHslsh() {
		return this.sHslsh;
	}
	
	/**
	 * @param sJzzt
	 */
	public void setSJzzt(String sJzzt) {
		this.sJzzt = sJzzt;
	}
	
    /**
     * @return sJzzt
     */
	public String getSJzzt() {
		return this.sJzzt;
	}
	
	/**
	 * @param sHszt
	 */
	public void setSHszt(String sHszt) {
		this.sHszt = sHszt;
	}
	
    /**
     * @return sHszt
     */
	public String getSHszt() {
		return this.sHszt;
	}
	
	/**
	 * @param sJzrq
	 */
	public void setSJzrq(java.util.Date sJzrq) {
		this.sJzrq = sJzrq;
	}
	
    /**
     * @return sJzrq
     */
	public java.util.Date getSJzrq() {
		return this.sJzrq;
	}
	
	/**
	 * @param billdtZt
	 */
	public void setBilldtZt(String billdtZt) {
		this.billdtZt = billdtZt;
	}
	
    /**
     * @return billdtZt
     */
	public String getBilldtZt() {
		return this.billdtZt;
	}
	
	/**
	 * @param sCounterpartybanktype
	 */
	public void setSCounterpartybanktype(String sCounterpartybanktype) {
		this.sCounterpartybanktype = sCounterpartybanktype;
	}
	
    /**
     * @return sCounterpartybanktype
     */
	public String getSCounterpartybanktype() {
		return this.sCounterpartybanktype;
	}
	
	/**
	 * @param sMcjg
	 */
	public void setSMcjg(String sMcjg) {
		this.sMcjg = sMcjg;
	}
	
    /**
     * @return sMcjg
     */
	public String getSMcjg() {
		return this.sMcjg;
	}
	
	/**
	 * @param sMrjg
	 */
	public void setSMrjg(String sMrjg) {
		this.sMrjg = sMrjg;
	}
	
    /**
     * @return sMrjg
     */
	public String getSMrjg() {
		return this.sMrjg;
	}
	
	/**
	 * @param sJjh
	 */
	public void setSJjh(String sJjh) {
		this.sJjh = sJjh;
	}
	
    /**
     * @return sJjh
     */
	public String getSJjh() {
		return this.sJjh;
	}
	
	/**
	 * @param hxJzjg
	 */
	public void setHxJzjg(String hxJzjg) {
		this.hxJzjg = hxJzjg;
	}
	
    /**
     * @return hxJzjg
     */
	public String getHxJzjg() {
		return this.hxJzjg;
	}
	
	/**
	 * @param billbuymode
	 */
	public void setBillbuymode(String billbuymode) {
		this.billbuymode = billbuymode;
	}
	
    /**
     * @return billbuymode
     */
	public String getBillbuymode() {
		return this.billbuymode;
	}
	
	/**
	 * @param galeDt
	 */
	public void setGaleDt(java.util.Date galeDt) {
		this.galeDt = galeDt;
	}
	
    /**
     * @return galeDt
     */
	public java.util.Date getGaleDt() {
		return this.galeDt;
	}
	
	/**
	 * @param busiEndFlag
	 */
	public void setBusiEndFlag(String busiEndFlag) {
		this.busiEndFlag = busiEndFlag;
	}
	
    /**
     * @return busiEndFlag
     */
	public String getBusiEndFlag() {
		return this.busiEndFlag;
	}
	
	/**
	 * @param busiEndType
	 */
	public void setBusiEndType(String busiEndType) {
		this.busiEndType = busiEndType;
	}
	
    /**
     * @return busiEndType
     */
	public String getBusiEndType() {
		return this.busiEndType;
	}
	
	/**
	 * @param bUSIENDDate
	 */
	public void setBUSIENDDate(java.util.Date bUSIENDDate) {
		this.bUSIENDDate = bUSIENDDate;
	}
	
    /**
     * @return bUSIENDDate
     */
	public java.util.Date getBUSIENDDate() {
		return this.bUSIENDDate;
	}
	
	/**
	 * @param aCCTDate
	 */
	public void setACCTDate(java.util.Date aCCTDate) {
		this.aCCTDate = aCCTDate;
	}
	
    /**
     * @return aCCTDate
     */
	public java.util.Date getACCTDate() {
		return this.aCCTDate;
	}
	
	/**
	 * @param ifSystemInner
	 */
	public void setIfSystemInner(String ifSystemInner) {
		this.ifSystemInner = ifSystemInner;
	}
	
    /**
     * @return ifSystemInner
     */
	public String getIfSystemInner() {
		return this.ifSystemInner;
	}
	
	/**
	 * @param acctFlowNo
	 */
	public void setAcctFlowNo(String acctFlowNo) {
		this.acctFlowNo = acctFlowNo;
	}
	
    /**
     * @return acctFlowNo
     */
	public String getAcctFlowNo() {
		return this.acctFlowNo;
	}
	
	/**
	 * @param acctUserNo
	 */
	public void setAcctUserNo(String acctUserNo) {
		this.acctUserNo = acctUserNo;
	}
	
    /**
     * @return acctUserNo
     */
	public String getAcctUserNo() {
		return this.acctUserNo;
	}
	
	/**
	 * @param acctUserName
	 */
	public void setAcctUserName(String acctUserName) {
		this.acctUserName = acctUserName;
	}
	
    /**
     * @return acctUserName
     */
	public String getAcctUserName() {
		return this.acctUserName;
	}
	
	/**
	 * @param acctAuthUserNo
	 */
	public void setAcctAuthUserNo(String acctAuthUserNo) {
		this.acctAuthUserNo = acctAuthUserNo;
	}
	
    /**
     * @return acctAuthUserNo
     */
	public String getAcctAuthUserNo() {
		return this.acctAuthUserNo;
	}
	
	/**
	 * @param acctAuthUserName
	 */
	public void setAcctAuthUserName(String acctAuthUserName) {
		this.acctAuthUserName = acctAuthUserName;
	}
	
    /**
     * @return acctAuthUserName
     */
	public String getAcctAuthUserName() {
		return this.acctAuthUserName;
	}
	
	/**
	 * @param cancelAcctUserNo
	 */
	public void setCancelAcctUserNo(String cancelAcctUserNo) {
		this.cancelAcctUserNo = cancelAcctUserNo;
	}
	
    /**
     * @return cancelAcctUserNo
     */
	public String getCancelAcctUserNo() {
		return this.cancelAcctUserNo;
	}
	
	/**
	 * @param cancelAcctUserName
	 */
	public void setCancelAcctUserName(String cancelAcctUserName) {
		this.cancelAcctUserName = cancelAcctUserName;
	}
	
    /**
     * @return cancelAcctUserName
     */
	public String getCancelAcctUserName() {
		return this.cancelAcctUserName;
	}
	
	/**
	 * @param cancelAcctAuthUserNo
	 */
	public void setCancelAcctAuthUserNo(String cancelAcctAuthUserNo) {
		this.cancelAcctAuthUserNo = cancelAcctAuthUserNo;
	}
	
    /**
     * @return cancelAcctAuthUserNo
     */
	public String getCancelAcctAuthUserNo() {
		return this.cancelAcctAuthUserNo;
	}
	
	/**
	 * @param cancelAcctAuthUserName
	 */
	public void setCancelAcctAuthUserName(String cancelAcctAuthUserName) {
		this.cancelAcctAuthUserName = cancelAcctAuthUserName;
	}
	
    /**
     * @return cancelAcctAuthUserName
     */
	public String getCancelAcctAuthUserName() {
		return this.cancelAcctAuthUserName;
	}
	
	/**
	 * @param agentFlag
	 */
	public void setAgentFlag(String agentFlag) {
		this.agentFlag = agentFlag;
	}
	
    /**
     * @return agentFlag
     */
	public String getAgentFlag() {
		return this.agentFlag;
	}
	
	/**
	 * @param rediscountInBranchId
	 */
	public void setRediscountInBranchId(String rediscountInBranchId) {
		this.rediscountInBranchId = rediscountInBranchId;
	}
	
    /**
     * @return rediscountInBranchId
     */
	public String getRediscountInBranchId() {
		return this.rediscountInBranchId;
	}
	
	/**
	 * @param rediscountInBranchNo
	 */
	public void setRediscountInBranchNo(String rediscountInBranchNo) {
		this.rediscountInBranchNo = rediscountInBranchNo;
	}
	
    /**
     * @return rediscountInBranchNo
     */
	public String getRediscountInBranchNo() {
		return this.rediscountInBranchNo;
	}
	
	/**
	 * @param credittype
	 */
	public void setCredittype(String credittype) {
		this.credittype = credittype;
	}
	
    /**
     * @return credittype
     */
	public String getCredittype() {
		return this.credittype;
	}
	
	/**
	 * @param creditrule
	 */
	public void setCreditrule(String creditrule) {
		this.creditrule = creditrule;
	}
	
    /**
     * @return creditrule
     */
	public String getCreditrule() {
		return this.creditrule;
	}
	
	/**
	 * @param creditZhbankCode
	 */
	public void setCreditZhbankCode(String creditZhbankCode) {
		this.creditZhbankCode = creditZhbankCode;
	}
	
    /**
     * @return creditZhbankCode
     */
	public String getCreditZhbankCode() {
		return this.creditZhbankCode;
	}
	
	/**
	 * @param creditZhbankName
	 */
	public void setCreditZhbankName(String creditZhbankName) {
		this.creditZhbankName = creditZhbankName;
	}
	
    /**
     * @return creditZhbankName
     */
	public String getCreditZhbankName() {
		return this.creditZhbankName;
	}
	
	/**
	 * @param improveDays
	 */
	public void setImproveDays(java.math.BigDecimal improveDays) {
		this.improveDays = improveDays;
	}
	
    /**
     * @return improveDays
     */
	public java.math.BigDecimal getImproveDays() {
		return this.improveDays;
	}
	
	/**
	 * @param yzFlag
	 */
	public void setYzFlag(String yzFlag) {
		this.yzFlag = yzFlag;
	}
	
    /**
     * @return yzFlag
     */
	public String getYzFlag() {
		return this.yzFlag;
	}
	
	/**
	 * @param coreTrandt
	 */
	public void setCoreTrandt(String coreTrandt) {
		this.coreTrandt = coreTrandt;
	}
	
    /**
     * @return coreTrandt
     */
	public String getCoreTrandt() {
		return this.coreTrandt;
	}
	
	/**
	 * @param coreTransq
	 */
	public void setCoreTransq(String coreTransq) {
		this.coreTransq = coreTransq;
	}
	
    /**
     * @return coreTransq
     */
	public String getCoreTransq() {
		return this.coreTransq;
	}
	
	/**
	 * @param quotationBatchId
	 */
	public void setQuotationBatchId(String quotationBatchId) {
		this.quotationBatchId = quotationBatchId;
	}
	
    /**
     * @return quotationBatchId
     */
	public String getQuotationBatchId() {
		return this.quotationBatchId;
	}
	
	/**
	 * @param matdt
	 */
	public void setMatdt(java.util.Date matdt) {
		this.matdt = matdt;
	}
	
    /**
     * @return matdt
     */
	public java.util.Date getMatdt() {
		return this.matdt;
	}
	
	/**
	 * @param tenordays
	 */
	public void setTenordays(String tenordays) {
		this.tenordays = tenordays;
	}
	
    /**
     * @return tenordays
     */
	public String getTenordays() {
		return this.tenordays;
	}
	
	/**
	 * @param tenordays2
	 */
	public void setTenordays2(String tenordays2) {
		this.tenordays2 = tenordays2;
	}
	
    /**
     * @return tenordays2
     */
	public String getTenordays2() {
		return this.tenordays2;
	}
	
	/**
	 * @param isDisc
	 */
	public void setIsDisc(String isDisc) {
		this.isDisc = isDisc;
	}
	
    /**
     * @return isDisc
     */
	public String getIsDisc() {
		return this.isDisc;
	}
	
	/**
	 * @param isOthercity
	 */
	public void setIsOthercity(String isOthercity) {
		this.isOthercity = isOthercity;
	}
	
    /**
     * @return isOthercity
     */
	public String getIsOthercity() {
		return this.isOthercity;
	}
	
	/**
	 * @param isPolicy
	 */
	public void setIsPolicy(String isPolicy) {
		this.isPolicy = isPolicy;
	}
	
    /**
     * @return isPolicy
     */
	public String getIsPolicy() {
		return this.isPolicy;
	}
	
	/**
	 * @param unAcctAmt
	 */
	public void setUnAcctAmt(java.math.BigDecimal unAcctAmt) {
		this.unAcctAmt = unAcctAmt;
	}
	
    /**
     * @return unAcctAmt
     */
	public java.math.BigDecimal getUnAcctAmt() {
		return this.unAcctAmt;
	}
	
	/**
	 * @param sourceType
	 */
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	
    /**
     * @return sourceType
     */
	public String getSourceType() {
		return this.sourceType;
	}
	
	/**
	 * @param sourceId
	 */
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	
    /**
     * @return sourceId
     */
	public String getSourceId() {
		return this.sourceId;
	}


}