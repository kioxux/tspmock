/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.fpt;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatSFtpRCustBusiSum
 * @类描述: bat_s_ftp_r_cust_busi_sum数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-09 17:17:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ftp_r_cust_busi_sum")
public class BatSFtpRCustBusiSum extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 客户ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "cust_id")
	private String custId;
	
	/** 客户类型 **/
	@Column(name = "cust_type", unique = false, nullable = true, length = 30)
	private String custType;
	
	/** 客户名称 **/
	@Column(name = "cust_nm", unique = false, nullable = true, length = 200)
	private String custNm;
	
	/** 客户经理 **/
	@Column(name = "mngr_cd", unique = false, nullable = true, length = 30)
	private String mngrCd;
	
	/** 机构编码 **/
	@Column(name = "org_cd", unique = false, nullable = true, length = 30)
	private String orgCd;
	
	/** 币种 **/
	@Column(name = "ccy_cd", unique = false, nullable = true, length = 30)
	private String ccyCd;
	
	/** 不良贷款余额 **/
	@Column(name = "bad_balance", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal badBalance;
	
	/** 正常类贷款余额 **/
	@Column(name = "nrml_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal nrmlBal;
	
	/** 关注类贷款余额 **/
	@Column(name = "att_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal attBal;
	
	/** 次级贷款余额 **/
	@Column(name = "scnd_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal scndBal;
	
	/** 可疑贷款余额 **/
	@Column(name = "susp_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal suspBal;
	
	/** 损失贷款余额 **/
	@Column(name = "loss_bal", unique = false, nullable = true, length = 30)
	private String lossBal;
	
	/** 行业类型 **/
	@Column(name = "indstry", unique = false, nullable = true, length = 30)
	private String indstry;
	
	/** 认定时间 **/
	@Column(name = "config_date", unique = false, nullable = true, length = 30)
	private String configDate;
	
	/** 客户风险状态 STD_CUST_RISK_STATUS **/
	@Column(name = "cust_risk_status", unique = false, nullable = true, length = 10)
	private Integer custRiskStatus;
	
	/** 逾期天数 **/
	@Column(name = "over_due_days", unique = false, nullable = true, length = 20)
	private String overDueDays;
	
	/** 信用等级 **/
	@Column(name = "rtng_cd", unique = false, nullable = true, length = 10)
	private String rtngCd;
	
	/** 证件类型 **/
	@Column(name = "cert_tp", unique = false, nullable = true, length = 20)
	private String certTp;
	
	/** 证件编号 **/
	@Column(name = "cert_no", unique = false, nullable = true, length = 32)
	private String certNo;
	
	/** 组织机构代码 **/
	@Column(name = "orgn_no", unique = false, nullable = true, length = 32)
	private String orgnNo;
	
	/** 信用卡额度 **/
	@Column(name = "card_lmt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal cardLmt;
	
	/** 信用卡余额 **/
	@Column(name = "card_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal cardBal;
	
	/** 信号主管人 **/
	@Column(name = "main_mgr", unique = false, nullable = true, length = 20)
	private String mainMgr;
	
	/** 信号主管机构 **/
	@Column(name = "main_br_id", unique = false, nullable = true, length = 20)
	private String mainBrId;
	
	/** 是否黑名单客户 STD_YES_NO **/
	@Column(name = "black_list_cust", unique = false, nullable = true, length = 1)
	private String blackListCust;
	
	/** 黑名单认定时间 **/
	@Column(name = "black_dt", unique = false, nullable = true, length = 10)
	private String blackDt;
	
	/** 是否集团客户 **/
	@Column(name = "group_cust_flag", unique = false, nullable = true, length = 2)
	private String groupCustFlag;
	
	/** 所属分行 **/
	@Column(name = "own_bank", unique = false, nullable = true, length = 10)
	private String ownBank;
	
	/** 核心客户号 **/
	@Column(name = "cust_no", unique = false, nullable = true, length = 30)
	private String custNo;
	
	/** 客户条线 **/
	@Column(name = "cntrct_bsns_line", unique = false, nullable = true, length = 100)
	private String cntrctBsnsLine;
	
	/** 逾期贷款金额 **/
	@Column(name = "overdue_loans_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal overdueLoansAmt;
	
	/** 贷款合同金额 **/
	@Column(name = "loan_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款合同余额 **/
	@Column(name = "loan_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal loanBal;
	
	/** 信贷合同金额 **/
	@Column(name = "cont_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal contAmt;
	
	/** 信贷合同余额 **/
	@Column(name = "cont_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal contBal;
	
	/** 信贷风险敞口金额 **/
	@Column(name = "cont_rsk_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal contRskAmt;
	
	/** 贷款发放金额 **/
	@Column(name = "loan_acc_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal loanAccAmt;
	
	/** 贷款余额 **/
	@Column(name = "loan_acc_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal loanAccBal;
	
	/** 信贷发放金额 **/
	@Column(name = "cont_acc_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal contAccAmt;
	
	/** 信贷余额 **/
	@Column(name = "cont_acc_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal contAccBal;
	
	/** 信贷垫款金额 **/
	@Column(name = "disk_amt", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal diskAmt;
	
	/** 信贷垫款余额 **/
	@Column(name = "disk_bal", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal diskBal;
	
	/** 授信协议额度 **/
	@Column(name = "line_sum", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal lineSum;
	
	/** 授信协议余额 **/
	@Column(name = "usbl_sum", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal usblSum;
	
	/** 预警分值 **/
	@Column(name = "rule_score", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal ruleScore;
	
	/** 数据日期 **/
	@Column(name = "data_dt", unique = false, nullable = true, length = 20)
	private String dataDt;
	
	/** 是否人工 **/
	@Column(name = "is_artificial", unique = false, nullable = true, length = 2)
	private String isArtificial;
	
	/** 人工预警等级 **/
	@Column(name = "risk_lvl_rg", unique = false, nullable = true, length = 4)
	private String riskLvlRg;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date dataDate;
	
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custType
	 */
	public void setCustType(String custType) {
		this.custType = custType;
	}
	
    /**
     * @return custType
     */
	public String getCustType() {
		return this.custType;
	}
	
	/**
	 * @param custNm
	 */
	public void setCustNm(String custNm) {
		this.custNm = custNm;
	}
	
    /**
     * @return custNm
     */
	public String getCustNm() {
		return this.custNm;
	}
	
	/**
	 * @param mngrCd
	 */
	public void setMngrCd(String mngrCd) {
		this.mngrCd = mngrCd;
	}
	
    /**
     * @return mngrCd
     */
	public String getMngrCd() {
		return this.mngrCd;
	}
	
	/**
	 * @param orgCd
	 */
	public void setOrgCd(String orgCd) {
		this.orgCd = orgCd;
	}
	
    /**
     * @return orgCd
     */
	public String getOrgCd() {
		return this.orgCd;
	}
	
	/**
	 * @param ccyCd
	 */
	public void setCcyCd(String ccyCd) {
		this.ccyCd = ccyCd;
	}
	
    /**
     * @return ccyCd
     */
	public String getCcyCd() {
		return this.ccyCd;
	}
	
	/**
	 * @param badBalance
	 */
	public void setBadBalance(java.math.BigDecimal badBalance) {
		this.badBalance = badBalance;
	}
	
    /**
     * @return badBalance
     */
	public java.math.BigDecimal getBadBalance() {
		return this.badBalance;
	}
	
	/**
	 * @param nrmlBal
	 */
	public void setNrmlBal(java.math.BigDecimal nrmlBal) {
		this.nrmlBal = nrmlBal;
	}
	
    /**
     * @return nrmlBal
     */
	public java.math.BigDecimal getNrmlBal() {
		return this.nrmlBal;
	}
	
	/**
	 * @param attBal
	 */
	public void setAttBal(java.math.BigDecimal attBal) {
		this.attBal = attBal;
	}
	
    /**
     * @return attBal
     */
	public java.math.BigDecimal getAttBal() {
		return this.attBal;
	}
	
	/**
	 * @param scndBal
	 */
	public void setScndBal(java.math.BigDecimal scndBal) {
		this.scndBal = scndBal;
	}
	
    /**
     * @return scndBal
     */
	public java.math.BigDecimal getScndBal() {
		return this.scndBal;
	}
	
	/**
	 * @param suspBal
	 */
	public void setSuspBal(java.math.BigDecimal suspBal) {
		this.suspBal = suspBal;
	}
	
    /**
     * @return suspBal
     */
	public java.math.BigDecimal getSuspBal() {
		return this.suspBal;
	}
	
	/**
	 * @param lossBal
	 */
	public void setLossBal(String lossBal) {
		this.lossBal = lossBal;
	}
	
    /**
     * @return lossBal
     */
	public String getLossBal() {
		return this.lossBal;
	}
	
	/**
	 * @param indstry
	 */
	public void setIndstry(String indstry) {
		this.indstry = indstry;
	}
	
    /**
     * @return indstry
     */
	public String getIndstry() {
		return this.indstry;
	}
	
	/**
	 * @param configDate
	 */
	public void setConfigDate(String configDate) {
		this.configDate = configDate;
	}
	
    /**
     * @return configDate
     */
	public String getConfigDate() {
		return this.configDate;
	}
	
	/**
	 * @param custRiskStatus
	 */
	public void setCustRiskStatus(Integer custRiskStatus) {
		this.custRiskStatus = custRiskStatus;
	}
	
    /**
     * @return custRiskStatus
     */
	public Integer getCustRiskStatus() {
		return this.custRiskStatus;
	}
	
	/**
	 * @param overDueDays
	 */
	public void setOverDueDays(String overDueDays) {
		this.overDueDays = overDueDays;
	}
	
    /**
     * @return overDueDays
     */
	public String getOverDueDays() {
		return this.overDueDays;
	}
	
	/**
	 * @param rtngCd
	 */
	public void setRtngCd(String rtngCd) {
		this.rtngCd = rtngCd;
	}
	
    /**
     * @return rtngCd
     */
	public String getRtngCd() {
		return this.rtngCd;
	}
	
	/**
	 * @param certTp
	 */
	public void setCertTp(String certTp) {
		this.certTp = certTp;
	}
	
    /**
     * @return certTp
     */
	public String getCertTp() {
		return this.certTp;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}
	
    /**
     * @return certNo
     */
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param orgnNo
	 */
	public void setOrgnNo(String orgnNo) {
		this.orgnNo = orgnNo;
	}
	
    /**
     * @return orgnNo
     */
	public String getOrgnNo() {
		return this.orgnNo;
	}
	
	/**
	 * @param cardLmt
	 */
	public void setCardLmt(java.math.BigDecimal cardLmt) {
		this.cardLmt = cardLmt;
	}
	
    /**
     * @return cardLmt
     */
	public java.math.BigDecimal getCardLmt() {
		return this.cardLmt;
	}
	
	/**
	 * @param cardBal
	 */
	public void setCardBal(java.math.BigDecimal cardBal) {
		this.cardBal = cardBal;
	}
	
    /**
     * @return cardBal
     */
	public java.math.BigDecimal getCardBal() {
		return this.cardBal;
	}
	
	/**
	 * @param mainMgr
	 */
	public void setMainMgr(String mainMgr) {
		this.mainMgr = mainMgr;
	}
	
    /**
     * @return mainMgr
     */
	public String getMainMgr() {
		return this.mainMgr;
	}
	
	/**
	 * @param mainBrId
	 */
	public void setMainBrId(String mainBrId) {
		this.mainBrId = mainBrId;
	}
	
    /**
     * @return mainBrId
     */
	public String getMainBrId() {
		return this.mainBrId;
	}
	
	/**
	 * @param blackListCust
	 */
	public void setBlackListCust(String blackListCust) {
		this.blackListCust = blackListCust;
	}
	
    /**
     * @return blackListCust
     */
	public String getBlackListCust() {
		return this.blackListCust;
	}
	
	/**
	 * @param blackDt
	 */
	public void setBlackDt(String blackDt) {
		this.blackDt = blackDt;
	}
	
    /**
     * @return blackDt
     */
	public String getBlackDt() {
		return this.blackDt;
	}
	
	/**
	 * @param groupCustFlag
	 */
	public void setGroupCustFlag(String groupCustFlag) {
		this.groupCustFlag = groupCustFlag;
	}
	
    /**
     * @return groupCustFlag
     */
	public String getGroupCustFlag() {
		return this.groupCustFlag;
	}
	
	/**
	 * @param ownBank
	 */
	public void setOwnBank(String ownBank) {
		this.ownBank = ownBank;
	}
	
    /**
     * @return ownBank
     */
	public String getOwnBank() {
		return this.ownBank;
	}
	
	/**
	 * @param custNo
	 */
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	
    /**
     * @return custNo
     */
	public String getCustNo() {
		return this.custNo;
	}
	
	/**
	 * @param cntrctBsnsLine
	 */
	public void setCntrctBsnsLine(String cntrctBsnsLine) {
		this.cntrctBsnsLine = cntrctBsnsLine;
	}
	
    /**
     * @return cntrctBsnsLine
     */
	public String getCntrctBsnsLine() {
		return this.cntrctBsnsLine;
	}
	
	/**
	 * @param overdueLoansAmt
	 */
	public void setOverdueLoansAmt(java.math.BigDecimal overdueLoansAmt) {
		this.overdueLoansAmt = overdueLoansAmt;
	}
	
    /**
     * @return overdueLoansAmt
     */
	public java.math.BigDecimal getOverdueLoansAmt() {
		return this.overdueLoansAmt;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBal
	 */
	public void setLoanBal(java.math.BigDecimal loanBal) {
		this.loanBal = loanBal;
	}
	
    /**
     * @return loanBal
     */
	public java.math.BigDecimal getLoanBal() {
		return this.loanBal;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param contBal
	 */
	public void setContBal(java.math.BigDecimal contBal) {
		this.contBal = contBal;
	}
	
    /**
     * @return contBal
     */
	public java.math.BigDecimal getContBal() {
		return this.contBal;
	}
	
	/**
	 * @param contRskAmt
	 */
	public void setContRskAmt(java.math.BigDecimal contRskAmt) {
		this.contRskAmt = contRskAmt;
	}
	
    /**
     * @return contRskAmt
     */
	public java.math.BigDecimal getContRskAmt() {
		return this.contRskAmt;
	}
	
	/**
	 * @param loanAccAmt
	 */
	public void setLoanAccAmt(java.math.BigDecimal loanAccAmt) {
		this.loanAccAmt = loanAccAmt;
	}
	
    /**
     * @return loanAccAmt
     */
	public java.math.BigDecimal getLoanAccAmt() {
		return this.loanAccAmt;
	}
	
	/**
	 * @param loanAccBal
	 */
	public void setLoanAccBal(java.math.BigDecimal loanAccBal) {
		this.loanAccBal = loanAccBal;
	}
	
    /**
     * @return loanAccBal
     */
	public java.math.BigDecimal getLoanAccBal() {
		return this.loanAccBal;
	}
	
	/**
	 * @param contAccAmt
	 */
	public void setContAccAmt(java.math.BigDecimal contAccAmt) {
		this.contAccAmt = contAccAmt;
	}
	
    /**
     * @return contAccAmt
     */
	public java.math.BigDecimal getContAccAmt() {
		return this.contAccAmt;
	}
	
	/**
	 * @param contAccBal
	 */
	public void setContAccBal(java.math.BigDecimal contAccBal) {
		this.contAccBal = contAccBal;
	}
	
    /**
     * @return contAccBal
     */
	public java.math.BigDecimal getContAccBal() {
		return this.contAccBal;
	}
	
	/**
	 * @param diskAmt
	 */
	public void setDiskAmt(java.math.BigDecimal diskAmt) {
		this.diskAmt = diskAmt;
	}
	
    /**
     * @return diskAmt
     */
	public java.math.BigDecimal getDiskAmt() {
		return this.diskAmt;
	}
	
	/**
	 * @param diskBal
	 */
	public void setDiskBal(java.math.BigDecimal diskBal) {
		this.diskBal = diskBal;
	}
	
    /**
     * @return diskBal
     */
	public java.math.BigDecimal getDiskBal() {
		return this.diskBal;
	}
	
	/**
	 * @param lineSum
	 */
	public void setLineSum(java.math.BigDecimal lineSum) {
		this.lineSum = lineSum;
	}
	
    /**
     * @return lineSum
     */
	public java.math.BigDecimal getLineSum() {
		return this.lineSum;
	}
	
	/**
	 * @param usblSum
	 */
	public void setUsblSum(java.math.BigDecimal usblSum) {
		this.usblSum = usblSum;
	}
	
    /**
     * @return usblSum
     */
	public java.math.BigDecimal getUsblSum() {
		return this.usblSum;
	}
	
	/**
	 * @param ruleScore
	 */
	public void setRuleScore(java.math.BigDecimal ruleScore) {
		this.ruleScore = ruleScore;
	}
	
    /**
     * @return ruleScore
     */
	public java.math.BigDecimal getRuleScore() {
		return this.ruleScore;
	}
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param isArtificial
	 */
	public void setIsArtificial(String isArtificial) {
		this.isArtificial = isArtificial;
	}
	
    /**
     * @return isArtificial
     */
	public String getIsArtificial() {
		return this.isArtificial;
	}
	
	/**
	 * @param riskLvlRg
	 */
	public void setRiskLvlRg(String riskLvlRg) {
		this.riskLvlRg = riskLvlRg;
	}
	
    /**
     * @return riskLvlRg
     */
	public String getRiskLvlRg() {
		return this.riskLvlRg;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(java.util.Date dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public java.util.Date getDataDate() {
		return this.dataDate;
	}


}