package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0103</br>
 * 任务名称：加工任务-业务处理-银承台账处理 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0103Mapper {
    /**
     * 清空临时表-综合票据系统-历史表-承兑批次表
     */
   // void truncateTmpPjpBtCcceptionBatch();



    /**
     * 票据表中同一笔票据存在正常与作废，去掉作废
     *
     * @return
     */
    int deletebtAcception( );


    /**
     * 插入临时表-综合票据系统-历史表-承兑批次表
     *
     * @return
     */
    int insertTmpPjpBtCcceptionBatch(@Param("openDay") String openDay);

    /**
     * 清空综合票据系统-临时表关联大库表-票据加工临时表
     */
   // void truncateTmpPjpEdratf();

  //  void truncateTmpAccAccp();
  //  int insertTmpAccAccp(@Param("openDay") String openDay);

  //  void truncateTmpPjgDrafaccEdratf();
    int insertTmpPjgDrafaccEdratf(@Param("openDay") String openDay);

  //  void truncateTmpApprLmtSub();
    int insertTmpApprLmtSub(@Param("openDay") String openDay);

    /**
     * 插入综合票据系统-临时表关联大库表-票据加工临时表
     *
     * @return
     */
    int insertTmpPjpEdratf(@Param("openDay") String openDay);

    /**
     * 插入票据加工临时表
     *
     * @param paramMap
     * @return
     */
    int insertTmpPjp(Map paramMap);


    /**
     * 插入票据台账表
     *
     * @param openDay
     * @return
     */
    int updateAccAccp01(@Param("openDay") String openDay);

    /**
     * 插入 临时表-银承台账处理
     *
     * @param openDay
     * @return
     */
    int insertTmpPjpDrafaccAccp(@Param("openDay") String openDay);

    /**
     * 更新票据台账表，更新  银承敞口金额、保证金
     *
     * @param openDay
     * @return
     */
    int updateAccAccp02(@Param("openDay") String openDay);

    /**
     *
     * 删除银承台账B
     * @param openDay
     * @return
     */
    int updateAccAccp4B(@Param("openDay") String openDay);

    /**
     * 插入-临时表银承台账
     *
     * @param openDay
     * @return
     */
    int insertTmpAccAccpCmis2(@Param("openDay") String openDay);


    /**
     * 插入银承台账票据明细
     *
     * @param openDay
     * @return
     */
    int insertAccAccpDrftSub(@Param("openDay") String openDay);

    /**
     * 更新  银承票据明细 保证金 、状态
     *
     * @param openDay
     * @return
     */
    int updateAccAccpDrftSub01(@Param("openDay") String openDay);

    /**
     * 插入-临时表银承台账
     *
     * @param openDay
     * @return
     */
    int insertTmpAccAccpCmis3(@Param("openDay") String openDay);

    /**
     *
     * 插入-综合票据系统-临时表-银承台账处理
     * @param openDay
     * @return
     */
    int insertTmpPjpDrafaccAccp4A(@Param("openDay") String openDay);//add 20211021

    /**
     *
     * 更新银承台账4A
     * @param openDay
     * @return
     */
    int updateAccAccp4A(@Param("openDay") String openDay);//add 20211021


    /**
     * 插入根据票据明细的状态处理银承台账
     *
     * @param openDay
     * @return
     */
    int insertTmpBizAccAccp1(@Param("openDay") String openDay);

    /**
     * 插入根据票据明细的状态处理银承台账
     *
     * @param openDay
     * @return
     */
    int insertTmpBizAccAccp2(@Param("openDay") String openDay);


    /**
     * 临时表-根据票据明细的状态处理银承台账
     *
     * @param openDay
     * @return
     */
    int insertTmpBizAccAccp(@Param("openDay") String openDay);

    /**
     * 依据票据明细的状态  更新  银承 台账的  状态
     *
     * @param openDay
     * @return
     */
    int updateAccAccpDrftSub02(@Param("openDay") String openDay);


    /**
     * 插入临时表银承台账票据明细-核心银承编号
     *
     * @param openDay
     * @return
     */
   // int insertTmpAccAccpSub(@Param("openDay") String openDay);

    /**
     * 不存在票据明细中的票据直接作废
     *
     * @param openDay
     * @return
     */
  //  int updateAccAccpAccStatus01(@Param("openDay") String openDay);

    /**
     * 到期垫款
     *
     * @param openDay
     * @return
     */
    int updateAccAccp03(@Param("openDay") String openDay);

    /**
     * 垫款结清
     *
     * @param openDay
     * @return
     */
    int updateAccAccp04(@Param("openDay") String openDay);


  /**
   *  删除票据汇总临时表
   *
   * @param openDay
   * @return
   */
  int deleteTmpAccCalculate(@Param("openDay") String openDay);


  /**
   * 插入票据汇总临时表
   *
   * @param openDay
   * @return
   */
  int insertTmpAccCalculate(@Param("openDay") String openDay);



}
