/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpLoanContInfo
 * @类描述: bat_t_rcp_loan_cont_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-15 10:22:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_rcp_loan_cont_info")
public class BatTRcpLoanContInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "app_no")
	private String appNo;
	
	/** 第三方流水号 **/
	@Column(name = "tp_no", unique = false, nullable = true, length = 128)
	private String tpNo;
	
	/** 渠道来源 **/
	@Column(name = "channel_type", unique = false, nullable = true, length = 8)
	private String channelType;
	
	/** 合作平台 **/
	@Column(name = "co_platform", unique = false, nullable = true, length = 16)
	private String coPlatform;
	
	/** 贷款属性 **/
	@Column(name = "loan_prop", unique = false, nullable = true, length = 8)
	private String loanProp;
	
	/** 产品类别 **/
	@Column(name = "prd_type", unique = false, nullable = true, length = 24)
	private String prdType;
	
	/** 产品代码 **/
	@Column(name = "prd_code", unique = false, nullable = true, length = 48)
	private String prdCode;
	
	/** 第三方对象ID **/
	@Column(name = "tp_obj_id", unique = false, nullable = true, length = 240)
	private String tpObjId;
	
	/** 网贷内部客户号 **/
	@Id
	@Column(name = "cust_id", unique = false, nullable = true, length = 64)
	private String custId;
	
	/** 核心客户号 **/
	@Column(name = "cust_id_core", unique = false, nullable = true, length = 64)
	private String custIdCore;
	
	/** 客户姓名 **/
	@Column(name = "cust_name", unique = false, nullable = true, length = 240)
	private String custName;
	
	/** 证件类型 10 身份证 **/
	@Column(name = "cert_type", unique = false, nullable = true, length = 8)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "cert_code", unique = false, nullable = true, length = 80)
	private String certCode;
	
	/** 签约类型 **/
	@Column(name = "sign_type", unique = false, nullable = true, length = 4)
	private String signType;
	
	/** 授信合同编号 **/
	@Column(name = "crd_cont_no", unique = false, nullable = true, length = 400)
	private String crdContNo;
	
	/** 借据编号 **/
	@Id
	@Column(name = "bill_no", unique = false, nullable = false, length = 400)
	private String billNo;
	
	/** 贷款大类 **/
	@Column(name = "loan_main_type", unique = false, nullable = true, length = 80)
	private String loanMainType;
	
	/** 贷款细类 **/
	@Column(name = "loan_sub_type", unique = false, nullable = true, length = 24)
	private String loanSubType;
	
	/** 主担保方式 **/
	@Column(name = "assure_main_type", unique = false, nullable = true, length = 12)
	private String assureMainType;
	
	/** 贷款用途 **/
	@Column(name = "loan_purpose", unique = false, nullable = true, length = 12)
	private String loanPurpose;
	
	/** 贷款用途细类 **/
	@Column(name = "loan_purpose_sub", unique = false, nullable = true, length = 12)
	private String loanPurposeSub;
	
	/** 币种 **/
	@Column(name = "currency", unique = false, nullable = true, length = 12)
	private String currency;
	
	/** 放款账户开户行行号 **/
	@Column(name = "loan_acc_bank", unique = false, nullable = true, length = 80)
	private String loanAccBank;
	
	/** 放款账号 **/
	@Column(name = "loan_acc", unique = false, nullable = true, length = 320)
	private String loanAcc;
	
	/** 放款卡号 **/
	@Column(name = "loan_card_no", unique = false, nullable = true, length = 320)
	private String loanCardNo;
	
	/** 还款账户开户行行号 **/
	@Column(name = "repay_acc_bank", unique = false, nullable = true, length = 80)
	private String repayAccBank;
	
	/** 还款账号 **/
	@Column(name = "repay_acc", unique = false, nullable = true, length = 320)
	private String repayAcc;
	
	/** 还款卡号 **/
	@Column(name = "repay_card_no", unique = false, nullable = true, length = 320)
	private String repayCardNo;
	
	/** 核心入账账号 **/
	@Column(name = "core_in_acc", unique = false, nullable = true, length = 80)
	private String coreInAcc;
	
	/** 贷款期限 **/
	@Column(name = "loan_term", unique = false, nullable = true, length = 24)
	private String loanTerm;
	
	/** 贷款起始日期 **/
	@Column(name = "loan_start_date", unique = false, nullable = true, length = 40)
	private String loanStartDate;
	
	/** 贷款到期日期 **/
	@Column(name = "loan_end_date", unique = false, nullable = true, length = 40)
	private String loanEndDate;
	
	/** 贷款金额 **/
	@Column(name = "loan_amt", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	@Column(name = "loan_bal", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal loanBal;
	
	/** 贷款结清日 **/
	@Column(name = "loan_clear_date", unique = false, nullable = true, length = 40)
	private String loanClearDate;
	
	/** 五级分类 **/
	@Column(name = "five_cate", unique = false, nullable = true, length = 8)
	private String fiveCate;
	
	/** 最早逾期日期(蚂蚁借呗专用) **/
	@Column(name = "overdue_date", unique = false, nullable = true, length = 40)
	private String overdueDate;
	
	/** 最早逾期标志 **/
	@Column(name = "overdue_flag", unique = false, nullable = true, length = 4)
	private String overdueFlag;
	
	/** 签订时间 **/
	@Column(name = "sign_time", unique = false, nullable = true, length = 80)
	private String signTime;
	
	/** 行业投向 **/
	@Column(name = "industry_dest", unique = false, nullable = true, length = 20)
	private String industryDest;
	
	/** 还款方式 **/
	@Column(name = "repay_type", unique = false, nullable = true, length = 8)
	private String repayType;
	
	/** 还款日类型 **/
	@Column(name = "repay_date_type", unique = false, nullable = true, length = 4)
	private String repayDateType;
	
	/** 利率调整方式 **/
	@Column(name = "rate_adj_type", unique = false, nullable = true, length = 4)
	private String rateAdjType;
	
	/** 利率调整月日数 **/
	@Column(name = "rate_adj_day", unique = false, nullable = true, length = 4)
	private String rateAdjDay;
	
	/** 计息周期 **/
	@Column(name = "interest_cycle", unique = false, nullable = true, length = 4)
	private String interestCycle;
	
	/** 扣款日期 **/
	@Column(name = "repay_day", unique = false, nullable = true, length = 8)
	private String repayDay;
	
	/** 固定周期 **/
	@Column(name = "fixed_cycle", unique = false, nullable = true, length = 10)
	private Integer fixedCycle;
	
	/** 罚息类型 **/
	@Column(name = "p_rate_type", unique = false, nullable = true, length = 8)
	private String pRateType;
	
	/** 罚息浮动利率 **/
	@Column(name = "p_rate_float", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal pRateFloat;
	
	/** 执行年利率（%） **/
	@Column(name = "exec_rate", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal execRate;
	
	/** 是否固定利率 **/
	@Column(name = "fixed_rate_flag", unique = false, nullable = true, length = 4)
	private String fixedRateFlag;
	
	/** 浮动方式（备用） **/
	@Column(name = "float_type", unique = false, nullable = true, length = 4)
	private String floatType;
	
	/** 浮动点数（备用） **/
	@Column(name = "float_point", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal floatPoint;
	
	/** 浮动比例（%） **/
	@Column(name = "float_scale", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal floatScale;
	
	/** 展期后生效利率生效方式（备用） **/
	@Column(name = "ext_rate_type", unique = false, nullable = true, length = 4)
	private String extRateType;
	
	/** 是否收罚息标志 **/
	@Column(name = "p_rate_flag", unique = false, nullable = true, length = 4)
	private String pRateFlag;
	
	/** 核心前台流水号 **/
	@Column(name = "core_front_no", unique = false, nullable = true, length = 80)
	private String coreFrontNo;
	
	/** 合同状态 LOAN_CONT_STATUS 101 合同未签订 102 审批作废 103 已签订 104 贷款作废 105 已放款 106 已结清 107 逾期 **/
	@Column(name = "cont_status", unique = false, nullable = true, length = 12)
	private String contStatus;
	
	/** 客户经理 **/
	@Column(name = "biz_manager_id", unique = false, nullable = true, length = 80)
	private String bizManagerId;
	
	/** 所属机构 **/
	@Column(name = "biz_org_id", unique = false, nullable = true, length = 40)
	private String bizOrgId;
	
	/** 录入日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 40)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "input_time", unique = false, nullable = true, length = 80)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "last_update_date", unique = false, nullable = true, length = 40)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "last_update_time", unique = false, nullable = true, length = 80)
	private String lastUpdateTime;
	
	/** 审批数据最新统计年月 **/
	@Column(name = "appr_data_ym", unique = false, nullable = true, length = 24)
	private String apprDataYm;
	
	/** 欠息金额 **/
	@Column(name = "overdue_interest", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal overdueInterest;
	
	/** 逾期本金 **/
	@Column(name = "overdue_amt", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal overdueAmt;
	
	/** 推介名单状态 **/
	@Column(name = "recommend_status", unique = false, nullable = true, length = 4)
	private String recommendStatus;
	
	/** 未结利息 **/
	@Column(name = "outstd_intst", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal outstdIntst;
	
	/** 表内利息 **/
	@Column(name = "inner_intst", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal innerIntst;
	
	/** 表外利息 **/
	@Column(name = "outer_intst", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal outerIntst;
	
	/** 逾期天数 **/
	@Column(name = "overdue_day", unique = false, nullable = true, length = 10)
	private Integer overdueDay;
	
	/** 合同编号（中文） **/
	@Column(name = "cn_cont_no", unique = false, nullable = true, length = 400)
	private String cnContNo;
	
	/** 编号 **/
	@Column(name = "cn_num", unique = false, nullable = true, length = 80)
	private String cnNum;
	
	/** 合同影像编号 **/
	@Column(name = "cont_image_no", unique = false, nullable = true, length = 400)
	private String contImageNo;
	
	/** 贷款归属机构 **/
	@Column(name = "manage_org_id", unique = false, nullable = true, length = 80)
	private String manageOrgId;
	
	/** 贷款投向 **/
	@Column(name = "loan_direction", unique = false, nullable = true, length = 40)
	private String loanDirection;
	
	/** 替补还款账号 **/
	@Column(name = "repay_acc_tb", unique = false, nullable = true, length = 80)
	private String repayAccTb;
	
	/** 推荐人工号 **/
	@Column(name = "referee_no", unique = false, nullable = true, length = 80)
	private String refereeNo;
	
	/** 放款账户开户行行名 **/
	@Column(name = "loan_acc_bank_name", unique = false, nullable = true, length = 400)
	private String loanAccBankName;
	
	/** 还款账户开户行行名 **/
	@Column(name = "repay_acc_bank_name", unique = false, nullable = true, length = 400)
	private String repayAccBankName;
	
	/** 放款账户户名 **/
	@Column(name = "loan_acc_name", unique = false, nullable = true, length = 400)
	private String loanAccName;
	
	/** LPR执行年利率（%） **/
	@Column(name = "lpr_rate", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal lprRate;
	
	/** LPR选取日期 **/
	@Column(name = "lpr_date", unique = false, nullable = true, length = 40)
	private String lprDate;
	
	/** LPR-BP点 **/
	@Column(name = "lpr_bp", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal lprBp;
	
	/** LPR存期 **/
	@Column(name = "lpr_term", unique = false, nullable = true, length = 40)
	private String lprTerm;
	
	/** LPR基点类型 **/
	@Column(name = "lpr_bp_type", unique = false, nullable = true, length = 40)
	private String lprBpType;
	
	/** LPR利率代码 **/
	@Column(name = "lpr_rate_code", unique = false, nullable = true, length = 40)
	private String lprRateCode;
	
	/** LPR利率调整方式（浮动利率采用） **/
	@Column(name = "lpr_adjust_type", unique = false, nullable = true, length = 40)
	private String lprAdjustType;
	
	/** 对方账号开户行号 **/
	@Column(name = "khh_no", unique = false, nullable = true, length = 400)
	private String khhNo;
	
	/** 对方账号开户行名 **/
	@Column(name = "khh_name", unique = false, nullable = true, length = 400)
	private String khhName;
	
	/** 对方账号 **/
	@Column(name = "trade_partner_acc", unique = false, nullable = true, length = 400)
	private String tradePartnerAcc;
	
	/** 对方账户名 **/
	@Column(name = "trade_partner_name", unique = false, nullable = true, length = 400)
	private String tradePartnerName;
	
	/** 对方账号种类1--本行账号2--他行账号 **/
	@Column(name = "is_self_bank_acc", unique = false, nullable = true, length = 400)
	private String isSelfBankAcc;
	
	/** 交易对手名称 **/
	@Column(name = "shoutuo_name", unique = false, nullable = true, length = 400)
	private String shoutuoName;
	
	/** 受托支付账号 **/
	@Column(name = "shoutuo_account", unique = false, nullable = true, length = 400)
	private String shoutuoAccount;
	
	/** 客户提款金额 **/
	@Column(name = "busi_amt", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal busiAmt;
	
	/** 机构方出资金额 **/
	@Column(name = "org_amt", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal orgAmt;
	
	/** 宽限期 **/
	@Column(name = "grace_date", unique = false, nullable = true, length = 20)
	private String graceDate;
	
	/** 罚息利率 **/
	@Column(name = "fine_rate", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal fineRate;
	
	/** 放款期限类型 1：短期 2：中长期 **/
	@Column(name = "term_type", unique = false, nullable = true, length = 100)
	private String termType;
	
	
	/**
	 * @param appNo
	 */
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	
    /**
     * @return appNo
     */
	public String getAppNo() {
		return this.appNo;
	}
	
	/**
	 * @param tpNo
	 */
	public void setTpNo(String tpNo) {
		this.tpNo = tpNo;
	}
	
    /**
     * @return tpNo
     */
	public String getTpNo() {
		return this.tpNo;
	}
	
	/**
	 * @param channelType
	 */
	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}
	
    /**
     * @return channelType
     */
	public String getChannelType() {
		return this.channelType;
	}
	
	/**
	 * @param coPlatform
	 */
	public void setCoPlatform(String coPlatform) {
		this.coPlatform = coPlatform;
	}
	
    /**
     * @return coPlatform
     */
	public String getCoPlatform() {
		return this.coPlatform;
	}
	
	/**
	 * @param loanProp
	 */
	public void setLoanProp(String loanProp) {
		this.loanProp = loanProp;
	}
	
    /**
     * @return loanProp
     */
	public String getLoanProp() {
		return this.loanProp;
	}
	
	/**
	 * @param prdType
	 */
	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}
	
    /**
     * @return prdType
     */
	public String getPrdType() {
		return this.prdType;
	}
	
	/**
	 * @param prdCode
	 */
	public void setPrdCode(String prdCode) {
		this.prdCode = prdCode;
	}
	
    /**
     * @return prdCode
     */
	public String getPrdCode() {
		return this.prdCode;
	}
	
	/**
	 * @param tpObjId
	 */
	public void setTpObjId(String tpObjId) {
		this.tpObjId = tpObjId;
	}
	
    /**
     * @return tpObjId
     */
	public String getTpObjId() {
		return this.tpObjId;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custIdCore
	 */
	public void setCustIdCore(String custIdCore) {
		this.custIdCore = custIdCore;
	}
	
    /**
     * @return custIdCore
     */
	public String getCustIdCore() {
		return this.custIdCore;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param signType
	 */
	public void setSignType(String signType) {
		this.signType = signType;
	}
	
    /**
     * @return signType
     */
	public String getSignType() {
		return this.signType;
	}
	
	/**
	 * @param crdContNo
	 */
	public void setCrdContNo(String crdContNo) {
		this.crdContNo = crdContNo;
	}
	
    /**
     * @return crdContNo
     */
	public String getCrdContNo() {
		return this.crdContNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param loanMainType
	 */
	public void setLoanMainType(String loanMainType) {
		this.loanMainType = loanMainType;
	}
	
    /**
     * @return loanMainType
     */
	public String getLoanMainType() {
		return this.loanMainType;
	}
	
	/**
	 * @param loanSubType
	 */
	public void setLoanSubType(String loanSubType) {
		this.loanSubType = loanSubType;
	}
	
    /**
     * @return loanSubType
     */
	public String getLoanSubType() {
		return this.loanSubType;
	}
	
	/**
	 * @param assureMainType
	 */
	public void setAssureMainType(String assureMainType) {
		this.assureMainType = assureMainType;
	}
	
    /**
     * @return assureMainType
     */
	public String getAssureMainType() {
		return this.assureMainType;
	}
	
	/**
	 * @param loanPurpose
	 */
	public void setLoanPurpose(String loanPurpose) {
		this.loanPurpose = loanPurpose;
	}
	
    /**
     * @return loanPurpose
     */
	public String getLoanPurpose() {
		return this.loanPurpose;
	}
	
	/**
	 * @param loanPurposeSub
	 */
	public void setLoanPurposeSub(String loanPurposeSub) {
		this.loanPurposeSub = loanPurposeSub;
	}
	
    /**
     * @return loanPurposeSub
     */
	public String getLoanPurposeSub() {
		return this.loanPurposeSub;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    /**
     * @return currency
     */
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param loanAccBank
	 */
	public void setLoanAccBank(String loanAccBank) {
		this.loanAccBank = loanAccBank;
	}
	
    /**
     * @return loanAccBank
     */
	public String getLoanAccBank() {
		return this.loanAccBank;
	}
	
	/**
	 * @param loanAcc
	 */
	public void setLoanAcc(String loanAcc) {
		this.loanAcc = loanAcc;
	}
	
    /**
     * @return loanAcc
     */
	public String getLoanAcc() {
		return this.loanAcc;
	}
	
	/**
	 * @param loanCardNo
	 */
	public void setLoanCardNo(String loanCardNo) {
		this.loanCardNo = loanCardNo;
	}
	
    /**
     * @return loanCardNo
     */
	public String getLoanCardNo() {
		return this.loanCardNo;
	}
	
	/**
	 * @param repayAccBank
	 */
	public void setRepayAccBank(String repayAccBank) {
		this.repayAccBank = repayAccBank;
	}
	
    /**
     * @return repayAccBank
     */
	public String getRepayAccBank() {
		return this.repayAccBank;
	}
	
	/**
	 * @param repayAcc
	 */
	public void setRepayAcc(String repayAcc) {
		this.repayAcc = repayAcc;
	}
	
    /**
     * @return repayAcc
     */
	public String getRepayAcc() {
		return this.repayAcc;
	}
	
	/**
	 * @param repayCardNo
	 */
	public void setRepayCardNo(String repayCardNo) {
		this.repayCardNo = repayCardNo;
	}
	
    /**
     * @return repayCardNo
     */
	public String getRepayCardNo() {
		return this.repayCardNo;
	}
	
	/**
	 * @param coreInAcc
	 */
	public void setCoreInAcc(String coreInAcc) {
		this.coreInAcc = coreInAcc;
	}
	
    /**
     * @return coreInAcc
     */
	public String getCoreInAcc() {
		return this.coreInAcc;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return loanTerm
     */
	public String getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}
	
    /**
     * @return loanStartDate
     */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}
	
    /**
     * @return loanEndDate
     */
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBal
	 */
	public void setLoanBal(java.math.BigDecimal loanBal) {
		this.loanBal = loanBal;
	}
	
    /**
     * @return loanBal
     */
	public java.math.BigDecimal getLoanBal() {
		return this.loanBal;
	}
	
	/**
	 * @param loanClearDate
	 */
	public void setLoanClearDate(String loanClearDate) {
		this.loanClearDate = loanClearDate;
	}
	
    /**
     * @return loanClearDate
     */
	public String getLoanClearDate() {
		return this.loanClearDate;
	}
	
	/**
	 * @param fiveCate
	 */
	public void setFiveCate(String fiveCate) {
		this.fiveCate = fiveCate;
	}
	
    /**
     * @return fiveCate
     */
	public String getFiveCate() {
		return this.fiveCate;
	}
	
	/**
	 * @param overdueDate
	 */
	public void setOverdueDate(String overdueDate) {
		this.overdueDate = overdueDate;
	}
	
    /**
     * @return overdueDate
     */
	public String getOverdueDate() {
		return this.overdueDate;
	}
	
	/**
	 * @param overdueFlag
	 */
	public void setOverdueFlag(String overdueFlag) {
		this.overdueFlag = overdueFlag;
	}
	
    /**
     * @return overdueFlag
     */
	public String getOverdueFlag() {
		return this.overdueFlag;
	}
	
	/**
	 * @param signTime
	 */
	public void setSignTime(String signTime) {
		this.signTime = signTime;
	}
	
    /**
     * @return signTime
     */
	public String getSignTime() {
		return this.signTime;
	}
	
	/**
	 * @param industryDest
	 */
	public void setIndustryDest(String industryDest) {
		this.industryDest = industryDest;
	}
	
    /**
     * @return industryDest
     */
	public String getIndustryDest() {
		return this.industryDest;
	}
	
	/**
	 * @param repayType
	 */
	public void setRepayType(String repayType) {
		this.repayType = repayType;
	}
	
    /**
     * @return repayType
     */
	public String getRepayType() {
		return this.repayType;
	}
	
	/**
	 * @param repayDateType
	 */
	public void setRepayDateType(String repayDateType) {
		this.repayDateType = repayDateType;
	}
	
    /**
     * @return repayDateType
     */
	public String getRepayDateType() {
		return this.repayDateType;
	}
	
	/**
	 * @param rateAdjType
	 */
	public void setRateAdjType(String rateAdjType) {
		this.rateAdjType = rateAdjType;
	}
	
    /**
     * @return rateAdjType
     */
	public String getRateAdjType() {
		return this.rateAdjType;
	}
	
	/**
	 * @param rateAdjDay
	 */
	public void setRateAdjDay(String rateAdjDay) {
		this.rateAdjDay = rateAdjDay;
	}
	
    /**
     * @return rateAdjDay
     */
	public String getRateAdjDay() {
		return this.rateAdjDay;
	}
	
	/**
	 * @param interestCycle
	 */
	public void setInterestCycle(String interestCycle) {
		this.interestCycle = interestCycle;
	}
	
    /**
     * @return interestCycle
     */
	public String getInterestCycle() {
		return this.interestCycle;
	}
	
	/**
	 * @param repayDay
	 */
	public void setRepayDay(String repayDay) {
		this.repayDay = repayDay;
	}
	
    /**
     * @return repayDay
     */
	public String getRepayDay() {
		return this.repayDay;
	}
	
	/**
	 * @param fixedCycle
	 */
	public void setFixedCycle(Integer fixedCycle) {
		this.fixedCycle = fixedCycle;
	}
	
    /**
     * @return fixedCycle
     */
	public Integer getFixedCycle() {
		return this.fixedCycle;
	}
	
	/**
	 * @param pRateType
	 */
	public void setPRateType(String pRateType) {
		this.pRateType = pRateType;
	}
	
    /**
     * @return pRateType
     */
	public String getPRateType() {
		return this.pRateType;
	}
	
	/**
	 * @param pRateFloat
	 */
	public void setPRateFloat(java.math.BigDecimal pRateFloat) {
		this.pRateFloat = pRateFloat;
	}
	
    /**
     * @return pRateFloat
     */
	public java.math.BigDecimal getPRateFloat() {
		return this.pRateFloat;
	}
	
	/**
	 * @param execRate
	 */
	public void setExecRate(java.math.BigDecimal execRate) {
		this.execRate = execRate;
	}
	
    /**
     * @return execRate
     */
	public java.math.BigDecimal getExecRate() {
		return this.execRate;
	}
	
	/**
	 * @param fixedRateFlag
	 */
	public void setFixedRateFlag(String fixedRateFlag) {
		this.fixedRateFlag = fixedRateFlag;
	}
	
    /**
     * @return fixedRateFlag
     */
	public String getFixedRateFlag() {
		return this.fixedRateFlag;
	}
	
	/**
	 * @param floatType
	 */
	public void setFloatType(String floatType) {
		this.floatType = floatType;
	}
	
    /**
     * @return floatType
     */
	public String getFloatType() {
		return this.floatType;
	}
	
	/**
	 * @param floatPoint
	 */
	public void setFloatPoint(java.math.BigDecimal floatPoint) {
		this.floatPoint = floatPoint;
	}
	
    /**
     * @return floatPoint
     */
	public java.math.BigDecimal getFloatPoint() {
		return this.floatPoint;
	}
	
	/**
	 * @param floatScale
	 */
	public void setFloatScale(java.math.BigDecimal floatScale) {
		this.floatScale = floatScale;
	}
	
    /**
     * @return floatScale
     */
	public java.math.BigDecimal getFloatScale() {
		return this.floatScale;
	}
	
	/**
	 * @param extRateType
	 */
	public void setExtRateType(String extRateType) {
		this.extRateType = extRateType;
	}
	
    /**
     * @return extRateType
     */
	public String getExtRateType() {
		return this.extRateType;
	}
	
	/**
	 * @param pRateFlag
	 */
	public void setPRateFlag(String pRateFlag) {
		this.pRateFlag = pRateFlag;
	}
	
    /**
     * @return pRateFlag
     */
	public String getPRateFlag() {
		return this.pRateFlag;
	}
	
	/**
	 * @param coreFrontNo
	 */
	public void setCoreFrontNo(String coreFrontNo) {
		this.coreFrontNo = coreFrontNo;
	}
	
    /**
     * @return coreFrontNo
     */
	public String getCoreFrontNo() {
		return this.coreFrontNo;
	}
	
	/**
	 * @param contStatus
	 */
	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}
	
    /**
     * @return contStatus
     */
	public String getContStatus() {
		return this.contStatus;
	}
	
	/**
	 * @param bizManagerId
	 */
	public void setBizManagerId(String bizManagerId) {
		this.bizManagerId = bizManagerId;
	}
	
    /**
     * @return bizManagerId
     */
	public String getBizManagerId() {
		return this.bizManagerId;
	}
	
	/**
	 * @param bizOrgId
	 */
	public void setBizOrgId(String bizOrgId) {
		this.bizOrgId = bizOrgId;
	}
	
    /**
     * @return bizOrgId
     */
	public String getBizOrgId() {
		return this.bizOrgId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}
	
	/**
	 * @param apprDataYm
	 */
	public void setApprDataYm(String apprDataYm) {
		this.apprDataYm = apprDataYm;
	}
	
    /**
     * @return apprDataYm
     */
	public String getApprDataYm() {
		return this.apprDataYm;
	}
	
	/**
	 * @param overdueInterest
	 */
	public void setOverdueInterest(java.math.BigDecimal overdueInterest) {
		this.overdueInterest = overdueInterest;
	}
	
    /**
     * @return overdueInterest
     */
	public java.math.BigDecimal getOverdueInterest() {
		return this.overdueInterest;
	}
	
	/**
	 * @param overdueAmt
	 */
	public void setOverdueAmt(java.math.BigDecimal overdueAmt) {
		this.overdueAmt = overdueAmt;
	}
	
    /**
     * @return overdueAmt
     */
	public java.math.BigDecimal getOverdueAmt() {
		return this.overdueAmt;
	}
	
	/**
	 * @param recommendStatus
	 */
	public void setRecommendStatus(String recommendStatus) {
		this.recommendStatus = recommendStatus;
	}
	
    /**
     * @return recommendStatus
     */
	public String getRecommendStatus() {
		return this.recommendStatus;
	}
	
	/**
	 * @param outstdIntst
	 */
	public void setOutstdIntst(java.math.BigDecimal outstdIntst) {
		this.outstdIntst = outstdIntst;
	}
	
    /**
     * @return outstdIntst
     */
	public java.math.BigDecimal getOutstdIntst() {
		return this.outstdIntst;
	}
	
	/**
	 * @param innerIntst
	 */
	public void setInnerIntst(java.math.BigDecimal innerIntst) {
		this.innerIntst = innerIntst;
	}
	
    /**
     * @return innerIntst
     */
	public java.math.BigDecimal getInnerIntst() {
		return this.innerIntst;
	}
	
	/**
	 * @param outerIntst
	 */
	public void setOuterIntst(java.math.BigDecimal outerIntst) {
		this.outerIntst = outerIntst;
	}
	
    /**
     * @return outerIntst
     */
	public java.math.BigDecimal getOuterIntst() {
		return this.outerIntst;
	}
	
	/**
	 * @param overdueDay
	 */
	public void setOverdueDay(Integer overdueDay) {
		this.overdueDay = overdueDay;
	}
	
    /**
     * @return overdueDay
     */
	public Integer getOverdueDay() {
		return this.overdueDay;
	}
	
	/**
	 * @param cnContNo
	 */
	public void setCnContNo(String cnContNo) {
		this.cnContNo = cnContNo;
	}
	
    /**
     * @return cnContNo
     */
	public String getCnContNo() {
		return this.cnContNo;
	}
	
	/**
	 * @param cnNum
	 */
	public void setCnNum(String cnNum) {
		this.cnNum = cnNum;
	}
	
    /**
     * @return cnNum
     */
	public String getCnNum() {
		return this.cnNum;
	}
	
	/**
	 * @param contImageNo
	 */
	public void setContImageNo(String contImageNo) {
		this.contImageNo = contImageNo;
	}
	
    /**
     * @return contImageNo
     */
	public String getContImageNo() {
		return this.contImageNo;
	}
	
	/**
	 * @param manageOrgId
	 */
	public void setManageOrgId(String manageOrgId) {
		this.manageOrgId = manageOrgId;
	}
	
    /**
     * @return manageOrgId
     */
	public String getManageOrgId() {
		return this.manageOrgId;
	}
	
	/**
	 * @param loanDirection
	 */
	public void setLoanDirection(String loanDirection) {
		this.loanDirection = loanDirection;
	}
	
    /**
     * @return loanDirection
     */
	public String getLoanDirection() {
		return this.loanDirection;
	}
	
	/**
	 * @param repayAccTb
	 */
	public void setRepayAccTb(String repayAccTb) {
		this.repayAccTb = repayAccTb;
	}
	
    /**
     * @return repayAccTb
     */
	public String getRepayAccTb() {
		return this.repayAccTb;
	}
	
	/**
	 * @param refereeNo
	 */
	public void setRefereeNo(String refereeNo) {
		this.refereeNo = refereeNo;
	}
	
    /**
     * @return refereeNo
     */
	public String getRefereeNo() {
		return this.refereeNo;
	}
	
	/**
	 * @param loanAccBankName
	 */
	public void setLoanAccBankName(String loanAccBankName) {
		this.loanAccBankName = loanAccBankName;
	}
	
    /**
     * @return loanAccBankName
     */
	public String getLoanAccBankName() {
		return this.loanAccBankName;
	}
	
	/**
	 * @param repayAccBankName
	 */
	public void setRepayAccBankName(String repayAccBankName) {
		this.repayAccBankName = repayAccBankName;
	}
	
    /**
     * @return repayAccBankName
     */
	public String getRepayAccBankName() {
		return this.repayAccBankName;
	}
	
	/**
	 * @param loanAccName
	 */
	public void setLoanAccName(String loanAccName) {
		this.loanAccName = loanAccName;
	}
	
    /**
     * @return loanAccName
     */
	public String getLoanAccName() {
		return this.loanAccName;
	}
	
	/**
	 * @param lprRate
	 */
	public void setLprRate(java.math.BigDecimal lprRate) {
		this.lprRate = lprRate;
	}
	
    /**
     * @return lprRate
     */
	public java.math.BigDecimal getLprRate() {
		return this.lprRate;
	}
	
	/**
	 * @param lprDate
	 */
	public void setLprDate(String lprDate) {
		this.lprDate = lprDate;
	}
	
    /**
     * @return lprDate
     */
	public String getLprDate() {
		return this.lprDate;
	}
	
	/**
	 * @param lprBp
	 */
	public void setLprBp(java.math.BigDecimal lprBp) {
		this.lprBp = lprBp;
	}
	
    /**
     * @return lprBp
     */
	public java.math.BigDecimal getLprBp() {
		return this.lprBp;
	}
	
	/**
	 * @param lprTerm
	 */
	public void setLprTerm(String lprTerm) {
		this.lprTerm = lprTerm;
	}
	
    /**
     * @return lprTerm
     */
	public String getLprTerm() {
		return this.lprTerm;
	}
	
	/**
	 * @param lprBpType
	 */
	public void setLprBpType(String lprBpType) {
		this.lprBpType = lprBpType;
	}
	
    /**
     * @return lprBpType
     */
	public String getLprBpType() {
		return this.lprBpType;
	}
	
	/**
	 * @param lprRateCode
	 */
	public void setLprRateCode(String lprRateCode) {
		this.lprRateCode = lprRateCode;
	}
	
    /**
     * @return lprRateCode
     */
	public String getLprRateCode() {
		return this.lprRateCode;
	}
	
	/**
	 * @param lprAdjustType
	 */
	public void setLprAdjustType(String lprAdjustType) {
		this.lprAdjustType = lprAdjustType;
	}
	
    /**
     * @return lprAdjustType
     */
	public String getLprAdjustType() {
		return this.lprAdjustType;
	}
	
	/**
	 * @param khhNo
	 */
	public void setKhhNo(String khhNo) {
		this.khhNo = khhNo;
	}
	
    /**
     * @return khhNo
     */
	public String getKhhNo() {
		return this.khhNo;
	}
	
	/**
	 * @param khhName
	 */
	public void setKhhName(String khhName) {
		this.khhName = khhName;
	}
	
    /**
     * @return khhName
     */
	public String getKhhName() {
		return this.khhName;
	}
	
	/**
	 * @param tradePartnerAcc
	 */
	public void setTradePartnerAcc(String tradePartnerAcc) {
		this.tradePartnerAcc = tradePartnerAcc;
	}
	
    /**
     * @return tradePartnerAcc
     */
	public String getTradePartnerAcc() {
		return this.tradePartnerAcc;
	}
	
	/**
	 * @param tradePartnerName
	 */
	public void setTradePartnerName(String tradePartnerName) {
		this.tradePartnerName = tradePartnerName;
	}
	
    /**
     * @return tradePartnerName
     */
	public String getTradePartnerName() {
		return this.tradePartnerName;
	}
	
	/**
	 * @param isSelfBankAcc
	 */
	public void setIsSelfBankAcc(String isSelfBankAcc) {
		this.isSelfBankAcc = isSelfBankAcc;
	}
	
    /**
     * @return isSelfBankAcc
     */
	public String getIsSelfBankAcc() {
		return this.isSelfBankAcc;
	}
	
	/**
	 * @param shoutuoName
	 */
	public void setShoutuoName(String shoutuoName) {
		this.shoutuoName = shoutuoName;
	}
	
    /**
     * @return shoutuoName
     */
	public String getShoutuoName() {
		return this.shoutuoName;
	}
	
	/**
	 * @param shoutuoAccount
	 */
	public void setShoutuoAccount(String shoutuoAccount) {
		this.shoutuoAccount = shoutuoAccount;
	}
	
    /**
     * @return shoutuoAccount
     */
	public String getShoutuoAccount() {
		return this.shoutuoAccount;
	}
	
	/**
	 * @param busiAmt
	 */
	public void setBusiAmt(java.math.BigDecimal busiAmt) {
		this.busiAmt = busiAmt;
	}
	
    /**
     * @return busiAmt
     */
	public java.math.BigDecimal getBusiAmt() {
		return this.busiAmt;
	}
	
	/**
	 * @param orgAmt
	 */
	public void setOrgAmt(java.math.BigDecimal orgAmt) {
		this.orgAmt = orgAmt;
	}
	
    /**
     * @return orgAmt
     */
	public java.math.BigDecimal getOrgAmt() {
		return this.orgAmt;
	}
	
	/**
	 * @param graceDate
	 */
	public void setGraceDate(String graceDate) {
		this.graceDate = graceDate;
	}
	
    /**
     * @return graceDate
     */
	public String getGraceDate() {
		return this.graceDate;
	}
	
	/**
	 * @param fineRate
	 */
	public void setFineRate(java.math.BigDecimal fineRate) {
		this.fineRate = fineRate;
	}
	
    /**
     * @return fineRate
     */
	public java.math.BigDecimal getFineRate() {
		return this.fineRate;
	}
	
	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType;
	}
	
    /**
     * @return termType
     */
	public String getTermType() {
		return this.termType;
	}


}