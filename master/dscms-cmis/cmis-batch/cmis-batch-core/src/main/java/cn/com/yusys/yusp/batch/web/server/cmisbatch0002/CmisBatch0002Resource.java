package cn.com.yusys.yusp.batch.web.server.cmisbatch0002;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002RespDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0002.CmisBatch0002Service;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询[核心系统-历史表-贷款账户主表]信息
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CMISBATCH0002:查询[核心系统-历史表-贷款账户主表]信息")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0002Resource.class);

    @Autowired
    private CmisBatch0002Service yuspBatch0002Service;

    /**
     * 交易码：cmisbatch0002
     * 交易描述：查询[核心系统-历史表-贷款账户主表]信息
     *
     * @param cmisbatch0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询[核心系统-历史表-贷款账户主表]信息")
    @PostMapping("/cmisbatch0002")
    protected @ResponseBody
    ResultDto<Cmisbatch0002RespDto> cmisbatch0002(@Validated @RequestBody Cmisbatch0002ReqDto cmisbatch0002ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, JSON.toJSONString(cmisbatch0002ReqDto));
        Cmisbatch0002RespDto cmisbatch0002RespDto = new Cmisbatch0002RespDto();// 响应Dto:调度运行管理信息查詢
        ResultDto<Cmisbatch0002RespDto> cmisbatch0002ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0002ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, JSON.toJSONString(cmisbatch0002ReqDto));
            cmisbatch0002RespDto = yuspBatch0002Service.cmisBatch0002(cmisbatch0002ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, JSON.toJSONString(cmisbatch0002RespDto));
            // 封装cmisbatch0002ResultDto中正确的返回码和返回信息
            cmisbatch0002ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0002ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, e.getMessage());
            // 封装cmisbatch0002ResultDto中异常返回码和返回信息
            cmisbatch0002ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0002ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0002RespDto到cmisbatch0002ResultDto中
        cmisbatch0002ResultDto.setData(cmisbatch0002RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, JSON.toJSONString(cmisbatch0002ResultDto));
        return cmisbatch0002ResultDto;
    }
}
