package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0304</br>
 * 任务名称：加工任务-客户处理-唤醒贷名单规则 </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月13日 下午14:56:54
 */
public interface Cmis0304Mapper {

    /**
     * 加工符合业务规则数据
     *
     * @param openDay
     * @return
     */
    int insertTmpCtr(@Param("openDay") String openDay);

    /**
     * 加工不准入规则数据
     *
     * @param openDay
     * @return
     */
    int insertTmpAcc1(@Param("openDay") String openDay);

    /**
     * 加工不准入规则数据
     *
     * @param openDay
     * @return
     */
    int insertTmpAcc2(@Param("openDay") String openDay);

    /**
     * 加工不准入规则数据
     *
     * @param openDay
     * @return
     */
    int insertTmpAcc3(@Param("openDay") String openDay);

    /**
     * 加工不准入规则数据
     *
     * @param openDay
     * @return
     */
    int insertTmpAcc4(@Param("openDay") String openDay);

    /**
     * 加工不准入规则数据
     *
     * @param openDay
     * @return
     */
    int insertTmpAcc5(@Param("openDay") String openDay);




    /**
     * 加工不准入规则数据
     *
     * @param openDay
     * @return
     */
    int insertTmpAcc8(@Param("openDay") String openDay);

    /**
     * 加工不准入规则数据
     *
     * @param openDay
     * @return
     */
    //int insertTmpAcc9(@Param("openDay") String openDay);



    /**
     * 加工唤醒贷名单信息
     *
     * @param openDay
     * @return
     */
    int insertHXD(@Param("openDay") String openDay);

    /**
     * 加工调查任务表
     *
     * @param openDay
     * @return
     */
    int insertLmt1(@Param("openDay") String openDay);

    /**
     *插入客户授信调查主表
     *
     * @param openDay
     * @return
     */
    int insertLmtsurvey(@Param("openDay") String openDay);



}
