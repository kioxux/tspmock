package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0109Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0109</br>
 * 任务名称：加工任务-业务处理-国结额度数据处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0109Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0109Service.class);
    @Autowired
    private TableUtilsService tableUtilsService;

    @Autowired
    private Cmis0109Mapper cmis0109Mapper;

    /**
     * 插入额度国结临时表-我行代开他行保函
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0109InsertTmpGjpLmtCvrgRzkz(String openDay) {

        logger.info("truncateTmpGjpLmtCvrgRzkz清空我行代开他行保函开始");
        // cmis0109Mapper.truncateTmpGjpLmtCvrgRzkz();// cmis_batch.tmp_gjp_lmt_cvrg_rzkz
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_gjp_lmt_cvrg_rzkz");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpGjpLmtCvrgRzkz清空我行代开他行保函结束");

        logger.info("加工到我行代开他行保函开始，请求参数:[{}]", openDay);
        int insertTmpGjpLmtCvrgRzkz = cmis0109Mapper.insertTmpGjpLmtCvrgRzkz(openDay);
        logger.info("加工到我行代开他行保函结束，返回参数:[{}]", insertTmpGjpLmtCvrgRzkz);
    }

    /**
     * 插入额度国结临时表-我行代开他行信用证
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0109InsertTmpGjpLmtPeerRzkz(String openDay) {

//        logger.info("truncateTmpGjpLmtPeerRzkz清空我行代开他行信用证开始");
//        // cmis0109Mapper.truncateTmpGjpLmtPeerRzkz();// cmis_batch.tmp_gjp_lmt_peer_rzkz
//        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_gjp_lmt_peer_rzkz");// 20211027 调整为调用重命名创建和删除相关表的公共方法
//        logger.info("truncateTmpGjpLmtPeerRzkz清空我行代开他行信用证结束");
//
//        logger.info("加工到我行代开他行信用证开始，请求参数:[{}]", openDay);
//        int insertTmpGjpLmtPeerRzkz01 = cmis0109Mapper.insertTmpGjpLmtPeerRzkz01(openDay);
//        logger.info("加工到我行代开他行信用证结束，返回参数:[{}]", insertTmpGjpLmtPeerRzkz01);

        logger.info("truncateTmpGjpLmtPeerRzkz02清空 我行代开他行信用证开始");
        // cmis0109Mapper.truncateTmpGjpLmtPeerRzkz02();// cmis_batch.tmp_gjp_lmt_peer_rzkz
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_gjp_lmt_peer_rzkz");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpGjpLmtPeerRzkz02清空 我行代开他行信用证结束");

        logger.info("加工到我行代开他行信用证开始，请求参数:[{}]", openDay);
        int insertTmpGjpLmtPeerRzkz02 = cmis0109Mapper.insertTmpGjpLmtPeerRzkz02(openDay);
        logger.info("加工到我行代开他行信用证结束，返回参数:[{}]", insertTmpGjpLmtPeerRzkz02);
    }

    /**
     * 插入额度国结临时表-福费廷同业类登记簿
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0109InsertTmpGjpLmtFftRzkz(String openDay) {

        logger.info("truncateTmpGjpLmtFftRzkz清空福费廷同业类登记簿开始");
        // cmis0109Mapper.truncateTmpGjpLmtFftRzkz();// cmis_batch.tmp_gjp_lmt_fft_rzkz
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_gjp_lmt_fft_rzkz");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpGjpLmtFftRzkz清空福费廷同业类登记簿结束");

        logger.info("加工到福费廷同业类登记簿开始，请求参数:[{}]", openDay);
        int insertTmpGjpLmtFftRzkz = cmis0109Mapper.insertTmpGjpLmtFftRzkz(openDay);
        logger.info("加工到福费廷同业类登记簿结束，返回参数:[{}]", insertTmpGjpLmtFftRzkz);

        logger.info("清空转贴现登记簿开始");
        // cmis0109Mapper.truncateTmpGjpLmtZtxRzkz();// cmis_batch.tmp_gjp_lmt_ztx_rzkz
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_gjp_lmt_ztx_rzkz");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空转贴现登记簿结束");

        // 20211101 修复zjw提出的bat_s_pjp_bt_rediscount_buyin中BUSI_END_DATE在load_data导入时将空转换为0000-00-00的问题 开始
        logger.info("更新[转贴现买入明细]中[业务结清日期BUSI_END_DATE]数据开始，请求参数:[{}]", openDay);
        int updateBatchBspbrb = cmis0109Mapper.updateBatchBspbrb(openDay);
        logger.info("更新[转贴现买入明细]中[业务结清日期BUSI_END_DATE]数据结束，返回参数:[{}]", updateBatchBspbrb);
        // 20211101 修复zjw提出的bat_s_pjp_bt_rediscount_buyin中BUSI_END_DATE在load_data导入时将空转换为0000-00-00的问题 结束

        logger.info("插入转贴现登记簿-转贴现初始占用数据插入开始，请求参数:[{}]", openDay);
        int insertTmpGjpLmtZtxRzkz = cmis0109Mapper.insertTmpGjpLmtZtxRzkz(openDay);
        logger.info("插入转贴现登记簿-转贴现初始占用数据插入结束，返回参数:[{}]", insertTmpGjpLmtZtxRzkz);

        logger.info("更新转贴现登记簿-插入关联缺失信息：额度扣减总行行名、贴出方行名开始，请求参数:[{}]", openDay);
        int updateTmpGjpLmtZtxRzkz01 = cmis0109Mapper.updateTmpGjpLmtZtxRzkz01(openDay);
        logger.info("更新转贴现登记簿-插入关联缺失信息：额度扣减总行行名、贴出方行名结束，返回参数:[{}]", updateTmpGjpLmtZtxRzkz01);

        logger.info("更新转贴现登记簿-插入关联缺失信息：额度扣减总行行名、贴出方行名开始，请求参数:[{}]", openDay);
        int updateTmpGjpLmtZtxRzkz02 = cmis0109Mapper.updateTmpGjpLmtZtxRzkz02(openDay);
        logger.info("更新转贴现登记簿-插入关联缺失信息：额度扣减总行行名、贴出方行名结束，返回参数:[{}]", updateTmpGjpLmtZtxRzkz02);

        logger.info("清空加工占用授信表-更新批次金额和批次余额开始");
        // cmis0109Mapper.truncateTmpGjpLmtZtxRzkz03();// cmis_batch.tmp_lmt_cop
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_cop");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空加工占用授信表-更新批次金额和批次余额结束");

        logger.info("插入加工占用授信表-更新批次金额和批次余额开始，请求参数:[{}]", openDay);
        int insertTmpGjpLmtZtxRzkz03 = cmis0109Mapper.insertTmpGjpLmtZtxRzkz03(openDay);
        logger.info("插入加工占用授信表-更新批次金额和批次余额结束，返回参数:[{}]", insertTmpGjpLmtZtxRzkz03);

        logger.info("更新转贴现登记簿-更新批次金额和批次余额开始，请求参数:[{}]", openDay);
        int updateTmpGjpLmtZtxRzkz03 = cmis0109Mapper.updateTmpGjpLmtZtxRzkz03(openDay);
        logger.info("更新转贴现登记簿-更新批次金额和批次余额结束，返回参数:[{}]", updateTmpGjpLmtZtxRzkz03);
    }
}
