/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKlnbDkfksx
 * @类描述: bat_s_core_klnb_dkfksx数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-13 19:43:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_core_klnb_dkfksx")
public class BatSCoreKlnbDkfksx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 法人代码 **/
	@Id
	@Column(name = "FARENDMA")
	private String farendma;
	
	/** 贷款借据号 **/
	@Id
	@Column(name = "DKJIEJUH")
	private String dkjiejuh;
	
	/** 贷款入账账号 **/
	@Column(name = "DKRZHZHH", unique = false, nullable = true, length = 35)
	private String dkrzhzhh;
	
	/** 贷款入账账号子序号 **/
	@Column(name = "DKRZHZXH", unique = false, nullable = true, length = 8)
	private String dkrzhzxh;
	
	/** 按揭项目编号 **/
	@Column(name = "ANJXMUBH", unique = false, nullable = true, length = 30)
	private String anjxmubh;
	
	/** 按揭项目名称 **/
	@Column(name = "ANJXMUMC", unique = false, nullable = true, length = 750)
	private String anjxmumc;
	
	/** 开发商客户号 **/
	@Column(name = "KFSKEHUH", unique = false, nullable = true, length = 20)
	private String kfskehuh;
	
	/** 开发商客户名 **/
	@Column(name = "KFSKEHUM", unique = false, nullable = true, length = 750)
	private String kfskehum;
	
	/** 开发商账号 **/
	@Column(name = "KFSZHHAO", unique = false, nullable = true, length = 35)
	private String kfszhhao;
	
	/** 开发商账号子序号 **/
	@Column(name = "KFSZHZXH", unique = false, nullable = true, length = 8)
	private String kfszhzxh;
	
	/** 开发商保证金账号 **/
	@Column(name = "KFSBZJZH", unique = false, nullable = true, length = 35)
	private String kfsbzjzh;
	
	/** 开发商保证金账号子序号 **/
	@Column(name = "KFSBZZXH", unique = false, nullable = true, length = 8)
	private String kfsbzzxh;
	
	/** 预收息扣息来源账号 **/
	@Column(name = "YSXLYZHH", unique = false, nullable = true, length = 35)
	private String ysxlyzhh;
	
	/** 预收息扣息来源账号子序号 **/
	@Column(name = "YSXLYZXH", unique = false, nullable = true, length = 8)
	private String ysxlyzxh;
	
	/** 转卖方比例 **/
	@Column(name = "ZHMFBILI", unique = false, nullable = true, length = 6)
	private java.math.BigDecimal zhmfbili;
	
	/** 保证金比例 **/
	@Column(name = "BAOZHJBL", unique = false, nullable = true, length = 6)
	private java.math.BigDecimal baozhjbl;
	
	/** 放款类型(1-多次放款 ,2-单次放款) **/
	@Column(name = "FANGKULX", unique = false, nullable = true, length = 1)
	private String fangkulx;
	
	/** 自动放款标志(1-是,0-否) **/
	@Column(name = "ZIDFKBZH", unique = false, nullable = true, length = 1)
	private String zidfkbzh;
	
	/** 自动放款借据管理模式(1-单笔借据,2-多笔借据) **/
	@Column(name = "ZDFKJJMS", unique = false, nullable = true, length = 1)
	private String zdfkjjms;
	
	/** 周期性放款标志(1-是,0-否) **/
	@Column(name = "ZHQIFKBZ", unique = false, nullable = true, length = 1)
	private String zhqifkbz;
	
	/** 放款周期 **/
	@Column(name = "FKZHOUQI", unique = false, nullable = true, length = 8)
	private String fkzhouqi;
	
	/** 放款金额方式(1-按百分比,2-按绝对金额) **/
	@Column(name = "FKFANGSH", unique = false, nullable = true, length = 1)
	private String fkfangsh;
	
	/** 每次放款金额或比例 **/
	@Column(name = "MCFKJEBL", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal mcfkjebl;
	
	/** 本次放款金额 **/
	@Column(name = "BENCFKJE", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal bencfkje;
	
	/** 定制放款计划(1-是,0-否) **/
	@Column(name = "DZHIFKJH", unique = false, nullable = true, length = 1)
	private String dzhifkjh;
	
	/** 允许特殊放款标志(0-不允许,1-允许) **/
	@Column(name = "YXTSFKBZ", unique = false, nullable = true, length = 1)
	private String yxtsfkbz;
	
	/** 放款资金来源类型(0-不适用,1-自有资金 ,2-外国政府 ,3-委托资金 ,4-其他银行 ) **/
	@Column(name = "ZIJILYLX", unique = false, nullable = true, length = 1)
	private String zijilylx;
	
	/** 放款资金处理方式(0-自主支付,1-转卖方,2-转待销账,3-临时冻结,4-受托支付) **/
	@Column(name = "FKZJCLFS", unique = false, nullable = true, length = 1)
	private String fkzjclfs;
	
	/** 放款记账方式(0-不过借款人存款户 ,1-过借款人存款户) **/
	@Column(name = "FKJZHFSH", unique = false, nullable = true, length = 1)
	private String fkjzhfsh;
	
	/** 受托支付核算码 **/
	@Column(name = "SHTZFHXM", unique = false, nullable = true, length = 30)
	private String shtzfhxm;
	
	/** 是否允许超额放款(1-是,0-否) **/
	@Column(name = "CHAOEFKU", unique = false, nullable = true, length = 1)
	private String chaoefku;
	
	/** 超额放款比例 **/
	@Column(name = "CHAOEFKB", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal chaoefkb;
	
	/** 借新还旧原贷款控制(0-不适用,1-不控制,2-无欠款,3-无欠本金,4-无欠利息) **/
	@Column(name = "JXHJDKKZ", unique = false, nullable = true, length = 1)
	private String jxhjdkkz;
	
	/** 借新还旧还款控制(0-不适用,1-不做控制,2-全部结清) **/
	@Column(name = "JXHJHKKZ", unique = false, nullable = true, length = 1)
	private String jxhjhkkz;
	
	/** 原贷款借据号 **/
	@Column(name = "YDKJIEJH", unique = false, nullable = true, length = 300)
	private String ydkjiejh;
	
	/** 允许追加贷款(1-是,0-否) **/
	@Column(name = "ZHUIJFKU", unique = false, nullable = true, length = 1)
	private String zhuijfku;
	
	/** 允许现金放款(1-是,0-否) **/
	@Column(name = "XIANJFKU", unique = false, nullable = true, length = 1)
	private String xianjfku;
	
	/** 允许对行内同名账户放款(1-是,0-否) **/
	@Column(name = "HNTMIFKU", unique = false, nullable = true, length = 1)
	private String hntmifku;
	
	/** 允许对行内非同名账户放款(1-是,0-否) **/
	@Column(name = "HNFTMFKU", unique = false, nullable = true, length = 1)
	private String hnftmfku;
	
	/** 允许对行外账户放款(1-是,0-否) **/
	@Column(name = "HWZHUFKU", unique = false, nullable = true, length = 1)
	private String hwzhufku;
	
	/** 允许对内部账户放款(1-是,0-否) **/
	@Column(name = "NEIBUFKU", unique = false, nullable = true, length = 1)
	private String neibufku;
	
	/** 放款汇率牌价类型(0-不适用,1-固定牌价,2-系统牌价-中间价,3-系统牌价-现汇买入价,4-系统牌价-现汇卖出价) **/
	@Column(name = "FKHLPJLX", unique = false, nullable = true, length = 1)
	private String fkhlpjlx;
	
	/** 放款折算汇率 **/
	@Column(name = "FKZSHUIL", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fkzshuil;
	
	/** 上次放款日期 **/
	@Column(name = "SCFKRIQI", unique = false, nullable = true, length = 8)
	private String scfkriqi;
	
	/** 已放款次数 **/
	@Column(name = "YIFKCISH", unique = false, nullable = true, length = 19)
	private Long yifkcish;
	
	/** 放款截止日期 **/
	@Column(name = "FKJZRIQI", unique = false, nullable = true, length = 8)
	private String fkjzriqi;
	
	/** 合作方编号 **/
	@Column(name = "HEZUOFBH", unique = false, nullable = true, length = 30)
	private String hezuofbh;
	
	/** 合作方名称 **/
	@Column(name = "HEZUOFMC", unique = false, nullable = true, length = 750)
	private String hezuofmc;
	
	/** 分行标识 **/
	@Column(name = "FENHBIOS", unique = false, nullable = false, length = 4)
	private String fenhbios;
	
	/** 维护柜员 **/
	@Column(name = "WEIHGUIY", unique = false, nullable = true, length = 8)
	private String weihguiy;
	
	/** 维护机构 **/
	@Column(name = "WEIHJIGO", unique = false, nullable = false, length = 12)
	private String weihjigo;
	
	/** 维护日期 **/
	@Column(name = "WEIHRIQI", unique = false, nullable = false, length = 8)
	private String weihriqi;
	
	/** 维护时间 **/
	@Column(name = "WEIHSHIJ", unique = false, nullable = true, length = 9)
	private String weihshij;
	
	/** 时间戳 **/
	@Column(name = "SHIJCHUO", unique = false, nullable = false, length = 19)
	private Long shijchuo;
	
	/** 记录状态(0-正常,1-删除) **/
	@Column(name = "JILUZTAI", unique = false, nullable = false, length = 1)
	private String jiluztai;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param dkrzhzhh
	 */
	public void setDkrzhzhh(String dkrzhzhh) {
		this.dkrzhzhh = dkrzhzhh;
	}
	
    /**
     * @return dkrzhzhh
     */
	public String getDkrzhzhh() {
		return this.dkrzhzhh;
	}
	
	/**
	 * @param dkrzhzxh
	 */
	public void setDkrzhzxh(String dkrzhzxh) {
		this.dkrzhzxh = dkrzhzxh;
	}
	
    /**
     * @return dkrzhzxh
     */
	public String getDkrzhzxh() {
		return this.dkrzhzxh;
	}
	
	/**
	 * @param anjxmubh
	 */
	public void setAnjxmubh(String anjxmubh) {
		this.anjxmubh = anjxmubh;
	}
	
    /**
     * @return anjxmubh
     */
	public String getAnjxmubh() {
		return this.anjxmubh;
	}
	
	/**
	 * @param anjxmumc
	 */
	public void setAnjxmumc(String anjxmumc) {
		this.anjxmumc = anjxmumc;
	}
	
    /**
     * @return anjxmumc
     */
	public String getAnjxmumc() {
		return this.anjxmumc;
	}
	
	/**
	 * @param kfskehuh
	 */
	public void setKfskehuh(String kfskehuh) {
		this.kfskehuh = kfskehuh;
	}
	
    /**
     * @return kfskehuh
     */
	public String getKfskehuh() {
		return this.kfskehuh;
	}
	
	/**
	 * @param kfskehum
	 */
	public void setKfskehum(String kfskehum) {
		this.kfskehum = kfskehum;
	}
	
    /**
     * @return kfskehum
     */
	public String getKfskehum() {
		return this.kfskehum;
	}
	
	/**
	 * @param kfszhhao
	 */
	public void setKfszhhao(String kfszhhao) {
		this.kfszhhao = kfszhhao;
	}
	
    /**
     * @return kfszhhao
     */
	public String getKfszhhao() {
		return this.kfszhhao;
	}
	
	/**
	 * @param kfszhzxh
	 */
	public void setKfszhzxh(String kfszhzxh) {
		this.kfszhzxh = kfszhzxh;
	}
	
    /**
     * @return kfszhzxh
     */
	public String getKfszhzxh() {
		return this.kfszhzxh;
	}
	
	/**
	 * @param kfsbzjzh
	 */
	public void setKfsbzjzh(String kfsbzjzh) {
		this.kfsbzjzh = kfsbzjzh;
	}
	
    /**
     * @return kfsbzjzh
     */
	public String getKfsbzjzh() {
		return this.kfsbzjzh;
	}
	
	/**
	 * @param kfsbzzxh
	 */
	public void setKfsbzzxh(String kfsbzzxh) {
		this.kfsbzzxh = kfsbzzxh;
	}
	
    /**
     * @return kfsbzzxh
     */
	public String getKfsbzzxh() {
		return this.kfsbzzxh;
	}
	
	/**
	 * @param ysxlyzhh
	 */
	public void setYsxlyzhh(String ysxlyzhh) {
		this.ysxlyzhh = ysxlyzhh;
	}
	
    /**
     * @return ysxlyzhh
     */
	public String getYsxlyzhh() {
		return this.ysxlyzhh;
	}
	
	/**
	 * @param ysxlyzxh
	 */
	public void setYsxlyzxh(String ysxlyzxh) {
		this.ysxlyzxh = ysxlyzxh;
	}
	
    /**
     * @return ysxlyzxh
     */
	public String getYsxlyzxh() {
		return this.ysxlyzxh;
	}
	
	/**
	 * @param zhmfbili
	 */
	public void setZhmfbili(java.math.BigDecimal zhmfbili) {
		this.zhmfbili = zhmfbili;
	}
	
    /**
     * @return zhmfbili
     */
	public java.math.BigDecimal getZhmfbili() {
		return this.zhmfbili;
	}
	
	/**
	 * @param baozhjbl
	 */
	public void setBaozhjbl(java.math.BigDecimal baozhjbl) {
		this.baozhjbl = baozhjbl;
	}
	
    /**
     * @return baozhjbl
     */
	public java.math.BigDecimal getBaozhjbl() {
		return this.baozhjbl;
	}
	
	/**
	 * @param fangkulx
	 */
	public void setFangkulx(String fangkulx) {
		this.fangkulx = fangkulx;
	}
	
    /**
     * @return fangkulx
     */
	public String getFangkulx() {
		return this.fangkulx;
	}
	
	/**
	 * @param zidfkbzh
	 */
	public void setZidfkbzh(String zidfkbzh) {
		this.zidfkbzh = zidfkbzh;
	}
	
    /**
     * @return zidfkbzh
     */
	public String getZidfkbzh() {
		return this.zidfkbzh;
	}
	
	/**
	 * @param zdfkjjms
	 */
	public void setZdfkjjms(String zdfkjjms) {
		this.zdfkjjms = zdfkjjms;
	}
	
    /**
     * @return zdfkjjms
     */
	public String getZdfkjjms() {
		return this.zdfkjjms;
	}
	
	/**
	 * @param zhqifkbz
	 */
	public void setZhqifkbz(String zhqifkbz) {
		this.zhqifkbz = zhqifkbz;
	}
	
    /**
     * @return zhqifkbz
     */
	public String getZhqifkbz() {
		return this.zhqifkbz;
	}
	
	/**
	 * @param fkzhouqi
	 */
	public void setFkzhouqi(String fkzhouqi) {
		this.fkzhouqi = fkzhouqi;
	}
	
    /**
     * @return fkzhouqi
     */
	public String getFkzhouqi() {
		return this.fkzhouqi;
	}
	
	/**
	 * @param fkfangsh
	 */
	public void setFkfangsh(String fkfangsh) {
		this.fkfangsh = fkfangsh;
	}
	
    /**
     * @return fkfangsh
     */
	public String getFkfangsh() {
		return this.fkfangsh;
	}
	
	/**
	 * @param mcfkjebl
	 */
	public void setMcfkjebl(java.math.BigDecimal mcfkjebl) {
		this.mcfkjebl = mcfkjebl;
	}
	
    /**
     * @return mcfkjebl
     */
	public java.math.BigDecimal getMcfkjebl() {
		return this.mcfkjebl;
	}
	
	/**
	 * @param bencfkje
	 */
	public void setBencfkje(java.math.BigDecimal bencfkje) {
		this.bencfkje = bencfkje;
	}
	
    /**
     * @return bencfkje
     */
	public java.math.BigDecimal getBencfkje() {
		return this.bencfkje;
	}
	
	/**
	 * @param dzhifkjh
	 */
	public void setDzhifkjh(String dzhifkjh) {
		this.dzhifkjh = dzhifkjh;
	}
	
    /**
     * @return dzhifkjh
     */
	public String getDzhifkjh() {
		return this.dzhifkjh;
	}
	
	/**
	 * @param yxtsfkbz
	 */
	public void setYxtsfkbz(String yxtsfkbz) {
		this.yxtsfkbz = yxtsfkbz;
	}
	
    /**
     * @return yxtsfkbz
     */
	public String getYxtsfkbz() {
		return this.yxtsfkbz;
	}
	
	/**
	 * @param zijilylx
	 */
	public void setZijilylx(String zijilylx) {
		this.zijilylx = zijilylx;
	}
	
    /**
     * @return zijilylx
     */
	public String getZijilylx() {
		return this.zijilylx;
	}
	
	/**
	 * @param fkzjclfs
	 */
	public void setFkzjclfs(String fkzjclfs) {
		this.fkzjclfs = fkzjclfs;
	}
	
    /**
     * @return fkzjclfs
     */
	public String getFkzjclfs() {
		return this.fkzjclfs;
	}
	
	/**
	 * @param fkjzhfsh
	 */
	public void setFkjzhfsh(String fkjzhfsh) {
		this.fkjzhfsh = fkjzhfsh;
	}
	
    /**
     * @return fkjzhfsh
     */
	public String getFkjzhfsh() {
		return this.fkjzhfsh;
	}
	
	/**
	 * @param shtzfhxm
	 */
	public void setShtzfhxm(String shtzfhxm) {
		this.shtzfhxm = shtzfhxm;
	}
	
    /**
     * @return shtzfhxm
     */
	public String getShtzfhxm() {
		return this.shtzfhxm;
	}
	
	/**
	 * @param chaoefku
	 */
	public void setChaoefku(String chaoefku) {
		this.chaoefku = chaoefku;
	}
	
    /**
     * @return chaoefku
     */
	public String getChaoefku() {
		return this.chaoefku;
	}
	
	/**
	 * @param chaoefkb
	 */
	public void setChaoefkb(java.math.BigDecimal chaoefkb) {
		this.chaoefkb = chaoefkb;
	}
	
    /**
     * @return chaoefkb
     */
	public java.math.BigDecimal getChaoefkb() {
		return this.chaoefkb;
	}
	
	/**
	 * @param jxhjdkkz
	 */
	public void setJxhjdkkz(String jxhjdkkz) {
		this.jxhjdkkz = jxhjdkkz;
	}
	
    /**
     * @return jxhjdkkz
     */
	public String getJxhjdkkz() {
		return this.jxhjdkkz;
	}
	
	/**
	 * @param jxhjhkkz
	 */
	public void setJxhjhkkz(String jxhjhkkz) {
		this.jxhjhkkz = jxhjhkkz;
	}
	
    /**
     * @return jxhjhkkz
     */
	public String getJxhjhkkz() {
		return this.jxhjhkkz;
	}
	
	/**
	 * @param ydkjiejh
	 */
	public void setYdkjiejh(String ydkjiejh) {
		this.ydkjiejh = ydkjiejh;
	}
	
    /**
     * @return ydkjiejh
     */
	public String getYdkjiejh() {
		return this.ydkjiejh;
	}
	
	/**
	 * @param zhuijfku
	 */
	public void setZhuijfku(String zhuijfku) {
		this.zhuijfku = zhuijfku;
	}
	
    /**
     * @return zhuijfku
     */
	public String getZhuijfku() {
		return this.zhuijfku;
	}
	
	/**
	 * @param xianjfku
	 */
	public void setXianjfku(String xianjfku) {
		this.xianjfku = xianjfku;
	}
	
    /**
     * @return xianjfku
     */
	public String getXianjfku() {
		return this.xianjfku;
	}
	
	/**
	 * @param hntmifku
	 */
	public void setHntmifku(String hntmifku) {
		this.hntmifku = hntmifku;
	}
	
    /**
     * @return hntmifku
     */
	public String getHntmifku() {
		return this.hntmifku;
	}
	
	/**
	 * @param hnftmfku
	 */
	public void setHnftmfku(String hnftmfku) {
		this.hnftmfku = hnftmfku;
	}
	
    /**
     * @return hnftmfku
     */
	public String getHnftmfku() {
		return this.hnftmfku;
	}
	
	/**
	 * @param hwzhufku
	 */
	public void setHwzhufku(String hwzhufku) {
		this.hwzhufku = hwzhufku;
	}
	
    /**
     * @return hwzhufku
     */
	public String getHwzhufku() {
		return this.hwzhufku;
	}
	
	/**
	 * @param neibufku
	 */
	public void setNeibufku(String neibufku) {
		this.neibufku = neibufku;
	}
	
    /**
     * @return neibufku
     */
	public String getNeibufku() {
		return this.neibufku;
	}
	
	/**
	 * @param fkhlpjlx
	 */
	public void setFkhlpjlx(String fkhlpjlx) {
		this.fkhlpjlx = fkhlpjlx;
	}
	
    /**
     * @return fkhlpjlx
     */
	public String getFkhlpjlx() {
		return this.fkhlpjlx;
	}
	
	/**
	 * @param fkzshuil
	 */
	public void setFkzshuil(java.math.BigDecimal fkzshuil) {
		this.fkzshuil = fkzshuil;
	}
	
    /**
     * @return fkzshuil
     */
	public java.math.BigDecimal getFkzshuil() {
		return this.fkzshuil;
	}
	
	/**
	 * @param scfkriqi
	 */
	public void setScfkriqi(String scfkriqi) {
		this.scfkriqi = scfkriqi;
	}
	
    /**
     * @return scfkriqi
     */
	public String getScfkriqi() {
		return this.scfkriqi;
	}
	
	/**
	 * @param yifkcish
	 */
	public void setYifkcish(Long yifkcish) {
		this.yifkcish = yifkcish;
	}
	
    /**
     * @return yifkcish
     */
	public Long getYifkcish() {
		return this.yifkcish;
	}
	
	/**
	 * @param fkjzriqi
	 */
	public void setFkjzriqi(String fkjzriqi) {
		this.fkjzriqi = fkjzriqi;
	}
	
    /**
     * @return fkjzriqi
     */
	public String getFkjzriqi() {
		return this.fkjzriqi;
	}
	
	/**
	 * @param hezuofbh
	 */
	public void setHezuofbh(String hezuofbh) {
		this.hezuofbh = hezuofbh;
	}
	
    /**
     * @return hezuofbh
     */
	public String getHezuofbh() {
		return this.hezuofbh;
	}
	
	/**
	 * @param hezuofmc
	 */
	public void setHezuofmc(String hezuofmc) {
		this.hezuofmc = hezuofmc;
	}
	
    /**
     * @return hezuofmc
     */
	public String getHezuofmc() {
		return this.hezuofmc;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}