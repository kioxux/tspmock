/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKitpKdlldm;
import cn.com.yusys.yusp.batch.service.load.core.BatSCoreKitpKdlldmService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatSCoreKitpKdlldmResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 23:46:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batscorekitpkdlldm")
public class BatSCoreKitpKdlldmResource {
    @Autowired
    private BatSCoreKitpKdlldmService batSCoreKitpKdlldmService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSCoreKitpKdlldm>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSCoreKitpKdlldm> list = batSCoreKitpKdlldmService.selectAll(queryModel);
        return new ResultDto<List<BatSCoreKitpKdlldm>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSCoreKitpKdlldm>> index(QueryModel queryModel) {
        List<BatSCoreKitpKdlldm> list = batSCoreKitpKdlldmService.selectByModel(queryModel);
        return new ResultDto<List<BatSCoreKitpKdlldm>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSCoreKitpKdlldm> create(@RequestBody BatSCoreKitpKdlldm batSCoreKitpKdlldm) throws URISyntaxException {
        batSCoreKitpKdlldmService.insert(batSCoreKitpKdlldm);
        return new ResultDto<BatSCoreKitpKdlldm>(batSCoreKitpKdlldm);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSCoreKitpKdlldm batSCoreKitpKdlldm) throws URISyntaxException {
        int result = batSCoreKitpKdlldmService.update(batSCoreKitpKdlldm);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String farendma, String fenhdaim, String jigouhao, String qiciiiii, String lilvbhlx, String lilvdama, String bizhongg, Long cunqitsh, String cunqiiii, String fdzhouqi, java.math.BigDecimal lilvdanc, String shengxrq) {
        int result = batSCoreKitpKdlldmService.deleteByPrimaryKey(farendma, fenhdaim, jigouhao, qiciiiii, lilvbhlx, lilvdama, bizhongg, cunqitsh, cunqiiii, fdzhouqi, lilvdanc, shengxrq);
        return new ResultDto<Integer>(result);
    }

}
