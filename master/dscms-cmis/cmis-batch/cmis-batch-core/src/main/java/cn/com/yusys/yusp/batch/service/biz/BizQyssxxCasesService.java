/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.biz;

import cn.com.yusys.yusp.batch.domain.biz.BizQyssxxCases;
import cn.com.yusys.yusp.batch.repository.mapper.biz.BizQyssxxCasesMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Cases;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BizQyssxxCasesService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BizQyssxxCasesService {
    private static final Logger logger = LoggerFactory.getLogger(BizQyssxxCasesService.class);
    @Autowired
    private BizQyssxxCasesMapper bizQyssxxCasesMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BizQyssxxCases selectByPrimaryKey(String appNo, String caseType, String serialNumber) {
        return bizQyssxxCasesMapper.selectByPrimaryKey(appNo, caseType, serialNumber);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BizQyssxxCases> selectAll(QueryModel model) {
        List<BizQyssxxCases> records = (List<BizQyssxxCases>) bizQyssxxCasesMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BizQyssxxCases> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BizQyssxxCases> list = bizQyssxxCasesMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BizQyssxxCases record) {
        return bizQyssxxCasesMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BizQyssxxCases record) {
        record.setInputTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        record.setLastUpdateDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        record.setLastUpdateTime(record.getInputTime());
        return bizQyssxxCasesMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BizQyssxxCases record) {
        return bizQyssxxCasesMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BizQyssxxCases record) {
        return bizQyssxxCasesMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String appNo, String caseType, String serialNumber) {
        return bizQyssxxCasesMapper.deleteByPrimaryKey(appNo, caseType, serialNumber);
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件
     *
     * @param civilcase
     * @param resultMap
     * @return
     */
    public BizQyssxxCases buildByCivilcase(Cases civilcase, Map<String, String> resultMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(resultMap));
        BizQyssxxCases bizQyssxxCases = GenericBuilder.of(BizQyssxxCases::new)
                .with(BizQyssxxCases::setAppNo, resultMap.get("appNo"))
                .with(BizQyssxxCases::setQueryType, resultMap.get("queryType"))// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCases::setCertCode, resultMap.get("certCode"))//
                .with(BizQyssxxCases::setCustName, resultMap.get("cusName"))//
                .with(BizQyssxxCases::setCaseType, "civil")   // 案件大类 ：civil 民事案件 ； criminal 刑事案件；administrative 行政案件；implement 执行案件；bankrupt 强制清算与破产案件
                .with(BizQyssxxCases::setSerialNumber, (DateUtils.getCurrentDate(DateFormatEnum.DATETIME_COMPACT) + UUID.randomUUID().toString().substring(0, 6))) // 案件序号
                .with(BizQyssxxCases::setNAjlx, civilcase.getN_ajlx())// 案件类型
                .with(BizQyssxxCases::setCAh, civilcase.getC_ah())// 案号
                .with(BizQyssxxCases::setCAhYs, civilcase.getC_ah_ys())// 原审案号
                .with(BizQyssxxCases::setCAhHx, civilcase.getC_ah_hx())// 后续案号
                .with(BizQyssxxCases::setNAjbs, civilcase.getN_ajbs())// 案件标识
                .with(BizQyssxxCases::setNJbfy, civilcase.getN_jbfy())// 经办法院
                .with(BizQyssxxCases::setNJbfyCj, civilcase.getN_jbfy_cj())// 法院所属层级 *
                .with(BizQyssxxCases::setNAjjzjd, civilcase.getN_ajjzjd())// 案件进展阶段 *
                .with(BizQyssxxCases::setNSlcx, civilcase.getN_slcx())// 审理程序 *
                .with(BizQyssxxCases::setCSsdy, civilcase.getC_ssdy())// 所属地域
                .with(BizQyssxxCases::setDLarq, civilcase.getD_larq())// 立案时间
                .with(BizQyssxxCases::setNLaay, civilcase.getN_laay())// 立案案由 *
                .with(BizQyssxxCases::setNQsbdje, String.valueOf(civilcase.getN_qsbdje()))// 立案标的金额 *
                .with(BizQyssxxCases::setNQsbdjeGj, String.valueOf(civilcase.getN_qsbdje_gj()))// 立案标的金额估计 *
                .with(BizQyssxxCases::setDJarq, civilcase.getD_jarq())// 结案时间
                .with(BizQyssxxCases::setNJaay, civilcase.getN_jaay())// 结案案由
                .with(BizQyssxxCases::setNJabdje, String.valueOf(civilcase.getN_jabdje()))// 结案标的金额 *
                .with(BizQyssxxCases::setNJabdjeGj, String.valueOf(civilcase.getN_jabdje_gj()))// 结案标的金额估计 *
                .with(BizQyssxxCases::setNJafs, civilcase.getN_jafs())// 结案方式 *
                .with(BizQyssxxCases::setNSsdw, civilcase.getN_ssdw())// 诉讼地位 *
                .with(BizQyssxxCases::setNSsdwYs, civilcase.getN_ssdw_ys())// 一审诉讼地位 *
                .with(BizQyssxxCases::setCGkwsGlah, civilcase.getC_gkws_glah())// 相关案件号
                .with(BizQyssxxCases::setCGkwsDsr, civilcase.getC_gkws_dsr())// 当事人
                .with(BizQyssxxCases::setCGkwsPjjg, civilcase.getC_gkws_pjjg())// 判决结果
                .with(BizQyssxxCases::setNFzje, civilcase.getN_fzje())// 犯罪金额
                .with(BizQyssxxCases::setNBqqpcje, civilcase.getN_bqqpcje())// 被请求赔偿金额
                .with(BizQyssxxCases::setNCcxzxje, civilcase.getN_ccxzxje())// 财产刑执行金额
                .with(BizQyssxxCases::setNCcxzxjeGj, civilcase.getN_ccxzxje_gj())// 财产刑执行金额估计
                .with(BizQyssxxCases::setNPcpcje, civilcase.getN_pcpcje())// 判处赔偿金额
                .with(BizQyssxxCases::setNPcpcjeGj, civilcase.getN_pcpcje_gj())// 判处赔偿金额估计
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
        return bizQyssxxCases;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件
     *
     * @param criminalcase
     * @param resultMap
     * @return
     */
    public BizQyssxxCases buildByCriminalcase(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.criminal.Cases criminalcase, Map<String, String> resultMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(resultMap));
        BizQyssxxCases bizQyssxxCases = GenericBuilder.of(BizQyssxxCases::new)
                .with(BizQyssxxCases::setAppNo, resultMap.get("appNo"))
                .with(BizQyssxxCases::setQueryType, resultMap.get("queryType"))// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCases::setCertCode, resultMap.get("certCode"))//
                .with(BizQyssxxCases::setCustName, resultMap.get("cusName"))//
                .with(BizQyssxxCases::setCaseType, "criminal")   // 案件大类 ：civil 民事案件 ； criminal 刑事案件；administrative 行政案件；implement 执行案件；bankrupt 强制清算与破产案件
                .with(BizQyssxxCases::setSerialNumber, (DateUtils.getCurrentDate(DateFormatEnum.DATETIME_COMPACT) + UUID.randomUUID().toString().substring(0, 6))) // 案件序号
                .with(BizQyssxxCases::setNAjlx, criminalcase.getN_ajlx())// 案件类型
                .with(BizQyssxxCases::setCAh, criminalcase.getC_ah())// 案号
                .with(BizQyssxxCases::setCAhYs, criminalcase.getC_ah_ys())// 原审案号
                .with(BizQyssxxCases::setCAhHx, criminalcase.getC_ah_hx())// 后续案号
                .with(BizQyssxxCases::setNAjbs, criminalcase.getN_ajbs())// 案件标识
                .with(BizQyssxxCases::setNJbfy, criminalcase.getN_jbfy())// 经办法院
                .with(BizQyssxxCases::setNJbfyCj, criminalcase.getN_jbfy_cj())// 法院所属层级 *
                .with(BizQyssxxCases::setNAjjzjd, criminalcase.getN_ajjzjd())// 案件进展阶段 *
                .with(BizQyssxxCases::setNSlcx, criminalcase.getN_slcx())// 审理程序 *
                .with(BizQyssxxCases::setCSsdy, criminalcase.getC_ssdy())// 所属地域
                .with(BizQyssxxCases::setDLarq, criminalcase.getD_larq())// 立案时间
                .with(BizQyssxxCases::setNLaay, criminalcase.getN_laay())// 立案案由 *
                .with(BizQyssxxCases::setNQsbdje, String.valueOf(criminalcase.getN_qsbdje()))// 立案标的金额 *
                .with(BizQyssxxCases::setNQsbdjeGj, String.valueOf(criminalcase.getN_qsbdje_gj()))// 立案标的金额估计 *
                .with(BizQyssxxCases::setDJarq, criminalcase.getD_jarq())// 结案时间
                .with(BizQyssxxCases::setNJaay, criminalcase.getN_jaay())// 结案案由
                .with(BizQyssxxCases::setNJabdje, String.valueOf(criminalcase.getN_jabdje()))// 结案标的金额 *
                .with(BizQyssxxCases::setNJabdjeGj, String.valueOf(criminalcase.getN_jabdje_gj()))// 结案标的金额估计 *
                .with(BizQyssxxCases::setNJafs, criminalcase.getN_jafs())// 结案方式 *
                .with(BizQyssxxCases::setNSsdw, criminalcase.getN_ssdw())// 诉讼地位 *
                .with(BizQyssxxCases::setNSsdwYs, criminalcase.getN_ssdw_ys())// 一审诉讼地位 *
                .with(BizQyssxxCases::setCGkwsGlah, criminalcase.getC_gkws_glah())// 相关案件号
                .with(BizQyssxxCases::setCGkwsDsr, criminalcase.getC_gkws_dsr())// 当事人
                .with(BizQyssxxCases::setCGkwsPjjg, criminalcase.getC_gkws_pjjg())// 判决结果
                .with(BizQyssxxCases::setNFzje, criminalcase.getN_fzje())// 犯罪金额
                .with(BizQyssxxCases::setNBqqpcje, criminalcase.getN_bqqpcje())// 被请求赔偿金额
                .with(BizQyssxxCases::setNCcxzxje, criminalcase.getN_ccxzxje())// 财产刑执行金额
                .with(BizQyssxxCases::setNCcxzxjeGj, criminalcase.getN_ccxzxje_gj())// 财产刑执行金额估计
                .with(BizQyssxxCases::setNPcpcje, criminalcase.getN_pcpcje())// 判处赔偿金额
                .with(BizQyssxxCases::setNPcpcjeGj, criminalcase.getN_pcpcje_gj())// 判处赔偿金额估计
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
        return bizQyssxxCases;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件
     *
     * @param administrativecase
     * @param resultMap
     * @return
     */
    public BizQyssxxCases buildByAdministrativecase(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.administrative.Cases administrativecase, Map<String, String> resultMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(resultMap));
        BizQyssxxCases bizQyssxxCases = GenericBuilder.of(BizQyssxxCases::new)
                .with(BizQyssxxCases::setAppNo, resultMap.get("appNo"))
                .with(BizQyssxxCases::setQueryType, resultMap.get("queryType"))// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCases::setCertCode, resultMap.get("certCode"))//
                .with(BizQyssxxCases::setCustName, resultMap.get("cusName"))//
                .with(BizQyssxxCases::setCaseType, "administrative")   // 案件大类 ：civil 民事案件 ； criminal 刑事案件；administrative 行政案件；implement 执行案件；bankrupt 强制清算与破产案件
                .with(BizQyssxxCases::setSerialNumber, (DateUtils.getCurrentDate(DateFormatEnum.DATETIME_COMPACT) + UUID.randomUUID().toString().substring(0, 6))) // 案件序号
                .with(BizQyssxxCases::setNAjlx, administrativecase.getN_ajlx())// 案件类型
                .with(BizQyssxxCases::setCAh, administrativecase.getC_ah())// 案号
                .with(BizQyssxxCases::setCAhYs, administrativecase.getC_ah_ys())// 原审案号
                .with(BizQyssxxCases::setCAhHx, administrativecase.getC_ah_hx())// 后续案号
                .with(BizQyssxxCases::setNAjbs, administrativecase.getN_ajbs())// 案件标识
                .with(BizQyssxxCases::setNJbfy, administrativecase.getN_jbfy())// 经办法院
                .with(BizQyssxxCases::setNJbfyCj, administrativecase.getN_jbfy_cj())// 法院所属层级 *
                .with(BizQyssxxCases::setNAjjzjd, administrativecase.getN_ajjzjd())// 案件进展阶段 *
                .with(BizQyssxxCases::setNSlcx, administrativecase.getN_slcx())// 审理程序 *
                .with(BizQyssxxCases::setCSsdy, administrativecase.getC_ssdy())// 所属地域
                .with(BizQyssxxCases::setDLarq, administrativecase.getD_larq())// 立案时间
                .with(BizQyssxxCases::setNLaay, administrativecase.getN_laay())// 立案案由 *
                .with(BizQyssxxCases::setNQsbdje, String.valueOf(administrativecase.getN_qsbdje()))// 立案标的金额 *
                .with(BizQyssxxCases::setNQsbdjeGj, String.valueOf(administrativecase.getN_qsbdje_gj()))// 立案标的金额估计 *
                .with(BizQyssxxCases::setDJarq, administrativecase.getD_jarq())// 结案时间
                .with(BizQyssxxCases::setNJaay, administrativecase.getN_jaay())// 结案案由
                .with(BizQyssxxCases::setNJabdje, String.valueOf(administrativecase.getN_jabdje()))// 结案标的金额 *
                .with(BizQyssxxCases::setNJabdjeGj, String.valueOf(administrativecase.getN_jabdje_gj()))// 结案标的金额估计 *
                .with(BizQyssxxCases::setNJafs, administrativecase.getN_jafs())// 结案方式 *
                .with(BizQyssxxCases::setNSsdw, administrativecase.getN_ssdw())// 诉讼地位 *
                .with(BizQyssxxCases::setNSsdwYs, administrativecase.getN_ssdw_ys())// 一审诉讼地位 *
                .with(BizQyssxxCases::setCGkwsGlah, administrativecase.getC_gkws_glah())// 相关案件号
                .with(BizQyssxxCases::setCGkwsDsr, administrativecase.getC_gkws_dsr())// 当事人
                .with(BizQyssxxCases::setCGkwsPjjg, administrativecase.getC_gkws_pjjg())// 判决结果
                .with(BizQyssxxCases::setNFzje, administrativecase.getN_fzje())// 犯罪金额
                .with(BizQyssxxCases::setNBqqpcje, administrativecase.getN_bqqpcje())// 被请求赔偿金额
                .with(BizQyssxxCases::setNCcxzxje, administrativecase.getN_ccxzxje())// 财产刑执行金额
                .with(BizQyssxxCases::setNCcxzxjeGj, administrativecase.getN_ccxzxje_gj())// 财产刑执行金额估计
                .with(BizQyssxxCases::setNPcpcje, administrativecase.getN_pcpcje())// 判处赔偿金额
                .with(BizQyssxxCases::setNPcpcjeGj, administrativecase.getN_pcpcje_gj())// 判处赔偿金额估计
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
        return bizQyssxxCases;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件
     *
     * @param implementcase
     * @param resultMap
     * @return
     */
    public BizQyssxxCases buildByImplementcase(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.implement.Cases implementcase, Map<String, String> resultMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(resultMap));
        BizQyssxxCases bizQyssxxCases = GenericBuilder.of(BizQyssxxCases::new)
                .with(BizQyssxxCases::setAppNo, resultMap.get("appNo"))
                .with(BizQyssxxCases::setQueryType, resultMap.get("queryType"))// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCases::setCertCode, resultMap.get("certCode"))//
                .with(BizQyssxxCases::setCustName, resultMap.get("cusName"))//
                .with(BizQyssxxCases::setCaseType, "implement")   // 案件大类 ：civil 民事案件 ； criminal 刑事案件；administrative 行政案件；implement 执行案件；bankrupt 强制清算与破产案件
                .with(BizQyssxxCases::setSerialNumber, (DateUtils.getCurrentDate(DateFormatEnum.DATETIME_COMPACT) + UUID.randomUUID().toString().substring(0, 6))) // 案件序号
                .with(BizQyssxxCases::setNAjlx, implementcase.getN_ajlx())// 案件类型
                .with(BizQyssxxCases::setCAh, implementcase.getC_ah())// 案号
                .with(BizQyssxxCases::setCAhYs, implementcase.getC_ah_ys())// 原审案号
                .with(BizQyssxxCases::setCAhHx, implementcase.getC_ah_hx())// 后续案号
                .with(BizQyssxxCases::setNAjbs, implementcase.getN_ajbs())// 案件标识
                .with(BizQyssxxCases::setNJbfy, implementcase.getN_jbfy())// 经办法院
                .with(BizQyssxxCases::setNJbfyCj, implementcase.getN_jbfy_cj())// 法院所属层级 *
                .with(BizQyssxxCases::setNAjjzjd, implementcase.getN_ajjzjd())// 案件进展阶段 *
                .with(BizQyssxxCases::setNSlcx, implementcase.getN_slcx())// 审理程序 *
                .with(BizQyssxxCases::setCSsdy, implementcase.getC_ssdy())// 所属地域
                .with(BizQyssxxCases::setDLarq, implementcase.getD_larq())// 立案时间
                .with(BizQyssxxCases::setNLaay, implementcase.getN_laay())// 立案案由 *
                .with(BizQyssxxCases::setNQsbdje, String.valueOf(implementcase.getN_qsbdje()))// 立案标的金额 *
                .with(BizQyssxxCases::setNQsbdjeGj, String.valueOf(implementcase.getN_qsbdje_gj()))// 立案标的金额估计 *
                .with(BizQyssxxCases::setDJarq, implementcase.getD_jarq())// 结案时间
                .with(BizQyssxxCases::setNJaay, implementcase.getN_jaay())// 结案案由
                .with(BizQyssxxCases::setNJabdje, String.valueOf(implementcase.getN_jabdje()))// 结案标的金额 *
                .with(BizQyssxxCases::setNJabdjeGj, String.valueOf(implementcase.getN_jabdje_gj()))// 结案标的金额估计 *
                .with(BizQyssxxCases::setNJafs, implementcase.getN_jafs())// 结案方式 *
                .with(BizQyssxxCases::setNSsdw, implementcase.getN_ssdw())// 诉讼地位 *
                .with(BizQyssxxCases::setNSsdwYs, implementcase.getN_ssdw_ys())// 一审诉讼地位 *
                .with(BizQyssxxCases::setCGkwsGlah, implementcase.getC_gkws_glah())// 相关案件号
                .with(BizQyssxxCases::setCGkwsDsr, implementcase.getC_gkws_dsr())// 当事人
                .with(BizQyssxxCases::setCGkwsPjjg, implementcase.getC_gkws_pjjg())// 判决结果
                .with(BizQyssxxCases::setNFzje, implementcase.getN_fzje())// 犯罪金额
                .with(BizQyssxxCases::setNBqqpcje, implementcase.getN_bqqpcje())// 被请求赔偿金额
                .with(BizQyssxxCases::setNCcxzxje, implementcase.getN_ccxzxje())// 财产刑执行金额
                .with(BizQyssxxCases::setNCcxzxjeGj, implementcase.getN_ccxzxje_gj())// 财产刑执行金额估计
                .with(BizQyssxxCases::setNPcpcje, implementcase.getN_pcpcje())// 判处赔偿金额
                .with(BizQyssxxCases::setNPcpcjeGj, implementcase.getN_pcpcje_gj())// 判处赔偿金额估计
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
        return bizQyssxxCases;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件
     *
     * @param bankruptcase
     * @param resultMap
     * @return
     */
    public BizQyssxxCases buildByBankruptcase(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.bankrupt.Cases bankruptcase, Map<String, String> resultMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(resultMap));
        BizQyssxxCases bizQyssxxCases = GenericBuilder.of(BizQyssxxCases::new)
                .with(BizQyssxxCases::setAppNo, resultMap.get("appNo"))
                .with(BizQyssxxCases::setQueryType, resultMap.get("queryType"))// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCases::setCertCode, resultMap.get("certCode"))//
                .with(BizQyssxxCases::setCustName, resultMap.get("cusName"))//
                .with(BizQyssxxCases::setCaseType, "bankrupt")   // 案件大类 ：civil 民事案件 ； criminal 刑事案件；administrative 行政案件；implement 执行案件；bankrupt 强制清算与破产案件
                .with(BizQyssxxCases::setSerialNumber, (DateUtils.getCurrentDate(DateFormatEnum.DATETIME_COMPACT) + UUID.randomUUID().toString().substring(0, 6))) // 案件序号
                .with(BizQyssxxCases::setNAjlx, bankruptcase.getN_ajlx())// 案件类型
                .with(BizQyssxxCases::setCAh, bankruptcase.getC_ah())// 案号
                .with(BizQyssxxCases::setCAhYs, bankruptcase.getC_ah_ys())// 原审案号
                .with(BizQyssxxCases::setCAhHx, bankruptcase.getC_ah_hx())// 后续案号
                .with(BizQyssxxCases::setNAjbs, bankruptcase.getN_ajbs())// 案件标识
                .with(BizQyssxxCases::setNJbfy, bankruptcase.getN_jbfy())// 经办法院
                .with(BizQyssxxCases::setNJbfyCj, bankruptcase.getN_jbfy_cj())// 法院所属层级 *
                .with(BizQyssxxCases::setNAjjzjd, bankruptcase.getN_ajjzjd())// 案件进展阶段 *
                .with(BizQyssxxCases::setNSlcx, bankruptcase.getN_slcx())// 审理程序 *
                .with(BizQyssxxCases::setCSsdy, bankruptcase.getC_ssdy())// 所属地域
                .with(BizQyssxxCases::setDLarq, bankruptcase.getD_larq())// 立案时间
                .with(BizQyssxxCases::setNLaay, bankruptcase.getN_laay())// 立案案由 *
                .with(BizQyssxxCases::setNQsbdje, String.valueOf(bankruptcase.getN_qsbdje()))// 立案标的金额 *
                .with(BizQyssxxCases::setNQsbdjeGj, String.valueOf(bankruptcase.getN_qsbdje_gj()))// 立案标的金额估计 *
                .with(BizQyssxxCases::setDJarq, bankruptcase.getD_jarq())// 结案时间
                .with(BizQyssxxCases::setNJaay, bankruptcase.getN_jaay())// 结案案由
                .with(BizQyssxxCases::setNJabdje, String.valueOf(bankruptcase.getN_jabdje()))// 结案标的金额 *
                .with(BizQyssxxCases::setNJabdjeGj, String.valueOf(bankruptcase.getN_jabdje_gj()))// 结案标的金额估计 *
                .with(BizQyssxxCases::setNJafs, bankruptcase.getN_jafs())// 结案方式 *
                .with(BizQyssxxCases::setNSsdw, bankruptcase.getN_ssdw())// 诉讼地位 *
                .with(BizQyssxxCases::setNSsdwYs, bankruptcase.getN_ssdw_ys())// 一审诉讼地位 *
                .with(BizQyssxxCases::setCGkwsGlah, bankruptcase.getC_gkws_glah())// 相关案件号
                .with(BizQyssxxCases::setCGkwsDsr, bankruptcase.getC_gkws_dsr())// 当事人
                .with(BizQyssxxCases::setCGkwsPjjg, bankruptcase.getC_gkws_pjjg())// 判决结果
                .with(BizQyssxxCases::setNFzje, bankruptcase.getN_fzje())// 犯罪金额
                .with(BizQyssxxCases::setNBqqpcje, bankruptcase.getN_bqqpcje())// 被请求赔偿金额
                .with(BizQyssxxCases::setNCcxzxje, bankruptcase.getN_ccxzxje())// 财产刑执行金额
                .with(BizQyssxxCases::setNCcxzxjeGj, bankruptcase.getN_ccxzxje_gj())// 财产刑执行金额估计
                .with(BizQyssxxCases::setNPcpcje, bankruptcase.getN_pcpcje())// 判处赔偿金额
                .with(BizQyssxxCases::setNPcpcjeGj, bankruptcase.getN_pcpcje_gj())// 判处赔偿金额估计
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
        return bizQyssxxCases;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件
     *
     * @param preservationcase
     * @param resultMap
     * @return
     */
    public BizQyssxxCases buildByPreservationcase(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.preservation.Cases preservationcase, Map<String, String> resultMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件开始,请求参数为:[{}]", JSON.toJSONString(resultMap));
        BizQyssxxCases bizQyssxxCases = GenericBuilder.of(BizQyssxxCases::new)
                .with(BizQyssxxCases::setAppNo, resultMap.get("appNo"))
                .with(BizQyssxxCases::setQueryType, resultMap.get("queryType"))// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCases::setCertCode, resultMap.get("certCode"))//
                .with(BizQyssxxCases::setCustName, resultMap.get("cusName"))//
                .with(BizQyssxxCases::setCaseType, "preservation")   // 案件大类 ：civil 民事案件 ； criminal 刑事案件；administrative 行政案件；implement 执行案件；bankrupt 强制清算与破产案件
                .with(BizQyssxxCases::setSerialNumber, (DateUtils.getCurrentDate(DateFormatEnum.DATETIME_COMPACT) + UUID.randomUUID().toString().substring(0, 6))) // 案件序号
                .with(BizQyssxxCases::setNAjlx, preservationcase.getN_ajlx())// 案件类型
                .with(BizQyssxxCases::setCAh, preservationcase.getC_ah())// 案号
                .with(BizQyssxxCases::setCAhYs, preservationcase.getC_ah_ys())// 原审案号
                .with(BizQyssxxCases::setCAhHx, preservationcase.getC_ah_hx())// 后续案号
                .with(BizQyssxxCases::setNAjbs, preservationcase.getN_ajbs())// 案件标识
                .with(BizQyssxxCases::setNJbfy, preservationcase.getN_jbfy())// 经办法院
                .with(BizQyssxxCases::setNJbfyCj, preservationcase.getN_jbfy_cj())// 法院所属层级 *
                .with(BizQyssxxCases::setNAjjzjd, preservationcase.getN_ajjzjd())// 案件进展阶段 *
                .with(BizQyssxxCases::setNSlcx, preservationcase.getN_slcx())// 审理程序 *
                .with(BizQyssxxCases::setCSsdy, preservationcase.getC_ssdy())// 所属地域
                .with(BizQyssxxCases::setDLarq, preservationcase.getD_larq())// 立案时间
                .with(BizQyssxxCases::setNLaay, preservationcase.getN_laay())// 立案案由 *
                .with(BizQyssxxCases::setNQsbdje, String.valueOf(preservationcase.getN_qsbdje()))// 立案标的金额 *
                .with(BizQyssxxCases::setNQsbdjeGj, String.valueOf(preservationcase.getN_qsbdje_gj()))// 立案标的金额估计 *
                .with(BizQyssxxCases::setDJarq, preservationcase.getD_jarq())// 结案时间
                .with(BizQyssxxCases::setNJaay, preservationcase.getN_jaay())// 结案案由
                .with(BizQyssxxCases::setNJabdje, String.valueOf(preservationcase.getN_jabdje()))// 结案标的金额 *
                .with(BizQyssxxCases::setNJabdjeGj, String.valueOf(preservationcase.getN_jabdje_gj()))// 结案标的金额估计 *
                .with(BizQyssxxCases::setNJafs, preservationcase.getN_jafs())// 结案方式 *
                .with(BizQyssxxCases::setNSsdw, preservationcase.getN_ssdw())// 诉讼地位 *
                .with(BizQyssxxCases::setNSsdwYs, preservationcase.getN_ssdw_ys())// 一审诉讼地位 *
                .with(BizQyssxxCases::setCGkwsGlah, preservationcase.getC_gkws_glah())// 相关案件号
                .with(BizQyssxxCases::setCGkwsDsr, preservationcase.getC_gkws_dsr())// 当事人
                .with(BizQyssxxCases::setCGkwsPjjg, preservationcase.getC_gkws_pjjg())// 判决结果
                .with(BizQyssxxCases::setNFzje, preservationcase.getN_fzje())// 犯罪金额
                .with(BizQyssxxCases::setNBqqpcje, preservationcase.getN_bqqpcje())// 被请求赔偿金额
                .with(BizQyssxxCases::setNCcxzxje, preservationcase.getN_ccxzxje())// 财产刑执行金额
                .with(BizQyssxxCases::setNCcxzxjeGj, preservationcase.getN_ccxzxje_gj())// 财产刑执行金额估计
                .with(BizQyssxxCases::setNPcpcje, preservationcase.getN_pcpcje())// 判处赔偿金额
                .with(BizQyssxxCases::setNPcpcjeGj, preservationcase.getN_pcpcje_gj())// 判处赔偿金额估计
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)案件结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCases));
        return bizQyssxxCases;
    }
}
