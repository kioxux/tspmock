package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0211</br>
 * 任务名称：加工任务-额度处理-Comstar额度处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0211Mapper {
    /**
     * 更新Comstar全量交易明细表中[trade_type]字段
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsDetailZjg00(@Param("openDay") String openDay);

    /**
     * 更新Comstar全量交易明细表中[start_date]字段
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsDetailZjg01(@Param("openDay") String openDay);

    /**
     * 更新Comstar全量交易明细表中[end_date]字段
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsDetailZjg02(@Param("openDay") String openDay);

    /**
     * 更新Comstar全量交易明细表中[bat_date]字段
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsDetailZjg03(@Param("openDay") String openDay);

    /**
     * 更新Comstar额度已用表中[bat_date]字段
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsAvailableZjg01(@Param("openDay") String openDay);

    /**
     * 清空Comstar与白名单额度信息表
     */
    void deleteTmpSbsAvailLmtWhiteInfo0A();

    /**
     * 插入Comstar与白名单额度信息表
     *
     * @param openDay
     * @return
     */
    int insertTmpSbsAvailLmtWhiteInfo0A(@Param("openDay") String openDay);

    /**
     * 更新Comstar全量交易明细表中[LMT_ID]字段为额度编号
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsAvailableZjg0A(@Param("openDay") String openDay);

    /**
     * 清空Comstar与批复额度分项基础信息
     */
    void deleteBatSSbsAvailableZjg0B();

    /**
     * 插入Comstar与批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int insertBatSSbsAvailableZjg0B(@Param("openDay") String openDay);

    /**
     * 更新Comstar全量交易明细表中[LMT_ID]字段为额度编号
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsAvailableZjg0B(@Param("openDay") String openDay);

    /**
     * 清空Comstar与批复额度分项基础信息(资产编号)
     */
    void deleteBatSSbsAvailableZjg0C();

    /**
     * 插入Comstar与批复额度分项基础信息(资产编号)
     *
     * @param openDay
     * @return
     */
    int insertBatSSbsAvailableZjg0C(@Param("openDay") String openDay);

    /**
     * 更新Comstar全量交易明细表中[LMT_ID]字段为额度编号
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsAvailableZjg0C(@Param("openDay") String openDay);

    /**
     * 清空Comstar与白名单额度信息表
     */
    void deleteBatSSbsDetailZjg0A();

    /**
     * 插入Comstar与白名单额度信息表
     *
     * @param openDay
     * @return
     */
    int insertBatSSbsDetailZjg0A(@Param("openDay") String openDay);

    /**
     * 更新Comstar全量交易明细表中[LMT_ID]字段为额度编号
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsDetailZjg0A(@Param("openDay") String openDay);

    /**
     * 清空Comstar与批复额度分项基础信息
     */
    void deleteBatSSbsDetailZjg0B();

    /**
     * 插入Comstar与批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int insertBatSSbsDetailZjg0B(@Param("openDay") String openDay);

    /**
     * 更新Comstar全量交易明细表中[LMT_ID]字段为额度编号
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsDetailZjg0B(@Param("openDay") String openDay);

    /**
     * 清空Comstar与批复额度分项基础信息(资产编号)
     */
    void deleteBatSSbsDetailZjg0C();

    /**
     * 插入Comstar与批复额度分项基础信息(资产编号)
     *
     * @param openDay
     * @return
     */
    int insertBatSSbsDetailZjg0C(@Param("openDay") String openDay);

    /**
     * 更新Comstar全量交易明细表中[LMT_ID]字段为额度编号
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsDetailZjg0C(@Param("openDay") String openDay);

    /**
     * 清空Comstar与批复额度分项基础信息
     */
    void deleteBatSSbsDetailZjg1();

    /**
     * 插入Comstar与批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int insertBatSSbsDetailZjg1(@Param("openDay") String openDay);

    /**
     * 更新Comstar全量交易明细表中[cus_id]字段
     *
     * @param openDay
     * @return
     */
    int updateBatSSbsDetailZjg1(@Param("openDay") String openDay);

    /**
     * 当天发生的新的占用插入至占用关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel(@Param("openDay") String openDay);

    /**
     * 清空授信占用关系表
     */
    void deleteLmtContRelCusName();

    /**
     * 插入授信占用关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelCusName(@Param("openDay") String openDay);

    /**
     * 增加关联同业客户信息
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelCusIntBankName(@Param("openDay") String openDay);

    /**
     * 更新占用关系表中的[cus_name]
     *
     * @param openDay
     * @return
     */
    int updateLmtContRelCusName(@Param("openDay") String openDay);

    /**
     * 清空授信占用关系表
     */
    void deleteLmtContRelLmtType();

    /**
     * 插入授信占用关系表
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelLmtType(@Param("openDay") String openDay);

    /**
     * 更新占用关系表中的[lmt_type]
     *
     * @param openDay
     * @return
     */
    int updateLmtContRelLmtType(@Param("openDay") String openDay);

    /**
     * ComStar的占用关系存在有余额，但是ComStar当日无数据的，则更新交易余额为0
     *
     * @param openDay
     * @return
     */
    int insertLmtContRelNonCom(@Param("openDay") String openDay);

    /**
     * ComStar的占用关系存在有余额，但是ComStar当日无数据的，则更新交易余额为0
     *
     * @param
     * @return
     */
    int updateLmtContRelNonCom(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void deleteLmtContRel01A();

    /**
     * 插入加工占用授信表
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel01A(@Param("openDay") String openDay);

    /**
     * 更新分项占用关系信息:3001-同业融资类额度.3005-同业交易类额度.300601-货币基金额度.300701-产品户买入反售额度(信用债)
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel01A(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void deleteLmtContRel01B();

    /**
     * 插入加工占用授信表
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel01B(@Param("openDay") String openDay);

    /**
     * 更新分项占用关系信息:300701-产品户买入反售额度(信用债)  数据在白名单额度表
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel01B(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void deleteLmtContRel02();

    /**
     * 插入加工占用授信表
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel02(@Param("openDay") String openDay);

    /**
     * 更新分项占用关系信息:3001-同业融资类额度.3005-同业交易类额度.300601-货币基金额度.300701-产品户买入反售额度(信用债)
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel02(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void deleteApprLmtSubBasicInfo();

    /**
     * 插入加工占用授信表
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo(@Param("openDay") String openDay);

    /**
     * 更新批复额度分项基础信息表中额度
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void deleteLmtWhiteInfo();

    /**
     * 插入加工占用授信表
     *
     * @param openDay
     * @return
     */
    int insertLmtWhiteInfo(@Param("openDay") String openDay);

    /**
     * 更新白名单额度信息表中[sig_use_amt]
     *
     * @param openDay
     * @return
     */
    int updateLmtWhiteInfo(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void deleteApprLmtSubBasicInfo11();

    /**
     * 插入加工占用授信表
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo11(@Param("openDay") String openDay);

    /**
     * 更新批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo11(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void deleteApprLmtSubBasicInfo12();

    /**
     * 插入加工占用授信表
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo12(@Param("openDay") String openDay);

    /**
     * 更新批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo12(@Param("openDay") String openDay);

    /**
     * 更新lmt_cont_rel中产品授信占用同业管理类/投资类的交易记录  占用余额  占用敞口余额
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel11(@Param("openDay") String openDay);

    /**
     * 额度分项如果为货币基金额度（LIMIT_SUB_NO=300601），则更新货币基金额度的父分项已用金额 = 已用金额+货币基金额度的已用；
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel21A(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void deleteLmtContRel21B();

    /**
     * 插入加工占用授信表
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel21B(@Param("openDay") String openDay);

    /**
     * 更新同业管理类额度、同业投资类的占用为管理类额度下关联的单笔投资授信总额，且余额更新为0
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel21B(@Param("openDay") String openDay);

    /**
     * 批复额度分项基础信息
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel24(@Param("openDay") String openDay);

    /**
     * updateTmpDealLmtDetails03-若3002-同业投资类、3006-同业管理类、15030101-穿透化额度已用金额=0，且额度已到期，则分项状态更新为99-失效已结清；
     *
     * @return
     */
    int updateTmpDealLmtDetails03(@Param("openDay") String openDay);

    /**
     * updateTmpDealLmtDetails04-若3002-同业投资类、3006-同业管理类、15030101-穿透化额度已用金额>0，且额度已到期，则分项状态更新为99-失效未结清；
     *
     * @param openDay
     * @return
     */
    int updateTmpDealLmtDetails04(@Param("openDay") String openDay);

    /**
     * updateTmpDealLmtDetails05-承销类额度到期的，更新状态为99
     *
     * @param openDay
     * @return
     */
    int updateTmpDealLmtDetails05(@Param("openDay") String openDay);


    /**
     * updateTmpDealLmtDetails02- 判断3001、3005、300601 以及单笔投资额度 是否额度已到期且已用金额>0，若是，则更新额度状态为90-失效未结清；
     *
     * @param openDay
     * @return
     */
    int updateTmpDealLmtDetails02(@Param("openDay") String openDay);

    /**
     * updateTmpDealLmtDetails01-判断3001、3005、300601 以及单笔投资额度是否已用金额为0且额度已到期，若是，则更新额度状态为99-失效已结清；
     *
     * @param openDay
     * @return
     */
    int updateTmpDealLmtDetails01(@Param("openDay") String openDay);


    /**
     * 清空加工占用授信表
     */
    void deleteLmtContRel31();

    /**
     * 插入加工占用授信表
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel31(@Param("openDay") String openDay);

    /**
     * 更新批复额度分项基础信息表中[SPAC_OUTSTND_AMT]
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel31(@Param("openDay") String openDay);


    /**
     * 更新分项占用关系信息加工表
     *
     * @param openDay
     * @return
     */
    int updateTmpDealLmtContRel(@Param("openDay") String openDay);
}
