/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatTCoreKlnaDkzhzr;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatTCoreKlnaDkzhzrMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: yusp-batch-core模块
 * @类名称: BatTCoreKlnaDkzhzrService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-08 23:46:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTCoreKlnaDkzhzrService {
    private static final Logger logger = LoggerFactory.getLogger(BatTCoreKlnaDkzhzrService.class);
    @Autowired
    private BatTCoreKlnaDkzhzrMapper batTCoreKlnaDkzhzrMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatTCoreKlnaDkzhzr selectByPrimaryKey(String farendma, String dkjiejuh) {
        return batTCoreKlnaDkzhzrMapper.selectByPrimaryKey(farendma, dkjiejuh);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatTCoreKlnaDkzhzr> selectAll(QueryModel model) {
        List<BatTCoreKlnaDkzhzr> records = (List<BatTCoreKlnaDkzhzr>) batTCoreKlnaDkzhzrMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatTCoreKlnaDkzhzr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTCoreKlnaDkzhzr> list = batTCoreKlnaDkzhzrMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatTCoreKlnaDkzhzr record) {
        return batTCoreKlnaDkzhzrMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatTCoreKlnaDkzhzr record) {
        return batTCoreKlnaDkzhzrMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatTCoreKlnaDkzhzr record) {
        return batTCoreKlnaDkzhzrMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatTCoreKlnaDkzhzr record) {
        return batTCoreKlnaDkzhzrMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String farendma, String dkjiejuh) {
        return batTCoreKlnaDkzhzrMapper.deleteByPrimaryKey(farendma, dkjiejuh);
    }

    /**
     * 清空落地表
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void truncateTTable() {
        // 重建相关表
        logger.info("重建贷款账户昨日余额表[bat_t_core_klna_dkzhzr]开始,请求参数为:[{}]");
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_t_core_klna_dkzhzr");
        logger.info("重建【CORE0002】贷款账户昨日余额表[bat_t_core_klna_dkzhzr]结束");
    }

}
