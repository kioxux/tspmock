/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.domain.step;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchStepExecutionContext
 * @类描述: batch_step_execution_context数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:02:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "batch_step_execution_context")
public class BatchStepExecutionContext extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 作业步实例ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "STEP_EXECUTION_ID")
	private Long stepExecutionId;
	
	/** 一个字符串版本的SERIALIZED_CONTEXT **/
	@Column(name = "SHORT_CONTEXT", unique = false, nullable = false, length = 2500)
	private String shortContext;
	
	/** 整个上下文序列化 **/
	@Column(name = "SERIALIZED_CONTEXT", unique = false, nullable = true, length = 65535)
	private String serializedContext;
	
	
	/**
	 * @param stepExecutionId
	 */
	public void setStepExecutionId(Long stepExecutionId) {
		this.stepExecutionId = stepExecutionId;
	}
	
    /**
     * @return stepExecutionId
     */
	public Long getStepExecutionId() {
		return this.stepExecutionId;
	}
	
	/**
	 * @param shortContext
	 */
	public void setShortContext(String shortContext) {
		this.shortContext = shortContext;
	}
	
    /**
     * @return shortContext
     */
	public String getShortContext() {
		return this.shortContext;
	}
	
	/**
	 * @param serializedContext
	 */
	public void setSerializedContext(String serializedContext) {
		this.serializedContext = serializedContext;
	}
	
    /**
     * @return serializedContext
     */
	public String getSerializedContext() {
		return this.serializedContext;
	}


}