package cn.com.yusys.yusp.batch.service.bak;


import cn.com.yusys.yusp.batch.repository.mapper.bak.BakD0031Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKD0031</br>
 * 任务名称：批前备份日表任务-备份福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ] </br>
 *
 * @author chenyong
 * @version 1.0
 * @since 2021年9月24日
 */
@Service
@Transactional
public class BakD0031Service {
    private static final Logger logger = LoggerFactory.getLogger(BakD0031Service.class);

    @Autowired
    private BakD0031Mapper bakD0031Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 备份当天的数据
     *
     * @param openDay
     */
    public void bakD0031InsertCurrent(String openDay) {
        // 备份当天的数据
        logger.info("备份当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakD0031Mapper.insertCurrent(openDay);
        logger.info("备份当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据结束,返回参数为:[{}]", insertCurrent);
    }


    /**
     * 删除当天的数据
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void bakD0031DeleteCurrent(String openDay) {
        logger.info("删除当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据开始,请求参数为:[{}]", openDay);
        // bakD0031Mapper.truncateCurrent();// cmis_batch.tmp_d_lmt_fft_rzkz
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_d_lmt_fft_rzkz");// 20211030 调整为调用重命名创建和删除相关表的公共方法
        logger.info("删除当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据结束");

//        logger.info("查询待删除当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据总次数开始,请求参数为:[{}]", openDay);
//        int queryD0031DeleteOpenDayTmpGjpLmtFftRzkzCounts = bakD0031Mapper.queryD0031DeleteOpenDayTmpGjpLmtFftRzkzCounts(openDay);
//        // 由于TDSQL不支持ceiling函数，此处在java中处理
//        BigDecimal times = new BigDecimal(queryD0031DeleteOpenDayTmpGjpLmtFftRzkzCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
//        queryD0031DeleteOpenDayTmpGjpLmtFftRzkzCounts = times.intValue();
//        logger.info("查询待删除当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据总次数结束,返回参数为:[{}]", queryD0031DeleteOpenDayTmpGjpLmtFftRzkzCounts);
//        // 删除当天的数据
//        for (int i = 0; i <= queryD0031DeleteOpenDayTmpGjpLmtFftRzkzCounts; i++) {
//            logger.info("第[" + i + "]次删除当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据开始,请求参数为:[{}]", openDay);
//            int deleteCurrent = bakD0031Mapper.deleteCurrent(openDay);
//            logger.info("第[" + i + "]次删除当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据结束,返回参数为:[{}]", deleteCurrent);
//        }
    }


    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakDEqualsOriginal(String openDay) {
        logger.info("查询当天备份的[福费廷同业类登记簿[tmp_d_lmt_fft_rzkz]]数据总条数]数据总条数开始,请求参数为:[{}]", openDay);
        int bakDCounts = bakD0031Mapper.queryD0031DeleteOpenDayTmpGjpLmtFftRzkzCounts(openDay);
        logger.info("查询当天备份的[福费廷同业类登记簿[tmp_d_lmt_fft_rzkz]]数据总条数]数据总条数结束,返回参数为:[{}]", bakDCounts);

        logger.info("查询当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]原表数据总条数开始,请求参数为:[{}]", openDay);
        int originalCounts = bakD0031Mapper.queryTmpGjpLmtFftRzkz(openDay);
        logger.info("查询当天的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

        if (bakDCounts != originalCounts) {
            logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakDCounts, originalCounts);
            throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakDCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
        } else {
            logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakDCounts, originalCounts);
        }
    }
}
