/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.gjp ;

import cn.com.yusys.yusp.batch.domain.load.gjp.BatTGjpTfbCmsLgInfoLog;
import cn.com.yusys.yusp.batch.repository.mapper.load.gjp.BatTGjpTfbCmsLgInfoLogMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTGjpTfbCmsLgInfoLogService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 21:47:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTGjpTfbCmsLgInfoLogService {

    @Autowired
    private BatTGjpTfbCmsLgInfoLogMapper batTGjpTfbCmsLgInfoLogMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTGjpTfbCmsLgInfoLog selectByPrimaryKey(String id) {
        return batTGjpTfbCmsLgInfoLogMapper.selectByPrimaryKey(id);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTGjpTfbCmsLgInfoLog> selectAll(QueryModel model) {
        List<BatTGjpTfbCmsLgInfoLog> records = (List<BatTGjpTfbCmsLgInfoLog>) batTGjpTfbCmsLgInfoLogMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTGjpTfbCmsLgInfoLog> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTGjpTfbCmsLgInfoLog> list = batTGjpTfbCmsLgInfoLogMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTGjpTfbCmsLgInfoLog record) {
        return batTGjpTfbCmsLgInfoLogMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTGjpTfbCmsLgInfoLog record) {
        return batTGjpTfbCmsLgInfoLogMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTGjpTfbCmsLgInfoLog record) {
        return batTGjpTfbCmsLgInfoLogMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTGjpTfbCmsLgInfoLog record) {
        return batTGjpTfbCmsLgInfoLogMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String id) {
        return batTGjpTfbCmsLgInfoLogMapper.deleteByPrimaryKey(id);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batTGjpTfbCmsLgInfoLogMapper.deleteByIds(ids);
    }

    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTGjpTfbCmsLgInfoLogMapper.truncateTTable();
    }
}
