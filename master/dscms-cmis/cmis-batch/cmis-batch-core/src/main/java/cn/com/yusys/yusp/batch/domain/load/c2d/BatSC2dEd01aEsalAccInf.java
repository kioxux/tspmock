/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.c2d;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dEd01aEsalAccInf
 * @类描述: bat_s_c2d_ed01a_esal_acc_inf数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 09:55:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_c2d_ed01a_esal_acc_inf")
public class BatSC2dEd01aEsalAccInf extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** ID **/
	@Id
	@Column(name = "ID")
	private String id;
	
	/** 报告编号 **/
	@Id
	@Column(name = "REPORT_ID")
	private String reportId;
	
	/** 借贷账户编号 **/
	@Column(name = "LE_ACC_NUMBER", unique = false, nullable = true, length = 8)
	private String leAccNumber;
	
	/** 账户活动状态 **/
	@Column(name = "ACCOUNT_STATE", unique = false, nullable = true, length = 60)
	private String accountState;
	
	/** 借贷账户类型 **/
	@Column(name = "LE_ACCOUNT_TYPE", unique = false, nullable = true, length = 60)
	private String leAccountType;
	
	/** 借款期限 **/
	@Column(name = "LOAN_PEROID", unique = false, nullable = true, length = 60)
	private String loanPeroid;
	
	/** 业务管理机构类型 **/
	@Column(name = "BM_ORG_TYPE", unique = false, nullable = true, length = 60)
	private String bmOrgType;
	
	/** 业务管理机构代码 **/
	@Column(name = "BM_ORG_NUMBER", unique = false, nullable = true, length = 60)
	private String bmOrgNumber;
	
	/** 授信协议编号 **/
	@Column(name = "CREDIT_NUMBER", unique = false, nullable = true, length = 8)
	private String creditNumber;
	
	/** 借贷业务种类大类 **/
	@Column(name = "CATEGORY_MAX", unique = false, nullable = true, length = 60)
	private String categoryMax;
	
	/** 借贷业务种类细分 **/
	@Column(name = "BUS_CATEGORY_MIN", unique = false, nullable = true, length = 60)
	private String busCategoryMin;
	
	/** 开户日期 **/
	@Column(name = "OPEN_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date openDate;
	
	/** 币种 **/
	@Column(name = "CURRENCY", unique = false, nullable = true, length = 60)
	private String currency;
	
	/** 借款金额 **/
	@Column(name = "LOAN_AMOUNT", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal loanAmount;
	
	/** 信用额度 **/
	@Column(name = "CREDIT_LOAN", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal creditLoan;
	
	/** 到期日期 **/
	@Column(name = "DUE_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date dueDate;
	
	/** 担保方式 **/
	@Column(name = "GUA_MODE", unique = false, nullable = true, length = 60)
	private String guaMode;
	
	/** 其他还款保证方式 **/
	@Column(name = "OTH_GUA_WAY", unique = false, nullable = true, length = 60)
	private String othGuaWay;
	
	/** 发放形式 **/
	@Column(name = "DISTRIBUTION", unique = false, nullable = true, length = 60)
	private String distribution;
	
	/** 共同借款标识 **/
	@Column(name = "CB_MARK", unique = false, nullable = true, length = 60)
	private String cbMark;
	
	/** 关闭日期 **/
	@Column(name = "CLOSE_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date closeDate;
	
	/** 信息报告日期 **/
	@Column(name = "REPORT_INFORM_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date reportInformDate;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param reportId
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	
    /**
     * @return reportId
     */
	public String getReportId() {
		return this.reportId;
	}
	
	/**
	 * @param leAccNumber
	 */
	public void setLeAccNumber(String leAccNumber) {
		this.leAccNumber = leAccNumber;
	}
	
    /**
     * @return leAccNumber
     */
	public String getLeAccNumber() {
		return this.leAccNumber;
	}
	
	/**
	 * @param accountState
	 */
	public void setAccountState(String accountState) {
		this.accountState = accountState;
	}
	
    /**
     * @return accountState
     */
	public String getAccountState() {
		return this.accountState;
	}
	
	/**
	 * @param leAccountType
	 */
	public void setLeAccountType(String leAccountType) {
		this.leAccountType = leAccountType;
	}
	
    /**
     * @return leAccountType
     */
	public String getLeAccountType() {
		return this.leAccountType;
	}
	
	/**
	 * @param loanPeroid
	 */
	public void setLoanPeroid(String loanPeroid) {
		this.loanPeroid = loanPeroid;
	}
	
    /**
     * @return loanPeroid
     */
	public String getLoanPeroid() {
		return this.loanPeroid;
	}
	
	/**
	 * @param bmOrgType
	 */
	public void setBmOrgType(String bmOrgType) {
		this.bmOrgType = bmOrgType;
	}
	
    /**
     * @return bmOrgType
     */
	public String getBmOrgType() {
		return this.bmOrgType;
	}
	
	/**
	 * @param bmOrgNumber
	 */
	public void setBmOrgNumber(String bmOrgNumber) {
		this.bmOrgNumber = bmOrgNumber;
	}
	
    /**
     * @return bmOrgNumber
     */
	public String getBmOrgNumber() {
		return this.bmOrgNumber;
	}
	
	/**
	 * @param creditNumber
	 */
	public void setCreditNumber(String creditNumber) {
		this.creditNumber = creditNumber;
	}
	
    /**
     * @return creditNumber
     */
	public String getCreditNumber() {
		return this.creditNumber;
	}
	
	/**
	 * @param categoryMax
	 */
	public void setCategoryMax(String categoryMax) {
		this.categoryMax = categoryMax;
	}
	
    /**
     * @return categoryMax
     */
	public String getCategoryMax() {
		return this.categoryMax;
	}
	
	/**
	 * @param busCategoryMin
	 */
	public void setBusCategoryMin(String busCategoryMin) {
		this.busCategoryMin = busCategoryMin;
	}
	
    /**
     * @return busCategoryMin
     */
	public String getBusCategoryMin() {
		return this.busCategoryMin;
	}
	
	/**
	 * @param openDate
	 */
	public void setOpenDate(java.util.Date openDate) {
		this.openDate = openDate;
	}
	
    /**
     * @return openDate
     */
	public java.util.Date getOpenDate() {
		return this.openDate;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    /**
     * @return currency
     */
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param loanAmount
	 */
	public void setLoanAmount(java.math.BigDecimal loanAmount) {
		this.loanAmount = loanAmount;
	}
	
    /**
     * @return loanAmount
     */
	public java.math.BigDecimal getLoanAmount() {
		return this.loanAmount;
	}
	
	/**
	 * @param creditLoan
	 */
	public void setCreditLoan(java.math.BigDecimal creditLoan) {
		this.creditLoan = creditLoan;
	}
	
    /**
     * @return creditLoan
     */
	public java.math.BigDecimal getCreditLoan() {
		return this.creditLoan;
	}
	
	/**
	 * @param dueDate
	 */
	public void setDueDate(java.util.Date dueDate) {
		this.dueDate = dueDate;
	}
	
    /**
     * @return dueDate
     */
	public java.util.Date getDueDate() {
		return this.dueDate;
	}
	
	/**
	 * @param guaMode
	 */
	public void setGuaMode(String guaMode) {
		this.guaMode = guaMode;
	}
	
    /**
     * @return guaMode
     */
	public String getGuaMode() {
		return this.guaMode;
	}
	
	/**
	 * @param othGuaWay
	 */
	public void setOthGuaWay(String othGuaWay) {
		this.othGuaWay = othGuaWay;
	}
	
    /**
     * @return othGuaWay
     */
	public String getOthGuaWay() {
		return this.othGuaWay;
	}
	
	/**
	 * @param distribution
	 */
	public void setDistribution(String distribution) {
		this.distribution = distribution;
	}
	
    /**
     * @return distribution
     */
	public String getDistribution() {
		return this.distribution;
	}
	
	/**
	 * @param cbMark
	 */
	public void setCbMark(String cbMark) {
		this.cbMark = cbMark;
	}
	
    /**
     * @return cbMark
     */
	public String getCbMark() {
		return this.cbMark;
	}
	
	/**
	 * @param closeDate
	 */
	public void setCloseDate(java.util.Date closeDate) {
		this.closeDate = closeDate;
	}
	
    /**
     * @return closeDate
     */
	public java.util.Date getCloseDate() {
		return this.closeDate;
	}
	
	/**
	 * @param reportInformDate
	 */
	public void setReportInformDate(java.util.Date reportInformDate) {
		this.reportInformDate = reportInformDate;
	}
	
    /**
     * @return reportInformDate
     */
	public java.util.Date getReportInformDate() {
		return this.reportInformDate;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}