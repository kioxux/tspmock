package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0402Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-业务处理- </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0402Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0402Service.class);

    @Autowired
    private Cmis0402Mapper cmis0402Mapper;

    /**
     * 插入贷后检查任务表
     * @param openDay
     */
    public void cmis0402InsertPspTaskList(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList = cmis0402Mapper.insertPspTaskList(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList);
    }
}
