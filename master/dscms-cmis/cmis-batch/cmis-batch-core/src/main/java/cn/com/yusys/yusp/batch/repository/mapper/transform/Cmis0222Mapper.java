package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0222</br>
 * 任务名称：加工任务-额度处理-授信分项状态处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0222Mapper {
    /**
     * 将要注销的未到期的零售批复放在中间表,清空中间表
     */
    void deleteTmpLmt01();

    /**
     * 删除合同主表0222
     */
    void deleteTempCtrLoanCont0222();//addby 20211018

    /**
     * 插入合同主表0222
     *
     * @param openDay
     * @return
     */

    int insertTempCtrLoanCont0222(@Param("openDay") String openDay);//addby 20211018


    /**
     * 将要注销的未到期的零售批复放在中间表,插入中间表
     *
     * @param openDay
     * @return
     */
    int insertTmpLmt01(@Param("openDay") String openDay);


    /**
     * 根据中间表更新零售批复状态
     *
     * @param openDay
     * @return
     */
    int updateLmtCrdReplyInfo(@Param("openDay") String openDay);

    /**
     * 根据中间表更新额度批复分项状态
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo(@Param("openDay") String openDay);

    /**
     * 删除批复主信息0222
     */
    void deleteTempApprStrMtableInfo0222();//addBy 20211018

    /**
     * 插入批复主信息0222
     *
     * @param openDay
     * @return
     */

    int insertTempApprStrMtableInfo0222(@Param("openDay") String openDay);//addBy 20211018

    /**
     * 清空临时表-已到期的分项改成授信失效未结清 01-单一客户额度、08-承销额度  统一改成失效未结清，下条语句处理金额=0的为失效已结清-
     */
    void truncateAlsbiAsmi();

    /**
     * 插入临时表-已到期的分项改成授信失效未结清 01-单一客户额度、08-承销额度  统一改成失效未结清，下条语句处理金额=0的为失效已结清
     *
     * @param openDay
     * @return
     */
    int insertAlsbiAsmi(@Param("openDay") String openDay);

    /**
     * 已到期的分项改成授信失效未结清 01-单一客户额度、08-承销额度  统一改成失效未结清，下条语句处理金额=0的为失效已结清
     *
     * @param openDay
     * @return
     */
    int updateAlsbiAsmi(@Param("openDay") String openDay);

    /**
     * 清空临时表-已到期的改成授信失效已结清 -01-单一客户额度、08-承销额度
     */
    void truncateTla05A();

    /**
     * 插入临时表-已到期的改成授信失效已结清 -01-单一客户额度、08-承销额度
     *
     * @param openDay
     * @return
     */
    int insertTla05A(@Param("openDay") String openDay);

    /**
     * 更新批复额度分项基础信息状态为结清
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo05A(@Param("openDay") String openDay);

    /**
     * 同业额度分项、单笔投资  已到期的分项改成授信失效未结清
     *
     * @param openDay
     * @return
     */
    int updateAlsbiA(@Param("openDay") String openDay);

    /**
     * 同业额度分项：同业票据类、同业贸融类  已到期的且已用金额分项改成授信失效已结清
     *
     * @param openDay
     * @return
     */
    int updateAlsbiB(@Param("openDay") String openDay);

    /**
     * 同业额度分项：非同业票据类、非同业贸融类的同业综合授信 和 单笔投资授信  已到期的更新为tmp表中的状态（处理ComStar数据时加工过）
     *
     * @param openDay
     * @return
     */
    int updateAlsbiC(@Param("openDay") String openDay);

    /**
     * 额度项下分项均失效已结清，则更新额度状态为失效已结清
     *
     * @param openDay
     * @return
     */
    int updateClAsmiA(@Param("openDay") String openDay);

    /**
     * 更新批复主信息
     *
     * @param openDay
     * @return
     */
    int updateApprStrMtableInfo(@Param("openDay") String openDay);

    /**
     * 合作方分项已到期，则更新额度状态为失效未结清
     *
     * @param openDay
     * @return
     */
    int updateClAcsiA(@Param("openDay") String openDay);

    /**
     * 合作方分项为失效未结清，且用信余额=0，则更新额度状态为失效未结清
     *
     * @param openDay
     * @return
     */
    int updateClAcsiB(@Param("openDay") String openDay);

    /**
     * 合作方额度项下分项均已失效已结清，则更显额度状态为失效已结清
     *
     * @param openDay
     * @return
     */
    int updateClAcsiC(@Param("openDay") String openDay);

    /**
     * 若集团额度项下，关联的额度台账状态均为失效已结清，则集团批复台账状态为失效已结清
     *
     * @param openDay
     * @return
     */
    int updateClAsmiB(@Param("openDay") String openDay);

    /**
     * 产品户买入反售白名单额度以tmp表为准
     *
     * @param openDay
     * @return
     */
    int updateClLwiTdlwi(@Param("openDay") String openDay);

    /**
     * 白名单额度，若额度已到期，且用信余额为0，则将额度移入历史表
     *
     * @param openDay
     * @return
     */
    int insertClLwih(@Param("openDay") String openDay);
}
