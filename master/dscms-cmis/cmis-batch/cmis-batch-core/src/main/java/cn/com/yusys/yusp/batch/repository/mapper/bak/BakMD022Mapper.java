package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKMD022</br>
 * 任务名称：批后备份月表日表任务-备份  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakMD022Mapper {
    /**
     * 查询待删除当天的[行名行号对照表[CFG_BANK_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryMD022DeleteOpenDayDCfgBankInfoCounts(@Param("openDay") String openDay);

    /**
     * 查询待删除当月的[风险预警业务表[BAT_BIZ_RISK_SIGN]]数据
     *
     * @param openDay
     */
    int queryMD0022DeleteBatBizRiskSignCounts(@Param("openDay") String openDay);

    /**
     * 删除当月的[风险预警业务表[BAT_BIZ_RISK_SIGN]]数据
     *
     * @param openDay
     */
    int bakMD022DeleteMBatBizRiskSign(@Param("openDay") String openDay);

    /**
     * 查询待删除当月的有效客户信息月表[bat_t_fpt_r_cust_busi_sum]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakMD022DeleteMFptRCustBusiSumCounts(@Param("openDay") String openDay);

    /**
     * 删除当月的有效客户信息月表[bat_t_fpt_r_cust_busi_sum]]数据
     *
     * @param openDay
     * @return
     */
    int bakMD022DeleteMFptRCustBusiSum(@Param("openDay") String openDay);

    /**
     * 备份当月的数据
     *
     * @param openDay
     * @return
     */
    int insertM(@Param("openDay") String openDay);

    /**
     * 备份当月的风险预警客户信息
     *
     * @param openDay
     * @return
     */
    int insertMFptRCustBusiSum(@Param("openDay") String openDay);

    /**
     * 删除当天的[行名行号对照表[CFG_BANK_INFO]]数据
     *
     * @param openDay
     */
    int bakMD022DeleteDCfgBankInfo(@Param("openDay") String openDay);

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertD(@Param("openDay") String openDay);

    /**
     * 查询当天备份的[行名行号对照表[CFG_BANK_INFO]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryOriginalCounts(@Param("openDay") String openDay);

    /**
     * 查询当月备份的[风险预警业务表[tmp_m_bat_biz_risk_sign]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryBakMCounts(@Param("openDay") String openDay);

    /**
     * 查询当月的[风险预警业务表[bat_biz_risk_sign]]原表数据总条数
     *
     * @param openDay
     * @return
     */
    int queryMOriginalCounts(@Param("openDay") String openDay);


}
