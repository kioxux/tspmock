/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.rcp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatTRcpMsRepayDetailFileTemp;
import cn.com.yusys.yusp.batch.repository.mapper.load.rcp.BatTRcpMsRepayDetailFileTempMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpMsRepayDetailFileTempService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:04:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTRcpMsRepayDetailFileTempService {

    @Autowired
    private BatTRcpMsRepayDetailFileTempMapper batTRcpMsRepayDetailFileTempMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTRcpMsRepayDetailFileTemp selectByPrimaryKey(String bizDate, String contrNbr, String repayRefNbr) {
        return batTRcpMsRepayDetailFileTempMapper.selectByPrimaryKey(bizDate, contrNbr, repayRefNbr);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTRcpMsRepayDetailFileTemp> selectAll(QueryModel model) {
        List<BatTRcpMsRepayDetailFileTemp> records = (List<BatTRcpMsRepayDetailFileTemp>) batTRcpMsRepayDetailFileTempMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTRcpMsRepayDetailFileTemp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTRcpMsRepayDetailFileTemp> list = batTRcpMsRepayDetailFileTempMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTRcpMsRepayDetailFileTemp record) {
        return batTRcpMsRepayDetailFileTempMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTRcpMsRepayDetailFileTemp record) {
        return batTRcpMsRepayDetailFileTempMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTRcpMsRepayDetailFileTemp record) {
        return batTRcpMsRepayDetailFileTempMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTRcpMsRepayDetailFileTemp record) {
        return batTRcpMsRepayDetailFileTempMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String bizDate, String contrNbr, String repayRefNbr) {
        return batTRcpMsRepayDetailFileTempMapper.deleteByPrimaryKey(bizDate, contrNbr, repayRefNbr);
    }

    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTRcpMsRepayDetailFileTempMapper.truncateTTable();
    }
}
