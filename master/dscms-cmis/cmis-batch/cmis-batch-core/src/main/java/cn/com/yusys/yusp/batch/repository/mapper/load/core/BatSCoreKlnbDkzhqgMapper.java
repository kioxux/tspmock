/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.load.core;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKlnbDkzhqg;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKlnbDkzhqgMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-02 08:18:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface BatSCoreKlnbDkzhqgMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    BatSCoreKlnbDkzhqg selectByPrimaryKey(@Param("farendma") String farendma, @Param("dkjiejuh") String dkjiejuh, @Param("benqqish") Long benqqish, @Param("benqizqs") Long benqizqs);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<BatSCoreKlnbDkzhqg> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(BatSCoreKlnbDkzhqg record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(BatSCoreKlnbDkzhqg record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(BatSCoreKlnbDkzhqg record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(BatSCoreKlnbDkzhqg record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("farendma") String farendma, @Param("dkjiejuh") String dkjiejuh, @Param("benqqish") Long benqqish, @Param("benqizqs") Long benqizqs);

    /**
     * 查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>10
     *
     * @param queryMap
     * @return
     */
    int countKlnbDkzhqgByQueryMap10(Map queryMap);

    /**
     * 查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>5
     *
     * @param queryMap
     * @return
     */
    int countKlnbDkzhqgByQueryMap05(Map queryMap);

    /**
     * 无查询条件
     *
     * @param queryMap
     * @return
     */
    int countKlnbDkzhqgByQueryMap0(Map queryMap);

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    int deleteByOpenDay(@Param("openDay") String openDay);

    /**
     * 更新S表中 数据日期 DATA_DATE 为营业日期
     *
     * @param openDay
     * @return
     */
    int updateDataDateByOpenDay(@Param("openDay") String openDay);

}