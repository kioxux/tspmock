package cn.com.yusys.yusp.batch.job.bak;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bak.BakMD022Service;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepLmtEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKMD022</br>
 * 任务名称：批后备份月表日表任务-备份风险预警业务表[BAT_BIZ_RISK_SIGN] </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月23日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class BakMD022Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(BakMD022Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private BakMD022Service bakMD022Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job bakMD022Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_JOB.key, JobStepLmtEnum.BAKMD022_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job bakMD022Job = this.jobBuilderFactory.get(JobStepLmtEnum.BAKMD022_JOB.key)
                .start(bakMD022UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(bakMD022CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                //.next(bakMD022DeleteMBatBizRiskSignStep(WILL_BE_INJECTED)) //备份风险预警业务表[BAT_BIZ_RISK_SIGN]-备份前先删除当月的数据
                .next(bakMD022InsertMStep(WILL_BE_INJECTED)) // 备份当天的数据
                // .next(bakMD022DeleteDCfgBankInfoStep(WILL_BE_INJECTED)) //备份行名行号对照表[CFG_BANK_INFO]-备份前先删除当天的数据
                .next(bakMD022InsertDStep(WILL_BE_INJECTED)) // 备份当天的数据
                .next(bakMD022UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return bakMD022Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakMD022UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_UPDATE_TASK010_STEP.key, JobStepLmtEnum.BAKMD022_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakMD022UpdateTask010Step = this.stepBuilderFactory.get(JobStepLmtEnum.BAKMD022_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BAKMD022_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_UPDATE_TASK010_STEP.key, JobStepLmtEnum.BAKMD022_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakMD022UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakMD022CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_CHECK_REL_STEP.key, JobStepLmtEnum.BAKMD022_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakMD022CheckRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKMD022_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BAKMD022_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_CHECK_REL_STEP.key, JobStepLmtEnum.BAKMD022_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_CHECK_REL_STEP.key, JobStepLmtEnum.BAKMD022_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return bakMD022CheckRelStep;
    }

    /**
     * 备份风险预警业务表[BAT_BIZ_RISK_SIGN]-备份前先删除当月的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakMD022DeleteMBatBizRiskSignStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_DELETE_M_BAT_BIZ_RISK_SIGN_STEP.key, JobStepLmtEnum.BAKMD022_DELETE_M_BAT_BIZ_RISK_SIGN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakMD022DeleteMBatBizRiskSignStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKMD022_DELETE_M_BAT_BIZ_RISK_SIGN_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakMD022Service.bakMD022DeleteMBatBizRiskSign(openDay);// 备份风险预警业务表[BAT_BIZ_RISK_SIGN]-备份前先删除当月的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_DELETE_M_BAT_BIZ_RISK_SIGN_STEP.key, JobStepLmtEnum.BAKMD022_DELETE_M_BAT_BIZ_RISK_SIGN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakMD022DeleteMBatBizRiskSignStep;
    }

    /**
     * 备份当月的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakMD022InsertMStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_INSERT_M_STEP.key, JobStepLmtEnum.BAKMD022_INSERT_M_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakMD022InsertCurrentStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKMD022_INSERT_M_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    String nextOpenDay = DateUtils.addDay(openDay, "yyyy-MM-dd", 1); // 切日后营业日期
                    logger.info("切日后营业日期为:[{}]", nextOpenDay);
                    Date nextOpenDayDate = DateUtils.parseDateByDef(nextOpenDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    int day = DateUtils.getMonthDay(nextOpenDayDate);// 获取当月中的天
                    if (day == 1) {
                        logger.info("获取当月中的天为:[{}],需要执行备份当月的数据操作", day);
                        bakMD022Service.bakMD022DeleteMBatBizRiskSign(openDay);// 备份风险预警业务表[BAT_BIZ_RISK_SIGN]-备份前先删除当月的数据
                        bakMD022Service.bakMD022InsertM(openDay);//备份当天的数据
                        bakMD022Service.checkBakMEqualsOriginal(openDay);//校验备份表和原表数据是否一致
                    } else {
                        logger.info("获取当月中的天为:[{}],不需要执行备份当月的数据操作", day);
                    }
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_INSERT_M_STEP.key, JobStepLmtEnum.BAKMD022_INSERT_M_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakMD022InsertCurrentStep;
    }

    /**
     * 备份行名行号对照表[CFG_BANK_INFO]-备份前先删除当天的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakMD022DeleteDCfgBankInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_DELETE_D_CFG_BANK_INFO_STEP.key, JobStepLmtEnum.BAKMD022_DELETE_D_CFG_BANK_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakMD022DeleteDCtrLoanContStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKMD022_DELETE_D_CFG_BANK_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakMD022Service.bakMD022DeleteDCfgBankInfo(openDay);// 备份贷款合同表[CTR_LOAN_CONT]-备份前先删除当天的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_DELETE_D_CFG_BANK_INFO_STEP.key, JobStepLmtEnum.BAKMD022_DELETE_D_CFG_BANK_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakMD022DeleteDCtrLoanContStep;
    }

    /**
     * 备份当天的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakMD022InsertDStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_INSERT_D_STEP.key, JobStepLmtEnum.BAKMD022_INSERT_D_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakMD022InsertCurrentStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKMD022_INSERT_D_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakMD022Service.bakMD022DeleteDCfgBankInfo(openDay);// 备份贷款合同表[CTR_LOAN_CONT]-备份前先删除当天的数据
                    bakMD022Service.bakMD022InsertD(openDay);//备份当天的数据
                    bakMD022Service.checkBakDEqualsOriginal(openDay);//校验备份表和原表数据是否一致
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_INSERT_D_STEP.key, JobStepLmtEnum.BAKMD022_INSERT_D_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakMD022InsertCurrentStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakMD022UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_UPDATE_TASK100_STEP.key, JobStepLmtEnum.BAKMD022_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakMD022UpdateTask100Step = this.stepBuilderFactory.get(JobStepLmtEnum.BAKMD022_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BAKMD022_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKMD022_UPDATE_TASK100_STEP.key, JobStepLmtEnum.BAKMD022_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakMD022UpdateTask100Step;
    }
}
