package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0206Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0206</br>
 * 任务名称：加工任务-额度处理-占用授信总金额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0206Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0206Service.class);
    @Autowired
    private Cmis0206Mapper cmis0206Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新额度占用关系05，对应SQL为CMIS0206-占用授信总金额.sql
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0206UpdateLmtContRel(String openDay) {
        logger.info("truncateLmtContRel21更新额度占用关系 清理额度占用临时表开始,请求参数为:[{}]", openDay);
        // cmis0206Mapper.truncateLmtContRel21();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateLmtContRel21更新额度占用关系 清理额度占用临时表结束");

        logger.info("表内业务  BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理规则 ：用信申请金额/用信合同金额(不管是否还款，不恢复额度)处理开始");

        logger.info("合同申请信息表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理开始,请求参数为:[{}]", openDay);
        int insertLmtContRel21Htsq = cmis0206Mapper.insertLmtContRel21Htsq(openDay);
        logger.info("合同申请信息表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理结束,返回参数为:[{}]", insertLmtContRel21Htsq);


        logger.info("最高额授信协议申请 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理开始,请求参数为:[{}]", openDay);
        int insertLmtContRel21Zgesxxysq = cmis0206Mapper.insertLmtContRel21Zgesxxysq(openDay);
        logger.info("最高额授信协议申请 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理结束,返回参数为:[{}]", insertLmtContRel21Zgesxxysq);

        logger.info("委托贷款申请信息表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理开始,请求参数为:[{}]", openDay);
        int insertLmtContRel21Wtdksqxx = cmis0206Mapper.insertLmtContRel21Wtdksqxx(openDay);
        logger.info("委托贷款申请信息表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理结束,返回参数为:[{}]", insertLmtContRel21Wtdksqxx);

        logger.info("合同主表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理 开始,请求参数为:[{}]", openDay);
        int insertLmtContRel21Htzb = cmis0206Mapper.insertLmtContRel21Htzb(openDay);
        logger.info("合同主表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理 结束,返回参数为:[{}]", insertLmtContRel21Htzb);

        logger.info("最高额授信协议 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理 开始,请求参数为:[{}]", openDay);
        int insertLmtContRel21Zgesxxy = cmis0206Mapper.insertLmtContRel21Zgesxxy(openDay);
        logger.info("最高额授信协议 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理 结束,返回参数为:[{}]", insertLmtContRel21Zgesxxy);

        logger.info("委托合同主表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理 开始,请求参数为:[{}]", openDay);
        int insertLmtContRel21Wthtzb = cmis0206Mapper.insertLmtContRel21Wthtzb(openDay);
        logger.info("委托合同主表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理 结束,返回参数为:[{}]", insertLmtContRel21Wthtzb);

        logger.info("updateLmtContRel21 更新额度占用关系 占用总金额（折人民币） 赋值为关系表总占用额 开始,请求参数为:[{}]", openDay);
        int updateLmtContRel21 = cmis0206Mapper.updateLmtContRel21(openDay);
        logger.info("updateLmtContRel21 更新额度占用关系 占用总金额（折人民币） 赋值为关系表总占用额 结束,返回参数为:[{}]", updateLmtContRel21);


        logger.info("更新额度占用关系 清理额度占用临时表开始,请求参数为:[{}]", openDay);
        // cmis0206Mapper.truncateLmtContRel22();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("更新额度占用关系 清理额度占用临时表结束");

        logger.info("BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）：表外 本合同项下最高可用信金额/(1-保证金比例)（不管是否项下台账是否存在结清，不恢复额度）处理开始,请求参数为:[{}]", openDay);
        int insertLmtContRel22 = cmis0206Mapper.insertLmtContRel22(openDay);
        logger.info("BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）：表外 本合同项下最高可用信金额/(1-保证金比例)（不管是否项下台账是否存在结清，不恢复额度）处理结束,返回参数为:[{}]", insertLmtContRel22);

        logger.info("插入额度占用关系处理开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt = cmis0206Mapper.insertTmpLmtAmt(openDay);
        logger.info("插入额度占用关系处理开始结束,返回参数为:[{}]", insertTmpLmtAmt);

        logger.info("根据授信占用临时表表数据 更新 授信占用关系表处理开始,请求参数为:[{}]", openDay);
        int updateLmtContRel22 = cmis0206Mapper.updateLmtContRel22(openDay);
        logger.info("根据授信占用临时表表数据 更新 授信占用关系表 处理结束,返回参数为:[{}]", updateLmtContRel22);

        logger.info("更新额度占用关系 占用总金额（折人民币） 赋值为关系表总占用额处理结束");

        logger.info("更新额度占用关系 占用总金额（折人民币） 赋值为关系表总占用额处理开始,请求参数为:[{}]", openDay);
        int updateLmtContRel22A = cmis0206Mapper.updateLmtContRel22A(openDay);
        logger.info("更新额度占用关系 占用总金额（折人民币） 赋值为关系表总占用额处理结束,返回参数为:[{}]", updateLmtContRel22A);
    }
}
