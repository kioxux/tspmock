package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0127</br>
 * 任务名称：加工任务-业务处理-非零内评数据更新 </br>
 * V1.1:根据问题编号:20211114-00035中台账相关的逻辑调整
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0127Mapper {

    /**
     * 非零内评数据更新
     *
     * @param openDay
     * @return
     */
    int updateMajorGradeInfo(@Param("openDay") String openDay);

    /**
     * 清空 临时表-贷款合同和非零内评合同关系表
     *
     * @param openDay
     * @return
     */
    int truncateTmpFlsCtrLoanCont(@Param("openDay") String openDay);

    /**
     * 插入 临时表-贷款合同和非零内评合同关系表
     *
     * @param openDay
     * @return
     */
    int insertTmpFlsCtrLoanCont(@Param("openDay") String openDay);

    /**
     * 更新贷款合同
     *
     * @param openDay
     * @return
     */
    int updateCtrLoanCont(@Param("openDay") String openDay);

    /**
     * 清空 临时表-银承合同和非零内评合同关系表
     *
     * @param openDay
     * @return
     */
    int truncateTmpFlsCtrAccpCont(@Param("openDay") String openDay);

    /**
     * 插入 临时表-银承合同和非零内评合同关系表
     *
     * @param openDay
     * @return
     */
    int insertTmpFlsCtrAccpCont(@Param("openDay") String openDay);

    /**
     * 更新银承合同
     *
     * @param openDay
     * @return
     */
    int updateCtrAccpCont(@Param("openDay") String openDay);

    /**
     * 清空 临时表-贴现合同和非零内评合同关系表
     *
     * @param openDay
     * @return
     */
    int truncateTmpFlsCtrDiscCont(@Param("openDay") String openDay);

    /**
     * 插入 临时表-贴现合同和非零内评合同关系表
     *
     * @param openDay
     * @return
     */
    int insertTmpFlsCtrDiscCont(@Param("openDay") String openDay);

    /**
     * 更新贴现合同
     *
     * @param openDay
     * @return
     */
    int updateCtrDiscCont(@Param("openDay") String openDay);

    /**
     * 清空 临时表-保函合同和非零内评合同关系表
     *
     * @param openDay
     * @return
     */
    int truncateTmpFlsCtrCvrgCont(@Param("openDay") String openDay);

    /**
     * 插入 临时表-保函合同和非零内评合同关系表
     *
     * @param openDay
     * @return
     */
    int insertTmpFlsCtrCvrgCont(@Param("openDay") String openDay);

    /**
     * 更新保函合同
     *
     * @param openDay
     * @return
     */
    int updateCtrCvrgCont(@Param("openDay") String openDay);

    /**
     * 清空 临时表-信用证合同和非零内评合同关系表
     *
     * @param openDay
     * @return
     */
    int truncateTmpFlsCtrTfLocCont(@Param("openDay") String openDay);

    /**
     * 插入 临时表-信用证合同和非零内评合同关系表
     *
     * @param openDay
     * @return
     */
    int insertTmpFlsCtrTfLocCont(@Param("openDay") String openDay);

    /**
     * 更新信用证合同
     *
     * @param openDay
     * @return
     */
    int updateCtrTfLocCont(@Param("openDay") String openDay);


    /**
     * 更新贷款台账
     *
     * @param openDay
     * @return
     */
    int updateAccLoan(@Param("openDay") String openDay);


    /**
     * 更新银承台账明细
     *
     * @param openDay
     * @return
     */
    int updateAccAccpDrftSub(@Param("openDay") String openDay);


    /**
     * 更新贴现台账
     *
     * @param openDay
     * @return
     */
    int updateAccDisc(@Param("openDay") String openDay);


    /**
     * 更新保函台账
     *
     * @param openDay
     * @return
     */
    int updateAccCvrs(@Param("openDay") String openDay);


    /**
     * 更新信用证台账
     *
     * @param openDay
     * @return
     */
    int updateAccTfLoc(@Param("openDay") String openDay);
}
