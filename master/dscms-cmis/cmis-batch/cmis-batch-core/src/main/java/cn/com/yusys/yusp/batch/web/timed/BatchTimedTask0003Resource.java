package cn.com.yusys.yusp.batch.web.timed;

import cn.com.yusys.yusp.batch.service.timed.BatchTimedTask0003Service;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 定时任务处理类:运行零售智能风控相关批量任务
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "BatchTimedTask0003:运行零售智能风控相关批量任务")
@RestController
@RequestMapping("/api/batch4use")
public class BatchTimedTask0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BatchTimedTask0003Resource.class);
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private BatchTimedTask0003Service batchTimedTask0003Service;

    @ApiOperation("运行零售智能风控相关批量任务")
    @GetMapping("/timedtask0003")
    protected @ResponseBody
    ResultDto<Void> timedtask0003() throws Exception {
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0003.key, BatEnums.BATCH_TIMED_TASK0003.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        int result = 0;// 返回结果，0成功，1失败
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0003.key, BatEnums.BATCH_TIMED_TASK0003.value, "");
        result = batchTimedTask0003Service.timedtask0003();
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0003.key, BatEnums.BATCH_TIMED_TASK0003.value, JSON.toJSONString(result));
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0003.key, BatEnums.BATCH_TIMED_TASK0003.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return new ResultDto<>();
    }

    @ApiOperation("运行零售智能风控相关批量任务")
    @PostMapping("/timedtask0003Post")
    protected @ResponseBody
    ResultDto<Void> timedtask0003Post() throws Exception {
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0003.key, BatEnums.BATCH_TIMED_TASK0003.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        int result = 0;// 返回结果，0成功，1失败
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0003.key, BatEnums.BATCH_TIMED_TASK0003.value, "");
        result = batchTimedTask0003Service.timedtask0003();
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0003.key, BatEnums.BATCH_TIMED_TASK0003.value, JSON.toJSONString(result));
        logger.info(TradeLogConstants.BATCH_TIMED_TASK_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0003.key, BatEnums.BATCH_TIMED_TASK0003.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return new ResultDto<>();
    }
}
