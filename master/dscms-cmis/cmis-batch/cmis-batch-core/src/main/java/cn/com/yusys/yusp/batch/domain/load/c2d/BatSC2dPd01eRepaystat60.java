/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.c2d;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dPd01eRepaystat60
 * @类描述: bat_s_c2d_pd01e_repaystat60数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:02:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_c2d_pd01e_repaystat60")
public class BatSC2dPd01eRepaystat60 extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** id **/
	@Id
	@Column(name = "ID")
	private String id;
	
	/** 信用报告id **/
	@Id
	@Column(name = "REPORT_ID")
	private String reportId;
	
	/** 起始年月 **/
	@Column(name = "START_YEARS", unique = false, nullable = true, length = 8)
	private String startYears;
	
	/** 截止年月 **/
	@Column(name = "END_YEARS", unique = false, nullable = true, length = 8)
	private String endYears;
	
	/** 月数 **/
	@Column(name = "MONTH_NUM", unique = false, nullable = true, length = 8)
	private String monthNum;
	
	/** 月份 **/
	@Column(name = "MONTH", unique = false, nullable = true, length = 19)
	private java.util.Date month;
	
	/** 还款状态 **/
	@Column(name = "REPAY_STATUS", unique = false, nullable = true, length = 60)
	private String repayStatus;
	
	/** 逾期（透支）总额 **/
	@Column(name = "OVERDUE_MONEY", unique = false, nullable = true, length = 22)
	private String overdueMoney;
	
	/** 关联ID **/
	@Column(name = "UNION_ID", unique = false, nullable = true, length = 32)
	private String unionId;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param reportId
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	
    /**
     * @return reportId
     */
	public String getReportId() {
		return this.reportId;
	}
	
	/**
	 * @param startYears
	 */
	public void setStartYears(String startYears) {
		this.startYears = startYears;
	}
	
    /**
     * @return startYears
     */
	public String getStartYears() {
		return this.startYears;
	}
	
	/**
	 * @param endYears
	 */
	public void setEndYears(String endYears) {
		this.endYears = endYears;
	}
	
    /**
     * @return endYears
     */
	public String getEndYears() {
		return this.endYears;
	}
	
	/**
	 * @param monthNum
	 */
	public void setMonthNum(String monthNum) {
		this.monthNum = monthNum;
	}
	
    /**
     * @return monthNum
     */
	public String getMonthNum() {
		return this.monthNum;
	}
	
	/**
	 * @param month
	 */
	public void setMonth(java.util.Date month) {
		this.month = month;
	}
	
    /**
     * @return month
     */
	public java.util.Date getMonth() {
		return this.month;
	}
	
	/**
	 * @param repayStatus
	 */
	public void setRepayStatus(String repayStatus) {
		this.repayStatus = repayStatus;
	}
	
    /**
     * @return repayStatus
     */
	public String getRepayStatus() {
		return this.repayStatus;
	}
	
	/**
	 * @param overdueMoney
	 */
	public void setOverdueMoney(String overdueMoney) {
		this.overdueMoney = overdueMoney;
	}
	
    /**
     * @return overdueMoney
     */
	public String getOverdueMoney() {
		return this.overdueMoney;
	}
	
	/**
	 * @param unionId
	 */
	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}
	
    /**
     * @return unionId
     */
	public String getUnionId() {
		return this.unionId;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}