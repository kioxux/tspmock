package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0215</br>
 * 任务名称：加工任务-额度处理-授信分项状态处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0215Mapper {

    /**
     * updateTdldLmtAmtAdd01 -更新批复额度分项基础信息加工表中字段 01
     *
     * @param openDay
     * @return
     */
    int updateTdldLmtAmtAdd01(@Param("openDay") String openDay);


    /**
     * truncateTmpLmtAmt01  -删除批复额度分项基础信息加工表 01
     */
    void truncateTmpLmtAmt01();


    /**
     * insertTmpLmtAmt - 插入批复额度分项基础信息加工表
     *
     * @param openDay
     * @return
     */

    int insertTmpLmtAmt(@Param("openDay") String openDay);

    /**
     * insertTmpLmtAmt - 更新批复额度分项基础信息加工表中字段
     *
     * @param openDay
     * @return
     */
    int updateTdldLmtAmtAdd02(@Param("openDay") String openDay);

    /**
     * truncateTmpAccAdddetails 删除加工台账低风险总额累加明细
     */
    void truncateTmpAccAdddetails();

    /**
     * insertTmpAccAdddetails  插入台账低风险总额累加明细
     *
     * @param openDay
     * @return
     */
    int insertTmpAccAdddetails(@Param("openDay") String openDay);

    /**
     * truncateTmpLmtAmt02 - 删除加工占用授信表02
     */
    void truncateTmpLmtAmt02();

    /**
     * insertTmpLmtAmt01 插入情况一类型数据01
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt01(@Param("openDay") String openDay);

    /**
     * insertTmpLmtAmt02 插入情况一类型数据02
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt02(@Param("openDay") String openDay);

    /**
     * insertTmpLmtAmt03 插入情况一类型数据03
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt03(@Param("openDay") String openDay);

    /**
     * insertTmpLmtAmt04 插入情况一类型数据04
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt04(@Param("openDay") String openDay);


    /**
     * updateTdldLmtAmtAdd03 -更新批复额度分项基础信息加工表中字段 03
     *
     * @param openDay
     * @return
     */
    int updateTdldLmtAmtAdd03(@Param("openDay") String openDay);

    /**
     *
     * 删除新信贷-临时表-加工占用授信表
     */
    void deleteTmpLmtAmt();

    /**
     *
     * 插入新信贷-临时表-加工占用授信表
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt05(@Param("openDay") String openDay);

    /**
     *
     * 更新批复额度分项基础信息加工表
     * @param openDay
     * @return
     */
    int updateTmpDealLmtDetails(@Param("openDay") String openDay);

}
