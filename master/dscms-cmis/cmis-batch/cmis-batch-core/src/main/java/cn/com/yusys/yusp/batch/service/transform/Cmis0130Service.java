package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0130Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0130</br>
 * 任务名称：加工任务-业务处理-同行业排行处理 </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月10日 下午00:58:54
 */
@Service
@Transactional
public class Cmis0130Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0130Service.class);

    @Autowired
    private Cmis0130Mapper cmis0130Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * 同行业营业收入排行表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0130IndustryBsinsIncomeTop(String openDay) {
        logger.info("重命名创建和删除 临时表-同行业营业收入排行客户号开始,请求参数为:[{}] ", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_lmt_reply_acc_cmis0130");
        logger.info("重命名创建和删除 临时表-同行业营业收入排行客户号结束");

        logger.info("插入临时表-同行业营业收入排行客户号开始,请求参数为:[{}] ", openDay);
        int insertTmpLmtReplyAcc = cmis0130Mapper.insertTmpLmtReplyAcc(openDay);
        logger.info("插入临时表-同行业营业收入排行客户号 结束,返回参数为:[{}] ", insertTmpLmtReplyAcc);
        tableUtilsService.analyzeTable("cmis_biz","tmp_lmt_reply_acc_cmis0130");//分析表

        logger.info("重命名创建和删除 同行业营业收入排行表 开始,请求参数为:[{}] ", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","industry_bsins_income_top");
        logger.info("重命名创建和删除 同行业营业收入排行表 结束");

        logger.info("插入单一授信同行业营业收入排行表 开始,请求参数为:[{}] ", openDay);
        int insertInduBsinsIncoTop = cmis0130Mapper.insertInduBsinsIncoTop(openDay);
        logger.info("插入单一授信同行业营业收入排行表 结束,返回参数为:[{}] ", insertInduBsinsIncoTop);
        tableUtilsService.analyzeTable("cmis_biz","industry_bsins_income_top");

        logger.info("插入集团授信同行业营业收入排行表 开始,请求参数为:[{}] ", openDay);
        int insertInduBsinsIncoTopGrp = cmis0130Mapper.insertInduBsinsIncoTopGrp(openDay);
        logger.info("插入集团授信同行业营业收入排行表 结束,返回参数为:[{}] ", insertInduBsinsIncoTopGrp);

        logger.info("重命名创建和删除 临时表同行业营业收入排行表 开始,请求参数为:[{}] ", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_industry_bsins_income_top");
        logger.info("重命名创建和删除 临时表同行业营业收入排行表 结束");

        logger.info("插入临时表同行业营业收入排行表 开始,请求参数为:[{}] ", openDay);
        int insertTmpInduBsinsIncoTop = cmis0130Mapper.insertTmpInduBsinsIncoTop(openDay);
        logger.info("插入临时表同行业营业收入排行表 结束,返回参数为:[{}] ", insertTmpInduBsinsIncoTop);
        tableUtilsService.analyzeTable("cmis_biz","tmp_industry_bsins_income_top");

        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的总资产年期末数 开始,请求参数为:[{}] ", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y1");
        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的总资产年期末数 结束");

        logger.info("插入新信贷-临时表-同行业营业收入排行的总资产年期末数 开始,请求参数为:[{}] ", openDay);
        int insertTmpIbitStatEndAmtY1 = cmis0130Mapper.insertTmpIbitStatEndAmtY1(openDay);
        logger.info("插入新信贷-临时表-同行业营业收入排行的总资产年期末数 结束,返回参数为:[{}] ", insertTmpIbitStatEndAmtY1);
        tableUtilsService.analyzeTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y1");

        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的净资产年期末数 开始,请求参数为:[{}] ", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y2");
        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的净资产年期末数 结束");

        logger.info("插入新信贷-临时表-同行业营业收入排行的净资产年期末数 开始,请求参数为:[{}] ", openDay);
        int insertTmpIbitStatEndAmtY2 = cmis0130Mapper.insertTmpIbitStatEndAmtY2(openDay);
        logger.info("插入新信贷-临时表-同行业营业收入排行的净资产年期末数 结束,返回参数为:[{}] ", insertTmpIbitStatEndAmtY2);
        tableUtilsService.analyzeTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y2");

        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的营业收入年期末数 开始,请求参数为:[{}] ", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y3");
        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的营业收入年期末数 结束");

        logger.info("插入新信贷-临时表-同行业营业收入排行的营业收入年期末数 开始,请求参数为:[{}] ", openDay);
        int insertTmpIbitStatEndAmtY3 = cmis0130Mapper.insertTmpIbitStatEndAmtY3(openDay);
        logger.info("插入新信贷-临时表-同行业营业收入排行的营业收入年期末数 结束,返回参数为:[{}] ", insertTmpIbitStatEndAmtY3);
        tableUtilsService.analyzeTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y3");

        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的净利润年期末数 开始,请求参数为:[{}] ", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y4");
        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的净利润年期末数 结束");

        logger.info("插入新信贷-临时表-同行业营业收入排行的净利润年期末数 开始,请求参数为:[{}] ", openDay);
        int insertTmpIbitStatEndAmtY4 = cmis0130Mapper.insertTmpIbitStatEndAmtY4(openDay);
        logger.info("插入新信贷-临时表-同行业营业收入排行的净利润年期末数 结束,返回参数为:[{}] ", insertTmpIbitStatEndAmtY4);
        tableUtilsService.analyzeTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y4");

        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的资产负债率年期末数 开始,请求参数为:[{}] ", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y5");
        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的资产负债率年期末数 结束");

        logger.info("插入新信贷-临时表-同行业营业收入排行的资产负债率年期末数 开始,请求参数为:[{}] ", openDay);
        int insertTmpIbitStatEndAmtY5 = cmis0130Mapper.insertTmpIbitStatEndAmtY5(openDay);
        logger.info("插入新信贷-临时表-同行业营业收入排行的资产负债率年期末数 结束,返回参数为:[{}] ", insertTmpIbitStatEndAmtY5);
        tableUtilsService.analyzeTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y5");

        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的营业融资总额年期末数 开始,请求参数为:[{}] ", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y6");
        logger.info("重命名创建和删除新信贷-临时表-同行业营业收入排行的营业融资总额年期末数 结束");

        logger.info("插入新信贷-临时表-同行业营业收入排行的营业融资总额年期末数 开始,请求参数为:[{}] ", openDay);
        int insertTmpIbitStatEndAmtY6 = cmis0130Mapper.insertTmpIbitStatEndAmtY6(openDay);
        logger.info("插入新信贷-临时表-同行业营业收入排行的营业融资总额年期末数 结束,返回参数为:[{}] ", insertTmpIbitStatEndAmtY6);
        tableUtilsService.analyzeTable("cmis_batch","tmp_ibit_STAT_END_AMT_Y6");

        logger.info("更新同行业营业收入排行表总资产字段 开始,请求参数为:[{}] ", openDay);
        int updateTotalRessetAmt = cmis0130Mapper.updateTotalRessetAmt(openDay);
        logger.info("更新同行业营业收入排行表总资产字段 结束,返回参数为:[{}] ", updateTotalRessetAmt);

        logger.info("更新同行业营业收入排行表净资产字段 开始,请求参数为:[{}] ", openDay);
        int updateTotalPureAmt = cmis0130Mapper.updateTotalPureAmt(openDay);
        logger.info("更新同行业营业收入排行表净资产字段 结束,返回参数为:[{}] ", updateTotalPureAmt);

        logger.info("更新同行业营业收入排行表营业收入字段 开始,请求参数为:[{}] ", openDay);
        int updateBsinsIncome = cmis0130Mapper.updateBsinsIncome(openDay);
        logger.info("更新同行业营业收入排行表营业收入字段 结束,返回参数为:[{}] ", updateBsinsIncome);

        logger.info("更新同行业营业收入排行表净利润字段 开始,请求参数为:[{}] ", openDay);
        int updateProfit = cmis0130Mapper.updateProfit(openDay);
        logger.info("更新同行业营业收入排行表净利润字段 结束,返回参数为:[{}] ", updateProfit);

        logger.info("更新同行业营业收入排行表资产负债率字段 开始,请求参数为:[{}] ", openDay);
        int updateAssetDebtRate = cmis0130Mapper.updateAssetDebtRate(openDay);
        logger.info("更新同行业营业收入排行表资产负债率字段 结束,返回参数为:[{}] ", updateAssetDebtRate);

        logger.info("更新同行业营业收入排行表营业融资总额字段 开始,请求参数为:[{}] ", openDay);
        int updateTotalOperFinance = cmis0130Mapper.updateTotalOperFinance(openDay);
        logger.info("更新同行业营业收入排行表营业融资总额字段 结束,返回参数为:[{}] ", updateTotalOperFinance);

        logger.info("更新同行业营业收入排行表销贷比字段 开始,请求参数为:[{}] ", openDay);
        int updateLoanSoldRate = cmis0130Mapper.updateLoanSoldRate(openDay);
        logger.info("更新同行业营业收入排行表销贷比字段 结束,返回参数为:[{}] ", updateLoanSoldRate);
    }
}
