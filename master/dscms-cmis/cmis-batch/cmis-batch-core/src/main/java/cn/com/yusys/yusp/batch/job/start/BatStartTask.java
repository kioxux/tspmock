package cn.com.yusys.yusp.batch.job.start;


import cn.com.yusys.yusp.batch.domain.bat.*;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.*;
import cn.com.yusys.yusp.batch.util.DateUtil;
import cn.com.yusys.yusp.batch.util.FileUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 批量任务处理类：</br>
 * 任务编号：BATSTART</br>
 * 任务名称：调度运行开始-更新批量相关表 </br>
 *
 * @author zhoufeifei
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class BatStartTask extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(BatStartTask.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");

    @Autowired
    private BatControlRunService batControlRunService;
    @Autowired
    private BatControlHisService batControlHisService;
    @Autowired
    private BatTaskCfgService batTaskCfgService;
    @Autowired
    private BatTransferCfgService batTransferCfgService;
    @Autowired
    private BatTaskRunService batTaskRunService;
    @Autowired
    private BatTaskRelService batTaskRelService;
    @Autowired
    private BatTaskMutexService batTaskMutexService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    @Bean
    public Job batStartJob() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.BATSTART_JOB.key, JobStepEnum.BATSTART_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job batStartJob = this.jobBuilderFactory.get(JobStepEnum.BATSTART_JOB.key)
                .start(batStartUpdateRunStep()) // 更新调度运行管理表
                .listener(new BatchJobListener())
                .build();
        return batStartJob;
    }


    /**
     * 跑批开始前先切日，
     * 更新调度运行管理(BAT_CONTROL_RUN)
     */
    @Bean
    @JobScope
    public Step batStartUpdateRunStep() {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step batStartUpdateRunStep = this.stepBuilderFactory.get(JobStepEnum.BATSTART_UPDATE_BAT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    /****************** 1.日结前判断开始 *****************/
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "1.日结前判断开始");
                    // 查询批量运行表
                    QueryModel queryModel = new QueryModel();
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(queryModel));
                    List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunList));
                    if (CollectionUtils.isEmpty(batControlRunList)) {
                        // 调度运行管理(BAT_CONTROL_RUN)未配置
                        logger.error(TradeLogConstants.BATCH_STEP_EXCEPTION_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, EchEnum.ECH080008.value);
                        throw BizException.error(null, EchEnum.ECH080008.key, EchEnum.ECH080008.value);
                    }

                    String openDay = batControlRunList.get(0).getOpenDay();//营业日期
                    String lastOpenDay = batControlRunList.get(0).getLastOpenDay();//上一营业日期
                    String controlStatus = batControlRunList.get(0).getControlStatus();//调度状态 STD_CONTROL_STATUS
                    if (!Objects.equals(controlStatus, BatEnums.CONTROL_STATUS_100.key)) {
                        logger.error(TradeLogConstants.BATCH_STEP_EXCEPTION_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "营业日期为[" + lastOpenDay + "]的调度任务状态为[" + controlStatus + "],任务还未成功");
                        throw BizException.error(null, EchEnum.ECH080009.key, "营业日期为[" + lastOpenDay + "]的调度任务状态为[" + controlStatus + "],任务还未成功");
                    }
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "1.日结前判断结束");
                    /****************** 1.日结前判断结束 *****************/

                    /****************** 2.翻日开始 ***********************/
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "2.翻日开始");
                    String nextOpenDay = DateUtils.addDay(openDay, "yyyy-MM-dd", 1); // 切日后营业日期
                    String nextOpenDayMsg = "切日后营业日期为[" + nextOpenDay + "]";
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, nextOpenDayMsg);
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(openDay));
                    int batControlRunDelete = batControlRunService.deleteByPrimaryKey(openDay);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunDelete));
                    BatControlRun batControlRunNew = new BatControlRun();
                    batControlRunNew.setOpenDay(nextOpenDay);
                    batControlRunNew.setLastOpenDay(openDay);
                    batControlRunNew.setBeginTime(DateUtils.getCurrDateTimeStr());
                    batControlRunNew.setSmsFlag(BatEnums.STD_NO.key);
                    batControlRunNew.setControlStatus(BatEnums.CONTROL_STATUS_010.key);// 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunNew));
                    int batControlRunInsert = batControlRunService.insertSelective(batControlRunNew);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunInsert));
                    if (batControlRunInsert >= 1) {
                        stringRedisTemplate.opsForValue().set("openDay", nextOpenDay);
                    }
                    // 删除营业日期（即T日）[调度运行历史(BAT_CONTROL_HIS)]表中的数据
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_HIS_MODUEL.key, BatEnums.BAT_CONTROL_HIS_MODUEL.value, JSON.toJSONString(openDay));
                    int batControlHisDelete = batControlHisService.deleteByPrimaryKey(openDay);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_HIS_MODUEL.key, BatEnums.BAT_CONTROL_HIS_MODUEL.value, JSON.toJSONString(batControlHisDelete));
                    // 插入数据至[调度运行历史(BAT_CONTROL_HIS)]表中，调度状态为"010-批量运行中"
                    BatControlHis batControlHis = new BatControlHis();
                    BeanUtils.copyProperties(batControlRunNew, batControlHis);
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_HIS_MODUEL.key, BatEnums.BAT_CONTROL_HIS_MODUEL.value, JSON.toJSONString(batControlHis));
                    int batControlHisInsert = batControlHisService.insertSelective(batControlHis);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_HIS_MODUEL.key, BatEnums.BAT_CONTROL_HIS_MODUEL.value, JSON.toJSONString(batControlHisInsert));
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "2.翻日结束");
                    /****************** 2.翻日结束 ***********************/

                    /****************** 3.初始化任务开始 *****************/
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.初始化任务开始");
                    QueryModel batTaskCfgQm = new QueryModel();
                    batTaskCfgQm.addCondition("useFlag", BatEnums.STD_YES.key);
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_CFG_MODUEL.key, BatEnums.BAT_TASK_CFG_MODUEL.value, JSON.toJSONString(batTaskCfgQm));
                    List<BatTaskCfg> batTaskCfgList = batTaskCfgService.selectAll(batTaskCfgQm);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_CFG_MODUEL.key, BatEnums.BAT_TASK_CFG_MODUEL.value, JSON.toJSONString(batTaskCfgList));
                    if (CollectionUtils.isEmpty(batTaskCfgList)) {
                        // 任务配置信息表(BAT_TASK_CFG)未配置,请及时检查!
                        logger.error(TradeLogConstants.BATCH_STEP_EXCEPTION_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, EchEnum.ECH080010.value);
                        throw BizException.error(null, EchEnum.ECH080010.key, EchEnum.ECH080010.value);
                    }
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.1.遍历批量任务管理任务到任务运行管理开始");
                    List<BatTaskRun> batTaskRunList = batTaskCfgList.stream().map(batTaskCfg -> {
                        logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "任务编号为:[" + batTaskCfg.getTaskNo() + "],任务名称为:[" + batTaskCfg.getTaskName() + "]");
                        logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.1.1.遍历批量任务管理任务到任务运行管理中基本数据开始");
                        BatTaskRun batTaskRun = new BatTaskRun();
                        BeanUtils.copyProperties(batTaskCfg, batTaskRun);
                        String taskNo = batTaskCfg.getTaskNo();//任务编号
                        String taskType = batTaskCfg.getTaskType();//任务类型
                        batTaskRun.setTaskDate(openDay);//任务日期
                        batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_000.key);// 任务状态 STD_TASK_TYPE,待执行
                        batTaskRun.setTaskBeginTime(DateUtils.getCurrDateTimeStr());// 任务开始时间,格式 YYYY-MM-DD HH:mm:ss
                        logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.1.1.遍历批量任务管理任务到任务运行管理中基本数据结束");
                        logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.1.2.计算依赖任务编号和互斥任务编号开始");
                        Map relMap = new HashMap();
                        relMap.put("relTaskNo", taskNo);
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_REL_MODUEL.key, BatEnums.BAT_TASK_REL_MODUEL.value, JSON.toJSONString(relMap));
                        List<BatTaskRel> batTaskRelList = batTaskRelService.selectByRelMap(relMap);
                        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_REL_MODUEL.key, BatEnums.BAT_TASK_REL_MODUEL.value, JSON.toJSONString(batTaskRelList));
                        String relTaskNos = null;  // 依赖任务编号
                        if (CollectionUtils.nonEmpty(batTaskRelList)) {
                            List<String> relTaskNoList = batTaskRelList.stream().map(batTaskRel -> {
                                String relTaskNo = batTaskRel.getRelTaskNo();
                                return relTaskNo;
                            }).collect(Collectors.toList());
                            relTaskNos = relTaskNoList.stream().collect(Collectors.joining(","));
                            logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "依赖任务编号为[" + relTaskNos + "]");
                            batTaskRun.setRelationFlag(BatEnums.STD_YES.key);
                            batTaskRun.setRelNo(taskNo);
                            batTaskRun.setRelTaskNo(relTaskNos);
                        }

                        Map mutexMap = new HashMap();
                        mutexMap.put("mutexTaskNo", taskNo);
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_MUTEX_MODUEL.key, BatEnums.BAT_TASK_MUTEX_MODUEL.value, JSON.toJSONString(mutexMap));
                        List<BatTaskMutex> batTaskMutexList = batTaskMutexService.selectByMutexMap(mutexMap);
                        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_MUTEX_MODUEL.key, BatEnums.BAT_TASK_MUTEX_MODUEL.value, JSON.toJSONString(batTaskMutexList));
                        String mutexTaskNos = null;   // 互斥任务编号
                        if (CollectionUtils.nonEmpty(batTaskMutexList)) {
                            List<String> mutexTaskNoList = batTaskMutexList.stream().map(batTaskMutex -> {
                                String mutexTaskNo = batTaskMutex.getMutexTaskNo();
                                return mutexTaskNo;
                            }).collect(Collectors.toList());
                            mutexTaskNos = String.join(",", mutexTaskNoList);
                            logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "互斥任务编号为[" + mutexTaskNos + "]");
                            batTaskRun.setMutexFlag(BatEnums.STD_YES.key);
                            batTaskRun.setMutexNo(taskNo);
                            batTaskRun.setMutexTaskNo(mutexTaskNos);
                        }

                        logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.1.2.计算依赖任务编号和互斥任务编号结束");
                        logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.1.3.根据任务类型设置任务运行管理中字段开始");
                        // 装数任务（由文件至表） 加工  信号文件全名  数据文件全名 工作目录 远程目录 本地目录
                        if (Objects.equals(BatEnums.TASK_TYPE_01.key, taskType)) {// 装数任务（由文件至表）
                            logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "任务编号为:[" + batTaskCfg.getTaskNo() + "],任务名称为:[" + batTaskCfg.getTaskName() + "]");
                            logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "任务类型为:装数任务（由文件至表）时设置任务运行管理中字段开始");
                            BatTransferCfg batTransferCfg = batTransferCfgService.selectByPrimaryKey(batTaskCfg.getTaskNo());
                            String fileExchangeType = batTaskCfg.getFileExchangeType();//文件交互方式 STD_FILE_EXCHANGE_TYPE
                            String fileDateType = batTaskCfg.getFileDateType();//文件日期类型
                            String dataDateType = batTaskCfg.getDataDateType();  // 数据日期类型
                            String signType = batTaskCfg.getSignalType(); // 信号类型
                            String signalFileName = batTaskCfg.getSignalFileName(); // 信号文件全名

                            String signalDateType = batTaskCfg.getSignalDateType();//信号日期类型
                            String dataFileName = batTaskCfg.getDataFileName();//数据文件全名
                            String batTTabName = batTaskCfg.getBatTTabName();//数据文件落地表名

                            String workDir = batTransferCfg.getWorkDir();//工作目录 必填，全路径，需要用文件日期替换路径中的日期
                            String remoteDir = batTransferCfg.getRemoteDir();//远程目录
                            String remoteSignalDir = batTransferCfg.getRemoteSignalDir();//远程信号目录
                            String localDir = batTransferCfg.getLocalDir();//本地目录 必填，全路径，需要用文件日期替换路径中的日期
                            String dbType = batTransferCfg.getDbType();// 数据库类型
                            String dbInstance = batTransferCfg.getDbInstance();//数据库实例
                            String dbIp = batTransferCfg.getDbIp();//数据库IP
                            String dbPort = batTransferCfg.getDbPort(); //数据库端口
                            String taskDate = openDay;

                            String fileDate = DateUtil.countRelativeDate(fileDateType, taskDate);// 文件日期
                            Date fileDateYmd = DateUtils.parseDate(fileDate, "yyyy-MM-dd");
                            fileDate = DateUtils.formatDate(fileDateYmd, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd
                            try {
                                workDir = FileUtils.bulidWorkFolder(workDir, fileDate);     // 工作处理目录
                                localDir = FileUtils.bulidLocalFolder(localDir, fileDate); // 本地处理目录
                            } catch (Exception e) {
                                String ech080011Value = EchEnum.ECH080011.value + e.getMessage();
                                logger.error(TradeLogConstants.BATCH_STEP_EXCEPTION_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, ech080011Value);
                                throw BizException.error(null, EchEnum.ECH080011.key, ech080011Value);
                            }
                            // 获取远程FTP服务端目录  Oracle:/Files/YYYYMMDD/DBTYPE/DBINSTANCE_DBIP/ -> /Files/20210414/Oracle/ORCL_10.28.124.175/ ,MySQL:/Files/YYYYMMDD/DBTYPE/DBIP_DBPORT/ -> /Files/20210421/MySQL/10.28.125.111_15515/
                            if (Objects.equals(BatEnums.DB_INSTANCE_ORACLE.key, dbType)) {
                                remoteDir = remoteDir.replaceAll("YYYYMMDD", fileDate).replace("DBTYPE", dbType).replace("DBINSTANCE", dbInstance).replace("DBIP", dbIp).replace("-", "");
                                remoteSignalDir = remoteDir;//远程信号目录
                            } else if (Objects.equals(BatEnums.DB_INSTANCE_MYSQL.key, dbType)) {
                                remoteDir = remoteDir.replaceAll("YYYYMMDD", fileDate).replace("DBTYPE", dbType).replace("DBIP", dbIp).replace("DBPORT", dbPort).replace("-", "");
                                remoteSignalDir = remoteDir;//远程信号目录
                            } else if (Objects.equals(BatEnums.DB_INSTANCE_GUASS.key, dbType)) {
                                // 数仓提供的文件目录： /tmp/outdata/outfile/xxd/YYYY-MM-DD
                                remoteDir = remoteDir.replaceAll("YYYY-MM-DD", taskDate);
                            } else {
                                // 贷记卡文件的远程路径单独处理，dbType设置为空
                                // 贷记卡的目录：/NFSDATA/FTPFILE/AICFTP/000064163056/000064163056/output/YYYYMMDD
                                remoteDir = FileUtils.bulidRemoteDir(remoteDir, fileDate);
                                remoteSignalDir = remoteDir;//远程信号目录
                                // 此处对贷记卡的数据文件全名做单独的处理
                                dataFileName =  FileUtils.bulidRunFileName(dataFileName, fileDateType, taskDate);
                            }
                            logger.info("获取远程FTP服务端目录为[{}]", remoteDir);
                            logger.info("信号类型：[" + signType + "]，文件交互方式：[" + fileExchangeType + "],工作处理目录：[" + workDir + "],本地处理目录：[" + localDir + "],远程目录：[" + remoteDir + "],远程信号目录：[" + remoteSignalDir + "],文件日期：[" + fileDate + "]");
                            // 当“信号类型”为“11-文件信号”，需要生成下载信号文件
                            if (Objects.equals(BatEnums.SIGNAL_TYPE_21.key, signType) || Objects.equals(BatEnums.SIGNAL_TYPE_11.key, signType)) {
                                signalFileName = FileUtils.bulidRunFileName(signalFileName, signalDateType, taskDate);
                                logger.info("当“信号类型”为“11-文件信号”，需要生成下载信号文件为[{}]", signalFileName);
                            }
                            batTaskRun.setDataFileName(dataFileName);// 数据文件全名
                            batTaskRun.setSignalList(signalFileName);// 信号清单
                            batTaskRun.setSignalFileName(signalFileName);// 信号文件全名
                            batTaskRun.setWorkDir(workDir);//工作目录
                            batTaskRun.setRemoteDir(remoteDir);//远程目录
                            batTaskRun.setRemoteSignalDir(remoteSignalDir);// 远程信号目录
                            batTaskRun.setLocalDir(localDir);//本地目录
                            logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "任务类型为:装数任务（由文件至表）时设置任务运行管理中字段结束");
                        }
                        logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.1.3.根据任务类型设置任务运行管理中字段结束");
                        return batTaskRun;
                    }).collect(Collectors.toList());
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.1.遍历批量任务管理任务到任务运行管理结束,总任务数为:[" + batTaskRunList.size() + "]");
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.2.插入任务运行管理开始");
                    batTaskRunList.stream().forEach(
                            batTaskRun -> {
                                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                                int result = batTaskRunService.insertSelective(batTaskRun);
                                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, result);
                            });
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.2.插入任务运行管理结束");

                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.3.修改调度运行开始任务状态开始");
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, "任务日期为:[" + openDay + "],任务编号为:[" + TaskEnum.BATSTART_TASK.key + "]");
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BATSTART_TASK.key);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.3.修改调度运行开始任务状态结束");
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, "3.初始化任务结束");
                    /****************** 3.初始化任务结束 *****************/
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.BATSTART_UPDATE_BAT_STEP.key, JobStepEnum.BATSTART_UPDATE_BAT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return batStartUpdateRunStep;
    }

    /**
     * 生成 JobParameter
     *
     * @param openDay 跑批日期
     * @param restart 是否需要重跑 需要则加时间戳
     * @return
     */
    private static JobParameters createJobParams(String openDay, String taskNo, boolean... restart) {
        JobParametersBuilder builder = new JobParametersBuilder().addString("openDay", openDay).addString("taskNo", taskNo);
        for (boolean arg : restart) {
            if (arg) {
                builder.addString("date", DateUtils.getCurrTimestamp().toString());
            }
        }
        return builder.toJobParameters();
    }

}
