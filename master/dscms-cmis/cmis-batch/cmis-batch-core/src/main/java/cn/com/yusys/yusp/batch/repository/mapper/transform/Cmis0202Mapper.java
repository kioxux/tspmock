package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0202</br>
 * 任务名称：加工任务-额度处理-表内占用总余额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0202Mapper {

    /**
     * 1.   清理额度占用临时表
     *
     * @return
     */
    void truncateLmtContRel01A();

    /**
     * 清空额度中间表(用于insertLmtContRel01A)
     *
     * @return
     */
    void truncateTmpLmtMid01A();

    /**
     * 插入额度中间表(用于insertLmtContRel01A)
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtMid01A(@Param("openDay") String openDay);

    /**
     * 用信最高额（未到期）：用信申请金额/用信合同金额（不管是否还款，不恢复额度）  更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel01A(@Param("openDay") String openDay);


    /**
     * 清空额度中间表(用于insertLmtContRel01B)
     *
     * @return
     */
    void truncateTmpLmtMid01B();

    /**
     * 插入额度中间表(用于insertLmtContRel01B)
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtMid01B(@Param("openDay") String openDay);

    /**
     * 规则：未到期最高额 BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则  （未到期）  用信申请金额/用信合同金额（不管是否还款，不恢复额度）
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel01B(@Param("openDay") String openDay);


    /**
     * 更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel01B(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @return
     */
    void truncateLmtContRel03();

    /**
     * 清空额度中间表(用于insertLmtContRel03A)
     *
     * @return
     */
    void truncateTmpLmtMid03A();

    /**
     * 插入额度中间表(用于insertLmtContRel03A)
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtMid03A(@Param("openDay") String openDay);//效率问题此步骤已拆分

    /**
     * 删除合同占用关系信息加工表0202
     */

    void truncateTmpDealContAaccRel0202();//add20211013


    /**
     * 插入合同占用关系信息加工表0202
     *
     * @param openDay
     * @return
     */
    int insertTmpDealContAaccRel0202(@Param("openDay") String openDay);//add20211013

    /**
     * 插入临时表-额度中间表 4A步骤
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtMid4A(@Param("openDay") String openDay);//add20211013


    /**
     * 插入临时表-额度中间表 4B步骤
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtMid4B(@Param("openDay") String openDay);//add20211013


    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel03A(@Param("openDay") String openDay);


    /**
     * 清空额度中间表(用于insertLmtContRel03B)
     *
     * @return
     */
    void truncateTmpLmtMid03B();

    /**
     * 插入额度中间表(用于insertLmtContRel03B)
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtMid03B(@Param("openDay") String openDay);

    /**
     * 用信一般合同（未到期）：用信申请金额/用信合同金额-用信申请下的台账总额+用信申请下的台账余额
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel03B(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel03(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @return
     */
    void truncateLmtContRel04();

    /**
     * 清空额度中间表(用于insertLmtContRel04A)
     *
     * @return
     */
    void truncateTmpLmtMid04A();

    /**
     * 插入额度中间表(用于insertLmtContRel04A)
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtMid04A(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel04(@Param("openDay") String openDay);

    /**
     * 规则： BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 （未到期） 用信申请金额/用信合同金额（不管是否还款，不恢复额度）
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel04A(@Param("openDay") String openDay);


    /**
     * 清空额度中间表(用于insertTmpLmtAmtEd)
     *
     * @return
     */
    void truncateTmpLmtMidEd();

    /**
     * 插入额度中间表(用于insertTmpLmtAmtEd)
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtMidEd(@Param("openDay") String openDay);

    /**
     * 已到期 ：未到期的最高额资产池协议（及在途）占用额度
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtEd(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel06(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     */
    void truncateLmtContRel06();

    /**
     * 最高额授信协议下台账占用授信的余额计算： 已到期 未到期的最高额授信协议下用信下台账余额、敞口余额
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtDk(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系 清理额度占用临时表
     */
    void truncateLmtContRel05();

    /**
     * 已到期 ：用信申请下的台账余额
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtTz(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel05(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系中的占用总敞口余额
     *
     * @param openDay
     * @return
     */
    int updateTdlcr(@Param("openDay") String openDay);


}
