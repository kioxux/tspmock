package cn.com.yusys.yusp.batch.service.load;

import cn.com.yusys.yusp.batch.repository.mapper.load.TableUtilsMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取数据库表相关服务类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月21日 下午11:56:54
 */
@Service
@Transactional
public class TableUtilsService {
    private static final Logger logger = LoggerFactory.getLogger(TableUtilsService.class);
    @Autowired
    private TableUtilsMapper tableUtilsMapper;

    /**
     * 获取数据库中某表中总条数
     *
     * @param dbName    数据库名
     * @param tableName 表名
     * @return
     */
    public Integer countTableLines(String dbName, String tableName) {
        logger.info("获取数据库英文名:[{}],表英文名:[{}]中总条数开始", dbName, tableName);
        Map countMap = new HashMap();
        countMap.put("dbName", dbName);//数据库英文名
        countMap.put("tableName", tableName);//表英文名
        int countTableLines = tableUtilsMapper.coutTableLines(countMap);
        logger.info("获取数据库英文名:[{}],表英文名:[{}]中总条数结束,总行数为:[{}]", dbName, tableName, countTableLines);
        return countTableLines;
    }


    /**
     * 获取数据库中批后备份表中总条数
     *
     * @param dbName    数据库名
     * @param tableName 表名
     * @return
     */
    public Integer coutBakDafTableLines(String dbName, String tableName) {
        logger.info("获取数据库英文名:[{}],表英文名:[{}]中总条数开始", dbName, tableName);
        Map bakDafMap = new HashMap();
        bakDafMap.put("dbName", dbName);//数据库英文名
        bakDafMap.put("tableName", tableName);//表英文名
        int coutBakDafTableLines = tableUtilsMapper.coutBakDafTableLines(bakDafMap);
        logger.info("获取数据库英文名:[{}],表英文名:[{}]中总条数结束,总行数为:[{}]", dbName, tableName, coutBakDafTableLines);
        return coutBakDafTableLines;
    }

    /**
     * 分析相关表
     *
     * @param dbName
     * @param tableName
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public List<Map> analyzeTable(String dbName, String tableName) {
        logger.info("分析数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map analyzeMap = new HashMap();
        analyzeMap.put("dbName", dbName);//数据库英文名
        analyzeMap.put("tableName", tableName);//表英文名
        List<Map> analyzeResultMap = tableUtilsMapper.analyzeTable(analyzeMap);
        logger.info("分析数据库英文名:[{}]中表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName, JSON.toJSONString(analyzeResultMap, SerializerFeature.WriteMapNullValue));
        return analyzeResultMap;
    }

    /**
     * 先删除相关表</br>
     * 根据备份表创建相关表</br>
     * 分析相关表</br>
     * 20211031:经和领导沟通确认采用此方案
     *
     * @param dbName
     * @param tableName
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void renameCreateDropTable(String dbName, String tableName) {
        logger.info("1.删除相关表,2.根据备份表创建相关表,3.分析相关表开始");
        String tableNameBak = tableName + "_model";//备份表统一使用 _model 为结尾
        logger.info("删除相关表的数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map dropMap = new HashMap();
        dropMap.put("dbName", dbName);//数据库英文名
        dropMap.put("tableName", tableName);//表英文名
        List<Map> dropResultMap = tableUtilsMapper.dropTable(dropMap);
        logger.info("删除相关表的数据库英文名:[{}]中表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName, JSON.toJSONString(dropResultMap, SerializerFeature.WriteMapNullValue));

        logger.info("删除相关表[/*sets:allsets*/]的数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map dropWithSetsMap = new HashMap();
        dropWithSetsMap.put("dbName", dbName);//数据库英文名
        dropWithSetsMap.put("tableName", tableName);//表英文名
        List<Map> dropResulWithSetsMap = tableUtilsMapper.dropTableWithSets(dropWithSetsMap);
        logger.info("删除相关表[/*sets:allsets*/]的数据库英文名:[{}]中表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName, JSON.toJSONString(dropResulWithSetsMap, SerializerFeature.WriteMapNullValue));
        
        logger.info("创建相关表的数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map createMap = new HashMap();
        createMap.put("dbName", dbName);//数据库英文名
        createMap.put("tableName", tableName);//表英文名
        createMap.put("tableNameBak", tableNameBak);//备份表英文名
        List<Map> createResultMap = tableUtilsMapper.createTable(createMap);
        logger.info("创建相关表的数据库英文名:[{}]中表英文名:[{}],备份表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName, tableNameBak, JSON.toJSONString(createResultMap, SerializerFeature.WriteMapNullValue));

        logger.info("分析数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map analyzeMap = new HashMap();
        analyzeMap.put("dbName", dbName);//数据库英文名
        analyzeMap.put("tableName", tableName);//表英文名
        List<Map> analyzeResultMapList = tableUtilsMapper.analyzeTable(analyzeMap);
        logger.info("分析数据库英文名:[{}]中表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName, JSON.toJSONString(analyzeResultMapList, SerializerFeature.WriteMapNullValue));
        logger.info("1.删除相关表,2.根据备份表创建相关表,3.分析相关表结束");
    }

    /**
     * 重命名创建和删除相关表</br>
     * 20211031:经和领导沟通确认不采用此方案</br>
     *
     * @param dbName
     * @param tableName
     */
    @Deprecated
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void renameCreateDropTable01(String dbName, String tableName) {
        logger.info("重命名相关表的数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map renameMap = new HashMap();
        renameMap.put("dbName", dbName);//数据库英文名
        renameMap.put("tableName", tableName);//表英文名
        renameMap.put("tableNameBak", tableName + "Bak");//备份表英文名
        List<Map> renameResultMap = tableUtilsMapper.renameTable(renameMap);
        logger.info("重命名相关表的数据库英文名:[{}]中表英文名:[{}],备份表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName, tableName + "Bak", JSON.toJSONString(renameResultMap, SerializerFeature.WriteMapNullValue));

        logger.info("创建相关表的数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map createMap = new HashMap();
        createMap.put("dbName", dbName);//数据库英文名
        createMap.put("tableName", tableName);//表英文名
        createMap.put("tableNameBak", tableName + "Bak");//备份表英文名
        List<Map> createResultMap = tableUtilsMapper.createTable(createMap);
        logger.info("创建相关表的数据库英文名:[{}]中表英文名:[{}],备份表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName, tableName + "Bak", JSON.toJSONString(createResultMap, SerializerFeature.WriteMapNullValue));

        logger.info("删除临时备份表的数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map dropBakMap = new HashMap();
        dropBakMap.put("dbName", dbName);//数据库英文名
        dropBakMap.put("tableNameBak", tableName + "Bak");//备份表英文名
        List<Map> dropBakResultMap = tableUtilsMapper.dropBakTable(dropBakMap);
        logger.info("删除临时备份表的数据库英文名:[{}]中表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName + "Bak", JSON.toJSONString(dropBakResultMap, SerializerFeature.WriteMapNullValue));

        logger.info("分析数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map analyzeMap = new HashMap();
        analyzeMap.put("dbName", dbName);//数据库英文名
        analyzeMap.put("tableName", tableName);//表英文名
        List<Map> analyzeResultMap = tableUtilsMapper.analyzeTable(analyzeMap);
        logger.info("分析数据库英文名:[{}]中表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName, JSON.toJSONString(analyzeResultMap, SerializerFeature.WriteMapNullValue));
    }

    /**
     * 重命名相关表
     *
     * @param dbName
     * @param tableName
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void renameTable(String dbName, String tableName) {
        logger.info("重命名相关表的数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map renameMap = new HashMap();
        renameMap.put("dbName", dbName);//数据库英文名
        renameMap.put("tableName", tableName);//表英文名
        renameMap.put("tableNameBak", tableName + "Bak");//备份表英文名
        List<Map> analyzeResultMap = tableUtilsMapper.renameTable(renameMap);
        logger.info("重命名相关表的数据库英文名:[{}]中表英文名:[{}],备份表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName, tableName + "Bak", JSON.toJSONString(analyzeResultMap, SerializerFeature.WriteMapNullValue));
    }

    /**
     * 创建相关表
     *
     * @param dbName
     * @param tableName
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void createTable(String dbName, String tableName) {
        logger.info("创建相关表的数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map createMap = new HashMap();
        createMap.put("dbName", dbName);//数据库英文名
        createMap.put("tableName", tableName);//表英文名
        createMap.put("tableNameBak", tableName + "Bak");//备份表英文名
        List<Map> analyzeResultMap = tableUtilsMapper.createTable(createMap);
        logger.info("创建相关表的数据库英文名:[{}]中表英文名:[{}],备份表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName, tableName + "Bak", JSON.toJSONString(analyzeResultMap, SerializerFeature.WriteMapNullValue));
    }

    /**
     * 删除临时备份表
     *
     * @param dbName
     * @param tableName
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void dropBakTable(String dbName, String tableName) {
        logger.info("删除临时备份表的数据库英文名:[{}]中表英文名:[{}]操作开始", dbName, tableName);
        Map dropBakMap = new HashMap();
        dropBakMap.put("dbName", dbName);//数据库英文名
        dropBakMap.put("tableNameBak", tableName + "Bak");//备份表英文名
        List<Map> analyzeResultMap = tableUtilsMapper.dropBakTable(dropBakMap);
        logger.info("删除临时备份表的数据库英文名:[{}]中表英文名:[{}]操作结束,返回结果是:[{}]", dbName, tableName + "Bak", JSON.toJSONString(analyzeResultMap, SerializerFeature.WriteMapNullValue));
    }

}
