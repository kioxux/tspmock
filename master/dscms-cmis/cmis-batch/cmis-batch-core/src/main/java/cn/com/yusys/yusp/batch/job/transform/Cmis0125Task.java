package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0125Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0125</br>
 * 任务名称：加工任务-业务处理-资金同业批复状态 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0125Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0125Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0125Service cmis0125Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job cmis0125Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0125_JOB.key, JobStepEnum.CMIS0125_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0125Job = this.jobBuilderFactory.get(JobStepEnum.CMIS0125_JOB.key)
                .start(cmis0125UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0125CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(cmis0125UpdateIntbankOrgAdmitAccStep(WILL_BE_INJECTED)) // 更新同业机构准入台账表
                .next(cmis0125UpdateIntbankOrgAdmitReplyStep(WILL_BE_INJECTED)) // 更新同业机构准入批复表
                .next(cmis0125UpdateLmtIntbankAccStep(WILL_BE_INJECTED)) // 更新同业授信台账表
                .next(cmis0125UpdateLmtIntbankReplyStep(WILL_BE_INJECTED)) // 更新同业授信批复表
                .next(cmis0125UpdateLmtSigInvestAccStep(WILL_BE_INJECTED)) // 更新单笔投资授信台账表
                .next(cmis0125UpdateLmtSigInvestBasicLmtAccStep(WILL_BE_INJECTED)) // 更新底层授信额度台账表
                .next(cmis0125UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0125Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0125UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0125_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0125UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0125_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0125_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0125_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0125UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0125CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0125_CHECK_REL_STEP.key, JobStepEnum.CMIS0125_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0125CheckRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0125_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0125_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0125_CHECK_REL_STEP.key, JobStepEnum.CMIS0125_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0125_CHECK_REL_STEP.key, JobStepEnum.CMIS0125_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0125CheckRelStep;
    }

    /**
     * 更新同业机构准入台账表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0125UpdateIntbankOrgAdmitAccStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_INTBANK_ORG_ADMIT_ACC_STEP.key, JobStepEnum.CMIS0125_UPDATE_INTBANK_ORG_ADMIT_ACC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0125UpdateIntbankOrgAdmitAccStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0125_UPDATE_INTBANK_ORG_ADMIT_ACC_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0125Service.cmis0125UpdateIntbankOrgAdmitAcc(openDay);//更新同业机构准入台账表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_INTBANK_ORG_ADMIT_ACC_STEP.key, JobStepEnum.CMIS0125_UPDATE_INTBANK_ORG_ADMIT_ACC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0125UpdateIntbankOrgAdmitAccStep;
    }
    /**
     * 更新同业机构准入批复表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0125UpdateIntbankOrgAdmitReplyStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_INTBANK_ORG_ADMIT_REPLY_STEP.key, JobStepEnum.CMIS0125_UPDATE_INTBANK_ORG_ADMIT_REPLY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0125UpdateIntbankOrgAdmitReplyStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0125_UPDATE_INTBANK_ORG_ADMIT_REPLY_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0125Service.cmis0125UpdateIntbankOrgAdmitReply(openDay);//更新同业机构准入批复表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_INTBANK_ORG_ADMIT_REPLY_STEP.key, JobStepEnum.CMIS0125_UPDATE_INTBANK_ORG_ADMIT_REPLY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0125UpdateIntbankOrgAdmitReplyStep;
    }
    /**
     * 更新同业授信台账表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0125UpdateLmtIntbankAccStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_LMT_INTBANK_ACC_STEP.key, JobStepEnum.CMIS0125_UPDATE_LMT_INTBANK_ACC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0125UpdateLmtIntbankAccStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0125_UPDATE_LMT_INTBANK_ACC_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0125Service.cmis0125UpdateLmtIntbankAcc(openDay);//更新同业授信台账表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_LMT_INTBANK_ACC_STEP.key, JobStepEnum.CMIS0125_UPDATE_LMT_INTBANK_ACC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0125UpdateLmtIntbankAccStep;
    }
    /**
     * 更新同业授信批复表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0125UpdateLmtIntbankReplyStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_LMT_INTBANK_REPLY_STEP.key, JobStepEnum.CMIS0125_UPDATE_LMT_INTBANK_REPLY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0125UpdateLmtIntbankReplyStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0125_UPDATE_LMT_INTBANK_REPLY_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0125Service.cmis0125UpdateLmtIntbankReply(openDay);//更新同业授信批复表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_LMT_INTBANK_REPLY_STEP.key, JobStepEnum.CMIS0125_UPDATE_LMT_INTBANK_REPLY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0125UpdateLmtIntbankReplyStep;
    }
    /**
     * 更新单笔投资授信台账表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0125UpdateLmtSigInvestAccStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_LMT_SIG_INVEST_ACC_STEP.key, JobStepEnum.CMIS0125_UPDATE_LMT_SIG_INVEST_ACC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0125UpdateLmtSigInvestAccStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0125_UPDATE_LMT_SIG_INVEST_ACC_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0125Service.cmis0125UpdateLmtSigInvestAcc(openDay);//更新单笔投资授信台账表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_LMT_SIG_INVEST_ACC_STEP.key, JobStepEnum.CMIS0125_UPDATE_LMT_SIG_INVEST_ACC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0125UpdateLmtSigInvestAccStep;
    }
    /**
     * 更新底层授信额度台账表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0125UpdateLmtSigInvestBasicLmtAccStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_LMT_SIG_INVEST_BASIC_LMT_ACC_STEP.key, JobStepEnum.CMIS0125_UPDATE_LMT_SIG_INVEST_BASIC_LMT_ACC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0125UpdateLmtSigInvestBasicLmtAccStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0125_UPDATE_LMT_SIG_INVEST_BASIC_LMT_ACC_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0125Service.cmis0125UpdateLmtSigInvestBasicLmtAcc(openDay);//更新底层授信额度台账表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_LMT_SIG_INVEST_BASIC_LMT_ACC_STEP.key, JobStepEnum.CMIS0125_UPDATE_LMT_SIG_INVEST_BASIC_LMT_ACC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0125UpdateLmtSigInvestBasicLmtAccStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0125UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0125_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0125UpdateTask100Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0125_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0125_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0125_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0125_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0125UpdateTask100Step;
    }


}
