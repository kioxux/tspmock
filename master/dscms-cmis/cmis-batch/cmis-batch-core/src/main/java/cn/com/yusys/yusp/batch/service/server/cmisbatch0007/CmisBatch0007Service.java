package cn.com.yusys.yusp.batch.service.server.cmisbatch0007;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007RespDto;
import cn.com.yusys.yusp.batch.service.transform.Cmis0425Service;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import com.alibaba.fastjson.JSON;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理Service:不定期分类任务信息生成
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class CmisBatch0007Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0007Service.class);

    @Autowired
    private Cmis0425Service cmis0425Service;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 交易码：cmisbatch0007
     * 交易描述：不定期分类任务信息生成
     *
     * @param cmisbatch0007ReqDto
     * @return
     * @throws Exception
     */
    // @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Cmisbatch0007RespDto cmisBatch0007(Cmisbatch0007ReqDto cmisbatch0007ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value, JSON.toJSONString(cmisbatch0007ReqDto));
        Cmisbatch0007RespDto cmisbatch0007RespDto = new Cmisbatch0007RespDto();//
        Map map = new HashMap<>();
        try {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            String checkType = null;
            map.put("openDay", openDay);
            if (Strings.isNotEmpty(cmisbatch0007ReqDto.getTaskNo())) {
                map.put("taskNo", cmisbatch0007ReqDto.getTaskNo());
            }
            if (Strings.isNotEmpty(cmisbatch0007ReqDto.getCusId())) {
                map.put("cusId", cmisbatch0007ReqDto.getCusId());
            }
            //获取不定期检查类型
            if (Strings.isNotEmpty(cmisbatch0007ReqDto.getCheckType())) {
                checkType = cmisbatch0007ReqDto.getCheckType();
            }
            logger.info("调用加工任务-贷后管理-风险分类子表数据生成开始");
            // 因添加tmp_risk_task_list临时表，需插入一次risk_task_list表
            cmis0425Service.cmis0425InsertTmpRiskTaskList(map); // 插入 风险分类任务临时主表
            cmis0425Service.cmis0425InsertRiskDebitInfo01(map); // 插入 风险分类借据信息[对公专项贷款风险分类借据信息]
            cmis0425Service.cmis0425InsertRiskDebitInfo02(map); // 插入 风险分类借据信息[个人经营性分类借据大额跑批矩阵]
            cmis0425Service.cmis0425InsertRiskDebitInfo03(map); // 插入 风险分类借据信息[个人经营性分类借据小额跑批矩阵]
            cmis0425Service.cmis0425InsertRiskDebitInfo04(map); // 插入 风险分类借据信息[个人消费分类借据纯线上贷跑批矩阵]
            cmis0425Service.cmis0425InsertRiskDebitInfo05(map); // 插入 风险分类借据信息[个人消费分类借据大额跑批矩阵]
            cmis0425Service.cmis0425InsertRiskDebitInfo06(map); // 插入 风险分类借据信息[个人消费分类借据小额跑批矩阵]
            cmis0425Service.cmis0425InsertRiskDebitInfo07(map); // 插入 风险分类借据信息[个人消费分类借据住房按揭与汽车]
            cmis0425Service.cmis0425InsertRiskGuarContAnaly(map); // 插入 风险分类担保合同分析
            cmis0425Service.cmis0425InsertRiskGuarntrList(map);// 插入 风险分类保证人检查
            cmis0425Service.cmis0425InsertRiskPldimnList(map);// 插入 风险分类抵质押物检查
            if ("1".equals(checkType)) {//判断风险分类检查类型是否为“公司客户风险分类”
                cmis0425Service.cmis0425InsertRiskFinReportAnaly(map);// 插入 企业财务情况
            }
            cmis0425Service.cmis0425InsertRiskRepayAnaly(map);// 插入 影响偿还因素分析
            cmis0425Service.insertRiskDebitInfoFromTmp(map); // 日间风险分类临时借据信息回插
            map.put("taskStatus", "2");
            logger.info("调用加工任务-贷后管理-风险分类子表数据生成结束");
            cmisbatch0007RespDto.setOpFlag(CmisCommonConstants.OP_FLAG_S);
            cmisbatch0007RespDto.setOpMessage(CmisCommonConstants.OP_MSG_S);
        } catch (Exception e) {
            map.put("taskStatus", "3");
            logger.info("调用加工任务-贷后管理-风险分类子表数据生成异常,更新该笔任务生成状态3异常");
            cmisbatch0007RespDto.setOpFlag(CmisCommonConstants.OP_FLAG_F);
            cmisbatch0007RespDto.setOpMessage(CmisCommonConstants.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        } finally {
            try {
                cmis0425Service.cmis0425ChangeRiskTaskListTaskStatus(map);
                logger.info("调用加工任务-贷后管理-风险分类子表数据生成异常,更新该笔任务生成状态[{}]",map.get("taskStatus"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value);
        return cmisbatch0007RespDto;
    }
}