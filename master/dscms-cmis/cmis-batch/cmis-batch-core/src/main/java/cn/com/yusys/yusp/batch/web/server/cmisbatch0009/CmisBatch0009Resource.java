package cn.com.yusys.yusp.batch.web.server.cmisbatch0009;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009ReqDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0009.CmisBatch0009Service;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询[贷款账户期供表]信息
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "CMISBATCH0009:查询[贷款账户期供表]信息")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0009Resource.class);

    @Autowired
    private CmisBatch0009Service cmisBatch0009Service;

    /**
     * 交易码：cmisbatch0009
     * 交易描述：查询[贷款账户期供表]关联信息
     *
     * @param cmisbatch0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询[贷款账户期供表]信息")
    @PostMapping("/cmisbatch0009")
    protected @ResponseBody
    ResultDto<Cmisbatch0009RespDto> cmisbatch0009(@Validated @RequestBody Cmisbatch0009ReqDto cmisbatch0009ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value, JSON.toJSONString(cmisbatch0009ReqDto));
        Cmisbatch0009RespDto cmisbatch0009RespDto = new Cmisbatch0009RespDto();// 响应Dto:调度运行管理信息查詢
        ResultDto<Cmisbatch0009RespDto> cmisbatch0009ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0009ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value, JSON.toJSONString(cmisbatch0009ReqDto));
            cmisbatch0009RespDto = cmisBatch0009Service.cmisBatch0009(cmisbatch0009ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value, JSON.toJSONString(cmisbatch0009RespDto));
            // 封装cmisbatch0009ResultDto中正确的返回码和返回信息
            cmisbatch0009ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0009ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value, e.getMessage());
            // 封装cmisbatch0009ResultDto中异常返回码和返回信息
            cmisbatch0009ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0009ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0009RespDto到cmisbatch0009ResultDto中
        cmisbatch0009ResultDto.setData(cmisbatch0009RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value, JSON.toJSONString(cmisbatch0009ResultDto));
        return cmisbatch0009ResultDto;
    }
}
