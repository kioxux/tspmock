package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKMD028</br>
 * 任务名称：批后备份月表日表任务-备份白名单额度信息历史[LMT_WHITE_INFO_HISTORY]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakMD028Mapper {
    /**
     * 查询待删除当天的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryMD028DeleteOpenDayDLmtWhiteInfoHistoryCounts(@Param("openDay") String openDay);


    /**
     * 查询待删除当月的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据
     *
     * @param openDay
     */
    int queryMD0028DeleteDLmtWhiteInfoHistory(@Param("openDay") String openDay);

    /**
     * 删除当天的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据
     *
     * @param openDay
     */
    int bakMD028DeleteDLmtWhiteInfoHistory(@Param("openDay") String openDay);

    /**
     * 删除当月的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据
     *
     * @param openDay
     */
    int bakMD028DeleteMLmtWhiteInfoHistory(@Param("openDay") String openDay);


    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertD(@Param("openDay") String openDay);

    /**
     * 备份当月的数据
     *
     * @param openDay
     * @return
     */
    int insertM(@Param("openDay") String openDay);


    /**
     * 查询当天备份的[分项占用关系信息[LMT_CONT_REL]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryOriginalCounts(@Param("openDay") String openDay);


    /**
     * 查询当月备份的[分项占用关系信息[TMP_M_LMT_CONT_REL]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryBakMCounts(@Param("openDay") String openDay);
}
