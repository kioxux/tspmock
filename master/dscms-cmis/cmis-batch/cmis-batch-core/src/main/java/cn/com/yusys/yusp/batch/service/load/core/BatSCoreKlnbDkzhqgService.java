/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKlnbDkzhqg;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatSCoreKlnbDkzhqgMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKlnbDkzhqgService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-02 08:18:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSCoreKlnbDkzhqgService {
    private static final Logger logger = LoggerFactory.getLogger(BatSCoreKlnbDkzhqgService.class);
    @Autowired
    private BatSCoreKlnbDkzhqgMapper batSCoreKlnbDkzhqgMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSCoreKlnbDkzhqg selectByPrimaryKey(String farendma, String dkjiejuh, Long benqqish, Long benqizqs) {
        return batSCoreKlnbDkzhqgMapper.selectByPrimaryKey(farendma, dkjiejuh, benqqish, benqizqs);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSCoreKlnbDkzhqg> selectAll(QueryModel model) {
        List<BatSCoreKlnbDkzhqg> records = (List<BatSCoreKlnbDkzhqg>) batSCoreKlnbDkzhqgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSCoreKlnbDkzhqg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSCoreKlnbDkzhqg> list = batSCoreKlnbDkzhqgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSCoreKlnbDkzhqg record) {
        return batSCoreKlnbDkzhqgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSCoreKlnbDkzhqg record) {
        return batSCoreKlnbDkzhqgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSCoreKlnbDkzhqg record) {
        return batSCoreKlnbDkzhqgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSCoreKlnbDkzhqg record) {
        return batSCoreKlnbDkzhqgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String farendma, String dkjiejuh, Long benqqish, Long benqizqs) {
        return batSCoreKlnbDkzhqgMapper.deleteByPrimaryKey(farendma, dkjiejuh, benqqish, benqizqs);
    }

    /**
     * 查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>10
     *
     * @param queryMap
     * @return
     */
    public int countKlnbDkzhqgByQueryMap10(Map queryMap) {
        return batSCoreKlnbDkzhqgMapper.countKlnbDkzhqgByQueryMap10(queryMap);
    }

    /**
     * 查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>5
     *
     * @param queryMap
     * @return
     */
    public int countKlnbDkzhqgByQueryMap05(Map queryMap) {
        return batSCoreKlnbDkzhqgMapper.countKlnbDkzhqgByQueryMap05(queryMap);
    }

    /**
     * 无查询条件
     *
     * @param queryMap
     * @return
     */
    public int countKlnbDkzhqgByQueryMap0(Map queryMap) {
        return batSCoreKlnbDkzhqgMapper.countKlnbDkzhqgByQueryMap0(queryMap);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建贷款账户期供表[bat_s_core_klnb_dkzhqg]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_core_klnb_dkzhqg");
        logger.info("重建【CORE0007】贷款账户期供表[bat_s_core_klnb_dkzhqg]结束");
        return 0;
    }


    /**
     * 将T表数据merge到S表
     *
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]开始,请求参数为:[{}]", openDay);
        int updateSResult = batSCoreKlnbDkzhqgMapper.updateDataDateByOpenDay(openDay);
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]结束,返回参数为:[{}]", updateSResult);
    }
}
