package cn.com.yusys.yusp.batch.service.timed;

import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.repository.mapper.timed.BatchTimedTask0001Mapper;
import cn.com.yusys.yusp.batch.repository.mapper.timed.BatchTimedTask0005Mapper;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * 定时任务处理类:营改增  </br>
 *
 * @author cc
 * @version 1.0
 */
@Service
@Transactional
public class BatchTimedTask0005Service {
    private static final Logger logger = LoggerFactory.getLogger(BatchTimedTask0005Service.class);
    @Autowired
    private BatchTimedTask0005Mapper batchTimedTask0005Mapper;

    @Autowired
    private BatchTimedTask0001Mapper batchTimedTask0001Mapper;
    @Autowired
    private BatControlRunService batControlRunService;
    @Resource
    private SequenceTemplateClient sequenceTemplateClient;

    /**
     * 营改增
     *
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public int timedtask0005() throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0005.key, BatEnums.BATCH_TIMED_TASK0005.value);
        // 查询批量运行表，获取任务日期，任务日期等于营业日期
        QueryModel queryModel = new QueryModel();
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(queryModel));
        List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunList));

        if (CollectionUtils.isEmpty(batControlRunList)) {
            // 调度运行管理(BAT_CONTROL_RUN)未配置
            logger.error(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0005.key, BatEnums.BATCH_TIMED_TASK0005.value, EchEnum.ECH080008.value);
            throw BizException.error(null, EchEnum.ECH080008.key, EchEnum.ECH080008.value);
        }
//        // 校验运行状态
//        if (!Objects.equals(batControlRunList.get(0).getControlStatus(), BatEnums.CONTROL_STATUS_010.key)) {
//            // 调度运行管理(BAT_CONTROL_RUN)中状态不是运行中，请及时检查!
//            logger.error(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0005.key, BatEnums.BATCH_TIMED_TASK0005.value, EchEnum.ECH080016.value);
//            throw BizException.error(null, EchEnum.ECH080016.key, EchEnum.ECH080016.value);
//        }

        String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());  // 生成出入库流水号
        String taskDate = batControlRunList.get(0).getOpenDay();//营业日期
        try {
            logger.info("truncate营改增数据表开始");
            batchTimedTask0005Mapper.truncateBatCoretYgz();
            logger.info("truncate营改增数据结束");

            logger.info("生成营改增数据开始，请求参数为:[{}]", "");
            int insertBatCoretYgz = batchTimedTask0005Mapper.insertBatCoretYgz(" ");
            logger.info("生成营改增数据结束,返回参数为:[{}]", JSON.toJSONString(insertBatCoretYgz));

            logger.info("生成营改增日志开始,请求参数为:[{}]", JSON.toJSONString(serno));
            int insertBatCoretYgzLog = batchTimedTask0005Mapper.insertBatCoretYgzLog(serno, taskDate);
            logger.info("生成营改增日志结束,返回参数为:[{}]", JSON.toJSONString(insertBatCoretYgzLog));

            logger.info("delete营改增重复数据开始");
            batchTimedTask0005Mapper.truncateRepeatBatCoretYgz();
            logger.info("delete营改增重复数据结束");
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0005.key, BatEnums.BATCH_TIMED_TASK0005.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0005.key, BatEnums.BATCH_TIMED_TASK0005.value, e.getMessage());
            throw new Exception(e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0005.key, BatEnums.BATCH_TIMED_TASK0005.value);
        return 0;
    }
}
