package cn.com.yusys.yusp.batch.web.server.cmisbatch0007;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007RespDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0007.CmisBatch0007Service;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:不定期分类任务信息生成
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CMISBATCH0007:不定期分类任务信息生成")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0007Resource.class);

    @Autowired
    private CmisBatch0007Service cmisBatch0007Service;

    /**
     * 交易码：cmisbatch0007
     * 交易描述：不定期分类任务信息生成
     *
     * @param cmisbatch0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("不定期分类任务信息生成")
    @PostMapping("/cmisbatch0007")
    protected @ResponseBody
    ResultDto<Cmisbatch0007RespDto> cmisbatch0007(@Validated @RequestBody Cmisbatch0007ReqDto cmisbatch0007ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value, JSON.toJSONString(cmisbatch0007ReqDto));
        Cmisbatch0007RespDto cmisbatch0007RespDto = new Cmisbatch0007RespDto();// 响应Dto:不定期分类任务信息生成
        ResultDto<Cmisbatch0007RespDto> cmisbatch0007ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0007ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value, JSON.toJSONString(cmisbatch0007ReqDto));
            cmisbatch0007RespDto = cmisBatch0007Service.cmisBatch0007(cmisbatch0007ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value, JSON.toJSONString(cmisbatch0007RespDto));
            // 封装cmisbatch0007ResultDto中正确的返回码和返回信息
            cmisbatch0007ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0007ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value, e.getMessage());
            // 封装cmisbatch0007ResultDto中异常返回码和返回信息
            cmisbatch0007ResultDto.setCode(e.getErrorCode());
            cmisbatch0007ResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value, e.getMessage());
            // 封装cmisbatch0007ResultDto中异常返回码和返回信息
            cmisbatch0007ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0007ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0007RespDto到cmisbatch0007ResultDto中
        cmisbatch0007ResultDto.setData(cmisbatch0007RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value, JSON.toJSONString(cmisbatch0007ResultDto));
        return cmisbatch0007ResultDto;
    }
}
