/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.fpt;

import cn.com.yusys.yusp.batch.domain.load.fpt.BatTFptRiskSign;
import cn.com.yusys.yusp.batch.service.load.fpt.BatTFptRiskSignService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTFptRiskSignResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-12 10:11:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battfptrisksign")
public class BatTFptRiskSignResource {
    @Autowired
    private BatTFptRiskSignService batTFptRiskSignService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTFptRiskSign>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTFptRiskSign> list = batTFptRiskSignService.selectAll(queryModel);
        return new ResultDto<List<BatTFptRiskSign>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTFptRiskSign>> index(QueryModel queryModel) {
        List<BatTFptRiskSign> list = batTFptRiskSignService.selectByModel(queryModel);
        return new ResultDto<List<BatTFptRiskSign>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{riskId}")
    protected ResultDto<BatTFptRiskSign> show(@PathVariable("riskId") String riskId) {
        BatTFptRiskSign batTFptRiskSign = batTFptRiskSignService.selectByPrimaryKey(riskId);
        return new ResultDto<BatTFptRiskSign>(batTFptRiskSign);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTFptRiskSign> create(@RequestBody BatTFptRiskSign batTFptRiskSign) throws URISyntaxException {
        batTFptRiskSignService.insert(batTFptRiskSign);
        return new ResultDto<BatTFptRiskSign>(batTFptRiskSign);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTFptRiskSign batTFptRiskSign) throws URISyntaxException {
        int result = batTFptRiskSignService.update(batTFptRiskSign);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{riskId}")
    protected ResultDto<Integer> delete(@PathVariable("riskId") String riskId) {
        int result = batTFptRiskSignService.deleteByPrimaryKey(riskId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batTFptRiskSignService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
