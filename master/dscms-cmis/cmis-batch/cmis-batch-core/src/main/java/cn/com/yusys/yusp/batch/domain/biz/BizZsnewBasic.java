/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.biz;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BizZsnewBasic
 * @类描述: biz_zsnew_basic数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "biz_zsnew_basic")
public class BizZsnewBasic extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "APP_NO")
	private String appNo;
	
	/** 顺序号 **/
	@Id
	@Column(name = "SEQ_NUM")
	private String seqNum;
	
	/** 贷后流水号 **/
	@Column(name = "PSP_SERNO", unique = false, nullable = true, length = 40)
	private String pspSerno;
	
	/** 统一信用代码 **/
	@Column(name = "CREDITCODE", unique = false, nullable = true, length = 40)
	private String creditcode;
	
	/** 注销日期 **/
	@Column(name = "CANDATE", unique = false, nullable = true, length = 10)
	private String candate;
	
	/** 最后年检年度 **/
	@Column(name = "ANCHEYEAR", unique = false, nullable = true, length = 4)
	private String ancheyear;
	
	/** 住址 **/
	@Column(name = "DOM", unique = false, nullable = true, length = 100)
	private String dom;
	
	/** 经营状态 **/
	@Column(name = "ENTSTATUS", unique = false, nullable = true, length = 40)
	private String entstatus;
	
	/** 企业英文名称 **/
	@Column(name = "ENTNAMEENG", unique = false, nullable = true, length = 80)
	private String entnameeng;
	
	/** 企业名称 **/
	@Column(name = "ENTNAME", unique = false, nullable = true, length = 80)
	private String entname;
	
	/** 个人标识码 **/
	@Column(name = "PALGORITHMID", unique = false, nullable = true, length = 200)
	private String palgorithmid;
	
	/** 核准日期 **/
	@Column(name = "APPRDATE", unique = false, nullable = true, length = 10)
	private String apprdate;
	
	/** 企业(机构)类型编码 **/
	@Column(name = "ENTTYPECODE", unique = false, nullable = true, length = 20)
	private String enttypecode;
	
	/** 曾用名 **/
	@Column(name = "ENTNAME_OLD", unique = false, nullable = true, length = 80)
	private String entnameOld;
	
	/** 许可经营项目 **/
	@Column(name = "ABUITEM", unique = false, nullable = true, length = 400)
	private String abuitem;
	
	/** 国际经济代码名称 **/
	@Column(name = "INDUSTRYCONAME", unique = false, nullable = true, length = 80)
	private String industryconame;
	
	/** 国际经济代行业代码 **/
	@Column(name = "INDUSTRYCOCODE", unique = false, nullable = true, length = 80)
	private String industrycocode;
	
	/** 注册号 **/
	@Column(name = "REGNO", unique = false, nullable = true, length = 40)
	private String regno;
	
	/** 注册资本币种 **/
	@Column(name = "REGCAPCUR", unique = false, nullable = true, length = 10)
	private String regcapcur;
	
	/** 注册资本（万元） **/
	@Column(name = "REGCAP", unique = false, nullable = true, length = 40)
	private String regcap;
	
	/** 登记机关 **/
	@Column(name = "REGORG", unique = false, nullable = true, length = 40)
	private String regorg;
	
	/** 经营业务范围 **/
	@Column(name = "ZSOPSCOPE", unique = false, nullable = true, length = 1000)
	private String zsopscope;
	
	/** 吊销日期 **/
	@Column(name = "REVDATE", unique = false, nullable = true, length = 10)
	private String revdate;
	
	/** 注册地址行政编号 **/
	@Column(name = "REGORGCODE", unique = false, nullable = true, length = 40)
	private String regorgcode;
	
	/** 实收资本(万元) **/
	@Column(name = "RECCAP", unique = false, nullable = true, length = 40)
	private String reccap;
	
	/** 法定代表人/负责人/执行事务合伙人 **/
	@Column(name = "FRNAME", unique = false, nullable = true, length = 40)
	private String frname;
	
	/** 成立日期 **/
	@Column(name = "ESDATE", unique = false, nullable = true, length = 10)
	private String esdate;
	
	/** 企业类型 **/
	@Column(name = "ENTTYPE", unique = false, nullable = true, length = 100)
	private String enttype;
	
	/** 经营期限自 **/
	@Column(name = "OPFROM", unique = false, nullable = true, length = 20)
	private String opfrom;
	
	/** 原注册号 **/
	@Column(name = "ORIREGNO", unique = false, nullable = true, length = 40)
	private String oriregno;
	
	/** 组织机构代码 **/
	@Column(name = "ORGCODES", unique = false, nullable = true, length = 80)
	private String orgcodes;
	
	/** 经营期限至 **/
	@Column(name = "OPTO", unique = false, nullable = true, length = 20)
	private String opto;
	
	/** 录入日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "LAST_UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	
	/**
	 * @param appNo
	 */
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	
    /**
     * @return appNo
     */
	public String getAppNo() {
		return this.appNo;
	}
	
	/**
	 * @param pspSerno
	 */
	public void setPspSerno(String pspSerno) {
		this.pspSerno = pspSerno;
	}
	
    /**
     * @return pspSerno
     */
	public String getPspSerno() {
		return this.pspSerno;
	}
	
	/**
	 * @param seqNum
	 */
	public void setSeqNum(String seqNum) {
		this.seqNum = seqNum;
	}
	
    /**
     * @return seqNum
     */
	public String getSeqNum() {
		return this.seqNum;
	}
	
	/**
	 * @param creditcode
	 */
	public void setCreditcode(String creditcode) {
		this.creditcode = creditcode;
	}
	
    /**
     * @return creditcode
     */
	public String getCreditcode() {
		return this.creditcode;
	}
	
	/**
	 * @param candate
	 */
	public void setCandate(String candate) {
		this.candate = candate;
	}
	
    /**
     * @return candate
     */
	public String getCandate() {
		return this.candate;
	}
	
	/**
	 * @param ancheyear
	 */
	public void setAncheyear(String ancheyear) {
		this.ancheyear = ancheyear;
	}
	
    /**
     * @return ancheyear
     */
	public String getAncheyear() {
		return this.ancheyear;
	}
	
	/**
	 * @param dom
	 */
	public void setDom(String dom) {
		this.dom = dom;
	}
	
    /**
     * @return dom
     */
	public String getDom() {
		return this.dom;
	}
	
	/**
	 * @param entstatus
	 */
	public void setEntstatus(String entstatus) {
		this.entstatus = entstatus;
	}
	
    /**
     * @return entstatus
     */
	public String getEntstatus() {
		return this.entstatus;
	}
	
	/**
	 * @param entnameeng
	 */
	public void setEntnameeng(String entnameeng) {
		this.entnameeng = entnameeng;
	}
	
    /**
     * @return entnameeng
     */
	public String getEntnameeng() {
		return this.entnameeng;
	}
	
	/**
	 * @param entname
	 */
	public void setEntname(String entname) {
		this.entname = entname;
	}
	
    /**
     * @return entname
     */
	public String getEntname() {
		return this.entname;
	}
	
	/**
	 * @param palgorithmid
	 */
	public void setPalgorithmid(String palgorithmid) {
		this.palgorithmid = palgorithmid;
	}
	
    /**
     * @return palgorithmid
     */
	public String getPalgorithmid() {
		return this.palgorithmid;
	}
	
	/**
	 * @param apprdate
	 */
	public void setApprdate(String apprdate) {
		this.apprdate = apprdate;
	}
	
    /**
     * @return apprdate
     */
	public String getApprdate() {
		return this.apprdate;
	}
	
	/**
	 * @param enttypecode
	 */
	public void setEnttypecode(String enttypecode) {
		this.enttypecode = enttypecode;
	}
	
    /**
     * @return enttypecode
     */
	public String getEnttypecode() {
		return this.enttypecode;
	}
	
	/**
	 * @param entnameOld
	 */
	public void setEntnameOld(String entnameOld) {
		this.entnameOld = entnameOld;
	}
	
    /**
     * @return entnameOld
     */
	public String getEntnameOld() {
		return this.entnameOld;
	}
	
	/**
	 * @param abuitem
	 */
	public void setAbuitem(String abuitem) {
		this.abuitem = abuitem;
	}
	
    /**
     * @return abuitem
     */
	public String getAbuitem() {
		return this.abuitem;
	}
	
	/**
	 * @param industryconame
	 */
	public void setIndustryconame(String industryconame) {
		this.industryconame = industryconame;
	}
	
    /**
     * @return industryconame
     */
	public String getIndustryconame() {
		return this.industryconame;
	}
	
	/**
	 * @param industrycocode
	 */
	public void setIndustrycocode(String industrycocode) {
		this.industrycocode = industrycocode;
	}
	
    /**
     * @return industrycocode
     */
	public String getIndustrycocode() {
		return this.industrycocode;
	}
	
	/**
	 * @param regno
	 */
	public void setRegno(String regno) {
		this.regno = regno;
	}
	
    /**
     * @return regno
     */
	public String getRegno() {
		return this.regno;
	}
	
	/**
	 * @param regcapcur
	 */
	public void setRegcapcur(String regcapcur) {
		this.regcapcur = regcapcur;
	}
	
    /**
     * @return regcapcur
     */
	public String getRegcapcur() {
		return this.regcapcur;
	}
	
	/**
	 * @param regcap
	 */
	public void setRegcap(String regcap) {
		this.regcap = regcap;
	}
	
    /**
     * @return regcap
     */
	public String getRegcap() {
		return this.regcap;
	}
	
	/**
	 * @param regorg
	 */
	public void setRegorg(String regorg) {
		this.regorg = regorg;
	}
	
    /**
     * @return regorg
     */
	public String getRegorg() {
		return this.regorg;
	}
	
	/**
	 * @param zsopscope
	 */
	public void setZsopscope(String zsopscope) {
		this.zsopscope = zsopscope;
	}
	
    /**
     * @return zsopscope
     */
	public String getZsopscope() {
		return this.zsopscope;
	}
	
	/**
	 * @param revdate
	 */
	public void setRevdate(String revdate) {
		this.revdate = revdate;
	}
	
    /**
     * @return revdate
     */
	public String getRevdate() {
		return this.revdate;
	}
	
	/**
	 * @param regorgcode
	 */
	public void setRegorgcode(String regorgcode) {
		this.regorgcode = regorgcode;
	}
	
    /**
     * @return regorgcode
     */
	public String getRegorgcode() {
		return this.regorgcode;
	}
	
	/**
	 * @param reccap
	 */
	public void setReccap(String reccap) {
		this.reccap = reccap;
	}
	
    /**
     * @return reccap
     */
	public String getReccap() {
		return this.reccap;
	}
	
	/**
	 * @param frname
	 */
	public void setFrname(String frname) {
		this.frname = frname;
	}
	
    /**
     * @return frname
     */
	public String getFrname() {
		return this.frname;
	}
	
	/**
	 * @param esdate
	 */
	public void setEsdate(String esdate) {
		this.esdate = esdate;
	}
	
    /**
     * @return esdate
     */
	public String getEsdate() {
		return this.esdate;
	}
	
	/**
	 * @param enttype
	 */
	public void setEnttype(String enttype) {
		this.enttype = enttype;
	}
	
    /**
     * @return enttype
     */
	public String getEnttype() {
		return this.enttype;
	}
	
	/**
	 * @param opfrom
	 */
	public void setOpfrom(String opfrom) {
		this.opfrom = opfrom;
	}
	
    /**
     * @return opfrom
     */
	public String getOpfrom() {
		return this.opfrom;
	}
	
	/**
	 * @param oriregno
	 */
	public void setOriregno(String oriregno) {
		this.oriregno = oriregno;
	}
	
    /**
     * @return oriregno
     */
	public String getOriregno() {
		return this.oriregno;
	}
	
	/**
	 * @param orgcodes
	 */
	public void setOrgcodes(String orgcodes) {
		this.orgcodes = orgcodes;
	}
	
    /**
     * @return orgcodes
     */
	public String getOrgcodes() {
		return this.orgcodes;
	}
	
	/**
	 * @param opto
	 */
	public void setOpto(String opto) {
		this.opto = opto;
	}
	
    /**
     * @return opto
     */
	public String getOpto() {
		return this.opto;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}


}