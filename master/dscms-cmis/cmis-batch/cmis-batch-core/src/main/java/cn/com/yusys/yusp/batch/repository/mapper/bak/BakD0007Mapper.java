package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKD0007</br>
 * 任务名称：批前备份日表任务-备份开证台账[ACC_TF_LOC]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakD0007Mapper {

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertCurrent(@Param("openDay") String openDay);

    /**
     * 删除当天的数据
     *
     * @return
     */
    void truncateCurrent();
    
    /**
     * 删除当天的数据
     *
     * @param openDay
     * @return
     */
    int deleteCurrent(@Param("openDay") String openDay);

    /**
     * 查询备份表[开证台账[ACC_TF_LOC]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryD0007DeleteOpenDayAccTfLocCounts(@Param("openDay") String openDay);


    /**
     * 查询原表[委托贷款台账[ACC_ENTRUST_LOAN]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryAccTfLocCounts(@Param("openDay") String openDay);
}
