package cn.com.yusys.yusp.batch.util;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.domain.bat.BatTransferCfg;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

/**
 * 文件处理工具类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public class FileUtils {
    private static final Logger logger = LoggerFactory.getLogger(FtpUtils.class);

    /**
     * 获取文件中行数
     *
     * @param filePath 文件完整路径
     * @return
     * @throws IOException
     */
    public static Long getFilesLines(String filePath) throws IOException {
        logger.info("获取文件完整路径为:[{}]中行数开始", filePath);
        Long filesLines = Files.lines(Paths.get(filePath)).count();
        logger.info("获取文件完整路径为:[{}]中行数结束,行数为:[{}]", filePath, filesLines);
        return filesLines;
    }

    /**
     * 根据文件名称、日期类型、任务日期组装运行文件（将文件名中的YYYYMMDD替换成文件日期）
     *
     * @param fileName 文件名称
     * @param dateType 日期类型
     * @param taskDate 任务日期
     * @return
     */
    public static String bulidRunFileName(String fileName, String dateType, String taskDate) {
        String runFileName = null;
        logger.info("根据文件名称、日期类型、任务日期组装运行文件（将文件名中的YYYY-MM-DD替换成文件日期）开始");
        if (fileName.endsWith("*")) {
            logger.info("此文件名为" + fileName + ",以*结尾表明此文件名是一个前缀");
            return fileName;
        }
        String fileDate = DateUtil.countRelativeDate(dateType, taskDate);
        Date fileDateYmdDate = DateUtils.parseDate(fileDate, "yyyy-MM-dd");
        String fileDateYmd = DateUtils.formatDate(fileDateYmdDate, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd
        runFileName = fileName.replaceAll("YYYY-MM-DD", fileDate).replaceAll("YYYYMMDD", fileDateYmd);
        logger.info("根据文件名称、日期类型、任务日期组装运行文件（将文件名中的YYYY-MM-DD替换成文件日期）结束，运行文件为：[" + runFileName + "]");
        return runFileName;
    }

    /**
     * 组装文件工作目录
     *
     * @param workDir  文件工作目录
     * @param fileDate 文件日期
     * @return
     * @throws Exception
     */
    public static String bulidWorkFolder(String workDir, String fileDate) throws Exception {
        logger.info("组装文件工作目录开始");
        workDir = workDir.replaceAll("YYYYMMDD", fileDate.replaceAll("-", "")).replaceAll("YYYY-MM-DD", fileDate);
        logger.info("组装文件工作目录结束，文件工作目录为：[" + workDir + "]");
        return workDir;
    }

    /**
     * 组装文件本地目录
     *
     * @param localDir 文件本地目录
     * @param fileDate 文件日期
     * @return
     * @throws Exception
     */
    public static String bulidLocalFolder(String localDir, String fileDate) throws Exception {
        logger.info("组装文件本地目录开始");
        localDir = localDir.replaceAll("YYYYMMDD", fileDate.replaceAll("-", "")).replaceAll("YYYY-MM-DD", fileDate);
        logger.info("组装文件本地目录结束，文件本地目录为：[" + localDir + "]");
        return localDir;
    }

    /**
     * 组装远程FTP服务端目录
     *
     * @param remoteDir 远程FTP服务端目录
     * @param fileDate  文件日期
     * @return
     */
    public static String bulidRemoteDir(String remoteDir, String fileDate) {
        logger.info("组装远程FTP服务端目录开始");
        remoteDir = remoteDir.replaceAll("YYYYMMDD", fileDate.replaceAll("-", "")).replaceAll("YYYY-MM-DD", fileDate);
        logger.info("组装远程FTP服务端目录结束，远程FTP服务端目录为：[" + remoteDir + "]");
        return remoteDir;
    }

    /**
     * 拷贝文件
     *
     * @param targetDir  目标目录
     * @param sourcePath 源路径
     * @param targetPath 目标路径
     * @throws BizException
     */
    public static void copy(String targetDir, String sourcePath, String targetPath) throws BizException {
        logger.info("拷贝文件开始,目标目录为:{},源路径为:{},目标路径为:{}", targetDir, sourcePath, targetPath);
        try {
            File targetFile = new File(targetDir);
            if (!targetFile.exists()) {
                logger.info("创建目标目录{}开始", targetDir);
                targetFile.mkdirs();
                logger.info("创建目标目录{}结束", targetDir);
            }
            cn.com.yusys.yusp.commons.util.io.FileUtils.fileCopy(sourcePath, targetPath);
        } catch (Exception e) {
            logger.error("拷贝文件异常,异常信息为[{}]", e.getMessage());
        }
        logger.info("拷贝文件结束,目标目录为:{},源路径为:{},目标路径为:{}", targetDir, sourcePath, targetPath);
    }

    /**
     * 删除3日前文件
     *
     * @param openDay        营业日期
     * @param batTransferCfg 文件传输配置信息
     * @param batTaskRun     任务运行管理
     * @throws Exception
     */
    public static void delete3DaysBeforeFiles(String openDay, BatTransferCfg batTransferCfg, BatTaskRun batTaskRun) throws Exception {
        logger.info("营业日期为:[" + openDay + "],删除3日前文件开始");
        try {
            String workDir = batTransferCfg.getWorkDir();//工作目录 必填，全路径，需要用文件日期替换路径中的日期
            String localDir = batTransferCfg.getLocalDir();//本地目录 必填，全路径，需要用文件日期替换路径中的日期
            String dataFileName = batTaskRun.getDataFileName();// 数据文件全名
            String deleteWorkFileName = batTaskRun.getBatTTabName().concat(".txt");// 工作处理目录下文件名称
            String beforeDate = DateUtils.addDay(openDay, "yyyy-MM-dd", -3);// 计算三天前文件日期
            Date deleteFileDateYmd = DateUtils.parseDate(beforeDate, "yyyy-MM-dd");
            String deleteFileDate = DateUtils.formatDate(deleteFileDateYmd, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd
            String deleteLocalDir = FileUtils.bulidLocalFolder(localDir, deleteFileDate); // 待删除的本地处理目录
            String deleteWorkDir = FileUtils.bulidWorkFolder(workDir, deleteFileDate);     // 待删除的工作处理目录

            logger.info("删除的本地处理文件开始");
            FileUtils.deleteFile(deleteLocalDir, dataFileName);
            logger.info("删除的本地处理文件结束");

            logger.info("删除的工作处理文件开始");
            FileUtils.deleteFile(deleteWorkDir, deleteWorkFileName);
            logger.info("删除的工作处理文件结束");

        } catch (Exception e) {
            logger.error("营业日期为:[" + openDay + "],删除3日前文件异常,异常信息为:" + e.getMessage());
        }
        logger.info("营业日期为:[" + openDay + "],删除3日前文件结束");

    }

    /**
     * 删除3日前文件夹
     *
     * @param openDay        营业日期
     * @param batTransferCfg 文件传输配置信息
     * @throws Exception
     */
    public static void delete3DaysBeforeDirectory(String openDay, BatTransferCfg batTransferCfg) throws Exception {
        logger.info("营业日期为:[" + openDay + "],删除3日前文件夹开始");
        try {
            String workDir = batTransferCfg.getWorkDir();//工作目录 必填，全路径，需要用文件日期替换路径中的日期
            String localDir = batTransferCfg.getLocalDir();//本地目录 必填，全路径，需要用文件日期替换路径中的日期
            String beforeDate = DateUtils.addDay(openDay, "yyyy-MM-dd", -3);// 计算三天前文件日期
            Date deleteFileDateYmd = DateUtils.parseDate(beforeDate, "yyyy-MM-dd");
            String deleteFileDate = DateUtils.formatDate(deleteFileDateYmd, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd
            String deleteLocalDir = FileUtils.bulidLocalFolder(localDir, deleteFileDate); // 待删除的本地处理目录
            String deleteWorkDir = FileUtils.bulidWorkFolder(workDir, deleteFileDate);     // 待删除的工作处理目录

            File deleteLocalDirectory = new File(deleteLocalDir);
            logger.info("删除的本地处理文件夹开始");
            org.apache.commons.io.FileUtils.deleteDirectory(deleteLocalDirectory);
            logger.info("删除的本地处理文件夹结束");

            File deleteWorkDirectory = new File(deleteWorkDir);
            logger.info("删除的工作处理文件夹开始");
            org.apache.commons.io.FileUtils.deleteDirectory(deleteWorkDirectory);
            logger.info("删除的工作处理文件夹结束");
        } catch (Exception e) {
            logger.error("营业日期为:[" + openDay + "],删除3日前文件夹异常,异常信息为:" + e.getMessage());
        }
        logger.info("营业日期为:[" + openDay + "],删除3日前文件夹结束");

    }

    /**
     * 删除本地目录文件
     *
     * @param fileFolder 文件目录
     * @param fileName   文件名称
     */
    public static void deleteFile(String fileFolder, String fileName) {
        logger.info("删除文件目录[" + fileFolder + "],文件名称[" + fileName + "]开始");
        try {
            if (isFileExists(fileFolder + fileName)) {
                File file = new File(fileFolder + fileName);
                file.delete();
            }
        } catch (Exception e) {
            logger.info("删除文件目录[" + fileFolder + "],文件名称[" + fileName + "]异常,异常信息为:" + e.getMessage());
        }
        logger.info("删除文件目录[" + fileFolder + "],文件名称[" + fileName + "]结束");
    }


    /**
     * 根据文件全路径检查文件是否存在
     *
     * @param fileURL 文件全路径
     * @return
     */
    public static boolean isFileExists(String fileURL) {
        boolean flag = false;
        File file = new File(fileURL);
        flag = file.exists();
        if (!flag) {
            logger.info("文件：[" + fileURL + "]不存在");
        } else {
            logger.info("文件：[" + fileURL + "]存在");
        }
        return flag;
    }

}
