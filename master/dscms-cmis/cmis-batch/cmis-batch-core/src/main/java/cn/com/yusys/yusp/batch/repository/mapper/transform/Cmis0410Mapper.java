package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0410</br>
 * 任务名称：加工任务-贷后管理-定期检查[清理贷后临时表数据] </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月17日 下午9:56:54
 */
public interface Cmis0410Mapper {

    /**
     * 清空监控对公客户信息表
     *
     * @param openDay
     * @return
     */
    int truncateTable01(@Param("openDay") String openDay);
    int truncateTable21(@Param("openDay") String openDay);
    int truncateTable22(@Param("openDay") String openDay);

    int truncateTableTmpCheckDate(Map map);
    int insertTmpCheckDate(Map map);
    int insertTmp(Map map);
   // int insertTmp2(Map map);
}
