/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.transform.cfg;

import cn.com.yusys.yusp.batch.domain.transform.cfg.CfgRiskBigamtParam;
import cn.com.yusys.yusp.batch.service.transform.cfg.CfgRiskBigamtParamService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: CfgRiskBigamtParamResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-22 20:25:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgriskbigamtparam")
public class CfgRiskBigamtParamResource {
    @Autowired
    private CfgRiskBigamtParamService cfgRiskBigamtParamService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgRiskBigamtParam>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgRiskBigamtParam> list = cfgRiskBigamtParamService.selectAll(queryModel);
        return new ResultDto<List<CfgRiskBigamtParam>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgRiskBigamtParam>> index(QueryModel queryModel) {
        List<CfgRiskBigamtParam> list = cfgRiskBigamtParamService.selectByModel(queryModel);
        return new ResultDto<List<CfgRiskBigamtParam>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgRiskBigamtParam> show(@PathVariable("pkId") String pkId) {
        CfgRiskBigamtParam cfgRiskBigamtParam = cfgRiskBigamtParamService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgRiskBigamtParam>(cfgRiskBigamtParam);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgRiskBigamtParam> create(@RequestBody CfgRiskBigamtParam cfgRiskBigamtParam) throws URISyntaxException {
        cfgRiskBigamtParamService.insert(cfgRiskBigamtParam);
        return new ResultDto<CfgRiskBigamtParam>(cfgRiskBigamtParam);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgRiskBigamtParam cfgRiskBigamtParam) throws URISyntaxException {
        int result = cfgRiskBigamtParamService.update(cfgRiskBigamtParam);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgRiskBigamtParamService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgRiskBigamtParamService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
