package cn.com.yusys.yusp.batch.service.server.cmisbatch0002;

import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKlnaDkzhzb;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002Resp;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002RespDto;
import cn.com.yusys.yusp.batch.service.load.core.BatSCoreKlnaDkzhzbService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CmisBatch0002Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0002Service.class);

    @Autowired
    private BatSCoreKlnaDkzhzbService batSCoreKlnaDkzhzbService;//核心系统-历史表-贷款账户主表

    /**
     * 交易码：cmisbatch0002
     * 交易描述：查询[核心系统-历史表-贷款账户主表]信息
     *
     * @param cmisbatch0002ReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Cmisbatch0002RespDto cmisBatch0002(Cmisbatch0002ReqDto cmisbatch0002ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value);
        Cmisbatch0002RespDto cmisbatch0002RespDto = new Cmisbatch0002RespDto();// 响应Dto:调度运行管理信息查詢
        try {
            //请求字段
            String dkjiejuh = cmisbatch0002ReqDto.getDkjiejuh();//贷款借据号
            String kehuhaoo = cmisbatch0002ReqDto.getKehuhaoo();//客户号
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("dkjiejuh", dkjiejuh);//贷款借据号
            queryModel.addCondition("kehuhaoo", kehuhaoo);//客户号
            queryModel.setSort("DATA_DATE desc");
            List<BatSCoreKlnaDkzhzb> batSCoreKlnaDkzhzbs = batSCoreKlnaDkzhzbService.selectByModel(queryModel);
            List<Cmisbatch0002Resp> cmisbatch0002List = new ArrayList<>();
            if (CollectionUtils.nonEmpty(batSCoreKlnaDkzhzbs)) {
                cmisbatch0002List = batSCoreKlnaDkzhzbs.stream().map(batSCoreKlnaDkzhzb -> {
                    Cmisbatch0002Resp cmisbatch0002Resp = new Cmisbatch0002Resp();
                    BeanUtils.copyProperties(batSCoreKlnaDkzhzb, cmisbatch0002Resp);
                    return cmisbatch0002Resp;
                }).collect(Collectors.toList());
            }
            cmisbatch0002RespDto.setCmisbatch0002List(cmisbatch0002List);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value);
        return cmisbatch0002RespDto;
    }
}
