package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0601</br>
 * 任务名称：加工任务-业务处理-生成短信  </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0601Mapper {

    /**
     * 插入短信通知表输入表
     * @param openDay
     * @return
     */
    int insertSmsManageInfo01(@Param("openDay") String openDay);

    /**
     * 插入短信通知表输入表
     * @param openDay
     * @return
     */
    int insertSmsManageInfo02(@Param("openDay") String openDay);

    /**
     * 插入短信通知表输入表
     * @param openDay
     * @return
     */
    int insertSmsManageInfo03(@Param("openDay") String openDay);

    /**
     * 插入短信通知表输入表
     * @param openDay
     * @return
     */
    int insertSmsManageInfo04(@Param("openDay") String openDay);

    /**
     * 插入短信通知表输入表
     * @param openDay
     * @return
     */
    int insertSmsManageInfo05(@Param("openDay") String openDay);

    /**
     * 插入短信通知表输入表
     * @param openDay
     * @return
     */
    int insertSmsManageInfoAccLoan(@Param("openDay") String openDay);

    /**
     * 插入短信通知表输入表
     * @param openDay
     * @return
     */
    int insertSmsManageInfoLmtReplyAcc(@Param("openDay") String openDay);

    /**
     * 个人经营性贷款（不包含小贷）--逾期后一天每月25日至月末每天逾期提示
     * @param openDay
     * @return
     */
    int insertSmsManageInfo08(@Param("openDay") String openDay);

    /**
     * 客户经理消息提醒 MSG_PL_M_0001 贷款到期前30天
     * @param openDay
     * @return
     */
    int insertSmsManageInfo09(@Param("openDay") String openDay);

    /**
     * 客户经理消息提醒 MSG_PL_M_0002 授信批复到期，发送规则是授信批复到期前45天
     * @param openDay
     * @return
     */
    int insertSmsManageInfo10(@Param("openDay") String openDay);

}
