/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.ypp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSYppTInfBuildUse
 * @类描述: bat_s_ypp_t_inf_build_use数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 17:10:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ypp_t_inf_build_use")
public class BatSYppTInfBuildUse extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 押品编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "GUAR_NO")
	private String guarNo;
	
	/** 土地证（不动产权证号） **/
	@Column(name = "LAND_NO", unique = false, nullable = true, length = 300)
	private String landNo;
	
	/** 土地使用权性质 **/
	@Column(name = "LAND_USE_QUAL", unique = false, nullable = true, length = 10)
	private String landUseQual;
	
	/** 土地使用权取得方式 **/
	@Column(name = "LAND_USE_WAY", unique = false, nullable = true, length = 10)
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	@Column(name = "LAND_USE_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	@Column(name = "LAND_USE_END_DATE", unique = false, nullable = true, length = 10)
	private String landUseEndDate;
	
	/** 土地用途 **/
	@Column(name = "LAND_PURP", unique = false, nullable = true, length = 10)
	private String landPurp;
	
	/** 土地使用权面积 **/
	@Column(name = "LAND_USE_AREA", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal landUseArea;
	
	/** 闲置土地类型 **/
	@Column(name = "LAND_NOTINUSE_TYPE", unique = false, nullable = true, length = 10)
	private String landNotinuseType;
	
	/** 土地说明 **/
	@Column(name = "LAND_EXPLAIN", unique = false, nullable = true, length = 4000)
	private String landExplain;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 土地详细地址 **/
	@Column(name = "LAND_DETAILADD", unique = false, nullable = true, length = 750)
	private String landDetailadd;
	
	/** 宗地号 **/
	@Column(name = "PARCEL_NO", unique = false, nullable = true, length = 75)
	private String parcelNo;
	
	/** 购买日期 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 购买价格 **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 是否有地上定着物 **/
	@Column(name = "LAND_UP", unique = false, nullable = true, length = 10)
	private String landUp;
	
	/** 定着物种类 **/
	@Column(name = "LAND_UP_TYPE", unique = false, nullable = true, length = 10)
	private String landUpType;
	
	/** 地上建筑物项数 **/
	@Column(name = "LAND_BUILD_AMOUNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal landBuildAmount;
	
	/** 定着物所有权人名称 **/
	@Column(name = "LAND_UP_OWNERSHIP_NAME", unique = false, nullable = true, length = 600)
	private String landUpOwnershipName;
	
	/** 定着物所有权人范围 **/
	@Column(name = "LAND_UP_OWNERSHIP_SCOPE", unique = false, nullable = true, length = 4000)
	private String landUpOwnershipScope;
	
	/** 地上定着物说明 **/
	@Column(name = "LAND_UP_EXPLAIN", unique = false, nullable = true, length = 4000)
	private String landUpExplain;
	
	/** 地上定着物总面积 **/
	@Column(name = "LAND_UP_ALL_AREA", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal landUpAllArea;
	
	/** 使用权抵押登记证号 **/
	@Column(name = "USE_CERT_NO", unique = false, nullable = true, length = 450)
	private String useCertNo;
	
	/** 使用权登记机关 **/
	@Column(name = "USE_CERT_DEP", unique = false, nullable = true, length = 450)
	private String useCertDep;
	
	/** 土地所在地段情况 **/
	@Column(name = "LAND_P_INFO", unique = false, nullable = true, length = 20)
	private String landPInfo;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 10)
	private String curType;
	
	/** 押品使用情况 **/
	@Column(name = "T_USAGE", unique = false, nullable = true, length = 10)
	private String tUsage;
	
	/** 备注(限1000个汉字) **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 1000)
	private String remark;
	
	/** 土地使用年限(年) **/
	@Column(name = "LAND_USE_YEAR", unique = false, nullable = true, length = 4)
	private java.math.BigDecimal landUseYear;
	
	/** 剩余使用年限 **/
	@Column(name = "LAND_REMAINDER_YEAR", unique = false, nullable = true, length = 10)
	private String landRemainderYear;
	
	/** 街道/村镇/路名 **/
	@Column(name = "STREET", unique = false, nullable = true, length = 100)
	private String street;
	
	/** 所属地理位置 **/
	@Column(name = "LOCATED_POSITION", unique = false, nullable = true, length = 10)
	private String locatedPosition;
	
	/** 所属地段 **/
	@Column(name = "AREA_LOCATION", unique = false, nullable = true, length = 10)
	private String areaLocation;
	
	/** 担保分类代码 **/
	@Column(name = "GUAR_TYPE_CD", unique = false, nullable = true, length = 9)
	private String guarTypeCd;
	
	/** 土地用途（慧押押） **/
	@Column(name = "LAND_USAGE", unique = false, nullable = true, length = 9)
	private String landUsage;
	
	/** 实时查封状态 **/
	@Column(name = "SS_CF_STATUS", unique = false, nullable = true, length = 5)
	private String ssCfStatus;
	
	/** 查封时间 **/
	@Column(name = "CF_DATE", unique = false, nullable = true, length = 10)
	private String cfDate;
	
	/** 最高可抵押顺位 **/
	@Column(name = "MAX_YP_STATUS", unique = false, nullable = true, length = 5)
	private String maxYpStatus;
	
	/** 房屋用途（在线抵押使用） **/
	@Column(name = "HOUSE_USAGE", unique = false, nullable = true, length = 9)
	private String houseUsage;
	
	/** 不动产单元号 **/
	@Column(name = "BDCDYH_NO", unique = false, nullable = true, length = 100)
	private String bdcdyhNo;
	
	/** 状态 **/
	@Column(name = "INSPECT_STATUS", unique = false, nullable = true, length = 2)
	private String inspectStatus;
	
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo;
	}
	
    /**
     * @return landNo
     */
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual;
	}
	
    /**
     * @return landUseQual
     */
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay;
	}
	
    /**
     * @return landUseWay
     */
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate;
	}
	
    /**
     * @return landUseBeginDate
     */
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate;
	}
	
    /**
     * @return landUseEndDate
     */
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp;
	}
	
    /**
     * @return landPurp
     */
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landUseArea
	 */
	public void setLandUseArea(java.math.BigDecimal landUseArea) {
		this.landUseArea = landUseArea;
	}
	
    /**
     * @return landUseArea
     */
	public java.math.BigDecimal getLandUseArea() {
		return this.landUseArea;
	}
	
	/**
	 * @param landNotinuseType
	 */
	public void setLandNotinuseType(String landNotinuseType) {
		this.landNotinuseType = landNotinuseType;
	}
	
    /**
     * @return landNotinuseType
     */
	public String getLandNotinuseType() {
		return this.landNotinuseType;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain;
	}
	
    /**
     * @return landExplain
     */
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param landDetailadd
	 */
	public void setLandDetailadd(String landDetailadd) {
		this.landDetailadd = landDetailadd;
	}
	
    /**
     * @return landDetailadd
     */
	public String getLandDetailadd() {
		return this.landDetailadd;
	}
	
	/**
	 * @param parcelNo
	 */
	public void setParcelNo(String parcelNo) {
		this.parcelNo = parcelNo;
	}
	
    /**
     * @return parcelNo
     */
	public String getParcelNo() {
		return this.parcelNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param landUp
	 */
	public void setLandUp(String landUp) {
		this.landUp = landUp;
	}
	
    /**
     * @return landUp
     */
	public String getLandUp() {
		return this.landUp;
	}
	
	/**
	 * @param landUpType
	 */
	public void setLandUpType(String landUpType) {
		this.landUpType = landUpType;
	}
	
    /**
     * @return landUpType
     */
	public String getLandUpType() {
		return this.landUpType;
	}
	
	/**
	 * @param landBuildAmount
	 */
	public void setLandBuildAmount(java.math.BigDecimal landBuildAmount) {
		this.landBuildAmount = landBuildAmount;
	}
	
    /**
     * @return landBuildAmount
     */
	public java.math.BigDecimal getLandBuildAmount() {
		return this.landBuildAmount;
	}
	
	/**
	 * @param landUpOwnershipName
	 */
	public void setLandUpOwnershipName(String landUpOwnershipName) {
		this.landUpOwnershipName = landUpOwnershipName;
	}
	
    /**
     * @return landUpOwnershipName
     */
	public String getLandUpOwnershipName() {
		return this.landUpOwnershipName;
	}
	
	/**
	 * @param landUpOwnershipScope
	 */
	public void setLandUpOwnershipScope(String landUpOwnershipScope) {
		this.landUpOwnershipScope = landUpOwnershipScope;
	}
	
    /**
     * @return landUpOwnershipScope
     */
	public String getLandUpOwnershipScope() {
		return this.landUpOwnershipScope;
	}
	
	/**
	 * @param landUpExplain
	 */
	public void setLandUpExplain(String landUpExplain) {
		this.landUpExplain = landUpExplain;
	}
	
    /**
     * @return landUpExplain
     */
	public String getLandUpExplain() {
		return this.landUpExplain;
	}
	
	/**
	 * @param landUpAllArea
	 */
	public void setLandUpAllArea(java.math.BigDecimal landUpAllArea) {
		this.landUpAllArea = landUpAllArea;
	}
	
    /**
     * @return landUpAllArea
     */
	public java.math.BigDecimal getLandUpAllArea() {
		return this.landUpAllArea;
	}
	
	/**
	 * @param useCertNo
	 */
	public void setUseCertNo(String useCertNo) {
		this.useCertNo = useCertNo;
	}
	
    /**
     * @return useCertNo
     */
	public String getUseCertNo() {
		return this.useCertNo;
	}
	
	/**
	 * @param useCertDep
	 */
	public void setUseCertDep(String useCertDep) {
		this.useCertDep = useCertDep;
	}
	
    /**
     * @return useCertDep
     */
	public String getUseCertDep() {
		return this.useCertDep;
	}
	
	/**
	 * @param landPInfo
	 */
	public void setLandPInfo(String landPInfo) {
		this.landPInfo = landPInfo;
	}
	
    /**
     * @return landPInfo
     */
	public String getLandPInfo() {
		return this.landPInfo;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param tUsage
	 */
	public void setTUsage(String tUsage) {
		this.tUsage = tUsage;
	}
	
    /**
     * @return tUsage
     */
	public String getTUsage() {
		return this.tUsage;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param landUseYear
	 */
	public void setLandUseYear(java.math.BigDecimal landUseYear) {
		this.landUseYear = landUseYear;
	}
	
    /**
     * @return landUseYear
     */
	public java.math.BigDecimal getLandUseYear() {
		return this.landUseYear;
	}
	
	/**
	 * @param landRemainderYear
	 */
	public void setLandRemainderYear(String landRemainderYear) {
		this.landRemainderYear = landRemainderYear;
	}
	
    /**
     * @return landRemainderYear
     */
	public String getLandRemainderYear() {
		return this.landRemainderYear;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param locatedPosition
	 */
	public void setLocatedPosition(String locatedPosition) {
		this.locatedPosition = locatedPosition;
	}
	
    /**
     * @return locatedPosition
     */
	public String getLocatedPosition() {
		return this.locatedPosition;
	}
	
	/**
	 * @param areaLocation
	 */
	public void setAreaLocation(String areaLocation) {
		this.areaLocation = areaLocation;
	}
	
    /**
     * @return areaLocation
     */
	public String getAreaLocation() {
		return this.areaLocation;
	}
	
	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}
	
    /**
     * @return guarTypeCd
     */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}
	
	/**
	 * @param landUsage
	 */
	public void setLandUsage(String landUsage) {
		this.landUsage = landUsage;
	}
	
    /**
     * @return landUsage
     */
	public String getLandUsage() {
		return this.landUsage;
	}
	
	/**
	 * @param ssCfStatus
	 */
	public void setSsCfStatus(String ssCfStatus) {
		this.ssCfStatus = ssCfStatus;
	}
	
    /**
     * @return ssCfStatus
     */
	public String getSsCfStatus() {
		return this.ssCfStatus;
	}
	
	/**
	 * @param cfDate
	 */
	public void setCfDate(String cfDate) {
		this.cfDate = cfDate;
	}
	
    /**
     * @return cfDate
     */
	public String getCfDate() {
		return this.cfDate;
	}
	
	/**
	 * @param maxYpStatus
	 */
	public void setMaxYpStatus(String maxYpStatus) {
		this.maxYpStatus = maxYpStatus;
	}
	
    /**
     * @return maxYpStatus
     */
	public String getMaxYpStatus() {
		return this.maxYpStatus;
	}
	
	/**
	 * @param houseUsage
	 */
	public void setHouseUsage(String houseUsage) {
		this.houseUsage = houseUsage;
	}
	
    /**
     * @return houseUsage
     */
	public String getHouseUsage() {
		return this.houseUsage;
	}
	
	/**
	 * @param bdcdyhNo
	 */
	public void setBdcdyhNo(String bdcdyhNo) {
		this.bdcdyhNo = bdcdyhNo;
	}
	
    /**
     * @return bdcdyhNo
     */
	public String getBdcdyhNo() {
		return this.bdcdyhNo;
	}
	
	/**
	 * @param inspectStatus
	 */
	public void setInspectStatus(String inspectStatus) {
		this.inspectStatus = inspectStatus;
	}
	
    /**
     * @return inspectStatus
     */
	public String getInspectStatus() {
		return this.inspectStatus;
	}


}