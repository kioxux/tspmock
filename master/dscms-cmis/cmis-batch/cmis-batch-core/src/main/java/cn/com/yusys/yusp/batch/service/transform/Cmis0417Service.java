package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0417Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0417</br>
 * 任务名称：加工任务-贷后管理-定期检查子表数据生成 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
public class Cmis0417Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0417Service.class);

    @Resource
    private Cmis0417Mapper cmis0417Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * 插入贷后检查结果表
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspCheckRst(Map map) {

         if( !( map.containsKey("taskNo") && map.get("taskNo")!=null ) ) {//日间手工发起的不进来


             logger.info(" 删除临时表 开始,请求参数为:[{}]", JSON.toJSONString(map));
             int deletetmpupdatedata4manage = cmis0417Mapper.deletetmpupdatedata4manage(map);
             logger.info(" 删除临时表 结束,返回参数为:[{}]", deletetmpupdatedata4manage);


             logger.info("插入临时表 获取任务中最新的责任人 开始,请求参数为:[{}]", JSON.toJSONString(map));
             int insertTmpUpdateData4 = cmis0417Mapper.insertTmpUpdateData4(map);
             logger.info(" 插入临时表 获取任务中最新的责任人 结束,返回参数为:[{}]", insertTmpUpdateData4);

             logger.info("更新任务表   任务中最新的责任人 开始,请求参数为:[{}]", JSON.toJSONString(map));
             int updatepsptasklistmanagerid = cmis0417Mapper.updatepsptasklistmanagerid(map);
             logger.info(" 更新任务表   任务中最新的责任人 结束,返回参数为:[{}]", updatepsptasklistmanagerid);
         }


        logger.info("插入贷后检查结果表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspCheckRst = cmis0417Mapper.insertPspCheckRst(map);
        logger.info("插入贷后检查结果表结束,返回参数为:[{}]", insertPspCheckRst);
    }

    /**
     * 插入定期检查借据信息
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspDebitInfo(Map map) {
        logger.info("插入首次检查借据信息开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspDebitInfo01 = cmis0417Mapper.insertPspDebitInfo01(map);
        logger.info("插入首次检查借据信息结束,返回参数为:[{}]", insertPspDebitInfo01);

        logger.info("插入定期检查借据信息开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspDebitInfo02 = cmis0417Mapper.insertPspDebitInfo02(map);
        logger.info("插入定期检查借据信息结束,返回参数为:[{}]", insertPspDebitInfo02);

        logger.info("插入委托贷款贷后检查借据信息开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertpspDebitInfowtdk = cmis0417Mapper.insertpspDebitInfowtdk(map);
        logger.info("插入委托贷款贷后检查借据信息结束,返回参数为:[{}]", insertpspDebitInfowtdk);

        logger.info("插入贷后检查银承借据开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertpspDebitInfoyc = cmis0417Mapper.insertpspDebitInfoyc(map);
        logger.info("插入贷后检查银承借据结束,返回参数为:[{}]", insertpspDebitInfoyc);

        logger.info("插入 贷后检查保函借据  开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertpspDebitInfobh = cmis0417Mapper.insertpspDebitInfobh(map);
        logger.info("插入 贷后检查保函借据  结束,返回参数为:[{}]", insertpspDebitInfobh);

        logger.info("插入 贷后检查信用证借据  开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertpspDebitInfoxyz = cmis0417Mapper.insertpspDebitInfoxyz(map);
        logger.info("插入 贷后检查信用证借据  结束,返回参数为:[{}]", insertpspDebitInfoxyz);

        logger.info("插入 贷后检查贴现借据  开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertpspDebitInfotx = cmis0417Mapper.insertpspDebitInfotx(map);
        logger.info("插入 贷后检查贴现借据  结束,返回参数为:[{}]", insertpspDebitInfotx);

    }

    /**
     * 插入定期检查保证人检查
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspGuarntrList(Map map) {
        logger.info("插入定期检查保证人检查开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspGuarntrList = cmis0417Mapper.insertPspGuarntrList(map);
        logger.info("插入定期检查保证人检查结束,返回参数为:[{}]", insertPspGuarntrList);
    }

    /**
     * 插入定期检查财务状况检查
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspFinSituCheck(Map map) {
        logger.info("清理客户最新财报时间开始,请求参数为:[{}]", JSON.toJSONString(map));
        int truncatePspFin = 0;
        if (null != map.get("cusId") && !"".equals(map.get("cusId"))) {
            truncatePspFin = cmis0417Mapper.truncatePspFin2(map);
        } else {
            truncatePspFin = cmis0417Mapper.truncatePspFin(map);
        }
        logger.info("清理客户最新财报时间结束,返回参数为:[{}]", truncatePspFin);
        logger.info("插入客户最新财报时间开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspFin = cmis0417Mapper.insertPspFin(map);
        logger.info("插入客户最新财报时间结束,返回参数为:[{}]", insertPspFin);

        logger.info("插入本期、上一年、上两年的损益表数据开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspFinSituCheck01 = cmis0417Mapper.insertPspFinSituCheck01(map);
        logger.info("插入本期、上一年、上两年的损益表数据结束,返回参数为:[{}]", insertPspFinSituCheck01);

        logger.info("插入本期、上一年、上两年的资产负债表数据 fnc_stat_bs  定期检查财务状况检查开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspFinSituCheck02 = cmis0417Mapper.insertPspFinSituCheck02(map);
        logger.info("插入本期、上一年、上两年的资产负债表数据 fnc_stat_bs  定期检查财务状况检查结束,返回参数为:[{}]", insertPspFinSituCheck02);

//        logger.info("插入本期、上一年、上两年的财报现金流量表数据开始,请求参数为:[{}]", JSON.toJSONString(map));
//        int insertPspFinSituCheck03 = cmis0417Mapper.insertPspFinSituCheck03(map);
//        logger.info("插入本期、上一年、上两年的财报现金流量表数据结束,返回参数为:[{}]", insertPspFinSituCheck03);

        logger.info("插入本期、上一年、上两年的 财报财务指标表数据开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspFinSituCheck04 = cmis0417Mapper.insertPspFinSituCheck04(map);
        logger.info("插入本期、上一年、上两年的 财报财务指标表数据结束,返回参数为:[{}]", insertPspFinSituCheck04);

    }

    /**
     * 插入定期检查财务状况检查
     *
     * @param map
     */
    @Transactional
    public void cmis0417UpdatePspFinSituCheck(Map map) {
        logger.info("插入定期检查财务状况检查开始,请求参数为:[{}]", JSON.toJSONString(map));
        int updatePspFinSituCheck = cmis0417Mapper.updatePspFinSituCheck(map);
        logger.info("插入定期检查财务状况检查结束,返回参数为:[{}]", updatePspFinSituCheck);
    }

    /**
     * 插入定期检查贷后管理建议落实情况
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspImplementOpt(Map map) {
        logger.info("插入定期检查贷后管理建议落实情况开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspImplementOpt = cmis0417Mapper.insertPspImplementOpt(map);
        logger.info("插入定期检查贷后管理建议落实情况结束,返回参数为:[{}]", insertPspImplementOpt);
    }


    /**
     * 插入定期检查抵质押物检查
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspPldimnList(Map map) {
        logger.info("插入定期检查贷后管理建议落实情况开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspPldimnList = cmis0417Mapper.insertPspPldimnList(map);
        logger.info("插入定期检查贷后管理建议落实情况结束,返回参数为:[{}]", insertPspPldimnList);
    }

    /**
     * 插入定期检查经营状况检查（通用）
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspOperStatusCheck(Map map) {
        logger.info("插入定期检查经营状况检查（通用）开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspOperStatusCheck = cmis0417Mapper.insertPspOperStatusCheck(map);
        logger.info("插入定期检查经营状况检查（通用）结束,返回参数为:[{}]", insertPspOperStatusCheck);
    }

    /**
     * 插入定期检查融资情况分析
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspCusFinAnaly(Map map) {
        logger.info("插入定期检查融资情况分析开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspCusFinAnaly = cmis0417Mapper.insertPspCusFinAnaly(map);
        logger.info("插入定期检查融资情况分析结束,返回参数为:[{}]", insertPspCusFinAnaly);
    }

    /**
     * 插入定期检查预警信号表
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspWarningInfoList(Map map) {
        logger.info("插入定期检查预警信号表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspWarningInfoList = cmis0417Mapper.insertPspWarningInfoList(map);
        logger.info("插入定期检查预警信号表 结束,返回参数为:[{}]", insertPspWarningInfoList);
    }

    /**
     * 插入定期检查正常类本行融资情况
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspBankFinSitu(Map map) {
        logger.info("插入定期检查正常类本行融资情况开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspBankFinSitu = cmis0417Mapper.insertPspBankFinSitu(map);
        logger.info("插入定期检查正常类本行融资情况结束,返回参数为:[{}]", insertPspBankFinSitu);
    }

    /**
     * 插入定期检查正常类对外担保分析
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspGuarCheck(Map map) {
        logger.info("插入定期检查正常类本行融资情况开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspGuarCheck = cmis0417Mapper.insertPspGuarCheck(map);
        logger.info("插入定期检查正常类本行融资情况结束,返回参数为:[{}]", insertPspGuarCheck);
    }

    /**
     * 插入对公定期检查不良类关联企业表
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspRelCorp(Map map) {
        logger.info("插入对公定期检查不良类关联企业表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspRelCorp = cmis0417Mapper.insertPspRelCorp(map);
        logger.info("插入对公定期检查不良类关联企业表结束,返回参数为:[{}]", insertPspRelCorp);
//        logger.info(" 不良类关联企业表 删除正常分类数据开始,请求参数为:[{}]", JSON.toJSONString(map));
//        int deletepspRelCorp = cmis0417Mapper.deletepspRelCorp(map);
//        logger.info("不良类关联企业表 删除正常分类数据结束,返回参数为:[{}]", deletepspRelCorp);
    }

    /**
     * 插入对公定期检查经营性物业贷款检查
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspOperStatusCheckProperty(Map map) {
        logger.info("插入对公定期检查经营性物业贷款检查开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspOperStatusCheckProperty = cmis0417Mapper.insertPspOperStatusCheckProperty(map);
        logger.info("插入对公定期检查经营性物业贷款检查结束,返回参数为:[{}]", insertPspOperStatusCheckProperty);
    }

    /**
     * 插入对公定期检查经营状况检查（房地产开发贷）
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspOperStatusCheckRealpro(Map map) {
        logger.info("插入对公定期检查经营状况检查（房地产开发贷）开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspOperStatusCheckRealpro = cmis0417Mapper.insertPspOperStatusCheckRealpro(map);
        logger.info("插入对公定期检查经营状况检查（房地产开发贷）结束,返回参数为:[{}]", insertPspOperStatusCheckRealpro);
    }

    /**
     * 对公定期检查经营状况检查（建筑业）
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspOperStatusCheckArch(Map map) {
        logger.info("插入对公定期检查经营状况检查（建筑业）开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspOperStatusCheckArch = cmis0417Mapper.insertPspOperStatusCheckArch(map);
        logger.info("插入对公定期检查经营状况检查（建筑业）结束,返回参数为:[{}]", insertPspOperStatusCheckArch);
    }

    /**
     * 插入对公定期检查经营状况检查（制造业）
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspOperStatusCheckManufacture(Map map) {
        logger.info("插入对公定期检查经营状况检查（制造业）开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspOperStatusCheckManufacture = cmis0417Mapper.insertPspOperStatusCheckManufacture(map);
        logger.info("插入对公定期检查经营状况检查（制造业）结束,返回参数为:[{}]", insertPspOperStatusCheckManufacture);
    }

    /**
     * 插入对公、个人客户基本信息分析表
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspCusBaseCase(Map map) {
        String checkType = "";
        if (CollectionUtils.nonEmpty(map)) {
            checkType = (map.get("checkType") + "").toString();
        }

        logger.info("插入对公客户基本信息分析表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspCusBaseCaseDG = cmis0417Mapper.insertPspCusBaseCaseDG(map);
        logger.info("插入对公客户基本信息分析表结束,返回参数为:[{}]", insertPspCusBaseCaseDG);

        logger.info("插入个人客户基本信息分析表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspCusBaseCaseGR = cmis0417Mapper.insertPspCusBaseCaseGR(map);
        logger.info("插入个人客户基本信息分析表结束,返回参数为:[{}]", insertPspCusBaseCaseGR);

        logger.info("更新对公、个人客户基本信息分析表[房地产开发贷检查 ,如果存在则更新 IS_REALPRO 为是]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int updatePspCusBaseCase01 = cmis0417Mapper.updatePspCusBaseCase01(map);
        logger.info("更新对公、个人客户基本信息分析表[房地产开发贷检查 ,如果存在则更新 IS_REALPRO 为是]结束,返回参数为:[{}]", updatePspCusBaseCase01);
        logger.info("更新对公、个人客户基本信息分析表[经营性物业贷检查,如果存在则更新 IS_PROPERTY 为是]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int updatePspCusBaseCase02 = cmis0417Mapper.updatePspCusBaseCase02(map);
        logger.info("更新对公、个人客户基本信息分析表[经营性物业贷检查,如果存在则更新 IS_PROPERTY 为是]结束,返回参数为:[{}]", updatePspCusBaseCase02);
        logger.info("更新对公、个人客户基本信息分析表[IS_FIXED固定资产贷款、项目贷款检查,如果存在则更新 IS_PROPERTY 为是]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int updatePspCusBaseCase03 = cmis0417Mapper.updatePspCusBaseCase03(map);
        logger.info("更新对公、个人客户基本信息分析表[IS_FIXED固定资产贷款、项目贷款检查,如果存在则更新 IS_PROPERTY 为是]结束,返回参数为:[{}]", updatePspCusBaseCase03);

        // 20211030 此处使用truncate会导致timeout,由于该方法供实时接口和批量接口同时使用，调整成每次1万条删除 开始
        //  logger.info("更新对公、个人客户基本信息分析表[清空分类临时表]开始,请求参数为:[{}]", JSON.toJSONString(map));
        //  int updatePspCusBaseCase04A = cmis0417Mapper.updatePspCusBaseCase04A(map);
        //  logger.info("更新对公、个人客户基本信息分析表[清空分类临时表] 结束， 返回参数为:[{}]", updatePspCusBaseCase04A);
        logger.info("查询待删除[客户的分类状态加工表|psp_cus_base_case_risk]数据总次数开始,请求参数为:[{}]", JSON.toJSONString(map));
        int batPcbcrCounts = cmis0417Mapper.batPcbcrCounts(map);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(batPcbcrCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        batPcbcrCounts = times.intValue();
        logger.info("查询待删除[客户的分类状态加工表|psp_cus_base_case_risk]数据总次数结束,返回参数为:[{}]", batPcbcrCounts);
        // 循环删除表的数据
        for (int i = 0; i <= batPcbcrCounts; i++) {
            logger.info("第[" + i + "]次删除[客户的分类状态加工表|psp_cus_base_case_risk]数据开始,请求参数为:[{}]", JSON.toJSONString(map));
            int deleteBatPcbcr = cmis0417Mapper.deleteBatPcbcr(map);
            logger.info("第[" + i + "]次删除[客户的分类状态加工表|psp_cus_base_case_risk]结束,返回参数为:[{}]", deleteBatPcbcr);
        }
        // 20211030 此处使用truncate会导致timeout,由于该方法供实时接口和批量接口同时使用，调整成每次1万条删除 结束

        logger.info("更新对公、个人客户基本信息分析表[加工分类临时表]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int updatePspCusBaseCase04B = cmis0417Mapper.updatePspCusBaseCase04B(map);
        logger.info("更新对公、个人客户基本信息分析表[加工分类临时表]结束,返回参数为:[{}]", updatePspCusBaseCase04B);

        int truncatePspCusBase = 0;
        logger.info("清理[更新检查中证件号，手机号临时表]开始,请求参数为:[{}]", JSON.toJSONString(map));
        if ("".equals(map.get("taskNo"))) {
            truncatePspCusBase = cmis0417Mapper.truncatePspCusBase(map);
        } else {
            truncatePspCusBase = cmis0417Mapper.truncatePspCusBase2(map);
        }
        logger.info("清理[更新检查中证件号，手机号临时表]结束,返回参数为:[{}]", truncatePspCusBase);

        logger.info("加工清理[更新检查中证件号，手机号临时表]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspCusBase = cmis0417Mapper.insertPspCusBase(map);
        logger.info("加工清理[更新检查中证件号，手机号临时表]结束,返回参数为:[{}]", insertPspCusBase);
        logger.info("更新[更新检查中证件号，手机号临时表]开始,请求参数为:[{}]", JSON.toJSONString(map));
        int updatePCBCertCode = cmis0417Mapper.updatePCBCertCode(map);
        logger.info("更新[更新检查中证件号，手机号临时表]结束,返回参数为:[{}]", updatePCBCertCode);
        // 如果插入临时表数据为空，则无分类状态，则不更新
//        if(updatePspCusBaseCase04B>0){
//            logger.info("更新对公、个人客户基本信息分析表[更新贷后客户的分类状态]开始,请求参数为:[{}]", JSON.toJSONString(map));
//            int updatePspCusBaseCase04 = cmis0417Mapper.updatePspCusBaseCase04(map);
//            logger.info("更新对公、个人客户基本信息分析表[更新贷后客户的分类状态]结束,返回参数为:[{}]", updatePspCusBaseCase04);
//        }

    }

    /**
     * 插入投后定期检查存量授信情况
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspLmtSitu(Map map) {
        logger.info("插入投后定期检查存量授信情况开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspLmtSitu = cmis0417Mapper.insertPspLmtSitu(map);
        logger.info("插入投后定期检查存量授信情况结束,返回参数为:[{}]", insertPspLmtSitu);
    }

    /**
     * 插入投后定期检查债券池用信业务情况
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspBondSitu(Map map) {
        logger.info("插入投后定期检查债券池用信业务情况开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspBondSitu = cmis0417Mapper.insertPspBondSitu(map);
        logger.info("插入投后定期检查债券池用信业务情况结束,返回参数为:[{}]", insertPspBondSitu);
    }

    /**
     * 插入资金业务客户基本信息分析表 自动检查自动完成
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspCapCusBaseCase(Map map) {
        logger.info("插入资金业务客户基本信息分析表开始,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspCapCusBaseCase01 = cmis0417Mapper.insertPspCapCusBaseCase01(map);
        logger.info("插入资金业务客户基本信息分析表结束,返回参数为:[{}]", insertPspCapCusBaseCase01);

        logger.info("自动检查自动完成开始,请求参数为:[{}]", JSON.toJSONString(map));
        int updatepspTaskListApproveStatus = cmis0417Mapper.updatepspTaskListApproveStatus(map);
        logger.info("自动检查自动完成结束,返回参数为:[{}]", updatepspTaskListApproveStatus);

        logger.info("更新报告类型为不良类开始,请求参数为:[{}]", JSON.toJSONString(map));
        int updateTaskTaskListRptType3 = cmis0417Mapper.updateTaskTaskListRptType3(map);
        logger.info("更新报告类型为不良类结束,返回参数为:[{}]", updateTaskTaskListRptType3);

        logger.info("更新报告类型为瑕疵类开始,请求参数为:[{}]", JSON.toJSONString(map));
        int updateTaskTaskListRptType2 = cmis0417Mapper.updateTaskTaskListRptType2(map);
        logger.info("更新报告类型为瑕疵类结束,返回参数为:[{}]", updateTaskTaskListRptType2);
    }

    /**
     * 修改贷后任务状态
     *
     * @param map
     */
    public void cmis0417ChangePspTaskList(Map map) {
        logger.info("修改贷后检查任务状态,请求参数为:[{}]", JSON.toJSONString(map));
        int updatepspTaskListTaskStatus = cmis0417Mapper.updatepspTaskListTaskStatus(map);
        logger.info("修改贷后检查任务状态,返回参数为:[{}]", updatepspTaskListTaskStatus);
    }

    /**
     * 手工发起的检查需插入tmp_psp_task_list，避免子表查数据查不到
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspTaskList(Map map) {
        // 20211031 yzl添加插入临时表之前将临时表中当前客户删除  开始
        logger.info("手工发起的检查需插入tmp_psp_task_list之前删除 当前客户,请求参数为:[{}]", JSON.toJSONString(map));
        int deleteTmpPspTaskList = cmis0417Mapper.deleteTmpPspTaskList(map);
        logger.info("手工发起的检查需插入tmp_psp_task_list之前删除 当前客户 ,返回参数为:[{}]", deleteTmpPspTaskList);

        logger.info("手工发起的检查需插入tmp_psp_debit_info之前删除 当前客户,请求参数为:[{}]", JSON.toJSONString(map));
        int deletetmpPpspDebitInfo = cmis0417Mapper.deletetmpPpspDebitInfo(map);
        logger.info("手工发起的检查需插入tmp_psp_debit_info之前删除 当前客户,返回参数为:[{}]", deletetmpPpspDebitInfo);
        // 20211031 yzl添加插入临时表之前将临时表中当前客户删除  结束

        logger.info("手工发起的检查需插入tmp_psp_task_list,请求参数为:[{}]", JSON.toJSONString(map));
        int updatepspTaskListTaskStatus = cmis0417Mapper.insertTmpPspTaskList(map);
        logger.info("手工发起的检查需插入tmp_psp_task_list,返回参数为:[{}]", updatepspTaskListTaskStatus);
    }

    /**
     * 日间手工发起，将借据信息插入原表
     *
     * @param map
     */
    @Transactional
    public void cmis0417InsertPspDebitInfoFromTmp(Map map) {
        logger.info("手工发起的检查需将借据信息插回原表,请求参数为:[{}]", JSON.toJSONString(map));
        int insertPspDebitInfoFromTmp = cmis0417Mapper.insertPspDebitInfoFromTmp(map);
        logger.info("手工发起的检查需将借据信息插回原表,返回参数为:[{}]", insertPspDebitInfoFromTmp);
    }
}
