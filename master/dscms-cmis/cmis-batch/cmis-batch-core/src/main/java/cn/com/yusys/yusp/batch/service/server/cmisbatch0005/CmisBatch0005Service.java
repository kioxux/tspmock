package cn.com.yusys.yusp.batch.service.server.cmisbatch0005;

import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.batch.domain.transform.cfg.CfgCusHandOver;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005Req;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005Resp;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005RespDto;
import cn.com.yusys.yusp.batch.repository.mapper.server.cmisbatch0005.CmisBatch0005Mapper;
import cn.com.yusys.yusp.batch.service.transform.cfg.CfgCusHandOverService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 接口处理Service:更新客户移交相关表
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class CmisBatch0005Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0005Service.class);
    @Autowired
    private CfgCusHandOverService cfgCusHandOverService;//客户移交范围配置表
    @Autowired
    private CmisBatch0005Mapper cmisBatch0005Mapper;

    /**
     * 交易码：cmisbatch0005
     * 交易描述：更新客户移交相关表
     *
     * @param cmisbatch0005ReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Cmisbatch0005RespDto cmisBatch0005(Cmisbatch0005ReqDto cmisbatch0005ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value);
        List<Cmisbatch0005Resp> cmisbatch0005RespList = new ArrayList<>();
        Cmisbatch0005RespDto cmisbatch0005RespDto = new Cmisbatch0005RespDto();// 响应Dto:更新客户移交相关表
        try {
            List<Cmisbatch0005Req> cmisbatch0005ReqList = cmisbatch0005ReqDto.getCmisbatch0005ReqList();//客户移交表清单

            cmisbatch0005RespList = cmisbatch0005ReqList.stream().map(cmisbatch0005Req -> {
                String handoverType = cmisbatch0005Req.getHandoverType();//移交方式
                String managerId = cmisbatch0005Req.getManagerId();//    管户客户经理ID
                String managerBrId = cmisbatch0005Req.getManagerBrId();//    管户机构ID
                String conditionValue = cmisbatch0005Req.getConditionValue();// 条件对应值，包括客户号或合同号
                String opFlag = DscmsEnum.OP_FLAG_S.key;
                String opMessage = DscmsEnum.OP_FLAG_S.value;
                QueryModel cchoQueryModel = new QueryModel();
                cchoQueryModel.addCondition("handoverCusMove", handoverType);//移交方式
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.CFG_CUS_HAND_OVER_MODUEL.key, BatEnums.CFG_CUS_HAND_OVER_MODUEL.value, JSON.toJSONString(cchoQueryModel));
                List<CfgCusHandOver> cchoList = cfgCusHandOverService.selectAll(cchoQueryModel);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.CFG_CUS_HAND_OVER_MODUEL.key, BatEnums.CFG_CUS_HAND_OVER_MODUEL.value, JSON.toJSONString(cchoList));
                if (CollectionUtils.isEmpty(cchoList)) {
                    // 客户移交范围配置表(CFG_CUS_HAND_OVER)未配置
                    logger.info(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value, EchEnum.ECH080012.value);
                    throw BizException.error(null, EchEnum.ECH080012.key, EchEnum.ECH080012.value);
                }

                logger.info("客户移交管户客户经理ID:[{}],管户机构ID:[{}],条件对应值，包括客户号或合同号:[{}]更新相关表开始", managerId, managerBrId, conditionValue);
                cchoList.stream().forEach(
                        cfgCusHandOver -> {
                            /** 客户移交数据库中文名 **/
                            String handoverDbCnName = cfgCusHandOver.getHandoverDbCnName();
                            /** 客户移交数据库英文名 **/
                            String handoverDbEnName = cfgCusHandOver.getHandoverDbEnName();
                            /** 客户移交表中文名 **/
                            String handoverTableCnName = cfgCusHandOver.getHandoverTableCnName();
                            /** 客户移交表英文名 **/
                            String handoverTableEnName = cfgCusHandOver.getHandoverTableEnName();
                            /** 待更新管户客户经理ID **/
                            String updateManagerId = cfgCusHandOver.getUpdateManagerId();
                            /** 待更新管户机构ID **/
                            String updateManagerBrId = cfgCusHandOver.getUpdateManagerBrId();
                            /** 客户移交条件字段 **/
                            String conditionField = cfgCusHandOver.getConditionField();
                            logger.info("客户移交数据库中文名:[{}],数据库英文名:[{}],表中文名:[{}],表英文名:[{}],待更新管户客户经理ID:[{}],待更新管户机构ID:[{}],条件字段:[{}],", handoverDbCnName
                                    , handoverDbEnName, handoverTableCnName, handoverTableEnName, updateManagerId, updateManagerBrId, conditionField);
                            Map updateMap = new HashMap();
                            updateMap.put("dbEnName", handoverDbEnName);//数据库英文名
                            updateMap.put("tableEnName", handoverTableEnName);//表英文名
                            updateMap.put("updateManagerId", updateManagerId);//待更新管户客户经理ID
                            updateMap.put("updateManagerBrId", updateManagerBrId);//待更新管户机构ID
                            updateMap.put("conditionField", conditionField);//数据库中文名
                            updateMap.put("managerId", managerId);//管户客户经理ID
                            updateMap.put("managerBrId", managerBrId);//管户机构ID
                            updateMap.put("conditionValue", conditionValue);// 条件对应值，包括客户号或合同号
                            updateMap.put("updDate", DateUtils.getCurrDateStr());//更新日期
                            updateMap.put("updateTime", DateUtils.getCurrTimestamp());//修改时间
                            // 根据传入的值更新相关表中对应的值
                            logger.info("根据传入的值更新相关表中对应的值,请求参数为:[{}]", JSON.toJSONString(updateMap));
                            int updateResult = cmisBatch0005Mapper.updateManagerInfoByMap(updateMap);
                            logger.info("根据传入的值更新相关表中对应的值,响应参数为:[{}]", JSON.toJSONString(updateResult));
                        });

                Cmisbatch0005Resp cmisbatch0005Resp = new Cmisbatch0005Resp();
                BeanUtils.copyProperties(cmisbatch0005Req, cmisbatch0005Resp);
                cmisbatch0005Resp.setOpFlag(opFlag);
                cmisbatch0005Resp.setOpMessage(opMessage);
                logger.info("客户移交管户客户经理ID:[{}],管户机构ID:[{}],条件对应值，包括客户号或合同号:[{}]更新相关表结束", managerId, managerBrId, conditionValue);
                return cmisbatch0005Resp;
            }).collect(Collectors.toList());
            cmisbatch0005RespDto.setCmisbatch0005RespList(cmisbatch0005RespList);
        } catch (BizException e) {
            Cmisbatch0005Resp cmisbatch0005Resp = new Cmisbatch0005Resp();
            cmisbatch0005Resp.setOpFlag(DscmsEnum.OP_FLAG_F.key);
            cmisbatch0005Resp.setOpMessage(e.getMessage());
            cmisbatch0005RespList.add(cmisbatch0005Resp);
            cmisbatch0005RespDto.setCmisbatch0005RespList(cmisbatch0005RespList);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value);
        return cmisbatch0005RespDto;
    }
}
