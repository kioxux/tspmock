package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0428Mapper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0428</br>
 * 任务名称：加工任务-贷后管理-生成自动化贷后对公名单  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月18日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0428Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0428Service.class);

    @Autowired
    private Cmis0428Mapper cmis0428Mapper;

    /**
     * 插入对公预跑批白名单
     *
     * @param openDay
     */
    public void insertAutoPspWhiteInfoCorp(String openDay) {
        logger.info(" 27号 插入对公预跑批白名单月表 开始,请求参数为:[{}]", openDay);
        int insertAutoPspWhiteInfoCorpMonth = cmis0428Mapper.insertAutoPspWhiteInfoCorpMonth(openDay);
        logger.info("27号 插入对公预跑批白名单月表  结束,返回参数为:[{}]", insertAutoPspWhiteInfoCorpMonth);

        logger.info(" 27号 删除对公预跑批白名单开始,请求参数为:[{}]", openDay);
        int deleteautoPspWhiteInfoCorp = cmis0428Mapper.deleteautoPspWhiteInfoCorp(openDay);
        logger.info("27号 删除对公预跑批白名单 结束,返回参数为:[{}]", deleteautoPspWhiteInfoCorp);


        logger.info("27号插入对公预跑批白名单开始,请求参数为:[{}]", openDay);
        int insertAutoPspWhiteInfoCorp = cmis0428Mapper.insertAutoPspWhiteInfoCorp(openDay);
        logger.info("27号插入对公预跑批白名单结束,返回参数为:[{}]", insertAutoPspWhiteInfoCorp);

        logger.info("27号删除 非专业担保公司 担保 开始,请求参数为:[{}]", openDay);
        int deleteautoPspWhiteInfoCorpNotGuarCom = cmis0428Mapper.deleteautoPspWhiteInfoCorpNotGuarCom(openDay);
        logger.info("27号删除 非专业担保公司 担保 结束,返回参数为:[{}]", deleteautoPspWhiteInfoCorpNotGuarCom);




        logger.info("27号获取三个月内是否存在最新的报告 如果没有则到人行去查,更新 need_query_crereport为1开始,请求参数为:[{}]", openDay);
        int updateAutoPspWhiteInfoCorp = cmis0428Mapper.updateAutoPspWhiteInfoCorp(openDay);
        logger.info("27号获取三个月内是否存在最新的报告 如果没有则到人行去查,更新 need_query_crereport为1结束,返回参数为:[{}]", updateAutoPspWhiteInfoCorp);
    }
}
