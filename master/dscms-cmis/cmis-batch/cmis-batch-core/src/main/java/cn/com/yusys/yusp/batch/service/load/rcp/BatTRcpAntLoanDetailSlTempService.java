/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.rcp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatTRcpAntLoanDetailSlTemp;
import cn.com.yusys.yusp.batch.repository.mapper.load.rcp.BatTRcpAntLoanDetailSlTempMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpAntLoanDetailSlTempService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-10 14:48:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTRcpAntLoanDetailSlTempService {

    @Autowired
    private BatTRcpAntLoanDetailSlTempMapper batTRcpAntLoanDetailSlTempMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTRcpAntLoanDetailSlTemp selectByPrimaryKey(String contractNo) {
        return batTRcpAntLoanDetailSlTempMapper.selectByPrimaryKey(contractNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTRcpAntLoanDetailSlTemp> selectAll(QueryModel model) {
        List<BatTRcpAntLoanDetailSlTemp> records = (List<BatTRcpAntLoanDetailSlTemp>) batTRcpAntLoanDetailSlTempMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTRcpAntLoanDetailSlTemp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTRcpAntLoanDetailSlTemp> list = batTRcpAntLoanDetailSlTempMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTRcpAntLoanDetailSlTemp record) {
        return batTRcpAntLoanDetailSlTempMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTRcpAntLoanDetailSlTemp record) {
        return batTRcpAntLoanDetailSlTempMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTRcpAntLoanDetailSlTemp record) {
        return batTRcpAntLoanDetailSlTempMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTRcpAntLoanDetailSlTemp record) {
        return batTRcpAntLoanDetailSlTempMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String contractNo) {
        return batTRcpAntLoanDetailSlTempMapper.deleteByPrimaryKey(contractNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batTRcpAntLoanDetailSlTempMapper.deleteByIds(ids);
    }
}
