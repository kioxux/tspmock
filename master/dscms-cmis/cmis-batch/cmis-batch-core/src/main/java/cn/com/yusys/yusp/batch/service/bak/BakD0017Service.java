package cn.com.yusys.yusp.batch.service.bak;

import cn.com.yusys.yusp.batch.repository.mapper.bak.BakD0017Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKD0017</br>
 * 任务名称：批前备份日表任务-备份贴现协议详情[CTR_DISC_CONT]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
@Service
@Transactional
public class BakD0017Service {
    private static final Logger logger = LoggerFactory.getLogger(BakD0017Service.class);

    @Autowired
    private BakD0017Mapper bakD0017Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 备份当天的数据
     *
     * @param openDay
     */
    public void bakD0017InsertCurrent(String openDay) {
        // 备份当天的数据
        logger.info("备份当天的[贴现协议详情[CTR_DISC_CONT]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakD0017Mapper.insertCurrent(openDay);
        logger.info("备份当天的[贴现协议详情[CTR_DISC_CONT]]数据结束,返回参数为:[{}]", insertCurrent);
    }


    /**
     * 删除当天的数据
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void bakD0017DeleteCurrent(String openDay) {

        logger.info("删除当天的[贴现协议详情[CTR_DISC_CONT]]数据开始,请求参数为:[{}]", openDay);
        // bakD0017Mapper.truncateCurrent();// cmis_biz.TMP_D_CTR_DISC_CONT
        tableUtilsService.renameCreateDropTable("cmis_biz", "TMP_D_CTR_DISC_CONT");// 20211030 调整为调用重命名创建和删除相关表的公共方法
        logger.info("删除当天的[贴现协议详情[CTR_DISC_CONT]]数据结束");
//        logger.info("查询待删除当天的[贴现协议详情[CTR_DISC_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
//        int queryD0017DeleteOpenDayCtrCtrDiscContCounts = bakD0017Mapper.queryD0017DeleteOpenDayCtrCtrDiscContCounts(openDay);
//        // 由于TDSQL不支持ceiling函数，此处在java中处理
//        BigDecimal times = new BigDecimal(queryD0017DeleteOpenDayCtrCtrDiscContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
//        queryD0017DeleteOpenDayCtrCtrDiscContCounts = times.intValue();
//        logger.info("查询待删除当天的[贴现协议详情[CTR_DISC_CONT]]数据总次数结束,返回参数为:[{}]", queryD0017DeleteOpenDayCtrCtrDiscContCounts);
//        // 删除当天的数据
//        for (int i = 0; i < queryD0017DeleteOpenDayCtrCtrDiscContCounts; i++) {
//            logger.info("第[" + i + "]次删除当天的[贴现协议详情[CTR_DISC_CONT]]数据开始,请求参数为:[{}]", openDay);
//            int deleteCurrent = bakD0017Mapper.deleteCurrent(openDay);
//            logger.info("第[" + i + "]次删除当天的[贴现协议详情[CTR_DISC_CONT]]数据结束,返回参数为:[{}]", deleteCurrent);
//        }
    }


    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakDEqualsOriginal(String openDay) {
        logger.info("查询当天备份的[贴现协议详情[TMP_D_CTR_DISC_CONT]]数据总条数开始,请求参数为:[{}]", openDay);
        int bakDCounts = bakD0017Mapper.queryD0017DeleteOpenDayCtrCtrDiscContCounts(openDay);
        logger.info("查询当天备份的[贴现协议详情[TMP_D_CTR_DISC_CONT]]数据总条数结束,返回参数为:[{}]", bakDCounts);

        logger.info("查询当天的[贴现协议详情[CTR_DISC_CONT]]原表数据总条数开始,请求参数为:[{}]", openDay);
        int originalCounts = bakD0017Mapper.queryCtrDiscContCounts(openDay);
        logger.info("查询当天的[贴现协议详情[CTR_DISC_CONT]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

        if (bakDCounts != originalCounts) {
            logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakDCounts, originalCounts);
            throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakDCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
        } else {
            logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakDCounts, originalCounts);
        }
    }
}
