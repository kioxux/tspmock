/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.djk;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTDjkDataLoan
 * @类描述: bat_t_djk_data_loan数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-07 10:57:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_djk_data_loan")
public class BatTDjkDataLoan extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 分期计划 ID **/
	@Id
	@Column(name = "LOAN_ID")
	private String loanId;
	
	/** 账户编号 **/
	@Id
	@Column(name = "ACCT_NO")
	private String acctNo;
	
	/** 交易参考号 **/
	@Column(name = "REF_NBR", unique = false, nullable = true, length = 23)
	private String refNbr;
	
	/** 卡号 **/
	@Column(name = "LOGICAL_CARD_NO", unique = false, nullable = true, length = 19)
	private String logicalCardNo;
	
	/** 分期注册日期 **/
	@Column(name = "REGISTER_DATE", unique = false, nullable = true, length = 8)
	private String registerDate;
	
	/** 分期类型 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 1)
	private String loanType;
	
	/** 分期状态 **/
	@Column(name = "LOAN_STATUS", unique = false, nullable = true, length = 1)
	private String loanStatus;
	
	/** 分期总期数 **/
	@Column(name = "LOAN_INIT_TERM", unique = false, nullable = true, length = 3)
	private String loanInitTerm;
	
	/** 当前期数 **/
	@Column(name = "CURR_TERM", unique = false, nullable = true, length = 3)
	private String currTerm;
	
	/** 分期总本金 **/
	@Column(name = "LOAN_INIT_PRIN", unique = false, nullable = true, length = 15)
	private String loanInitPrin;
	
	/** 分期每期应还本金 **/
	@Column(name = "LOAN_FIXED_PMT_PRIN", unique = false, nullable = true, length = 15)
	private String loanFixedPmtPrin;
	
	/** 分期首期应还本金 **/
	@Column(name = "LOAN_FIRST_TERM_PRIN", unique = false, nullable = true, length = 15)
	private String loanFirstTermPrin;
	
	/** 分期末期应还本金 **/
	@Column(name = "LOAN_FINAL_TERM_PRIN", unique = false, nullable = true, length = 15)
	private String loanFinalTermPrin;
	
	/** 分期总手续费 **/
	@Column(name = "LOAN_INIT_FEE1", unique = false, nullable = true, length = 15)
	private String loanInitFee1;
	
	/** 分期每期手续费 **/
	@Column(name = "LOAN_FIXED_FEE1", unique = false, nullable = true, length = 15)
	private String loanFixedFee1;
	
	/** 分期首期手续费 **/
	@Column(name = "LOAN_FIRST_TERM_FEE1", unique = false, nullable = true, length = 15)
	private String loanFirstTermFee1;
	
	/** 分期末期手续费 **/
	@Column(name = "LOAN_FINAL_TERM_FEE1", unique = false, nullable = true, length = 15)
	private String loanFinalTermFee1;
	
	/** 还清日期 **/
	@Column(name = "PAID_OUT_DATE", unique = false, nullable = true, length = 8)
	private String paidOutDate;
	
	/** 分期当前总余额 **/
	@Column(name = "LOAN_CURR_BAL", unique = false, nullable = true, length = 15)
	private String loanCurrBal;
	
	/** 分期未到期本金 **/
	@Column(name = "LOAN_BAL_XFROUT", unique = false, nullable = true, length = 15)
	private String loanBalXfrout;
	
	/** 分期计划代码 **/
	@Column(name = "LOAN_CODE", unique = false, nullable = true, length = 4)
	private String loanCode;
	
	/** 账户类型 **/
	@Column(name = "ACCT_TYPE", unique = false, nullable = true, length = 1)
	private String acctType;
	
	/** 剩余期数 **/
	@Column(name = "REMAIN_TERM", unique = false, nullable = true, length = 2)
	private String remainTerm;
	
	/** 分期中止原因代码 **/
	@Column(name = "TERMINATE_REASON_CD", unique = false, nullable = true, length = 1)
	private String terminateReasonCd;
	
	/** 已偿手续费 **/
	@Column(name = "FEE_PAID", unique = false, nullable = true, length = 15)
	private String feePaid;
	
	/** 分期提前中止日期 **/
	@Column(name = "TERMINATE_DATE", unique = false, nullable = true, length = 8)
	private String terminateDate;
	
	/** 提前还款标志 **/
	@Column(name = "ADV_PMT_IND", unique = false, nullable = true, length = 2)
	private String advPmtInd;
	
	/** 营销人员姓名 **/
	@Column(name = "MARKETER_NAME", unique = false, nullable = true, length = 80)
	private String marketerName;
	
	/** 营销人员编号 **/
	@Column(name = "MARKETER_ID", unique = false, nullable = true, length = 20)
	private String marketerId;
	
	/** 营销人员所属分行 **/
	@Column(name = "MARKETER_BRANCH_ID", unique = false, nullable = true, length = 9)
	private String marketerBranchId;
	
	/** 分期款项用途 **/
	@Column(name = "LOAN_PRIN_APP", unique = false, nullable = true, length = 8)
	private String loanPrinApp;
	
	/** 是否抵押 **/
	@Column(name = "GUARANTY", unique = false, nullable = true, length = 1)
	private String guaranty;
	
	/** 渠道编号 **/
	@Column(name = "CHANNEL_ID", unique = false, nullable = true, length = 2)
	private String channelId;
	
	/** 未还分期本金总额 **/
	@Column(name = "UNPAY_PRIN", unique = false, nullable = true, length = 15)
	private String unpayPrin;
	
	/** 未还分期交易费 **/
	@Column(name = "UNPAY_TXNFEE", unique = false, nullable = true, length = 9)
	private String unpayTxnfee;
	
	/** 未还分期利息 **/
	@Column(name = "UNPAY_INTEREST", unique = false, nullable = true, length = 9)
	private String unpayInterest;
	
	/** 预留域 **/
	@Column(name = "FILLER", unique = false, nullable = true, length = 9)
	private String filler;
	
	
	/**
	 * @param loanId
	 */
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}
	
    /**
     * @return loanId
     */
	public String getLoanId() {
		return this.loanId;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param refNbr
	 */
	public void setRefNbr(String refNbr) {
		this.refNbr = refNbr;
	}
	
    /**
     * @return refNbr
     */
	public String getRefNbr() {
		return this.refNbr;
	}
	
	/**
	 * @param logicalCardNo
	 */
	public void setLogicalCardNo(String logicalCardNo) {
		this.logicalCardNo = logicalCardNo;
	}
	
    /**
     * @return logicalCardNo
     */
	public String getLogicalCardNo() {
		return this.logicalCardNo;
	}
	
	/**
	 * @param registerDate
	 */
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	
    /**
     * @return registerDate
     */
	public String getRegisterDate() {
		return this.registerDate;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
    /**
     * @return loanType
     */
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param loanStatus
	 */
	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}
	
    /**
     * @return loanStatus
     */
	public String getLoanStatus() {
		return this.loanStatus;
	}
	
	/**
	 * @param loanInitTerm
	 */
	public void setLoanInitTerm(String loanInitTerm) {
		this.loanInitTerm = loanInitTerm;
	}
	
    /**
     * @return loanInitTerm
     */
	public String getLoanInitTerm() {
		return this.loanInitTerm;
	}
	
	/**
	 * @param currTerm
	 */
	public void setCurrTerm(String currTerm) {
		this.currTerm = currTerm;
	}
	
    /**
     * @return currTerm
     */
	public String getCurrTerm() {
		return this.currTerm;
	}
	
	/**
	 * @param loanInitPrin
	 */
	public void setLoanInitPrin(String loanInitPrin) {
		this.loanInitPrin = loanInitPrin;
	}
	
    /**
     * @return loanInitPrin
     */
	public String getLoanInitPrin() {
		return this.loanInitPrin;
	}
	
	/**
	 * @param loanFixedPmtPrin
	 */
	public void setLoanFixedPmtPrin(String loanFixedPmtPrin) {
		this.loanFixedPmtPrin = loanFixedPmtPrin;
	}
	
    /**
     * @return loanFixedPmtPrin
     */
	public String getLoanFixedPmtPrin() {
		return this.loanFixedPmtPrin;
	}
	
	/**
	 * @param loanFirstTermPrin
	 */
	public void setLoanFirstTermPrin(String loanFirstTermPrin) {
		this.loanFirstTermPrin = loanFirstTermPrin;
	}
	
    /**
     * @return loanFirstTermPrin
     */
	public String getLoanFirstTermPrin() {
		return this.loanFirstTermPrin;
	}
	
	/**
	 * @param loanFinalTermPrin
	 */
	public void setLoanFinalTermPrin(String loanFinalTermPrin) {
		this.loanFinalTermPrin = loanFinalTermPrin;
	}
	
    /**
     * @return loanFinalTermPrin
     */
	public String getLoanFinalTermPrin() {
		return this.loanFinalTermPrin;
	}
	
	/**
	 * @param loanInitFee1
	 */
	public void setLoanInitFee1(String loanInitFee1) {
		this.loanInitFee1 = loanInitFee1;
	}
	
    /**
     * @return loanInitFee1
     */
	public String getLoanInitFee1() {
		return this.loanInitFee1;
	}
	
	/**
	 * @param loanFixedFee1
	 */
	public void setLoanFixedFee1(String loanFixedFee1) {
		this.loanFixedFee1 = loanFixedFee1;
	}
	
    /**
     * @return loanFixedFee1
     */
	public String getLoanFixedFee1() {
		return this.loanFixedFee1;
	}
	
	/**
	 * @param loanFirstTermFee1
	 */
	public void setLoanFirstTermFee1(String loanFirstTermFee1) {
		this.loanFirstTermFee1 = loanFirstTermFee1;
	}
	
    /**
     * @return loanFirstTermFee1
     */
	public String getLoanFirstTermFee1() {
		return this.loanFirstTermFee1;
	}
	
	/**
	 * @param loanFinalTermFee1
	 */
	public void setLoanFinalTermFee1(String loanFinalTermFee1) {
		this.loanFinalTermFee1 = loanFinalTermFee1;
	}
	
    /**
     * @return loanFinalTermFee1
     */
	public String getLoanFinalTermFee1() {
		return this.loanFinalTermFee1;
	}
	
	/**
	 * @param paidOutDate
	 */
	public void setPaidOutDate(String paidOutDate) {
		this.paidOutDate = paidOutDate;
	}
	
    /**
     * @return paidOutDate
     */
	public String getPaidOutDate() {
		return this.paidOutDate;
	}
	
	/**
	 * @param loanCurrBal
	 */
	public void setLoanCurrBal(String loanCurrBal) {
		this.loanCurrBal = loanCurrBal;
	}
	
    /**
     * @return loanCurrBal
     */
	public String getLoanCurrBal() {
		return this.loanCurrBal;
	}
	
	/**
	 * @param loanBalXfrout
	 */
	public void setLoanBalXfrout(String loanBalXfrout) {
		this.loanBalXfrout = loanBalXfrout;
	}
	
    /**
     * @return loanBalXfrout
     */
	public String getLoanBalXfrout() {
		return this.loanBalXfrout;
	}
	
	/**
	 * @param loanCode
	 */
	public void setLoanCode(String loanCode) {
		this.loanCode = loanCode;
	}
	
    /**
     * @return loanCode
     */
	public String getLoanCode() {
		return this.loanCode;
	}
	
	/**
	 * @param acctType
	 */
	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}
	
    /**
     * @return acctType
     */
	public String getAcctType() {
		return this.acctType;
	}
	
	/**
	 * @param remainTerm
	 */
	public void setRemainTerm(String remainTerm) {
		this.remainTerm = remainTerm;
	}
	
    /**
     * @return remainTerm
     */
	public String getRemainTerm() {
		return this.remainTerm;
	}
	
	/**
	 * @param terminateReasonCd
	 */
	public void setTerminateReasonCd(String terminateReasonCd) {
		this.terminateReasonCd = terminateReasonCd;
	}
	
    /**
     * @return terminateReasonCd
     */
	public String getTerminateReasonCd() {
		return this.terminateReasonCd;
	}
	
	/**
	 * @param feePaid
	 */
	public void setFeePaid(String feePaid) {
		this.feePaid = feePaid;
	}
	
    /**
     * @return feePaid
     */
	public String getFeePaid() {
		return this.feePaid;
	}
	
	/**
	 * @param terminateDate
	 */
	public void setTerminateDate(String terminateDate) {
		this.terminateDate = terminateDate;
	}
	
    /**
     * @return terminateDate
     */
	public String getTerminateDate() {
		return this.terminateDate;
	}
	
	/**
	 * @param advPmtInd
	 */
	public void setAdvPmtInd(String advPmtInd) {
		this.advPmtInd = advPmtInd;
	}
	
    /**
     * @return advPmtInd
     */
	public String getAdvPmtInd() {
		return this.advPmtInd;
	}
	
	/**
	 * @param marketerName
	 */
	public void setMarketerName(String marketerName) {
		this.marketerName = marketerName;
	}
	
    /**
     * @return marketerName
     */
	public String getMarketerName() {
		return this.marketerName;
	}
	
	/**
	 * @param marketerId
	 */
	public void setMarketerId(String marketerId) {
		this.marketerId = marketerId;
	}
	
    /**
     * @return marketerId
     */
	public String getMarketerId() {
		return this.marketerId;
	}
	
	/**
	 * @param marketerBranchId
	 */
	public void setMarketerBranchId(String marketerBranchId) {
		this.marketerBranchId = marketerBranchId;
	}
	
    /**
     * @return marketerBranchId
     */
	public String getMarketerBranchId() {
		return this.marketerBranchId;
	}
	
	/**
	 * @param loanPrinApp
	 */
	public void setLoanPrinApp(String loanPrinApp) {
		this.loanPrinApp = loanPrinApp;
	}
	
    /**
     * @return loanPrinApp
     */
	public String getLoanPrinApp() {
		return this.loanPrinApp;
	}
	
	/**
	 * @param guaranty
	 */
	public void setGuaranty(String guaranty) {
		this.guaranty = guaranty;
	}
	
    /**
     * @return guaranty
     */
	public String getGuaranty() {
		return this.guaranty;
	}
	
	/**
	 * @param channelId
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	
    /**
     * @return channelId
     */
	public String getChannelId() {
		return this.channelId;
	}
	
	/**
	 * @param unpayPrin
	 */
	public void setUnpayPrin(String unpayPrin) {
		this.unpayPrin = unpayPrin;
	}
	
    /**
     * @return unpayPrin
     */
	public String getUnpayPrin() {
		return this.unpayPrin;
	}
	
	/**
	 * @param unpayTxnfee
	 */
	public void setUnpayTxnfee(String unpayTxnfee) {
		this.unpayTxnfee = unpayTxnfee;
	}
	
    /**
     * @return unpayTxnfee
     */
	public String getUnpayTxnfee() {
		return this.unpayTxnfee;
	}
	
	/**
	 * @param unpayInterest
	 */
	public void setUnpayInterest(String unpayInterest) {
		this.unpayInterest = unpayInterest;
	}
	
    /**
     * @return unpayInterest
     */
	public String getUnpayInterest() {
		return this.unpayInterest;
	}
	
	/**
	 * @param filler
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
    /**
     * @return filler
     */
	public String getFiller() {
		return this.filler;
	}


}