package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0424Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0424</br>
 * 任务名称：加工任务-贷后管理-业务变更风险分类  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0424Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0424Service.class);

    @Autowired
    private Cmis0424Mapper cmis0424Mapper;

    /**
     * 插入贷后检查任务表-对公特殊业务
     *
     * @param openDay
     */
    public void cmis0424InsertPspRiskDgyw(String openDay) {
        logger.info("对公特殊业务变更分类任务");
        // 1.新发放贷款生成分类任务：客户名下不存在6个月内有最新有效分类结果，则业务发放或交易的同时由系统同步生成一笔新发放或交易业务的分类任务,任务是按照客户来生成；
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspRiskDgyw01 = cmis0424Mapper.insertPspRiskDgyw01(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspRiskDgyw01);
    }

    /**
     * 插入贷后检查任务表-个人经营性
     *
     * @param openDay
     */
//    public void cmis0424InsertPspRiskGrjyx(String openDay) {
//        logger.info("个人经营性特殊业务变更分类任务");
//        // 新发放贷款生成分类任务：客户名下不存在6个月内有最新有效分类结果，则业务发放或交易的同时由系统同步生成一笔新发放或交易业务的分类任务,任务是按照客户来生成；
//        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
//        int insertPspRiskGrjyx01 = cmis0424Mapper.insertPspRiskGrjyx01(openDay);
//        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspRiskGrjyx01);
//    }

    /**
     * 插入贷后检查任务表-个人消费性
     *
     * @param openDay
     */
//    public void cmis0424InsertPspRiskGrxfx(String openDay) {
//        logger.info("个人消费性特殊业务变更分类任务");
//        // 新发放贷款生成分类任务：客户名下不存在6个月内有最新有效分类结果，则业务发放或交易的同时由系统同步生成一笔新发放或交易业务的分类任务,任务是按照客户来生成
//        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
//        int insertPspRiskGrxfx01 = cmis0424Mapper.insertPspRiskGrxfx01(openDay);
//        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspRiskGrxfx01);
//    }
}
