package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0430</br>
 * 任务名称：加工任务-贷后管理-自动化贷后规则  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月18日 下午11:56:54
 */
public interface Cmis0430Mapper {

    /**
     * 月底删除自动化贷后客户加工数据临时表中数据
     *
     * @param openDay
     * @return
     */
    int deleteTmpAutoPspOverdue(@Param("openDay") String openDay);

    /**
     * 插入自动化贷后客户加工数据临时表中数据(对公近一年流动资金逾期次数,（>=2 不符合))
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue01(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(对公近一年流动资金逾期次数,（>=2 不符合))
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp01(@Param("openDay") String openDay);




    /**
     * 插入自动化贷后客户加工数据临时表中数据(对公近一年连续逾期期数，（>=0 不符合))
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue02(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(对公近一年连续逾期期数，（>=0 不符合))
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp02(@Param("openDay") String openDay);



    /**
     * 插入自动化贷后客户加工数据临时表中数据(cp_loan_overdue  对公未结清贷款逾期期数)
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue03(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(cp_loan_overdue  对公未结清贷款逾期期数)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp03(@Param("openDay") String openDay);



    /**
     * 插入自动化贷后客户加工数据临时表中数据(exist_bad_fiveclass  对公五级分类存在正常以下)
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue04(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(exist_bad_fiveclass  对公五级分类存在正常以下)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp04(@Param("openDay") String openDay);




    /**
     * 插入自动化贷后客户加工数据临时表中数据(acc_overdue_days  台账逾期天数)
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue05(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(acc_overdue_days  台账逾期天数)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp05(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(台账存在逾期天数)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp0A(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(exist_high_risk_level风险预警等级红 黑)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp0B(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(exist_black_list  存在黑灰名单)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp0C(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(exist_xa_case_info  存在小安涉诉)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp0D(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(更新失信被执行人标记)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp0E(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(工商 营业信息  不是 在营（开业） 更新 exist_business_checkfs )
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp0F(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(经营企业半年内发生过法人变更，校验不通过  更新exist_business_checkfs )
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp0G(@Param("openDay") String openDay);


    /**
     * 对公自动化贷后跑批白名单存在不符合规则，更新为不通过
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorpautostatus(@Param("openDay") String openDay);

    /**
     * 更新对公自动化贷后跑批白名单(更新为通过)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp0End(@Param("openDay") String openDay);






    /**
     * 插入自动化贷后客户加工数据临时表中数据(acc_overdue_days  台账逾期天数)
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue11(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(acc_overdue_days  台账逾期天数)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer11(@Param("openDay") String openDay);






    /**
     * 插入自动化贷后客户加工数据临时表中数据(经营性贷款近一年历史逾期次数<=2)
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue12(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(经营性贷款近一年历史逾期次数<=2)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer12(@Param("openDay") String openDay);




    /**
     * 插入自动化贷后客户加工数据临时表中数据(经营性贷款连续逾期次数<=1)
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue13(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(经营性贷款连续逾期次数<=1)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer13(@Param("openDay") String openDay);




    /**
     * 插入自动化贷后客户加工数据临时表中数据(近一年内房贷、消费贷逾期不超过3次（含）：房贷、消费贷近1年历史逾期累计次数<=3)
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue14(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(近一年内房贷、消费贷逾期不超过3次（含）：房贷、消费贷近1年历史逾期累计次数<=3)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer14(@Param("openDay") String openDay);






    /**
     * 插入自动化贷后客户加工数据临时表中数据(近一年内房贷、消费贷无连续逾期、欠息记录：房贷、消费贷近一年连续逾期期数<=1)
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue15(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(近一年内房贷、消费贷无连续逾期、欠息记录：房贷、消费贷近一年连续逾期期数<=1)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer15(@Param("openDay") String openDay);



    /**
     * 插入自动化贷后客户加工数据临时表中数据(未结清贷款（所有类型），当前无逾期)
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue16(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(未结清贷款（所有类型），当前无逾期)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer16(@Param("openDay") String openDay);



    /**
     * 插入自动化贷后客户加工数据临时表中数据(所有贷款，五级分类为正常)
     *
     * @param openDay
     * @return
     */
    int insertTmpAutoPspOverdue17(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(所有贷款，五级分类为正常)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer17(@Param("openDay") String openDay);


    /**
     * 更新个人自动化贷后跑批白名单(AUTO_STATUS通过状态(1 通过 0 不通过))
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer1A(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单不符合规则的标记为不通过
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPerautostatus2(@Param("openDay") String openDay);



    /**
     * 更新个人自动化贷后跑批白名单(实际控制人通过状态为不通过则对公客户的通过状态为不通过  AUTO_TYPE  个人或者实际控制人(1或者0))
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer1B(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(exist_high_risk_level 存在红黑预警等级 )
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer1C(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(exist_black_list  存在黑灰名单)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer1D(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单( exist_xa_case_info  存在小安涉诉)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer1E(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(发送小安 查询失信被执行人规则)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer1F(@Param("openDay") String openDay);

    /**
     * 更新个人自动化贷后跑批白名单(更新为通过)
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoPer0End(@Param("openDay") String openDay);

    /**
     * 插入自动化检查对公任务生成
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList01(@Param("openDay") String openDay);

    /**
     * 插入自动化检查个人任务生成
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList02(@Param("openDay") String openDay);

    /**
     * 存在自动化贷后对公名单之内，则更新贷后检查任务为自动通过
     *
     * @param openDay
     * @return
     */
    int updatePspTaskListAutoCorp(@Param("openDay") String openDay);




    /**
     *  存在自动化贷后个人名单之内，则更新贷后检查任务为自动通过
     *
     * @param openDay
     * @return
     */
    int updatePspTaskListAutoPer(@Param("openDay") String openDay);


}
