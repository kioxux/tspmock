package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0117Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0117</br>
 * 任务名称：加工任务-业务处理-统计报表生成 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0117Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0117Service.class);

    @Autowired
    private Cmis0117Mapper cmis0117Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 插入 资产管理统计表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int cmis0117InsertBatBizAssetsAnalyse(String openDay) {
        logger.info("删除资产管理统计表非月末的数据开始,请求参数为:[{}]", openDay);
        int deleteBatBizAssetsAnalyse = cmis0117Mapper.deleteBatBizAssetsAnalyse(openDay);
        logger.info("删除资产管理统计表非月末的数据结束,返回参数为:[{}]", deleteBatBizAssetsAnalyse);

        logger.info("重命名创建和删除贷款台账信息表0117开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","temp_acc_loan_0117");
        logger.info("重命名创建和删除贷款台账信息表0117结束");

        logger.info("插入贷款台账信息表0117开始,请求参数为:[{}]", openDay);
        int insertTempAccLoan0117 = cmis0117Mapper.insertTempAccLoan0117(openDay);
        logger.info("插入贷款台账信息表0117结束,返回参数为:[{}]", insertTempAccLoan0117);
        tableUtilsService.analyzeTable("cmis_biz","temp_acc_loan_0117");//分析表

        logger.info("插入资产管理统计表开始,请求参数为:[{}]", openDay);
        int insertBatBizAssetsAnalyse = cmis0117Mapper.insertBatBizAssetsAnalyse(openDay);
        logger.info("插入资产管理统计表结束,返回参数为:[{}]", insertBatBizAssetsAnalyse);
        return insertBatBizAssetsAnalyse;
    }

    /**
     * 插入不良资产统计表
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int insertBatBizBadAssets(String openDay) {
        logger.info("重命名创建和删除不良资产统计表数据开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","bat_biz_bad_assets");
        logger.info("重命名创建和删除不良资产统计表数据结束");
        logger.info("插入不良资产统计表开始,请求参数为:[{}]", openDay);
        int insertBatBizBadAssets = cmis0117Mapper.insertBatBizBadAssets(openDay);
        logger.info("插入不良资产统计表结束,返回参数为:[{}]", insertBatBizBadAssets);
        tableUtilsService.analyzeTable("cmis_biz","bat_biz_bad_assets");//分析表
        return insertBatBizBadAssets;
    }

    /**
     * 插入和更新客户数量统计表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void insertUpdateBatBizCusCount(String openDay) {
//        logger.info("重命名创建和删除客户数量统计历史表数据开始,请求参数为:[{}]", openDay);
//        tableUtilsService.renameCreateDropTable("cmis_biz","bat_biz_cus_count_his");
//        logger.info("重命名创建和删除客户数量统计历史表数据结束");

//        logger.info("插入客户数量统计历史表开始,请求参数为:[{}]", openDay);
//        int insertBatBizCusCountHis = cmis0117Mapper.insertBatBizCusCountHis(openDay);
//        logger.info("插入客户数量统计历史表结束,返回参数为:[{}]", insertBatBizCusCountHis);
//        tableUtilsService.analyzeTable("cmis_biz","bat_biz_cus_count_his");//分析表




//        logger.info("计算 当前管户融资户数= 当前客户经理下的对公客户数量与个人客户数量 开始");
//        logger.info("重命名创建和删除 临时表-主管客户经理和对公客户数量关系表 开始,请求参数为:[{}]", openDay);
//        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_cus_count_corp");
//        logger.info("重命名创建和删除 临时表-主管客户经理和对公客户数量关系表 结束");
//        logger.info("插入 临时表-主管客户经理和对公客户数量关系表 开始,请求参数为:[{}]", openDay);
//        int insertTmpCusCountCorp = cmis0117Mapper.insertTmpCusCountCorp(openDay);
//        logger.info("插入 临时表-主管客户经理和对公客户数量关系表 结束,返回参数为:[{}]", insertTmpCusCountCorp);
//        tableUtilsService.analyzeTable("cmis_batch","tmp_cus_count_corp");//分析表
//        logger.info("当前客户经理下的对公客户数量 开始,请求参数为:[{}]", openDay);
//        int updateBatBizCusCount01 = cmis0117Mapper.updateBatBizCusCount01(openDay);
//        logger.info("当前客户经理下的对公客户数量 结束,返回参数为:[{}]", updateBatBizCusCount01);


//        logger.info("重命名创建和删除 临时表-主管客户经理和个人客户数量关系表 开始,请求参数为:[{}]", openDay);
//        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_cus_count_indiv");//分析表
//        logger.info("重命名创建和删除 临时表-主管客户经理和个人客户数量关系表 结束");
//        logger.info("插入 临时表-主管客户经理和个人客户数量关系表 开始,请求参数为:[{}]", openDay);
//        int insertTmpCusCountIndiv = cmis0117Mapper.insertTmpCusCountIndiv(openDay);
//        logger.info("插入 临时表-主管客户经理和个人客户数量关系表 结束,返回参数为:[{}]", insertTmpCusCountIndiv);
//        tableUtilsService.analyzeTable("cmis_batch","tmp_cus_count_indiv");//分析表
//        logger.info("当前客户经理下的累加个人客户数量 开始,请求参数为:[{}]", openDay);
//        int updateBatBizCusCount02 = cmis0117Mapper.updateBatBizCusCount02(openDay);
//        logger.info("当前客户经理下的累加个人客户数量 结束,返回参数为:[{}]", updateBatBizCusCount02);



        logger.info("重命名创建和删除客户数量统计表开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","bat_biz_cus_count");
        logger.info("重命名创建和删除客户数量统计表结束");


        logger.info("重命名创建和删除 临时表-主管客户经理和小企业客户数量关系表 开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_cus_count_corp_smcon1");
        logger.info("重命名创建和删除 临时表-主管客户经理和小企业客户数量关系表 结束");



        logger.info("重命名创建和删除 临时表-主管客户经理和贷款台账客户数量关系表 开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_cus_count_acc_loan");
        logger.info("重命名创建和删除 临时表-主管客户经理和贷款台账客户数量关系表 结束");



        logger.info("重命名创建和删除 临时表-主管客户经理和公司类融资户数量关系表 开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_cus_count_corp_smcon0");//分析
        logger.info("重命名创建和删除 临时表-主管客户经理和公司类融资户数量关系表 结束");







//        logger.info("非贷款客户数 客户数等于 acc_accp 银承台账客户数+ acc_cvrs 保函台账客户数 + acc_disc 贴现台账客户数 + acc_tf_loc 开证台账客户数  开始 ");
//        logger.info("重命名创建和删除 临时表-主管客户经理和银承台账客户数量关系表 开始,请求参数为:[{}]", openDay);
//        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_cus_count_acc_accp");
//        logger.info("重命名创建和删除 临时表-主管客户经理和银承台账客户数量关系表 结束");
//        logger.info("插入 临时表-主管客户经理和银承台账客户数量关系表 开始,请求参数为:[{}]", openDay);
//        int insertTmpCusCountAccAccp = cmis0117Mapper.insertTmpCusCountAccAccp(openDay);
//        logger.info("插入 临时表-主管客户经理和银承台账客户数量关系表 结束,返回参数为:[{}]", insertTmpCusCountAccAccp);
//        tableUtilsService.analyzeTable("cmis_batch","tmp_cus_count_acc_accp");//分析表
//
//        logger.info("银承台账客户数 开始,请求参数为:[{}]", openDay);
//        int updateBatBizCusCount07 = cmis0117Mapper.updateBatBizCusCount07(openDay);
//        logger.info("银承台账客户数 结束,返回参数为:[{}]", updateBatBizCusCount07);
//
//        logger.info("重命名创建和删除 临时表-主管客户经理和保函台账客户数量关系表 开始,请求参数为:[{}]", openDay);
//        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_cus_count_acc_cvrs");
//        logger.info("重命名创建和删除 临时表-主管客户经理和保函台账客户数量关系表 结束");
//        logger.info("插入 临时表-主管客户经理和保函台账客户数量关系表 开始,请求参数为:[{}]", openDay);
//        int insertTmpCusCountAccCvrs = cmis0117Mapper.insertTmpCusCountAccCvrs(openDay);
//        logger.info("插入 临时表-主管客户经理和保函台账客户数量关系表 结束,返回参数为:[{}]", insertTmpCusCountAccCvrs);
//        tableUtilsService.analyzeTable("cmis_batch","tmp_cus_count_acc_cvrs");//分析表
//
//        logger.info("保函台账客户数 开始,请求参数为:[{}]", openDay);
//        int updateBatBizCusCount08 = cmis0117Mapper.updateBatBizCusCount08(openDay);
//        logger.info("保函台账客户数 结束,返回参数为:[{}]", updateBatBizCusCount08);
//
//        logger.info("重命名创建和删除 临时表-主管客户经理和贴现台账客户数量关系表 开始,请求参数为:[{}]", openDay);
//        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_cus_count_acc_disc");
//        logger.info("重命名创建和删除 临时表-主管客户经理和贴现台账客户数量关系表 结束");
//        logger.info("插入 临时表-主管客户经理和贴现台账客户数量关系表 开始,请求参数为:[{}]", openDay);
//        int insertTmpCusCountAccDisc = cmis0117Mapper.insertTmpCusCountAccDisc(openDay);
//        logger.info("插入 临时表-主管客户经理和贴现台账客户数量关系表 结束,返回参数为:[{}]", insertTmpCusCountAccDisc);
//        tableUtilsService.analyzeTable("cmis_batch","tmp_cus_count_acc_disc");//分析表
//
//        logger.info("贴现台账客户数 开始,请求参数为:[{}]", openDay);
//        int updateBatBizCusCount09 = cmis0117Mapper.updateBatBizCusCount09(openDay);
//        logger.info("贴现台账客户数 结束,返回参数为:[{}]", updateBatBizCusCount09);
//
//        logger.info("重命名创建和删除 临时表-主管客户经理和开证台账客户数量关系表 开始,请求参数为:[{}]", openDay);
//        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_cus_count_acc_tf_loc");
//        logger.info("重命名创建和删除 临时表-主管客户经理和开证台账客户数量关系表 结束");
//        logger.info("插入 临时表-主管客户经理和开证台账客户数量关系表 开始,请求参数为:[{}]", openDay);
//        int insertTmpCusCountAccTfLoc = cmis0117Mapper.insertTmpCusCountAccTfLoc(openDay);
//        logger.info("插入 临时表-主管客户经理和开证台账客户数量关系表 结束,返回参数为:[{}]", insertTmpCusCountAccTfLoc);
//        tableUtilsService.analyzeTable("cmis_batch","tmp_cus_count_acc_tf_loc");//分析表
//
//        logger.info("开证台账客户数 开始,请求参数为:[{}]", openDay);
//        int updateBatBizCusCount10 = cmis0117Mapper.updateBatBizCusCount10(openDay);
//        logger.info("开证台账客户数 结束,返回参数为:[{}]", updateBatBizCusCount10);
//        logger.info("非贷款客户数 客户数等于 acc_accp 银承台账客户数+ acc_cvrs 保函台账客户数 + acc_disc 贴现台账客户数 + acc_tf_loc 开证台账客户数  结束 ");
//





        logger.info("1 理临时表开始,请求参数为:[{}]", openDay);
        int deletetmpUPdateData4ForCountcus = cmis0117Mapper.deletetmpUPdateData4ForCountcus(openDay);
        logger.info("1 理临时表 结束,返回参数为:[{}]", deletetmpUPdateData4ForCountcus);




        logger.info("2 插入贷款与表外正常未结清的客户数据 开始,请求参数为:[{}]", openDay);
        int inserttmpUPdateData4ForCountcus = cmis0117Mapper.inserttmpUPdateData4ForCountcus(openDay);
        logger.info("2 插入贷款与表外正常未结清的客户数据 结束,返回参数为:[{}]", inserttmpUPdateData4ForCountcus);


        logger.info("3 插入客户数量统计表开始,请求参数为:[{}]", openDay);
        int insertBatBizCusCount = cmis0117Mapper.insertBatBizCusCount(openDay);
        logger.info("3 插入客户数量统计表结束,返回参数为:[{}]", insertBatBizCusCount);
        tableUtilsService.analyzeTable("cmis_biz","bat_biz_cus_count");//分析表


        logger.info("  4 统计小企业户数 开始,请求参数为:[{}]", openDay);
        int insertTmpCusCountCorpSmcon1 = cmis0117Mapper.insertTmpCusCountCorpSmcon1(openDay);
        logger.info(" 4 统计小企业户数 结束,返回参数为:[{}]", insertTmpCusCountCorpSmcon1);
        tableUtilsService.analyzeTable("cmis_batch","tmp_cus_count_corp_smcon1");//分析表


        logger.info("5 更新小企业客户数量 开始,请求参数为:[{}]", openDay);
        int updateBatBizCusCount04 = cmis0117Mapper.updateBatBizCusCount04(openDay);
        logger.info("5 更新小企业客户数量 结束,返回参数为:[{}]", updateBatBizCusCount04);


        logger.info("  6 插入  当前管户 总融资户数 开始,请求参数为:[{}]", openDay);
        int insertTmpCusCountCorpSmcon0 = cmis0117Mapper.insertTmpCusCountCorpSmcon0(openDay);
        logger.info("  6 插入  当前管户 总融资户数 结束,返回参数为:[{}]", insertTmpCusCountCorpSmcon0);
        tableUtilsService.analyzeTable("cmis_batch","tmp_cus_count_corp_smcon0");//分析表



        logger.info(" 7 更新 当前管户 总融资户数 开始,请求参数为:[{}]", openDay);
        int updateBatBizCusCount01 = cmis0117Mapper.updateBatBizCusCount01(openDay);
        logger.info(" 7 更新 当前管户总融资户数 结束,返回参数为:[{}]", updateBatBizCusCount01);




        logger.info(" 8 当前客户经理下的公司类融资户数= 总融资户数 减去  小企业融资户数 开始,请求参数为:[{}]", openDay);
        int updateBatBizCusCount03 = cmis0117Mapper.updateBatBizCusCount03(openDay);
        logger.info(" 8 当前客户经理下的公司类融资户数= 总融资户数 减去  小企业融资户数 结束,返回参数为:[{}]", updateBatBizCusCount03);





        logger.info("  9 当前客户经理下的公司类融资户数（小微与零售客户经理下）直接更新为0 开始,请求参数为:[{}]", openDay);
        int updateBatBizCusCount03SmallAndPer = cmis0117Mapper.updateBatBizCusCount03SmallAndPer(openDay);
        logger.info("  9 当前客户经理下的公司类融资户数（小微与零售客户经理下）直接更新为0 结束,返回参数为:[{}]", updateBatBizCusCount03SmallAndPer);



        logger.info(" 10  统计贷款户数 开始,请求参数为:[{}]", openDay);
        int insertTmpCusCountAccLoan = cmis0117Mapper.insertTmpCusCountAccLoan(openDay);
        logger.info("10  统计贷款户数 结束,返回参数为:[{}]", insertTmpCusCountAccLoan);
        tableUtilsService.analyzeTable("cmis_batch","tmp_cus_count_acc_loan");//分析表

        logger.info("11 更新贷款客户数  开始,请求参数为:[{}]", openDay);
        int updateBatBizCusCount06 = cmis0117Mapper.updateBatBizCusCount06(openDay);
        logger.info("11 更新贷款客户数 结束,返回参数为:[{}]", updateBatBizCusCount06);



        logger.info(" 12 非贷款客户数 =总融资户数 减去  贷款户数 开始,请求参数为:[{}]", openDay);
        int updateBatBizCusCount = cmis0117Mapper.updateBatBizCusCount(openDay);
        logger.info("  12 非贷款客户数 =总融资户数 减去  贷款户数 结束,返回参数为:[{}]", updateBatBizCusCount);

    }
}
