package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis020BMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS020B</br>
 * 任务名称：加工任务-额度处理-更新台账占用合同关系处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis020BService {
    private static final Logger logger = LoggerFactory.getLogger(Cmis020BService.class);
    @Autowired
    private Cmis020BMapper cmis020BMapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新合同占用关系信息加工
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis020BUpdateContAccRel(String openDay) {
        logger.info("deleteTempAccLoan020B删除贷款台账信息表020B开始");
        // cmis020BMapper.deleteTempAccLoan020B();// cmis_biz.temp_acc_loan_020B
        tableUtilsService.renameCreateDropTable("cmis_biz", "temp_acc_loan_020B");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTempAccLoan020B删除贷款台账信息表020B结束");

        logger.info("插入贷款台账信息表020B开始,请求参数为:[{}]", openDay);
        int insertTempAccLoan020B = cmis020BMapper.insertTempAccLoan020B(openDay);
        logger.info("插入贷款台账信息表020B结束,返回参数为:[{}]", insertTempAccLoan020B);

        logger.info("deleteTmpDealContAccRel020B删除合同占用关系信息加工表020B开始");
        // cmis020BMapper.deleteTmpDealContAccRel020B();// cmis_lmt.tmp_deal_cont_acc_rel_020B
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_deal_cont_acc_rel_020B");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTmpDealContAccRel020B删除合同占用关系信息加工表020B结束");

        logger.info("插入合同占用关系信息加工表020B开始,请求参数为:[{}]", openDay);
        int insertTmpDealContAccRel020B = cmis020BMapper.insertTmpDealContAccRel020B(openDay);
        logger.info("插入合同占用关系信息加工表020B结束,返回参数为:[{}]", insertTmpDealContAccRel020B);

        logger.info("银承合同产生垫款插入 tmp_deal_cont_acc_rel开始 ,请求参数为:[{}]", openDay);
        int insertTdcarYcht = cmis020BMapper.insertTdcarYcht(openDay);
        logger.info("银承合同产生垫款插入 tmp_deal_cont_acc_rel结束,返回参数为:[{}]", insertTdcarYcht);

        logger.info("最高额授信协议下银承产生垫款插入开始,请求参数为:[{}]", openDay);
        int insertTdcarZgesxxy = cmis020BMapper.insertTdcarZgesxxy(openDay);
        logger.info("最高额授信协议下银承产生垫款插入结束,返回参数为:[{}]", insertTdcarZgesxxy);

        logger.info("最高额授信协议下银承产生垫款插入开始,请求参数为:[{}]", openDay);
        int insertTdcarZgeYc = cmis020BMapper.insertTdcarZgeYc(openDay);
        logger.info("最高额授信协议下银承产生垫款插入结束,返回参数为:[{}]", insertTdcarZgeYc);


        logger.info("deleteLmtContAccRel4A清空临时表-更新cont_acc_rel贷款与垫款占用金额和余额开始,请求参数为:[{}]", openDay);
        // cmis020BMapper.deleteLmtContAccRel4A();// cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteLmtContAccRel4A清空临时表-更新cont_acc_rel贷款与垫款占用金额和余额结束");

        logger.info("插入临时表-更新cont_acc_rel贷款与垫款占用金额和余额开始,请求参数为:[{}]", openDay);
        int insertLmtContAccRel4A = cmis020BMapper.insertLmtContAccRel4A(openDay);
        logger.info("插入临时表-更新cont_acc_rel贷款与垫款占用金额和余额结束,返回参数为:[{}]", insertLmtContAccRel4A);

        logger.info("更新cont_acc_rel贷款占用金额和余额开始,请求参数为:[{}]", openDay);
        int updateLmtContAccRel4A = cmis020BMapper.updateLmtContAccRel4A(openDay);
        logger.info("更新cont_acc_rel贷款占用金额和余额结束,返回参数为:[{}]", updateLmtContAccRel4A);


        logger.info("truncateTdcarPla清空临时表 更新cont_acc_rel在途贷款占用金额和余额开始,请求参数为:[{}]", openDay);
        // cmis020BMapper.truncateTdcarPla();// cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTdcarPla清空临时表 更新cont_acc_rel在途贷款占用金额和余额结束");


        logger.info("deleteTempPvpLoanApp020B删除贷款出账申请表开始");
        // cmis020BMapper.deleteTempPvpLoanApp020B();// cmis_biz.temp_pvp_loan_app_020B
        tableUtilsService.renameCreateDropTable("cmis_biz", "temp_pvp_loan_app_020B");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTempPvpLoanApp020B删除贷款出账申请表结束");

        logger.info("插入贷款出账申请表开始,请求参数为:[{}]", openDay);
        int insertTempPvpLoanApp020B = cmis020BMapper.insertTempPvpLoanApp020B(openDay);
        logger.info("插入贷款出账申请表结束,返回参数为:[{}]", insertTempPvpLoanApp020B);

        logger.info("插入临时表 更新cont_acc_rel在途贷款占用金额和余额开始,请求参数为:[{}]", openDay);
        int insertTdcarPla = cmis020BMapper.insertTdcarPla(openDay);
        logger.info("插入临时表 更新cont_acc_rel在途贷款占用金额和余额结束,返回参数为:[{}]", insertTdcarPla);


        logger.info("更新cont_acc_rel在途贷款占用金额和余额开始,请求参数为:[{}]", openDay);
        int updateTdcarPla = cmis020BMapper.updateTdcarPla(openDay);
        logger.info("更新cont_acc_rel在途贷款占用金额和余额结束,返回参数为:[{}]", updateTdcarPla);

        logger.info("truncateTdcarAcl清空临时表 更新cont_acc_rel 委托贷款占用金额和余额开始,请求参数为:[{}]", openDay);
        // cmis020BMapper.truncateTdcarAcl();// cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTdcarAcl清空临时表 更新cont_acc_rel 委托贷款占用金额和余额结束");

        logger.info("插入临时表 更新cont_acc_rel 委托贷款占用金额和余额开始,请求参数为:[{}]", openDay);
        int insertTdcarAcl = cmis020BMapper.insertTdcarAcl(openDay);
        logger.info("插入临时表 更新cont_acc_rel 委托贷款占用金额和余额结束,返回参数为:[{}]", insertTdcarAcl);

        logger.info("更新cont_acc_rel 委托贷款占用金额和余额开始,请求参数为:[{}]", openDay);
        int updateTdcarAcl = cmis020BMapper.updateTdcarAcl(openDay);
        logger.info("更新cont_acc_rel 委托贷款占用金额和余额结束,返回参数为:[{}]", updateTdcarAcl);

        logger.info("truncateTdcarPela清空临时表 更新cont_acc_rel 在途委托贷款申请占用金额和余额开始,请求参数为:[{}]", openDay);
        // cmis020BMapper.truncateTdcarPela();// cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTdcarPela清空临时表 更新cont_acc_rel 在途委托贷款申请占用金额和余额结束");

        logger.info("插入临时表 更新cont_acc_rel 在途委托贷款申请占用金额和余额开始,请求参数为:[{}]", openDay);
        int insertTdcarPela = cmis020BMapper.insertTdcarPela(openDay);
        logger.info("插入临时表 更新cont_acc_rel 在途委托贷款申请占用金额和余额结束,返回参数为:[{}]", insertTdcarPela);

        logger.info("更新cont_acc_rel 在途委托贷款申请占用金额和余额开始,请求参数为:[{}]", openDay);
        int updateTdcarPela = cmis020BMapper.updateTdcarPela(openDay);
        logger.info("更新cont_acc_rel 在途委托贷款申请占用金额和余额结束,返回参数为:[{}]", updateTdcarPela);

        logger.info("truncateTdcarAa清空临时表 更新cont_acc_rel 银承占用金额和余额开始,请求参数为:[{}]", openDay);
        // cmis020BMapper.truncateTdcarAa();// cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTdcarAa清空临时表 更新cont_acc_rel 银承占用金额和余额结束");

        logger.info("deleteTempAccAccpDrftSub020b删除银承台账票据明细020b开始");
        // cmis020BMapper.deleteTempAccAccpDrftSub020b();// cmis_biz.temp_acc_accp_drft_sub_020b
        tableUtilsService.renameCreateDropTable("cmis_biz", "temp_acc_accp_drft_sub_020b");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTempAccAccpDrftSub020b删除银承台账票据明细020b结束");

        logger.info("插入银承台账票据明细020b开始,请求参数为:[{}]", openDay);
        int insertTempAccAccpDrftSub020b = cmis020BMapper.insertTempAccAccpDrftSub020b(openDay);
        logger.info("插入银承台账票据明细020b结束,返回参数为:[{}]", insertTempAccAccpDrftSub020b);

        logger.info("插入临时表 更新cont_acc_rel 银承占用金额和余额开始,请求参数为:[{}]", openDay);
        int insertTdcarAa = cmis020BMapper.insertTdcarAa(openDay);
        logger.info("插入临时表 更新cont_acc_rel 银承占用金额和余额结束,返回参数为:[{}]", insertTdcarAa);

        logger.info("更新cont_acc_rel  银承占用金额和余额开始,请求参数为:[{}]", openDay);
        int updateTdcarAa = cmis020BMapper.updateTdcarAa(openDay);
        logger.info("更新cont_acc_rel  银承占用金额和余额结束,返回参数为:[{}]", updateTdcarAa);


        logger.info("更新cont_acc_rel  银承占用总金额和总敞口金额，是剔除作废与未用退回的票据 开始,请求参数为:[{}]", openDay);
        int updateContAccRelAccTotalAmtAndSpanAmt = cmis020BMapper.updateContAccRelAccTotalAmtAndSpanAmt(openDay);
        logger.info("更新cont_acc_rel  银承占用总金额和总敞口金额，是剔除作废与未用退回的票据 结束,返回参数为:[{}]", updateContAccRelAccTotalAmtAndSpanAmt);


        logger.info("清空临时表 更新cont_acc_rel 在途银承申请占用金额和余额开始,请求参数为:[{}]", openDay);
        // cmis020BMapper.deleteTdcarPaa();// cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空临时表 更新cont_acc_rel 在途银承申请占用金额和余额结束");

        logger.info("插入临时表 更新cont_acc_rel 在途银承申请占用金额和余额开始,请求参数为:[{}]", openDay);
        int insertTdcarPaa = cmis020BMapper.insertTdcarPaa(openDay);
        logger.info("插入临时表 更新cont_acc_rel 在途银承申请占用金额和余额结束,返回参数为:[{}]", insertTdcarPaa);

        logger.info("更新cont_acc_rel  在途银承申请占用金额和余额开始,请求参数为:[{}]", openDay);
        int updateTdcarPaa = cmis020BMapper.updateTdcarPaa(openDay);
        logger.info("更新cont_acc_rel  在途银承申请占用金额和余额结束,返回参数为:[{}]", updateTdcarPaa);

        logger.info("truncateTdcarAc清空临时表 更新cont_acc_rel 保函占用金额和余额开始,请求参数为:[{}]", openDay);
        // cmis020BMapper.truncateTdcarAc();// cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTdcarAc清空临时表 更新cont_acc_rel 保函占用金额和余额结束");

        logger.info("插入临时表 更新cont_acc_rel 保函占用金额和余额开始,请求参数为:[{}]", openDay);
        int insertTdcarAc = cmis020BMapper.insertTdcarAc(openDay);
        logger.info("插入临时表 更新cont_acc_rel 保函占用金额和余额结束,返回参数为:[{}]", insertTdcarAc);

        logger.info("更新cont_acc_rel  保函占用金额和余额开始,请求参数为:[{}]", openDay);
        int updateTdcarAc = cmis020BMapper.updateTdcarAc(openDay);
        logger.info("更新cont_acc_rel  保函占用金额和余额结束,返回参数为:[{}]", updateTdcarAc);

        logger.info("deleteTdcarAtl清空临时表 更新cont_acc_rel 信用证占用金额和余额开始,请求参数为:[{}]", openDay);
        // cmis020BMapper.deleteTdcarAtl();//cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTdcarAtl清空临时表 更新cont_acc_rel 信用证占用金额和余额结束");

        logger.info("插入临时表 更新cont_acc_rel 信用证占用金额和余额开始,请求参数为:[{}]", openDay);
        int insertTdcarAtl = cmis020BMapper.insertTdcarAtl(openDay);
        logger.info("插入临时表 更新cont_acc_rel 信用证占用金额和余额结束,返回参数为:[{}]", insertTdcarAtl);

        logger.info("更新cont_acc_rel  信用证占用金额和余额开始,请求参数为:[{}]", openDay);
        int updateTdcarAtl = cmis020BMapper.updateTdcarAtl(openDay);
        logger.info("更新cont_acc_rel  信用证占用金额和余额结束,返回参数为:[{}]", updateTdcarAtl);

        logger.info("truncateTdcarAd清空临时表 更新cont_acc_rel 贴现占用金额和余额开始,请求参数为:[{}]", openDay);
        // cmis020BMapper.truncateTdcarAd();// cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTdcarAd清空临时表 更新cont_acc_rel 贴现占用金额和余额结束");

        logger.info("插入临时表 更新cont_acc_rel 贴现占用金额和余额开始,请求参数为:[{}]", openDay);
        int insertTdcarAd = cmis020BMapper.insertTdcarAd(openDay);
        logger.info("插入临时表 更新cont_acc_rel 贴现占用金额和余额结束,返回参数为:[{}]", insertTdcarAd);

        logger.info("更新cont_acc_rel  贴现占用金额和余额开始,请求参数为:[{}]", openDay);
        int updateTdcarAd = cmis020BMapper.updateTdcarAd(openDay);
        logger.info("更新cont_acc_rel  贴现占用金额和余额结束,返回参数为:[{}]", updateTdcarAd);

        logger.info("更新cont_acc_rel  注销和作废的占用关系占用余额为0开始,请求参数为:[{}]", openDay);
        int updateTdcar = cmis020BMapper.updateTdcar(openDay);
        logger.info("更新cont_acc_rel  注销和作废的占用关系占用余额为0结束,返回参数为:[{}]", updateTdcar);

        logger.info("若担保方式为低风险担保，则更新占用敞口为0开始,请求参数为:[{}]", openDay);
        int updateTdcarGuarMode = cmis020BMapper.updateTdcarGuarMode(openDay);
        logger.info("若担保方式为低风险担保，则更新占用敞口为0结束,返回参数为:[{}]", updateTdcarGuarMode);

        logger.info("已到期的台账占用关系，若占用状态还是生效，则占用状态改为到期未结清开始,请求参数为:[{}]", openDay);
        int updateContARToSX = cmis020BMapper.updateContARToSX(openDay);
        logger.info("已到期的台账占用关系，若占用状态还是生效，则占用状态改为到期未结清结束,返回参数为:[{}]", updateContARToSX);
    }
}
