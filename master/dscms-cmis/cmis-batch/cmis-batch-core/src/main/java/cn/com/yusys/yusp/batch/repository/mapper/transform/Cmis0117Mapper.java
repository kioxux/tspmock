package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0117</br>
 * 任务名称：加工任务-业务处理-统计报表生成 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0117Mapper {
    /**
     * 删除资产管理统计表非月末的数据
     *
     * @param openDay
     * @return
     */
    int deleteBatBizAssetsAnalyse(@Param("openDay") String openDay);


    /**
     * 插入贷款台账信息表0117
     *
     * @param openDay
     * @return
     */
    int insertTempAccLoan0117(@Param("openDay") String openDay);//add 20211019

    /**
     * 插入资产管理统计表
     *
     * @param openDay
     * @return
     */
    int insertBatBizAssetsAnalyse(@Param("openDay") String openDay);


    /**
     * 插入不良资产统计表
     *
     * @param openDay
     * @return
     */
    int insertBatBizBadAssets(@Param("openDay") String openDay);

    /** 1
     *清理临时表
     *
     * @param openDay
     * @return
     */
    int deletetmpUPdateData4ForCountcus(@Param("openDay") String openDay);

    /** 2
     *  插入贷款与表外正常未结清的客户数据
     *
     * @param openDay
     * @return
     */
    int inserttmpUPdateData4ForCountcus(@Param("openDay") String openDay);




    /** 3
     * 插入客户数量统计表
     *
     * @param openDay
     * @return
     */
    int insertBatBizCusCount(@Param("openDay") String openDay);

    /** 4
     * 4 统计小企业户数
     *
     * @param openDay
     * @return
     */
    int insertTmpCusCountCorpSmcon1(@Param("openDay") String openDay);

    /** 5
     * 更新小企业户数
     *
     * @param openDay
     * @return
     */
    int updateBatBizCusCount04(@Param("openDay") String openDay);


    /**
     * 6 插入  当前管户总融资户数
     *
     * @param openDay
     * @return
     */
    int insertTmpCusCountCorpSmcon0(@Param("openDay") String openDay);


    /** 7
     * 更新 当前管户总融资户数
     *
     * @param openDay
     * @return
     */
    int updateBatBizCusCount01(@Param("openDay") String openDay);






    /**
     * 8 当前客户经理下的公司类融资户数= 总融资户数 减去  小企业融资户数
     *
     * @param openDay
     * @return
     */
    int updateBatBizCusCount03(@Param("openDay") String openDay);



    /**
     *    9 当前客户经理下的公司类融资户数（小微与零售客户经理下）直接更新为0
     *
     * @param openDay
     * @return
     */
    int updateBatBizCusCount03SmallAndPer(@Param("openDay") String openDay);



    /**
     * 10  统计贷款户数
     *
     * @param openDay
     * @return
     */
    int insertTmpCusCountAccLoan(@Param("openDay") String openDay);

    /**
     * 11 更新贷款客户数
     *
     * @param openDay
     * @return
     */
    int updateBatBizCusCount06(@Param("openDay") String openDay);



    /**
     * 12 非贷款客户数 =总融资户数 减去  贷款户数
     *
     * @param openDay
     * @return
     */
    int updateBatBizCusCount(@Param("openDay") String openDay);




}
