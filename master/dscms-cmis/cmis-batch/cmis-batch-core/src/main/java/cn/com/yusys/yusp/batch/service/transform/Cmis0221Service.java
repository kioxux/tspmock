package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0221Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0221</br>
 * 加工任务-额度处理-将额度相关临时表更新到额度相关表</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0221Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0221Service.class);
    @Autowired
    private Cmis0221Mapper cmis0221Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 根据授信分项额度比对表处理原始表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0221LmtTmpAlsbiTcalsbi(String openDay) {
        logger.info("truncateLmtTmpAlsbiTcalsbi清空批复额度分项额度比对开始,请求参数为:[{}]", openDay);
        // cmis0221Mapper.truncateLmtTmpAlsbiTcalsbi();//cmis_lmt.tmp_alsbi_tcalsbi
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_alsbi_tcalsbi");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateLmtTmpAlsbiTcalsbi清空批复额度分项额度比对结束");

        logger.info("插入批复额度分项额度比对开始,请求参数为:[{}]", openDay);
        int insertLmtTmpAlsbiTcalsbi = cmis0221Mapper.insertLmtTmpAlsbiTcalsbi(openDay);
        logger.info("插入批复额度分项额度比对结束,返回参数为:[{}]", insertLmtTmpAlsbiTcalsbi);

        logger.info("根据批复额度分项额度比对更新批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        int updateByLmtTmpAlsbiTcalsbi = cmis0221Mapper.updateByLmtTmpAlsbiTcalsbi(openDay);
        logger.info("根据批复额度分项额度比对更新批复额度分项基础信息结束,返回参数为:[{}]", updateByLmtTmpAlsbiTcalsbi);
    }

    /**
     * 根据授信分项占用关系额度比对表处理原始表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0221LmtTmpLcrTclcr(String openDay) {
        logger.info("清空分项占用关系信息比对开始,请求参数为:[{}]", openDay);
        // cmis0221Mapper.truncateLmtTmpLcrTclcr();// cmis_lmt.tmp_lcr_tclcr
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_lcr_tclcr");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空分项占用关系信息比对结束");

        logger.info("插入分项占用关系信息比对开始,请求参数为:[{}]", openDay);
        int insertLmtTmpLcrTclcr = cmis0221Mapper.insertLmtTmpLcrTclcr(openDay);
        logger.info("插入分项占用关系信息比对结束,返回参数为:[{}]", insertLmtTmpLcrTclcr);

        logger.info("根据分项占用关系信息比对更新分项占用关系信息开始,请求参数为:[{}]", openDay);
        int updateByLmtTmpLcrTclcr = cmis0221Mapper.updateByLmtTmpLcrTclcr(openDay);
        logger.info("根据分项占用关系信息比对更新分项占用关系信息结束,返回参数为:[{}]", updateByLmtTmpLcrTclcr);

        logger.info("删除-新信贷-临时表-额度通用临时表开始");
        // cmis0221Mapper.deleteTmpTdcarAl();//cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("删除-新信贷-临时表-额度通用临时表结束");

        logger.info("插入-新信贷-临时表-额度通用临时表开始,请求参数为:[{}]", openDay);
        int insertTmpTdcarAl = cmis0221Mapper.insertTmpTdcarAl(openDay);
        logger.info("插入-新信贷-临时表-额度通用临时表结束,返回参数为:[{}]", insertTmpTdcarAl);

        logger.info("更新分项占用关系信息开始,请求参数为:[{}]", openDay);
        int updateLmtContRelLmt = cmis0221Mapper.updateLmtContRelLmt(openDay);
        logger.info("更新分项占用关系信息结束,返回参数为:[{}]", updateLmtContRelLmt);

        logger.info("ComStar新做的业务插入lmt_cont_rel开始,请求参数为:[{}]", openDay);
        int insertComstarLmtContRel = cmis0221Mapper.insertComstarLmtContRel(openDay);
        logger.info("ComStar新做的业务插入lmt_cont_rel结束,返回参数为:[{}]", insertComstarLmtContRel);
    }


    /**
     * 根据台账占用合同关系额度比对表处理原始表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0221LmtTmpCarTccar(String openDay) {


        logger.info("更细分项占用关系信息开始,请求参数为:[{}]", openDay);
        int updateLmtContRel = cmis0221Mapper.updateLmtContRel(openDay);
        logger.info("更细分项占用关系信息结束,返回参数为:[{}]", updateLmtContRel);

        logger.info("truncateLmtTmpCarTccar清空合同占用关系额度比对开始,请求参数为:[{}]", openDay);
        // cmis0221Mapper.truncateLmtTmpCarTccar();// cmis_lmt.tmp_car_tccar
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_car_tccar");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateLmtTmpCarTccar清空合同占用关系额度比对结束");

        logger.info("插入合同占用关系额度比对开始,请求参数为:[{}]", openDay);
        int insertLmtTmpCarTccar = cmis0221Mapper.insertLmtTmpCarTccar(openDay);
        logger.info("插入合同占用关系额度比对结束,返回参数为:[{}]", insertLmtTmpCarTccar);

        logger.info("根据合同占用关系额度比对更新合同占用关系信息开始,请求参数为:[{}]", openDay);
        int updateByLmtTmpCarTccar = cmis0221Mapper.updateByLmtTmpCarTccar(openDay);
        logger.info("根据合同占用关系额度比对更新合同占用关系信息结束,返回参数为:[{}]", updateByLmtTmpCarTccar);

        // 20211102  解决insertLmtTmpCarTccar2主键冲突问题,修改表为tmp_tdcar_al 开始
        logger.info("truncateLmtTmpCarTccar2清空合同占用关系额度比对开始,请求参数为:[{}]", openDay);
        // cmis_batch.tmp_tdcar_al
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_tdcar_al");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("truncateLmtTmpCarTccar2清空合同占用关系额度比对结束");
        // 20211102  解决insertLmtTmpCarTccar2主键冲突问题,修改表为tmp_tdcar_al 结束

        logger.info("插入台账占用关系额度比对（更新状态）开始,请求参数为:[{}]", openDay);
        int insertLmtTmpCarTccar2 = cmis0221Mapper.insertLmtTmpCarTccar2(openDay);
        logger.info("插入台账占用关系额度比对（更新状态）结束,返回参数为:[{}]", insertLmtTmpCarTccar2);

        logger.info("根据台账占用关系额度比对更新合同占用关系信息（更新状态）开始,请求参数为:[{}]", openDay);
        int updateByLmtTmpCarTccar2 = cmis0221Mapper.updateByLmtTmpCarTccar2(openDay);
        logger.info("根据台账占用关系额度比对更新合同占用关系信息（更新状态）结束,返回参数为:[{}]", updateByLmtTmpCarTccar2);

        logger.info("根据台账占用关系额度比对更新合同占用关系信息（更新状态）开始,请求参数为:[{}]", openDay);
        int updateByLmtTmpCarTccar20 = cmis0221Mapper.updateByLmtTmpCarTccar20(openDay);
        logger.info("根据台账占用关系额度比对更新合同占用关系信息结束,返回参数为:[{}]", updateByLmtTmpCarTccar2);

    }


    /**
     * 根据白名单额度比对表处理原始表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0221LmtTmpLwiTclwi(String openDay) {
        logger.info("truncateLmtTmpLwiTclwi清空白名单额度信息额度比对开始,请求参数为:[{}]", openDay);
        // cmis0221Mapper.truncateLmtTmpLwiTclwi();//cmis_lmt.tmp_lwi_tclwi
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_lwi_tclwi");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateLmtTmpLwiTclwi清空白名单额度信息额度比对结束");

        logger.info("插入白名单额度信息额度比对开始,请求参数为:[{}]", openDay);
        int insertLmtTmpLwiTclwi = cmis0221Mapper.insertLmtTmpLwiTclwi(openDay);
        logger.info("插入白名单额度信息额度比对结束,返回参数为:[{}]", insertLmtTmpLwiTclwi);

        logger.info("根据白名单额度信息额度比对更新白名单额度信息表开始,请求参数为:[{}]", openDay);
        int updateByLmtTmpLwiTclwi = cmis0221Mapper.updateByLmtTmpLwiTclwi(openDay);
        logger.info("根据白名单额度信息额度比对更新白名单额度信息表结束,返回参数为:[{}]", updateByLmtTmpLwiTclwi);
    }

    /**
     * 根据合作方额度比表处理原始表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0221LmtTmpAcsiTcacsi(String openDay) {
        logger.info("truncateLmtTmpAcsiTcacsi清空合作方授信分项信息额度比对开始,请求参数为:[{}]", openDay);
        // cmis0221Mapper.truncateLmtTmpAcsiTcacsi();//cmis_lmt.tmp_acsi_tcacsi
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_acsi_tcacsi");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateLmtTmpAcsiTcacsi清空合作方授信分项信息额度比对结束");

        logger.info("插入合作方授信分项信息额度比对开始,请求参数为:[{}]", openDay);
        int insertLmtTmpAcsiTcacsi = cmis0221Mapper.insertLmtTmpAcsiTcacsi(openDay);
        logger.info("插入合作方授信分项信息额度比对结束,返回参数为:[{}]", insertLmtTmpAcsiTcacsi);

        logger.info("根据合作方授信分项信息额度比对更新合作方授信分项信息开始,请求参数为:[{}]", openDay);
        int updateByLmtTmpAcsiTcacsi = cmis0221Mapper.updateByLmtTmpAcsiTcacsi(openDay);
        logger.info("根据合作方授信分项信息额度比对更新合作方授信分项信息结束,返回参数为:[{}]", updateByLmtTmpAcsiTcacsi);
    }
}
