/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.rcp;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpAntLoanDetailTemp;
import cn.com.yusys.yusp.batch.repository.mapper.load.rcp.BatSRcpAntLoanDetailTempMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpAntLoanDetailTempService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:02:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSRcpAntLoanDetailTempService {

    private static final Logger logger = LoggerFactory.getLogger(BatSRcpAntLoanDetailTempService.class);

    @Autowired
    private BatSRcpAntLoanDetailTempMapper batSRcpAntLoanDetailTempMapper;
    @Autowired
    private TableUtilsService tableUtilsService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSRcpAntLoanDetailTemp selectByPrimaryKey(String contractNo) {
        return batSRcpAntLoanDetailTempMapper.selectByPrimaryKey(contractNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSRcpAntLoanDetailTemp> selectAll(QueryModel model) {
        List<BatSRcpAntLoanDetailTemp> records = (List<BatSRcpAntLoanDetailTemp>) batSRcpAntLoanDetailTempMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSRcpAntLoanDetailTemp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSRcpAntLoanDetailTemp> list = batSRcpAntLoanDetailTempMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSRcpAntLoanDetailTemp record) {
        return batSRcpAntLoanDetailTempMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSRcpAntLoanDetailTemp record) {
        return batSRcpAntLoanDetailTempMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSRcpAntLoanDetailTemp record) {
        return batSRcpAntLoanDetailTempMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSRcpAntLoanDetailTemp record) {
        return batSRcpAntLoanDetailTempMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String contractNo) {
        return batSRcpAntLoanDetailTempMapper.deleteByPrimaryKey(contractNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSRcpAntLoanDetailTempMapper.deleteByIds(ids);
    }

    /**
     * 将T表数据merge到S表
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]开始,请求参数为:{}", openDay);
        int updateSResult = batSRcpAntLoanDetailTempMapper.updateDataDateByOpenDay(openDay);
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]结束,返回参数为:{}", updateSResult);
    }

    /**
     * 删除S表当天数据
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建蚂蚁借呗放款（合约）明细临时表[bat_s_rcp_ant_loan_detail_temp]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_rcp_ant_loan_detail_temp");
        logger.info("重建蚂蚁借呗放款（合约）明细临时表[bat_s_rcp_ant_loan_detail_temp]结束");
        return 0;
    }
}
