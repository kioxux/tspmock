/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.domain.job;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobExecution
 * @类描述: batch_job_execution数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "batch_job_execution")
public class BatchJobExecution extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 作业执行器ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "JOB_EXECUTION_ID")
	private Long jobExecutionId;
	
	/** 版本号 **/
	@Column(name = "VERSION", unique = false, nullable = true, length = 19)
	private Long version;
	
	/** 作业实例编号 **/
	@Column(name = "JOB_INSTANCE_ID", unique = false, nullable = false, length = 19)
	private Long jobInstanceId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date createTime;
	
	/** 执行器开始执行时间 **/
	@Column(name = "START_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date startTime;
	
	/** 执行器结束执行时间 **/
	@Column(name = "END_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date endTime;
	
	/** 执行的状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 10)
	private String status;
	
	/** 退出编码 **/
	@Column(name = "EXIT_CODE", unique = false, nullable = true, length = 2500)
	private String exitCode;
	
	/** 退出描述 **/
	@Column(name = "EXIT_MESSAGE", unique = false, nullable = true, length = 2500)
	private String exitMessage;
	
	/** 上次更新时间 **/
	@Column(name = "LAST_UPDATED", unique = false, nullable = true, length = 19)
	private java.util.Date lastUpdated;
	
	/** 作业配置文件位置 **/
	@Column(name = "JOB_CONFIGURATION_LOCATION", unique = false, nullable = true, length = 2500)
	private String jobConfigurationLocation;
	
	
	/**
	 * @param jobExecutionId
	 */
	public void setJobExecutionId(Long jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}
	
    /**
     * @return jobExecutionId
     */
	public Long getJobExecutionId() {
		return this.jobExecutionId;
	}
	
	/**
	 * @param version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}
	
    /**
     * @return version
     */
	public Long getVersion() {
		return this.version;
	}
	
	/**
	 * @param jobInstanceId
	 */
	public void setJobInstanceId(Long jobInstanceId) {
		this.jobInstanceId = jobInstanceId;
	}
	
    /**
     * @return jobInstanceId
     */
	public Long getJobInstanceId() {
		return this.jobInstanceId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param startTime
	 */
	public void setStartTime(java.util.Date startTime) {
		this.startTime = startTime;
	}
	
    /**
     * @return startTime
     */
	public java.util.Date getStartTime() {
		return this.startTime;
	}
	
	/**
	 * @param endTime
	 */
	public void setEndTime(java.util.Date endTime) {
		this.endTime = endTime;
	}
	
    /**
     * @return endTime
     */
	public java.util.Date getEndTime() {
		return this.endTime;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param exitCode
	 */
	public void setExitCode(String exitCode) {
		this.exitCode = exitCode;
	}
	
    /**
     * @return exitCode
     */
	public String getExitCode() {
		return this.exitCode;
	}
	
	/**
	 * @param exitMessage
	 */
	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}
	
    /**
     * @return exitMessage
     */
	public String getExitMessage() {
		return this.exitMessage;
	}
	
	/**
	 * @param lastUpdated
	 */
	public void setLastUpdated(java.util.Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
    /**
     * @return lastUpdated
     */
	public java.util.Date getLastUpdated() {
		return this.lastUpdated;
	}
	
	/**
	 * @param jobConfigurationLocation
	 */
	public void setJobConfigurationLocation(String jobConfigurationLocation) {
		this.jobConfigurationLocation = jobConfigurationLocation;
	}
	
    /**
     * @return jobConfigurationLocation
     */
	public String getJobConfigurationLocation() {
		return this.jobConfigurationLocation;
	}


}