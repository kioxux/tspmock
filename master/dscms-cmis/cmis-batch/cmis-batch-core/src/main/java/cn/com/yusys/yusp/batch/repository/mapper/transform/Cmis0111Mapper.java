package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0111</br>
 * 任务名称：加工任务-业务处理-档案日终处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0111Mapper {
    /**
     * 插入消息提示表：档案归还延期提醒
     *
     * @param openDay
     * @return
     */
    int insertWbMsgNotice01(@Param("openDay") String openDay);


    /**
     * 插入消息提示表：档案归还延期提醒
     *
     * @param openDay
     * @return
     */
    int uptDocReadDetailInfo(@Param("openDay") String openDay);

    /**
     * 更新档案台账列表状态
     *
     * @param openDay
     * @return
     */
    int uptDocAccList(@Param("openDay") String openDay);
}
