/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKcdbHkdjxx;
import cn.com.yusys.yusp.batch.service.load.core.BatSCoreKcdbHkdjxxService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatSCoreKcdbHkdjxxResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 23:46:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batscorekcdbhkdjxx")
public class BatSCoreKcdbHkdjxxResource {
    @Autowired
    private BatSCoreKcdbHkdjxxService batSCoreKcdbHkdjxxService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSCoreKcdbHkdjxx>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSCoreKcdbHkdjxx> list = batSCoreKcdbHkdjxxService.selectAll(queryModel);
        return new ResultDto<List<BatSCoreKcdbHkdjxx>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSCoreKcdbHkdjxx>> index(QueryModel queryModel) {
        List<BatSCoreKcdbHkdjxx> list = batSCoreKcdbHkdjxxService.selectByModel(queryModel);
        return new ResultDto<List<BatSCoreKcdbHkdjxx>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSCoreKcdbHkdjxx> create(@RequestBody BatSCoreKcdbHkdjxx batSCoreKcdbHkdjxx) throws URISyntaxException {
        batSCoreKcdbHkdjxxService.insert(batSCoreKcdbHkdjxx);
        return new ResultDto<BatSCoreKcdbHkdjxx>(batSCoreKcdbHkdjxx);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSCoreKcdbHkdjxx batSCoreKcdbHkdjxx) throws URISyntaxException {
        int result = batSCoreKcdbHkdjxxService.update(batSCoreKcdbHkdjxx);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String farendma, String laokahao, String xinkahao, Long xuhaozhi) {
        int result = batSCoreKcdbHkdjxxService.deleteByPrimaryKey(farendma, laokahao, xinkahao, xuhaozhi);
        return new ResultDto<Integer>(result);
    }

}
