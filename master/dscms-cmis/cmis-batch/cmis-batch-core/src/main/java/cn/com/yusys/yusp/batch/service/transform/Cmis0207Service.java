package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0207Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0207</br>
 * 任务名称：加工任务-额度处理-占用授信总敞口金额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0207Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0207Service.class);
    @Autowired
    private Cmis0207Mapper cmis0207Mapper;

    /**
     * @param openDay
     */
    @Deprecated
    public void cmis0207UpdateLmtContRel(String openDay) {

    }
}
