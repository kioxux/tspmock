package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0120Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0120</br>
 * 任务名称：加工任务-业务处理-零售智能风控蚂蚁借呗 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2021年6月12日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0120Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0120Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0120Service cmis0120Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job cmis0120Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0120_JOB.key, JobStepEnum.CMIS0120_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0120Job = this.jobBuilderFactory.get(JobStepEnum.CMIS0120_JOB.key)
                .start(cmis0120UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0120CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(cmis0120InsertCtrLoanContStep(WILL_BE_INJECTED)) // 插入贷款合同表
                //.next(cmis0120UpdateCtrLoanContStep(WILL_BE_INJECTED)) // 更新贷款合同表
                .next(cmis0120InsertAccLoanStep(WILL_BE_INJECTED)) // 插入贷款台账表
                .next(cmis0120UpdateAccLoanStep(WILL_BE_INJECTED)) // 更新贷款台账表
                .next(cmis0120UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0120Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0120UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0120_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0120_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0120UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0120_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0120_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0120_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0120_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0120UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0120CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0120_CHECK_REL_STEP.key, JobStepEnum.CMIS0120_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0120CheckRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0120_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0120_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0120_CHECK_REL_STEP.key, JobStepEnum.CMIS0120_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0120_CHECK_REL_STEP.key, JobStepEnum.CMIS0120_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0120CheckRelStep;
    }

    /**
     * 插入贷款合同表
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0120InsertCtrLoanContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0120_INSERT_CTR_LOAN_CONT_STEP.key, JobStepEnum.CMIS0120_INSERT_CTR_LOAN_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0120InsertCtrLoanContStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0120_INSERT_CTR_LOAN_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0120Service.cmis0120InsertCtrLoanCont(openDay);//插入贷款合同表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0120_INSERT_CTR_LOAN_CONT_STEP.key, JobStepEnum.CMIS0120_INSERT_CTR_LOAN_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0120InsertCtrLoanContStep;
    }

//    /**
//     * 更新贷款合同表
//     * @param openDay 营业日期
//     * @return
//     */
//    @Bean
//    @JobScope
//    public Step cmis0120UpdateCtrLoanContStep(@Value("#{jobParameters[openDay]}") String openDay) {
//        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0120_UPDATE_CTR_LOAN_CONT_STEP.key, JobStepEnum.CMIS0120_UPDATE_CTR_LOAN_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
//        Step cmis0120UpdateCtrLoanContStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0120_UPDATE_CTR_LOAN_CONT_STEP.key)
//                .tasklet((stepContribution, chunkContext) -> {
//                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
//                    cmis0120Service.cmis0120UpdateCtrLoanCont(openDay);//更新贷款合同表
//                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0120_UPDATE_CTR_LOAN_CONT_STEP.key, JobStepEnum.CMIS0120_UPDATE_CTR_LOAN_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
//                    return RepeatStatus.FINISHED;
//                }).build();
//        return cmis0120UpdateCtrLoanContStep;
//    }

    /**
     * 插入贷款台账表
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0120InsertAccLoanStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0120_INSERT_ACC_LOAN_STEP.key, JobStepEnum.CMIS0120_INSERT_ACC_LOAN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0120InsertAccLoanStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0120_INSERT_ACC_LOAN_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0120Service.cmis0120InsertAccLoan(openDay);//插入贷款台账表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0120_INSERT_ACC_LOAN_STEP.key, JobStepEnum.CMIS0120_INSERT_ACC_LOAN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0120InsertAccLoanStep;
    }

    /**
     * 更新贷款台账表
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0120UpdateAccLoanStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0120_UPDATE_ACC_LOAN_STEP.key, JobStepEnum.CMIS0120_UPDATE_ACC_LOAN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0120UpdateAccLoanStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0120_UPDATE_ACC_LOAN_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0120Service.cmis0120UpdateAccLoan(openDay);//更新贷款台账表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0120_UPDATE_ACC_LOAN_STEP.key, JobStepEnum.CMIS0120_UPDATE_ACC_LOAN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0120UpdateAccLoanStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0120UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0120_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0120_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0120UpdateTask100Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0120_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0120_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0120_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0120_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0120UpdateTask100Step;
    }


}
