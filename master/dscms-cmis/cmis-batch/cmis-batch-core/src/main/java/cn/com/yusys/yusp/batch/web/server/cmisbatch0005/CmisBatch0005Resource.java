package cn.com.yusys.yusp.batch.web.server.cmisbatch0005;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005RespDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0005.CmisBatch0005Service;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:更新客户移交相关表
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CMISBATCH0005:更新客户移交相关表")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0005Resource.class);

    @Autowired
    private CmisBatch0005Service cmisBatch0005Service;

    /**
     * 交易码：cmisbatch0005
     * 交易描述：更新客户移交相关表
     *
     * @param cmisbatch0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("更新客户移交相关表")
    @PostMapping("/cmisbatch0005")
    protected @ResponseBody
    ResultDto<Cmisbatch0005RespDto> cmisbatch0005(@Validated @RequestBody Cmisbatch0005ReqDto cmisbatch0005ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value, JSON.toJSONString(cmisbatch0005ReqDto));
        Cmisbatch0005RespDto cmisbatch0005RespDto = new Cmisbatch0005RespDto();// 响应Dto:更新客户移交相关表
        ResultDto<Cmisbatch0005RespDto> cmisbatch0005ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0005ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value, JSON.toJSONString(cmisbatch0005ReqDto));
            cmisbatch0005RespDto = cmisBatch0005Service.cmisBatch0005(cmisbatch0005ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value, JSON.toJSONString(cmisbatch0005RespDto));
            // 封装cmisbatch0005ResultDto中正确的返回码和返回信息
            cmisbatch0005ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0005ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value, e.getMessage());
            // 封装cmisbatch0005ResultDto中异常返回码和返回信息
            cmisbatch0005ResultDto.setCode(e.getErrorCode());
            cmisbatch0005ResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value, e.getMessage());
            // 封装cmisbatch0005ResultDto中异常返回码和返回信息
            cmisbatch0005ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0005ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0005RespDto到cmisbatch0005ResultDto中
        cmisbatch0005ResultDto.setData(cmisbatch0005RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0005.key, DscmsEnum.TRADE_CODE_CMISBATCH0005.value, JSON.toJSONString(cmisbatch0005ResultDto));
        return cmisbatch0005ResultDto;
    }
}
