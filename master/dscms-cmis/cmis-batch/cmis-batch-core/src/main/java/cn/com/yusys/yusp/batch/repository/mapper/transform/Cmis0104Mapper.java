package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0104</br>
 * 任务名称：加工任务-业务处理-贴现台账处理 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0104Mapper {
    /**
     * 插入贴现台账
     *
     * @param openDay
     * @return
     */
    int insertAccDisc(@Param("openDay") String openDay);

    /**
     * 同步贴现台账状态
     *
     * @param openDay
     * @return
     */
    int updateAccDisc(@Param("openDay") String openDay);
}
