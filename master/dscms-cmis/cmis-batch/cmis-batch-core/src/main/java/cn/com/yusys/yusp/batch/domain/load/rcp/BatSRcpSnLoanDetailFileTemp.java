/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpSnLoanDetailFileTemp
 * @类描述: bat_s_rcp_sn_loan_detail_file_temp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-04 15:50:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_rcp_sn_loan_detail_file_temp")
public class BatSRcpSnLoanDetailFileTemp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 贷款编号  苏宁银行的借据编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CONTRACT_NO")
	private String contractNo;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATA", unique = false, nullable = true, length = 10)
	private String dataData;
	
	/** 项目编码  联合贷平台分配（01001001） **/
	@Column(name = "PRD_CODE", unique = false, nullable = true, length = 60)
	private String prdCode;
	
	/** 放款业务流水号   牵头行内部对外放款确认流水号（放款时的businessNo） **/
	@Column(name = "LOAN_BUSINESS_NO", unique = false, nullable = true, length = 60)
	private String loanBusinessNo;
	
	/** 客户姓名 **/
	@Column(name = "CUST_NAME", unique = false, nullable = true, length = 30)
	private String custName;
	
	/** 证件类型  身份证：10 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 10)
	private String certType;
	
	/** 证件号 **/
	@Column(name = "CERT_NO", unique = false, nullable = true, length = 30)
	private String certNo;
	
	/** 放款日期 牵头行账务核心登记的贷款建账日（YYYYMMDD） **/
	@Column(name = "TXN_DATE", unique = false, nullable = true, length = 8)
	private String txnDate;
	
	/** 放款金额  单位元 **/
	@Column(name = "TXN_AMT", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal txnAmt;
	
	/** 币种  人民币 **/
	@Column(name = "CURRENCY", unique = false, nullable = true, length = 3)
	private String currency;
	
	/** 总期数   例：等额本息现金分期12期--12 **/
	@Column(name = "PERIOD_SUM", unique = false, nullable = true, length = 3)
	private String periodSum;
	
	/** 放款状态  S：成功；F：失败； **/
	@Column(name = "LOAN_STATUS", unique = false, nullable = true, length = 3)
	private String loanStatus;
	
	/** 业务流水号 :授信审批申请中业务流水号 **/
	@Column(name = "CRD_BUSINESS_NO", unique = false, nullable = true, length = 60)
	private String crdBusinessNo;
	
	/** 录入日期  **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期  **/
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "LAST_UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	
	/**
	 * @param dataData
	 */
	public void setDataData(String dataData) {
		this.dataData = dataData;
	}
	
    /**
     * @return dataData
     */
	public String getDataData() {
		return this.dataData;
	}
	
	/**
	 * @param prdCode
	 */
	public void setPrdCode(String prdCode) {
		this.prdCode = prdCode;
	}
	
    /**
     * @return prdCode
     */
	public String getPrdCode() {
		return this.prdCode;
	}
	
	/**
	 * @param loanBusinessNo
	 */
	public void setLoanBusinessNo(String loanBusinessNo) {
		this.loanBusinessNo = loanBusinessNo;
	}
	
    /**
     * @return loanBusinessNo
     */
	public String getLoanBusinessNo() {
		return this.loanBusinessNo;
	}
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}
	
    /**
     * @return certNo
     */
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param txnDate
	 */
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	
    /**
     * @return txnDate
     */
	public String getTxnDate() {
		return this.txnDate;
	}
	
	/**
	 * @param txnAmt
	 */
	public void setTxnAmt(java.math.BigDecimal txnAmt) {
		this.txnAmt = txnAmt;
	}
	
    /**
     * @return txnAmt
     */
	public java.math.BigDecimal getTxnAmt() {
		return this.txnAmt;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    /**
     * @return currency
     */
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param periodSum
	 */
	public void setPeriodSum(String periodSum) {
		this.periodSum = periodSum;
	}
	
    /**
     * @return periodSum
     */
	public String getPeriodSum() {
		return this.periodSum;
	}
	
	/**
	 * @param loanStatus
	 */
	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}
	
    /**
     * @return loanStatus
     */
	public String getLoanStatus() {
		return this.loanStatus;
	}
	
	/**
	 * @param crdBusinessNo
	 */
	public void setCrdBusinessNo(String crdBusinessNo) {
		this.crdBusinessNo = crdBusinessNo;
	}
	
    /**
     * @return crdBusinessNo
     */
	public String getCrdBusinessNo() {
		return this.crdBusinessNo;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}


}