package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0428</br>
 * 任务名称：加工任务-贷后管理-生成自动化贷后对公名单  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月18日 下午11:56:54
 */
public interface Cmis0428Mapper {


    /**
     *  插入对公预跑批白名单月表
     *
     * @param openDay
     * @return
     */
    int insertAutoPspWhiteInfoCorpMonth(@Param("openDay") String openDay);
    /**
     *  27号删除对公预跑批白名单
     *
     * @param openDay
     * @return
     */
    int deleteautoPspWhiteInfoCorp(@Param("openDay") String openDay);


    /**
     * 插入对公预跑批白名单
     *
     * @param openDay
     * @return
     */
    int insertAutoPspWhiteInfoCorp(@Param("openDay") String openDay);

    /**
     * 删除 非专业担保公司 担保
     *
     * @param openDay
     * @return
     */
    int deleteautoPspWhiteInfoCorpNotGuarCom(@Param("openDay") String openDay);



    /**
     * 27号获取三个月内是否存在最新的报告 如果没有则到人行去查,更新 need_query_crereport为
     *
     * @param openDay
     * @return
     */
    int updateAutoPspWhiteInfoCorp(@Param("openDay") String openDay);

}
