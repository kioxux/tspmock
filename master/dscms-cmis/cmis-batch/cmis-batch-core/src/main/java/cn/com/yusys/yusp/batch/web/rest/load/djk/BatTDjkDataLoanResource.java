/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.djk;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.djk.BatTDjkDataLoan;
import cn.com.yusys.yusp.batch.service.load.djk.BatTDjkDataLoanService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTDjkDataLoanResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-07 10:57:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battdjkdataloan")
public class BatTDjkDataLoanResource {
    @Autowired
    private BatTDjkDataLoanService batTDjkDataLoanService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTDjkDataLoan>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTDjkDataLoan> list = batTDjkDataLoanService.selectAll(queryModel);
        return new ResultDto<List<BatTDjkDataLoan>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTDjkDataLoan>> index(QueryModel queryModel) {
        List<BatTDjkDataLoan> list = batTDjkDataLoanService.selectByModel(queryModel);
        return new ResultDto<List<BatTDjkDataLoan>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTDjkDataLoan> create(@RequestBody BatTDjkDataLoan batTDjkDataLoan) throws URISyntaxException {
        batTDjkDataLoanService.insert(batTDjkDataLoan);
        return new ResultDto<BatTDjkDataLoan>(batTDjkDataLoan);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTDjkDataLoan batTDjkDataLoan) throws URISyntaxException {
        int result = batTDjkDataLoanService.update(batTDjkDataLoan);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String loanId, String acctNo) {
        int result = batTDjkDataLoanService.deleteByPrimaryKey(loanId, acctNo);
        return new ResultDto<Integer>(result);
    }

}
