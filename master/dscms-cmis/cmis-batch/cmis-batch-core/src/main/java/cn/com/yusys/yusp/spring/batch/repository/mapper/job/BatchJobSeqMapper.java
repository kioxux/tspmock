/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.repository.mapper.job;

import cn.com.yusys.yusp.spring.batch.domain.job.BatchJobSeq;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

import java.util.List;
/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobSeqMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface BatchJobSeqMapper {


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<BatchJobSeq> selectByModel(QueryModel model);
	

}