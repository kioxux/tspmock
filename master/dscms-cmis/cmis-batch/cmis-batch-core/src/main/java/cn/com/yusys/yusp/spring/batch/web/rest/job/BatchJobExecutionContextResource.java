/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.web.rest.job;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.spring.batch.domain.job.BatchJobExecutionContext;
import cn.com.yusys.yusp.spring.batch.service.job.BatchJobExecutionContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobExecutionContextResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batchjobexecutioncontext")
public class BatchJobExecutionContextResource {
    @Autowired
    private BatchJobExecutionContextService batchJobExecutionContextService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatchJobExecutionContext>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatchJobExecutionContext> list = batchJobExecutionContextService.selectAll(queryModel);
        return new ResultDto<List<BatchJobExecutionContext>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatchJobExecutionContext>> index(QueryModel queryModel) {
        List<BatchJobExecutionContext> list = batchJobExecutionContextService.selectByModel(queryModel);
        return new ResultDto<List<BatchJobExecutionContext>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatchJobExecutionContext>> query(@RequestBody QueryModel queryModel) {
        List<BatchJobExecutionContext> list = batchJobExecutionContextService.selectByModel(queryModel);
        return new ResultDto<List<BatchJobExecutionContext>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{jobExecutionId}")
    protected ResultDto<BatchJobExecutionContext> show(@PathVariable("jobExecutionId") Long jobExecutionId) {
        BatchJobExecutionContext batchJobExecutionContext = batchJobExecutionContextService.selectByPrimaryKey(jobExecutionId);
        return new ResultDto<BatchJobExecutionContext>(batchJobExecutionContext);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatchJobExecutionContext> create(@RequestBody BatchJobExecutionContext batchJobExecutionContext) throws URISyntaxException {
        batchJobExecutionContextService.insert(batchJobExecutionContext);
        return new ResultDto<BatchJobExecutionContext>(batchJobExecutionContext);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatchJobExecutionContext batchJobExecutionContext) throws URISyntaxException {
        int result = batchJobExecutionContextService.update(batchJobExecutionContext);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{jobExecutionId}")
    protected ResultDto<Integer> delete(@PathVariable("jobExecutionId") Long jobExecutionId) {
        int result = batchJobExecutionContextService.deleteByPrimaryKey(jobExecutionId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batchJobExecutionContextService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
