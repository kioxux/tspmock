package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0302Mapper;
//import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0303Mapper;
import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0303Mapper;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0302</br>
 * 任务名称：加工任务-客户处理- 增享贷名单生成   增享贷名单信息,   是再当前客户贷款的抵押物抵押率想超85%，而叠加的增享贷信用贷款来补充   </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0303Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0303Service.class);

    @Autowired
      private Cmis0303Mapper cmis0303Mapper;
    @Autowired
    private BatControlRunService batControlRunService;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 插入增享贷名单
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0303InsertCusLstZxdStep(String openDay) {

        QueryModel queryModel = new QueryModel();
        List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
        String lastOpenDay = batControlRunList.get(0).getLastOpenDay();//营业日期



        logger.info(" 申请时间超过30天，办理状态  02  待办、 07 失效、 04 拒绝 、01 日终准入拒绝、   00 初始化 删除掉重新跑批  开始,请求参数为:[{}]", openDay);
        int deleteCusLstZxdByStatus = cmis0303Mapper.deleteCusLstZxdByStatus(openDay);
        logger.info(" 申请时间超过30天，办理状态  02  待办、 07 失效、 04 拒绝 、01 日终准入拒绝、   00 初始化 删除掉重新跑批 结束,返回参数为:[{}]", deleteCusLstZxdByStatus);


        logger.info("重命名创建和删除临时表_贷款台账信息表_客户号开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_acc_loan_cusid_cmis0303");
        logger.info("重命名创建和删除临时表_贷款台账信息表_客户号结束");
        logger.info("插入临时表_贷款台账信息表_客户号开始,请求参数为:[{}]", openDay);
        int insertTmpAccLoanCusId = cmis0303Mapper.insertTmpAccLoanCusId(openDay);
        logger.info("插入临时表_贷款台账信息表_客户号结束,返回参数为:[{}]", insertTmpAccLoanCusId);
        tableUtilsService.analyzeTable("cmis_biz","tmp_acc_loan_cusid_cmis0303");//分析表

        logger.info("重命名创建和删除临时表担保合同和合同的关系开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_grt_guar_cont_rel_cmis0303");
        logger.info("重命名创建和删除临时表担保合同和合同的关系结束");
        logger.info("插入临时表担保合同和合同的关系开始,请求参数为:[{}]", openDay);
        int insertTmpGrtGuarContRel = cmis0303Mapper.insertTmpGrtGuarContRel(openDay);
        logger.info("插入临时表担保合同和合同的关系结束,返回参数为:[{}]", insertTmpGrtGuarContRel);
        tableUtilsService.analyzeTable("cmis_biz","tmp_grt_guar_cont_rel_cmis0303");//分析表

        logger.info("重命名创建和删除临时表-担保合同关联信息表开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_grt_guar_rel_cmis0303");
        logger.info("重命名创建和删除临时表-担保合同关联信息表结束");
        logger.info("插入临时表-担保合同关联信息表开始,请求参数为:[{}]", openDay);
        int insertTmpGrtGuarBizRel = cmis0303Mapper.insertTmpGrtGuarBizRel(openDay);
        logger.info("插入临时表-担保合同关联信息表结束,返回参数为:[{}]", insertTmpGrtGuarBizRel);
        tableUtilsService.analyzeTable("cmis_biz","tmp_grt_guar_rel_cmis0303");//分析表
        logger.info("删除押品统一编号重复临时表_配偶客户信息表开始");
        cmis0303Mapper.deleteTmpGrtGuarBizRel(openDay);
        logger.info("删除押品统一编号重复临时表_配偶客户信息表结束");
        
        logger.info("重命名创建和删除临时表_配偶客户信息表开始");
        tableUtilsService.renameCreateDropTable("cmis_cus","tmp_cus_indiv_contact_cmis0303");
        logger.info("重命名创建和删除临时表_配偶客户信息表结束");

        logger.info("插入临时表_配偶客户信息表开始,请求参数为:[{}]", openDay);
        int insertTmpCusIndivContact = cmis0303Mapper.insertTmpCusIndivContact(openDay);
        logger.info("插入临时表_配偶客户信息表结束,返回参数为:[{}]", insertTmpCusIndivContact);
        tableUtilsService.analyzeTable("cmis_cus","tmp_cus_indiv_contact_cmis0303");//分析表

        logger.info("重命名创建和删除临时表-授信调查批复表开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_lmt_crd_reply_info_cmis0303");
        logger.info("重命名创建和删除临时表-授信调查批复表结束");
        logger.info("插入临时表-授信调查批复表开始,请求参数为:[{}]", openDay);
        int insertTmpLmtCrdReplyInfo = cmis0303Mapper.insertTmpLmtCrdReplyInfo(openDay);
        logger.info("插入临时表-授信调查批复表结束,返回参数为:[{}]", insertTmpLmtCrdReplyInfo);
        tableUtilsService.analyzeTable("cmis_biz","tmp_lmt_crd_reply_info_cmis0303");//分析表

        logger.info("重命名创建和删除临时表-授信调查批复表开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_cus_lst_zxd_cmis0303");
        logger.info("重命名创建和删除临时表-授信调查批复表结束");
        logger.info("插入临时表-增享贷名单信息开始,请求参数为:[{}]", openDay);
        int insertTmpCusLstZxd = cmis0303Mapper.insertTmpCusLstZxd(openDay);
        logger.info("插入临时表-增享贷名单信息结束,返回参数为:[{}]", insertTmpCusLstZxd);
        tableUtilsService.analyzeTable("cmis_biz","tmp_cus_lst_zxd_cmis0303");//分析表

        logger.info("重命名创建和删除临时表-系统用户表开始");
        tableUtilsService.renameCreateDropTable("yusp_admin","tmp_admin_sm_user_cmis0303");
        logger.info("重命名创建和删除临时表-系统用户表结束");
        logger.info("插入临时表-系统用户表开始,请求参数为:[{}]", openDay);
        int insertTmpSmUser = cmis0303Mapper.insertTmpSmUser(openDay);
        logger.info("插入临时表-系统用户表结束,返回参数为:[{}]", insertTmpSmUser);
        tableUtilsService.analyzeTable("yusp_admin","tmp_admin_sm_user_cmis0303");//分析表

        logger.info("插入增享贷名单信息开始,请求参数为:[{}]", openDay);
         int insertCusLstWtsx01 = cmis0303Mapper.insertCusLstZxd01(lastOpenDay);
         logger.info("插入增享贷名单信息结束,返回参数为:[{}]", insertCusLstWtsx01);

        logger.info(" 押品 自有 ，非自有的删除开始,请求参数为:[{}]", openDay);
        int deleteCusLstZxdForOwner = cmis0303Mapper.deleteCusLstZxdForOwner(lastOpenDay);
        logger.info(" 押品 自有 ，非自有的删除 结束,返回参数为:[{}]", deleteCusLstZxdForOwner);

        logger.info("借款人是否存在逾期1开始,请求参数为:[{}]", openDay);
        int insertCusLstZxd02 = cmis0303Mapper.insertCusLstZxd0(openDay);
        logger.info("借款人是否存在逾期1结束,返回参数为:[{}]", insertCusLstZxd02);
        logger.info("借款人是否存在逾期2开始,请求参数为:[{}]", openDay);
         int insertCusLstZxd0 = cmis0303Mapper.insertCusLstZxd02(openDay);
         logger.info("借款人是否存在逾期2结束,返回参数为:[{}]", insertCusLstZxd0);

        logger.info("重命名创建和删除借款人增享贷名单信息临时表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_cus_lst_zxd1");
        logger.info("重命名创建和删除增享贷名单信息临时表结束");
        logger.info("重命名创建和删除配偶增享贷名单信息临时表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_cus_lst_zxd2");
        logger.info("重命名创建和删除配偶增享贷名单信息临时表结束");
        logger.info("加工借款人增享贷名单信息临时表开始,请求参数为:[{}]", openDay);
        int insertTmp = cmis0303Mapper.insertTmpLmtCustZxd1(openDay);
        logger.info("加工借款人增享贷名单信息临时表结束,返回参数为:[{}]", insertTmp);
        logger.info("加工配偶增享贷名单信息临时表开始,请求参数为:[{}]", openDay);
        int insertTmp2 = cmis0303Mapper.insertTmpLmtCustZxd2(openDay);
        logger.info("加工配偶增享贷名单信息临时表结束,返回参数为:[{}]", insertTmp2);
        logger.info("借款人借据汇总开始,请求参数为:[{}]", openDay);
         int insertCusLstZxd03 = cmis0303Mapper.insertCusLstZxd03(openDay);
         logger.info("借款人借据汇总结束,返回参数为:[{}]", insertCusLstZxd03);

        logger.info("清理配偶是否逾期临时表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_cus_lst_zxd3");
        logger.info("清理配偶是否逾期临时表结束");
        logger.info("加工是否逾期临时表开始,请求参数为:[{}]", openDay);
        int insertTmp3 = cmis0303Mapper.insertTmp3(openDay);
        logger.info("加工是否逾期临时表结束,返回参数为:[{}]", insertTmp3);

        logger.info("配偶是否存在逾期开始,请求参数为:[{}]", openDay);
         int insertCusLstZxd04 = cmis0303Mapper.insertCusLstZxd04(openDay);
         logger.info("配偶是否存在逾期结束,返回参数为:[{}]", insertCusLstZxd04);

        logger.info("配偶借据汇总开始,请求参数为:[{}]", openDay);
        int insertCusLstZxd05 = cmis0303Mapper.insertCusLstZxd05(openDay);
        logger.info("配偶借据汇总结束,返回参数为:[{}]", insertCusLstZxd05);

        logger.info("清理临时表-贷款借据号数量统计表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_bat_s_core_klnb_dkzhqg_cmis0303");
        logger.info("清理临时表-贷款借据号数量统计表结束");
        logger.info("插入临时表-贷款借据号数量统计表开始,请求参数为:[{}]", openDay);
        int insertTmpBatKlnbDkzhqg = cmis0303Mapper.insertTmpBatKlnbDkzhqg(openDay);
        logger.info("插入临时表-贷款借据号数量统计表结束,返回参数为:[{}]", insertTmpBatKlnbDkzhqg);
        tableUtilsService.analyzeTable("cmis_batch","tmp_bat_s_core_klnb_dkzhqg_cmis0303");//分析表

        logger.info("清理还款期限临时表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_cus_lst_zxd4");
        logger.info("清理还款期限临时表结束");
        logger.info("加工还款期限临时表开始,请求参数为:[{}]", openDay);
        int insertTmp4 = cmis0303Mapper.insertTmp4(openDay);
        logger.info("加工还款期限临时表结束,返回参数为:[{}]", insertTmp4);

        logger.info("增享贷名单内的借据已还款期数开始,请求参数为:[{}]", openDay);
        int insertCusLstZxd06 = cmis0303Mapper.insertCusLstZxd06(openDay);
        logger.info("增享贷名单内的借据已还款期数结束,返回参数为:[{}]", insertCusLstZxd06);


    }


}
