package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0212Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0212</br>
 * 任务名称：加工任务-额度处理-合作方额度处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0212Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0212Service.class);
    @Autowired
    private Cmis0212Mapper cmis0212Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * 更新合作方授信分项信息
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0212UpdateApprCoopSubInfo(String openDay) {
        logger.info("truncateApprLmtSubBasicInfo04A清理占用关系表开始,请求参数为:[{}]", openDay);
        // cmis0212Mapper.truncateApprLmtSubBasicInfo04A();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateApprLmtSubBasicInfo04A清理占用关系表结束");

        logger.info("truncateTmpDealLmtContRel0212删除分项占用关系信息加工表开始,请求参数为:[{}]", openDay);
        // cmis0212Mapper.truncateTmpDealLmtContRel0212();// cmis_lmt.tmp_deal_lmt_cont_rel_0212
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_deal_lmt_cont_rel_0212");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpDealLmtContRel0212删除分项占用关系信息加工表结束");

        logger.info("插入分项占用关系信息加工表  开始,请求参数为:[{}]", openDay);
        int insertTmpDealLmtContRel0212 = cmis0212Mapper.insertTmpDealLmtContRel0212(openDay);
        logger.info("插入分项占用关系信息加工表 结束,返回参数为:[{}]", insertTmpDealLmtContRel0212);


        logger.info("合作方授信关联lmtcontrel ,计算合作方占用额  开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfo04A = cmis0212Mapper.insertApprLmtSubBasicInfo04A(openDay);
        logger.info("合作方授信关联lmtcontrel ,计算合作方占用额 结束,返回参数为:[{}]", insertApprLmtSubBasicInfo04A);

        logger.info("更新批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo04A = cmis0212Mapper.updateApprLmtSubBasicInfo04A(openDay);
        logger.info("更新批复额度分项基础信息结束,返回参数为:[{}]", updateApprLmtSubBasicInfo04A);

        logger.info("清理占用关系表开始,请求参数为:[{}]", openDay);
        // cmis0212Mapper.truncateApprLmtSubBasicInfo04B();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清理占用关系表结束");

        logger.info("truncateTmpDealContAccRel0212删除分项占用关系信息加工表开始,请求参数为:[{}]", openDay);
        // cmis0212Mapper.truncateTmpDealContAccRel0212();// cmis_lmt.tmp_deal_cont_acc_rel_0212
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_deal_cont_acc_rel_0212");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpDealContAccRel0212删除分项占用关系信息加工表结束");

        logger.info("插入合同占用关系信息加工表开始,请求参数为:[{}]", openDay);
        int insertTmpDealContAccRel0212 = cmis0212Mapper.insertTmpDealContAccRel0212(openDay);
        logger.info("插入合同占用关系信息加工表结束,返回参数为:[{}]", insertTmpDealContAccRel0212);

        logger.info("用信余额汇总 合作方分项下关联的合同关联的台账余额开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfo04B = cmis0212Mapper.insertApprLmtSubBasicInfo04B(openDay);
        logger.info("用信余额汇总 合作方分项下关联的合同关联的台账余额结束,返回参数为:[{}]", insertApprLmtSubBasicInfo04B);

        logger.info("用信余额汇总 合作方分项下关联的合同关联的台账余额开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo04B = cmis0212Mapper.updateApprLmtSubBasicInfo04B(openDay);
        logger.info("用信余额汇总 合作方分项下关联的合同关联的台账余额结束,返回参数为:[{}]", updateApprLmtSubBasicInfo04B);
    }

}
