package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0602Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0602</br>
 * 任务名称：加工任务-批量管理-提醒事项推送</br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0602Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0602Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0602Service cmis0602Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job cmis0602Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_JOB.key, JobStepEnum.CMIS0602_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0602Job = this.jobBuilderFactory.get(JobStepEnum.CMIS0602_JOB.key)
                .start(cmis0602UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0602CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(cmis0602QueryWbMsgNotice0003Step(WILL_BE_INJECTED)) //查询风险提示表
                .next(cmis0602QueryWbMsgNotice0004Step(WILL_BE_INJECTED)) //查询风险提示表
                .next(cmis0602QueryWbMsgNotice0005Step(WILL_BE_INJECTED)) //查询风险提示表
                .next(cmis0602QueryWbMsgNotice0006Step(WILL_BE_INJECTED)) //查询风险提示表
                .next(cmis0602QueryWbMsgNotice0007Step(WILL_BE_INJECTED)) //查询风险提示表
                .next(cmis0602QueryWbMsgNotice0008Step(WILL_BE_INJECTED)) //查询风险提示表
                .next(cmis0602QueryWbMsgNotice0009Step(WILL_BE_INJECTED)) //查询风险提示表
                .next(cmis0602QuerySmsManageInfoC0009Step(WILL_BE_INJECTED)) //查询短信通知表输入表
                .next(cmis0602QuerySmsManageInfoC0010Step(WILL_BE_INJECTED)) //查询短信通知表输入表
                // 由于OCA中消息队列对于延迟发送存在问题，将发送给客户的短信步骤调整到后面 开始
//                .next(cmis0602QuerySmsManageInfoC0001Step(WILL_BE_INJECTED)) //查询短信通知表输入表   改成单独的定时任务BatchTimedTask0008
//                .next(cmis0602QuerySmsManageInfoC0002Step(WILL_BE_INJECTED)) //查询短信通知表输入表
//                .next(cmis0602QuerySmsManageInfoC0003Step(WILL_BE_INJECTED)) //查询短信通知表输入表
//                .next(cmis0602QuerySmsManageInfoC0004Step(WILL_BE_INJECTED)) //查询短信通知表输入表
//                .next(cmis0602QuerySmsManageInfoC0005Step(WILL_BE_INJECTED)) //查询短信通知表输入表
//                .next(cmis0602QuerySmsManageInfoC0006Step(WILL_BE_INJECTED)) //查询短信通知表输入表
//                .next(cmis0602QuerySmsManageInfoC0007Step(WILL_BE_INJECTED)) //查询短信通知表输入表
//                .next(cmis0602QuerySmsManageInfoC0008Step(WILL_BE_INJECTED)) //查询短信通知表输入表
                // 由于OCA中消息队列对于延迟发送存在问题，将发送给客户的短信步骤调整到后面 结束
                .next(cmis0602UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0602Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0602_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0602_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0602_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_CHECK_REL_STEP.key, JobStepEnum.CMIS0602_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602CheckRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0602_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0602_CHECK_REL_STEP.key, JobStepEnum.CMIS0602_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0602_CHECK_REL_STEP.key, JobStepEnum.CMIS0602_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0602CheckRelStep;
    }

    /**
     * 查询短信通知表输入表,MSG_PL_C_0001
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QuerySmsManageInfoC0001Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QuerySmsManageInfoC0001Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    boolean checkTimeFlag = this.checkTime();
                    if (checkTimeFlag) {
                        cmis0602Service.cmis0602QuerySmsManageInfoC0001(openDay);//查询短信通知表输入表
                        return RepeatStatus.FINISHED;
                    }
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.value, "检查当前时间不在短信发送的时间段，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.value, "检查当前时间不在短信发送的时间段，该线程休眠10秒钟结束");
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0602QuerySmsManageInfoC0001Step;
    }

    /**
     * 检查当前时间是否在短信发送的时间段
     *
     * @return
     */
    public boolean checkTime() {
        Boolean result = false;
        LocalTime localTimeNow = LocalTime.now();
        LocalTime beginTime = LocalTime.of(10, 0);
        LocalTime endTime = LocalTime.of(17, 0);
        logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0001_STEP.value,
                "短信发送开始时间为[" + beginTime + "],结束时间为[" + endTime + "],当前时间为:[" + localTimeNow + "]");
        if (localTimeNow.isAfter(beginTime) && localTimeNow.isBefore(endTime)) {
            result = true;
        }
        return result;
    }

    /**
     * 查询短信通知表输入表,MSG_PL_C_0002
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QuerySmsManageInfoC0002Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0002_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0002_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QuerySmsManageInfoC0002Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0002_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QuerySmsManageInfoC0002(openDay);//查询短信通知表输入表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0002_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0002_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QuerySmsManageInfoC0002Step;
    }

    /**
     * 查询短信通知表输入表,MSG_PL_C_0003
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QuerySmsManageInfoC0003Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0003_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0003_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QuerySmsManageInfoC0003Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0003_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QuerySmsManageInfoC0003(openDay);//查询短信通知表输入表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0003_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0003_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QuerySmsManageInfoC0003Step;
    }

    /**
     * 查询短信通知表输入表,MSG_PL_C_0004
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QuerySmsManageInfoC0004Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0004_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0004_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QuerySmsManageInfoC0004Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0004_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QuerySmsManageInfoC0004(openDay);//查询短信通知表输入表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0004_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0004_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QuerySmsManageInfoC0004Step;
    }

    /**
     * 查询短信通知表输入表,MSG_PL_C_0005
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QuerySmsManageInfoC0005Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0005_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0005_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QuerySmsManageInfoC0005Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0005_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QuerySmsManageInfoC0005(openDay);//查询短信通知表输入表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0005_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0005_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QuerySmsManageInfoC0005Step;
    }

    /**
     * 查询短信通知表输入表,MSG_PL_C_0006
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QuerySmsManageInfoC0006Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0006_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0006_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QuerySmsManageInfoC0006Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0006_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QuerySmsManageInfoC0006(openDay);//查询短信通知表输入表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0006_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0006_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QuerySmsManageInfoC0006Step;
    }

    /**
     * 查询短信通知表输入表,MSG_PL_C_0007
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QuerySmsManageInfoC0007Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0007_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0007_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QuerySmsManageInfoC0007Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0007_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QuerySmsManageInfoC0007(openDay);//查询短信通知表输入表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0007_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0007_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QuerySmsManageInfoC0007Step;
    }

    /**
     * 查询短信通知表输入表,MSG_PL_C_0008
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QuerySmsManageInfoC0008Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0008_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0008_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QuerySmsManageInfoC0008Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0008_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QuerySmsManageInfoC0008(openDay);//查询短信通知表输入表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0008_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0008_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QuerySmsManageInfoC0008Step;
    }

    /**
     * 查询短信通知表输入表,MSG_PL_C_0009
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QuerySmsManageInfoC0009Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0009_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0009_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QuerySmsManageInfoC0009Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0009_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QuerySmsManageInfoC0009(openDay);//查询短信通知表输入表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0009_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0009_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QuerySmsManageInfoC0009Step;
    }

    /**
     * 查询短信通知表输入表,MSG_PL_C_0007
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QuerySmsManageInfoC0010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0010_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QuerySmsManageInfoC0010Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QuerySmsManageInfoC0010(openDay);//查询短信通知表输入表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0010_STEP.key, JobStepEnum.CMIS0602_QUERY_SMS_MANAGE_INFO_C0010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QuerySmsManageInfoC0010Step;
    }

    /**
     * 查询风险提示表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QueryWbMsgNotice0003Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0003_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0003_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QueryWbMsgNotice0003Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0003_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QueryWbMsgNotice0003(openDay);//查询风险提示表,对应短信编号ID：MSG_PL_M_0003(您名下档案调阅未归还，已逾期【doc_overdue_day】天！)
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0003_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0003_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QueryWbMsgNotice0003Step;
    }

    /**
     * 查询风险提示表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QueryWbMsgNotice0004Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0004_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0004_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QueryWbMsgNotice0004Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0004_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QueryWbMsgNotice0004(openDay);//查询风险提示表,对应短信编号ID：MSG_PL_M_0004(您好，您管户下诉讼当事人【cus_name】【cus_id】，距离诉讼时效到期还有【beforeMonth】个月！)
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0004_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0004_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QueryWbMsgNotice0004Step;
    }

    /**
     * 查询风险提示表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QueryWbMsgNotice0005Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0005_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0005_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QueryWbMsgNotice0005Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0005_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QueryWbMsgNotice0005(openDay);//查询风险提示表,对应短信编号ID：MSG_PL_M_0005(您好，您管户下抵债资产抵押物名称【pldimn_memo】客户名称【cus_name】客户号【cus_id】，距离抵债资产处置时效到期日前还有【beforeMonth】个月！)
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0005_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0005_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QueryWbMsgNotice0005Step;
    }

    /**
     * 查询风险提示表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QueryWbMsgNotice0006Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0006_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0006_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QueryWbMsgNotice0006Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0006_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QueryWbMsgNotice0006(openDay);//查询风险提示表,对应短信编号ID：MSG_PL_M_0006(您好，您管户下客户【cus_name】【cus_id】已完成单户呆账核销流程满两年，请进行核销后事项处理！)
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0006_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0006_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QueryWbMsgNotice0006Step;
    }

    /**
     * 查询风险提示表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QueryWbMsgNotice0007Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0007_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0007_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QueryWbMsgNotice0007Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0007_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0602Service.cmis0602QueryWbMsgNotice0007(openDay);//查询风险提示表,对应短信编号ID：MSG_PL_M_0007(您好，您管户下业务流水号【pbdwab_serno】核销总户数【total_writeoff_cus】核销笔数【total_writeoff_num】已完成批量核销流程满两年，请进行核销后事项处理！)
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0007_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0007_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QueryWbMsgNotice0007Step;
    }

    /**
     * 查询风险提示表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QueryWbMsgNotice0008Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0008_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0008_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QueryWbMsgNotice0008Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0008_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QueryWbMsgNotice0008(openDay);//查询风险提示表,对应短信编号ID：MSG_PL_M_0008(您好，您管户下业务流水号【pcwa_serno】核销总户数【total_writeoff_cus】已完成信用卡核销流程满两年，请进行核销后事项处理！)
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0008_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0008_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QueryWbMsgNotice0008Step;
    }

    /**
     * 查询风险提示表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602QueryWbMsgNotice0009Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0009_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0009_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602QueryWbMsgNotice0009Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0009_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0602Service.cmis0602QueryWbMsgNotice0009(openDay);//查询风险提示表,对应短信编号ID：MSG_PL_M_0009(您好，您管户下资金同业客户【cus_name】【cus_id】准入名单年审即将到期，请及时处理！)
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0009_STEP.key, JobStepEnum.CMIS0602_QUERY_WB_MSG_NOTICE0009_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602QueryWbMsgNotice0009Step;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0602UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0602_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0602_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0602UpdateTask100Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0602_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0602_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0602_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0602_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0602UpdateTask100Step;
    }
}
