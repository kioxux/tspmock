package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0222Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0222</br>
 * 任务名称：加工任务-额度处理-授信分项状态处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0222Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0222Service.class);
    @Autowired
    private Cmis0222Mapper cmis0222Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新合作方授信分项信息
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0222UpdateApprCoopSubInfo(String openDay) {
        logger.info("deleteTmpLmt01将要注销的未到期的零售批复放在中间表,清空中间表开始,请求参数为:[{}]", openDay);
        // cmis0222Mapper.deleteTmpLmt01();//cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTmpLmt01将要注销的未到期的零售批复放在中间表,清空中间表结束");

        logger.info("deleteTempCtrLoanCont0222删除合同主表0222开始");
        // cmis0222Mapper.deleteTempCtrLoanCont0222();// cmis_biz.temp_ctr_loan_cont_0222
        tableUtilsService.renameCreateDropTable("cmis_biz", "temp_ctr_loan_cont_0222");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTempCtrLoanCont0222删除合同主表0222结束");


        logger.info("插入合同主表0222开始,请求参数为:[{}]", openDay);
        int insertTempCtrLoanCont0222 = cmis0222Mapper.insertTempCtrLoanCont0222(openDay);
        logger.info("插入合同主表0222结束,返回参数为:[{}]", insertTempCtrLoanCont0222);


        logger.info("将要注销的未到期的零售批复放在中间表,插入中间表开始,请求参数为:[{}]", openDay);
        int insertTmpLmt01 = cmis0222Mapper.insertTmpLmt01(openDay);
        logger.info("将要注销的未到期的零售批复放在中间表,插入中间表结束,返回参数为:[{}]", insertTmpLmt01);


        logger.info("根据中间表更新零售批复状态开始,请求参数为:[{}]", openDay);
        int updateLmtCrdReplyInfo = cmis0222Mapper.updateLmtCrdReplyInfo(openDay);
        logger.info("根据中间表更新零售批复状态结束,返回参数为:[{}]", updateLmtCrdReplyInfo);

        logger.info("根据中间表更新额度批复分项状态开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo = cmis0222Mapper.updateApprLmtSubBasicInfo(openDay);
        logger.info("根据中间表更新额度批复分项状态结束,返回参数为:[{}]", updateApprLmtSubBasicInfo);


        logger.info("deleteTempApprStrMtableInfo0222删除批复主信息0222开始");
        // cmis0222Mapper.deleteTempApprStrMtableInfo0222();//cmis_lmt.temp_appr_str_mtable_info_0222
        tableUtilsService.renameCreateDropTable("cmis_lmt", "temp_appr_str_mtable_info_0222");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTempApprStrMtableInfo0222删除批复主信息0222结束");

        logger.info("插入批复主信息0222,插入中间表开始,请求参数为:[{}]", openDay);
        int insertTempApprStrMtableInfo0222 = cmis0222Mapper.insertTempApprStrMtableInfo0222(openDay);
        logger.info("插入批复主信息0222,插入中间表结束,返回参数为:[{}]", insertTempApprStrMtableInfo0222);

        // 20221024  同步zjw代码 修复global table:affected_rows is not same问题 开始
        logger.info("truncateAlsbiAsmi清空临时表-已到期的分项改成授信失效未结清 01-单一客户额度、08-承销额度  统一改成失效未结清，下条语句处理金额=0的为失效已结清 开始");
        // cmis0222Mapper.truncateAlsbiAsmi();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateAlsbiAsmi清空临时表-已到期的分项改成授信失效未结清 01-单一客户额度、08-承销额度  统一改成失效未结清，下条语句处理金额=0的为失效已结清 结束");

        logger.info("插入临时表-已到期的分项改成授信失效未结清 01-单一客户额度、08-承销额度  统一改成失效未结清，下条语句处理金额=0的为失效已结清开始,请求参数为:[{}]", openDay);
        int insertAlsbiAsmi = cmis0222Mapper.insertAlsbiAsmi(openDay);
        logger.info("插入临时表-已到期的分项改成授信失效未结清 01-单一客户额度、08-承销额度  统一改成失效未结清，下条语句处理金额=0的为失效已结清结束,返回参数为:[{}]", insertAlsbiAsmi);

        logger.info("已到期的分项改成授信失效未结清 01-单一客户额度、08-承销额度  统一改成失效未结清，下条语句处理金额=0的为失效已结清开始,请求参数为:[{}]", openDay);
        int updateAlsbiAsmi = cmis0222Mapper.updateAlsbiAsmi(openDay);
        logger.info("已到期的分项改成授信失效未结清 01-单一客户额度、08-承销额度  统一改成失效未结清，下条语句处理金额=0的为失效已结清结束,返回参数为:[{}]", updateAlsbiAsmi);
        // 20221024  同步zjw代码 修复global table:affected_rows is not same问题 结束

        // 20211022 同步zjw代码 修复global table:affected_rows is not same问题 开始
        logger.info("truncateTla05A清空临时表-已到期的改成授信失效已结清 -01-单一客户额度、08-承销额度开始");
        // cmis0222Mapper.truncateTla05A();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTla05A清空临时表-已到期的改成授信失效已结清 -01-单一客户额度、08-承销额度结束");

        logger.info("插入临时表-已到期的改成授信失效已结清 -01-单一客户额度、08-承销额度开始,请求参数为:[{}]", openDay);
        int insertTla05A = cmis0222Mapper.insertTla05A(openDay);
        logger.info("插入临时表-已到期的改成授信失效已结清 -01-单一客户额度、08-承销额度结束,返回参数为:[{}]", insertTla05A);

        logger.info("更新批复额度分项基础信息状态为结清开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo05A = cmis0222Mapper.updateApprLmtSubBasicInfo05A(openDay);
        logger.info("更新批复额度分项基础信息状态为结清结束,返回参数为:[{}]", updateApprLmtSubBasicInfo05A);
        // 20211022  同步zjw代码 修复global table:affected_rows is not same问题 结束

        logger.info("同业额度分项、单笔投资  已到期的分项改成授信失效未结清开始,请求参数为:[{}]", openDay);
        int updateAlsbiA = cmis0222Mapper.updateAlsbiA(openDay);
        logger.info("同业额度分项、单笔投资  已到期的分项改成授信失效未结清结束,返回参数为:[{}]", updateAlsbiA);

        logger.info("同业额度分项：同业票据类、同业贸融类  已到期的且已用金额分项改成授信失效已结清开始,请求参数为:[{}]", openDay);
        int updateAlsbiB = cmis0222Mapper.updateAlsbiB(openDay);
        logger.info("同业额度分项：同业票据类、同业贸融类  已到期的且已用金额分项改成授信失效已结清结束,返回参数为:[{}]", updateAlsbiB);

        logger.info("同业额度分项：非同业票据类、非同业贸融类的同业综合授信 和 单笔投资授信  已到期的更新为tmp表中的状态（处理ComStar数据时加工过）开始,请求参数为:[{}]", openDay);
        int updateAlsbiC = cmis0222Mapper.updateAlsbiC(openDay);
        logger.info("同业额度分项：非同业票据类、非同业贸融类的同业综合授信 和 单笔投资授信  已到期的更新为tmp表中的状态（处理ComStar数据时加工过）结束,返回参数为:[{}]", updateAlsbiC);


        logger.info("额度项下分项均失效已结清，则更新额度状态为失效已结清开始,请求参数为:[{}]", openDay);
        int updateClAsmiA = cmis0222Mapper.updateClAsmiA(openDay);
        logger.info("额度项下分项均失效已结清，则更新额度状态为失效已结清结束,返回参数为:[{}]", updateClAsmiA);

        logger.info("更新批复主信息开始,请求参数为:[{}]", openDay);
        int updateApprStrMtableInfo = cmis0222Mapper.updateApprStrMtableInfo(openDay);
        logger.info("更新批复主信息结束,返回参数为:[{}]", updateApprStrMtableInfo);

        logger.info("合作方分项已到期，则更新额度状态为失效未结清开始,请求参数为:[{}]", openDay);
        int updateClAcsiA = cmis0222Mapper.updateClAcsiA(openDay);
        logger.info("合作方分项已到期，则更新额度状态为失效未结清结束,返回参数为:[{}]", updateClAcsiA);


        logger.info("合作方分项为失效未结清，且用信余额=0，则更新额度状态为失效未结清开始,请求参数为:[{}]", openDay);
        int updateClAcsiB = cmis0222Mapper.updateClAcsiB(openDay);
        logger.info("合作方分项为失效未结清，且用信余额=0，则更新额度状态为失效未结清结束,返回参数为:[{}]", updateClAcsiB);


        logger.info("合作方额度项下分项均已失效已结清，则更显额度状态为失效已结清开始,请求参数为:[{}]", openDay);
        int updateClAcsiC = cmis0222Mapper.updateClAcsiC(openDay);
        logger.info("合作方额度项下分项均已失效已结清，则更显额度状态为失效已结清结束,返回参数为:[{}]", updateClAcsiC);


        logger.info("若集团额度项下，关联的额度台账状态均为失效已结清，则集团批复台账状态为失效已结清开始,请求参数为:[{}]", openDay);
        int updateClAsmiB = cmis0222Mapper.updateClAsmiB(openDay);
        logger.info("若集团额度项下，关联的额度台账状态均为失效已结清，则集团批复台账状态为失效已结清结束,返回参数为:[{}]", updateClAsmiB);

        logger.info("产品户买入反售白名单额度以tmp表为准开始,请求参数为:[{}]", openDay);
        int updateClLwiTdlwi = cmis0222Mapper.updateClLwiTdlwi(openDay);
        logger.info("产品户买入反售白名单额度以tmp表为准结束,返回参数为:[{}]", updateClLwiTdlwi);


        logger.info("白名单额度，若额度已到期，且用信余额为0，则将额度移入历史表开始,请求参数为:[{}]", openDay);
        int insertClLwih = cmis0222Mapper.insertClLwih(openDay);
        logger.info("白名单额度，若额度已到期，且用信余额为0，则将额度移入历史表结束,返回参数为:[{}]", insertClLwih);

    }

}
