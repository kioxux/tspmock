/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtRediscountSellBatch
 * @类描述: bat_s_pjp_bt_rediscount_sell_batch数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-03 22:08:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_pjp_bt_rediscount_sell_batch")
public class BatSPjpBtRediscountSellBatch extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 批次id **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "REDISC_SELL_BATCH_ID")
	private String rediscSellBatchId;
	
	/** 批次号 **/
	@Column(name = "S_BATCH_NO", unique = false, nullable = true, length = 40)
	private String sBatchNo;
	
	/** 当前操作机构id **/
	@Column(name = "S_BRANCH_ID", unique = false, nullable = true, length = 40)
	private String sBranchId;
	
	/** 产品ID **/
	@Column(name = "S_PROD_ID", unique = false, nullable = true, length = 40)
	private String sProdId;
	
	/** 操作员id **/
	@Column(name = "S_OPERATOR_ID", unique = false, nullable = true, length = 40)
	private String sOperatorId;
	
	/** 票据状态 **/
	@Column(name = "S_BILL_STATUS", unique = false, nullable = true, length = 10)
	private String sBillStatus;
	
	/** 创建时间 **/
	@Column(name = "D_CREATE_DT", unique = false, nullable = true, length = 20)
	private String dCreateDt;
	
	/** 票据类型 **/
	@Column(name = "S_BILL_TYPE", unique = false, nullable = true, length = 10)
	private String sBillType;
	
	/** 票据介质 **/
	@Column(name = "S_BILL_MEDIA", unique = false, nullable = true, length = 1)
	private String sBillMedia;
	
	/** 工作流caseID **/
	@Column(name = "CASEID", unique = false, nullable = true, length = 10)
	private Integer caseid;
	
	/** 任务项id **/
	@Column(name = "WORK_ITEM_ID", unique = false, nullable = true, length = 10)
	private Integer workItemId;
	
	/** 卖出利率 **/
	@Column(name = "SALERATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal salerate;
	
	/** 贴入方名称 **/
	@Column(name = "REDISCOUNTINNAME", unique = false, nullable = true, length = 150)
	private String rediscountinname;
	
	/** 贴入方大额行号 **/
	@Column(name = "REDISCOUNTINBANKCODE", unique = false, nullable = true, length = 20)
	private String rediscountinbankcode;
	
	/** 贴入方账号 **/
	@Column(name = "REDISCOUNTINACCOUNT", unique = false, nullable = true, length = 40)
	private String rediscountinaccount;
	
	/** 贴入方组织机构代码号 **/
	@Column(name = "REDISCOUNTINORGCODE", unique = false, nullable = true, length = 20)
	private String rediscountinorgcode;
	
	/** 贴出方名称 **/
	@Column(name = "REDISCOUNTOUTNAME", unique = false, nullable = true, length = 150)
	private String rediscountoutname;
	
	/** 贴出方大额行号 **/
	@Column(name = "REDISCOUNTOUTCODE", unique = false, nullable = true, length = 20)
	private String rediscountoutcode;
	
	/** 贴出方账号 **/
	@Column(name = "REDISCOUNTOUTACCOUNT", unique = false, nullable = true, length = 40)
	private String rediscountoutaccount;
	
	/** 贴出方组织机构代码号 **/
	@Column(name = "REDISCOUNTOUTORGCODE", unique = false, nullable = true, length = 20)
	private String rediscountoutorgcode;
	
	/** 转贴现种类 **/
	@Column(name = "REDISC_TYPE", unique = false, nullable = true, length = 10)
	private String rediscType;
	
	/** 利率类型 **/
	@Column(name = "SRATETYPE", unique = false, nullable = true, length = 10)
	private String sratetype;
	
	/** 清算标记 **/
	@Column(name = "CLEARING_FLAG", unique = false, nullable = true, length = 10)
	private String clearingFlag;
	
	/** 不得转让标记 **/
	@Column(name = "NOT_ATTORN_FLAG", unique = false, nullable = true, length = 10)
	private String notAttornFlag;
	
	/** 转贴现赎回利率 **/
	@Column(name = "BRPDINTRSTRATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal brpdintrstrate;
	
	/** 赎回利率利率类型 **/
	@Column(name = "SRPDRATETYPE", unique = false, nullable = true, length = 10)
	private String srpdratetype;
	
	/** 赎回实付金额 **/
	@Column(name = "BRPDPAYMENT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal brpdpayment;
	
	/** 回购开放日 **/
	@Column(name = "D_REDE_START_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dRedeStartDt;
	
	/** 回购截止日 **/
	@Column(name = "D_REDE_END_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dRedeEndDt;
	
	/** 贴出人备注 **/
	@Column(name = "REDISCOUNTOUTREMARK", unique = false, nullable = true, length = 800)
	private String rediscountoutremark;
	
	/** 同城、异地 **/
	@Column(name = "S_CITYTYPE", unique = false, nullable = true, length = 10)
	private String sCitytype;
	
	/** 是否系统内 **/
	@Column(name = "IF_SYSTEM_INNER", unique = false, nullable = true, length = 10)
	private String ifSystemInner;
	
	/** 总金额 **/
	@Column(name = "R_TOTALAMOUNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal rTotalamount;
	
	/** 总实付金额 **/
	@Column(name = "R_TOTALPAYMENT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal rTotalpayment;
	
	/** 总利息 **/
	@Column(name = "R_TOTALINTEREST", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal rTotalinterest;
	
	/** 总笔数 **/
	@Column(name = "R_TOTALBILL", unique = false, nullable = true, length = 10)
	private Integer rTotalbill;
	
	/** 是否双向 **/
	@Column(name = "S_IF_BIDIRECT", unique = false, nullable = true, length = 10)
	private String sIfBidirect;
	
	/** 库存状态 **/
	@Column(name = "STORAGE_STS", unique = false, nullable = true, length = 10)
	private String storageSts;
	
	/** 标志 票据来源种类 0 直贴进来的票 1 转帖进来的票 **/
	@Column(name = "TARGETCODE_TYPE", unique = false, nullable = true, length = 10)
	private String targetcodeType;
	
	/** 加入状态 **/
	@Column(name = "ADD_STATUS", unique = false, nullable = true, length = 10)
	private String addStatus;
	
	/** 卖出回购到期批次id **/
	@Column(name = "RETURN_BUY_BATCH_ID", unique = false, nullable = true, length = 40)
	private String returnBuyBatchId;
	
	/** 双向卖断到期日 **/
	@Column(name = "BIDIRECT_DT", unique = false, nullable = true, length = 10)
	private java.util.Date bidirectDt;
	
	/** 回购到期日 **/
	@Column(name = "RETURN_GOU_DT", unique = false, nullable = true, length = 10)
	private java.util.Date returnGouDt;
	
	/** 批次买入账号 **/
	@Column(name = "BATCH_REDISCOUNT_INACCOUNT", unique = false, nullable = true, length = 40)
	private String batchRediscountInaccount;
	
	/** 批次卖出账号 **/
	@Column(name = "BATCH_REDISCOUNT_OUTACCOUNT", unique = false, nullable = true, length = 40)
	private String batchRediscountOutaccount;
	
	/** 校验位 **/
	@Column(name = "SEQUENCE_ID", unique = false, nullable = true, length = 10)
	private String sequenceId;
	
	/** 贴出方类别 **/
	@Column(name = "REDISCOUNTOUTROLE", unique = false, nullable = true, length = 10)
	private String rediscountoutrole;
	
	/** 贴出方承接行行号 **/
	@Column(name = "REDISCOUNTOUTAGCYCODE", unique = false, nullable = true, length = 20)
	private String rediscountoutagcycode;
	
	/** 转贴现日 **/
	@Column(name = "D_SELL_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dSellDt;
	
	/** 协议编号 **/
	@Column(name = "S_REDISCT_AGREE_NO", unique = false, nullable = true, length = 12)
	private String sRedisctAgreeNo;
	
	/** 对方行行号 **/
	@Column(name = "S_BUYER_BANK_CODE", unique = false, nullable = true, length = 40)
	private String sBuyerBankCode;
	
	/** 往来帐号 **/
	@Column(name = "S_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sAccount;
	
	/** 付息方式 **/
	@Column(name = "S_INT_PAYWAY", unique = false, nullable = true, length = 1)
	private String sIntPayway;
	
	/** 客户经理ID **/
	@Column(name = "S_CUST_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String sCustManagerId;
	
	/** 是否顺延 **/
	@Column(name = "SIFPOSTPONE", unique = false, nullable = true, length = 10)
	private String sifpostpone;
	
	/** 异地顺延天数 **/
	@Column(name = "POSTPONEDAYS", unique = false, nullable = true, length = 10)
	private String postponedays;
	
	/** 同城顺延天数 **/
	@Column(name = "POSTPONEDAYSSAMECITY", unique = false, nullable = true, length = 10)
	private String postponedayssamecity;
	
	/** 是否回购状态 0=未到期 1=已到期未回购 2=已到期已回购 3=到期未回购转买断 **/
	@Column(name = "BILLDT_ZT", unique = false, nullable = true, length = 2)
	private String billdtZt;
	
	/** 代理行业务标记 1代理行业务 0本行业务 **/
	@Column(name = "AGENT_FLAG", unique = false, nullable = true, length = 2)
	private String agentFlag;
	
	/** 贴入机构ID **/
	@Column(name = "REDISCOUNT_IN_BRANCH_ID", unique = false, nullable = true, length = 50)
	private String rediscountInBranchId;
	
	/** 贴入机构号 **/
	@Column(name = "REDISCOUNT_IN_BRANCH_NO", unique = false, nullable = true, length = 30)
	private String rediscountInBranchNo;
	
	/** 卖断业务批次增加核算方式  1方式一，2方式二 **/
	@Column(name = "ACCOUNT_WAY", unique = false, nullable = true, length = 20)
	private String accountWay;
	
	/** 数据移植标记 1:移植数据  0：正常数据 **/
	@Column(name = "YZ_FLAG", unique = false, nullable = true, length = 2)
	private String yzFlag;
	
	/** 报价ID **/
	@Column(name = "QUOTATION_BATCH_ID", unique = false, nullable = true, length = 40)
	private String quotationBatchId;
	
	
	/**
	 * @param rediscSellBatchId
	 */
	public void setRediscSellBatchId(String rediscSellBatchId) {
		this.rediscSellBatchId = rediscSellBatchId;
	}
	
    /**
     * @return rediscSellBatchId
     */
	public String getRediscSellBatchId() {
		return this.rediscSellBatchId;
	}
	
	/**
	 * @param sBatchNo
	 */
	public void setSBatchNo(String sBatchNo) {
		this.sBatchNo = sBatchNo;
	}
	
    /**
     * @return sBatchNo
     */
	public String getSBatchNo() {
		return this.sBatchNo;
	}
	
	/**
	 * @param sBranchId
	 */
	public void setSBranchId(String sBranchId) {
		this.sBranchId = sBranchId;
	}
	
    /**
     * @return sBranchId
     */
	public String getSBranchId() {
		return this.sBranchId;
	}
	
	/**
	 * @param sProdId
	 */
	public void setSProdId(String sProdId) {
		this.sProdId = sProdId;
	}
	
    /**
     * @return sProdId
     */
	public String getSProdId() {
		return this.sProdId;
	}
	
	/**
	 * @param sOperatorId
	 */
	public void setSOperatorId(String sOperatorId) {
		this.sOperatorId = sOperatorId;
	}
	
    /**
     * @return sOperatorId
     */
	public String getSOperatorId() {
		return this.sOperatorId;
	}
	
	/**
	 * @param sBillStatus
	 */
	public void setSBillStatus(String sBillStatus) {
		this.sBillStatus = sBillStatus;
	}
	
    /**
     * @return sBillStatus
     */
	public String getSBillStatus() {
		return this.sBillStatus;
	}
	
	/**
	 * @param dCreateDt
	 */
	public void setDCreateDt(String dCreateDt) {
		this.dCreateDt = dCreateDt;
	}
	
    /**
     * @return dCreateDt
     */
	public String getDCreateDt() {
		return this.dCreateDt;
	}
	
	/**
	 * @param sBillType
	 */
	public void setSBillType(String sBillType) {
		this.sBillType = sBillType;
	}
	
    /**
     * @return sBillType
     */
	public String getSBillType() {
		return this.sBillType;
	}
	
	/**
	 * @param sBillMedia
	 */
	public void setSBillMedia(String sBillMedia) {
		this.sBillMedia = sBillMedia;
	}
	
    /**
     * @return sBillMedia
     */
	public String getSBillMedia() {
		return this.sBillMedia;
	}
	
	/**
	 * @param caseid
	 */
	public void setCaseid(Integer caseid) {
		this.caseid = caseid;
	}
	
    /**
     * @return caseid
     */
	public Integer getCaseid() {
		return this.caseid;
	}
	
	/**
	 * @param workItemId
	 */
	public void setWorkItemId(Integer workItemId) {
		this.workItemId = workItemId;
	}
	
    /**
     * @return workItemId
     */
	public Integer getWorkItemId() {
		return this.workItemId;
	}
	
	/**
	 * @param salerate
	 */
	public void setSalerate(java.math.BigDecimal salerate) {
		this.salerate = salerate;
	}
	
    /**
     * @return salerate
     */
	public java.math.BigDecimal getSalerate() {
		return this.salerate;
	}
	
	/**
	 * @param rediscountinname
	 */
	public void setRediscountinname(String rediscountinname) {
		this.rediscountinname = rediscountinname;
	}
	
    /**
     * @return rediscountinname
     */
	public String getRediscountinname() {
		return this.rediscountinname;
	}
	
	/**
	 * @param rediscountinbankcode
	 */
	public void setRediscountinbankcode(String rediscountinbankcode) {
		this.rediscountinbankcode = rediscountinbankcode;
	}
	
    /**
     * @return rediscountinbankcode
     */
	public String getRediscountinbankcode() {
		return this.rediscountinbankcode;
	}
	
	/**
	 * @param rediscountinaccount
	 */
	public void setRediscountinaccount(String rediscountinaccount) {
		this.rediscountinaccount = rediscountinaccount;
	}
	
    /**
     * @return rediscountinaccount
     */
	public String getRediscountinaccount() {
		return this.rediscountinaccount;
	}
	
	/**
	 * @param rediscountinorgcode
	 */
	public void setRediscountinorgcode(String rediscountinorgcode) {
		this.rediscountinorgcode = rediscountinorgcode;
	}
	
    /**
     * @return rediscountinorgcode
     */
	public String getRediscountinorgcode() {
		return this.rediscountinorgcode;
	}
	
	/**
	 * @param rediscountoutname
	 */
	public void setRediscountoutname(String rediscountoutname) {
		this.rediscountoutname = rediscountoutname;
	}
	
    /**
     * @return rediscountoutname
     */
	public String getRediscountoutname() {
		return this.rediscountoutname;
	}
	
	/**
	 * @param rediscountoutcode
	 */
	public void setRediscountoutcode(String rediscountoutcode) {
		this.rediscountoutcode = rediscountoutcode;
	}
	
    /**
     * @return rediscountoutcode
     */
	public String getRediscountoutcode() {
		return this.rediscountoutcode;
	}
	
	/**
	 * @param rediscountoutaccount
	 */
	public void setRediscountoutaccount(String rediscountoutaccount) {
		this.rediscountoutaccount = rediscountoutaccount;
	}
	
    /**
     * @return rediscountoutaccount
     */
	public String getRediscountoutaccount() {
		return this.rediscountoutaccount;
	}
	
	/**
	 * @param rediscountoutorgcode
	 */
	public void setRediscountoutorgcode(String rediscountoutorgcode) {
		this.rediscountoutorgcode = rediscountoutorgcode;
	}
	
    /**
     * @return rediscountoutorgcode
     */
	public String getRediscountoutorgcode() {
		return this.rediscountoutorgcode;
	}
	
	/**
	 * @param rediscType
	 */
	public void setRediscType(String rediscType) {
		this.rediscType = rediscType;
	}
	
    /**
     * @return rediscType
     */
	public String getRediscType() {
		return this.rediscType;
	}
	
	/**
	 * @param sratetype
	 */
	public void setSratetype(String sratetype) {
		this.sratetype = sratetype;
	}
	
    /**
     * @return sratetype
     */
	public String getSratetype() {
		return this.sratetype;
	}
	
	/**
	 * @param clearingFlag
	 */
	public void setClearingFlag(String clearingFlag) {
		this.clearingFlag = clearingFlag;
	}
	
    /**
     * @return clearingFlag
     */
	public String getClearingFlag() {
		return this.clearingFlag;
	}
	
	/**
	 * @param notAttornFlag
	 */
	public void setNotAttornFlag(String notAttornFlag) {
		this.notAttornFlag = notAttornFlag;
	}
	
    /**
     * @return notAttornFlag
     */
	public String getNotAttornFlag() {
		return this.notAttornFlag;
	}
	
	/**
	 * @param brpdintrstrate
	 */
	public void setBrpdintrstrate(java.math.BigDecimal brpdintrstrate) {
		this.brpdintrstrate = brpdintrstrate;
	}
	
    /**
     * @return brpdintrstrate
     */
	public java.math.BigDecimal getBrpdintrstrate() {
		return this.brpdintrstrate;
	}
	
	/**
	 * @param srpdratetype
	 */
	public void setSrpdratetype(String srpdratetype) {
		this.srpdratetype = srpdratetype;
	}
	
    /**
     * @return srpdratetype
     */
	public String getSrpdratetype() {
		return this.srpdratetype;
	}
	
	/**
	 * @param brpdpayment
	 */
	public void setBrpdpayment(java.math.BigDecimal brpdpayment) {
		this.brpdpayment = brpdpayment;
	}
	
    /**
     * @return brpdpayment
     */
	public java.math.BigDecimal getBrpdpayment() {
		return this.brpdpayment;
	}
	
	/**
	 * @param dRedeStartDt
	 */
	public void setDRedeStartDt(java.util.Date dRedeStartDt) {
		this.dRedeStartDt = dRedeStartDt;
	}
	
    /**
     * @return dRedeStartDt
     */
	public java.util.Date getDRedeStartDt() {
		return this.dRedeStartDt;
	}
	
	/**
	 * @param dRedeEndDt
	 */
	public void setDRedeEndDt(java.util.Date dRedeEndDt) {
		this.dRedeEndDt = dRedeEndDt;
	}
	
    /**
     * @return dRedeEndDt
     */
	public java.util.Date getDRedeEndDt() {
		return this.dRedeEndDt;
	}
	
	/**
	 * @param rediscountoutremark
	 */
	public void setRediscountoutremark(String rediscountoutremark) {
		this.rediscountoutremark = rediscountoutremark;
	}
	
    /**
     * @return rediscountoutremark
     */
	public String getRediscountoutremark() {
		return this.rediscountoutremark;
	}
	
	/**
	 * @param sCitytype
	 */
	public void setSCitytype(String sCitytype) {
		this.sCitytype = sCitytype;
	}
	
    /**
     * @return sCitytype
     */
	public String getSCitytype() {
		return this.sCitytype;
	}
	
	/**
	 * @param ifSystemInner
	 */
	public void setIfSystemInner(String ifSystemInner) {
		this.ifSystemInner = ifSystemInner;
	}
	
    /**
     * @return ifSystemInner
     */
	public String getIfSystemInner() {
		return this.ifSystemInner;
	}
	
	/**
	 * @param rTotalamount
	 */
	public void setRTotalamount(java.math.BigDecimal rTotalamount) {
		this.rTotalamount = rTotalamount;
	}
	
    /**
     * @return rTotalamount
     */
	public java.math.BigDecimal getRTotalamount() {
		return this.rTotalamount;
	}
	
	/**
	 * @param rTotalpayment
	 */
	public void setRTotalpayment(java.math.BigDecimal rTotalpayment) {
		this.rTotalpayment = rTotalpayment;
	}
	
    /**
     * @return rTotalpayment
     */
	public java.math.BigDecimal getRTotalpayment() {
		return this.rTotalpayment;
	}
	
	/**
	 * @param rTotalinterest
	 */
	public void setRTotalinterest(java.math.BigDecimal rTotalinterest) {
		this.rTotalinterest = rTotalinterest;
	}
	
    /**
     * @return rTotalinterest
     */
	public java.math.BigDecimal getRTotalinterest() {
		return this.rTotalinterest;
	}
	
	/**
	 * @param rTotalbill
	 */
	public void setRTotalbill(Integer rTotalbill) {
		this.rTotalbill = rTotalbill;
	}
	
    /**
     * @return rTotalbill
     */
	public Integer getRTotalbill() {
		return this.rTotalbill;
	}
	
	/**
	 * @param sIfBidirect
	 */
	public void setSIfBidirect(String sIfBidirect) {
		this.sIfBidirect = sIfBidirect;
	}
	
    /**
     * @return sIfBidirect
     */
	public String getSIfBidirect() {
		return this.sIfBidirect;
	}
	
	/**
	 * @param storageSts
	 */
	public void setStorageSts(String storageSts) {
		this.storageSts = storageSts;
	}
	
    /**
     * @return storageSts
     */
	public String getStorageSts() {
		return this.storageSts;
	}
	
	/**
	 * @param targetcodeType
	 */
	public void setTargetcodeType(String targetcodeType) {
		this.targetcodeType = targetcodeType;
	}
	
    /**
     * @return targetcodeType
     */
	public String getTargetcodeType() {
		return this.targetcodeType;
	}
	
	/**
	 * @param addStatus
	 */
	public void setAddStatus(String addStatus) {
		this.addStatus = addStatus;
	}
	
    /**
     * @return addStatus
     */
	public String getAddStatus() {
		return this.addStatus;
	}
	
	/**
	 * @param returnBuyBatchId
	 */
	public void setReturnBuyBatchId(String returnBuyBatchId) {
		this.returnBuyBatchId = returnBuyBatchId;
	}
	
    /**
     * @return returnBuyBatchId
     */
	public String getReturnBuyBatchId() {
		return this.returnBuyBatchId;
	}
	
	/**
	 * @param bidirectDt
	 */
	public void setBidirectDt(java.util.Date bidirectDt) {
		this.bidirectDt = bidirectDt;
	}
	
    /**
     * @return bidirectDt
     */
	public java.util.Date getBidirectDt() {
		return this.bidirectDt;
	}
	
	/**
	 * @param returnGouDt
	 */
	public void setReturnGouDt(java.util.Date returnGouDt) {
		this.returnGouDt = returnGouDt;
	}
	
    /**
     * @return returnGouDt
     */
	public java.util.Date getReturnGouDt() {
		return this.returnGouDt;
	}
	
	/**
	 * @param batchRediscountInaccount
	 */
	public void setBatchRediscountInaccount(String batchRediscountInaccount) {
		this.batchRediscountInaccount = batchRediscountInaccount;
	}
	
    /**
     * @return batchRediscountInaccount
     */
	public String getBatchRediscountInaccount() {
		return this.batchRediscountInaccount;
	}
	
	/**
	 * @param batchRediscountOutaccount
	 */
	public void setBatchRediscountOutaccount(String batchRediscountOutaccount) {
		this.batchRediscountOutaccount = batchRediscountOutaccount;
	}
	
    /**
     * @return batchRediscountOutaccount
     */
	public String getBatchRediscountOutaccount() {
		return this.batchRediscountOutaccount;
	}
	
	/**
	 * @param sequenceId
	 */
	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}
	
    /**
     * @return sequenceId
     */
	public String getSequenceId() {
		return this.sequenceId;
	}
	
	/**
	 * @param rediscountoutrole
	 */
	public void setRediscountoutrole(String rediscountoutrole) {
		this.rediscountoutrole = rediscountoutrole;
	}
	
    /**
     * @return rediscountoutrole
     */
	public String getRediscountoutrole() {
		return this.rediscountoutrole;
	}
	
	/**
	 * @param rediscountoutagcycode
	 */
	public void setRediscountoutagcycode(String rediscountoutagcycode) {
		this.rediscountoutagcycode = rediscountoutagcycode;
	}
	
    /**
     * @return rediscountoutagcycode
     */
	public String getRediscountoutagcycode() {
		return this.rediscountoutagcycode;
	}
	
	/**
	 * @param dSellDt
	 */
	public void setDSellDt(java.util.Date dSellDt) {
		this.dSellDt = dSellDt;
	}
	
    /**
     * @return dSellDt
     */
	public java.util.Date getDSellDt() {
		return this.dSellDt;
	}
	
	/**
	 * @param sRedisctAgreeNo
	 */
	public void setSRedisctAgreeNo(String sRedisctAgreeNo) {
		this.sRedisctAgreeNo = sRedisctAgreeNo;
	}
	
    /**
     * @return sRedisctAgreeNo
     */
	public String getSRedisctAgreeNo() {
		return this.sRedisctAgreeNo;
	}
	
	/**
	 * @param sBuyerBankCode
	 */
	public void setSBuyerBankCode(String sBuyerBankCode) {
		this.sBuyerBankCode = sBuyerBankCode;
	}
	
    /**
     * @return sBuyerBankCode
     */
	public String getSBuyerBankCode() {
		return this.sBuyerBankCode;
	}
	
	/**
	 * @param sAccount
	 */
	public void setSAccount(String sAccount) {
		this.sAccount = sAccount;
	}
	
    /**
     * @return sAccount
     */
	public String getSAccount() {
		return this.sAccount;
	}
	
	/**
	 * @param sIntPayway
	 */
	public void setSIntPayway(String sIntPayway) {
		this.sIntPayway = sIntPayway;
	}
	
    /**
     * @return sIntPayway
     */
	public String getSIntPayway() {
		return this.sIntPayway;
	}
	
	/**
	 * @param sCustManagerId
	 */
	public void setSCustManagerId(String sCustManagerId) {
		this.sCustManagerId = sCustManagerId;
	}
	
    /**
     * @return sCustManagerId
     */
	public String getSCustManagerId() {
		return this.sCustManagerId;
	}
	
	/**
	 * @param sifpostpone
	 */
	public void setSifpostpone(String sifpostpone) {
		this.sifpostpone = sifpostpone;
	}
	
    /**
     * @return sifpostpone
     */
	public String getSifpostpone() {
		return this.sifpostpone;
	}
	
	/**
	 * @param postponedays
	 */
	public void setPostponedays(String postponedays) {
		this.postponedays = postponedays;
	}
	
    /**
     * @return postponedays
     */
	public String getPostponedays() {
		return this.postponedays;
	}
	
	/**
	 * @param postponedayssamecity
	 */
	public void setPostponedayssamecity(String postponedayssamecity) {
		this.postponedayssamecity = postponedayssamecity;
	}
	
    /**
     * @return postponedayssamecity
     */
	public String getPostponedayssamecity() {
		return this.postponedayssamecity;
	}
	
	/**
	 * @param billdtZt
	 */
	public void setBilldtZt(String billdtZt) {
		this.billdtZt = billdtZt;
	}
	
    /**
     * @return billdtZt
     */
	public String getBilldtZt() {
		return this.billdtZt;
	}
	
	/**
	 * @param agentFlag
	 */
	public void setAgentFlag(String agentFlag) {
		this.agentFlag = agentFlag;
	}
	
    /**
     * @return agentFlag
     */
	public String getAgentFlag() {
		return this.agentFlag;
	}
	
	/**
	 * @param rediscountInBranchId
	 */
	public void setRediscountInBranchId(String rediscountInBranchId) {
		this.rediscountInBranchId = rediscountInBranchId;
	}
	
    /**
     * @return rediscountInBranchId
     */
	public String getRediscountInBranchId() {
		return this.rediscountInBranchId;
	}
	
	/**
	 * @param rediscountInBranchNo
	 */
	public void setRediscountInBranchNo(String rediscountInBranchNo) {
		this.rediscountInBranchNo = rediscountInBranchNo;
	}
	
    /**
     * @return rediscountInBranchNo
     */
	public String getRediscountInBranchNo() {
		return this.rediscountInBranchNo;
	}
	
	/**
	 * @param accountWay
	 */
	public void setAccountWay(String accountWay) {
		this.accountWay = accountWay;
	}
	
    /**
     * @return accountWay
     */
	public String getAccountWay() {
		return this.accountWay;
	}
	
	/**
	 * @param yzFlag
	 */
	public void setYzFlag(String yzFlag) {
		this.yzFlag = yzFlag;
	}
	
    /**
     * @return yzFlag
     */
	public String getYzFlag() {
		return this.yzFlag;
	}
	
	/**
	 * @param quotationBatchId
	 */
	public void setQuotationBatchId(String quotationBatchId) {
		this.quotationBatchId = quotationBatchId;
	}
	
    /**
     * @return quotationBatchId
     */
	public String getQuotationBatchId() {
		return this.quotationBatchId;
	}


}