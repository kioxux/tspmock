package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0209</br>
 * 任务名称：加工任务-额度处理-同业交易对手额度占用</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0209Mapper {
    /**
     * 清空加工占用授信表
     */
    void truncateTmpLmtAmt1();

    /**
     * 代开保函 处理规则 本客户下 的 保函 金额 与余额
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt1A(@Param("openDay") String openDay);

    /**
     * 代开信用证 处理规则 本客户下 的 保函 金额 与余额
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt1B(@Param("openDay") String openDay);

    /**
     * 福费廷（二级市场） 处理规则    本客户下 的 保函 金额 与余额
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt1C(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateTmpLmtAmt1(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void truncateTmpLmtAmt2();

    /**
     * 代开银票 处理规则  占用同业额度
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt2(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateTmpLmtAmt2(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void truncateTmpLmtAmt3();

    /**
     * 3- 代开银票  处理规则  占用同业额度
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt3(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateTmpLmtAmt3(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void truncateTmpLmtAmt4();

    /**
     * 银票贴现	占用 承兑行白名单额度lmt_white_info
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt4(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateTmpLmtAmt4(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void truncateTmpLmtAmt5();

    /**
     * 商票贴现	占用 商票保贴额度
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmt5(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateTmpLmtAmt5(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void truncateTmpLmtAmtYpzy();

    /**
     * 银票质押占用承兑行白名单额度 占用余额更新
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtYpzy(@Param("openDay") String openDay);

    /**
     * 清空授信其他更新表
     */
    void truncateTmpLmtAmtOther();

    /**
     * 插入授信其他更新表
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtOther(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateTmpLmtAmtYpzy(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表
     */
    void truncateTmpLmtAmtZccypzy();

    /**
     * 资产池银票质押入池占用承兑行白名单额度 占用余额更新
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtZccypzy(@Param("openDay") String openDay);

    /**
     * 清空授信其他更新表
     */
    void truncateTmpLmtAmtOtherZccypzy();

    /**
     * 插入授信其他更新表
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtOtherZccypzy(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int updateTmpLmtAmtZccypzy(@Param("openDay") String openDay);

    /**
     * 清空临时表-更新转贴现占同业客户额度
     */
    void truncateTmpLmtCop01();

    /**
     * 插入临时表-更新转贴现占同业客户额度
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtCop01(@Param("openDay") String openDay);

    /**
     * 更新转贴现占同业客户额度
     *
     * @param openDay
     * @return
     */
    int updateTmpLmtCop01(@Param("openDay") String openDay);
}
