package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0107Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0107</br>
 * 任务名称：加工任务-业务处理-当天未出账业务处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0107Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0107Service.class);

    @Autowired
    private Cmis0107Mapper cmis0107Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新业务中与未出账业务表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0107UpdateCmisBiz(String openDay) {

        logger.info("重命名创建和删除临时表-授权信息表0107A开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","temp_pvp_authorize_0107A");
        logger.info("重命名创建和删除临时表-授权信息表0107A结束");

        logger.info("重命名创建和删除临时表-授权信息表0107B开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz","temp_pvp_authorize_0107B");
        logger.info("重命名创建和删除临时表-授权信息表0107B结束");

        logger.info("插入授权信息表0107B开始,请求参数为:[{}]", openDay);
        int insertTempPvpAuthorize0107B = cmis0107Mapper.insertTempPvpAuthorize0107B(openDay);
        logger.info("插入授权信息表0107B结束,返回参数为:[{}]", insertTempPvpAuthorize0107B);

        logger.info("插入授权信息表0107A开始,请求参数为:[{}]", openDay);
        int insertTempPvpAuthorize0107A = cmis0107Mapper.insertTempPvpAuthorize0107A(openDay);
        logger.info("插入授权信息表0107A结束,返回参数为:[{}]", insertTempPvpAuthorize0107A);


//        logger.info("贷款出账表作废开始,请求参数为:[{}]", openDay);
//        int updatePvpLoanApp = cmis0107Mapper.updatePvpLoanApp(openDay);
//        logger.info("贷款出账表作废结束,返回参数为:[{}]", updatePvpLoanApp);

        logger.info("贷款台账表作废开始,请求参数为:[{}]", openDay);
        int updateAccLoan = cmis0107Mapper.updateAccLoan(openDay);
        logger.info("贷款台账表作废结束,返回参数为:[{}]", updateAccLoan);

//        logger.info("银承出账表作废开始,请求参数为:[{}]", openDay);
//        int updatePvpAccpApp = cmis0107Mapper.updatePvpAccpApp(openDay);
//        logger.info("银承出账表作废结束,返回参数为:[{}]", updatePvpAccpApp);

        logger.info("委托贷款台账表作废开始,请求参数为:[{}]", openDay);
        int updateAccEntrustLoan = cmis0107Mapper.updateAccEntrustLoan(openDay);
        logger.info("委托贷款台账表作废结束,返回参数为:[{}]", updateAccEntrustLoan);
    }
}
