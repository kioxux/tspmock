package cn.com.yusys.yusp.batch.service.timed;

import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * 定时任务处理类:运行定时任务-消息提醒任务相关批量任务  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月20日 下午11:56:54
 */
@Service
@Transactional
public class BatchTimedTask0007Service {
    private static final Logger logger = LoggerFactory.getLogger(BatchTimedTask0007Service.class);
    @Resource
    private ApplicationContext applicationContext;
    @Resource
    private SimpleJobLauncher customJobLauncher;
    @Autowired
    private BatControlRunService batControlRunService;
    @Autowired
    private BatTaskRunService batTaskRunService;
    private static ExecutorService priFlag05PendingTaskThreadPool = Executors.newFixedThreadPool(150);// 并发启动任务级别为[定时任务-消息提醒任务]的任务的线程池

    /**
     * 运行定时任务-消息提醒任务相关批量任务
     *
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public int timedtask0007() throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0007.key, BatEnums.BATCH_TIMED_TASK0007.value);
        try {
            // 查询批量运行表，获取任务日期，任务日期等于营业日期
            QueryModel queryModel = new QueryModel();
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(queryModel));
            List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunList));
            if (CollectionUtils.isEmpty(batControlRunList)) {
                // 调度运行管理(BAT_CONTROL_RUN)未配置
                logger.error(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0007.key, BatEnums.BATCH_TIMED_TASK0007.value, EchEnum.ECH080008.value);
                throw BizException.error(null, EchEnum.ECH080008.key, EchEnum.ECH080008.value);
            }
            // 校验运行状态
            if (!Objects.equals(batControlRunList.get(0).getControlStatus(), BatEnums.CONTROL_STATUS_010.key)) {
                // 调度运行管理(BAT_CONTROL_RUN)中状态不是运行中，请及时检查!
                logger.error(TradeLogConstants.BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0007.key, BatEnums.BATCH_TIMED_TASK0007.value, EchEnum.ECH080016.value);
                throw BizException.error(null, EchEnum.ECH080016.key, EchEnum.ECH080016.value);
            }

            String taskDate = batControlRunList.get(0).getLastOpenDay();//营业日期
            // 根据任务日期和任务级别查询bat_task_run中是否有任务状态为执行失败的，如果有则重新发起；如果没有则直接启动
            boolean isFailedFlag = false;//是否存在失败任务标志
            QueryModel failedQueryModel = new QueryModel();
            failedQueryModel.addCondition("taskDate", taskDate);
            failedQueryModel.addCondition("priFlag", BatEnums.PRI_FLAG_05.key);// 定时任务-消息提醒任务任务
            failedQueryModel.addCondition("taskStatus", BatEnums.TASK_STATUS_101.key);//执行失败
            logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表开始,请求参数为:[{}]", taskDate, BatEnums.PRI_FLAG_05.key, JSON.toJSONString(failedQueryModel));
            List<BatTaskRun> failedTaskList = batTaskRunService.selectFailedTaskList(failedQueryModel);
            logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表结束,响应参数为:[{}]", taskDate, BatEnums.PRI_FLAG_05.key, JSON.toJSONString(failedTaskList));

            if (CollectionUtils.nonEmpty(failedTaskList)) {
                isFailedFlag = true;
                logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表不为空，则存在失败任务标志,重新执行错误的任务", taskDate, BatEnums.PRI_FLAG_05.key);
            } else {
                isFailedFlag = false;
                logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表为空，则不存在失败任务标志,正常发起", taskDate, BatEnums.PRI_FLAG_05.key);
            }
            if (isFailedFlag) {
                try {
                    // 存在失败任务标志
                    List<String> failedTaskNoList = failedTaskList.stream().map(failedTask -> {
                        String failedTaskNo = failedTask.getTaskNo();
                        return failedTaskNo;
                    }).collect(Collectors.toList());
                    String failedTaskNos = failedTaskNoList.stream().collect(Collectors.joining(","));
                    logger.info("任务日期:[{}],任务级别:[{}]中重新执行的任务编号为:[{}]", taskDate, BatEnums.PRI_FLAG_05.key, failedTaskNos);
                    CountDownLatch priFlag05FailedTaskCd = new CountDownLatch(failedTaskNoList.size());
                    for (BatTaskRun failedTask : failedTaskList) {
                        priFlag05PendingTaskThreadPool.submit(() -> {
                            try {
                                // Job都是小写,除了bak之外
                                String failedTaskNo = StringUtils.EMPTY;
                                if (failedTask.getTaskNo().contains("BAK")) {
                                    failedTaskNo = failedTask.getTaskNo().replaceAll("BAK", "bak");
                                } else if (failedTask.getTaskNo().equals("CMIS020A") || failedTask.getTaskNo().equals("CMIS020B")
                                        || failedTask.getTaskNo().equals("CMIS020C") || failedTask.getTaskNo().equals("CMIS020D")
                                        || failedTask.getTaskNo().equals("CMIS020E") || failedTask.getTaskNo().equals("CMIS020F")
                                        || failedTask.getTaskNo().equals("CMIS020G") || failedTask.getTaskNo().equals("CMIS020H")
                                        || failedTask.getTaskNo().contains("CMIS417") || failedTask.getTaskNo().contains("CMIS418")
                                        || failedTask.getTaskNo().contains("CMIS425") || failedTask.getTaskNo().contains("CMIS431")) {
                                    failedTaskNo = failedTask.getTaskNo().replaceAll("CMIS", "cmis");
                                } else if (failedTask.getTaskNo().equals("BATEND")) {
                                    // 此处对BATEND任务单独处理
                                    failedTaskNo = "batEnd";
                                } else {
                                    failedTaskNo = failedTask.getTaskNo().toLowerCase();
                                }
                                String failedTaskName = failedTask.getTaskName();
                                try {
                                    logger.info("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用开始");
                                    customJobLauncher.run((Job) applicationContext.getBean(failedTaskNo + "Job"), createJobParams(taskDate, failedTaskNo, false));
                                    logger.info("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用完成");
                                } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                                    logger.error("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用异常，异常信息为:[" + e.getMessage() + "]");
                                    throw BizException.error(null, EchEnum.ECH080014.key, "任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用异常，异常信息为:[" + e.getMessage() + "]");
                                }
                            } catch (Exception ex) {
                                logger.error("任务日期:[" + taskDate + "],任务编号:[" + failedTask.getTaskNo() + "],任务名称:[" + failedTask.getTaskName() + "]调用线程池异常，异常信息为:[" + ex.getMessage() + "]");
                            } finally {
                                priFlag05FailedTaskCd.countDown();
                            }
                        });
                    }
                } catch (BizException e) {
                    logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                } catch (Exception e) {
                    logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                }
            } else {
                try {
                    // 待运行任务标志，当所有任务处理完成，跳出循环
                    boolean flag = true;
                    // 查询待运行的任务列表
                    QueryModel btrlQm = new QueryModel();
                    btrlQm.addCondition("taskDate", taskDate);
                    btrlQm.addCondition("priFlag", BatEnums.PRI_FLAG_05.key);// 贷记卡任务
                    btrlQm.addCondition("taskStatus", BatEnums.TASK_STATUS_000.key);
                    logger.info("查询待运行的任务列表,请求参数为[{}]", JSON.toJSONString(btrlQm));
                    List<BatTaskRun> pendingTaskList = batTaskRunService.selectPendingTask(btrlQm);
                    logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATSTART_RUN_STEP.key, JobStepEnum.BATSTART_RUN_STEP.value, "查询待运行的任务列表数量为:[" + pendingTaskList.size() + "]");

                    CountDownLatch priFlag02PendingTaskCd = new CountDownLatch(pendingTaskList.size());
                    for (BatTaskRun pendingTask : pendingTaskList) {
                        priFlag05PendingTaskThreadPool.submit(() -> {
                            try {
                                // Job都是小写,除了bak之外
                                String pendingTaskNo = StringUtils.EMPTY;
                                if (pendingTask.getTaskNo().contains("BAK")) {
                                    pendingTaskNo = pendingTask.getTaskNo().replaceAll("BAK", "bak");
                                } else if (pendingTask.getTaskNo().equals("CMIS020A") || pendingTask.getTaskNo().equals("CMIS020B")
                                        || pendingTask.getTaskNo().equals("CMIS020C") || pendingTask.getTaskNo().equals("CMIS020D")
                                        || pendingTask.getTaskNo().equals("CMIS020E") || pendingTask.getTaskNo().equals("CMIS020F")
                                        || pendingTask.getTaskNo().equals("CMIS020G") || pendingTask.getTaskNo().equals("CMIS020H")
                                        || pendingTask.getTaskNo().contains("CMIS417") || pendingTask.getTaskNo().contains("CMIS418")
                                        || pendingTask.getTaskNo().contains("CMIS425") || pendingTask.getTaskNo().contains("CMIS431")) {
                                    pendingTaskNo = pendingTask.getTaskNo().replaceAll("CMIS", "cmis");
                                } else if (pendingTask.getTaskNo().equals("BATEND")) {
                                    // 此处对BATEND任务单独处理
                                    pendingTaskNo = "batEnd";
                                } else {
                                    pendingTaskNo = pendingTask.getTaskNo().toLowerCase();
                                }
                                String pendingTaskName = pendingTask.getTaskName();
                                try {
                                    logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]开始调用开始开始调用开始");
                                    customJobLauncher.run((Job) applicationContext.getBean(pendingTaskNo + "Job"), createJobParams(taskDate, pendingTaskNo, false));
                                    logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用完成");
                                } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                                    logger.error("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]");
                                    throw BizException.error(null, EchEnum.ECH080014.key, "任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]");
                                }
                            } catch (Exception ex) {
                                logger.error("任务日期:[" + taskDate + "],任务编号:[" + pendingTask.getTaskNo() + "],任务名称:[" + pendingTask.getTaskName() + "]调用线程池异常，异常信息为:[" + ex.getMessage() + "]");
                            } finally {
                                priFlag02PendingTaskCd.countDown();
                            }
                        });
                    }

                } catch (BizException e) {
                    logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");

                } catch (Exception e) {
                    logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                }
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0007.key, BatEnums.BATCH_TIMED_TASK0007.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0007.key, BatEnums.BATCH_TIMED_TASK0007.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, BatEnums.BATCH_TIMED_TASK0007.key, BatEnums.BATCH_TIMED_TASK0007.value);
        return 0;
    }

    /**
     * 生成 JobParameter
     *
     * @param openDay 跑批日期
     * @param restart 是否需要重跑 需要则加时间戳
     * @return
     */
    private static JobParameters createJobParams(String openDay, String taskNo, boolean... restart) {
        JobParametersBuilder builder = new JobParametersBuilder().addString("openDay", openDay).addString("taskNo", taskNo);
        for (boolean arg : restart) {
            if (arg) {
                builder.addString("date", DateUtils.getCurrTimestamp().toString());
            }
        }
        return builder.toJobParameters();
    }
}
