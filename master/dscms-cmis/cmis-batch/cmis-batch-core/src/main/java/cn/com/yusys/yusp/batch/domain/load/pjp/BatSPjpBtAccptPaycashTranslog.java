/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtAccptPaycashTranslog
 * @类描述: bat_s_pjp_bt_accpt_paycash_translog数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-03 10:58:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_pjp_bt_accpt_paycash_translog")
public class BatSPjpBtAccptPaycashTranslog extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 交、补交保证金批次Id **/
	@Column(name = "PAYCASH_BATCH_ID", unique = false, nullable = true, length = 40)
	private String paycashBatchId;
	
	/** 承兑明细id **/
	@Column(name = "ACCEPTION_BILL_ID", unique = false, nullable = true, length = 40)
	private String acceptionBillId;
	
	/** 票号 **/
	@Column(name = "S_BILL_NO", unique = false, nullable = true, length = 40)
	private String sBillNo;
	
	/** 机构ID **/
	@Column(name = "S_BRANCH_ID", unique = false, nullable = true, length = 40)
	private String sBranchId;
	
	/** 操作员ID **/
	@Column(name = "S_OPERATOR_ID", unique = false, nullable = true, length = 40)
	private String sOperatorId;
	
	/** 申请人开户行行号 **/
	@Column(name = "S_CUST_BANKNO", unique = false, nullable = true, length = 40)
	private String sCustBankno;
	
	/** 申请人开户行行名 **/
	@Column(name = "S_CUST_BANKNAME", unique = false, nullable = true, length = 100)
	private String sCustBankname;
	
	/** 申请人名称 **/
	@Column(name = "S_CUST_NAME", unique = false, nullable = true, length = 150)
	private String sCustName;
	
	/** 申请人账号 **/
	@Column(name = "S_CUST_ACCT", unique = false, nullable = true, length = 40)
	private String sCustAcct;
	
	/** 申请人组织机构代码 **/
	@Column(name = "S_CUST_ORGCODE", unique = false, nullable = true, length = 20)
	private String sCustOrgcode;
	
	/** 复核员ID **/
	@Column(name = "S_CHECKER_ID", unique = false, nullable = true, length = 40)
	private String sCheckerId;
	
	/** 转出账号 **/
	@Column(name = "OUT_ACCOUNT", unique = false, nullable = true, length = 40)
	private String outAccount;
	
	/** 转出账户名 **/
	@Column(name = "OUT_ACCT_NAME", unique = false, nullable = true, length = 150)
	private String outAcctName;
	
	/** 转出账号币种 **/
	@Column(name = "OUT_ACCT_CURRENCY", unique = false, nullable = true, length = 10)
	private String outAcctCurrency;
	
	/** 转入账号 **/
	@Column(name = "IN_ACCOUNT", unique = false, nullable = true, length = 40)
	private String inAccount;
	
	/** 转入账号名称 **/
	@Column(name = "IN_ACCT_NAME", unique = false, nullable = true, length = 150)
	private String inAcctName;
	
	/** 票面总金额 **/
	@Column(name = "PAY_AMOUNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal payAmount;
	
	/** 申请日期 **/
	@Column(name = "APPLY_DT", unique = false, nullable = true, length = 20)
	private String applyDt;
	
	/** 业务标识 STD_RZ_BUSS_FLAG **/
	@Column(name = "BUSS_FLAG", unique = false, nullable = true, length = 10)
	private String bussFlag;
	
	/** 状态 STD_RZ_STATUS_FLAG **/
	@Column(name = "STATUS_FLAG", unique = false, nullable = true, length = 10)
	private String statusFlag;
	
	/** 交易时间 **/
	@Column(name = "TRANS_TIME", unique = false, nullable = true, length = 20)
	private String transTime;
	
	/** 交易流水 **/
	@Column(name = "TRANS_NO", unique = false, nullable = true, length = 40)
	private String transNo;
	
	/** 发信贷标记 **/
	@Column(name = "SEND_CREDIT_FLAG", unique = false, nullable = true, length = 10)
	private String sendCreditFlag;
	
	/** 转出账号开户行 **/
	@Column(name = "OUT_ACCT_BANKNO", unique = false, nullable = true, length = 12)
	private String outAcctBankno;
	
	/** 转入账号开户行 **/
	@Column(name = "IN_ACCT_BANKNO", unique = false, nullable = true, length = 12)
	private String inAcctBankno;
	
	/** 发信贷流水号 **/
	@Column(name = "CREDIT_TRANSNO", unique = false, nullable = true, length = 40)
	private String creditTransno;
	
	/** 发信贷错误信息 **/
	@Column(name = "CREDIT_RESPMSG", unique = false, nullable = true, length = 500)
	private String creditRespmsg;
	
	/** 有效金额 **/
	@Column(name = "IN_FORCE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal inForceAmt;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TME", unique = false, nullable = true, length = 40)
    private String createTme;
	
	/** 转出账号子序号 **/
	@Column(name = "OUT_ACCOUNT_SEQ", unique = false, nullable = true, length = 10)
	private String outAccountSeq;
	
	/** 转入账号子序号 **/
	@Column(name = "IN_ACCOUNT_SEQ", unique = false, nullable = true, length = 10)
	private String inAccountSeq;
	
	/** 账号id **/
	@Column(name = "MARGIN_ID", unique = false, nullable = true, length = 40)
	private String marginId;
	
	/** 保证金类型 STD_RZ_IS_BUSI_FLAG **/
	@Column(name = "IS_BUSI_FLAG", unique = false, nullable = true, length = 4)
	private String isBusiFlag;
	
	/** 计息方式 STD_RZ_INTEREST_MODE **/
	@Column(name = "INTEREST_MODE", unique = false, nullable = true, length = 10)
	private String interestMode;
	
	/** 新核心移植标记 STD_RZ_HXYZ_FLAG **/
	@Column(name = "HXYZ_FLAG", unique = false, nullable = true, length = 5)
	private String hxyzFlag;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param paycashBatchId
	 */
	public void setPaycashBatchId(String paycashBatchId) {
		this.paycashBatchId = paycashBatchId;
	}
	
    /**
     * @return paycashBatchId
     */
	public String getPaycashBatchId() {
		return this.paycashBatchId;
	}
	
	/**
	 * @param acceptionBillId
	 */
	public void setAcceptionBillId(String acceptionBillId) {
		this.acceptionBillId = acceptionBillId;
	}
	
    /**
     * @return acceptionBillId
     */
	public String getAcceptionBillId() {
		return this.acceptionBillId;
	}
	
	/**
	 * @param sBillNo
	 */
	public void setSBillNo(String sBillNo) {
		this.sBillNo = sBillNo;
	}
	
    /**
     * @return sBillNo
     */
	public String getSBillNo() {
		return this.sBillNo;
	}
	
	/**
	 * @param sBranchId
	 */
	public void setSBranchId(String sBranchId) {
		this.sBranchId = sBranchId;
	}
	
    /**
     * @return sBranchId
     */
	public String getSBranchId() {
		return this.sBranchId;
	}
	
	/**
	 * @param sOperatorId
	 */
	public void setSOperatorId(String sOperatorId) {
		this.sOperatorId = sOperatorId;
	}
	
    /**
     * @return sOperatorId
     */
	public String getSOperatorId() {
		return this.sOperatorId;
	}
	
	/**
	 * @param sCustBankno
	 */
	public void setSCustBankno(String sCustBankno) {
		this.sCustBankno = sCustBankno;
	}
	
    /**
     * @return sCustBankno
     */
	public String getSCustBankno() {
		return this.sCustBankno;
	}
	
	/**
	 * @param sCustBankname
	 */
	public void setSCustBankname(String sCustBankname) {
		this.sCustBankname = sCustBankname;
	}
	
    /**
     * @return sCustBankname
     */
	public String getSCustBankname() {
		return this.sCustBankname;
	}
	
	/**
	 * @param sCustName
	 */
	public void setSCustName(String sCustName) {
		this.sCustName = sCustName;
	}
	
    /**
     * @return sCustName
     */
	public String getSCustName() {
		return this.sCustName;
	}
	
	/**
	 * @param sCustAcct
	 */
	public void setSCustAcct(String sCustAcct) {
		this.sCustAcct = sCustAcct;
	}
	
    /**
     * @return sCustAcct
     */
	public String getSCustAcct() {
		return this.sCustAcct;
	}
	
	/**
	 * @param sCustOrgcode
	 */
	public void setSCustOrgcode(String sCustOrgcode) {
		this.sCustOrgcode = sCustOrgcode;
	}
	
    /**
     * @return sCustOrgcode
     */
	public String getSCustOrgcode() {
		return this.sCustOrgcode;
	}
	
	/**
	 * @param sCheckerId
	 */
	public void setSCheckerId(String sCheckerId) {
		this.sCheckerId = sCheckerId;
	}
	
    /**
     * @return sCheckerId
     */
	public String getSCheckerId() {
		return this.sCheckerId;
	}
	
	/**
	 * @param outAccount
	 */
	public void setOutAccount(String outAccount) {
		this.outAccount = outAccount;
	}
	
    /**
     * @return outAccount
     */
	public String getOutAccount() {
		return this.outAccount;
	}
	
	/**
	 * @param outAcctName
	 */
	public void setOutAcctName(String outAcctName) {
		this.outAcctName = outAcctName;
	}
	
    /**
     * @return outAcctName
     */
	public String getOutAcctName() {
		return this.outAcctName;
	}
	
	/**
	 * @param outAcctCurrency
	 */
	public void setOutAcctCurrency(String outAcctCurrency) {
		this.outAcctCurrency = outAcctCurrency;
	}
	
    /**
     * @return outAcctCurrency
     */
	public String getOutAcctCurrency() {
		return this.outAcctCurrency;
	}
	
	/**
	 * @param inAccount
	 */
	public void setInAccount(String inAccount) {
		this.inAccount = inAccount;
	}
	
    /**
     * @return inAccount
     */
	public String getInAccount() {
		return this.inAccount;
	}
	
	/**
	 * @param inAcctName
	 */
	public void setInAcctName(String inAcctName) {
		this.inAcctName = inAcctName;
	}
	
    /**
     * @return inAcctName
     */
	public String getInAcctName() {
		return this.inAcctName;
	}
	
	/**
	 * @param payAmount
	 */
	public void setPayAmount(java.math.BigDecimal payAmount) {
		this.payAmount = payAmount;
	}
	
    /**
     * @return payAmount
     */
	public java.math.BigDecimal getPayAmount() {
		return this.payAmount;
	}
	
	/**
	 * @param applyDt
	 */
	public void setApplyDt(String applyDt) {
		this.applyDt = applyDt;
	}
	
    /**
     * @return applyDt
     */
	public String getApplyDt() {
		return this.applyDt;
	}
	
	/**
	 * @param bussFlag
	 */
	public void setBussFlag(String bussFlag) {
		this.bussFlag = bussFlag;
	}
	
    /**
     * @return bussFlag
     */
	public String getBussFlag() {
		return this.bussFlag;
	}
	
	/**
	 * @param statusFlag
	 */
	public void setStatusFlag(String statusFlag) {
		this.statusFlag = statusFlag;
	}
	
    /**
     * @return statusFlag
     */
	public String getStatusFlag() {
		return this.statusFlag;
	}
	
	/**
	 * @param transTime
	 */
	public void setTransTime(String transTime) {
		this.transTime = transTime;
	}
	
    /**
     * @return transTime
     */
	public String getTransTime() {
		return this.transTime;
	}
	
	/**
	 * @param transNo
	 */
	public void setTransNo(String transNo) {
		this.transNo = transNo;
	}
	
    /**
     * @return transNo
     */
	public String getTransNo() {
		return this.transNo;
	}
	
	/**
	 * @param sendCreditFlag
	 */
	public void setSendCreditFlag(String sendCreditFlag) {
		this.sendCreditFlag = sendCreditFlag;
	}
	
    /**
     * @return sendCreditFlag
     */
	public String getSendCreditFlag() {
		return this.sendCreditFlag;
	}
	
	/**
	 * @param outAcctBankno
	 */
	public void setOutAcctBankno(String outAcctBankno) {
		this.outAcctBankno = outAcctBankno;
	}
	
    /**
     * @return outAcctBankno
     */
	public String getOutAcctBankno() {
		return this.outAcctBankno;
	}
	
	/**
	 * @param inAcctBankno
	 */
	public void setInAcctBankno(String inAcctBankno) {
		this.inAcctBankno = inAcctBankno;
	}
	
    /**
     * @return inAcctBankno
     */
	public String getInAcctBankno() {
		return this.inAcctBankno;
	}
	
	/**
	 * @param creditTransno
	 */
	public void setCreditTransno(String creditTransno) {
		this.creditTransno = creditTransno;
	}
	
    /**
     * @return creditTransno
     */
	public String getCreditTransno() {
		return this.creditTransno;
	}
	
	/**
	 * @param creditRespmsg
	 */
	public void setCreditRespmsg(String creditRespmsg) {
		this.creditRespmsg = creditRespmsg;
	}
	
    /**
     * @return creditRespmsg
     */
	public String getCreditRespmsg() {
		return this.creditRespmsg;
	}
	
	/**
	 * @param inForceAmt
	 */
	public void setInForceAmt(java.math.BigDecimal inForceAmt) {
		this.inForceAmt = inForceAmt;
	}
	
    /**
     * @return inForceAmt
     */
	public java.math.BigDecimal getInForceAmt() {
		return this.inForceAmt;
	}
	
	/**
	 * @param createTme
	 */
	public void setCreateTme(String createTme) {
		this.createTme = createTme;
	}
	
    /**
     * @return createTme
     */
	public String getCreateTme() {
		return this.createTme;
	}
	
	/**
	 * @param outAccountSeq
	 */
	public void setOutAccountSeq(String outAccountSeq) {
		this.outAccountSeq = outAccountSeq;
	}
	
    /**
     * @return outAccountSeq
     */
	public String getOutAccountSeq() {
		return this.outAccountSeq;
	}
	
	/**
	 * @param inAccountSeq
	 */
	public void setInAccountSeq(String inAccountSeq) {
		this.inAccountSeq = inAccountSeq;
	}
	
    /**
     * @return inAccountSeq
     */
	public String getInAccountSeq() {
		return this.inAccountSeq;
	}
	
	/**
	 * @param marginId
	 */
	public void setMarginId(String marginId) {
		this.marginId = marginId;
	}
	
    /**
     * @return marginId
     */
	public String getMarginId() {
		return this.marginId;
	}
	
	/**
	 * @param isBusiFlag
	 */
	public void setIsBusiFlag(String isBusiFlag) {
		this.isBusiFlag = isBusiFlag;
	}
	
    /**
     * @return isBusiFlag
     */
	public String getIsBusiFlag() {
		return this.isBusiFlag;
	}
	
	/**
	 * @param interestMode
	 */
	public void setInterestMode(String interestMode) {
		this.interestMode = interestMode;
	}
	
    /**
     * @return interestMode
     */
	public String getInterestMode() {
		return this.interestMode;
	}
	
	/**
	 * @param hxyzFlag
	 */
	public void setHxyzFlag(String hxyzFlag) {
		this.hxyzFlag = hxyzFlag;
	}
	
    /**
     * @return hxyzFlag
     */
	public String getHxyzFlag() {
		return this.hxyzFlag;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}