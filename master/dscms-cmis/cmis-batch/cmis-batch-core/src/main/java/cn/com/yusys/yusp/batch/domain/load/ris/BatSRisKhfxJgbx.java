/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.ris;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRisKhfxJgbx
 * @类描述: bat_s_ris_khfx_jgbx数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-25 16:59:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ris_khfx_jgbx")
public class BatSRisKhfxJgbx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 数据日期 **/
	@Id
	@Column(name = "DATA_DT")
	private String dataDt;
	
	/** 客户类型代码 **/
	@Id
	@Column(name = "CUST_TYPE_ID")
	private String custTypeId;
	
	/** 客户编号 **/
	@Id
	@Column(name = "CUST_ID")
	private String custId;
	
	/** 客户名称 **/
	@Id
	@Column(name = "CUST_NAME")
	private String custName;
	
	/** 资产余额 **/
	@Column(name = "BALANCE", unique = false, nullable = true, length = 26)
	private java.math.BigDecimal balance;
	
	/** 应收利息 **/
	@Column(name = "RECEIVABLE_INTEREST", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal receivableInterest;
	
	/** 一般风险暴露 **/
	@Column(name = "NORMAL_RISK_EXPO", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal normalRiskExpo;
	
	/** 特定风险暴露 **/
	@Column(name = "SPECIAL_RISK_EXPO", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal specialRiskExpo;
	
	/** 交易对手风险暴露 **/
	@Column(name = "COUNTERPARTY_EXPO", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal counterpartyExpo;
	
	/** 潜在风险暴露 **/
	@Column(name = "POTENTIAL_EXPO", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal potentialExpo;
	
	/** 缓释金额 **/
	@Column(name = "COLL_AMT", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal collAmt;
	
	/** 减值准备 **/
	@Column(name = "RESERVE", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal reserve;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param custTypeId
	 */
	public void setCustTypeId(String custTypeId) {
		this.custTypeId = custTypeId;
	}
	
    /**
     * @return custTypeId
     */
	public String getCustTypeId() {
		return this.custTypeId;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param balance
	 */
	public void setBalance(java.math.BigDecimal balance) {
		this.balance = balance;
	}
	
    /**
     * @return balance
     */
	public java.math.BigDecimal getBalance() {
		return this.balance;
	}
	
	/**
	 * @param receivableInterest
	 */
	public void setReceivableInterest(java.math.BigDecimal receivableInterest) {
		this.receivableInterest = receivableInterest;
	}
	
    /**
     * @return receivableInterest
     */
	public java.math.BigDecimal getReceivableInterest() {
		return this.receivableInterest;
	}
	
	/**
	 * @param normalRiskExpo
	 */
	public void setNormalRiskExpo(java.math.BigDecimal normalRiskExpo) {
		this.normalRiskExpo = normalRiskExpo;
	}
	
    /**
     * @return normalRiskExpo
     */
	public java.math.BigDecimal getNormalRiskExpo() {
		return this.normalRiskExpo;
	}
	
	/**
	 * @param specialRiskExpo
	 */
	public void setSpecialRiskExpo(java.math.BigDecimal specialRiskExpo) {
		this.specialRiskExpo = specialRiskExpo;
	}
	
    /**
     * @return specialRiskExpo
     */
	public java.math.BigDecimal getSpecialRiskExpo() {
		return this.specialRiskExpo;
	}
	
	/**
	 * @param counterpartyExpo
	 */
	public void setCounterpartyExpo(java.math.BigDecimal counterpartyExpo) {
		this.counterpartyExpo = counterpartyExpo;
	}
	
    /**
     * @return counterpartyExpo
     */
	public java.math.BigDecimal getCounterpartyExpo() {
		return this.counterpartyExpo;
	}
	
	/**
	 * @param potentialExpo
	 */
	public void setPotentialExpo(java.math.BigDecimal potentialExpo) {
		this.potentialExpo = potentialExpo;
	}
	
    /**
     * @return potentialExpo
     */
	public java.math.BigDecimal getPotentialExpo() {
		return this.potentialExpo;
	}
	
	/**
	 * @param collAmt
	 */
	public void setCollAmt(java.math.BigDecimal collAmt) {
		this.collAmt = collAmt;
	}
	
    /**
     * @return collAmt
     */
	public java.math.BigDecimal getCollAmt() {
		return this.collAmt;
	}
	
	/**
	 * @param reserve
	 */
	public void setReserve(java.math.BigDecimal reserve) {
		this.reserve = reserve;
	}
	
    /**
     * @return reserve
     */
	public java.math.BigDecimal getReserve() {
		return this.reserve;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}