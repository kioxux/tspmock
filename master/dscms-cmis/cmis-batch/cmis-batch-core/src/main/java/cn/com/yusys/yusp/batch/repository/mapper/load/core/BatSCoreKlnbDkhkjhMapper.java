/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.load.core;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003RespDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.batch.domain.load.core.BatSCoreKlnbDkhkjh;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKlnbDkhkjhMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface BatSCoreKlnbDkhkjhMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    BatSCoreKlnbDkhkjh selectByPrimaryKey(@Param("dkjiejuh") String dkjiejuh, @Param("benqqish") Long benqqish, @Param("benqizqs") Long benqizqs);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<BatSCoreKlnbDkhkjh> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(BatSCoreKlnbDkhkjh record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(BatSCoreKlnbDkhkjh record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(BatSCoreKlnbDkhkjh record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(BatSCoreKlnbDkhkjh record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("dkjiejuh") String dkjiejuh, @Param("benqqish") Long benqqish, @Param("benqizqs") Long benqizqs);

    /**
     * 根据贷款借据号 查询[贷款还款计划表]和[贷款期供交易明细]关联信息
     *
     * @param queryModel
     * @return
     */
    Cmisbatch0003RespDto selectByDkjiejuh(QueryModel queryModel);

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    int deleteByOpenDay(@Param("openDay") String openDay);

    /**
     * 更新S表中 数据日期 DATA_DATE 为营业日期
     *
     * @param openDay
     * @return
     */
    int updateDataDateByOpenDay(@Param("openDay") String openDay);
}