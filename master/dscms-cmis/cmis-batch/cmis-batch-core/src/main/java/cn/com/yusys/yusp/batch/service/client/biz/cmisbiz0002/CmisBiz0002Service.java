package cn.com.yusys.yusp.batch.service.client.biz.cmisbiz0002;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisbiz0002.req.CmisBiz0002ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0002.resp.CmisBiz0002RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisBizClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：集团客户授信复审
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CmisBiz0002Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBiz0002Service.class);

    // 1）注入：封装的接口类:业务服务模块
    @Autowired
    private CmisBizClientService cmisBizClientService;

    /**
     * 业务逻辑处理方法：集团客户授信复审
     *
     * @param cmisBiz0002ReqDto
     * @return
     */
    @Transactional
    public CmisBiz0002RespDto cmisbiz0002(CmisBiz0002ReqDto cmisBiz0002ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0002.key, DscmsEnum.TRADE_CODE_CMISBIZ0002.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0002.key, DscmsEnum.TRADE_CODE_CMISBIZ0002.value, JSON.toJSONString(cmisBiz0002ReqDto));
        ResultDto<CmisBiz0002RespDto> cmisBiz0002ResultDto = cmisBizClientService.cmisBiz0002(cmisBiz0002ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0002.key, DscmsEnum.TRADE_CODE_CMISBIZ0002.value, JSON.toJSONString(cmisBiz0002ResultDto));

        String cmisBiz0002Code = Optional.ofNullable(cmisBiz0002ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisBiz0002Meesage = Optional.ofNullable(cmisBiz0002ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisBiz0002RespDto cmisbiz0002RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisBiz0002ResultDto.getCode())) {
            //  获取相关的值并解析
            cmisbiz0002RespDto = cmisBiz0002ResultDto.getData(); //此处只调用，不做异常抛出
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0002.key, DscmsEnum.TRADE_CODE_CMISBIZ0002.value);
        return cmisbiz0002RespDto;
    }
}
