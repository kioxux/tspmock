/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.c2d;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dEd01bAcPayInfSub
 * @类描述: bat_s_c2d_ed01b_ac_pay_inf_sub数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:02:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_c2d_ed01b_ac_pay_inf_sub")
public class BatSC2dEd01bAcPayInfSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** ID **/
	@Id
	@Column(name = "ID")
	private String id;
	
	/** 报告编号 **/
	@Id
	@Column(name = "REPORT_ID")
	private String reportId;
	
	/** 关联ID **/
	@Id
	@Column(name = "UNION_ID")
	private String unionId;
	
	/** 信息报告日期 **/
	@Column(name = "REPORT_INFORM_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date reportInformDate;
	
	/** 余额 **/
	@Column(name = "BALANCE", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal balance;
	
	/** 余额变化日期 **/
	@Column(name = "BALANCE_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date balanceDate;
	
	/** 五级分类 **/
	@Column(name = "FILE_CLASS", unique = false, nullable = true, length = 60)
	private String fileClass;
	
	/** 五级分类认定日期 **/
	@Column(name = "FILE_CLASS_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date fileClassDate;
	
	/** 最近一次实际还款日期 **/
	@Column(name = "LAST_ACT_REPAY_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date lastActRepayDate;
	
	/** 最近一次实际还款总额 **/
	@Column(name = "LAST_ACT_REPAY_TOTAL", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal lastActRepayTotal;
	
	/** 最近一次还款形式 **/
	@Column(name = "LAST_ACT_REPAY_TYPE", unique = false, nullable = true, length = 60)
	private String lastActRepayType;
	
	/** 最近一次约定还款日期 **/
	@Column(name = "LAST_APP_REPAY_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date lastAppRepayDate;
	
	/** 最近一次应还总额 **/
	@Column(name = "LAST_APP_REPAY_TOTAL", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal lastAppRepayTotal;
	
	/** 逾期总额 **/
	@Column(name = "OVER_TOTAL", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal overTotal;
	
	/** 逾期本金 **/
	@Column(name = "OVER_PRINCIPAL", unique = false, nullable = true, length = 22)
	private java.math.BigDecimal overPrincipal;
	
	/** 逾期月数 **/
	@Column(name = "OVER_MONTH_NUM", unique = false, nullable = true, length = 8)
	private java.math.BigDecimal overMonthNum;
	
	/** 剩余还款月数 **/
	@Column(name = "SUR_MONTH_NUM", unique = false, nullable = true, length = 8)
	private java.math.BigDecimal surMonthNum;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param reportId
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	
    /**
     * @return reportId
     */
	public String getReportId() {
		return this.reportId;
	}
	
	/**
	 * @param unionId
	 */
	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}
	
    /**
     * @return unionId
     */
	public String getUnionId() {
		return this.unionId;
	}
	
	/**
	 * @param reportInformDate
	 */
	public void setReportInformDate(java.util.Date reportInformDate) {
		this.reportInformDate = reportInformDate;
	}
	
    /**
     * @return reportInformDate
     */
	public java.util.Date getReportInformDate() {
		return this.reportInformDate;
	}
	
	/**
	 * @param balance
	 */
	public void setBalance(java.math.BigDecimal balance) {
		this.balance = balance;
	}
	
    /**
     * @return balance
     */
	public java.math.BigDecimal getBalance() {
		return this.balance;
	}
	
	/**
	 * @param balanceDate
	 */
	public void setBalanceDate(java.util.Date balanceDate) {
		this.balanceDate = balanceDate;
	}
	
    /**
     * @return balanceDate
     */
	public java.util.Date getBalanceDate() {
		return this.balanceDate;
	}
	
	/**
	 * @param fileClass
	 */
	public void setFileClass(String fileClass) {
		this.fileClass = fileClass;
	}
	
    /**
     * @return fileClass
     */
	public String getFileClass() {
		return this.fileClass;
	}
	
	/**
	 * @param fileClassDate
	 */
	public void setFileClassDate(java.util.Date fileClassDate) {
		this.fileClassDate = fileClassDate;
	}
	
    /**
     * @return fileClassDate
     */
	public java.util.Date getFileClassDate() {
		return this.fileClassDate;
	}
	
	/**
	 * @param lastActRepayDate
	 */
	public void setLastActRepayDate(java.util.Date lastActRepayDate) {
		this.lastActRepayDate = lastActRepayDate;
	}
	
    /**
     * @return lastActRepayDate
     */
	public java.util.Date getLastActRepayDate() {
		return this.lastActRepayDate;
	}
	
	/**
	 * @param lastActRepayTotal
	 */
	public void setLastActRepayTotal(java.math.BigDecimal lastActRepayTotal) {
		this.lastActRepayTotal = lastActRepayTotal;
	}
	
    /**
     * @return lastActRepayTotal
     */
	public java.math.BigDecimal getLastActRepayTotal() {
		return this.lastActRepayTotal;
	}
	
	/**
	 * @param lastActRepayType
	 */
	public void setLastActRepayType(String lastActRepayType) {
		this.lastActRepayType = lastActRepayType;
	}
	
    /**
     * @return lastActRepayType
     */
	public String getLastActRepayType() {
		return this.lastActRepayType;
	}
	
	/**
	 * @param lastAppRepayDate
	 */
	public void setLastAppRepayDate(java.util.Date lastAppRepayDate) {
		this.lastAppRepayDate = lastAppRepayDate;
	}
	
    /**
     * @return lastAppRepayDate
     */
	public java.util.Date getLastAppRepayDate() {
		return this.lastAppRepayDate;
	}
	
	/**
	 * @param lastAppRepayTotal
	 */
	public void setLastAppRepayTotal(java.math.BigDecimal lastAppRepayTotal) {
		this.lastAppRepayTotal = lastAppRepayTotal;
	}
	
    /**
     * @return lastAppRepayTotal
     */
	public java.math.BigDecimal getLastAppRepayTotal() {
		return this.lastAppRepayTotal;
	}
	
	/**
	 * @param overTotal
	 */
	public void setOverTotal(java.math.BigDecimal overTotal) {
		this.overTotal = overTotal;
	}
	
    /**
     * @return overTotal
     */
	public java.math.BigDecimal getOverTotal() {
		return this.overTotal;
	}
	
	/**
	 * @param overPrincipal
	 */
	public void setOverPrincipal(java.math.BigDecimal overPrincipal) {
		this.overPrincipal = overPrincipal;
	}
	
    /**
     * @return overPrincipal
     */
	public java.math.BigDecimal getOverPrincipal() {
		return this.overPrincipal;
	}
	
	/**
	 * @param overMonthNum
	 */
	public void setOverMonthNum(java.math.BigDecimal overMonthNum) {
		this.overMonthNum = overMonthNum;
	}
	
    /**
     * @return overMonthNum
     */
	public java.math.BigDecimal getOverMonthNum() {
		return this.overMonthNum;
	}
	
	/**
	 * @param surMonthNum
	 */
	public void setSurMonthNum(java.math.BigDecimal surMonthNum) {
		this.surMonthNum = surMonthNum;
	}
	
    /**
     * @return surMonthNum
     */
	public java.math.BigDecimal getSurMonthNum() {
		return this.surMonthNum;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}