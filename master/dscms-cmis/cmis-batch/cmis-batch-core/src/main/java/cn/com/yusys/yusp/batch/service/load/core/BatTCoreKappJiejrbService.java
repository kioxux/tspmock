/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.core;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.core.BatTCoreKappJiejrb;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatTCoreKappJiejrbMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTCoreKappJiejrbService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTCoreKappJiejrbService {

    @Autowired
    private BatTCoreKappJiejrbMapper batTCoreKappJiejrbMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTCoreKappJiejrb selectByPrimaryKey(String farendma, String guobiedm, String fenhdaim, String huobdaih, String duigduis, String jiejiari) {
        return batTCoreKappJiejrbMapper.selectByPrimaryKey(farendma, guobiedm, fenhdaim, huobdaih, duigduis, jiejiari);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTCoreKappJiejrb> selectAll(QueryModel model) {
        List<BatTCoreKappJiejrb> records = (List<BatTCoreKappJiejrb>) batTCoreKappJiejrbMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTCoreKappJiejrb> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTCoreKappJiejrb> list = batTCoreKappJiejrbMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTCoreKappJiejrb record) {
        return batTCoreKappJiejrbMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTCoreKappJiejrb record) {
        return batTCoreKappJiejrbMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTCoreKappJiejrb record) {
        return batTCoreKappJiejrbMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTCoreKappJiejrb record) {
        return batTCoreKappJiejrbMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String farendma, String guobiedm, String fenhdaim, String huobdaih, String duigduis, String jiejiari) {
        return batTCoreKappJiejrbMapper.deleteByPrimaryKey(farendma, guobiedm, fenhdaim, huobdaih, duigduis, jiejiari);
    }

    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTCoreKappJiejrbMapper.truncateTTable();
    }
}
