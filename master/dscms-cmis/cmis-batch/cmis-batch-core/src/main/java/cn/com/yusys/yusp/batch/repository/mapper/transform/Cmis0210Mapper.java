package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0210</br>
 * 任务名称：加工任务-额度处理-普通授信额度占用处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0210Mapper {
    /**
     * 处理BIZ_STATUS交易业务状态200有效
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A(@Param("openDay") String openDay);

    /**
     * 1>贷款合同表-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A1Dkht(@Param("openDay") String openDay);

    /**
     * 1>最高额授信协议-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A1Zgesxxy(@Param("openDay") String openDay);

    /**
     * 1>委托贷款合同详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A1Wtdkhtxq(@Param("openDay") String openDay);

    /**
     * 1>贴现协议详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A1Txxyxq(@Param("openDay") String openDay);

    /**
     * 1>保函协议详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A1Bhxyxq(@Param("openDay") String openDay);

    /**
     * 1>开证合同详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A1Kzhtxq(@Param("openDay") String openDay);

    /**
     * 1>银承合同详情-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A1Ychtxq(@Param("openDay") String openDay);


    /**
     * 1>资产池协议台账表-关联合同状态处理；合同状态为正常且合同未到期，占用关系更新为 200-生效
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A1Zcc(@Param("openDay") String openDay);


    /**
     * 1>贷款合同表-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A2Dkht(@Param("openDay") String openDay);

    /**
     * 1>最高额授信协议-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A2Zgesxxy(@Param("openDay") String openDay);


    /**
     * 1>委托贷款合同详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A2Wtdkhtxq(@Param("openDay") String openDay);

    /**
     * 1>贴现协议详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A2Txxyxq(@Param("openDay") String openDay);

    /**
     * 1>保函协议详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A2Bhxyxq(@Param("openDay") String openDay);


    /**
     * 1>开证合同详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A2Kzhtxq(@Param("openDay") String openDay);

    /**
     * 1>银承合同详情-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A2Ychtxq(@Param("openDay") String openDay);

    /**
     * 1>资产池协议台账表-关联合同状态处理；合同状态为作废，占用关系更新为 400-作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A2Zcc(@Param("openDay") String openDay);


    /**
     * 1> 贷款合同表-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A3Dkht(@Param("openDay") String openDay);

    /**
     * 1> 最高额授信协议-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A3Zge(@Param("openDay") String openDay);


    /**
     * 1> 委托贷款合同详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A3Wtdk(@Param("openDay") String openDay);

    /**
     * 1> 贴现协议详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A3Txxx(@Param("openDay") String openDay);

    /**
     * 1> 保函协议详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A3Bhxy(@Param("openDay") String openDay);

    /**
     * 1> 开证合同详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A3Kzht(@Param("openDay") String openDay);

    /**
     * 1> 银承合同详情-关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A3Ycht(@Param("openDay") String openDay);

    /**
     * 1>资产池协议台账表 -关联合同状态处理；合同状态为注销或者中止，项下存在非作废或者关闭的台账 且 台账不存在余额>0的，则 占用状态更新为 注销
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A3Zcc(@Param("openDay") String openDay);

    /**
     * 1> 贷款合同表-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A4Dkht(@Param("openDay") String openDay);

    /**
     * 1> 最高额授信协议-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A4Zge(@Param("openDay") String openDay);

    /**
     * 1> 委托贷款合同详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A4Wtdkht(@Param("openDay") String openDay);

    /**
     * 1> 贴现协议详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A4Txxy(@Param("openDay") String openDay);

    /**
     * 1> 保函协议详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A4Bhxy(@Param("openDay") String openDay);

    /**
     * 1> 开证合同详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A4Kzht(@Param("openDay") String openDay);

    /**
     * 1> 银承合同详情-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A4Ycht(@Param("openDay") String openDay);

    /**
     * 1> 资产池协议台账表-关联合同状态处理；合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A4Zcc(@Param("openDay") String openDay);

    /**
     * 1> 贷款合同表-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A5Dkht(@Param("openDay") String openDay);

    /**
     * 1> 最高额授信协议-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A5Zge(@Param("openDay") String openDay);

    /**
     * 1> 委托贷款合同详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A5Wtdkht(@Param("openDay") String openDay);

    /**
     * 1> 贴现协议详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A5Txxy(@Param("openDay") String openDay);

    /**
     * 1> 保函协议详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A5Bhxy(@Param("openDay") String openDay);

    /**
     * 1> 开证合同详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A5Kzht(@Param("openDay") String openDay);

    /**
     * 1> 银承合同详情-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A5Ycht(@Param("openDay") String openDay);

    /**
     * 1> 资产池协议台账表-关联合同状态处理；合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel0A5Zcc(@Param("openDay") String openDay);


    /**
     * 更新分项占用关系信息
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel4A(@Param("openDay") String openDay);

    /**
     * 清理关系表
     *
     * @return
     */
    void truncateApprLmtSubBasicInfo01();

    /**
     * 其他类型 处理额度
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo01(@Param("openDay") String openDay);

    /**
     * 根据关系表更新授信占用
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo01(@Param("openDay") String openDay);

    /**
     * 清空加工占用授信表[同业额度分项项下已用金额汇总(同业票据类、同业贸融类)]
     */
    void truncateTmpDealLmtDetails();

    /**
     * 插入加工占用授信表[同业额度分项项下已用金额汇总(同业票据类、同业贸融类)]
     *
     * @param openDay
     * @return
     */
    int insertTmpDealLmtDetails(@Param("openDay") String openDay);

    /**
     * 更新加工占用授信表[同业额度分项项下已用金额汇总(同业票据类、同业贸融类)]
     *
     * @param openDay
     * @return
     */
    int updateTmpDealLmtDetails(@Param("openDay") String openDay);

    /**
     * 清理关系表
     *
     * @return
     */
    void truncateApprLmtSubBasicInfo02();

    /**
     * 插入关系表[一级额度分项下合同占额  + 项下二级分项下 合同占用类型的 合同占用总余额之和]
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfo02(@Param("openDay") String openDay);

    /**
     * 更新关系表[一级额度分项下合同占额  + 项下二级分项下 合同占用类型的 合同占用总余额之和]
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo02(@Param("openDay") String openDay);

    /**
     * 插入占用关系表-承兑行白名单额度已用金额更新
     *
     * @param openDay
     * @return
     */
    int insertTdlwiTla(@Param("openDay") String openDay);

    /**
     * 承兑行白名单额度已用金额更新
     *
     * @param openDay
     * @return
     */
    int updateTdlwiTla(@Param("openDay") String openDay);
}
