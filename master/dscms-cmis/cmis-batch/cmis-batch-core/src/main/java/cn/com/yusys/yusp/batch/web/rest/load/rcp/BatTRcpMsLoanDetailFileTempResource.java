/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.rcp;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatTRcpMsLoanDetailFileTemp;
import cn.com.yusys.yusp.batch.service.load.rcp.BatTRcpMsLoanDetailFileTempService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpMsLoanDetailFileTempResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:04:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battrcpmsloandetailfiletemp")
public class BatTRcpMsLoanDetailFileTempResource {
    @Autowired
    private BatTRcpMsLoanDetailFileTempService batTRcpMsLoanDetailFileTempService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTRcpMsLoanDetailFileTemp>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTRcpMsLoanDetailFileTemp> list = batTRcpMsLoanDetailFileTempService.selectAll(queryModel);
        return new ResultDto<List<BatTRcpMsLoanDetailFileTemp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTRcpMsLoanDetailFileTemp>> index(QueryModel queryModel) {
        List<BatTRcpMsLoanDetailFileTemp> list = batTRcpMsLoanDetailFileTempService.selectByModel(queryModel);
        return new ResultDto<List<BatTRcpMsLoanDetailFileTemp>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTRcpMsLoanDetailFileTemp> create(@RequestBody BatTRcpMsLoanDetailFileTemp batTRcpMsLoanDetailFileTemp) throws URISyntaxException {
        batTRcpMsLoanDetailFileTempService.insert(batTRcpMsLoanDetailFileTemp);
        return new ResultDto<BatTRcpMsLoanDetailFileTemp>(batTRcpMsLoanDetailFileTemp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTRcpMsLoanDetailFileTemp batTRcpMsLoanDetailFileTemp) throws URISyntaxException {
        int result = batTRcpMsLoanDetailFileTempService.update(batTRcpMsLoanDetailFileTemp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String contrNbr, String refNbr) {
        int result = batTRcpMsLoanDetailFileTempService.deleteByPrimaryKey(contrNbr, refNbr);
        return new ResultDto<Integer>(result);
    }

}
