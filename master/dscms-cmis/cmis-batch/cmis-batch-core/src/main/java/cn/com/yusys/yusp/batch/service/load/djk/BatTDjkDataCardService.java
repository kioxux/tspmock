/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.djk;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.djk.BatTDjkDataCard;
import cn.com.yusys.yusp.batch.repository.mapper.load.djk.BatTDjkDataCardMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTDjkDataCardService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 09:53:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTDjkDataCardService {
    private static final Logger logger = LoggerFactory.getLogger(BatTDjkDataCardService.class);
    @Autowired
    private BatTDjkDataCardMapper batTDjkDataCardMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTDjkDataCard selectByPrimaryKey(String logicalCardNo, String custId) {
        return batTDjkDataCardMapper.selectByPrimaryKey(logicalCardNo, custId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTDjkDataCard> selectAll(QueryModel model) {
        List<BatTDjkDataCard> records = (List<BatTDjkDataCard>) batTDjkDataCardMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTDjkDataCard> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTDjkDataCard> list = batTDjkDataCardMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTDjkDataCard record) {
        return batTDjkDataCardMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTDjkDataCard record) {
        return batTDjkDataCardMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTDjkDataCard record) {
        return batTDjkDataCardMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTDjkDataCard record) {
        return batTDjkDataCardMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String logicalCardNo, String custId) {
        return batTDjkDataCardMapper.deleteByPrimaryKey(logicalCardNo, custId);
    }
    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTDjkDataCardMapper.truncateTTable();
    }
}
