package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0223Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepLmtEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0223</br>
 * 任务名称：加工任务-额度处理-将额度相关临时表全量插入到额度正式表</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0223Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0223Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0223Service cmis0223Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息


    @Bean
    public Job cmis0223Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_JOB.key, JobStepLmtEnum.CMIS0223_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0223Job = this.jobBuilderFactory.get(JobStepLmtEnum.CMIS0223_JOB.key)
                .start(cmis0223UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0223CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(cmis0223DeleteInsertContAccRelStep(WILL_BE_INJECTED))//清空和插入合同占用关系信息
                .next(cmis0223DeleteInsertLmtContRelStep(WILL_BE_INJECTED))//清空和插入分项占用关系信息
                .next(cmis0223UpdateApprLmtSubBasicInfoStep(WILL_BE_INJECTED))//批复额度分项基础信息中金额更新
                .next(cmis0223UpdateApprCoopSubInfoStep(WILL_BE_INJECTED))//合作方授信分项信息中金额更新
                .next(cmis0223UpdateLmtWhiteInfoStep(WILL_BE_INJECTED))//白名单额度信息中金额更新
                .next(cmis0223UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0223Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0223UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_TASK010_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0223UpdateTask010Step = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0223_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0223_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_TASK010_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0223UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0223CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0223_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0223CheckRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0223_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0223_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0223_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0223_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0223CheckRelStep;
    }

    /**
     * 清空和插入合同占用关系信息
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0223DeleteInsertContAccRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_DELETE_INSERT_CONT_ACC_REL_INFO_STEP.key, JobStepLmtEnum.CMIS0223_DELETE_INSERT_CONT_ACC_REL_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0223DeleteInsertContAccRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0223_DELETE_INSERT_CONT_ACC_REL_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0223Service.cmis0223DeleteInsertContAccRel(openDay);//清空和插入合同占用关系信息
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_DELETE_INSERT_CONT_ACC_REL_INFO_STEP.key, JobStepLmtEnum.CMIS0223_DELETE_INSERT_CONT_ACC_REL_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0223DeleteInsertContAccRelStep;
    }


    /**
     * 清空和插入分项占用关系信息
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0223DeleteInsertLmtContRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0223DeleteInsertLmtContRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0223Service.cmis0223DeleteInsertLmtContRel(openDay);//清空和插入分项占用关系信息
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0223DeleteInsertLmtContRelStep;
    }


    /**
     * 批复额度分项基础信息中金额更新
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0223UpdateApprLmtSubBasicInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0223UpdateApprLmtSubBasicInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0223Service.cmis0223UpdateApprLmtSubBasicInfo(openDay);//批复额度分项基础信息中金额更新
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0223UpdateApprLmtSubBasicInfoStep;
    }

    /**
     * 合作方授信分项信息中金额更新
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0223UpdateApprCoopSubInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0223UpdateApprCoopSubInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0223Service.cmis0223UpdateApprCoopSubInfo(openDay);//更新合作方授信分项信息
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0223UpdateApprCoopSubInfoStep;
    }

    /**
     * 白名单额度信息中金额更新
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0223UpdateLmtWhiteInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0223UpdateLmtWhiteInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0223Service.cmis0223UpdateLmtWhiteInfo(openDay);//白名单额度信息中金额更新
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0223UpdateLmtWhiteInfoStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0223UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_TASK100_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0223UpdateTask100Step = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0223_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0223_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0223_UPDATE_TASK100_STEP.key, JobStepLmtEnum.CMIS0223_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0223UpdateTask100Step;
    }
}
