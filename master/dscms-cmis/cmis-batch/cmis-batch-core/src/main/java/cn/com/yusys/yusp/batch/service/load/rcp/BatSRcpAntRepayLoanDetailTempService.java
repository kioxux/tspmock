/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.rcp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpAntRepayLoanDetailTemp;
import cn.com.yusys.yusp.batch.repository.mapper.load.rcp.BatSRcpAntRepayLoanDetailTempMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpAntRepayLoanDetailTempService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:02:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSRcpAntRepayLoanDetailTempService {

    private static final Logger logger = LoggerFactory.getLogger(BatSRcpAntRepayLoanDetailTempService.class);


    @Autowired
    private BatSRcpAntRepayLoanDetailTempMapper batSRcpAntRepayLoanDetailTempMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSRcpAntRepayLoanDetailTemp selectByPrimaryKey(String contractNo) {
        return batSRcpAntRepayLoanDetailTempMapper.selectByPrimaryKey(contractNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSRcpAntRepayLoanDetailTemp> selectAll(QueryModel model) {
        List<BatSRcpAntRepayLoanDetailTemp> records = (List<BatSRcpAntRepayLoanDetailTemp>) batSRcpAntRepayLoanDetailTempMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSRcpAntRepayLoanDetailTemp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSRcpAntRepayLoanDetailTemp> list = batSRcpAntRepayLoanDetailTempMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSRcpAntRepayLoanDetailTemp record) {
        return batSRcpAntRepayLoanDetailTempMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSRcpAntRepayLoanDetailTemp record) {
        return batSRcpAntRepayLoanDetailTempMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSRcpAntRepayLoanDetailTemp record) {
        return batSRcpAntRepayLoanDetailTempMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSRcpAntRepayLoanDetailTemp record) {
        return batSRcpAntRepayLoanDetailTempMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String contractNo) {
        return batSRcpAntRepayLoanDetailTempMapper.deleteByPrimaryKey(contractNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSRcpAntRepayLoanDetailTempMapper.deleteByIds(ids);
    }

    /**
     * 将T表数据merge到S表
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]开始,请求参数为:{}", openDay);
        int updateSResult = batSRcpAntRepayLoanDetailTempMapper.updateDataDateByOpenDay(openDay);
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]结束,返回参数为:{}", updateSResult);
    }

    /**
     * 删除S表当天数据
     * @param openDay
     * @return
     */
    public int deleteByOpenDay(String openDay) {
        return batSRcpAntRepayLoanDetailTempMapper.deleteByOpenDay(openDay);
    }
}
