package cn.com.yusys.yusp.batch.service.bak;

import cn.com.yusys.yusp.batch.repository.mapper.bak.BakD0000Mapper;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKD0000</br>
 * 任务名称：批前备份日表任务-删除N天前数据</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
@Service
@Transactional
@Deprecated
public class BakD0000Service {
    private static final Logger logger = LoggerFactory.getLogger(BakD0000Service.class);

    @Autowired
    private BakD0000Mapper bakD0000Mapper;

    /**
     * 删除3天前的[银承台账[ACC_ACCP]]数据
     *
     * @param openDay
     */
    public void bakD0001Delete3DaysAccAccp(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("删除3天前的[银承台账[ACC_ACCP]]数据开始,请求参数为:[{}]", openDay);
        int bakD0001Delete3DaysAccAccpCounts = bakD0000Mapper.bakD0001Delete3DaysAccAccpCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0001Delete3DaysAccAccpCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0001Delete3DaysAccAccpCounts = times.intValue();
        logger.info("删除3天前的[银承台账[ACC_ACCP]]数据结束,返回参数为:[{}]", bakD0001Delete3DaysAccAccpCounts);

        for (int i = 0; i <= bakD0001Delete3DaysAccAccpCounts; i++) {
            // 删除3天前的数据
            logger.info("删除3天前的[银承台账[ACC_ACCP]]数据开始,请求参数为:[{}]", openDay);
            int bakD0001Delete3DaysAccAccp = bakD0000Mapper.bakD0001Delete3DaysAccAccp(openDay);
            logger.info("删除3天前的[银承台账[ACC_ACCP]]数据结束,返回参数为:[{}]", bakD0001Delete3DaysAccAccp);
        }
    }

    /**
     * 删除3天前的[银承台账票据明细 [ACC_ACCP_DRFT_SUB]]数据
     *
     * @param openDay
     */
    public void bakD0002Delete3DaysAccAccpDrftSub(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[银承台账票据明细 [ACC_ACCP_DRFT_SUB]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0002Delete3DaysAccAccpDrftSubCounts = bakD0000Mapper.bakD0002Delete3DaysAccAccpDrftSubCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0002Delete3DaysAccAccpDrftSubCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0002Delete3DaysAccAccpDrftSubCounts = times.intValue();
        logger.info("查询待删除3天前的[银承台账票据明细 [ACC_ACCP_DRFT_SUB]]数据总次数结束,返回参数为:[{}]", bakD0002Delete3DaysAccAccpDrftSubCounts);

        for (int i = 0; i <= bakD0002Delete3DaysAccAccpDrftSubCounts; i++) {
            // 删除3天前的数据
            logger.info("第[" + i + "]次删除3天前的[银承台账票据明细 [ACC_ACCP_DRFT_SUB]]数据开始,请求参数为:[{}]", openDay);
            int bakD0002Delete3DaysAccAccpDrftSub = bakD0000Mapper.bakD0002Delete3DaysAccAccpDrftSub(openDay);
            logger.info("第[" + i + "]次删除3天前的[银承台账票据明细 [ACC_ACCP_DRFT_SUB]]数据结束,返回参数为:[{}]", bakD0002Delete3DaysAccAccpDrftSub);
        }
    }

    /**
     * 删除3天前的[保函台账[ACC_CVRS]]数据
     *
     * @param openDay
     */
    public void bakD0003Delete3DaysAccCvrs(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[保函台账[ACC_CVRS]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0003Delete3DaysAccCvrsCounts = bakD0000Mapper.bakD0003Delete3DaysAccCvrsCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0003Delete3DaysAccCvrsCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0003Delete3DaysAccCvrsCounts = times.intValue();
        logger.info("查询待删除3天前的[保函台账[ACC_CVRS]]数据总次数结束,返回参数为:[{}]", bakD0003Delete3DaysAccCvrsCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0003Delete3DaysAccCvrsCounts; i++) {
            // 删除3天前的数据
            logger.info("第[" + i + "]次删除3天前的[保函台账[ACC_CVRS]]数据开始,请求参数为:[{}]", openDay);
            int bakD0003Delete3DaysAccCvrs = bakD0000Mapper.bakD0003Delete3DaysAccCvrs(openDay);
            logger.info("第[" + i + "]次删除3天前的[保函台账[ACC_CVRS]]数据结束,返回参数为:[{}]", bakD0003Delete3DaysAccCvrs);
        }
    }

    /**
     * 删除3天前的[贴现台账[ACC_DISC]]数据
     *
     * @param openDay
     */
    public void bakD0004Delete3DaysAccDisc(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[贴现台账[ACC_DISC]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0004Delete3DaysAccDiscCounts = bakD0000Mapper.bakD0004Delete3DaysAccDiscCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0004Delete3DaysAccDiscCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0004Delete3DaysAccDiscCounts = times.intValue();
        logger.info("查询待删除3天前的[贴现台账[ACC_DISC]]数据总次数结束,返回参数为:[{}]", bakD0004Delete3DaysAccDiscCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0004Delete3DaysAccDiscCounts; i++) {
            // 删除3天前的数据
            logger.info("第[" + i + "]次删除3天前的[贴现台账[ACC_DISC]]数据开始,请求参数为:[{}]", openDay);
            int bakD0004Delete3DaysAccDisc = bakD0000Mapper.bakD0004Delete3DaysAccDisc(openDay);
            logger.info("第[" + i + "]次删除3天前的[贴现台账[ACC_DISC]]数据结束,返回参数为:[{}]", bakD0004Delete3DaysAccDisc);
        }
    }

    /**
     * 删除3天前的[委托贷款台账[ACC_ENTRUST_LOAN]]数据
     *
     * @param openDay
     */
    public void bakD0005Delete3DaysAccEntrustLoan(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[贴现台账[ACC_DISC]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0005Delete3DaysccEntrustLoanCounts = bakD0000Mapper.bakD0005Delete3DaysccEntrustLoanCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0005Delete3DaysccEntrustLoanCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0005Delete3DaysccEntrustLoanCounts = times.intValue();
        logger.info("查询待删除3天前的[贴现台账[ACC_DISC]]数据总次数结束,返回参数为:[{}]", bakD0005Delete3DaysccEntrustLoanCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0005Delete3DaysccEntrustLoanCounts; i++) {
            // 删除3天前的数据
            logger.info("第[" + i + "]次删除3天前的[贴现台账[ACC_DISC]]数据开始,请求参数为:[{}]", openDay);
            int bakD0005Delete3DaysAccEntrustLoan = bakD0000Mapper.bakD0005Delete3DaysAccEntrustLoan(openDay);
            logger.info("第[" + i + "]次删除3天前的[贴现台账[ACC_DISC]]数据结束,返回参数为:[{}]", bakD0005Delete3DaysAccEntrustLoan);
        }
    }

    /**
     * 删除3天前的[贷款台账信息表[ACC_LOAN]]数据
     *
     * @param openDay
     */
    public void bakD0006Delete3DaysAccLoan(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[贷款台账信息表[ACC_LOAN]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0006Delete3DaysAccLoanCounts = bakD0000Mapper.bakD0006Delete3DaysAccLoanCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0006Delete3DaysAccLoanCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0006Delete3DaysAccLoanCounts = times.intValue();
        logger.info("查询待删除3天前的[贷款台账信息表[ACC_LOAN]]数据总次数结束,返回参数为:[{}]", bakD0006Delete3DaysAccLoanCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0006Delete3DaysAccLoanCounts; i++) {
            // 删除3天前的数据
            logger.info("第[" + i + "]次删除3天前的[贷款台账信息表[ACC_LOAN]]数据开始,请求参数为:[{}]", openDay);
            int bakD0006Delete3DaysAccLoan = bakD0000Mapper.bakD0006Delete3DaysAccLoan(openDay);
            logger.info("第[" + i + "]次删除3天前的[贷款台账信息表[ACC_LOAN]]数据结束,返回参数为:[{}]", bakD0006Delete3DaysAccLoan);
        }
    }

    /**
     * 删除3天前的[开证台账[ACC_TF_LOC]]数据
     *
     * @param openDay
     */
    public void bakD0007Delete3DaysAccTfLoc(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[开证台账[ACC_TF_LOC]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0007Delete3DaysAccTfLocCounts = bakD0000Mapper.bakD0007Delete3DaysAccTfLocCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0007Delete3DaysAccTfLocCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0007Delete3DaysAccTfLocCounts = times.intValue();
        logger.info("查询待删除3天前的[开证台账[ACC_TF_LOC]]数据总次数结束,返回参数为:[{}]", bakD0007Delete3DaysAccTfLocCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0007Delete3DaysAccTfLocCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[贷款台账信息表[ACC_LOAN]]数据开始,请求参数为:[{}]", openDay);
            int bakD0007Delete3DaysAccTfLoc = bakD0000Mapper.bakD0007Delete3DaysAccTfLoc(openDay);
            logger.info("第[" + i + "]次删除3天前的[开证台账[ACC_TF_LOC]]数据结束,返回参数为:[{}]", bakD0007Delete3DaysAccTfLoc);
        }
    }

    /**
     * 删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据
     *
     * @param openDay
     */
    public void bakD0008Delete3DaysApprCoopInfo(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0008Delete3DaysApprCoopInfoCounts = bakD0000Mapper.bakD0008Delete3DaysApprCoopInfoCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0008Delete3DaysApprCoopInfoCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0008Delete3DaysApprCoopInfoCounts = times.intValue();
        logger.info("查询待删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据总次数结束,返回参数为:[{}]", bakD0008Delete3DaysApprCoopInfoCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0008Delete3DaysApprCoopInfoCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据开始,请求参数为:[{}]", openDay);
            int bakD0008Delete3DaysApprCoopInfo = bakD0000Mapper.bakD0008Delete3DaysApprCoopInfo(openDay);
            logger.info("第[" + i + "]次删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据结束,返回参数为:[{}]", bakD0008Delete3DaysApprCoopInfo);
        }
    }

    /**
     * 删除3天前的[合作方授信分项信息[APPR_COOP_SUB_INFO]]数据
     *
     * @param openDay
     */
    public void bakD0009Delete3DaysApprCoopSub(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0009Delete3DaysApprCoopSubCounts = bakD0000Mapper.bakD0009Delete3DaysApprCoopSubCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0009Delete3DaysApprCoopSubCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0009Delete3DaysApprCoopSubCounts = times.intValue();
        logger.info("查询待删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据总次数结束,返回参数为:[{}]", bakD0009Delete3DaysApprCoopSubCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0009Delete3DaysApprCoopSubCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据开始,请求参数为:[{}]", openDay);
            int bakD0009Delete3DaysApprCoopSub = bakD0000Mapper.bakD0009Delete3DaysApprCoopSub(openDay);
            logger.info("第[" + i + "]次删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据结束,返回参数为:[{}]", bakD0009Delete3DaysApprCoopSub);
        }
    }

    /**
     * 删除3天前的[批复额度分项基础信息[APPR_LMT_SUB_BASIC_INFO]]数据
     *
     * @param openDay
     */
    public void bakD0010Delete3DaysApprLmtSubBasicInfo(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0010Delete3DaysApprLmtSubBasicInfoCounts = bakD0000Mapper.bakD0010Delete3DaysApprLmtSubBasicInfoCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0010Delete3DaysApprLmtSubBasicInfoCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0010Delete3DaysApprLmtSubBasicInfoCounts = times.intValue();
        logger.info("查询待删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据总次数结束,返回参数为:[{}]", bakD0010Delete3DaysApprLmtSubBasicInfoCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0010Delete3DaysApprLmtSubBasicInfoCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据开始,请求参数为:[{}]", openDay);
            int bakD0010Delete3DaysApprLmtSubBasicInfo = bakD0000Mapper.bakD0010Delete3DaysApprLmtSubBasicInfo(openDay);
            logger.info("第[" + i + "]次删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据结束,返回参数为:[{}]", bakD0010Delete3DaysApprLmtSubBasicInfo);
        }
    }

    /**
     * 删除3天前的[批复主信息[APPR_STR_MTABLE_INFO]]数据
     *
     * @param openDay
     */
    public void bakD0011Delete3DaysApprStrMtableInfo(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);
        logger.info("查询待删除3天前的[批复适用机构[APPR_STR_ORG_INFO]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0010Delete3DaysApprLmtSubBasicInfoCounts = bakD0000Mapper.bakD0011Delete3DaysApprStrMtableInfoCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0010Delete3DaysApprLmtSubBasicInfoCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0010Delete3DaysApprLmtSubBasicInfoCounts = times.intValue();
        logger.info("查询待删除3天前的[批复适用机构[APPR_STR_ORG_INFO]]数据总次数结束,返回参数为:[{}]", bakD0010Delete3DaysApprLmtSubBasicInfoCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0010Delete3DaysApprLmtSubBasicInfoCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据开始,请求参数为:[{}]", openDay);
            int bakD0011Delete3DaysApprStrMtableInfo = bakD0000Mapper.bakD0011Delete3DaysApprStrMtableInfo(openDay);
            logger.info("第[" + i + "]次删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据结束,返回参数为:[{}]", bakD0011Delete3DaysApprStrMtableInfo);
        }
    }

    /**
     * 删除3天前的[批复适用机构[APPR_STR_ORG_INFO]]数据
     *
     * @param openDay
     */
    public void bakD0012Delete3DaysApprStrOrgInfo(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[批复适用机构[APPR_STR_ORG_INFO]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0012Delete3DaysApprStrOrgInfoCounts = bakD0000Mapper.bakD0012Delete3DaysApprStrOrgInfoCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0012Delete3DaysApprStrOrgInfoCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0012Delete3DaysApprStrOrgInfoCounts = times.intValue();
        logger.info("查询待删除3天前的[批复适用机构[APPR_STR_ORG_INFO]]数据总次数结束,返回参数为:[{}]", bakD0012Delete3DaysApprStrOrgInfoCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0012Delete3DaysApprStrOrgInfoCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[批复适用机构[APPR_STR_ORG_INFO]]数据开始,请求参数为:[{}]", openDay);
            int bakD0012Delete3DaysApprStrOrgInfo = bakD0000Mapper.bakD0012Delete3DaysApprStrOrgInfo(openDay);
            logger.info("第[" + i + "]次删除3天前的[批复适用机构[APPR_STR_ORG_INFO]]数据结束,返回参数为:[{}]", bakD0012Delete3DaysApprStrOrgInfo);
        }
    }

    /**
     * 删除3天前的[合同占用关系信息[CONT_ACC_REL]]数据
     *
     * @param openDay
     */
    public void bakD0013Delete3DaysContAccRel(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);
        logger.info("查询待删除3天前的[合同占用关系信息[CONT_ACC_REL]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0013Delete3DaysContAccRelCounts = bakD0000Mapper.bakD0013Delete3DaysContAccRelCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0013Delete3DaysContAccRelCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0013Delete3DaysContAccRelCounts = times.intValue();
        logger.info("查询待删除3天前的[合同占用关系信息[CONT_ACC_REL]]数据总次数结束,返回参数为:[{}]", bakD0013Delete3DaysContAccRelCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0013Delete3DaysContAccRelCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[合同占用关系信息[CONT_ACC_REL]]数据开始,请求参数为:[{}]", openDay);
            int bakD0013Delete3DaysContAccRel = bakD0000Mapper.bakD0013Delete3DaysContAccRel(openDay);
            logger.info("第[" + i + "]次删除3天前的[合同占用关系信息[CONT_ACC_REL]]数据结束,返回参数为:[{}]", bakD0013Delete3DaysContAccRel);
        }
    }

    /**
     * 删除3天前的[银承合同详情[CTR_ACCP_CONT]]数据
     *
     * @param openDay
     */
    public void bakD0014Delete3DaysCtrAccpCont(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[银承合同详情[CTR_ACCP_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0014Delete3DaysCtrAccpContCounts = bakD0000Mapper.bakD0014Delete3DaysCtrAccpContCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0014Delete3DaysCtrAccpContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0014Delete3DaysCtrAccpContCounts = times.intValue();
        logger.info("查询待删除3天前的[银承合同详情[CTR_ACCP_CONT]]数据总次数结束,返回参数为:[{}]", bakD0014Delete3DaysCtrAccpContCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0014Delete3DaysCtrAccpContCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[银承合同详情[CTR_ACCP_CONT]]数据开始,请求参数为:[{}]", openDay);
            int bakD0014Delete3DaysCtrAccpCont = bakD0000Mapper.bakD0014Delete3DaysCtrAccpCont(openDay);
            logger.info("第[" + i + "]次删除3天前的[银承合同详情[CTR_ACCP_CONT]]数据结束,返回参数为:[{}]", bakD0014Delete3DaysCtrAccpCont);
        }
    }

    /**
     * 删除3天前的[资产池协议[CTR_ASPL_DETAILS]]数据
     *
     * @param openDay
     */
    public void bakD0015Delete3DaysCtrAsplDetails(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[资产池协议[CTR_ASPL_DETAILS]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0015Delete3DaysCtrAsplDetailsCounts = bakD0000Mapper.bakD0015Delete3DaysCtrAsplDetailsCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0015Delete3DaysCtrAsplDetailsCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0015Delete3DaysCtrAsplDetailsCounts = times.intValue();
        logger.info("查询待删除3天前的[资产池协议[CTR_ASPL_DETAILS]]数据总次数结束,返回参数为:[{}]", bakD0015Delete3DaysCtrAsplDetailsCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0015Delete3DaysCtrAsplDetailsCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[资产池协议[CTR_ASPL_DETAILS]]数据开始,请求参数为:[{}]", openDay);
            int bakD0015Delete3DaysCtrAsplDetails = bakD0000Mapper.bakD0015Delete3DaysCtrAsplDetails(openDay);
            logger.info("第[" + i + "]次删除3天前的[资产池协议[CTR_ASPL_DETAILS]]数据结束,返回参数为:[{}]", bakD0015Delete3DaysCtrAsplDetails);
        }
    }

    /**
     * 删除3天前的[保函协议详情[CTR_CVRG_CONT]]数据
     *
     * @param openDay
     */
    public void bakD0016Delete3DaysCtrCvrgCont(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);
        logger.info("查询待删除3天前的[保函协议详情[CTR_CVRG_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0016Delete3DaysCtrCvrgContCounts = bakD0000Mapper.bakD0016Delete3DaysCtrCvrgContCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0016Delete3DaysCtrCvrgContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0016Delete3DaysCtrCvrgContCounts = times.intValue();
        logger.info("查询待删除3天前的[保函协议详情[CTR_CVRG_CONT]]数据总次数结束,返回参数为:[{}]", bakD0016Delete3DaysCtrCvrgContCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0016Delete3DaysCtrCvrgContCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[保函协议详情[CTR_CVRG_CONT]]数据开始,请求参数为:[{}]", openDay);
            int bakD0016Delete3DaysCtrCvrgCont = bakD0000Mapper.bakD0016Delete3DaysCtrCvrgCont(openDay);
            logger.info("第[" + i + "]次删除3天前的[保函协议详情[CTR_CVRG_CONT]]数据结束,返回参数为:[{}]", bakD0016Delete3DaysCtrCvrgCont);
        }
    }

    /**
     * 删除3天前的[贴现协议详情[CTR_DISC_CONT]]数据
     *
     * @param openDay
     */
    public void bakD0017Delete3DaysCtrDiscCont(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[贴现协议详情[CTR_DISC_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0017Delete3DaysCtrDiscContCounts = bakD0000Mapper.bakD0017Delete3DaysCtrDiscContCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0017Delete3DaysCtrDiscContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0017Delete3DaysCtrDiscContCounts = times.intValue();
        logger.info("查询待删除3天前的[贴现协议详情[CTR_DISC_CONT]]数据总次数结束,返回参数为:[{}]", bakD0017Delete3DaysCtrDiscContCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0017Delete3DaysCtrDiscContCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[贴现协议详情[CTR_DISC_CONT]]数据开始,请求参数为:[{}]", openDay);
            int bakD0017Delete3DaysCtrDiscCont = bakD0000Mapper.bakD0017Delete3DaysCtrDiscCont(openDay);
            logger.info("第[" + i + "]次删除3天前的[贴现协议详情[CTR_DISC_CONT]]数据结束,返回参数为:[{}]", bakD0017Delete3DaysCtrDiscCont);
        }
    }

    /**
     * 删除3天前的[贴现协议汇票明细[CTR_DISC_PORDER_SUB]]数据
     *
     * @param openDay
     */
    public void bakD0018Delete3DaysCtrDiscPorderSub(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[贴现协议汇票明细[CTR_DISC_PORDER_SUB]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0018Delete3DaysCtrDiscPorderSubCounts = bakD0000Mapper.bakD0018Delete3DaysCtrDiscPorderSubCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0018Delete3DaysCtrDiscPorderSubCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0018Delete3DaysCtrDiscPorderSubCounts = times.intValue();
        logger.info("查询待删除3天前的[贴现协议汇票明细[CTR_DISC_PORDER_SUB]]数据总次数结束,返回参数为:[{}]", bakD0018Delete3DaysCtrDiscPorderSubCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0018Delete3DaysCtrDiscPorderSubCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[贴现协议汇票明细[CTR_DISC_PORDER_SUB]]数据开始,请求参数为:[{}]", openDay);
            int bakD0018Delete3DaysCtrDiscPorderSub = bakD0000Mapper.bakD0018Delete3DaysCtrDiscPorderSub(openDay);
            logger.info("第[" + i + "]次删除3天前的[贴现协议汇票明细[CTR_DISC_PORDER_SUB]]数据结束,返回参数为:[{}]", bakD0018Delete3DaysCtrDiscPorderSub);
        }
    }

    /**
     * 删除3天前的[委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]]数据
     *
     * @param openDay
     */
    public void bakD0019Delete3DaysCtrEntrustLoanCont(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0019Delete3DaysCtrEntrustLoanContCounts = bakD0000Mapper.bakD0019Delete3DaysCtrEntrustLoanContCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0019Delete3DaysCtrEntrustLoanContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0019Delete3DaysCtrEntrustLoanContCounts = times.intValue();
        logger.info("查询待删除3天前的[委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]]数据总次数结束,返回参数为:[{}]", bakD0019Delete3DaysCtrEntrustLoanContCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0019Delete3DaysCtrEntrustLoanContCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]]数据开始,请求参数为:[{}]", openDay);
            int bakD0019Delete3DaysCtrEntrustLoanCont = bakD0000Mapper.bakD0019Delete3DaysCtrEntrustLoanCont(openDay);
            logger.info("第[" + i + "]次删除3天前的[委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]]数据结束,返回参数为:[{}]", bakD0019Delete3DaysCtrEntrustLoanCont);
        }
    }

    /**
     * 删除3天前的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据
     *
     * @param openDay
     */
    public void bakD0020Delete3DaysCtrHighAmtAgrCont(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0020Delete3DaysCtrHighAmtAgrContCounts = bakD0000Mapper.bakD0020Delete3DaysCtrHighAmtAgrContCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0020Delete3DaysCtrHighAmtAgrContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0020Delete3DaysCtrHighAmtAgrContCounts = times.intValue();
        logger.info("查询待删除3天前的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据总次数结束,返回参数为:[{}]", bakD0020Delete3DaysCtrHighAmtAgrContCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0020Delete3DaysCtrHighAmtAgrContCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据开始,请求参数为:[{}]", openDay);
            int bakD0020Delete3DaysCtrHighAmtAgrCont = bakD0000Mapper.bakD0020Delete3DaysCtrHighAmtAgrCont(openDay);
            logger.info("第[" + i + "]次删除3天前的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据结束,返回参数为:[{}]", bakD0020Delete3DaysCtrHighAmtAgrCont);
        }
    }

    /**
     * 删除3天前的[贷款合同表[CTR_LOAN_CONT]]数据
     *
     * @param openDay
     */
    public void bakD0021Delete3DaysCtrLoanCont(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[贷款合同表[CTR_LOAN_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0021Delete3DaysCtrLoanContCounts = bakD0000Mapper.bakD0021Delete3DaysCtrLoanContCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0021Delete3DaysCtrLoanContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0021Delete3DaysCtrLoanContCounts = times.intValue();
        logger.info("查询待删除3天前的[贷款合同表[CTR_LOAN_CONT]]数据总次数结束,返回参数为:[{}]", bakD0021Delete3DaysCtrLoanContCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0021Delete3DaysCtrLoanContCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[贷款合同表[CTR_LOAN_CONT]]数据开始,请求参数为:[{}]", openDay);
            int bakD0021Delete3DaysCtrLoanCont = bakD0000Mapper.bakD0021Delete3DaysCtrLoanCont(openDay);
            logger.info("第[" + i + "]次删除3天前的[贷款合同表[CTR_LOAN_CONT]]数据结束,返回参数为:[{}]", bakD0021Delete3DaysCtrLoanCont);
        }
    }

    /**
     * 删除3天前的[行名行号对照表[CFG_BANK_INFO]]数据
     *
     * @param openDay
     */
    public void bakD0022Delete3DaysCfgBankInfo(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[行名行号对照表[CFG_BANK_INFO]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0022Delete3DaysCfgBankInfoCounts = bakD0000Mapper.bakD0022Delete3DaysCfgBankInfoCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0022Delete3DaysCfgBankInfoCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0022Delete3DaysCfgBankInfoCounts = times.intValue();
        logger.info("查询待删除3天前的[行名行号对照表[CFG_BANK_INFO]]数据总次数结束,返回参数为:[{}]", bakD0022Delete3DaysCfgBankInfoCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0022Delete3DaysCfgBankInfoCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[行名行号对照表[CFG_BANK_INFO]]数据开始,请求参数为:[{}]", openDay);
            int bakD0022Delete3DaysCfgBankInfo = bakD0000Mapper.bakD0022Delete3DaysCfgBankInfo(openDay);
            logger.info("第[" + i + "]次删除3天前的[行名行号对照表[CFG_BANK_INFO]]数据结束,返回参数为:[{}]", bakD0022Delete3DaysCfgBankInfo);
        }
    }

    /**
     * 删除3天前的[开证合同详情[CTR_TF_LOC_CONT]]数据
     *
     * @param openDay
     */
    public void bakD0023Delete3DaysCtrTfLocCont(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[开证合同详情[CTR_TF_LOC_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0023Delete3DaysCtrTfLocContCounts = bakD0000Mapper.bakD0023Delete3DaysCtrTfLocContCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0023Delete3DaysCtrTfLocContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0023Delete3DaysCtrTfLocContCounts = times.intValue();
        logger.info("查询待删除3天前的[开证合同详情[CTR_TF_LOC_CONT]]数据总次数结束,返回参数为:[{}]", bakD0023Delete3DaysCtrTfLocContCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0023Delete3DaysCtrTfLocContCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[开证合同详情[CTR_TF_LOC_CONT]]数据开始,请求参数为:[{}]", openDay);
            int bakD0023Delete3DaysCtrTfLocCont = bakD0000Mapper.bakD0023Delete3DaysCtrTfLocCont(openDay);
            logger.info("第[" + i + "]次删除3天前的[开证合同详情[CTR_TF_LOC_CONT]]数据结束,返回参数为:[{}]", bakD0023Delete3DaysCtrTfLocCont);
        }
    }

    /**
     * 删除3天前的[权证台账[GUAR_WARRANT_INFO]]数据
     *
     * @param openDay
     */
    public void bakD0024Delete3DaysGuarWarrantInfo(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[权证台账[GUAR_WARRANT_INFO]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0024Delete3DaysGuarWarrantInfoCounts = bakD0000Mapper.bakD0024Delete3DaysGuarWarrantInfoCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0024Delete3DaysGuarWarrantInfoCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0024Delete3DaysGuarWarrantInfoCounts = times.intValue();
        logger.info("查询待删除3天前的[权证台账[GUAR_WARRANT_INFO]]数据总次数结束,返回参数为:[{}]", bakD0024Delete3DaysGuarWarrantInfoCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0024Delete3DaysGuarWarrantInfoCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[权证台账[GUAR_WARRANT_INFO]]数据开始,请求参数为:[{}]", openDay);
            int bakD0024Delete3DaysGuarWarrantInfo = bakD0000Mapper.bakD0024Delete3DaysGuarWarrantInfo(openDay);
            logger.info("第[" + i + "]次删除3天前的[权证台账[GUAR_WARRANT_INFO]]数据结束,返回参数为:[{}]", bakD0024Delete3DaysGuarWarrantInfo);
        }
    }

    /**
     * 删除3天前的[担保合同表[GRT_GUAR_CONT]]数据
     *
     * @param openDay
     */
    public void bakD0025Delete3DaysGrtGuarCont(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[担保合同表[GRT_GUAR_CONT]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0025Delete3DaysGrtGuarContCounts = bakD0000Mapper.bakD0025Delete3DaysGrtGuarContCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0025Delete3DaysGrtGuarContCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0025Delete3DaysGrtGuarContCounts = times.intValue();
        logger.info("查询待删除3天前的[担保合同表[GRT_GUAR_CONT]]数据总次数结束,返回参数为:[{}]", bakD0025Delete3DaysGrtGuarContCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0025Delete3DaysGrtGuarContCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[担保合同表[GRT_GUAR_CONT]]数据开始,请求参数为:[{}]", openDay);
            int bakD0025Delete3DaysGrtGuarCont = bakD0000Mapper.bakD0025Delete3DaysGrtGuarCont(openDay);
            logger.info("第[" + i + "]次删除3天前的[担保合同表[GRT_GUAR_CONT]]数据结束,返回参数为:[{}]", bakD0025Delete3DaysGrtGuarCont);
        }
    }

    /**
     * 删除3天前的[分项占用关系信息[LMT_CONT_REL]]数据
     *
     * @param openDay
     */
    public void bakD0026Delete3DaysLmtContRel(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[分项占用关系信息[LMT_CONT_REL]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0026Delete3DaysLmtContRelCounts = bakD0000Mapper.bakD0026Delete3DaysLmtContRelCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0026Delete3DaysLmtContRelCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0026Delete3DaysLmtContRelCounts = times.intValue();
        logger.info("查询待删除3天前的[分项占用关系信息[LMT_CONT_REL]]数据总次数结束,返回参数为:[{}]", bakD0026Delete3DaysLmtContRelCounts);

        // 删除3天前的数据
        for (int i = 0; i <= bakD0026Delete3DaysLmtContRelCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[分项占用关系信息[LMT_CONT_REL]]数据开始,请求参数为:[{}]", openDay);
            int bakD0026Delete3DaysLmtContRel = bakD0000Mapper.bakD0026Delete3DaysLmtContRel(openDay);
            logger.info("第[" + i + "]次删除3天前的[分项占用关系信息[LMT_CONT_REL]]数据结束,返回参数为:[{}]", bakD0026Delete3DaysLmtContRel);
        }
    }

    /**
     * 删除3天前的[白名单额度信息[LMT_WHITE_INFO]]数据
     *
     * @param openDay
     */
    public void bakD0027Delete3DaysLmtWhiteInfo(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[白名单额度信息[LMT_WHITE_INFO]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0027Delete3DaysLmtWhiteInfoCounts = bakD0000Mapper.bakD0027Delete3DaysLmtWhiteInfoCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0027Delete3DaysLmtWhiteInfoCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0027Delete3DaysLmtWhiteInfoCounts = times.intValue();
        logger.info("查询待删除3天前的[白名单额度信息[LMT_WHITE_INFO]]数据总次数结束,返回参数为:[{}]", bakD0027Delete3DaysLmtWhiteInfoCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0027Delete3DaysLmtWhiteInfoCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[白名单额度信息[LMT_WHITE_INFO]]数据开始,请求参数为:[{}]", openDay);
            int bakD0027Delete3DaysLmtWhiteInfo = bakD0000Mapper.bakD0027Delete3DaysLmtWhiteInfo(openDay);
            logger.info("第[" + i + "]次删除3天前的[白名单额度信息[LMT_WHITE_INFO]]数据结束,返回参数为:[{}]", bakD0027Delete3DaysLmtWhiteInfo);
        }
    }

    /**
     * 删除3天前的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据
     *
     * @param openDay
     */
    public void bakD0028Delete3DaysLmtWhiteInfoHistory(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据总次数开始,请求参数为:[{}]", openDay);
        int bakD0028Delete3DaysLmtWhiteInfoHistoryCounts = bakD0000Mapper.bakD0028Delete3DaysLmtWhiteInfoHistoryCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(bakD0028Delete3DaysLmtWhiteInfoHistoryCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakD0028Delete3DaysLmtWhiteInfoHistoryCounts = times.intValue();
        logger.info("查询待删除3天前的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据总次数结束,返回参数为:[{}]", bakD0028Delete3DaysLmtWhiteInfoHistoryCounts);
        // 删除3天前的数据
        for (int i = 0; i <= bakD0028Delete3DaysLmtWhiteInfoHistoryCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据开始,请求参数为:[{}]", openDay);
            int bakD0028Delete3DaysLmtWhiteInfoHistory = bakD0000Mapper.bakD0028Delete3DaysLmtWhiteInfoHistory(openDay);
            logger.info("第[" + i + "]次删除3天前的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据结束,返回参数为:[{}]", bakD0028Delete3DaysLmtWhiteInfoHistory);
        }
    }

    /**
     * 删除3天前的[我行代开他行保函[TMP_GJP_LMT_CVRG_RZKZ]]数据
     *
     * @param openDay
     */
    public void bakD0029Delete3DaysTmpGjpLmtCvrgRzkz(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[我行代开他行保函[TMP_GJP_LMT_CVRG_RZKZ]]数据总次数开始,请求参数为:[{}]", openDay);
        int queryD0029Delete3DaysTmpGjpLmtCvrgRzkzCounts = bakD0000Mapper.queryD0029Delete3DaysTmpGjpLmtCvrgRzkzCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(queryD0029Delete3DaysTmpGjpLmtCvrgRzkzCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        queryD0029Delete3DaysTmpGjpLmtCvrgRzkzCounts = times.intValue();
        logger.info("查询待删除3天前的[我行代开他行保函[TMP_GJP_LMT_CVRG_RZKZ]]数据总次数结束,返回参数为:[{}]", queryD0029Delete3DaysTmpGjpLmtCvrgRzkzCounts);

        // 删除3天前的数据
        for (int i = 0; i <= queryD0029Delete3DaysTmpGjpLmtCvrgRzkzCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[我行代开他行保函[TMP_GJP_LMT_CVRG_RZKZ]]数据开始,请求参数为:[{}]", openDay);
            int bakD0029Delete3DaysTmpGjpLmtCvrgRzkz = bakD0000Mapper.bakD0029Delete3DaysTmpGjpLmtCvrgRzkz(openDay);
            logger.info("第[" + i + "]次删除3天前的[我行代开他行保函[TMP_GJP_LMT_CVRG_RZKZ]]数据结束,返回参数为:[{}]", bakD0029Delete3DaysTmpGjpLmtCvrgRzkz);
        }
    }

    /**
     * 删除3天前的[我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]]数据
     *
     * @param openDay
     */
    public void bakD0030Delete3DaysTmpGjpLmtPeerRzkz(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]]数据总次数开始,请求参数为:[{}]", openDay);
        int queryD0030Delete3DaysTmpGjpLmtPeerRzkzCounts = bakD0000Mapper.queryD0030Delete3DaysTmpGjpLmtPeerRzkzCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(queryD0030Delete3DaysTmpGjpLmtPeerRzkzCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        queryD0030Delete3DaysTmpGjpLmtPeerRzkzCounts = times.intValue();
        logger.info("查询待删除3天前的[我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]]]数据总次数结束,返回参数为:[{}]", queryD0030Delete3DaysTmpGjpLmtPeerRzkzCounts);

        // 删除3天前的数据
        for (int i = 0; i <= queryD0030Delete3DaysTmpGjpLmtPeerRzkzCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]]数据开始,请求参数为:[{}]", openDay);
            int bakD0030Delete3DaysTmpGjpLmtPeerRzkz = bakD0000Mapper.bakD0030Delete3DaysTmpGjpLmtPeerRzkz(openDay);
            logger.info("第[" + i + "]次删除3天前的[我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]]数据结束,返回参数为:[{}]", bakD0030Delete3DaysTmpGjpLmtPeerRzkz);
        }

    }

    /**
     * 删除3天前的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据
     *
     * @param openDay
     */
    public void bakD0031Delete3DaysTmpGjpLmtFftRzkz(String openDay) {
        logger.info("计算3天前日期开始,请求参数为:[{}]", openDay);
        openDay = DateUtils.addDay(openDay, "yyyy-MM-dd", -3); // 3天前日期
        logger.info("计算3天前日期结束,返回参数为:[{}]", openDay);

        logger.info("查询待删除3天前的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据总次数开始,请求参数为:[{}]", openDay);
        int queryD0031Delete3DaysTmpGjpLmtFftRzkzCounts = bakD0000Mapper.queryD0031Delete3DaysTmpGjpLmtFftRzkzCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(queryD0031Delete3DaysTmpGjpLmtFftRzkzCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        queryD0031Delete3DaysTmpGjpLmtFftRzkzCounts = times.intValue();
        logger.info("查询待删除3天前的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据总次数结束,返回参数为:[{}]", queryD0031Delete3DaysTmpGjpLmtFftRzkzCounts);

        // 删除3天前的数据
        for (int i = 0; i <= queryD0031Delete3DaysTmpGjpLmtFftRzkzCounts; i++) {
            logger.info("第[" + i + "]次删除3天前的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据开始,请求参数为:[{}]", openDay);
            int bakD0031Delete3DaysTmpGjpLmtFftRzkz = bakD0000Mapper.bakD0031Delete3DaysTmpGjpLmtFftRzkz(openDay);
            logger.info("第[" + i + "]次删除3天前的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据结束,返回参数为:[{}]", bakD0031Delete3DaysTmpGjpLmtFftRzkz);
        }

    }


}
