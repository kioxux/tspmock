package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0214</br>
 * 任务名称：加工任务-额度处理-授信分项计算可出账金额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0214Mapper {


    /**
     * 插入临时表-批复主信息
     *
     * @param openDay
     * @return
     */
    int insertTmpApprStrMtableInfo(@Param("openDay") String openDay);


    /**
     * 插入临时表-分项占用关系信息加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpDealLmtContRel(@Param("openDay") String openDay);


    /**
     * 插入临时表-合同占用关系信息加工表
     *
     * @param openDay
     * @return
     */
    int insertTmpDealContAccRel(@Param("openDay") String openDay);


    /**
     * 插入临时表-合同占用关系信息加工表合同
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtCont(@Param("openDay") String openDay);

    /**
     * 插入临时表-合同占用关系信息加工表台账
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtAcc(@Param("openDay") String openDay);


    /**
     * 插入关系表
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfoB1(@Param("openDay") String openDay);

    /**
     * 计算普通贷款台账 垫款台账  已出账金额 （授信可循环 则取贷款余额，不循环 则取 贷款金额 ）， 可出账金额=（授信总额（低风险授信）/授信敞口金额  减去  已出账金额）
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfoB1(@Param("openDay") String openDay);

    /**
     * 插入临时表-分项项下关联的非最高额授信协议的合同下的已出账金额之和
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtCont2(@Param("openDay") String openDay);

    /**
     * 插入二级分项项下的已出账金额之和
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtB(@Param("openDay") String openDay);

    /**
     * 插入分项项下关联的台账已出账金额之和
     *
     * @param openDay
     * @return
     */
    int insertTmpLmtAmtAcc2(@Param("openDay") String openDay);


    /**
     * 插入关系表
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfoB2(@Param("openDay") String openDay);

    /**
     * 含有二级分项的一级分项已出账金额汇总
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfoB2(@Param("openDay") String openDay);

    /**
     * 插入关系表
     *
     * @param openDay
     * @return
     */
    int insertApprLmtSubBasicInfoC1(@Param("openDay") String openDay);

    /**
     * 含有二级分项的一级分项已出账金额汇总
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfoC1(@Param("openDay") String openDay);

}
