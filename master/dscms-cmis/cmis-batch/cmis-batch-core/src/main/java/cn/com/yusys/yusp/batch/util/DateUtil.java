package cn.com.yusys.yusp.batch.util;

import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * 日期处理公共方法类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public class DateUtil {
    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);


    /**
     * 根据日期类型、任务日期计算相对日期（日期类型为空或异常时，原样返回任务日期;日期类型不为空时，计算相对日期）
     *
     * @param dateType 日期类型[T(、T+1、T-1三种方式]
     * @param taskDate 任务日期
     * @return 相对日期
     */
    public static String countRelativeDate(String dateType, String taskDate) {
        logger.info("根据日期类型：[" + dateType + "]、任务日期：[" + taskDate + "]计算相对日期开始");
        String retDate = "";
        try {
            if (dateType != null && !"".equals(dateType)) {// 如果日期类型不为空，根据日期类型计算相对日期
                // 计算日期
                if (Objects.equals(BatEnums.DATE_TYPE_T.key, dateType)) {
                    retDate = taskDate;
                } else if (Objects.equals(BatEnums.DATE_TYPE_T_PLUS.key, dateType.substring(0, 2))) {
                    int num = Integer.parseInt(dateType.substring(2));
                    retDate = DateUtils.addDay(taskDate, "yyyy-MM-dd", num);
                } else if (Objects.equals(BatEnums.DATE_TYPE_T_MINUS.key, dateType.substring(0, 2))) {
                    int num = Integer.parseInt(dateType.substring(1));
                    retDate = DateUtils.addDay(taskDate, "yyyy-MM-dd", num);
                } else {
                    retDate = taskDate;
                    logger.error("无匹配的日期类型" + dateType);
                }
            } else { // 日期类型为空时，将日期原样返回
                retDate = taskDate;
            }
        } catch (Exception e) {
            retDate = taskDate;
            logger.error("根据日期类型：[" + dateType + "]、任务日期：[" + taskDate + "]计算相对日期异常：" + e.getMessage(), e);
        }
        logger.info("根据日期类型：[" + dateType + "]、任务日期：[" + taskDate + "]计算相对日期结束，相对日期为：[" + retDate + "]");
        return retDate;
    }

    /**
     * 根据日期类型、任务日期计算相对日期（日期类型为空或异常时，原样返回任务日期;日期类型不为空时，计算相对日期）
     *
     * @param dateType    日期类型[T(、T+1、T-1三种方式]
     * @param datePattern 日期格式，yyyy-MM-dd
     * @param taskDate    任务日期
     * @return 相对日期
     */
    public static String countRelativeDate(String dateType, String datePattern, String taskDate) {
        logger.info("根据日期类型：[" + dateType + "]、日期格式：[" + datePattern + "]、任务日期：[" + taskDate + "]计算相对日期开始");
        String retDate = "";
        try {
            if (dateType != null && !"".equals(dateType)) {// 如果日期类型不为空，根据日期类型计算相对日期
                // 计算日期
                if (Objects.equals(BatEnums.DATE_TYPE_T.key, dateType)) {
                    retDate = taskDate;
                } else if (Objects.equals(BatEnums.DATE_TYPE_T_PLUS.key, dateType.substring(0, 2))) {
                    int num = Integer.parseInt(dateType.substring(2));
                    retDate = DateUtils.addDay(taskDate, datePattern, num);
                } else if (Objects.equals(BatEnums.DATE_TYPE_T_MINUS.key, dateType.substring(0, 2))) {
                    int num = Integer.parseInt(dateType.substring(1));
                    retDate = DateUtils.addDay(taskDate, datePattern, num);
                } else {
                    retDate = taskDate;
                    logger.error("无匹配的日期类型" + dateType);
                }
            } else { // 日期类型为空时，将日期原样返回
                retDate = taskDate;
            }
        } catch (Exception e) {
            retDate = taskDate;
            logger.error("根据日期类型：[" + dateType + "]、日期格式：[" + datePattern + "]、任务日期：[" + taskDate + "]计算相对日期异常：" + e.getMessage(), e);
        }
        logger.info("根据日期类型：[" + dateType + "]、日期格式：[" + datePattern + "]、任务日期：[" + taskDate + "]计算相对日期结束，相对日期为：[" + retDate + "]");
        return retDate;
    }

    public static void main(String[] args) {
        String dateType = "T-1";
        String datePattern = "yyyy-MM-dd";
        String taskDate = "2021-06-17";
        String result = countRelativeDate(dateType, datePattern, taskDate);
        System.out.println(result);
    }
}
