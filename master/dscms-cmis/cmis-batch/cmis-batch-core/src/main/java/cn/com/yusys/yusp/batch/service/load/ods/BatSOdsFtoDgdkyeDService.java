/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.ods;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpRptFiveClassifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.ods.BatSOdsFtoDgdkyeD;
import cn.com.yusys.yusp.batch.repository.mapper.load.ods.BatSOdsFtoDgdkyeDMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSOdsFtoDgdkyeDService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-25 16:35:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSOdsFtoDgdkyeDService {
    private static final Logger logger = LoggerFactory.getLogger(BatSRcpRptFiveClassifyService.class);
    @Autowired
    private TableUtilsService tableUtilsService;
    @Autowired
    private BatSOdsFtoDgdkyeDMapper batSOdsFtoDgdkyeDMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSOdsFtoDgdkyeD selectByPrimaryKey(String acctdt, String brchno, java.math.BigDecimal loanbl) {
        return batSOdsFtoDgdkyeDMapper.selectByPrimaryKey(acctdt, brchno, loanbl);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSOdsFtoDgdkyeD> selectAll(QueryModel model) {
        List<BatSOdsFtoDgdkyeD> records = (List<BatSOdsFtoDgdkyeD>) batSOdsFtoDgdkyeDMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSOdsFtoDgdkyeD> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSOdsFtoDgdkyeD> list = batSOdsFtoDgdkyeDMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSOdsFtoDgdkyeD record) {
        return batSOdsFtoDgdkyeDMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSOdsFtoDgdkyeD record) {
        return batSOdsFtoDgdkyeDMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSOdsFtoDgdkyeD record) {
        return batSOdsFtoDgdkyeDMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSOdsFtoDgdkyeD record) {
        return batSOdsFtoDgdkyeDMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String acctdt, String brchno, java.math.BigDecimal loanbl) {
        return batSOdsFtoDgdkyeDMapper.deleteByPrimaryKey(acctdt, brchno, loanbl);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建数仓-机构上日对公贷款余额[bat_s_ods_fto_dgdkye_d]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_ods_fto_dgdkye_d");
        logger.info("重建数仓-机构上日对公贷款余额[bat_s_ods_fto_dgdkye_d]结束");
        return 0;
    }
}
