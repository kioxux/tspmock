package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0401Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0401</br>
 * 任务名称：加工任务-业务处理-对公经营性首次检查  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0401Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0401Service.class);

    @Autowired
    private Cmis0401Mapper cmis0401Mapper;

    /**
     * 插入贷后检查任务表
     * @param openDay
     */
    public void cmis0401InsertPspTaskList(String openDay) {
        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList = cmis0401Mapper.insertPspTaskList(openDay);
        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList);
    }
}
