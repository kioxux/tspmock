package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-业务处理- </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0411Mapper {

    /**
     * 先处理村镇贷后检查：村镇 公司类贷款，根据客户五级分类为后三类或本金逾期、欠息达60天的，每45天检查一次。进入诉讼的可不产生贷后任务。
     *
     * @param openDay
     * @return
     */
    int insertTownPspTAskList01(@Param("openDay") String openDay);

    /**
     * 村镇：其他情况 公司类贷款（除低风险业务），贷后定期任务的频率为120天
     *
     * @param openDay
     * @return
     */
    int insertTownPspTAskList02(@Param("openDay") String openDay);

    /**
     * 清空监控对公客户信息表
     *
     * @param openDay
     * @return
     */
    int truncateBatBizMonitorCusinfo(@Param("openDay") String openDay);

    /**
     * 插入监控对公客户信息表
     *
     * @param openDay
     * @return
     */
    int insertBatBizMonitorCusinfo(@Param("openDay") String openDay);

    /**
     * 插入监控对公客户台账信息表
     *
     * @param openDay
     * @return
     */
    int insertBatBizMonitorCusaccinfo(@Param("openDay") String openDay);

    /**
     * 清空 临时表-贷后检查监控客户与对公客户台账信息关系表
     *
     * @param openDay
     * @return
     */
    int truncateTmpBatBizMonitorCusAcc(@Param("openDay") String openDay);

    /**
     * 插入 临时表-贷后检查监控客户与对公客户台账信息关系表
     *
     * @param openDay
     * @return
     */
    int insertTmpBatBizMonitorCusAcc(@Param("openDay") String openDay);

    /**
     * 更新监控对公客户信息表
     *
     * @param openDay
     * @return
     */
    int updateBatBizMonitorCusinfo(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList01(@Param("openDay") String openDay);

    /**
     * 清空监控对公客户台账信息表
     *
     * @param openDay
     * @return
     */
    int truncateBatBizMonitorCusaccinfo(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表,五级分类 为正常、关注 ， 客户名下仅有 委托贷款
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList02(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表,其他贷款业务 要求 完成时间按照监控频率配置表
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList03(@Param("openDay") String openDay);

    /**
     * 插入贷后检查任务表,瑕疵类贷款和不良贷款 任务生成日期为上次检查任务完成日期+1 日生成，要求完成日期为生成日期 +60T；  五级分类 为次级以下， 完成时间为生成日期+60T
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList04(@Param("openDay") String openDay);


    /**
     * 纯表外业务检查
     *
     * @param openDay
     * @return
     */
    int insertPspTaskList05(@Param("openDay") String openDay);

    /**
     *  删除同一天客户产生多个任务，保留这一天最早的日终产生的任务
     *
     * @param openDay
     * @return
     */
    int deletetmpPspTastListForCusMore(@Param("openDay") String openDay);

}
