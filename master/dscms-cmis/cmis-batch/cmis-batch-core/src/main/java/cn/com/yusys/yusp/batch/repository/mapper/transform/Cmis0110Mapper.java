package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0110</br>
 * 任务名称：加工任务-业务处理-资产池协议处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0110Mapper {
    /**
     * 自动注销-银承借据 和 贷款借据
     *
     * @param openDay
     * @return
     */
    int updateCtrAsplDetails01(@Param("openDay") String openDay);

}
