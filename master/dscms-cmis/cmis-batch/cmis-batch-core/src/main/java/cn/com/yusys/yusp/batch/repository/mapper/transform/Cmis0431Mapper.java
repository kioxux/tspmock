package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0431</br>
 * 任务名称：加工任务-贷后管理-风险分类子表数据生成</br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月16日 上午0:28:14
 */
public interface Cmis0431Mapper {

    /**
     * 回插入 风险分类任务表
     * @param openDay
     * @return
     */
    int insertRiskTaskList(String openDay);

    /**
     * 回插入 风险分类借据信息表
     * @param openDay
     * @return
     */
    int insertRiskDebitInfo(String openDay);
}
