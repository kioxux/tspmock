/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.transform.cfg;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: CfgPspMonFreParam
 * @类描述: cfg_psp_mon_fre_param数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-22 20:25:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_psp_mon_fre_param")
public class CfgPspMonFreParam extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "pk_id")
	private String pkId;
	
	/** 企业规模 **/
	@Column(name = "CORP_SCALE", unique = false, nullable = true, length = 5)
	private String corpScale;
	
	/** 客户总贷款余额最小值 **/
	@Column(name = "total_loan_balance_min", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalLoanBalanceMin;
	
	/** 客户总贷款余额最大值 **/
	@Column(name = "total_loan_balance_max", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalLoanBalanceMax;
	
	/** 加权平均预期损失率最小值 **/
	@Column(name = "weight_avg_loss_rate_min", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal weightAvgLossRateMin;
	
	/** 加权平均预期损失率最大值 **/
	@Column(name = "weight_avg_loss_rate_max", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal weightAvgLossRateMax;
	
	/** 强担保监控频率 **/
	@Column(name = "str_gua_frequency", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal strGuaFrequency;
	
	/** 一般担保监控频率 **/
	@Column(name = "gen_gua_frequency", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal genGuaFrequency;
	
	/** 信息备注 **/
	@Column(name = "cfg_remark", unique = false, nullable = true, length = 500)
	private String cfgRemark;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param corpScale
	 */
	public void setCorpScale(String corpScale) {
		this.corpScale = corpScale;
	}
	
    /**
     * @return corpScale
     */
	public String getCorpScale() {
		return this.corpScale;
	}
	
	/**
	 * @param totalLoanBalanceMin
	 */
	public void setTotalLoanBalanceMin(java.math.BigDecimal totalLoanBalanceMin) {
		this.totalLoanBalanceMin = totalLoanBalanceMin;
	}
	
    /**
     * @return totalLoanBalanceMin
     */
	public java.math.BigDecimal getTotalLoanBalanceMin() {
		return this.totalLoanBalanceMin;
	}
	
	/**
	 * @param totalLoanBalanceMax
	 */
	public void setTotalLoanBalanceMax(java.math.BigDecimal totalLoanBalanceMax) {
		this.totalLoanBalanceMax = totalLoanBalanceMax;
	}
	
    /**
     * @return totalLoanBalanceMax
     */
	public java.math.BigDecimal getTotalLoanBalanceMax() {
		return this.totalLoanBalanceMax;
	}
	
	/**
	 * @param weightAvgLossRateMin
	 */
	public void setWeightAvgLossRateMin(java.math.BigDecimal weightAvgLossRateMin) {
		this.weightAvgLossRateMin = weightAvgLossRateMin;
	}
	
    /**
     * @return weightAvgLossRateMin
     */
	public java.math.BigDecimal getWeightAvgLossRateMin() {
		return this.weightAvgLossRateMin;
	}
	
	/**
	 * @param weightAvgLossRateMax
	 */
	public void setWeightAvgLossRateMax(java.math.BigDecimal weightAvgLossRateMax) {
		this.weightAvgLossRateMax = weightAvgLossRateMax;
	}
	
    /**
     * @return weightAvgLossRateMax
     */
	public java.math.BigDecimal getWeightAvgLossRateMax() {
		return this.weightAvgLossRateMax;
	}
	
	/**
	 * @param strGuaFrequency
	 */
	public void setStrGuaFrequency(java.math.BigDecimal strGuaFrequency) {
		this.strGuaFrequency = strGuaFrequency;
	}
	
    /**
     * @return strGuaFrequency
     */
	public java.math.BigDecimal getStrGuaFrequency() {
		return this.strGuaFrequency;
	}
	
	/**
	 * @param genGuaFrequency
	 */
	public void setGenGuaFrequency(java.math.BigDecimal genGuaFrequency) {
		this.genGuaFrequency = genGuaFrequency;
	}
	
    /**
     * @return genGuaFrequency
     */
	public java.math.BigDecimal getGenGuaFrequency() {
		return this.genGuaFrequency;
	}
	
	/**
	 * @param cfgRemark
	 */
	public void setCfgRemark(String cfgRemark) {
		this.cfgRemark = cfgRemark;
	}
	
    /**
     * @return cfgRemark
     */
	public String getCfgRemark() {
		return this.cfgRemark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}