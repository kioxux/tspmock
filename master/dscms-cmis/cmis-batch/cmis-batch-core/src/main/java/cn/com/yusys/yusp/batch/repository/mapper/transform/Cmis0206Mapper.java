package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0206</br>
 * 任务名称：加工任务-额度处理-占用授信总金额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0206Mapper {


    /**
     * 更新额度占用关系
     *
     * @return
     */
    void truncateLmtContRel21();

    /**
     * 合同申请信息表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel21Htsq(@Param("openDay") String openDay);

    /**
     * 最高额授信协议申请 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel21Zgesxxysq(@Param("openDay") String openDay);


    /**
     * 委托贷款申请信息表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel21Wtdksqxx(@Param("openDay") String openDay);


    /**
     * 合同主表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel21Htzb(@Param("openDay") String openDay);


    /**
     * 最高额授信协议 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel21Zgesxxy(@Param("openDay") String openDay);


    /**
     * 委托合同主表 表内业务BIZ_TOTAL_AMT_CNY 占用总金额（折人民币）处理
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel21Wthtzb(@Param("openDay") String openDay);


    /**
     * 更新额度占用关系 占用总金额（折人民币） 赋值为关系表总占用额
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel21(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系
     *
     * @return
     */
    void truncateLmtContRel22();

    /**
     * 更新额度占用关系
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel22(@Param("openDay") String openDay);

    /**
     * 资产池占用授信总金额与敞口金额
     *
     * @param openDay
     * @return
     */

    int insertTmpLmtAmt(@Param("openDay") String openDay);

    /**
     * 根据授信占用临时表表数据 更新 授信占用关系表
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel22(@Param("openDay") String openDay);

    /**
     * 更新额度占用关系 占用总金额（折人民币） 赋值为关系表总占用额
     *
     * @param openDay
     * @return
     */
    int updateLmtContRel22A(@Param("openDay") String openDay);
}
