package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0418Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0418</br><?xml version="1.0" encoding="UTF-8"?>
 <!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
 <mapper namespace="cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0418Mapper">
 <insert id="insertPspTaskList">
 insert  into cmis_psp.psp_task_list(
 <!--主键-->
 PK_ID,
 <!--任务编号-->
 TASK_NO,
 <!--任务类型-->
 TASK_TYPE,
 <!--检查类型-->
 CHECK_TYPE,
 <!--客户编号-->
 CUS_ID,
 <!--客户名称-->
 CUS_NAME,
 <!--借据编号-->
 BILL_NO,
 <!--合同编号-->
 CONT_NO,
 <!--产品名称-->
 PRD_NAME,
 <!--任务生成日期-->
 TASK_START_DT,
 <!--任务要求完成日期-->
 TASK_END_DT,
 <!--任务执行人-->
 EXEC_ID,
 <!--任务执行机构-->
 EXEC_BR_ID,
 <!--检查状态-->
 CHECK_STATUS,
 <!--审批状态-->
 APPROVE_STATUS,
 <!--报告类型-->
 RPT_TYPE,
 <!--检查日期-->
 CHECK_DATE,
 <!--借据起始日-->
 LOAN_START_DATE,
 <!--借据到期日-->
 LOAN_END_DATE,
 <!--风险等级-->
 RISK_LVL,
 <!--担保方式-->
 GUAR_MODE,
 <!--任务派发人员-->
 ISSUE_ID,
 <!--任务派发人员所属机构-->
 ISSUE_BR_ID,
 <!--任务下发日期-->
 ISSUE_DATE,
 <!--贷款金额（元） -->
 LOAN_AMT,
 <!--申请延期完成日期 -->
 DELAY_DATE,
 <!--任务状态-->
 TASK_STATUS,
 <!--登记人-->
 INPUT_ID,
 <!--登记机构-->
 INPUT_BR_ID,
 <!--登记日期-->
 INPUT_DATE,
 <!--最近修改人-->
 UPD_ID,
 <!--最近修改机构-->
 UPD_BR_ID,
 <!--最近修改日期-->
 UPD_DATE,
 <!--创建时间-->
 CREATE_TIME,
 <!--修改时间-->
 UPDATE_TIME,
 <!-- 任务说明-->
 TASK_REMARKS
 ) SELECT
 <!--主键-->
 PK_ID,
 <!--任务编号-->
 TASK_NO,
 <!--任务类型-->
 TASK_TYPE,
 <!--检查类型-->
 CHECK_TYPE,
 <!--客户编号-->
 CUS_ID,
 <!--客户名称-->
 CUS_NAME,
 <!--借据编号-->
 BILL_NO,
 <!--合同编号-->
 CONT_NO,
 <!--产品名称-->
 PRD_NAME,
 <!--任务生成日期-->
 TASK_START_DT,
 <!--任务要求完成日期-->
 TASK_END_DT,
 <!--任务执行人-->
 EXEC_ID,
 <!--任务执行机构-->
 EXEC_BR_ID,
 <!--检查状态-->
 CHECK_STATUS,
 <!--审批状态-->
 APPROVE_STATUS,
 <!--报告类型-->
 RPT_TYPE,
 <!--检查日期-->
 CHECK_DATE,
 <!--借据起始日-->
 LOAN_START_DATE,
 <!--借据到期日-->
 LOAN_END_DATE,
 <!--风险等级-->
 RISK_LVL,
 <!--担保方式-->
 GUAR_MODE,
 <!--任务派发人员-->
 ISSUE_ID,
 <!--任务派发人员所属机构-->
 ISSUE_BR_ID,
 <!--任务下发日期-->
 ISSUE_DATE,
 <!--贷款金额（元） -->
 LOAN_AMT,
 <!--申请延期完成日期 -->
 DELAY_DATE,
 <!--任务状态-->
 TASK_STATUS,
 <!--登记人-->
 INPUT_ID,
 <!--登记机构-->
 INPUT_BR_ID,
 <!--登记日期-->
 INPUT_DATE,
 <!--最近修改人-->
 UPD_ID,
 <!--最近修改机构-->
 UPD_BR_ID,
 <!--最近修改日期-->
 UPD_DATE,
 <!--创建时间-->
 CREATE_TIME,
 <!--修改时间-->
 UPDATE_TIME,
 <!-- 任务说明-->
 TASK_REMARKS
 from cmis_psp.tmp_psp_task_list
 </insert>

 <insert id="insertTmpPspTask01">
 insert into cmis_psp.psp_debit_info(
 <!--主键-->
 PK_ID,
 <!--任务编号-->
 TASK_NO,
 <!--客户编号-->
 CUS_ID,
 <!--客户名称-->
 CUS_NAME,
 <!--合同编号-->
 CONT_NO,
 <!--借据编号-->
 BILL_NO,
 <!--产品编号-->
 PRD_ID,
 <!--产品名称-->
 PRD_NAME,
 <!--担保方式-->
 GUAR_MODE,
 <!--借据起始日-->
 LOAN_START_DATE,
 <!--借据到期日-->
 LOAN_END_DATE,
 <!--借据金额（元）-->
 LOAN_AMT,
 <!--借据余额（元）-->
 LOAN_BALANCE,
 <!--拖欠利息（元）-->
 DEBIT_INT,
 <!--逾期天数-->
 OVERDUE_DAY,
 <!--执行年利率-->
 EXEC_RATE_YEAR,
 <!--五级分类-->
 FIVE_CLASS,
 <!--创建时间-->
 CREATE_TIME,
 <!--修改时间-->
 UPDATE_TIME,
 <!-- 借据描述-->
 bill_desc
 ) SELECT
 <!--主键-->
 PK_ID,
 <!--任务编号-->
 TASK_NO,
 <!--客户编号-->
 CUS_ID,
 <!--客户名称-->
 CUS_NAME,
 <!--合同编号-->
 CONT_NO,
 <!--借据编号-->
 BILL_NO,
 <!--产品编号-->
 PRD_ID,
 <!--产品名称-->
 PRD_NAME,
 <!--担保方式-->
 GUAR_MODE,
 <!--借据起始日-->
 LOAN_START_DATE,
 <!--借据到期日-->
 LOAN_END_DATE,
 <!--借据金额（元）-->
 LOAN_AMT,
 <!--借据余额（元）-->
 LOAN_BALANCE,
 <!--拖欠利息（元）-->
 DEBIT_INT,
 <!--逾期天数-->
 OVERDUE_DAY,
 <!--执行年利率-->
 EXEC_RATE_YEAR,
 <!--五级分类-->
 FIVE_CLASS,
 <!--创建时间-->
 CREATE_TIME,
 <!--修改时间-->
 UPDATE_TIME,
 <!-- 借据描述-->
 bill_desc
 from cmis_psp.tmp_psp_debit_info
 </insert>

 <!--村镇银行小微经营性贷款产品贷后任务改为个人经营性 TODO 同步代码-->
 <update id="insertTmpPspTask02">
 update  cmis_psp.psp_task_list ptl  set check_type = '22'
 where EXISTS (select 1 from   yusp_admin.admin_sm_org so where so.org_type='A' and  ptl.EXEC_BR_ID = so.org_code)
 and ptl.check_type = '24' and ptl.approve_status = '000'
 </update >

 <!--村镇银行小微消费性贷款产品贷后任务改为个人消费性 TODO 同步代码-->
 <update id="insertTmpPspTask03">
 update  cmis_psp.psp_task_list ptl  set check_type = '23'
 where EXISTS (select 1 from   yusp_admin.admin_sm_org so where so.org_type='A' and  ptl.EXEC_BR_ID = so.org_code)
 and ptl.check_type = '25' and ptl.approve_status = '000'
 </update >

 </mapper>

 * 任务名称：加工任务-贷后管理-定期检查[清理贷后临时表数据] </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月17日 下午9:56:54
 * TODO 同步代码
 */
@Service
public class Cmis0418Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0418Service.class);

    @Resource
    private Cmis0418Mapper cmis0418Mapper;

    @Transactional
    public void cmis0420InsertTmpPspTask01(String openDay) {
        logger.info("首次检查 定期检查 贷后临时表数据插入正式表   开始,请求参数为:[{}]", openDay);
        int insertPspTaskList = cmis0418Mapper.insertPspTaskList(openDay);
        logger.info("首次检查 定期检查 贷后临时表数据插入正式表   结束,返回参数为:[{}]", insertPspTaskList);
        logger.info(" 贷后借据临时表插入正式表  开始,请求参数为:[{}]", openDay);
        int insertTmpPspTask01 = cmis0418Mapper.insertTmpPspTask01(openDay);
        logger.info("贷后借据临时表插入正式表   结束,返回参数为:[{}]", insertTmpPspTask01);
        logger.info("更新村镇银行小微贷后检查任务的检查类型为个人经营性开始,请求参数为:[{}]", openDay);
        int insertTmpPspTask02 = cmis0418Mapper.insertTmpPspTask02(openDay);
        logger.info("更新村镇银行小微贷后检查任务结束,返回参数为:[{}]", insertTmpPspTask01);
        logger.info("更新村镇银行小微贷后检查任务的检查类型为个人消费性开始,请求参数为:[{}]", openDay);
        int insertTmpPspTask03 = cmis0418Mapper.insertTmpPspTask03(openDay);
        logger.info("更新村镇银行小微贷后检查任务结束,返回参数为:[{}]", insertTmpPspTask01);
    }
}
