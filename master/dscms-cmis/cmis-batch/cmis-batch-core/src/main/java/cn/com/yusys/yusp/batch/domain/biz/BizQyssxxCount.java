/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.biz;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BizQyssxxCount
 * @类描述: biz_qyssxx_count数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "biz_qyssxx_count")
public class BizQyssxxCount extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "APP_NO")
	private String appNo;

	/** 贷后流水号 **/
	@Column(name = "PSP_SERNO", unique = false, nullable = true, length = 40)
	private String pspSerno;
	
	/** 查询类型   0：查询自然人， 1：查询组织机构 **/
	@Column(name = "QUERY_TYPE", unique = false, nullable = true, length = 4)
	private String queryType;
	
	/** 身份证号/组织机构代码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 50)
	private String certCode;
	
	/** 姓名/企业名称 **/
	@Column(name = "CUST_NAME", unique = false, nullable = true, length = 1000)
	private String custName;
	
	/** 案件总数 **/
	@Column(name = "COUNT_TOTAL", unique = false, nullable = true, length = 10)
	private String countTotal;
	
	/** 已结案总数 **/
	@Column(name = "COUNT_JIE_TOTAL", unique = false, nullable = true, length = 10)
	private String countJieTotal;
	
	/** 未结案总数 **/
	@Column(name = "COUNT_WEI_TOTAL", unique = false, nullable = true, length = 10)
	private String countWeiTotal;
	
	/** 原告总数 **/
	@Column(name = "COUNT_YUANGAO", unique = false, nullable = true, length = 10)
	private String countYuangao;
	
	/** 原告已结案总数 **/
	@Column(name = "COUNT_JIE_YUANGAO", unique = false, nullable = true, length = 10)
	private String countJieYuangao;
	
	/** 原告未结案总数 **/
	@Column(name = "COUNT_WEI_YUANGAO", unique = false, nullable = true, length = 10)
	private String countWeiYuangao;
	
	/** 被告总数 **/
	@Column(name = "COUNT_BEIGAO", unique = false, nullable = true, length = 10)
	private String countBeigao;
	
	/** 被告已结案总数 **/
	@Column(name = "COUNT_JIE_BEIGAO", unique = false, nullable = true, length = 10)
	private String countJieBeigao;
	
	/** 被告未结案总数 **/
	@Column(name = "COUNT_WEI_BEIGAO", unique = false, nullable = true, length = 10)
	private String countWeiBeigao;
	
	/** 第三人总数 **/
	@Column(name = "COUNT_OTHER", unique = false, nullable = true, length = 10)
	private String countOther;
	
	/** 第三人已结案总数 **/
	@Column(name = "COUNT_JIE_OTHER", unique = false, nullable = true, length = 10)
	private String countJieOther;
	
	/** 第三人未结案总数 **/
	@Column(name = "COUNT_WEI_OTHER", unique = false, nullable = true, length = 10)
	private String countWeiOther;
	
	/** 涉案总金额 **/
	@Column(name = "MONEY_TOTAL", unique = false, nullable = true, length = 30)
	private String moneyTotal;
	
	/** 已结案金额 **/
	@Column(name = "MONEY_JIE_TOTAL", unique = false, nullable = true, length = 30)
	private String moneyJieTotal;
	
	/** 未结案金额 **/
	@Column(name = "MONEY_WEI_TOTAL", unique = false, nullable = true, length = 30)
	private String moneyWeiTotal;
	
	/** 未结案金额百分比 **/
	@Column(name = "MONEY_WEI_PERCENT", unique = false, nullable = true, length = 30)
	private String moneyWeiPercent;
	
	/** 原告金额 **/
	@Column(name = "MONEY_YUANGAO", unique = false, nullable = true, length = 30)
	private String moneyYuangao;
	
	/** 原告已结金额 **/
	@Column(name = "MONEY_JIE_YUANGAO", unique = false, nullable = true, length = 30)
	private String moneyJieYuangao;
	
	/** 原告未结案金额 **/
	@Column(name = "MONEY_WEI_YUANGAO", unique = false, nullable = true, length = 30)
	private String moneyWeiYuangao;
	
	/** 被告金额 **/
	@Column(name = "MONEY_BEIGAO", unique = false, nullable = true, length = 30)
	private String moneyBeigao;
	
	/** 被告已结案金额 **/
	@Column(name = "MONEY_JIE_BEIGAO", unique = false, nullable = true, length = 30)
	private String moneyJieBeigao;
	
	/** 被告未结案金额 **/
	@Column(name = "MONEY_WEI_BEIGAO", unique = false, nullable = true, length = 30)
	private String moneyWeiBeigao;
	
	/** 第三人金额 **/
	@Column(name = "MONEY_OTHER", unique = false, nullable = true, length = 30)
	private String moneyOther;
	
	/** 第三人已结案金额 **/
	@Column(name = "MONEY_JIE_OTHER", unique = false, nullable = true, length = 30)
	private String moneyJieOther;
	
	/** 第三人未结案金额 **/
	@Column(name = "MONEY_WEI_OTHER", unique = false, nullable = true, length = 30)
	private String moneyWeiOther;
	
	/** 涉案案由分布 **/
	@Column(name = "AY_STAT", unique = false, nullable = true, length = 1000)
	private String ayStat;
	
	/** 涉案地点分布 **/
	@Column(name = "AREA_STAT", unique = false, nullable = true, length = 1000)
	private String areaStat;
	
	/** 涉案时间分布 **/
	@Column(name = "LARQ_STAT", unique = false, nullable = true, length = 1000)
	private String larqStat;
	
	/** 结案方式分布 **/
	@Column(name = "JAFS_STAT", unique = false, nullable = true, length = 1000)
	private String jafsStat;
	
	/** 录入日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "LAST_UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	
	/**
	 * @param appNo
	 */
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	
    /**
     * @return appNo
     */
	public String getAppNo() {
		return this.appNo;
	}
	
	/**
	 * @param pspSerno
	 */
	public void setPspSerno(String pspSerno) {
		this.pspSerno = pspSerno;
	}
	
    /**
     * @return pspSerno
     */
	public String getPspSerno() {
		return this.pspSerno;
	}

	/**
	 * @param queryType
	 */
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	
    /**
     * @return queryType
     */
	public String getQueryType() {
		return this.queryType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param countTotal
	 */
	public void setCountTotal(String countTotal) {
		this.countTotal = countTotal;
	}
	
    /**
     * @return countTotal
     */
	public String getCountTotal() {
		return this.countTotal;
	}
	
	/**
	 * @param countJieTotal
	 */
	public void setCountJieTotal(String countJieTotal) {
		this.countJieTotal = countJieTotal;
	}
	
    /**
     * @return countJieTotal
     */
	public String getCountJieTotal() {
		return this.countJieTotal;
	}
	
	/**
	 * @param countWeiTotal
	 */
	public void setCountWeiTotal(String countWeiTotal) {
		this.countWeiTotal = countWeiTotal;
	}
	
    /**
     * @return countWeiTotal
     */
	public String getCountWeiTotal() {
		return this.countWeiTotal;
	}
	
	/**
	 * @param countYuangao
	 */
	public void setCountYuangao(String countYuangao) {
		this.countYuangao = countYuangao;
	}
	
    /**
     * @return countYuangao
     */
	public String getCountYuangao() {
		return this.countYuangao;
	}
	
	/**
	 * @param countJieYuangao
	 */
	public void setCountJieYuangao(String countJieYuangao) {
		this.countJieYuangao = countJieYuangao;
	}
	
    /**
     * @return countJieYuangao
     */
	public String getCountJieYuangao() {
		return this.countJieYuangao;
	}
	
	/**
	 * @param countWeiYuangao
	 */
	public void setCountWeiYuangao(String countWeiYuangao) {
		this.countWeiYuangao = countWeiYuangao;
	}
	
    /**
     * @return countWeiYuangao
     */
	public String getCountWeiYuangao() {
		return this.countWeiYuangao;
	}
	
	/**
	 * @param countBeigao
	 */
	public void setCountBeigao(String countBeigao) {
		this.countBeigao = countBeigao;
	}
	
    /**
     * @return countBeigao
     */
	public String getCountBeigao() {
		return this.countBeigao;
	}
	
	/**
	 * @param countJieBeigao
	 */
	public void setCountJieBeigao(String countJieBeigao) {
		this.countJieBeigao = countJieBeigao;
	}
	
    /**
     * @return countJieBeigao
     */
	public String getCountJieBeigao() {
		return this.countJieBeigao;
	}
	
	/**
	 * @param countWeiBeigao
	 */
	public void setCountWeiBeigao(String countWeiBeigao) {
		this.countWeiBeigao = countWeiBeigao;
	}
	
    /**
     * @return countWeiBeigao
     */
	public String getCountWeiBeigao() {
		return this.countWeiBeigao;
	}
	
	/**
	 * @param countOther
	 */
	public void setCountOther(String countOther) {
		this.countOther = countOther;
	}
	
    /**
     * @return countOther
     */
	public String getCountOther() {
		return this.countOther;
	}
	
	/**
	 * @param countJieOther
	 */
	public void setCountJieOther(String countJieOther) {
		this.countJieOther = countJieOther;
	}
	
    /**
     * @return countJieOther
     */
	public String getCountJieOther() {
		return this.countJieOther;
	}
	
	/**
	 * @param countWeiOther
	 */
	public void setCountWeiOther(String countWeiOther) {
		this.countWeiOther = countWeiOther;
	}
	
    /**
     * @return countWeiOther
     */
	public String getCountWeiOther() {
		return this.countWeiOther;
	}
	
	/**
	 * @param moneyTotal
	 */
	public void setMoneyTotal(String moneyTotal) {
		this.moneyTotal = moneyTotal;
	}
	
    /**
     * @return moneyTotal
     */
	public String getMoneyTotal() {
		return this.moneyTotal;
	}
	
	/**
	 * @param moneyJieTotal
	 */
	public void setMoneyJieTotal(String moneyJieTotal) {
		this.moneyJieTotal = moneyJieTotal;
	}
	
    /**
     * @return moneyJieTotal
     */
	public String getMoneyJieTotal() {
		return this.moneyJieTotal;
	}
	
	/**
	 * @param moneyWeiTotal
	 */
	public void setMoneyWeiTotal(String moneyWeiTotal) {
		this.moneyWeiTotal = moneyWeiTotal;
	}
	
    /**
     * @return moneyWeiTotal
     */
	public String getMoneyWeiTotal() {
		return this.moneyWeiTotal;
	}
	
	/**
	 * @param moneyWeiPercent
	 */
	public void setMoneyWeiPercent(String moneyWeiPercent) {
		this.moneyWeiPercent = moneyWeiPercent;
	}
	
    /**
     * @return moneyWeiPercent
     */
	public String getMoneyWeiPercent() {
		return this.moneyWeiPercent;
	}
	
	/**
	 * @param moneyYuangao
	 */
	public void setMoneyYuangao(String moneyYuangao) {
		this.moneyYuangao = moneyYuangao;
	}
	
    /**
     * @return moneyYuangao
     */
	public String getMoneyYuangao() {
		return this.moneyYuangao;
	}
	
	/**
	 * @param moneyJieYuangao
	 */
	public void setMoneyJieYuangao(String moneyJieYuangao) {
		this.moneyJieYuangao = moneyJieYuangao;
	}
	
    /**
     * @return moneyJieYuangao
     */
	public String getMoneyJieYuangao() {
		return this.moneyJieYuangao;
	}
	
	/**
	 * @param moneyWeiYuangao
	 */
	public void setMoneyWeiYuangao(String moneyWeiYuangao) {
		this.moneyWeiYuangao = moneyWeiYuangao;
	}
	
    /**
     * @return moneyWeiYuangao
     */
	public String getMoneyWeiYuangao() {
		return this.moneyWeiYuangao;
	}
	
	/**
	 * @param moneyBeigao
	 */
	public void setMoneyBeigao(String moneyBeigao) {
		this.moneyBeigao = moneyBeigao;
	}
	
    /**
     * @return moneyBeigao
     */
	public String getMoneyBeigao() {
		return this.moneyBeigao;
	}
	
	/**
	 * @param moneyJieBeigao
	 */
	public void setMoneyJieBeigao(String moneyJieBeigao) {
		this.moneyJieBeigao = moneyJieBeigao;
	}
	
    /**
     * @return moneyJieBeigao
     */
	public String getMoneyJieBeigao() {
		return this.moneyJieBeigao;
	}
	
	/**
	 * @param moneyWeiBeigao
	 */
	public void setMoneyWeiBeigao(String moneyWeiBeigao) {
		this.moneyWeiBeigao = moneyWeiBeigao;
	}
	
    /**
     * @return moneyWeiBeigao
     */
	public String getMoneyWeiBeigao() {
		return this.moneyWeiBeigao;
	}
	
	/**
	 * @param moneyOther
	 */
	public void setMoneyOther(String moneyOther) {
		this.moneyOther = moneyOther;
	}
	
    /**
     * @return moneyOther
     */
	public String getMoneyOther() {
		return this.moneyOther;
	}
	
	/**
	 * @param moneyJieOther
	 */
	public void setMoneyJieOther(String moneyJieOther) {
		this.moneyJieOther = moneyJieOther;
	}
	
    /**
     * @return moneyJieOther
     */
	public String getMoneyJieOther() {
		return this.moneyJieOther;
	}
	
	/**
	 * @param moneyWeiOther
	 */
	public void setMoneyWeiOther(String moneyWeiOther) {
		this.moneyWeiOther = moneyWeiOther;
	}
	
    /**
     * @return moneyWeiOther
     */
	public String getMoneyWeiOther() {
		return this.moneyWeiOther;
	}
	
	/**
	 * @param ayStat
	 */
	public void setAyStat(String ayStat) {
		this.ayStat = ayStat;
	}
	
    /**
     * @return ayStat
     */
	public String getAyStat() {
		return this.ayStat;
	}
	
	/**
	 * @param areaStat
	 */
	public void setAreaStat(String areaStat) {
		this.areaStat = areaStat;
	}
	
    /**
     * @return areaStat
     */
	public String getAreaStat() {
		return this.areaStat;
	}
	
	/**
	 * @param larqStat
	 */
	public void setLarqStat(String larqStat) {
		this.larqStat = larqStat;
	}
	
    /**
     * @return larqStat
     */
	public String getLarqStat() {
		return this.larqStat;
	}
	
	/**
	 * @param jafsStat
	 */
	public void setJafsStat(String jafsStat) {
		this.jafsStat = jafsStat;
	}
	
    /**
     * @return jafsStat
     */
	public String getJafsStat() {
		return this.jafsStat;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}


}