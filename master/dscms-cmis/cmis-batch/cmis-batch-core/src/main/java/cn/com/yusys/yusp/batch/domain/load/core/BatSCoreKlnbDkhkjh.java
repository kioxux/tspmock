/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSCoreKlnbDkhkjh
 * @类描述: bat_s_core_klnb_dkhkjh数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_core_klnb_dkhkjh")
public class BatSCoreKlnbDkhkjh extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 贷款借据号 **/
	@Id
	@Column(name = "dkjiejuh")
	private String dkjiejuh;
	
	/** 本期期数 **/
	@Id
	@Column(name = "benqqish")
	private Long benqqish;
	
	/** 本期子期数 **/
	@Id
	@Column(name = "benqizqs")
	private Long benqizqs;
	
	/** 起始日期 **/
	@Column(name = "qishriqi", unique = false, nullable = true, length = 8)
	private String qishriqi;
	
	/** 终止日期 **/
	@Column(name = "zhzhriqi", unique = false, nullable = true, length = 8)
	private String zhzhriqi;
	
	/** 每期还款总额 **/
	@Column(name = "meiqhkze", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal meiqhkze;
	
	/** 初始本金 **/
	@Column(name = "chushibj", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal chushibj;
	
	/** 初始利息 **/
	@Column(name = "chushilx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal chushilx;
	
	/** 宽限期到期日 **/
	@Column(name = "kxqdqirq", unique = false, nullable = true, length = 8)
	private String kxqdqirq;
	
	/** 本金 **/
	@Column(name = "benjinnn", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal benjinnn;
	
	/** 应收应计利息 **/
	@Column(name = "ysyjlixi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ysyjlixi;
	
	/** 催收应计利息 **/
	@Column(name = "csyjlixi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal csyjlixi;
	
	/** 应收欠息 **/
	@Column(name = "ysqianxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ysqianxi;
	
	/** 催收欠息 **/
	@Column(name = "csqianxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal csqianxi;
	
	/** 应收应计罚息 **/
	@Column(name = "ysyjfaxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal ysyjfaxi;
	
	/** 催收应计罚息 **/
	@Column(name = "csyjfaxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal csyjfaxi;
	
	/** 应收罚息 **/
	@Column(name = "yshofaxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yshofaxi;
	
	/** 催收罚息 **/
	@Column(name = "cshofaxi", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal cshofaxi;
	
	/** 应计复息 **/
	@Column(name = "yingjifx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yingjifx;
	
	/** 复息 **/
	@Column(name = "fuxiiiii", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal fuxiiiii;
	
	/** 上次还款日 **/
	@Column(name = "schkriqi", unique = false, nullable = true, length = 8)
	private String schkriqi;
	
	/** 期供种类(1-标准期供,2-定制期供,3-多频期供) **/
	@Column(name = "qigengzl", unique = false, nullable = true, length = 1)
	private String qigengzl;
	
	/** 本期状态(0-正常,1-逾期,2-呆滞,3-呆账,4-核销,5-结清,6-不还,7-减免,8-宽限期) **/
	@Column(name = "benqizht", unique = false, nullable = true, length = 1)
	private String benqizht;
	
	/** 应计非应计状态(0-应计,1-非应计) **/
	@Column(name = "yjfyjzht", unique = false, nullable = true, length = 1)
	private String yjfyjzht;
	
	/** 本期应还本金 **/
	@Column(name = "benqyhbj", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal benqyhbj;
	
	/** 本期应还利息 **/
	@Column(name = "benqyhlx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal benqyhlx;
	
	/** 调整计划还款类型(1-实际归还,2-指定本金,3-指定利息,4-指定本息) **/
	@Column(name = "tzjhhklx", unique = false, nullable = true, length = 1)
	private String tzjhhklx;
	
	/** 摘要 **/
	@Column(name = "zhaiyoms", unique = false, nullable = true, length = 300)
	private String zhaiyoms;
	
	/** 账户余额 **/
	@Column(name = "zhanghye", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal zhanghye;
	
	/** 应计贴息 **/
	@Column(name = "yingjitx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yingjitx;
	
	/** 应收贴息 **/
	@Column(name = "yingshtx", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yingshtx;
	
	/** 贴息复利 **/
	@Column(name = "tiexfuli", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal tiexfuli;
	
	/** data_date **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param benqqish
	 */
	public void setBenqqish(Long benqqish) {
		this.benqqish = benqqish;
	}
	
    /**
     * @return benqqish
     */
	public Long getBenqqish() {
		return this.benqqish;
	}
	
	/**
	 * @param benqizqs
	 */
	public void setBenqizqs(Long benqizqs) {
		this.benqizqs = benqizqs;
	}
	
    /**
     * @return benqizqs
     */
	public Long getBenqizqs() {
		return this.benqizqs;
	}
	
	/**
	 * @param qishriqi
	 */
	public void setQishriqi(String qishriqi) {
		this.qishriqi = qishriqi;
	}
	
    /**
     * @return qishriqi
     */
	public String getQishriqi() {
		return this.qishriqi;
	}
	
	/**
	 * @param zhzhriqi
	 */
	public void setZhzhriqi(String zhzhriqi) {
		this.zhzhriqi = zhzhriqi;
	}
	
    /**
     * @return zhzhriqi
     */
	public String getZhzhriqi() {
		return this.zhzhriqi;
	}
	
	/**
	 * @param meiqhkze
	 */
	public void setMeiqhkze(java.math.BigDecimal meiqhkze) {
		this.meiqhkze = meiqhkze;
	}
	
    /**
     * @return meiqhkze
     */
	public java.math.BigDecimal getMeiqhkze() {
		return this.meiqhkze;
	}
	
	/**
	 * @param chushibj
	 */
	public void setChushibj(java.math.BigDecimal chushibj) {
		this.chushibj = chushibj;
	}
	
    /**
     * @return chushibj
     */
	public java.math.BigDecimal getChushibj() {
		return this.chushibj;
	}
	
	/**
	 * @param chushilx
	 */
	public void setChushilx(java.math.BigDecimal chushilx) {
		this.chushilx = chushilx;
	}
	
    /**
     * @return chushilx
     */
	public java.math.BigDecimal getChushilx() {
		return this.chushilx;
	}
	
	/**
	 * @param kxqdqirq
	 */
	public void setKxqdqirq(String kxqdqirq) {
		this.kxqdqirq = kxqdqirq;
	}
	
    /**
     * @return kxqdqirq
     */
	public String getKxqdqirq() {
		return this.kxqdqirq;
	}
	
	/**
	 * @param benjinnn
	 */
	public void setBenjinnn(java.math.BigDecimal benjinnn) {
		this.benjinnn = benjinnn;
	}
	
    /**
     * @return benjinnn
     */
	public java.math.BigDecimal getBenjinnn() {
		return this.benjinnn;
	}
	
	/**
	 * @param ysyjlixi
	 */
	public void setYsyjlixi(java.math.BigDecimal ysyjlixi) {
		this.ysyjlixi = ysyjlixi;
	}
	
    /**
     * @return ysyjlixi
     */
	public java.math.BigDecimal getYsyjlixi() {
		return this.ysyjlixi;
	}
	
	/**
	 * @param csyjlixi
	 */
	public void setCsyjlixi(java.math.BigDecimal csyjlixi) {
		this.csyjlixi = csyjlixi;
	}
	
    /**
     * @return csyjlixi
     */
	public java.math.BigDecimal getCsyjlixi() {
		return this.csyjlixi;
	}
	
	/**
	 * @param ysqianxi
	 */
	public void setYsqianxi(java.math.BigDecimal ysqianxi) {
		this.ysqianxi = ysqianxi;
	}
	
    /**
     * @return ysqianxi
     */
	public java.math.BigDecimal getYsqianxi() {
		return this.ysqianxi;
	}
	
	/**
	 * @param csqianxi
	 */
	public void setCsqianxi(java.math.BigDecimal csqianxi) {
		this.csqianxi = csqianxi;
	}
	
    /**
     * @return csqianxi
     */
	public java.math.BigDecimal getCsqianxi() {
		return this.csqianxi;
	}
	
	/**
	 * @param ysyjfaxi
	 */
	public void setYsyjfaxi(java.math.BigDecimal ysyjfaxi) {
		this.ysyjfaxi = ysyjfaxi;
	}
	
    /**
     * @return ysyjfaxi
     */
	public java.math.BigDecimal getYsyjfaxi() {
		return this.ysyjfaxi;
	}
	
	/**
	 * @param csyjfaxi
	 */
	public void setCsyjfaxi(java.math.BigDecimal csyjfaxi) {
		this.csyjfaxi = csyjfaxi;
	}
	
    /**
     * @return csyjfaxi
     */
	public java.math.BigDecimal getCsyjfaxi() {
		return this.csyjfaxi;
	}
	
	/**
	 * @param yshofaxi
	 */
	public void setYshofaxi(java.math.BigDecimal yshofaxi) {
		this.yshofaxi = yshofaxi;
	}
	
    /**
     * @return yshofaxi
     */
	public java.math.BigDecimal getYshofaxi() {
		return this.yshofaxi;
	}
	
	/**
	 * @param cshofaxi
	 */
	public void setCshofaxi(java.math.BigDecimal cshofaxi) {
		this.cshofaxi = cshofaxi;
	}
	
    /**
     * @return cshofaxi
     */
	public java.math.BigDecimal getCshofaxi() {
		return this.cshofaxi;
	}
	
	/**
	 * @param yingjifx
	 */
	public void setYingjifx(java.math.BigDecimal yingjifx) {
		this.yingjifx = yingjifx;
	}
	
    /**
     * @return yingjifx
     */
	public java.math.BigDecimal getYingjifx() {
		return this.yingjifx;
	}
	
	/**
	 * @param fuxiiiii
	 */
	public void setFuxiiiii(java.math.BigDecimal fuxiiiii) {
		this.fuxiiiii = fuxiiiii;
	}
	
    /**
     * @return fuxiiiii
     */
	public java.math.BigDecimal getFuxiiiii() {
		return this.fuxiiiii;
	}
	
	/**
	 * @param schkriqi
	 */
	public void setSchkriqi(String schkriqi) {
		this.schkriqi = schkriqi;
	}
	
    /**
     * @return schkriqi
     */
	public String getSchkriqi() {
		return this.schkriqi;
	}
	
	/**
	 * @param qigengzl
	 */
	public void setQigengzl(String qigengzl) {
		this.qigengzl = qigengzl;
	}
	
    /**
     * @return qigengzl
     */
	public String getQigengzl() {
		return this.qigengzl;
	}
	
	/**
	 * @param benqizht
	 */
	public void setBenqizht(String benqizht) {
		this.benqizht = benqizht;
	}
	
    /**
     * @return benqizht
     */
	public String getBenqizht() {
		return this.benqizht;
	}
	
	/**
	 * @param yjfyjzht
	 */
	public void setYjfyjzht(String yjfyjzht) {
		this.yjfyjzht = yjfyjzht;
	}
	
    /**
     * @return yjfyjzht
     */
	public String getYjfyjzht() {
		return this.yjfyjzht;
	}
	
	/**
	 * @param benqyhbj
	 */
	public void setBenqyhbj(java.math.BigDecimal benqyhbj) {
		this.benqyhbj = benqyhbj;
	}
	
    /**
     * @return benqyhbj
     */
	public java.math.BigDecimal getBenqyhbj() {
		return this.benqyhbj;
	}
	
	/**
	 * @param benqyhlx
	 */
	public void setBenqyhlx(java.math.BigDecimal benqyhlx) {
		this.benqyhlx = benqyhlx;
	}
	
    /**
     * @return benqyhlx
     */
	public java.math.BigDecimal getBenqyhlx() {
		return this.benqyhlx;
	}
	
	/**
	 * @param tzjhhklx
	 */
	public void setTzjhhklx(String tzjhhklx) {
		this.tzjhhklx = tzjhhklx;
	}
	
    /**
     * @return tzjhhklx
     */
	public String getTzjhhklx() {
		return this.tzjhhklx;
	}
	
	/**
	 * @param zhaiyoms
	 */
	public void setZhaiyoms(String zhaiyoms) {
		this.zhaiyoms = zhaiyoms;
	}
	
    /**
     * @return zhaiyoms
     */
	public String getZhaiyoms() {
		return this.zhaiyoms;
	}
	
	/**
	 * @param zhanghye
	 */
	public void setZhanghye(java.math.BigDecimal zhanghye) {
		this.zhanghye = zhanghye;
	}
	
    /**
     * @return zhanghye
     */
	public java.math.BigDecimal getZhanghye() {
		return this.zhanghye;
	}
	
	/**
	 * @param yingjitx
	 */
	public void setYingjitx(java.math.BigDecimal yingjitx) {
		this.yingjitx = yingjitx;
	}
	
    /**
     * @return yingjitx
     */
	public java.math.BigDecimal getYingjitx() {
		return this.yingjitx;
	}
	
	/**
	 * @param yingshtx
	 */
	public void setYingshtx(java.math.BigDecimal yingshtx) {
		this.yingshtx = yingshtx;
	}
	
    /**
     * @return yingshtx
     */
	public java.math.BigDecimal getYingshtx() {
		return this.yingshtx;
	}
	
	/**
	 * @param tiexfuli
	 */
	public void setTiexfuli(java.math.BigDecimal tiexfuli) {
		this.tiexfuli = tiexfuli;
	}
	
    /**
     * @return tiexfuli
     */
	public java.math.BigDecimal getTiexfuli() {
		return this.tiexfuli;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}