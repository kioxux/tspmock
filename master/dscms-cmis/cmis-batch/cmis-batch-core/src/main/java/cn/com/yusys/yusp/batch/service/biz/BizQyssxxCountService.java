/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.biz;

import cn.com.yusys.yusp.batch.domain.biz.BizQyssxxCount;
import cn.com.yusys.yusp.batch.repository.mapper.biz.BizQyssxxCountMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Count;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BizQyssxxCountService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BizQyssxxCountService {
    private static final Logger logger = LoggerFactory.getLogger(BizQyssxxCountService.class);
    @Autowired
    private BizQyssxxCountMapper bizQyssxxCountMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BizQyssxxCount selectByPrimaryKey(String appNo) {
        return bizQyssxxCountMapper.selectByPrimaryKey(appNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BizQyssxxCount> selectAll(QueryModel model) {
        List<BizQyssxxCount> records = (List<BizQyssxxCount>) bizQyssxxCountMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BizQyssxxCount> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BizQyssxxCount> list = bizQyssxxCountMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BizQyssxxCount record) {
        return bizQyssxxCountMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BizQyssxxCount record) {
        record.setInputTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        record.setLastUpdateDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        record.setLastUpdateTime(record.getInputTime());
        return bizQyssxxCountMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BizQyssxxCount record) {
        return bizQyssxxCountMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BizQyssxxCount record) {
        return bizQyssxxCountMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String appNo) {
        return bizQyssxxCountMapper.deleteByPrimaryKey(appNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return bizQyssxxCountMapper.deleteByIds(ids);
    }
    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计
     *
     * @param count
     * @param corpMap
     * @return
     */
    public BizQyssxxCount buildByCount(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Count count, Map<String, String> corpMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装案件总数统计信息-开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        BizQyssxxCount bizQyssxxCount = GenericBuilder.of(BizQyssxxCount::new)
                .with(BizQyssxxCount::setAppNo, corpMap.get("appNo"))
                .with(BizQyssxxCount::setPspSerno, corpMap.get("autoSerno")) // 贷后百名单流水号
                .with(BizQyssxxCount::setQueryType, "1")// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCount::setCertCode, corpMap.get("certCode"))//
                .with(BizQyssxxCount::setCustName, corpMap.get("cusName"))//
                .with(BizQyssxxCount::setCountTotal, String.valueOf(count.getCount_total()))// 案件总数
                .with(BizQyssxxCount::setCountJieTotal, String.valueOf(count.getCount_jie_total()))// 已结案总数
                .with(BizQyssxxCount::setCountWeiTotal, String.valueOf(count.getCount_wei_total()))// 未结案总数
                .with(BizQyssxxCount::setCountYuangao, String.valueOf(count.getCount_yuangao()))// 原告总数
                .with(BizQyssxxCount::setCountJieYuangao, String.valueOf(count.getCount_jie_yuangao()))// 原告已结案总数
                .with(BizQyssxxCount::setCountWeiYuangao, String.valueOf(count.getCount_wei_yuangao()))// 原告未结案总数
                .with(BizQyssxxCount::setCountBeigao, String.valueOf(count.getCount_beigao()))// 被告总数
                .with(BizQyssxxCount::setCountJieBeigao, String.valueOf(count.getCount_jie_beigao()))// 被告已结案总数
                .with(BizQyssxxCount::setCountWeiBeigao, String.valueOf(count.getCount_wei_beigao()))// 被告未结案总数
                .with(BizQyssxxCount::setCountOther, String.valueOf(count.getCount_other()))// 第三人总数
                .with(BizQyssxxCount::setCountJieOther, String.valueOf(count.getCount_jie_other()))// 第三人已结案总数
                .with(BizQyssxxCount::setCountWeiOther, String.valueOf(count.getCount_wei_other()))// 第三人未结案总数
                .with(BizQyssxxCount::setMoneyTotal, String.valueOf(count.getMoney_total()))// 涉案总金额
                .with(BizQyssxxCount::setMoneyJieTotal, String.valueOf(count.getMoney_jie_total()))// 已结案金额
                .with(BizQyssxxCount::setMoneyWeiTotal, String.valueOf(count.getMoney_wei_total()))// 未结案金额
                .with(BizQyssxxCount::setMoneyWeiPercent, String.valueOf(count.getMoney_wei_percent()))// 未结案金额百分比
                .with(BizQyssxxCount::setMoneyYuangao, String.valueOf(count.getMoney_yuangao()))// 原告金额
                .with(BizQyssxxCount::setMoneyJieYuangao, String.valueOf(count.getMoney_jie_yuangao()))// 原告已结金额
                .with(BizQyssxxCount::setMoneyWeiYuangao, String.valueOf(count.getMoney_wei_yuangao()))// 原告未结案金额
                .with(BizQyssxxCount::setMoneyBeigao, String.valueOf(count.getMoney_beigao()))// 被告金额
                .with(BizQyssxxCount::setMoneyJieBeigao, String.valueOf(count.getMoney_jie_beigao()))// 被告已结案金额
                .with(BizQyssxxCount::setMoneyWeiBeigao, String.valueOf(count.getMoney_wei_beigao()))// 被告未结案金额
                .with(BizQyssxxCount::setMoneyOther, String.valueOf(count.getMoney_other()))// 第三人金额
                .with(BizQyssxxCount::setMoneyJieOther, String.valueOf(count.getMoney_jie_other()))// 第三人已结案金额
                .with(BizQyssxxCount::setMoneyWeiOther, String.valueOf(count.getMoney_wei_other()))// 第三人未结案金额
                // .with(bizQyssxxCount::setAyStat, String.valueOf(count.getAy_stat()))// 涉案案由分布
                // .with(bizQyssxxCount::setAreaStat, String.valueOf(count.getArea_stat()))// 涉案地点分布
                // .with(bizQyssxxCount::setLarqStat,String.valueOf( count.getLarq_stat()))// 涉案时间分布
                // .with(bizQyssxxCount::setJafsStat, String.valueOf(count.getJafs_stat()))// 结案方式分布
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装案件总数统计信息-结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
        return bizQyssxxCount;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计
     *
     * @param civilcount
     * @param corpMap
     * @return
     */
    public BizQyssxxCount buildByCivilcount(Count civilcount, Map<String, String> corpMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        BizQyssxxCount bizQyssxxCount = GenericBuilder.of(BizQyssxxCount::new)
                .with(BizQyssxxCount::setAppNo, corpMap.get("appNo"))
                .with(BizQyssxxCount::setPspSerno, corpMap.get("autoSerno")) // 贷后百名单流水号
                .with(BizQyssxxCount::setQueryType, "1")// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCount::setCertCode, corpMap.get("certCode"))//
                .with(BizQyssxxCount::setCustName, corpMap.get("cusName"))//
                .with(BizQyssxxCount::setCountTotal, String.valueOf(civilcount.getCount_total()))// 案件总数
                .with(BizQyssxxCount::setCountJieTotal, String.valueOf(civilcount.getCount_jie_total()))// 已结案总数
                .with(BizQyssxxCount::setCountWeiTotal, String.valueOf(civilcount.getCount_wei_total()))// 未结案总数
                .with(BizQyssxxCount::setCountYuangao, String.valueOf(civilcount.getCount_yuangao()))// 原告总数
                .with(BizQyssxxCount::setCountJieYuangao, String.valueOf(civilcount.getCount_jie_yuangao()))// 原告已结案总数
                .with(BizQyssxxCount::setCountWeiYuangao, String.valueOf(civilcount.getCount_wei_yuangao()))// 原告未结案总数
                .with(BizQyssxxCount::setCountBeigao, String.valueOf(civilcount.getCount_beigao()))// 被告总数
                .with(BizQyssxxCount::setCountJieBeigao, String.valueOf(civilcount.getCount_jie_beigao()))// 被告已结案总数
                .with(BizQyssxxCount::setCountWeiBeigao, String.valueOf(civilcount.getCount_wei_beigao()))// 被告未结案总数
                .with(BizQyssxxCount::setCountOther, String.valueOf(civilcount.getCount_other()))// 第三人总数
                .with(BizQyssxxCount::setCountJieOther, String.valueOf(civilcount.getCount_jie_other()))// 第三人已结案总数
                .with(BizQyssxxCount::setCountWeiOther, String.valueOf(civilcount.getCount_wei_other()))// 第三人未结案总数
                .with(BizQyssxxCount::setMoneyTotal, String.valueOf(civilcount.getMoney_total()))// 涉案总金额
                .with(BizQyssxxCount::setMoneyJieTotal, String.valueOf(civilcount.getMoney_jie_total()))// 已结案金额
                .with(BizQyssxxCount::setMoneyWeiTotal, String.valueOf(civilcount.getMoney_wei_total()))// 未结案金额
                .with(BizQyssxxCount::setMoneyWeiPercent, String.valueOf(civilcount.getMoney_wei_percent()))// 未结案金额百分比
                .with(BizQyssxxCount::setMoneyYuangao, String.valueOf(civilcount.getMoney_yuangao()))// 原告金额
                .with(BizQyssxxCount::setMoneyJieYuangao, String.valueOf(civilcount.getMoney_jie_yuangao()))// 原告已结金额
                .with(BizQyssxxCount::setMoneyWeiYuangao, String.valueOf(civilcount.getMoney_wei_yuangao()))// 原告未结案金额
                .with(BizQyssxxCount::setMoneyBeigao, String.valueOf(civilcount.getMoney_beigao()))// 被告金额
                .with(BizQyssxxCount::setMoneyJieBeigao, String.valueOf(civilcount.getMoney_jie_beigao()))// 被告已结案金额
                .with(BizQyssxxCount::setMoneyWeiBeigao, String.valueOf(civilcount.getMoney_wei_beigao()))// 被告未结案金额
                .with(BizQyssxxCount::setMoneyOther, String.valueOf(civilcount.getMoney_other()))// 第三人金额
                .with(BizQyssxxCount::setMoneyJieOther, String.valueOf(civilcount.getMoney_jie_other()))// 第三人已结案金额
                .with(BizQyssxxCount::setMoneyWeiOther, String.valueOf(civilcount.getMoney_wei_other()))// 第三人未结案金额
                // .with(bizQyssxxCount::setAyStat, String.valueOf(civilcount.getAy_stat()))// 涉案案由分布
                // .with(bizQyssxxCount::setAreaStat, String.valueOf(civilcount.getArea_stat()))// 涉案地点分布
                // .with(bizQyssxxCount::setLarqStat,String.valueOf( civilcount.getLarq_stat()))// 涉案时间分布
                // .with(bizQyssxxCount::setJafsStat, String.valueOf(civilcount.getJafs_stat()))// 结案方式分布
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
        return bizQyssxxCount;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计
     *
     * @param criminalcount
     * @param corpMap
     * @return
     */
    public BizQyssxxCount buildByCriminalcount(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.criminal.Count criminalcount, Map<String, String> corpMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        BizQyssxxCount bizQyssxxCount = GenericBuilder.of(BizQyssxxCount::new)
                .with(BizQyssxxCount::setAppNo, corpMap.get("appNo"))
                .with(BizQyssxxCount::setPspSerno, corpMap.get("autoSerno")) // 贷后百名单流水号
                .with(BizQyssxxCount::setQueryType, "1")// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCount::setCertCode, corpMap.get("certCode"))//
                .with(BizQyssxxCount::setCustName, corpMap.get("cusName"))//
                .with(BizQyssxxCount::setCountTotal, String.valueOf(criminalcount.getCount_total()))// 案件总数
                .with(BizQyssxxCount::setCountJieTotal, String.valueOf(criminalcount.getCount_jie_total()))// 已结案总数
                .with(BizQyssxxCount::setCountWeiTotal, String.valueOf(criminalcount.getCount_wei_total()))// 未结案总数
                .with(BizQyssxxCount::setCountYuangao, String.valueOf(criminalcount.getCount_yuangao()))// 原告总数
                .with(BizQyssxxCount::setCountJieYuangao, String.valueOf(criminalcount.getCount_jie_yuangao()))// 原告已结案总数
                .with(BizQyssxxCount::setCountWeiYuangao, String.valueOf(criminalcount.getCount_wei_yuangao()))// 原告未结案总数
                .with(BizQyssxxCount::setCountBeigao, String.valueOf(criminalcount.getCount_beigao()))// 被告总数
                .with(BizQyssxxCount::setCountJieBeigao, String.valueOf(criminalcount.getCount_jie_beigao()))// 被告已结案总数
                .with(BizQyssxxCount::setCountWeiBeigao, String.valueOf(criminalcount.getCount_wei_beigao()))// 被告未结案总数
                .with(BizQyssxxCount::setCountOther, String.valueOf(criminalcount.getCount_other()))// 第三人总数
                .with(BizQyssxxCount::setCountJieOther, String.valueOf(criminalcount.getCount_jie_other()))// 第三人已结案总数
                .with(BizQyssxxCount::setCountWeiOther, String.valueOf(criminalcount.getCount_wei_other()))// 第三人未结案总数
                .with(BizQyssxxCount::setMoneyTotal, String.valueOf(criminalcount.getMoney_total()))// 涉案总金额
                .with(BizQyssxxCount::setMoneyJieTotal, String.valueOf(criminalcount.getMoney_jie_total()))// 已结案金额
                .with(BizQyssxxCount::setMoneyWeiTotal, String.valueOf(criminalcount.getMoney_wei_total()))// 未结案金额
                .with(BizQyssxxCount::setMoneyWeiPercent, String.valueOf(criminalcount.getMoney_wei_percent()))// 未结案金额百分比
                .with(BizQyssxxCount::setMoneyYuangao, String.valueOf(criminalcount.getMoney_yuangao()))// 原告金额
                .with(BizQyssxxCount::setMoneyJieYuangao, String.valueOf(criminalcount.getMoney_jie_yuangao()))// 原告已结金额
                .with(BizQyssxxCount::setMoneyWeiYuangao, String.valueOf(criminalcount.getMoney_wei_yuangao()))// 原告未结案金额
                .with(BizQyssxxCount::setMoneyBeigao, String.valueOf(criminalcount.getMoney_beigao()))// 被告金额
                .with(BizQyssxxCount::setMoneyJieBeigao, String.valueOf(criminalcount.getMoney_jie_beigao()))// 被告已结案金额
                .with(BizQyssxxCount::setMoneyWeiBeigao, String.valueOf(criminalcount.getMoney_wei_beigao()))// 被告未结案金额
                .with(BizQyssxxCount::setMoneyOther, String.valueOf(criminalcount.getMoney_other()))// 第三人金额
                .with(BizQyssxxCount::setMoneyJieOther, String.valueOf(criminalcount.getMoney_jie_other()))// 第三人已结案金额
                .with(BizQyssxxCount::setMoneyWeiOther, String.valueOf(criminalcount.getMoney_wei_other()))// 第三人未结案金额
                // .with(bizQyssxxCount::setAyStat, String.valueOf(criminalcount.getAy_stat()))// 涉案案由分布
                // .with(bizQyssxxCount::setAreaStat, String.valueOf(criminalcount.getArea_stat()))// 涉案地点分布
                // .with(bizQyssxxCount::setLarqStat,String.valueOf( criminalcount.getLarq_stat()))// 涉案时间分布
                // .with(bizQyssxxCount::setJafsStat, String.valueOf(criminalcount.getJafs_stat()))// 结案方式分布
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
        return bizQyssxxCount;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计
     *
     * @param administrativecount
     * @param corpMap
     * @return
     */
    public BizQyssxxCount buildByAdministrativecount(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.administrative.Count administrativecount, Map<String, String> corpMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        BizQyssxxCount bizQyssxxCount = GenericBuilder.of(BizQyssxxCount::new)
                .with(BizQyssxxCount::setAppNo, corpMap.get("appNo"))
                .with(BizQyssxxCount::setPspSerno, corpMap.get("autoSerno")) // 贷后百名单流水号
                .with(BizQyssxxCount::setQueryType, "1")// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCount::setCertCode, corpMap.get("certCode"))//
                .with(BizQyssxxCount::setCustName, corpMap.get("cusName"))//
                .with(BizQyssxxCount::setCountTotal, String.valueOf(administrativecount.getCount_total()))// 案件总数
                .with(BizQyssxxCount::setCountJieTotal, String.valueOf(administrativecount.getCount_jie_total()))// 已结案总数
                .with(BizQyssxxCount::setCountWeiTotal, String.valueOf(administrativecount.getCount_wei_total()))// 未结案总数
                .with(BizQyssxxCount::setCountYuangao, String.valueOf(administrativecount.getCount_yuangao()))// 原告总数
                .with(BizQyssxxCount::setCountJieYuangao, String.valueOf(administrativecount.getCount_jie_yuangao()))// 原告已结案总数
                .with(BizQyssxxCount::setCountWeiYuangao, String.valueOf(administrativecount.getCount_wei_yuangao()))// 原告未结案总数
                .with(BizQyssxxCount::setCountBeigao, String.valueOf(administrativecount.getCount_beigao()))// 被告总数
                .with(BizQyssxxCount::setCountJieBeigao, String.valueOf(administrativecount.getCount_jie_beigao()))// 被告已结案总数
                .with(BizQyssxxCount::setCountWeiBeigao, String.valueOf(administrativecount.getCount_wei_beigao()))// 被告未结案总数
                .with(BizQyssxxCount::setCountOther, String.valueOf(administrativecount.getCount_other()))// 第三人总数
                .with(BizQyssxxCount::setCountJieOther, String.valueOf(administrativecount.getCount_jie_other()))// 第三人已结案总数
                .with(BizQyssxxCount::setCountWeiOther, String.valueOf(administrativecount.getCount_wei_other()))// 第三人未结案总数
                .with(BizQyssxxCount::setMoneyTotal, String.valueOf(administrativecount.getMoney_total()))// 涉案总金额
                .with(BizQyssxxCount::setMoneyJieTotal, String.valueOf(administrativecount.getMoney_jie_total()))// 已结案金额
                .with(BizQyssxxCount::setMoneyWeiTotal, String.valueOf(administrativecount.getMoney_wei_total()))// 未结案金额
                .with(BizQyssxxCount::setMoneyWeiPercent, String.valueOf(administrativecount.getMoney_wei_percent()))// 未结案金额百分比
                .with(BizQyssxxCount::setMoneyYuangao, String.valueOf(administrativecount.getMoney_yuangao()))// 原告金额
                .with(BizQyssxxCount::setMoneyJieYuangao, String.valueOf(administrativecount.getMoney_jie_yuangao()))// 原告已结金额
                .with(BizQyssxxCount::setMoneyWeiYuangao, String.valueOf(administrativecount.getMoney_wei_yuangao()))// 原告未结案金额
                .with(BizQyssxxCount::setMoneyBeigao, String.valueOf(administrativecount.getMoney_beigao()))// 被告金额
                .with(BizQyssxxCount::setMoneyJieBeigao, String.valueOf(administrativecount.getMoney_jie_beigao()))// 被告已结案金额
                .with(BizQyssxxCount::setMoneyWeiBeigao, String.valueOf(administrativecount.getMoney_wei_beigao()))// 被告未结案金额
                .with(BizQyssxxCount::setMoneyOther, String.valueOf(administrativecount.getMoney_other()))// 第三人金额
                .with(BizQyssxxCount::setMoneyJieOther, String.valueOf(administrativecount.getMoney_jie_other()))// 第三人已结案金额
                .with(BizQyssxxCount::setMoneyWeiOther, String.valueOf(administrativecount.getMoney_wei_other()))// 第三人未结案金额
                // .with(bizQyssxxCount::setAyStat, String.valueOf(administrativecount.getAy_stat()))// 涉案案由分布
                // .with(bizQyssxxCount::setAreaStat, String.valueOf(administrativecount.getArea_stat()))// 涉案地点分布
                // .with(bizQyssxxCount::setLarqStat,String.valueOf( administrativecount.getLarq_stat()))// 涉案时间分布
                // .with(bizQyssxxCount::setJafsStat, String.valueOf(administrativecount.getJafs_stat()))// 结案方式分布
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
        return bizQyssxxCount;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计
     *
     * @param implementcount
     * @param corpMap
     * @return
     */
    public BizQyssxxCount buildByImplementcount(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.implement.Count implementcount, Map<String, String> corpMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        BizQyssxxCount bizQyssxxCount = GenericBuilder.of(BizQyssxxCount::new)
                .with(BizQyssxxCount::setAppNo, corpMap.get("appNo"))
                .with(BizQyssxxCount::setPspSerno, corpMap.get("autoSerno")) // 贷后百名单流水号
                .with(BizQyssxxCount::setQueryType, "1")// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCount::setCertCode, corpMap.get("certCode"))//
                .with(BizQyssxxCount::setCustName, corpMap.get("cusName"))//
                .with(BizQyssxxCount::setCountTotal, String.valueOf(implementcount.getCount_total()))// 案件总数
                .with(BizQyssxxCount::setCountJieTotal, String.valueOf(implementcount.getCount_jie_total()))// 已结案总数
                .with(BizQyssxxCount::setCountWeiTotal, String.valueOf(implementcount.getCount_wei_total()))// 未结案总数
                .with(BizQyssxxCount::setCountYuangao, String.valueOf(implementcount.getCount_yuangao()))// 原告总数
                .with(BizQyssxxCount::setCountJieYuangao, String.valueOf(implementcount.getCount_jie_yuangao()))// 原告已结案总数
                .with(BizQyssxxCount::setCountWeiYuangao, String.valueOf(implementcount.getCount_wei_yuangao()))// 原告未结案总数
                .with(BizQyssxxCount::setCountBeigao, String.valueOf(implementcount.getCount_beigao()))// 被告总数
                .with(BizQyssxxCount::setCountJieBeigao, String.valueOf(implementcount.getCount_jie_beigao()))// 被告已结案总数
                .with(BizQyssxxCount::setCountWeiBeigao, String.valueOf(implementcount.getCount_wei_beigao()))// 被告未结案总数
                .with(BizQyssxxCount::setCountOther, String.valueOf(implementcount.getCount_other()))// 第三人总数
                .with(BizQyssxxCount::setCountJieOther, String.valueOf(implementcount.getCount_jie_other()))// 第三人已结案总数
                .with(BizQyssxxCount::setCountWeiOther, String.valueOf(implementcount.getCount_wei_other()))// 第三人未结案总数
                .with(BizQyssxxCount::setMoneyTotal, String.valueOf(implementcount.getMoney_total()))// 涉案总金额
                .with(BizQyssxxCount::setMoneyJieTotal, String.valueOf(implementcount.getMoney_jie_total()))// 已结案金额
                .with(BizQyssxxCount::setMoneyWeiTotal, String.valueOf(implementcount.getMoney_wei_total()))// 未结案金额
                .with(BizQyssxxCount::setMoneyWeiPercent, String.valueOf(implementcount.getMoney_wei_percent()))// 未结案金额百分比
                .with(BizQyssxxCount::setMoneyYuangao, String.valueOf(implementcount.getMoney_yuangao()))// 原告金额
                .with(BizQyssxxCount::setMoneyJieYuangao, String.valueOf(implementcount.getMoney_jie_yuangao()))// 原告已结金额
                .with(BizQyssxxCount::setMoneyWeiYuangao, String.valueOf(implementcount.getMoney_wei_yuangao()))// 原告未结案金额
                .with(BizQyssxxCount::setMoneyBeigao, String.valueOf(implementcount.getMoney_beigao()))// 被告金额
                .with(BizQyssxxCount::setMoneyJieBeigao, String.valueOf(implementcount.getMoney_jie_beigao()))// 被告已结案金额
                .with(BizQyssxxCount::setMoneyWeiBeigao, String.valueOf(implementcount.getMoney_wei_beigao()))// 被告未结案金额
                .with(BizQyssxxCount::setMoneyOther, String.valueOf(implementcount.getMoney_other()))// 第三人金额
                .with(BizQyssxxCount::setMoneyJieOther, String.valueOf(implementcount.getMoney_jie_other()))// 第三人已结案金额
                .with(BizQyssxxCount::setMoneyWeiOther, String.valueOf(implementcount.getMoney_wei_other()))// 第三人未结案金额
                // .with(bizQyssxxCount::setAyStat, String.valueOf(implementcount.getAy_stat()))// 涉案案由分布
                // .with(bizQyssxxCount::setAreaStat, String.valueOf(implementcount.getArea_stat()))// 涉案地点分布
                // .with(bizQyssxxCount::setLarqStat,String.valueOf( implementcount.getLarq_stat()))// 涉案时间分布
                // .with(bizQyssxxCount::setJafsStat, String.valueOf(implementcount.getJafs_stat()))// 结案方式分布
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
        return bizQyssxxCount;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计
     *
     * @param bankruptcount
     * @param corpMap
     * @return
     */
    public BizQyssxxCount buildByBankruptcount(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.bankrupt.Count bankruptcount, Map<String, String> corpMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        BizQyssxxCount bizQyssxxCount = GenericBuilder.of(BizQyssxxCount::new)
                .with(BizQyssxxCount::setAppNo, corpMap.get("appNo"))
                .with(BizQyssxxCount::setPspSerno, corpMap.get("autoSerno")) // 贷后百名单流水号
                .with(BizQyssxxCount::setQueryType, "1")// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCount::setCertCode, corpMap.get("certCode"))//
                .with(BizQyssxxCount::setCustName, corpMap.get("cusName"))//
                .with(BizQyssxxCount::setCountTotal, String.valueOf(bankruptcount.getCount_total()))// 案件总数
                .with(BizQyssxxCount::setCountJieTotal, String.valueOf(bankruptcount.getCount_jie_total()))// 已结案总数
                .with(BizQyssxxCount::setCountWeiTotal, String.valueOf(bankruptcount.getCount_wei_total()))// 未结案总数
                .with(BizQyssxxCount::setCountYuangao, String.valueOf(bankruptcount.getCount_yuangao()))// 原告总数
                .with(BizQyssxxCount::setCountJieYuangao, String.valueOf(bankruptcount.getCount_jie_yuangao()))// 原告已结案总数
                .with(BizQyssxxCount::setCountWeiYuangao, String.valueOf(bankruptcount.getCount_wei_yuangao()))// 原告未结案总数
                .with(BizQyssxxCount::setCountBeigao, String.valueOf(bankruptcount.getCount_beigao()))// 被告总数
                .with(BizQyssxxCount::setCountJieBeigao, String.valueOf(bankruptcount.getCount_jie_beigao()))// 被告已结案总数
                .with(BizQyssxxCount::setCountWeiBeigao, String.valueOf(bankruptcount.getCount_wei_beigao()))// 被告未结案总数
                .with(BizQyssxxCount::setCountOther, String.valueOf(bankruptcount.getCount_other()))// 第三人总数
                .with(BizQyssxxCount::setCountJieOther, String.valueOf(bankruptcount.getCount_jie_other()))// 第三人已结案总数
                .with(BizQyssxxCount::setCountWeiOther, String.valueOf(bankruptcount.getCount_wei_other()))// 第三人未结案总数
                .with(BizQyssxxCount::setMoneyTotal, String.valueOf(bankruptcount.getMoney_total()))// 涉案总金额
                .with(BizQyssxxCount::setMoneyJieTotal, String.valueOf(bankruptcount.getMoney_jie_total()))// 已结案金额
                .with(BizQyssxxCount::setMoneyWeiTotal, String.valueOf(bankruptcount.getMoney_wei_total()))// 未结案金额
                .with(BizQyssxxCount::setMoneyWeiPercent, String.valueOf(bankruptcount.getMoney_wei_percent()))// 未结案金额百分比
                .with(BizQyssxxCount::setMoneyYuangao, String.valueOf(bankruptcount.getMoney_yuangao()))// 原告金额
                .with(BizQyssxxCount::setMoneyJieYuangao, String.valueOf(bankruptcount.getMoney_jie_yuangao()))// 原告已结金额
                .with(BizQyssxxCount::setMoneyWeiYuangao, String.valueOf(bankruptcount.getMoney_wei_yuangao()))// 原告未结案金额
                .with(BizQyssxxCount::setMoneyBeigao, String.valueOf(bankruptcount.getMoney_beigao()))// 被告金额
                .with(BizQyssxxCount::setMoneyJieBeigao, String.valueOf(bankruptcount.getMoney_jie_beigao()))// 被告已结案金额
                .with(BizQyssxxCount::setMoneyWeiBeigao, String.valueOf(bankruptcount.getMoney_wei_beigao()))// 被告未结案金额
                .with(BizQyssxxCount::setMoneyOther, String.valueOf(bankruptcount.getMoney_other()))// 第三人金额
                .with(BizQyssxxCount::setMoneyJieOther, String.valueOf(bankruptcount.getMoney_jie_other()))// 第三人已结案金额
                .with(BizQyssxxCount::setMoneyWeiOther, String.valueOf(bankruptcount.getMoney_wei_other()))// 第三人未结案金额
                // .with(bizQyssxxCount::setAyStat, String.valueOf(bankruptcount.getAy_stat()))// 涉案案由分布
                // .with(bizQyssxxCount::setAreaStat, String.valueOf(bankruptcount.getArea_stat()))// 涉案地点分布
                // .with(bizQyssxxCount::setLarqStat,String.valueOf( bankruptcount.getLarq_stat()))// 涉案时间分布
                // .with(bizQyssxxCount::setJafsStat, String.valueOf(bankruptcount.getJafs_stat()))// 结案方式分布
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
        return bizQyssxxCount;
    }

    /**
     * 根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计
     *
     * @param preservationcount
     * @param corpMap
     * @return
     */
    public BizQyssxxCount buildByPreservationcount(cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.preservation.Count preservationcount, Map<String, String> corpMap) {
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        BizQyssxxCount bizQyssxxCount = GenericBuilder.of(BizQyssxxCount::new)
                .with(BizQyssxxCount::setAppNo, corpMap.get("appNo"))
                .with(BizQyssxxCount::setPspSerno, corpMap.get("autoSerno")) // 贷后百名单流水号
                .with(BizQyssxxCount::setQueryType, "1")// 查询类型   0：查询自然人， 1：查询组织机构
                .with(BizQyssxxCount::setCertCode, corpMap.get("certCode"))//
                .with(BizQyssxxCount::setCustName, corpMap.get("cusName"))//
                .with(BizQyssxxCount::setCountTotal, String.valueOf(preservationcount.getCount_total()))// 案件总数
                .with(BizQyssxxCount::setCountJieTotal, String.valueOf(preservationcount.getCount_jie_total()))// 已结案总数
                .with(BizQyssxxCount::setCountWeiTotal, String.valueOf(preservationcount.getCount_wei_total()))// 未结案总数
                .with(BizQyssxxCount::setCountYuangao, String.valueOf(preservationcount.getCount_yuangao()))// 原告总数
                .with(BizQyssxxCount::setCountJieYuangao, String.valueOf(preservationcount.getCount_jie_yuangao()))// 原告已结案总数
                .with(BizQyssxxCount::setCountWeiYuangao, String.valueOf(preservationcount.getCount_wei_yuangao()))// 原告未结案总数
                .with(BizQyssxxCount::setCountBeigao, String.valueOf(preservationcount.getCount_beigao()))// 被告总数
                .with(BizQyssxxCount::setCountJieBeigao, String.valueOf(preservationcount.getCount_jie_beigao()))// 被告已结案总数
                .with(BizQyssxxCount::setCountWeiBeigao, String.valueOf(preservationcount.getCount_wei_beigao()))// 被告未结案总数
                .with(BizQyssxxCount::setCountOther, String.valueOf(preservationcount.getCount_other()))// 第三人总数
                .with(BizQyssxxCount::setCountJieOther, String.valueOf(preservationcount.getCount_jie_other()))// 第三人已结案总数
                .with(BizQyssxxCount::setCountWeiOther, String.valueOf(preservationcount.getCount_wei_other()))// 第三人未结案总数
                .with(BizQyssxxCount::setMoneyTotal, String.valueOf(preservationcount.getMoney_total()))// 涉案总金额
                .with(BizQyssxxCount::setMoneyJieTotal, String.valueOf(preservationcount.getMoney_jie_total()))// 已结案金额
                .with(BizQyssxxCount::setMoneyWeiTotal, String.valueOf(preservationcount.getMoney_wei_total()))// 未结案金额
                .with(BizQyssxxCount::setMoneyWeiPercent, String.valueOf(preservationcount.getMoney_wei_percent()))// 未结案金额百分比
                .with(BizQyssxxCount::setMoneyYuangao, String.valueOf(preservationcount.getMoney_yuangao()))// 原告金额
                .with(BizQyssxxCount::setMoneyJieYuangao, String.valueOf(preservationcount.getMoney_jie_yuangao()))// 原告已结金额
                .with(BizQyssxxCount::setMoneyWeiYuangao, String.valueOf(preservationcount.getMoney_wei_yuangao()))// 原告未结案金额
                .with(BizQyssxxCount::setMoneyBeigao, String.valueOf(preservationcount.getMoney_beigao()))// 被告金额
                .with(BizQyssxxCount::setMoneyJieBeigao, String.valueOf(preservationcount.getMoney_jie_beigao()))// 被告已结案金额
                .with(BizQyssxxCount::setMoneyWeiBeigao, String.valueOf(preservationcount.getMoney_wei_beigao()))// 被告未结案金额
                .with(BizQyssxxCount::setMoneyOther, String.valueOf(preservationcount.getMoney_other()))// 第三人金额
                .with(BizQyssxxCount::setMoneyJieOther, String.valueOf(preservationcount.getMoney_jie_other()))// 第三人已结案金额
                .with(BizQyssxxCount::setMoneyWeiOther, String.valueOf(preservationcount.getMoney_wei_other()))// 第三人未结案金额
                // .with(bizQyssxxCount::setAyStat, String.valueOf(preservationcount.getAy_stat()))// 涉案案由分布
                // .with(bizQyssxxCount::setAreaStat, String.valueOf(preservationcount.getArea_stat()))// 涉案地点分布
                // .with(bizQyssxxCount::setLarqStat,String.valueOf( preservationcount.getLarq_stat()))// 涉案时间分布
                // .with(bizQyssxxCount::setJafsStat, String.valueOf(preservationcount.getJafs_stat()))// 结案方式分布
                .build();
        logger.info("根据[响应DTO：涉诉信息查询接口]封装涉诉企业涉诉信息(公开模型)统计结束,响应参数为:[{}]", JSON.toJSONString(bizQyssxxCount));
        return bizQyssxxCount;
    }
}
