package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0121</br>
 * 任务名称：加工任务-业务处理-零售智能风控马上金融 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0121Mapper {

    /**
     * 插入贷款合同表
     *
     * @param openDay
     * @return
     */
    int insertCtrLoanCont(@Param("openDay") String openDay);

    /**
     * 清空 临时表-马上金融贷款放款明细和客户信息关系表
     *
     * @param openDay
     * @return
     */
    int truncateTmpRcpMsLoanCus(@Param("openDay") String openDay);

    /**
     * 插入 临时表-马上金融贷款放款明细和客户信息关系表
     *
     * @param openDay
     * @return
     */
    int insertTmpRcpMsLoanCus(@Param("openDay") String openDay);

    /**
     * 更新贷款合同表
     *
     * @param openDay
     * @return
     */
    int updateCtrLoanCont(@Param("openDay") String openDay);

    /**
     * 插入贷款台账表
     *
     * @param openDay
     * @return
     */
    int insertAccLoan(@Param("openDay") String openDay);

    /**
     * 更新贷款台账表
     *
     * @param openDay
     * @return
     */
    int updateAccLoan(@Param("openDay") String openDay);


}
