package cn.com.yusys.yusp.batch.service.bak;

import cn.com.yusys.yusp.batch.repository.mapper.bak.BakMD022Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKMD022</br>
 * 任务名称：批后备份月表日表任务-备份风险预警业务表[BAT_BIZ_RISK_SIGN]和行名行号对照表[CFG_BANK_INFO] </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
@Service
@Transactional
public class BakMD022Service {
    private static final Logger logger = LoggerFactory.getLogger(BakMD022Service.class);

    @Autowired
    private BakMD022Mapper bakMD022Mapper;

    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 删除当月的[风险预警业务表[BAT_BIZ_RISK_SIGN]]数据
     *
     * @param openDay
     */
    public void bakMD022DeleteMBatBizRiskSign(String openDay) {
        logger.info("查询当月的[风险预警业务表[tmp_m_bat_biz_risk_sign]]数据开始,请求参数为:[{}]", openDay);
        int queryMD0022DeleteBatBizRiskSignCounts = bakMD022Mapper.queryMD0022DeleteBatBizRiskSignCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(queryMD0022DeleteBatBizRiskSignCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        queryMD0022DeleteBatBizRiskSignCounts = times.intValue();
        logger.info("查询当月的[风险预警业务表[tmp_m_bat_biz_risk_sign]]数据结束,返回参数为:[{}]", queryMD0022DeleteBatBizRiskSignCounts);

        // 删除当月的数据
        for (int i = 0; i < queryMD0022DeleteBatBizRiskSignCounts; i++) {
            logger.info("第[" + i + "]次删除当月的[风险预警业务表[tmp_m_bat_biz_risk_sign]]数据开始,请求参数为:[{}]", openDay);
            int bakMD022DeleteMBatBizRiskSign = bakMD022Mapper.bakMD022DeleteMBatBizRiskSign(openDay);
            logger.info("第[" + i + "]次删除当月的[风险预警业务表[tmp_m_bat_biz_risk_sign]]数据结束,返回参数为:[{}]", bakMD022DeleteMBatBizRiskSign);
        }

        logger.info("查询当月的[有效客户信息月表[tmp_m_bat_t_fpt_r_cust_busi_sum ]]数据开始,请求参数为:[{}]", openDay);
        int bakMD022DeleteMFptRCustBusiSumCounts = bakMD022Mapper.bakMD022DeleteMFptRCustBusiSumCounts(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times2 = new BigDecimal(bakMD022DeleteMFptRCustBusiSumCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        bakMD022DeleteMFptRCustBusiSumCounts = times2.intValue();
        logger.info("查询当月的[有效客户信息月表[tmp_m_bat_t_fpt_r_cust_busi_sum ]]数据结束,返回参数为:[{}]", bakMD022DeleteMFptRCustBusiSumCounts);

        // 删除当月的数据
        for (int i = 0; i < bakMD022DeleteMFptRCustBusiSumCounts; i++) {
            logger.info("第[" + i + "]次删除当月的[有效客户信息月表[tmp_m_bat_t_fpt_r_cust_busi_sum ]]数据开始,请求参数为:[{}]", openDay);
            int bakMD022DeleteMFptRCustBusiSum = bakMD022Mapper.bakMD022DeleteMFptRCustBusiSum(openDay);
            logger.info("第[" + i + "]次删除当月的[有效客户信息月表[tmp_m_bat_t_fpt_r_cust_busi_sum ]]数据结束,返回参数为:[{}]", bakMD022DeleteMFptRCustBusiSum);
        }
    }

    /**
     * 备份当月的数据
     *
     * @param openDay
     */
    public void bakMD022InsertM(String openDay) {
        logger.info("备份当月的[风险预警业务表[tmp_m_bat_biz_risk_sign]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakMD022Mapper.insertM(openDay);
        logger.info("备份当月的[风险预警业务表[tmp_m_bat_biz_risk_sign]]数据结束,返回参数为:[{}]", insertCurrent);

    }

    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakMEqualsOriginal(String openDay) {
        String nextOpenDay = DateUtils.addDay(openDay, "yyyy-MM-dd", 1); // 切日后营业日期
        logger.info("切日后营业日期为:[{}]", nextOpenDay);
        Date nextOpenDayDate = DateUtils.parseDateByDef(nextOpenDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
        int day = DateUtils.getMonthDay(nextOpenDayDate);// 获取当月中的天
        logger.info("获取当月中的天为:[{}]", day);

        if (day == 1) {
            logger.info("查询当月备份的[风险预警业务表[tmp_m_bat_biz_risk_sign]]数据总条数开始,请求参数为:[{}]", openDay);
            int bakMCounts = bakMD022Mapper.queryBakMCounts(openDay);
            logger.info("查询当月备份的[风险预警业务表[tmp_m_bat_biz_risk_sign]]数据总条数结束,返回参数为:[{}]", bakMCounts);

            logger.info("查询当月的[风险预警业务表[bat_biz_risk_sign]]原表数据总条数开始,请求参数为:[{}]", openDay);
            int originalCounts = bakMD022Mapper.queryMOriginalCounts(openDay);
            logger.info("查询当月的[风险预警业务表[bat_biz_risk_sign]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

            if (bakMCounts != originalCounts) {
                logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakMCounts, originalCounts);
                throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakMCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
            } else {
                logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakMCounts, originalCounts);
            }
        }
    }

    /**
     * 重命名创建和删除当天的[行名行号对照表[CFG_BANK_INFO]]数据
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void bakMD022DeleteDCfgBankInfo(String openDay) {

        logger.info("重命名创建和删除当天的[行名行号对照表[TMP_DAF_CFG_BANK_INFO]]数据开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_cfg","TMP_DAF_CFG_BANK_INFO");
        logger.info("重命名创建和删除当天的[行名行号对照表[TMP_DAF_CFG_BANK_INFO]]数据结束");

//        logger.info("查询待删除当天的[行名行号对照表[CFG_BANK_INFO]]数据总次数开始,请求参数为:[{}]", openDay);
//        int queryMD022DeleteOpenDayDCfgBankInfoCounts = bakMD022Mapper.queryMD022DeleteOpenDayDCfgBankInfoCounts(openDay);
//        // 由于TDSQL不支持ceiling函数，此处在java中处理
//        BigDecimal times2 = new BigDecimal(queryMD022DeleteOpenDayDCfgBankInfoCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
//        queryMD022DeleteOpenDayDCfgBankInfoCounts = times2.intValue();
//        logger.info("查询待删除当天的[行名行号对照表[CFG_BANK_INFO]]数据总次数结束,返回参数为:[{}]", queryMD022DeleteOpenDayDCfgBankInfoCounts);
//        // 删除当天的数据
//        for (int i = 0; i < queryMD022DeleteOpenDayDCfgBankInfoCounts; i++) {
//            logger.info("第[" + i + "]次删除当天的[行名行号对照表[CFG_BANK_INFO]]数据开始,请求参数为:[{}]", openDay);
//            int bakMD022DeleteDCfgBankInfo = bakMD022Mapper.bakMD022DeleteDCfgBankInfo(openDay);
//            logger.info("第[" + i + "]次删除当天的[行名行号对照表[CFG_BANK_INFO]]数据结束,返回参数为:[{}]", bakMD022DeleteDCfgBankInfo);
//        }

    }

    /**
     * 备份当天的数据
     *
     * @param openDay
     */
    public void bakMD022InsertD(String openDay) {
        // 备份当天的数据
        logger.info("备份当天的[行名行号对照表[CFG_BANK_INFO]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakMD022Mapper.insertD(openDay);
        logger.info("备份当天的[行名行号对照表[CFG_BANK_INFO]]数据结束,返回参数为:[{}]", insertCurrent);
    }


    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakDEqualsOriginal(String openDay) {
        logger.info("查询当天备份的[行名行号对照表[TMP_DAF_CFG_BANK_INFO]]数据总条数开始,请求参数为:[{}]", openDay);
        int bakDCounts = tableUtilsService.coutBakDafTableLines("cmis_cfg", "TMP_DAF_CFG_BANK_INFO");
        logger.info("查询当天备份的[行名行号对照表[TMP_DAF_CFG_BANK_INFO]]数据总条数结束,返回参数为:[{}]", bakDCounts);

        logger.info("查询当天的[行名行号对照表 [CFG_BANK_INFO]]原表数据总条数开始,请求参数为:[{}]", openDay);
        int originalCounts = tableUtilsService.countTableLines("cmis_cfg", "CFG_BANK_INFO");
        logger.info("查询当天的[行名行号对照表 [CFG_BANK_INFO]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

        if (bakDCounts != originalCounts) {
            logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakDCounts, originalCounts);
            throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakDCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
        } else {
            logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakDCounts, originalCounts);
        }
    }


}
