package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0107</br>
 * 任务名称：加工任务-业务处理-当天未出账业务处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0107Mapper {


    /**
     *
     * 插入授权信息表0107B
     * @param openDay
     * @return
     */
    int insertTempPvpAuthorize0107B(@Param("openDay") String openDay);

    /**
     * 插入授权信息表0107A
     *
     * @param openDay
     * @return
     */
    int insertTempPvpAuthorize0107A(@Param("openDay") String openDay);


    /**
     * 贷款出账表作废
     *
     * @param openDay
     * @return
     */
   // int updatePvpLoanApp(@Param("openDay") String openDay);

    /**
     * 贷款台账表作废
     *
     * @param openDay
     * @return
     */
    int updateAccLoan(@Param("openDay") String openDay);

    /**
     * 银承出账表作废
     *
     * @param openDay
     * @return
     */
  //  int updatePvpAccpApp(@Param("openDay") String openDay);

    /**
     * 委托贷款台账表作废
     *
     * @param openDay
     * @return
     */
    int updateAccEntrustLoan(@Param("openDay") String openDay);
}
