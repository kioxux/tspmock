/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.core;

import cn.com.yusys.yusp.batch.domain.load.core.BatTCoreKlnbDkjcsx;
import cn.com.yusys.yusp.batch.repository.mapper.load.core.BatTCoreKlnbDkjcsxMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatTCoreKlnbDkjcsxService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 23:46:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTCoreKlnbDkjcsxService {

    @Autowired
    private BatTCoreKlnbDkjcsxMapper batTCoreKlnbDkjcsxMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTCoreKlnbDkjcsx selectByPrimaryKey(String farendma, String dkjiejuh) {
        return batTCoreKlnbDkjcsxMapper.selectByPrimaryKey(farendma, dkjiejuh);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTCoreKlnbDkjcsx> selectAll(QueryModel model) {
        List<BatTCoreKlnbDkjcsx> records = (List<BatTCoreKlnbDkjcsx>) batTCoreKlnbDkjcsxMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTCoreKlnbDkjcsx> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTCoreKlnbDkjcsx> list = batTCoreKlnbDkjcsxMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTCoreKlnbDkjcsx record) {
        return batTCoreKlnbDkjcsxMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTCoreKlnbDkjcsx record) {
        return batTCoreKlnbDkjcsxMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTCoreKlnbDkjcsx record) {
        return batTCoreKlnbDkjcsxMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTCoreKlnbDkjcsx record) {
        return batTCoreKlnbDkjcsxMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String farendma, String dkjiejuh) {
        return batTCoreKlnbDkjcsxMapper.deleteByPrimaryKey(farendma, dkjiejuh);
    }

    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTCoreKlnbDkjcsxMapper.truncateTTable();
    }
}
