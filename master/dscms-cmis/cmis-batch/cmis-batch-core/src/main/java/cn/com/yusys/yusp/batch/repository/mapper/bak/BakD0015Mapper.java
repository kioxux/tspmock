package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKD0015</br>
 * 任务名称：批前备份日表任务-备份资产池协议[CTR_ASPL_DETAILS] </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakD0015Mapper {

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertCurrent(@Param("openDay") String openDay);


    /**
     * 删除当天数据
     *
     * @return
     */
    void truncateCurrent();

    /**
     * 删除当天的数据
     *
     * @param openDay
     * @return
     */
    int deleteCurrent(@Param("openDay") String openDay);

    /**
     * 查询备份表当天的[资产池协议[TMP_D_CTR_ASPL_DETAILS]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryD0015DeleteOpenDayCtrAsplDetailsCounts(@Param("openDay") String openDay);

    /**
     * 查询原表当天的[资产池协议[CTR_ASPL_DETAILS]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryCtrAsplDetailsCounts(@Param("openDay") String openDay);
}
