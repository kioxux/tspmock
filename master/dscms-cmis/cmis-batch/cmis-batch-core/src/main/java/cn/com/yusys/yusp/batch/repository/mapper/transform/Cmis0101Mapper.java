package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0101</br>
 * 任务名称：加工任务-业务处理-贷款台账处理 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0101Mapper {
    /**
     * 删除银承台账表0101
     */
   // void deleteTempAccAccp0101();

    /**
     * 插入银承台账表0101
     *
     * @param openDay
     * @return
     */
   // int insertTempAccAccp0101(@Param("openDay") String openDay);

    /**
     * 垫款处理
     *
     * @param openDay
     * @return
     */
    int cmis0101InsertAccLoan(@Param("openDay") String openDay);

    /**
     * 更新  人民币金额 汇率
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan01A(@Param("openDay") String openDay);

    /**
     * 更新 台账状态 、借据起始日、到期日、借据余额 、结清日期
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan01(@Param("openDay") String openDay);

    /**
     * 贷款台账人民币余额补充
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan02A(@Param("openDay") String openDay);

    /**
     * 借据结清更新信贷台账状态
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan02(@Param("openDay") String openDay);

    /**
     * 借据核销更新信贷台账状态
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan03A(@Param("openDay") String openDay);

    /**
     * 更新欠息、罚息
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan03(@Param("openDay") String openDay);

    /**
     * 更新还款方式
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan04(@Param("openDay") String openDay);

    /**
     * 同步利率调整方式
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan05(@Param("openDay") String openDay);


    /**
     * 更新利率调整类型
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan05Tmp2(@Param("openDay") String openDay);


    /**
     * 清空核心利率信息临时表
     *
     * @return
     */
    void cmis0101DeleteAccLoan06();


    /**
     * 插入数据到核心利率信息临时表
     *
     * @param openDay
     */
    int cmis0101InsertAccLoan06(@Param("openDay") String openDay);


    /**
     * 更新逾期利率
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan06(@Param("openDay") String openDay);

    /**
     * 更新逾期罚息浮动比
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan07(@Param("openDay") String openDay);

    /**
     * 同步执行利率
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan08(@Param("openDay") String openDay);

    /**
     * 先将台账状态为正常的利息逾期天数字段 更新为0，再由后面的SQL根据核心数据更新该字段
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan09(@Param("openDay") String openDay);

    /**
     * 清空数据到贷款借据号和最大期供逾期天数关系表
     *
     * @return
     */
    void cmis0101TruncateTmp();

    /**
     * 插入数据到贷款借据号和最大期供逾期天数关系表
     */
    int cmis0101InsertTmp(@Param("openDay") String openDay);

    /**
     * 更新信贷逾期天数为 期供逾期天数
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan10(@Param("openDay") String openDay);

    /**
     * 清空数据到贷款借据号和最大期供逾期天数关系表
     *
     * @return
     */
    void cmis0101TruncateTmp01();

    /**
     * 插入数据到合同与申请加工临时表
     *
     * @param openDay
     * @return
     */
    int cmis0101InsertAccLoan01(@Param("openDay") String openDay);

    /**
     * 更新合同申请信息表中合同号为空的数据
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan11(@Param("openDay") String openDay);

    /**
     * 更新合同主表里合同汇率为空的数据
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan12(@Param("openDay") String openDay);

    /**
     * 更新合同申请信息表中申请汇率为空的数据
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan13(@Param("openDay") String openDay);

    /**
     * 更新复利利率
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan14(@Param("openDay") String openDay);


    /**
     * 更新复利浮动比
     *
     * @param openDay
     */
    int cmis0101UpdateAccLoan15(@Param("openDay") String openDay);

    /**
     * 更新正常本金和逾期本金
     *
     * @param openDay
     * @return
     */
    int cmis0101UpdateAccLoan16(@Param("openDay") String openDay);

    /**
     * 更新台账状态 、借据起始日、到期日、借据余额 、结清日期
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan01(@Param("openDay") String openDay);

    /**
     * 更新台账折人民币金额、汇率
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan01A(@Param("openDay") String openDay);

    /**
     * 贷款台账人民币余额补充更新
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan02A(@Param("openDay") String openDay);

    /**
     * 借据结清更新信贷台账状态
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan02(@Param("openDay") String openDay);

    /**
     * 借据核销更新信贷台账状态
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan03A(@Param("openDay") String openDay);

    /**
     * 欠息、罚息
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan03(@Param("openDay") String openDay);

    /**
     * 更新还款方式
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan04(@Param("openDay") String openDay);

    /**
     * 同步利率调整方式
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan05Mode(@Param("openDay") String openDay);

    /**
     * 同步利率调整选项 RATE_ADJ_TYPE
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan05Type(@Param("openDay") String openDay);

    /**
     * 更新逾期利率
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan06(@Param("openDay") String openDay);

    /**
     * 更新逾期罚息浮动比
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan07(@Param("openDay") String openDay);

    /**
     * 同步执行利率
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan08(@Param("openDay") String openDay);

    /**
     * 更新信贷台账状态正常的逾期天数为0
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan09(@Param("openDay") String openDay);

    /**
     * 清空临时表-贷款借据号和最大期供逾期天数关系表
     */
    void truncateEntrustTmpCoreDkzhqgDkjcsx();

    /**
     * 加工临时表-贷款借据号和最大期供逾期天数关系表
     *
     * @param openDay
     */
    int insertEntrustTmpCoreDkzhqgDkjcsx(@Param("openDay") String openDay);

    /**
     * 更新信贷逾期天数为期供逾期天数
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan10(@Param("openDay") String openDay);

    /**
     * 删除合同与申请加工临时表
     */
    void deleteEntrustTmpIqpCtr();

    /**
     * 插入数据到合同与申请加工临时表
     *
     * @param openDay
     */
    int cmis0101InsertAccEntrustLoan01(@Param("openDay") String openDay);

    /**
     * 更新合同申请信息表中合同号为空的数据
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan11(@Param("openDay") String openDay);

    /**
     * 更新合同主表里合同汇率为空的数据
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan12(@Param("openDay") String openDay);


    /**
     * 更新合同申请信息表中申请汇率为空的数据
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan13(@Param("openDay") String openDay);

    /**
     * 更新复利利率
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan14(@Param("openDay") String openDay);

    /**
     * 更新复利浮动比
     *
     * @param openDay
     */
    int cmis0101UpdateAccEntrustLoan15(@Param("openDay") String openDay);

}
