package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0151</br>
 * 任务名称：加工任务-业务处理-网金数据初始化到历史表 </br>
 *
 * @author sunzhe
 * @version 1.0
 * @since 2021年10月05日 下午9:56:54
 */
public interface Cmis0151Mapper {

    /**
     * 插入数据到合同历史表
     *
     * @param openDay
     * @return
     */
    int cmis0151InsertWjCtrLoanContHist03(@Param("openDay") String openDay);


    /**
     * 插入数据到台账历史表
     *
     * @param openDay
     * @return
     */
    int cmis0151InsertWjAccLoanHist04(@Param("openDay") String openDay);



}
