/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.check;

import org.apache.ibatis.annotations.Param;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateOutStandAmtBWMapper
 * @类描述: #Dao类
 * @功能描述: 数据更新额度校正-合同临时表-表外占用总余额与敞口余额
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCheckUpdateOutStandAmtBWMapper {


    /**
     * 1、最高额（未到期）：占用总余额、占用敞口余额； 银票、保函、信用证、贴现
     */
    int insertOutStandAmtBWZge1(@Param("cusId") String cusId);

    int insertOutStandAmtBWZge2(@Param("cusId") String cusId);

    int insertOutStandAmtBWZge3(@Param("cusId") String cusId);

    int insertOutStandAmtBWZge4(@Param("cusId") String cusId);

    int insertOutStandAmtBWZge5(@Param("cusId") String cusId);

    int insertOutStandAmtBWZge6(@Param("cusId") String cusId);

    int insertOutStandAmtBWZge7(@Param("cusId") String cusId);

    int insertOutStandAmtBWZge8(@Param("cusId") String cusId);

    int insertOutStandAmtBWZge9(@Param("cusId") String cusId);

    int insertOutStandAmtBWZge10(@Param("cusId") String cusId);


    /**
     * 2、一般合同（未到期）：占用总余额、占用敞口余额； 银票、保函、信用证、贴现
     */
    int insertOutStandAmtBWYbht1(@Param("cusId") String cusId);

    int insertOutStandAmtBWYbht2(@Param("cusId") String cusId);

    int insertOutStandAmtBWYbht3(@Param("cusId") String cusId);

    int insertOutStandAmtBWYbht4(@Param("cusId") String cusId);

    int insertOutStandAmtBWYbht5(@Param("cusId") String cusId);

    int insertOutStandAmtBWYbht6(@Param("cusId") String cusId);

    int insertOutStandAmtBWYbht7(@Param("cusId") String cusId);

    int insertOutStandAmtBWYbht8(@Param("cusId") String cusId);

    int insertOutStandAmtBWYbht9(@Param("cusId") String cusId);

    int insertOutStandAmtBWYbht10(@Param("cusId") String cusId);


    /**
     * 3、更新低风险担保方式占用敞口金额和敞口余额为0
     */
    int updateOutStandAmtBWDfxdb(@Param("cusId") String cusId);

    /**
     * 4、银票贴现占用敞口金额为0
     */
    int updateOutStandAmtBWYptx(@Param("cusId") String cusId);

}