package cn.com.yusys.yusp.batch.web.server.cmisbatch0010;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0010.Cmisbatch0010ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0010.Cmisbatch0010RespDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0010.CmisBatch0010Service;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:提供日终调度平台调起批量任务
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CMISBATCH0010:提供日终调度平台调起批量任务")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0010Resource.class);

    @Autowired
    private CmisBatch0010Service cmisBatch0010Service;

    /**
     * 交易码：cmisbatch0010
     * 交易描述：提供日终调度平台调起批量任务
     *
     * @param cmisbatch0010ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提供日终调度平台调起批量任务")
    @PostMapping("/cmisbatch0010")
    protected @ResponseBody
    ResultDto<Cmisbatch0010RespDto> cmisbatch0010(@Validated @RequestBody Cmisbatch0010ReqDto cmisbatch0010ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0010.key, DscmsEnum.TRADE_CODE_CMISBATCH0010.value, JSON.toJSONString(cmisbatch0010ReqDto));
        Cmisbatch0010RespDto cmisbatch0010RespDto = new Cmisbatch0010RespDto();// 响应Dto:提供日终调度平台调起批量任务
        ResultDto<Cmisbatch0010RespDto> cmisbatch0010ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0010ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0010.key, DscmsEnum.TRADE_CODE_CMISBATCH0010.value, JSON.toJSONString(cmisbatch0010ReqDto));
            cmisbatch0010RespDto = cmisBatch0010Service.cmisBatch0010(cmisbatch0010ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0010.key, DscmsEnum.TRADE_CODE_CMISBATCH0010.value, JSON.toJSONString(cmisbatch0010RespDto));
            // 封装cmisbatch0010ResultDto中正确的返回码和返回信息
            cmisbatch0010ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0010ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0010.key, DscmsEnum.TRADE_CODE_CMISBATCH0010.value, e.getMessage());
            // 封装cmisbatch0010ResultDto中异常返回码和返回信息
            cmisbatch0010ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0010ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0010RespDto到cmisbatch0010ResultDto中
        cmisbatch0010ResultDto.setData(cmisbatch0010RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0010.key, DscmsEnum.TRADE_CODE_CMISBATCH0010.value, JSON.toJSONString(cmisbatch0010ResultDto));
        return cmisbatch0010ResultDto;
    }
}
