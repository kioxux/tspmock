package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0417Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS417C</br>
 * 任务名称：加工任务-贷后管理-贷后检查子表数据生成[插入定期检查财务状况检查]  </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月22日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis417CTask extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis417CTask.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0417Service cmis417Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job cmis417CJob() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS417C_JOB.key, JobStepEnum.CMIS417C_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis417CJob = this.jobBuilderFactory.get(JobStepEnum.CMIS417C_JOB.key)
                .start(cmis417CUpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis417CCheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(cmis417CInsertPspFinSituCheckStep(WILL_BE_INJECTED))// 插入定期检查财务状况检查
                .next(cmis417CUpdatePspFinSituCheckStep(WILL_BE_INJECTED))// 更新定期检查财务状况检查
                .next(cmis417CUpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis417CJob;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis417CUpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS417C_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS417C_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis417UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.CMIS417C_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS417C_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS417C_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS417C_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis417UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis417CCheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS417C_CHECK_REL_STEP.key, JobStepEnum.CMIS417C_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis417CheckRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS417C_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS417C_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS417C_CHECK_REL_STEP.key, JobStepEnum.CMIS417C_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS417C_CHECK_REL_STEP.key, JobStepEnum.CMIS417C_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis417CheckRelStep;
    }

    /**
     * 插入定期检查财务状况检查
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis417CInsertPspFinSituCheckStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0417_INSERT_PSP_FIN_SITU_CHECK_STEP.key, JobStepEnum.CMIS0417_INSERT_PSP_FIN_SITU_CHECK_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis417InsertPspFinSituCheckStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0417_INSERT_PSP_FIN_SITU_CHECK_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Map map = new HashMap<>();
                    map.put("openDay",openDay);
                    cmis417Service.cmis0417InsertPspFinSituCheck(map);//插入定期检查财务状况检查
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0417_INSERT_PSP_FIN_SITU_CHECK_STEP.key, JobStepEnum.CMIS0417_INSERT_PSP_FIN_SITU_CHECK_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis417InsertPspFinSituCheckStep;
    }

    /**
     * 更新定期检查财务状况检查
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis417CUpdatePspFinSituCheckStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0417_UPDATE_PSP_FIN_SITU_CHECK_STEP.key, JobStepEnum.CMIS0417_UPDATE_PSP_FIN_SITU_CHECK_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis417UpdatePspFinSituCheckStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0417_UPDATE_PSP_FIN_SITU_CHECK_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Map map = new HashMap<>();
                    map.put("openDay",openDay);
                    cmis417Service.cmis0417UpdatePspFinSituCheck(map);//插入定期检查财务状况检查
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0417_UPDATE_PSP_FIN_SITU_CHECK_STEP.key, JobStepEnum.CMIS0417_UPDATE_PSP_FIN_SITU_CHECK_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis417UpdatePspFinSituCheckStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis417CUpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS417C_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS417C_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis417UpdateTask100Step = this.stepBuilderFactory.get(JobStepEnum.CMIS417C_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS417C_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS417C_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS417C_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis417UpdateTask100Step;
    }
}
