/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.LmtCheckDataOneService;

import cn.com.yusys.yusp.batch.repository.mapper.check.LmtCheckUpdateLmtContRelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateLmtContRelService
 * @类描述: #服务类
 * @功能描述: 数据更新额度校正-合同临时表-更新占用总金额与敞口金额
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCheckUpdateLmtContRelService {

    private static final Logger logger = LoggerFactory.getLogger(LmtCheckUpdateLmtContRelService.class);

    @Autowired
    private LmtCheckUpdateLmtContRelMapper lmtCheckUpdateLmtContRelMapper;

    public void checkOutLmtCheckUpdateLmtContRel(String cusId){
        logger.info("4.LmtCheckUpdateLmtContRel数据更新额度校正-合同临时表-更新占用总金额与敞口金额 --------start");
        /** 4.1、贷款合同/贷款合同申请：更新中间表业务的申请状态和审批状态 **/
        updateLmtContRelDkht(cusId) ;

        /** 4.2、委托贷款合同/委托贷款合同申请：更新中间表业务的申请状态和审批状态 **/
        updateLmtContRelWtdk(cusId) ;

        /** 4.2、最高额授信协议/最高额授信协议申请：更新中间表业务的申请状态和审批状态 **/
        updateLmtContRelZgesxxy(cusId) ;

        /** 4.3、资产池协议/资产池协议申请：更新中间表业务的申请状态和审批状态 **/
        updateLmtContRelZcc(cusId) ;

        /** 4.4、银票合同/银票合同申请：更新中间表业务的申请状态和审批状态 **/
        updateLmtContRelYpht(cusId) ;

        /** 4.5、保函合同/保函合同申请：更新中间表业务的申请状态和审批状态 **/
        updateLmtContRelBhht(cusId) ;

        /** 4.6、信用证合同/信用证合同申请：更新中间表业务的申请状态和审批状态 **/
        updateLmtContRelXyzht(cusId) ;

        /** 4.7、贴现合同/贴现合同申请：更新中间表业务的申请状态和审批状态 **/
        updateLmtContRelTxht(cusId) ;
        logger.info("4.LmtCheckUpdateLmtContRel数据更新额度校正-合同临时表-更新占用总金额与敞口金额 --------end");
    }

    /**
     * 4.1、贷款合同/贷款合同申请：更新中间表业务的申请状态和审批状态
     * @param cusId
     * @return
     */
    public int updateLmtContRelDkht(String cusId){
        logger.info(" 4.1、贷款合同/贷款合同申请：更新中间表业务的申请状态和审批状态 ----------------");
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelDkht_1(cusId);
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelDkht_2(cusId);
        return lmtCheckUpdateLmtContRelMapper.updateLmtContRelDkht_3(cusId);
    }


    /**
     * 4.2、委托贷款合同/委托贷款合同申请：更新中间表业务的申请状态和审批状态
     * @param cusId
     * @return
     */
    public int updateLmtContRelWtdk(String cusId){
        logger.info(" 4.2、委托贷款合同/委托贷款合同申请：更新中间表业务的申请状态和审批状态 ----------------");
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelWtdk_1(cusId);
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelWtdk_2(cusId);
        return lmtCheckUpdateLmtContRelMapper.updateLmtContRelWtdk_3(cusId);
    }


    /**
     * 4.2、最高额授信协议/最高额授信协议申请：更新中间表业务的申请状态和审批状态
     * @param cusId
     * @return
     */
    public int updateLmtContRelZgesxxy(String cusId){
        logger.info(" 4.2、最高额授信协议/最高额授信协议申请：更新中间表业务的申请状态和审批状态----------------");
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelZgesxxy_1(cusId);
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelZgesxxy_2(cusId);
        return lmtCheckUpdateLmtContRelMapper.updateLmtContRelZgesxxy_3(cusId);
    }


    /**
     * 4.3、资产池协议/资产池协议申请：更新中间表业务的申请状态和审批状态
     * @param cusId
     * @return
     */
    public int updateLmtContRelZcc(String cusId){
        logger.info(" 4.3、资产池协议/资产池协议申请：更新中间表业务的申请状态和审批状态 ----------------");
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelZcc_1(cusId);
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelZcc_2(cusId);
        return lmtCheckUpdateLmtContRelMapper.updateLmtContRelZcc_3(cusId);
    }


    /**
     * 4.4、银票合同/银票合同申请：更新中间表业务的申请状态和审批状态
     * @param cusId
     * @return
     */
    public int updateLmtContRelYpht(String cusId){
        logger.info(" 4.4、银票合同/银票合同申请：更新中间表业务的申请状态和审批状态 ----------------");
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelYpht_1(cusId);
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelYpht_2(cusId);
        return lmtCheckUpdateLmtContRelMapper.updateLmtContRelYpht_3(cusId);
    }


    /**
     * 4.5、保函合同/保函合同申请：更新中间表业务的申请状态和审批状态
     * @param cusId
     * @return
     */
    public int updateLmtContRelBhht(String cusId){
        logger.info(" 4.5、保函合同/保函合同申请：更新中间表业务的申请状态和审批状态 ----------------");
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelBhht_1(cusId);
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelBhht_2(cusId);
        return lmtCheckUpdateLmtContRelMapper.updateLmtContRelBhht_3(cusId);
    }


    /**
     * 4.6、信用证合同/信用证合同申请：更新中间表业务的申请状态和审批状态
     * @param cusId
     * @return
     */
    public int updateLmtContRelXyzht(String cusId){
        logger.info(" 4.6、信用证合同/信用证合同申请：更新中间表业务的申请状态和审批状态 ----------------");
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelXyzht_1(cusId);
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelXyzht_2(cusId);
        return lmtCheckUpdateLmtContRelMapper.updateLmtContRelXyzht_3(cusId);
    }


    /**
     * 4.7、贴现合同/贴现合同申请：更新中间表业务的申请状态和审批状态
     * @param cusId
     * @return
     */
    public int updateLmtContRelTxht(String cusId){
        logger.info(" 4.7、贴现合同/贴现合同申请：更新中间表业务的申请状态和审批状态 ----------------");
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelTxht_1(cusId);
        lmtCheckUpdateLmtContRelMapper.updateLmtContRelTxht_2(cusId);
        return lmtCheckUpdateLmtContRelMapper.updateLmtContRelTxht_3(cusId);
    }


}
