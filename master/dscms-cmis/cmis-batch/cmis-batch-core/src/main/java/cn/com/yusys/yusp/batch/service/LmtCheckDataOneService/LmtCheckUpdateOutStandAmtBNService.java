/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.LmtCheckDataOneService;

import cn.com.yusys.yusp.batch.repository.mapper.check.LmtCheckUpdateOutStandAmtBNMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateLmtContRelService
 * @类描述: #服务类
 * @功能描述: 数据更新额度校正-合同临时表-表内占用总余额与敞口余额
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCheckUpdateOutStandAmtBNService {

    private static final Logger logger = LoggerFactory.getLogger(LmtCheckUpdateOutStandAmtBNService.class);

    @Autowired
    private LmtCheckUpdateOutStandAmtBNMapper lmtCheckUpdateOutStandAmtBNMapper;

    public void checkOutLmtCheckUpdateOutStandAmtBN(String cusId){
        logger.info("5.LmtCheckUpdateOutStandAmtBN数据更新额度校正-合同临时表-表内占用总余额与敞口余额 --------start");
        /** 1、未到期的最高额贷款合同（及在途）占用额度  占用总余额  = 合同金额 ; 占用敞口 = 非低风险为合同金额、低风险为0 **/
        insertUpdateOutStandAmtBNZgedkht(cusId) ;

        /** 2、未到期的最高额委托贷款合同（及在途）占用额度：lmt_cont_rel表中 占用总余额 = 合同金额; 占用敞口 = 非低风险为合同金额、低风险为0 **/
        insertUpdateOutStandAmtBNZgewtdkht(cusId) ;

        /** 3、未到期的一般贷款合同（及在途）占用额度：lmt_cont_rel表中  占用总余额 = 合同金额 - 已发放贷款金额 + 已发放贷款余额; 占用敞口 = 非低风险为占用总余额、低风险为0 **/
        insertUpdateOutStandAmtBNYbdkht(cusId) ;

        /** 4、未到期的一般委托贷款合同（及在途）占用额度：lmt_cont_rel表中 占用总余额 = 合同金额 - 已发放贷款金额 + 已发放贷款余额；占用敞口 = 非低风险为占用总余额、低风险为0 **/
        insertUpdateOutStandAmtBNYbwtdkht(cusId) ;

        /** 5、未到期的最高额授信协议（及在途）占用额度：lmt_cont_rel表中  占用总余额 = 协议最高可用信金额；占用敞口 = 非低风险为协议最高可用信金额、低风险为0 **/
        insertUpdateOutStandAmtBNZgsxxy(cusId) ;

        /** 6、未到期的最高额资产池协议（及在途）占用额度 **/
        insertUpdateOutStandAmtBNZgezccxy(cusId) ;

        /** 7、已到期 ：用信申请下的台账余额；包含：最高额授信协议、贷款合同、银票合同、保函协议、信用证合同、银票合同、资产池合同、贴现协议 **/
        insertUpdateOutStandAmtBNBelongYx(cusId) ;
        logger.info("5.LmtCheckUpdateOutStandAmtBN数据更新额度校正-合同临时表-表内占用总余额与敞口余额 --------end");
    }

    /**
     * 1、未到期的最高额贷款合同（及在途）占用额度  占用总余额  = 合同金额 ; 占用敞口 = 非低风险为合同金额、低风险为0
     */
    public int insertUpdateOutStandAmtBNZgedkht(String cusId){
        logger.info("1、未到期的最高额贷款合同（及在途）占用额度  占用总余额  = 合同金额 ; 占用敞口 = 非低风险为合同金额、低风险为0 --------");
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgedkht1(cusId);
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgedkht2(cusId);
        return lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgedkht3(cusId);
    }

    /**
     * 2、未到期的最高额委托贷款合同（及在途）占用额度：lmt_cont_rel表中 占用总余额 = 合同金额; 占用敞口 = 非低风险为合同金额、低风险为0
     */
    public int insertUpdateOutStandAmtBNZgewtdkht(String cusId){
        logger.info("2、未到期的最高额委托贷款合同（及在途）占用额度：lmt_cont_rel表中 占用总余额 = 合同金额; 占用敞口 = 非低风险为合同金额、低风险为0 --------");
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgewtdkht1(cusId);
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgewtdkht2(cusId);
        return lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgewtdkht3(cusId);
    }

    /**
     * 3、未到期的一般贷款合同（及在途）占用额度：lmt_cont_rel表中  占用总余额 = 合同金额 - 已发放贷款金额 + 已发放贷款余额; 占用敞口 = 非低风险为占用总余额、低风险为0
     */
    public int insertUpdateOutStandAmtBNYbdkht(String cusId){
        logger.info("3、未到期的一般贷款合同（及在途）占用额度：lmt_cont_rel表中  占用总余额 = 合同金额 - 已发放贷款金额 + 已发放贷款余额; 占用敞口 = 非低风险为占用总余额、低风险为0 --------");
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNYbdkht1(cusId);
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNYbdkht2(cusId);
        return lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNYbdkht3(cusId);
    }

    /**
     * 4、未到期的一般委托贷款合同（及在途）占用额度：lmt_cont_rel表中 占用总余额 = 合同金额 - 已发放贷款金额 + 已发放贷款余额；占用敞口 = 非低风险为占用总余额、低风险为0
     */
    public int insertUpdateOutStandAmtBNYbwtdkht(String cusId){
        logger.info("4、未到期的一般委托贷款合同（及在途）占用额度：lmt_cont_rel表中 占用总余额 = 合同金额 - 已发放贷款金额 + 已发放贷款余额；占用敞口 = 非低风险为占用总余额、低风险为0 --------");
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNYbwtdkht1(cusId);
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNYbwtdkht2(cusId);
        return lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNYbwtdkht3(cusId);
    }

    /**
     * 5、未到期的最高额授信协议（及在途）占用额度：lmt_cont_rel表中  占用总余额 = 协议最高可用信金额；占用敞口 = 非低风险为协议最高可用信金额、低风险为0
     */
    public int insertUpdateOutStandAmtBNZgsxxy(String cusId){
        logger.info("5、未到期的最高额授信协议（及在途）占用额度：lmt_cont_rel表中  占用总余额 = 协议最高可用信金额；占用敞口 = 非低风险为协议最高可用信金额、低风险为0 --------");
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgsxxy1(cusId);
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgsxxy2(cusId);
        return lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgsxxy3(cusId);
    }

    /**
     * 6、未到期的最高额资产池协议（及在途）占用额度
     */
    public int insertUpdateOutStandAmtBNZgezccxy(String cusId){
        logger.info("6、未到期的最高额资产池协议（及在途）占用额度 --------");
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgezccxy1(cusId);
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgezccxy2(cusId);
        return lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNZgezccxy3(cusId);
    }

    /**
     * 7、已到期 ：用信申请下的台账余额
     * 包含：最高额授信协议、贷款合同、银票合同、保函协议、信用证合同、银票合同、资产池合同、贴现协议
     */
    public int insertUpdateOutStandAmtBNBelongYx(String cusId){
        logger.info("7、已到期 ：用信申请下的台账余额:包含：最高额授信协议、贷款合同、银票合同、保函协议、信用证合同、银票合同、资产池合同、贴现协议 --------");
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNBelongYx1(cusId);
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNBelongYx2(cusId);
        lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNBelongYx3(cusId);
        return lmtCheckUpdateOutStandAmtBNMapper.insertUpdateOutStandAmtBNBelongYx4(cusId);
    }

}
