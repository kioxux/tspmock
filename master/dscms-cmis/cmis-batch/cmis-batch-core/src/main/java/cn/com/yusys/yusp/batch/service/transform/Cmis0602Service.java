package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.domain.cfg.WbMsgNotice;
import cn.com.yusys.yusp.batch.domain.sms.SmsManageInfo;
import cn.com.yusys.yusp.batch.domain.sms.SmsManageInfoHis;
import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0602Mapper;
import cn.com.yusys.yusp.batch.service.cfg.WbMsgNoticeService;
import cn.com.yusys.yusp.batch.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.batch.service.sms.SmsManageInfoHisService;
import cn.com.yusys.yusp.batch.service.sms.SmsManageInfoService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0602</br>
 * 任务名称：加工任务-批量管理-提醒事项推送</br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0602Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0602Service.class);
    @Autowired
    private MessageCommonService messageCommonService;
    @Autowired
    private SmsManageInfoService smsManageInfoService;
    @Autowired
    private SmsManageInfoHisService smsManageInfoHisService;
    @Autowired
    private WbMsgNoticeService wbMsgNoticeService;



    /**
     * 查询短信通知表输入表，MSG_PL_C_0001
     *
     * @param openDay
     */
    public void cmis0602QuerySmsManageInfoC0001(String openDay) {
        String messageType = "MSG_PL_C_0001"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人
        this.commMsgPlC0001(openDay, messageType, receivedUserType);
    }

    /**
     * 查询短信通知表输入表，MSG_PL_C_0002
     *
     * @param openDay
     */
    public void cmis0602QuerySmsManageInfoC0002(String openDay) {
        String messageType = "MSG_PL_C_0002"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人
        this.commMsgPlC0001(openDay, messageType, receivedUserType);
    }

    /**
     * 查询短信通知表输入表，MSG_PL_C_0003
     *
     * @param openDay
     */
    public void cmis0602QuerySmsManageInfoC0003(String openDay) {
        String messageType = "MSG_PL_C_0003"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人
        this.commMsgPlC0001(openDay, messageType, receivedUserType);
    }

    /**
     * 查询短信通知表输入表，MSG_PL_C_0004
     *
     * @param openDay
     */
    public void cmis0602QuerySmsManageInfoC0004(String openDay) {
        String messageType = "MSG_PL_C_0004"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人
        this.commMsgPlC0001(openDay, messageType, receivedUserType);
    }

    /**
     * 短信提醒客户公共方法
     *
     * @param openDay
     * @param messageType
     * @param receivedUserType
     */
    public void commMsgPlC0001(String openDay, String messageType, String receivedUserType) {
        QueryModel model = new QueryModel();
        model.addCondition("subCode", messageType);
        logger.info("查询短信通知表输入表开始,请求参数为:[{}]", openDay);
        List<SmsManageInfo> smsManageInfoList = smsManageInfoService.selectAll(model);
        logger.info("查询短信通知表输入表结束,响应参数为:[{}]", smsManageInfoList.size());
        DecimalFormat decimalFormat = new DecimalFormat("###.00");
        int countNum = 0;
        // 友情提醒：尊敬的${cusName}，您的${prdName}贷款还款日为${repayDateYy}年${repayDateMm}月${repayDateDd}日，本次须归还本息${repayAmt}元，请确保您的账户余额充足,如有疑问请咨询96065或经办客户经理，如非本人请转告或忽略!
        for (int i = 0; i < smsManageInfoList.size(); i++) {
            SmsManageInfo smsManageInfo = smsManageInfoList.get(i);
            String smsId = smsManageInfo.getSmsId();// 主键
            String cusName = smsManageInfo.getCusName();
            String prdName = smsManageInfo.getPrdName();
            String repayDateYy = smsManageInfo.getRepayDateYy();
            String repayDateMm = smsManageInfo.getRepayDateMm();
            String repayDateDd = smsManageInfo.getRepayDateDd();
            BigDecimal repayAmt = smsManageInfo.getRepayAmt();
            String smsContent = smsManageInfo.getSmsContent(); // 借据号
            String phone = smsManageInfo.getPhone();// 接收短信的手机号
            if ("".equals(phone) || null == phone) {
                this.updateSmsManageInfo(openDay, smsManageInfo);
                continue;
            }
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", cusName);
            paramMap.put("prdName", prdName);
            paramMap.put("smsContent", "****" + smsContent.substring(smsContent.length()-4)); // 借据号
            paramMap.put("repayDateYy", repayDateYy);
            paramMap.put("repayDateMm", repayDateMm);
            paramMap.put("repayDateDd", repayDateDd);
            paramMap.put("repayAmt", decimalFormat.format(repayAmt)); // 不支持BigDecimal类型

            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, "", phone);
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                smsManageInfo.setSmsSendState("1"); // 1-已发送
                this.updateSmsManageInfo(openDay, smsManageInfo);
            }
            if (countNum > 500) {
//                try {
//                    logger.info("调用短信发送后每500条时候，该线程休眠10秒钟开始");
//                    TimeUnit.SECONDS.sleep(10); // sleep 10秒钟
//                    logger.info("调用短信发送后每500条时候，该线程休眠10秒钟结束");
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                countNum = 0;
                break;
            } else {
                countNum++;
            }
        }

    }

    /**
     * 更新短信通知表
     * @param openDay
     * @param smsManageInfo
     */
    public void updateSmsManageInfo(String openDay, SmsManageInfo smsManageInfo) {
        SmsManageInfoHis smsManageInfoHis = new SmsManageInfoHis();
        BeanUtils.copyProperties(smsManageInfo, smsManageInfoHis);
        smsManageInfoHis.setTaskDate(openDay); // 任务日期
        logger.info("将短信通知表输入表中值备份到短信通知历史表中开始,请求参数为:[{}]", JSON.toJSONString(smsManageInfoHis));
        int smsHisInsertResult = smsManageInfoHisService.insertSelective(smsManageInfoHis);
        logger.info("将短信通知表输入表中值备份到短信通知历史表中结束,响应参数为:[{}]", JSON.toJSONString(smsHisInsertResult));

        logger.info("将短信通知表输入表中值删除开始,请求参数为:[{}]", JSON.toJSONString(smsManageInfo.getSmsId()));
        int smsDeleteResult = smsManageInfoService.deleteByPrimaryKey(smsManageInfo.getSmsId());
        logger.info("将短信通知表输入表中值删除结束,响应参数为:[{}]", JSON.toJSONString(smsDeleteResult));
    }

    /**
     * 查询短信通知表输入表，MSG_PL_C_0005
     *
     * @param openDay
     */
    public void cmis0602QuerySmsManageInfoC0005(String openDay) {
        String messageType = "MSG_PL_C_0005"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人
        this.commMsgPlC0005(openDay, messageType, receivedUserType);
    }

    /**
     * 查询短信通知表输入表，MSG_PL_C_0006
     *
     * @param openDay
     */
    public void cmis0602QuerySmsManageInfoC0006(String openDay) {
        String messageType = "MSG_PL_C_0006"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人
        this.commMsgPlC0005(openDay, messageType, receivedUserType);
    }

    /**
     * 查询短信通知表输入表，MSG_PL_C_0007
     *
     * @param openDay
     */
    public void cmis0602QuerySmsManageInfoC0007(String openDay) {
        String messageType = "MSG_PL_C_0007"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人
        this.commMsgPlC0005(openDay, messageType, receivedUserType);
    }

    /**
     * 查询短信通知表输入表，MSG_PL_C_0008
     *
     * @param openDay
     */
    public void cmis0602QuerySmsManageInfoC0008(String openDay) {
        String messageType = "MSG_PL_C_0008"; // 短信编号
        String receivedUserType = "2"; // 接收人员类型 1--客户经理 2--借款人
        this.commMsgPlC0005(openDay, messageType, receivedUserType);
    }
    /**
     * 短信催收客户公共方法
     *
     * @param openDay
     * @param messageType
     * @param receivedUserType
     */
    public void commMsgPlC0005(String openDay, String messageType, String receivedUserType) {
        QueryModel model = new QueryModel();
        model.addCondition("subCode", messageType);
        logger.info("查询短信通知表输入表开始,请求参数为:[{}]", openDay);
        List<SmsManageInfo> smsManageInfoList = smsManageInfoService.selectAll(model);
        logger.info("查询短信通知表输入表结束,响应参数为:[{}]", smsManageInfoList.size());
        DecimalFormat decimalFormat = new DecimalFormat("###.00");
        int countNum = 0;
        // 友情提醒：尊敬的${cusName}，截止到${currrentYear}年${currrentMonth}月${currentDay}日，您的${prdName}贷款逾期金额总计${overToatalAmt}元，已产生不良信息。请您及时还款，以免您的不良信息向金融信用信息基础数据库提供，如有疑问请咨询96065或经办客户经理，如非本人请转告或忽略!
        for (int i = 0; i < smsManageInfoList.size(); i++) {
            SmsManageInfo smsManageInfo = smsManageInfoList.get(i);
            String smsId = smsManageInfo.getSmsId();// 主键
            String cusName = smsManageInfo.getCusName();
            String prdName = smsManageInfo.getPrdName();
            String currrentYear = smsManageInfo.getCurrrentYear();
            String currrentMonth = smsManageInfo.getCurrrentMonth();
            String currentDay = smsManageInfo.getCurrentDay();
            BigDecimal overToatalAmt = smsManageInfo.getOverTotalAmt();
            String smsContent = smsManageInfo.getSmsContent(); // 短信内容
            String phone = smsManageInfo.getPhone(); // 接收短信的手机号
            if ("".equals(phone) || null == phone) {
                this.updateSmsManageInfo(openDay, smsManageInfo);
                continue;
            }
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", cusName);
            paramMap.put("prdName", prdName);
            paramMap.put("smsContent", "****" + smsContent.substring(smsContent.length() - 4)); // 借据编号：***后四位
            paramMap.put("currrentYear", currrentYear);
            paramMap.put("currrentMonth", currrentMonth);
            paramMap.put("currentDay", currentDay);
            paramMap.put("overToatalAmt", decimalFormat.format(overToatalAmt)); // 不支持BigDecimal类型

            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, "", phone);
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                smsManageInfo.setSmsSendState("1"); // 1-已发送
                this.updateSmsManageInfo(openDay, smsManageInfo);
            }
            if (countNum > 500) {
//                try {
//                    logger.info("调用短信发送后每500条时候，该线程休眠10秒钟开始");
//                    TimeUnit.SECONDS.sleep(10); // sleep 10秒钟
//                    logger.info("调用短信发送后每500条时候，该线程休眠10秒钟结束");
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                countNum = 0;
                break;
            } else {
                countNum++;
            }
        }
    }

    /**
     * 查询风险提示表</br>
     * 对应短信编号ID：MSG_PL_M_0001(您好，您管户下的${cusName}客户下${prdName}业务（借据编号：${smsContent}）将于${repayDateYy}年${repayDateMm}月${repayDateDd}日到期，请通知客户及时还款！)
     *
     * @param openDay
     */
    public void cmis0602QuerySmsManageInfoC0009(String openDay) {
        String messageType = "MSG_PL_M_0001"; // 短信编号
        String receivedUserType = "1"; // 接收人员类型 1--客户经理 2--借款人
        QueryModel model = new QueryModel();
        model.addCondition("subCode", messageType);
        logger.info("查询短信通知表输入表开始,请求参数为:[{}]", openDay);
        List<SmsManageInfo> smsManageInfoList = smsManageInfoService.selectAll(model);
        logger.info("查询短信通知表输入表结束,响应参数为:[{}]", smsManageInfoList.size());
        int countNum = 0;
        // 您好，您管户下的${cusName}客户下${prdName}业务（借据编号：${smsContent}）将于${repayDateYy}年${repayDateMm}月${repayDateDd}日到期，请通知客户及时还款！
        for (int i = 0; i < smsManageInfoList.size(); i++) {
            SmsManageInfo smsManageInfo = smsManageInfoList.get(i);
            String smsId = smsManageInfo.getSmsId();// 主键
            String cusName = smsManageInfo.getCusName();
            String prdName = smsManageInfo.getPrdName();
            String repayDateYy = smsManageInfo.getRepayDateYy();
            String repayDateMm = smsManageInfo.getRepayDateMm();
            String repayDateDd = smsManageInfo.getRepayDateDd();
            String smsContent = smsManageInfo.getSmsContent(); // 借据号
            String phone = smsManageInfo.getPhone();// 接收短信的手机号
            String userId = smsManageInfo.getUserId(); // 客户经理号
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", cusName);
            paramMap.put("prdName", prdName);
            paramMap.put("repayDateYy", repayDateYy);
            paramMap.put("repayDateMm", repayDateMm);
            paramMap.put("repayDateDd", repayDateDd);
            paramMap.put("smsContent", smsContent); // 借据号

            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, userId, phone);
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                smsManageInfo.setSmsSendState("1"); // 1-已发送
                this.updateSmsManageInfo(openDay, smsManageInfo);
            }
            if (countNum > 500) {
                try {
                    logger.info("调用短信发送后每500条时候，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10); // sleep 10秒钟
                    logger.info("调用短信发送后每500条时候，该线程休眠10秒钟结束");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                countNum = 0;
            } else {
                countNum++;
            }
        }
    }

    /**
     * 查询风险提示表</br>
     * 对应短信编号ID：MSG_PL_M_0002(您好，您管户下的${cusName}客户的授信将于${repayDateYy}年${repayDateMm}月${repayDateDd}日到期，请及时发起授信续作！)
     *
     * @param openDay
     */
    public void cmis0602QuerySmsManageInfoC0010(String openDay) {
        String messageType = "MSG_PL_M_0002"; // 短信编号
        String receivedUserType = "1"; //接收人员类型 1--客户经理 2--借款人
        QueryModel model = new QueryModel();
        model.addCondition("subCode", messageType);
        logger.info("查询短信通知表输入表开始,请求参数为:[{}]", openDay);
        List<SmsManageInfo> smsManageInfoList = smsManageInfoService.selectAll(model);
        logger.info("查询短信通知表输入表结束,响应参数为:[{}]", smsManageInfoList.size());
        int countNum = 0;
        // 您好，您管户下的${cusName}客户的授信将于${repayDateYy}年${repayDateMm}月${repayDateDd}日到期，请及时发起授信续作！
        for (int i = 0; i < smsManageInfoList.size(); i++) {
            SmsManageInfo smsManageInfo = smsManageInfoList.get(i);
            String smsId = smsManageInfo.getSmsId();// 主键
            String cusName = smsManageInfo.getCusName();
            String repayDateYy = smsManageInfo.getRepayDateYy();
            String repayDateMm = smsManageInfo.getRepayDateMm();
            String repayDateDd = smsManageInfo.getRepayDateDd();
            String smsContent = smsManageInfo.getSmsContent();
            String phone = smsManageInfo.getPhone();// 接收短信的手机号
            String userId = smsManageInfo.getUserId(); // 客户经理号
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", cusName);
            paramMap.put("repayDateYy", repayDateYy);
            paramMap.put("repayDateMm", repayDateMm);
            paramMap.put("repayDateDd", repayDateDd);

            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, userId, phone);
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                smsManageInfo.setSmsSendState("1"); // 1-已发送
                this.updateSmsManageInfo(openDay, smsManageInfo);
            }
            if (countNum > 500) {
                try {
                    logger.info("调用短信发送后每500条时候，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10); // sleep 10秒钟
                    logger.info("调用短信发送后每500条时候，该线程休眠10秒钟结束");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                countNum = 0;
            } else {
                countNum++;
            }
        }
    }

    /**
     * 查询风险提示表</br>
     * 对应短信编号ID：MSG_PL_M_0003(您名下档案调阅未归还，已逾期${docOverdueDay}天！)
     *
     * @param openDay
     */
    public void cmis0602QueryWbMsgNotice0003(String openDay) {
        QueryModel model = new QueryModel();
        model.addCondition("messageStatus", "1");
        model.addCondition("messageTypeId", "MSG_PL_M_0003");
        model.addCondition("oprType", "01");// 新增
        logger.info("根据消息状态查询风险提示表开始,请求参数为:[{}]", openDay);
        List<WbMsgNotice> wbMsgNoticeList = wbMsgNoticeService.selectAll(model);
        logger.info("根据消息状态查询风险提示表结束,响应参数为:[{}]", wbMsgNoticeList.size());

        wbMsgNoticeList.stream().forEach(wbMsgNotice -> {
            String docOverdueDay = wbMsgNotice.getDocOverdueDay();
            String inputId = wbMsgNotice.getInputId(); // 存量的业务存的是中文名称,发不了也不影响。(已找季建确认)
            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
            String messageType = "MSG_PL_M_0003";//短信编号
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("docOverdueDay", docOverdueDay);
            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, inputId, "");
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                wbMsgNotice.setMessageStatus("2");
                wbMsgNotice.setOprType("02");//删除
                wbMsgNoticeService.updateSelective(wbMsgNotice);
            }
        });

    }

    /**
     * 查询风险提示表</br>
     * 对应短信编号ID：MSG_PL_M_0004(您好，您管户下诉讼当事人${cusName}${cusId}，距离诉讼时效到期还有${beforeMonth}个月！)
     *
     * @param openDay
     */
    public void cmis0602QueryWbMsgNotice0004(String openDay) {
        QueryModel model = new QueryModel();
        model.addCondition("messageStatus", "1");
        model.addCondition("messageTypeId", "MSG_PL_M_0004");
        model.addCondition("oprType", "01");// 新增
        logger.info("根据消息状态查询风险提示表开始,请求参数为:[{}]", openDay);
        List<WbMsgNotice> wbMsgNoticeList = wbMsgNoticeService.selectByModel(model);
        logger.info("根据消息状态查询风险提示表结束,响应参数为:[{}]", wbMsgNoticeList.size());

        wbMsgNoticeList.stream().forEach(wbMsgNotice -> {
            String cusName = wbMsgNotice.getCusName();
            String cusId = wbMsgNotice.getCusId();
            String beforeMonth = wbMsgNotice.getBeforeMonth();
            String inputId = wbMsgNotice.getInputId();
            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
            String messageType = "MSG_PL_M_0004";//短信编号
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", cusName);
            paramMap.put("cusId", cusId);
            paramMap.put("beforeMonth", beforeMonth);
            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, inputId, "");
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                wbMsgNotice.setMessageStatus("2");
                wbMsgNotice.setOprType("02");//删除
                wbMsgNoticeService.updateSelective(wbMsgNotice);
            }
        });

    }

    /**
     * 查询风险提示表</br>
     * 对应短信编号ID：MSG_PL_M_0005(您好，您管户下抵债资产抵押物名称${pldimnMemo}客户名称${cusName}客户号${cusId}，距离抵债资产处置时效到期日前还有${beforeMonth}个月！)
     *
     * @param openDay
     */
    public void cmis0602QueryWbMsgNotice0005(String openDay) {
        QueryModel model = new QueryModel();
        model.addCondition("messageStatus", "1");
        model.addCondition("messageTypeId", "MSG_PL_M_0005");
        model.addCondition("oprType", "01");// 新增
        logger.info("根据消息状态查询风险提示表开始,请求参数为:[{}]", openDay);
        List<WbMsgNotice> wbMsgNoticeList = wbMsgNoticeService.selectByModel(model);
        logger.info("根据消息状态查询风险提示表结束,响应参数为:[{}]", wbMsgNoticeList.size());

        wbMsgNoticeList.stream().forEach(wbMsgNotice -> {
            String pldimnMemo = wbMsgNotice.getPldimnMemo();
            String cusName = wbMsgNotice.getCusName();
            String cusId = wbMsgNotice.getCusId();
            String beforeMonth = wbMsgNotice.getBeforeMonth();
            String inputId = wbMsgNotice.getInputId();
            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
            String messageType = "MSG_PL_M_0005";//短信编号
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("pldimnMemo", pldimnMemo);
            paramMap.put("cusName", cusName);
            paramMap.put("cusId", cusId);
            paramMap.put("beforeMonth", beforeMonth);
            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, inputId, "");
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                wbMsgNotice.setMessageStatus("2");
                wbMsgNotice.setOprType("02");//删除
                wbMsgNoticeService.updateSelective(wbMsgNotice);
            }
        });

    }

    /**
     * 查询风险提示表</br>
     * 对应短信编号ID：MSG_PL_M_0006(您好，您管户下客户${cusName}${cusId}已完成单户呆账核销流程满两年，请进行核销后事项处理！)
     *
     * @param openDay
     */
    public void cmis0602QueryWbMsgNotice0006(String openDay) {
        QueryModel model = new QueryModel();
        model.addCondition("messageStatus", "1");
        model.addCondition("messageTypeId", "MSG_PL_M_0006");
        model.addCondition("oprType", "01");// 新增
        logger.info("根据消息状态查询风险提示表开始,请求参数为:[{}]", openDay);
        List<WbMsgNotice> wbMsgNoticeList = wbMsgNoticeService.selectByModel(model);
        logger.info("根据消息状态查询风险提示表结束,响应参数为:[{}]", wbMsgNoticeList.size());

        wbMsgNoticeList.stream().forEach(wbMsgNotice -> {
            String cusName = wbMsgNotice.getCusName();
            String cusId = wbMsgNotice.getCusId();
            String inputId = wbMsgNotice.getInputId();
            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
            String messageType = "MSG_PL_M_0006";//短信编号
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", cusName);
            paramMap.put("cusId", cusId);
            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, inputId, "");
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                wbMsgNotice.setMessageStatus("2");
                wbMsgNotice.setOprType("02");//删除
                wbMsgNoticeService.updateSelective(wbMsgNotice);
            }
        });

    }

    /**
     * 查询风险提示表</br>
     * 对应短信编号ID：MSG_PL_M_0007(您好，您管户下业务流水号${pbdwabSerno}核销总户数${totalWriteoffCus}核销笔数${totalWriteoffNum}已完成批量核销流程满两年，请进行核销后事项处理！)
     *
     * @param openDay
     */
    public void cmis0602QueryWbMsgNotice0007(String openDay) {
        QueryModel model = new QueryModel();
        model.addCondition("messageStatus", "1");
        model.addCondition("messageTypeId", "MSG_PL_M_0007");
        model.addCondition("oprType", "01");// 新增
        logger.info("根据消息状态查询风险提示表开始,请求参数为:[{}]", openDay);
        List<WbMsgNotice> wbMsgNoticeList = wbMsgNoticeService.selectByModel(model);
        logger.info("根据消息状态查询风险提示表结束,响应参数为:[{}]", wbMsgNoticeList.size());

        wbMsgNoticeList.stream().forEach(wbMsgNotice -> {
            String pbdwabSerno = wbMsgNotice.getPbdwabSerno();
            String totalWriteoffCus = wbMsgNotice.getTotalWriteoffCus();
            String totalWriteoffNum = wbMsgNotice.getTotalWriteoffNum();
            String inputId = wbMsgNotice.getInputId();
            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
            String messageType = "MSG_PL_M_0007";//短信编号
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("pbdwabSerno", pbdwabSerno);
            paramMap.put("totalWriteoffCus", totalWriteoffCus);
            paramMap.put("totalWriteoffNum", totalWriteoffNum);

            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, inputId, "");
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                wbMsgNotice.setMessageStatus("2");
                wbMsgNotice.setOprType("02");//删除
                wbMsgNoticeService.updateSelective(wbMsgNotice);
            }
        });

    }

    /**
     * 查询风险提示表</br>
     * 对应短信编号ID：MSG_PL_M_0008(您好，您管户下业务流水号${pcwaSerno}核销总户数${totalWriteoffCus}已完成信用卡核销流程满两年，请进行核销后事项处理！)
     *
     * @param openDay
     */
    public void cmis0602QueryWbMsgNotice0008(String openDay) {
        QueryModel model = new QueryModel();
        model.addCondition("messageStatus", "1");
        model.addCondition("messageTypeId", "MSG_PL_M_0008");
        model.addCondition("oprType", "01");// 新增
        logger.info("根据消息状态查询风险提示表开始,请求参数为:[{}]", openDay);
        List<WbMsgNotice> wbMsgNoticeList = wbMsgNoticeService.selectByModel(model);
        logger.info("根据消息状态查询风险提示表结束,响应参数为:[{}]", wbMsgNoticeList.size());

        wbMsgNoticeList.stream().forEach(wbMsgNotice -> {
            String pcwaSerno = wbMsgNotice.getPcwaSerno();
            String totalWriteoffCus = wbMsgNotice.getTotalWriteoffCus();
            String inputId = wbMsgNotice.getInputId();
            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
            String messageType = "MSG_PL_M_0008";//短信编号
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("pcwaSerno", pcwaSerno);
            paramMap.put("totalWriteoffCus", totalWriteoffCus);

            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, inputId, "");
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                wbMsgNotice.setMessageStatus("2");
                wbMsgNotice.setOprType("02");//删除
                wbMsgNoticeService.updateSelective(wbMsgNotice);
            }
        });

    }

    /**
     * 查询风险提示表</br>
     * 对应短信编号ID：MSG_PL_M_0009(您好，您管户下资金同业客户${cusName}${cusId}准入名单年审即将到期，请及时处理！)
     *
     * @param openDay
     */
    public void cmis0602QueryWbMsgNotice0009(String openDay) {
        QueryModel model = new QueryModel();
        model.addCondition("messageStatus", "1");
        model.addCondition("messageTypeId", "MSG_PL_M_0009");
        model.addCondition("oprType", "01");// 新增
        logger.info("根据消息状态查询风险提示表开始,请求参数为:[{}]", openDay);
        List<WbMsgNotice> wbMsgNoticeList = wbMsgNoticeService.selectByModel(model);
        logger.info("根据消息状态查询风险提示表结束,响应参数为:[{}]", wbMsgNoticeList.size());

        wbMsgNoticeList.stream().forEach(wbMsgNotice -> {
            String cusName = wbMsgNotice.getCusName();
            String cusId = wbMsgNotice.getCusId();
            String inputId = wbMsgNotice.getInputId();
            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
            String messageType = "MSG_PL_M_0009";//短信编号
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", cusName);
            paramMap.put("cusId", cusId);
            logger.info("调用短信发送方法开始");
            ResultDto<Integer> sendMessageResultDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, inputId, "");
            logger.info("调用短信发送方法结束");
            if (sendMessageResultDto != null && Objects.equals(ResultDto.success().getCode(), sendMessageResultDto.getCode())) {
                wbMsgNotice.setMessageStatus("2");
                wbMsgNotice.setOprType("02");//删除
                wbMsgNoticeService.updateSelective(wbMsgNotice);
            }
        });

    }


}
