/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: yusp-batch-core模块
 * @类名称: BatControlRunResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-09 17:12:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batcontrolrun")
public class BatControlRunResource {
    @Autowired
    private BatControlRunService batControlRunService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatControlRun>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatControlRun> list = batControlRunService.selectAll(queryModel);
        return new ResultDto<List<BatControlRun>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatControlRun>> index(QueryModel queryModel) {
        List<BatControlRun> list = batControlRunService.selectByModel(queryModel);
        return new ResultDto<List<BatControlRun>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatControlRun>> query(@RequestBody QueryModel queryModel) {
        List<BatControlRun> list = batControlRunService.selectByModel(queryModel);
        return new ResultDto<List<BatControlRun>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{openDay}")
    protected ResultDto<BatControlRun> show(@PathVariable("openDay") String openDay) {
        BatControlRun batControlRun = batControlRunService.selectByPrimaryKey(openDay);
        return new ResultDto<BatControlRun>(batControlRun);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatControlRun> create(@RequestBody BatControlRun batControlRun) throws URISyntaxException {
        batControlRunService.insert(batControlRun);
        return new ResultDto<BatControlRun>(batControlRun);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatControlRun batControlRun) throws URISyntaxException {
        int result = batControlRunService.update(batControlRun);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{openDay}")
    protected ResultDto<Integer> delete(@PathVariable("openDay") String openDay) {
        int result = batControlRunService.deleteByPrimaryKey(openDay);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batControlRunService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
