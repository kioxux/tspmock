/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatTransferCfg;
import cn.com.yusys.yusp.batch.service.bat.BatTransferCfgService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTransferCfgResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-13 15:40:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battransfercfg")
public class BatTransferCfgResource {
    @Autowired
    private BatTransferCfgService batTransferCfgService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTransferCfg>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTransferCfg> list = batTransferCfgService.selectAll(queryModel);
        return new ResultDto<List<BatTransferCfg>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTransferCfg>> index(QueryModel queryModel) {
        List<BatTransferCfg> list = batTransferCfgService.selectByModel(queryModel);
        return new ResultDto<List<BatTransferCfg>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatTransferCfg>> query(@RequestBody QueryModel queryModel) {
        List<BatTransferCfg> list = batTransferCfgService.selectByModel(queryModel);
        return new ResultDto<List<BatTransferCfg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{taskNo}")
    protected ResultDto<BatTransferCfg> show(@PathVariable("taskNo") String taskNo) {
        BatTransferCfg batTransferCfg = batTransferCfgService.selectByPrimaryKey(taskNo);
        return new ResultDto<BatTransferCfg>(batTransferCfg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTransferCfg> create(@RequestBody BatTransferCfg batTransferCfg) throws URISyntaxException {
        batTransferCfgService.insert(batTransferCfg);
        return new ResultDto<BatTransferCfg>(batTransferCfg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTransferCfg batTransferCfg) throws URISyntaxException {
        int result = batTransferCfgService.update(batTransferCfg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{taskNo}")
    protected ResultDto<Integer> delete(@PathVariable("taskNo") String taskNo) {
        int result = batTransferCfgService.deleteByPrimaryKey(taskNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batTransferCfgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
