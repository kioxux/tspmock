package cn.com.yusys.yusp.batch.service.server.cmisbatch0004;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004RespDto;
import cn.com.yusys.yusp.batch.service.load.core.BatSCoreKlnbDkzhqgService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class CmisBatch0004Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0004Service.class);
    @Autowired
    private BatSCoreKlnbDkzhqgService batSCoreKlnbDkzhqgService;//核心系统-历史表-贷款账户期供表

    /**
     * 交易码：cmisbatch0004
     * 交易描述：查询[贷款账户主表]和[贷款账户还款表]关联信息
     *
     * @param cmisbatch0004ReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Cmisbatch0004RespDto cmisBatch0004(Cmisbatch0004ReqDto cmisbatch0004ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value);
        Cmisbatch0004RespDto cmisbatch0004RespDto = new Cmisbatch0004RespDto();// 响应Dto:查询[贷款账户主表]和[贷款账户还款表]关联信息
        try {
            //请求字段
            String dkjiejuh = cmisbatch0004ReqDto.getDkjiejuh();//贷款借据号
            String queryType = cmisbatch0004ReqDto.getQueryType();//查询类型
            int countNum = 0;//返回条数
            Map queryMap = new HashMap<>();
            queryMap.put("dkjiejuh", dkjiejuh);
            if (Objects.equals(DscmsEnum.CMISBATCH0004_QUERYTYPE_10.key, queryType)) {
                // 查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>10
                countNum = batSCoreKlnbDkzhqgService.countKlnbDkzhqgByQueryMap10(queryMap);
            } else if (Objects.equals(DscmsEnum.CMISBATCH0004_QUERYTYPE_05.key, queryType)) {
                //查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>5
                countNum = batSCoreKlnbDkzhqgService.countKlnbDkzhqgByQueryMap05(queryMap);
            } else if (Objects.equals(DscmsEnum.CMISBATCH0004_QUERYTYPE_0.key, queryType)) {
                // 无查询条件
                countNum = batSCoreKlnbDkzhqgService.countKlnbDkzhqgByQueryMap0(queryMap);
            }
            cmisbatch0004RespDto.setCountNum(countNum);
            BeanUtils.copyProperties(cmisbatch0004ReqDto, cmisbatch0004RespDto);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value);
        return cmisbatch0004RespDto;
    }
}
