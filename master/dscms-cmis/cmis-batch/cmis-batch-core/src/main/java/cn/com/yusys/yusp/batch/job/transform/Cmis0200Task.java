package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0200Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepLmtEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0200</br>
 * 任务名称：加工任务-额度处理-批前台账比对和将额度相关表备份到额度相关临时表</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0200Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0200Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0200Service cmis0200Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息


    @Bean
    public Job cmis0200Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_JOB.key, JobStepLmtEnum.CMIS0200_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0200Job = this.jobBuilderFactory.get(JobStepLmtEnum.CMIS0200_JOB.key)
                .start(cmis0200UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0200CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(cmis0200DeleteInsertTmpBalanceCompareAccDkStep(WILL_BE_INJECTED))// 清空余额比对差异台账和插入贷款台账余额比对表生成
                .next(cmis0200DeleteInsertTmpBalanceCompareAccYcStep(WILL_BE_INJECTED))// 清空余额比对差异台账和插入银承台账余额比对表生成
                .next(cmis0200DeleteInsertTmpBalanceCompareAccTxStep(WILL_BE_INJECTED))// 清空余额比对差异台账和插入贴现台账余额比对表生成
                .next(cmis0200DeleteInsertTmpBalanceCompareAccXyzStep(WILL_BE_INJECTED))// 清空余额比对差异台账和插入信用证台账余额比对表生成
                .next(cmis0200DeleteInsertTmpBalanceCompareAccBhStep(WILL_BE_INJECTED))// 清空余额比对差异台账和插入保函台账余额比对表生成
                .next(cmis0200DeleteInsertTmpBalanceCompareAccDkbhStep(WILL_BE_INJECTED))// 清空余额比对差异台账和插入代开保函余额比对表生成
                .next(cmis0200DeleteInsertTmpBalanceCompareAccDkxyzStep(WILL_BE_INJECTED))// 清空余额比对差异台账和插入代开信用证余额比对表生成
                .next(cmis0200DeleteInsertTmpBalanceCompareAccFftzStep(WILL_BE_INJECTED))// 清空余额比对差异台账和插入福费廷余额比对表生成
                .next(cmis0200InsertTmpBalanceCompareAccFhtdqzxStep(WILL_BE_INJECTED))
                .next(cmis0200InsertTmpBalanceCompareAccFhtdqzxBStep(WILL_BE_INJECTED))
                .next(cmis0200InsertTmpBalanceCompareAccFhtdqzxCStep(WILL_BE_INJECTED))
                .next(cmis0200InsertTmpBalanceCompareAccFhtdqzxDStep(WILL_BE_INJECTED))
                .next(cmis0200InsertTmpBalanceCompareAccFhtdqzxEStep(WILL_BE_INJECTED))
                .next(cmis0200InsertTmpBalanceCompareAccFhtdqzxFStep(WILL_BE_INJECTED))
                .next(cmis0200InsertTmpBalanceCompareAccFhtdqzxGStep(WILL_BE_INJECTED))
                .next(cmis0200DeleteTmpBalanceCompareAccFhtStep(WILL_BE_INJECTED))
                .next(cmis0200InsertTmpBalanceCompareAccFhtdqzxHStep(WILL_BE_INJECTED))
                .next(cmis0200UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0200Job;
    }


    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_UPDATE_TASK010_STEP.key, JobStepLmtEnum.CMIS0200_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200UpdateTask010Step = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0200_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_UPDATE_TASK010_STEP.key, JobStepLmtEnum.CMIS0200_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0200_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200CheckRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0200_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0200_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0200_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0200CheckRelStep;
    }

    /**
     * 清空余额比对差异台账和插入贷款台账余额比对表生成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200DeleteInsertTmpBalanceCompareAccDkStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DK_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DK_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200DeleteInsertTmpBalanceCompareAccDkStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DK_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200DeleteInsertTmpBalanceCompareAccDk(openDay);// 清空余额比对差异台账和插入贷款台账余额比对表生成
                    cmis0200Service.cmis0200DeleteInsertTmpBalanceCompareAccWtdk(openDay);// 清空余额比对差异台账和插入贷款台账余额比对表生成
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DK_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DK_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200DeleteInsertTmpBalanceCompareAccDkStep;
    }



    /**
     * 清空余额比对差异台账和插入银承台账余额比对表生成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200DeleteInsertTmpBalanceCompareAccYcStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_YC_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_YC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200DeleteInsertTmpBalanceCompareAccYcStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_YC_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200DeleteInsertTmpBalanceCompareAccYc(openDay);// 清空余额比对差异台账和插入银承台账余额比对表生成
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_YC_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_YC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200DeleteInsertTmpBalanceCompareAccYcStep;
    }

    /**
     * 清空余额比对差异台账和插入贴现台账余额比对表生成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200DeleteInsertTmpBalanceCompareAccTxStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_TX_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_TX_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200DeleteInsertTmpBalanceCompareAccTxStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_TX_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200DeleteInsertTmpBalanceCompareAccTx(openDay);// 清空余额比对差异台账和插入贴现台账余额比对表生成
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_TX_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_TX_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200DeleteInsertTmpBalanceCompareAccTxStep;
    }

    /**
     * 清空余额比对差异台账和插入信用证台账余额比对表生成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200DeleteInsertTmpBalanceCompareAccXyzStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_XYZ_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_XYZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200DeleteInsertTmpBalanceCompareAccXyzStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_XYZ_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200DeleteInsertTmpBalanceCompareAccXyz(openDay);//清空余额比对差异台账和插入信用证台账余额比对表生成
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_XYZ_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_XYZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200DeleteInsertTmpBalanceCompareAccXyzStep;
    }

    /**
     * 清空余额比对差异台账和插入保函台账余额比对表生成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200DeleteInsertTmpBalanceCompareAccBhStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_BH_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_BH_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200DeleteInsertTmpBalanceCompareAccBhStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_BH_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200DeleteInsertTmpBalanceCompareAccBh(openDay);//清空余额比对差异台账和插入保函台账余额比对表生成
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_BH_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_BH_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200DeleteInsertTmpBalanceCompareAccBhStep;
    }

    /**
     * 清空余额比对差异台账和插入代开保函余额比对表生成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200DeleteInsertTmpBalanceCompareAccDkbhStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DKBH_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DKBH_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200DeleteInsertTmpBalanceCompareAccDkbhStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DKBH_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200DeleteInsertTmpBalanceCompareAccDkbh(openDay);//清空余额比对差异台账和插入代开保函余额比对表生成
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DKBH_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DKBH_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200DeleteInsertTmpBalanceCompareAccDkbhStep;
    }

    /**
     * 清空余额比对差异台账和插入代开信用证余额比对表生成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200DeleteInsertTmpBalanceCompareAccDkxyzStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DKXYZ_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DKXYZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200DeleteInsertTmpBalanceCompareAccDkxyzStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DKXYZ_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200DeleteInsertTmpBalanceCompareAccDkxyz(openDay);//清空余额比对差异台账和插入代开信用证余额比对表生成
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DKXYZ_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_DKXYZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200DeleteInsertTmpBalanceCompareAccDkxyzStep;
    }

    /**
     * 清空余额比对差异台账和插入福费廷余额比对表生成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200DeleteInsertTmpBalanceCompareAccFftzStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_FFTZ_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_FFTZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200DeleteInsertTmpBalanceCompareAccFftzStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_FFTZ_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200DeleteInsertTmpBalanceCompareAccFftz(openDay);//清空余额比对差异台账和插入福费廷余额比对表生成
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_FFTZ_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_INSERT_TMP_BALANCE_COMPARE_ACC_FFTZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200DeleteInsertTmpBalanceCompareAccFftzStep;
    }


    /**
     * 贷款合同到期注销 插入tmp_balance_compare_acc
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200InsertTmpBalanceCompareAccFhtdqzxStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200InsertTmpBalanceCompareAccFhtdqzxStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200TruncateTmpBalanceCompareAccDkhtdq(openDay);//
                    cmis0200Service.cmis0200InsertTmpBalanceCompareAccFhtdqzx(openDay);//
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200InsertTmpBalanceCompareAccFhtdqzxStep;
    }


    /**
     * 委托贷款合同到期注销 插入tmp_balance_compare_acc B
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200InsertTmpBalanceCompareAccFhtdqzxBStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_B_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_B_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200InsertTmpBalanceCompareAccFhtdqzxBStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200TruncateTmpBalanceCompareAccWtdkhtdq(openDay);//
                    cmis0200Service.cmis0200InsertTmpBalanceCompareAccFhtdqzxB(openDay);//
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_B_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_B_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200InsertTmpBalanceCompareAccFhtdqzxBStep;
    }

    /**
     * 银承合同到期注销 插入tmp_balance_compare_acc C
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200InsertTmpBalanceCompareAccFhtdqzxCStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_C_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_C_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200InsertTmpBalanceCompareAccFhtdqzxCStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200TruncateTmpBalanceCompareAccYchtdq(openDay);
                    cmis0200Service.cmis0200InsertTmpBalanceCompareAccFhtdqzxC(openDay);//
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_C_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_C_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200InsertTmpBalanceCompareAccFhtdqzxCStep;
    }


    /**
     * 贴现合同到期注销 插入tmp_balance_compare_acc D
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200InsertTmpBalanceCompareAccFhtdqzxDStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_D_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_D_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200InsertTmpBalanceCompareAccFhtdqzxDStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200TruncateTmpBalanceCompareAccTxhtdq(openDay);//
                    cmis0200Service.cmis0200InsertTmpBalanceCompareAccFhtdqzxD(openDay);//
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_D_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_D_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200InsertTmpBalanceCompareAccFhtdqzxDStep;
    }


    /**
     * 贴现合同到期注销 插入tmp_balance_compare_acc E
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200InsertTmpBalanceCompareAccFhtdqzxEStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_E_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_E_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200InsertTmpBalanceCompareAccFhtdqzxEStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200TruncateTmpBalanceCompareAccXyzhtdq(openDay);//
                    cmis0200Service.cmis0200InsertTmpBalanceCompareAccFhtdqzxE(openDay);//
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_E_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_E_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200InsertTmpBalanceCompareAccFhtdqzxEStep;
    }

    /**
     * 保函合同到期 注销 插入tmp_balance_compare_acc F
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200InsertTmpBalanceCompareAccFhtdqzxFStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_F_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_F_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200InsertTmpBalanceCompareAccFhtdqzxFStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200TruncateTmpBalanceCompareAccBhhtdq(openDay);//
                    cmis0200Service.cmis0200InsertTmpBalanceCompareAccFhtdqzxF(openDay);//
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_F_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_F_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200InsertTmpBalanceCompareAccFhtdqzxFStep;
    }

    /**
     * 最高额授信协议到期注销 插入tmp_balance_compare_acc G
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200InsertTmpBalanceCompareAccFhtdqzxGStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_G_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_G_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200InsertTmpBalanceCompareAccFhtdqzxGStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200TruncateTmpBalanceCompareAccZgehtdq(openDay);
                    cmis0200Service.cmis0200InsertTmpBalanceCompareAccFhtdqzxG(openDay);//
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_G_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_G_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200InsertTmpBalanceCompareAccFhtdqzxGStep;
    }


    /**
     * 资产池协议到期注销 清空tmp_balance_compare_acc
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200DeleteTmpBalanceCompareAccFhtStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_TMP_BALANCE_COMPARE_ACC_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_TMP_BALANCE_COMPARE_ACC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200InsertTmpBalanceCompareAccFhtdqzxGStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200DeleteTmpBalanceCompareAcc(openDay);//
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_DELETE_TMP_BALANCE_COMPARE_ACC_STEP.key, JobStepLmtEnum.CMIS0200_DELETE_TMP_BALANCE_COMPARE_ACC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200InsertTmpBalanceCompareAccFhtdqzxGStep;
    }


    /**
     * 最高额授信协议到期注销 插入tmp_balance_compare_acc G
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200InsertTmpBalanceCompareAccFhtdqzxHStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_H_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_H_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200InsertTmpBalanceCompareAccFhtdqzxHStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0200Service.cmis0200InsertTmpBalanceCompareAccFhtdqzxH(openDay);//
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_H_STEP.key, JobStepLmtEnum.CMIS0200_INSERT_TMP_BALANCE_COMPARE_ACC_DQZX_H_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200InsertTmpBalanceCompareAccFhtdqzxHStep;
    }


    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0200UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_UPDATE_TASK100_STEP.key, JobStepLmtEnum.CMIS0200_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0200UpdateTask100Step = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0200_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0200_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0200_UPDATE_TASK100_STEP.key, JobStepLmtEnum.CMIS0200_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0200UpdateTask100Step;
    }


}
