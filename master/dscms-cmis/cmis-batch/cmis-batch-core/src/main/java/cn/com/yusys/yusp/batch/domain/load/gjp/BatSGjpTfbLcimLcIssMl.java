/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.gjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGjpTfbLcimLcIssMl
 * @类描述: bat_s_gjp_tfb_lcim_lc_iss_ml数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-03 22:11:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_gjp_tfb_lcim_lc_iss_ml")
public class BatSGjpTfbLcimLcIssMl extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 系统实体编号 **/
	@Column(name = "SYS_ENTY_ID", unique = false, nullable = true, length = 32)
	private String sysEntyId;
	
	/** 系统删除标识 **/
	@Column(name = "SYS_DEL_FLG", unique = false, nullable = true, length = 1)
	private String sysDelFlg;
	
	/** 创建人 **/
	@Column(name = "SYS_CRT_USER", unique = false, nullable = true, length = 32)
	private String sysCrtUser;
	
	/** 创建日期 **/
	@Column(name = "SYS_CRT_DT", unique = false, nullable = true, length = 20)
	private String sysCrtDt;
	
	/** 修改人编号 **/
	@Column(name = "SYS_MODIFY_USER", unique = false, nullable = true, length = 32)
	private String sysModifyUser;
	
	/** 修改日期 **/
	@Column(name = "SYS_MODIFY_DT", unique = false, nullable = true, length = 20)
	private String sysModifyDt;
	
	/** 累计到单金额 **/
	@Column(name = "AB_AMT_TTL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal abAmtTtl;
	
	/** 背批金额 **/
	@Column(name = "AB_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal abBal;
	
	/** 来单次数 **/
	@Column(name = "AB_TIMES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal abTimes;
	
	/** 通知行BIC **/
	@Column(name = "ADV_BK_ID", unique = false, nullable = true, length = 16)
	private String advBkId;
	
	/** 通知行名址 **/
	@Column(name = "ADV_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String advBkNmAddr;
	
	/** 转通知行BIC **/
	@Column(name = "ADV_BK_THR_ID", unique = false, nullable = true, length = 16)
	private String advBkThrId;
	
	/** 转通知行名址 **/
	@Column(name = "ADV_BK_THR_NM_ADDR", unique = false, nullable = true, length = 140)
	private String advBkThrNmAddr;
	
	/** 船名、航次 **/
	@Column(name = "AIR_NO", unique = false, nullable = true, length = 32)
	private String airNo;
	
	/** 修改类型 **/
	@Column(name = "AMD_TYPE", unique = false, nullable = true, length = 8)
	private String amdType;
	
	/** 申请人代码 **/
	@Column(name = "APP_ID", unique = false, nullable = true, length = 32)
	private String appId;
	
	/** 申请人名称 **/
	@Column(name = "APP_NM", unique = false, nullable = true, length = 140)
	private String appNm;
	
	/** 申请人名址 **/
	@Column(name = "APP_NM_ADDR", unique = false, nullable = true, length = 140)
	private String appNmAddr;
	
	/** 即远期标识 **/
	@Column(name = "AT_SIGHT_USANCE_FLG", unique = false, nullable = true, length = 1)
	private String atSightUsanceFlg;
	
	/** 指定银行 **/
	@Column(name = "AVL_BK_FLG", unique = false, nullable = true, length = 1)
	private String avlBkFlg;
	
	/** 兑付银行BIC **/
	@Column(name = "AVL_BK_ID", unique = false, nullable = true, length = 16)
	private String avlBkId;
	
	/** 兑付银行名址 **/
	@Column(name = "AVL_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String avlBkNmAddr;
	
	/** 兑付银行类型 **/
	@Column(name = "AVL_BK_TYPE", unique = false, nullable = true, length = 8)
	private String avlBkType;
	
	/** 兑付方法 **/
	@Column(name = "AVL_BY", unique = false, nullable = true, length = 8)
	private String avlBy;
	
	/** 兑付类型 **/
	@Column(name = "AVL_TYPE", unique = false, nullable = true, length = 128)
	private String avlType;
	
	/** 提单背书笔数 **/
	@Column(name = "BD_TIMES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bdTimes;
	
	/** 受益人所在国别/地区 **/
	@Column(name = "BEN_CNT", unique = false, nullable = true, length = 3)
	private String benCnt;
	
	/** 国别/地区 **/
	@Column(name = "BEN_CNT_CDE", unique = false, nullable = true, length = 3)
	private String benCntCde;
	
	/** 受益人ID **/
	@Column(name = "BEN_ID", unique = false, nullable = true, length = 32)
	private String benId;
	
	/** 受益人名址 **/
	@Column(name = "BEN_NM_ADDR", unique = false, nullable = true, length = 140)
	private String benNmAddr;
	
	/** 业务归属 **/
	@Column(name = "BIZ_BR_CDE", unique = false, nullable = true, length = 8)
	private String bizBrCde;
	
	/** 是FALSE闭卷 **/
	@Column(name = "CLS_FLG", unique = false, nullable = true, length = 8)
	private String clsFlg;
	
	/** 保兑指示 **/
	@Column(name = "CNF_INS", unique = false, nullable = true, length = 8)
	private String cnfIns;
	
	/** 合同号 **/
	@Column(name = "CNTRCT_NO", unique = false, nullable = true, length = 100)
	private String cntrctNo;
	
	/** 跨境人民币标识 **/
	@Column(name = "CROSS_RMB_FLG", unique = false, nullable = true, length = 1)
	private String crossRmbFlg;
	
	/** 延期付款条款 **/
	@Column(name = "DEF_PAY_CLAUSE", unique = false, nullable = true, length = 256)
	private String defPayClause;
	
	/** 需提交副本单据 **/
	@Column(name = "DOC_COPY_FLG", unique = false, nullable = true, length = 1)
	private String docCopyFlg;
	
	/** 汇票金额比例 **/
	@Column(name = "DRAFT_AMT_PCT", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal draftAmtPct;
	
	/** 汇票描述 **/
	@Column(name = "DRAFT_DESC", unique = false, nullable = true, length = 256)
	private String draftDesc;
	
	/** 缮制汇票 **/
	@Column(name = "DRAFT_FLG", unique = false, nullable = true, length = 1)
	private String draftFlg;
	
	/** 汇票付款类型 **/
	@Column(name = "DRAFT_TYPE", unique = false, nullable = true, length = 8)
	private String draftType;
	
	/** 汇票付款人BIC **/
	@Column(name = "DWE_BIC", unique = false, nullable = true, length = 16)
	private String dweBic;
	
	/** 付款人名址 **/
	@Column(name = "DWE_NM_ADDR", unique = false, nullable = true, length = 140)
	private String dweNmAddr;
	
	/** 货物名称 **/
	@Column(name = "GDS", unique = false, nullable = true, length = 128)
	private String gds;
	
	/** H.S.CODE **/
	@Column(name = "GDS_ID", unique = false, nullable = true, length = 128)
	private String gdsId;
	
	/** 开证附言 **/
	@Column(name = "ISS_SWF_72", unique = false, nullable = true, length = 256)
	private String issSwf72;
	
	/** 是FALSE最后到单 **/
	@Column(name = "LAST_AB_FLG", unique = false, nullable = true, length = 1)
	private String lastAbFlg;
	
	/** 最迟装运日 **/
	@Column(name = "LAST_SHPMT_DT", unique = false, nullable = true, length = 8)
	private String lastShpmtDt;
	
	/** 信用证金额 **/
	@Column(name = "LC_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lcAmt;
	
	/** 附加金额 **/
	@Column(name = "LC_AMT_ADT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lcAmtAdt;
	
	/** 最高金额 **/
	@Column(name = "LC_AMT_MAX", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lcAmtMax;
	
	/** 信用证币种 **/
	@Column(name = "LC_CCY", unique = false, nullable = true, length = 3)
	private String lcCcy;
	
	/** 草本需要确认 **/
	@Column(name = "LC_COPY_CNF", unique = false, nullable = true, length = 1)
	private String lcCopyCnf;
	
	/** 开证日期 **/
	@Column(name = "LC_DT", unique = false, nullable = true, length = 8)
	private String lcDt;
	
	/** 信用证效期 **/
	@Column(name = "LC_EXPIRY_DT", unique = false, nullable = true, length = 8)
	private String lcExpiryDt;
	
	/** 信用证有效地点 **/
	@Column(name = "LC_EXPIRY_PLACE", unique = false, nullable = true, length = 256)
	private String lcExpiryPlace;
	
	/** 开证类型 **/
	@Column(name = "LC_FLG", unique = false, nullable = true, length = 8)
	private String lcFlg;
	
	/** 信用证编号 **/
	@Column(name = "LC_NO", unique = false, nullable = true, length = 32)
	private String lcNo;
	
	/** 开证方式 **/
	@Column(name = "LC_TYPE", unique = false, nullable = true, length = 8)
	private String lcType;
	
	/** 快递编号 **/
	@Column(name = "MAIL_NO", unique = false, nullable = true, length = 32)
	private String mailNo;
	
	/** 快邮方式 **/
	@Column(name = "MAIL_TYPE", unique = false, nullable = true, length = 8)
	private String mailType;
	
	/** 混合付款条款 **/
	@Column(name = "MIX_PAY_CLAUSE", unique = false, nullable = true, length = 256)
	private String mixPayClause;
	
	/** 短装比例 **/
	@Column(name = "NEG_TLRNC", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal negTlrnc;
	
	/** 往来银行BIC **/
	@Column(name = "OTH_BK_ID", unique = false, nullable = true, length = 16)
	private String othBkId;
	
	/** 他行信用证号码 **/
	@Column(name = "OTH_BK_LC_NO", unique = false, nullable = true, length = 32)
	private String othBkLcNo;
	
	/** 往来银行名址 **/
	@Column(name = "OTH_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String othBkNmAddr;
	
	/** 其他费用 **/
	@Column(name = "OTH_FEE", unique = false, nullable = true, length = 256)
	private String othFee;
	
	/** 累计已付汇/承兑金额 **/
	@Column(name = "PAY_AMT_TTL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal payAmtTtl;
	
	/** 信用证余额 **/
	@Column(name = "PAY_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal payBal;
	
	/** 卸货港 **/
	@Column(name = "PORT_DISCHG", unique = false, nullable = true, length = 128)
	private String portDischg;
	
	/** 起运港 **/
	@Column(name = "PORT_LOAD", unique = false, nullable = true, length = 128)
	private String portLoad;
	
	/** 溢装比例 **/
	@Column(name = "POS_TLRNC", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal posTlrnc;
	
	/** 产品名称 **/
	@Column(name = "PROD_NM", unique = false, nullable = true, length = 8)
	private String prodNm;
	
	/** 我行在偿付行账号 **/
	@Column(name = "REIM_BK_ACC", unique = false, nullable = true, length = 40)
	private String reimBkAcc;
	
	/** 偿付行费用负担 **/
	@Column(name = "REIM_BK_FEE_BY", unique = false, nullable = true, length = 8)
	private String reimBkFeeBy;
	
	/** 指定偿付行 **/
	@Column(name = "REIM_BK_FLG", unique = false, nullable = true, length = 1)
	private String reimBkFlg;
	
	/** 偿付行BIC **/
	@Column(name = "REIM_BK_ID", unique = false, nullable = true, length = 16)
	private String reimBkId;
	
	/** 偿付行或议付行名址 **/
	@Column(name = "REIM_BK_NM_ADDR", unique = false, nullable = true, length = 140)
	private String reimBkNmAddr;
	
	/** 偿付附言(72) **/
	@Column(name = "REIM_SWF_72", unique = false, nullable = true, length = 256)
	private String reimSwf72;
	
	/** 已办理过提货担保总金额 **/
	@Column(name = "SG_AMT_TTL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal sgAmtTtl;
	
	/** 提货担保次数 **/
	@Column(name = "SG_TIMES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sgTimes;
	
	/** 付款期限 **/
	@Column(name = "TENOR_DAYS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal tenorDays;
	
	/** 溢短装是FALSE支持小数点 **/
	@Column(name = "TLRNC_POINT_FLG", unique = false, nullable = true, length = 1)
	private String tlrncPointFlg;
	
	/** 贸易性质 **/
	@Column(name = "TRADE_PROP", unique = false, nullable = true, length = 8)
	private String tradeProp;
	
	/** 假远期 **/
	@Column(name = "USANCE_AT_SIGHT_FLG", unique = false, nullable = true, length = 1)
	private String usanceAtSightFlg;
	
	/** 假远期加点数 **/
	@Column(name = "USANCE_AT_SIGHT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal usanceAtSightPoint;
	
	/** 我行代开代理行 **/
	@Column(name = "OTH_BK_BIC", unique = false, nullable = true, length = 32)
	private String othBkBic;
	
	/** 修改次数 **/
	@Column(name = "AMD_TIMES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal amdTimes;
	
	/** 修改(更正)总次数 **/
	@Column(name = "AMD_TIMES_TTL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal amdTimesTtl;
	
	/** 装船期 **/
	@Column(name = "SHPMT_DT_ISS", unique = false, nullable = true, length = 390)
	private String shpmtDtIss;
	
	/** 合同币种 **/
	@Column(name = "CNTRCT_CCY", unique = false, nullable = true, length = 3)
	private String cntrctCcy;
	
	/** 合同金额 **/
	@Column(name = "CNTRCT_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal cntrctAmt;
	
	/** 撤证日期 **/
	@Column(name = "CAC_DT", unique = false, nullable = true, length = 8)
	private String cacDt;
	
	/** 是FALSE接受撤证 **/
	@Column(name = "RCAN_FLG", unique = false, nullable = true, length = 8)
	private String rcanFlg;
	
	/** 附言(72) **/
	@Column(name = "SWF_72", unique = false, nullable = true, length = 1024)
	private String swf72;
	
	/** 是FALSE密押关系 **/
	@Column(name = "LC_SIGNK_FLG", unique = false, nullable = true, length = 1)
	private String lcSignkFlg;
	
	/** 是FALSE已经做过来单登记 **/
	@Column(name = "AB_FLG", unique = false, nullable = true, length = 1)
	private String abFlg;
	
	/** 特殊经济区域企业标识 **/
	@Column(name = "SA_FLG", unique = false, nullable = true, length = 1)
	private String saFlg;
	
	/** 提货担保注销标识 **/
	@Column(name = "SG_CLS_FLG", unique = false, nullable = true, length = 1)
	private String sgClsFlg;
	
	/** 预提不符点标识 **/
	@Column(name = "CD_ADV_FLG", unique = false, nullable = true, length = 1)
	private String cdAdvFlg;
	
	/** 是FALSE做过提单背书 **/
	@Column(name = "BD_FLG", unique = false, nullable = true, length = 1)
	private String bdFlg;
	
	/** 客户组织机构代码 **/
	@Column(name = "KERNEL_ORG_CODE", unique = false, nullable = true, length = 32)
	private String kernelOrgCode;
	
	/** 闭卷日期 **/
	@Column(name = "LC_CLS_DT", unique = false, nullable = true, length = 8)
	private String lcClsDt;
	
	/** 通知行业务编号 **/
	@Column(name = "ADV_BK_REF_NO", unique = false, nullable = true, length = 32)
	private String advBkRefNo;
	
	/** 信贷业务编号 **/
	@Column(name = "CMS_REF_NO", unique = false, nullable = true, length = 40)
	private String cmsRefNo;
	
	/** 结束名称 **/
	@Column(name = "ENG_NAME", unique = false, nullable = true, length = 140)
	private String engName;
	
	/** 信用证撤销标识 **/
	@Column(name = "LC_CAC_FLG", unique = false, nullable = true, length = 1)
	private String lcCacFlg;
	
	/** 事件步骤 **/
	@Column(name = "CASE_STEP", unique = false, nullable = true, length = 8)
	private String caseStep;
	
	/** 外汇局备案号码 **/
	@Column(name = "REMARK_NO", unique = false, nullable = true, length = 64)
	private String remarkNo;
	
	/** 未付表外余额 **/
	@Column(name = "UN_PAY_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal unPayBal;
	
	/** 内部更正次数 **/
	@Column(name = "CORR_TIMES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal corrTimes;
	
	/** 兑付银行国别 **/
	@Column(name = "AVL_BEN_CNT", unique = false, nullable = true, length = 140)
	private String avlBenCnt;
	
	/** 系统业务编号 **/
	@Column(name = "SYS_REF_NO", unique = false, nullable = true, length = 32)
	private String sysRefNo;
	
	/** 关联业务编号 **/
	@Column(name = "SYS_REL_NO", unique = false, nullable = true, length = 32)
	private String sysRelNo;
	
	/** 主关联业务编号 **/
	@Column(name = "SYS_MAIN_REL_NO", unique = false, nullable = true, length = 32)
	private String sysMainRelNo;
	
	/** 超额承兑金额 **/
	@Column(name = "ACP_EXP_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal acpExpAmt;
	
	/** 信贷余额 **/
	@Column(name = "CMS_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal cmsBal;
	
	/** 装船、发运、和接受监管的地点(44A) **/
	@Column(name = "SHPMT_PLACE", unique = false, nullable = true, length = 128)
	private String shpmtPlace;
	
	/** 货物发送最终目的地(44B) **/
	@Column(name = "FINAL_DEST", unique = false, nullable = true, length = 128)
	private String finalDest;
	
	/** 适用规则 **/
	@Column(name = "APP_RULES", unique = false, nullable = true, length = 8)
	private String appRules;
	
	/** CLS_REASON **/
	@Column(name = "CLS_REASON", unique = false, nullable = true, length = 1024)
	private String clsReason;
	
	/** ACK_CMS **/
	@Column(name = "ACK_CMS", unique = false, nullable = true, length = 1)
	private String ackCms;
	
	/** LC_FORM **/
	@Column(name = "LC_FORM", unique = false, nullable = true, length = 8)
	private String lcForm;
	
	/** AMD_BY **/
	@Column(name = "AMD_BY", unique = false, nullable = true, length = 8)
	private String amdBy;
	
	/** 单据期限 **/
	@Column(name = "PRESNT_DESC", unique = false, nullable = true, length = 256)
	private String presntDesc;
	
	/** PRESNT_DAYS **/
	@Column(name = "PRESNT_DAYS", unique = false, nullable = true, length = 32)
	private String presntDays;
	
	/** LC_ISS_SELF_FLG **/
	@Column(name = "LC_ISS_SELF_FLG", unique = false, nullable = true, length = 1)
	private String lcIssSelfFlg;
	
	/** AGT_FLG **/
	@Column(name = "AGT_FLG", unique = false, nullable = true, length = 1)
	private String agtFlg;
	
	/** DEDUCT_ACC_NO **/
	@Column(name = "DEDUCT_ACC_NO", unique = false, nullable = true, length = 40)
	private String deductAccNo;
	
	/** LC_EXPIRY_DAYS **/
	@Column(name = "LC_EXPIRY_DAYS", unique = false, nullable = true, length = 10)
	private Integer lcExpiryDays;
	
	/** 风险是否增加 **/
	@Column(name = "RISK_UP", unique = false, nullable = true, length = 1)
	private String riskUp;
	
	/** 即远假转换类型 **/
	@Column(name = "USANCE_AMD_TYPE", unique = false, nullable = true, length = 8)
	private String usanceAmdType;
	
	/** 闭卷前信用证金额 **/
	@Column(name = "OLD_PAY_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal oldPayBal;
	
	/** 委托行行号 **/
	@Column(name = "CONSN_BK_NO", unique = false, nullable = true, length = 32)
	private String consnBkNo;
	
	/** 委托行名称 **/
	@Column(name = "CONSN_BK_NM", unique = false, nullable = true, length = 140)
	private String consnBkNm;
	
	/** 是否允许分批装运 **/
	@Column(name = "PARTL_SHPMT_CLAUSE", unique = false, nullable = true, length = 128)
	private String partlShpmtClause;
	
	/** 是否接受修改 **/
	@Column(name = "RAMD_ACK_FLG", unique = false, nullable = true, length = 8)
	private String ramdAckFlg;
	
	/** 上一次信用证最大金额 **/
	@Column(name = "OLD_LC_AMT_MAX", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal oldLcAmtMax;
	
	/** 受益人名称 **/
	@Column(name = "BEN_NM", unique = false, nullable = true, length = 140)
	private String benNm;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param sysEntyId
	 */
	public void setSysEntyId(String sysEntyId) {
		this.sysEntyId = sysEntyId;
	}
	
    /**
     * @return sysEntyId
     */
	public String getSysEntyId() {
		return this.sysEntyId;
	}
	
	/**
	 * @param sysDelFlg
	 */
	public void setSysDelFlg(String sysDelFlg) {
		this.sysDelFlg = sysDelFlg;
	}
	
    /**
     * @return sysDelFlg
     */
	public String getSysDelFlg() {
		return this.sysDelFlg;
	}
	
	/**
	 * @param sysCrtUser
	 */
	public void setSysCrtUser(String sysCrtUser) {
		this.sysCrtUser = sysCrtUser;
	}
	
    /**
     * @return sysCrtUser
     */
	public String getSysCrtUser() {
		return this.sysCrtUser;
	}
	
	/**
	 * @param sysCrtDt
	 */
	public void setSysCrtDt(String sysCrtDt) {
		this.sysCrtDt = sysCrtDt;
	}
	
    /**
     * @return sysCrtDt
     */
	public String getSysCrtDt() {
		return this.sysCrtDt;
	}
	
	/**
	 * @param sysModifyUser
	 */
	public void setSysModifyUser(String sysModifyUser) {
		this.sysModifyUser = sysModifyUser;
	}
	
    /**
     * @return sysModifyUser
     */
	public String getSysModifyUser() {
		return this.sysModifyUser;
	}
	
	/**
	 * @param sysModifyDt
	 */
	public void setSysModifyDt(String sysModifyDt) {
		this.sysModifyDt = sysModifyDt;
	}
	
    /**
     * @return sysModifyDt
     */
	public String getSysModifyDt() {
		return this.sysModifyDt;
	}
	
	/**
	 * @param abAmtTtl
	 */
	public void setAbAmtTtl(java.math.BigDecimal abAmtTtl) {
		this.abAmtTtl = abAmtTtl;
	}
	
    /**
     * @return abAmtTtl
     */
	public java.math.BigDecimal getAbAmtTtl() {
		return this.abAmtTtl;
	}
	
	/**
	 * @param abBal
	 */
	public void setAbBal(java.math.BigDecimal abBal) {
		this.abBal = abBal;
	}
	
    /**
     * @return abBal
     */
	public java.math.BigDecimal getAbBal() {
		return this.abBal;
	}
	
	/**
	 * @param abTimes
	 */
	public void setAbTimes(java.math.BigDecimal abTimes) {
		this.abTimes = abTimes;
	}
	
    /**
     * @return abTimes
     */
	public java.math.BigDecimal getAbTimes() {
		return this.abTimes;
	}
	
	/**
	 * @param advBkId
	 */
	public void setAdvBkId(String advBkId) {
		this.advBkId = advBkId;
	}
	
    /**
     * @return advBkId
     */
	public String getAdvBkId() {
		return this.advBkId;
	}
	
	/**
	 * @param advBkNmAddr
	 */
	public void setAdvBkNmAddr(String advBkNmAddr) {
		this.advBkNmAddr = advBkNmAddr;
	}
	
    /**
     * @return advBkNmAddr
     */
	public String getAdvBkNmAddr() {
		return this.advBkNmAddr;
	}
	
	/**
	 * @param advBkThrId
	 */
	public void setAdvBkThrId(String advBkThrId) {
		this.advBkThrId = advBkThrId;
	}
	
    /**
     * @return advBkThrId
     */
	public String getAdvBkThrId() {
		return this.advBkThrId;
	}
	
	/**
	 * @param advBkThrNmAddr
	 */
	public void setAdvBkThrNmAddr(String advBkThrNmAddr) {
		this.advBkThrNmAddr = advBkThrNmAddr;
	}
	
    /**
     * @return advBkThrNmAddr
     */
	public String getAdvBkThrNmAddr() {
		return this.advBkThrNmAddr;
	}
	
	/**
	 * @param airNo
	 */
	public void setAirNo(String airNo) {
		this.airNo = airNo;
	}
	
    /**
     * @return airNo
     */
	public String getAirNo() {
		return this.airNo;
	}
	
	/**
	 * @param amdType
	 */
	public void setAmdType(String amdType) {
		this.amdType = amdType;
	}
	
    /**
     * @return amdType
     */
	public String getAmdType() {
		return this.amdType;
	}
	
	/**
	 * @param appId
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
    /**
     * @return appId
     */
	public String getAppId() {
		return this.appId;
	}
	
	/**
	 * @param appNm
	 */
	public void setAppNm(String appNm) {
		this.appNm = appNm;
	}
	
    /**
     * @return appNm
     */
	public String getAppNm() {
		return this.appNm;
	}
	
	/**
	 * @param appNmAddr
	 */
	public void setAppNmAddr(String appNmAddr) {
		this.appNmAddr = appNmAddr;
	}
	
    /**
     * @return appNmAddr
     */
	public String getAppNmAddr() {
		return this.appNmAddr;
	}
	
	/**
	 * @param atSightUsanceFlg
	 */
	public void setAtSightUsanceFlg(String atSightUsanceFlg) {
		this.atSightUsanceFlg = atSightUsanceFlg;
	}
	
    /**
     * @return atSightUsanceFlg
     */
	public String getAtSightUsanceFlg() {
		return this.atSightUsanceFlg;
	}
	
	/**
	 * @param avlBkFlg
	 */
	public void setAvlBkFlg(String avlBkFlg) {
		this.avlBkFlg = avlBkFlg;
	}
	
    /**
     * @return avlBkFlg
     */
	public String getAvlBkFlg() {
		return this.avlBkFlg;
	}
	
	/**
	 * @param avlBkId
	 */
	public void setAvlBkId(String avlBkId) {
		this.avlBkId = avlBkId;
	}
	
    /**
     * @return avlBkId
     */
	public String getAvlBkId() {
		return this.avlBkId;
	}
	
	/**
	 * @param avlBkNmAddr
	 */
	public void setAvlBkNmAddr(String avlBkNmAddr) {
		this.avlBkNmAddr = avlBkNmAddr;
	}
	
    /**
     * @return avlBkNmAddr
     */
	public String getAvlBkNmAddr() {
		return this.avlBkNmAddr;
	}
	
	/**
	 * @param avlBkType
	 */
	public void setAvlBkType(String avlBkType) {
		this.avlBkType = avlBkType;
	}
	
    /**
     * @return avlBkType
     */
	public String getAvlBkType() {
		return this.avlBkType;
	}
	
	/**
	 * @param avlBy
	 */
	public void setAvlBy(String avlBy) {
		this.avlBy = avlBy;
	}
	
    /**
     * @return avlBy
     */
	public String getAvlBy() {
		return this.avlBy;
	}
	
	/**
	 * @param avlType
	 */
	public void setAvlType(String avlType) {
		this.avlType = avlType;
	}
	
    /**
     * @return avlType
     */
	public String getAvlType() {
		return this.avlType;
	}
	
	/**
	 * @param bdTimes
	 */
	public void setBdTimes(java.math.BigDecimal bdTimes) {
		this.bdTimes = bdTimes;
	}
	
    /**
     * @return bdTimes
     */
	public java.math.BigDecimal getBdTimes() {
		return this.bdTimes;
	}
	
	/**
	 * @param benCnt
	 */
	public void setBenCnt(String benCnt) {
		this.benCnt = benCnt;
	}
	
    /**
     * @return benCnt
     */
	public String getBenCnt() {
		return this.benCnt;
	}
	
	/**
	 * @param benCntCde
	 */
	public void setBenCntCde(String benCntCde) {
		this.benCntCde = benCntCde;
	}
	
    /**
     * @return benCntCde
     */
	public String getBenCntCde() {
		return this.benCntCde;
	}
	
	/**
	 * @param benId
	 */
	public void setBenId(String benId) {
		this.benId = benId;
	}
	
    /**
     * @return benId
     */
	public String getBenId() {
		return this.benId;
	}
	
	/**
	 * @param benNmAddr
	 */
	public void setBenNmAddr(String benNmAddr) {
		this.benNmAddr = benNmAddr;
	}
	
    /**
     * @return benNmAddr
     */
	public String getBenNmAddr() {
		return this.benNmAddr;
	}
	
	/**
	 * @param bizBrCde
	 */
	public void setBizBrCde(String bizBrCde) {
		this.bizBrCde = bizBrCde;
	}
	
    /**
     * @return bizBrCde
     */
	public String getBizBrCde() {
		return this.bizBrCde;
	}
	
	/**
	 * @param clsFlg
	 */
	public void setClsFlg(String clsFlg) {
		this.clsFlg = clsFlg;
	}
	
    /**
     * @return clsFlg
     */
	public String getClsFlg() {
		return this.clsFlg;
	}
	
	/**
	 * @param cnfIns
	 */
	public void setCnfIns(String cnfIns) {
		this.cnfIns = cnfIns;
	}
	
    /**
     * @return cnfIns
     */
	public String getCnfIns() {
		return this.cnfIns;
	}
	
	/**
	 * @param cntrctNo
	 */
	public void setCntrctNo(String cntrctNo) {
		this.cntrctNo = cntrctNo;
	}
	
    /**
     * @return cntrctNo
     */
	public String getCntrctNo() {
		return this.cntrctNo;
	}
	
	/**
	 * @param crossRmbFlg
	 */
	public void setCrossRmbFlg(String crossRmbFlg) {
		this.crossRmbFlg = crossRmbFlg;
	}
	
    /**
     * @return crossRmbFlg
     */
	public String getCrossRmbFlg() {
		return this.crossRmbFlg;
	}
	
	/**
	 * @param defPayClause
	 */
	public void setDefPayClause(String defPayClause) {
		this.defPayClause = defPayClause;
	}
	
    /**
     * @return defPayClause
     */
	public String getDefPayClause() {
		return this.defPayClause;
	}
	
	/**
	 * @param docCopyFlg
	 */
	public void setDocCopyFlg(String docCopyFlg) {
		this.docCopyFlg = docCopyFlg;
	}
	
    /**
     * @return docCopyFlg
     */
	public String getDocCopyFlg() {
		return this.docCopyFlg;
	}
	
	/**
	 * @param draftAmtPct
	 */
	public void setDraftAmtPct(java.math.BigDecimal draftAmtPct) {
		this.draftAmtPct = draftAmtPct;
	}
	
    /**
     * @return draftAmtPct
     */
	public java.math.BigDecimal getDraftAmtPct() {
		return this.draftAmtPct;
	}
	
	/**
	 * @param draftDesc
	 */
	public void setDraftDesc(String draftDesc) {
		this.draftDesc = draftDesc;
	}
	
    /**
     * @return draftDesc
     */
	public String getDraftDesc() {
		return this.draftDesc;
	}
	
	/**
	 * @param draftFlg
	 */
	public void setDraftFlg(String draftFlg) {
		this.draftFlg = draftFlg;
	}
	
    /**
     * @return draftFlg
     */
	public String getDraftFlg() {
		return this.draftFlg;
	}
	
	/**
	 * @param draftType
	 */
	public void setDraftType(String draftType) {
		this.draftType = draftType;
	}
	
    /**
     * @return draftType
     */
	public String getDraftType() {
		return this.draftType;
	}
	
	/**
	 * @param dweBic
	 */
	public void setDweBic(String dweBic) {
		this.dweBic = dweBic;
	}
	
    /**
     * @return dweBic
     */
	public String getDweBic() {
		return this.dweBic;
	}
	
	/**
	 * @param dweNmAddr
	 */
	public void setDweNmAddr(String dweNmAddr) {
		this.dweNmAddr = dweNmAddr;
	}
	
    /**
     * @return dweNmAddr
     */
	public String getDweNmAddr() {
		return this.dweNmAddr;
	}
	
	/**
	 * @param gds
	 */
	public void setGds(String gds) {
		this.gds = gds;
	}
	
    /**
     * @return gds
     */
	public String getGds() {
		return this.gds;
	}
	
	/**
	 * @param gdsId
	 */
	public void setGdsId(String gdsId) {
		this.gdsId = gdsId;
	}
	
    /**
     * @return gdsId
     */
	public String getGdsId() {
		return this.gdsId;
	}
	
	/**
	 * @param issSwf72
	 */
	public void setIssSwf72(String issSwf72) {
		this.issSwf72 = issSwf72;
	}
	
    /**
     * @return issSwf72
     */
	public String getIssSwf72() {
		return this.issSwf72;
	}
	
	/**
	 * @param lastAbFlg
	 */
	public void setLastAbFlg(String lastAbFlg) {
		this.lastAbFlg = lastAbFlg;
	}
	
    /**
     * @return lastAbFlg
     */
	public String getLastAbFlg() {
		return this.lastAbFlg;
	}
	
	/**
	 * @param lastShpmtDt
	 */
	public void setLastShpmtDt(String lastShpmtDt) {
		this.lastShpmtDt = lastShpmtDt;
	}
	
    /**
     * @return lastShpmtDt
     */
	public String getLastShpmtDt() {
		return this.lastShpmtDt;
	}
	
	/**
	 * @param lcAmt
	 */
	public void setLcAmt(java.math.BigDecimal lcAmt) {
		this.lcAmt = lcAmt;
	}
	
    /**
     * @return lcAmt
     */
	public java.math.BigDecimal getLcAmt() {
		return this.lcAmt;
	}
	
	/**
	 * @param lcAmtAdt
	 */
	public void setLcAmtAdt(java.math.BigDecimal lcAmtAdt) {
		this.lcAmtAdt = lcAmtAdt;
	}
	
    /**
     * @return lcAmtAdt
     */
	public java.math.BigDecimal getLcAmtAdt() {
		return this.lcAmtAdt;
	}
	
	/**
	 * @param lcAmtMax
	 */
	public void setLcAmtMax(java.math.BigDecimal lcAmtMax) {
		this.lcAmtMax = lcAmtMax;
	}
	
    /**
     * @return lcAmtMax
     */
	public java.math.BigDecimal getLcAmtMax() {
		return this.lcAmtMax;
	}
	
	/**
	 * @param lcCcy
	 */
	public void setLcCcy(String lcCcy) {
		this.lcCcy = lcCcy;
	}
	
    /**
     * @return lcCcy
     */
	public String getLcCcy() {
		return this.lcCcy;
	}
	
	/**
	 * @param lcCopyCnf
	 */
	public void setLcCopyCnf(String lcCopyCnf) {
		this.lcCopyCnf = lcCopyCnf;
	}
	
    /**
     * @return lcCopyCnf
     */
	public String getLcCopyCnf() {
		return this.lcCopyCnf;
	}
	
	/**
	 * @param lcDt
	 */
	public void setLcDt(String lcDt) {
		this.lcDt = lcDt;
	}
	
    /**
     * @return lcDt
     */
	public String getLcDt() {
		return this.lcDt;
	}
	
	/**
	 * @param lcExpiryDt
	 */
	public void setLcExpiryDt(String lcExpiryDt) {
		this.lcExpiryDt = lcExpiryDt;
	}
	
    /**
     * @return lcExpiryDt
     */
	public String getLcExpiryDt() {
		return this.lcExpiryDt;
	}
	
	/**
	 * @param lcExpiryPlace
	 */
	public void setLcExpiryPlace(String lcExpiryPlace) {
		this.lcExpiryPlace = lcExpiryPlace;
	}
	
    /**
     * @return lcExpiryPlace
     */
	public String getLcExpiryPlace() {
		return this.lcExpiryPlace;
	}
	
	/**
	 * @param lcFlg
	 */
	public void setLcFlg(String lcFlg) {
		this.lcFlg = lcFlg;
	}
	
    /**
     * @return lcFlg
     */
	public String getLcFlg() {
		return this.lcFlg;
	}
	
	/**
	 * @param lcNo
	 */
	public void setLcNo(String lcNo) {
		this.lcNo = lcNo;
	}
	
    /**
     * @return lcNo
     */
	public String getLcNo() {
		return this.lcNo;
	}
	
	/**
	 * @param lcType
	 */
	public void setLcType(String lcType) {
		this.lcType = lcType;
	}
	
    /**
     * @return lcType
     */
	public String getLcType() {
		return this.lcType;
	}
	
	/**
	 * @param mailNo
	 */
	public void setMailNo(String mailNo) {
		this.mailNo = mailNo;
	}
	
    /**
     * @return mailNo
     */
	public String getMailNo() {
		return this.mailNo;
	}
	
	/**
	 * @param mailType
	 */
	public void setMailType(String mailType) {
		this.mailType = mailType;
	}
	
    /**
     * @return mailType
     */
	public String getMailType() {
		return this.mailType;
	}
	
	/**
	 * @param mixPayClause
	 */
	public void setMixPayClause(String mixPayClause) {
		this.mixPayClause = mixPayClause;
	}
	
    /**
     * @return mixPayClause
     */
	public String getMixPayClause() {
		return this.mixPayClause;
	}
	
	/**
	 * @param negTlrnc
	 */
	public void setNegTlrnc(java.math.BigDecimal negTlrnc) {
		this.negTlrnc = negTlrnc;
	}
	
    /**
     * @return negTlrnc
     */
	public java.math.BigDecimal getNegTlrnc() {
		return this.negTlrnc;
	}
	
	/**
	 * @param othBkId
	 */
	public void setOthBkId(String othBkId) {
		this.othBkId = othBkId;
	}
	
    /**
     * @return othBkId
     */
	public String getOthBkId() {
		return this.othBkId;
	}
	
	/**
	 * @param othBkLcNo
	 */
	public void setOthBkLcNo(String othBkLcNo) {
		this.othBkLcNo = othBkLcNo;
	}
	
    /**
     * @return othBkLcNo
     */
	public String getOthBkLcNo() {
		return this.othBkLcNo;
	}
	
	/**
	 * @param othBkNmAddr
	 */
	public void setOthBkNmAddr(String othBkNmAddr) {
		this.othBkNmAddr = othBkNmAddr;
	}
	
    /**
     * @return othBkNmAddr
     */
	public String getOthBkNmAddr() {
		return this.othBkNmAddr;
	}
	
	/**
	 * @param othFee
	 */
	public void setOthFee(String othFee) {
		this.othFee = othFee;
	}
	
    /**
     * @return othFee
     */
	public String getOthFee() {
		return this.othFee;
	}
	
	/**
	 * @param payAmtTtl
	 */
	public void setPayAmtTtl(java.math.BigDecimal payAmtTtl) {
		this.payAmtTtl = payAmtTtl;
	}
	
    /**
     * @return payAmtTtl
     */
	public java.math.BigDecimal getPayAmtTtl() {
		return this.payAmtTtl;
	}
	
	/**
	 * @param payBal
	 */
	public void setPayBal(java.math.BigDecimal payBal) {
		this.payBal = payBal;
	}
	
    /**
     * @return payBal
     */
	public java.math.BigDecimal getPayBal() {
		return this.payBal;
	}
	
	/**
	 * @param portDischg
	 */
	public void setPortDischg(String portDischg) {
		this.portDischg = portDischg;
	}
	
    /**
     * @return portDischg
     */
	public String getPortDischg() {
		return this.portDischg;
	}
	
	/**
	 * @param portLoad
	 */
	public void setPortLoad(String portLoad) {
		this.portLoad = portLoad;
	}
	
    /**
     * @return portLoad
     */
	public String getPortLoad() {
		return this.portLoad;
	}
	
	/**
	 * @param posTlrnc
	 */
	public void setPosTlrnc(java.math.BigDecimal posTlrnc) {
		this.posTlrnc = posTlrnc;
	}
	
    /**
     * @return posTlrnc
     */
	public java.math.BigDecimal getPosTlrnc() {
		return this.posTlrnc;
	}
	
	/**
	 * @param prodNm
	 */
	public void setProdNm(String prodNm) {
		this.prodNm = prodNm;
	}
	
    /**
     * @return prodNm
     */
	public String getProdNm() {
		return this.prodNm;
	}
	
	/**
	 * @param reimBkAcc
	 */
	public void setReimBkAcc(String reimBkAcc) {
		this.reimBkAcc = reimBkAcc;
	}
	
    /**
     * @return reimBkAcc
     */
	public String getReimBkAcc() {
		return this.reimBkAcc;
	}
	
	/**
	 * @param reimBkFeeBy
	 */
	public void setReimBkFeeBy(String reimBkFeeBy) {
		this.reimBkFeeBy = reimBkFeeBy;
	}
	
    /**
     * @return reimBkFeeBy
     */
	public String getReimBkFeeBy() {
		return this.reimBkFeeBy;
	}
	
	/**
	 * @param reimBkFlg
	 */
	public void setReimBkFlg(String reimBkFlg) {
		this.reimBkFlg = reimBkFlg;
	}
	
    /**
     * @return reimBkFlg
     */
	public String getReimBkFlg() {
		return this.reimBkFlg;
	}
	
	/**
	 * @param reimBkId
	 */
	public void setReimBkId(String reimBkId) {
		this.reimBkId = reimBkId;
	}
	
    /**
     * @return reimBkId
     */
	public String getReimBkId() {
		return this.reimBkId;
	}
	
	/**
	 * @param reimBkNmAddr
	 */
	public void setReimBkNmAddr(String reimBkNmAddr) {
		this.reimBkNmAddr = reimBkNmAddr;
	}
	
    /**
     * @return reimBkNmAddr
     */
	public String getReimBkNmAddr() {
		return this.reimBkNmAddr;
	}
	
	/**
	 * @param reimSwf72
	 */
	public void setReimSwf72(String reimSwf72) {
		this.reimSwf72 = reimSwf72;
	}
	
    /**
     * @return reimSwf72
     */
	public String getReimSwf72() {
		return this.reimSwf72;
	}
	
	/**
	 * @param sgAmtTtl
	 */
	public void setSgAmtTtl(java.math.BigDecimal sgAmtTtl) {
		this.sgAmtTtl = sgAmtTtl;
	}
	
    /**
     * @return sgAmtTtl
     */
	public java.math.BigDecimal getSgAmtTtl() {
		return this.sgAmtTtl;
	}
	
	/**
	 * @param sgTimes
	 */
	public void setSgTimes(java.math.BigDecimal sgTimes) {
		this.sgTimes = sgTimes;
	}
	
    /**
     * @return sgTimes
     */
	public java.math.BigDecimal getSgTimes() {
		return this.sgTimes;
	}
	
	/**
	 * @param tenorDays
	 */
	public void setTenorDays(java.math.BigDecimal tenorDays) {
		this.tenorDays = tenorDays;
	}
	
    /**
     * @return tenorDays
     */
	public java.math.BigDecimal getTenorDays() {
		return this.tenorDays;
	}
	
	/**
	 * @param tlrncPointFlg
	 */
	public void setTlrncPointFlg(String tlrncPointFlg) {
		this.tlrncPointFlg = tlrncPointFlg;
	}
	
    /**
     * @return tlrncPointFlg
     */
	public String getTlrncPointFlg() {
		return this.tlrncPointFlg;
	}
	
	/**
	 * @param tradeProp
	 */
	public void setTradeProp(String tradeProp) {
		this.tradeProp = tradeProp;
	}
	
    /**
     * @return tradeProp
     */
	public String getTradeProp() {
		return this.tradeProp;
	}
	
	/**
	 * @param usanceAtSightFlg
	 */
	public void setUsanceAtSightFlg(String usanceAtSightFlg) {
		this.usanceAtSightFlg = usanceAtSightFlg;
	}
	
    /**
     * @return usanceAtSightFlg
     */
	public String getUsanceAtSightFlg() {
		return this.usanceAtSightFlg;
	}
	
	/**
	 * @param usanceAtSightPoint
	 */
	public void setUsanceAtSightPoint(java.math.BigDecimal usanceAtSightPoint) {
		this.usanceAtSightPoint = usanceAtSightPoint;
	}
	
    /**
     * @return usanceAtSightPoint
     */
	public java.math.BigDecimal getUsanceAtSightPoint() {
		return this.usanceAtSightPoint;
	}
	
	/**
	 * @param othBkBic
	 */
	public void setOthBkBic(String othBkBic) {
		this.othBkBic = othBkBic;
	}
	
    /**
     * @return othBkBic
     */
	public String getOthBkBic() {
		return this.othBkBic;
	}
	
	/**
	 * @param amdTimes
	 */
	public void setAmdTimes(java.math.BigDecimal amdTimes) {
		this.amdTimes = amdTimes;
	}
	
    /**
     * @return amdTimes
     */
	public java.math.BigDecimal getAmdTimes() {
		return this.amdTimes;
	}
	
	/**
	 * @param amdTimesTtl
	 */
	public void setAmdTimesTtl(java.math.BigDecimal amdTimesTtl) {
		this.amdTimesTtl = amdTimesTtl;
	}
	
    /**
     * @return amdTimesTtl
     */
	public java.math.BigDecimal getAmdTimesTtl() {
		return this.amdTimesTtl;
	}
	
	/**
	 * @param shpmtDtIss
	 */
	public void setShpmtDtIss(String shpmtDtIss) {
		this.shpmtDtIss = shpmtDtIss;
	}
	
    /**
     * @return shpmtDtIss
     */
	public String getShpmtDtIss() {
		return this.shpmtDtIss;
	}
	
	/**
	 * @param cntrctCcy
	 */
	public void setCntrctCcy(String cntrctCcy) {
		this.cntrctCcy = cntrctCcy;
	}
	
    /**
     * @return cntrctCcy
     */
	public String getCntrctCcy() {
		return this.cntrctCcy;
	}
	
	/**
	 * @param cntrctAmt
	 */
	public void setCntrctAmt(java.math.BigDecimal cntrctAmt) {
		this.cntrctAmt = cntrctAmt;
	}
	
    /**
     * @return cntrctAmt
     */
	public java.math.BigDecimal getCntrctAmt() {
		return this.cntrctAmt;
	}
	
	/**
	 * @param cacDt
	 */
	public void setCacDt(String cacDt) {
		this.cacDt = cacDt;
	}
	
    /**
     * @return cacDt
     */
	public String getCacDt() {
		return this.cacDt;
	}
	
	/**
	 * @param rcanFlg
	 */
	public void setRcanFlg(String rcanFlg) {
		this.rcanFlg = rcanFlg;
	}
	
    /**
     * @return rcanFlg
     */
	public String getRcanFlg() {
		return this.rcanFlg;
	}
	
	/**
	 * @param swf72
	 */
	public void setSwf72(String swf72) {
		this.swf72 = swf72;
	}
	
    /**
     * @return swf72
     */
	public String getSwf72() {
		return this.swf72;
	}
	
	/**
	 * @param lcSignkFlg
	 */
	public void setLcSignkFlg(String lcSignkFlg) {
		this.lcSignkFlg = lcSignkFlg;
	}
	
    /**
     * @return lcSignkFlg
     */
	public String getLcSignkFlg() {
		return this.lcSignkFlg;
	}
	
	/**
	 * @param abFlg
	 */
	public void setAbFlg(String abFlg) {
		this.abFlg = abFlg;
	}
	
    /**
     * @return abFlg
     */
	public String getAbFlg() {
		return this.abFlg;
	}
	
	/**
	 * @param saFlg
	 */
	public void setSaFlg(String saFlg) {
		this.saFlg = saFlg;
	}
	
    /**
     * @return saFlg
     */
	public String getSaFlg() {
		return this.saFlg;
	}
	
	/**
	 * @param sgClsFlg
	 */
	public void setSgClsFlg(String sgClsFlg) {
		this.sgClsFlg = sgClsFlg;
	}
	
    /**
     * @return sgClsFlg
     */
	public String getSgClsFlg() {
		return this.sgClsFlg;
	}
	
	/**
	 * @param cdAdvFlg
	 */
	public void setCdAdvFlg(String cdAdvFlg) {
		this.cdAdvFlg = cdAdvFlg;
	}
	
    /**
     * @return cdAdvFlg
     */
	public String getCdAdvFlg() {
		return this.cdAdvFlg;
	}
	
	/**
	 * @param bdFlg
	 */
	public void setBdFlg(String bdFlg) {
		this.bdFlg = bdFlg;
	}
	
    /**
     * @return bdFlg
     */
	public String getBdFlg() {
		return this.bdFlg;
	}
	
	/**
	 * @param kernelOrgCode
	 */
	public void setKernelOrgCode(String kernelOrgCode) {
		this.kernelOrgCode = kernelOrgCode;
	}
	
    /**
     * @return kernelOrgCode
     */
	public String getKernelOrgCode() {
		return this.kernelOrgCode;
	}
	
	/**
	 * @param lcClsDt
	 */
	public void setLcClsDt(String lcClsDt) {
		this.lcClsDt = lcClsDt;
	}
	
    /**
     * @return lcClsDt
     */
	public String getLcClsDt() {
		return this.lcClsDt;
	}
	
	/**
	 * @param advBkRefNo
	 */
	public void setAdvBkRefNo(String advBkRefNo) {
		this.advBkRefNo = advBkRefNo;
	}
	
    /**
     * @return advBkRefNo
     */
	public String getAdvBkRefNo() {
		return this.advBkRefNo;
	}
	
	/**
	 * @param cmsRefNo
	 */
	public void setCmsRefNo(String cmsRefNo) {
		this.cmsRefNo = cmsRefNo;
	}
	
    /**
     * @return cmsRefNo
     */
	public String getCmsRefNo() {
		return this.cmsRefNo;
	}
	
	/**
	 * @param engName
	 */
	public void setEngName(String engName) {
		this.engName = engName;
	}
	
    /**
     * @return engName
     */
	public String getEngName() {
		return this.engName;
	}
	
	/**
	 * @param lcCacFlg
	 */
	public void setLcCacFlg(String lcCacFlg) {
		this.lcCacFlg = lcCacFlg;
	}
	
    /**
     * @return lcCacFlg
     */
	public String getLcCacFlg() {
		return this.lcCacFlg;
	}
	
	/**
	 * @param caseStep
	 */
	public void setCaseStep(String caseStep) {
		this.caseStep = caseStep;
	}
	
    /**
     * @return caseStep
     */
	public String getCaseStep() {
		return this.caseStep;
	}
	
	/**
	 * @param remarkNo
	 */
	public void setRemarkNo(String remarkNo) {
		this.remarkNo = remarkNo;
	}
	
    /**
     * @return remarkNo
     */
	public String getRemarkNo() {
		return this.remarkNo;
	}
	
	/**
	 * @param unPayBal
	 */
	public void setUnPayBal(java.math.BigDecimal unPayBal) {
		this.unPayBal = unPayBal;
	}
	
    /**
     * @return unPayBal
     */
	public java.math.BigDecimal getUnPayBal() {
		return this.unPayBal;
	}
	
	/**
	 * @param corrTimes
	 */
	public void setCorrTimes(java.math.BigDecimal corrTimes) {
		this.corrTimes = corrTimes;
	}
	
    /**
     * @return corrTimes
     */
	public java.math.BigDecimal getCorrTimes() {
		return this.corrTimes;
	}
	
	/**
	 * @param avlBenCnt
	 */
	public void setAvlBenCnt(String avlBenCnt) {
		this.avlBenCnt = avlBenCnt;
	}
	
    /**
     * @return avlBenCnt
     */
	public String getAvlBenCnt() {
		return this.avlBenCnt;
	}
	
	/**
	 * @param sysRefNo
	 */
	public void setSysRefNo(String sysRefNo) {
		this.sysRefNo = sysRefNo;
	}
	
    /**
     * @return sysRefNo
     */
	public String getSysRefNo() {
		return this.sysRefNo;
	}
	
	/**
	 * @param sysRelNo
	 */
	public void setSysRelNo(String sysRelNo) {
		this.sysRelNo = sysRelNo;
	}
	
    /**
     * @return sysRelNo
     */
	public String getSysRelNo() {
		return this.sysRelNo;
	}
	
	/**
	 * @param sysMainRelNo
	 */
	public void setSysMainRelNo(String sysMainRelNo) {
		this.sysMainRelNo = sysMainRelNo;
	}
	
    /**
     * @return sysMainRelNo
     */
	public String getSysMainRelNo() {
		return this.sysMainRelNo;
	}
	
	/**
	 * @param acpExpAmt
	 */
	public void setAcpExpAmt(java.math.BigDecimal acpExpAmt) {
		this.acpExpAmt = acpExpAmt;
	}
	
    /**
     * @return acpExpAmt
     */
	public java.math.BigDecimal getAcpExpAmt() {
		return this.acpExpAmt;
	}
	
	/**
	 * @param cmsBal
	 */
	public void setCmsBal(java.math.BigDecimal cmsBal) {
		this.cmsBal = cmsBal;
	}
	
    /**
     * @return cmsBal
     */
	public java.math.BigDecimal getCmsBal() {
		return this.cmsBal;
	}
	
	/**
	 * @param shpmtPlace
	 */
	public void setShpmtPlace(String shpmtPlace) {
		this.shpmtPlace = shpmtPlace;
	}
	
    /**
     * @return shpmtPlace
     */
	public String getShpmtPlace() {
		return this.shpmtPlace;
	}
	
	/**
	 * @param finalDest
	 */
	public void setFinalDest(String finalDest) {
		this.finalDest = finalDest;
	}
	
    /**
     * @return finalDest
     */
	public String getFinalDest() {
		return this.finalDest;
	}
	
	/**
	 * @param appRules
	 */
	public void setAppRules(String appRules) {
		this.appRules = appRules;
	}
	
    /**
     * @return appRules
     */
	public String getAppRules() {
		return this.appRules;
	}
	
	/**
	 * @param clsReason
	 */
	public void setClsReason(String clsReason) {
		this.clsReason = clsReason;
	}
	
    /**
     * @return clsReason
     */
	public String getClsReason() {
		return this.clsReason;
	}
	
	/**
	 * @param ackCms
	 */
	public void setAckCms(String ackCms) {
		this.ackCms = ackCms;
	}
	
    /**
     * @return ackCms
     */
	public String getAckCms() {
		return this.ackCms;
	}
	
	/**
	 * @param lcForm
	 */
	public void setLcForm(String lcForm) {
		this.lcForm = lcForm;
	}
	
    /**
     * @return lcForm
     */
	public String getLcForm() {
		return this.lcForm;
	}
	
	/**
	 * @param amdBy
	 */
	public void setAmdBy(String amdBy) {
		this.amdBy = amdBy;
	}
	
    /**
     * @return amdBy
     */
	public String getAmdBy() {
		return this.amdBy;
	}
	
	/**
	 * @param presntDesc
	 */
	public void setPresntDesc(String presntDesc) {
		this.presntDesc = presntDesc;
	}
	
    /**
     * @return presntDesc
     */
	public String getPresntDesc() {
		return this.presntDesc;
	}
	
	/**
	 * @param presntDays
	 */
	public void setPresntDays(String presntDays) {
		this.presntDays = presntDays;
	}
	
    /**
     * @return presntDays
     */
	public String getPresntDays() {
		return this.presntDays;
	}
	
	/**
	 * @param lcIssSelfFlg
	 */
	public void setLcIssSelfFlg(String lcIssSelfFlg) {
		this.lcIssSelfFlg = lcIssSelfFlg;
	}
	
    /**
     * @return lcIssSelfFlg
     */
	public String getLcIssSelfFlg() {
		return this.lcIssSelfFlg;
	}
	
	/**
	 * @param agtFlg
	 */
	public void setAgtFlg(String agtFlg) {
		this.agtFlg = agtFlg;
	}
	
    /**
     * @return agtFlg
     */
	public String getAgtFlg() {
		return this.agtFlg;
	}
	
	/**
	 * @param deductAccNo
	 */
	public void setDeductAccNo(String deductAccNo) {
		this.deductAccNo = deductAccNo;
	}
	
    /**
     * @return deductAccNo
     */
	public String getDeductAccNo() {
		return this.deductAccNo;
	}
	
	/**
	 * @param lcExpiryDays
	 */
	public void setLcExpiryDays(Integer lcExpiryDays) {
		this.lcExpiryDays = lcExpiryDays;
	}
	
    /**
     * @return lcExpiryDays
     */
	public Integer getLcExpiryDays() {
		return this.lcExpiryDays;
	}
	
	/**
	 * @param riskUp
	 */
	public void setRiskUp(String riskUp) {
		this.riskUp = riskUp;
	}
	
    /**
     * @return riskUp
     */
	public String getRiskUp() {
		return this.riskUp;
	}
	
	/**
	 * @param usanceAmdType
	 */
	public void setUsanceAmdType(String usanceAmdType) {
		this.usanceAmdType = usanceAmdType;
	}
	
    /**
     * @return usanceAmdType
     */
	public String getUsanceAmdType() {
		return this.usanceAmdType;
	}
	
	/**
	 * @param oldPayBal
	 */
	public void setOldPayBal(java.math.BigDecimal oldPayBal) {
		this.oldPayBal = oldPayBal;
	}
	
    /**
     * @return oldPayBal
     */
	public java.math.BigDecimal getOldPayBal() {
		return this.oldPayBal;
	}
	
	/**
	 * @param consnBkNo
	 */
	public void setConsnBkNo(String consnBkNo) {
		this.consnBkNo = consnBkNo;
	}
	
    /**
     * @return consnBkNo
     */
	public String getConsnBkNo() {
		return this.consnBkNo;
	}
	
	/**
	 * @param consnBkNm
	 */
	public void setConsnBkNm(String consnBkNm) {
		this.consnBkNm = consnBkNm;
	}
	
    /**
     * @return consnBkNm
     */
	public String getConsnBkNm() {
		return this.consnBkNm;
	}
	
	/**
	 * @param partlShpmtClause
	 */
	public void setPartlShpmtClause(String partlShpmtClause) {
		this.partlShpmtClause = partlShpmtClause;
	}
	
    /**
     * @return partlShpmtClause
     */
	public String getPartlShpmtClause() {
		return this.partlShpmtClause;
	}
	
	/**
	 * @param ramdAckFlg
	 */
	public void setRamdAckFlg(String ramdAckFlg) {
		this.ramdAckFlg = ramdAckFlg;
	}
	
    /**
     * @return ramdAckFlg
     */
	public String getRamdAckFlg() {
		return this.ramdAckFlg;
	}
	
	/**
	 * @param oldLcAmtMax
	 */
	public void setOldLcAmtMax(java.math.BigDecimal oldLcAmtMax) {
		this.oldLcAmtMax = oldLcAmtMax;
	}
	
    /**
     * @return oldLcAmtMax
     */
	public java.math.BigDecimal getOldLcAmtMax() {
		return this.oldLcAmtMax;
	}
	
	/**
	 * @param benNm
	 */
	public void setBenNm(String benNm) {
		this.benNm = benNm;
	}
	
    /**
     * @return benNm
     */
	public String getBenNm() {
		return this.benNm;
	}


}