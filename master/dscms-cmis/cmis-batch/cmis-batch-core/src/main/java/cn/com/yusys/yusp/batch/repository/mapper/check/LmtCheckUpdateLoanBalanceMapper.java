/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.check;

import org.apache.ibatis.annotations.Param;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateLoanBalanceMapper
 * @类描述: #Dao类
 * @功能描述: 数据更新额度校正-计算分项用信金额
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCheckUpdateLoanBalanceMapper {

    /**
     * 1、计算分项的用信金额
     */
    int calculateSubLmtAmt1(@Param("cusId") String cusId);

    int calculateSubLmtAmt2(@Param("cusId") String cusId);

    int calculateSubLmtAmt3(@Param("cusId") String cusId);

    int calculateSubLmtAmt4(@Param("cusId") String cusId);


    /**
     * 2、一级分项用信余额汇总至二级分项
     *  二级分项项下的用信台账余额之和 + 分项项下关联的非最高额授信协议的合同下的用信台账余额之和 + 分项项下关联的用信台账余额之和
     */
    int collect1LevelSubLmtAmtTo2LevelSub1(@Param("cusId") String cusId);

    int collect1LevelSubLmtAmtTo2LevelSub2(@Param("cusId") String cusId);

    int collect1LevelSubLmtAmtTo2LevelSub3(@Param("cusId") String cusId);

    int collect1LevelSubLmtAmtTo2LevelSub4(@Param("cusId") String cusId);

}