/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.ods;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpRptFiveClassifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.ods.BatSOdsFtoYwtxDkyeD;
import cn.com.yusys.yusp.batch.repository.mapper.load.ods.BatSOdsFtoYwtxDkyeDMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSOdsFtoYwtxDkyeDService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-25 16:35:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSOdsFtoYwtxDkyeDService {
    private static final Logger logger = LoggerFactory.getLogger(BatSRcpRptFiveClassifyService.class);
    @Autowired
    private TableUtilsService tableUtilsService;
    @Autowired
    private BatSOdsFtoYwtxDkyeDMapper batSOdsFtoYwtxDkyeDMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatSOdsFtoYwtxDkyeD selectByPrimaryKey(String acctdt, String brchno, String stline, java.math.BigDecimal loanbl) {
        return batSOdsFtoYwtxDkyeDMapper.selectByPrimaryKey(acctdt, brchno, stline, loanbl);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatSOdsFtoYwtxDkyeD> selectAll(QueryModel model) {
        List<BatSOdsFtoYwtxDkyeD> records = (List<BatSOdsFtoYwtxDkyeD>) batSOdsFtoYwtxDkyeDMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatSOdsFtoYwtxDkyeD> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSOdsFtoYwtxDkyeD> list = batSOdsFtoYwtxDkyeDMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatSOdsFtoYwtxDkyeD record) {
        return batSOdsFtoYwtxDkyeDMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatSOdsFtoYwtxDkyeD record) {
        return batSOdsFtoYwtxDkyeDMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatSOdsFtoYwtxDkyeD record) {
        return batSOdsFtoYwtxDkyeDMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatSOdsFtoYwtxDkyeD record) {
        return batSOdsFtoYwtxDkyeDMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String acctdt, String brchno, String stline, java.math.BigDecimal loanbl) {
        return batSOdsFtoYwtxDkyeDMapper.deleteByPrimaryKey(acctdt, brchno, stline, loanbl);
    }


    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建零售智能风控系统-历史表-五级分类[bat_s_ods_fto_ywtx_dkye_d]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_ods_fto_ywtx_dkye_d");
        logger.info("重建零售智能风控系统-历史表-五级分类[bat_s_ods_fto_ywtx_dkye_d]结束");
        return 0;
    }
}
