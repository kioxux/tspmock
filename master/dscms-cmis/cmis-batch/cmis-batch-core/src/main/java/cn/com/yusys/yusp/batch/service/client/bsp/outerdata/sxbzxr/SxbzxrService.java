package cn.com.yusys.yusp.batch.service.client.bsp.outerdata.sxbzxr;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.req.SxbzxrReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp.SxbzxrRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2OuterdataClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;


/**
 * 查询失信被执行人
 */
@Service
public class SxbzxrService {
    private static final Logger logger = LoggerFactory.getLogger(SxbzxrService.class);
    // 1）注入：BSP封装调用外部数据平台的接口
    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

    /**
     * 查询失信被执行人
     *
     * @param sxbzxrReqDto
     * @return
     * @throws BizException
     */
    @Transactional
    public SxbzxrRespDto sxbzxr(SxbzxrReqDto sxbzxrReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value, JSON.toJSONString(sxbzxrReqDto));
        ResultDto<SxbzxrRespDto> sxbzxrResultDto = dscms2OuterdataClientService.sxbzxr(sxbzxrReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value, JSON.toJSONString(sxbzxrResultDto));
        String sxbzxrCode = Optional.ofNullable(sxbzxrResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String sxbzxrMeesage = Optional.ofNullable(sxbzxrResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        SxbzxrRespDto sxbzxrRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, sxbzxrResultDto.getCode())) {
            //  获取相关的值并解析
            sxbzxrRespDto = sxbzxrResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, sxbzxrCode, sxbzxrMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value);
        return sxbzxrRespDto;
    }

    /**
     * 查询失信被执行人
     *
     * @param sxbzxrReqDto
     * @return
     * @throws BizException
     */
    @Transactional
    public ResultDto<SxbzxrRespDto> sxbzxrNew(SxbzxrReqDto sxbzxrReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SXBZXR.key, EsbEnum.TRADE_CODE_SXBZXR.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SXBZXR.key, EsbEnum.TRADE_CODE_SXBZXR.value, JSON.toJSONString(sxbzxrReqDto));
        ResultDto<SxbzxrRespDto> sxbzxrResultDto = dscms2OuterdataClientService.sxbzxr(sxbzxrReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SXBZXR.key, EsbEnum.TRADE_CODE_SXBZXR.value, JSON.toJSONString(sxbzxrResultDto));
        return sxbzxrResultDto;
    }
    /**
     * @param corpMap
     * @return
     */
    public SxbzxrReqDto buildByCorpMap(Map<String, String> corpMap) {
        logger.info("根据查询结果来组装[请求Dto：失信被执行人查询接口]开始,请求参数为:[{}]", JSON.toJSONString(corpMap));
        SxbzxrReqDto sxbzxrReqDto = new SxbzxrReqDto();
        String cusId = corpMap.get("cusId");
        String cusName = corpMap.get("cusName");
        String certType = corpMap.get("certType");
        String certCode = corpMap.get("certCode");
        String type = toConverType(certType);//查询类型 0：查询自然人， 1：查询组织机构
        String name = cusName;//姓名/企业名称
        String qyid = certCode;//身份证号/组织机构代码
        sxbzxrReqDto.setType(type);
        sxbzxrReqDto.setName(name);
        sxbzxrReqDto.setQyid(qyid);
        logger.info("根据查询结果来组装[请求Dto：失信被执行人查询接口]结束,响应参数为:[{}]", JSON.toJSONString(sxbzxrReqDto));
        return sxbzxrReqDto;
    }

    /**
     * 根据证件类型获取查询类型
     *
     * @param certType
     * @return
     */
    private String toConverType(String certType) {
        String type = "";
        if ("C".equals(certType)) { //户口簿
            type = "0";
        } else if ("B".equals(certType)) {//护照
            type = "0";
        } else if ("D".equals(certType)) {//港澳居民来往内地通行证
            type = "0";
        } else if ("E".equals(certType)) {//台湾同胞来往内地通行证
            type = "0";
        } else if ("12".equals(certType)) { // 外国人居留证
            type = "0";
        } else if ("Y".equals(certType)) {//警官证
            type = "0";
        } else if ("13".equals(certType)) {//香港身份证
            type = "0";
        } else if ("14".equals(certType)) {//澳门身份证
            type = "0";
        } else if ("15".equals(certType)) {//台湾身份证
            type = "0";
        } else if ("16".equals(certType)) {//其他证件
            type = "0";
        } else if ("A".equals(certType)) {//居民身份证及其他以公民身份证号为标识的证件
            type = "0";
        } else if ("11".equals(certType)) {//军人身份证件
            type = "0";
        } else if ("06".equals(certType)) {//工商注册号
            type = "1";
        } else if ("01".equals(certType)) {//机关和事业单位登记号
            type = "1";
        } else if ("02".equals(certType)) {//社会团体登记号
            type = "1";
        } else if ("03".equals(certType)) {//民办非企业登记号
            type = "1";
        } else if ("04".equals(certType)) {//基金会登记号
            type = "1";
        } else if ("05".equals(certType)) {//宗教证书登记号
            type = "1";
        } else if ("P2".equals(certType)) {//中征码
            type = "1";
        } else if ("R".equals(certType)) {//统一社会信用代码
            type = "1";
        } else if ("Q".equals(certType)) {//组织机构代码
            type = "1";
        } else if ("07".equals(certType)) {//纳税人识别号（国税）
            type = "1";
        } else if ("08".equals(certType)) {//纳税人识别号（地税）
            type = "1";
        }
        return type;
    }
}
