/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.check;

import org.apache.ibatis.annotations.Param;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateLmtSubUseMapper
 * @类描述: #Dao类
 * @功能描述: 数据更新额度校正-计算分项用信金额
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCheckUpdateLmtSubUseMapper {

    /**
     * 1、若合同审批状态为在途，则更新占用关系为200-生效
     */
    int updateLmtSubUseHtspzt(@Param("cusId") String cusId);

    /**
     * 2、若合同项下台账还有生效状态的，则更新合同状态为生效
     */
    int updateLmtSubUseHtxtzsx(@Param("cusId") String cusId);

    /**
     * 3、若合同状态为生效，则更新占用关系状态为生效
     */
    int updateLmtSubUseHtztsx(@Param("cusId") String cusId);

    /**
     * 4、若合同状态为作废，则更新占用关系状态为作废
     */
    int updateLmtSubUseHtztzf(@Param("cusId") String cusId);

    /**
     * 5、若合同状态为注销或中止，项下存在非作废的台账，则更新占用关系状态为 300-注销
     */
    int updateLmtSubUseHtztzxorzzAndExistFzftz(@Param("cusId") String cusId);

    /**
     * 6、若合同状态为注销或者中止，项下不存在非作废或者关闭的台账，则 占用状态更新为 作废
     */
    int updateLmtSubUseHtztCancelOrStopAndNoExistFzftz(@Param("cusId") String cusId);

    /**
     * 7、合同状态为注销或者中止或者生效或者到期，项下存在非作废或者关闭的台账且月>0，则 占用状态更新为 500-到期未结清
     */
    int updateLmtSubUseHtztCancelOrStopOrActiveOrEnd1(@Param("cusId") String cusId);

    int updateLmtSubUseHtztCancelOrStopOrActiveOrEnd2(@Param("cusId") String cusId);

    /**
     * 8、将计算后的合同占用关系 更新至  正式表
     */
    int updateLmtSubUseCalculateToFormalTable1_1(@Param("cusId") String cusId);

    int updateLmtSubUseCalculateToFormalTable1_2(@Param("cusId") String cusId);

    /**
     * 9、将计算后的合同占用关系 更新至  正式表
     */
    int updateLmtSubUseCalculateToFormalTable2_1(@Param("cusId") String cusId);

    int updateLmtSubUseCalculateToFormalTable2_2(@Param("cusId") String cusId);

    /**
     * 10、将计算后的台账占用关系 更新至  正式表（台账占用额度）
     */
    int updateLmtSubUseTzzyToFormal(@Param("cusId") String cusId);

    /**
     * 11、根据占用关系更新分项 总额已用、敞口已用
     */
    int updateLmtSubUseZeyyAndCkyy1(@Param("cusId") String cusId);

    int updateLmtSubUseZeyyAndCkyy2(@Param("cusId") String cusId);

    int updateLmtSubUseZeyyAndCkyy3(@Param("cusId") String cusId);

    int updateLmtSubUseZeyyAndCkyy4(@Param("cusId") String cusId);

    /**
     * 12、二级分项占额需要汇总至一级分项
     */
    int updateLmtSubUse2LevelZeTo1Level1(@Param("cusId") String cusId);

    int updateLmtSubUse2LevelZeTo1Level2(@Param("cusId") String cusId);

    int updateLmtSubUse2LevelZeTo1Level3(@Param("cusId") String cusId);

    int updateLmtSubUse2LevelZeTo1Level4(@Param("cusId") String cusId);

}