package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0114</br>
 * 任务名称：加工任务-业务处理-lpr利率同步  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0114Mapper {

    /**
     * 清空LPR配置表
     *
     * @param openDay
     * @return
     */

    int truncatecfgLprRate(@Param("openDay") String openDay);
    /**
     * 插入LPR配置表
     *
     * @param openDay
     * @return
     */
    int insertCfgLprRate(@Param("openDay") String openDay);

    /**
     * 将lpr利率表的生效日期减一
     *
     * @param openDay
     * @return
     */
    int updateCfgLprRate(@Param("openDay") String openDay);
}
