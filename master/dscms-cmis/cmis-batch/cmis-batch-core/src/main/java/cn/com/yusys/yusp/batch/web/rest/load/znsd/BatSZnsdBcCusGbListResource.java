/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.znsd;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.znsd.BatSZnsdBcCusGbList;
import cn.com.yusys.yusp.batch.service.load.znsd.BatSZnsdBcCusGbListService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSZnsdBcCusGbListResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-28 14:24:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsznsdbccusgblist")
public class BatSZnsdBcCusGbListResource {
    @Autowired
    private BatSZnsdBcCusGbListService batSZnsdBcCusGbListService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSZnsdBcCusGbList>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSZnsdBcCusGbList> list = batSZnsdBcCusGbListService.selectAll(queryModel);
        return new ResultDto<List<BatSZnsdBcCusGbList>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSZnsdBcCusGbList>> index(QueryModel queryModel) {
        List<BatSZnsdBcCusGbList> list = batSZnsdBcCusGbListService.selectByModel(queryModel);
        return new ResultDto<List<BatSZnsdBcCusGbList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{id}")
    protected ResultDto<BatSZnsdBcCusGbList> show(@PathVariable("id") String id) {
        BatSZnsdBcCusGbList batSZnsdBcCusGbList = batSZnsdBcCusGbListService.selectByPrimaryKey(id);
        return new ResultDto<BatSZnsdBcCusGbList>(batSZnsdBcCusGbList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSZnsdBcCusGbList> create(@RequestBody BatSZnsdBcCusGbList batSZnsdBcCusGbList) throws URISyntaxException {
        batSZnsdBcCusGbListService.insert(batSZnsdBcCusGbList);
        return new ResultDto<BatSZnsdBcCusGbList>(batSZnsdBcCusGbList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSZnsdBcCusGbList batSZnsdBcCusGbList) throws URISyntaxException {
        int result = batSZnsdBcCusGbListService.update(batSZnsdBcCusGbList);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{id}")
    protected ResultDto<Integer> delete(@PathVariable("id") String id) {
        int result = batSZnsdBcCusGbListService.deleteByPrimaryKey(id);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSZnsdBcCusGbListService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
