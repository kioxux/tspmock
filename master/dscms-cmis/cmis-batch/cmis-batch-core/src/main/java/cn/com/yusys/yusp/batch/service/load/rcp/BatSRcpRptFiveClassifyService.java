/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.rcp;

import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpRptFiveClassify;
import cn.com.yusys.yusp.batch.repository.mapper.load.rcp.BatSRcpRptFiveClassifyMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpRptFiveClassifyService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:02:45
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSRcpRptFiveClassifyService {
    private static final Logger logger = LoggerFactory.getLogger(BatSRcpRptFiveClassifyService.class);
    @Autowired
    private TableUtilsService tableUtilsService;
    @Autowired
    private BatSRcpRptFiveClassifyMapper batSRcpRptFiveClassifyMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatSRcpRptFiveClassify selectByPrimaryKey(String seralNo) {
        return batSRcpRptFiveClassifyMapper.selectByPrimaryKey(seralNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatSRcpRptFiveClassify> selectAll(QueryModel model) {
        List<BatSRcpRptFiveClassify> records = (List<BatSRcpRptFiveClassify>) batSRcpRptFiveClassifyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatSRcpRptFiveClassify> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSRcpRptFiveClassify> list = batSRcpRptFiveClassifyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatSRcpRptFiveClassify record) {
        return batSRcpRptFiveClassifyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatSRcpRptFiveClassify record) {
        return batSRcpRptFiveClassifyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatSRcpRptFiveClassify record) {
        return batSRcpRptFiveClassifyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatSRcpRptFiveClassify record) {
        return batSRcpRptFiveClassifyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String seralNo) {
        return batSRcpRptFiveClassifyMapper.deleteByPrimaryKey(seralNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSRcpRptFiveClassifyMapper.deleteByIds(ids);
    }

    /**
     * 将T表数据merge到S表
     *
     * @param openDay
     */
    public void mergeT2S(String openDay) {
        // 更新S表中 数据日期 DATA_DATE 为营业日期
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]开始,请求参数为:{}", openDay);
        int updateSResult = batSRcpRptFiveClassifyMapper.updateDataDateByOpenDay(openDay);
        logger.info("更新S表中[数据日期 DATA_DATE 为营业日期]结束,返回参数为:{}", updateSResult);
    }

    /**
     * 删除S表当天数据
     *
     * @param openDay
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
//        return batSRcpRptFiveClassifyMapper.deleteByOpenDay(openDay);
        // 重建相关表
        logger.info("重建零售智能风控系统-历史表-五级分类[bat_s_rcp_rpt_five_classify]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_rcp_rpt_five_classify");
        logger.info("重建零售智能风控系统-历史表-五级分类[bat_s_rcp_rpt_five_classify]结束");
        return 0;
    }
}
