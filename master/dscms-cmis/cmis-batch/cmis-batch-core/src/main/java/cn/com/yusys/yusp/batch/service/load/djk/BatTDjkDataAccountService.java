/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.djk;

import java.util.List;

import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpRptFiveClassifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.batch.domain.load.djk.BatTDjkDataAccount;
import cn.com.yusys.yusp.batch.repository.mapper.load.djk.BatTDjkDataAccountMapper;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTDjkDataAccountService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-07 15:41:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTDjkDataAccountService {
    private static final Logger logger = LoggerFactory.getLogger(BatSRcpRptFiveClassifyService.class);
    @Autowired
    private TableUtilsService tableUtilsService;
    @Autowired
    private BatTDjkDataAccountMapper batTDjkDataAccountMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTDjkDataAccount selectByPrimaryKey(String acctNo, String custId) {
        return batTDjkDataAccountMapper.selectByPrimaryKey(acctNo, custId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTDjkDataAccount> selectAll(QueryModel model) {
        List<BatTDjkDataAccount> records = (List<BatTDjkDataAccount>) batTDjkDataAccountMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTDjkDataAccount> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTDjkDataAccount> list = batTDjkDataAccountMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTDjkDataAccount record) {
        return batTDjkDataAccountMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTDjkDataAccount record) {
        return batTDjkDataAccountMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTDjkDataAccount record) {
        return batTDjkDataAccountMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTDjkDataAccount record) {
        return batTDjkDataAccountMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String acctNo, String custId) {
        return batTDjkDataAccountMapper.deleteByPrimaryKey(acctNo, custId);
    }

    /**
     * 清空落地表
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void truncateTTable() {
        // 重建相关表
        logger.info("重建账户基本信息文件[bat_t_djk_data_account]开始");
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_t_djk_data_account");
        logger.info("重建账户基本信息文件类[bat_t_djk_data_account]结束");
    }
}
