package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0211Mapper;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0211</br>
 * 任务名称：加工任务-额度处理-Comstar额度处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0211Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0211Service.class);
    @Autowired
    private Cmis0211Mapper cmis0211Mapper;

    /**
     * 更新Comstar全量交易明细表中字段
     *
     * @param openDay
     */
    public void cmis0211UpdateLmtComstarField(String openDay) {
        // 更新Comstar全量交易明细表中字段
        logger.info("更新Comstar全量交易明细表中[trade_type]字段开始,请求参数为:[{}]", openDay);
        int updateBatSSbsDetailZjg00 = cmis0211Mapper.updateBatSSbsDetailZjg00(openDay);
        logger.info("更新Comstar全量交易明细表中[trade_type]字段结束,返回参数为:[{}]", updateBatSSbsDetailZjg00);

        logger.info("更新Comstar全量交易明细表中[start_date]字段开始,请求参数为:[{}]", openDay);
        int updateBatSSbsDetailZjg01 = cmis0211Mapper.updateBatSSbsDetailZjg01(openDay);
        logger.info("更新Comstar全量交易明细表中[start_date]字段结束,返回参数为:[{}]", updateBatSSbsDetailZjg01);

        logger.info("更新Comstar全量交易明细表中[end_date]字段开始,请求参数为:[{}]", openDay);
        int updateBatSSbsDetailZjg02 = cmis0211Mapper.updateBatSSbsDetailZjg02(openDay);
        logger.info("更新Comstar全量交易明细表中[end_date]字段结束,返回参数为:[{}]", updateBatSSbsDetailZjg02);

        logger.info("更新Comstar全量交易明细表中[bat_date]字段开始,请求参数为:[{}]", openDay);
        int updateBatSSbsDetailZjg03 = cmis0211Mapper.updateBatSSbsDetailZjg03(openDay);
        logger.info("更新Comstar全量交易明细表中[bat_date]字段结束,返回参数为:[{}]", updateBatSSbsDetailZjg03);

        logger.info("更新Comstar额度已用表中[bat_date]字段开始,请求参数为:[{}]", openDay);
        int updateBatSSbsAvailableZjg01 = cmis0211Mapper.updateBatSSbsAvailableZjg01(openDay);
        logger.info("更新Comstar额度已用表中[bat_date]字段结束,返回参数为:[{}]", updateBatSSbsAvailableZjg01);
    }

    /**
     * 更新中间表LMT_ID字段为额度编号0A
     *
     * @param openDay
     */
    public void cmis0211UpdateAvailableZjg0A(String openDay) {
        // 更新中间表LMT_ID字段为额度编号
        logger.info("清空Comstar与白名单额度信息表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteTmpSbsAvailLmtWhiteInfo0A();
        logger.info("清空Comstar与白名单额度信息表结束");

        logger.info("插入Comstar与白名单额度信息表开始,请求参数为:[{}]", openDay);
        int insertTmpSbsAvailLmtWhiteInfo0A = cmis0211Mapper.insertTmpSbsAvailLmtWhiteInfo0A(openDay);
        logger.info("插入Comstar与白名单额度信息表结束,返回参数为:[{}]", insertTmpSbsAvailLmtWhiteInfo0A);

        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号开始,请求参数为:[{}]", openDay);
        int updateBatSSbsAvailableZjg0A = cmis0211Mapper.updateBatSSbsAvailableZjg0A(openDay);
        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号结束,返回参数为:[{}]", updateBatSSbsAvailableZjg0A);
    }

    /**
     * 更新中间表LMT_ID字段为额度编号0B
     *
     * @param openDay
     */
    public void cmis0211UpdateAvailableZjg0B(String openDay) {
        logger.info("清空Comstar与批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteBatSSbsAvailableZjg0B();
        logger.info("清空Comstar与批复额度分项基础信息结束");

        logger.info("插入Comstar与批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        int insertBatSSbsAvailableZjg0B = cmis0211Mapper.insertBatSSbsAvailableZjg0B(openDay);
        logger.info("插入Comstar与批复额度分项基础信息结束,返回参数为:[{}]", insertBatSSbsAvailableZjg0B);

        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号开始,请求参数为:[{}]", openDay);
        int updateBatSSbsAvailableZjg0B = cmis0211Mapper.updateBatSSbsAvailableZjg0B(openDay);
        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号结束,返回参数为:[{}]", updateBatSSbsAvailableZjg0B);
    }

    /**
     * 更新中间表LMT_ID字段为额度编号0C
     *
     * @param openDay
     */
    public void cmis0211UpdateAvailableZjg0C(String openDay) {
        logger.info("清空Comstar与批复额度分项基础信息(资产编号)开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteBatSSbsAvailableZjg0C();
        logger.info("清空Comstar与批复额度分项基础信息(资产编号)结束");

        logger.info("插入Comstar与批复额度分项基础信息(资产编号)开始,请求参数为:[{}]", openDay);
        int insertBatSSbsAvailableZjg0C = cmis0211Mapper.insertBatSSbsAvailableZjg0C(openDay);
        logger.info("插入Comstar与批复额度分项基础信息(资产编号)结束,返回参数为:[{}]", insertBatSSbsAvailableZjg0C);

        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号开始,请求参数为:[{}]", openDay);
        int updateBatSSbsAvailableZjg0C = cmis0211Mapper.updateBatSSbsAvailableZjg0C(openDay);
        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号结束,返回参数为:[{}]", updateBatSSbsAvailableZjg0C);
    }

    /**
     * 更新Comstar全量交易明细表中[LMT_ID]字段为额度编号0A
     *
     * @param openDay
     */
    public void cmis0211UpdateSbsDetailZjg0A(String openDay) {
        logger.info("清空Comstar与白名单额度信息表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteBatSSbsDetailZjg0A();
        logger.info("清空Comstar与白名单额度信息表结束");

        logger.info("插入Comstar与白名单额度信息表开始,请求参数为:[{}]", openDay);
        int insertBatSSbsDetailZjg0A = cmis0211Mapper.insertBatSSbsDetailZjg0A(openDay);
        logger.info("插入Comstar与白名单额度信息表结束,返回参数为:[{}]", insertBatSSbsDetailZjg0A);

        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号开始,请求参数为:[{}]", openDay);
        int updateBatSSbsDetailZjg0A = cmis0211Mapper.updateBatSSbsDetailZjg0A(openDay);
        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号结束,返回参数为:[{}]", updateBatSSbsDetailZjg0A);
    }

    /**
     * 更新Comstar全量交易明细表中[LMT_ID]字段为额度编号0B
     *
     * @param openDay
     */
    public void cmis0211UpdateSbsDetailZjg0B(String openDay) {
        logger.info("清空Comstar与批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteBatSSbsDetailZjg0B();
        logger.info("清空Comstar与批复额度分项基础信息结束");

        logger.info("插入Comstar与批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        int insertBatSSbsDetailZjg0B = cmis0211Mapper.insertBatSSbsDetailZjg0B(openDay);
        logger.info("插入Comstar与批复额度分项基础信息结束,返回参数为:[{}]", insertBatSSbsDetailZjg0B);

        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号开始,请求参数为:[{}]", openDay);
        int updateBatSSbsDetailZjg0B = cmis0211Mapper.updateBatSSbsDetailZjg0B(openDay);
        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号结束,返回参数为:[{}]", updateBatSSbsDetailZjg0B);

    }

    /**
     * 更新Comstar全量交易明细表中[LMT_ID]字段为额度编号0C
     *
     * @param openDay
     */
    public void cmis0211UpdateSbsDetailZjg0C(String openDay) {
        logger.info("清空Comstar与批复额度分项基础信息(资产编号)开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteBatSSbsDetailZjg0C();
        logger.info("清空Comstar与批复额度分项基础信息(资产编号)结束");

        logger.info("插入Comstar与批复额度分项基础信息(资产编号)开始,请求参数为:[{}]", openDay);
        int insertBatSSbsDetailZjg0C = cmis0211Mapper.insertBatSSbsDetailZjg0C(openDay);
        logger.info("插入Comstar与批复额度分项基础信息(资产编号)结束,返回参数为:[{}]", insertBatSSbsDetailZjg0C);

        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号开始,请求参数为:[{}]", openDay);
        int updateBatSSbsDetailZjg0C = cmis0211Mapper.updateBatSSbsDetailZjg0C(openDay);
        logger.info("更新Comstar全量交易明细表中[LMT_ID]字段为额度编号结束,返回参数为:[{}]", updateBatSSbsDetailZjg0C);
    }

    /**
     * 更新Comstar全量交易明细表中[cus_id]字段
     *
     * @param openDay
     */
    public void cmis0211UpdateSbsDetailZjg1(String openDay) {
        // 更新中间表cus_id字段
        logger.info("清空Comstar与批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteBatSSbsDetailZjg1();
        logger.info("清空Comstar与批复额度分项基础信息结束");

        logger.info("插入Comstar与批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        int insertBatSSbsDetailZjg1 = cmis0211Mapper.insertBatSSbsDetailZjg1(openDay);
        logger.info("插入Comstar与批复额度分项基础信息结束,返回参数为:[{}]", insertBatSSbsDetailZjg1);

        logger.info("更新Comstar全量交易明细表中[cus_id]字段开始,请求参数为:[{}]", openDay);
        int updateBatSSbsDetailZjg1 = cmis0211Mapper.updateBatSSbsDetailZjg1(openDay);
        logger.info("更新Comstar全量交易明细表中[cus_id]字段结束,返回参数为:[{}]", updateBatSSbsDetailZjg1);

    }

    /**
     * 更新Comstar全量交易明细表中[cus_id]字段
     *
     * @param openDay
     */
    public void cmis0211UpdateSbsDetailZjg(String openDay) {

        // 当天发生的新的占用插入至占用关系表
        logger.info("插入当天发生的新的占用至占用关系表开始,请求参数为:[{}]", openDay);
        int insertLmtContRel = cmis0211Mapper.insertLmtContRel(openDay);
        logger.info("插入当天发生的新的占用至占用关系表结束,返回参数为:[{}]", insertLmtContRel);

        // 新插入的占用更新下客户名称
        logger.info("清空授信占用关系表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteLmtContRelCusName();
        logger.info("清空授信占用关系表结束");

        logger.info("插入授信占用关系表开始,请求参数为:[{}]", openDay);
        int insertLmtContRelCusName = cmis0211Mapper.insertLmtContRelCusName(openDay);
        logger.info("插入授信占用关系表结束,返回参数为:[{}]", insertLmtContRelCusName);

        logger.info("更新占用关系表中的[cus_name]开始,请求参数为:[{}]", openDay);
        int updateLmtContRelCusName1 = cmis0211Mapper.updateLmtContRelCusName(openDay);
        logger.info("更新占用关系表中的[cus_name]开始,请求参数为:[{}]", updateLmtContRelCusName1);

        // 新插入的占用更新下客户名称
        logger.info("清空授信占用关系表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteLmtContRelCusName();
        logger.info("清空授信占用关系表结束");

        logger.info("增加关联同业客户信息,请求参数为:[{}]", openDay);
        int insertLmtContRelCusIntBankName = cmis0211Mapper.insertLmtContRelCusIntBankName(openDay);
        logger.info("增加关联同业客户信息,返回参数为:[{}]", insertLmtContRelCusIntBankName);

        logger.info("更新占用关系表中的[cus_name]开始,请求参数为:[{}]", openDay);
        int updateLmtContRelCusName2 = cmis0211Mapper.updateLmtContRelCusName(openDay);
        logger.info("更新占用关系表中的[cus_name]开始,请求参数为:[{}]", updateLmtContRelCusName2);

        // 更新授信类型
        logger.info("清空授信占用关系表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteLmtContRelLmtType();
        logger.info("清空授信占用关系表结束");

        logger.info("插入授信占用关系表开始,请求参数为:[{}]", openDay);
        int insertLmtContRelLmtType = cmis0211Mapper.insertLmtContRelLmtType(openDay);
        logger.info("插入授信占用关系表结束,返回参数为:[{}]", insertLmtContRelLmtType);

        logger.info("更新占用关系表中的[lmt_type]开始,请求参数为:[{}]", openDay);
        int updateLmtContRelLmtType = cmis0211Mapper.updateLmtContRelLmtType(openDay);
        logger.info("更新占用关系表中的[lmt_type]开始,请求参数为:[{}]", updateLmtContRelLmtType);

        // ComStar的占用关系存在有余额，但是ComStar当日无数据的，则更新交易余额为0
        logger.info("清空授信占用关系表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteLmtContRelLmtType();
        logger.info("清空授信占用关系表结束");

        logger.info("插入授信占用关系表开始,请求参数为:[{}]", openDay);
        int insertLmtContRelNonCom = cmis0211Mapper.insertLmtContRelNonCom(openDay);
        logger.info("插入授信占用关系表结束,返回参数为:[{}]", insertLmtContRelNonCom);

        logger.info(" ComStar的占用关系存在有余额，但是ComStar当日无数据的，则更新交易余额为0开始,请求参数为:[{}]", openDay);
        int updateLmtContRelNonCom = cmis0211Mapper.updateLmtContRelNonCom(openDay);
        logger.info(" ComStar的占用关系存在有余额，但是ComStar当日无数据的，则更新交易余额为0结束,返回参数为:[{}]", updateLmtContRelNonCom);


        // 1.根据推送数据的额度分项编号关联对应额度表，若额度品种为3001-同业融资类额度.3005-同业交易类额度.300601-货币基金额度.300701-产品户买入反售额度(信用债)，则占用关系的占用总金额.占用总余额.占用敞口金额.占用敞口余额以当日推送数据为准
        logger.info("1.根据推送数据的额度分项编号关联对应额度表，若额度品种为3001-同业融资类额度.3005-同业交易类额度.300601-货币基金额度.300701-产品户买入反售额度(信用债)，则占用关系的占用总金额.占用总余额.占用敞口金额.占用敞口余额以当日推送数据为准");
        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteLmtContRel01A();
        logger.info("清空加工占用授信表结束");

        logger.info("插入加工占用授信表开始,请求参数为:[{}]", openDay);
        int insertLmtContRel01A = cmis0211Mapper.insertLmtContRel01A(openDay);
        logger.info("插入加工占用授信表结束,返回参数为:[{}]", insertLmtContRel01A);

        logger.info("更新分项占用关系信息:3001-同业融资类额度.3005-同业交易类额度.300601-货币基金额度.300701-产品户买入反售额度(信用债)开始,请求参数为:[{}]", openDay);
        int updateLmtContRel01A = cmis0211Mapper.updateLmtContRel01A(openDay);
        logger.info("更新分项占用关系信息:3001-同业融资类额度.3005-同业交易类额度.300601-货币基金额度.300701-产品户买入反售额度(信用债)结束,返回参数为:[{}]", updateLmtContRel01A);

        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteLmtContRel01B();
        logger.info("清空加工占用授信表结束");

        logger.info("插入加工占用授信表开始,请求参数为:[{}]", openDay);
        int insertLmtContRel01B = cmis0211Mapper.insertLmtContRel01B(openDay);
        logger.info("插入加工占用授信表结束,返回参数为:[{}]", insertLmtContRel01B);

        logger.info("更新分项占用关系信息:300701-产品户买入反售额度(信用债)  数据在白名单额度表开始,请求参数为:[{}]", openDay);
        int updateLmtContRel01B = cmis0211Mapper.updateLmtContRel01B(openDay);
        logger.info("更新分项占用关系信息:300701-产品户买入反售额度(信用债)  数据在白名单额度表结束,返回参数为:[{}]", updateLmtContRel01B);

        // 2.判断额度品种为3001-同业融资类额度.3005-同业交易类额度.300601-货币基金额度.300701-产品户买入反售额度(信用债)对应的占用关系是否存在到期日已到期，但是占用余额不为0的，将其更新为0
        logger.info("2.判断额度品种为3001-同业融资类额度.3005-同业交易类额度.300601-货币基金额度.300701-产品户买入反售额度(信用债)对应的占用关系是否存在到期日已到期，但是占用余额不为0的，将其更新为0");
        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteLmtContRel02();
        logger.info("清空加工占用授信表结束");

        logger.info("插入加工占用授信表开始,请求参数为:[{}]", openDay);
        int insertLmtContRel02 = cmis0211Mapper.insertLmtContRel02(openDay);
        logger.info("插入加工占用授信表结束,返回参数为:[{}]", insertLmtContRel02);

        logger.info("更新分项占用关系信息:3001-同业融资类额度.3005-同业交易类额度.300601-货币基金额度.300701-产品户买入反售额度(信用债)开始,请求参数为:[{}]", openDay);
        int updateLmtContRel02 = cmis0211Mapper.updateLmtContRel02(openDay);
        logger.info("更新分项占用关系信息:3001-同业融资类额度.3005-同业交易类额度.300601-货币基金额度.300701-产品户买入反售额度(信用债)结束,返回参数为:[{}]", updateLmtContRel02);

        // 3.其他类型的占用关系数据，以当日推送的全量数据，根据额度分项编号及资产编号，进行加加减减的汇总，作为占用关系的占用总金额.占用总余额.占用敞口金额.占用敞口余额；
        logger.info("3.其他类型的占用关系数据，以当日推送的全量数据，根据额度分项编号及资产编号，进行加加减减的汇总，作为占用关系的占用总金额.占用总余额.占用敞口金额.占用敞口余额；");

        logger.info("4.ComStar管控的额度，如果当日额度占用情况表未推送数据，则更新占用和余额为0");
        // <!-- 1.APPR_LMT_SUB_BASIC_INFO	ComStar管控的额度，如果当日额度占用情况表未推送数据，则占用和余额为0-->
        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteApprLmtSubBasicInfo();
        logger.info("清空加工占用授信表结束");

        logger.info("插入加工占用授信表开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfo = cmis0211Mapper.insertApprLmtSubBasicInfo(openDay);
        logger.info("插入加工占用授信表结束,返回参数为:[{}]", insertApprLmtSubBasicInfo);

        logger.info("更新批复额度分项基础信息表中额度开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo = cmis0211Mapper.updateApprLmtSubBasicInfo(openDay);
        logger.info("更新批复额度分项基础信息表中额度结束,返回参数为:[{}]", updateApprLmtSubBasicInfo);

        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteLmtWhiteInfo();
        logger.info("清空加工占用授信表结束");

        logger.info("插入加工占用授信表开始,请求参数为:[{}]", openDay);
        int insertLmtWhiteInfo = cmis0211Mapper.insertLmtWhiteInfo(openDay);
        logger.info("插入加工占用授信表结束,返回参数为:[{}]", insertLmtWhiteInfo);

        logger.info("更新白名单额度信息表中[sig_use_amt]开始,请求参数为:[{}]", openDay);
        int updateLmtWhiteInfo = cmis0211Mapper.updateLmtWhiteInfo(openDay);
        logger.info("更新白名单额度信息表中[sig_use_amt]结束,返回参数为:[{}]", updateLmtWhiteInfo);

        // 根据limit_available_zjg推送额度分项编号关联额度模块批复分项流水号更新OUTSTND_AMT为推送的已用金额
        // <!-- 涉及额度：3001-同业融资类额度 3005-同业交易类额度  300601货币基金  40开头的单笔投资业务额度  300701-产品户买入返售（信用债）  -->
        logger.info("根据limit_available_zjg推送额度分项编号关联额度模块批复分项流水号更新OUTSTND_AMT为推送的已用金额");
        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteApprLmtSubBasicInfo11();
        logger.info("清空加工占用授信表结束");

        logger.info("插入加工占用授信表开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfo11 = cmis0211Mapper.insertApprLmtSubBasicInfo11(openDay);
        logger.info("插入加工占用授信表结束,返回参数为:[{}]", insertApprLmtSubBasicInfo11);

        logger.info("更新批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo11 = cmis0211Mapper.updateApprLmtSubBasicInfo11(openDay);
        logger.info("更新批复额度分项基础信息结束,返回参数为:[{}]", updateApprLmtSubBasicInfo11);


        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteApprLmtSubBasicInfo12();
        logger.info("清空加工占用授信表结束");

        logger.info("插入加工占用授信表开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfo12 = cmis0211Mapper.insertApprLmtSubBasicInfo12(openDay);
        logger.info("插入加工占用授信表结束,返回参数为:[{}]", insertApprLmtSubBasicInfo12);

        logger.info("更新批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfo12 = cmis0211Mapper.updateApprLmtSubBasicInfo12(openDay);
        logger.info("更新批复额度分项基础信息结束,返回参数为:[{}]", updateApprLmtSubBasicInfo12);

        // <!-- 更新lmt_cont_rel中产品授信占用同业管理类/投资类的交易记录  占用余额  占用敞口余额 -->
        logger.info("更新分项占用关系信息开始,请求参数为:[{}]", openDay);
        int updateLmtContRel11 = cmis0211Mapper.updateLmtContRel11(openDay);
        logger.info("更新分项占用关系信息结束,返回参数为:[{}]", updateLmtContRel11);

        logger.info("额度分项如果为货币基金额度（LIMIT_SUB_NO=300601），则更新货币基金额度的父分项已用金额 = 已用金额+货币基金额度的已用");
        // <!--额度分项如果为货币基金额度（LIMIT_SUB_NO=300601），则更新货币基金额度的父分项已用金额 = 已用金额+货币基金额度的已用；-->
        logger.info("更新分项占用关系信息开始,请求参数为:[{}]", openDay);
        int updateLmtContRel21A = cmis0211Mapper.updateLmtContRel21A(openDay);
        logger.info("更新分项占用关系信息结束,返回参数为:[{}]", updateLmtContRel21A);
        // <!-- 更新同业管理类额度、同业投资类的占用为管理类额度下关联的单笔投资授信总额，且余额更新为0 -->

        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteLmtContRel21B();
        logger.info("清空加工占用授信表结束");

        logger.info("插入加工占用授信表开始,请求参数为:[{}]", openDay);
        int insertLmtContRel21B = cmis0211Mapper.insertLmtContRel21B(openDay);
        logger.info("插入加工占用授信表结束,返回参数为:[{}]", insertLmtContRel21B);

        logger.info("更新批复额度分项基础信息开始,请求参数为:[{}]", openDay);
        int updateLmtContRel21B = cmis0211Mapper.updateLmtContRel21B(openDay);
        logger.info("更新批复额度分项基础信息结束,返回参数为:[{}]", updateLmtContRel21B);

        // <!-- 再将货币基金授信分项 占用额累加到父授信分项 处理占用金额，及占用敞口金额-->
        logger.info("再将货币基金授信分项 占用额累加到父授信分项");
        //  <!-- 	SPAC_OUTSTND_AMT		(同业授信额度.产品授信额度.主体授信额度)敞口已用额度 = 已用额度 -->
        // <!-- 批复额度分项基础信息 -->
        logger.info("更新批复额度分项基础信息表中[SPAC_OUTSTND_AMT]开始,请求参数为:[{}]", openDay);
        int updateLmtContRel24 = cmis0211Mapper.updateLmtContRel24(openDay);
        logger.info("更新批复额度分项基础信息表中[SPAC_OUTSTND_AMT]结束,返回参数为:[{}]", updateLmtContRel24);

        logger.info("若3002-同业投资类、3006-同业管理类、15030101-穿透化额度已用金额=0，且额度已到期，则分项状态更新为99-失效已结清开始,请求参数为:[{}]", openDay);
        int updateTmpDealLmtDetails03 = cmis0211Mapper.updateTmpDealLmtDetails03(openDay);
        logger.info("若3002-同业投资类、3006-同业管理类、15030101-穿透化额度已用金额=0，且额度已到期，则分项状态更新为99-失效已结清结束,返回参数为:[{}]", updateTmpDealLmtDetails03);

        logger.info("若3002-同业投资类、3006-同业管理类、15030101-穿透化额度已用金额>0，且额度已到期，则分项状态更新为99-失效未结清；开始,请求参数为:[{}]", openDay);
        int updateTmpDealLmtDetails04 = cmis0211Mapper.updateTmpDealLmtDetails04(openDay);
        logger.info("若3002-同业投资类、3006-同业管理类、15030101-穿透化额度已用金额>0，且额度已到期，则分项状态更新为99-失效未结清；结束,返回参数为:[{}]", updateTmpDealLmtDetails04);

        logger.info("承销类额度到期的，更新状态为99开始,请求参数为:[{}]", openDay);
        int updateTmpDealLmtDetails05 = cmis0211Mapper.updateTmpDealLmtDetails05(openDay);
        logger.info("承销类额度到期的，更新状态为99结束,返回参数为:[{}]", updateTmpDealLmtDetails05);


        logger.info("判断3001、3005、300601 以及单笔投资额度 是否额度已到期且已用金额>0，若是，则更新额度状态为90-失效未结清；开始,请求参数为:[{}]", openDay);
        int updateTmpDealLmtDetails02 = cmis0211Mapper.updateTmpDealLmtDetails02(openDay);
        logger.info("判断3001、3005、300601 以及单笔投资额度 是否额度已到期且已用金额>0，若是，则更新额度状态为90-失效未结清；结束,返回参数为:[{}]", updateTmpDealLmtDetails02);


        logger.info("判断3001、3005、300601 以及单笔投资额度是否已用金额为0且额度已到期，若是，则更新额度状态为99-失效已结清；开始,请求参数为:[{}]", openDay);
        int updateTmpDealLmtDetails01 = cmis0211Mapper.updateTmpDealLmtDetails01(openDay);
        logger.info("判断3001、3005、300601 以及单笔投资额度是否已用金额为0且额度已到期，若是，则更新额度状态为99-失效已结清；结束,返回参数为:[{}]", updateTmpDealLmtDetails01);


        // <!-- 1.根据limit_available_zjg推送额度分项编号关联白名单额度表SUB_ACC_NO额度分项编号更新SIG_USE_AMT已用限额为推送的已用金额； -->
        logger.info("根据limit_available_zjg推送额度分项编号关联白名单额度表SUB_ACC_NO额度分项编号更新SIG_USE_AMT已用限额为推送的已用金额");
        logger.info("清空加工占用授信表开始,请求参数为:[{}]", openDay);
        cmis0211Mapper.deleteLmtContRel31();
        logger.info("清空加工占用授信表结束");

        logger.info("插入加工占用授信表开始,请求参数为:[{}]", openDay);
        int insertLmtContRel31 = cmis0211Mapper.insertLmtContRel31(openDay);
        logger.info("插入加工占用授信表结束,返回参数为:[{}]", insertLmtContRel31);

        logger.info("更新分项占用关系信息开始,请求参数为:[{}]", openDay);
        int updateLmtContRel31 = cmis0211Mapper.updateLmtContRel31(openDay);
        logger.info("更新分项占用关系信息结束,返回参数为:[{}]", updateLmtContRel31);

        logger.info("更新分项占用关系信息加工表开始,请求参数为:[{}]", openDay);
        int updateTmpDealLmtContRel = cmis0211Mapper.updateTmpDealLmtContRel(openDay);
        logger.info("更新分项占用关系信息加工表结束,返回参数为:[{}]", updateTmpDealLmtContRel);
    }
}
