/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.bat;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: yusp-batch-core模块
 * @类名称: BatControlRun
 * @类描述: bat_control_run数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-09 17:12:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_control_run")
public class BatControlRun extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 营业日期  **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "OPEN_DAY")
	private String openDay;
	
	/** 最大线程数 **/
	@Column(name = "THREAD_NUM", unique = false, nullable = true, length = 10)
	private Integer threadNum;
	
	/** 调度预警时间 预计调度作业结束时间，超过此时间发送短信报警。格式HH24：MM **/
	@Column(name = "WARN_TIME", unique = false, nullable = true, length = 8)
	private String warnTime;
	
	/** 强制退出时间 调度作业强制退出的时间。格式HH24：MM **/
	@Column(name = "EXIT_TIME", unique = false, nullable = true, length = 8)
	private String exitTime;
	
	/** 等待时间（秒）调度作业循环处理任务的等待时间 **/
	@Column(name = "WAIT_TIME", unique = false, nullable = true, length = 10)
	private Integer waitTime;
	
	/** 是否已发送短信 STD_YES_NO **/
	@Column(name = "SMS_FLAG", unique = false, nullable = true, length = 1)
	private String smsFlag;
	
	/** 调度开始时间 格式 YYYY-MM-DD HH:mm:ss **/
	@Column(name = "BEGIN_TIME", unique = false, nullable = true, length = 20)
	private String beginTime;
	
	/** 调度结束时间 格式 YYYY-MM-DD HH:mm:ss **/
	@Column(name = "END_TIME", unique = false, nullable = true, length = 20)
	private String endTime;
	
	/** 调度状态 STD_CONTROL_STATUS **/
	@Column(name = "CONTROL_STATUS", unique = false, nullable = true, length = 3)
	private String controlStatus;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date updateTime;
	
	/** 上一营业日期 **/
	@Column(name = "LAST_OPEN_DAY", unique = false, nullable = true, length = 10)
	private String lastOpenDay;
	
	
	/**
	 * @param openDay
	 */
	public void setOpenDay(String openDay) {
		this.openDay = openDay;
	}
	
    /**
     * @return openDay
     */
	public String getOpenDay() {
		return this.openDay;
	}
	
	/**
	 * @param threadNum
	 */
	public void setThreadNum(Integer threadNum) {
		this.threadNum = threadNum;
	}
	
    /**
     * @return threadNum
     */
	public Integer getThreadNum() {
		return this.threadNum;
	}
	
	/**
	 * @param warnTime
	 */
	public void setWarnTime(String warnTime) {
		this.warnTime = warnTime;
	}
	
    /**
     * @return warnTime
     */
	public String getWarnTime() {
		return this.warnTime;
	}
	
	/**
	 * @param exitTime
	 */
	public void setExitTime(String exitTime) {
		this.exitTime = exitTime;
	}
	
    /**
     * @return exitTime
     */
	public String getExitTime() {
		return this.exitTime;
	}
	
	/**
	 * @param waitTime
	 */
	public void setWaitTime(Integer waitTime) {
		this.waitTime = waitTime;
	}
	
    /**
     * @return waitTime
     */
	public Integer getWaitTime() {
		return this.waitTime;
	}
	
	/**
	 * @param smsFlag
	 */
	public void setSmsFlag(String smsFlag) {
		this.smsFlag = smsFlag;
	}
	
    /**
     * @return smsFlag
     */
	public String getSmsFlag() {
		return this.smsFlag;
	}
	
	/**
	 * @param beginTime
	 */
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	
    /**
     * @return beginTime
     */
	public String getBeginTime() {
		return this.beginTime;
	}
	
	/**
	 * @param endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
    /**
     * @return endTime
     */
	public String getEndTime() {
		return this.endTime;
	}
	
	/**
	 * @param controlStatus
	 */
	public void setControlStatus(String controlStatus) {
		this.controlStatus = controlStatus;
	}
	
    /**
     * @return controlStatus
     */
	public String getControlStatus() {
		return this.controlStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param lastOpenDay
	 */
	public void setLastOpenDay(String lastOpenDay) {
		this.lastOpenDay = lastOpenDay;
	}
	
    /**
     * @return lastOpenDay
     */
	public String getLastOpenDay() {
		return this.lastOpenDay;
	}


}