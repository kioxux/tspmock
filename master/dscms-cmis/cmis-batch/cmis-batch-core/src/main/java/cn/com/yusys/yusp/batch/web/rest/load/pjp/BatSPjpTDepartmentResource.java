/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.pjp;

import cn.com.yusys.yusp.batch.domain.load.pjp.BatSPjpTDepartment;
import cn.com.yusys.yusp.batch.service.load.pjp.BatSPjpTDepartmentService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpTDepartmentResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-15 16:47:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batspjptdepartment")
public class BatSPjpTDepartmentResource {
    @Autowired
    private BatSPjpTDepartmentService batSPjpTDepartmentService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSPjpTDepartment>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSPjpTDepartment> list = batSPjpTDepartmentService.selectAll(queryModel);
        return new ResultDto<List<BatSPjpTDepartment>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSPjpTDepartment>> index(QueryModel queryModel) {
        List<BatSPjpTDepartment> list = batSPjpTDepartmentService.selectByModel(queryModel);
        return new ResultDto<List<BatSPjpTDepartment>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{id}")
    protected ResultDto<BatSPjpTDepartment> show(@PathVariable("id") String id) {
        BatSPjpTDepartment batSPjpTDepartment = batSPjpTDepartmentService.selectByPrimaryKey(id);
        return new ResultDto<BatSPjpTDepartment>(batSPjpTDepartment);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSPjpTDepartment> create(@RequestBody BatSPjpTDepartment batSPjpTDepartment) throws URISyntaxException {
        batSPjpTDepartmentService.insert(batSPjpTDepartment);
        return new ResultDto<BatSPjpTDepartment>(batSPjpTDepartment);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSPjpTDepartment batSPjpTDepartment) throws URISyntaxException {
        int result = batSPjpTDepartmentService.update(batSPjpTDepartment);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{id}")
    protected ResultDto<Integer> delete(@PathVariable("id") String id) {
        int result = batSPjpTDepartmentService.deleteByPrimaryKey(id);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSPjpTDepartmentService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
