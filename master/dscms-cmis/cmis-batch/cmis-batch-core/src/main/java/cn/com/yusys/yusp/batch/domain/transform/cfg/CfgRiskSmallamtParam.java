/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.transform.cfg;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: CfgRiskSmallamtParam
 * @类描述: cfg_risk_smallamt_param数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-10 21:55:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_risk_smallamt_param")
public class CfgRiskSmallamtParam extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "pk_id")
	private String pkId;
	
	/** 信用等级评定 **/
	@Column(name = "CUS_CRD_GRADE", unique = false, nullable = true, length = 10)
	private String cusCrdGrade;
	
	/** 担保方式 STD_ZB_GUAR_WAY **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 逾期天数最小值 **/
	@Column(name = "OVERDUE_DAY_MIN", unique = false, nullable = true, length = 10)
	private Integer overdueDayMin;
	
	/** 逾期天数最大值 **/
	@Column(name = "OVERDUE_DAY_MAX", unique = false, nullable = true, length = 10)
	private Integer overdueDayMax;
	
	/** 分类状态 STD_FIVE_CLASS **/
	@Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String fiveClass;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusCrdGrade
	 */
	public void setCusCrdGrade(String cusCrdGrade) {
		this.cusCrdGrade = cusCrdGrade;
	}
	
    /**
     * @return cusCrdGrade
     */
	public String getCusCrdGrade() {
		return this.cusCrdGrade;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param overdueDayMin
	 */
	public void setOverdueDayMin(Integer overdueDayMin) {
		this.overdueDayMin = overdueDayMin;
	}
	
    /**
     * @return overdueDayMin
     */
	public Integer getOverdueDayMin() {
		return this.overdueDayMin;
	}
	
	/**
	 * @param overdueDayMax
	 */
	public void setOverdueDayMax(Integer overdueDayMax) {
		this.overdueDayMax = overdueDayMax;
	}
	
    /**
     * @return overdueDayMax
     */
	public Integer getOverdueDayMax() {
		return this.overdueDayMax;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
	
    /**
     * @return fiveClass
     */
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}