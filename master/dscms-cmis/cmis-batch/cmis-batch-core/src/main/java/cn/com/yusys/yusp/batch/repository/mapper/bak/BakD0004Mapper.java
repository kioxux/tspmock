package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKD0004</br>
 * 任务名称：批前备份日表任务-备份贴现台账[ACC_DISC]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakD0004Mapper {

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertCurrent(@Param("openDay") String openDay);

    /**
     * 删除当天的数据
     *
     * @return
     */
    void truncateCurrent();

    /**
     * 删除当天的数据
     *
     * @param openDay
     * @return
     */
    int deleteCurrent(@Param("openDay") String openDay);


    /**
     * 查询待删除当天的[贴现台账[ACC_DISC]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD00004DeleteOpenDayAccDiscCounts(@Param("openDay") String openDay);


    /**
     * 查询当天的[贴现台账[ACC_DISC]]原表数据总条数
     *
     * @param openDay
     * @return
     */
    int queryAccDiscConts(@Param("openDay") String openDay);
}
