/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.gjp;

import cn.com.yusys.yusp.batch.domain.load.gjp.BatSGjpTfbComMgnReg;
import cn.com.yusys.yusp.batch.repository.mapper.load.gjp.BatSGjpTfbComMgnRegMapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGjpTfbComMgnRegService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-11-07 15:51:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatSGjpTfbComMgnRegService {
    private static final Logger logger = LoggerFactory.getLogger(BatSGjpTfbComMgnRegService.class);
    @Autowired
    private BatSGjpTfbComMgnRegMapper batSGjpTfbComMgnRegMapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatSGjpTfbComMgnReg selectByPrimaryKey(String id) {
        return batSGjpTfbComMgnRegMapper.selectByPrimaryKey(id);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatSGjpTfbComMgnReg> selectAll(QueryModel model) {
        List<BatSGjpTfbComMgnReg> records = (List<BatSGjpTfbComMgnReg>) batSGjpTfbComMgnRegMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatSGjpTfbComMgnReg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatSGjpTfbComMgnReg> list = batSGjpTfbComMgnRegMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatSGjpTfbComMgnReg record) {
        return batSGjpTfbComMgnRegMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatSGjpTfbComMgnReg record) {
        return batSGjpTfbComMgnRegMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatSGjpTfbComMgnReg record) {
        return batSGjpTfbComMgnRegMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatSGjpTfbComMgnReg record) {
        return batSGjpTfbComMgnRegMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String id) {
        return batSGjpTfbComMgnRegMapper.deleteByPrimaryKey(id);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batSGjpTfbComMgnRegMapper.deleteByIds(ids);
    }


    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int deleteByOpenDay(String openDay) {
        // 重建相关表
        logger.info("重建保证金登记主表[gjp_tfb_com_mgn_reg]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "bat_s_gjp_tfb_com_mgn_reg");
        logger.info("重建保证金登记主表[gjp_tfb_com_mgn_reg]结束");
        return 0;
    }
}
