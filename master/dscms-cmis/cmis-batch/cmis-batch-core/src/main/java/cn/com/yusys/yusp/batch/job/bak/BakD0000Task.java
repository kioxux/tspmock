package cn.com.yusys.yusp.batch.job.bak;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bak.BakD0000Service;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepLmtEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKD0000</br>
 * 任务名称：批前备份日表任务-删除N天前数据</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午11:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
@Deprecated
public class BakD0000Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(BakD0000Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private BakD0000Service bakD0000Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job bakD0000Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0000_JOB.key, JobStepLmtEnum.BAKD0000_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job bakD0000Job = this.jobBuilderFactory.get(JobStepLmtEnum.BAKD0000_JOB.key)
                .start(bakD0000UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(bakD0000CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                // 将数据量大的放在前面优先删除 开始
                .next(bakD0002Delete3DaysAccAccpDrftSubStep(WILL_BE_INJECTED)) //银承台账票据明细 [ACC_ACCP_DRFT_SUB]-删除3天前的数据
                .next(bakD0006Delete3DaysAccLoanStep(WILL_BE_INJECTED)) //备份贷款台账信息表[ACC_LOAN]-删除3天前的数据
                .next(bakD0010Delete3DaysApprLmtSubBasicInfoStep(WILL_BE_INJECTED)) //备份批复额度分项基础信息[APPR_LMT_SUB_BASIC_INFO]-删除3天前的数据
                .next(bakD0025Delete3DaysGrtGuarContStep(WILL_BE_INJECTED)) //备份担保合同表[GRT_GUAR_CONT]-删除3天前的数据
                .next(bakD0011Delete3DaysApprStrMtableInfoStep(WILL_BE_INJECTED)) //备份批复主信息[APPR_STR_MTABLE_INFO]-删除3天前的数据
                .next(bakD0022Delete3DaysCfgBankInfoStep(WILL_BE_INJECTED)) //行名行号对照表[CFG_BANK_INFO]-删除3天前的数据
                .next(bakD0026Delete3DaysLmtContRelStep(WILL_BE_INJECTED)) //备份分项占用关系信息[LMT_CONT_REL]-删除3天前的数据
                .next(bakD0004Delete3DaysAccDiscStep(WILL_BE_INJECTED)) //备份贴现台账[ACC_DISC]-删除3天前的数据
                // 将数据量大的放在前面优先删除 结束
                .next(bakD0001Delete3DaysAccAccpStep(WILL_BE_INJECTED)) // 备份银承台账[ACC_ACCP]-删除3天前的数据
                .next(bakD0003Delete3DaysAccCvrsStep(WILL_BE_INJECTED)) //备份保函台账[ACC_CVRS]-删除3天前的数据
                .next(bakD0005Delete3DaysAccEntrustLoanStep(WILL_BE_INJECTED)) //备份委托贷款台账[ACC_ENTRUST_LOAN]-删除3天前的数据
                .next(bakD0007Delete3DaysAccTfLocStep(WILL_BE_INJECTED)) //备份开证台账[ACC_TF_LOC]-删除3天前的数据
                .next(bakD0008Delete3DaysApprCoopInfoStep(WILL_BE_INJECTED)) //备份合作方授信台账信息[APPR_COOP_INFO]-删除3天前的数据
                .next(bakD0009Delete3DaysApprCoopSubStep(WILL_BE_INJECTED)) //备份合作方授信分项信息[APPR_COOP_SUB_INFO]-删除3天前的数据
                .next(bakD0012Delete3DaysApprStrOrgInfoStep(WILL_BE_INJECTED)) //备份批复适用机构[APPR_STR_ORG_INFO]-删除3天前的数据
                .next(bakD0013Delete3DaysContAccRelStep(WILL_BE_INJECTED)) //备份合同占用关系信息[CONT_ACC_REL]-删除3天前的数据
                .next(bakD0014Delete3DaysCtrAccpContStep(WILL_BE_INJECTED)) //备份银承合同详情[CTR_ACCP_CONT]-删除3天前的数据
                .next(bakD0015Delete3DaysCtrAsplDetailsStep(WILL_BE_INJECTED)) //备份资产池协议[CTR_ASPL_DETAILS]-删除3天前的数据
                .next(bakD0016Delete3DaysCtrCvrgContStep(WILL_BE_INJECTED)) //备份保函协议详情[CTR_CVRG_CONT]-删除3天前的数据
                .next(bakD0017Delete3DaysCtrDiscContStep(WILL_BE_INJECTED)) //备份贴现协议详情[CTR_DISC_CONT]-删除3天前的数据
                .next(bakD0018Delete3DaysCtrDiscPorderSubStep(WILL_BE_INJECTED)) //备份贴现协议汇票明细[CTR_DISC_PORDER_SUB]-删除3天前的数据
                .next(bakD0019Delete3DaysCtrEntrustLoanContStep(WILL_BE_INJECTED)) //备份委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]-删除3天前的数据
                .next(bakD0020Delete3DaysCtrHighAmtAgrContStep(WILL_BE_INJECTED)) //备份最高额授信协议[CTR_HIGH_AMT_AGR_CONT]-删除3天前的数据
                .next(bakD0021Delete3DaysCtrLoanContStep(WILL_BE_INJECTED)) //备份贷款合同表[CTR_LOAN_CONT]-删除3天前的数据
                .next(bakD0023Delete3DaysCtrTfLocContStep(WILL_BE_INJECTED)) //备份开证合同详情[CTR_TF_LOC_CONT]-删除3天前的数据
                .next(bakD0024Delete3DaysGuarWarrantInfoStep(WILL_BE_INJECTED)) //备份权证台账[GUAR_WARRANT_INFO]-删除3天前的数据
                .next(bakD0027Delete3DaysLmtWhiteInfoStep(WILL_BE_INJECTED)) //备份白名单额度信息[LMT_WHITE_INFO]-删除3天前的数据
                .next(bakD0028Delete3DaysLmtWhiteInfoHistoryStep(WILL_BE_INJECTED)) //备份白名单额度信息历史[LMT_WHITE_INFO_HISTORY]-删除3天前的数据
                .next(bakD0029Delete3DaysTmpGjpLmtCvrgRzkzStep(WILL_BE_INJECTED))//备份我行代开他行保函[TMP_GJP_LMT_CVRG_RZKZ]-删除3天前的数据
                .next(bakD0030Delete3DaysTmpGjpLmtPeerRzkzStep(WILL_BE_INJECTED))//备份我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]-删除3天前的数据
                .next(bakD0031Delete3DaysTmpGjpLmtFftRzkzStep(WILL_BE_INJECTED))//备份福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]-删除3天前的数据
                .next(bakD0000UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return bakD0000Job;
    }


    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0000UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0000_UPDATE_TASK010_STEP.key, JobStepLmtEnum.BAKD0000_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0000UpdateTask010Step = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0000_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BAKD0000_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0000_UPDATE_TASK010_STEP.key, JobStepLmtEnum.BAKD0000_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0000UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0000CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0000_CHECK_REL_STEP.key, JobStepLmtEnum.BAKD0000_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0000CheckRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0000_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BAKD0000_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.BAKD0000_CHECK_REL_STEP.key, JobStepLmtEnum.BAKD0000_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    try {
                        TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.BAKD0000_CHECK_REL_STEP.key, JobStepLmtEnum.BAKD0000_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return bakD0000CheckRelStep;
    }

    /**
     * 备份银承台账[ACC_ACCP]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0001Delete3DaysAccAccpStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0001_DELETE_3DAYS_ACC_ACCP_STEP.key, JobStepLmtEnum.BAKD0001_DELETE_3DAYS_ACC_ACCP_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0001Delete3DaysAccAccpStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0001_DELETE_3DAYS_ACC_ACCP_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0001Delete3DaysAccAccp(openDay);// 备份银承台账[ACC_ACCP]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0001_DELETE_3DAYS_ACC_ACCP_STEP.key, JobStepLmtEnum.BAKD0001_DELETE_3DAYS_ACC_ACCP_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0001Delete3DaysAccAccpStep;
    }

    /**
     * 备份银承台账票据明细 [ACC_ACCP_DRFT_SUB]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0002Delete3DaysAccAccpDrftSubStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0002_DELETE_3DAYS_ACC_ACCP_DRFT_SUB_STEP.key, JobStepLmtEnum.BAKD0002_DELETE_3DAYS_ACC_ACCP_DRFT_SUB_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0002Delete3DaysAccAccpDrftSubStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0002_DELETE_3DAYS_ACC_ACCP_DRFT_SUB_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0002Delete3DaysAccAccpDrftSub(openDay);// 备份银承台账票据明细 [ACC_ACCP_DRFT_SUB]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0002_DELETE_3DAYS_ACC_ACCP_DRFT_SUB_STEP.key, JobStepLmtEnum.BAKD0002_DELETE_3DAYS_ACC_ACCP_DRFT_SUB_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0002Delete3DaysAccAccpDrftSubStep;
    }

    /**
     * 备份保函台账[ACC_CVRS]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0003Delete3DaysAccCvrsStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0003_DELETE_3DAYS_ACC_CVRS_STEP.key, JobStepLmtEnum.BAKD0003_DELETE_3DAYS_ACC_CVRS_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0003Delete3DaysAccCvrsStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0003_DELETE_3DAYS_ACC_CVRS_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0003Delete3DaysAccCvrs(openDay);// 备份保函台账[ACC_CVRS]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0003_DELETE_3DAYS_ACC_CVRS_STEP.key, JobStepLmtEnum.BAKD0003_DELETE_3DAYS_ACC_CVRS_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0003Delete3DaysAccCvrsStep;
    }

    /**
     * 备份贴现台账[ACC_DISC]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0004Delete3DaysAccDiscStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0004_DELETE_3DAYS_ACC_DISC_STEP.key, JobStepLmtEnum.BAKD0004_DELETE_3DAYS_ACC_DISC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0004Delete3DaysAccDiscStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0004_DELETE_3DAYS_ACC_DISC_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0004Delete3DaysAccDisc(openDay);// 备份贴现台账[ACC_DISC]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0004_DELETE_3DAYS_ACC_DISC_STEP.key, JobStepLmtEnum.BAKD0004_DELETE_3DAYS_ACC_DISC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0004Delete3DaysAccDiscStep;
    }

    /**
     * 备份委托贷款台账[ACC_ENTRUST_LOAN]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0005Delete3DaysAccEntrustLoanStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0005_DELETE_3DAYS_ACC_ENTRUST_LOAN_STEP.key, JobStepLmtEnum.BAKD0005_DELETE_3DAYS_ACC_ENTRUST_LOAN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0005Delete3DaysAccEntrustLoanStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0005_DELETE_3DAYS_ACC_ENTRUST_LOAN_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0005Delete3DaysAccEntrustLoan(openDay);// 备份委托贷款台账[ACC_ENTRUST_LOAN]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0005_DELETE_3DAYS_ACC_ENTRUST_LOAN_STEP.key, JobStepLmtEnum.BAKD0005_DELETE_3DAYS_ACC_ENTRUST_LOAN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0005Delete3DaysAccEntrustLoanStep;
    }

    /**
     * 备份贷款台账信息表[ACC_LOAN]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0006Delete3DaysAccLoanStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0006_DELETE_3DAYS_ACC_LOAN_STEP.key, JobStepLmtEnum.BAKD0006_DELETE_3DAYS_ACC_LOAN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0006Delete3DaysAccLoanStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0006_DELETE_3DAYS_ACC_LOAN_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0006Delete3DaysAccLoan(openDay);// 备份贷款台账信息表[ACC_LOAN]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0006_DELETE_3DAYS_ACC_LOAN_STEP.key, JobStepLmtEnum.BAKD0006_DELETE_3DAYS_ACC_LOAN_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0006Delete3DaysAccLoanStep;
    }

    /**
     * 备份开证台账[ACC_TF_LOC]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0007Delete3DaysAccTfLocStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0007_DELETE_3DAYS_ACC_TF_LOC_STEP.key, JobStepLmtEnum.BAKD0007_DELETE_3DAYS_ACC_TF_LOC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0007Delete3DaysAccTfLocStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0007_DELETE_3DAYS_ACC_TF_LOC_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0007Delete3DaysAccTfLoc(openDay);// 备份开证台账[ACC_TF_LOC]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0007_DELETE_3DAYS_ACC_TF_LOC_STEP.key, JobStepLmtEnum.BAKD0007_DELETE_3DAYS_ACC_TF_LOC_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0007Delete3DaysAccTfLocStep;
    }

    /**
     * 备份合作方授信台账信息[APPR_COOP_INFO] -删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0008Delete3DaysApprCoopInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0008_DELETE_3DAYS_APPR_COOP_INFO_STEP.key, JobStepLmtEnum.BAKD0008_DELETE_3DAYS_APPR_COOP_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0008Delete3DaysApprCoopInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0008_DELETE_3DAYS_APPR_COOP_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0008Delete3DaysApprCoopInfo(openDay);// 备份合作方授信台账信息[APPR_COOP_INFO] -删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0008_DELETE_3DAYS_APPR_COOP_INFO_STEP.key, JobStepLmtEnum.BAKD0008_DELETE_3DAYS_APPR_COOP_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0008Delete3DaysApprCoopInfoStep;
    }

    /**
     * 备份合作方授信分项信息[APPR_COOP_SUB_INFO]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0009Delete3DaysApprCoopSubStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0009_DELETE_3DAYS_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.BAKD0009_DELETE_3DAYS_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0009Delete3DaysApprCoopSubStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0009_DELETE_3DAYS_APPR_COOP_SUB_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0009Delete3DaysApprCoopSub(openDay);// 备份合作方授信分项信息[APPR_COOP_SUB_INFO]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0009_DELETE_3DAYS_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.BAKD0009_DELETE_3DAYS_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0009Delete3DaysApprCoopSubStep;
    }

    /**
     * 备份批复额度分项基础信息[APPR_LMT_SUB_BASIC_INFO] -删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0010Delete3DaysApprLmtSubBasicInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0010_DELETE_3DAYS_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepLmtEnum.BAKD0010_DELETE_3DAYS_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0010Delete3DaysApprLmtSubBasicInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0010_DELETE_3DAYS_APPR_LMT_SUB_BASIC_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0010Delete3DaysApprLmtSubBasicInfo(openDay);// 备份批复额度分项基础信息[APPR_LMT_SUB_BASIC_INFO] -删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0010_DELETE_3DAYS_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepLmtEnum.BAKD0010_DELETE_3DAYS_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0010Delete3DaysApprLmtSubBasicInfoStep;
    }

    /**
     * 备份批复主信息[APPR_STR_MTABLE_INFO]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0011Delete3DaysApprStrMtableInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0011_DELETE_3DAYS_APPR_STR_MTABLE_INFO_STEP.key, JobStepLmtEnum.BAKD0011_DELETE_3DAYS_APPR_STR_MTABLE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0011Delete3DaysApprStrMtableInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0011_DELETE_3DAYS_APPR_STR_MTABLE_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0011Delete3DaysApprStrMtableInfo(openDay);// 备份批复主信息[APPR_STR_MTABLE_INFO]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0011_DELETE_3DAYS_APPR_STR_MTABLE_INFO_STEP.key, JobStepLmtEnum.BAKD0011_DELETE_3DAYS_APPR_STR_MTABLE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0011Delete3DaysApprStrMtableInfoStep;
    }

    /**
     * 备份批复适用机构[APPR_STR_ORG_INFO]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0012Delete3DaysApprStrOrgInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0012_DELETE_3DAYS_APPR_STR_ORG_INFO_STEP.key, JobStepLmtEnum.BAKD0012_DELETE_3DAYS_APPR_STR_ORG_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0012Delete3DaysApprStrOrgInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0012_DELETE_3DAYS_APPR_STR_ORG_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0012Delete3DaysApprStrOrgInfo(openDay);//备份批复适用机构[APPR_STR_ORG_INFO]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0012_DELETE_3DAYS_APPR_STR_ORG_INFO_STEP.key, JobStepLmtEnum.BAKD0012_DELETE_3DAYS_APPR_STR_ORG_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0012Delete3DaysApprStrOrgInfoStep;
    }

    /**
     * 备份合同占用关系信息[CONT_ACC_REL]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0013Delete3DaysContAccRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0013_DELETE_3DAYS_CONT_ACC_REL_STEP.key, JobStepLmtEnum.BAKD0013_DELETE_3DAYS_CONT_ACC_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0013Delete3DaysContAccRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0013_DELETE_3DAYS_CONT_ACC_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0013Delete3DaysContAccRel(openDay);//备份合同占用关系信息[CONT_ACC_REL]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0013_DELETE_3DAYS_CONT_ACC_REL_STEP.key, JobStepLmtEnum.BAKD0013_DELETE_3DAYS_CONT_ACC_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0013Delete3DaysContAccRelStep;
    }

    /**
     * 备份银承合同详情[CTR_ACCP_CONT]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0014Delete3DaysCtrAccpContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0014_DELETE_3DAYS_CTR_ACCP_CONT_STEP.key, JobStepLmtEnum.BAKD0014_DELETE_3DAYS_CTR_ACCP_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0014Delete3DaysCtrAccpContStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0014_DELETE_3DAYS_CTR_ACCP_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0014Delete3DaysCtrAccpCont(openDay);// 备份银承合同详情[CTR_ACCP_CONT]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0014_DELETE_3DAYS_CTR_ACCP_CONT_STEP.key, JobStepLmtEnum.BAKD0014_DELETE_3DAYS_CTR_ACCP_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0014Delete3DaysCtrAccpContStep;
    }

    /**
     * 备份资产池协议[CTR_ASPL_DETAILS]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0015Delete3DaysCtrAsplDetailsStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0015_DELETE_3DAYS_CTR_ASPL_DETAILS_STEP.key, JobStepLmtEnum.BAKD0015_DELETE_3DAYS_CTR_ASPL_DETAILS_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0015Delete3DaysCtrAsplDetailsStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0015_DELETE_3DAYS_CTR_ASPL_DETAILS_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0015Delete3DaysCtrAsplDetails(openDay);// 备份资产池协议[CTR_ASPL_DETAILS]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0015_DELETE_3DAYS_CTR_ASPL_DETAILS_STEP.key, JobStepLmtEnum.BAKD0015_DELETE_3DAYS_CTR_ASPL_DETAILS_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0015Delete3DaysCtrAsplDetailsStep;
    }

    /**
     * 备份保函协议详情[CTR_CVRG_CONT]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0016Delete3DaysCtrCvrgContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0016_DELETE_3DAYS_CTR_CVRG_CONT_STEP.key, JobStepLmtEnum.BAKD0016_DELETE_3DAYS_CTR_CVRG_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0016Delete3DaysCtrCvrgContStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0016_DELETE_3DAYS_CTR_CVRG_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0016Delete3DaysCtrCvrgCont(openDay);// 备份保函协议详情[CTR_CVRG_CONT]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0016_DELETE_3DAYS_CTR_CVRG_CONT_STEP.key, JobStepLmtEnum.BAKD0016_DELETE_3DAYS_CTR_CVRG_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0016Delete3DaysCtrCvrgContStep;
    }

    /**
     * 备份贴现协议详情[CTR_DISC_CONT]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0017Delete3DaysCtrDiscContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0017_DELETE_3DAYS_CTR_DISC_CONT_STEP.key, JobStepLmtEnum.BAKD0017_DELETE_3DAYS_CTR_DISC_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0017Delete3DaysCtrDiscContStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0017_DELETE_3DAYS_CTR_DISC_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0017Delete3DaysCtrDiscCont(openDay);// 备份贴现协议详情[CTR_DISC_CONT]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0017_DELETE_3DAYS_CTR_DISC_CONT_STEP.key, JobStepLmtEnum.BAKD0017_DELETE_3DAYS_CTR_DISC_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0017Delete3DaysCtrDiscContStep;
    }

    /**
     * 备份贴现协议汇票明细[CTR_DISC_PORDER_SUB]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0018Delete3DaysCtrDiscPorderSubStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0018_DELETE_3DAYS_CTR_DISC_PORDER_SUB_STEP.key, JobStepLmtEnum.BAKD0018_DELETE_3DAYS_CTR_DISC_PORDER_SUB_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0018Delete3DaysCtrDiscPorderSubStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0018_DELETE_3DAYS_CTR_DISC_PORDER_SUB_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0018Delete3DaysCtrDiscPorderSub(openDay);// 备份贴现协议汇票明细[CTR_DISC_PORDER_SUB]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0018_DELETE_3DAYS_CTR_DISC_PORDER_SUB_STEP.key, JobStepLmtEnum.BAKD0018_DELETE_3DAYS_CTR_DISC_PORDER_SUB_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0018Delete3DaysCtrDiscPorderSubStep;
    }

    /**
     * 备份委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0019Delete3DaysCtrEntrustLoanContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0019_DELETE_3DAYS_CTR_ENTRUST_LOAN_CONT_STEP.key, JobStepLmtEnum.BAKD0019_DELETE_3DAYS_CTR_ENTRUST_LOAN_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0019Delete3DaysCtrEntrustLoanContStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0019_DELETE_3DAYS_CTR_ENTRUST_LOAN_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0019Delete3DaysCtrEntrustLoanCont(openDay);// 备份委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0019_DELETE_3DAYS_CTR_ENTRUST_LOAN_CONT_STEP.key, JobStepLmtEnum.BAKD0019_DELETE_3DAYS_CTR_ENTRUST_LOAN_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0019Delete3DaysCtrEntrustLoanContStep;
    }

    /**
     * 备份最高额授信协议[CTR_HIGH_AMT_AGR_CONT]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0020Delete3DaysCtrHighAmtAgrContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0020_DELETE_3DAYS_CTR_HIGH_AMT_AGR_CONT_STEP.key, JobStepLmtEnum.BAKD0020_DELETE_3DAYS_CTR_HIGH_AMT_AGR_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0020Delete3DaysCtrHighAmtAgrContStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0020_DELETE_3DAYS_CTR_HIGH_AMT_AGR_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0020Delete3DaysCtrHighAmtAgrCont(openDay);// 备份最高额授信协议[CTR_HIGH_AMT_AGR_CONT]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0020_DELETE_3DAYS_CTR_HIGH_AMT_AGR_CONT_STEP.key, JobStepLmtEnum.BAKD0020_DELETE_3DAYS_CTR_HIGH_AMT_AGR_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0020Delete3DaysCtrHighAmtAgrContStep;
    }

    /**
     * 备份贷款合同表[CTR_LOAN_CONT]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0021Delete3DaysCtrLoanContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0021_DELETE_3DAYS_CTR_LOAN_CONT_STEP.key, JobStepLmtEnum.BAKD0021_DELETE_3DAYS_CTR_LOAN_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0021Delete3DaysCtrLoanContStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0021_DELETE_3DAYS_CTR_LOAN_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0021Delete3DaysCtrLoanCont(openDay);// 备份贷款合同表[CTR_LOAN_CONT]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0021_DELETE_3DAYS_CTR_LOAN_CONT_STEP.key, JobStepLmtEnum.BAKD0021_DELETE_3DAYS_CTR_LOAN_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0021Delete3DaysCtrLoanContStep;
    }

    /**
     * 备份行名行号对照表[CFG_BANK_INFO]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0022Delete3DaysCfgBankInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0022_DELETE_3DAYS_CFG_BANK_INFO_STEP.key, JobStepLmtEnum.BAKD0022_DELETE_3DAYS_CFG_BANK_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0022Delete3DaysCfgBankInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0022_DELETE_3DAYS_CFG_BANK_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0022Delete3DaysCfgBankInfo(openDay);// 行名行号对照表[CFG_BANK_INFO]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0022_DELETE_3DAYS_CFG_BANK_INFO_STEP.key, JobStepLmtEnum.BAKD0022_DELETE_3DAYS_CFG_BANK_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0022Delete3DaysCfgBankInfoStep;
    }

    /**
     * 备份开证合同详情[CTR_TF_LOC_CONT]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0023Delete3DaysCtrTfLocContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0023_DELETE_3DAYS_CTR_TF_LOC_CONT_STEP.key, JobStepLmtEnum.BAKD0023_DELETE_3DAYS_CTR_TF_LOC_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0023Delete3DaysCtrTfLocContStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0023_DELETE_3DAYS_CTR_TF_LOC_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0023Delete3DaysCtrTfLocCont(openDay);// 备份开证合同详情[CTR_TF_LOC_CONT]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0023_DELETE_3DAYS_CTR_TF_LOC_CONT_STEP.key, JobStepLmtEnum.BAKD0023_DELETE_3DAYS_CTR_TF_LOC_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0023Delete3DaysCtrTfLocContStep;
    }

    /**
     * 备份权证台账[GUAR_WARRANT_INFO]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0024Delete3DaysGuarWarrantInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0024_DELETE_3DAYS_GUAR_WARRANT_INFO_STEP.key, JobStepLmtEnum.BAKD0024_DELETE_3DAYS_GUAR_WARRANT_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0024Delete3DaysGuarWarrantInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0024_DELETE_3DAYS_GUAR_WARRANT_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0024Delete3DaysGuarWarrantInfo(openDay);// 备份权证台账[GUAR_WARRANT_INFO]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0024_DELETE_3DAYS_GUAR_WARRANT_INFO_STEP.key, JobStepLmtEnum.BAKD0024_DELETE_3DAYS_GUAR_WARRANT_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0024Delete3DaysGuarWarrantInfoStep;
    }

    /**
     * 备份担保合同表[GRT_GUAR_CONT]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0025Delete3DaysGrtGuarContStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_DELETE_3DAYS_GRT_GUAR_CONT_STEP.key, JobStepLmtEnum.BAKD0025_DELETE_3DAYS_GRT_GUAR_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0025Delete3DaysGrtGuarContStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0025_DELETE_3DAYS_GRT_GUAR_CONT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0025Delete3DaysGrtGuarCont(openDay);// 备份担保合同表[GRT_GUAR_CONT]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0025_DELETE_3DAYS_GRT_GUAR_CONT_STEP.key, JobStepLmtEnum.BAKD0025_DELETE_3DAYS_GRT_GUAR_CONT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0025Delete3DaysGrtGuarContStep;
    }

    /**
     * 备份分项占用关系信息[LMT_CONT_REL]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0026Delete3DaysLmtContRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0026_DELETE_3DAYS_LMT_CONT_REL_STEP.key, JobStepLmtEnum.BAKD0026_DELETE_3DAYS_LMT_CONT_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0026Delete3DaysLmtContRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0026_DELETE_3DAYS_LMT_CONT_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0026Delete3DaysLmtContRel(openDay);// 备份分项占用关系信息[LMT_CONT_REL]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0026_DELETE_3DAYS_LMT_CONT_REL_STEP.key, JobStepLmtEnum.BAKD0026_DELETE_3DAYS_LMT_CONT_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0026Delete3DaysLmtContRelStep;
    }

    /**
     * 备份白名单额度信息[LMT_WHITE_INFO]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0027Delete3DaysLmtWhiteInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0027_DELETE_3DAYS_LMT_WHITE_INFO_STEP.key, JobStepLmtEnum.BAKD0027_DELETE_3DAYS_LMT_WHITE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0027Delete3DaysLmtWhiteInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0027_DELETE_3DAYS_LMT_WHITE_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0027Delete3DaysLmtWhiteInfo(openDay);// 备份白名单额度信息[LMT_WHITE_INFO]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0027_DELETE_3DAYS_LMT_WHITE_INFO_STEP.key, JobStepLmtEnum.BAKD0027_DELETE_3DAYS_LMT_WHITE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0027Delete3DaysLmtWhiteInfoStep;
    }

    /**
     * 备份白名单额度信息历史[LMT_WHITE_INFO_HISTORY]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0028Delete3DaysLmtWhiteInfoHistoryStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0028_DELETE_3DAYS_LMT_WHITE_INFO_HISTORY_STEP.key, JobStepLmtEnum.BAKD0028_DELETE_3DAYS_LMT_WHITE_INFO_HISTORY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0028Delete3DaysLmtWhiteInfoHistoryStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0028_DELETE_3DAYS_LMT_WHITE_INFO_HISTORY_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0028Delete3DaysLmtWhiteInfoHistory(openDay);// 备份白名单额度信息历史[LMT_WHITE_INFO_HISTORY]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0028_DELETE_3DAYS_LMT_WHITE_INFO_HISTORY_STEP.key, JobStepLmtEnum.BAKD0028_DELETE_3DAYS_LMT_WHITE_INFO_HISTORY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0028Delete3DaysLmtWhiteInfoHistoryStep;
    }

    /**
     * 备份我行代开他行保函[TMP_GJP_LMT_CVRG_RZKZ]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0029Delete3DaysTmpGjpLmtCvrgRzkzStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0029_DELETE_3DAYS_TMP_GJP_LMT_CVRG_RZKZ_STEP.key, JobStepLmtEnum.BAKD0029_DELETE_3DAYS_TMP_GJP_LMT_CVRG_RZKZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0029Delete3DaysTmpGjpLmtCvrgRzkzStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0029_DELETE_3DAYS_TMP_GJP_LMT_CVRG_RZKZ_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0029Delete3DaysTmpGjpLmtCvrgRzkz(openDay);// 备份白名单额度信息历史[LMT_WHITE_INFO_HISTORY]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0029_DELETE_3DAYS_TMP_GJP_LMT_CVRG_RZKZ_STEP.key, JobStepLmtEnum.BAKD0029_DELETE_3DAYS_TMP_GJP_LMT_CVRG_RZKZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0029Delete3DaysTmpGjpLmtCvrgRzkzStep;
    }


    /**
     * 备份我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0030Delete3DaysTmpGjpLmtPeerRzkzStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0030_DELETE_3DAYS_TMP_GJP_LMT_PEER_RZKZ_STEP.key, JobStepLmtEnum.BAKD0030_DELETE_3DAYS_TMP_GJP_LMT_PEER_RZKZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0030Delete3DaysTmpGjpLmtPeerRzkzStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0030_DELETE_3DAYS_TMP_GJP_LMT_PEER_RZKZ_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0030Delete3DaysTmpGjpLmtPeerRzkz(openDay);// 备份白名单额度信息历史[LMT_WHITE_INFO_HISTORY]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0030_DELETE_3DAYS_TMP_GJP_LMT_PEER_RZKZ_STEP.key, JobStepLmtEnum.BAKD0030_DELETE_3DAYS_TMP_GJP_LMT_PEER_RZKZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0030Delete3DaysTmpGjpLmtPeerRzkzStep;
    }


    /**
     * 备份福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]-删除3天前的数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0031Delete3DaysTmpGjpLmtFftRzkzStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0031_DELETE_3DAYS_TMP_GJP_LMT_FFT_RZKZ_STEP.key, JobStepLmtEnum.BAKD0031_DELETE_3DAYS_TMP_GJP_LMT_FFT_RZKZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0031Delete3DaysTmpGjpLmtFftRzkzStep = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0031_DELETE_3DAYS_TMP_GJP_LMT_FFT_RZKZ_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    bakD0000Service.bakD0031Delete3DaysTmpGjpLmtFftRzkz(openDay);// 备份白名单额度信息历史[LMT_WHITE_INFO_HISTORY]-删除3天前的数据
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0031_DELETE_3DAYS_TMP_GJP_LMT_FFT_RZKZ_STEP.key, JobStepLmtEnum.BAKD0031_DELETE_3DAYS_TMP_GJP_LMT_FFT_RZKZ_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0031Delete3DaysTmpGjpLmtFftRzkzStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step bakD0000UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.BAKD0000_UPDATE_TASK100_STEP.key, JobStepLmtEnum.BAKD0000_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step bakD0000UpdateTask100Step = this.stepBuilderFactory.get(JobStepLmtEnum.BAKD0000_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BAKD0000_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.BAKD0000_UPDATE_TASK100_STEP.key, JobStepLmtEnum.BAKD0000_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return bakD0000UpdateTask100Step;
    }


}
