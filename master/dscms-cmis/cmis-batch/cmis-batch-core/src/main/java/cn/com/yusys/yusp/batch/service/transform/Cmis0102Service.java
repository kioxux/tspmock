package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0102Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0102</br>
 * 任务名称：加工任务-业务处理-保函台账处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0102Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0102Service.class);

    @Autowired
    private Cmis0102Mapper cmis0102Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 清空和初始化国结临时表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0102TruncateAndInsertTmpGjp(String openDay) {
        logger.info("重命名创建和删除 国结保函业务发信贷台账开始");
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_gjp_acc_cvrg");
        logger.info("重命名创建和删除 国结保函业务发信贷台账结束");
        logger.info("插入国结保函业务发信贷台账开始,请求参数为:[{}]", openDay);
        int insertTmpGjpAccCvrg = cmis0102Mapper.insertTmpGjpAccCvrg(openDay);
        logger.info("插入国结保函业务发信贷台账结束,返回参数为:[{}]", insertTmpGjpAccCvrg);

        logger.info("清空国结保证金台账开始");
        cmis0102Mapper.truncateTmpGjpSecurityAmt();
        logger.info("清空国结保证金台账结束");
        logger.info("插入国结保证金台账开始,请求参数为:[{}]", openDay);
        int insertTmpGjpSecurityAmt = cmis0102Mapper.insertTmpGjpSecurityAmt(openDay);
        logger.info("插入国结保证金台账结束,返回参数为:[{}]", insertTmpGjpSecurityAmt);
    }

    /**
     * 更新保函台账
     *
     * @param openDay
     */
    public void cmis0102UpdateAccCvrs(String openDay) {
        logger.info("更新保函台账原始敞口金额开始,请求参数为:[{}]", openDay);
        int cmis0102UpdateAccCvrs01 = cmis0102Mapper.cmis0102UpdateAccCvrs01(openDay);
        logger.info("更新保函台账原始敞口金额结束,返回参数为:[{}]", cmis0102UpdateAccCvrs01);
        logger.info("更新保函台账开始,请求参数为:[{}]", openDay);
        int cmis0102UpdateAccCvrs02 = cmis0102Mapper.cmis0102UpdateAccCvrs02(openDay);
        logger.info("更新保函台账结束,返回参数为:[{}]", cmis0102UpdateAccCvrs02);
    }
}
