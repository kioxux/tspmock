package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0223</br>
 * 任务名称：加工任务-额度处理-将额度相关临时表全量插入到额度正式表</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis0223Mapper {
    /**
     * 清空合同占用关系信息
     */
    void truncateContAccRel();

    /**
     * 插入合同占用关系信息
     *
     * @param openDay
     * @return
     */
    int insertContAccRel(@Param("openDay") String openDay);

    /**
     * 清空分项占用关系信息
     */
    void truncateLmtContRel();

    /**
     * 插入分项占用关系信息
     *
     * @param openDay
     * @return
     */
    int insertLmtContRel(@Param("openDay") String openDay);

    /**
     * 批复额度分项基础信息中金额更新
     *
     * @param openDay
     * @return
     */
    int updateApprLmtSubBasicInfo(@Param("openDay") String openDay);

    /**
     * 更新合作方授信分项信息
     *
     * @param openDay
     * @return
     */
    int updateApprCoopSubInfo(@Param("openDay") String openDay);

    /**
     * 白名单额度信息中金额更新
     *
     * @param openDay
     * @return
     */
    int updateLmtWhiteInfo(@Param("openDay") String openDay);
}
