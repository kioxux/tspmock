/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.fls;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSFlsLoanCustomerExp
 * @类描述: bat_s_fls_loan_customer_exp数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-14 16:26:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_fls_loan_customer_exp")
public class BatSFlsLoanCustomerExp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 评级申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CUSTOMERID")
	private String customerid;
	
	/** 授信申请流水号 **/
	@Column(name = "LMT_SERNO", unique = false, nullable = true, length = 40)
	private String lmtSerno;
	
	/** 客户号 **/
	@Column(name = "CREADITID", unique = false, nullable = true, length = 40)
	private String creaditid;
	
	/** 客户名称 **/
	@Column(name = "ENTERPRISENAME", unique = false, nullable = true, length = 80)
	private String enterprisename;
	
	/** 专业贷款最终认定等级 **/
	@Column(name = "IRSCREDITLEVEL", unique = false, nullable = true, length = 40)
	private String irscreditlevel;
	
	/** 专业贷款预期损失率 **/
	@Column(name = "IRSPD", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal irspd;
	
	/** 评级生效日期 **/
	@Column(name = "IRSBEGINDATE", unique = false, nullable = true, length = 10)
	private String irsbegindate;
	
	/** 评级到期日期 **/
	@Column(name = "IRSENDDATE", unique = false, nullable = true, length = 10)
	private String irsenddate;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param customerid
	 */
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	
    /**
     * @return customerid
     */
	public String getCustomerid() {
		return this.customerid;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno;
	}
	
    /**
     * @return lmtSerno
     */
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param creaditid
	 */
	public void setCreaditid(String creaditid) {
		this.creaditid = creaditid;
	}
	
    /**
     * @return creaditid
     */
	public String getCreaditid() {
		return this.creaditid;
	}
	
	/**
	 * @param enterprisename
	 */
	public void setEnterprisename(String enterprisename) {
		this.enterprisename = enterprisename;
	}
	
    /**
     * @return enterprisename
     */
	public String getEnterprisename() {
		return this.enterprisename;
	}
	
	/**
	 * @param irscreditlevel
	 */
	public void setIrscreditlevel(String irscreditlevel) {
		this.irscreditlevel = irscreditlevel;
	}
	
    /**
     * @return irscreditlevel
     */
	public String getIrscreditlevel() {
		return this.irscreditlevel;
	}
	
	/**
	 * @param irspd
	 */
	public void setIrspd(java.math.BigDecimal irspd) {
		this.irspd = irspd;
	}
	
    /**
     * @return irspd
     */
	public java.math.BigDecimal getIrspd() {
		return this.irspd;
	}
	
	/**
	 * @param irsbegindate
	 */
	public void setIrsbegindate(String irsbegindate) {
		this.irsbegindate = irsbegindate;
	}
	
    /**
     * @return irsbegindate
     */
	public String getIrsbegindate() {
		return this.irsbegindate;
	}
	
	/**
	 * @param irsenddate
	 */
	public void setIrsenddate(String irsenddate) {
		this.irsenddate = irsenddate;
	}
	
    /**
     * @return irsenddate
     */
	public String getIrsenddate() {
		return this.irsenddate;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}