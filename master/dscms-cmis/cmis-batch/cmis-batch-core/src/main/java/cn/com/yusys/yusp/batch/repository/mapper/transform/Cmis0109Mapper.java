package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0109</br>
 * 任务名称：加工任务-业务处理-国结额度数据处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0109Mapper {
    /**
     * 加工到我行代开他行保函
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpLmtCvrgRzkz(@Param("openDay") String openDay);

    /**
     * 加工到我行代开他行信用证
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpLmtPeerRzkz01(@Param("openDay") String openDay);

    /**
     * 加工到我行代开他行信用证
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpLmtPeerRzkz02(@Param("openDay") String openDay);


    /**
     * 加工到福费廷同业类登记簿
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpLmtFftRzkz(@Param("openDay") String openDay);

    /**
     * 更新[转贴现买入明细]中[业务结清日期BUSI_END_DATE]数据
     *
     * @param openDay
     * @return
     */
    int updateBatchBspbrb(@Param("openDay") String openDay);

    /**
     * 插入转贴现登记簿-转贴现初始占用数据插入
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpLmtZtxRzkz(@Param("openDay") String openDay);

    /**
     * 更新转贴现登记簿-插入关联缺失信息：额度扣减总行行名、贴出方行名
     *
     * @param openDay
     * @return
     */
    int updateTmpGjpLmtZtxRzkz01(@Param("openDay") String openDay);

    /**
     * 更新转贴现登记簿-插入关联缺失信息：额度扣减总行行名、贴出方行名
     *
     * @param openDay
     * @return
     */
    int updateTmpGjpLmtZtxRzkz02(@Param("openDay") String openDay);

    /**
     * 插入加工占用授信表-更新批次金额和批次余额
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpLmtZtxRzkz03(@Param("openDay") String openDay);

    /**
     * 更新转贴现登记簿-更新批次金额和批次余额
     *
     * @param openDay
     * @return
     */
    int updateTmpGjpLmtZtxRzkz03(@Param("openDay") String openDay);

}
