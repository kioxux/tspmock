/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTCoreKappJiejrb
 * @类描述: bat_t_core_kapp_jiejrb数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_core_kapp_jiejrb")
public class BatTCoreKappJiejrb extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 法人代码 **/
	@Id
	@Column(name = "farendma")
	private String farendma;
	
	/** 国别代码 **/
	@Id
	@Column(name = "GUOBIEDM")
	private String guobiedm;
	
	/** 分行代码 **/
	@Id
	@Column(name = "FENHDAIM")
	private String fenhdaim;
	
	/** 货币代号 **/
	@Id
	@Column(name = "HUOBDAIH")
	private String huobdaih;
	
	/** 对公对私标志 **/
	@Id
	@Column(name = "DUIGDUIS")
	private String duigduis;
	
	/** 节假日 **/
	@Id
	@Column(name = "JIEJIARI")
	private String jiejiari;
	
	/** 生效日期 **/
	@Column(name = "SHENGXRQ", unique = false, nullable = true, length = 8)
	private String shengxrq;
	
	/** 备注信息 **/
	@Column(name = "BEIZHUXX", unique = false, nullable = true, length = 600)
	private String beizhuxx;
	
	/** 分行标识 **/
	@Column(name = "FENHBIOS", unique = false, nullable = false, length = 4)
	private String fenhbios;
	
	/** 维护柜员 **/
	@Column(name = "WEIHGUIY", unique = false, nullable = false, length = 8)
	private String weihguiy;
	
	/** 维护机构 **/
	@Column(name = "weihjigo", unique = false, nullable = false, length = 12)
	private String weihjigo;
	
	/** 维护日期 **/
	@Column(name = "WEIHRIQI", unique = false, nullable = false, length = 8)
	private String weihriqi;
	
	/** 维护时间 **/
	@Column(name = "WEIHSHIJ", unique = false, nullable = true, length = 9)
	private String weihshij;
	
	/** 时间戳 **/
	@Column(name = "SHIJCHUO", unique = false, nullable = false, length = 19)
	private Long shijchuo;
	
	/** 记录状态 **/
	@Column(name = "JILUZTAI", unique = false, nullable = false, length = 1)
	private String jiluztai;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param guobiedm
	 */
	public void setGuobiedm(String guobiedm) {
		this.guobiedm = guobiedm;
	}
	
    /**
     * @return guobiedm
     */
	public String getGuobiedm() {
		return this.guobiedm;
	}
	
	/**
	 * @param fenhdaim
	 */
	public void setFenhdaim(String fenhdaim) {
		this.fenhdaim = fenhdaim;
	}
	
    /**
     * @return fenhdaim
     */
	public String getFenhdaim() {
		return this.fenhdaim;
	}
	
	/**
	 * @param huobdaih
	 */
	public void setHuobdaih(String huobdaih) {
		this.huobdaih = huobdaih;
	}
	
    /**
     * @return huobdaih
     */
	public String getHuobdaih() {
		return this.huobdaih;
	}
	
	/**
	 * @param duigduis
	 */
	public void setDuigduis(String duigduis) {
		this.duigduis = duigduis;
	}
	
    /**
     * @return duigduis
     */
	public String getDuigduis() {
		return this.duigduis;
	}
	
	/**
	 * @param jiejiari
	 */
	public void setJiejiari(String jiejiari) {
		this.jiejiari = jiejiari;
	}
	
    /**
     * @return jiejiari
     */
	public String getJiejiari() {
		return this.jiejiari;
	}
	
	/**
	 * @param shengxrq
	 */
	public void setShengxrq(String shengxrq) {
		this.shengxrq = shengxrq;
	}
	
    /**
     * @return shengxrq
     */
	public String getShengxrq() {
		return this.shengxrq;
	}
	
	/**
	 * @param beizhuxx
	 */
	public void setBeizhuxx(String beizhuxx) {
		this.beizhuxx = beizhuxx;
	}
	
    /**
     * @return beizhuxx
     */
	public String getBeizhuxx() {
		return this.beizhuxx;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}


}