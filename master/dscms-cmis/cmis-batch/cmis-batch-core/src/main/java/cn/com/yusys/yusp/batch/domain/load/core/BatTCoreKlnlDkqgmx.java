/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTCoreKlnlDkqgmx
 * @类描述: bat_t_core_klnl_dkqgmx数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_core_klnl_dkqgmx")
public class BatTCoreKlnlDkqgmx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 法人代码
     **/
    @Id
    @Column(name = "farendma")
    private String farendma;
    /**
     * 贷款账号
     **/
    @Column(name = "dkzhangh", unique = false, nullable = false, length = 40)
    private String dkzhangh;
    /**
     * 贷款借据号
     **/
    @Id
    @Column(name = "dkjiejuh")
    private String dkjiejuh;
    /**
     * 交易日期
     **/
    @Column(name = "jiaoyirq", unique = false, nullable = true, length = 8)
    private String jiaoyirq;
    /**
     * 本期期数
     **/
    @Id
    @Column(name = "benqqish")
    private String benqqish;
    /**
     * 本期子期数
     **/
    @Id
    @Column(name = "benqizqs")
    private Long benqizqs;
    /**
     * 应还日期
     **/
    @Column(name = "yinghriq", unique = false, nullable = true, length = 8)
    private String yinghriq;
    /**
     * 还款账号
     **/
    @Column(name = "huankzhh", unique = false, nullable = true, length = 35)
    private String huankzhh;
    /**
     * 还款账号子序号
     **/
    @Column(name = "hkzhhzxh", unique = false, nullable = true, length = 8)
    private String hkzhhzxh;
    /**
     * 还款状态 STD_HUANKZHT
     **/
    @Column(name = "huankzht", unique = false, nullable = true, length = 1)
    private String huankzht;
    /**
     * 初始本金
     **/
    @Column(name = "chushibj", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal chushibj;
    /**
     * 初始利息
     **/
    @Column(name = "chushilx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal chushilx;

    /**
     * 本金发生额
     **/
    @Column(name = "benjinfs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal benjinfs;

    /**
     * 本金
     **/
    @Column(name = "benjinnn", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal benjinnn;

    /**
     * 应收应计利息发生额
     **/
    @Column(name = "ysyjlxfs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal ysyjlxfs;

    /**
     * 应收应计利息
     **/
    @Column(name = "ysyjlixi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal ysyjlixi;

    /**
     * 催收应计利息发生额
     **/
    @Column(name = "csyjlxfs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal csyjlxfs;

    /**
     * 催收应计利息
     **/
    @Column(name = "csyjlixi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal csyjlixi;

    /**
     * 应收欠息发生额
     **/
    @Column(name = "yshqxifs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yshqxifs;

    /**
     * 应收欠息
     **/
    @Column(name = "ysqianxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal ysqianxi;

    /**
     * 催收欠息发生额
     **/
    @Column(name = "cshqxifs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal cshqxifs;

    /**
     * 催收欠息
     **/
    @Column(name = "csqianxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal csqianxi;

    /**
     * 应收应计罚息发生额
     **/
    @Column(name = "ysyjfxfs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal ysyjfxfs;

    /**
     * 应收应计罚息
     **/
    @Column(name = "ysyjfaxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal ysyjfaxi;

    /**
     * 催收应计罚息发生额
     **/
    @Column(name = "csyjfxfs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal csyjfxfs;

    /**
     * 催收应计罚息
     **/
    @Column(name = "csyjfaxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal csyjfaxi;

    /**
     * 应收罚息发生额
     **/
    @Column(name = "yshfxifs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yshfxifs;

    /**
     * 应收罚息
     **/
    @Column(name = "yshofaxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yshofaxi;

    /**
     * 催收罚息发生额
     **/
    @Column(name = "cshofxfs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal cshofxfs;

    /**
     * 催收罚息
     **/
    @Column(name = "cshofaxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal cshofaxi;

    /**
     * 应计复息发生额
     **/
    @Column(name = "yjifxifs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yjifxifs;

    /**
     * 应计复息
     **/
    @Column(name = "yingjifx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yingjifx;

    /**
     * 复息发生额
     **/
    @Column(name = "fuxiiifs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal fuxiiifs;

    /**
     * 复息
     **/
    @Column(name = "fuxiiiii", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal fuxiiiii;

    /**
     * 核销利息发生额
     **/
    @Column(name = "hexlixfs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal hexlixfs;

    /**
     * 核销利息
     **/
    @Column(name = "hexiaolx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal hexiaolx;

    /**
     * 利息调整发生额
     **/
    @Column(name = "lxitzhfs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal lxitzhfs;

    /**
     * 利息调整
     **/
    @Column(name = "lxtiaozh", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal lxtiaozh;

    /**
     * 应收罚金发生额
     **/
    @Column(name = "yshfajfs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yshfajfs;

    /**
     * 应收罚金
     **/
    @Column(name = "yingshfj", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yingshfj;

    /**
     * 应收费用发生额
     **/
    @Column(name = "yshfynfs", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yshfynfs;

    /**
     * 应收费用
     **/
    @Column(name = "yingshfy", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yingshfy;

    /**
     * 已核销本金利息发生额
     **/
    @Column(name = "yhxbxfse", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yhxbxfse;

    /**
     * 已核销本金利息
     **/
    @Column(name = "yihxbjlx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yihxbjlx;

    /**
     * 本期状态 STD_BENQIZHT
     **/
    @Column(name = "benqizht", unique = false, nullable = true, length = 1)
    private String benqizht;

    /**
     * 应计非应计状态 STD_YJFYJZHT
     **/
    @Column(name = "yjfyjzht", unique = false, nullable = true, length = 1)
    private String yjfyjzht;

    /**
     * 还款顺序编号
     **/
    @Column(name = "hkshxubh", unique = false, nullable = true, length = 30)
    private String hkshxubh;


    /**
     * 明细序号
     **/
    @Id
    @Column(name = "mingxixh")
    private Long mingxixh;


    /**
     * 交易机构
     **/
    @Column(name = "jiaoyijg", unique = false, nullable = true, length = 12)
    private String jiaoyijg;

    /**
     * 交易柜员
     **/
    @Column(name = "jiaoyigy", unique = false, nullable = true, length = 8)
    private String jiaoyigy;

    /**
     * 交易流水
     **/
    @Column(name = "jiaoyils", unique = false, nullable = true, length = 32)
    private String jiaoyils;

    /**
     * 交易事件
     **/
    @Column(name = "jiaoyisj", unique = false, nullable = true, length = 10)
    private String jiaoyisj;

    /**
     * 事件说明
     **/
    @Column(name = "shjshuom", unique = false, nullable = true, length = 80)
    private String shjshuom;

    /**
     * 交易码
     **/
    @Column(name = "jiaoyima", unique = false, nullable = true, length = 20)
    private String jiaoyima;

    /**
     * 摘要
     **/
    @Column(name = "zhaiyoms", unique = false, nullable = true, length = 300)
    private String zhaiyoms;

    /**
     * 分行标识
     **/
    @Column(name = "fenhbios", unique = false, nullable = false, length = 4)
    private String fenhbios;

    /**
     * 维护柜员
     **/
    @Column(name = "WEIHGUIY", unique = false, nullable = true, length = 8)
    private String weihguiy;

    /**
     * 维护机构
     **/
    @Column(name = "weihjigo", unique = false, nullable = false, length = 12)
    private String weihjigo;

    /**
     * 维护日期
     **/
    @Column(name = "weihriqi", unique = false, nullable = false, length = 8)
    private String weihriqi;

    /**
     * 维护时间
     **/
    @Column(name = "weihshij", unique = false, nullable = true, length = 9)
    private String weihshij;

    /**
     * 时间戳
     **/
    @Column(name = "shijchuo", unique = false, nullable = false, length = 19)
    private Long shijchuo;

    /**
     * 记录状态 STD_JILUZTAI
     **/
    @Column(name = "jiluztai", unique = false, nullable = false, length = 1)
    private String jiluztai;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param dkzhangh
	 */
	public void setDkzhangh(String dkzhangh) {
		this.dkzhangh = dkzhangh;
	}
	
    /**
     * @return dkzhangh
     */
	public String getDkzhangh() {
		return this.dkzhangh;
	}
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param jiaoyirq
	 */
	public void setJiaoyirq(String jiaoyirq) {
		this.jiaoyirq = jiaoyirq;
	}
	
    /**
     * @return jiaoyirq
     */
	public String getJiaoyirq() {
		return this.jiaoyirq;
	}
	
	/**
	 * @param benqqish
	 */
	public void setBenqqish(String benqqish) {
		this.benqqish = benqqish;
	}
	
    /**
     * @return benqqish
     */
	public String getBenqqish() {
		return this.benqqish;
	}
	
	/**
	 * @param benqizqs
	 */
	public void setBenqizqs(Long benqizqs) {
		this.benqizqs = benqizqs;
	}
	
    /**
     * @return benqizqs
     */
	public Long getBenqizqs() {
		return this.benqizqs;
	}
	
	/**
	 * @param yinghriq
	 */
	public void setYinghriq(String yinghriq) {
		this.yinghriq = yinghriq;
	}
	
    /**
     * @return yinghriq
     */
	public String getYinghriq() {
		return this.yinghriq;
	}
	
	/**
	 * @param huankzhh
	 */
	public void setHuankzhh(String huankzhh) {
		this.huankzhh = huankzhh;
	}
	
    /**
     * @return huankzhh
     */
	public String getHuankzhh() {
		return this.huankzhh;
	}
	
	/**
	 * @param hkzhhzxh
	 */
	public void setHkzhhzxh(String hkzhhzxh) {
		this.hkzhhzxh = hkzhhzxh;
	}
	
    /**
     * @return hkzhhzxh
     */
	public String getHkzhhzxh() {
		return this.hkzhhzxh;
	}
	
	/**
	 * @param huankzht
	 */
	public void setHuankzht(String huankzht) {
		this.huankzht = huankzht;
	}
	
    /**
     * @return huankzht
     */
	public String getHuankzht() {
		return this.huankzht;
	}
	
	/**
	 * @param chushibj
	 */
	public void setChushibj(java.math.BigDecimal chushibj) {
		this.chushibj = chushibj;
	}
	
    /**
     * @return chushibj
     */
	public java.math.BigDecimal getChushibj() {
		return this.chushibj;
	}
	
	/**
	 * @param chushilx
	 */
	public void setChushilx(java.math.BigDecimal chushilx) {
		this.chushilx = chushilx;
	}
	
    /**
     * @return chushilx
     */
	public java.math.BigDecimal getChushilx() {
		return this.chushilx;
	}
	
	/**
	 * @param benjinfs
	 */
	public void setBenjinfs(java.math.BigDecimal benjinfs) {
		this.benjinfs = benjinfs;
	}
	
    /**
     * @return benjinfs
     */
	public java.math.BigDecimal getBenjinfs() {
		return this.benjinfs;
	}
	
	/**
	 * @param benjinnn
	 */
	public void setBenjinnn(java.math.BigDecimal benjinnn) {
		this.benjinnn = benjinnn;
	}
	
    /**
     * @return benjinnn
     */
	public java.math.BigDecimal getBenjinnn() {
		return this.benjinnn;
	}
	
	/**
	 * @param ysyjlxfs
	 */
	public void setYsyjlxfs(java.math.BigDecimal ysyjlxfs) {
		this.ysyjlxfs = ysyjlxfs;
	}
	
    /**
     * @return ysyjlxfs
     */
	public java.math.BigDecimal getYsyjlxfs() {
		return this.ysyjlxfs;
	}
	
	/**
	 * @param ysyjlixi
	 */
	public void setYsyjlixi(java.math.BigDecimal ysyjlixi) {
		this.ysyjlixi = ysyjlixi;
	}
	
    /**
     * @return ysyjlixi
     */
	public java.math.BigDecimal getYsyjlixi() {
		return this.ysyjlixi;
	}
	
	/**
	 * @param csyjlxfs
	 */
	public void setCsyjlxfs(java.math.BigDecimal csyjlxfs) {
		this.csyjlxfs = csyjlxfs;
	}
	
    /**
     * @return csyjlxfs
     */
	public java.math.BigDecimal getCsyjlxfs() {
		return this.csyjlxfs;
	}
	
	/**
	 * @param csyjlixi
	 */
	public void setCsyjlixi(java.math.BigDecimal csyjlixi) {
		this.csyjlixi = csyjlixi;
	}
	
    /**
     * @return csyjlixi
     */
	public java.math.BigDecimal getCsyjlixi() {
		return this.csyjlixi;
	}
	
	/**
	 * @param yshqxifs
	 */
	public void setYshqxifs(java.math.BigDecimal yshqxifs) {
		this.yshqxifs = yshqxifs;
	}
	
    /**
     * @return yshqxifs
     */
	public java.math.BigDecimal getYshqxifs() {
		return this.yshqxifs;
	}
	
	/**
	 * @param ysqianxi
	 */
	public void setYsqianxi(java.math.BigDecimal ysqianxi) {
		this.ysqianxi = ysqianxi;
	}
	
    /**
     * @return ysqianxi
     */
	public java.math.BigDecimal getYsqianxi() {
		return this.ysqianxi;
	}
	
	/**
	 * @param cshqxifs
	 */
	public void setCshqxifs(java.math.BigDecimal cshqxifs) {
		this.cshqxifs = cshqxifs;
	}
	
    /**
     * @return cshqxifs
     */
	public java.math.BigDecimal getCshqxifs() {
		return this.cshqxifs;
	}
	
	/**
	 * @param csqianxi
	 */
	public void setCsqianxi(java.math.BigDecimal csqianxi) {
		this.csqianxi = csqianxi;
	}
	
    /**
     * @return csqianxi
     */
	public java.math.BigDecimal getCsqianxi() {
		return this.csqianxi;
	}
	
	/**
	 * @param ysyjfxfs
	 */
	public void setYsyjfxfs(java.math.BigDecimal ysyjfxfs) {
		this.ysyjfxfs = ysyjfxfs;
	}
	
    /**
     * @return ysyjfxfs
     */
	public java.math.BigDecimal getYsyjfxfs() {
		return this.ysyjfxfs;
	}
	
	/**
	 * @param ysyjfaxi
	 */
	public void setYsyjfaxi(java.math.BigDecimal ysyjfaxi) {
		this.ysyjfaxi = ysyjfaxi;
	}
	
    /**
     * @return ysyjfaxi
     */
	public java.math.BigDecimal getYsyjfaxi() {
		return this.ysyjfaxi;
	}
	
	/**
	 * @param csyjfxfs
	 */
	public void setCsyjfxfs(java.math.BigDecimal csyjfxfs) {
		this.csyjfxfs = csyjfxfs;
	}
	
    /**
     * @return csyjfxfs
     */
	public java.math.BigDecimal getCsyjfxfs() {
		return this.csyjfxfs;
	}
	
	/**
	 * @param csyjfaxi
	 */
	public void setCsyjfaxi(java.math.BigDecimal csyjfaxi) {
		this.csyjfaxi = csyjfaxi;
	}
	
    /**
     * @return csyjfaxi
     */
	public java.math.BigDecimal getCsyjfaxi() {
		return this.csyjfaxi;
	}
	
	/**
	 * @param yshfxifs
	 */
	public void setYshfxifs(java.math.BigDecimal yshfxifs) {
		this.yshfxifs = yshfxifs;
	}
	
    /**
     * @return yshfxifs
     */
	public java.math.BigDecimal getYshfxifs() {
		return this.yshfxifs;
	}
	
	/**
	 * @param yshofaxi
	 */
	public void setYshofaxi(java.math.BigDecimal yshofaxi) {
		this.yshofaxi = yshofaxi;
	}
	
    /**
     * @return yshofaxi
     */
	public java.math.BigDecimal getYshofaxi() {
		return this.yshofaxi;
	}
	
	/**
	 * @param cshofxfs
	 */
	public void setCshofxfs(java.math.BigDecimal cshofxfs) {
		this.cshofxfs = cshofxfs;
	}
	
    /**
     * @return cshofxfs
     */
	public java.math.BigDecimal getCshofxfs() {
		return this.cshofxfs;
	}
	
	/**
	 * @param cshofaxi
	 */
	public void setCshofaxi(java.math.BigDecimal cshofaxi) {
		this.cshofaxi = cshofaxi;
	}
	
    /**
     * @return cshofaxi
     */
	public java.math.BigDecimal getCshofaxi() {
		return this.cshofaxi;
	}
	
	/**
	 * @param yjifxifs
	 */
	public void setYjifxifs(java.math.BigDecimal yjifxifs) {
		this.yjifxifs = yjifxifs;
	}
	
    /**
     * @return yjifxifs
     */
	public java.math.BigDecimal getYjifxifs() {
		return this.yjifxifs;
	}
	
	/**
	 * @param yingjifx
	 */
	public void setYingjifx(java.math.BigDecimal yingjifx) {
		this.yingjifx = yingjifx;
	}
	
    /**
     * @return yingjifx
     */
	public java.math.BigDecimal getYingjifx() {
		return this.yingjifx;
	}
	
	/**
	 * @param fuxiiifs
	 */
	public void setFuxiiifs(java.math.BigDecimal fuxiiifs) {
		this.fuxiiifs = fuxiiifs;
	}
	
    /**
     * @return fuxiiifs
     */
	public java.math.BigDecimal getFuxiiifs() {
		return this.fuxiiifs;
	}
	
	/**
	 * @param fuxiiiii
	 */
	public void setFuxiiiii(java.math.BigDecimal fuxiiiii) {
		this.fuxiiiii = fuxiiiii;
	}
	
    /**
     * @return fuxiiiii
     */
	public java.math.BigDecimal getFuxiiiii() {
		return this.fuxiiiii;
	}
	
	/**
	 * @param hexlixfs
	 */
	public void setHexlixfs(java.math.BigDecimal hexlixfs) {
		this.hexlixfs = hexlixfs;
	}
	
    /**
     * @return hexlixfs
     */
	public java.math.BigDecimal getHexlixfs() {
		return this.hexlixfs;
	}
	
	/**
	 * @param hexiaolx
	 */
	public void setHexiaolx(java.math.BigDecimal hexiaolx) {
		this.hexiaolx = hexiaolx;
	}
	
    /**
     * @return hexiaolx
     */
	public java.math.BigDecimal getHexiaolx() {
		return this.hexiaolx;
	}
	
	/**
	 * @param lxitzhfs
	 */
	public void setLxitzhfs(java.math.BigDecimal lxitzhfs) {
		this.lxitzhfs = lxitzhfs;
	}
	
    /**
     * @return lxitzhfs
     */
	public java.math.BigDecimal getLxitzhfs() {
		return this.lxitzhfs;
	}
	
	/**
	 * @param lxtiaozh
	 */
	public void setLxtiaozh(java.math.BigDecimal lxtiaozh) {
		this.lxtiaozh = lxtiaozh;
	}
	
    /**
     * @return lxtiaozh
     */
	public java.math.BigDecimal getLxtiaozh() {
		return this.lxtiaozh;
	}
	
	/**
	 * @param yshfajfs
	 */
	public void setYshfajfs(java.math.BigDecimal yshfajfs) {
		this.yshfajfs = yshfajfs;
	}
	
    /**
     * @return yshfajfs
     */
	public java.math.BigDecimal getYshfajfs() {
		return this.yshfajfs;
	}
	
	/**
	 * @param yingshfj
	 */
	public void setYingshfj(java.math.BigDecimal yingshfj) {
		this.yingshfj = yingshfj;
	}
	
    /**
     * @return yingshfj
     */
	public java.math.BigDecimal getYingshfj() {
		return this.yingshfj;
	}
	
	/**
	 * @param yshfynfs
	 */
	public void setYshfynfs(java.math.BigDecimal yshfynfs) {
		this.yshfynfs = yshfynfs;
	}
	
    /**
     * @return yshfynfs
     */
	public java.math.BigDecimal getYshfynfs() {
		return this.yshfynfs;
	}
	
	/**
	 * @param yingshfy
	 */
	public void setYingshfy(java.math.BigDecimal yingshfy) {
		this.yingshfy = yingshfy;
	}
	
    /**
     * @return yingshfy
     */
	public java.math.BigDecimal getYingshfy() {
		return this.yingshfy;
	}
	
	/**
	 * @param yhxbxfse
	 */
	public void setYhxbxfse(java.math.BigDecimal yhxbxfse) {
		this.yhxbxfse = yhxbxfse;
	}
	
    /**
     * @return yhxbxfse
     */
	public java.math.BigDecimal getYhxbxfse() {
		return this.yhxbxfse;
	}
	
	/**
	 * @param yihxbjlx
	 */
	public void setYihxbjlx(java.math.BigDecimal yihxbjlx) {
		this.yihxbjlx = yihxbjlx;
	}
	
    /**
     * @return yihxbjlx
     */
	public java.math.BigDecimal getYihxbjlx() {
		return this.yihxbjlx;
	}
	
	/**
	 * @param benqizht
	 */
	public void setBenqizht(String benqizht) {
		this.benqizht = benqizht;
	}
	
    /**
     * @return benqizht
     */
	public String getBenqizht() {
		return this.benqizht;
	}
	
	/**
	 * @param yjfyjzht
	 */
	public void setYjfyjzht(String yjfyjzht) {
		this.yjfyjzht = yjfyjzht;
	}
	
    /**
     * @return yjfyjzht
     */
	public String getYjfyjzht() {
		return this.yjfyjzht;
	}
	
	/**
	 * @param hkshxubh
	 */
	public void setHkshxubh(String hkshxubh) {
		this.hkshxubh = hkshxubh;
	}
	
    /**
     * @return hkshxubh
     */
	public String getHkshxubh() {
		return this.hkshxubh;
	}
	
	/**
	 * @param mingxixh
	 */
	public void setMingxixh(Long mingxixh) {
		this.mingxixh = mingxixh;
	}
	
    /**
     * @return mingxixh
     */
	public Long getMingxixh() {
		return this.mingxixh;
	}
	
	/**
	 * @param jiaoyijg
	 */
	public void setJiaoyijg(String jiaoyijg) {
		this.jiaoyijg = jiaoyijg;
	}
	
    /**
     * @return jiaoyijg
     */
	public String getJiaoyijg() {
		return this.jiaoyijg;
	}
	
	/**
	 * @param jiaoyigy
	 */
	public void setJiaoyigy(String jiaoyigy) {
		this.jiaoyigy = jiaoyigy;
	}
	
    /**
     * @return jiaoyigy
     */
	public String getJiaoyigy() {
		return this.jiaoyigy;
	}
	
	/**
	 * @param jiaoyils
	 */
	public void setJiaoyils(String jiaoyils) {
		this.jiaoyils = jiaoyils;
	}
	
    /**
     * @return jiaoyils
     */
	public String getJiaoyils() {
		return this.jiaoyils;
	}
	
	/**
	 * @param jiaoyisj
	 */
	public void setJiaoyisj(String jiaoyisj) {
		this.jiaoyisj = jiaoyisj;
	}
	
    /**
     * @return jiaoyisj
     */
	public String getJiaoyisj() {
		return this.jiaoyisj;
	}
	
	/**
	 * @param shjshuom
	 */
	public void setShjshuom(String shjshuom) {
		this.shjshuom = shjshuom;
	}
	
    /**
     * @return shjshuom
     */
	public String getShjshuom() {
		return this.shjshuom;
	}
	
	/**
	 * @param jiaoyima
	 */
	public void setJiaoyima(String jiaoyima) {
		this.jiaoyima = jiaoyima;
	}
	
    /**
     * @return jiaoyima
     */
	public String getJiaoyima() {
		return this.jiaoyima;
	}
	
	/**
	 * @param zhaiyoms
	 */
	public void setZhaiyoms(String zhaiyoms) {
		this.zhaiyoms = zhaiyoms;
	}
	
    /**
     * @return zhaiyoms
     */
	public String getZhaiyoms() {
		return this.zhaiyoms;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}


}