package cn.com.yusys.yusp.batch.service.bak;

import cn.com.yusys.yusp.batch.repository.mapper.bak.BakD0001Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKD0001</br>
 * 任务名称：批前备份日表任务-备份银承台账[ACC_ACCP]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
@Service
@Transactional
public class BakD0001Service {
    private static final Logger logger = LoggerFactory.getLogger(BakD0001Service.class);

    @Autowired
    private BakD0001Mapper bakD0001Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 删除当天的数据
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void bakD0001DeleteCurrent(String openDay) {
        logger.info("删除当天的[银承台账[ACC_ACCP]]数据开始,请求参数为:[{}]", openDay);
        // bakD0001Mapper.truncateCurrent();// cmis_biz.tmp_d_acc_accp
        tableUtilsService.renameCreateDropTable("cmis_biz", "tmp_d_acc_accp");// 20211030 调整为调用重命名创建和删除相关表的公共方法
        logger.info("删除当天的[银承台账[ACC_ACCP]]数据结束");

//        logger.info("查询待删除当天的[银承台账[ACC_ACCP]]数据总次数开始,请求参数为:[{}]", openDay);
//        int queryD00001DeleteOpenDayAccAccpCounts = bakD0001Mapper.queryD00001DeleteOpenDayAccAccpCounts(openDay);
//        // 由于TDSQL不支持ceiling函数，此处在java中处理
//        BigDecimal times = new BigDecimal(queryD00001DeleteOpenDayAccAccpCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
//        queryD00001DeleteOpenDayAccAccpCounts = times.intValue();
//        logger.info("查询待删除当天的[银承台账[ACC_ACCP]]数据总次数结束,返回参数为:[{}]", queryD00001DeleteOpenDayAccAccpCounts);
//        // 删除当天的数据
//        for (int i = 0; i <= queryD00001DeleteOpenDayAccAccpCounts; i++) {
//            logger.info("第[" + i + "]次删除当天的[银承台账[ACC_ACCP]]数据开始,请求参数为:[{}]", openDay);
//            int deleteCurrent = bakD0001Mapper.deleteCurrent(openDay);
//            logger.info("第[" + i + "]次删除当天的[银承台账[ACC_ACCP]]数据结束,返回参数为:[{}]", deleteCurrent);
//        }
    }

    /**
     * 备份当天的数据
     *
     * @param openDay
     */
    public void bakD0001InsertCurrent(String openDay) {
        // 备份当天的数据
        logger.info("备份当天的[银承台账[ACC_ACCP]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakD0001Mapper.insertCurrent(openDay);
        logger.info("备份当天的[银承台账[ACC_ACCP]]数据结束,返回参数为:[{}]", insertCurrent);
    }

    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakDEqualsOriginal(String openDay) {
        logger.info("查询当天备份的[银承台账[ACC_ACCP]]数据总条数开始,请求参数为:[{}]", openDay);
        int bakDCounts = bakD0001Mapper.queryD00001DeleteOpenDayAccAccpCounts(openDay);
        logger.info("查询当天备份的[银承台账[ACC_ACCP]]数据总条数结束,返回参数为:[{}]", bakDCounts);

        logger.info("查询当天的[银承台账[ACC_ACCP]]原表数据总条数开始,请求参数为:[{}]", openDay);
        int originalCounts = bakD0001Mapper.queryOriginalCounts(openDay);
        logger.info("查询当天的[银承台账[ACC_ACCP]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

        if (bakDCounts != originalCounts) {
            logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakDCounts, originalCounts);
            throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakDCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
        } else {
            logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakDCounts, originalCounts);
        }
    }
}
