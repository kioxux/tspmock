/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.biz;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: TmpLmtAppCorp
 * @类描述: tmp_lmt_app_corp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-27 01:41:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_lmt_app_corp")
public class TmpLmtAppCorp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 集团申请流水号 **/
	@Id
	@Column(name = "GRP_SERNO")
	private String grpSerno;
	
	/** 任务日期 **/
	@Id
	@Column(name = "TASK_DATE")
	private String taskDate;
	
	/** 授信类型 **/
	@Column(name = "LMT_TYPE", unique = false, nullable = false, length = 5)
	private String lmtType;
	
	/** 额度分项编号 **/
	@Column(name = "LIMIT_SUB_NO", unique = false, nullable = true, length = 40)
	private String limitSubNo;
	
	/** 额度分项名称 **/
	@Column(name = "LIMIT_SUB_NAME", unique = false, nullable = true, length = 255)
	private String limitSubName;
	
	/** 集团客户编号 **/
	@Column(name = "GRP_CUS_ID", unique = false, nullable = true, length = 40)
	private String grpCusId;
	
	/** 集团客户名称 **/
	@Column(name = "GRP_CUS_NAME", unique = false, nullable = true, length = 80)
	private String grpCusName;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 原授信流水号 **/
	@Column(name = "ORIGI_LMT_SERNO", unique = false, nullable = true, length = 40)
	private String origiLmtSerno;
	
	/** 原授信批复流水号 **/
	@Column(name = "ORIGI_LMT_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String origiLmtReplySerno;
	
	/** 敞口额度合计 **/
	@Column(name = "OPEN_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal openTotalLmtAmt;
	
	/** 低风险额度合计 **/
	@Column(name = "LOW_RISK_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lowRiskTotalLmtAmt;
	
	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;
	
	/** 原授信期限 **/
	@Column(name = "ORIGI_LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer origiLmtTerm;
	
	/** 成员额度调剂说明 **/
	@Column(name = "MEM_LMT_ADJUST_DESC", unique = false, nullable = true, length = 65535)
	private String memLmtAdjustDesc;
	
	/** 是否重组贷款 **/
	@Column(name = "IS_RESTRU_LOAN", unique = false, nullable = true, length = 5)
	private String isRestruLoan;
	
	/** 是否允许额度在成员间调剂 **/
	@Column(name = "IS_ALLOW_ALLOCAT_MEM", unique = false, nullable = true, length = 5)
	private String isAllowAllocatMem;
	
	/** 综合评价 **/
	@Column(name = "INTE_EVLU", unique = false, nullable = true, length = 65535)
	private String inteEvlu;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = false, length = 5)
	private String approveStatus;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 返回结果 **/
	@Column(name = "RET_RESULT", unique = false, nullable = true, length = 20)
	private String retResult;
	
	/** 返回码 **/
	@Column(name = "RET_CODE", unique = false, nullable = true, length = 20)
	private String retCode;
	
	/** 返回信息 **/
	@Column(name = "RET_MSG", unique = false, nullable = true, length = 400)
	private String retMsg;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param grpSerno
	 */
	public void setGrpSerno(String grpSerno) {
		this.grpSerno = grpSerno;
	}
	
    /**
     * @return grpSerno
     */
	public String getGrpSerno() {
		return this.grpSerno;
	}
	
	/**
	 * @param taskDate
	 */
	public void setTaskDate(String taskDate) {
		this.taskDate = taskDate;
	}
	
    /**
     * @return taskDate
     */
	public String getTaskDate() {
		return this.taskDate;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}
	
    /**
     * @return lmtType
     */
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param limitSubNo
	 */
	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo;
	}
	
    /**
     * @return limitSubNo
     */
	public String getLimitSubNo() {
		return this.limitSubNo;
	}
	
	/**
	 * @param limitSubName
	 */
	public void setLimitSubName(String limitSubName) {
		this.limitSubName = limitSubName;
	}
	
    /**
     * @return limitSubName
     */
	public String getLimitSubName() {
		return this.limitSubName;
	}
	
	/**
	 * @param grpCusId
	 */
	public void setGrpCusId(String grpCusId) {
		this.grpCusId = grpCusId;
	}
	
    /**
     * @return grpCusId
     */
	public String getGrpCusId() {
		return this.grpCusId;
	}
	
	/**
	 * @param grpCusName
	 */
	public void setGrpCusName(String grpCusName) {
		this.grpCusName = grpCusName;
	}
	
    /**
     * @return grpCusName
     */
	public String getGrpCusName() {
		return this.grpCusName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param origiLmtSerno
	 */
	public void setOrigiLmtSerno(String origiLmtSerno) {
		this.origiLmtSerno = origiLmtSerno;
	}
	
    /**
     * @return origiLmtSerno
     */
	public String getOrigiLmtSerno() {
		return this.origiLmtSerno;
	}
	
	/**
	 * @param origiLmtReplySerno
	 */
	public void setOrigiLmtReplySerno(String origiLmtReplySerno) {
		this.origiLmtReplySerno = origiLmtReplySerno;
	}
	
    /**
     * @return origiLmtReplySerno
     */
	public String getOrigiLmtReplySerno() {
		return this.origiLmtReplySerno;
	}
	
	/**
	 * @param openTotalLmtAmt
	 */
	public void setOpenTotalLmtAmt(java.math.BigDecimal openTotalLmtAmt) {
		this.openTotalLmtAmt = openTotalLmtAmt;
	}
	
    /**
     * @return openTotalLmtAmt
     */
	public java.math.BigDecimal getOpenTotalLmtAmt() {
		return this.openTotalLmtAmt;
	}
	
	/**
	 * @param lowRiskTotalLmtAmt
	 */
	public void setLowRiskTotalLmtAmt(java.math.BigDecimal lowRiskTotalLmtAmt) {
		this.lowRiskTotalLmtAmt = lowRiskTotalLmtAmt;
	}
	
    /**
     * @return lowRiskTotalLmtAmt
     */
	public java.math.BigDecimal getLowRiskTotalLmtAmt() {
		return this.lowRiskTotalLmtAmt;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param origiLmtTerm
	 */
	public void setOrigiLmtTerm(Integer origiLmtTerm) {
		this.origiLmtTerm = origiLmtTerm;
	}
	
    /**
     * @return origiLmtTerm
     */
	public Integer getOrigiLmtTerm() {
		return this.origiLmtTerm;
	}
	
	/**
	 * @param memLmtAdjustDesc
	 */
	public void setMemLmtAdjustDesc(String memLmtAdjustDesc) {
		this.memLmtAdjustDesc = memLmtAdjustDesc;
	}
	
    /**
     * @return memLmtAdjustDesc
     */
	public String getMemLmtAdjustDesc() {
		return this.memLmtAdjustDesc;
	}
	
	/**
	 * @param isRestruLoan
	 */
	public void setIsRestruLoan(String isRestruLoan) {
		this.isRestruLoan = isRestruLoan;
	}
	
    /**
     * @return isRestruLoan
     */
	public String getIsRestruLoan() {
		return this.isRestruLoan;
	}
	
	/**
	 * @param isAllowAllocatMem
	 */
	public void setIsAllowAllocatMem(String isAllowAllocatMem) {
		this.isAllowAllocatMem = isAllowAllocatMem;
	}
	
    /**
     * @return isAllowAllocatMem
     */
	public String getIsAllowAllocatMem() {
		return this.isAllowAllocatMem;
	}
	
	/**
	 * @param inteEvlu
	 */
	public void setInteEvlu(String inteEvlu) {
		this.inteEvlu = inteEvlu;
	}
	
    /**
     * @return inteEvlu
     */
	public String getInteEvlu() {
		return this.inteEvlu;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param retResult
	 */
	public void setRetResult(String retResult) {
		this.retResult = retResult;
	}
	
    /**
     * @return retResult
     */
	public String getRetResult() {
		return this.retResult;
	}
	
	/**
	 * @param retCode
	 */
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}
	
    /**
     * @return retCode
     */
	public String getRetCode() {
		return this.retCode;
	}
	
	/**
	 * @param retMsg
	 */
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	
    /**
     * @return retMsg
     */
	public String getRetMsg() {
		return this.retMsg;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}