/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.biz;

import cn.com.yusys.yusp.batch.domain.biz.TmpLmtAppPer;
import cn.com.yusys.yusp.batch.repository.mapper.biz.TmpLmtAppPerMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: TmpLmtAppPerService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-24 11:12:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class TmpLmtAppPerService {

    @Autowired
    private TmpLmtAppPerMapper tmpLmtAppPerMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public TmpLmtAppPer selectByPrimaryKey(String pkId, String serno, String taskDate) {
        return tmpLmtAppPerMapper.selectByPrimaryKey(pkId, serno, taskDate);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<TmpLmtAppPer> selectAll(QueryModel model) {
        List<TmpLmtAppPer> records = (List<TmpLmtAppPer>) tmpLmtAppPerMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<TmpLmtAppPer> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<TmpLmtAppPer> list = tmpLmtAppPerMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(TmpLmtAppPer record) {
        return tmpLmtAppPerMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(TmpLmtAppPer record) {
        return tmpLmtAppPerMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(TmpLmtAppPer record) {
        return tmpLmtAppPerMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(TmpLmtAppPer record) {
        return tmpLmtAppPerMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String serno, String taskDate) {
        return tmpLmtAppPerMapper.deleteByPrimaryKey(pkId, serno, taskDate);
    }

    /**
     * 查询调用单一客户授信复审信息
     *
     * @param queryModel
     * @return
     */
    @Transactional(readOnly = true)
    public List<TmpLmtAppPer> queryApprLmtSubBasicInfo(QueryModel queryModel) {
        List<TmpLmtAppPer> records = (List<TmpLmtAppPer>) tmpLmtAppPerMapper.queryApprLmtSubBasicInfo(queryModel);
        return records;
    }
}
