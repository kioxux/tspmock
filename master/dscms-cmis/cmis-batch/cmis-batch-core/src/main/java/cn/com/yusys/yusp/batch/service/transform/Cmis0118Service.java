package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0116Mapper;
import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0118Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0118</br>
 * 任务名称：加工任务-业务处理-其他事项申报日终  </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0118Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0118Service.class);

    @Autowired
    private Cmis0118Mapper cmis0118Mapper;


    /**
     *  更新外币利率定价申请信息表
     * @param openDay
     */
    public void cmis0018UpdateOtherForRateApp(String openDay) {
        logger.info("更新外币利率定价申请信息表开始，请求参数：[{}]",openDay);
        int updateOtherForRateApp=cmis0118Mapper.updateOtherForRateApp(openDay);
        logger.info("更新外币利率定价申请信息表结束，响应参数：[{}]",updateOtherForRateApp);
    }

    /**
     * 更新 保证金存款特惠利率客户名单
     * @param openDay
     */
    public void cmis0018UpdateOtherBailDepPreferRateAppCusList(String openDay) {
        logger.info("更新保证金存款特惠利率客户名单开始，请求参数：[{}]",openDay);
        int updateOtherBailDepPreferRateAppCusList=cmis0118Mapper.updateOtherBailDepPreferRateAppCusList(openDay);
        logger.info("更新保证金存款特惠利率客户名单结束，响应参数：[{}]",updateOtherBailDepPreferRateAppCusList);
    }

    /**
     * 更新 银票手续费优惠客户名单
     * @param openDay
     */
    public void cmis0018UpdateOtherAccpPerferFeeAppCusList(String openDay) {
        logger.info("更新银票手续费优惠客户名单开始，请求参数：[{}]",openDay);
        int updateOtherAccpPerferFeeAppCusList=cmis0118Mapper.updateOtherAccpPerferFeeAppCusList(openDay);
        logger.info("更新银票手续费优惠客户名单结束，响应参数：[{}]",updateOtherAccpPerferFeeAppCusList);
    }
}
