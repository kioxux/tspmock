package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0115</br>
 * 任务名称：加工任务-业务处理-风险预警处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0115Mapper {

    /**
     * 插入风险预警业务表
     *
     * @param openDay
     */
    int insertBatBizRiskSign(@Param("openDay") String openDay);


}
