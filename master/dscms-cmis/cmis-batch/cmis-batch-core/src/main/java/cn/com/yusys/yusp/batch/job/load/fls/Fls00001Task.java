package cn.com.yusys.yusp.batch.job.load.fls;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskCfg;
import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.domain.bat.BatTransferCfg;
import cn.com.yusys.yusp.batch.domain.load.fls.BatSFlsAppContractInfo;
import cn.com.yusys.yusp.batch.domain.load.fls.BatTFlsAppContractInfo;
import cn.com.yusys.yusp.batch.job.common.OdsCommonFileItemReader;
import cn.com.yusys.yusp.batch.job.common.OdsSCommonFileItemReader;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.repository.mapper.load.fls.BatSFlsAppContractInfoMapper;
import cn.com.yusys.yusp.batch.repository.mapper.load.fls.BatTFlsAppContractInfoMapper;
import cn.com.yusys.yusp.batch.service.bat.BatTaskCfgService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.bat.BatTransferCfgService;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.batch.service.load.fls.BatSFlsAppContractInfoService;
import cn.com.yusys.yusp.batch.service.load.fls.BatTFlsAppContractInfoService;
import cn.com.yusys.yusp.batch.util.DataLoadUtils;
import cn.com.yusys.yusp.batch.util.FileUtils;
import cn.com.yusys.yusp.batch.util.ShellUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.BatConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：FLS00001</br>
 * 任务名称：文件处理任务-非零评级系统-合同表[fls_app_contract_info]</br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2021年7月5日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Fls00001Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Fls00001Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Value("${batch.task.throttle-limit:96}")
    public int throttleLimit;
    @Value("${batch.proxyHost:10.28.111.113}")
    public String proxyHost;
    @Value("${batch.proxyPort:15770}")
    public String proxyPort;
    @Value("${batch.user:dba}")
    public String user;
    @Value("${batch.password:123456}")
    public String password;
    @Autowired
    @Qualifier("batchTaskPoolExecutor")
    private ThreadPoolTaskExecutor batchTaskExecutor;
    @Autowired
    private BatTFlsAppContractInfoService tService;//T表
    @Autowired
    private BatSFlsAppContractInfoService sService;//S表
    @Autowired
    private BatTransferCfgService batTransferCfgService;//文件传输配置信息
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskCfgService batTaskCfgService;//任务配置信息
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息
    @Autowired
    private TableUtilsService tableUtilsService;

    @Bean
    public Job fls00001Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_JOB.key, JobStepEnum.FLS00001_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job fls00001Job = this.jobBuilderFactory.get(JobStepEnum.FLS00001_JOB.key)
                .start(fls00001UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(fls00001CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(fls00001CheckOkFileStep(WILL_BE_INJECTED))//检查文件是否已经生成
                .next(fls00001FetchFileStep(WILL_BE_INJECTED))//获取文件到本地目录
                // .next(fls00001TruncateAndLoadTStep(WILL_BE_INJECTED))//清空落地表
                // .next(fls00001LoadTStep())//将数据从文件插入落地表
                .next(fls00001DeleteAndLoadSStep(WILL_BE_INJECTED))//删除S表当天数据
                // .next(fls00001LoadSStep())//将数据从文件插入S表
                // .next(fls00001MergeT2SStep(WILL_BE_INJECTED))//将T表数据merge到S表
                .next(fls00001UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return fls00001Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step fls00001UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_UPDATE_TASK010_STEP.key, JobStepEnum.FLS00001_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step fls00001UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.FLS00001_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.FLS00001_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.FLS00001_UPDATE_TASK010_STEP.key, JobStepEnum.FLS00001_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return fls00001UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step fls00001CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_CHECK_REL_STEP.key, JobStepEnum.FLS00001_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step fls00001CheckRelStep = this.stepBuilderFactory.get(JobStepEnum.FLS00001_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.FLS00001_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.FLS00001_CHECK_REL_STEP.key, JobStepEnum.FLS00001_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.FLS00001_CHECK_REL_STEP.key, JobStepEnum.FLS00001_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return fls00001CheckRelStep;
    }

    /**
     * 将T表数据merge到S表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step fls00001MergeT2SStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_MERGE_T2S_STEP.key, JobStepEnum.FLS00001_MERGE_T2S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step fls00001MergeT2SStep = this.stepBuilderFactory.get(JobStepEnum.FLS00001_MERGE_T2S_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    sService.mergeT2S(openDay);
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.FLS00001_MERGE_T2S_STEP.key, JobStepEnum.FLS00001_MERGE_T2S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();

        return fls00001MergeT2SStep;
    }

    /**
     * 将数据从文件插入S表
     *
     * @return
     */
    @Bean
    @JobScope
    public Step fls00001LoadSStep() {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_LOAD_S_STEP.key, JobStepEnum.FLS00001_LOAD_S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        logger.info("最大使用线程池数量为[{}]", throttleLimit);
        Step fls00001LoadSStep = this.stepBuilderFactory.get(JobStepEnum.FLS00001_LOAD_S_STEP.key)
                .<BatSFlsAppContractInfo, BatSFlsAppContractInfo>chunk(BatConstance.CHUNK_SIZE)
                .reader(batSFlsAppContractInfoReader(WILL_BE_INJECTED))
                .writer(list -> {
                    // 获取线程批量Session
                    SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH);
                    // 获取Mapper
                    BatSFlsAppContractInfoMapper mapper = sqlSession.getMapper(BatSFlsAppContractInfoMapper.class);
                    list.forEach(sInfo -> {
                        mapper.insert(sInfo);
                    });
                    logger.info(TradeLogConstants.BATCH_STEP_RESULT_PREFIX_LOGGER, JobStepEnum.FLS00001_LOAD_S_STEP.key, JobStepEnum.FLS00001_LOAD_S_STEP.value, list.size());
                    // 推送更新
                    sqlSession.flushStatements();
                    // 关闭线程批量Session
                    sqlSession.close();
                    TimeUnit.MILLISECONDS.sleep(500);// sleep 500毫秒(0.5秒)
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.FLS00001_LOAD_S_STEP.key, JobStepEnum.FLS00001_LOAD_S_STEP.value, "关闭线程批量Session后该线程休眠500毫秒");
                })
                .taskExecutor(batchTaskExecutor)
                .throttleLimit(throttleLimit)
                .build();
        return fls00001LoadSStep;
    }


    /**
     * 删除S表当天数据
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step fls00001DeleteAndLoadSStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step fls00001DeleteAndLoadSStep = this.stepBuilderFactory.get(JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    int result = sService.deleteByOpenDay(openDay);
                    logger.info(TradeLogConstants.BATCH_STEP_RESULT_PREFIX_LOGGER, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.value, result);
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.FLS00001_TASK.key);
                    String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
                    String dataFileName = batTTabName.concat(".txt");//数据文件名
                    String batSTabName = "bat_s_fls_app_contract_info";//数据文件S表名
                    String logFileName = batSTabName.concat(".log");//日志文件名
                    logger.info("load_data中相关参数值为proxy_host is[{}], proxy_port is[{}], user is[{}], password is[{}]", proxyHost, proxyPort, user, password);
                    String filePathDate = DateUtils.formatDate(openDayDate, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd
                    // String cmd = "imp_tdsql.sh 10.28.111.113 15770 dba 123456 cmis_batch.bat_t_core_klnl_dkqgmx 20210702 bat_t_core_klnl_dkqgmx.txt  bat_t_core_klnl_dkqgmx.log";
                    String cmd = "imp_tdsql.sh".concat(" ")
                            .concat(proxyHost).concat(" ")// IP地址
                            .concat(proxyPort).concat(" ")// 端口
                            .concat(user).concat(" ") //用户名
                            .concat(password).concat(" ")//密码
                            .concat("cmis_batch.").concat(batSTabName).concat(" ")//导入的表名
                            .concat(filePathDate).concat(" ")//文件路径
                            .concat(dataFileName).concat(" ")// 文件名称
                            .concat(logFileName);// 日志名称
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.value, "待执行命令为:[" + cmd + "]");
                    ShellUtils.runShellByProcessBuilder(cmd);

                    // 检查转换后的文件中的数据条数和数据库中的数据条数是否相等
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.value, "检查转换后的文件中的数据条数和数据库中的数据条数是否相等开始");
                    String dbName = "cmis_batch";
                    String tableName = "bat_s_fls_app_contract_info";
                    Integer countTableLines = tableUtilsService.countTableLines(dbName, tableName);
                    DataLoadUtils dataLoadUtils = new DataLoadUtils();
                    boolean flag = dataLoadUtils.checkFileLinesEqualsDb(batTaskRun, countTableLines);
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.value, "检查转换后的文件中的数据条数和数据库中的数据条数是否相等结束,返回标志为:[" + flag + "]");

                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.key, JobStepEnum.FLS00001_DELETE_AND_LOAD_S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();

        return fls00001DeleteAndLoadSStep;
    }

    /**
     * 将数据从文件插入落地表
     *
     * @return
     */
    @Bean
    @JobScope
    public Step fls00001LoadTStep() {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_LOAD_T_STEP.key, JobStepEnum.FLS00001_LOAD_T_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        logger.info("最大使用线程池数量为[{}]", throttleLimit);
        Step fls00001LoadTStep = this.stepBuilderFactory.get(JobStepEnum.FLS00001_LOAD_T_STEP.key)
                .<BatTFlsAppContractInfo, BatTFlsAppContractInfo>chunk(BatConstance.CHUNK_SIZE)
                .reader(batTFlsAppContractInfoReader(WILL_BE_INJECTED))
                .writer(list -> {
                    // 获取线程批量Session
                    SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH);
                    // 获取Mapper
                    BatTFlsAppContractInfoMapper mapper = sqlSession.getMapper(BatTFlsAppContractInfoMapper.class);
                    list.forEach(tInfo -> {
                        mapper.insert(tInfo);
                    });
                    logger.info(TradeLogConstants.BATCH_STEP_RESULT_PREFIX_LOGGER, JobStepEnum.FLS00001_LOAD_T_STEP.key, JobStepEnum.FLS00001_LOAD_T_STEP.value, list.size());
                    // 推送更新
                    sqlSession.flushStatements();
                    // 关闭线程批量Session
                    sqlSession.close();
                    TimeUnit.MILLISECONDS.sleep(500);// sleep 500毫秒(0.5秒)
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.FLS00001_LOAD_T_STEP.key, JobStepEnum.FLS00001_LOAD_T_STEP.value, "关闭线程批量Session后该线程休眠500毫秒");
                })
                .taskExecutor(batchTaskExecutor)
                .throttleLimit(throttleLimit)
                .build();

        return fls00001LoadTStep;
    }


    /**
     * 清空落地表
     *
     * @return
     */
    @Bean
    @JobScope
    public Step fls00001TruncateAndLoadTStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_TRUNCATE_AND_LOAD_T_STEP.key, JobStepEnum.FLS00001_TRUNCATE_AND_LOAD_T_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step fls00001TruncateAndLoadTStep = this.stepBuilderFactory.get(JobStepEnum.FLS00001_TRUNCATE_AND_LOAD_T_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    tService.truncateTTable();

                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.FLS00001_TASK.key);
                    String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
                    String dataFileName = batTTabName.concat(".txt");//数据文件名
                    String logFileName = batTTabName.concat(".log");//日志文件名
                    logger.info("load_data中相关参数值为proxy_host is[{}], proxy_port is[{}], user is[{}], password is[{}]", proxyHost, proxyPort, user, password);
                    Date openDayDate = DateUtils.parseDateByDef(openDay);
                    String filePathDate = DateUtils.formatDate(openDayDate, "yyyyMMdd");// 将yyyy-MM-dd 调整为 yyyyMMdd
                    // String cmd = "imp_tdsql.sh 10.28.111.113 15770 dba 123456 cmis_batch.bat_t_core_klnl_dkqgmx 20210702 bat_t_core_klnl_dkqgmx.txt  bat_t_core_klnl_dkqgmx.log";
                    String cmd = "imp_tdsql.sh".concat(" ")
                            .concat(proxyHost).concat(" ")// IP地址
                            .concat(proxyPort).concat(" ")// 端口
                            .concat(user).concat(" ") //用户名
                            .concat(password).concat(" ")//密码
                            .concat("cmis_batch.").concat(batTTabName).concat(" ")//导入的表名
                            .concat(filePathDate).concat(" ")//文件路径
                            .concat(dataFileName).concat(" ")// 文件名称
                            .concat(logFileName);// 日志名称
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.FLS00001_TRUNCATE_AND_LOAD_T_STEP.key, JobStepEnum.FLS00001_TRUNCATE_AND_LOAD_T_STEP.value, "待执行命令为:[" + cmd + "]");
                    ShellUtils.runShellByProcessBuilder(cmd);
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.FLS00001_TRUNCATE_AND_LOAD_T_STEP.key, JobStepEnum.FLS00001_TRUNCATE_AND_LOAD_T_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return fls00001TruncateAndLoadTStep;
    }

    /**
     * 获取文件到本地目录
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step fls00001FetchFileStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_FETCH_FILE_STEP.key, JobStepEnum.FLS00001_FETCH_FILE_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step fls00001FetchFileStep = this.stepBuilderFactory.get(JobStepEnum.FLS00001_FETCH_FILE_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.FLS00001_TASK.key);
                    BatTransferCfg batTransferCfg = batTransferCfgService.selectByPrimaryKey(TaskEnum.FLS00001_TASK.key);
                    BatTaskCfg batTaskCfg = batTaskCfgService.selectByPrimaryKey(TaskEnum.FLS00001_TASK.key);
                    DataLoadUtils dataLoadUtils = new DataLoadUtils();
                    dataLoadUtils.downloadTaskFiles(batTransferCfg, batTaskRun);
                    // 调用Shell脚本 将文件由GBK格式转换为UTF-8格式
                    dataLoadUtils.changeCharacter(batTaskRun);
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.FLS00001_FETCH_FILE_STEP.key, JobStepEnum.FLS00001_FETCH_FILE_STEP.value, "删除3日前文件开始");
                    FileUtils.delete3DaysBeforeFiles(openDay, batTransferCfg, batTaskRun);
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.FLS00001_FETCH_FILE_STEP.key, JobStepEnum.FLS00001_FETCH_FILE_STEP.value, "删除3日前文件结束");
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.FLS00001_FETCH_FILE_STEP.key, JobStepEnum.FLS00001_FETCH_FILE_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();

        return fls00001FetchFileStep;
    }

    /**
     * 检查文件是否已经生成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step fls00001CheckOkFileStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_CHECK_OK_FILE_STEP.key, JobStepEnum.FLS00001_CHECK_OK_FILE_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step fls00001CheckOkFileStep = this.stepBuilderFactory.get(JobStepEnum.FLS00001_CHECK_OK_FILE_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.FLS00001_TASK.key);
                    BatTransferCfg batTransferCfg = batTransferCfgService.selectByPrimaryKey(TaskEnum.FLS00001_TASK.key);

                    DataLoadUtils dataLoadUtils = new DataLoadUtils();
                    //  检查远程目录信号文件是否存在
                    boolean remoteFileFlag = dataLoadUtils.checkOdsSign(batTransferCfg, batTaskRun);
                    if (remoteFileFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.FLS00001_CHECK_OK_FILE_STEP.key, JobStepEnum.FLS00001_CHECK_OK_FILE_STEP.value, "检查文件未生成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.FLS00001_CHECK_OK_FILE_STEP.key, JobStepEnum.FLS00001_CHECK_OK_FILE_STEP.value, "检查文件未生成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return fls00001CheckOkFileStep;
    }

    /**
     * 将文件解析到数据库(S表)中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @StepScope
    public FlatFileItemReader<? extends BatSFlsAppContractInfo> batSFlsAppContractInfoReader(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_READER_S_STEP.key, JobStepEnum.FLS00001_READER_S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.FLS00001_TASK.key);
        String delimiter = batTaskRun.getFileSplit();//文件分隔符
        String workDir = batTaskRun.getWorkDir();//工作目录
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String workFileName = batTTabName.concat(".txt");
        String filePath = workDir + workFileName;
        FlatFileItemReader sIitemReader = new OdsSCommonFileItemReader<>(BatSFlsAppContractInfo.class, filePath, openDay, delimiter, BatConstance.ENCODING_UTF8);
        logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.FLS00001_READER_S_STEP.key, JobStepEnum.FLS00001_READER_S_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return sIitemReader;
    }

    /**
     * 将文件解析到数据库(T表)中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @StepScope
    public FlatFileItemReader<? extends BatTFlsAppContractInfo> batTFlsAppContractInfoReader(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_READER_T_STEP.key, JobStepEnum.FLS00001_READER_T_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.FLS00001_TASK.key);
        String delimiter = batTaskRun.getFileSplit();//文件分隔符
        String workDir = batTaskRun.getWorkDir();//工作目录
        String batTTabName = batTaskRun.getBatTTabName();//数据文件落地表名
        String workFileName = batTTabName.concat(".txt");
        String filePath = workDir + workFileName;
        FlatFileItemReader tItemReader = new OdsCommonFileItemReader<>(BatTFlsAppContractInfo.class, filePath, openDay, delimiter, BatConstance.ENCODING_UTF8);
        logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.FLS00001_READER_T_STEP.key, JobStepEnum.FLS00001_READER_T_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return tItemReader;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step fls00001UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.FLS00001_UPDATE_TASK100_STEP.key, JobStepEnum.FLS00001_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step fls00001UpdateTask100Step = this.stepBuilderFactory.get(JobStepEnum.FLS00001_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.FLS00001_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.FLS00001_UPDATE_TASK100_STEP.key, JobStepEnum.FLS00001_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return fls00001UpdateTask100Step;
    }
}
