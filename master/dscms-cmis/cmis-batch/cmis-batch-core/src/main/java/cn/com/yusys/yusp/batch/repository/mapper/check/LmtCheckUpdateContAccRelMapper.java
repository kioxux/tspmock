/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.check;

import org.apache.ibatis.annotations.Param;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdateContAccRelMapper
 * @类描述: #Dao类
 * @功能描述: 数据更新额度校正-台账临时表
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCheckUpdateContAccRelMapper {

    /**
     * -- 2.1、贷款台账：金额及余额、及状态更新
     */
    int update_dktz(@Param("cusId") String cusId);

    int insertTmpTdcarAlData(@Param("cusId") String cusId);


    /**
     * 2.2、贷款出账申请：金额及余额、及状态更新
     */
    int insert_dkczsq(@Param("cusId") String cusId);
    int update_dkczsq(@Param("cusId") String cusId);


    /**
     * 2.3、委托贷款台账：金额及余额、及状态更新
     */
    int insert_wtdktz(@Param("cusId") String cusId);
    int update_wtdktz(@Param("cusId") String cusId);


    /**
     * 2.4、委托贷款出账申请：金额及余额、及状态更新
     */
    int insert_wtdkczsq(@Param("cusId") String cusId);
    int update_wtdkczsq(@Param("cusId") String cusId);

    /**
     * 2.5、银票台账：金额及余额、及状态更新
     */
    int update_yptz_1(@Param("cusId") String cusId);

    int update_yptz_2(@Param("cusId") String cusId);

    int update_yptz_3(@Param("cusId") String cusId);

    /**
     * 2.6、银票台账出账申请：金额及余额、及状态更新
     */
    int update_yptzczsq(@Param("cusId") String cusId);

    /**
     * 2.7、保函台账：金额及余额、及状态更新
     */
    int update_bhtz(@Param("cusId") String cusId);

    /**
     * 2.8、信用证台账：金额及余额、及状态更新
     */
    int update_xyztz(@Param("cusId") String cusId);

    /**
     * 2.9、贴现台账：金额及余额、及状态更新
     */
    int update_txtz(@Param("cusId") String cusId);

    /**
     * 2.10、交易业务状态如果为注销、作废，则更新占用余额和占用敞口余额为0
     */
    int update_ck_jyzt(@Param("cusId") String cusId);

    /**
     * 2.11、若担保方式为低风险担保，则更新占用敞口为0
     */
    int update_ck_dbfs(@Param("cusId") String cusId);
}