package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0202Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0202</br>
 * 任务名称：加工任务-额度处理-表内占用总余额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0202Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0202Service.class);
    @Autowired
    private TableUtilsService tableUtilsService;
    @Autowired
    private Cmis0202Mapper cmis0202Mapper;

    /**
     * 更新额度占用关系01，对应SQL为CMIS0202-表内占用总余额.sql
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0202UpdateLmtContRel(String openDay) {
        logger.info("********[表内业务  BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 用信最高额（未到期）：用信申请金额/用信合同金额（不管是否还款，不恢复额度） ]********开始");
        logger.info("更新额度占用关系开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateLmtContRel01A();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("更新额度占用关系结束");

        logger.info("清空额度中间表(用于insertLmtContRel01A)开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateTmpLmtMid01A();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_mid");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空额度中间表(用于insertLmtContRel01A)结束");

        logger.info("插入额度中间表(用于insertLmtContRel01A)开始,请求参数为:[{}]", openDay);
        int insertTmpLmtMid01A = cmis0202Mapper.insertTmpLmtMid01A(openDay);
        logger.info("插入额度中间表(用于insertLmtContRel01A)结束,返回参数为:[{}]", insertTmpLmtMid01A);


        logger.info("表内业务  BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 用信最高额（未到期）：用信申请金额/用信合同金额（不管是否还款，不恢复额度）开始,请求参数为:[{}]", openDay);
        int insertLmtContRel01A = cmis0202Mapper.insertLmtContRel01A(openDay);
        logger.info("表内业务  BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 用信最高额（未到期）：用信申请金额/用信合同金额（不管是否还款，不恢复额度）结束,返回参数为:[{}]", insertLmtContRel01A);

        logger.info("********[表内业务  BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 用信最高额（未到期）：用信申请金额/用信合同金额（不管是否还款，不恢复额度） ]********结束");


        logger.info("********[额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额 ]********开始");
        logger.info("truncateTmpLmtMid01B清空额度中间表(用于insertLmtContRel01B)开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateTmpLmtMid01B();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_mid");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpLmtMid01B清空额度中间表(用于insertLmtContRel01B)结束");

        logger.info("插入额度中间表(用于insertLmtContRel01B)开始,请求参数为:[{}]", openDay);
        int insertTmpLmtMid01B = cmis0202Mapper.insertTmpLmtMid01B(openDay);
        logger.info("插入额度中间表(用于insertLmtContRel01B)结束,返回参数为:[{}]", insertTmpLmtMid01B);

        logger.info("规则：未到期最高额 BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则  （未到期）  用信申请金额/用信合同金额（不管是否还款，不恢复额度）开始,请求参数为:[{}]", openDay);
        int insertLmtContRel01B = cmis0202Mapper.insertLmtContRel01B(openDay);
        logger.info("规则：未到期最高额 BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则  （未到期）  用信申请金额/用信合同金额（不管是否还款，不恢复额度）结束,返回参数为:[{}]", insertLmtContRel01B);

        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额开始,请求参数为:[{}]", openDay);
        int updateLmtContRel01B = cmis0202Mapper.updateLmtContRel01B(openDay);
        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额结束,返回参数为:[{}]", updateLmtContRel01B);
        logger.info("********[额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额 ]********结束");


        logger.info("truncateLmtContRel03更新额度占用关系开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateLmtContRel03();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateLmtContRel03更新额度占用关系结束");

        logger.info("truncateTmpLmtMid03A清空额度中间表(用于insertLmtContRel03A)开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateTmpLmtMid03A();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_mid");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpLmtMid03A清空额度中间表(用于insertLmtContRel03A)结束");


        /*效率问题此步骤已拆分 modiffby曹际洲-》20211013*/
//        logger.info("插入额度中间表(用于insertLmtContRel03A)开始,请求参数为:[{}]", openDay);
//        int insertTmpLmtMid03A = cmis0202Mapper.insertTmpLmtMid03A(openDay);
//        logger.info("插入额度中间表(用于insertLmtContRel03A)结束,返回参数为:[{}]", insertTmpLmtMid03A);

        logger.info("truncateTmpDealContAaccRel0202清空合同占用关系信息加工表0202开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateTmpDealContAaccRel0202();
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_deal_cont_acc_rel_0202");// 20211027 调整为调用重命名创建和删除相关表
        logger.info("truncateTmpDealContAaccRel0202清空合同占用关系信息加工表0202结束");

        logger.info("插入合同占用关系信息加工表0202开始插入合同占用关系信息加工表0202,请求参数为:[{}]", openDay);
        int insertTmpDealContAaccRel0202 = cmis0202Mapper.insertTmpDealContAaccRel0202(openDay);
        logger.info("插入合同占用关系信息加工表0202结束,返回参数为:[{}]", insertTmpDealContAaccRel0202);


        logger.info("插入临时表-额度中间表 4A步骤开始,请求参数为:[{}]", openDay);
        int insertTmpLmtMid4A = cmis0202Mapper.insertTmpLmtMid4A(openDay);
        logger.info("插入临时表-额度中间表 4A步骤结束,返回参数为:[{}]", insertTmpLmtMid4A);

        logger.info("插入临时表-额度中间表 4B步骤开始,请求参数为:[{}]", openDay);
        int insertTmpLmtMid4B = cmis0202Mapper.insertTmpLmtMid4B(openDay);
        logger.info("插入临时表-额度中间表 4B步骤结束,返回参数为:[{}]", insertTmpLmtMid4B);

        logger.info("表内业务  BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则   用信一般合同（未到期）：用信申请金额/用信合同金额-用信申请下的台账总额+用信申请下的台账余额,请求参数为:[{}] ", openDay);
        int insertLmtContRel03A = cmis0202Mapper.insertLmtContRel03A(openDay);
        logger.info("表内业务  BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则   用信一般合同（未到期）：用信申请金额/用信合同金额-用信申请下的台账总额+用信申请下的台账余额结束,返回参数为:[{}]", insertLmtContRel03A);

        logger.info("truncateTmpLmtMid03B清空额度中间表(用于insertLmtContRel03B)开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateTmpLmtMid03B();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_mid");// 20211027 调整为调用重命名创建和删除相关表
        logger.info("truncateTmpLmtMid03B清空额度中间表(用于insertLmtContRel03B)结束");

        logger.info("插入额度中间表(用于insertLmtContRel03B)开始,请求参数为:[{}]", openDay);
        int insertTmpLmtMid03B = cmis0202Mapper.insertTmpLmtMid03B(openDay);
        logger.info("插入额度中间表(用于insertLmtContRel03B)结束,返回参数为:[{}]", insertTmpLmtMid03B);

        logger.info("用信一般合同（未到期）：用信申请金额/用信合同金额-用信申请下的台账总额+用信申请下的台账余额,请求参数为:[{}] ", openDay);
        int insertLmtContRel03B = cmis0202Mapper.insertLmtContRel03B(openDay);
        logger.info("用信一般合同（未到期）：用信申请金额/用信合同金额-用信申请下的台账总额+用信申请下的台账余额结束,返回参数为:[{}]", insertLmtContRel03B);

        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额开始,请求参数为:[{}]", openDay);
        int updateLmtContRel03 = cmis0202Mapper.updateLmtContRel03(openDay);
        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额结束,返回参数为:[{}]", updateLmtContRel03);


        logger.info("********[规则： BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 （未到期） 用信申请金额/用信合同金额（不管是否还款，不恢复额度） ]********开始");
        logger.info("truncateLmtContRel04更新额度占用关系开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateLmtContRel04();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表
        logger.info("truncateLmtContRel04更新额度占用关系结束");

        logger.info("truncateTmpLmtMid04A清空额度中间表(用于insertLmtContRel04A)开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateTmpLmtMid04A();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_mid");// 20211027 调整为调用重命名创建和删除相关表
        logger.info("truncateTmpLmtMid04A清空额度中间表(用于insertLmtContRel04A)结束");

        logger.info("插入额度中间表(用于insertLmtContRel04A)开始,请求参数为:[{}]", openDay);
        int insertTmpLmtMid04A = cmis0202Mapper.insertTmpLmtMid04A(openDay);
        logger.info("插入额度中间表(用于insertLmtContRel04A)结束,返回参数为:[{}]", insertTmpLmtMid04A);

        logger.info("规则： BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 （未到期） 用信申请金额/用信合同金额（不管是否还款，不恢复额度）开始,请求参数为:[{}]", openDay);
        int insertLmtContRel04A = cmis0202Mapper.insertLmtContRel04A(openDay);
        logger.info("规则： BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 （未到期） 用信申请金额/用信合同金额（不管是否还款，不恢复额度）结束,返回参数为:[{}]", insertLmtContRel04A);
        logger.info("********[规则： BIZ_TOTAL_BALANCE_AMT_CNY 占用总余额（折人民币）处理规则 （未到期） 用信申请金额/用信合同金额（不管是否还款，不恢复额度） ]********结束");


        logger.info("********[未到期的最高额资产池协议（及在途）占用额度]********开始");
        logger.info("truncateTmpLmtMidEd清空额度中间表(用于insertTmpLmtAmtEd)开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateTmpLmtMidEd();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_mid");// 20211027 调整为调用重命名创建和删除相关表
        logger.info("truncateTmpLmtMidEd清空额度中间表(用于insertTmpLmtAmtEd)结束");

        logger.info("插入额度中间表(用于insertTmpLmtAmtEd)开始,请求参数为:[{}]", openDay);
        int insertTmpLmtMidEd = cmis0202Mapper.insertTmpLmtMidEd(openDay);
        logger.info("插入额度中间表(用于insertTmpLmtAmtEd)结束,返回参数为:[{}]", insertTmpLmtMidEd);

        logger.info("已到期 ：未到期的最高额资产池协议（及在途）占用额度,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtEd = cmis0202Mapper.insertTmpLmtAmtEd(openDay);
        logger.info("已到期 ：未到期的最高额资产池协议（及在途）占用额度,返回参数为:[{}]", insertTmpLmtAmtEd);

        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额开始,请求参数为:[{}]", openDay);
        int updateLmtContRel06 = cmis0202Mapper.updateLmtContRel06(openDay);
        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额结束,返回参数为:[{}]", updateLmtContRel06);
        logger.info("********[未到期的最高额资产池协议（及在途）占用额度]********结束");


        logger.info("truncateLmtContRel06更新额度占用关系 ,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateLmtContRel06();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表
        logger.info("truncateLmtContRel06更新额度占用关系 结束");

        logger.info("最高额授信协议下台账占用授信的余额计算： 已到期 未到期的最高额授信协议下用信下台账余额、敞口余额开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtDk = cmis0202Mapper.insertTmpLmtAmtDk(openDay);
        logger.info("最高额授信协议下台账占用授信的余额计算： 已到期 未到期的最高额授信协议下用信下台账余额、敞口余额结束,返回参数为:[{}]", insertTmpLmtAmtDk);

        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额开始,请求参数为:[{}]", openDay);
        int updateLmtContRel04 = cmis0202Mapper.updateLmtContRel04(openDay);
        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额结束,返回参数为:[{}]", updateLmtContRel04);

        logger.info("truncateLmtContRel05更新额度占用关系 清理额度占用临时表开始,请求参数为:[{}]", openDay);
        // cmis0202Mapper.truncateLmtContRel05();
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_cop");// 20211027 调整为调用重命名创建和删除相关表
        logger.info("truncateLmtContRel05更新额度占用关系 清理额度占用临时表结束");

        logger.info("已到期 ：用信申请下的台账余额开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtTz = cmis0202Mapper.insertTmpLmtAmtTz(openDay);
        logger.info("已到期 ：用信申请下的台账余额结束,返回参数为:[{}]", insertTmpLmtAmtTz);

        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额开始,请求参数为:[{}]", openDay);
        int updateLmtContRel05 = cmis0202Mapper.updateLmtContRel05(openDay);
        logger.info("更新额度占用关系 占用总余额（折人民币） 赋值为关系表总占用额结束,返回参数为:[{}]", updateLmtContRel05);

        logger.info("更新额度占用关系中的占用总敞口余额开始,请求参数为:[{}]", openDay);
        int updateTdlcr = cmis0202Mapper.updateTdlcr(openDay);
        logger.info("更新额度占用关系中的占用总敞口余额结束,返回参数为:[{}]", updateTdlcr);
    }
}
