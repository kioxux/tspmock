package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKD0002</br>
 * 任务名称：批前备份日表任务-备份银承台账票据明细 [ACC_ACCP_DRFT_SUB]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakD0002Mapper {

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertCurrent(@Param("openDay") String openDay);

    /**
     * 删除当天的数据
     *
     * @return
     */
    void truncateCurrent();

    /**
     * 删除当天的数据
     *
     * @param openDay
     * @return
     */
    int deleteCurrent(@Param("openDay") String openDay);


    /**
     * 查询当天备份的[银承台账票据明细 [ACC_ACCP_DRFT_SUB]]数据总条数
     *
     * @param openDay
     * @return
     */
    int bakD0002DeleteOpenDayAccAccpDrftSubCounts(@Param("openDay") String openDay);

    /**
     * 查询当天的[银承台账票据明细 [ACC_ACCP_DRFT_SUB]]原表数据总条数
     *
     * @param openDay
     * @return
     */
    int queryAccAccpDrftSubCounts(@Param("openDay") String openDay);

}
