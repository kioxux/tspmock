package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0411Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-贷后管理-对公定期检查 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0411Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0411Service.class);

    @Autowired
    private Cmis0411Mapper cmis0411Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * 处理村镇贷后检查
     *
     * @param openDay
     */
    public void cmis0411InsertTownPspTAskList(String openDay) {
        logger.info("处理村镇贷后检查：村镇 公司类贷款，根据客户五级分类为后三类或本金逾期、欠息达60天的，每45天检查一次。进入诉讼的可不产生贷后任务。开始,请求参数为:[{}]", openDay);
        int insertTownPspTAskList01 = cmis0411Mapper.insertTownPspTAskList01(openDay);
        logger.info("处理村镇贷后检查：村镇 公司类贷款，根据客户五级分类为后三类或本金逾期、欠息达60天的，每45天检查一次。进入诉讼的可不产生贷后任务。结束,返回参数为:[{}]", insertTownPspTAskList01);

        logger.info("村镇：其他情况 公司类贷款（除低风险业务），贷后定期任务的频率为120天开始,请求参数为:[{}]", openDay);
        int insertTownPspTAskList02 = cmis0411Mapper.insertTownPspTAskList02(openDay);
        logger.info("村镇：其他情况 公司类贷款（除低风险业务），贷后定期任务的频率为120天结束,返回参数为:[{}]", insertTownPspTAskList02);
    }


    /**
     * 插入监控对公客户信息表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0411InsertBatBizMonitorCusinfo(String openDay) {
        // 重建相关表
        logger.info("重建贷后检查监控客户数据表[bat_biz_monitor_cusinfo]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz", "bat_biz_monitor_cusinfo");
        logger.info("重建【Cmis0411】贷后检查监控客户数据表[bat_biz_monitor_cusinfo]结束");

        logger.info("插入监控对公客户信息表开始,请求参数为:[{}]", openDay);
        int insertBatBizMonitorCusinfo = cmis0411Mapper.insertBatBizMonitorCusinfo(openDay);
        logger.info("插入监控对公客户信息表结束,返回参数为:[{}]", insertBatBizMonitorCusinfo);
    }

    /**
     * 插入 监控对公客户台账信息表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0411InsertBatBizMonitorCusaccinfo(String openDay) {
        // 重建相关表
        logger.info("重建贷后检查监控客户台账数据表[bat_biz_monitor_cusaccinfo]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_biz", "bat_biz_monitor_cusaccinfo");
        logger.info("重建【Cmis0411】贷后检查监控客户台账数据表[bat_biz_monitor_cusaccinfo]结束");

        logger.info("插入监控对公客户台账信息表开始,请求参数为:[{}]", openDay);
        int insertBatBizMonitorCusaccinfo = cmis0411Mapper.insertBatBizMonitorCusaccinfo(openDay);
        logger.info("插入监控对公客户台账信息表结束,返回参数为:[{}]", insertBatBizMonitorCusaccinfo);
    }

    /**
     * 更新监控对公客户信息表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0411UpdateBatBizMonitorCusinfo(String openDay) {
        // 重建相关表
        logger.info("重建贷后检查监控客户与对公客户台账信息关系表[tmp_bat_biz_monitor_cus_acc]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_bat_biz_monitor_cus_acc");
        logger.info("重建【Cmis0411】贷后检查监控客户与对公客户台账信息关系表[tmp_bat_biz_monitor_cus_acc]结束");

        logger.info("插入 临时表-贷后检查监控客户与对公客户台账信息关系表 开始,请求参数为:[{}]", openDay);
        int insertTmpBatBizMonitorCusAcc = cmis0411Mapper.insertTmpBatBizMonitorCusAcc(openDay);
        logger.info("插入 临时表-贷后检查监控客户与对公客户台账信息关系表 结束,返回参数为:[{}]", insertTmpBatBizMonitorCusAcc);

        logger.info("更新监控对公客户信息表开始,请求参数为:[{}]", openDay);
        int updateBatBizMonitorCusinfo = cmis0411Mapper.updateBatBizMonitorCusinfo(openDay);
        logger.info("更新监控对公客户信息表结束,返回参数为:[{}]", updateBatBizMonitorCusinfo);
    }

    /**
     * 插入 贷后检查任务表
     *
     * @param openDay
     */
    public void cmis0411InsertPspTaskList(String openDay) {
        logger.info("插入贷后检查任务表,五级分类 为正常、关注 ， 客户名下仅有客户名下仅有 010022   经营性物业贷款开始,请求参数为:[{}]", openDay);
        int insertPspTaskList01 = cmis0411Mapper.insertPspTaskList01(openDay);
        logger.info("插入贷后检查任务表,五级分类 为正常、关注 ， 客户名下仅有客户名下仅有 010022   经营性物业贷款结束,返回参数为:[{}]", insertPspTaskList01);

        logger.info("插入贷后检查任务表,五级分类 为正常、关注 ， 客户名下仅有 委托贷款开始,请求参数为:[{}]", openDay);
        int insertPspTaskList02 = cmis0411Mapper.insertPspTaskList02(openDay);
        logger.info("插入贷后检查任务表,五级分类 为正常、关注 ， 客户名下仅有 委托贷款结束,返回参数为:[{}]", insertPspTaskList02);

        logger.info("插入贷后检查任务表,其他贷款业务 要求 完成时间按照监控频率配置表开始,请求参数为:[{}]", openDay);
        int insertPspTaskList03 = cmis0411Mapper.insertPspTaskList03(openDay);
        logger.info("插入贷后检查任务表,其他贷款业务 要求 完成时间按照监控频率配置表结束,返回参数为:[{}]", insertPspTaskList03);

        logger.info("插入贷后检查任务表,瑕疵类贷款和不良贷款 任务生成日期为上次检查任务完成日期+1 日生成，要求完成日期为生成日期 +60T；  五级分类 为次级以下， 完成时间为生成日期+60T开始,请求参数为:[{}]", openDay);
        int insertPspTaskList04 = cmis0411Mapper.insertPspTaskList04(openDay);
        logger.info("插入贷后检查任务表,瑕疵类贷款和不良贷款 任务生成日期为上次检查任务完成日期+1 日生成，要求完成日期为生成日期 +60T；  五级分类 为次级以下， 完成时间为生成日期+60T结束,返回参数为:[{}]", insertPspTaskList04);

        logger.info(" 纯表外业务检查 开始,请求参数为:[{}]", openDay);
        int insertPspTaskList05 = cmis0411Mapper.insertPspTaskList05(openDay);
        logger.info(" 纯表外业务检查 结束,返回参数为:[{}]", insertPspTaskList05);

        logger.info("删除同一天客户产生多个任务，保留这一天最早的日终产生的任务 开始,请求参数为:[{}]", openDay);
        int deletetmpPspTastListForCusMore = cmis0411Mapper.deletetmpPspTastListForCusMore(openDay);
        logger.info("删除同一天客户产生多个任务，保留这一天最早的日终产生的任务 结束,返回参数为:[{}]", deletetmpPspTastListForCusMore);


//        logger.info("插入贷后检查任务表,自动检查  客户风险分类是正常类 且 授信额度<=200万 客户下的担保方式只能是抵押或者保证，且保证是专业担保公司，台账状态为正常 余额大于0开始,请求参数为:[{}]", openDay);
//        int insertPspTaskList05 = cmis0411Mapper.insertPspTaskList05(openDay);
//        logger.info("插入贷后检查任务表,自动检查  客户风险分类是正常类 且 授信额度<=200万 客户下的担保方式只能是抵押或者保证，且保证是专业担保公司，台账状态为正常 余额大于0结束,返回参数为:[{}]", insertPspTaskList05);
    }
}
