/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.transform.gjp;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: TmpPjgDrafacc
 * @类描述: tmp_pjg_drafacc数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 10:37:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_pjg_drafacc")
public class TmpPjgDrafacc extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 借据编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "BILL_NO")
	private String billNo;
	
	/** 保证金 **/
	@Column(name = "SECURITY_SUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal securitySum;
	
	/** 协议号 **/
	@Column(name = "DRAFTID", unique = false, nullable = true, length = 40)
	private String draftid;
	
	/** 币种 **/
	@Column(name = "CURRENCY", unique = false, nullable = true, length = 3)
	private String currency;
	
	/** 票面总金额 **/
	@Column(name = "BILLSUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billsum;
	
	/** 票据状态 STD_PJG_STATUS **/
	@Column(name = "PJG_STATUS", unique = false, nullable = true, length = 1)
	private String pjgStatus;
	
	/** 票据签发日 **/
	@Column(name = "STARTDATE", unique = false, nullable = true, length = 10)
	private String startdate;
	
	/** 票据承兑日 **/
	@Column(name = "PAYDATE", unique = false, nullable = true, length = 10)
	private String paydate;
	
	/** 票据到期日 **/
	@Column(name = "ENDDATE", unique = false, nullable = true, length = 10)
	private String enddate;
	
	/** 票据日期 **/
	@Column(name = "OFFDATE", unique = false, nullable = true, length = 10)
	private String offdate;
	
	/** 系统内部行号（机构） **/
	@Column(name = "ORGID", unique = false, nullable = true, length = 20)
	private String orgid;
	
	/** 票据介质 **/
	@Column(name = "SUBID", unique = false, nullable = true, length = 10)
	private String subid;
	
	/** 票据客户号 **/
	@Column(name = "CUSID", unique = false, nullable = true, length = 30)
	private String cusid;
	
	/** 票据客户名称 **/
	@Column(name = "CUSNAME", unique = false, nullable = true, length = 80)
	private String cusname;
	
	/** 票据借据号 **/
	@Column(name = "BILLNO", unique = false, nullable = true, length = 80)
	private String billno;
	
	/** 收款人名称 **/
	@Column(name = "RECVNA", unique = false, nullable = true, length = 80)
	private String recvna;
	
	/** 收款人账号 **/
	@Column(name = "RECVAC", unique = false, nullable = true, length = 80)
	private String recvac;
	
	/** 收款人开户行 **/
	@Column(name = "RECVBK", unique = false, nullable = true, length = 80)
	private String recvbk;
	
	/** 系统内部行号 **/
	@Column(name = "PYERBK", unique = false, nullable = true, length = 80)
	private String pyerbk;
	
	/** 票据担保方式 STD_DKDBFSHI **/
	@Column(name = "PJG_GUARANTEE_TYPE", unique = false, nullable = true, length = 2)
	private String pjgGuaranteeType;
	
	/** 票据手续费 **/
	@Column(name = "F_CHARGE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal fCharge;
	
	/** 承兑行行号 **/
	@Column(name = "S_AGCYSVCR", unique = false, nullable = true, length = 20)
	private String sAgcysvcr;
	
	/** 承兑行名称 **/
	@Column(name = "S_AGCYSVCRNAME", unique = false, nullable = true, length = 120)
	private String sAgcysvcrname;
	
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param securitySum
	 */
	public void setSecuritySum(java.math.BigDecimal securitySum) {
		this.securitySum = securitySum;
	}
	
    /**
     * @return securitySum
     */
	public java.math.BigDecimal getSecuritySum() {
		return this.securitySum;
	}
	
	/**
	 * @param draftid
	 */
	public void setDraftid(String draftid) {
		this.draftid = draftid;
	}
	
    /**
     * @return draftid
     */
	public String getDraftid() {
		return this.draftid;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    /**
     * @return currency
     */
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param billsum
	 */
	public void setBillsum(java.math.BigDecimal billsum) {
		this.billsum = billsum;
	}
	
    /**
     * @return billsum
     */
	public java.math.BigDecimal getBillsum() {
		return this.billsum;
	}
	
	/**
	 * @param pjgStatus
	 */
	public void setPjgStatus(String pjgStatus) {
		this.pjgStatus = pjgStatus;
	}
	
    /**
     * @return pjgStatus
     */
	public String getPjgStatus() {
		return this.pjgStatus;
	}
	
	/**
	 * @param startdate
	 */
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	
    /**
     * @return startdate
     */
	public String getStartdate() {
		return this.startdate;
	}
	
	/**
	 * @param paydate
	 */
	public void setPaydate(String paydate) {
		this.paydate = paydate;
	}
	
    /**
     * @return paydate
     */
	public String getPaydate() {
		return this.paydate;
	}
	
	/**
	 * @param enddate
	 */
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	
    /**
     * @return enddate
     */
	public String getEnddate() {
		return this.enddate;
	}
	
	/**
	 * @param offdate
	 */
	public void setOffdate(String offdate) {
		this.offdate = offdate;
	}
	
    /**
     * @return offdate
     */
	public String getOffdate() {
		return this.offdate;
	}
	
	/**
	 * @param orgid
	 */
	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}
	
    /**
     * @return orgid
     */
	public String getOrgid() {
		return this.orgid;
	}
	
	/**
	 * @param subid
	 */
	public void setSubid(String subid) {
		this.subid = subid;
	}
	
    /**
     * @return subid
     */
	public String getSubid() {
		return this.subid;
	}
	
	/**
	 * @param cusid
	 */
	public void setCusid(String cusid) {
		this.cusid = cusid;
	}
	
    /**
     * @return cusid
     */
	public String getCusid() {
		return this.cusid;
	}
	
	/**
	 * @param cusname
	 */
	public void setCusname(String cusname) {
		this.cusname = cusname;
	}
	
    /**
     * @return cusname
     */
	public String getCusname() {
		return this.cusname;
	}
	
	/**
	 * @param billno
	 */
	public void setBillno(String billno) {
		this.billno = billno;
	}
	
    /**
     * @return billno
     */
	public String getBillno() {
		return this.billno;
	}
	
	/**
	 * @param recvna
	 */
	public void setRecvna(String recvna) {
		this.recvna = recvna;
	}
	
    /**
     * @return recvna
     */
	public String getRecvna() {
		return this.recvna;
	}
	
	/**
	 * @param recvac
	 */
	public void setRecvac(String recvac) {
		this.recvac = recvac;
	}
	
    /**
     * @return recvac
     */
	public String getRecvac() {
		return this.recvac;
	}
	
	/**
	 * @param recvbk
	 */
	public void setRecvbk(String recvbk) {
		this.recvbk = recvbk;
	}
	
    /**
     * @return recvbk
     */
	public String getRecvbk() {
		return this.recvbk;
	}
	
	/**
	 * @param pyerbk
	 */
	public void setPyerbk(String pyerbk) {
		this.pyerbk = pyerbk;
	}
	
    /**
     * @return pyerbk
     */
	public String getPyerbk() {
		return this.pyerbk;
	}
	
	/**
	 * @param pjgGuaranteeType
	 */
	public void setPjgGuaranteeType(String pjgGuaranteeType) {
		this.pjgGuaranteeType = pjgGuaranteeType;
	}
	
    /**
     * @return pjgGuaranteeType
     */
	public String getPjgGuaranteeType() {
		return this.pjgGuaranteeType;
	}
	
	/**
	 * @param fCharge
	 */
	public void setFCharge(java.math.BigDecimal fCharge) {
		this.fCharge = fCharge;
	}
	
    /**
     * @return fCharge
     */
	public java.math.BigDecimal getFCharge() {
		return this.fCharge;
	}
	
	/**
	 * @param sAgcysvcr
	 */
	public void setSAgcysvcr(String sAgcysvcr) {
		this.sAgcysvcr = sAgcysvcr;
	}
	
    /**
     * @return sAgcysvcr
     */
	public String getSAgcysvcr() {
		return this.sAgcysvcr;
	}
	
	/**
	 * @param sAgcysvcrname
	 */
	public void setSAgcysvcrname(String sAgcysvcrname) {
		this.sAgcysvcrname = sAgcysvcrname;
	}
	
    /**
     * @return sAgcysvcrname
     */
	public String getSAgcysvcrname() {
		return this.sAgcysvcrname;
	}


}