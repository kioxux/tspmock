package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0301</br>
 * 任务名称：加工任务-业务处理-客户信息处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0301Mapper {
    /**
     * 更新客户管理任务信息[如果临时客户5天没有维护成正式客户，则更新客户任务表的状态为已完成]
     * @param openDay
     * @return
     */
    int updateCusManaTask(String openDay);
    /**
     * 更新客户基本信息[如果临时客户5天没有维护成正式客户，则更新此客户的管护人管护机构为空]
     *
     * @param openDay
     * @return
     */
    int updateCusBase(@Param("openDay") String openDay);

    /**
     * 清空临时表-不良客户信息表
     */
    void truncateTmpBadCus();

    /**
     *  加工临时表-不良客户信息表
     *
     * @param openDay
     * @return
     */
    int insertTmpBadCus(@Param("openDay") String openDay);

    /**
     *  初始化对公信息表的是否不良记录
     *
     * @param openDay
     * @return
     */
    int updateBadCusInitCusCorp(@Param("openDay") String openDay);

    /**
     *   更新对公信息表的是否不良记录
     *
     * @param openDay
     * @return
     */
    int updateBadCusbyCusCorp(@Param("openDay") String openDay);
}
