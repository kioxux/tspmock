/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.cfg;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: WbMsgNotice
 * @类描述: wb_msg_notice数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "wb_msg_notice")
public class WbMsgNotice extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 消息类型 **/
	@Column(name = "MESSAGE_TYPE", unique = false, nullable = true, length = 32)
	private String messageType;
	
	/** 内容 **/
	@Column(name = "CONTENT", unique = false, nullable = true, length = 1000)
	private String content;
	
	/** 消息状态 **/
	@Column(name = "MESSAGE_STATUS", unique = false, nullable = true, length = 5)
	private String messageStatus;
	
	/** 发布时间 **/
	@Column(name = "PUB_TIME", unique = false, nullable = true, length = 20)
	private String pubTime;
	
	/** 接收人类型 **/
	@Column(name = "RECEIVER_TYPE", unique = false, nullable = true, length = 20)
	private String receiverType;
	
	/** 短信编号 **/
	@Column(name = "MESSAGE_TYPE_ID", unique = false, nullable = true, length = 20)
	private String messageTypeId;
	
	/** 档案逾期天数 **/
	@Column(name = "doc_overdue_day", unique = false, nullable = true, length = 20)
	private String docOverdueDay;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 抵质押物名称 **/
	@Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 80)
	private String pldimnMemo;
	
	/** 批量呆销业务流水号 **/
	@Column(name = "PBDWAB_SERNO", unique = false, nullable = true, length = 40)
	private String pbdwabSerno;
	
	/** 核销总户数 **/
	@Column(name = "TOTAL_WRITEOFF_CUS", unique = false, nullable = true, length = 20)
	private String totalWriteoffCus;
	
	/** 核销总笔数 **/
	@Column(name = "TOTAL_WRITEOFF_NUM", unique = false, nullable = true, length = 20)
	private String totalWriteoffNum;
	
	/** 信用卡核销业务流水号 **/
	@Column(name = "PCWA_SERNO", unique = false, nullable = true, length = 40)
	private String pcwaSerno;
	
	/** 提前月份 **/
	@Column(name = "BEFORE_MONTH", unique = false, nullable = true, length = 20)
	private String beforeMonth;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param messageType
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
    /**
     * @return messageType
     */
	public String getMessageType() {
		return this.messageType;
	}
	
	/**
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
    /**
     * @return content
     */
	public String getContent() {
		return this.content;
	}
	
	/**
	 * @param messageStatus
	 */
	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}
	
    /**
     * @return messageStatus
     */
	public String getMessageStatus() {
		return this.messageStatus;
	}
	
	/**
	 * @param pubTime
	 */
	public void setPubTime(String pubTime) {
		this.pubTime = pubTime;
	}
	
    /**
     * @return pubTime
     */
	public String getPubTime() {
		return this.pubTime;
	}
	
	/**
	 * @param receiverType
	 */
	public void setReceiverType(String receiverType) {
		this.receiverType = receiverType;
	}
	
    /**
     * @return receiverType
     */
	public String getReceiverType() {
		return this.receiverType;
	}
	
	/**
	 * @param messageTypeId
	 */
	public void setMessageTypeId(String messageTypeId) {
		this.messageTypeId = messageTypeId;
	}
	
    /**
     * @return messageTypeId
     */
	public String getMessageTypeId() {
		return this.messageTypeId;
	}
	
	/**
	 * @param docOverdueDay
	 */
	public void setDocOverdueDay(String docOverdueDay) {
		this.docOverdueDay = docOverdueDay;
	}
	
    /**
     * @return docOverdueDay
     */
	public String getDocOverdueDay() {
		return this.docOverdueDay;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}
	
    /**
     * @return pldimnMemo
     */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param pbdwabSerno
	 */
	public void setPbdwabSerno(String pbdwabSerno) {
		this.pbdwabSerno = pbdwabSerno;
	}
	
    /**
     * @return pbdwabSerno
     */
	public String getPbdwabSerno() {
		return this.pbdwabSerno;
	}
	
	/**
	 * @param totalWriteoffCus
	 */
	public void setTotalWriteoffCus(String totalWriteoffCus) {
		this.totalWriteoffCus = totalWriteoffCus;
	}
	
    /**
     * @return totalWriteoffCus
     */
	public String getTotalWriteoffCus() {
		return this.totalWriteoffCus;
	}
	
	/**
	 * @param totalWriteoffNum
	 */
	public void setTotalWriteoffNum(String totalWriteoffNum) {
		this.totalWriteoffNum = totalWriteoffNum;
	}
	
    /**
     * @return totalWriteoffNum
     */
	public String getTotalWriteoffNum() {
		return this.totalWriteoffNum;
	}
	
	/**
	 * @param pcwaSerno
	 */
	public void setPcwaSerno(String pcwaSerno) {
		this.pcwaSerno = pcwaSerno;
	}
	
    /**
     * @return pcwaSerno
     */
	public String getPcwaSerno() {
		return this.pcwaSerno;
	}
	
	/**
	 * @param beforeMonth
	 */
	public void setBeforeMonth(String beforeMonth) {
		this.beforeMonth = beforeMonth;
	}
	
    /**
     * @return beforeMonth
     */
	public String getBeforeMonth() {
		return this.beforeMonth;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}