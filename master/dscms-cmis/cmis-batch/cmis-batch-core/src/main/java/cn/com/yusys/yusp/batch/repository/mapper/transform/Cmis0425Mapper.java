package cn.com.yusys.yusp.batch.repository.mapper.transform;

import com.alibaba.fastjson.JSON;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0425</br>
 * 任务名称：加工任务-贷后管理-风险分类子表数据生成</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0425Mapper {
    /**
     * 插入风险分类借据信息[对公专项贷款风险分类借据信息]
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfo01(Map map);


    /**
     * 委托贷款借据
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfowtdk(Map map);

    /**
     * 风险分类银承借据
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfoyc(Map map);

    /**
     * 风险分类保函借据
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfobh(Map map);

    /**
     * 风险分类信用证借据
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfoxyz(Map map);


    /**
     * 风险分类贴现借据
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfotx(Map map);






    /**
     * 插入风险分类借据信息[1)先执行大额： 个人经营性分类借据大额跑批矩阵]
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfo02(Map map);

    /**
     * 插入风险分类借据信息[2)再执行小额： 个人经营性分类借据小额跑批矩阵]
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfo03(Map map);

    /**
     * 插入风险分类借据信息[个人消费分类借据纯线上贷跑批矩阵]
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfo04(Map map);

    /**
     * 插入风险分类借据信息[个人消费分类借据大额跑批矩阵]
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfo05(Map map);

    /**
     * 插入风险分类借据信息[个人消费分类借据小额跑批矩阵]
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfo06(Map map);

    /**
     * 插入风险分类借据信息[个人消费分类借据住房按揭与汽车]
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfo07(Map map);

    int deleteRiskDebitInfoTmp(Map map);

    /**
     * 插入 风险分类担保合同分析
     *
     * @param map
     * @return
     */
    int insertRiskGuarContAnaly(Map map);

    /**
     * 插入 风险分类担保合同分析
     *
     * @param map
     * @return
     */
    int insertRiskGuarntrList(Map map);

    /**
     * 插入 风险分类抵质押物检查
     *
     * @param map
     * @return
     */
    int insertRiskPldimnList(Map map);

    int truncateRiskFin(Map map);

    /**
     * 清理客户最新财报时间 日间调用
     *
     * @param map
     * @return
     */
    int truncateRiskFin2(Map map);

    /**
     * 插入客户最新财报时间
     *
     * @param map
     * @return
     */
    int insertRiskFin(Map map);

    /**
     * 插入 企业财务情况-损益表
     *
     * @param map
     * @return
     */
    int insertRiskFinReportAnaly01(Map map);

    /**
     * 插入 企业财务情况-资产负债表
     *
     * @param map
     * @return
     */
    int insertRiskFinReportAnaly02(Map map);

    /**
     * 插入 企业财务情况-现金流量表
     *
     * @param map
     * @return
     */
    int insertRiskFinReportAnaly03(Map map);

    /**
     * 插入 企业财务情况-财务指标表
     *
     * @param map
     * @return
     */
    int insertRiskFinReportAnaly04(Map map);

    /**
     * 插入 影响偿还因素分析
     *
     * @param map
     * @return
     */
    int insertRiskRepayAnaly(Map map);

    /**
     * 更新风险分类任务表
     *
     * @param openDay
     * @return
     */
    int updateRiskTaskList(@Param("openDay") String openDay);

    /**
     * 清空临时表
     */
    void truncatetmpupdate();

    /**
     * 个人消费分类借据纯线上贷跑批矩阵自动分类数据插入
     *
     * @param openDay
     * @return
     */
    int inserttmpupdateOnlineLoan(@Param("openDay") String openDay);

    /**
     * 个人消费分类借据纯线上贷跑批矩阵自动分类数据更新到贷款台账
     *
     * @param openDay
     * @return
     */
    int updatetmpupdateOnlineLoan(@Param("openDay") String openDay);

    /**
     * 个人消费分类借据纯线上贷跑批矩阵自动分类数据更新为通过
     *
     * @param openDay
     * @return
     */
    int updateOnlineLoanTask(@Param("openDay") String openDay);


    /**
     * 插入 风险分类任务临时主表
     *
     * @param map
     */
    int insertTmpRiskTaskList(Map map);

    /**
     * 日间风险分类临时借据信息回插
     *
     * @param map
     * @return
     */
    int insertRiskDebitInfoFromTmp(Map map);

    /**
     * 删除 风险分类借据信息临时表 日间手工发起分类使用
     *
     * @param map
     */
    int deletTmpPspTaskList(Map map);

    /**
     * 删除 风险借据信息临时表 日间手工发起分类使用
     *
     * @param map
     */
    int deletTmpRiskDebitInfo(Map map);
    /**
     * 更新风险分类子表任务生成状态
     *
     * @author jijian_yx
     * @date 2021/9/30 1:46
     **/
    int updateRiskTaskListTaskStatus(Map map);
}
