/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.transform.gjp;

import cn.com.yusys.yusp.batch.domain.transform.gjp.TmpPjgDrafacc;
import cn.com.yusys.yusp.batch.service.transform.gjp.TmpPjgDrafaccService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: TmpPjgDrafaccResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 10:37:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/tmppjgdrafacc")
public class TmpPjgDrafaccResource {
    @Autowired
    private TmpPjgDrafaccService tmpPjgDrafaccService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<TmpPjgDrafacc>> query() {
        QueryModel queryModel = new QueryModel();
        List<TmpPjgDrafacc> list = tmpPjgDrafaccService.selectAll(queryModel);
        return new ResultDto<List<TmpPjgDrafacc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<TmpPjgDrafacc>> index(QueryModel queryModel) {
        List<TmpPjgDrafacc> list = tmpPjgDrafaccService.selectByModel(queryModel);
        return new ResultDto<List<TmpPjgDrafacc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{billNo}")
    protected ResultDto<TmpPjgDrafacc> show(@PathVariable("billNo") String billNo) {
        TmpPjgDrafacc tmpPjgDrafacc = tmpPjgDrafaccService.selectByPrimaryKey(billNo);
        return new ResultDto<TmpPjgDrafacc>(tmpPjgDrafacc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<TmpPjgDrafacc> create(@RequestBody TmpPjgDrafacc tmpPjgDrafacc) throws URISyntaxException {
        tmpPjgDrafaccService.insert(tmpPjgDrafacc);
        return new ResultDto<TmpPjgDrafacc>(tmpPjgDrafacc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody TmpPjgDrafacc tmpPjgDrafacc) throws URISyntaxException {
        int result = tmpPjgDrafaccService.update(tmpPjgDrafacc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{billNo}")
    protected ResultDto<Integer> delete(@PathVariable("billNo") String billNo) {
        int result = tmpPjgDrafaccService.deleteByPrimaryKey(billNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = tmpPjgDrafaccService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
