package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0400Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0400</br>
 * 任务名称：加工任务-贷后管理-首次检查[清理贷后首次检查临时表数据]  </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月17日 上午00:56:54
 */
@Service
@Transactional
public class Cmis0400Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0400Service.class);

    @Autowired
    private Cmis0400Mapper cmis0400Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;
    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0400TruncateTable(String openDay) {

        // 重建相关表
        logger.info("重建贷后检查任务表-临时表[tmp_psp_task_list]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_psp_task_list");
        logger.info("重建【CMIS0400】贷后检查任务表-临时表[tmp_psp_task_list]结束");

        // 重建相关表
        logger.info("重建定期检查借据信息表[tmp_psp_debit_info]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_psp_debit_info");
        logger.info("重建【CMIS0400】定期检查借据信息表[tmp_psp_debit_info]结束");

        // 重建相关表
        logger.info("重建客户最近一次贷后检查任务临时表[tmp_last_psp_task_list]开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_last_psp_task_list");
        logger.info("重建【CMIS0400】客户最近一次贷后检查任务临时表[tmp_last_psp_task_list]结束");
//        logger.info("22.清理贷后临时表-未完成的贷后检查任务表-开始,请求参数为:[{}]", openDay);
//        int truncateTable22 = cmis0400Mapper.truncateTable22(openDay);
//        logger.info("22.清理贷后临时表-未完成的贷后检查任务结束,返回参数为:[{}]", truncateTable22);
    }

    public void cmis0400InsertTmp(Map map) {
        int truncateTLMPTL = 0;
        logger.info("清理同一客户最后一次检查历史任务临时表-开始,请求参数为:[{}]", map);
        if (null != map.get("cusId") && !"".equals(map.get("cusId"))) {
            truncateTLMPTL = cmis0400Mapper.deleteTLMPTL(map);
        } else {
            // 重建相关表
            logger.info("重建客户最新贷后历史任务表[tmp_last_max_psp_task_list]开始,请求参数为:[{}]");
            tableUtilsService.renameCreateDropTable("cmis_psp", "tmp_last_max_psp_task_list");
            logger.info("重建【CMIS0400】客户最新贷后历史任务表[tmp_last_max_psp_task_list]结束");
        }
        logger.info("清理同一客户最后一次检查历史任务临时表-结束,返回参数为:[{}]", truncateTLMPTL);
        logger.info("加工同一客户最后一次检查历史任务临时表-开始,请求参数为:[{}]", map);
        int insertTLMPTL = cmis0400Mapper.insertTLMPTL(map);
        logger.info("加工同一客户最后一次检查历史任务临时表-结束,返回参数为:[{}]", insertTLMPTL);

        logger.info("加工获取同一客户最后一次检查历史任务-开始,请求参数为:[{}]", map);
        int insertTmp = cmis0400Mapper.insertTmp(map);
        logger.info("加工获取同一客户最后一次检查历史任务-结束,返回参数为:[{}]", insertTmp);

//        logger.info("加工未完成的贷后检查任务-开始,请求参数为:[{}]", map);
//        int insertTmp2 = cmis0400Mapper.insertTmp2(map);
//        logger.info("加工未完成的贷后检查任务-结束,返回参数为:[{}]", insertTmp2);
    }
}
