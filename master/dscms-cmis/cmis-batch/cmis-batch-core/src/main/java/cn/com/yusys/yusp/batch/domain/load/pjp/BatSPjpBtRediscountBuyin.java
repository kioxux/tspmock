/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtRediscountBuyin
 * @类描述: bat_s_pjp_bt_rediscount_buyin数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-03 22:08:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_pjp_bt_rediscount_buyin")
public class BatSPjpBtRediscountBuyin extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "REDISC_BUY_BILL_ID")
	private String rediscBuyBillId;
	
	/** 批次ID **/
	@Column(name = "REDISC_BUY_BATCH_ID", unique = false, nullable = true, length = 40)
	private String rediscBuyBatchId;
	
	/** 票据明细对象ID **/
	@Column(name = "BILLINFO_ID", unique = false, nullable = true, length = 40)
	private String billinfoId;
	
	/** 票号 **/
	@Column(name = "S_BILL_NO", unique = false, nullable = true, length = 40)
	private String sBillNo;
	
	/** 出票日 **/
	@Column(name = "D_ISSUE_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dIssueDt;
	
	/** 到期日 **/
	@Column(name = "D_DUE_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dDueDt;
	
	/** 出票人名称 **/
	@Column(name = "S_ISSUER_NAME", unique = false, nullable = true, length = 80)
	private String sIssuerName;
	
	/** 出票人账号 **/
	@Column(name = "S_ISSUER_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sIssuerAccount;
	
	/** 出票人开户行名称 **/
	@Column(name = "S_ISSUER_BANK_NAME", unique = false, nullable = true, length = 80)
	private String sIssuerBankName;
	
	/** 出票人开户行号 **/
	@Column(name = "S_ISSUER_BANK_CODE", unique = false, nullable = true, length = 40)
	private String sIssuerBankCode;
	
	/** 票面金额 **/
	@Column(name = "F_BILL_AMOUNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fBillAmount;
	
	/** 承兑人 **/
	@Column(name = "S_ACCEPTOR", unique = false, nullable = true, length = 80)
	private String sAcceptor;
	
	/** 收款人名称 **/
	@Column(name = "S_PAYEE_NAME", unique = false, nullable = true, length = 80)
	private String sPayeeName;
	
	/** 收款人开户行行号 **/
	@Column(name = "S_PAYEE_BANK_NAME", unique = false, nullable = true, length = 80)
	private String sPayeeBankName;
	
	/** 收款人账号 **/
	@Column(name = "S_PAYEE_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sPayeeAccount;
	
	/** 票据种类 **/
	@Column(name = "S_BILL_TYPE", unique = false, nullable = true, length = 10)
	private String sBillType;
	
	/** 票据介质 **/
	@Column(name = "S_BILL_MEDIA", unique = false, nullable = true, length = 10)
	private String sBillMedia;
	
	/** 票据状态 **/
	@Column(name = "S_BILL_STATUS", unique = false, nullable = true, length = 10)
	private String sBillStatus;
	
	/** 操作机构id **/
	@Column(name = "S_BRANCH_ID", unique = false, nullable = true, length = 40)
	private String sBranchId;
	
	/** 计息天数 **/
	@Column(name = "F_INT_DAYS", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal fIntDays;
	
	/** 实付金额（电票：报文实付金额） **/
	@Column(name = "F_PAYMENT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fPayment;
	
	/** 回复人备注 **/
	@Column(name = "REPLY_REMARK", unique = false, nullable = true, length = 800)
	private String replyRemark;
	
	/** 创建日期 **/
	@Column(name = "D_CREATE_DT", unique = false, nullable = true, length = 20)
	private String dCreateDt;
	
	/** 付款行开户行名称 **/
	@Column(name = "S_PAYER_BANK_NAME", unique = false, nullable = true, length = 80)
	private String sPayerBankName;
	
	/** 收款人开户行行号 **/
	@Column(name = "S_PAYEE_BANK_CODE", unique = false, nullable = true, length = 40)
	private String sPayeeBankCode;
	
	/** 利率 **/
	@Column(name = "F_RATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fRate;
	
	/** 利率类型 **/
	@Column(name = "S_RATE_TYPE", unique = false, nullable = true, length = 10)
	private String sRateType;
	
	/** 返售开放日 **/
	@Column(name = "D_RESELL_START_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dResellStartDt;
	
	/** 返售截至日 **/
	@Column(name = "D_RESELL_END_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dResellEndDt;
	
	/** 操作员id **/
	@Column(name = "S_OPERATOR_ID", unique = false, nullable = true, length = 40)
	private String sOperatorId;
	
	/** 产品类型 **/
	@Column(name = "PRODUCT_ID", unique = false, nullable = true, length = 40)
	private String productId;
	
	/** 报文状态 **/
	@Column(name = "S_MSG_STATUS", unique = false, nullable = true, length = 15)
	private String sMsgStatus;
	
	/** 利息 **/
	@Column(name = "D_INTEREST", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal dInterest;
	
	/** 报文状态码--报文处理说明 **/
	@Column(name = "S_MSG_CODE", unique = false, nullable = true, length = 800)
	private String sMsgCode;
	
	/** 电子签名 **/
	@Column(name = "S_ELCTRN_NAME", unique = false, nullable = true, length = 2000)
	private String sElctrnName;
	
	/** 贴入方名称 **/
	@Column(name = "REDISCOUNTINNAME", unique = false, nullable = true, length = 150)
	private String rediscountinname;
	
	/** 贴入方大额行号 **/
	@Column(name = "REDISCOUNTINBANKCODE", unique = false, nullable = true, length = 20)
	private String rediscountinbankcode;
	
	/** 贴入方账号 **/
	@Column(name = "REDISCOUNTINACCOUNT", unique = false, nullable = true, length = 40)
	private String rediscountinaccount;
	
	/** 贴入方组织机构代码号 **/
	@Column(name = "REDISCOUNTINORGCODE", unique = false, nullable = true, length = 20)
	private String rediscountinorgcode;
	
	/** 贴出方名称 **/
	@Column(name = "REDISCOUNTOUTNAME", unique = false, nullable = true, length = 150)
	private String rediscountoutname;
	
	/** 贴出方大额行号 **/
	@Column(name = "REDISCOUNTOUTCODE", unique = false, nullable = true, length = 12)
	private String rediscountoutcode;
	
	/** 贴出方账号 **/
	@Column(name = "REDISCOUNTOUTACCOUNT", unique = false, nullable = true, length = 40)
	private String rediscountoutaccount;
	
	/** 贴出方组织机构代码号 **/
	@Column(name = "REDISCOUNTOUTORGCODE", unique = false, nullable = true, length = 20)
	private String rediscountoutorgcode;
	
	/** 赎回利率 **/
	@Column(name = "REDEEMRATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal redeemrate;
	
	/** 不得转让标记 **/
	@Column(name = "NOT_ATTORN_FLAG", unique = false, nullable = true, length = 10)
	private String notAttornFlag;
	
	/** 清算标记 **/
	@Column(name = "CLEARING_FLAG", unique = false, nullable = true, length = 10)
	private String clearingFlag;
	
	/** 己方实付金额 **/
	@Column(name = "SELFAMOUNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal selfamount;
	
	/** 贴出人备注 **/
	@Column(name = "REDISCOUNTOUTREMARK", unique = false, nullable = true, length = 800)
	private String rediscountoutremark;
	
	/** 转贴现种类 **/
	@Column(name = "REDISC_TYPE", unique = false, nullable = true, length = 10)
	private String rediscType;
	
	/** EDUREDUCETYPE **/
	@Column(name = "EDUREDUCETYPE", unique = false, nullable = true, length = 10)
	private String edureducetype;
	
	/** EDUREDUCEOBJECT **/
	@Column(name = "EDUREDUCEOBJECT", unique = false, nullable = true, length = 10)
	private String edureduceobject;
	
	/** 贴入人承接行行号 **/
	@Column(name = "DSCNTBKAGCYSVCR", unique = false, nullable = true, length = 12)
	private String dscntbkagcysvcr;
	
	/** 贴出人承接行行号 **/
	@Column(name = "DSCNTPROPSRIDAGCYSVCR", unique = false, nullable = true, length = 12)
	private String dscntpropsridagcysvcr;
	
	/** DSCNTROLE **/
	@Column(name = "DSCNTROLE", unique = false, nullable = true, length = 40)
	private String dscntrole;
	
	/** ED_DRWRROLE **/
	@Column(name = "ED_DRWRROLE", unique = false, nullable = true, length = 10)
	private String edDrwrrole;
	
	/** 赎回利率利率类型 **/
	@Column(name = "SRPD_RATE_TYPE", unique = false, nullable = true, length = 10)
	private String srpdRateType;
	
	/** 转贴赎回利息 **/
	@Column(name = "BRPDINT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal brpdint;
	
	/** 赎回实付金额 **/
	@Column(name = "BRPDPAYMENT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal brpdpayment;
	
	/** 是否同城 **/
	@Column(name = "S_IF_SAME_CITY", unique = false, nullable = true, length = 10)
	private String sIfSameCity;
	
	/** 是否双向 **/
	@Column(name = "S_IF_BIDIRECT", unique = false, nullable = true, length = 10)
	private String sIfBidirect;
	
	/** 库存状态 **/
	@Column(name = "STORAGE_STS", unique = false, nullable = true, length = 10)
	private String storageSts;
	
	/** 双买使用转卖明细表id **/
	@Column(name = "BIDIRECT_SALE_BILL_ID", unique = false, nullable = true, length = 40)
	private String bidirectSaleBillId;
	
	/** 承兑行行号（付款人开户行） **/
	@Column(name = "S_ACCEPTORBANKCODE", unique = false, nullable = true, length = 12)
	private String sAcceptorbankcode;
	
	/** 双买到期日 **/
	@Column(name = "BIDIRECT_DT", unique = false, nullable = true, length = 10)
	private java.util.Date bidirectDt;
	
	/** 回购到期日 **/
	@Column(name = "RETURN_GOU_DT", unique = false, nullable = true, length = 10)
	private java.util.Date returnGouDt;
	
	/** 承兑日期 **/
	@Column(name = "ACCEPTOR_DT", unique = false, nullable = true, length = 10)
	private java.util.Date acceptorDt;
	
	/** 添加状态 **/
	@Column(name = "ADD_STATUS", unique = false, nullable = true, length = 10)
	private String addStatus;
	
	/** 贴入方类别 **/
	@Column(name = "REDISCOUNTINROLE", unique = false, nullable = true, length = 10)
	private String rediscountinrole;
	
	/** 贴出方类别 **/
	@Column(name = "REDISCOUNTOUTROLE", unique = false, nullable = true, length = 10)
	private String rediscountoutrole;
	
	/** 报文带过来的批次号 **/
	@Column(name = "BATCHNO", unique = false, nullable = true, length = 10)
	private String batchno;
	
	/** 设否设置额度扣减规则 **/
	@Column(name = "CREDITRULE", unique = false, nullable = true, length = 10)
	private String creditrule;
	
	/** 额度扣减结果 **/
	@Column(name = "CREDITRESULT", unique = false, nullable = true, length = 10)
	private String creditresult;
	
	/** 支付交易序号 **/
	@Column(name = "S_TRF_ID", unique = false, nullable = true, length = 40)
	private String sTrfId;
	
	/** 票据前手（纸票） **/
	@Column(name = "S_PREHAND", unique = false, nullable = true, length = 80)
	private String sPrehand;
	
	/** 贴出人名称（纸票） **/
	@Column(name = "S_OWNER_BILL_NAME", unique = false, nullable = true, length = 200)
	private String sOwnerBillName;
	
	/** 买入日期 **/
	@Column(name = "D_BUYIN_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dBuyinDt;
	
	/** 是否回购状态 0=未到期 1=已到期未回购 2=已到期已回购 3=到期未回购转买断 **/
	@Column(name = "BILLDT_ZT", unique = false, nullable = true, length = 2)
	private String billdtZt;
	
	/** 对手银行类别-国有、外资等 **/
	@Column(name = "S_COUNTERPARTYBANKTYPE", unique = false, nullable = true, length = 5)
	private String sCounterpartybanktype;
	
	/** 计息到期日 **/
	@Column(name = "GALE_DT", unique = false, nullable = true, length = 10)
	private java.util.Date galeDt;
	
	/** 业务结清 标记 0未结清；1已结清 **/
	@Column(name = "BUSI_END_FLAG", unique = false, nullable = true, length = 10)
	private String busiEndFlag;
	
	/** 业务结清类型 **/
	@Column(name = "BUSI_END_TYPE", unique = false, nullable = true, length = 10)
	private String busiEndType;
	
	/** 业务结清日期 **/
	@Column(name = "BUSI_END_date", unique = false, nullable = true, length = 10)
	private java.util.Date bUSIENDDate;
	
	/** 记账日期 **/
	@Column(name = "ACCT_date", unique = false, nullable = true, length = 10)
	private java.util.Date aCCTDate;
	
	/** 记账流水号 **/
	@Column(name = "ACCT_FLOW_NO", unique = false, nullable = true, length = 60)
	private String acctFlowNo;
	
	/** 记账柜员号 **/
	@Column(name = "ACCT_USER_NO", unique = false, nullable = true, length = 60)
	private String acctUserNo;
	
	/** 记账柜员名 **/
	@Column(name = "ACCT_USER_NAME", unique = false, nullable = true, length = 60)
	private String acctUserName;
	
	/** 记账授权柜员号 **/
	@Column(name = "ACCT_AUTH_USER_NO", unique = false, nullable = true, length = 60)
	private String acctAuthUserNo;
	
	/** 记账授权柜名称 **/
	@Column(name = "ACCT_AUTH_USER_NAME", unique = false, nullable = true, length = 60)
	private String acctAuthUserName;
	
	/** 撤销记账柜名 **/
	@Column(name = "CANCEL_ACCT_USER_NO", unique = false, nullable = true, length = 60)
	private String cancelAcctUserNo;
	
	/** 撤销记账柜名 **/
	@Column(name = "CANCEL_ACCT_USER_NAME", unique = false, nullable = true, length = 60)
	private String cancelAcctUserName;
	
	/** 撤销记账授权人号 **/
	@Column(name = "CANCEL_ACCT_AUTH_USER_NO", unique = false, nullable = true, length = 60)
	private String cancelAcctAuthUserNo;
	
	/** 撤销记账授权人 **/
	@Column(name = "CANCEL_ACCT_AUTH_USER_NAME", unique = false, nullable = true, length = 60)
	private String cancelAcctAuthUserName;
	
	/** 代理行业务标记 1代理行业务 0本行业务 **/
	@Column(name = "AGENT_FLAG", unique = false, nullable = true, length = 2)
	private String agentFlag;
	
	/** 隔夜待持 0持有 1不持有 **/
	@Column(name = "OVERNIGHT_FLAG", unique = false, nullable = true, length = 6)
	private String overnightFlag;
	
	/** 额度扣减总行行号 **/
	@Column(name = "CREDIT_ZHBANK_CODE", unique = false, nullable = true, length = 100)
	private String creditZhbankCode;
	
	/** 额度扣减总行行名 **/
	@Column(name = "CREDIT_ZHBANK_NAME", unique = false, nullable = true, length = 200)
	private String creditZhbankName;
	
	/** 占用额度类型 **/
	@Column(name = "CREDITTYPE", unique = false, nullable = true, length = 20)
	private String credittype;
	
	/** 计息调整天数 **/
	@Column(name = "IMPROVE_DAYS", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal improveDays;
	
	/** 数据移植标记 1:移植数据  0：正常数据 **/
	@Column(name = "YZ_FLAG", unique = false, nullable = true, length = 2)
	private String yzFlag;
	
	/** 核心返回的记账日期 **/
	@Column(name = "CORE_TRANDT", unique = false, nullable = true, length = 40)
	private String coreTrandt;
	
	/** 核心返回的记账流水 **/
	@Column(name = "CORE_TRANSQ", unique = false, nullable = true, length = 100)
	private String coreTransq;
	
	/** 贴出方机构号【系统内有值】 **/
	@Column(name = "REDISCOUNT_OUT_BRANCH_ID", unique = false, nullable = true, length = 40)
	private String rediscountOutBranchId;
	
	/** 贴出方机构号【系统内有值】 **/
	@Column(name = "REDISCOUNT_OUT_BRANCH_NO", unique = false, nullable = true, length = 40)
	private String rediscountOutBranchNo;
	
	/** 卖出回购到期买回时 存原转卖的批次ID **/
	@Column(name = "SALE_BATCH_ID", unique = false, nullable = true, length = 50)
	private String saleBatchId;
	
	
	/**
	 * @param rediscBuyBillId
	 */
	public void setRediscBuyBillId(String rediscBuyBillId) {
		this.rediscBuyBillId = rediscBuyBillId;
	}
	
    /**
     * @return rediscBuyBillId
     */
	public String getRediscBuyBillId() {
		return this.rediscBuyBillId;
	}
	
	/**
	 * @param rediscBuyBatchId
	 */
	public void setRediscBuyBatchId(String rediscBuyBatchId) {
		this.rediscBuyBatchId = rediscBuyBatchId;
	}
	
    /**
     * @return rediscBuyBatchId
     */
	public String getRediscBuyBatchId() {
		return this.rediscBuyBatchId;
	}
	
	/**
	 * @param billinfoId
	 */
	public void setBillinfoId(String billinfoId) {
		this.billinfoId = billinfoId;
	}
	
    /**
     * @return billinfoId
     */
	public String getBillinfoId() {
		return this.billinfoId;
	}
	
	/**
	 * @param sBillNo
	 */
	public void setSBillNo(String sBillNo) {
		this.sBillNo = sBillNo;
	}
	
    /**
     * @return sBillNo
     */
	public String getSBillNo() {
		return this.sBillNo;
	}
	
	/**
	 * @param dIssueDt
	 */
	public void setDIssueDt(java.util.Date dIssueDt) {
		this.dIssueDt = dIssueDt;
	}
	
    /**
     * @return dIssueDt
     */
	public java.util.Date getDIssueDt() {
		return this.dIssueDt;
	}
	
	/**
	 * @param dDueDt
	 */
	public void setDDueDt(java.util.Date dDueDt) {
		this.dDueDt = dDueDt;
	}
	
    /**
     * @return dDueDt
     */
	public java.util.Date getDDueDt() {
		return this.dDueDt;
	}
	
	/**
	 * @param sIssuerName
	 */
	public void setSIssuerName(String sIssuerName) {
		this.sIssuerName = sIssuerName;
	}
	
    /**
     * @return sIssuerName
     */
	public String getSIssuerName() {
		return this.sIssuerName;
	}
	
	/**
	 * @param sIssuerAccount
	 */
	public void setSIssuerAccount(String sIssuerAccount) {
		this.sIssuerAccount = sIssuerAccount;
	}
	
    /**
     * @return sIssuerAccount
     */
	public String getSIssuerAccount() {
		return this.sIssuerAccount;
	}
	
	/**
	 * @param sIssuerBankName
	 */
	public void setSIssuerBankName(String sIssuerBankName) {
		this.sIssuerBankName = sIssuerBankName;
	}
	
    /**
     * @return sIssuerBankName
     */
	public String getSIssuerBankName() {
		return this.sIssuerBankName;
	}
	
	/**
	 * @param sIssuerBankCode
	 */
	public void setSIssuerBankCode(String sIssuerBankCode) {
		this.sIssuerBankCode = sIssuerBankCode;
	}
	
    /**
     * @return sIssuerBankCode
     */
	public String getSIssuerBankCode() {
		return this.sIssuerBankCode;
	}
	
	/**
	 * @param fBillAmount
	 */
	public void setFBillAmount(java.math.BigDecimal fBillAmount) {
		this.fBillAmount = fBillAmount;
	}
	
    /**
     * @return fBillAmount
     */
	public java.math.BigDecimal getFBillAmount() {
		return this.fBillAmount;
	}
	
	/**
	 * @param sAcceptor
	 */
	public void setSAcceptor(String sAcceptor) {
		this.sAcceptor = sAcceptor;
	}
	
    /**
     * @return sAcceptor
     */
	public String getSAcceptor() {
		return this.sAcceptor;
	}
	
	/**
	 * @param sPayeeName
	 */
	public void setSPayeeName(String sPayeeName) {
		this.sPayeeName = sPayeeName;
	}
	
    /**
     * @return sPayeeName
     */
	public String getSPayeeName() {
		return this.sPayeeName;
	}
	
	/**
	 * @param sPayeeBankName
	 */
	public void setSPayeeBankName(String sPayeeBankName) {
		this.sPayeeBankName = sPayeeBankName;
	}
	
    /**
     * @return sPayeeBankName
     */
	public String getSPayeeBankName() {
		return this.sPayeeBankName;
	}
	
	/**
	 * @param sPayeeAccount
	 */
	public void setSPayeeAccount(String sPayeeAccount) {
		this.sPayeeAccount = sPayeeAccount;
	}
	
    /**
     * @return sPayeeAccount
     */
	public String getSPayeeAccount() {
		return this.sPayeeAccount;
	}
	
	/**
	 * @param sBillType
	 */
	public void setSBillType(String sBillType) {
		this.sBillType = sBillType;
	}
	
    /**
     * @return sBillType
     */
	public String getSBillType() {
		return this.sBillType;
	}
	
	/**
	 * @param sBillMedia
	 */
	public void setSBillMedia(String sBillMedia) {
		this.sBillMedia = sBillMedia;
	}
	
    /**
     * @return sBillMedia
     */
	public String getSBillMedia() {
		return this.sBillMedia;
	}
	
	/**
	 * @param sBillStatus
	 */
	public void setSBillStatus(String sBillStatus) {
		this.sBillStatus = sBillStatus;
	}
	
    /**
     * @return sBillStatus
     */
	public String getSBillStatus() {
		return this.sBillStatus;
	}
	
	/**
	 * @param sBranchId
	 */
	public void setSBranchId(String sBranchId) {
		this.sBranchId = sBranchId;
	}
	
    /**
     * @return sBranchId
     */
	public String getSBranchId() {
		return this.sBranchId;
	}
	
	/**
	 * @param fIntDays
	 */
	public void setFIntDays(java.math.BigDecimal fIntDays) {
		this.fIntDays = fIntDays;
	}
	
    /**
     * @return fIntDays
     */
	public java.math.BigDecimal getFIntDays() {
		return this.fIntDays;
	}
	
	/**
	 * @param fPayment
	 */
	public void setFPayment(java.math.BigDecimal fPayment) {
		this.fPayment = fPayment;
	}
	
    /**
     * @return fPayment
     */
	public java.math.BigDecimal getFPayment() {
		return this.fPayment;
	}
	
	/**
	 * @param replyRemark
	 */
	public void setReplyRemark(String replyRemark) {
		this.replyRemark = replyRemark;
	}
	
    /**
     * @return replyRemark
     */
	public String getReplyRemark() {
		return this.replyRemark;
	}
	
	/**
	 * @param dCreateDt
	 */
	public void setDCreateDt(String dCreateDt) {
		this.dCreateDt = dCreateDt;
	}
	
    /**
     * @return dCreateDt
     */
	public String getDCreateDt() {
		return this.dCreateDt;
	}
	
	/**
	 * @param sPayerBankName
	 */
	public void setSPayerBankName(String sPayerBankName) {
		this.sPayerBankName = sPayerBankName;
	}
	
    /**
     * @return sPayerBankName
     */
	public String getSPayerBankName() {
		return this.sPayerBankName;
	}
	
	/**
	 * @param sPayeeBankCode
	 */
	public void setSPayeeBankCode(String sPayeeBankCode) {
		this.sPayeeBankCode = sPayeeBankCode;
	}
	
    /**
     * @return sPayeeBankCode
     */
	public String getSPayeeBankCode() {
		return this.sPayeeBankCode;
	}
	
	/**
	 * @param fRate
	 */
	public void setFRate(java.math.BigDecimal fRate) {
		this.fRate = fRate;
	}
	
    /**
     * @return fRate
     */
	public java.math.BigDecimal getFRate() {
		return this.fRate;
	}
	
	/**
	 * @param sRateType
	 */
	public void setSRateType(String sRateType) {
		this.sRateType = sRateType;
	}
	
    /**
     * @return sRateType
     */
	public String getSRateType() {
		return this.sRateType;
	}
	
	/**
	 * @param dResellStartDt
	 */
	public void setDResellStartDt(java.util.Date dResellStartDt) {
		this.dResellStartDt = dResellStartDt;
	}
	
    /**
     * @return dResellStartDt
     */
	public java.util.Date getDResellStartDt() {
		return this.dResellStartDt;
	}
	
	/**
	 * @param dResellEndDt
	 */
	public void setDResellEndDt(java.util.Date dResellEndDt) {
		this.dResellEndDt = dResellEndDt;
	}
	
    /**
     * @return dResellEndDt
     */
	public java.util.Date getDResellEndDt() {
		return this.dResellEndDt;
	}
	
	/**
	 * @param sOperatorId
	 */
	public void setSOperatorId(String sOperatorId) {
		this.sOperatorId = sOperatorId;
	}
	
    /**
     * @return sOperatorId
     */
	public String getSOperatorId() {
		return this.sOperatorId;
	}
	
	/**
	 * @param productId
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
    /**
     * @return productId
     */
	public String getProductId() {
		return this.productId;
	}
	
	/**
	 * @param sMsgStatus
	 */
	public void setSMsgStatus(String sMsgStatus) {
		this.sMsgStatus = sMsgStatus;
	}
	
    /**
     * @return sMsgStatus
     */
	public String getSMsgStatus() {
		return this.sMsgStatus;
	}
	
	/**
	 * @param dInterest
	 */
	public void setDInterest(java.math.BigDecimal dInterest) {
		this.dInterest = dInterest;
	}
	
    /**
     * @return dInterest
     */
	public java.math.BigDecimal getDInterest() {
		return this.dInterest;
	}
	
	/**
	 * @param sMsgCode
	 */
	public void setSMsgCode(String sMsgCode) {
		this.sMsgCode = sMsgCode;
	}
	
    /**
     * @return sMsgCode
     */
	public String getSMsgCode() {
		return this.sMsgCode;
	}
	
	/**
	 * @param sElctrnName
	 */
	public void setSElctrnName(String sElctrnName) {
		this.sElctrnName = sElctrnName;
	}
	
    /**
     * @return sElctrnName
     */
	public String getSElctrnName() {
		return this.sElctrnName;
	}
	
	/**
	 * @param rediscountinname
	 */
	public void setRediscountinname(String rediscountinname) {
		this.rediscountinname = rediscountinname;
	}
	
    /**
     * @return rediscountinname
     */
	public String getRediscountinname() {
		return this.rediscountinname;
	}
	
	/**
	 * @param rediscountinbankcode
	 */
	public void setRediscountinbankcode(String rediscountinbankcode) {
		this.rediscountinbankcode = rediscountinbankcode;
	}
	
    /**
     * @return rediscountinbankcode
     */
	public String getRediscountinbankcode() {
		return this.rediscountinbankcode;
	}
	
	/**
	 * @param rediscountinaccount
	 */
	public void setRediscountinaccount(String rediscountinaccount) {
		this.rediscountinaccount = rediscountinaccount;
	}
	
    /**
     * @return rediscountinaccount
     */
	public String getRediscountinaccount() {
		return this.rediscountinaccount;
	}
	
	/**
	 * @param rediscountinorgcode
	 */
	public void setRediscountinorgcode(String rediscountinorgcode) {
		this.rediscountinorgcode = rediscountinorgcode;
	}
	
    /**
     * @return rediscountinorgcode
     */
	public String getRediscountinorgcode() {
		return this.rediscountinorgcode;
	}
	
	/**
	 * @param rediscountoutname
	 */
	public void setRediscountoutname(String rediscountoutname) {
		this.rediscountoutname = rediscountoutname;
	}
	
    /**
     * @return rediscountoutname
     */
	public String getRediscountoutname() {
		return this.rediscountoutname;
	}
	
	/**
	 * @param rediscountoutcode
	 */
	public void setRediscountoutcode(String rediscountoutcode) {
		this.rediscountoutcode = rediscountoutcode;
	}
	
    /**
     * @return rediscountoutcode
     */
	public String getRediscountoutcode() {
		return this.rediscountoutcode;
	}
	
	/**
	 * @param rediscountoutaccount
	 */
	public void setRediscountoutaccount(String rediscountoutaccount) {
		this.rediscountoutaccount = rediscountoutaccount;
	}
	
    /**
     * @return rediscountoutaccount
     */
	public String getRediscountoutaccount() {
		return this.rediscountoutaccount;
	}
	
	/**
	 * @param rediscountoutorgcode
	 */
	public void setRediscountoutorgcode(String rediscountoutorgcode) {
		this.rediscountoutorgcode = rediscountoutorgcode;
	}
	
    /**
     * @return rediscountoutorgcode
     */
	public String getRediscountoutorgcode() {
		return this.rediscountoutorgcode;
	}
	
	/**
	 * @param redeemrate
	 */
	public void setRedeemrate(java.math.BigDecimal redeemrate) {
		this.redeemrate = redeemrate;
	}
	
    /**
     * @return redeemrate
     */
	public java.math.BigDecimal getRedeemrate() {
		return this.redeemrate;
	}
	
	/**
	 * @param notAttornFlag
	 */
	public void setNotAttornFlag(String notAttornFlag) {
		this.notAttornFlag = notAttornFlag;
	}
	
    /**
     * @return notAttornFlag
     */
	public String getNotAttornFlag() {
		return this.notAttornFlag;
	}
	
	/**
	 * @param clearingFlag
	 */
	public void setClearingFlag(String clearingFlag) {
		this.clearingFlag = clearingFlag;
	}
	
    /**
     * @return clearingFlag
     */
	public String getClearingFlag() {
		return this.clearingFlag;
	}
	
	/**
	 * @param selfamount
	 */
	public void setSelfamount(java.math.BigDecimal selfamount) {
		this.selfamount = selfamount;
	}
	
    /**
     * @return selfamount
     */
	public java.math.BigDecimal getSelfamount() {
		return this.selfamount;
	}
	
	/**
	 * @param rediscountoutremark
	 */
	public void setRediscountoutremark(String rediscountoutremark) {
		this.rediscountoutremark = rediscountoutremark;
	}
	
    /**
     * @return rediscountoutremark
     */
	public String getRediscountoutremark() {
		return this.rediscountoutremark;
	}
	
	/**
	 * @param rediscType
	 */
	public void setRediscType(String rediscType) {
		this.rediscType = rediscType;
	}
	
    /**
     * @return rediscType
     */
	public String getRediscType() {
		return this.rediscType;
	}
	
	/**
	 * @param edureducetype
	 */
	public void setEdureducetype(String edureducetype) {
		this.edureducetype = edureducetype;
	}
	
    /**
     * @return edureducetype
     */
	public String getEdureducetype() {
		return this.edureducetype;
	}
	
	/**
	 * @param edureduceobject
	 */
	public void setEdureduceobject(String edureduceobject) {
		this.edureduceobject = edureduceobject;
	}
	
    /**
     * @return edureduceobject
     */
	public String getEdureduceobject() {
		return this.edureduceobject;
	}
	
	/**
	 * @param dscntbkagcysvcr
	 */
	public void setDscntbkagcysvcr(String dscntbkagcysvcr) {
		this.dscntbkagcysvcr = dscntbkagcysvcr;
	}
	
    /**
     * @return dscntbkagcysvcr
     */
	public String getDscntbkagcysvcr() {
		return this.dscntbkagcysvcr;
	}
	
	/**
	 * @param dscntpropsridagcysvcr
	 */
	public void setDscntpropsridagcysvcr(String dscntpropsridagcysvcr) {
		this.dscntpropsridagcysvcr = dscntpropsridagcysvcr;
	}
	
    /**
     * @return dscntpropsridagcysvcr
     */
	public String getDscntpropsridagcysvcr() {
		return this.dscntpropsridagcysvcr;
	}
	
	/**
	 * @param dscntrole
	 */
	public void setDscntrole(String dscntrole) {
		this.dscntrole = dscntrole;
	}
	
    /**
     * @return dscntrole
     */
	public String getDscntrole() {
		return this.dscntrole;
	}
	
	/**
	 * @param edDrwrrole
	 */
	public void setEdDrwrrole(String edDrwrrole) {
		this.edDrwrrole = edDrwrrole;
	}
	
    /**
     * @return edDrwrrole
     */
	public String getEdDrwrrole() {
		return this.edDrwrrole;
	}
	
	/**
	 * @param srpdRateType
	 */
	public void setSrpdRateType(String srpdRateType) {
		this.srpdRateType = srpdRateType;
	}
	
    /**
     * @return srpdRateType
     */
	public String getSrpdRateType() {
		return this.srpdRateType;
	}
	
	/**
	 * @param brpdint
	 */
	public void setBrpdint(java.math.BigDecimal brpdint) {
		this.brpdint = brpdint;
	}
	
    /**
     * @return brpdint
     */
	public java.math.BigDecimal getBrpdint() {
		return this.brpdint;
	}
	
	/**
	 * @param brpdpayment
	 */
	public void setBrpdpayment(java.math.BigDecimal brpdpayment) {
		this.brpdpayment = brpdpayment;
	}
	
    /**
     * @return brpdpayment
     */
	public java.math.BigDecimal getBrpdpayment() {
		return this.brpdpayment;
	}
	
	/**
	 * @param sIfSameCity
	 */
	public void setSIfSameCity(String sIfSameCity) {
		this.sIfSameCity = sIfSameCity;
	}
	
    /**
     * @return sIfSameCity
     */
	public String getSIfSameCity() {
		return this.sIfSameCity;
	}
	
	/**
	 * @param sIfBidirect
	 */
	public void setSIfBidirect(String sIfBidirect) {
		this.sIfBidirect = sIfBidirect;
	}
	
    /**
     * @return sIfBidirect
     */
	public String getSIfBidirect() {
		return this.sIfBidirect;
	}
	
	/**
	 * @param storageSts
	 */
	public void setStorageSts(String storageSts) {
		this.storageSts = storageSts;
	}
	
    /**
     * @return storageSts
     */
	public String getStorageSts() {
		return this.storageSts;
	}
	
	/**
	 * @param bidirectSaleBillId
	 */
	public void setBidirectSaleBillId(String bidirectSaleBillId) {
		this.bidirectSaleBillId = bidirectSaleBillId;
	}
	
    /**
     * @return bidirectSaleBillId
     */
	public String getBidirectSaleBillId() {
		return this.bidirectSaleBillId;
	}
	
	/**
	 * @param sAcceptorbankcode
	 */
	public void setSAcceptorbankcode(String sAcceptorbankcode) {
		this.sAcceptorbankcode = sAcceptorbankcode;
	}
	
    /**
     * @return sAcceptorbankcode
     */
	public String getSAcceptorbankcode() {
		return this.sAcceptorbankcode;
	}
	
	/**
	 * @param bidirectDt
	 */
	public void setBidirectDt(java.util.Date bidirectDt) {
		this.bidirectDt = bidirectDt;
	}
	
    /**
     * @return bidirectDt
     */
	public java.util.Date getBidirectDt() {
		return this.bidirectDt;
	}
	
	/**
	 * @param returnGouDt
	 */
	public void setReturnGouDt(java.util.Date returnGouDt) {
		this.returnGouDt = returnGouDt;
	}
	
    /**
     * @return returnGouDt
     */
	public java.util.Date getReturnGouDt() {
		return this.returnGouDt;
	}
	
	/**
	 * @param acceptorDt
	 */
	public void setAcceptorDt(java.util.Date acceptorDt) {
		this.acceptorDt = acceptorDt;
	}
	
    /**
     * @return acceptorDt
     */
	public java.util.Date getAcceptorDt() {
		return this.acceptorDt;
	}
	
	/**
	 * @param addStatus
	 */
	public void setAddStatus(String addStatus) {
		this.addStatus = addStatus;
	}
	
    /**
     * @return addStatus
     */
	public String getAddStatus() {
		return this.addStatus;
	}
	
	/**
	 * @param rediscountinrole
	 */
	public void setRediscountinrole(String rediscountinrole) {
		this.rediscountinrole = rediscountinrole;
	}
	
    /**
     * @return rediscountinrole
     */
	public String getRediscountinrole() {
		return this.rediscountinrole;
	}
	
	/**
	 * @param rediscountoutrole
	 */
	public void setRediscountoutrole(String rediscountoutrole) {
		this.rediscountoutrole = rediscountoutrole;
	}
	
    /**
     * @return rediscountoutrole
     */
	public String getRediscountoutrole() {
		return this.rediscountoutrole;
	}
	
	/**
	 * @param batchno
	 */
	public void setBatchno(String batchno) {
		this.batchno = batchno;
	}
	
    /**
     * @return batchno
     */
	public String getBatchno() {
		return this.batchno;
	}
	
	/**
	 * @param creditrule
	 */
	public void setCreditrule(String creditrule) {
		this.creditrule = creditrule;
	}
	
    /**
     * @return creditrule
     */
	public String getCreditrule() {
		return this.creditrule;
	}
	
	/**
	 * @param creditresult
	 */
	public void setCreditresult(String creditresult) {
		this.creditresult = creditresult;
	}
	
    /**
     * @return creditresult
     */
	public String getCreditresult() {
		return this.creditresult;
	}
	
	/**
	 * @param sTrfId
	 */
	public void setSTrfId(String sTrfId) {
		this.sTrfId = sTrfId;
	}
	
    /**
     * @return sTrfId
     */
	public String getSTrfId() {
		return this.sTrfId;
	}
	
	/**
	 * @param sPrehand
	 */
	public void setSPrehand(String sPrehand) {
		this.sPrehand = sPrehand;
	}
	
    /**
     * @return sPrehand
     */
	public String getSPrehand() {
		return this.sPrehand;
	}
	
	/**
	 * @param sOwnerBillName
	 */
	public void setSOwnerBillName(String sOwnerBillName) {
		this.sOwnerBillName = sOwnerBillName;
	}
	
    /**
     * @return sOwnerBillName
     */
	public String getSOwnerBillName() {
		return this.sOwnerBillName;
	}
	
	/**
	 * @param dBuyinDt
	 */
	public void setDBuyinDt(java.util.Date dBuyinDt) {
		this.dBuyinDt = dBuyinDt;
	}
	
    /**
     * @return dBuyinDt
     */
	public java.util.Date getDBuyinDt() {
		return this.dBuyinDt;
	}
	
	/**
	 * @param billdtZt
	 */
	public void setBilldtZt(String billdtZt) {
		this.billdtZt = billdtZt;
	}
	
    /**
     * @return billdtZt
     */
	public String getBilldtZt() {
		return this.billdtZt;
	}
	
	/**
	 * @param sCounterpartybanktype
	 */
	public void setSCounterpartybanktype(String sCounterpartybanktype) {
		this.sCounterpartybanktype = sCounterpartybanktype;
	}
	
    /**
     * @return sCounterpartybanktype
     */
	public String getSCounterpartybanktype() {
		return this.sCounterpartybanktype;
	}
	
	/**
	 * @param galeDt
	 */
	public void setGaleDt(java.util.Date galeDt) {
		this.galeDt = galeDt;
	}
	
    /**
     * @return galeDt
     */
	public java.util.Date getGaleDt() {
		return this.galeDt;
	}
	
	/**
	 * @param busiEndFlag
	 */
	public void setBusiEndFlag(String busiEndFlag) {
		this.busiEndFlag = busiEndFlag;
	}
	
    /**
     * @return busiEndFlag
     */
	public String getBusiEndFlag() {
		return this.busiEndFlag;
	}
	
	/**
	 * @param busiEndType
	 */
	public void setBusiEndType(String busiEndType) {
		this.busiEndType = busiEndType;
	}
	
    /**
     * @return busiEndType
     */
	public String getBusiEndType() {
		return this.busiEndType;
	}
	
	/**
	 * @param bUSIENDDate
	 */
	public void setBUSIENDDate(java.util.Date bUSIENDDate) {
		this.bUSIENDDate = bUSIENDDate;
	}
	
    /**
     * @return bUSIENDDate
     */
	public java.util.Date getBUSIENDDate() {
		return this.bUSIENDDate;
	}
	
	/**
	 * @param aCCTDate
	 */
	public void setACCTDate(java.util.Date aCCTDate) {
		this.aCCTDate = aCCTDate;
	}
	
    /**
     * @return aCCTDate
     */
	public java.util.Date getACCTDate() {
		return this.aCCTDate;
	}
	
	/**
	 * @param acctFlowNo
	 */
	public void setAcctFlowNo(String acctFlowNo) {
		this.acctFlowNo = acctFlowNo;
	}
	
    /**
     * @return acctFlowNo
     */
	public String getAcctFlowNo() {
		return this.acctFlowNo;
	}
	
	/**
	 * @param acctUserNo
	 */
	public void setAcctUserNo(String acctUserNo) {
		this.acctUserNo = acctUserNo;
	}
	
    /**
     * @return acctUserNo
     */
	public String getAcctUserNo() {
		return this.acctUserNo;
	}
	
	/**
	 * @param acctUserName
	 */
	public void setAcctUserName(String acctUserName) {
		this.acctUserName = acctUserName;
	}
	
    /**
     * @return acctUserName
     */
	public String getAcctUserName() {
		return this.acctUserName;
	}
	
	/**
	 * @param acctAuthUserNo
	 */
	public void setAcctAuthUserNo(String acctAuthUserNo) {
		this.acctAuthUserNo = acctAuthUserNo;
	}
	
    /**
     * @return acctAuthUserNo
     */
	public String getAcctAuthUserNo() {
		return this.acctAuthUserNo;
	}
	
	/**
	 * @param acctAuthUserName
	 */
	public void setAcctAuthUserName(String acctAuthUserName) {
		this.acctAuthUserName = acctAuthUserName;
	}
	
    /**
     * @return acctAuthUserName
     */
	public String getAcctAuthUserName() {
		return this.acctAuthUserName;
	}
	
	/**
	 * @param cancelAcctUserNo
	 */
	public void setCancelAcctUserNo(String cancelAcctUserNo) {
		this.cancelAcctUserNo = cancelAcctUserNo;
	}
	
    /**
     * @return cancelAcctUserNo
     */
	public String getCancelAcctUserNo() {
		return this.cancelAcctUserNo;
	}
	
	/**
	 * @param cancelAcctUserName
	 */
	public void setCancelAcctUserName(String cancelAcctUserName) {
		this.cancelAcctUserName = cancelAcctUserName;
	}
	
    /**
     * @return cancelAcctUserName
     */
	public String getCancelAcctUserName() {
		return this.cancelAcctUserName;
	}
	
	/**
	 * @param cancelAcctAuthUserNo
	 */
	public void setCancelAcctAuthUserNo(String cancelAcctAuthUserNo) {
		this.cancelAcctAuthUserNo = cancelAcctAuthUserNo;
	}
	
    /**
     * @return cancelAcctAuthUserNo
     */
	public String getCancelAcctAuthUserNo() {
		return this.cancelAcctAuthUserNo;
	}
	
	/**
	 * @param cancelAcctAuthUserName
	 */
	public void setCancelAcctAuthUserName(String cancelAcctAuthUserName) {
		this.cancelAcctAuthUserName = cancelAcctAuthUserName;
	}
	
    /**
     * @return cancelAcctAuthUserName
     */
	public String getCancelAcctAuthUserName() {
		return this.cancelAcctAuthUserName;
	}
	
	/**
	 * @param agentFlag
	 */
	public void setAgentFlag(String agentFlag) {
		this.agentFlag = agentFlag;
	}
	
    /**
     * @return agentFlag
     */
	public String getAgentFlag() {
		return this.agentFlag;
	}
	
	/**
	 * @param overnightFlag
	 */
	public void setOvernightFlag(String overnightFlag) {
		this.overnightFlag = overnightFlag;
	}
	
    /**
     * @return overnightFlag
     */
	public String getOvernightFlag() {
		return this.overnightFlag;
	}
	
	/**
	 * @param creditZhbankCode
	 */
	public void setCreditZhbankCode(String creditZhbankCode) {
		this.creditZhbankCode = creditZhbankCode;
	}
	
    /**
     * @return creditZhbankCode
     */
	public String getCreditZhbankCode() {
		return this.creditZhbankCode;
	}
	
	/**
	 * @param creditZhbankName
	 */
	public void setCreditZhbankName(String creditZhbankName) {
		this.creditZhbankName = creditZhbankName;
	}
	
    /**
     * @return creditZhbankName
     */
	public String getCreditZhbankName() {
		return this.creditZhbankName;
	}
	
	/**
	 * @param credittype
	 */
	public void setCredittype(String credittype) {
		this.credittype = credittype;
	}
	
    /**
     * @return credittype
     */
	public String getCredittype() {
		return this.credittype;
	}
	
	/**
	 * @param improveDays
	 */
	public void setImproveDays(java.math.BigDecimal improveDays) {
		this.improveDays = improveDays;
	}
	
    /**
     * @return improveDays
     */
	public java.math.BigDecimal getImproveDays() {
		return this.improveDays;
	}
	
	/**
	 * @param yzFlag
	 */
	public void setYzFlag(String yzFlag) {
		this.yzFlag = yzFlag;
	}
	
    /**
     * @return yzFlag
     */
	public String getYzFlag() {
		return this.yzFlag;
	}
	
	/**
	 * @param coreTrandt
	 */
	public void setCoreTrandt(String coreTrandt) {
		this.coreTrandt = coreTrandt;
	}
	
    /**
     * @return coreTrandt
     */
	public String getCoreTrandt() {
		return this.coreTrandt;
	}
	
	/**
	 * @param coreTransq
	 */
	public void setCoreTransq(String coreTransq) {
		this.coreTransq = coreTransq;
	}
	
    /**
     * @return coreTransq
     */
	public String getCoreTransq() {
		return this.coreTransq;
	}
	
	/**
	 * @param rediscountOutBranchId
	 */
	public void setRediscountOutBranchId(String rediscountOutBranchId) {
		this.rediscountOutBranchId = rediscountOutBranchId;
	}
	
    /**
     * @return rediscountOutBranchId
     */
	public String getRediscountOutBranchId() {
		return this.rediscountOutBranchId;
	}
	
	/**
	 * @param rediscountOutBranchNo
	 */
	public void setRediscountOutBranchNo(String rediscountOutBranchNo) {
		this.rediscountOutBranchNo = rediscountOutBranchNo;
	}
	
    /**
     * @return rediscountOutBranchNo
     */
	public String getRediscountOutBranchNo() {
		return this.rediscountOutBranchNo;
	}
	
	/**
	 * @param saleBatchId
	 */
	public void setSaleBatchId(String saleBatchId) {
		this.saleBatchId = saleBatchId;
	}
	
    /**
     * @return saleBatchId
     */
	public String getSaleBatchId() {
		return this.saleBatchId;
	}


}