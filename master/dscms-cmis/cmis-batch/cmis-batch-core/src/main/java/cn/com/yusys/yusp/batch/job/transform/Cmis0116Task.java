package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0116Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0116</br>
 * 任务名称：加工任务-业务处理-信用卡额度同步  </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0116Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0116Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0116Service cmis0116Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job cmis0116Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_JOB.key, JobStepEnum.CMIS0116_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0116Job = this.jobBuilderFactory.get(JobStepEnum.CMIS0116_JOB.key)
                //账户信息处理
                .start(cmis0116UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0116CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(cmis0016DeleteApprStrMtableInfoStep(WILL_BE_INJECTED)) //清除 批复分项信息
                .next(cmis0016DeleteApprLmtSubBasicInfoStep(WILL_BE_INJECTED)) //清除 批复主信息
                .next(cmis0016UpdateBatSDjkDataAccountStep(WILL_BE_INJECTED))//更新批复主信息
                .next(cmis0016InsertApprLmtSubBasicInfoStep(WILL_BE_INJECTED)) //插入批复分项信息
                .next(cmis0016UpdateApprLmtSubBasicInfoStep(WILL_BE_INJECTED)) //更新批复主信息
                .next(cmis0016InsertApprStrMtableInfoStep(WILL_BE_INJECTED)) //插入批复主信息
                //处理大额分期
                .next(cmis0116InsertApprLmtSubBasicInfo02Step(WILL_BE_INJECTED))//插入批复分项信息，新增额度分项  对应大额分期文件
                .next(cmis0016InsertLmtContRelStep(WILL_BE_INJECTED))
                .next(cmis0016DeleteContAccRelStep(WILL_BE_INJECTED))
                .next(cmis0016InsertContAccRelStep(WILL_BE_INJECTED))
                .next(cmis0016UpdateLmtContRelStep(WILL_BE_INJECTED))
                .next(cmis0116UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0116Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0116UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0116_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0116UpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0116_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_UPDATE_TASK010_STEP.key, JobStepEnum.CMIS0116_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0116UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0116CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_CHECK_REL_STEP.key, JobStepEnum.CMIS0116_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0116CheckRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0116_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0116_CHECK_REL_STEP.key, JobStepEnum.CMIS0116_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.CMIS0116_CHECK_REL_STEP.key, JobStepEnum.CMIS0116_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0116CheckRelStep;
    }

    /**
     * 清除 批复分项信息
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0016DeleteApprStrMtableInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_DELETE_APPR_STR_MTABLE_INFO_STEP.key, JobStepEnum.CMIS0116_DELETE_APPR_STR_MTABLE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0016DeleteApprStrMtableInfoStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_DELETE_APPR_STR_MTABLE_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016DeleteApprStrMtableInfo(openDay);//清除 批复分项信息
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_DELETE_APPR_STR_MTABLE_INFO_STEP.key, JobStepEnum.CMIS0116_DELETE_APPR_STR_MTABLE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0016DeleteApprStrMtableInfoStep;
    }


    /**
     * 清除 批复主信息
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0016DeleteApprLmtSubBasicInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_DELETE_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepEnum.CMIS0116_DELETE_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0016DeleteApprLmtSubBasicInfoStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_DELETE_APPR_LMT_SUB_BASIC_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016DeleteApprLmtSubBasicInfo01(openDay);//清除 批复主信息
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_DELETE_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepEnum.CMIS0116_DELETE_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0016DeleteApprLmtSubBasicInfoStep;
    }



    /**
     *
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0016UpdateBatSDjkDataAccountStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_UPDATE_BAT_S_DJK_DATA_ACCOUNT_STEP.key, JobStepEnum.CMIS0116_UPDATE_BAT_S_DJK_DATA_ACCOUNT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0016UpdateBatSDjkDataAccountStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_UPDATE_BAT_S_DJK_DATA_ACCOUNT_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016UpdateBatSDjkDataAccount(openDay);//清除 批复主信息
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_UPDATE_BAT_S_DJK_DATA_ACCOUNT_STEP.key, JobStepEnum.CMIS0116_UPDATE_BAT_S_DJK_DATA_ACCOUNT_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0016UpdateBatSDjkDataAccountStep;
    }


    /**
     * 插入批复分项信息
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0016InsertApprLmtSubBasicInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_INSERT_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepEnum.CMIS0116_INSERT_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0016InsertApprLmtSubBasicInfoStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_INSERT_APPR_LMT_SUB_BASIC_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016InsertApprLmtSubBasicInfo(openDay);//插入批复分项信息
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_INSERT_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepEnum.CMIS0116_INSERT_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0016InsertApprLmtSubBasicInfoStep;
    }

    /**
     * 更新批复分项信息
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0016UpdateApprLmtSubBasicInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_UPDATE_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepEnum.CMIS0116_UPDATE_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0016UpdateApprLmtSubBasicInfoStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_UPDATE_APPR_LMT_SUB_BASIC_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016UpdateApprLmtSubBasicInfo(openDay);// 更新批复分项信息
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_UPDATE_APPR_LMT_SUB_BASIC_INFO_STEP.key, JobStepEnum.CMIS0116_UPDATE_APPR_LMT_SUB_BASIC_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0016UpdateApprLmtSubBasicInfoStep;
    }


    /**
     * 插入批复主信息
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0016InsertApprStrMtableInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_INSERT_APPR_STR_MTABLE_INFO_STEP.key, JobStepEnum.CMIS0116_INSERT_APPR_STR_MTABLE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0116InsertApprStrMtableInfoStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_INSERT_APPR_STR_MTABLE_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016InsertApprStrMtableInfo01(openDay);//插入批复主信息
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_INSERT_APPR_STR_MTABLE_INFO_STEP.key, JobStepEnum.CMIS0116_INSERT_APPR_STR_MTABLE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0116InsertApprStrMtableInfoStep;
    }


    /**
     * 插入批复分项信息，新增额度分项  对应大额分期文件
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0116InsertApprLmtSubBasicInfo02Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_INSERT_APPR_LMT_SUB_BASIC_INFO02_STEP.key, JobStepEnum.CMIS0116_INSERT_APPR_LMT_SUB_BASIC_INFO02_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0116InsertApprLmtSubBasicInfo02Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_INSERT_APPR_LMT_SUB_BASIC_INFO02_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016InsertApprLmtSubBasicInfo02(openDay);//插入批复分项信息，新增额度分项  对应大额分期文件
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_INSERT_APPR_LMT_SUB_BASIC_INFO02_STEP.key, JobStepEnum.CMIS0116_INSERT_APPR_LMT_SUB_BASIC_INFO02_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0116InsertApprLmtSubBasicInfo02Step;
    }


    /**
     * 插入分项占用关系，复核大额分期合同数据，是否都存在分项占用关系，如不存在，则新增；
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0016InsertLmtContRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_INSERT_LMT_CONT_REL_STEP.key, JobStepEnum.CMIS0116_INSERT_LMT_CONT_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0016InsertLmtContRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_INSERT_LMT_CONT_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016InsertLmtContRel(openDay);//插入分项占用关系，复核大额分期合同数据，是否都存在分项占用关系，如不存在，则新增；
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_INSERT_LMT_CONT_REL_STEP.key, JobStepEnum.CMIS0116_INSERT_LMT_CONT_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0016InsertLmtContRelStep;
    }


    /**
     * 删除合同占用关系信息，复核大额分期通联推送台账数据，是否存在台账占用关系，如不存在，则新增，如存在则更新余额；
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0016DeleteContAccRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_DELETE_CONT_ACC_REL_STEP.key, JobStepEnum.CMIS0116_DELETE_CONT_ACC_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0016DeleteContAccRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_DELETE_CONT_ACC_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016DeleteContAccRel(openDay);//插入分项占用关系，复核大额分期通联推送台账数据，是否存在台账占用关系，如不存在，则新增，如存在则更新余额；
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_DELETE_CONT_ACC_REL_STEP.key, JobStepEnum.CMIS0116_DELETE_CONT_ACC_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0016DeleteContAccRelStep;
    }

    /**
     * 插入合同占用关系信息，复核大额分期通联推送台账数据，是否存在台账占用关系，如不存在，则新增，如存在则更新余额；
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0016InsertContAccRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_DELETE_CONT_ACC_REL_STEP.key, JobStepEnum.CMIS0116_DELETE_CONT_ACC_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0016InsertContAccRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_DELETE_CONT_ACC_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016InsertContAccRel(openDay);//插入分项占用关系，复核大额分期通联推送台账数据，是否存在台账占用关系，如不存在，则新增，如存在则更新余额；
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_DELETE_CONT_ACC_REL_STEP.key, JobStepEnum.CMIS0116_DELETE_CONT_ACC_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0016InsertContAccRelStep;
    }

    /**
     * 22010201 专项消费分期 分项占用关系信息 占用总余额（折人民币）更新</br>
     * 22010201 专项消费分期 批复额度分项基础信息更新占用额</br>
     * 22010201专项消费分期 更新额度分项的 敞口已用总额更新</br>
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0016UpdateLmtContRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_INSERT_LMT_CONT_REL_STEP.key, JobStepEnum.CMIS0116_INSERT_LMT_CONT_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0016UpdateLmtContRelStep = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_INSERT_LMT_CONT_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    cmis0116Service.cmis0016LmtContRel(openDay);//22010201 专项消费分期 分项占用关系信息 占用总余额（折人民币）更新
                    cmis0116Service.cmis0016ApprLmtSubBasicInfo02A(openDay);//22010201 专项消费分期 批复额度分项基础信息更新占用额
                    cmis0116Service.updateApprLmtSubBasicInfo03(openDay);//22010201专项消费分期 更新额度分项的 敞口已用总额更新
                    // TODO 后续拆分到不同步骤中，根据执行效率
                    cmis0116Service.cmis0016ApprLmtSubBasicInfo04(openDay);//更新额度分项的LOAN_BALANCE贷款余额
                    cmis0116Service.cmis0016ApprLmtSubBasicInfo05(openDay);//更新22010201专项消费分期 额度分项的PVP_OUTSTND_AMT已出账金额
                    cmis0116Service.cmis0016ApprLmtSubBasicInfo06(openDay);//更新AVL_OUTSTND_AMT可出账金额
                    cmis0116Service.cmis0116ApprStrMtableInfo02(openDay);//授信批复台账表 22010201	专项消费分期
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_INSERT_LMT_CONT_REL_STEP.key, JobStepEnum.CMIS0116_INSERT_LMT_CONT_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0016UpdateLmtContRelStep;
    }



    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0116UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.CMIS0116_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0116_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0116UpdateTask100Step = this.stepBuilderFactory.get(JobStepEnum.CMIS0116_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0116_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.CMIS0116_UPDATE_TASK100_STEP.key, JobStepEnum.CMIS0116_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0116UpdateTask100Step;
    }
}
