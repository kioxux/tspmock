package cn.com.yusys.yusp.batch.web.server.cmisbatch0001;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001RespDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0001.CmisBatch0001Service;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:调度运行管理信息查詢
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CMISBATCH0001:调度运行管理信息查詢")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0001Resource.class);

    @Autowired
    private CmisBatch0001Service cmisBatch0001Service;

    /**
     * 交易码：cmisbatch0001
     * 交易描述：调度运行管理信息查詢
     *
     * @param cmisbatch0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("调度运行管理信息查詢")
    @PostMapping("/cmisbatch0001")
    protected @ResponseBody
    ResultDto<Cmisbatch0001RespDto> cmisbatch0001(@Validated @RequestBody Cmisbatch0001ReqDto cmisbatch0001ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value, JSON.toJSONString(cmisbatch0001ReqDto));
        Cmisbatch0001RespDto cmisbatch0001RespDto = new Cmisbatch0001RespDto();// 响应Dto:调度运行管理信息查詢
        ResultDto<Cmisbatch0001RespDto> cmisbatch0001ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0001ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value, JSON.toJSONString(cmisbatch0001ReqDto));
            cmisbatch0001RespDto = cmisBatch0001Service.cmisBatch0001(cmisbatch0001ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value, JSON.toJSONString(cmisbatch0001RespDto));
            // 封装cmisbatch0001ResultDto中正确的返回码和返回信息
            cmisbatch0001ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0001ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value, e.getMessage());
            // 封装cmisbatch0001ResultDto中异常返回码和返回信息
            cmisbatch0001ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0001ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0001RespDto到cmisbatch0001ResultDto中
        cmisbatch0001ResultDto.setData(cmisbatch0001RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0001.key, DscmsEnum.TRADE_CODE_CMISBATCH0001.value, JSON.toJSONString(cmisbatch0001ResultDto));
        return cmisbatch0001ResultDto;
    }
}
