package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0427Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0427</br>
 * 任务名称：加工任务-贷后管理-贷后催收任务  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0427Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0427Service.class);

    @Autowired
    private Cmis0427Mapper cmis0427Mapper;

    /**
     * 插入催收任务信息表
     *
     * @param openDay
     */
    public void cmis0427InsertPlaBcmTaskInfo(String openDay) {
        logger.info("删除当天催收任务重复的数据开始");
        cmis0427Mapper.deletePlaBcmTaskInfo01();
        logger.info("删除当天催收任务重复的数据结束");
        logger.info("插入催收任务信息表[分类为正常 关注][小微条线]开始,请求参数为:[{}]", openDay);
        int insertPlaBcmTaskInfo01 = cmis0427Mapper.insertPlaBcmTaskInfo01(openDay);
        logger.info("插入催收任务信息表[分类为正常 关注][小微条线]结束,返回参数为:[{}]", insertPlaBcmTaskInfo01);
        logger.info("插入催收任务信息表[分类为正常 关注][非小微条线]开始,请求参数为:[{}]", openDay);
        int insertPlaBcmTaskInfo02 = cmis0427Mapper.insertPlaBcmTaskInfo02(openDay);
        logger.info("插入催收任务信息表[分类为正常 关注][非小微条线]结束,返回参数为:[{}]", insertPlaBcmTaskInfo02);
        logger.info("插入催收任务信息表[不良贷款][小微条线]开始,请求参数为:[{}]", openDay);
        int insertPlaBcmTaskInfo03 = cmis0427Mapper.insertPlaBcmTaskInfo03(openDay);
        logger.info("插入催收任务信息表[不良贷款][小微条线]结束,返回参数为:[{}]", insertPlaBcmTaskInfo03);
        logger.info("插入催收任务信息表[不良贷款][非小微条线]开始,请求参数为:[{}]", openDay);
        int insertPlaBcmTaskInfo04 = cmis0427Mapper.insertPlaBcmTaskInfo04(openDay);
        logger.info("插入催收任务信息表[不良贷款][非小微条线]结束,返回参数为:[{}]", insertPlaBcmTaskInfo04);
    }

    /**
     * 插入催收关联业务表
     *
     * @param openDay
     */
    public void cmis0427InsertPlaBcmRel(String openDay) {
        logger.info("删除当天借据重复的数据开始");
        cmis0427Mapper.deletePlaBcmTaskInfo02();
        logger.info("删除当天借据重复的数据结束");
        logger.info("插入小微催收关联借据开始,请求参数为:[{}]", openDay);
        int insertPlaBcmRel01 = cmis0427Mapper.insertPlaBcmRel01(openDay);
        logger.info("插入小微催收关联借据表结束,返回参数为:[{}]", insertPlaBcmRel01);
        logger.info("插入非小微催收关联借据表开始,请求参数为:[{}]", openDay);
        int insertPlaBcmRel02 = cmis0427Mapper.insertPlaBcmRel02(openDay);
        logger.info("插入非小微催收关联借据表结束,返回参数为:[{}]", insertPlaBcmRel02);
    }
}
