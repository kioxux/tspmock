package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0127Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0127</br>
 * 任务名称：加工任务-业务处理-非零内评数据更新 </br>
 * V1.1:根据问题编号:20211114-00035中台账相关的逻辑调整
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0127Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0127Service.class);

    @Autowired
    private Cmis0127Mapper cmis0127Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新专业贷款最终认定等级
     *
     * @param openDay
     */
    public void cmis0127UpdateMajorGradeInfo(String openDay) {
        logger.info("更新专业贷款最终认定等级开始,请求参数为:[{}]", openDay);
        int updateMajorGradeInfo = cmis0127Mapper.updateMajorGradeInfo(openDay);
        logger.info("更新专业贷款最终认定等级结束,返回参数为:[{}]", updateMajorGradeInfo);
    }

    /**
     * 更新贷款合同
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0127UpdateCtrLoanCont(String openDay) {
        logger.info("清空 临时表-贷款合同和非零内评合同关系表 开始,请求参数为:[{}]", openDay);
        // int truncateTmpFlsCtrLoanCont = cmis0127Mapper.truncateTmpFlsCtrLoanCont(openDay);// cmis_batch.tmp_fls_ctr_loan_cont
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_fls_ctr_loan_cont");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("清空  临时表-贷款合同和非零内评合同关系表 结束");

        logger.info("插入 临时表-贷款合同和非零内评合同关系表 开始,请求参数为:[{}]", openDay);
        int insertTmpFlsCtrLoanCont = cmis0127Mapper.insertTmpFlsCtrLoanCont(openDay);
        logger.info("插入 临时表-贷款合同和非零内评合同关系表 结束,返回参数为:[{}]", insertTmpFlsCtrLoanCont);

        logger.info("更新贷款合同开始,请求参数为:[{}]", openDay);
        int updateCtrLoanCont = cmis0127Mapper.updateCtrLoanCont(openDay);
        logger.info("更新贷款合同结束,返回参数为:[{}]", updateCtrLoanCont);
    }

    /**
     * 更新银承合同
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0127UpdateCtrAccpCont(String openDay) {
        logger.info("清空 临时表-银承合同和非零内评合同关系表 开始,请求参数为:[{}]", openDay);
        // int truncateTmpFlsCtrAccpCont = cmis0127Mapper.truncateTmpFlsCtrAccpCont(openDay);// cmis_batch.tmp_fls_ctr_accp_cont
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_fls_ctr_accp_cont");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("清空 临时表-银承合同和非零内评合同关系表 结束");

        logger.info("插入 临时表-银承合同和非零内评合同关系表 开始,请求参数为:[{}]", openDay);
        int insertTmpFlsCtrAccpCont = cmis0127Mapper.insertTmpFlsCtrAccpCont(openDay);
        logger.info("插入 临时表-银承合同和非零内评合同关系表 结束,返回参数为:[{}]", insertTmpFlsCtrAccpCont);

        logger.info("更新银承合同开始,请求参数为:[{}]", openDay);
        int updateCtrAccpCont = cmis0127Mapper.updateCtrAccpCont(openDay);
        logger.info("更新银承合同结束,返回参数为:[{}]", updateCtrAccpCont);
    }

    /**
     * 更新贴现合同
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0127UpdateCtrDiscCont(String openDay) {
        logger.info("清空 临时表-贴现合同和非零内评合同关系表 开始,请求参数为:[{}]", openDay);
        // int truncateTmpFlsCtrDiscCont = cmis0127Mapper.truncateTmpFlsCtrDiscCont(openDay);// cmis_batch.tmp_fls_ctr_disc_cont
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_fls_ctr_disc_cont");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("清空 临时表-贴现合同和非零内评合同关系表 结束");

        logger.info("插入 临时表-贴现合同和非零内评合同关系表 开始,请求参数为:[{}]", openDay);
        int insertTmpFlsCtrDiscCont = cmis0127Mapper.insertTmpFlsCtrDiscCont(openDay);
        logger.info("插入 临时表-贴现合同和非零内评合同关系表 结束,返回参数为:[{}]", insertTmpFlsCtrDiscCont);

        logger.info("更新贴现合同开始,请求参数为:[{}]", openDay);
        int updateCtrDiscCont = cmis0127Mapper.updateCtrDiscCont(openDay);
        logger.info("更新贴现合同结束,返回参数为:[{}]", updateCtrDiscCont);
    }

    /**
     * 更新保函合同
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0127UpdateCtrCvrgCont(String openDay) {
        logger.info("清空 临时表-保函合同和非零内评合同关系表 开始,请求参数为:[{}]", openDay);
        // int truncateTmpFlsCtrCvrgCont = cmis0127Mapper.truncateTmpFlsCtrCvrgCont(openDay);// cmis_batch.tmp_fls_ctr_cvrg_cont
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_fls_ctr_cvrg_cont");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("清空 临时表-保函合同和非零内评合同关系表 结束");

        logger.info("插入 临时表-保函合同和非零内评合同关系表 开始,请求参数为:[{}]", openDay);
        int insertTmpFlsCtrCvrgCont = cmis0127Mapper.insertTmpFlsCtrCvrgCont(openDay);
        logger.info("插入 临时表-保函合同和非零内评合同关系表 结束,返回参数为:[{}]", insertTmpFlsCtrCvrgCont);

        logger.info("更新保函合同开始,请求参数为:[{}]", openDay);
        int updateCtrCvrgCont = cmis0127Mapper.updateCtrCvrgCont(openDay);
        logger.info("更新保函合同结束,返回参数为:[{}]", updateCtrCvrgCont);
    }

    /**
     * 更新信用证合同
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0127UpdateCtrTfLocCont(String openDay) {
        logger.info("清空 临时表-信用证合同和非零内评合同关系表 开始,请求参数为:[{}]", openDay);
        // int truncateTmpFlsCtrTfLocCont = cmis0127Mapper.truncateTmpFlsCtrTfLocCont(openDay);// cmis_batch.tmp_fls_ctr_tf_loc_cont
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_fls_ctr_tf_loc_cont");// 20211101 调整为调用[1.删除相关表,2.根据备份表创建相关表,3.分析相关表]的公共方法
        logger.info("清空 临时表-信用证合同和非零内评合同关系表 结束");

        logger.info("插入 临时表-信用证合同和非零内评合同关系表 开始,请求参数为:[{}]", openDay);
        int insertTmpFlsCtrTfLocCont = cmis0127Mapper.insertTmpFlsCtrTfLocCont(openDay);
        logger.info("插入 临时表-信用证合同和非零内评合同关系表 结束,返回参数为:[{}]", insertTmpFlsCtrTfLocCont);

        logger.info("更新信用证合同开始,请求参数为:[{}]", openDay);
        int updateCtrTfLocCont = cmis0127Mapper.updateCtrTfLocCont(openDay);
        logger.info("更新信用证合同结束,返回参数为:[{}]", updateCtrTfLocCont);
    }

    /**
     * 更新贷款台账
     *
     * @param openDay
     */
    public void cmis0127UpdateAccLoan(String openDay) {
        logger.info("更新贷款台账开始,请求参数为:[{}]", openDay);
        int updateAccLoan = cmis0127Mapper.updateAccLoan(openDay);
        logger.info("更新贷款台账结束,返回参数为:[{}]", updateAccLoan);
    }

    /**
     * 更新银承台账明细
     *
     * @param openDay
     */
    public void cmis0127UpdateAccAccpDrftSub(String openDay) {
        logger.info("更新银承台账明细开始,请求参数为:[{}]", openDay);
        int updateAccAccpDrftSub = cmis0127Mapper.updateAccAccpDrftSub(openDay);
        logger.info("更新银承台账明细结束,返回参数为:[{}]", updateAccAccpDrftSub);
    }

    /**
     * 更新贴现台账
     *
     * @param openDay
     */
    public void cmis0127UpdateAccDisc(String openDay) {
        logger.info("更新贴现台账开始,请求参数为:[{}]", openDay);
        int updateAccDisc = cmis0127Mapper.updateAccDisc(openDay);
        logger.info("更新贴现台账结束,返回参数为:[{}]", updateAccDisc);
    }

    /**
     * 更新保函台账
     *
     * @param openDay
     */
    public void cmis0127UpdateAccCvrs(String openDay) {
        logger.info("更新保函台账开始,请求参数为:[{}]", openDay);
        int updateAccCvrs = cmis0127Mapper.updateAccCvrs(openDay);
        logger.info("更新保函台账结束,返回参数为:[{}]", updateAccCvrs);
    }

    /**
     * 更新信用证台账
     *
     * @param openDay
     */
    public void cmis0127UpdateAccTfLoc(String openDay) {
        logger.info("更新信用证台账开始,请求参数为:[{}]", openDay);
        int updateAccTfLoc = cmis0127Mapper.updateAccTfLoc(openDay);
        logger.info("更新信用证台账结束,返回参数为:[{}]", updateAccTfLoc);
    }
}
