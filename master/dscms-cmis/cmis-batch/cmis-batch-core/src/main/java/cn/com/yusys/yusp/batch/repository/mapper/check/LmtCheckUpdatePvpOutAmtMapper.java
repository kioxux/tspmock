/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.check;

import org.apache.ibatis.annotations.Param;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckUpdatePvpOutAmtMapper
 * @类描述: #Dao类
 * @功能描述: 数据更新额度校正-计算分项可出账金额
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCheckUpdatePvpOutAmtMapper {


    /**
     * 1、计算分项已出账金额、可出账金额
     */
    int calculatePvpOutAmtAlreadyAndCanOutAmt1(@Param("cusId") String cusId);

    int calculatePvpOutAmtAlreadyAndCanOutAmt2(@Param("cusId") String cusId);

    int calculatePvpOutAmtAlreadyAndCanOutAmt3(@Param("cusId") String cusId);

    int calculatePvpOutAmtAlreadyAndCanOutAmt4(@Param("cusId") String cusId);


    /**
     * 2、二级分项已出账汇总至一级分项
     */
    int collectPvpOutAmt2LevelSubOutAmtTo1LevelSub1(@Param("cusId") String cusId);

    int collectPvpOutAmt2LevelSubOutAmtTo1LevelSub2(@Param("cusId") String cusId);

    int collectPvpOutAmt2LevelSubOutAmtTo1LevelSub3(@Param("cusId") String cusId);

    int collectPvpOutAmt2LevelSubOutAmtTo1LevelSub4(@Param("cusId") String cusId);

}