/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpSnRepayPlanFileTemp
 * @类描述: bat_s_rcp_sn_repay_plan_file_temp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 15:42:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_rcp_sn_repay_plan_file_temp")
public class BatSRcpSnRepayPlanFileTemp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 项目编码  联合贷平台分配（01001001） **/
	@Id
	@Column(name = "PRD_CODE")
	private String prdCode;
	
	/** 贷款编号  苏宁银行的借据编号 **/
	@Id
	@Column(name = "CONTRACT_NO")
	private String contractNo;
	
	/** 期数  只提供标准期供，提前还款发生缩期，会产生非标准结清子期供，非标准期供不提供 **/
	@Id
	@Column(name = "PERIOD")
	private String period;
	
	/** 本期状态  0-正常；1-逾期；4-核销；5-结清 **/
	@Column(name = "CURR_STATUS", unique = false, nullable = true, length = 10)
	private String currStatus;
	
	/** 起始日期  YYYYMMDD **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 到期日期  YYYYMMDD **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 宽限期到期日期 **/
	@Column(name = "GRACE_TERM_END_DATE", unique = false, nullable = true, length = 10)
	private String graceTermEndDate;
	
	/** 初始应还本金   还款日前使用，到还款日应还本金，单位元 **/
	@Column(name = "CSYH_BJ", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal csyhBj;
	
	/** 初始应还利息   还款日前使用，到还款日应还利息，单位元 **/
	@Column(name = "CSYH_LX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal csyhLx;
	
	/** 初始应还贴息    单位元 **/
	@Column(name = "CSYH_TX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal csyhTx;
	
	/** 剩余未还本金    还款日及其之后使用，当期未还本金，单位元 **/
	@Column(name = "SYWH_BJ", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal sywhBj;
	
	/** 已还本金      当期已还本金，单位元 **/
	@Column(name = "YH_BJ", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal yhBj;
	
	/** 剩余未还欠息  当期未还利息，也是剩余未还,单位元（当期未到期其值非准确值） **/
	@Column(name = "SYWH_QX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal sywhQx;
	
	/** 已还利息    当期已还利息，单位元 **/
	@Column(name = "YH_LX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal yhLx;
	
	/** 剩余未还罚息  当期未还罚息，也是剩余未还,单位元 **/
	@Column(name = "SYWH_FAX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal sywhFax;
	
	/** 已还罚息  当期已还罚息，单位元 **/
	@Column(name = "YH_FAX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal yhFax;
	
	/** 剩余未还复息  当期未还复息，也是剩余未还,单位元 **/
	@Column(name = "SYWH_FUX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal sywhFux;
	
	/** 已还复息  当期已还复息，单位元 **/
	@Column(name = "YH_FUX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal yhFux;
	
	/** 应收费用  单位元 **/
	@Column(name = "YS_FEE", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal ysFee;
	
	/** 手续费   单位元 **/
	@Column(name = "SX_FEE", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal sxFee;
	
	/** 初始减免利息  当期应减免利息，单位元 **/
	@Column(name = "CSJM_LX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal csjmLx;
	
	/** 已减免利息   当期已减免利息，单位元 **/
	@Column(name = "YJM_LX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal yjmLx;
	
	/** 初始减免罚息  当期应减免罚息，单位元 **/
	@Column(name = "CSJM_FAX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal csjmFax;
	
	/** 已减免罚息   当期已减免罚息，单位元 **/
	@Column(name = "YJM_FAX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal yjmFax;
	
	/** 初始减免复息   当期应减免复息，单位元 **/
	@Column(name = "CSJM_FUX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal csjmFux;
	
	/** 已减免复息    当期已减免复息，单位元 **/
	@Column(name = "YJM_FUX", unique = false, nullable = true, length = 17)
	private java.math.BigDecimal yjmFux;
	
	/** 录入日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "LAST_UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	/** 零售智能风控数据日期 **/
	@Column(name = "RCP_DATA_DATE", unique = false, nullable = true, length = 10)
	private String rcpDataDate;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param prdCode
	 */
	public void setPrdCode(String prdCode) {
		this.prdCode = prdCode;
	}
	
    /**
     * @return prdCode
     */
	public String getPrdCode() {
		return this.prdCode;
	}
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param period
	 */
	public void setPeriod(String period) {
		this.period = period;
	}
	
    /**
     * @return period
     */
	public String getPeriod() {
		return this.period;
	}
	
	/**
	 * @param currStatus
	 */
	public void setCurrStatus(String currStatus) {
		this.currStatus = currStatus;
	}
	
    /**
     * @return currStatus
     */
	public String getCurrStatus() {
		return this.currStatus;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param graceTermEndDate
	 */
	public void setGraceTermEndDate(String graceTermEndDate) {
		this.graceTermEndDate = graceTermEndDate;
	}
	
    /**
     * @return graceTermEndDate
     */
	public String getGraceTermEndDate() {
		return this.graceTermEndDate;
	}
	
	/**
	 * @param csyhBj
	 */
	public void setCsyhBj(java.math.BigDecimal csyhBj) {
		this.csyhBj = csyhBj;
	}
	
    /**
     * @return csyhBj
     */
	public java.math.BigDecimal getCsyhBj() {
		return this.csyhBj;
	}
	
	/**
	 * @param csyhLx
	 */
	public void setCsyhLx(java.math.BigDecimal csyhLx) {
		this.csyhLx = csyhLx;
	}
	
    /**
     * @return csyhLx
     */
	public java.math.BigDecimal getCsyhLx() {
		return this.csyhLx;
	}
	
	/**
	 * @param csyhTx
	 */
	public void setCsyhTx(java.math.BigDecimal csyhTx) {
		this.csyhTx = csyhTx;
	}
	
    /**
     * @return csyhTx
     */
	public java.math.BigDecimal getCsyhTx() {
		return this.csyhTx;
	}
	
	/**
	 * @param sywhBj
	 */
	public void setSywhBj(java.math.BigDecimal sywhBj) {
		this.sywhBj = sywhBj;
	}
	
    /**
     * @return sywhBj
     */
	public java.math.BigDecimal getSywhBj() {
		return this.sywhBj;
	}
	
	/**
	 * @param yhBj
	 */
	public void setYhBj(java.math.BigDecimal yhBj) {
		this.yhBj = yhBj;
	}
	
    /**
     * @return yhBj
     */
	public java.math.BigDecimal getYhBj() {
		return this.yhBj;
	}
	
	/**
	 * @param sywhQx
	 */
	public void setSywhQx(java.math.BigDecimal sywhQx) {
		this.sywhQx = sywhQx;
	}
	
    /**
     * @return sywhQx
     */
	public java.math.BigDecimal getSywhQx() {
		return this.sywhQx;
	}
	
	/**
	 * @param yhLx
	 */
	public void setYhLx(java.math.BigDecimal yhLx) {
		this.yhLx = yhLx;
	}
	
    /**
     * @return yhLx
     */
	public java.math.BigDecimal getYhLx() {
		return this.yhLx;
	}
	
	/**
	 * @param sywhFax
	 */
	public void setSywhFax(java.math.BigDecimal sywhFax) {
		this.sywhFax = sywhFax;
	}
	
    /**
     * @return sywhFax
     */
	public java.math.BigDecimal getSywhFax() {
		return this.sywhFax;
	}
	
	/**
	 * @param yhFax
	 */
	public void setYhFax(java.math.BigDecimal yhFax) {
		this.yhFax = yhFax;
	}
	
    /**
     * @return yhFax
     */
	public java.math.BigDecimal getYhFax() {
		return this.yhFax;
	}
	
	/**
	 * @param sywhFux
	 */
	public void setSywhFux(java.math.BigDecimal sywhFux) {
		this.sywhFux = sywhFux;
	}
	
    /**
     * @return sywhFux
     */
	public java.math.BigDecimal getSywhFux() {
		return this.sywhFux;
	}
	
	/**
	 * @param yhFux
	 */
	public void setYhFux(java.math.BigDecimal yhFux) {
		this.yhFux = yhFux;
	}
	
    /**
     * @return yhFux
     */
	public java.math.BigDecimal getYhFux() {
		return this.yhFux;
	}
	
	/**
	 * @param ysFee
	 */
	public void setYsFee(java.math.BigDecimal ysFee) {
		this.ysFee = ysFee;
	}
	
    /**
     * @return ysFee
     */
	public java.math.BigDecimal getYsFee() {
		return this.ysFee;
	}
	
	/**
	 * @param sxFee
	 */
	public void setSxFee(java.math.BigDecimal sxFee) {
		this.sxFee = sxFee;
	}
	
    /**
     * @return sxFee
     */
	public java.math.BigDecimal getSxFee() {
		return this.sxFee;
	}
	
	/**
	 * @param csjmLx
	 */
	public void setCsjmLx(java.math.BigDecimal csjmLx) {
		this.csjmLx = csjmLx;
	}
	
    /**
     * @return csjmLx
     */
	public java.math.BigDecimal getCsjmLx() {
		return this.csjmLx;
	}
	
	/**
	 * @param yjmLx
	 */
	public void setYjmLx(java.math.BigDecimal yjmLx) {
		this.yjmLx = yjmLx;
	}
	
    /**
     * @return yjmLx
     */
	public java.math.BigDecimal getYjmLx() {
		return this.yjmLx;
	}
	
	/**
	 * @param csjmFax
	 */
	public void setCsjmFax(java.math.BigDecimal csjmFax) {
		this.csjmFax = csjmFax;
	}
	
    /**
     * @return csjmFax
     */
	public java.math.BigDecimal getCsjmFax() {
		return this.csjmFax;
	}
	
	/**
	 * @param yjmFax
	 */
	public void setYjmFax(java.math.BigDecimal yjmFax) {
		this.yjmFax = yjmFax;
	}
	
    /**
     * @return yjmFax
     */
	public java.math.BigDecimal getYjmFax() {
		return this.yjmFax;
	}
	
	/**
	 * @param csjmFux
	 */
	public void setCsjmFux(java.math.BigDecimal csjmFux) {
		this.csjmFux = csjmFux;
	}
	
    /**
     * @return csjmFux
     */
	public java.math.BigDecimal getCsjmFux() {
		return this.csjmFux;
	}
	
	/**
	 * @param yjmFux
	 */
	public void setYjmFux(java.math.BigDecimal yjmFux) {
		this.yjmFux = yjmFux;
	}
	
    /**
     * @return yjmFux
     */
	public java.math.BigDecimal getYjmFux() {
		return this.yjmFux;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}
	
	/**
	 * @param rcpDataDate
	 */
	public void setRcpDataDate(String rcpDataDate) {
		this.rcpDataDate = rcpDataDate;
	}
	
    /**
     * @return rcpDataDate
     */
	public String getRcpDataDate() {
		return this.rcpDataDate;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}