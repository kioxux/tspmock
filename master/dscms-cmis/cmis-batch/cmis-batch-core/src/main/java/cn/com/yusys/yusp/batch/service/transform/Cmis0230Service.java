package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0230Mapper;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0230</br>
 * 任务名称：加工任务-额度处理-全行贷款额度分配更新 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0230Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0230Service.class);

    @Autowired
    private Cmis0230Mapper cmis0230Mapper;

    /**
     * 全行贷款额度分配更新
     *
     * @param openDay
     */
    public void cmis0230UpdateManaLmt(String openDay) {
        String nextOpenDay = DateUtils.addDay(openDay, "yyyy-MM-dd", 1); // 切日后营业日期
        logger.info("切日后营业日期为:[{}]", nextOpenDay);
        Map paramMap = new HashMap<String, String>();
        paramMap.put("nextOpenDay", nextOpenDay);// 切日后营业日期
        paramMap.put("openDay", openDay);//切日前营业日期

        logger.info("1.分支机构额度管控备份至历史表开始,请求参数为:[{}]", openDay);
        int insertManaOrgLmtHis = cmis0230Mapper.insertManaOrgLmtHis(openDay);
        logger.info("1.分支机构额度管控备份至历史表结束,返回参数为:[{}]", insertManaOrgLmtHis);

        logger.info("2.业务条线额度管控备份至历史表开始,请求参数为:[{}]", openDay);
        int insertManaBusiLmtHis = cmis0230Mapper.insertManaBusiLmtHis(openDay);
        logger.info("2.业务条线额度管控备份至历史表结束,返回参数为:[{}]", insertManaBusiLmtHis);

        logger.info("全行贷款额度分配：日终月末最后一天日终，更新[分支机构额度管控表]当月可净新增对公贷款投放金额 初始化为10亿开始,请求参数为:[{}]", nextOpenDay);
        int updateManaOrgLmt = cmis0230Mapper.updateManaOrgLmt(nextOpenDay);
        logger.info("全行贷款额度分配：日终月末最后一天日终，更新[分支机构额度管控表]当月可净新增对公贷款投放金额 初始化为10亿结束,返回参数为:[{}]", updateManaOrgLmt);

        logger.info("全行贷款额度分配：日终月末最后一天日终，更新[业务条线额度管控表]当月可净新增对公贷款投放金额 初始化为10亿开始,请求参数为:[{}]", nextOpenDay);
        int updateManaBusiLmt = cmis0230Mapper.updateManaBusiLmt(nextOpenDay);
        logger.info("全行贷款额度分配：日终月末最后一天日终，更新[业务条线额度管控表]当月可净新增对公贷款投放金额 初始化为10亿结束,返回参数为:[{}]", updateManaBusiLmt);

        logger.info("全行贷款额度分配:月末获取大数据平台推送机构上月末对公贷款余额 更新至 分支机构额度管控表开始,请求参数为:[{}]", nextOpenDay);
        int updateMolBsofdm = cmis0230Mapper.updateMolBsofdm(nextOpenDay);
        logger.info("全行贷款额度分配:月末获取大数据平台推送机构上月末对公贷款余额 更新至 分支机构额度管控表结束,返回参数为:[{}]", updateMolBsofdm);

        logger.info("全行贷款额度分配:每日 获取的大数据平台推送机构上日对公贷款余额 更新至 分支机构额度管控表开始,请求参数为:[{}]", nextOpenDay);
        int updateMolBsofdd = cmis0230Mapper.updateMolBsofdd(nextOpenDay);
        logger.info("全行贷款额度分配:每日 获取的大数据平台推送机构上日对公贷款余额 更新至 分支机构额度管控表结束,返回参数为:[{}]", updateMolBsofdd);

        // 20211030 同步zjw代码增加 更新业务条线额度管控表  开始
        logger.info("全行贷款额度分配：月末获取大数据平台推送机构条线上月末贷款余额 更新至 业务条线额度管控表开始,请求参数为:[{}]", nextOpenDay);
        int updateMblBsofydm = cmis0230Mapper.updateMblBsofydm(nextOpenDay);
        logger.info("全行贷款额度分配：月末获取大数据平台推送机构条线上月末贷款余额 更新至 业务条线额度管控表结束,返回参数为:[{}]", updateMblBsofydm);

        logger.info("全行贷款额度分配：每日 获取的大数据平台推送机构上日机构条线上月末贷款余额 更新至 业务条线额度管控表开始,请求参数为:[{}]", nextOpenDay);
        int updateMblBsofydd = cmis0230Mapper.updateMblBsofydd(nextOpenDay);
        logger.info("全行贷款额度分配：每日 获取的大数据平台推送机构上日机构条线上月末贷款余额 更新至 业务条线额度管控表结束,返回参数为:[{}]", updateMblBsofydd);
        // 20211030 同步zjw代码增加 更新业务条线额度管控表  结束

        logger.info("贴现限额：日终月末最后一天更新状态为失效开始,请求参数为:[{}]", JSON.toJSONString(paramMap));
        int updateLmtDiscOrg = cmis0230Mapper.updateLmtDiscOrg(paramMap);
        logger.info("贴现限额：日终月末最后一天更新状态为失效结束,返回参数为:[{}]", updateLmtDiscOrg);

        //大额风险暴露客户类型，两边码值不对应，进行转换  add by zhangjw 20211107
        logger.info("大额风险暴露客户类型，两边码值不对应，进行转换开始,请求参数为:[{}]", openDay);
        int updateDefxKhlx01 = cmis0230Mapper.updateDefxKhlx01(nextOpenDay);
        int updateDefxKhlx02 = cmis0230Mapper.updateDefxKhlx02(nextOpenDay);
        int updateDefxKhlx03 = cmis0230Mapper.updateDefxKhlx03(nextOpenDay);
        int updateDefxKhlx04 = cmis0230Mapper.updateDefxKhlx04(nextOpenDay);

        int updateDefxKhlxHz01 = cmis0230Mapper.updateDefxKhlxHz01(nextOpenDay);
        int updateDefxKhlxHz02 = cmis0230Mapper.updateDefxKhlxHz02(nextOpenDay);
        int updateDefxKhlxHz03 = cmis0230Mapper.updateDefxKhlxHz03(nextOpenDay);
        int updateDefxKhlxHz04 = cmis0230Mapper.updateDefxKhlxHz04(nextOpenDay);
        logger.info("大额风险暴露客户类型，两边码值不对应，进行转换结束,返回值:[{}] [{}] [{}] [{}] [{}] [{}] [{}] [{}]",
                updateDefxKhlx01,updateDefxKhlx02,updateDefxKhlx03,updateDefxKhlx04,updateDefxKhlxHz01,updateDefxKhlxHz02,updateDefxKhlxHz03,updateDefxKhlxHz04);

        // 20211030 通过查询切日前营业日期代替传参,以避免SQL执行报错 开始
        logger.info("大额风险暴露-客户维度风险暴露汇总：查询15个月以前的数据开始,请求参数为:[{}]", openDay);
        int queryDmRisKhFxJgbxJk15M = cmis0230Mapper.queryDmRisKhFxJgbxJk15M(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times = new BigDecimal(queryDmRisKhFxJgbxJk15M).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        queryDmRisKhFxJgbxJk15M = times.intValue();
        logger.info("大额风险暴露-客户维度风险暴露汇总：查询15个月以前的数据结束,返回参数为:[{}]", queryDmRisKhFxJgbxJk15M);

        for (int i = 0; i < queryDmRisKhFxJgbxJk15M; i++) {
            logger.info("第[" + i + "]大额风险暴露-客户维度风险暴露汇总：清理15个月以前的数据开始,请求参数为:[{}]", openDay);
            int deleteDmRisKhFxJgbxJk15M = cmis0230Mapper.deleteDmRisKhFxJgbxJk15M(openDay);
            logger.info("第[" + i + "]大额风险暴露-客户维度风险暴露汇总：清理15个月以前的数据结束,返回参数为:[{}]", deleteDmRisKhFxJgbxJk15M);
        }

        logger.info("大额风险暴露-风险暴露指标配置历史表：查询15个月以前的数据开始,请求参数为:[{}]", openDay);
        int queryDeRiskIndexCfgHis15M = cmis0230Mapper.queryDeRiskIndexCfgHis15M(openDay);
        // 由于TDSQL不支持ceiling函数，此处在java中处理
        BigDecimal times2 = new BigDecimal(queryDeRiskIndexCfgHis15M).divide(new BigDecimal(10000L), RoundingMode.CEILING);
        queryDeRiskIndexCfgHis15M = times2.intValue();
        logger.info("大额风险暴露-风险暴露指标配置历史表：查询15个月以前的数据结束,返回参数为:[{}]", queryDeRiskIndexCfgHis15M);

        for (int i = 0; i < queryDeRiskIndexCfgHis15M; i++) {
            logger.info("第[" + i + "]大额风险暴露-风险暴露指标配置历史表：清理15个月以前的数据开始,请求参数为:[{}]", openDay);
            int deleteDeRiskIndexCfgHis15M = cmis0230Mapper.deleteDeRiskIndexCfgHis15M(openDay);
            logger.info("第[" + i + "]大额风险暴露-风险暴露指标配置历史表：清理15个月以前的数据结束,返回参数为:[{}]", deleteDeRiskIndexCfgHis15M);
        }
        // 20211030 通过查询切日前营业日期代替传参,以避免SQL执行报错 结束

        logger.info("大额风险暴露：大额风险暴露的配置表备份至历史表开始,请求参数为:[{}]", openDay);
        int insertDeRiskIndexCfgHis = cmis0230Mapper.insertDeRiskIndexCfgHis(openDay);
        logger.info("大额风险暴露：大额风险暴露的配置表备份至历史表结束,返回参数为:[{}]", insertDeRiskIndexCfgHis);

        logger.info("清空额度库中集团客户与成员关系表开始,请求参数为:[{}]", openDay);
        int deleteLmtCusGrpMemberRel = cmis0230Mapper.deleteLmtCusGrpMemberRel(openDay);
        logger.info("清空额度库中集团客户与成员关系表结束,返回参数为:[{}]", deleteLmtCusGrpMemberRel);

        logger.info("插入额度库中集团客户与成员关系表开始,请求参数为:[{}]", openDay);
        int insertLmtCusGrpMemberRel = cmis0230Mapper.insertLmtCusGrpMemberRel(openDay);
        logger.info("插入额度库中集团客户与成员关系表结束,返回参数为:[{}]", insertLmtCusGrpMemberRel);

        logger.info("清空额度库中集团客户信息表开始,请求参数为:[{}]", openDay);
        int deleteLmtCusGrp = cmis0230Mapper.deleteLmtCusGrp(openDay);
        logger.info("清空额度库中集团客户信息表结束,返回参数为:[{}]", deleteLmtCusGrp);

        logger.info("插入额度库中集团客户信息表开始,请求参数为:[{}]", openDay);
        int insertLmtCusGrp = cmis0230Mapper.insertLmtCusGrp(openDay);
        logger.info("插入额度库中集团客户信息表结束,返回参数为:[{}]", insertLmtCusGrp);

        logger.info("清空批复主信息及批复分项信息表A步骤开始,请求参数为:[{}]", openDay);
        int deleteTmpTdcarAlA = cmis0230Mapper.deleteTmpTdcarAlA(openDay);
        logger.info("清空批复主信息及批复分项信息表A步骤结束,返回参数为:[{}]", deleteTmpTdcarAlA);

        logger.info("插入批复主信息及批复分项信息表A步骤开始,请求参数为:[{}]", openDay);
        int insertTmpTdcarAlA = cmis0230Mapper.insertTmpTdcarAlA(openDay);
        logger.info("插入批复主信息及批复分项信息表A步骤结束,返回参数为:[{}]", insertTmpTdcarAlA);

        logger.info("将最新的集团客户编号同步至批复主信息及批复分项信息表中 A步骤开始,请求参数为:[{}]", openDay);
        int updateTmpTdcarAlA = cmis0230Mapper.updateTmpTdcarAlA(openDay);
        logger.info("将最新的集团客户编号同步至批复主信息及批复分项信息表中 A步骤结束,返回参数为:[{}]", updateTmpTdcarAlA);


        logger.info("清空批复主信息及批复分项信息表B步骤开始,请求参数为:[{}]", openDay);
        int deleteTmpTdcarAlB = cmis0230Mapper.deleteTmpTdcarAlB(openDay);
        logger.info("清空批复主信息及批复分项信息表B步骤结束,返回参数为:[{}]", deleteTmpTdcarAlB);

        logger.info("插入批复主信息及批复分项信息表B步骤开始,请求参数为:[{}]", openDay);
        int insertTmpTdcarAlB = cmis0230Mapper.insertTmpTdcarAlB(openDay);
        logger.info("插入批复主信息及批复分项信息表B步骤结束,返回参数为:[{}]", insertTmpTdcarAlB);

        logger.info("将最新的集团客户编号同步至批复主信息及批复分项信息表中 B步骤开始,请求参数为:[{}]", openDay);
        int updateTmpTdcarAlB = cmis0230Mapper.updateTmpTdcarAlB(openDay);
        logger.info("将最新的集团客户编号同步至批复主信息及批复分项信息表中 B步骤结束,返回参数为:[{}]", updateTmpTdcarAlB);
    }
}
