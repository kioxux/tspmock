package cn.com.yusys.yusp.batch.service.bak;

import cn.com.yusys.yusp.batch.repository.mapper.bak.BakD0009Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：BAKD0009</br>
 * 任务名称：批前备份日表任务-备份合作方授信分项信息[APPR_COOP_SUB_INFO]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
@Service
@Transactional
public class BakD0009Service {
    private static final Logger logger = LoggerFactory.getLogger(BakD0009Service.class);

    @Autowired
    private BakD0009Mapper bakD0009Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 备份当天的数据
     *
     * @param openDay
     */
    public void bakD0009InsertCurrent(String openDay) {
        // 备份当天的数据
        logger.info("备份当天的[合作方授信分项信息[APPR_COOP_SUB_INFO]]数据开始,请求参数为:[{}]", openDay);
        int insertCurrent = bakD0009Mapper.insertCurrent(openDay);
        logger.info("备份当天的[合作方授信分项信息[APPR_COOP_SUB_INFO]]数据结束,返回参数为:[{}]", insertCurrent);
    }

    /**
     * 删除当天的数据
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void bakD0009DeleteCurrent(String openDay) {
        // 删除当天的数据
        logger.info("删除当天的[合作方授信分项信息[APPR_COOP_SUB_INFO]]数据开始,请求参数为:[{}]", openDay);
        // bakD0009Mapper.truncateCurrent();// cmis_lmt.TMP_D_APPR_COOP_SUB_INFO
        tableUtilsService.renameCreateDropTable("cmis_lmt", "TMP_D_APPR_COOP_SUB_INFO");// 20211030 调整为调用重命名创建和删除相关表的公共方法
        logger.info("删除当天的[合作方授信分项信息[APPR_COOP_SUB_INFO]]数据结束");

//        logger.info("查询待删除当天的[合作方授信分项信息[APPR_COOP_SUB_INFO]]数据总次数开始,请求参数为:[{}]", openDay);
//        int queryD0009DeleteOpenDayAccCoopSubInfoCounts = bakD0009Mapper.queryD0009DeleteOpenDayAccCoopSubInfoCounts(openDay);
//        // 由于TDSQL不支持ceiling函数，此处在java中处理
//        BigDecimal times = new BigDecimal(queryD0009DeleteOpenDayAccCoopSubInfoCounts).divide(new BigDecimal(10000L), RoundingMode.CEILING);
//        queryD0009DeleteOpenDayAccCoopSubInfoCounts = times.intValue();
//        logger.info("查询待删除当天的[合作方授信分项信息[APPR_COOP_SUB_INFO]]数据总次数结束,返回参数为:[{}]", queryD0009DeleteOpenDayAccCoopSubInfoCounts);
//        // 删除当天的数据
//        for (int i = 0; i <= queryD0009DeleteOpenDayAccCoopSubInfoCounts; i++) {
//            // 删除当天的数据
//            logger.info("第[" + i + "]次删除当天的[合作方授信分项信息[APPR_COOP_SUB_INFO]]数据开始,请求参数为:[{}]", openDay);
//            int deleteCurrent = bakD0009Mapper.deleteCurrent(openDay);
//            logger.info("第[" + i + "]次删除当天的[合作方授信分项信息[APPR_COOP_SUB_INFO]]数据结束,返回参数为:[{}]", deleteCurrent);
//        }
    }


    /**
     * 校验备份表和原表数据是否一致
     *
     * @param openDay
     */
    public void checkBakDEqualsOriginal(String openDay) {
        logger.info("查询当天备份的[合作方授信分项信息[TMP_D_APPR_COOP_SUB_INFO]]数据总条数开始,请求参数为:[{}]", openDay);
        int bakDCounts = bakD0009Mapper.queryD0009DeleteOpenDayAccCoopSubInfoCounts(openDay);
        logger.info("查询当天备份的[合作方授信分项信息[TMP_D_APPR_COOP_SUB_INFO]]数据总条数结束,返回参数为:[{}]", bakDCounts);

        logger.info("查询当天的[合作方授信分项信息[APPR_COOP_SUB_INFO]]原表数据总条数开始,请求参数为:[{}]", openDay);
        int originalCounts = bakD0009Mapper.queryApprCoopSubInfoCounts(openDay);
        logger.info("查询当天的[合作方授信分项信息[APPR_COOP_SUB_INFO]]原表数据总条数结束,返回参数为:[{}]", originalCounts);

        if (bakDCounts != originalCounts) {
            logger.error("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者不相等，请检查！", openDay, bakDCounts, originalCounts);
            throw BizException.error(null, EchEnum.ECH080015.key, "任务日期为:[" + openDay + "],备份表中总条数为:[" + bakDCounts + "],原表中总条数为:[" + originalCounts + "],两者不相等，请检查！");
        } else {
            logger.info("任务日期为:[{}],备份表中总条数为:[{}],原表中总条数为:[{}],两者相等！", openDay, bakDCounts, originalCounts);
        }
    }
}
