package cn.com.yusys.yusp.batch.service.server.cmisbatch0008;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0008.Cmisbatch0008ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0008.Cmisbatch0008RespDto;
import cn.com.yusys.yusp.batch.repository.mapper.server.cmisbatch0008.CmisBatch0008Mapper;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import com.alibaba.fastjson.JSON;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * 接口处理Service:任务加急初始化数据
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class CmisBatch0008Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0008Service.class);

    @Resource
    private CmisBatch0008Mapper cmisBatch0008Mapper;

    /**
     * 交易码：cmisbatch0008
     * 交易描述：任务加急初始化数据
     *
     * @param cmisbatch0008ReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {Exception.class})
    public Cmisbatch0008RespDto cmisBatch0008(Cmisbatch0008ReqDto cmisbatch0008ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0008.key, DscmsEnum.TRADE_CODE_CMISBATCH0008.value, JSON.toJSONString(cmisbatch0008ReqDto));
        Cmisbatch0008RespDto cmisbatch0008RespDto = new Cmisbatch0008RespDto();//
        try {
            String dataDate = Optional.ofNullable(cmisbatch0008ReqDto.getUpdPeriod()).orElse(Strings.EMPTY);
//            logger.info("清空任务加急表开始,请求参数为:[{}]", dataDate);
//            cmisBatch0008Mapper.truncateTaskUrgentDetailInfo();
//            logger.info("清空任务加急表结束");

            logger.info("更新历史结余，历史结余=上期剩余,请求参数为:[{}]", dataDate);
            int updateTaskUrgentDetailInfo04 = cmisBatch0008Mapper.updateTaskUrgentDetailInfo04(dataDate);
            logger.info("更新历史结余，历史结余=上期剩余，返回参数为:[{}]", updateTaskUrgentDetailInfo04);

            logger.info("插入任务加急表开始,请求参数为:[{}]", dataDate);
            cmisBatch0008Mapper.insertTaskUrgentDetailInfo(dataDate);
            logger.info("插入任务加急表结束");

            logger.info("更新任务加急表,计算本次新增笔数开始,请求参数为:[{}]", dataDate);
            int updateTaskUrgentDetailInfo01 = cmisBatch0008Mapper.updateTaskUrgentDetailInfo01(dataDate);
            logger.info("更新任务加急表,计算本次新增笔数结束,返回参数为:[{}]", updateTaskUrgentDetailInfo01);

            logger.info("更新任务加急表,计算总优先笔数：历史结余笔数+本次新增笔数开始,请求参数为:[{}]", dataDate);
            int updateTaskUrgentDetailInfo02 = cmisBatch0008Mapper.updateTaskUrgentDetailInfo02(dataDate);
            logger.info("更新任务加急表,计算总优先笔数：历史结余笔数+本次新增笔数结束,返回参数为:[{}]", updateTaskUrgentDetailInfo02);

            logger.info("更新任务加急表,计算当前剩余笔数等于总优先笔数开始,请求参数为:[{}]", dataDate);
            int updateTaskUrgentDetailInfo03 = cmisBatch0008Mapper.updateTaskUrgentDetailInfo03(dataDate);
            logger.info("更新任务加急表,计算当前剩余笔数等于总优先笔数结束,返回参数为:[{}]", updateTaskUrgentDetailInfo03);

            cmisbatch0008RespDto.setOpFlag(CmisCommonConstants.OP_FLAG_S);
            cmisbatch0008RespDto.setOpMessage(CmisCommonConstants.OP_MSG_S);
        } catch (Exception e) {
            cmisbatch0008RespDto.setOpFlag(CmisCommonConstants.OP_FLAG_F);
            cmisbatch0008RespDto.setOpMessage(CmisCommonConstants.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0008.key, DscmsEnum.TRADE_CODE_CMISBATCH0008.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0008.key, DscmsEnum.TRADE_CODE_CMISBATCH0008.value);

        return cmisbatch0008RespDto;
    }
}