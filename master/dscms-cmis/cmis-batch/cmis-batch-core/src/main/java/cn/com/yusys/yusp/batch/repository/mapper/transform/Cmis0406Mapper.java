package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0406</br>
 * 任务名称：加工任务-贷后管理-首次检查[贷后临时表数据回插]  </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年09月17日 下午2:56:22
 */
public interface Cmis0406Mapper {

    /**
     * 插入贷后检查任务表
     * @param openDay
     * @return
     */
    int insertPspTaskList(@Param("openDay") String openDay);
}
