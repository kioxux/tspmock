/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.ris;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.ris.BatSRisKhfxJgbx;
import cn.com.yusys.yusp.batch.service.load.ris.BatSRisKhfxJgbxService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRisKhfxJgbxResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-25 16:59:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsriskhfxjgbx")
public class BatSRisKhfxJgbxResource {
    @Autowired
    private BatSRisKhfxJgbxService batSRisKhfxJgbxService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSRisKhfxJgbx>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSRisKhfxJgbx> list = batSRisKhfxJgbxService.selectAll(queryModel);
        return new ResultDto<List<BatSRisKhfxJgbx>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSRisKhfxJgbx>> index(QueryModel queryModel) {
        List<BatSRisKhfxJgbx> list = batSRisKhfxJgbxService.selectByModel(queryModel);
        return new ResultDto<List<BatSRisKhfxJgbx>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSRisKhfxJgbx> create(@RequestBody BatSRisKhfxJgbx batSRisKhfxJgbx) throws URISyntaxException {
        batSRisKhfxJgbxService.insert(batSRisKhfxJgbx);
        return new ResultDto<BatSRisKhfxJgbx>(batSRisKhfxJgbx);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSRisKhfxJgbx batSRisKhfxJgbx) throws URISyntaxException {
        int result = batSRisKhfxJgbxService.update(batSRisKhfxJgbx);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String dataDt, String custTypeId, String custId, String custName) {
        int result = batSRisKhfxJgbxService.deleteByPrimaryKey(dataDt, custTypeId, custId, custName);
        return new ResultDto<Integer>(result);
    }

}
