/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.djk;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSDjkDataAccount
 * @类描述: bat_s_djk_data_account数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-07 15:41:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_djk_data_account")
public class BatSDjkDataAccount extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 账户编号 **/
	@Id
	@Column(name = "ACCT_NO")
	private String acctNo;
	
	/** 客户编号 **/
	@Id
	@Column(name = "CUST_ID")
	private String custId;
	
	/** 产品代码 **/
	@Column(name = "PRODUCT_CD", unique = false, nullable = true, length = 6)
	private String productCd;
	
	/** 币种 **/
	@Column(name = "CURR_CD", unique = false, nullable = true, length = 3)
	private String currCd;
	
	/** 信用额度 **/
	@Column(name = "CREDIT_LIMIT", unique = false, nullable = true, length = 13)
	private String creditLimit;
	
	/** 临时额度 **/
	@Column(name = "TEMP_LIMIT", unique = false, nullable = true, length = 13)
	private String tempLimit;
	
	/** 临时额度开始日期 **/
	@Column(name = "TEMP_LIMIT_BEGIN_DATE", unique = false, nullable = true, length = 8)
	private String tempLimitBeginDate;
	
	/** 临时额度结束日期 **/
	@Column(name = "TEMP_LIMIT_END_DATE", unique = false, nullable = true, length = 8)
	private String tempLimitEndDate;
	
	/** 取现额度比例 **/
	@Column(name = "CASH_LIMIT_RT", unique = false, nullable = true, length = 5)
	private String cashLimitRt;
	
	/** 授权超限比例 **/
	@Column(name = "OVRLMT_RATE", unique = false, nullable = true, length = 5)
	private String ovrlmtRate;
	
	/** 额度内分期额度比例 **/
	@Column(name = "LOAN_LIMIT_RT", unique = false, nullable = true, length = 5)
	private String loanLimitRt;
	
	/** 当前余额 **/
	@Column(name = "CURR_BAL", unique = false, nullable = true, length = 15)
	private String currBal;
	
	/** 取现余额 **/
	@Column(name = "CASH_BAL", unique = false, nullable = true, length = 15)
	private String cashBal;
	
	/** 本金余额 **/
	@Column(name = "PRINCIPAL_BAL", unique = false, nullable = true, length = 15)
	private String principalBal;
	
	/** 额度内分期余额 **/
	@Column(name = "LOAN_BAL", unique = false, nullable = true, length = 15)
	private String loanBal;
	
	/** 全部应还款额 **/
	@Column(name = "QUAL_GRACE_BAL", unique = false, nullable = true, length = 15)
	private String qualGraceBal;
	
	/** 积分余额 **/
	@Column(name = "POINT_BAL", unique = false, nullable = true, length = 13)
	private String pointBal;
	
	/** 创建日期 **/
	@Column(name = "SETUP_DATE", unique = false, nullable = true, length = 8)
	private String setupDate;
	
	/** 账单周期 **/
	@Column(name = "BILLING_CYCLE", unique = false, nullable = true, length = 2)
	private String billingCycle;
	
	/** 寄送账单标志 **/
	@Column(name = "STMT_FLAG", unique = false, nullable = true, length = 1)
	private String stmtFlag;
	
	/** 账单寄送地址标志 **/
	@Column(name = "STMT_MAIL_ADDR_IND", unique = false, nullable = true, length = 1)
	private String stmtMailAddrInd;
	
	/** 账单介质类型 **/
	@Column(name = "STMT_MEDIA_TYPE", unique = false, nullable = true, length = 1)
	private String stmtMediaType;
	
	/** 锁定码 **/
	@Column(name = "BLOCK_CODE", unique = false, nullable = true, length = 27)
	private String blockCode;
	
	/** 账龄 **/
	@Column(name = "AGE_CD", unique = false, nullable = true, length = 1)
	private String ageCd;
	
	/** 约定还款类型 **/
	@Column(name = "DD_IND", unique = false, nullable = true, length = 1)
	private String ddInd;
	
	/** 约定还款银行名称 **/
	@Column(name = "DD_BANK_NAME", unique = false, nullable = true, length = 80)
	private String ddBankName;
	
	/** 约定还款开户行号 **/
	@Column(name = "DD_BANK_BRANCH", unique = false, nullable = true, length = 9)
	private String ddBankBranch;
	
	/** 约定还款扣款账号 **/
	@Column(name = "DD_BANK_ACCT_NO", unique = false, nullable = true, length = 40)
	private String ddBankAcctNo;
	
	/** 约定还款扣款账户姓名 **/
	@Column(name = "DD_BANK_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String ddBankAcctName;
	
	/** 最终销户日期 **/
	@Column(name = "CLOSED_DATE", unique = false, nullable = true, length = 8)
	private String closedDate;
	
	/** 预销户日期 **/
	@Column(name = "CANCEL_DATE", unique = false, nullable = true, length = 8)
	private String cancelDate;
	
	/** 最小还款额 **/
	@Column(name = "TOT_DUE_AMT", unique = false, nullable = true, length = 15)
	private String totDueAmt;
	
	/** 还款历史信息 **/
	@Column(name = "PAYMENT_HIST", unique = false, nullable = true, length = 24)
	private String paymentHist;
	
	/** 上期还款金额 **/
	@Column(name = "CTD_PAYMENT_AMT", unique = false, nullable = true, length = 15)
	private String ctdPaymentAmt;
	
	/** 客户最后交易日期 **/
	@Column(name = "LAST_TRANS_DATE", unique = false, nullable = true, length = 8)
	private String lastTransDate;
	
	/** 本月实际还款金额 **/
	@Column(name = "ACTUAL_PAYMENT_AMT", unique = false, nullable = true, length = 15)
	private String actualPaymentAmt;
	
	/** 核销金额 **/
	@Column(name = "CHARGE_OFF_AMT", unique = false, nullable = true, length = 15)
	private String chargeOffAmt;
	
	/** 核销日期 **/
	@Column(name = "CHARGE_OFF_DATE", unique = false, nullable = true, length = 8)
	private String chargeOffDate;
	
	/** 未出账单余额 **/
	@Column(name = "UNSTMT_BAL", unique = false, nullable = true, length = 15)
	private String unstmtBal;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 300)
	private String remark;
	
	/** 当年消费金额 **/
	@Column(name = "YTD_RETAIL_AMT", unique = false, nullable = true, length = 15)
	private String ytdRetailAmt;
	
	/** 入催次数 **/
	@Column(name = "COLLECT_TIMES", unique = false, nullable = true, length = 2)
	private String collectTimes;
	
	/** 入催原因 **/
	@Column(name = "COLLECT_REASON", unique = false, nullable = true, length = 2)
	private String collectReason;
	
	/** 账户类型 **/
	@Column(name = "ACCT_TYPE", unique = false, nullable = true, length = 1)
	private String acctType;
	
	/** 行内客户号 **/
	@Column(name = "BANK_CUSTOMER_ID", unique = false, nullable = true, length = 20)
	private String bankCustomerId;
	
	/** 当期消费本金余额 **/
	@Column(name = "CTD_REAIL_PRIN_BAL", unique = false, nullable = true, length = 15)
	private String ctdReailPrinBal;
	
	/** 已出账单消费本金余额 **/
	@Column(name = "STMT_REAIL_PRIN_BAL", unique = false, nullable = true, length = 15)
	private String stmtReailPrinBal;
	
	/** 当期取现本金余额 **/
	@Column(name = "CTD_CASH_PRIN_BAL", unique = false, nullable = true, length = 15)
	private String ctdCashPrinBal;
	
	/** 已出账单取现本金余额 **/
	@Column(name = "STMT_CASH_PRIN_BAL", unique = false, nullable = true, length = 15)
	private String stmtCashPrinBal;
	
	/** 当期分期应还本金余额 **/
	@Column(name = "CTD_LOAN_PRIN_BAL", unique = false, nullable = true, length = 15)
	private String ctdLoanPrinBal;
	
	/** 已出账单分期应还本金余额 **/
	@Column(name = "STMT_LOAN_PRIN_BAL", unique = false, nullable = true, length = 15)
	private String stmtLoanPrinBal;
	
	/** 额度外分期余额 **/
	@Column(name = "LARGE_LOAN_BAL_XFROUT", unique = false, nullable = true, length = 15)
	private String largeLoanBalXfrout;
	
	/** 溢缴款 **/
	@Column(name = "DEPOSIT_BAL", unique = false, nullable = true, length = 15)
	private String depositBal;
	
	/** 当期分期交易费 **/
	@Column(name = "CTD_LOAN_TXN_FEE", unique = false, nullable = true, length = 15)
	private String ctdLoanTxnFee;
	
	/** 已出账单分期交易费 **/
	@Column(name = "STMT_LOAN_TXN_FEE", unique = false, nullable = true, length = 15)
	private String stmtLoanTxnFee;
	
	/** 当期取现交易费 **/
	@Column(name = "CTD_CASH_TXN_FEE", unique = false, nullable = true, length = 15)
	private String ctdCashTxnFee;
	
	/** 已出账单取现交易费 **/
	@Column(name = "STMT_CASH_TXN_FEE", unique = false, nullable = true, length = 15)
	private String stmtCashTxnFee;
	
	/** 当期其他交易费 **/
	@Column(name = "CTD_OTHER_TXN_FEE", unique = false, nullable = true, length = 15)
	private String ctdOtherTxnFee;
	
	/** 已出账单其他交易费 **/
	@Column(name = "STMT_OTHER_TXN_FEE", unique = false, nullable = true, length = 15)
	private String stmtOtherTxnFee;
	
	/** 当期年费 **/
	@Column(name = "CTD_ANUAL_FEE", unique = false, nullable = true, length = 15)
	private String ctdAnualFee;
	
	/** 已出账单年费 **/
	@Column(name = "STMT_ANUAL_FEE", unique = false, nullable = true, length = 15)
	private String stmtAnualFee;
	
	/** 当期违约费 **/
	@Column(name = "CTD_LATE_CHARGE_FEE", unique = false, nullable = true, length = 15)
	private String ctdLateChargeFee;
	
	/** 已出账单违约费 **/
	@Column(name = "STMT_LATE_CHARGE_FEE", unique = false, nullable = true, length = 15)
	private String stmtLateChargeFee;
	
	/** 当期服务费 **/
	@Column(name = "CTD_SERVICE_FEE", unique = false, nullable = true, length = 15)
	private String ctdServiceFee;
	
	/** 已出账单服务费 **/
	@Column(name = "STMT_SERVICE_FEE", unique = false, nullable = true, length = 15)
	private String stmtServiceFee;
	
	/** 当期利息 **/
	@Column(name = "CTD_INT", unique = false, nullable = true, length = 15)
	private String ctdInt;
	
	/** 已出账单利息 **/
	@Column(name = "STMT_INT", unique = false, nullable = true, length = 15)
	private String stmtInt;
	
	/** 发卡网点 **/
	@Column(name = "OWNING_BRANCH", unique = false, nullable = true, length = 9)
	private String owningBranch;
	
	/** 未匹配借记金额 **/
	@Column(name = "UNMATCH_DB_AMT", unique = false, nullable = true, length = 15)
	private String unmatchDbAmt;
	
	/** 未匹配贷记金额 **/
	@Column(name = "UNMATCH_CR_AMT", unique = false, nullable = true, length = 15)
	private String unmatchCrAmt;
	
	/** 催收记录类型 **/
	@Column(name = "COLL_REC_TYPE", unique = false, nullable = true, length = 1)
	private String collRecType;
	
	/** 进入催收日期 **/
	@Column(name = "COLLECT_DATE", unique = false, nullable = true, length = 8)
	private String collectDate;
	
	/** 满意日期 **/
	@Column(name = "SANTISFIED_DATE", unique = false, nullable = true, length = 8)
	private String santisfiedDate;
	
	/** 超限金额 **/
	@Column(name = "OVERLIMIT_AMT", unique = false, nullable = true, length = 15)
	private String overlimitAmt;
	
	/** 有无分期账户 **/
	@Column(name = "LOAN_IND", unique = false, nullable = true, length = 1)
	private String loanInd;
	
	/** 上一账单日 **/
	@Column(name = "LAST_STMT_DATE", unique = false, nullable = true, length = 8)
	private String lastStmtDate;
	
	/** 上期到期还款日 **/
	@Column(name = "LAST_DUE_DATE", unique = false, nullable = true, length = 8)
	private String lastDueDate;
	
	/** 当期欠款余额 **/
	@Column(name = "REMAIN_GRACE_BAL", unique = false, nullable = true, length = 15)
	private String remainGraceBal;
	
	/** 30 天以下欠款 **/
	@Column(name = "PAST_DUE_AMT_1", unique = false, nullable = true, length = 15)
	private String pastDueAmt1;
	
	/** 30-59 天欠款 **/
	@Column(name = "PAST_DUE_AMT_2", unique = false, nullable = true, length = 15)
	private String pastDueAmt2;
	
	/** 60-89 天欠款 **/
	@Column(name = "PAST_DUE_AMT_3", unique = false, nullable = true, length = 15)
	private String pastDueAmt3;
	
	/** 90-119 天欠款 **/
	@Column(name = "PAST_DUE_AMT_4", unique = false, nullable = true, length = 15)
	private String pastDueAmt4;
	
	/** 120-149 天欠款 **/
	@Column(name = "PAST_DUE_AMT_5", unique = false, nullable = true, length = 15)
	private String pastDueAmt5;
	
	/** 150-179 天欠款 **/
	@Column(name = "PAST_DUE_AMT_6", unique = false, nullable = true, length = 15)
	private String pastDueAmt6;
	
	/** 180 天及以上欠款 **/
	@Column(name = "PAST_DUE_AMT_7", unique = false, nullable = true, length = 15)
	private String pastDueAmt7;
	
	/** 24 个月账龄历史信息 **/
	@Column(name = "AGE_HIST", unique = false, nullable = true, length = 24)
	private String ageHist;
	
	/** 本周期取现笔数 **/
	@Column(name = "CTD_CASH_CNT", unique = false, nullable = true, length = 6)
	private String ctdCashCnt;
	
	/** 约定还款他行代扣标识 **/
	@Column(name = "DD_OTHER_BANK_IND", unique = false, nullable = true, length = 1)
	private String ddOtherBankInd;
	
	/** 默认逻辑卡号 **/
	@Column(name = "DEFAULT_LOGICAL_CARD_NO", unique = false, nullable = true, length = 19)
	private String defaultLogicalCardNo;
	
	/** 消费利息 **/
	@Column(name = "RETAIL_INT", unique = false, nullable = true, length = 15)
	private String retailInt;
	
	/** 取现利息 **/
	@Column(name = "CASH_INT", unique = false, nullable = true, length = 15)
	private String cashInt;
	
	/** 分期利息 **/
	@Column(name = "LOAN_INT", unique = false, nullable = true, length = 15)
	private String loanInt;
	
	/** 生息的应收账款余额 **/
	@Column(name = "PENDING_INT_BAL", unique = false, nullable = true, length = 15)
	private String pendingIntBal;
	
	/** 约定还款开户行行号 2 **/
	@Column(name = "DD_BANK_BRANCH_2", unique = false, nullable = true, length = 20)
	private String ddBankBranch2;
	
	/** 是否全额还款 **/
	@Column(name = "GRACE_DAYS_FULL_IND", unique = false, nullable = true, length = 1)
	private String graceDaysFullInd;
	
	/** 核算分行 **/
	@Column(name = "GLP_BRANCH", unique = false, nullable = true, length = 9)
	private String glpBranch;
	
	/** 账户开立时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 14)
	private String createTime;
	
	/** 最近一次维护日期 **/
	@Column(name = "LAST_MODIFIED_TIME", unique = false, nullable = true, length = 14)
	private String lastModifiedTime;
	
	/** 预留字段 **/
	@Column(name = "FILLER", unique = false, nullable = true, length = 62)
	private String filler;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param productCd
	 */
	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}
	
    /**
     * @return productCd
     */
	public String getProductCd() {
		return this.productCd;
	}
	
	/**
	 * @param currCd
	 */
	public void setCurrCd(String currCd) {
		this.currCd = currCd;
	}
	
    /**
     * @return currCd
     */
	public String getCurrCd() {
		return this.currCd;
	}
	
	/**
	 * @param creditLimit
	 */
	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}
	
    /**
     * @return creditLimit
     */
	public String getCreditLimit() {
		return this.creditLimit;
	}
	
	/**
	 * @param tempLimit
	 */
	public void setTempLimit(String tempLimit) {
		this.tempLimit = tempLimit;
	}
	
    /**
     * @return tempLimit
     */
	public String getTempLimit() {
		return this.tempLimit;
	}
	
	/**
	 * @param tempLimitBeginDate
	 */
	public void setTempLimitBeginDate(String tempLimitBeginDate) {
		this.tempLimitBeginDate = tempLimitBeginDate;
	}
	
    /**
     * @return tempLimitBeginDate
     */
	public String getTempLimitBeginDate() {
		return this.tempLimitBeginDate;
	}
	
	/**
	 * @param tempLimitEndDate
	 */
	public void setTempLimitEndDate(String tempLimitEndDate) {
		this.tempLimitEndDate = tempLimitEndDate;
	}
	
    /**
     * @return tempLimitEndDate
     */
	public String getTempLimitEndDate() {
		return this.tempLimitEndDate;
	}
	
	/**
	 * @param cashLimitRt
	 */
	public void setCashLimitRt(String cashLimitRt) {
		this.cashLimitRt = cashLimitRt;
	}
	
    /**
     * @return cashLimitRt
     */
	public String getCashLimitRt() {
		return this.cashLimitRt;
	}
	
	/**
	 * @param ovrlmtRate
	 */
	public void setOvrlmtRate(String ovrlmtRate) {
		this.ovrlmtRate = ovrlmtRate;
	}
	
    /**
     * @return ovrlmtRate
     */
	public String getOvrlmtRate() {
		return this.ovrlmtRate;
	}
	
	/**
	 * @param loanLimitRt
	 */
	public void setLoanLimitRt(String loanLimitRt) {
		this.loanLimitRt = loanLimitRt;
	}
	
    /**
     * @return loanLimitRt
     */
	public String getLoanLimitRt() {
		return this.loanLimitRt;
	}
	
	/**
	 * @param currBal
	 */
	public void setCurrBal(String currBal) {
		this.currBal = currBal;
	}
	
    /**
     * @return currBal
     */
	public String getCurrBal() {
		return this.currBal;
	}
	
	/**
	 * @param cashBal
	 */
	public void setCashBal(String cashBal) {
		this.cashBal = cashBal;
	}
	
    /**
     * @return cashBal
     */
	public String getCashBal() {
		return this.cashBal;
	}
	
	/**
	 * @param principalBal
	 */
	public void setPrincipalBal(String principalBal) {
		this.principalBal = principalBal;
	}
	
    /**
     * @return principalBal
     */
	public String getPrincipalBal() {
		return this.principalBal;
	}
	
	/**
	 * @param loanBal
	 */
	public void setLoanBal(String loanBal) {
		this.loanBal = loanBal;
	}
	
    /**
     * @return loanBal
     */
	public String getLoanBal() {
		return this.loanBal;
	}
	
	/**
	 * @param qualGraceBal
	 */
	public void setQualGraceBal(String qualGraceBal) {
		this.qualGraceBal = qualGraceBal;
	}
	
    /**
     * @return qualGraceBal
     */
	public String getQualGraceBal() {
		return this.qualGraceBal;
	}
	
	/**
	 * @param pointBal
	 */
	public void setPointBal(String pointBal) {
		this.pointBal = pointBal;
	}
	
    /**
     * @return pointBal
     */
	public String getPointBal() {
		return this.pointBal;
	}
	
	/**
	 * @param setupDate
	 */
	public void setSetupDate(String setupDate) {
		this.setupDate = setupDate;
	}
	
    /**
     * @return setupDate
     */
	public String getSetupDate() {
		return this.setupDate;
	}
	
	/**
	 * @param billingCycle
	 */
	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}
	
    /**
     * @return billingCycle
     */
	public String getBillingCycle() {
		return this.billingCycle;
	}
	
	/**
	 * @param stmtFlag
	 */
	public void setStmtFlag(String stmtFlag) {
		this.stmtFlag = stmtFlag;
	}
	
    /**
     * @return stmtFlag
     */
	public String getStmtFlag() {
		return this.stmtFlag;
	}
	
	/**
	 * @param stmtMailAddrInd
	 */
	public void setStmtMailAddrInd(String stmtMailAddrInd) {
		this.stmtMailAddrInd = stmtMailAddrInd;
	}
	
    /**
     * @return stmtMailAddrInd
     */
	public String getStmtMailAddrInd() {
		return this.stmtMailAddrInd;
	}
	
	/**
	 * @param stmtMediaType
	 */
	public void setStmtMediaType(String stmtMediaType) {
		this.stmtMediaType = stmtMediaType;
	}
	
    /**
     * @return stmtMediaType
     */
	public String getStmtMediaType() {
		return this.stmtMediaType;
	}
	
	/**
	 * @param blockCode
	 */
	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}
	
    /**
     * @return blockCode
     */
	public String getBlockCode() {
		return this.blockCode;
	}
	
	/**
	 * @param ageCd
	 */
	public void setAgeCd(String ageCd) {
		this.ageCd = ageCd;
	}
	
    /**
     * @return ageCd
     */
	public String getAgeCd() {
		return this.ageCd;
	}
	
	/**
	 * @param ddInd
	 */
	public void setDdInd(String ddInd) {
		this.ddInd = ddInd;
	}
	
    /**
     * @return ddInd
     */
	public String getDdInd() {
		return this.ddInd;
	}
	
	/**
	 * @param ddBankName
	 */
	public void setDdBankName(String ddBankName) {
		this.ddBankName = ddBankName;
	}
	
    /**
     * @return ddBankName
     */
	public String getDdBankName() {
		return this.ddBankName;
	}
	
	/**
	 * @param ddBankBranch
	 */
	public void setDdBankBranch(String ddBankBranch) {
		this.ddBankBranch = ddBankBranch;
	}
	
    /**
     * @return ddBankBranch
     */
	public String getDdBankBranch() {
		return this.ddBankBranch;
	}
	
	/**
	 * @param ddBankAcctNo
	 */
	public void setDdBankAcctNo(String ddBankAcctNo) {
		this.ddBankAcctNo = ddBankAcctNo;
	}
	
    /**
     * @return ddBankAcctNo
     */
	public String getDdBankAcctNo() {
		return this.ddBankAcctNo;
	}
	
	/**
	 * @param ddBankAcctName
	 */
	public void setDdBankAcctName(String ddBankAcctName) {
		this.ddBankAcctName = ddBankAcctName;
	}
	
    /**
     * @return ddBankAcctName
     */
	public String getDdBankAcctName() {
		return this.ddBankAcctName;
	}
	
	/**
	 * @param closedDate
	 */
	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}
	
    /**
     * @return closedDate
     */
	public String getClosedDate() {
		return this.closedDate;
	}
	
	/**
	 * @param cancelDate
	 */
	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}
	
    /**
     * @return cancelDate
     */
	public String getCancelDate() {
		return this.cancelDate;
	}
	
	/**
	 * @param totDueAmt
	 */
	public void setTotDueAmt(String totDueAmt) {
		this.totDueAmt = totDueAmt;
	}
	
    /**
     * @return totDueAmt
     */
	public String getTotDueAmt() {
		return this.totDueAmt;
	}
	
	/**
	 * @param paymentHist
	 */
	public void setPaymentHist(String paymentHist) {
		this.paymentHist = paymentHist;
	}
	
    /**
     * @return paymentHist
     */
	public String getPaymentHist() {
		return this.paymentHist;
	}
	
	/**
	 * @param ctdPaymentAmt
	 */
	public void setCtdPaymentAmt(String ctdPaymentAmt) {
		this.ctdPaymentAmt = ctdPaymentAmt;
	}
	
    /**
     * @return ctdPaymentAmt
     */
	public String getCtdPaymentAmt() {
		return this.ctdPaymentAmt;
	}
	
	/**
	 * @param lastTransDate
	 */
	public void setLastTransDate(String lastTransDate) {
		this.lastTransDate = lastTransDate;
	}
	
    /**
     * @return lastTransDate
     */
	public String getLastTransDate() {
		return this.lastTransDate;
	}
	
	/**
	 * @param actualPaymentAmt
	 */
	public void setActualPaymentAmt(String actualPaymentAmt) {
		this.actualPaymentAmt = actualPaymentAmt;
	}
	
    /**
     * @return actualPaymentAmt
     */
	public String getActualPaymentAmt() {
		return this.actualPaymentAmt;
	}
	
	/**
	 * @param chargeOffAmt
	 */
	public void setChargeOffAmt(String chargeOffAmt) {
		this.chargeOffAmt = chargeOffAmt;
	}
	
    /**
     * @return chargeOffAmt
     */
	public String getChargeOffAmt() {
		return this.chargeOffAmt;
	}
	
	/**
	 * @param chargeOffDate
	 */
	public void setChargeOffDate(String chargeOffDate) {
		this.chargeOffDate = chargeOffDate;
	}
	
    /**
     * @return chargeOffDate
     */
	public String getChargeOffDate() {
		return this.chargeOffDate;
	}
	
	/**
	 * @param unstmtBal
	 */
	public void setUnstmtBal(String unstmtBal) {
		this.unstmtBal = unstmtBal;
	}
	
    /**
     * @return unstmtBal
     */
	public String getUnstmtBal() {
		return this.unstmtBal;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param ytdRetailAmt
	 */
	public void setYtdRetailAmt(String ytdRetailAmt) {
		this.ytdRetailAmt = ytdRetailAmt;
	}
	
    /**
     * @return ytdRetailAmt
     */
	public String getYtdRetailAmt() {
		return this.ytdRetailAmt;
	}
	
	/**
	 * @param collectTimes
	 */
	public void setCollectTimes(String collectTimes) {
		this.collectTimes = collectTimes;
	}
	
    /**
     * @return collectTimes
     */
	public String getCollectTimes() {
		return this.collectTimes;
	}
	
	/**
	 * @param collectReason
	 */
	public void setCollectReason(String collectReason) {
		this.collectReason = collectReason;
	}
	
    /**
     * @return collectReason
     */
	public String getCollectReason() {
		return this.collectReason;
	}
	
	/**
	 * @param acctType
	 */
	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}
	
    /**
     * @return acctType
     */
	public String getAcctType() {
		return this.acctType;
	}
	
	/**
	 * @param bankCustomerId
	 */
	public void setBankCustomerId(String bankCustomerId) {
		this.bankCustomerId = bankCustomerId;
	}
	
    /**
     * @return bankCustomerId
     */
	public String getBankCustomerId() {
		return this.bankCustomerId;
	}
	
	/**
	 * @param ctdReailPrinBal
	 */
	public void setCtdReailPrinBal(String ctdReailPrinBal) {
		this.ctdReailPrinBal = ctdReailPrinBal;
	}
	
    /**
     * @return ctdReailPrinBal
     */
	public String getCtdReailPrinBal() {
		return this.ctdReailPrinBal;
	}
	
	/**
	 * @param stmtReailPrinBal
	 */
	public void setStmtReailPrinBal(String stmtReailPrinBal) {
		this.stmtReailPrinBal = stmtReailPrinBal;
	}
	
    /**
     * @return stmtReailPrinBal
     */
	public String getStmtReailPrinBal() {
		return this.stmtReailPrinBal;
	}
	
	/**
	 * @param ctdCashPrinBal
	 */
	public void setCtdCashPrinBal(String ctdCashPrinBal) {
		this.ctdCashPrinBal = ctdCashPrinBal;
	}
	
    /**
     * @return ctdCashPrinBal
     */
	public String getCtdCashPrinBal() {
		return this.ctdCashPrinBal;
	}
	
	/**
	 * @param stmtCashPrinBal
	 */
	public void setStmtCashPrinBal(String stmtCashPrinBal) {
		this.stmtCashPrinBal = stmtCashPrinBal;
	}
	
    /**
     * @return stmtCashPrinBal
     */
	public String getStmtCashPrinBal() {
		return this.stmtCashPrinBal;
	}
	
	/**
	 * @param ctdLoanPrinBal
	 */
	public void setCtdLoanPrinBal(String ctdLoanPrinBal) {
		this.ctdLoanPrinBal = ctdLoanPrinBal;
	}
	
    /**
     * @return ctdLoanPrinBal
     */
	public String getCtdLoanPrinBal() {
		return this.ctdLoanPrinBal;
	}
	
	/**
	 * @param stmtLoanPrinBal
	 */
	public void setStmtLoanPrinBal(String stmtLoanPrinBal) {
		this.stmtLoanPrinBal = stmtLoanPrinBal;
	}
	
    /**
     * @return stmtLoanPrinBal
     */
	public String getStmtLoanPrinBal() {
		return this.stmtLoanPrinBal;
	}
	
	/**
	 * @param largeLoanBalXfrout
	 */
	public void setLargeLoanBalXfrout(String largeLoanBalXfrout) {
		this.largeLoanBalXfrout = largeLoanBalXfrout;
	}
	
    /**
     * @return largeLoanBalXfrout
     */
	public String getLargeLoanBalXfrout() {
		return this.largeLoanBalXfrout;
	}
	
	/**
	 * @param depositBal
	 */
	public void setDepositBal(String depositBal) {
		this.depositBal = depositBal;
	}
	
    /**
     * @return depositBal
     */
	public String getDepositBal() {
		return this.depositBal;
	}
	
	/**
	 * @param ctdLoanTxnFee
	 */
	public void setCtdLoanTxnFee(String ctdLoanTxnFee) {
		this.ctdLoanTxnFee = ctdLoanTxnFee;
	}
	
    /**
     * @return ctdLoanTxnFee
     */
	public String getCtdLoanTxnFee() {
		return this.ctdLoanTxnFee;
	}
	
	/**
	 * @param stmtLoanTxnFee
	 */
	public void setStmtLoanTxnFee(String stmtLoanTxnFee) {
		this.stmtLoanTxnFee = stmtLoanTxnFee;
	}
	
    /**
     * @return stmtLoanTxnFee
     */
	public String getStmtLoanTxnFee() {
		return this.stmtLoanTxnFee;
	}
	
	/**
	 * @param ctdCashTxnFee
	 */
	public void setCtdCashTxnFee(String ctdCashTxnFee) {
		this.ctdCashTxnFee = ctdCashTxnFee;
	}
	
    /**
     * @return ctdCashTxnFee
     */
	public String getCtdCashTxnFee() {
		return this.ctdCashTxnFee;
	}
	
	/**
	 * @param stmtCashTxnFee
	 */
	public void setStmtCashTxnFee(String stmtCashTxnFee) {
		this.stmtCashTxnFee = stmtCashTxnFee;
	}
	
    /**
     * @return stmtCashTxnFee
     */
	public String getStmtCashTxnFee() {
		return this.stmtCashTxnFee;
	}
	
	/**
	 * @param ctdOtherTxnFee
	 */
	public void setCtdOtherTxnFee(String ctdOtherTxnFee) {
		this.ctdOtherTxnFee = ctdOtherTxnFee;
	}
	
    /**
     * @return ctdOtherTxnFee
     */
	public String getCtdOtherTxnFee() {
		return this.ctdOtherTxnFee;
	}
	
	/**
	 * @param stmtOtherTxnFee
	 */
	public void setStmtOtherTxnFee(String stmtOtherTxnFee) {
		this.stmtOtherTxnFee = stmtOtherTxnFee;
	}
	
    /**
     * @return stmtOtherTxnFee
     */
	public String getStmtOtherTxnFee() {
		return this.stmtOtherTxnFee;
	}
	
	/**
	 * @param ctdAnualFee
	 */
	public void setCtdAnualFee(String ctdAnualFee) {
		this.ctdAnualFee = ctdAnualFee;
	}
	
    /**
     * @return ctdAnualFee
     */
	public String getCtdAnualFee() {
		return this.ctdAnualFee;
	}
	
	/**
	 * @param stmtAnualFee
	 */
	public void setStmtAnualFee(String stmtAnualFee) {
		this.stmtAnualFee = stmtAnualFee;
	}
	
    /**
     * @return stmtAnualFee
     */
	public String getStmtAnualFee() {
		return this.stmtAnualFee;
	}
	
	/**
	 * @param ctdLateChargeFee
	 */
	public void setCtdLateChargeFee(String ctdLateChargeFee) {
		this.ctdLateChargeFee = ctdLateChargeFee;
	}
	
    /**
     * @return ctdLateChargeFee
     */
	public String getCtdLateChargeFee() {
		return this.ctdLateChargeFee;
	}
	
	/**
	 * @param stmtLateChargeFee
	 */
	public void setStmtLateChargeFee(String stmtLateChargeFee) {
		this.stmtLateChargeFee = stmtLateChargeFee;
	}
	
    /**
     * @return stmtLateChargeFee
     */
	public String getStmtLateChargeFee() {
		return this.stmtLateChargeFee;
	}
	
	/**
	 * @param ctdServiceFee
	 */
	public void setCtdServiceFee(String ctdServiceFee) {
		this.ctdServiceFee = ctdServiceFee;
	}
	
    /**
     * @return ctdServiceFee
     */
	public String getCtdServiceFee() {
		return this.ctdServiceFee;
	}
	
	/**
	 * @param stmtServiceFee
	 */
	public void setStmtServiceFee(String stmtServiceFee) {
		this.stmtServiceFee = stmtServiceFee;
	}
	
    /**
     * @return stmtServiceFee
     */
	public String getStmtServiceFee() {
		return this.stmtServiceFee;
	}
	
	/**
	 * @param ctdInt
	 */
	public void setCtdInt(String ctdInt) {
		this.ctdInt = ctdInt;
	}
	
    /**
     * @return ctdInt
     */
	public String getCtdInt() {
		return this.ctdInt;
	}
	
	/**
	 * @param stmtInt
	 */
	public void setStmtInt(String stmtInt) {
		this.stmtInt = stmtInt;
	}
	
    /**
     * @return stmtInt
     */
	public String getStmtInt() {
		return this.stmtInt;
	}
	
	/**
	 * @param owningBranch
	 */
	public void setOwningBranch(String owningBranch) {
		this.owningBranch = owningBranch;
	}
	
    /**
     * @return owningBranch
     */
	public String getOwningBranch() {
		return this.owningBranch;
	}
	
	/**
	 * @param unmatchDbAmt
	 */
	public void setUnmatchDbAmt(String unmatchDbAmt) {
		this.unmatchDbAmt = unmatchDbAmt;
	}
	
    /**
     * @return unmatchDbAmt
     */
	public String getUnmatchDbAmt() {
		return this.unmatchDbAmt;
	}
	
	/**
	 * @param unmatchCrAmt
	 */
	public void setUnmatchCrAmt(String unmatchCrAmt) {
		this.unmatchCrAmt = unmatchCrAmt;
	}
	
    /**
     * @return unmatchCrAmt
     */
	public String getUnmatchCrAmt() {
		return this.unmatchCrAmt;
	}
	
	/**
	 * @param collRecType
	 */
	public void setCollRecType(String collRecType) {
		this.collRecType = collRecType;
	}
	
    /**
     * @return collRecType
     */
	public String getCollRecType() {
		return this.collRecType;
	}
	
	/**
	 * @param collectDate
	 */
	public void setCollectDate(String collectDate) {
		this.collectDate = collectDate;
	}
	
    /**
     * @return collectDate
     */
	public String getCollectDate() {
		return this.collectDate;
	}
	
	/**
	 * @param santisfiedDate
	 */
	public void setSantisfiedDate(String santisfiedDate) {
		this.santisfiedDate = santisfiedDate;
	}
	
    /**
     * @return santisfiedDate
     */
	public String getSantisfiedDate() {
		return this.santisfiedDate;
	}
	
	/**
	 * @param overlimitAmt
	 */
	public void setOverlimitAmt(String overlimitAmt) {
		this.overlimitAmt = overlimitAmt;
	}
	
    /**
     * @return overlimitAmt
     */
	public String getOverlimitAmt() {
		return this.overlimitAmt;
	}
	
	/**
	 * @param loanInd
	 */
	public void setLoanInd(String loanInd) {
		this.loanInd = loanInd;
	}
	
    /**
     * @return loanInd
     */
	public String getLoanInd() {
		return this.loanInd;
	}
	
	/**
	 * @param lastStmtDate
	 */
	public void setLastStmtDate(String lastStmtDate) {
		this.lastStmtDate = lastStmtDate;
	}
	
    /**
     * @return lastStmtDate
     */
	public String getLastStmtDate() {
		return this.lastStmtDate;
	}
	
	/**
	 * @param lastDueDate
	 */
	public void setLastDueDate(String lastDueDate) {
		this.lastDueDate = lastDueDate;
	}
	
    /**
     * @return lastDueDate
     */
	public String getLastDueDate() {
		return this.lastDueDate;
	}
	
	/**
	 * @param remainGraceBal
	 */
	public void setRemainGraceBal(String remainGraceBal) {
		this.remainGraceBal = remainGraceBal;
	}
	
    /**
     * @return remainGraceBal
     */
	public String getRemainGraceBal() {
		return this.remainGraceBal;
	}
	
	/**
	 * @param pastDueAmt1
	 */
	public void setPastDueAmt1(String pastDueAmt1) {
		this.pastDueAmt1 = pastDueAmt1;
	}
	
    /**
     * @return pastDueAmt1
     */
	public String getPastDueAmt1() {
		return this.pastDueAmt1;
	}
	
	/**
	 * @param pastDueAmt2
	 */
	public void setPastDueAmt2(String pastDueAmt2) {
		this.pastDueAmt2 = pastDueAmt2;
	}
	
    /**
     * @return pastDueAmt2
     */
	public String getPastDueAmt2() {
		return this.pastDueAmt2;
	}
	
	/**
	 * @param pastDueAmt3
	 */
	public void setPastDueAmt3(String pastDueAmt3) {
		this.pastDueAmt3 = pastDueAmt3;
	}
	
    /**
     * @return pastDueAmt3
     */
	public String getPastDueAmt3() {
		return this.pastDueAmt3;
	}
	
	/**
	 * @param pastDueAmt4
	 */
	public void setPastDueAmt4(String pastDueAmt4) {
		this.pastDueAmt4 = pastDueAmt4;
	}
	
    /**
     * @return pastDueAmt4
     */
	public String getPastDueAmt4() {
		return this.pastDueAmt4;
	}
	
	/**
	 * @param pastDueAmt5
	 */
	public void setPastDueAmt5(String pastDueAmt5) {
		this.pastDueAmt5 = pastDueAmt5;
	}
	
    /**
     * @return pastDueAmt5
     */
	public String getPastDueAmt5() {
		return this.pastDueAmt5;
	}
	
	/**
	 * @param pastDueAmt6
	 */
	public void setPastDueAmt6(String pastDueAmt6) {
		this.pastDueAmt6 = pastDueAmt6;
	}
	
    /**
     * @return pastDueAmt6
     */
	public String getPastDueAmt6() {
		return this.pastDueAmt6;
	}
	
	/**
	 * @param pastDueAmt7
	 */
	public void setPastDueAmt7(String pastDueAmt7) {
		this.pastDueAmt7 = pastDueAmt7;
	}
	
    /**
     * @return pastDueAmt7
     */
	public String getPastDueAmt7() {
		return this.pastDueAmt7;
	}
	
	/**
	 * @param ageHist
	 */
	public void setAgeHist(String ageHist) {
		this.ageHist = ageHist;
	}
	
    /**
     * @return ageHist
     */
	public String getAgeHist() {
		return this.ageHist;
	}
	
	/**
	 * @param ctdCashCnt
	 */
	public void setCtdCashCnt(String ctdCashCnt) {
		this.ctdCashCnt = ctdCashCnt;
	}
	
    /**
     * @return ctdCashCnt
     */
	public String getCtdCashCnt() {
		return this.ctdCashCnt;
	}
	
	/**
	 * @param ddOtherBankInd
	 */
	public void setDdOtherBankInd(String ddOtherBankInd) {
		this.ddOtherBankInd = ddOtherBankInd;
	}
	
    /**
     * @return ddOtherBankInd
     */
	public String getDdOtherBankInd() {
		return this.ddOtherBankInd;
	}
	
	/**
	 * @param defaultLogicalCardNo
	 */
	public void setDefaultLogicalCardNo(String defaultLogicalCardNo) {
		this.defaultLogicalCardNo = defaultLogicalCardNo;
	}
	
    /**
     * @return defaultLogicalCardNo
     */
	public String getDefaultLogicalCardNo() {
		return this.defaultLogicalCardNo;
	}
	
	/**
	 * @param retailInt
	 */
	public void setRetailInt(String retailInt) {
		this.retailInt = retailInt;
	}
	
    /**
     * @return retailInt
     */
	public String getRetailInt() {
		return this.retailInt;
	}
	
	/**
	 * @param cashInt
	 */
	public void setCashInt(String cashInt) {
		this.cashInt = cashInt;
	}
	
    /**
     * @return cashInt
     */
	public String getCashInt() {
		return this.cashInt;
	}
	
	/**
	 * @param loanInt
	 */
	public void setLoanInt(String loanInt) {
		this.loanInt = loanInt;
	}
	
    /**
     * @return loanInt
     */
	public String getLoanInt() {
		return this.loanInt;
	}
	
	/**
	 * @param pendingIntBal
	 */
	public void setPendingIntBal(String pendingIntBal) {
		this.pendingIntBal = pendingIntBal;
	}
	
    /**
     * @return pendingIntBal
     */
	public String getPendingIntBal() {
		return this.pendingIntBal;
	}
	
	/**
	 * @param ddBankBranch2
	 */
	public void setDdBankBranch2(String ddBankBranch2) {
		this.ddBankBranch2 = ddBankBranch2;
	}
	
    /**
     * @return ddBankBranch2
     */
	public String getDdBankBranch2() {
		return this.ddBankBranch2;
	}
	
	/**
	 * @param graceDaysFullInd
	 */
	public void setGraceDaysFullInd(String graceDaysFullInd) {
		this.graceDaysFullInd = graceDaysFullInd;
	}
	
    /**
     * @return graceDaysFullInd
     */
	public String getGraceDaysFullInd() {
		return this.graceDaysFullInd;
	}
	
	/**
	 * @param glpBranch
	 */
	public void setGlpBranch(String glpBranch) {
		this.glpBranch = glpBranch;
	}
	
    /**
     * @return glpBranch
     */
	public String getGlpBranch() {
		return this.glpBranch;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public String getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param lastModifiedTime
	 */
	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	
    /**
     * @return lastModifiedTime
     */
	public String getLastModifiedTime() {
		return this.lastModifiedTime;
	}
	
	/**
	 * @param filler
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
    /**
     * @return filler
     */
	public String getFiller() {
		return this.filler;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}