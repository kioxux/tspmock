package cn.com.yusys.yusp.batch.service.server.cmisbatch0011;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskCfg;
import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.BatTaskRunDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.Cmisbatch0011ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.Cmisbatch0011RespDto;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskCfgService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理Service:提供日终调度平台查询批量任务状态
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class CmisBatch0011Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0011Service.class);
    @Autowired
    private BatControlRunService batControlRunService;
    @Autowired
    private BatTaskRunService batTaskRunService;
    @Autowired
    private BatTaskCfgService batTaskCfgService;

    /**
     * 交易码：cmisbatch0011
     * 交易描述：提供日终调度平台查询批量任务状态
     *
     * @param cmisbatch0011ReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Cmisbatch0011RespDto cmisBatch0011(Cmisbatch0011ReqDto cmisbatch0011ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value);
        Cmisbatch0011RespDto cmisbatch0011RespDto = new Cmisbatch0011RespDto();// 响应Dto:提供日终调度平台查询批量任务状态
        try {
            String priFlag = cmisbatch0011ReqDto.getPriFlag();//任务级别
            String taskDate = cmisbatch0011ReqDto.getTaskDate();//任务日期,格式YYYY-MM-DD
            cmisbatch0011RespDto.setPriFlag(priFlag);//任务级别

            if (Objects.equals(BatEnums.PRI_FLAG_01.key, priFlag)) {
                cmisbatch0011RespDto = this.parallelQueryByPriFlag01(taskDate, priFlag);
            } else if (Objects.equals(BatEnums.PRI_FLAG_02.key, priFlag)) {
                cmisbatch0011RespDto = this.parallelQueryByPriFlag02(taskDate, priFlag);
            }else if (Objects.equals(BatEnums.PRI_FLAG_03.key, priFlag)) {
                cmisbatch0011RespDto = this.parallelQueryByPriFlag03(taskDate, priFlag);
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value);
        return cmisbatch0011RespDto;
    }


    /**
     * 提供日终调度平台查询批量任务状态(根据优先级别[日终一阶段(加载外围系统文件)]查询任务信息)
     *
     * @param taskDate
     * @param priFlag
     * @return
     * @throws Exception
     */
    private Cmisbatch0011RespDto parallelQueryByPriFlag01(String taskDate, String priFlag) throws Exception {
        Cmisbatch0011RespDto cmisbatch0011RespDto = new Cmisbatch0011RespDto();// 响应Dto:提供日终调度平台查询批量任务状态
        try {
            QueryModel btrlQm = new QueryModel();
            btrlQm.addCondition("taskDate", taskDate);
            btrlQm.addCondition("priFlag", priFlag); // 日终一阶段(加载外围系统文件)
            List<BatTaskRun> batTaskRunList = batTaskRunService.selectAll(btrlQm);
            logger.info("根据优先级别[" + priFlag + "]能查询任务运行信息结束,总条数为[" + batTaskRunList.size() + "]");
            java.util.List<BatTaskRunDto> batTaskRunDtoList = new ArrayList<>();

            Map queryMap = new HashMap();
            queryMap.put("taskDate", taskDate);
            queryMap.put("priFlag", priFlag);
            // 根据优先级别[日终一阶段(加载外围系统文件)]查询任务信息
            Map priTaskInfoMap = batTaskRunService.selectPriFlag01TaskInfo(queryMap);

            if (CollectionUtils.isEmpty(batTaskRunList)) {
                cmisbatch0011RespDto.setPriTaskStatus(BatEnums.TASK_STATUS_000.key);//该任务级别总状态,待日终调度平台调用
            } else {
                cmisbatch0011RespDto.setPriTaskStatus((String) priTaskInfoMap.get("pritaskstatus"));//该任务级别总状态
            }

            Long pritasknum = (Long) priTaskInfoMap.get("pritasknum");
            cmisbatch0011RespDto.setPriTaskNum(pritasknum.intValue());//该任务级别总任务数
            Long pritask100num = (Long) priTaskInfoMap.get("pritask100num");
            cmisbatch0011RespDto.setPriTask100Num(pritask100num.intValue());//该任务级别中执行成功的任务数
            Long pritask010num = (Long) priTaskInfoMap.get("pritask010num");
            cmisbatch0011RespDto.setPriTask010Num(pritask010num.intValue());//该任务级别中执行中的任务数
            cmisbatch0011RespDto.setPriErrorTaskNos((String) priTaskInfoMap.get("prierrortasknos"));//该任务级别的错误任务编号
            cmisbatch0011RespDto.setPriTaskErrorCodes((String) priTaskInfoMap.get("pritaskerrorcodes"));//该任务级别的错误码
            cmisbatch0011RespDto.setPriTaskErrorInfos((String) priTaskInfoMap.get("pritaskerrorinfos"));//该任务级别的错误信息

            btrlQm = new QueryModel();
            btrlQm.addCondition("taskDate", taskDate);
            btrlQm.addCondition("priFlag", priFlag); // 日终一阶段(加载外围系统文件)
            batTaskRunList = batTaskRunService.selectAll(btrlQm);
            batTaskRunDtoList = new ArrayList<>();
            if (CollectionUtils.nonEmpty(batTaskRunList)) {
                batTaskRunDtoList = batTaskRunList.stream().map(batTaskRun -> {
                    BatTaskRunDto batTaskRunDto = new BatTaskRunDto();
                    BeanUtils.copyProperties(batTaskRun, batTaskRunDto);
                    return batTaskRunDto;
                }).collect(Collectors.toList());
            }
            cmisbatch0011RespDto.setList(batTaskRunDtoList);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        return cmisbatch0011RespDto;
    }

    /**
     * 提供日终调度平台查询批量任务状态(根据优先级别[日终二阶段(台账和额度等加工任务)]查询任务信息)
     *
     * @param taskDate
     * @param priFlag
     * @return
     * @throws Exception
     */
    private Cmisbatch0011RespDto parallelQueryByPriFlag02(String taskDate, String priFlag) throws Exception {
        Cmisbatch0011RespDto cmisbatch0011RespDto = new Cmisbatch0011RespDto();// 响应Dto:提供日终调度平台查询批量任务状态
        try {

            QueryModel btrlQm = new QueryModel();
            btrlQm.addCondition("taskDate", taskDate);
            btrlQm.addCondition("priFlag", priFlag); // 日终二阶段(台账和额度等加工任务)
            List<BatTaskRun> batTaskRunList = batTaskRunService.selectAll(btrlQm);
            logger.info("根据优先级别[" + priFlag + "]能查询任务运行信息结束,总条数为[" + batTaskRunList.size() + "]");

            QueryModel btcQm = new QueryModel();
            btcQm.addCondition("priFlag", priFlag); // 日终二阶段(台账和额度等加工任务)
            btcQm.addCondition("useFlag", BatEnums.STD_YES.key);//启用标志
            List<BatTaskCfg> batTaskCfgList = batTaskCfgService.selectAll(btcQm);
            logger.info("根据优先级别[" + priFlag + "]能查询任务配置信息结束,总条数为[" + batTaskCfgList.size() + "]");
            if (CollectionUtils.isEmpty(batTaskCfgList)) {
                throw BizException.error(null, EchEnum.ECH080010.key, "根据优先级别[" + priFlag + "]未能查询任务配置信息");
            }
            java.util.List<BatTaskRunDto> batTaskRunDtoList = new ArrayList<>();

            Map queryMap = new HashMap();
            queryMap.put("taskDate", taskDate);
            queryMap.put("priFlag", priFlag);
            // 根据优先级别[日终二阶段(台账和额度等加工任务)]查询任务信息
            Map priTaskInfoMap = batTaskRunService.selectPriFlag02TaskInfo(queryMap);

            Long pritask000num = (Long) priTaskInfoMap.get("pritask000num");//该任务级别中待执行的任务数
            logger.info("根据优先级别[日终二阶段(台账和额度等加工任务)]查询任务信息,该任务级别中待执行的任务数值为[{}]", pritask000num);
            if (pritask000num.intValue() == batTaskCfgList.size()) {
                // 如果该任务级别中待执行的任务数 = 该任务级别中配置总数 则日终调度平台可以调用
                cmisbatch0011RespDto.setPriTaskStatus(BatEnums.TASK_STATUS_000.key);//该任务级别总状态,待日终调度平台调用
            } else {
                cmisbatch0011RespDto.setPriTaskStatus((String) priTaskInfoMap.get("pritaskstatus"));//该任务级别总状态
            }

            Long pritasknum = (Long) priTaskInfoMap.get("pritasknum");
            cmisbatch0011RespDto.setPriTaskNum(pritasknum.intValue());//该任务级别总任务数
            Long pritask100num = (Long) priTaskInfoMap.get("pritask100num");
            cmisbatch0011RespDto.setPriTask100Num(pritask100num.intValue());//该任务级别中执行成功的任务数
            Long pritask010num = (Long) priTaskInfoMap.get("pritask010num");
            cmisbatch0011RespDto.setPriTask010Num(pritask010num.intValue());//该任务级别中执行中的任务数
            cmisbatch0011RespDto.setPriErrorTaskNos((String) priTaskInfoMap.get("prierrortasknos"));//该任务级别的错误任务编号
            cmisbatch0011RespDto.setPriTaskErrorCodes((String) priTaskInfoMap.get("pritaskerrorcodes"));//该任务级别的错误码
            cmisbatch0011RespDto.setPriTaskErrorInfos((String) priTaskInfoMap.get("pritaskerrorinfos"));//该任务级别的错误信息

            btrlQm = new QueryModel();
            btrlQm.addCondition("taskDate", taskDate);
            btrlQm.addCondition("priFlag", priFlag); // 日终二阶段(台账和额度等加工任务)
            batTaskRunList = batTaskRunService.selectAll(btrlQm);
            batTaskRunDtoList = new ArrayList<>();
            if (CollectionUtils.nonEmpty(batTaskRunList)) {
                batTaskRunDtoList = batTaskRunList.stream().map(batTaskRun -> {
                    BatTaskRunDto batTaskRunDto = new BatTaskRunDto();
                    BeanUtils.copyProperties(batTaskRun, batTaskRunDto);
                    return batTaskRunDto;
                }).collect(Collectors.toList());
            }
            cmisbatch0011RespDto.setList(batTaskRunDtoList);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        return cmisbatch0011RespDto;
    }


    /**
     * 提供日终调度平台查询批量任务状态(根据优先级别[日终三阶段(贷后管理风险分类)]查询任务信息)
     *
     * @param taskDate
     * @param priFlag
     * @return
     * @throws Exception
     */
    private Cmisbatch0011RespDto parallelQueryByPriFlag03(String taskDate, String priFlag) throws Exception {
        Cmisbatch0011RespDto cmisbatch0011RespDto = new Cmisbatch0011RespDto();// 响应Dto:提供日终调度平台查询批量任务状态
        try {

            QueryModel btrlQm = new QueryModel();
            btrlQm.addCondition("taskDate", taskDate);
            btrlQm.addCondition("priFlag", priFlag); // 日终三阶段(贷后管理风险分类)
            List<BatTaskRun> batTaskRunList = batTaskRunService.selectAll(btrlQm);
            logger.info("根据优先级别[" + priFlag + "]能查询任务运行信息结束,总条数为[" + batTaskRunList.size() + "]");

            QueryModel btcQm = new QueryModel();
            btcQm.addCondition("priFlag", priFlag); // 日终二阶段(台账和额度等加工任务)
            btcQm.addCondition("useFlag", BatEnums.STD_YES.key);//启用标志
            List<BatTaskCfg> batTaskCfgList = batTaskCfgService.selectAll(btcQm);
            logger.info("根据优先级别[" + priFlag + "]能查询任务配置信息结束,总条数为[" + batTaskCfgList.size() + "]");
            if (CollectionUtils.isEmpty(batTaskCfgList)) {
                throw BizException.error(null, EchEnum.ECH080010.key, "根据优先级别[" + priFlag + "]未能查询任务配置信息");
            }
            java.util.List<BatTaskRunDto> batTaskRunDtoList = new ArrayList<>();

            Map queryMap = new HashMap();
            queryMap.put("taskDate", taskDate);
            queryMap.put("priFlag", priFlag);
            // 根据优先级别[日终三阶段(贷后管理风险分类)]查询任务信息
            Map priTaskInfoMap = batTaskRunService.selectPriFlag03TaskInfo(queryMap);

            Long pritask000num = (Long) priTaskInfoMap.get("pritask000num");//该任务级别中待执行的任务数
            logger.info("根据优先级别[日终三阶段(贷后管理风险分类)]查询任务信息,该任务级别中待执行的任务数值为[{}]", pritask000num);
            if (pritask000num.intValue() == batTaskCfgList.size()) {
                // 如果该任务级别中待执行的任务数 = 该任务级别中配置总数 则日终调度平台可以调用
                cmisbatch0011RespDto.setPriTaskStatus(BatEnums.TASK_STATUS_000.key);//该任务级别总状态,待日终调度平台调用
            } else {
                cmisbatch0011RespDto.setPriTaskStatus((String) priTaskInfoMap.get("pritaskstatus"));//该任务级别总状态
            }

            Long pritasknum = (Long) priTaskInfoMap.get("pritasknum");
            cmisbatch0011RespDto.setPriTaskNum(pritasknum.intValue());//该任务级别总任务数
            Long pritask100num = (Long) priTaskInfoMap.get("pritask100num");
            cmisbatch0011RespDto.setPriTask100Num(pritask100num.intValue());//该任务级别中执行成功的任务数
            Long pritask010num = (Long) priTaskInfoMap.get("pritask010num");
            cmisbatch0011RespDto.setPriTask010Num(pritask010num.intValue());//该任务级别中执行中的任务数
            cmisbatch0011RespDto.setPriErrorTaskNos((String) priTaskInfoMap.get("prierrortasknos"));//该任务级别的错误任务编号
            cmisbatch0011RespDto.setPriTaskErrorCodes((String) priTaskInfoMap.get("pritaskerrorcodes"));//该任务级别的错误码
            cmisbatch0011RespDto.setPriTaskErrorInfos((String) priTaskInfoMap.get("pritaskerrorinfos"));//该任务级别的错误信息

            btrlQm = new QueryModel();
            btrlQm.addCondition("taskDate", taskDate);
            btrlQm.addCondition("priFlag", priFlag); // 日终二阶段(台账和额度等加工任务)
            batTaskRunList = batTaskRunService.selectAll(btrlQm);
            batTaskRunDtoList = new ArrayList<>();
            if (CollectionUtils.nonEmpty(batTaskRunList)) {
                batTaskRunDtoList = batTaskRunList.stream().map(batTaskRun -> {
                    BatTaskRunDto batTaskRunDto = new BatTaskRunDto();
                    BeanUtils.copyProperties(batTaskRun, batTaskRunDto);
                    return batTaskRunDto;
                }).collect(Collectors.toList());
            }
            cmisbatch0011RespDto.setList(batTaskRunDtoList);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0011.key, DscmsEnum.TRADE_CODE_CMISBATCH0011.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        return cmisbatch0011RespDto;
    }
}
