/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.bat;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTaskCfg
 * @类描述: bat_task_cfg数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-15 20:15:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_task_cfg")
public class BatTaskCfg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 任务编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "TASK_NO")
	private String taskNo;
	
	/** 任务名称 **/
	@Column(name = "TASK_NAME", unique = false, nullable = true, length = 100)
	private String taskName;
	
	/** 任务类型 STD_TASK_TYPE **/
	@Column(name = "TASK_TYPE", unique = false, nullable = true, length = 2)
	private String taskType;
	
	/** 优先级别 **/
	@Column(name = "PRI_FLAG", unique = false, nullable = true, length = 1)
	private String priFlag;
	
	/** 执行频率 STD_RUN_PERIOD **/
	@Column(name = "RUN_PERIOD", unique = false, nullable = true, length = 1)
	private String runPeriod;
	
	/** 频率规则 **/
	@Column(name = "PERIOD_RULE", unique = false, nullable = true, length = 100)
	private String periodRule;
	
	/** 任务执行类 **/
	@Column(name = "TASK_CLASS", unique = false, nullable = true, length = 200)
	private String taskClass;
	
	/** 分页提交记录数0表示不分页。 **/
	@Column(name = "COMMIT_NUM", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal commitNum;
	
	/** 任务处理事件数 **/
	@Column(name = "EVENT_NUM", unique = false, nullable = true, length = 10)
	private Integer eventNum;
	
	/** 任务执行时点标志 STD_RUN_TIME_FLAG **/
	@Column(name = "RUN_TIME_FLAG", unique = false, nullable = true, length = 1)
	private String runTimeFlag;
	
	/** 最早开始时间 格式 HH:mm **/
	@Column(name = "FIRST_RUN_TIME", unique = false, nullable = true, length = 8)
	private String firstRunTime;
	
	/** 最晚开始时间 格式 HH:mm **/
	@Column(name = "LAST_RUN_TIME", unique = false, nullable = true, length = 8)
	private String lastRunTime;
	
	/** 预估执行时间 格式 HH:mm:ss **/
	@Column(name = "RUN_TIME", unique = false, nullable = true, length = 8)
	private String runTime;
	
	/** 是否存在互斥 STD_YES_NO **/
	@Column(name = "MUTEX_FLAG", unique = false, nullable = true, length = 1)
	private String mutexFlag;
	
	/** 互斥编号 **/
	@Column(name = "MUTEX_NO", unique = false, nullable = true, length = 200)
	private String mutexNo;
	
	/** 是否存在依赖 STD_YES_NO **/
	@Column(name = "RELATION_FLAG", unique = false, nullable = true, length = 1)
	private String relationFlag;
	
	/** 依赖编号 **/
	@Column(name = "REL_NO", unique = false, nullable = true, length = 400)
	private String relNo;
	
	/** 文件交互方式 STD_FILE_EXCHANGE_TYPE **/
	@Column(name = "FILE_EXCHANGE_TYPE", unique = false, nullable = true, length = 2)
	private String fileExchangeType;
	
	/** 文件分隔符 **/
	@Column(name = "FILE_SPLIT", unique = false, nullable = true, length = 5)
	private String fileSplit;
	
	/** 文件尾类型 STD_FILE_END_TYPE **/
	@Column(name = "FILE_END_TYPE", unique = false, nullable = true, length = 1)
	private String fileEndType;
	
	/** 文件是否压缩 STD_YES_NO **/
	@Column(name = "FILE_COMPRESS_FLAG", unique = false, nullable = true, length = 1)
	private String fileCompressFlag;
	
	/** 信号类型 STD_SIGNAL_TYPE **/
	@Column(name = "SIGNAL_TYPE", unique = false, nullable = true, length = 2)
	private String signalType;
	
	/** 信号清单用于信号检查。依据任务配置信息中的“信号类型”进行配置，如果依赖于多个信号时，则以逗号分隔。 **/
	@Column(name = "SIGNAL_LIST", unique = false, nullable = true, length = 600)
	private String signalList;
	
	/** 文件日期类型 **/
	@Column(name = "FILE_DATE_TYPE", unique = false, nullable = true, length = 3)
	private String fileDateType;

	/**
	 * 数据日期类型
	 **/
	@Column(name = "DATA_DATE_TYPE", unique = false, nullable = true, length = 3)
	private String dataDateType;

	/**
	 * 信号日期类型
	 **/
	@Column(name = "SIGNAL_DATE_TYPE", unique = false, nullable = true, length = 3)
	private String signalDateType;

	/**
	 * 数据文件全名
	 **/
	@Column(name = "DATA_FILE_NAME", unique = false, nullable = true, length = 100)
	private String dataFileName;

	/**
	 * 信号文件全名
	 **/
	@Column(name = "SIGNAL_FILE_NAME", unique = false, nullable = true, length = 100)
	private String signalFileName;

	/**
	 * 数据文件落地表名
	 **/
	@Column(name = "BAT_T_TAB_NAME", unique = false, nullable = true, length = 100)
	private String batTTabName;

	/**
	 * 是否全量标志 STD_YES_NO
	 **/
	@Column(name = "FULL_FLAG", unique = false, nullable = true, length = 1)
	private String fullFlag;

	/**
	 * MD5存放类型 STD_MD5_FLAG
	 **/
	@Column(name = "MD5_FLAG", unique = false, nullable = true, length = 2)
	private String md5Flag;

	/**
	 * 是否忽略信号 STD_YES_NO
	 **/
	@Column(name = "IGNORE_SIGNAL_FLAG", unique = false, nullable = true, length = 1)
	private String ignoreSignalFlag;

	/**
	 * 启用标志 STD_YES_NO
	 **/
	@Column(name = "USE_FLAG", unique = false, nullable = true, length = 1)
	private String useFlag;

	/**
	 * 预警时间
	 **/
	@Column(name = "WARN_TIME", unique = false, nullable = true, length = 10)
	private String warnTime;
	
	/** 预警频率（秒） **/
	@Column(name = "WARN_FREQUENCY", unique = false, nullable = true, length = 10)
	private Integer warnFrequency;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date createTime;

    /** 修改时间 **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date updateTime;
	
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param taskName
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
    /**
     * @return taskName
     */
	public String getTaskName() {
		return this.taskName;
	}
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	
    /**
     * @return taskType
     */
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param priFlag
	 */
	public void setPriFlag(String priFlag) {
		this.priFlag = priFlag;
	}
	
    /**
     * @return priFlag
     */
	public String getPriFlag() {
		return this.priFlag;
	}
	
	/**
	 * @param runPeriod
	 */
	public void setRunPeriod(String runPeriod) {
		this.runPeriod = runPeriod;
	}
	
    /**
     * @return runPeriod
     */
	public String getRunPeriod() {
		return this.runPeriod;
	}
	
	/**
	 * @param periodRule
	 */
	public void setPeriodRule(String periodRule) {
		this.periodRule = periodRule;
	}
	
    /**
     * @return periodRule
     */
	public String getPeriodRule() {
		return this.periodRule;
	}
	
	/**
	 * @param taskClass
	 */
	public void setTaskClass(String taskClass) {
		this.taskClass = taskClass;
	}
	
    /**
     * @return taskClass
     */
	public String getTaskClass() {
		return this.taskClass;
	}
	
	/**
	 * @param commitNum
	 */
	public void setCommitNum(java.math.BigDecimal commitNum) {
		this.commitNum = commitNum;
	}
	
    /**
     * @return commitNum
     */
	public java.math.BigDecimal getCommitNum() {
		return this.commitNum;
	}
	
	/**
	 * @param eventNum
	 */
	public void setEventNum(Integer eventNum) {
		this.eventNum = eventNum;
	}
	
    /**
     * @return eventNum
     */
	public Integer getEventNum() {
		return this.eventNum;
	}
	
	/**
	 * @param runTimeFlag
	 */
	public void setRunTimeFlag(String runTimeFlag) {
		this.runTimeFlag = runTimeFlag;
	}
	
    /**
     * @return runTimeFlag
     */
	public String getRunTimeFlag() {
		return this.runTimeFlag;
	}
	
	/**
	 * @param firstRunTime
	 */
	public void setFirstRunTime(String firstRunTime) {
		this.firstRunTime = firstRunTime;
	}
	
    /**
     * @return firstRunTime
     */
	public String getFirstRunTime() {
		return this.firstRunTime;
	}
	
	/**
	 * @param lastRunTime
	 */
	public void setLastRunTime(String lastRunTime) {
		this.lastRunTime = lastRunTime;
	}
	
    /**
     * @return lastRunTime
     */
	public String getLastRunTime() {
		return this.lastRunTime;
	}
	
	/**
	 * @param runTime
	 */
	public void setRunTime(String runTime) {
		this.runTime = runTime;
	}
	
    /**
     * @return runTime
     */
	public String getRunTime() {
		return this.runTime;
	}
	
	/**
	 * @param mutexFlag
	 */
	public void setMutexFlag(String mutexFlag) {
		this.mutexFlag = mutexFlag;
	}
	
    /**
     * @return mutexFlag
     */
	public String getMutexFlag() {
		return this.mutexFlag;
	}
	
	/**
	 * @param mutexNo
	 */
	public void setMutexNo(String mutexNo) {
		this.mutexNo = mutexNo;
	}
	
    /**
     * @return mutexNo
     */
	public String getMutexNo() {
		return this.mutexNo;
	}
	
	/**
	 * @param relationFlag
	 */
	public void setRelationFlag(String relationFlag) {
		this.relationFlag = relationFlag;
	}
	
    /**
     * @return relationFlag
     */
	public String getRelationFlag() {
		return this.relationFlag;
	}
	
	/**
	 * @param relNo
	 */
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}
	
    /**
     * @return relNo
     */
	public String getRelNo() {
		return this.relNo;
	}
	
	/**
	 * @param fileExchangeType
	 */
	public void setFileExchangeType(String fileExchangeType) {
		this.fileExchangeType = fileExchangeType;
	}
	
    /**
     * @return fileExchangeType
     */
	public String getFileExchangeType() {
		return this.fileExchangeType;
	}
	
	/**
	 * @param fileSplit
	 */
	public void setFileSplit(String fileSplit) {
		this.fileSplit = fileSplit;
	}
	
    /**
     * @return fileSplit
     */
	public String getFileSplit() {
		return this.fileSplit;
	}
	
	/**
	 * @param fileEndType
	 */
	public void setFileEndType(String fileEndType) {
		this.fileEndType = fileEndType;
	}
	
    /**
     * @return fileEndType
     */
	public String getFileEndType() {
		return this.fileEndType;
	}
	
	/**
	 * @param fileCompressFlag
	 */
	public void setFileCompressFlag(String fileCompressFlag) {
		this.fileCompressFlag = fileCompressFlag;
	}
	
    /**
     * @return fileCompressFlag
     */
	public String getFileCompressFlag() {
		return this.fileCompressFlag;
	}
	
	/**
	 * @param signalType
	 */
	public void setSignalType(String signalType) {
		this.signalType = signalType;
	}
	
    /**
     * @return signalType
     */
	public String getSignalType() {
		return this.signalType;
	}
	
	/**
	 * @param signalList
	 */
	public void setSignalList(String signalList) {
		this.signalList = signalList;
	}
	
    /**
     * @return signalList
     */
	public String getSignalList() {
		return this.signalList;
	}
	
	/**
	 * @param fileDateType
	 */
	public void setFileDateType(String fileDateType) {
		this.fileDateType = fileDateType;
	}
	
    /**
     * @return fileDateType
     */
	public String getFileDateType() {
		return this.fileDateType;
	}
	
	/**
	 * @param dataDateType
	 */
	public void setDataDateType(String dataDateType) {
		this.dataDateType = dataDateType;
	}
	
    /**
     * @return dataDateType
     */
	public String getDataDateType() {
		return this.dataDateType;
	}

	/**
	 * @param signalDateType
	 */
	public void setSignalDateType(String signalDateType) {
		this.signalDateType = signalDateType;
	}

	/**
	 * @return signalDateType
	 */
	public String getSignalDateType() {
		return this.signalDateType;
	}

	/**
	 * @return dataFileName
	 */
	public String getDataFileName() {
		return this.dataFileName;
	}

	/**
	 * @param dataFileName
	 */
	public void setDataFileName(String dataFileName) {
		this.dataFileName = dataFileName;
	}

	/**
	 * @return signalFileName
	 */
	public String getSignalFileName() {
		return this.signalFileName;
	}

	/**
	 * @param signalFileName
	 */
	public void setSignalFileName(String signalFileName) {
		this.signalFileName = signalFileName;
	}

	/**
	 * @return batTTabName
	 */
	public String getBatTTabName() {
		return this.batTTabName;
	}

	/**
	 * @param batTTabName
	 */
	public void setBatTTabName(String batTTabName) {
		this.batTTabName = batTTabName;
	}

	/**
	 * @return fullFlag
	 */
	public String getFullFlag() {
		return this.fullFlag;
	}

	/**
	 * @param fullFlag
	 */
	public void setFullFlag(String fullFlag) {
		this.fullFlag = fullFlag;
	}

	/**
	 * @return md5Flag
	 */
	public String getMd5Flag() {
		return this.md5Flag;
	}

	/**
	 * @param md5Flag
	 */
	public void setMd5Flag(String md5Flag) {
		this.md5Flag = md5Flag;
	}

	/**
	 * @param ignoreSignalFlag
	 */
	public void setIgnoreSignalFlag(String ignoreSignalFlag) {
		this.ignoreSignalFlag = ignoreSignalFlag;
	}
	
    /**
     * @return ignoreSignalFlag
     */
	public String getIgnoreSignalFlag() {
		return this.ignoreSignalFlag;
	}
	
	/**
	 * @param useFlag
	 */
	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}
	
    /**
     * @return useFlag
     */
	public String getUseFlag() {
		return this.useFlag;
	}
	
	/**
	 * @param warnTime
	 */
	public void setWarnTime(String warnTime) {
		this.warnTime = warnTime;
	}
	
    /**
     * @return warnTime
     */
	public String getWarnTime() {
		return this.warnTime;
	}
	
	/**
	 * @param warnFrequency
	 */
	public void setWarnFrequency(Integer warnFrequency) {
		this.warnFrequency = warnFrequency;
	}
	
    /**
     * @return warnFrequency
     */
	public Integer getWarnFrequency() {
		return this.warnFrequency;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}