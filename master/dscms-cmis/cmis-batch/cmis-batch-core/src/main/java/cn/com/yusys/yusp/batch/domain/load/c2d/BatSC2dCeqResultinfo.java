/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.c2d;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dCeqResultinfo
 * @类描述: bat_s_c2d_ceq_resultinfo数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:14:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_c2d_ceq_resultinfo")
public class BatSC2dCeqResultinfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 查询结果编号 **/
	@Id
	@Column(name = "ID")
	private String id;
	
	/** 客户姓名 **/
	@Id
	@Column(name = "CLIENT_NAME")
	private String clientName;
	
	/** 信用报告编号 **/
	@Id
	@Column(name = "REPORT_ID")
	private String reportId;
	
	/** 中征码 **/
	@Column(name = "LOANCARD_CODE", unique = false, nullable = true, length = 16)
	private String loancardCode;
	
	/** 统一社会信用代码 **/
	@Column(name = "US_CREDIT_CODE", unique = false, nullable = true, length = 18)
	private String usCreditCode;
	
	/** 组织机构代码 **/
	@Column(name = "COPR_CODE", unique = false, nullable = true, length = 10)
	private String coprCode;
	
	/** 机构信用代码 **/
	@Column(name = "ORG_CREDIT_CODE", unique = false, nullable = true, length = 18)
	private String orgCreditCode;
	
	/** 国税 **/
	@Column(name = "GS_REGI_CODE", unique = false, nullable = true, length = 60)
	private String gsRegiCode;
	
	/** 地税 **/
	@Column(name = "DS_REGI_CODE", unique = false, nullable = true, length = 60)
	private String dsRegiCode;
	
	/** 登记注册类型 **/
	@Column(name = "REGI_TYPE_CODE", unique = false, nullable = true, length = 2)
	private String regiTypeCode;
	
	/** 登记注册号 **/
	@Column(name = "FRGCORPNO", unique = false, nullable = true, length = 120)
	private String frgcorpno;
	
	/** 关联档案ID **/
	@Column(name = "AUTHARCHIVE_ID", unique = false, nullable = true, length = 32)
	private String autharchiveId;
	
	/** 查询原因 **/
	@Column(name = "QRY_REASON", unique = false, nullable = true, length = 2)
	private String qryReason;
	
	/** 查询版式 **/
	@Column(name = "QUERY_FORMAT", unique = false, nullable = true, length = 2)
	private String queryFormat;
	
	/** 备注 **/
	@Column(name = "NOTE", unique = false, nullable = true, length = 100)
	private String note;
	
	/** 操作机构 **/
	@Column(name = "OPER_ORG", unique = false, nullable = true, length = 16)
	private String operOrg;
	
	/** 操作用户 **/
	@Column(name = "OPERATOR", unique = false, nullable = true, length = 50)
	private String operator;
	
	/** 征信用户 **/
	@Column(name = "CREDIT_USER", unique = false, nullable = true, length = 45)
	private String creditUser;
	
	/** 金融机构代码 **/
	@Column(name = "QUERY_ORG", unique = false, nullable = true, length = 16)
	private String queryOrg;
	
	/** 数字解读标志（0-非数字解读，2-数字解读） **/
	@Column(name = "QUERY_TYPE", unique = false, nullable = true, length = 14)
	private String queryType;
	
	/** 查询要求时效 **/
	@Column(name = "QTIME_LIMIT", unique = false, nullable = true, length = 16)
	private String qtimeLimit;
	
	/** 结果类型 **/
	@Column(name = "RESULT_TYPE", unique = false, nullable = true, length = 10)
	private String resultType;
	
	/** 查询人行通信模式（1-WebService，2-MQ，3-http） **/
	@Column(name = "QUERY_MODE", unique = false, nullable = true, length = 1)
	private String queryMode;
	
	/** 数据状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 1)
	private String status;
	
	/** 信用报告来源（1-本地，2-人行） **/
	@Column(name = "SOURCE", unique = false, nullable = true, length = 1)
	private String source;
	
	/** 人行登陆认证标识 **/
	@Column(name = "CERTIFICATION_MARK", unique = false, nullable = true, length = 64)
	private String certificationMark;
	
	/** 批量标志（1-单笔查询，2-批量查询） **/
	@Column(name = "BATCH_FLAG", unique = false, nullable = true, length = 1)
	private String batchFlag;
	
	/** 批量查询批次号 **/
	@Column(name = "MSG_NO", unique = false, nullable = true, length = 32)
	private String msgNo;
	
	/** 请求流水号 **/
	@Column(name = "FLOW_ID", unique = false, nullable = true, length = 32)
	private String flowId;
	
	/** 渠道编号 **/
	@Column(name = "CHANNEL_ID", unique = false, nullable = true, length = 2)
	private String channelId;
	
	/** 系统编号 **/
	@Column(name = "CSTMSYS_ID", unique = false, nullable = true, length = 20)
	private String cstmsysId;
	
	/** 请求客户端IP **/
	@Column(name = "CLIENT_IP", unique = false, nullable = true, length = 30)
	private String clientIp;
	
	/** 错误信息 **/
	@Column(name = "ERROR_INFO", unique = false, nullable = true, length = 450)
	private String errorInfo;
	
	/** 查询时间 **/
	@Column(name = "QUERY_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date queryTime;
	
	/** 操作时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 审批方式（1-同步，2-异步） **/
	@Column(name = "CHECK_WAY", unique = false, nullable = true, length = 1)
	private String checkWay;
	
	/** 关联业务号 **/
	@Column(name = "ASSOCBSNSS_DATA", unique = false, nullable = true, length = 300)
	private String assocbsnssData;
	
	/** 复核用户 **/
	@Column(name = "REK_USER", unique = false, nullable = true, length = 50)
	private String rekUser;
	
	/** 复核机构 **/
	@Column(name = "REK_ORG", unique = false, nullable = true, length = 16)
	private String rekOrg;
	
	/** 复核时间 **/
	@Column(name = "REK_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date rekTime;
	
	/** 复核请求ID **/
	@Column(name = "CHECK_ID", unique = false, nullable = true, length = 32)
	private String checkId;
	
	/** 关联信用报告ID **/
	@Column(name = "CREDIT_ID", unique = false, nullable = true, length = 32)
	private String creditId;
	
	/** 档案补录类型（1-非贷后无资料，2-贷后无资料，3-贷后无借据编号，4-贷后两者皆无） **/
	@Column(name = "ARCHIVE_REVISE", unique = false, nullable = true, length = 1)
	private String archiveRevise;
	
	/** 查询用户密码 **/
	@Column(name = "PASSWORD", unique = false, nullable = true, length = 100)
	private String password;
	
	/** 信用报告版本 **/
	@Column(name = "REPORT_VERSION", unique = false, nullable = true, length = 32)
	private String reportVersion;
	
	/** 信用报告格式 **/
	@Column(name = "REPORT_FORMAT", unique = false, nullable = true, length = 32)
	private String reportFormat;
	
	/** 查询原因备用 **/
	@Column(name = "QUERYREASON_ID", unique = false, nullable = true, length = 32)
	private String queryreasonId;
	
	/** HTML路径 **/
	@Column(name = "HTML_PATH", unique = false, nullable = true, length = 200)
	private String htmlPath;
	
	/** XML路径 **/
	@Column(name = "XML_PATH", unique = false, nullable = true, length = 200)
	private String xmlPath;
	
	/** JSON路径 **/
	@Column(name = "JSON_PATH", unique = false, nullable = true, length = 200)
	private String jsonPath;
	
	/** PDF路径 **/
	@Column(name = "PDF_PATH", unique = false, nullable = true, length = 200)
	private String pdfPath;
	
	/** 报告路径备用 **/
	@Column(name = "CONTEXT_FILE_PATH", unique = false, nullable = true, length = 528)
	private String contextFilePath;
	
	/** 征信中心查询用时 **/
	@Column(name = "USE_TIME", unique = false, nullable = true, length = 7)
	private java.math.BigDecimal useTime;
	
	/** 接口发起用户 **/
	@Column(name = "CALL_SYS_USER", unique = false, nullable = true, length = 100)
	private String callSysUser;
	
	/** 接入系统审批员 **/
	@Column(name = "RECHECK_USER_NAME", unique = false, nullable = true, length = 100)
	private String recheckUserName;
	
	/** 预警类型 **/
	@Column(name = "QUERY_CONTROL_WARNING_TYPE", unique = false, nullable = true, length = 8)
	private String queryControlWarningType;
	
	/** 流水号 **/
	@Column(name = "SERIAL_NUMBER", unique = false, nullable = true, length = 32)
	private String serialNumber;
	
	/** 批量查询号 **/
	@Column(name = "BATCH_QUERY_NO", unique = false, nullable = true, length = 32)
	private String batchQueryNo;
	
	/** 扩展字段1 **/
	@Column(name = "EXT1", unique = false, nullable = true, length = 60)
	private String ext1;
	
	/** 扩展字段2 **/
	@Column(name = "EXT2", unique = false, nullable = true, length = 60)
	private String ext2;
	
	/** 扩展字段3 **/
	@Column(name = "EXT3", unique = false, nullable = true, length = 60)
	private String ext3;
	
	/** 扩展字段4 **/
	@Column(name = "EXT4", unique = false, nullable = true, length = 60)
	private String ext4;
	
	/** 扩展字段5 **/
	@Column(name = "EXT5", unique = false, nullable = true, length = 60)
	private String ext5;
	
	/** 扩展字段6 **/
	@Column(name = "EXT6", unique = false, nullable = true, length = 60)
	private String ext6;
	
	/** REPORT_QUERY_DATETIME **/
	@Column(name = "REPORT_QUERY_DATETIME", unique = false, nullable = true, length = 19)
	private java.util.Date reportQueryDatetime;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param clientName
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
    /**
     * @return clientName
     */
	public String getClientName() {
		return this.clientName;
	}
	
	/**
	 * @param loancardCode
	 */
	public void setLoancardCode(String loancardCode) {
		this.loancardCode = loancardCode;
	}
	
    /**
     * @return loancardCode
     */
	public String getLoancardCode() {
		return this.loancardCode;
	}
	
	/**
	 * @param usCreditCode
	 */
	public void setUsCreditCode(String usCreditCode) {
		this.usCreditCode = usCreditCode;
	}
	
    /**
     * @return usCreditCode
     */
	public String getUsCreditCode() {
		return this.usCreditCode;
	}
	
	/**
	 * @param coprCode
	 */
	public void setCoprCode(String coprCode) {
		this.coprCode = coprCode;
	}
	
    /**
     * @return coprCode
     */
	public String getCoprCode() {
		return this.coprCode;
	}
	
	/**
	 * @param orgCreditCode
	 */
	public void setOrgCreditCode(String orgCreditCode) {
		this.orgCreditCode = orgCreditCode;
	}
	
    /**
     * @return orgCreditCode
     */
	public String getOrgCreditCode() {
		return this.orgCreditCode;
	}
	
	/**
	 * @param gsRegiCode
	 */
	public void setGsRegiCode(String gsRegiCode) {
		this.gsRegiCode = gsRegiCode;
	}
	
    /**
     * @return gsRegiCode
     */
	public String getGsRegiCode() {
		return this.gsRegiCode;
	}
	
	/**
	 * @param dsRegiCode
	 */
	public void setDsRegiCode(String dsRegiCode) {
		this.dsRegiCode = dsRegiCode;
	}
	
    /**
     * @return dsRegiCode
     */
	public String getDsRegiCode() {
		return this.dsRegiCode;
	}
	
	/**
	 * @param regiTypeCode
	 */
	public void setRegiTypeCode(String regiTypeCode) {
		this.regiTypeCode = regiTypeCode;
	}
	
    /**
     * @return regiTypeCode
     */
	public String getRegiTypeCode() {
		return this.regiTypeCode;
	}
	
	/**
	 * @param frgcorpno
	 */
	public void setFrgcorpno(String frgcorpno) {
		this.frgcorpno = frgcorpno;
	}
	
    /**
     * @return frgcorpno
     */
	public String getFrgcorpno() {
		return this.frgcorpno;
	}
	
	/**
	 * @param autharchiveId
	 */
	public void setAutharchiveId(String autharchiveId) {
		this.autharchiveId = autharchiveId;
	}
	
    /**
     * @return autharchiveId
     */
	public String getAutharchiveId() {
		return this.autharchiveId;
	}
	
	/**
	 * @param qryReason
	 */
	public void setQryReason(String qryReason) {
		this.qryReason = qryReason;
	}
	
    /**
     * @return qryReason
     */
	public String getQryReason() {
		return this.qryReason;
	}
	
	/**
	 * @param queryFormat
	 */
	public void setQueryFormat(String queryFormat) {
		this.queryFormat = queryFormat;
	}
	
    /**
     * @return queryFormat
     */
	public String getQueryFormat() {
		return this.queryFormat;
	}
	
	/**
	 * @param note
	 */
	public void setNote(String note) {
		this.note = note;
	}
	
    /**
     * @return note
     */
	public String getNote() {
		return this.note;
	}
	
	/**
	 * @param operOrg
	 */
	public void setOperOrg(String operOrg) {
		this.operOrg = operOrg;
	}
	
    /**
     * @return operOrg
     */
	public String getOperOrg() {
		return this.operOrg;
	}
	
	/**
	 * @param operator
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}
	
    /**
     * @return operator
     */
	public String getOperator() {
		return this.operator;
	}
	
	/**
	 * @param creditUser
	 */
	public void setCreditUser(String creditUser) {
		this.creditUser = creditUser;
	}
	
    /**
     * @return creditUser
     */
	public String getCreditUser() {
		return this.creditUser;
	}
	
	/**
	 * @param queryOrg
	 */
	public void setQueryOrg(String queryOrg) {
		this.queryOrg = queryOrg;
	}
	
    /**
     * @return queryOrg
     */
	public String getQueryOrg() {
		return this.queryOrg;
	}
	
	/**
	 * @param queryType
	 */
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	
    /**
     * @return queryType
     */
	public String getQueryType() {
		return this.queryType;
	}
	
	/**
	 * @param qtimeLimit
	 */
	public void setQtimeLimit(String qtimeLimit) {
		this.qtimeLimit = qtimeLimit;
	}
	
    /**
     * @return qtimeLimit
     */
	public String getQtimeLimit() {
		return this.qtimeLimit;
	}
	
	/**
	 * @param resultType
	 */
	public void setResultType(String resultType) {
		this.resultType = resultType;
	}
	
    /**
     * @return resultType
     */
	public String getResultType() {
		return this.resultType;
	}
	
	/**
	 * @param queryMode
	 */
	public void setQueryMode(String queryMode) {
		this.queryMode = queryMode;
	}
	
    /**
     * @return queryMode
     */
	public String getQueryMode() {
		return this.queryMode;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param source
	 */
	public void setSource(String source) {
		this.source = source;
	}
	
    /**
     * @return source
     */
	public String getSource() {
		return this.source;
	}
	
	/**
	 * @param certificationMark
	 */
	public void setCertificationMark(String certificationMark) {
		this.certificationMark = certificationMark;
	}
	
    /**
     * @return certificationMark
     */
	public String getCertificationMark() {
		return this.certificationMark;
	}
	
	/**
	 * @param batchFlag
	 */
	public void setBatchFlag(String batchFlag) {
		this.batchFlag = batchFlag;
	}
	
    /**
     * @return batchFlag
     */
	public String getBatchFlag() {
		return this.batchFlag;
	}
	
	/**
	 * @param msgNo
	 */
	public void setMsgNo(String msgNo) {
		this.msgNo = msgNo;
	}
	
    /**
     * @return msgNo
     */
	public String getMsgNo() {
		return this.msgNo;
	}
	
	/**
	 * @param flowId
	 */
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	
    /**
     * @return flowId
     */
	public String getFlowId() {
		return this.flowId;
	}
	
	/**
	 * @param channelId
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	
    /**
     * @return channelId
     */
	public String getChannelId() {
		return this.channelId;
	}
	
	/**
	 * @param cstmsysId
	 */
	public void setCstmsysId(String cstmsysId) {
		this.cstmsysId = cstmsysId;
	}
	
    /**
     * @return cstmsysId
     */
	public String getCstmsysId() {
		return this.cstmsysId;
	}
	
	/**
	 * @param clientIp
	 */
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	
    /**
     * @return clientIp
     */
	public String getClientIp() {
		return this.clientIp;
	}
	
	/**
	 * @param errorInfo
	 */
	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}
	
    /**
     * @return errorInfo
     */
	public String getErrorInfo() {
		return this.errorInfo;
	}
	
	/**
	 * @param queryTime
	 */
	public void setQueryTime(java.util.Date queryTime) {
		this.queryTime = queryTime;
	}
	
    /**
     * @return queryTime
     */
	public java.util.Date getQueryTime() {
		return this.queryTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param checkWay
	 */
	public void setCheckWay(String checkWay) {
		this.checkWay = checkWay;
	}
	
    /**
     * @return checkWay
     */
	public String getCheckWay() {
		return this.checkWay;
	}
	
	/**
	 * @param assocbsnssData
	 */
	public void setAssocbsnssData(String assocbsnssData) {
		this.assocbsnssData = assocbsnssData;
	}
	
    /**
     * @return assocbsnssData
     */
	public String getAssocbsnssData() {
		return this.assocbsnssData;
	}
	
	/**
	 * @param rekUser
	 */
	public void setRekUser(String rekUser) {
		this.rekUser = rekUser;
	}
	
    /**
     * @return rekUser
     */
	public String getRekUser() {
		return this.rekUser;
	}
	
	/**
	 * @param rekOrg
	 */
	public void setRekOrg(String rekOrg) {
		this.rekOrg = rekOrg;
	}
	
    /**
     * @return rekOrg
     */
	public String getRekOrg() {
		return this.rekOrg;
	}
	
	/**
	 * @param rekTime
	 */
	public void setRekTime(java.util.Date rekTime) {
		this.rekTime = rekTime;
	}
	
    /**
     * @return rekTime
     */
	public java.util.Date getRekTime() {
		return this.rekTime;
	}
	
	/**
	 * @param checkId
	 */
	public void setCheckId(String checkId) {
		this.checkId = checkId;
	}
	
    /**
     * @return checkId
     */
	public String getCheckId() {
		return this.checkId;
	}
	
	/**
	 * @param creditId
	 */
	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}
	
    /**
     * @return creditId
     */
	public String getCreditId() {
		return this.creditId;
	}
	
	/**
	 * @param reportId
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	
    /**
     * @return reportId
     */
	public String getReportId() {
		return this.reportId;
	}
	
	/**
	 * @param archiveRevise
	 */
	public void setArchiveRevise(String archiveRevise) {
		this.archiveRevise = archiveRevise;
	}
	
    /**
     * @return archiveRevise
     */
	public String getArchiveRevise() {
		return this.archiveRevise;
	}
	
	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
    /**
     * @return password
     */
	public String getPassword() {
		return this.password;
	}
	
	/**
	 * @param reportVersion
	 */
	public void setReportVersion(String reportVersion) {
		this.reportVersion = reportVersion;
	}
	
    /**
     * @return reportVersion
     */
	public String getReportVersion() {
		return this.reportVersion;
	}
	
	/**
	 * @param reportFormat
	 */
	public void setReportFormat(String reportFormat) {
		this.reportFormat = reportFormat;
	}
	
    /**
     * @return reportFormat
     */
	public String getReportFormat() {
		return this.reportFormat;
	}
	
	/**
	 * @param queryreasonId
	 */
	public void setQueryreasonId(String queryreasonId) {
		this.queryreasonId = queryreasonId;
	}
	
    /**
     * @return queryreasonId
     */
	public String getQueryreasonId() {
		return this.queryreasonId;
	}
	
	/**
	 * @param htmlPath
	 */
	public void setHtmlPath(String htmlPath) {
		this.htmlPath = htmlPath;
	}
	
    /**
     * @return htmlPath
     */
	public String getHtmlPath() {
		return this.htmlPath;
	}
	
	/**
	 * @param xmlPath
	 */
	public void setXmlPath(String xmlPath) {
		this.xmlPath = xmlPath;
	}
	
    /**
     * @return xmlPath
     */
	public String getXmlPath() {
		return this.xmlPath;
	}
	
	/**
	 * @param jsonPath
	 */
	public void setJsonPath(String jsonPath) {
		this.jsonPath = jsonPath;
	}
	
    /**
     * @return jsonPath
     */
	public String getJsonPath() {
		return this.jsonPath;
	}
	
	/**
	 * @param pdfPath
	 */
	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}
	
    /**
     * @return pdfPath
     */
	public String getPdfPath() {
		return this.pdfPath;
	}
	
	/**
	 * @param contextFilePath
	 */
	public void setContextFilePath(String contextFilePath) {
		this.contextFilePath = contextFilePath;
	}
	
    /**
     * @return contextFilePath
     */
	public String getContextFilePath() {
		return this.contextFilePath;
	}
	
	/**
	 * @param useTime
	 */
	public void setUseTime(java.math.BigDecimal useTime) {
		this.useTime = useTime;
	}
	
    /**
     * @return useTime
     */
	public java.math.BigDecimal getUseTime() {
		return this.useTime;
	}
	
	/**
	 * @param callSysUser
	 */
	public void setCallSysUser(String callSysUser) {
		this.callSysUser = callSysUser;
	}
	
    /**
     * @return callSysUser
     */
	public String getCallSysUser() {
		return this.callSysUser;
	}
	
	/**
	 * @param recheckUserName
	 */
	public void setRecheckUserName(String recheckUserName) {
		this.recheckUserName = recheckUserName;
	}
	
    /**
     * @return recheckUserName
     */
	public String getRecheckUserName() {
		return this.recheckUserName;
	}
	
	/**
	 * @param queryControlWarningType
	 */
	public void setQueryControlWarningType(String queryControlWarningType) {
		this.queryControlWarningType = queryControlWarningType;
	}
	
    /**
     * @return queryControlWarningType
     */
	public String getQueryControlWarningType() {
		return this.queryControlWarningType;
	}
	
	/**
	 * @param serialNumber
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
    /**
     * @return serialNumber
     */
	public String getSerialNumber() {
		return this.serialNumber;
	}
	
	/**
	 * @param batchQueryNo
	 */
	public void setBatchQueryNo(String batchQueryNo) {
		this.batchQueryNo = batchQueryNo;
	}
	
    /**
     * @return batchQueryNo
     */
	public String getBatchQueryNo() {
		return this.batchQueryNo;
	}
	
	/**
	 * @param ext1
	 */
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	
    /**
     * @return ext1
     */
	public String getExt1() {
		return this.ext1;
	}
	
	/**
	 * @param ext2
	 */
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	
    /**
     * @return ext2
     */
	public String getExt2() {
		return this.ext2;
	}
	
	/**
	 * @param ext3
	 */
	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}
	
    /**
     * @return ext3
     */
	public String getExt3() {
		return this.ext3;
	}
	
	/**
	 * @param ext4
	 */
	public void setExt4(String ext4) {
		this.ext4 = ext4;
	}
	
    /**
     * @return ext4
     */
	public String getExt4() {
		return this.ext4;
	}
	
	/**
	 * @param ext5
	 */
	public void setExt5(String ext5) {
		this.ext5 = ext5;
	}
	
    /**
     * @return ext5
     */
	public String getExt5() {
		return this.ext5;
	}
	
	/**
	 * @param ext6
	 */
	public void setExt6(String ext6) {
		this.ext6 = ext6;
	}
	
    /**
     * @return ext6
     */
	public String getExt6() {
		return this.ext6;
	}
	
	/**
	 * @param reportQueryDatetime
	 */
	public void setReportQueryDatetime(java.util.Date reportQueryDatetime) {
		this.reportQueryDatetime = reportQueryDatetime;
	}
	
    /**
     * @return reportQueryDatetime
     */
	public java.util.Date getReportQueryDatetime() {
		return this.reportQueryDatetime;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}