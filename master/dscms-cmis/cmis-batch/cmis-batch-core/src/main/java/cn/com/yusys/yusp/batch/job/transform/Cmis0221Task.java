package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0221Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepLmtEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0221</br>
 * 加工任务-额度处理-将额度相关临时表更新到额度相关表</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0221Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0221Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0221Service cmis0221Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息


    @Bean
    public Job cmis0221Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_JOB.key, JobStepLmtEnum.CMIS0221_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0221Job = this.jobBuilderFactory.get(JobStepLmtEnum.CMIS0221_JOB.key)
                .start(cmis0221UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0221CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(cmis0221LmtTmpAlsbiTcalsbiStep(WILL_BE_INJECTED))//根据授信分项额度比对表处理原始表
                .next(cmis0221LmtTmpLcrTclcrStep(WILL_BE_INJECTED))//根据授信分项占用关系额度比对表处理原始表
                .next(cmis0221LmtTmpCarTccarStep(WILL_BE_INJECTED))//根据台账占用合同关系额度比对表处理原始表
                .next(cmis0221LmtTmpLwiTclwiStep(WILL_BE_INJECTED))//根据白名单额度比对表处理原始表
                .next(cmis0221LmtTmpAcsiTcacsiStep(WILL_BE_INJECTED))//根据合作方额度比表处理原始表
                .next(cmis0221UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0221Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0221UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_UPDATE_TASK010_STEP.key, JobStepLmtEnum.CMIS0221_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0221UpdateTask010Step = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0221_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0221_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_UPDATE_TASK010_STEP.key, JobStepLmtEnum.CMIS0221_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0221UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0221CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0221_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0221CheckRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0221_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0221_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag) {
                        return RepeatStatus.FINISHED;
                    }
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0221_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0221_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0221CheckRelStep;
    }


    /**
     * 根据授信分项额度比对表处理原始表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0221LmtTmpAlsbiTcalsbiStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_LMT_TMP_ALSBI_TCALSBI_STEP.key, JobStepLmtEnum.CMIS0221_LMT_TMP_ALSBI_TCALSBI_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0221LmtTmpAlsbiTcalsbiStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0221_LMT_TMP_ALSBI_TCALSBI_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0221Service.cmis0221LmtTmpAlsbiTcalsbi(openDay);// 根据授信分项额度比对表处理原始表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_LMT_TMP_ALSBI_TCALSBI_STEP.key, JobStepLmtEnum.CMIS0221_LMT_TMP_ALSBI_TCALSBI_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0221LmtTmpAlsbiTcalsbiStep;
    }


    /**
     * 根据授信分项占用关系额度比对表处理原始表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0221LmtTmpLcrTclcrStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_LMT_TMP_LCR_TCLCR_STEP.key, JobStepLmtEnum.CMIS0221_LMT_TMP_LCR_TCLCR_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0221LmtTmpLcrTclcrStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0221_LMT_TMP_LCR_TCLCR_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0221Service.cmis0221LmtTmpLcrTclcr(openDay);// 根据授信分项占用关系额度比对表处理原始表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_LMT_TMP_LCR_TCLCR_STEP.key, JobStepLmtEnum.CMIS0221_LMT_TMP_LCR_TCLCR_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0221LmtTmpLcrTclcrStep;
    }


    /**
     * 根据台账占用合同关系额度比对表处理原始表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0221LmtTmpCarTccarStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_LMT_TMP_CAR_TCCAR_STEP.key, JobStepLmtEnum.CMIS0221_LMT_TMP_CAR_TCCAR_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0221LmtTmpCarTccarStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0221_LMT_TMP_CAR_TCCAR_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0221Service.cmis0221LmtTmpCarTccar(openDay); // 根据台账占用合同关系额度比对表处理原始表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_LMT_TMP_CAR_TCCAR_STEP.key, JobStepLmtEnum.CMIS0221_LMT_TMP_CAR_TCCAR_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0221LmtTmpCarTccarStep;
    }


    /**
     * 根据白名单额度比对表处理原始表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0221LmtTmpLwiTclwiStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_LMT_TMP_LWI_TCLWI_STEP.key, JobStepLmtEnum.CMIS0221_LMT_TMP_LWI_TCLWI_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0221LmtTmpLwiTclwiStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0221_LMT_TMP_LWI_TCLWI_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0221Service.cmis0221LmtTmpLwiTclwi(openDay); //根据白名单额度比对表处理原始表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_LMT_TMP_LWI_TCLWI_STEP.key, JobStepLmtEnum.CMIS0221_LMT_TMP_LWI_TCLWI_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0221LmtTmpLwiTclwiStep;
    }


    /**
     * 根据合作方额度比表处理原始表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0221LmtTmpAcsiTcacsiStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_LMT_TMP_ACSI_TCACSI_STEP.key, JobStepLmtEnum.CMIS0221_LMT_TMP_ACSI_TCACSI_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0221LmtTmpAcsiTcacsiStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0221_LMT_TMP_ACSI_TCACSI_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0221Service.cmis0221LmtTmpAcsiTcacsi(openDay); // 根据合作方额度比表处理原始表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_LMT_TMP_ACSI_TCACSI_STEP.key, JobStepLmtEnum.CMIS0221_LMT_TMP_ACSI_TCACSI_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0221LmtTmpAcsiTcacsiStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0221UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_UPDATE_TASK100_STEP.key, JobStepLmtEnum.CMIS0221_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0221UpdateTask100Step = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0221_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0221_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0221_UPDATE_TASK100_STEP.key, JobStepLmtEnum.CMIS0221_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0221UpdateTask100Step;
    }


}
