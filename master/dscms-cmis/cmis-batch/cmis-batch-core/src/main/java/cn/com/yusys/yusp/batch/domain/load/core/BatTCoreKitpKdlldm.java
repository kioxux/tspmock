/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTCoreKitpKdlldm
 * @类描述: bat_t_core_kitp_kdlldm数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_core_kitp_kdlldm")
public class BatTCoreKitpKdlldm extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 法人代码
     **/
    @Id
    @Column(name = "farendma")
    private String farendma;
    /**
     * 分行代码
     **/
    @Id
    @Column(name = "FENHDAIM")
    private String fenhdaim;
    /**
     * 机构号
     **/
    @Id
    @Column(name = "JIGOUHAO")
    private String jigouhao;
    /**
     * 期次
     **/
    @Id
    @Column(name = "QICIIIII")
    private String qiciiiii;
    /**
     * 利率编号类型
     **/
    @Id
    @Column(name = "LILVBHLX")
    private String lilvbhlx;
    /**
     * 利率代码
     **/
    @Id
    @Column(name = "LILVDAMA")
    private String lilvdama;
    /**
     * 币种
     **/
    @Id
    @Column(name = "BIZHONGG")
    private String bizhongg;
    /**
     * 存期天数
     **/
    @Id
    @Column(name = "CUNQITSH")
    private Long cunqitsh;
    /**
     * 存期
     **/
    @Id
    @Column(name = "CUNQIIII")
    private String cunqiiii;
    /**
     * 浮动周期
     **/
    @Id
    @Column(name = "FDZHOUQI")
    private String fdzhouqi;
    /**
     * 利率档次
     **/
    @Id
    @Column(name = "LILVDANC")
    private java.math.BigDecimal lilvdanc;
    /**
     * 浮动范围设置方式
     **/
    @Column(name = "FDFWSZFS", unique = false, nullable = true, length = 1)
    private String fdfwszfs;
    /**
     * 利率参数类型
     **/
    @Column(name = "LILVCSLX", unique = false, nullable = true, length = 1)
    private String lilvcslx;
    /**
     * 参考利率
     **/
    @Column(name = "CKLILVVV", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal cklilvvv;
    /**
     * 参考利率代码
     **/
    @Column(name = "CKLILVDM", unique = false, nullable = true, length = 20)
    private String cklilvdm;
    /**
     * 参考利率编号类型
     **/
    @Column(name = "CKLLBHLX", unique = false, nullable = true, length = 1)
    private String ckllbhlx;

    /**
     * 参考利率存期
     **/
    @Column(name = "CKCUNQII", unique = false, nullable = true, length = 6)
    private String ckcunqii;

    /**
     * 参考利率档次
     **/
    @Column(name = "CKLILVDC", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal cklilvdc;

    /**
     * 参考浮动周期
     **/
    @Column(name = "CKFDZHQI", unique = false, nullable = true, length = 8)
    private String ckfdzhqi;

    /**
     * 利率浮动类型
     **/
    @Column(name = "LILVFDLX", unique = false, nullable = true, length = 1)
    private String lilvfdlx;

    /**
     * 利率浮动点数
     **/
    @Column(name = "LILVFDZH", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal lilvfdzh;

    /**
     * 利率浮动比例
     **/
    @Column(name = "LILVFDBL", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal lilvfdbl;

    /**
     * 浮动范围同步标志
     **/
    @Column(name = "FDFWTBBZ", unique = false, nullable = true, length = 1)
    private String fdfwtbbz;

    /**
     * 利率上浮最大点数
     **/
    @Column(name = "LLSFZDDS", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal llsfzdds;

    /**
     * 利率下浮最大点数
     **/
    @Column(name = "LLXFZDDS", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal llxfzdds;

    /**
     * 利率上浮最大百分比
     **/
    @Column(name = "LLSFBFBI", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal llsfbfbi;

    /**
     * 利率下浮最大百分比
     **/
    @Column(name = "LLXFBFBI", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal llxfbfbi;

    /**
     * 最大利率
     **/
    @Column(name = "ZUIDLILV", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal zuidlilv;

    /**
     * 最小利率
     **/
    @Column(name = "ZUIXLILV", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal zuixlilv;
    /**
     * 生效日期
     **/
    @Id
    @Column(name = "SHENGXRQ")
    private String shengxrq;


    /**
     * 年月利率标志
     **/
    @Column(name = "NYLILVBZ", unique = false, nullable = true, length = 1)
    private String nylilvbz;

    /**
     * 是否允许优惠
     **/
    @Column(name = "YUNXUYHU", unique = false, nullable = true, length = 1)
    private String yunxuyhu;

    /**
     * 优惠规则适用方式
     **/
    @Column(name = "YHGZSYFS", unique = false, nullable = true, length = 1)
    private String yhgzsyfs;

    /**
     * 存期备注
     **/
    @Column(name = "CQBEIZHU", unique = false, nullable = true, length = 300)
    private String cqbeizhu;

    /**
     * 备注信息
     **/
    @Column(name = "BEIZHUXX", unique = false, nullable = true, length = 300)
    private String beizhuxx;

    /**
     * 分行标识
     **/
    @Column(name = "FENHBIOS", unique = false, nullable = false, length = 4)
    private String fenhbios;

    /**
     * 维护柜员
     **/
    @Column(name = "WEIHGUIY", unique = false, nullable = false, length = 8)
    private String weihguiy;

    /**
     * 维护机构
     **/
    @Column(name = "weihjigo", unique = false, nullable = false, length = 12)
    private String weihjigo;

    /**
     * 维护日期
     **/
    @Column(name = "WEIHRIQI", unique = false, nullable = false, length = 8)
    private String weihriqi;

    /**
     * 维护时间
     **/
    @Column(name = "WEIHSHIJ", unique = false, nullable = true, length = 9)
    private String weihshij;

    /**
     * 时间戳
     **/
    @Column(name = "SHIJCHUO", unique = false, nullable = false, length = 19)
    private Long shijchuo;

    /**
     * 记录状态 STD_JILUZTAI
     **/
    @Column(name = "JILUZTAI", unique = false, nullable = false, length = 1)
    private String jiluztai;


    /**
     * @param farendma
     */
    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param fenhdaim
	 */
	public void setFenhdaim(String fenhdaim) {
		this.fenhdaim = fenhdaim;
	}
	
    /**
     * @return fenhdaim
     */
	public String getFenhdaim() {
		return this.fenhdaim;
	}
	
	/**
	 * @param jigouhao
	 */
	public void setJigouhao(String jigouhao) {
		this.jigouhao = jigouhao;
	}
	
    /**
     * @return jigouhao
     */
	public String getJigouhao() {
		return this.jigouhao;
	}
	
	/**
	 * @param qiciiiii
	 */
	public void setQiciiiii(String qiciiiii) {
		this.qiciiiii = qiciiiii;
	}
	
    /**
     * @return qiciiiii
     */
	public String getQiciiiii() {
		return this.qiciiiii;
	}
	
	/**
	 * @param lilvbhlx
	 */
	public void setLilvbhlx(String lilvbhlx) {
		this.lilvbhlx = lilvbhlx;
	}
	
    /**
     * @return lilvbhlx
     */
	public String getLilvbhlx() {
		return this.lilvbhlx;
	}
	
	/**
	 * @param lilvdama
	 */
	public void setLilvdama(String lilvdama) {
		this.lilvdama = lilvdama;
	}
	
    /**
     * @return lilvdama
     */
	public String getLilvdama() {
		return this.lilvdama;
	}
	
	/**
	 * @param bizhongg
	 */
	public void setBizhongg(String bizhongg) {
		this.bizhongg = bizhongg;
	}
	
    /**
     * @return bizhongg
     */
	public String getBizhongg() {
		return this.bizhongg;
	}
	
	/**
	 * @param cunqitsh
	 */
	public void setCunqitsh(Long cunqitsh) {
		this.cunqitsh = cunqitsh;
	}
	
    /**
     * @return cunqitsh
     */
	public Long getCunqitsh() {
		return this.cunqitsh;
	}
	
	/**
	 * @param cunqiiii
	 */
	public void setCunqiiii(String cunqiiii) {
		this.cunqiiii = cunqiiii;
	}
	
    /**
     * @return cunqiiii
     */
	public String getCunqiiii() {
		return this.cunqiiii;
	}
	
	/**
	 * @param fdzhouqi
	 */
	public void setFdzhouqi(String fdzhouqi) {
		this.fdzhouqi = fdzhouqi;
	}
	
    /**
     * @return fdzhouqi
     */
	public String getFdzhouqi() {
		return this.fdzhouqi;
	}
	
	/**
	 * @param lilvdanc
	 */
	public void setLilvdanc(java.math.BigDecimal lilvdanc) {
		this.lilvdanc = lilvdanc;
	}
	
    /**
     * @return lilvdanc
     */
	public java.math.BigDecimal getLilvdanc() {
		return this.lilvdanc;
	}
	
	/**
	 * @param fdfwszfs
	 */
	public void setFdfwszfs(String fdfwszfs) {
		this.fdfwszfs = fdfwszfs;
	}
	
    /**
     * @return fdfwszfs
     */
	public String getFdfwszfs() {
		return this.fdfwszfs;
	}
	
	/**
	 * @param lilvcslx
	 */
	public void setLilvcslx(String lilvcslx) {
		this.lilvcslx = lilvcslx;
	}
	
    /**
     * @return lilvcslx
     */
	public String getLilvcslx() {
		return this.lilvcslx;
	}
	
	/**
	 * @param cklilvvv
	 */
	public void setCklilvvv(java.math.BigDecimal cklilvvv) {
		this.cklilvvv = cklilvvv;
	}
	
    /**
     * @return cklilvvv
     */
	public java.math.BigDecimal getCklilvvv() {
		return this.cklilvvv;
	}
	
	/**
	 * @param cklilvdm
	 */
	public void setCklilvdm(String cklilvdm) {
		this.cklilvdm = cklilvdm;
	}
	
    /**
     * @return cklilvdm
     */
	public String getCklilvdm() {
		return this.cklilvdm;
	}
	
	/**
	 * @param ckllbhlx
	 */
	public void setCkllbhlx(String ckllbhlx) {
		this.ckllbhlx = ckllbhlx;
	}
	
    /**
     * @return ckllbhlx
     */
	public String getCkllbhlx() {
		return this.ckllbhlx;
	}
	
	/**
	 * @param ckcunqii
	 */
	public void setCkcunqii(String ckcunqii) {
		this.ckcunqii = ckcunqii;
	}
	
    /**
     * @return ckcunqii
     */
	public String getCkcunqii() {
		return this.ckcunqii;
	}
	
	/**
	 * @param cklilvdc
	 */
	public void setCklilvdc(java.math.BigDecimal cklilvdc) {
		this.cklilvdc = cklilvdc;
	}
	
    /**
     * @return cklilvdc
     */
	public java.math.BigDecimal getCklilvdc() {
		return this.cklilvdc;
	}
	
	/**
	 * @param ckfdzhqi
	 */
	public void setCkfdzhqi(String ckfdzhqi) {
		this.ckfdzhqi = ckfdzhqi;
	}
	
    /**
     * @return ckfdzhqi
     */
	public String getCkfdzhqi() {
		return this.ckfdzhqi;
	}
	
	/**
	 * @param lilvfdlx
	 */
	public void setLilvfdlx(String lilvfdlx) {
		this.lilvfdlx = lilvfdlx;
	}
	
    /**
     * @return lilvfdlx
     */
	public String getLilvfdlx() {
		return this.lilvfdlx;
	}
	
	/**
	 * @param lilvfdzh
	 */
	public void setLilvfdzh(java.math.BigDecimal lilvfdzh) {
		this.lilvfdzh = lilvfdzh;
	}
	
    /**
     * @return lilvfdzh
     */
	public java.math.BigDecimal getLilvfdzh() {
		return this.lilvfdzh;
	}
	
	/**
	 * @param lilvfdbl
	 */
	public void setLilvfdbl(java.math.BigDecimal lilvfdbl) {
		this.lilvfdbl = lilvfdbl;
	}
	
    /**
     * @return lilvfdbl
     */
	public java.math.BigDecimal getLilvfdbl() {
		return this.lilvfdbl;
	}
	
	/**
	 * @param fdfwtbbz
	 */
	public void setFdfwtbbz(String fdfwtbbz) {
		this.fdfwtbbz = fdfwtbbz;
	}
	
    /**
     * @return fdfwtbbz
     */
	public String getFdfwtbbz() {
		return this.fdfwtbbz;
	}
	
	/**
	 * @param llsfzdds
	 */
	public void setLlsfzdds(java.math.BigDecimal llsfzdds) {
		this.llsfzdds = llsfzdds;
	}
	
    /**
     * @return llsfzdds
     */
	public java.math.BigDecimal getLlsfzdds() {
		return this.llsfzdds;
	}
	
	/**
	 * @param llxfzdds
	 */
	public void setLlxfzdds(java.math.BigDecimal llxfzdds) {
		this.llxfzdds = llxfzdds;
	}
	
    /**
     * @return llxfzdds
     */
	public java.math.BigDecimal getLlxfzdds() {
		return this.llxfzdds;
	}
	
	/**
	 * @param llsfbfbi
	 */
	public void setLlsfbfbi(java.math.BigDecimal llsfbfbi) {
		this.llsfbfbi = llsfbfbi;
	}
	
    /**
     * @return llsfbfbi
     */
	public java.math.BigDecimal getLlsfbfbi() {
		return this.llsfbfbi;
	}
	
	/**
	 * @param llxfbfbi
	 */
	public void setLlxfbfbi(java.math.BigDecimal llxfbfbi) {
		this.llxfbfbi = llxfbfbi;
	}
	
    /**
     * @return llxfbfbi
     */
	public java.math.BigDecimal getLlxfbfbi() {
		return this.llxfbfbi;
	}
	
	/**
	 * @param zuidlilv
	 */
	public void setZuidlilv(java.math.BigDecimal zuidlilv) {
		this.zuidlilv = zuidlilv;
	}
	
    /**
     * @return zuidlilv
     */
	public java.math.BigDecimal getZuidlilv() {
		return this.zuidlilv;
	}
	
	/**
	 * @param zuixlilv
	 */
	public void setZuixlilv(java.math.BigDecimal zuixlilv) {
		this.zuixlilv = zuixlilv;
	}
	
    /**
     * @return zuixlilv
     */
	public java.math.BigDecimal getZuixlilv() {
		return this.zuixlilv;
	}
	
	/**
	 * @param shengxrq
	 */
	public void setShengxrq(String shengxrq) {
		this.shengxrq = shengxrq;
	}
	
    /**
     * @return shengxrq
     */
	public String getShengxrq() {
		return this.shengxrq;
	}
	
	/**
	 * @param nylilvbz
	 */
	public void setNylilvbz(String nylilvbz) {
		this.nylilvbz = nylilvbz;
	}
	
    /**
     * @return nylilvbz
     */
	public String getNylilvbz() {
		return this.nylilvbz;
	}
	
	/**
	 * @param yunxuyhu
	 */
	public void setYunxuyhu(String yunxuyhu) {
		this.yunxuyhu = yunxuyhu;
	}
	
    /**
     * @return yunxuyhu
     */
	public String getYunxuyhu() {
		return this.yunxuyhu;
	}
	
	/**
	 * @param yhgzsyfs
	 */
	public void setYhgzsyfs(String yhgzsyfs) {
		this.yhgzsyfs = yhgzsyfs;
	}
	
    /**
     * @return yhgzsyfs
     */
	public String getYhgzsyfs() {
		return this.yhgzsyfs;
	}
	
	/**
	 * @param cqbeizhu
	 */
	public void setCqbeizhu(String cqbeizhu) {
		this.cqbeizhu = cqbeizhu;
	}
	
    /**
     * @return cqbeizhu
     */
	public String getCqbeizhu() {
		return this.cqbeizhu;
	}
	
	/**
	 * @param beizhuxx
	 */
	public void setBeizhuxx(String beizhuxx) {
		this.beizhuxx = beizhuxx;
	}
	
    /**
     * @return beizhuxx
     */
	public String getBeizhuxx() {
		return this.beizhuxx;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}


}