package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0424</br>
 * 任务名称：加工任务-贷后管理-业务变更风险分类  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0424Mapper {
    /**
     * 借新还旧 、收回再贷 、展期、垫款 产生分类任务，是按照客户来生成
     *
     * @param openDay
     * @return
     */
    int insertPspRiskDgyw01(@Param("openDay") String openDay);


    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    //int insertPspRiskGrjyx01(@Param("openDay") String openDay);


    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    //int insertPspRiskGrxfx01(@Param("openDay") String openDay);


}
