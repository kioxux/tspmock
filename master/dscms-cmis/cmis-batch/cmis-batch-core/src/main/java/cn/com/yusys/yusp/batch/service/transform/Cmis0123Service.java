package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0123Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0123</br>
 * 任务名称：加工任务-业务处理-资金同业授信客户准入名单年审提醒 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0123Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0123Service.class);

    @Autowired
    private Cmis0123Mapper cmis0123Mapper;

    /**
     * 插入消息提示表 ：资金同业客户 准入名单年审即将到期
     *
     * @param openDay
     */
    public void cmis0123InsertWbMsgNotice(String openDay) {
        logger.info("插入消息提示表 ：资金同业客户 准入名单年审即将到期开始,请求参数为:[{}]", openDay);
        int insertWbMsgNotice = cmis0123Mapper.insertWbMsgNotice(openDay);
        logger.info("插入消息提示表 ：资金同业客户 准入名单年审即将到期结束,返回参数为:[{}]", insertWbMsgNotice);
    }


    /**
     * 插入消息提示表 ：资金同业客户 准入名单年审即将到期
     *
     * @param openDay
     */
    public void cmis0123InsertIntbankOrgAdmitApp(String openDay) {
        logger.info("插入消息提示表 ：资金同业客户 准入名单年审任务生成开始,请求参数为:[{}]", openDay);
        int insertIntbankOrgAdmitApp = cmis0123Mapper.insertIntbankOrgAdmitApp(openDay);
        logger.info("插入消息提示表 ：资金同业客户 准入名单年审任务生成结束,返回参数为:[{}]", insertIntbankOrgAdmitApp);
    }


    /**
     * 插入消息提示表 ：更新同业机构准入台账表的状态
     *
     * @param openDay
     */
    public void cmis0123UpdateInbankOrgAdmitAcc(String openDay) {
        logger.info(" updateInbankOrgAdmitAcc 更新同业机构准入台账表,请求参数为:[{}]", openDay);
        int updateInbankOrgAdmitAcc = cmis0123Mapper.updateInbankOrgAdmitAcc(openDay);
        logger.info(" updateInbankOrgAdmitAcc 更新同业机构准入台账表,返回参数为:[{}]", updateInbankOrgAdmitAcc);
    }
}
