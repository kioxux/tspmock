/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.gjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTGjpTfbRateFxsOur
 * @类描述: bat_t_gjp_tfb_rate_fxs_our数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:40:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_gjp_tfb_rate_fxs_our")
public class BatTGjpTfbRateFxsOur extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 货币对 **/
	@Column(name = "BASE_CCY", unique = false, nullable = true, length = 3)
	private String baseCcy;
	
	/** 币种 **/
	@Column(name = "CCY", unique = false, nullable = true, length = 3)
	private String ccy;
	
	/** 汇率单位 **/
	@Column(name = "RATE_UNIT", unique = false, nullable = true, length = 12)
	private java.math.BigDecimal rateUnit;
	
	/** 汇中间价 **/
	@Column(name = "MID_RATE", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal midRate;
	
	/** 汇卖价 **/
	@Column(name = "RSP", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal rsp;
	
	/** 汇买价 **/
	@Column(name = "RPP", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal rpp;
	
	/** 钞卖价 **/
	@Column(name = "CSP", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal csp;
	
	/** 钞买价 **/
	@Column(name = "CPP", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal cpp;
	
	/** 钞中间价 **/
	@Column(name = "CMP", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal cmp;
	
	/** 牌价日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 8)
	private String updDate;
	
	/** 更新时间 **/
	@Column(name = "UPD_TIME", unique = false, nullable = true, length = 8)
	private String updTime;
	
	/** 渠道 **/
	@Column(name = "CHANNEL", unique = false, nullable = true, length = 8)
	private String channel;
	
	/** 期限 **/
	@Column(name = "TENOR", unique = false, nullable = true, length = 12)
	private String tenor;
	
	/** 渠道 **/
	@Column(name = "MARKET", unique = false, nullable = true, length = 12)
	private String market;
	
	/** 市场买价 **/
	@Column(name = "SRC_BID", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal srcBid;
	
	/** 市场卖价 **/
	@Column(name = "SRC_ASK", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal srcAsk;
	
	/** 更新ID **/
	@Column(name = "REFRESH_ID", unique = false, nullable = true, length = 32)
	private String refreshId;
	
	/** 更新时间 **/
	@Column(name = "REFRESH_DT", unique = false, nullable = true, length = 32)
	private String refreshDt;
	
	/** 最后变更时间 **/
	@Column(name = "LAST_REFRESH_DT", unique = false, nullable = true, length = 32)
	private String lastRefreshDt;
	
	/** 手动更新加锁标志 **/
	@Column(name = "MANU_LOCK", unique = false, nullable = true, length = 1)
	private String manuLock;
	
	/** 手动更新日期 **/
	@Column(name = "MANU_DATE", unique = false, nullable = true, length = 8)
	private String manuDate;
	
	/** 创建人 **/
	@Column(name = "SYS_CRT_USER", unique = false, nullable = true, length = 32)
	private String sysCrtUser;
	
	/** 创建日期 **/
	@Column(name = "SYS_CRT_DT", unique = false, nullable = true, length = 60)
	private String sysCrtDt;
	
	/** 修改人编号 **/
	@Column(name = "SYS_MODIFY_USER", unique = false, nullable = true, length = 32)
	private String sysModifyUser;
	
	/** 修改日期 **/
	@Column(name = "SYS_MODIFY_DT", unique = false, nullable = true, length = 10)
	private String sysModifyDt;
	
	/** 系统实体编号 **/
	@Column(name = "SYS_ENTY_ID", unique = false, nullable = true, length = 32)
	private String sysEntyId;
	
	/** 系统删除标识 STD_SYS_DEL_FLG **/
	@Column(name = "SYS_DEL_FLG", unique = false, nullable = true, length = 1)
	private String sysDelFlg;
	
	/** 基准价 **/
	@Column(name = "BASE_RATE", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal baseRate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param baseCcy
	 */
	public void setBaseCcy(String baseCcy) {
		this.baseCcy = baseCcy;
	}
	
    /**
     * @return baseCcy
     */
	public String getBaseCcy() {
		return this.baseCcy;
	}
	
	/**
	 * @param ccy
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	
    /**
     * @return ccy
     */
	public String getCcy() {
		return this.ccy;
	}
	
	/**
	 * @param rateUnit
	 */
	public void setRateUnit(java.math.BigDecimal rateUnit) {
		this.rateUnit = rateUnit;
	}
	
    /**
     * @return rateUnit
     */
	public java.math.BigDecimal getRateUnit() {
		return this.rateUnit;
	}
	
	/**
	 * @param midRate
	 */
	public void setMidRate(java.math.BigDecimal midRate) {
		this.midRate = midRate;
	}
	
    /**
     * @return midRate
     */
	public java.math.BigDecimal getMidRate() {
		return this.midRate;
	}
	
	/**
	 * @param rsp
	 */
	public void setRsp(java.math.BigDecimal rsp) {
		this.rsp = rsp;
	}
	
    /**
     * @return rsp
     */
	public java.math.BigDecimal getRsp() {
		return this.rsp;
	}
	
	/**
	 * @param rpp
	 */
	public void setRpp(java.math.BigDecimal rpp) {
		this.rpp = rpp;
	}
	
    /**
     * @return rpp
     */
	public java.math.BigDecimal getRpp() {
		return this.rpp;
	}
	
	/**
	 * @param csp
	 */
	public void setCsp(java.math.BigDecimal csp) {
		this.csp = csp;
	}
	
    /**
     * @return csp
     */
	public java.math.BigDecimal getCsp() {
		return this.csp;
	}
	
	/**
	 * @param cpp
	 */
	public void setCpp(java.math.BigDecimal cpp) {
		this.cpp = cpp;
	}
	
    /**
     * @return cpp
     */
	public java.math.BigDecimal getCpp() {
		return this.cpp;
	}
	
	/**
	 * @param cmp
	 */
	public void setCmp(java.math.BigDecimal cmp) {
		this.cmp = cmp;
	}
	
    /**
     * @return cmp
     */
	public java.math.BigDecimal getCmp() {
		return this.cmp;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param updTime
	 */
	public void setUpdTime(String updTime) {
		this.updTime = updTime;
	}
	
    /**
     * @return updTime
     */
	public String getUpdTime() {
		return this.updTime;
	}
	
	/**
	 * @param channel
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
    /**
     * @return channel
     */
	public String getChannel() {
		return this.channel;
	}
	
	/**
	 * @param tenor
	 */
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	
    /**
     * @return tenor
     */
	public String getTenor() {
		return this.tenor;
	}
	
	/**
	 * @param market
	 */
	public void setMarket(String market) {
		this.market = market;
	}
	
    /**
     * @return market
     */
	public String getMarket() {
		return this.market;
	}
	
	/**
	 * @param srcBid
	 */
	public void setSrcBid(java.math.BigDecimal srcBid) {
		this.srcBid = srcBid;
	}
	
    /**
     * @return srcBid
     */
	public java.math.BigDecimal getSrcBid() {
		return this.srcBid;
	}
	
	/**
	 * @param srcAsk
	 */
	public void setSrcAsk(java.math.BigDecimal srcAsk) {
		this.srcAsk = srcAsk;
	}
	
    /**
     * @return srcAsk
     */
	public java.math.BigDecimal getSrcAsk() {
		return this.srcAsk;
	}
	
	/**
	 * @param refreshId
	 */
	public void setRefreshId(String refreshId) {
		this.refreshId = refreshId;
	}
	
    /**
     * @return refreshId
     */
	public String getRefreshId() {
		return this.refreshId;
	}
	
	/**
	 * @param refreshDt
	 */
	public void setRefreshDt(String refreshDt) {
		this.refreshDt = refreshDt;
	}
	
    /**
     * @return refreshDt
     */
	public String getRefreshDt() {
		return this.refreshDt;
	}
	
	/**
	 * @param lastRefreshDt
	 */
	public void setLastRefreshDt(String lastRefreshDt) {
		this.lastRefreshDt = lastRefreshDt;
	}
	
    /**
     * @return lastRefreshDt
     */
	public String getLastRefreshDt() {
		return this.lastRefreshDt;
	}
	
	/**
	 * @param manuLock
	 */
	public void setManuLock(String manuLock) {
		this.manuLock = manuLock;
	}
	
    /**
     * @return manuLock
     */
	public String getManuLock() {
		return this.manuLock;
	}
	
	/**
	 * @param manuDate
	 */
	public void setManuDate(String manuDate) {
		this.manuDate = manuDate;
	}
	
    /**
     * @return manuDate
     */
	public String getManuDate() {
		return this.manuDate;
	}
	
	/**
	 * @param sysCrtUser
	 */
	public void setSysCrtUser(String sysCrtUser) {
		this.sysCrtUser = sysCrtUser;
	}
	
    /**
     * @return sysCrtUser
     */
	public String getSysCrtUser() {
		return this.sysCrtUser;
	}
	
	/**
	 * @param sysCrtDt
	 */
	public void setSysCrtDt(String sysCrtDt) {
		this.sysCrtDt = sysCrtDt;
	}
	
    /**
     * @return sysCrtDt
     */
	public String getSysCrtDt() {
		return this.sysCrtDt;
	}
	
	/**
	 * @param sysModifyUser
	 */
	public void setSysModifyUser(String sysModifyUser) {
		this.sysModifyUser = sysModifyUser;
	}
	
    /**
     * @return sysModifyUser
     */
	public String getSysModifyUser() {
		return this.sysModifyUser;
	}
	
	/**
	 * @param sysModifyDt
	 */
	public void setSysModifyDt(String sysModifyDt) {
		this.sysModifyDt = sysModifyDt;
	}
	
    /**
     * @return sysModifyDt
     */
	public String getSysModifyDt() {
		return this.sysModifyDt;
	}
	
	/**
	 * @param sysEntyId
	 */
	public void setSysEntyId(String sysEntyId) {
		this.sysEntyId = sysEntyId;
	}
	
    /**
     * @return sysEntyId
     */
	public String getSysEntyId() {
		return this.sysEntyId;
	}
	
	/**
	 * @param sysDelFlg
	 */
	public void setSysDelFlg(String sysDelFlg) {
		this.sysDelFlg = sysDelFlg;
	}
	
    /**
     * @return sysDelFlg
     */
	public String getSysDelFlg() {
		return this.sysDelFlg;
	}
	
	/**
	 * @param baseRate
	 */
	public void setBaseRate(java.math.BigDecimal baseRate) {
		this.baseRate = baseRate;
	}
	
    /**
     * @return baseRate
     */
	public java.math.BigDecimal getBaseRate() {
		return this.baseRate;
	}


}