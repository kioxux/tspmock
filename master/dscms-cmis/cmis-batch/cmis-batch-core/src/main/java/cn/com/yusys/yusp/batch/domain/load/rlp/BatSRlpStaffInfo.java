/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rlp;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRlpStaffInfo
 * @类描述: bat_s_rlp_staff_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-23 03:14:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_rlp_staff_info")
public class BatSRlpStaffInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_STAFF_INFO")
	private String pkStaffInfo;
	
	/** 工号（行员号） **/
	@Id
	@Column(name = "PSNCODE")
	private String psncode;
	
	/** 姓名 **/
	@Column(name = "PSNNAME", unique = false, nullable = true, length = 100)
	private String psnname;
	
	/** 人力编制部门号 **/
	@Column(name = "HRDEPTCODE", unique = false, nullable = true, length = 20)
	private String hrdeptcode;
	
	/** 人力编制部门 **/
	@Column(name = "HRDEPTNAME", unique = false, nullable = true, length = 100)
	private String hrdeptname;
	
	/** 人力工作部门编号 **/
	@Column(name = "HRWORKDEPTCODE", unique = false, nullable = true, length = 20)
	private String hrworkdeptcode;
	
	/** 人力工作部门 **/
	@Column(name = "HRWORKDEPTNAME", unique = false, nullable = true, length = 100)
	private String hrworkdeptname;
	
	/** 核心编制部门编号 **/
	@Column(name = "SYSDEPTCODE", unique = false, nullable = true, length = 20)
	private String sysdeptcode;
	
	/** 核心编制部门 **/
	@Column(name = "SYSDEPTNAME", unique = false, nullable = true, length = 100)
	private String sysdeptname;
	
	/** 核心工作部门编号 **/
	@Column(name = "SYSWORKDEPTCODE", unique = false, nullable = true, length = 20)
	private String sysworkdeptcode;
	
	/** 核心工作部门 **/
	@Column(name = "SYSWORKDEPTNAME", unique = false, nullable = true, length = 100)
	private String sysworkdeptname;
	
	/** 岗位编号 **/
	@Column(name = "JOBCODE", unique = false, nullable = true, length = 20)
	private String jobcode;
	
	/** 岗位 **/
	@Column(name = "JOBNAME", unique = false, nullable = true, length = 100)
	private String jobname;
	
	/** 职务编号 **/
	@Column(name = "DUTYRANKCODE", unique = false, nullable = true, length = 20)
	private String dutyrankcode;
	
	/** 职务 **/
	@Column(name = "DUTYRANKNAME", unique = false, nullable = true, length = 100)
	private String dutyrankname;
	
	/** 员工状态编号 **/
	@Column(name = "PSNSTAT", unique = false, nullable = true, length = 20)
	private String psnstat;
	
	/** 工作状态编号 **/
	@Column(name = "WORKSTAT", unique = false, nullable = true, length = 20)
	private String workstat;
	
	/** 用工性质编号 **/
	@Column(name = "PSNCL", unique = false, nullable = true, length = 20)
	private String psncl;
	
	/** 证件类型编号 **/
	@Column(name = "CARDIDTYPE", unique = false, nullable = true, length = 10)
	private String cardidtype;
	
	/** 证件号 **/
	@Column(name = "CARDID", unique = false, nullable = true, length = 30)
	private String cardid;
	
	/** 手机号 **/
	@Column(name = "MOBILE", unique = false, nullable = true, length = 20)
	private String mobile;
	
	/** 微信号 **/
	@Column(name = "WECHAT", unique = false, nullable = true, length = 30)
	private String wechat;
	
	/** 邮箱 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 30)
	private String email;
	
	/** 性别编号 STD_RZ_XB **/
	@Column(name = "SEX", unique = false, nullable = true, length = 1)
	private String sex;
	
	/** 出生日期 **/
	@Column(name = "BIRTHDAY", unique = false, nullable = true, length = 10)
	private String birthday;
	
	/** 年龄 **/
	@Column(name = "AGE", unique = false, nullable = true, length = 10)
	private Integer age;
	
	/** 政治面貌编号 **/
	@Column(name = "POLITICS", unique = false, nullable = true, length = 20)
	private String politics;
	
	/** 学历编号 **/
	@Column(name = "EDUCATION", unique = false, nullable = true, length = 20)
	private String education;
	
	/** 职称编号 **/
	@Column(name = "TITLE", unique = false, nullable = true, length = 20)
	private String title;
	
	/** 参加工作时间 **/
	@Column(name = "WORKDATE", unique = false, nullable = true, length = 10)
	private String workdate;
	
	/** 入行时间 **/
	@Column(name = "BANKDATE", unique = false, nullable = true, length = 10)
	private String bankdate;
	
	/** 工龄 **/
	@Column(name = "WORKAGE", unique = false, nullable = true, length = 10)
	private Integer workage;
	
	/** 行龄 **/
	@Column(name = "BANKAGE", unique = false, nullable = true, length = 10)
	private Integer bankage;
	
	/** 上柜权限编号 **/
	@Column(name = "CNTSTAT", unique = false, nullable = true, length = 1)
	private String cntstat;
	
	/** 营销证书到期日 **/
	@Column(name = "SALECERTDATE", unique = false, nullable = true, length = 10)
	private String salecertdate;
	
	/** 反假证书到期日 **/
	@Column(name = "ANTICERTDATE", unique = false, nullable = true, length = 10)
	private String anticertdate;
	
	/** 营销证书编号 **/
	@Column(name = "SALECERTNO", unique = false, nullable = true, length = 20)
	private String salecertno;
	
	/** 反假证书编号 **/
	@Column(name = "ANTICERTNO", unique = false, nullable = true, length = 20)
	private String anticertno;
	
	/** 工资卡号 **/
	@Column(name = "SALARYCARD", unique = false, nullable = true, length = 30)
	private String salarycard;
	
	/** 征信查询权限 **/
	@Column(name = "CREDITAUTH", unique = false, nullable = true, length = 1)
	private String creditauth;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param pkStaffInfo
	 */
	public void setPkStaffInfo(String pkStaffInfo) {
		this.pkStaffInfo = pkStaffInfo;
	}
	
    /**
     * @return pkStaffInfo
     */
	public String getPkStaffInfo() {
		return this.pkStaffInfo;
	}
	
	/**
	 * @param psncode
	 */
	public void setPsncode(String psncode) {
		this.psncode = psncode;
	}
	
    /**
     * @return psncode
     */
	public String getPsncode() {
		return this.psncode;
	}
	
	/**
	 * @param psnname
	 */
	public void setPsnname(String psnname) {
		this.psnname = psnname;
	}
	
    /**
     * @return psnname
     */
	public String getPsnname() {
		return this.psnname;
	}
	
	/**
	 * @param hrdeptcode
	 */
	public void setHrdeptcode(String hrdeptcode) {
		this.hrdeptcode = hrdeptcode;
	}
	
    /**
     * @return hrdeptcode
     */
	public String getHrdeptcode() {
		return this.hrdeptcode;
	}
	
	/**
	 * @param hrdeptname
	 */
	public void setHrdeptname(String hrdeptname) {
		this.hrdeptname = hrdeptname;
	}
	
    /**
     * @return hrdeptname
     */
	public String getHrdeptname() {
		return this.hrdeptname;
	}
	
	/**
	 * @param hrworkdeptcode
	 */
	public void setHrworkdeptcode(String hrworkdeptcode) {
		this.hrworkdeptcode = hrworkdeptcode;
	}
	
    /**
     * @return hrworkdeptcode
     */
	public String getHrworkdeptcode() {
		return this.hrworkdeptcode;
	}
	
	/**
	 * @param hrworkdeptname
	 */
	public void setHrworkdeptname(String hrworkdeptname) {
		this.hrworkdeptname = hrworkdeptname;
	}
	
    /**
     * @return hrworkdeptname
     */
	public String getHrworkdeptname() {
		return this.hrworkdeptname;
	}
	
	/**
	 * @param sysdeptcode
	 */
	public void setSysdeptcode(String sysdeptcode) {
		this.sysdeptcode = sysdeptcode;
	}
	
    /**
     * @return sysdeptcode
     */
	public String getSysdeptcode() {
		return this.sysdeptcode;
	}
	
	/**
	 * @param sysdeptname
	 */
	public void setSysdeptname(String sysdeptname) {
		this.sysdeptname = sysdeptname;
	}
	
    /**
     * @return sysdeptname
     */
	public String getSysdeptname() {
		return this.sysdeptname;
	}
	
	/**
	 * @param sysworkdeptcode
	 */
	public void setSysworkdeptcode(String sysworkdeptcode) {
		this.sysworkdeptcode = sysworkdeptcode;
	}
	
    /**
     * @return sysworkdeptcode
     */
	public String getSysworkdeptcode() {
		return this.sysworkdeptcode;
	}
	
	/**
	 * @param sysworkdeptname
	 */
	public void setSysworkdeptname(String sysworkdeptname) {
		this.sysworkdeptname = sysworkdeptname;
	}
	
    /**
     * @return sysworkdeptname
     */
	public String getSysworkdeptname() {
		return this.sysworkdeptname;
	}
	
	/**
	 * @param jobcode
	 */
	public void setJobcode(String jobcode) {
		this.jobcode = jobcode;
	}
	
    /**
     * @return jobcode
     */
	public String getJobcode() {
		return this.jobcode;
	}
	
	/**
	 * @param jobname
	 */
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	
    /**
     * @return jobname
     */
	public String getJobname() {
		return this.jobname;
	}
	
	/**
	 * @param dutyrankcode
	 */
	public void setDutyrankcode(String dutyrankcode) {
		this.dutyrankcode = dutyrankcode;
	}
	
    /**
     * @return dutyrankcode
     */
	public String getDutyrankcode() {
		return this.dutyrankcode;
	}
	
	/**
	 * @param dutyrankname
	 */
	public void setDutyrankname(String dutyrankname) {
		this.dutyrankname = dutyrankname;
	}
	
    /**
     * @return dutyrankname
     */
	public String getDutyrankname() {
		return this.dutyrankname;
	}
	
	/**
	 * @param psnstat
	 */
	public void setPsnstat(String psnstat) {
		this.psnstat = psnstat;
	}
	
    /**
     * @return psnstat
     */
	public String getPsnstat() {
		return this.psnstat;
	}
	
	/**
	 * @param workstat
	 */
	public void setWorkstat(String workstat) {
		this.workstat = workstat;
	}
	
    /**
     * @return workstat
     */
	public String getWorkstat() {
		return this.workstat;
	}
	
	/**
	 * @param psncl
	 */
	public void setPsncl(String psncl) {
		this.psncl = psncl;
	}
	
    /**
     * @return psncl
     */
	public String getPsncl() {
		return this.psncl;
	}
	
	/**
	 * @param cardidtype
	 */
	public void setCardidtype(String cardidtype) {
		this.cardidtype = cardidtype;
	}
	
    /**
     * @return cardidtype
     */
	public String getCardidtype() {
		return this.cardidtype;
	}
	
	/**
	 * @param cardid
	 */
	public void setCardid(String cardid) {
		this.cardid = cardid;
	}
	
    /**
     * @return cardid
     */
	public String getCardid() {
		return this.cardid;
	}
	
	/**
	 * @param mobile
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
    /**
     * @return mobile
     */
	public String getMobile() {
		return this.mobile;
	}
	
	/**
	 * @param wechat
	 */
	public void setWechat(String wechat) {
		this.wechat = wechat;
	}
	
    /**
     * @return wechat
     */
	public String getWechat() {
		return this.wechat;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
    /**
     * @return email
     */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
    /**
     * @return sex
     */
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param birthday
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
    /**
     * @return birthday
     */
	public String getBirthday() {
		return this.birthday;
	}
	
	/**
	 * @param age
	 */
	public void setAge(Integer age) {
		this.age = age;
	}
	
    /**
     * @return age
     */
	public Integer getAge() {
		return this.age;
	}
	
	/**
	 * @param politics
	 */
	public void setPolitics(String politics) {
		this.politics = politics;
	}
	
    /**
     * @return politics
     */
	public String getPolitics() {
		return this.politics;
	}
	
	/**
	 * @param education
	 */
	public void setEducation(String education) {
		this.education = education;
	}
	
    /**
     * @return education
     */
	public String getEducation() {
		return this.education;
	}
	
	/**
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
    /**
     * @return title
     */
	public String getTitle() {
		return this.title;
	}
	
	/**
	 * @param workdate
	 */
	public void setWorkdate(String workdate) {
		this.workdate = workdate;
	}
	
    /**
     * @return workdate
     */
	public String getWorkdate() {
		return this.workdate;
	}
	
	/**
	 * @param bankdate
	 */
	public void setBankdate(String bankdate) {
		this.bankdate = bankdate;
	}
	
    /**
     * @return bankdate
     */
	public String getBankdate() {
		return this.bankdate;
	}
	
	/**
	 * @param workage
	 */
	public void setWorkage(Integer workage) {
		this.workage = workage;
	}
	
    /**
     * @return workage
     */
	public Integer getWorkage() {
		return this.workage;
	}
	
	/**
	 * @param bankage
	 */
	public void setBankage(Integer bankage) {
		this.bankage = bankage;
	}
	
    /**
     * @return bankage
     */
	public Integer getBankage() {
		return this.bankage;
	}
	
	/**
	 * @param cntstat
	 */
	public void setCntstat(String cntstat) {
		this.cntstat = cntstat;
	}
	
    /**
     * @return cntstat
     */
	public String getCntstat() {
		return this.cntstat;
	}
	
	/**
	 * @param salecertdate
	 */
	public void setSalecertdate(String salecertdate) {
		this.salecertdate = salecertdate;
	}
	
    /**
     * @return salecertdate
     */
	public String getSalecertdate() {
		return this.salecertdate;
	}
	
	/**
	 * @param anticertdate
	 */
	public void setAnticertdate(String anticertdate) {
		this.anticertdate = anticertdate;
	}
	
    /**
     * @return anticertdate
     */
	public String getAnticertdate() {
		return this.anticertdate;
	}
	
	/**
	 * @param salecertno
	 */
	public void setSalecertno(String salecertno) {
		this.salecertno = salecertno;
	}
	
    /**
     * @return salecertno
     */
	public String getSalecertno() {
		return this.salecertno;
	}
	
	/**
	 * @param anticertno
	 */
	public void setAnticertno(String anticertno) {
		this.anticertno = anticertno;
	}
	
    /**
     * @return anticertno
     */
	public String getAnticertno() {
		return this.anticertno;
	}
	
	/**
	 * @param salarycard
	 */
	public void setSalarycard(String salarycard) {
		this.salarycard = salarycard;
	}
	
    /**
     * @return salarycard
     */
	public String getSalarycard() {
		return this.salarycard;
	}
	
	/**
	 * @param creditauth
	 */
	public void setCreditauth(String creditauth) {
		this.creditauth = creditauth;
	}
	
    /**
     * @return creditauth
     */
	public String getCreditauth() {
		return this.creditauth;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}