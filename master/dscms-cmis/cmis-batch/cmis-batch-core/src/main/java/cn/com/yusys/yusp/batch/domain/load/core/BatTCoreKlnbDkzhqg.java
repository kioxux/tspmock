/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.core;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTCoreKlnbDkzhqg
 * @类描述: bat_t_core_klnb_dkzhqg数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:34:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_core_klnb_dkzhqg")
public class BatTCoreKlnbDkzhqg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 法人代码
     **/
    @Id
    @Column(name = "farendma")
    private String farendma;
    /**
     * 贷款账号
     **/
    @Column(name = "dkzhangh", unique = false, nullable = false, length = 40)
    private String dkzhangh;
    /**
     * 贷款借据号
     **/
    @Id
    @Column(name = "dkjiejuh")
    private String dkjiejuh;
    /**
     * 本期期数
     **/
    @Id
    @Column(name = "benqqish")
    private Long benqqish;
    /**
     * 本期子期数
     **/
    @Id
    @Column(name = "benqizqs")
    private Long benqizqs;
    /**
     * 起始日期
     **/
    @Column(name = "qishriqi", unique = false, nullable = true, length = 8)
    private String qishriqi;
    /**
     * 到期日期
     **/
    @Column(name = "daoqriqi", unique = false, nullable = true, length = 8)
    private String daoqriqi;
    /**
     * 宽限期到期日
     **/
    @Column(name = "kxqdqirq", unique = false, nullable = true, length = 8)
    private String kxqdqirq;
    /**
     * 上次还款日
     **/
    @Column(name = "schkriqi", unique = false, nullable = true, length = 8)
    private String schkriqi;
    /**
     * 下次还款日
     **/
    @Column(name = "xchkriqi", unique = false, nullable = true, length = 8)
    private String xchkriqi;
    /**
     * 初始本金
     **/
    @Column(name = "chushibj", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal chushibj;
    /**
     * 初始利息
     **/
    @Column(name = "chushilx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal chushilx;
    /**
     * 本金
     **/
    @Column(name = "benjinnn", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal benjinnn;
    /**
     * 应收应计利息
     **/
    @Column(name = "ysyjlixi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal ysyjlixi;
    /**
     * 催收应计利息
     **/
    @Column(name = "csyjlixi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal csyjlixi;
    /**
     * 应收欠息
     **/
    @Column(name = "ysqianxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal ysqianxi;
    /**
     * 催收欠息
     **/
    @Column(name = "csqianxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal csqianxi;
    /**
     * 应收应计罚息
     **/
    @Column(name = "ysyjfaxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal ysyjfaxi;
    /**
     * 催收应计罚息
     **/
    @Column(name = "csyjfaxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal csyjfaxi;
    /**
     * 应收罚息
     **/
    @Column(name = "yshofaxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yshofaxi;
    /**
     * 催收罚息
     **/
    @Column(name = "cshofaxi", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal cshofaxi;
    /**
     * 应计复息
     **/
    @Column(name = "yingjifx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yingjifx;
    /**
     * 复息
     **/
    @Column(name = "fuxiiiii", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal fuxiiiii;
    /**
     * 核销利息
     **/
    @Column(name = "hexiaolx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal hexiaolx;
    /**
     * 实际利率利息收入
     **/
    @Column(name = "sjlllxsr", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal sjlllxsr;
    /**
     * 利息调整
     **/
    @Column(name = "lxtiaozh", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal lxtiaozh;
    /**
     * 实际应计利息
     **/
    @Column(name = "sjyjlixi", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal sjyjlixi;
    /**
     * 实际应计罚息
     **/
    @Column(name = "sjyjfaxi", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal sjyjfaxi;
    /**
     * 实际应计复息
     **/
    @Column(name = "sjyjfuxi", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal sjyjfuxi;
    /**
     * 实际利率实际利息收入
     **/
    @Column(name = "sjllsjsr", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal sjllsjsr;
    /**
     * 应计贴息
     **/
    @Column(name = "yingjitx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yingjitx;
    /**
     * 应收贴息
     **/
    @Column(name = "yingshtx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yingshtx;
    /**
     * 应收费用
     **/
    @Column(name = "yingshfy", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yingshfy;
    /**
     * 应收罚金
     **/
    @Column(name = "yingshfj", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yingshfj;
    /**
     * 已核销本金利息
     **/
    @Column(name = "yihxbjlx", unique = false, nullable = true, length = 21)
    private java.math.BigDecimal yihxbjlx;
    /**
     * 实际核销本金利息
     **/
    @Column(name = "sjhxbjlx", unique = false, nullable = true, length = 20)
    private java.math.BigDecimal sjhxbjlx;
    /**
     * 本期状态 STD_BENQIZHT
     **/
    @Column(name = "benqizht", unique = false, nullable = true, length = 1)
    private String benqizht;
    /**
     * 应计非应计状态 STD_YJFYJZHT
     **/
    @Column(name = "yjfyjzht", unique = false, nullable = true, length = 1)
    private String yjfyjzht;
    /**
     * 期供种类 STD_QIGENGZL
     **/
    @Column(name = "qigengzl", unique = false, nullable = true, length = 1)
    private String qigengzl;
    /**
     * 基准起始日
     **/
    @Column(name = "jzhqshrq", unique = false, nullable = true, length = 8)
    private String jzhqshrq;
    /**
     * 基准到期日
     **/
    @Column(name = "jzhdqirq", unique = false, nullable = true, length = 8)
    private String jzhdqirq;
    /**
     * 明细序号
     **/
    @Column(name = "mingxixh", unique = false, nullable = true, length = 19)
    private Long mingxixh;
    /**
     * 期供逾期天数
     **/
    @Column(name = "qgyqtnsh", unique = false, nullable = true, length = 19)
    private Long qgyqtnsh;
    /**
     * 分行标识
     **/
    @Column(name = "fenhbios", unique = false, nullable = false, length = 4)
    private String fenhbios;
    /**
     * 维护柜员
     **/
    @Column(name = "WEIHGUIY", unique = false, nullable = true, length = 8)
    private String weihguiy;

    /**
     * 维护机构
     **/
    @Column(name = "weihjigo", unique = false, nullable = false, length = 12)
    private String weihjigo;

    /**
     * 维护日期
     **/
    @Column(name = "weihriqi", unique = false, nullable = false, length = 8)
    private String weihriqi;

    /**
     * 维护时间
     **/
    @Column(name = "weihshij", unique = false, nullable = true, length = 9)
    private String weihshij;

    /**
     * 时间戳
     **/
    @Column(name = "shijchuo", unique = false, nullable = false, length = 19)
    private Long shijchuo;

    /**
     * 记录状态 STD_JILUZTAI
     **/
    @Column(name = "jiluztai", unique = false, nullable = false, length = 1)
    private String jiluztai;
	
	
	/**
	 * @param farendma
	 */
	public void setFarendma(String farendma) {
		this.farendma = farendma;
	}
	
    /**
     * @return farendma
     */
	public String getFarendma() {
		return this.farendma;
	}
	
	/**
	 * @param dkzhangh
	 */
	public void setDkzhangh(String dkzhangh) {
		this.dkzhangh = dkzhangh;
	}
	
    /**
     * @return dkzhangh
     */
	public String getDkzhangh() {
		return this.dkzhangh;
	}
	
	/**
	 * @param dkjiejuh
	 */
	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}
	
    /**
     * @return dkjiejuh
     */
	public String getDkjiejuh() {
		return this.dkjiejuh;
	}
	
	/**
	 * @param benqqish
	 */
	public void setBenqqish(Long benqqish) {
		this.benqqish = benqqish;
	}
	
    /**
     * @return benqqish
     */
	public Long getBenqqish() {
		return this.benqqish;
	}
	
	/**
	 * @param benqizqs
	 */
	public void setBenqizqs(Long benqizqs) {
		this.benqizqs = benqizqs;
	}
	
    /**
     * @return benqizqs
     */
	public Long getBenqizqs() {
		return this.benqizqs;
	}
	
	/**
	 * @param qishriqi
	 */
	public void setQishriqi(String qishriqi) {
		this.qishriqi = qishriqi;
	}
	
    /**
     * @return qishriqi
     */
	public String getQishriqi() {
		return this.qishriqi;
	}
	
	/**
	 * @param daoqriqi
	 */
	public void setDaoqriqi(String daoqriqi) {
		this.daoqriqi = daoqriqi;
	}
	
    /**
     * @return daoqriqi
     */
	public String getDaoqriqi() {
		return this.daoqriqi;
	}
	
	/**
	 * @param kxqdqirq
	 */
	public void setKxqdqirq(String kxqdqirq) {
		this.kxqdqirq = kxqdqirq;
	}
	
    /**
     * @return kxqdqirq
     */
	public String getKxqdqirq() {
		return this.kxqdqirq;
	}
	
	/**
	 * @param schkriqi
	 */
	public void setSchkriqi(String schkriqi) {
		this.schkriqi = schkriqi;
	}
	
    /**
     * @return schkriqi
     */
	public String getSchkriqi() {
		return this.schkriqi;
	}
	
	/**
	 * @param xchkriqi
	 */
	public void setXchkriqi(String xchkriqi) {
		this.xchkriqi = xchkriqi;
	}
	
    /**
     * @return xchkriqi
     */
	public String getXchkriqi() {
		return this.xchkriqi;
	}
	
	/**
	 * @param chushibj
	 */
	public void setChushibj(java.math.BigDecimal chushibj) {
		this.chushibj = chushibj;
	}
	
    /**
     * @return chushibj
     */
	public java.math.BigDecimal getChushibj() {
		return this.chushibj;
	}
	
	/**
	 * @param chushilx
	 */
	public void setChushilx(java.math.BigDecimal chushilx) {
		this.chushilx = chushilx;
	}
	
    /**
     * @return chushilx
     */
	public java.math.BigDecimal getChushilx() {
		return this.chushilx;
	}
	
	/**
	 * @param benjinnn
	 */
	public void setBenjinnn(java.math.BigDecimal benjinnn) {
		this.benjinnn = benjinnn;
	}
	
    /**
     * @return benjinnn
     */
	public java.math.BigDecimal getBenjinnn() {
		return this.benjinnn;
	}
	
	/**
	 * @param ysyjlixi
	 */
	public void setYsyjlixi(java.math.BigDecimal ysyjlixi) {
		this.ysyjlixi = ysyjlixi;
	}
	
    /**
     * @return ysyjlixi
     */
	public java.math.BigDecimal getYsyjlixi() {
		return this.ysyjlixi;
	}
	
	/**
	 * @param csyjlixi
	 */
	public void setCsyjlixi(java.math.BigDecimal csyjlixi) {
		this.csyjlixi = csyjlixi;
	}
	
    /**
     * @return csyjlixi
     */
	public java.math.BigDecimal getCsyjlixi() {
		return this.csyjlixi;
	}
	
	/**
	 * @param ysqianxi
	 */
	public void setYsqianxi(java.math.BigDecimal ysqianxi) {
		this.ysqianxi = ysqianxi;
	}
	
    /**
     * @return ysqianxi
     */
	public java.math.BigDecimal getYsqianxi() {
		return this.ysqianxi;
	}
	
	/**
	 * @param csqianxi
	 */
	public void setCsqianxi(java.math.BigDecimal csqianxi) {
		this.csqianxi = csqianxi;
	}
	
    /**
     * @return csqianxi
     */
	public java.math.BigDecimal getCsqianxi() {
		return this.csqianxi;
	}
	
	/**
	 * @param ysyjfaxi
	 */
	public void setYsyjfaxi(java.math.BigDecimal ysyjfaxi) {
		this.ysyjfaxi = ysyjfaxi;
	}
	
    /**
     * @return ysyjfaxi
     */
	public java.math.BigDecimal getYsyjfaxi() {
		return this.ysyjfaxi;
	}
	
	/**
	 * @param csyjfaxi
	 */
	public void setCsyjfaxi(java.math.BigDecimal csyjfaxi) {
		this.csyjfaxi = csyjfaxi;
	}
	
    /**
     * @return csyjfaxi
     */
	public java.math.BigDecimal getCsyjfaxi() {
		return this.csyjfaxi;
	}
	
	/**
	 * @param yshofaxi
	 */
	public void setYshofaxi(java.math.BigDecimal yshofaxi) {
		this.yshofaxi = yshofaxi;
	}
	
    /**
     * @return yshofaxi
     */
	public java.math.BigDecimal getYshofaxi() {
		return this.yshofaxi;
	}
	
	/**
	 * @param cshofaxi
	 */
	public void setCshofaxi(java.math.BigDecimal cshofaxi) {
		this.cshofaxi = cshofaxi;
	}
	
    /**
     * @return cshofaxi
     */
	public java.math.BigDecimal getCshofaxi() {
		return this.cshofaxi;
	}
	
	/**
	 * @param yingjifx
	 */
	public void setYingjifx(java.math.BigDecimal yingjifx) {
		this.yingjifx = yingjifx;
	}
	
    /**
     * @return yingjifx
     */
	public java.math.BigDecimal getYingjifx() {
		return this.yingjifx;
	}
	
	/**
	 * @param fuxiiiii
	 */
	public void setFuxiiiii(java.math.BigDecimal fuxiiiii) {
		this.fuxiiiii = fuxiiiii;
	}
	
    /**
     * @return fuxiiiii
     */
	public java.math.BigDecimal getFuxiiiii() {
		return this.fuxiiiii;
	}
	
	/**
	 * @param hexiaolx
	 */
	public void setHexiaolx(java.math.BigDecimal hexiaolx) {
		this.hexiaolx = hexiaolx;
	}
	
    /**
     * @return hexiaolx
     */
	public java.math.BigDecimal getHexiaolx() {
		return this.hexiaolx;
	}
	
	/**
	 * @param sjlllxsr
	 */
	public void setSjlllxsr(java.math.BigDecimal sjlllxsr) {
		this.sjlllxsr = sjlllxsr;
	}
	
    /**
     * @return sjlllxsr
     */
	public java.math.BigDecimal getSjlllxsr() {
		return this.sjlllxsr;
	}
	
	/**
	 * @param lxtiaozh
	 */
	public void setLxtiaozh(java.math.BigDecimal lxtiaozh) {
		this.lxtiaozh = lxtiaozh;
	}
	
    /**
     * @return lxtiaozh
     */
	public java.math.BigDecimal getLxtiaozh() {
		return this.lxtiaozh;
	}
	
	/**
	 * @param sjyjlixi
	 */
	public void setSjyjlixi(java.math.BigDecimal sjyjlixi) {
		this.sjyjlixi = sjyjlixi;
	}
	
    /**
     * @return sjyjlixi
     */
	public java.math.BigDecimal getSjyjlixi() {
		return this.sjyjlixi;
	}
	
	/**
	 * @param sjyjfaxi
	 */
	public void setSjyjfaxi(java.math.BigDecimal sjyjfaxi) {
		this.sjyjfaxi = sjyjfaxi;
	}
	
    /**
     * @return sjyjfaxi
     */
	public java.math.BigDecimal getSjyjfaxi() {
		return this.sjyjfaxi;
	}
	
	/**
	 * @param sjyjfuxi
	 */
	public void setSjyjfuxi(java.math.BigDecimal sjyjfuxi) {
		this.sjyjfuxi = sjyjfuxi;
	}
	
    /**
     * @return sjyjfuxi
     */
	public java.math.BigDecimal getSjyjfuxi() {
		return this.sjyjfuxi;
	}
	
	/**
	 * @param sjllsjsr
	 */
	public void setSjllsjsr(java.math.BigDecimal sjllsjsr) {
		this.sjllsjsr = sjllsjsr;
	}
	
    /**
     * @return sjllsjsr
     */
	public java.math.BigDecimal getSjllsjsr() {
		return this.sjllsjsr;
	}
	
	/**
	 * @param yingjitx
	 */
	public void setYingjitx(java.math.BigDecimal yingjitx) {
		this.yingjitx = yingjitx;
	}
	
    /**
     * @return yingjitx
     */
	public java.math.BigDecimal getYingjitx() {
		return this.yingjitx;
	}
	
	/**
	 * @param yingshtx
	 */
	public void setYingshtx(java.math.BigDecimal yingshtx) {
		this.yingshtx = yingshtx;
	}
	
    /**
     * @return yingshtx
     */
	public java.math.BigDecimal getYingshtx() {
		return this.yingshtx;
	}
	
	/**
	 * @param yingshfy
	 */
	public void setYingshfy(java.math.BigDecimal yingshfy) {
		this.yingshfy = yingshfy;
	}
	
    /**
     * @return yingshfy
     */
	public java.math.BigDecimal getYingshfy() {
		return this.yingshfy;
	}
	
	/**
	 * @param yingshfj
	 */
	public void setYingshfj(java.math.BigDecimal yingshfj) {
		this.yingshfj = yingshfj;
	}
	
    /**
     * @return yingshfj
     */
	public java.math.BigDecimal getYingshfj() {
		return this.yingshfj;
	}
	
	/**
	 * @param yihxbjlx
	 */
	public void setYihxbjlx(java.math.BigDecimal yihxbjlx) {
		this.yihxbjlx = yihxbjlx;
	}
	
    /**
     * @return yihxbjlx
     */
	public java.math.BigDecimal getYihxbjlx() {
		return this.yihxbjlx;
	}
	
	/**
	 * @param sjhxbjlx
	 */
	public void setSjhxbjlx(java.math.BigDecimal sjhxbjlx) {
		this.sjhxbjlx = sjhxbjlx;
	}
	
    /**
     * @return sjhxbjlx
     */
	public java.math.BigDecimal getSjhxbjlx() {
		return this.sjhxbjlx;
	}
	
	/**
	 * @param benqizht
	 */
	public void setBenqizht(String benqizht) {
		this.benqizht = benqizht;
	}
	
    /**
     * @return benqizht
     */
	public String getBenqizht() {
		return this.benqizht;
	}
	
	/**
	 * @param yjfyjzht
	 */
	public void setYjfyjzht(String yjfyjzht) {
		this.yjfyjzht = yjfyjzht;
	}
	
    /**
     * @return yjfyjzht
     */
	public String getYjfyjzht() {
		return this.yjfyjzht;
	}
	
	/**
	 * @param qigengzl
	 */
	public void setQigengzl(String qigengzl) {
		this.qigengzl = qigengzl;
	}
	
    /**
     * @return qigengzl
     */
	public String getQigengzl() {
		return this.qigengzl;
	}
	
	/**
	 * @param jzhqshrq
	 */
	public void setJzhqshrq(String jzhqshrq) {
		this.jzhqshrq = jzhqshrq;
	}
	
    /**
     * @return jzhqshrq
     */
	public String getJzhqshrq() {
		return this.jzhqshrq;
	}
	
	/**
	 * @param jzhdqirq
	 */
	public void setJzhdqirq(String jzhdqirq) {
		this.jzhdqirq = jzhdqirq;
	}
	
    /**
     * @return jzhdqirq
     */
	public String getJzhdqirq() {
		return this.jzhdqirq;
	}
	
	/**
	 * @param mingxixh
	 */
	public void setMingxixh(Long mingxixh) {
		this.mingxixh = mingxixh;
	}
	
    /**
     * @return mingxixh
     */
	public Long getMingxixh() {
		return this.mingxixh;
	}
	
	/**
	 * @param qgyqtnsh
	 */
	public void setQgyqtnsh(Long qgyqtnsh) {
		this.qgyqtnsh = qgyqtnsh;
	}
	
    /**
     * @return qgyqtnsh
     */
	public Long getQgyqtnsh() {
		return this.qgyqtnsh;
	}
	
	/**
	 * @param fenhbios
	 */
	public void setFenhbios(String fenhbios) {
		this.fenhbios = fenhbios;
	}
	
    /**
     * @return fenhbios
     */
	public String getFenhbios() {
		return this.fenhbios;
	}
	
	/**
	 * @param weihguiy
	 */
	public void setWeihguiy(String weihguiy) {
		this.weihguiy = weihguiy;
	}
	
    /**
     * @return weihguiy
     */
	public String getWeihguiy() {
		return this.weihguiy;
	}
	
	/**
	 * @param weihjigo
	 */
	public void setWeihjigo(String weihjigo) {
		this.weihjigo = weihjigo;
	}
	
    /**
     * @return weihjigo
     */
	public String getWeihjigo() {
		return this.weihjigo;
	}
	
	/**
	 * @param weihriqi
	 */
	public void setWeihriqi(String weihriqi) {
		this.weihriqi = weihriqi;
	}
	
    /**
     * @return weihriqi
     */
	public String getWeihriqi() {
		return this.weihriqi;
	}
	
	/**
	 * @param weihshij
	 */
	public void setWeihshij(String weihshij) {
		this.weihshij = weihshij;
	}
	
    /**
     * @return weihshij
     */
	public String getWeihshij() {
		return this.weihshij;
	}
	
	/**
	 * @param shijchuo
	 */
	public void setShijchuo(Long shijchuo) {
		this.shijchuo = shijchuo;
	}
	
    /**
     * @return shijchuo
     */
	public Long getShijchuo() {
		return this.shijchuo;
	}
	
	/**
	 * @param jiluztai
	 */
	public void setJiluztai(String jiluztai) {
		this.jiluztai = jiluztai;
	}
	
    /**
     * @return jiluztai
     */
	public String getJiluztai() {
		return this.jiluztai;
	}


}