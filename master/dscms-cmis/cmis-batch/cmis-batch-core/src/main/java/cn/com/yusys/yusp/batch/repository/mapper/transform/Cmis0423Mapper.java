package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0423</br>
 * 任务名称：加工任务-贷后管理-个人消费性风险分类  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0423Mapper {
    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspRisk01(@Param("openDay") String openDay);
}
