package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0415Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS</br>
 * 任务名称：加工任务-业务处理- </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0415Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0415Service.class);

    @Resource
    private Cmis0415Mapper cmis0415Mapper;

    /**
     * 插入贷后检查任务表
     * @param openDay
     */
    public void cmis0415InsertPspTaskListStep(String openDay) {

//        logger.info("插入贷后检查任务表 小微消费类贷款 定期检查 不良 类贷款客户风险分类后三类 开始 ,请求参数为:[{}]", openDay);
//        int insertPspTaskList01 = cmis0415Mapper.insertPspTaskList01(openDay);
//        logger.info("插入贷后检查任务表 小微消费类贷款 定期检查 不良 类贷款客户风险分类后三类 结束,返回参数为:[{}]", insertPspTaskList01);
//
//        logger.info("插入贷后检查任务表 房屋按揭类 贷款 借据出现逾期欠息 开始,请求参数为:[{}]", openDay);
//        int insertPspTaskList02 = cmis0415Mapper.insertPspTaskList02(openDay);
//        logger.info("插入贷后检查任务表 房屋按揭类 贷款 借据出现逾期欠息 结束,返回参数为:[{}]", insertPspTaskList02);
//
//        logger.info("插入贷后检查任务表 房屋按揭类 期房已过交房日期90天  房屋按揭类 期房已过交房日期180天 开始,请求参数为:[{}]", openDay);
//        int insertPspTaskList03 = cmis0415Mapper.insertPspTaskList03(openDay);
//        logger.info("插入贷后检查任务表  房屋按揭类 期房已过交房日期90天  房屋按揭类 期房已过交房日期180天 结束,返回参数为:[{}]", insertPspTaskList03);

        logger.info("插入贷后检查任务表 零售非按揭类 线下小微消费性瑕疵类贷款 上月整月产生借据逾期欠息 开始,请求参数为:[{}]", openDay);
        int insertPspTaskList04 = cmis0415Mapper.insertPspTaskList04(openDay);
        logger.info("插入贷后检查任务表 零售非按揭类 线下小微消费性贷款 上月整月产生借据逾期欠息 结束,返回参数为:[{}]", insertPspTaskList04);

        logger.info("插入贷后检查任务表 零售非按揭类 线下小微消费性不良类贷款 上月整月产生借据逾期欠息 开始,请求参数为:[{}]", openDay);
        int insertPspTaskList05 = cmis0415Mapper.insertPspTaskList05(openDay);
        logger.info("插入贷后检查任务表 零售非按揭类 线下小微消费性贷款 上月整月产生借据逾期欠息 结束,返回参数为:[{}]", insertPspTaskList04);

        logger.info("插入贷后检查任务表 小微消费类正常类贷款自动检查 开始,请求参数为:[{}]", openDay);
        int insertPspTaskList06 = cmis0415Mapper.insertPspTaskList06(openDay);
        logger.info("插入贷后检查任务表 小微消费类贷款自动检查 结束,返回参数为:[{}]", insertPspTaskList05);

//        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
//       // int insertPspTaskList06 = cmis0415Mapper.insertPspTaskList06(openDay);
//        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList06);
//
//        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
//     //   int insertPspTaskList07 = cmis0415Mapper.insertPspTaskList07(openDay);
//        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList07);
//
//        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
//       // int insertPspTaskList08 = cmis0415Mapper.insertPspTaskList08(openDay);
//        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList08);
//
//        logger.info("插入贷后检查任务表开始,请求参数为:[{}]", openDay);
//       // int insertPspTaskList09 = cmis0415Mapper.insertPspTaskList09(openDay);
//        logger.info("插入贷后检查任务表结束,返回参数为:[{}]", insertPspTaskList09);
    }
}
