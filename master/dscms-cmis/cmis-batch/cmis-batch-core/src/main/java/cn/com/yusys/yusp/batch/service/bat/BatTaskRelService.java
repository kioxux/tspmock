/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRel;
import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.repository.mapper.bat.BatTaskRelMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTaskRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-15 14:17:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTaskRelService {
    private static final Logger logger = LoggerFactory.getLogger(BatTaskRelService.class);
    @Autowired
    private BatTaskRelMapper batTaskRelMapper;

    @Autowired
    private BatTaskRunService batTaskRunService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BatTaskRel selectByPrimaryKey(String relNo, String relTaskNo) {
        return batTaskRelMapper.selectByPrimaryKey(relNo, relTaskNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<BatTaskRel> selectAll(QueryModel model) {
        List<BatTaskRel> records = (List<BatTaskRel>) batTaskRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<BatTaskRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTaskRel> list = batTaskRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(BatTaskRel record) {
        record.setInputDate(DateUtils.getCurrDateStr()); // 登记日期
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setCreateTime(DateUtils.getCurrTimestamp());// 创建时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(BatTaskRel record) {
        record.setInputDate(DateUtils.getCurrDateStr()); // 登记日期
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setCreateTime(DateUtils.getCurrTimestamp());// 创建时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(BatTaskRel record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(BatTaskRel record) {
        record.setUpdDate(DateUtils.getCurrDateStr()); // 最近修改日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
        return batTaskRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String relNo, String relTaskNo) {
        return batTaskRelMapper.deleteByPrimaryKey(relNo, relTaskNo);
    }

    /**
     * 根据任务编号查询该任务编号对应的依赖任务信息
     *
     * @param relMap
     * @return
     */
    public List<BatTaskRel> selectByRelMap(Map relMap) {
        List<BatTaskRel> records = (List<BatTaskRel>) batTaskRelMapper.selectByRelMap(relMap);
        return records;
    }

    /**
     * 依赖关系检查
     *
     * @param batTaskRun
     * @return 检查通过 true ; 检查不通过 false
     */
    public boolean checkRel(BatTaskRun batTaskRun) {
        Boolean result = false;
        String relationFlag = batTaskRun.getRelationFlag();// 是否存在依赖
        String relTaskNo = batTaskRun.getRelTaskNo();//依赖任务编号
        String taskDate = batTaskRun.getTaskDate(); // 任务日期
        logger.info("任务日期为:[{}],任务编号为:[{}],是否存在依赖为:[{}],依赖任务编号:[{}]", taskDate, batTaskRun.getTaskNo(), relationFlag, relTaskNo);
        if (Objects.equals(BatEnums.STD_YES.key, relationFlag) && Objects.nonNull(relTaskNo) && StringUtils.nonEmpty(relTaskNo)) {
            // 存在依赖 则校验依赖
            logger.info("任务日期为:[{}],任务编号为:[{}],存在依赖的任务编号为:[{}],校验依赖开始", taskDate, batTaskRun.getTaskNo(), relTaskNo);
            String[] relTaskNos = relTaskNo.split(",");
            for (String checkRelTaskNo : relTaskNos) {
                // 查询【任务运行管理(BAT_TASK_RUN)】表判断依赖任务是否执行成功
                if (checkRelTaskNo.length() == 8) {
                    // 依赖任务编号如果是8位则直接根据主键查询
                    BatTaskRun checkBatTaskRun = batTaskRunService.selectByPrimaryKey(taskDate, checkRelTaskNo);

                    if (Objects.nonNull(checkBatTaskRun) && Objects.equals(checkBatTaskRun.getTaskStatus(), BatEnums.TASK_STATUS_100.key)) {
                        logger.info("任务日期为:[{}],任务编号为:[{}]的依赖任务编号为:[{}]执行成功,任务状态为:[{}]", taskDate, batTaskRun.getTaskNo(), checkRelTaskNo, checkBatTaskRun.getTaskStatus());
                        // 执行成功
                        result = true;
                    } else {
                        logger.info("任务日期为:[{}],任务编号为:[{}]的依赖任务编号为:[{}]还未执行成功,任务状态为:[{}]", taskDate, batTaskRun.getTaskNo(), checkRelTaskNo, checkBatTaskRun.getTaskStatus());
                        result = false;
                        break;
                    }
                } else {
                    // 依赖任务编号如果不是8位则直接根据模糊查询，查询任务状态不为执行成功的列表
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("taskDate", taskDate);
                    queryModel.addCondition("taskNo", "%".concat(checkRelTaskNo).concat("%"));
                    List<BatTaskRun> nonSuccessList = batTaskRunService.queryNonSuccessTaskStatus(queryModel);

                    if (CollectionUtils.nonEmpty(nonSuccessList)) {
                        logger.info("任务日期为:[{}],任务编号为:[{}]的依赖任务编号为:[{}]还未执行成功,任务状态为:[{}]", taskDate, batTaskRun.getTaskNo(), checkRelTaskNo, nonSuccessList.get(0).getTaskStatus());
                        result = false;
                        break;
                    } else {
                        logger.info("任务日期为:[{}],任务编号为:[{}]的依赖任务编号为:[{}]执行成功,任务状态为:[{}]", taskDate, batTaskRun.getTaskNo(), checkRelTaskNo, BatEnums.TASK_STATUS_100.key);
                        // 执行成功
                        result = true;
                    }
                }
            }
            logger.info("任务日期为:[{}],任务编号为:[{}],存在依赖的任务编号为:[{}],校验依赖结束", taskDate, batTaskRun.getTaskNo(), relTaskNo);
        } else {
            // 不存在依赖 则直接通过
            logger.info("任务日期为:[{}],任务编号为:[{}],不存在依赖，则直接通过", taskDate, batTaskRun.getTaskNo());
            result = true;
        }
        return result;
    }


}
