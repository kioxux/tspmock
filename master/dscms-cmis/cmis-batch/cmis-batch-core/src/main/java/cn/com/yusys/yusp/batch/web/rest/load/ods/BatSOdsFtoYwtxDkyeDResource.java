/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.ods;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.ods.BatSOdsFtoYwtxDkyeD;
import cn.com.yusys.yusp.batch.service.load.ods.BatSOdsFtoYwtxDkyeDService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSOdsFtoYwtxDkyeDResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-25 16:35:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsodsftoywtxdkyed")
public class BatSOdsFtoYwtxDkyeDResource {
    @Autowired
    private BatSOdsFtoYwtxDkyeDService batSOdsFtoYwtxDkyeDService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSOdsFtoYwtxDkyeD>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSOdsFtoYwtxDkyeD> list = batSOdsFtoYwtxDkyeDService.selectAll(queryModel);
        return new ResultDto<List<BatSOdsFtoYwtxDkyeD>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSOdsFtoYwtxDkyeD>> index(QueryModel queryModel) {
        List<BatSOdsFtoYwtxDkyeD> list = batSOdsFtoYwtxDkyeDService.selectByModel(queryModel);
        return new ResultDto<List<BatSOdsFtoYwtxDkyeD>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSOdsFtoYwtxDkyeD> create(@RequestBody BatSOdsFtoYwtxDkyeD batSOdsFtoYwtxDkyeD) throws URISyntaxException {
        batSOdsFtoYwtxDkyeDService.insert(batSOdsFtoYwtxDkyeD);
        return new ResultDto<BatSOdsFtoYwtxDkyeD>(batSOdsFtoYwtxDkyeD);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSOdsFtoYwtxDkyeD batSOdsFtoYwtxDkyeD) throws URISyntaxException {
        int result = batSOdsFtoYwtxDkyeDService.update(batSOdsFtoYwtxDkyeD);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String acctdt, String brchno, String stline, java.math.BigDecimal loanbl) {
        int result = batSOdsFtoYwtxDkyeDService.deleteByPrimaryKey(acctdt, brchno, stline, loanbl);
        return new ResultDto<Integer>(result);
    }

}
