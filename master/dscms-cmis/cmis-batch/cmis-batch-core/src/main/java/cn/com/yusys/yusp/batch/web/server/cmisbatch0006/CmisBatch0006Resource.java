package cn.com.yusys.yusp.batch.web.server.cmisbatch0006;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006RespDto;
import cn.com.yusys.yusp.batch.service.server.cmisbatch0006.CmisBatch0006Service;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:不定期检查任务信息生成
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CMISBATCH0006:不定期检查任务信息生成")
@RestController
@RequestMapping("/api/batch4inner")
public class CmisBatch0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0006Resource.class);

    @Autowired
    private CmisBatch0006Service cmisBatch0006Service;

    /**
     * 交易码：cmisbatch0006
     * 交易描述：不定期检查任务信息生成
     *
     * @param cmisbatch0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("不定期检查任务信息生成")
    @PostMapping("/cmisbatch0006")
    protected @ResponseBody
    ResultDto<Cmisbatch0006RespDto> cmisbatch0006(@Validated @RequestBody Cmisbatch0006ReqDto cmisbatch0006ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, JSON.toJSONString(cmisbatch0006ReqDto));
        Cmisbatch0006RespDto cmisbatch0006RespDto = new Cmisbatch0006RespDto();// 响应Dto:不定期检查任务信息生成
        ResultDto<Cmisbatch0006RespDto> cmisbatch0006ResultDto = new ResultDto<>();
        try {
            // 从cmisbatch0006ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, JSON.toJSONString(cmisbatch0006ReqDto));
            cmisbatch0006RespDto = cmisBatch0006Service.cmisBatch0006Ext(cmisbatch0006ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, JSON.toJSONString(cmisbatch0006RespDto));
            // 封装cmisbatch0006ResultDto中正确的返回码和返回信息
            cmisbatch0006ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbatch0006ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, e.getMessage());
            // 封装cmisbatch0006ResultDto中异常返回码和返回信息
            cmisbatch0006ResultDto.setCode(e.getErrorCode());
            cmisbatch0006ResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, e.getMessage());
            // 封装cmisbatch0006ResultDto中异常返回码和返回信息
            cmisbatch0006ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbatch0006ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbatch0006RespDto到cmisbatch0006ResultDto中
        cmisbatch0006ResultDto.setData(cmisbatch0006RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, JSON.toJSONString(cmisbatch0006ResultDto));
        return cmisbatch0006ResultDto;
    }
}
