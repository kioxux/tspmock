package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0105</br>
 * 任务名称：加工任务-业务处理-信用证台账处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0105Mapper {
    /**
     * 清空国结国内证业务发信贷台账
     */
    void truncateTmpGjpAccTfLoc();

    /**
     * 插入国结国内证业务发信贷台账表
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpAccTfLoc01(@Param("openDay") String openDay);

    /**
     * 插入国结国内证业务发信贷台账表
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpAccTfLoc02(@Param("openDay") String openDay);

    /**
     * 清空票据加工临时表
     */
    void truncateTmpGjpSecurityAmt();

    /**
     * 插入票据加工临时表
     *
     * @param openDay
     * @return
     */
    int insertTmpGjpSecurityAmt(@Param("openDay") String openDay);

    /**
     * 更新信用证台账原始敞口金额
     *
     * @param openDay
     * @return
     */
    int updateAccTfLoc01(@Param("openDay") String openDay);

    /**
     * 加工信用证台账
     *
     * @param openDay
     * @return
     */
    int updateAccTfLoc02(@Param("openDay") String openDay);
}
