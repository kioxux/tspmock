/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.djk;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSDjkDataTxn
 * @类描述: bat_s_djk_data_txn数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 19:35:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_djk_data_txn")
public class BatSDjkDataTxn extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 交易流水号 **/
	@Id
	@Column(name = "TXN_SEQ")
	private String txnSeq;
	
	/** 账户编号 **/
	@Id
	@Column(name = "ACCT_NO")
	private String acctNo;
	
	/** 卡号 **/
	@Column(name = "CARD_NO", unique = false, nullable = true, length = 19)
	private String cardNo;
	
	/** 交易日期 **/
	@Column(name = "TXN_DATE", unique = false, nullable = true, length = 8)
	private String txnDate;
	
	/** 交易时间 **/
	@Column(name = "TXN_TIME", unique = false, nullable = true, length = 6)
	private String txnTime;
	
	/** 交易金额 **/
	@Column(name = "TXN_AMT", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal txnAmt;
	
	/** 交易币种代码 **/
	@Column(name = "TXN_CURR_CD", unique = false, nullable = true, length = 3)
	private String txnCurrCd;
	
	/** 授权码 **/
	@Column(name = "AUTH_CODE", unique = false, nullable = true, length = 6)
	private String authCode;
	
	/** 交易码 **/
	@Column(name = "TXN_CODE", unique = false, nullable = true, length = 4)
	private String txnCode;
	
	/** 记账日期 **/
	@Column(name = "POST_DATE", unique = false, nullable = true, length = 8)
	private String postDate;
	
	/** 记账币种金额 **/
	@Column(name = "POST_AMT", unique = false, nullable = true, length = 15)
	private java.math.BigDecimal postAmt;
	
	/** 记账币种代码 **/
	@Column(name = "POST_CURR_CD", unique = false, nullable = true, length = 3)
	private String postCurrCd;
	
	/** 交易参考号 **/
	@Column(name = "REF_NBR", unique = false, nullable = true, length = 23)
	private String refNbr;
	
	/** 账单交易描述 **/
	@Column(name = "TXN_SHORT_DESC", unique = false, nullable = true, length = 40)
	private String txnShortDesc;
	
	/** 积分数值 **/
	@Column(name = "POINT", unique = false, nullable = true, length = 13)
	private String point;
	
	/** 受理分行代码 **/
	@Column(name = "ACQ_BRANCH_ID", unique = false, nullable = true, length = 9)
	private String acqBranchId;
	
	/** 受卡方标识码 **/
	@Column(name = "CARD_ACCEPTOR_ID", unique = false, nullable = true, length = 15)
	private String cardAcceptorId;
	
	/** 受理机构名称地址 **/
	@Column(name = "ACQ_NAME_ADDR", unique = false, nullable = true, length = 40)
	private String acqNameAddr;
	
	/** 商户类别代码 **/
	@Column(name = "MCC", unique = false, nullable = true, length = 4)
	private String mcc;
	
	/** 交易渠道 **/
	@Column(name = "CHANNEL", unique = false, nullable = true, length = 20)
	private String channel;
	
	/** 交易接入组织 **/
	@Column(name = "INPUT_SOURCE", unique = false, nullable = true, length = 20)
	private String inputSource;
	
	/** 终端号 **/
	@Column(name = "TERMINAL_ID", unique = false, nullable = true, length = 15)
	private String terminalId;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 200)
	private String remark;
	
	/** 借贷标记 **/
	@Column(name = "DB_CR_IND", unique = false, nullable = true, length = 1)
	private String dbCrInd;
	
	/** 代理机构标识码 **/
	@Column(name = "ACQ_INST_ID", unique = false, nullable = true, length = 11)
	private String acqInstId;
	
	/** 发送机构标识码 **/
	@Column(name = "FWD_INST_ID", unique = false, nullable = true, length = 11)
	private String fwdInstId;
	
	/** 终端类型 **/
	@Column(name = "TERMINAL_TYPE", unique = false, nullable = true, length = 2)
	private String terminalType;
	
	/** 对手方姓名 **/
	@Column(name = "OPPO_NAME", unique = false, nullable = true, length = 30)
	private String oppoName;
	
	/** 对手方账号 **/
	@Column(name = "OPPO_ACCOUNT", unique = false, nullable = true, length = 28)
	private String oppoAccount;
	
	/** 银联贷记交易标识 **/
	@Column(name = "CUP_PAY4OTHER_IND", unique = false, nullable = true, length = 1)
	private String cupPay4otherInd;
	
	/** 付款方账户号 **/
	@Column(name = "CUP_PAY_ACCTNO", unique = false, nullable = true, length = 40)
	private String cupPayAcctno;
	
	/** 付款方姓名 **/
	@Column(name = "CUP_PAY_NAME", unique = false, nullable = true, length = 120)
	private String cupPayName;
	
	/** 对方金融机构网点代码类型 **/
	@Column(name = "COUNTERPARTY_BANK_NO_TYPE", unique = false, nullable = true, length = 2)
	private String counterpartyBankNoType;
	
	/** 对手方行号 **/
	@Column(name = "COUNTERPARTY_BANK_NO_NO", unique = false, nullable = true, length = 16)
	private String counterpartyBankNoNo;
	
	/** 对手方行名 **/
	@Column(name = "COUNTERPARTY_BANK_NAME", unique = false, nullable = true, length = 64)
	private String counterpartyBankName;
	
	/** 对方金融机构网点行政区划代码 **/
	@Column(name = "COUNTERPARTY_BANK_AREA_NO", unique = false, nullable = true, length = 9)
	private String counterpartyBankAreaNo;
	
	/** 预留域 **/
	@Column(name = "FILLER", unique = false, nullable = true, length = 109)
	private String filler;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param txnSeq
	 */
	public void setTxnSeq(String txnSeq) {
		this.txnSeq = txnSeq;
	}
	
    /**
     * @return txnSeq
     */
	public String getTxnSeq() {
		return this.txnSeq;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param cardNo
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
    /**
     * @return cardNo
     */
	public String getCardNo() {
		return this.cardNo;
	}
	
	/**
	 * @param txnDate
	 */
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	
    /**
     * @return txnDate
     */
	public String getTxnDate() {
		return this.txnDate;
	}
	
	/**
	 * @param txnTime
	 */
	public void setTxnTime(String txnTime) {
		this.txnTime = txnTime;
	}
	
    /**
     * @return txnTime
     */
	public String getTxnTime() {
		return this.txnTime;
	}
	
	/**
	 * @param txnAmt
	 */
	public void setTxnAmt(java.math.BigDecimal txnAmt) {
		this.txnAmt = txnAmt;
	}
	
    /**
     * @return txnAmt
     */
	public java.math.BigDecimal getTxnAmt() {
		return this.txnAmt;
	}
	
	/**
	 * @param txnCurrCd
	 */
	public void setTxnCurrCd(String txnCurrCd) {
		this.txnCurrCd = txnCurrCd;
	}
	
    /**
     * @return txnCurrCd
     */
	public String getTxnCurrCd() {
		return this.txnCurrCd;
	}
	
	/**
	 * @param authCode
	 */
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	
    /**
     * @return authCode
     */
	public String getAuthCode() {
		return this.authCode;
	}
	
	/**
	 * @param txnCode
	 */
	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}
	
    /**
     * @return txnCode
     */
	public String getTxnCode() {
		return this.txnCode;
	}
	
	/**
	 * @param postDate
	 */
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	
    /**
     * @return postDate
     */
	public String getPostDate() {
		return this.postDate;
	}
	
	/**
	 * @param postAmt
	 */
	public void setPostAmt(java.math.BigDecimal postAmt) {
		this.postAmt = postAmt;
	}
	
    /**
     * @return postAmt
     */
	public java.math.BigDecimal getPostAmt() {
		return this.postAmt;
	}
	
	/**
	 * @param postCurrCd
	 */
	public void setPostCurrCd(String postCurrCd) {
		this.postCurrCd = postCurrCd;
	}
	
    /**
     * @return postCurrCd
     */
	public String getPostCurrCd() {
		return this.postCurrCd;
	}
	
	/**
	 * @param refNbr
	 */
	public void setRefNbr(String refNbr) {
		this.refNbr = refNbr;
	}
	
    /**
     * @return refNbr
     */
	public String getRefNbr() {
		return this.refNbr;
	}
	
	/**
	 * @param txnShortDesc
	 */
	public void setTxnShortDesc(String txnShortDesc) {
		this.txnShortDesc = txnShortDesc;
	}
	
    /**
     * @return txnShortDesc
     */
	public String getTxnShortDesc() {
		return this.txnShortDesc;
	}
	
	/**
	 * @param point
	 */
	public void setPoint(String point) {
		this.point = point;
	}
	
    /**
     * @return point
     */
	public String getPoint() {
		return this.point;
	}
	
	/**
	 * @param acqBranchId
	 */
	public void setAcqBranchId(String acqBranchId) {
		this.acqBranchId = acqBranchId;
	}
	
    /**
     * @return acqBranchId
     */
	public String getAcqBranchId() {
		return this.acqBranchId;
	}
	
	/**
	 * @param cardAcceptorId
	 */
	public void setCardAcceptorId(String cardAcceptorId) {
		this.cardAcceptorId = cardAcceptorId;
	}
	
    /**
     * @return cardAcceptorId
     */
	public String getCardAcceptorId() {
		return this.cardAcceptorId;
	}
	
	/**
	 * @param acqNameAddr
	 */
	public void setAcqNameAddr(String acqNameAddr) {
		this.acqNameAddr = acqNameAddr;
	}
	
    /**
     * @return acqNameAddr
     */
	public String getAcqNameAddr() {
		return this.acqNameAddr;
	}
	
	/**
	 * @param mcc
	 */
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
	
    /**
     * @return mcc
     */
	public String getMcc() {
		return this.mcc;
	}
	
	/**
	 * @param channel
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
    /**
     * @return channel
     */
	public String getChannel() {
		return this.channel;
	}
	
	/**
	 * @param inputSource
	 */
	public void setInputSource(String inputSource) {
		this.inputSource = inputSource;
	}
	
    /**
     * @return inputSource
     */
	public String getInputSource() {
		return this.inputSource;
	}
	
	/**
	 * @param terminalId
	 */
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	
    /**
     * @return terminalId
     */
	public String getTerminalId() {
		return this.terminalId;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param dbCrInd
	 */
	public void setDbCrInd(String dbCrInd) {
		this.dbCrInd = dbCrInd;
	}
	
    /**
     * @return dbCrInd
     */
	public String getDbCrInd() {
		return this.dbCrInd;
	}
	
	/**
	 * @param acqInstId
	 */
	public void setAcqInstId(String acqInstId) {
		this.acqInstId = acqInstId;
	}
	
    /**
     * @return acqInstId
     */
	public String getAcqInstId() {
		return this.acqInstId;
	}
	
	/**
	 * @param fwdInstId
	 */
	public void setFwdInstId(String fwdInstId) {
		this.fwdInstId = fwdInstId;
	}
	
    /**
     * @return fwdInstId
     */
	public String getFwdInstId() {
		return this.fwdInstId;
	}
	
	/**
	 * @param terminalType
	 */
	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}
	
    /**
     * @return terminalType
     */
	public String getTerminalType() {
		return this.terminalType;
	}
	
	/**
	 * @param oppoName
	 */
	public void setOppoName(String oppoName) {
		this.oppoName = oppoName;
	}
	
    /**
     * @return oppoName
     */
	public String getOppoName() {
		return this.oppoName;
	}
	
	/**
	 * @param oppoAccount
	 */
	public void setOppoAccount(String oppoAccount) {
		this.oppoAccount = oppoAccount;
	}
	
    /**
     * @return oppoAccount
     */
	public String getOppoAccount() {
		return this.oppoAccount;
	}
	
	/**
	 * @param cupPay4otherInd
	 */
	public void setCupPay4otherInd(String cupPay4otherInd) {
		this.cupPay4otherInd = cupPay4otherInd;
	}
	
    /**
     * @return cupPay4otherInd
     */
	public String getCupPay4otherInd() {
		return this.cupPay4otherInd;
	}
	
	/**
	 * @param cupPayAcctno
	 */
	public void setCupPayAcctno(String cupPayAcctno) {
		this.cupPayAcctno = cupPayAcctno;
	}
	
    /**
     * @return cupPayAcctno
     */
	public String getCupPayAcctno() {
		return this.cupPayAcctno;
	}
	
	/**
	 * @param cupPayName
	 */
	public void setCupPayName(String cupPayName) {
		this.cupPayName = cupPayName;
	}
	
    /**
     * @return cupPayName
     */
	public String getCupPayName() {
		return this.cupPayName;
	}
	
	/**
	 * @param counterpartyBankNoType
	 */
	public void setCounterpartyBankNoType(String counterpartyBankNoType) {
		this.counterpartyBankNoType = counterpartyBankNoType;
	}
	
    /**
     * @return counterpartyBankNoType
     */
	public String getCounterpartyBankNoType() {
		return this.counterpartyBankNoType;
	}
	
	/**
	 * @param counterpartyBankNoNo
	 */
	public void setCounterpartyBankNoNo(String counterpartyBankNoNo) {
		this.counterpartyBankNoNo = counterpartyBankNoNo;
	}
	
    /**
     * @return counterpartyBankNoNo
     */
	public String getCounterpartyBankNoNo() {
		return this.counterpartyBankNoNo;
	}
	
	/**
	 * @param counterpartyBankName
	 */
	public void setCounterpartyBankName(String counterpartyBankName) {
		this.counterpartyBankName = counterpartyBankName;
	}
	
    /**
     * @return counterpartyBankName
     */
	public String getCounterpartyBankName() {
		return this.counterpartyBankName;
	}
	
	/**
	 * @param counterpartyBankAreaNo
	 */
	public void setCounterpartyBankAreaNo(String counterpartyBankAreaNo) {
		this.counterpartyBankAreaNo = counterpartyBankAreaNo;
	}
	
    /**
     * @return counterpartyBankAreaNo
     */
	public String getCounterpartyBankAreaNo() {
		return this.counterpartyBankAreaNo;
	}
	
	/**
	 * @param filler
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
    /**
     * @return filler
     */
	public String getFiller() {
		return this.filler;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}