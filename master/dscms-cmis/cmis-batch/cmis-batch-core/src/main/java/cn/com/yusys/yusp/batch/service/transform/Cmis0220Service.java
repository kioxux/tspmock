package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0220Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0220</br>
 * 任务名称：加工任务-额度处理-批后额度比对结果处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0220Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0220Service.class);
    @Autowired
    private Cmis0220Mapper cmis0220Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 加工授信分项额度比对表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void insertTmpCompareLmtDetails(String openDay) {
        logger.info("truncateTmpCompareLmtDetails清空批复额度分项额度比对表开始,请求参数为:[{}]", openDay);
        // cmis0220Mapper.truncateTmpCompareLmtDetails();// cmis_lmt.tmp_compare_lmt_details
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_compare_lmt_details");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpCompareLmtDetails清空批复额度分项额度比对表结束");

        logger.info("插入授信分项额度比对表开始,请求参数为:[{}]", openDay);
        int insertTmpCompareLmtDetails = cmis0220Mapper.insertTmpCompareLmtDetails(openDay);
        logger.info("插入授信分项额度比对表结束,返回参数为:[{}]", insertTmpCompareLmtDetails);
    }

    /**
     * 加工授信分项占用关系额度比对表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void insertTmpCompareLmtContRel(String openDay) {
        logger.info("清空分项占用关系信息比对表开始,请求参数为:[{}]", openDay);
        // cmis0220Mapper.truncateTmpCompareLmtContRel();// cmis_lmt.tmp_compare_lmt_cont_Rel
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_compare_lmt_cont_Rel");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空分项占用关系信息比对表结束");

        logger.info("插入分项占用关系信息比对表开始,请求参数为:[{}]", openDay);
        int insertTmpCompareLmtContRel = cmis0220Mapper.insertTmpCompareLmtContRel(openDay);
        logger.info("插入分项占用关系信息比对表结束,返回参数为:[{}]", insertTmpCompareLmtContRel);
    }


    /**
     * 加工台账占用合同关系额度比对表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void insertTmpCompareContAccRel(String openDay) {
        logger.info("清空合同占用关系额度比对表开始,请求参数为:[{}]", openDay);
        // cmis0220Mapper.truncateTmpCompareContAccRel();// cmis_lmt.tmp_compare_cont_acc_rel
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_compare_cont_acc_rel");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空合同占用关系额度比对表结束");

        logger.info("插入合同占用关系额度比对表开始,请求参数为:[{}]", openDay);
        int insertTmpCompareContAccRel = cmis0220Mapper.insertTmpCompareContAccRel(openDay);
        logger.info("插入合同占用关系额度比对表结束,返回参数为:[{}]", insertTmpCompareContAccRel);
    }


    /**
     * 加工白名单额度比对表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void insertTmpCompareLmtWhiteInfo(String openDay) {
        logger.info("清空白名单额度信息表加工表开始,请求参数为:[{}]", openDay);
        // cmis0220Mapper.truncateTmpCompareLmtWhiteInfo();// cmis_lmt.tmp_compare_lmt_white_info
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_compare_lmt_white_info");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空白名单额度信息表加工表结束");

        logger.info("插入白名单额度信息表加工表开始,请求参数为:[{}]", openDay);
        int insertTmpCompareLmtWhiteInfo = cmis0220Mapper.insertTmpCompareLmtWhiteInfo(openDay);
        logger.info("插入白名单额度信息表加工表结束,返回参数为:[{}]", insertTmpCompareLmtWhiteInfo);
    }


    /**
     * 加工合作方额度比对表
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void insertTmpCompareApprCoopSubInfo(String openDay) {
        logger.info("清空合作方授信分项信息额度比对表开始,请求参数为:[{}]", openDay);
        // cmis0220Mapper.truncateTmpCompareApprCoopSubInfo();// cmis_lmt.tmp_compare_appr_coop_sub_info
        tableUtilsService.renameCreateDropTable("cmis_lmt", "tmp_compare_appr_coop_sub_info");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("清空合作方授信分项信息额度比对表结束");

        logger.info("插入合作方授信分项信息额度比对表开始,请求参数为:[{}]", openDay);
        int insertTmpCompareApprCoopSubInfo = cmis0220Mapper.insertTmpCompareApprCoopSubInfo(openDay);
        logger.info("插入合作方授信分项信息额度比对表结束,返回参数为:[{}]", insertTmpCompareApprCoopSubInfo);
    }
}
