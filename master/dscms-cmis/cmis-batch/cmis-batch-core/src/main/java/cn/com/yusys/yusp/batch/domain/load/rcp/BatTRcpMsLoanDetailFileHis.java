/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpMsLoanDetailFileHis
 * @类描述: bat_t_rcp_ms_loan_detail_file_his数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:04:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_rcp_ms_loan_detail_file_his")
public class BatTRcpMsLoanDetailFileHis extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 业务日期 YYYYMMDD **/
	@Id
	@Column(name = "biz_date", unique = false, nullable = false, length = 10)
	private String bizDate;
	/** 合同号 每笔借据对应唯一的合同号 **/
	@Id
	@Column(name = "contr_nbr")
	private String contrNbr;

	/** 贷款编号 交易参考号 **/
	@Id
	@Column(name = "ref_nbr")
	private String refNbr;

	/** 放款日期 YYYYMMDD **/
	@Column(name = "txn_date", unique = false, nullable = false, length = 8)
	private String txnDate;
	
	/** 到期日期 **/
	@Column(name = "maturity", unique = false, nullable = false, length = 8)
	private String maturity;
	
	/** 放款金额 合作方银行承贷部分金额 **/
	@Column(name = "txn_amt", unique = false, nullable = false, length = 17)
	private java.math.BigDecimal txnAmt;
	
	/** 放款总期数 **/
	@Column(name = "init_term", unique = false, nullable = false, length = 10)
	private Integer initTerm;
	
	/** 还款方式1-等额本息;2-等额本金 **/
	@Column(name = "pmt_type", unique = false, nullable = false, length = 1)
	private String pmtType;
	
	/** 录入日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "input_time", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "last_update_date", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "last_update_time", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	
	/**
	 * @param bizDate
	 */
	public void setBizDate(String bizDate) {
		this.bizDate = bizDate;
	}
	
    /**
     * @return bizDate
     */
	public String getBizDate() {
		return this.bizDate;
	}
	
	/**
	 * @param contrNbr
	 */
	public void setContrNbr(String contrNbr) {
		this.contrNbr = contrNbr;
	}
	
    /**
     * @return contrNbr
     */
	public String getContrNbr() {
		return this.contrNbr;
	}
	
	/**
	 * @param refNbr
	 */
	public void setRefNbr(String refNbr) {
		this.refNbr = refNbr;
	}
	
    /**
     * @return refNbr
     */
	public String getRefNbr() {
		return this.refNbr;
	}
	
	/**
	 * @param txnDate
	 */
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	
    /**
     * @return txnDate
     */
	public String getTxnDate() {
		return this.txnDate;
	}
	
	/**
	 * @param maturity
	 */
	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}
	
    /**
     * @return maturity
     */
	public String getMaturity() {
		return this.maturity;
	}
	
	/**
	 * @param txnAmt
	 */
	public void setTxnAmt(java.math.BigDecimal txnAmt) {
		this.txnAmt = txnAmt;
	}
	
    /**
     * @return txnAmt
     */
	public java.math.BigDecimal getTxnAmt() {
		return this.txnAmt;
	}
	
	/**
	 * @param initTerm
	 */
	public void setInitTerm(Integer initTerm) {
		this.initTerm = initTerm;
	}
	
    /**
     * @return initTerm
     */
	public Integer getInitTerm() {
		return this.initTerm;
	}
	
	/**
	 * @param pmtType
	 */
	public void setPmtType(String pmtType) {
		this.pmtType = pmtType;
	}
	
    /**
     * @return pmtType
     */
	public String getPmtType() {
		return this.pmtType;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}


}