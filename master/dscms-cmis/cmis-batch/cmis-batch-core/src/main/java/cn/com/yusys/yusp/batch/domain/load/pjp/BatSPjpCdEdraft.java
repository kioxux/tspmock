/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpCdEdraft
 * @类描述: bat_s_pjp_cd_edraft数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-03 10:58:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_pjp_cd_edraft")
public class BatSPjpCdEdraft extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 票据种类 **/
	@Column(name = "ED_TP", unique = false, nullable = true, length = 10)
	private String edTp;
	
	/** 票据号码 **/
	@Column(name = "ED_IDNB", unique = false, nullable = true, length = 50)
	private String edIdnb;
	
	/** 币种 **/
	@Column(name = "ED_ISSEAMTVALUE", unique = false, nullable = true, length = 5)
	private String edIsseamtvalue;
	
	/** 票据金额 **/
	@Column(name = "ED_ISSEAMT", unique = false, nullable = true, length = 25)
	private java.math.BigDecimal edIsseamt;
	
	/** 出票日期 **/
	@Column(name = "ED_ISSEDT", unique = false, nullable = true, length = 20)
	private String edIssedt;
	
	/** 到期日 **/
	@Column(name = "ED_DUEDT", unique = false, nullable = true, length = 20)
	private String edDuedt;
	
	/** 票据交易合同号 **/
	@Column(name = "ED_TXLCTRCTNB", unique = false, nullable = true, length = 50)
	private String edTxlctrctnb;
	
	/** 票据发票号码 **/
	@Column(name = "ED_INVCNB", unique = false, nullable = true, length = 50)
	private String edInvcnb;
	
	/** 不得转让标记 **/
	@Column(name = "ED_BANENDRSMTMK", unique = false, nullable = true, length = 10)
	private String edBanendrsmtmk;
	
	/** 出票人类别 **/
	@Column(name = "ED_DRWRROLE", unique = false, nullable = true, length = 10)
	private String edDrwrrole;
	
	/** 出票人名称 **/
	@Column(name = "ED_DRWRNM", unique = false, nullable = true, length = 400)
	private String edDrwrnm;
	
	/** 出票人组织机构代码 **/
	@Column(name = "ED_DRWRCMONID", unique = false, nullable = true, length = 50)
	private String edDrwrcmonid;
	
	/** 出票人账号 **/
	@Column(name = "ED_DRWRACCTID", unique = false, nullable = true, length = 50)
	private String edDrwracctid;
	
	/** 出票人开户行行号 **/
	@Column(name = "ED_DRWRACCTSVCR", unique = false, nullable = true, length = 12)
	private String edDrwracctsvcr;
	
	/** 出票人承接行行号 **/
	@Column(name = "ED_DRWRAGCYACCTSVCR", unique = false, nullable = true, length = 15)
	private String edDrwragcyacctsvcr;
	
	/** 出票人信用等级 **/
	@Column(name = "ED_DRWRCDTRATGS", unique = false, nullable = true, length = 10)
	private String edDrwrcdtratgs;
	
	/** 出票人评价机构 **/
	@Column(name = "ED_DRWRCDTRATGAGCY", unique = false, nullable = true, length = 100)
	private String edDrwrcdtratgagcy;
	
	/** 出票人评级到期日 **/
	@Column(name = "ED_DRWRCDTRATGDUEDT", unique = false, nullable = true, length = 20)
	private String edDrwrcdtratgduedt;
	
	/** 出票保证人名称 **/
	@Column(name = "ED_DRWRGUARNTRNM", unique = false, nullable = true, length = 400)
	private String edDrwrguarntrnm;
	
	/** 出票保证人保证日期 **/
	@Column(name = "ED_DRWRGUARNTEEDT", unique = false, nullable = true, length = 20)
	private String edDrwrguarnteedt;
	
	/** 收款人名称 **/
	@Column(name = "ED_PYEENM", unique = false, nullable = true, length = 400)
	private String edPyeenm;
	
	/** 收款人账号 **/
	@Column(name = "ED_PYEEACCTID", unique = false, nullable = true, length = 50)
	private String edPyeeacctid;
	
	/** 收款人开户行行号 **/
	@Column(name = "ED_PYEEACCTSVCR", unique = false, nullable = true, length = 12)
	private String edPyeeacctsvcr;
	
	/** 承兑人名称 **/
	@Column(name = "ED_ACCPTRNM", unique = false, nullable = true, length = 400)
	private String edAccptrnm;
	
	/** 承兑人账号 **/
	@Column(name = "ED_ACCPTRID", unique = false, nullable = true, length = 50)
	private String edAccptrid;
	
	/** 承兑人开户行行号 **/
	@Column(name = "ED_ACCPTRSVCR", unique = false, nullable = true, length = 12)
	private String edAccptrsvcr;
	
	/** 保证人名称 **/
	@Column(name = "ED_ACCPTRGUARNTRNM", unique = false, nullable = true, length = 400)
	private String edAccptrguarntrnm;
	
	/** 承兑承兑保证日期(本行出票) **/
	@Column(name = "ED_ACCPTRGUARNTEEDT", unique = false, nullable = true, length = 20)
	private String edAccptrguarnteedt;
	
	/** 保证金账号 **/
	@Column(name = "ED_MARGINACCT", unique = false, nullable = true, length = 50)
	private String edMarginacct;
	
	/** 最近一次交易ID **/
	@Column(name = "ED_LASTMSGID", unique = false, nullable = true, length = 50)
	private String edLastmsgid;
	
	/** 最近一次报文发送时间 **/
	@Column(name = "ED_LASTMSGDATE", unique = false, nullable = true, length = 20)
	private String edLastmsgdate;
	
	/** 当前行内处理状态 **/
	@Column(name = "ED_CURECDSDRAFTSTS", unique = false, nullable = true, length = 100)
	private String edCurecdsdraftsts;
	
	/** 当前票据状态编码 **/
	@Column(name = "ED_CURECDSDRAFTSTSCODE", unique = false, nullable = true, length = 50)
	private String edCurecdsdraftstscode;
	
	/** 上一次票据状态(行内出票、ECDS状

态) **/
	@Column(name = "ED_LASTCURECDSDRAFTSTS", unique = false, nullable = true, length = 100)
	private String edLastcurecdsdraftsts;
	
	/** 上一次票据状态编码 **/
	@Column(name = "ED_LASTCURECDSDRAFTSTSCODE", unique = false, nullable = true, length = 50)
	private String edLastcurecdsdraftstscode;
	
	/** 票据承兑类型(本行承兑、本行客户承兑、他行

承兑) **/
	@Column(name = "ED_DRAFTACCPTRTP", unique = false, nullable = true, length = 40)
	private String edDraftaccptrtp;
	
	/** 票据介质类型(纸票、电票) **/
	@Column(name = "ED_DRAFTSTUFF", unique = false, nullable = true, length = 20)
	private String edDraftstuff;
	
	/** 银票,商票 **/
	@Column(name = "ED_DRAFTTP", unique = false, nullable = true, length = 20)
	private String edDrafttp;
	
	/** 票据所有状态(完全所有、部分权限、无权

限) **/
	@Column(name = "ED_DRAFTUSEAUTHRTY", unique = false, nullable = true, length = 50)
	private String edDraftuseauthrty;
	
	/** 票据持有类型 **/
	@Column(name = "ED_DRAFTOWNERSTS", unique = false, nullable = true, length = 50)
	private String edDraftownersts;
	
	/** 当前行内处理状态 **/
	@Column(name = "ED_CURINNERDRAFTSTS", unique = false, nullable = true, length = 50)
	private String edCurinnerdraftsts;
	
	/** 当前交易类型（出票、承兑、提示收票、贴现买入、

贴现卖出、转贴现买入等） **/
	@Column(name = "ED_CURTXLTP", unique = false, nullable = true, length = 50)
	private String edCurtxltp;
	
	/** 当前交易类型代码 **/
	@Column(name = "ED_CURTXLTPNO", unique = false, nullable = true, length = 100)
	private String edCurtxltpno;
	
	/** 票据持有行行号 **/
	@Column(name = "ED_DRAFTOWNERSVCR", unique = false, nullable = true, length = 40)
	private String edDraftownersvcr;
	
	/** 票据当前持有人帐号(包括客户持有、本行持

有) **/
	@Column(name = "ED_DRAFTOWNERACCTID", unique = false, nullable = true, length = 60)
	private String edDraftowneracctid;
	
	/** 票据持有客户组织机构代码(本行客户持有时

有效) **/
	@Column(name = "ED_DRAFTOWNERCMONID", unique = false, nullable = true, length = 40)
	private String edDraftownercmonid;
	
	/** 票据持有行机构ID(系统部门树上的机构

ID) **/
	@Column(name = "ED_DRAFTOWNINNERORGID", unique = false, nullable = true, length = 50)
	private String edDraftowninnerorgid;
	
	/** 出票客户ID(本行出票，对应的客户管理系

统ID) **/
	@Column(name = "ED_DRWRINNERENTPUEID", unique = false, nullable = true, length = 50)
	private String edDrwrinnerentpueid;
	
	/** 票据所属客户ID **/
	@Column(name = "ED_DRAFTOWNCUSTOMERID", unique = false, nullable = true, length = 50)
	private String edDraftowncustomerid;
	
	/** 票据持有人名称 **/
	@Column(name = "ED_DRAFTOWNERNAME", unique = false, nullable = true, length = 200)
	private String edDraftownername;
	
	/** 承兑人信用等级 **/
	@Column(name = "ED_ACCPTRCDTRATGS", unique = false, nullable = true, length = 10)
	private String edAccptrcdtratgs;
	
	/** 承兑人评级到期日 **/
	@Column(name = "ED_ACCPTRCDTRATGAGCY", unique = false, nullable = true, length = 120)
	private String edAccptrcdtratgagcy;
	
	/** 承兑人评级到期日 **/
	@Column(name = "ED_ACCPTRCDTRATGDUEDT", unique = false, nullable = true, length = 20)
	private String edAccptrcdtratgduedt;
	
	/** 承兑回复日期 **/
	@Column(name = "ED_ACCPTRSGNUPDT", unique = false, nullable = true, length = 20)
	private String edAccptrsgnupdt;
	
	/** 产品ID **/
	@Column(name = "PRODUCT_ID", unique = false, nullable = true, length = 40)
	private String productId;
	
	/** 持票人名称 **/
	@Column(name = "S_OWNER_BILL_NAME", unique = false, nullable = true, length = 40)
	private String sOwnerBillName;
	
	/** 创建日期 **/
	@Column(name = "D_CREATE_DT", unique = false, nullable = true, length = 10)
	private String dCreateDt;
	
	/** 备注 **/
	@Column(name = "S_REMARK", unique = false, nullable = true, length = 800)
	private String sRemark;
	
	/** 用途 **/
	@Column(name = "S_USAGE", unique = false, nullable = true, length = 80)
	private String sUsage;
	
	/** 收款人开户行全称 **/
	@Column(name = "S_PAYEE_BANK_NAME", unique = false, nullable = true, length = 120)
	private String sPayeeBankName;
	
	/** 承兑行名称 **/
	@Column(name = "S_ACCEPTOR_BANK_NAME", unique = false, nullable = true, length = 120)
	private String sAcceptorBankName;
	
	/** 出票人开户行名称 **/
	@Column(name = "S_ISSUER_BANK_NAME", unique = false, nullable = true, length = 120)
	private String sIssuerBankName;
	
	/** 是否我行承兑 **/
	@Column(name = "S_IF_DIRECT_ACCEP", unique = false, nullable = true, length = 1)
	private String sIfDirectAccep;
	
	/** 追索标识 **/
	@Column(name = "S_STROVER_SIGN", unique = false, nullable = true, length = 40)
	private String sStroverSign;
	
	/** 处理标识 **/
	@Column(name = "S_DEAL_STATUS", unique = false, nullable = true, length = 10)
	private String sDealStatus;
	
	/** 清算方式 **/
	@Column(name = "CLEARING_FLAG", unique = false, nullable = true, length = 10)
	private String clearingFlag;
	
	/** 票据交易合同号 **/
	@Column(name = "ED_CONTRACTNB", unique = false, nullable = true, length = 50)
	private String edContractnb;
	
	/** 备注 **/
	@Column(name = "ED_RMRK", unique = false, nullable = true, length = 800)
	private String edRmrk;
	
	/** 票据逾期状态 **/
	@Column(name = "ED_OVERFLAG", unique = false, nullable = true, length = 4)
	private String edOverflag;
	
	/** 票据持有人开户行名称 **/
	@Column(name = "S_DRAFTOWNERSVCR_NAME", unique = false, nullable = true, length = 120)
	private String sDraftownersvcrName;
	
	/** 承兑协议号 **/
	@Column(name = "ED_ACCEPTOR_PROTO", unique = false, nullable = true, length = 40)
	private String edAcceptorProto;
	
	/** 承兑日期 **/
	@Column(name = "ED_ACCEPTOR_DT", unique = false, nullable = true, length = 20)
	private String edAcceptorDt;
	
	/** 承兑人地址 **/
	@Column(name = "ED_ACCEPTOR_ADDRESS", unique = false, nullable = true, length = 120)
	private String edAcceptorAddress;
	
	/** 发送方报文控制状态 **/
	@Column(name = "S_SENDER_MSG_STATUS", unique = false, nullable = true, length = 10)
	private String sSenderMsgStatus;
	
	/** 接收方报文控制状态 **/
	@Column(name = "S_RECEIVER_MSG_STATUS", unique = false, nullable = true, length = 10)
	private String sReceiverMsgStatus;
	
	/** 是否双向 STD_RZ_IF_BIDIRECT **/
	@Column(name = "S_IF_BIDIRECT", unique = false, nullable = true, length = 10)
	private String sIfBidirect;
	
	/** 是否已存在 STD_RZ_IF_EXIST **/
	@Column(name = "BILL_IF_EXIST", unique = false, nullable = true, length = 10)
	private String billIfExist;
	
	/** 批次号 **/
	@Column(name = "BATCH_NO", unique = false, nullable = true, length = 40)
	private String batchNo;
	
	/** 明细id **/
	@Column(name = "BILL_ID", unique = false, nullable = true, length = 40)
	private String billId;
	
	/** 核心批次号 **/
	@Column(name = "S_COREBATCHNUMBER", unique = false, nullable = true, length = 50)
	private String sCorebatchnumber;
	
	/** 是否确认 STD_BANK_JUDGE **/
	@Column(name = "BANK_JUDGE", unique = false, nullable = true, length = 10)
	private String bankJudge;
	
	/** 暂存信息 **/
	@Column(name = "TS_ZANCUN", unique = false, nullable = true, length = 50)
	private String tsZancun;
	
	/** 明细主键 **/
	@Column(name = "DISCBILLID", unique = false, nullable = true, length = 50)
	private String discbillid;
	
	/** 批次号 **/
	@Column(name = "S_BATCH_NO", unique = false, nullable = true, length = 50)
	private String sBatchNo;
	
	/** 出入库状态 **/
	@Column(name = "INSTORAGE_STS", unique = false, nullable = true, length = 2)
	private String instorageSts;
	
	/** 贴现日/转贴现日 **/
	@Column(name = "ED_BUYDT", unique = false, nullable = true, length = 20)
	private String edBuydt;
	
	/** 【作废】贴现转贴现买入保存129帐号 供打印

记账凭证使用 **/
	@Column(name = "ED_ACCOUNT_129NUM", unique = false, nullable = true, length = 40)
	private String edAccount129num;
	
	/** 纸票状态 **/
	@Column(name = "S_PCDSSTATUS", unique = false, nullable = true, length = 100)
	private String sPcdsstatus;
	
	/** 是否有影像附件  默认为0 STD_RZ_HAS_IIMAGE **/
	@Column(name = "HAS_IMAGE", unique = false, nullable = true, length = 2)
	private String hasImage;
	
	/** 承兑行类别 **/
	@Column(name = "ACCP_BANK_SORT", unique = false, nullable = true, length = 10)
	private String accpBankSort;
	
	/** 票据所有权 STD_RZ_PROPRIETORSHIP **/
	@Column(name = "PROPRIETORSHIP", unique = false, nullable = true, length = 2)
	private String proprietorship;
	
	/** 最近买入类型票据买入方式 **/
	@Column(name = "BILLBUYMODE", unique = false, nullable = true, length = 10)
	private String billbuymode;
	
	/** 买入成本利率 **/
	@Column(name = "BILLBUYCOSTRATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal billbuycostrate;
	
	/** 票据持有日期(买断式买入为票据到期日、买入返售的为返售到期日、双买的为双买到期日) **/
	@Column(name = "BILLPOSSESSDATE", unique = false, nullable = true, length = 20)
	private String billpossessdate;
	
	/** 买入成本利率类型 **/
	@Column(name = "BUYCOSTRATETYPE", unique = false, nullable = true, length = 10)
	private String buycostratetype;
	
	/** 票据流通性 **/
	@Column(name = "S_NEGOTIABILITY", unique = false, nullable = true, length = 50)
	private String sNegotiability;
	
	/** 瑕疵原因 **/
	@Column(name = "S_NEGOTIABILITYREASON", unique = false, nullable = true, length = 50)
	private String sNegotiabilityreason;
	
	/** 瑕疵原因备注 **/
	@Column(name = "S_NEGOTIABILITYRMK", unique = false, nullable = true, length = 100)
	private String sNegotiabilityrmk;
	
	/** 【作废】是否存放在本行 **/
	@Column(name = "S_DRAFTSTOREPLACE", unique = false, nullable = true, length = 50)
	private String sDraftstoreplace;
	
	/** 【作废】票据存放地名称  **/
	@Column(name = "S_DRAFTSTOREPLACENAME", unique = false, nullable = true, length = 200)
	private String sDraftstoreplacename;
	
	/** 【作废】票据存放地行号 **/
	@Column(name = "S_DRAFTSTOREPLACEBANKCODE", unique = false, nullable = true, length = 20)
	private String sDraftstoreplacebankcode;
	
	/** 【作废】买入机构 **/
	@Column(name = "S_MRJG", unique = false, nullable = true, length = 10)
	private String sMrjg;
	
	/** 【作废】承兑机构 **/
	@Column(name = "S_CDJG", unique = false, nullable = true, length = 10)
	private String sCdjg;
	
	/** 代保管对象ID **/
	@Column(name = "DRAFTSTORAGE_ID", unique = false, nullable = true, length = 40)
	private String draftstorageId;
	
	/** 票据池对象ID **/
	@Column(name = "DRAFTPOOL_ID", unique = false, nullable = true, length = 40)
	private String draftpoolId;
	
	/** 是否为风险票据 STD_RZ_RISK_BILL **/
	@Column(name = "S_IF_RISK_BILL", unique = false, nullable = true, length = 2)
	private String sIfRiskBill;
	
	/** 是否是票据池对象 **/
	@Column(name = "S_IF_POOLFLAG", unique = false, nullable = true, length = 10)
	private String sIfPoolflag;
	
	/** 【作废】 **/
	@Column(name = "ED_DRAFTOWNERBANKNAME", unique = false, nullable = true, length = 80)
	private String edDraftownerbankname;
	
	/** 【作废】 **/
	@Column(name = "ED_DRAFTOWNERBANKCODE", unique = false, nullable = true, length = 40)
	private String edDraftownerbankcode;
	
	/** 【作废】OUT_001表示托收出库，OUT_002表示非托

收出库 **/
	@Column(name = "STOCK_OUT_TYPE", unique = false, nullable = true, length = 10)
	private String stockOutType;
	
	/** 【作废】批次号  **/
	@Column(name = "ED_BATCH_NO", unique = false, nullable = true, length = 20)
	private String edBatchNo;
	
	/** 【作废】信贷借据号 **/
	@Column(name = "JJH", unique = false, nullable = true, length = 50)
	private String jjh;
	
	/** 【作废】托收出库状态 **/
	@Column(name = "TS_STOREOUTTYPE", unique = false, nullable = true, length = 10)
	private String tsStoreouttype;
	
	/** 【作废】出库批次号 **/
	@Column(name = "ED_OUTSTOCK_BATCH_NO", unique = false, nullable = true, length = 20)
	private String edOutstockBatchNo;
	
	/** 【作废】托收批次ID **/
	@Column(name = "CD_EDRAFTBATCHID", unique = false, nullable = true, length = 40)
	private String cdEdraftbatchid;
	
	/** 【作废】托收退回原因 **/
	@Column(name = "TS_BACKRMK", unique = false, nullable = true, length = 2000)
	private String tsBackrmk;
	
	/** 库管机构id **/
	@Column(name = "ED_STOCKORGID", unique = false, nullable = true, length = 50)
	private String edStockorgid;
	
	/** 买入来源ID **/
	@Column(name = "SOURCE_ID", unique = false, nullable = true, length = 50)
	private String sourceId;
	
	/** 上次买入来源ID  **/
	@Column(name = "LAST_SOURCE_ID", unique = false, nullable = true, length = 50)
	private String lastSourceId;
	
	/** 余额标记 STD_RZ_BALANCE_FLAG **/
	@Column(name = "BALANCE_FLAG", unique = false, nullable = true, length = 10)
	private String balanceFlag;
	
	/** 余额机构号 **/
	@Column(name = "BALANCE_BRCH_ID", unique = false, nullable = true, length = 60)
	private String balanceBrchId;
	
	/** 最初来源 业务类型 **/
	@Column(name = "SOURCE_BUY_MODE", unique = false, nullable = true, length = 10)
	private String sourceBuyMode;
	
	/** 原产品ID   撤销记账使用  **/
	@Column(name = "OLD_PRODUCT_ID", unique = false, nullable = true, length = 60)
	private String oldProductId;
	
	/** 买入批次号 **/
	@Column(name = "BUYINBATCHNO", unique = false, nullable = true, length = 40)
	private String buyinbatchno;
	
	/** 卖出回购业务专用字段卖出回购式存 卖出清单ID **/
	@Column(name = "SALE_ID", unique = false, nullable = true, length = 60)
	private String saleId;
	
	/** 交易机构Id **/
	@Column(name = "TRADEBRANCHID", unique = false, nullable = true, length = 40)
	private String tradebranchid;
	
	/** 隔夜待持 **/
	@Column(name = "OVERNIGHT_FLAG", unique = false, nullable = true, length = 6)
	private String overnightFlag;
	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param edTp
	 */
	public void setEdTp(String edTp) {
		this.edTp = edTp;
	}
	
    /**
     * @return edTp
     */
	public String getEdTp() {
		return this.edTp;
	}
	
	/**
	 * @param edIdnb
	 */
	public void setEdIdnb(String edIdnb) {
		this.edIdnb = edIdnb;
	}
	
    /**
     * @return edIdnb
     */
	public String getEdIdnb() {
		return this.edIdnb;
	}
	
	/**
	 * @param edIsseamtvalue
	 */
	public void setEdIsseamtvalue(String edIsseamtvalue) {
		this.edIsseamtvalue = edIsseamtvalue;
	}
	
    /**
     * @return edIsseamtvalue
     */
	public String getEdIsseamtvalue() {
		return this.edIsseamtvalue;
	}
	
	/**
	 * @param edIsseamt
	 */
	public void setEdIsseamt(java.math.BigDecimal edIsseamt) {
		this.edIsseamt = edIsseamt;
	}
	
    /**
     * @return edIsseamt
     */
	public java.math.BigDecimal getEdIsseamt() {
		return this.edIsseamt;
	}
	
	/**
	 * @param edIssedt
	 */
	public void setEdIssedt(String edIssedt) {
		this.edIssedt = edIssedt;
	}
	
    /**
     * @return edIssedt
     */
	public String getEdIssedt() {
		return this.edIssedt;
	}
	
	/**
	 * @param edDuedt
	 */
	public void setEdDuedt(String edDuedt) {
		this.edDuedt = edDuedt;
	}
	
    /**
     * @return edDuedt
     */
	public String getEdDuedt() {
		return this.edDuedt;
	}
	
	/**
	 * @param edTxlctrctnb
	 */
	public void setEdTxlctrctnb(String edTxlctrctnb) {
		this.edTxlctrctnb = edTxlctrctnb;
	}
	
    /**
     * @return edTxlctrctnb
     */
	public String getEdTxlctrctnb() {
		return this.edTxlctrctnb;
	}
	
	/**
	 * @param edInvcnb
	 */
	public void setEdInvcnb(String edInvcnb) {
		this.edInvcnb = edInvcnb;
	}
	
    /**
     * @return edInvcnb
     */
	public String getEdInvcnb() {
		return this.edInvcnb;
	}
	
	/**
	 * @param edBanendrsmtmk
	 */
	public void setEdBanendrsmtmk(String edBanendrsmtmk) {
		this.edBanendrsmtmk = edBanendrsmtmk;
	}
	
    /**
     * @return edBanendrsmtmk
     */
	public String getEdBanendrsmtmk() {
		return this.edBanendrsmtmk;
	}
	
	/**
	 * @param edDrwrrole
	 */
	public void setEdDrwrrole(String edDrwrrole) {
		this.edDrwrrole = edDrwrrole;
	}
	
    /**
     * @return edDrwrrole
     */
	public String getEdDrwrrole() {
		return this.edDrwrrole;
	}
	
	/**
	 * @param edDrwrnm
	 */
	public void setEdDrwrnm(String edDrwrnm) {
		this.edDrwrnm = edDrwrnm;
	}
	
    /**
     * @return edDrwrnm
     */
	public String getEdDrwrnm() {
		return this.edDrwrnm;
	}
	
	/**
	 * @param edDrwrcmonid
	 */
	public void setEdDrwrcmonid(String edDrwrcmonid) {
		this.edDrwrcmonid = edDrwrcmonid;
	}
	
    /**
     * @return edDrwrcmonid
     */
	public String getEdDrwrcmonid() {
		return this.edDrwrcmonid;
	}
	
	/**
	 * @param edDrwracctid
	 */
	public void setEdDrwracctid(String edDrwracctid) {
		this.edDrwracctid = edDrwracctid;
	}
	
    /**
     * @return edDrwracctid
     */
	public String getEdDrwracctid() {
		return this.edDrwracctid;
	}
	
	/**
	 * @param edDrwracctsvcr
	 */
	public void setEdDrwracctsvcr(String edDrwracctsvcr) {
		this.edDrwracctsvcr = edDrwracctsvcr;
	}
	
    /**
     * @return edDrwracctsvcr
     */
	public String getEdDrwracctsvcr() {
		return this.edDrwracctsvcr;
	}
	
	/**
	 * @param edDrwragcyacctsvcr
	 */
	public void setEdDrwragcyacctsvcr(String edDrwragcyacctsvcr) {
		this.edDrwragcyacctsvcr = edDrwragcyacctsvcr;
	}
	
    /**
     * @return edDrwragcyacctsvcr
     */
	public String getEdDrwragcyacctsvcr() {
		return this.edDrwragcyacctsvcr;
	}
	
	/**
	 * @param edDrwrcdtratgs
	 */
	public void setEdDrwrcdtratgs(String edDrwrcdtratgs) {
		this.edDrwrcdtratgs = edDrwrcdtratgs;
	}
	
    /**
     * @return edDrwrcdtratgs
     */
	public String getEdDrwrcdtratgs() {
		return this.edDrwrcdtratgs;
	}
	
	/**
	 * @param edDrwrcdtratgagcy
	 */
	public void setEdDrwrcdtratgagcy(String edDrwrcdtratgagcy) {
		this.edDrwrcdtratgagcy = edDrwrcdtratgagcy;
	}
	
    /**
     * @return edDrwrcdtratgagcy
     */
	public String getEdDrwrcdtratgagcy() {
		return this.edDrwrcdtratgagcy;
	}
	
	/**
	 * @param edDrwrcdtratgduedt
	 */
	public void setEdDrwrcdtratgduedt(String edDrwrcdtratgduedt) {
		this.edDrwrcdtratgduedt = edDrwrcdtratgduedt;
	}
	
    /**
     * @return edDrwrcdtratgduedt
     */
	public String getEdDrwrcdtratgduedt() {
		return this.edDrwrcdtratgduedt;
	}
	
	/**
	 * @param edDrwrguarntrnm
	 */
	public void setEdDrwrguarntrnm(String edDrwrguarntrnm) {
		this.edDrwrguarntrnm = edDrwrguarntrnm;
	}
	
    /**
     * @return edDrwrguarntrnm
     */
	public String getEdDrwrguarntrnm() {
		return this.edDrwrguarntrnm;
	}
	
	/**
	 * @param edDrwrguarnteedt
	 */
	public void setEdDrwrguarnteedt(String edDrwrguarnteedt) {
		this.edDrwrguarnteedt = edDrwrguarnteedt;
	}
	
    /**
     * @return edDrwrguarnteedt
     */
	public String getEdDrwrguarnteedt() {
		return this.edDrwrguarnteedt;
	}
	
	/**
	 * @param edPyeenm
	 */
	public void setEdPyeenm(String edPyeenm) {
		this.edPyeenm = edPyeenm;
	}
	
    /**
     * @return edPyeenm
     */
	public String getEdPyeenm() {
		return this.edPyeenm;
	}
	
	/**
	 * @param edPyeeacctid
	 */
	public void setEdPyeeacctid(String edPyeeacctid) {
		this.edPyeeacctid = edPyeeacctid;
	}
	
    /**
     * @return edPyeeacctid
     */
	public String getEdPyeeacctid() {
		return this.edPyeeacctid;
	}
	
	/**
	 * @param edPyeeacctsvcr
	 */
	public void setEdPyeeacctsvcr(String edPyeeacctsvcr) {
		this.edPyeeacctsvcr = edPyeeacctsvcr;
	}
	
    /**
     * @return edPyeeacctsvcr
     */
	public String getEdPyeeacctsvcr() {
		return this.edPyeeacctsvcr;
	}
	
	/**
	 * @param edAccptrnm
	 */
	public void setEdAccptrnm(String edAccptrnm) {
		this.edAccptrnm = edAccptrnm;
	}
	
    /**
     * @return edAccptrnm
     */
	public String getEdAccptrnm() {
		return this.edAccptrnm;
	}
	
	/**
	 * @param edAccptrid
	 */
	public void setEdAccptrid(String edAccptrid) {
		this.edAccptrid = edAccptrid;
	}
	
    /**
     * @return edAccptrid
     */
	public String getEdAccptrid() {
		return this.edAccptrid;
	}
	
	/**
	 * @param edAccptrsvcr
	 */
	public void setEdAccptrsvcr(String edAccptrsvcr) {
		this.edAccptrsvcr = edAccptrsvcr;
	}
	
    /**
     * @return edAccptrsvcr
     */
	public String getEdAccptrsvcr() {
		return this.edAccptrsvcr;
	}
	
	/**
	 * @param edAccptrguarntrnm
	 */
	public void setEdAccptrguarntrnm(String edAccptrguarntrnm) {
		this.edAccptrguarntrnm = edAccptrguarntrnm;
	}
	
    /**
     * @return edAccptrguarntrnm
     */
	public String getEdAccptrguarntrnm() {
		return this.edAccptrguarntrnm;
	}
	
	/**
	 * @param edAccptrguarnteedt
	 */
	public void setEdAccptrguarnteedt(String edAccptrguarnteedt) {
		this.edAccptrguarnteedt = edAccptrguarnteedt;
	}
	
    /**
     * @return edAccptrguarnteedt
     */
	public String getEdAccptrguarnteedt() {
		return this.edAccptrguarnteedt;
	}
	
	/**
	 * @param edMarginacct
	 */
	public void setEdMarginacct(String edMarginacct) {
		this.edMarginacct = edMarginacct;
	}
	
    /**
     * @return edMarginacct
     */
	public String getEdMarginacct() {
		return this.edMarginacct;
	}
	
	/**
	 * @param edLastmsgid
	 */
	public void setEdLastmsgid(String edLastmsgid) {
		this.edLastmsgid = edLastmsgid;
	}
	
    /**
     * @return edLastmsgid
     */
	public String getEdLastmsgid() {
		return this.edLastmsgid;
	}
	
	/**
	 * @param edLastmsgdate
	 */
	public void setEdLastmsgdate(String edLastmsgdate) {
		this.edLastmsgdate = edLastmsgdate;
	}
	
    /**
     * @return edLastmsgdate
     */
	public String getEdLastmsgdate() {
		return this.edLastmsgdate;
	}
	
	/**
	 * @param edCurecdsdraftsts
	 */
	public void setEdCurecdsdraftsts(String edCurecdsdraftsts) {
		this.edCurecdsdraftsts = edCurecdsdraftsts;
	}
	
    /**
     * @return edCurecdsdraftsts
     */
	public String getEdCurecdsdraftsts() {
		return this.edCurecdsdraftsts;
	}
	
	/**
	 * @param edCurecdsdraftstscode
	 */
	public void setEdCurecdsdraftstscode(String edCurecdsdraftstscode) {
		this.edCurecdsdraftstscode = edCurecdsdraftstscode;
	}
	
    /**
     * @return edCurecdsdraftstscode
     */
	public String getEdCurecdsdraftstscode() {
		return this.edCurecdsdraftstscode;
	}
	
	/**
	 * @param edLastcurecdsdraftsts
	 */
	public void setEdLastcurecdsdraftsts(String edLastcurecdsdraftsts) {
		this.edLastcurecdsdraftsts = edLastcurecdsdraftsts;
	}
	
    /**
     * @return edLastcurecdsdraftsts
     */
	public String getEdLastcurecdsdraftsts() {
		return this.edLastcurecdsdraftsts;
	}
	
	/**
	 * @param edLastcurecdsdraftstscode
	 */
	public void setEdLastcurecdsdraftstscode(String edLastcurecdsdraftstscode) {
		this.edLastcurecdsdraftstscode = edLastcurecdsdraftstscode;
	}
	
    /**
     * @return edLastcurecdsdraftstscode
     */
	public String getEdLastcurecdsdraftstscode() {
		return this.edLastcurecdsdraftstscode;
	}
	
	/**
	 * @param edDraftaccptrtp
	 */
	public void setEdDraftaccptrtp(String edDraftaccptrtp) {
		this.edDraftaccptrtp = edDraftaccptrtp;
	}
	
    /**
     * @return edDraftaccptrtp
     */
	public String getEdDraftaccptrtp() {
		return this.edDraftaccptrtp;
	}
	
	/**
	 * @param edDraftstuff
	 */
	public void setEdDraftstuff(String edDraftstuff) {
		this.edDraftstuff = edDraftstuff;
	}
	
    /**
     * @return edDraftstuff
     */
	public String getEdDraftstuff() {
		return this.edDraftstuff;
	}
	
	/**
	 * @param edDrafttp
	 */
	public void setEdDrafttp(String edDrafttp) {
		this.edDrafttp = edDrafttp;
	}
	
    /**
     * @return edDrafttp
     */
	public String getEdDrafttp() {
		return this.edDrafttp;
	}
	
	/**
	 * @param edDraftuseauthrty
	 */
	public void setEdDraftuseauthrty(String edDraftuseauthrty) {
		this.edDraftuseauthrty = edDraftuseauthrty;
	}
	
    /**
     * @return edDraftuseauthrty
     */
	public String getEdDraftuseauthrty() {
		return this.edDraftuseauthrty;
	}
	
	/**
	 * @param edDraftownersts
	 */
	public void setEdDraftownersts(String edDraftownersts) {
		this.edDraftownersts = edDraftownersts;
	}
	
    /**
     * @return edDraftownersts
     */
	public String getEdDraftownersts() {
		return this.edDraftownersts;
	}
	
	/**
	 * @param edCurinnerdraftsts
	 */
	public void setEdCurinnerdraftsts(String edCurinnerdraftsts) {
		this.edCurinnerdraftsts = edCurinnerdraftsts;
	}
	
    /**
     * @return edCurinnerdraftsts
     */
	public String getEdCurinnerdraftsts() {
		return this.edCurinnerdraftsts;
	}
	
	/**
	 * @param edCurtxltp
	 */
	public void setEdCurtxltp(String edCurtxltp) {
		this.edCurtxltp = edCurtxltp;
	}
	
    /**
     * @return edCurtxltp
     */
	public String getEdCurtxltp() {
		return this.edCurtxltp;
	}
	
	/**
	 * @param edCurtxltpno
	 */
	public void setEdCurtxltpno(String edCurtxltpno) {
		this.edCurtxltpno = edCurtxltpno;
	}
	
    /**
     * @return edCurtxltpno
     */
	public String getEdCurtxltpno() {
		return this.edCurtxltpno;
	}
	
	/**
	 * @param edDraftownersvcr
	 */
	public void setEdDraftownersvcr(String edDraftownersvcr) {
		this.edDraftownersvcr = edDraftownersvcr;
	}
	
    /**
     * @return edDraftownersvcr
     */
	public String getEdDraftownersvcr() {
		return this.edDraftownersvcr;
	}
	
	/**
	 * @param edDraftowneracctid
	 */
	public void setEdDraftowneracctid(String edDraftowneracctid) {
		this.edDraftowneracctid = edDraftowneracctid;
	}
	
    /**
     * @return edDraftowneracctid
     */
	public String getEdDraftowneracctid() {
		return this.edDraftowneracctid;
	}
	
	/**
	 * @param edDraftownercmonid
	 */
	public void setEdDraftownercmonid(String edDraftownercmonid) {
		this.edDraftownercmonid = edDraftownercmonid;
	}
	
    /**
     * @return edDraftownercmonid
     */
	public String getEdDraftownercmonid() {
		return this.edDraftownercmonid;
	}
	
	/**
	 * @param edDraftowninnerorgid
	 */
	public void setEdDraftowninnerorgid(String edDraftowninnerorgid) {
		this.edDraftowninnerorgid = edDraftowninnerorgid;
	}
	
    /**
     * @return edDraftowninnerorgid
     */
	public String getEdDraftowninnerorgid() {
		return this.edDraftowninnerorgid;
	}
	
	/**
	 * @param edDrwrinnerentpueid
	 */
	public void setEdDrwrinnerentpueid(String edDrwrinnerentpueid) {
		this.edDrwrinnerentpueid = edDrwrinnerentpueid;
	}
	
    /**
     * @return edDrwrinnerentpueid
     */
	public String getEdDrwrinnerentpueid() {
		return this.edDrwrinnerentpueid;
	}
	
	/**
	 * @param edDraftowncustomerid
	 */
	public void setEdDraftowncustomerid(String edDraftowncustomerid) {
		this.edDraftowncustomerid = edDraftowncustomerid;
	}
	
    /**
     * @return edDraftowncustomerid
     */
	public String getEdDraftowncustomerid() {
		return this.edDraftowncustomerid;
	}
	
	/**
	 * @param edDraftownername
	 */
	public void setEdDraftownername(String edDraftownername) {
		this.edDraftownername = edDraftownername;
	}
	
    /**
     * @return edDraftownername
     */
	public String getEdDraftownername() {
		return this.edDraftownername;
	}
	
	/**
	 * @param edAccptrcdtratgs
	 */
	public void setEdAccptrcdtratgs(String edAccptrcdtratgs) {
		this.edAccptrcdtratgs = edAccptrcdtratgs;
	}
	
    /**
     * @return edAccptrcdtratgs
     */
	public String getEdAccptrcdtratgs() {
		return this.edAccptrcdtratgs;
	}
	
	/**
	 * @param edAccptrcdtratgagcy
	 */
	public void setEdAccptrcdtratgagcy(String edAccptrcdtratgagcy) {
		this.edAccptrcdtratgagcy = edAccptrcdtratgagcy;
	}
	
    /**
     * @return edAccptrcdtratgagcy
     */
	public String getEdAccptrcdtratgagcy() {
		return this.edAccptrcdtratgagcy;
	}
	
	/**
	 * @param edAccptrcdtratgduedt
	 */
	public void setEdAccptrcdtratgduedt(String edAccptrcdtratgduedt) {
		this.edAccptrcdtratgduedt = edAccptrcdtratgduedt;
	}
	
    /**
     * @return edAccptrcdtratgduedt
     */
	public String getEdAccptrcdtratgduedt() {
		return this.edAccptrcdtratgduedt;
	}
	
	/**
	 * @param edAccptrsgnupdt
	 */
	public void setEdAccptrsgnupdt(String edAccptrsgnupdt) {
		this.edAccptrsgnupdt = edAccptrsgnupdt;
	}
	
    /**
     * @return edAccptrsgnupdt
     */
	public String getEdAccptrsgnupdt() {
		return this.edAccptrsgnupdt;
	}
	
	/**
	 * @param productId
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
    /**
     * @return productId
     */
	public String getProductId() {
		return this.productId;
	}
	
	/**
	 * @param sOwnerBillName
	 */
	public void setSOwnerBillName(String sOwnerBillName) {
		this.sOwnerBillName = sOwnerBillName;
	}
	
    /**
     * @return sOwnerBillName
     */
	public String getSOwnerBillName() {
		return this.sOwnerBillName;
	}
	
	/**
	 * @param dCreateDt
	 */
	public void setDCreateDt(String dCreateDt) {
		this.dCreateDt = dCreateDt;
	}
	
    /**
     * @return dCreateDt
     */
	public String getDCreateDt() {
		return this.dCreateDt;
	}
	
	/**
	 * @param sRemark
	 */
	public void setSRemark(String sRemark) {
		this.sRemark = sRemark;
	}
	
    /**
     * @return sRemark
     */
	public String getSRemark() {
		return this.sRemark;
	}
	
	/**
	 * @param sUsage
	 */
	public void setSUsage(String sUsage) {
		this.sUsage = sUsage;
	}
	
    /**
     * @return sUsage
     */
	public String getSUsage() {
		return this.sUsage;
	}
	
	/**
	 * @param sPayeeBankName
	 */
	public void setSPayeeBankName(String sPayeeBankName) {
		this.sPayeeBankName = sPayeeBankName;
	}
	
    /**
     * @return sPayeeBankName
     */
	public String getSPayeeBankName() {
		return this.sPayeeBankName;
	}
	
	/**
	 * @param sAcceptorBankName
	 */
	public void setSAcceptorBankName(String sAcceptorBankName) {
		this.sAcceptorBankName = sAcceptorBankName;
	}
	
    /**
     * @return sAcceptorBankName
     */
	public String getSAcceptorBankName() {
		return this.sAcceptorBankName;
	}
	
	/**
	 * @param sIssuerBankName
	 */
	public void setSIssuerBankName(String sIssuerBankName) {
		this.sIssuerBankName = sIssuerBankName;
	}
	
    /**
     * @return sIssuerBankName
     */
	public String getSIssuerBankName() {
		return this.sIssuerBankName;
	}
	
	/**
	 * @param sIfDirectAccep
	 */
	public void setSIfDirectAccep(String sIfDirectAccep) {
		this.sIfDirectAccep = sIfDirectAccep;
	}
	
    /**
     * @return sIfDirectAccep
     */
	public String getSIfDirectAccep() {
		return this.sIfDirectAccep;
	}
	
	/**
	 * @param sStroverSign
	 */
	public void setSStroverSign(String sStroverSign) {
		this.sStroverSign = sStroverSign;
	}
	
    /**
     * @return sStroverSign
     */
	public String getSStroverSign() {
		return this.sStroverSign;
	}
	
	/**
	 * @param sDealStatus
	 */
	public void setSDealStatus(String sDealStatus) {
		this.sDealStatus = sDealStatus;
	}
	
    /**
     * @return sDealStatus
     */
	public String getSDealStatus() {
		return this.sDealStatus;
	}
	
	/**
	 * @param clearingFlag
	 */
	public void setClearingFlag(String clearingFlag) {
		this.clearingFlag = clearingFlag;
	}
	
    /**
     * @return clearingFlag
     */
	public String getClearingFlag() {
		return this.clearingFlag;
	}
	
	/**
	 * @param edContractnb
	 */
	public void setEdContractnb(String edContractnb) {
		this.edContractnb = edContractnb;
	}
	
    /**
     * @return edContractnb
     */
	public String getEdContractnb() {
		return this.edContractnb;
	}
	
	/**
	 * @param edRmrk
	 */
	public void setEdRmrk(String edRmrk) {
		this.edRmrk = edRmrk;
	}
	
    /**
     * @return edRmrk
     */
	public String getEdRmrk() {
		return this.edRmrk;
	}
	
	/**
	 * @param edOverflag
	 */
	public void setEdOverflag(String edOverflag) {
		this.edOverflag = edOverflag;
	}
	
    /**
     * @return edOverflag
     */
	public String getEdOverflag() {
		return this.edOverflag;
	}
	
	/**
	 * @param sDraftownersvcrName
	 */
	public void setSDraftownersvcrName(String sDraftownersvcrName) {
		this.sDraftownersvcrName = sDraftownersvcrName;
	}
	
    /**
     * @return sDraftownersvcrName
     */
	public String getSDraftownersvcrName() {
		return this.sDraftownersvcrName;
	}
	
	/**
	 * @param edAcceptorProto
	 */
	public void setEdAcceptorProto(String edAcceptorProto) {
		this.edAcceptorProto = edAcceptorProto;
	}
	
    /**
     * @return edAcceptorProto
     */
	public String getEdAcceptorProto() {
		return this.edAcceptorProto;
	}
	
	/**
	 * @param edAcceptorDt
	 */
	public void setEdAcceptorDt(String edAcceptorDt) {
		this.edAcceptorDt = edAcceptorDt;
	}
	
    /**
     * @return edAcceptorDt
     */
	public String getEdAcceptorDt() {
		return this.edAcceptorDt;
	}
	
	/**
	 * @param edAcceptorAddress
	 */
	public void setEdAcceptorAddress(String edAcceptorAddress) {
		this.edAcceptorAddress = edAcceptorAddress;
	}
	
    /**
     * @return edAcceptorAddress
     */
	public String getEdAcceptorAddress() {
		return this.edAcceptorAddress;
	}
	
	/**
	 * @param sSenderMsgStatus
	 */
	public void setSSenderMsgStatus(String sSenderMsgStatus) {
		this.sSenderMsgStatus = sSenderMsgStatus;
	}
	
    /**
     * @return sSenderMsgStatus
     */
	public String getSSenderMsgStatus() {
		return this.sSenderMsgStatus;
	}
	
	/**
	 * @param sReceiverMsgStatus
	 */
	public void setSReceiverMsgStatus(String sReceiverMsgStatus) {
		this.sReceiverMsgStatus = sReceiverMsgStatus;
	}
	
    /**
     * @return sReceiverMsgStatus
     */
	public String getSReceiverMsgStatus() {
		return this.sReceiverMsgStatus;
	}
	
	/**
	 * @param sIfBidirect
	 */
	public void setSIfBidirect(String sIfBidirect) {
		this.sIfBidirect = sIfBidirect;
	}
	
    /**
     * @return sIfBidirect
     */
	public String getSIfBidirect() {
		return this.sIfBidirect;
	}
	
	/**
	 * @param billIfExist
	 */
	public void setBillIfExist(String billIfExist) {
		this.billIfExist = billIfExist;
	}
	
    /**
     * @return billIfExist
     */
	public String getBillIfExist() {
		return this.billIfExist;
	}
	
	/**
	 * @param batchNo
	 */
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	
    /**
     * @return batchNo
     */
	public String getBatchNo() {
		return this.batchNo;
	}
	
	/**
	 * @param billId
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}
	
    /**
     * @return billId
     */
	public String getBillId() {
		return this.billId;
	}
	
	/**
	 * @param sCorebatchnumber
	 */
	public void setSCorebatchnumber(String sCorebatchnumber) {
		this.sCorebatchnumber = sCorebatchnumber;
	}
	
    /**
     * @return sCorebatchnumber
     */
	public String getSCorebatchnumber() {
		return this.sCorebatchnumber;
	}
	
	/**
	 * @param bankJudge
	 */
	public void setBankJudge(String bankJudge) {
		this.bankJudge = bankJudge;
	}
	
    /**
     * @return bankJudge
     */
	public String getBankJudge() {
		return this.bankJudge;
	}
	
	/**
	 * @param tsZancun
	 */
	public void setTsZancun(String tsZancun) {
		this.tsZancun = tsZancun;
	}
	
    /**
     * @return tsZancun
     */
	public String getTsZancun() {
		return this.tsZancun;
	}
	
	/**
	 * @param discbillid
	 */
	public void setDiscbillid(String discbillid) {
		this.discbillid = discbillid;
	}
	
    /**
     * @return discbillid
     */
	public String getDiscbillid() {
		return this.discbillid;
	}
	
	/**
	 * @param sBatchNo
	 */
	public void setSBatchNo(String sBatchNo) {
		this.sBatchNo = sBatchNo;
	}
	
    /**
     * @return sBatchNo
     */
	public String getSBatchNo() {
		return this.sBatchNo;
	}
	
	/**
	 * @param instorageSts
	 */
	public void setInstorageSts(String instorageSts) {
		this.instorageSts = instorageSts;
	}
	
    /**
     * @return instorageSts
     */
	public String getInstorageSts() {
		return this.instorageSts;
	}
	
	/**
	 * @param edBuydt
	 */
	public void setEdBuydt(String edBuydt) {
		this.edBuydt = edBuydt;
	}
	
    /**
     * @return edBuydt
     */
	public String getEdBuydt() {
		return this.edBuydt;
	}
	
	/**
	 * @param edAccount129num
	 */
	public void setEdAccount129num(String edAccount129num) {
		this.edAccount129num = edAccount129num;
	}
	
    /**
     * @return edAccount129num
     */
	public String getEdAccount129num() {
		return this.edAccount129num;
	}
	
	/**
	 * @param sPcdsstatus
	 */
	public void setSPcdsstatus(String sPcdsstatus) {
		this.sPcdsstatus = sPcdsstatus;
	}
	
    /**
     * @return sPcdsstatus
     */
	public String getSPcdsstatus() {
		return this.sPcdsstatus;
	}
	
	/**
	 * @param hasImage
	 */
	public void setHasImage(String hasImage) {
		this.hasImage = hasImage;
	}
	
    /**
     * @return hasImage
     */
	public String getHasImage() {
		return this.hasImage;
	}
	
	/**
	 * @param accpBankSort
	 */
	public void setAccpBankSort(String accpBankSort) {
		this.accpBankSort = accpBankSort;
	}
	
    /**
     * @return accpBankSort
     */
	public String getAccpBankSort() {
		return this.accpBankSort;
	}
	
	/**
	 * @param proprietorship
	 */
	public void setProprietorship(String proprietorship) {
		this.proprietorship = proprietorship;
	}
	
    /**
     * @return proprietorship
     */
	public String getProprietorship() {
		return this.proprietorship;
	}
	
	/**
	 * @param billbuymode
	 */
	public void setBillbuymode(String billbuymode) {
		this.billbuymode = billbuymode;
	}
	
    /**
     * @return billbuymode
     */
	public String getBillbuymode() {
		return this.billbuymode;
	}
	
	/**
	 * @param billbuycostrate
	 */
	public void setBillbuycostrate(java.math.BigDecimal billbuycostrate) {
		this.billbuycostrate = billbuycostrate;
	}
	
    /**
     * @return billbuycostrate
     */
	public java.math.BigDecimal getBillbuycostrate() {
		return this.billbuycostrate;
	}
	
	/**
	 * @param billpossessdate
	 */
	public void setBillpossessdate(String billpossessdate) {
		this.billpossessdate = billpossessdate;
	}
	
    /**
     * @return billpossessdate
     */
	public String getBillpossessdate() {
		return this.billpossessdate;
	}
	
	/**
	 * @param buycostratetype
	 */
	public void setBuycostratetype(String buycostratetype) {
		this.buycostratetype = buycostratetype;
	}
	
    /**
     * @return buycostratetype
     */
	public String getBuycostratetype() {
		return this.buycostratetype;
	}
	
	/**
	 * @param sNegotiability
	 */
	public void setSNegotiability(String sNegotiability) {
		this.sNegotiability = sNegotiability;
	}
	
    /**
     * @return sNegotiability
     */
	public String getSNegotiability() {
		return this.sNegotiability;
	}
	
	/**
	 * @param sNegotiabilityreason
	 */
	public void setSNegotiabilityreason(String sNegotiabilityreason) {
		this.sNegotiabilityreason = sNegotiabilityreason;
	}
	
    /**
     * @return sNegotiabilityreason
     */
	public String getSNegotiabilityreason() {
		return this.sNegotiabilityreason;
	}
	
	/**
	 * @param sNegotiabilityrmk
	 */
	public void setSNegotiabilityrmk(String sNegotiabilityrmk) {
		this.sNegotiabilityrmk = sNegotiabilityrmk;
	}
	
    /**
     * @return sNegotiabilityrmk
     */
	public String getSNegotiabilityrmk() {
		return this.sNegotiabilityrmk;
	}
	
	/**
	 * @param sDraftstoreplace
	 */
	public void setSDraftstoreplace(String sDraftstoreplace) {
		this.sDraftstoreplace = sDraftstoreplace;
	}
	
    /**
     * @return sDraftstoreplace
     */
	public String getSDraftstoreplace() {
		return this.sDraftstoreplace;
	}
	
	/**
	 * @param sDraftstoreplacename
	 */
	public void setSDraftstoreplacename(String sDraftstoreplacename) {
		this.sDraftstoreplacename = sDraftstoreplacename;
	}
	
    /**
     * @return sDraftstoreplacename
     */
	public String getSDraftstoreplacename() {
		return this.sDraftstoreplacename;
	}
	
	/**
	 * @param sDraftstoreplacebankcode
	 */
	public void setSDraftstoreplacebankcode(String sDraftstoreplacebankcode) {
		this.sDraftstoreplacebankcode = sDraftstoreplacebankcode;
	}
	
    /**
     * @return sDraftstoreplacebankcode
     */
	public String getSDraftstoreplacebankcode() {
		return this.sDraftstoreplacebankcode;
	}
	
	/**
	 * @param sMrjg
	 */
	public void setSMrjg(String sMrjg) {
		this.sMrjg = sMrjg;
	}
	
    /**
     * @return sMrjg
     */
	public String getSMrjg() {
		return this.sMrjg;
	}
	
	/**
	 * @param sCdjg
	 */
	public void setSCdjg(String sCdjg) {
		this.sCdjg = sCdjg;
	}
	
    /**
     * @return sCdjg
     */
	public String getSCdjg() {
		return this.sCdjg;
	}
	
	/**
	 * @param draftstorageId
	 */
	public void setDraftstorageId(String draftstorageId) {
		this.draftstorageId = draftstorageId;
	}
	
    /**
     * @return draftstorageId
     */
	public String getDraftstorageId() {
		return this.draftstorageId;
	}
	
	/**
	 * @param draftpoolId
	 */
	public void setDraftpoolId(String draftpoolId) {
		this.draftpoolId = draftpoolId;
	}
	
    /**
     * @return draftpoolId
     */
	public String getDraftpoolId() {
		return this.draftpoolId;
	}
	
	/**
	 * @param sIfRiskBill
	 */
	public void setSIfRiskBill(String sIfRiskBill) {
		this.sIfRiskBill = sIfRiskBill;
	}
	
    /**
     * @return sIfRiskBill
     */
	public String getSIfRiskBill() {
		return this.sIfRiskBill;
	}
	
	/**
	 * @param sIfPoolflag
	 */
	public void setSIfPoolflag(String sIfPoolflag) {
		this.sIfPoolflag = sIfPoolflag;
	}
	
    /**
     * @return sIfPoolflag
     */
	public String getSIfPoolflag() {
		return this.sIfPoolflag;
	}
	
	/**
	 * @param edDraftownerbankname
	 */
	public void setEdDraftownerbankname(String edDraftownerbankname) {
		this.edDraftownerbankname = edDraftownerbankname;
	}
	
    /**
     * @return edDraftownerbankname
     */
	public String getEdDraftownerbankname() {
		return this.edDraftownerbankname;
	}
	
	/**
	 * @param edDraftownerbankcode
	 */
	public void setEdDraftownerbankcode(String edDraftownerbankcode) {
		this.edDraftownerbankcode = edDraftownerbankcode;
	}
	
    /**
     * @return edDraftownerbankcode
     */
	public String getEdDraftownerbankcode() {
		return this.edDraftownerbankcode;
	}
	
	/**
	 * @param stockOutType
	 */
	public void setStockOutType(String stockOutType) {
		this.stockOutType = stockOutType;
	}
	
    /**
     * @return stockOutType
     */
	public String getStockOutType() {
		return this.stockOutType;
	}
	
	/**
	 * @param edBatchNo
	 */
	public void setEdBatchNo(String edBatchNo) {
		this.edBatchNo = edBatchNo;
	}
	
    /**
     * @return edBatchNo
     */
	public String getEdBatchNo() {
		return this.edBatchNo;
	}
	
	/**
	 * @param jjh
	 */
	public void setJjh(String jjh) {
		this.jjh = jjh;
	}
	
    /**
     * @return jjh
     */
	public String getJjh() {
		return this.jjh;
	}
	
	/**
	 * @param tsStoreouttype
	 */
	public void setTsStoreouttype(String tsStoreouttype) {
		this.tsStoreouttype = tsStoreouttype;
	}
	
    /**
     * @return tsStoreouttype
     */
	public String getTsStoreouttype() {
		return this.tsStoreouttype;
	}
	
	/**
	 * @param edOutstockBatchNo
	 */
	public void setEdOutstockBatchNo(String edOutstockBatchNo) {
		this.edOutstockBatchNo = edOutstockBatchNo;
	}
	
    /**
     * @return edOutstockBatchNo
     */
	public String getEdOutstockBatchNo() {
		return this.edOutstockBatchNo;
	}
	
	/**
	 * @param cdEdraftbatchid
	 */
	public void setCdEdraftbatchid(String cdEdraftbatchid) {
		this.cdEdraftbatchid = cdEdraftbatchid;
	}
	
    /**
     * @return cdEdraftbatchid
     */
	public String getCdEdraftbatchid() {
		return this.cdEdraftbatchid;
	}
	
	/**
	 * @param tsBackrmk
	 */
	public void setTsBackrmk(String tsBackrmk) {
		this.tsBackrmk = tsBackrmk;
	}
	
    /**
     * @return tsBackrmk
     */
	public String getTsBackrmk() {
		return this.tsBackrmk;
	}
	
	/**
	 * @param edStockorgid
	 */
	public void setEdStockorgid(String edStockorgid) {
		this.edStockorgid = edStockorgid;
	}
	
    /**
     * @return edStockorgid
     */
	public String getEdStockorgid() {
		return this.edStockorgid;
	}
	
	/**
	 * @param sourceId
	 */
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	
    /**
     * @return sourceId
     */
	public String getSourceId() {
		return this.sourceId;
	}
	
	/**
	 * @param lastSourceId
	 */
	public void setLastSourceId(String lastSourceId) {
		this.lastSourceId = lastSourceId;
	}
	
    /**
     * @return lastSourceId
     */
	public String getLastSourceId() {
		return this.lastSourceId;
	}
	
	/**
	 * @param balanceFlag
	 */
	public void setBalanceFlag(String balanceFlag) {
		this.balanceFlag = balanceFlag;
	}
	
    /**
     * @return balanceFlag
     */
	public String getBalanceFlag() {
		return this.balanceFlag;
	}
	
	/**
	 * @param balanceBrchId
	 */
	public void setBalanceBrchId(String balanceBrchId) {
		this.balanceBrchId = balanceBrchId;
	}
	
    /**
     * @return balanceBrchId
     */
	public String getBalanceBrchId() {
		return this.balanceBrchId;
	}
	
	/**
	 * @param sourceBuyMode
	 */
	public void setSourceBuyMode(String sourceBuyMode) {
		this.sourceBuyMode = sourceBuyMode;
	}
	
    /**
     * @return sourceBuyMode
     */
	public String getSourceBuyMode() {
		return this.sourceBuyMode;
	}
	
	/**
	 * @param oldProductId
	 */
	public void setOldProductId(String oldProductId) {
		this.oldProductId = oldProductId;
	}
	
    /**
     * @return oldProductId
     */
	public String getOldProductId() {
		return this.oldProductId;
	}
	
	/**
	 * @param buyinbatchno
	 */
	public void setBuyinbatchno(String buyinbatchno) {
		this.buyinbatchno = buyinbatchno;
	}
	
    /**
     * @return buyinbatchno
     */
	public String getBuyinbatchno() {
		return this.buyinbatchno;
	}
	
	/**
	 * @param saleId
	 */
	public void setSaleId(String saleId) {
		this.saleId = saleId;
	}
	
    /**
     * @return saleId
     */
	public String getSaleId() {
		return this.saleId;
	}
	
	/**
	 * @param tradebranchid
	 */
	public void setTradebranchid(String tradebranchid) {
		this.tradebranchid = tradebranchid;
	}
	
    /**
     * @return tradebranchid
     */
	public String getTradebranchid() {
		return this.tradebranchid;
	}
	
	/**
	 * @param overnightFlag
	 */
	public void setOvernightFlag(String overnightFlag) {
		this.overnightFlag = overnightFlag;
	}
	
    /**
     * @return overnightFlag
     */
	public String getOvernightFlag() {
		return this.overnightFlag;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}