package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0302Mapper;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0302</br>
 * 任务名称：加工任务-客户处理-名单生成  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0302Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0302Service.class);

    @Autowired
    private Cmis0302Mapper cmis0302Mapper;

    @Autowired
    private BatControlRunService batControlRunService;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 插入问题授权名单
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0302InsertCusLstWtsx(String openDay) {
        // 信贷系统在每月末日终将当月新增账面不良（后三类）、新增逾期或欠息 90 天以上（含）的客户名单批量导入登记簿
        QueryModel queryModel = new QueryModel();
        List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
        String lastOpenDay = batControlRunList.get(0).getLastOpenDay();//营业日期
//        logger.info("重命名创建和删除临时表-贷款台账表临时表(contNo)开始");
//        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_acc_loan_contno_cmis0302");
//        logger.info("重命名创建和删除临时表-贷款台账表临时表(contNo)结束");
//        logger.info("插入贷款台账表临时表(contNo)开始,请求参数为:[{}]", openDay);
//        int insertTmpAccLoanCont1 = cmis0302Mapper.insertTmpAccLoanCont1(openDay);
//        logger.info("插入对贷款台账表临时表(contNo)结束,返回参数为:[{}]", insertTmpAccLoanCont1);
//        tableUtilsService.analyzeTable("cmis_biz","tmp_acc_loan_contno_cmis0302");//分析表

        logger.info("重命名创建和删除临时表-贷款台账表临时表(cusId)开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_acc_loan_cusid_cmis0302");
        logger.info("重命名创建和删除临时表-贷款台账表临时表(cusId)结束");
        logger.info("插入贷款台账表临时表(cusId)开始,请求参数为:[{}]", openDay);
        int insertTmpAccLoanCont2 = cmis0302Mapper.insertTmpAccLoanCont2(openDay);
        logger.info("插入对贷款台账表临时表(cusId)结束,返回参数为:[{}]", insertTmpAccLoanCont2);
        tableUtilsService.analyzeTable("cmis_biz","tmp_acc_loan_cusid_cmis0302");//分析表

//        logger.info("重命名创建和删除临时表-个人社会关系信息开始");
//        tableUtilsService.renameCreateDropTable("cmis_cus","tmp_cus_indiv_social_cusidrel");
//        logger.info("重命名创建和删除临时表-个人社会关系信息结束");
//        logger.info("插入临时表-个人社会关系信息开始,请求参数为:[{}]", openDay);
//        int insertTmpCusindivCocial = cmis0302Mapper.insertTmpCusindivSocial(openDay);
//        logger.info("插入临时表-个人社会关系信息结束,返回参数为:[{}]", insertTmpCusindivCocial);
//        tableUtilsService.analyzeTable("cmis_cus","tmp_cus_indiv_social_cusidrel");//分析表
//
//        logger.info("重命名创建和删除临时表-批复额度分项基础信息开始");
//        tableUtilsService.renameCreateDropTable("cmis_lmt","TMP_S_APPR_LMT_SUB_BASIC_INFO");
//        logger.info("重命名创建和删除临时表-批复额度分项基础信息结束");
//        logger.info("临时表插入批复额度分项基础信息开始,请求参数为:[{}]", openDay);
//        int insertTmpSApprLmtSub = cmis0302Mapper.insertTmpSApprLmtSub(openDay);
//        logger.info("临时表插入批复额度分项基础信息结束,返回参数为:[{}]", insertTmpSApprLmtSub);
//        tableUtilsService.analyzeTable("cmis_lmt","TMP_S_APPR_LMT_SUB_BASIC_INFO");//分析表
//
//        logger.info("重命名创建和删除临时表_插入无还本续贷名单信息客户号exists开始");
//        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_insertCusLstWhbxd_cusid_1");
//        logger.info("重命名创建和删除临时表_插入无还本续贷名单信息客户号exists结束");
//        logger.info("插入临时表_插入无还本续贷名单信息客户号exists开始,请求参数为:[{}]", openDay);
//        List<String> prdList = cmis0302Mapper.selectPrdIdList();
//        int insertTmpInsertCusLstWhbxd1 = cmis0302Mapper.insertTmpInsertCusLstWhbxd1(prdList);
//        logger.info("插入临时表_插入无还本续贷名单信息客户号exists结束,返回参数为:[{}]", insertTmpInsertCusLstWhbxd1);
//        tableUtilsService.analyzeTable("cmis_biz","tmp_insertCusLstWhbxd_cusid_1");//分析表

        logger.info("重命名创建和删除临时表_插入无还本续贷名单信息客户号notexists开始");
        tableUtilsService.renameCreateDropTable("cmis_biz","tmp_insertCusLstWhbxd_cusid_2");
        logger.info("重命名创建和删除临时表_插入无还本续贷名单信息客户号notexists结束");
        logger.info("插入临时表_插入无还本续贷名单信息客户号notexists开始,请求参数为:[{}]", openDay);
        int insertTmpInsertCusLstWhbxd2 = cmis0302Mapper.insertTmpInsertCusLstWhbxd2(openDay);
        logger.info("插入临时表_插入无还本续贷名单信息客户号notexists结束,返回参数为:[{}]", insertTmpInsertCusLstWhbxd2);
        tableUtilsService.analyzeTable("cmis_biz","tmp_insertCusLstWhbxd_cusid_2");//分析表

        logger.info("插入对公客户问题授信名单开始,请求参数为:[{}]", openDay);
        int insertCusLstWtsx01 = cmis0302Mapper.insertCusLstWtsx01(lastOpenDay);
        logger.info("插入对公客户问题授信名单结束,返回参数为:[{}]", insertCusLstWtsx01);
        logger.info("插入个人客户问题授信名单开始,请求参数为:[{}]", openDay);
        int insertCusLstWtsx02 = cmis0302Mapper.insertCusLstWtsx02(lastOpenDay);
        logger.info("插入个人客户问题授信名单结束,返回参数为:[{}]", insertCusLstWtsx02);
        logger.info("更新客户问题授信名单导入原因开始,请求参数为:[{}]", openDay);
        int updateCusLstWtsx = cmis0302Mapper.updateCusLstWtsx(lastOpenDay);
        logger.info("更新客户问题授信名单导入原因结束,返回参数为:[{}]", updateCusLstWtsx);
    }

    /**
     * 插入小微无还本续贷名单信息
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0302InsertCusLstWhbxd(String openDay) {
        QueryModel queryModel = new QueryModel();
        List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
        String lastOpenDay = batControlRunList.get(0).getLastOpenDay();//营业日期
        logger.info("插入无还本续贷名单信息开始,请求参数为:[{}]", openDay);
        int insertCusLstWhbxd = cmis0302Mapper.insertCusLstWhbxd(lastOpenDay);
        logger.info("插入无还本续贷名单信息结束,返回参数为:[{}]", insertCusLstWhbxd);


        logger.info("期供表中 还款日超出到期日5天  大于0 不合格  去掉名单 开始,请求参数为:[{}]", openDay);
        int deleteCusLstMclWhbxd1 = cmis0302Mapper.deleteCusLstMclWhbxd1(lastOpenDay);
        logger.info("期供表中 还款日超出到期日5天  大于0 不合格  去掉名单 结束,返回参数为:[{}]", deleteCusLstMclWhbxd1);


        logger.info("期供表中 还款日比到期日大并且 在1 到4天之间 的次数大于3不合格    去掉名单开始,请求参数为:[{}]", openDay);
        int deleteCusLstMclWhbxd2 = cmis0302Mapper.deleteCusLstMclWhbxd2(lastOpenDay);
        logger.info("期供表中 还款日比到期日大并且 在1 到4天之间 的次数大于3不合格    去掉名单 结束,返回参数为:[{}]", deleteCusLstMclWhbxd2);

        logger.info("更新无还本续贷名单信息开始,请求参数为:[{}]", openDay);
        int uptCusLstWhbxd = cmis0302Mapper.uptCusLstWhbxd(openDay);
        logger.info("更新无还本续贷名单信息结束,返回参数为:[{}]", insertCusLstWhbxd);
    }
}
