/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.repository.mapper.check;

import org.apache.ibatis.annotations.Param;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: LmtCheckInsertLmtContRelMapper
 * @类描述: #Dao类
 * @功能描述: 数据插入额度校正-合同临时表
 * @创建人: ZRC
 * @创建时间: 2021-09-30 16:22:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCheckInsertLmtContRelMapper {

    /**
     * 3.1、贷款合同
     */
    int insertLmtLoanRelDkht(@Param("cusId") String cusId);

    /**
     * 3.2、贷款合同申请
     */
    int insertLmtLoanRelDkhtsq(@Param("cusId") String cusId);

    /**
     * 3.3、委托贷款合同
     */
    int insertLmtLoanRelWtdkht(@Param("cusId") String cusId);

    /**
     * 3.4、委托贷款合同申请
     */
    int insertLmtLoanRelWtdkhtsq(@Param("cusId") String cusId);

    /**
     * 3.5、最高额授信协议
     */
    int insertLmtLoanRelZgesxxy(@Param("cusId") String cusId);

    /**
     * 3.6、最高额授信协议申请
     */
    int insertLmtLoanRelZgesxxysq(@Param("cusId") String cusId);

    /**
     * 3.7、资产池协议
     */
    int insertLmtLoanRelZccxy(@Param("cusId") String cusId);

    /**
     * 3.8、资产池协议申请
     */
    int insertLmtLoanRelZccxysq(@Param("cusId") String cusId);

    /**
     * 3.9、银票合同
     */
    int insertLmtLoanRelYpht(@Param("cusId") String cusId);

    /**
     * 3.10、银票合同申请
     */
    int insertLmtLoanRelYphtsq(@Param("cusId") String cusId);

    /**
     * 3.11、保函合同
     */
    int insertLmtLoanRelBhht(@Param("cusId") String cusId);

    /**
     * 3.12、保函合同申请
     */
    int insertLmtLoanRelBhhtsq(@Param("cusId") String cusId);

    /**
     * 3.13、信用证合同
     */
    int insertLmtLoanRelXyzht(@Param("cusId") String cusId);

    /**
     * 3.14、信用证合同申请
     */
    int insertLmtLoanRelXyzhtsq(@Param("cusId") String cusId);

    /**
     * 3.15、贴现合同
     */
    int insertLmtLoanRelTxht(@Param("cusId") String cusId);

    /**
     * 3.16、贴现合同申请
     */
    int insertLmtLoanRelTxhtsq(@Param("cusId") String cusId);
}