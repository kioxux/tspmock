package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

import java.util.Map;


/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0418</br>
 * 任务名称：加工任务-贷后管理-定期检查[清理贷后临时表数据] </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月17日 下午9:56:54
 * TODO 同步代码
 */
public interface Cmis0418Mapper {

    /**
     * 插入贷后检查任务表
     * @param openDay
     * @return
     */
    int insertPspTaskList(@Param("openDay") String openDay);
    int insertTmpPspTask01(@Param("openDay") String openDay);
    int insertTmpPspTask02(@Param("openDay") String openDay);
    int insertTmpPspTask03(@Param("openDay") String openDay);

}
