package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0431Mapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;


/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0431</br>
 * 任务名称：加工任务-贷后管理-贷后临时表数据回插 </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月16日 上午0:28:14
 */
@Service
@Transactional
public class Cmis0431Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0431Service.class);

    @Resource
    private Cmis0431Mapper cmis0431Mapper;

    /**
     * 贷后 风险分类任务临时表 数据回插
     * @param openDay
     */
    public void insertRiskTaskList(String openDay) {
        logger.info("[风险分类任务临时表]回插入[风险分类任务表]开始,请求参数为:[{}]", openDay);
        int insertRiskTaskList = cmis0431Mapper.insertRiskTaskList(openDay);
        logger.info("[风险分类任务临时表]回插CMIS0426入风险分类[风险分类任务表]结束,返回参数为:[{}]", insertRiskTaskList);
    }

    /**
     * 贷后 风险分类借据信息临时表 数据回插
     * @param openDay
     */
    public void insertRiskDebitInfo(String openDay) {
        logger.info("[风险分类借据信息临时表]回插入[风险分类借据信息表]开始,请求参数为:[{}]", openDay);
        int insertRiskDebitInfo = cmis0431Mapper.insertRiskDebitInfo(openDay);
        logger.info("[风险分类借据信息临时表]回插入[风险分类借据信息表]结束,返回参数为:[{}]", insertRiskDebitInfo);
    }
}
