package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKD0005</br>
 * 任务名称：批前备份日表任务-备份委托贷款台账[ACC_ENTRUST_LOAN]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakD0005Mapper {

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertCurrent(@Param("openDay") String openDay);

    /**
     * 删除当天的数据
     *
     * @return
     */
    void truncateCurrent();

    /**
     * 删除当天的数据
     *
     * @param openDay
     * @return
     */
    int deleteCurrent(@Param("openDay") String openDay);

    /**
     * 查询待删除当天的[委托贷款台账[ACC_ENTRUST_LOAN]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryD0005DeleteOpenDayAccEntrustLoanCounts(@Param("openDay") String openDay);

    /**
     *
     *
     * 查询原表[委托贷款台账[ACC_ENTRUST_LOAN]]数据条数
     * @param openDay
     * @return
     */
    int queryAccEntrustLoan(@Param("openDay") String openDay);
}
