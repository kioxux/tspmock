/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.ypp;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSYppTGuarCertiRela
 * @类描述: bat_s_ypp_t_guar_certi_rela数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-01 20:11:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_ypp_t_guar_certi_rela")
public class BatSYppTGuarCertiRela extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/**
	 * 押品编号
	 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 60)
	private String guarNo;
	
	/** 权证名称 **/
	@Column(name = "CERTI_NAME", unique = false, nullable = true, length = 300)
	private String certiName;
	
	/** 权证类型 **/
	@Column(name = "CERTI_TYPE_CD", unique = false, nullable = true, length = 300)
	private String certiTypeCd;

	/**
	 * 权利凭证号
	 **/
	@Column(name = "CERTI_RECORD_ID", unique = false, nullable = true, length = 600)
	private String certiRecordId;

	/**
	 * 权证发证机关名称
	 **/
	@Column(name = "CERTI_ORG_NAME", unique = false, nullable = true, length = 120)
	private String certiOrgName;

	/**
	 * 权证发证日期
	 **/
	@Column(name = "CERTI_START_DATE", unique = false, nullable = true, length = 60)
	private String certiStartDate;

	/**
	 * 权证到期日期
	 **/
	@Column(name = "CERTI_END_DATE", unique = false, nullable = true, length = 60)
	private String certiEndDate;
	
	/** 权证入库日期 **/
	@Column(name = "IN_DATE", unique = false, nullable = true, length = 300)
	private String inDate;

	/**
	 * 权证正常出库日期
	 **/
	@Column(name = "OUT_DATE", unique = false, nullable = true, length = 60)
	private String outDate;

	/**
	 * 权证临时借用人名称
	 **/
	@Column(name = "TEMP_BORROWER_NAME", unique = false, nullable = true, length = 120)
	private String tempBorrowerName;

	/**
	 * 权证外借日期
	 **/
	@Column(name = "TEMP_OUT_DATE", unique = false, nullable = true, length = 60)
	private String tempOutDate;

	/**
	 * 权证实际归还日期
	 **/
	@Column(name = "REAL_BACK_DATE", unique = false, nullable = true, length = 60)
	private String realBackDate;

	/**
	 * 权证临时出库期限
	 **/
	@Column(name = "TEMP_OUT_TERM", unique = false, nullable = true, length = 60)
	private String tempOutTerm;

	/** 权证外借原因 **/
	@Column(name = "TEMP_OUT_REASON", unique = false, nullable = true, length = 600)
	private String tempOutReason;

	/** 权证续期原因 **/
	@Column(name = "EXT_REASON", unique = false, nullable = true, length = 600)
	private String extReason;

	/** 权证预计归还日期 **/
	@Column(name = "PRE_BACK_DATE", unique = false, nullable = true, length = 60)
	private String preBackDate;

	/** 权证表面信息是否发生重大变化 **/
	@Column(name = "CERTI_CHANGE_IND", unique = false, nullable = true, length = 1200)
	private String certiChangeInd;

	/**
	 * 权证表面信息重大变化情况描述
	 **/
	@Column(name = "CERTI_CHANGE_REASON", unique = false, nullable = true, length = 1200)
	private String certiChangeReason;

	/**
	 * 权证状态
	 **/
	@Column(name = "CERTI_STATE", unique = false, nullable = true, length = 40)
	private String certiState;

	/**
	 * 登记人
	 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 60)
	private String inputId;

	/**
	 * 登记机构
	 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 60)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 60)
	private String inputDate;

	/**
	 * 是否遗失/灭失
	 **/
	@Column(name = "DESTROY_IND", unique = false, nullable = true, length = 40)
	private String destroyInd;

	/**
	 * 权证备注信息
	 **/
	@Column(name = "CERTI_COMMENT", unique = false, nullable = true, length = 1200)
	private String certiComment;

	/**
	 * 权证类型其他文本说明
	 **/
	@Column(name = "CERTI_TYPE_OTHER", unique = false, nullable = true, length = 1200)
	private String certiTypeOther;
	
	/** 其他文本输入 **/
	@Column(name = "REASON_OTHER_TEXT", unique = false, nullable = true, length = 1200)
	private String reasonOtherText;
	
	/** 是否他项权证 **/
	@Column(name = "IS_OTHER_CERTI", unique = false, nullable = true, length = 10)
	private String isOtherCerti;
	
	/** 权利金额 **/
	@Column(name = "CERTI_AMT", unique = false, nullable = true, length = 40)
	private String certiAmt;

	/**
	 * 抵押顺位标识
	 **/
	@Column(name = "PLEDGE_IDENTITY", unique = false, nullable = true, length = 40)
	private String pledgeIdentity;
	
	/** 核心担保编号 **/
	@Column(name = "GUAR_CORE_NUM", unique = false, nullable = true, length = 60)
	private String guarCoreNum;

	/**
	 * 担保合同
	 **/
	@Column(name = "GUAR_CONT", unique = false, nullable = true, length = 10)
	private String guarCont;

	/**
	 * 债权起始日期
	 **/
	@Column(name = "DEBT_START_DATE", unique = false, nullable = true, length = 60)
	private String debtStartDate;
	
	/** 债权到期日期 **/
	@Column(name = "DEBT_END_DATE", unique = false, nullable = true, length = 60)
	private String debtEndDate;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param certiName
	 */
	public void setCertiName(String certiName) {
		this.certiName = certiName;
	}
	
    /**
     * @return certiName
     */
	public String getCertiName() {
		return this.certiName;
	}
	
	/**
	 * @param certiTypeCd
	 */
	public void setCertiTypeCd(String certiTypeCd) {
		this.certiTypeCd = certiTypeCd;
	}
	
    /**
     * @return certiTypeCd
     */
	public String getCertiTypeCd() {
		return this.certiTypeCd;
	}
	
	/**
	 * @param certiRecordId
	 */
	public void setCertiRecordId(String certiRecordId) {
		this.certiRecordId = certiRecordId;
	}
	
    /**
     * @return certiRecordId
     */
	public String getCertiRecordId() {
		return this.certiRecordId;
	}
	
	/**
	 * @param certiOrgName
	 */
	public void setCertiOrgName(String certiOrgName) {
		this.certiOrgName = certiOrgName;
	}
	
    /**
     * @return certiOrgName
     */
	public String getCertiOrgName() {
		return this.certiOrgName;
	}
	
	/**
	 * @param certiStartDate
	 */
	public void setCertiStartDate(String certiStartDate) {
		this.certiStartDate = certiStartDate;
	}
	
    /**
     * @return certiStartDate
     */
	public String getCertiStartDate() {
		return this.certiStartDate;
	}
	
	/**
	 * @param certiEndDate
	 */
	public void setCertiEndDate(String certiEndDate) {
		this.certiEndDate = certiEndDate;
	}
	
    /**
     * @return certiEndDate
     */
	public String getCertiEndDate() {
		return this.certiEndDate;
	}
	
	/**
	 * @param inDate
	 */
	public void setInDate(String inDate) {
		this.inDate = inDate;
	}
	
    /**
     * @return inDate
     */
	public String getInDate() {
		return this.inDate;
	}
	
	/**
	 * @param outDate
	 */
	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}
	
    /**
     * @return outDate
     */
	public String getOutDate() {
		return this.outDate;
	}
	
	/**
	 * @param tempBorrowerName
	 */
	public void setTempBorrowerName(String tempBorrowerName) {
		this.tempBorrowerName = tempBorrowerName;
	}
	
    /**
     * @return tempBorrowerName
     */
	public String getTempBorrowerName() {
		return this.tempBorrowerName;
	}
	
	/**
	 * @param tempOutDate
	 */
	public void setTempOutDate(String tempOutDate) {
		this.tempOutDate = tempOutDate;
	}
	
    /**
     * @return tempOutDate
     */
	public String getTempOutDate() {
		return this.tempOutDate;
	}
	
	/**
	 * @param realBackDate
	 */
	public void setRealBackDate(String realBackDate) {
		this.realBackDate = realBackDate;
	}
	
    /**
     * @return realBackDate
     */
	public String getRealBackDate() {
		return this.realBackDate;
	}
	
	/**
	 * @param tempOutTerm
	 */
	public void setTempOutTerm(String tempOutTerm) {
		this.tempOutTerm = tempOutTerm;
	}
	
    /**
     * @return tempOutTerm
     */
	public String getTempOutTerm() {
		return this.tempOutTerm;
	}
	
	/**
	 * @param tempOutReason
	 */
	public void setTempOutReason(String tempOutReason) {
		this.tempOutReason = tempOutReason;
	}
	
    /**
     * @return tempOutReason
     */
	public String getTempOutReason() {
		return this.tempOutReason;
	}
	
	/**
	 * @param extReason
	 */
	public void setExtReason(String extReason) {
		this.extReason = extReason;
	}
	
    /**
     * @return extReason
     */
	public String getExtReason() {
		return this.extReason;
	}
	
	/**
	 * @param preBackDate
	 */
	public void setPreBackDate(String preBackDate) {
		this.preBackDate = preBackDate;
	}
	
    /**
     * @return preBackDate
     */
	public String getPreBackDate() {
		return this.preBackDate;
	}
	
	/**
	 * @param certiChangeInd
	 */
	public void setCertiChangeInd(String certiChangeInd) {
		this.certiChangeInd = certiChangeInd;
	}
	
    /**
     * @return certiChangeInd
     */
	public String getCertiChangeInd() {
		return this.certiChangeInd;
	}
	
	/**
	 * @param certiChangeReason
	 */
	public void setCertiChangeReason(String certiChangeReason) {
		this.certiChangeReason = certiChangeReason;
	}
	
    /**
     * @return certiChangeReason
     */
	public String getCertiChangeReason() {
		return this.certiChangeReason;
	}
	
	/**
	 * @param certiState
	 */
	public void setCertiState(String certiState) {
		this.certiState = certiState;
	}
	
    /**
     * @return certiState
     */
	public String getCertiState() {
		return this.certiState;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param destroyInd
	 */
	public void setDestroyInd(String destroyInd) {
		this.destroyInd = destroyInd;
	}
	
    /**
     * @return destroyInd
     */
	public String getDestroyInd() {
		return this.destroyInd;
	}
	
	/**
	 * @param certiComment
	 */
	public void setCertiComment(String certiComment) {
		this.certiComment = certiComment;
	}

	/**
	 * @return certiComment
	 */
	public String getCertiComment() {
		return this.certiComment;
	}

	/**
	 * @param certiTypeOther
	 */
	public void setCertiTypeOther(String certiTypeOther) {
		this.certiTypeOther = certiTypeOther;
	}

	/**
	 * @return certiTypeOther
	 */
	public String getCertiTypeOther() {
		return this.certiTypeOther;
	}

	public String getReasonOtherText() {
		return reasonOtherText;
	}

	public void setReasonOtherText(String reasonOtherText) {
		this.reasonOtherText = reasonOtherText;
	}
	
	/**
	 * @param isOtherCerti
	 */
	public void setIsOtherCerti(String isOtherCerti) {
		this.isOtherCerti = isOtherCerti;
	}
	
    /**
     * @return isOtherCerti
     */
	public String getIsOtherCerti() {
		return this.isOtherCerti;
	}
	
	/**
	 * @param certiAmt
	 */
	public void setCertiAmt(String certiAmt) {
		this.certiAmt = certiAmt;
	}
	
    /**
     * @return certiAmt
     */
	public String getCertiAmt() {
		return this.certiAmt;
	}
	
	/**
	 * @param pledgeIdentity
	 */
	public void setPledgeIdentity(String pledgeIdentity) {
		this.pledgeIdentity = pledgeIdentity;
	}
	
    /**
     * @return pledgeIdentity
     */
	public String getPledgeIdentity() {
		return this.pledgeIdentity;
	}
	
	/**
	 * @param guarCoreNum
	 */
	public void setGuarCoreNum(String guarCoreNum) {
		this.guarCoreNum = guarCoreNum;
	}
	
    /**
     * @return guarCoreNum
     */
	public String getGuarCoreNum() {
		return this.guarCoreNum;
	}
	
	/**
	 * @param guarCont
	 */
	public void setGuarCont(String guarCont) {
		this.guarCont = guarCont;
	}
	
    /**
     * @return guarCont
     */
	public String getGuarCont() {
		return this.guarCont;
	}
	
	/**
	 * @param debtStartDate
	 */
	public void setDebtStartDate(String debtStartDate) {
		this.debtStartDate = debtStartDate;
	}
	
    /**
     * @return debtStartDate
     */
	public String getDebtStartDate() {
		return this.debtStartDate;
	}
	
	/**
	 * @param debtEndDate
	 */
	public void setDebtEndDate(String debtEndDate) {
		this.debtEndDate = debtEndDate;
	}
	
    /**
     * @return debtEndDate
     */
	public String getDebtEndDate() {
		return this.debtEndDate;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}