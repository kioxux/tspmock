package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0119</br>
 * 任务名称：加工任务-业务处理-任务加急 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0122Mapper {

    /**
     * 插入风险提示表,距离诉讼时效到期日前12个月 、6个月 3个月提醒开始
     * @param openDay
     * @return
     */
    int insertWbMsgNotice01(@Param("openDay") String openDay);
    /**
     * 插入风险提示表开始,距离抵债资产处置时效到期日前6个月 、4个月、1个月
     * @param openDay
     * @return
     */
    int insertWbMsgNotice02(@Param("openDay") String openDay);
    /**
     * 插入风险提示表开始,单户核销  核销后2年（24个月）开始提醒，提醒相关人员进行核销后事项处理
     * @param openDay
     * @return
     */
    int insertWbMsgNotice03(@Param("openDay") String openDay);
    /**
     * 插入风险提示表开始,批量核销  核销后2年（24个月）开始提醒，提醒相关人员进行核销后事项处理
     * @param openDay
     * @return
     */
    int insertWbMsgNotice04(@Param("openDay") String openDay);
    /**
     * 插入风险提示表开始,信用卡核销  核销后2年（24个月）开始提醒，提醒相关人员进行核销后事项处理
     * @param openDay
     * @return
     */
    int insertWbMsgNotice05(@Param("openDay") String openDay);
}
