package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0421</br>
 * 任务名称：加工任务-贷后管理-公司客户风险分类  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0421Mapper {
    /**
     * 删除临时表
     *
     * @param openDay
     * @return
     */

    int truncatetmpupdatedate042fl(@Param("openDay") String openDay);
    /**
     * 插入对公贷款客户
     *
     * @param openDay
     * @return
     */

    int insertruncatetmpupdatedate0421dk(@Param("openDay") String openDay);
    /**
     * 插入对公委托贷款客户
     *
     * @param openDay
     * @return
     */


    int insertruncatetmpupdatedate0421wt(@Param("openDay") String openDay);
    /**
     * 插入对公银承客户
     *
     * @param openDay
     * @return
     */
    int insertruncatetmpupdatedate0421yc(@Param("openDay") String openDay);

    /**
     * 插入对公保函客户
     *
     * @param openDay
     * @return
     */

    int insertruncatetmpupdatedate0421bh(@Param("openDay") String openDay);
    /**
     * 插入信用证客户
     *
     * @param openDay
     * @return
     */
    int insertruncatetmpupdatedate0421xyz(@Param("openDay") String openDay);

    /**
     * 插入贴现客户
     *
     * @param openDay
     * @return
     */
    int insertruncatetmpupdatedate0421tx(@Param("openDay") String openDay);
    /**
     * 插入贷后检查任务表
     *
     * @param openDay
     * @return
     */
    int insertPspRisk01(@Param("openDay") String openDay);




    /**
     * 清理临时表2
     *
     * @param openDay
     * @return
     */

   // int truncatetmpupdatedate04212fl(@Param("openDay") String openDay);



    /**
     * 贷款存在分类结果的客户
     *
     * @param openDay
     * @return
     */


    //int inserttmpupdatedata2(@Param("openDay") String openDay);



    /**
     * 清理临时表
     *
     * @param openDay
     * @return
     */

    //int truncatetmpupdatedate04213fl(@Param("openDay") String openDay);
    /**
     * 任务待发起的客户
     *
     * @param openDay
     * @return
     */
    //int inserttmpupdatedata3(@Param("openDay") String openDay);


    /**
     *  6个月之内首次风险分类任务生成
     *
     * @param openDay
     * @return
     */
    int insertPspRisk02(@Param("openDay") String openDay);
}
