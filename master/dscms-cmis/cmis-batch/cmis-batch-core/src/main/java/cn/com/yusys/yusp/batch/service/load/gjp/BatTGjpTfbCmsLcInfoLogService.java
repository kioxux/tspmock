/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.service.load.gjp ;

import cn.com.yusys.yusp.batch.domain.load.gjp.BatTGjpTfbCmsLcInfoLog;
import cn.com.yusys.yusp.batch.repository.mapper.load.gjp.BatTGjpTfbCmsLcInfoLogMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTGjpTfbCmsLcInfoLogService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 21:47:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BatTGjpTfbCmsLcInfoLogService {

    @Autowired
    private BatTGjpTfbCmsLcInfoLogMapper batTGjpTfbCmsLcInfoLogMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BatTGjpTfbCmsLcInfoLog selectByPrimaryKey(String id) {
        return batTGjpTfbCmsLcInfoLogMapper.selectByPrimaryKey(id);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BatTGjpTfbCmsLcInfoLog> selectAll(QueryModel model) {
        List<BatTGjpTfbCmsLcInfoLog> records = (List<BatTGjpTfbCmsLcInfoLog>) batTGjpTfbCmsLcInfoLogMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BatTGjpTfbCmsLcInfoLog> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BatTGjpTfbCmsLcInfoLog> list = batTGjpTfbCmsLcInfoLogMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BatTGjpTfbCmsLcInfoLog record) {
        return batTGjpTfbCmsLcInfoLogMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BatTGjpTfbCmsLcInfoLog record) {
        return batTGjpTfbCmsLcInfoLogMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BatTGjpTfbCmsLcInfoLog record) {
        return batTGjpTfbCmsLcInfoLogMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BatTGjpTfbCmsLcInfoLog record) {
        return batTGjpTfbCmsLcInfoLogMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String id) {
        return batTGjpTfbCmsLcInfoLogMapper.deleteByPrimaryKey(id);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return batTGjpTfbCmsLcInfoLogMapper.deleteByIds(ids);
    }

    /**
     * 清空落地表
     */
    public void truncateTTable() {
        batTGjpTfbCmsLcInfoLogMapper.truncateTTable();
    }
}
