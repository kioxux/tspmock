/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.fls;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSFlsAppLoanBill
 * @类描述: bat_s_fls_app_loan_bill数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-06 11:10:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_fls_app_loan_bill")
public class BatSFlsAppLoanBill extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 借据编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "LOAN_ID")
	private String loanId;
	
	/** 合同编号 **/
	@Column(name = "CONT_ID", unique = false, nullable = true, length = 40)
	private String contId;

	/** 债项等级 **/
	@Column(name = "LOAN_LEVEL", unique = false, nullable = true, length = 20)
	private String loanLevel;

	/** EAD **/
	@Column(name = "EAD", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal ead;
	
	/** LGD **/
	@Column(name = "LGD", unique = false, nullable = true, length = 24)
	private java.math.BigDecimal lgd;
	

	
	/** 数据日期 **/
	@Column(name = "data_date", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param loanId
	 */
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}
	
    /**
     * @return loanId
     */
	public String getLoanId() {
		return this.loanId;
	}
	
	/**
	 * @param contId
	 */
	public void setContId(String contId) {
		this.contId = contId;
	}
	
    /**
     * @return contId
     */
	public String getContId() {
		return this.contId;
	}
	
	/**
	 * @param ead
	 */
	public void setEad(java.math.BigDecimal ead) {
		this.ead = ead;
	}
	
    /**
     * @return ead
     */
	public java.math.BigDecimal getEad() {
		return this.ead;
	}
	
	/**
	 * @param lgd
	 */
	public void setLgd(java.math.BigDecimal lgd) {
		this.lgd = lgd;
	}
	
    /**
     * @return lgd
     */
	public java.math.BigDecimal getLgd() {
		return this.lgd;
	}
	
	/**
	 * @param loanLevel
	 */
	public void setLoanLevel(String loanLevel) {
		this.loanLevel = loanLevel;
	}
	
    /**
     * @return loanLevel
     */
	public String getLoanLevel() {
		return this.loanLevel;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}