/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.rcp;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpSnLoanDetailFileTemp;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpSnLoanDetailFileTempService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpSnLoanDetailFileTempResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-04 15:50:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsrcpsnloandetailfiletemp")
public class BatSRcpSnLoanDetailFileTempResource {
    @Autowired
    private BatSRcpSnLoanDetailFileTempService batSRcpSnLoanDetailFileTempService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSRcpSnLoanDetailFileTemp>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSRcpSnLoanDetailFileTemp> list = batSRcpSnLoanDetailFileTempService.selectAll(queryModel);
        return new ResultDto<List<BatSRcpSnLoanDetailFileTemp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSRcpSnLoanDetailFileTemp>> index(QueryModel queryModel) {
        List<BatSRcpSnLoanDetailFileTemp> list = batSRcpSnLoanDetailFileTempService.selectByModel(queryModel);
        return new ResultDto<List<BatSRcpSnLoanDetailFileTemp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{contractNo}")
    protected ResultDto<BatSRcpSnLoanDetailFileTemp> show(@PathVariable("contractNo") String contractNo) {
        BatSRcpSnLoanDetailFileTemp batSRcpSnLoanDetailFileTemp = batSRcpSnLoanDetailFileTempService.selectByPrimaryKey(contractNo);
        return new ResultDto<BatSRcpSnLoanDetailFileTemp>(batSRcpSnLoanDetailFileTemp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSRcpSnLoanDetailFileTemp> create(@RequestBody BatSRcpSnLoanDetailFileTemp batSRcpSnLoanDetailFileTemp) throws URISyntaxException {
        batSRcpSnLoanDetailFileTempService.insert(batSRcpSnLoanDetailFileTemp);
        return new ResultDto<BatSRcpSnLoanDetailFileTemp>(batSRcpSnLoanDetailFileTemp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSRcpSnLoanDetailFileTemp batSRcpSnLoanDetailFileTemp) throws URISyntaxException {
        int result = batSRcpSnLoanDetailFileTempService.update(batSRcpSnLoanDetailFileTemp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{contractNo}")
    protected ResultDto<Integer> delete(@PathVariable("contractNo") String contractNo) {
        int result = batSRcpSnLoanDetailFileTempService.deleteByPrimaryKey(contractNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSRcpSnLoanDetailFileTempService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
