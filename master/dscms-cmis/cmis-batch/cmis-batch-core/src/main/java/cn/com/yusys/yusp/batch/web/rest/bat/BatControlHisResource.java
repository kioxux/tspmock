/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatControlHis;
import cn.com.yusys.yusp.batch.service.bat.BatControlHisService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: yusp-batch-core模块
 * @类名称: BatControlHisResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-09 17:19:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batcontrolhis")
public class BatControlHisResource {
    @Autowired
    private BatControlHisService batControlHisService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatControlHis>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatControlHis> list = batControlHisService.selectAll(queryModel);
        return new ResultDto<List<BatControlHis>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatControlHis>> index(QueryModel queryModel) {
        List<BatControlHis> list = batControlHisService.selectByModel(queryModel);
        return new ResultDto<List<BatControlHis>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatControlHis>> query(@RequestBody QueryModel queryModel) {
        List<BatControlHis> list = batControlHisService.selectByModel(queryModel);
        return new ResultDto<List<BatControlHis>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{openDay}")
    protected ResultDto<BatControlHis> show(@PathVariable("openDay") String openDay) {
        BatControlHis batControlHis = batControlHisService.selectByPrimaryKey(openDay);
        return new ResultDto<BatControlHis>(batControlHis);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatControlHis> create(@RequestBody BatControlHis batControlHis) throws URISyntaxException {
        batControlHisService.insert(batControlHis);
        return new ResultDto<BatControlHis>(batControlHis);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatControlHis batControlHis) throws URISyntaxException {
        int result = batControlHisService.update(batControlHis);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{openDay}")
    protected ResultDto<Integer> delete(@PathVariable("openDay") String openDay) {
        int result = batControlHisService.deleteByPrimaryKey(openDay);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batControlHisService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
