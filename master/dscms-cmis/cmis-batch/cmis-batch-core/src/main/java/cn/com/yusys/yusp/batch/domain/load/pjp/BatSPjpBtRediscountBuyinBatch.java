/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.pjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSPjpBtRediscountBuyinBatch
 * @类描述: bat_s_pjp_bt_rediscount_buyin_batch数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-03 22:08:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_pjp_bt_rediscount_buyin_batch")
public class BatSPjpBtRediscountBuyinBatch extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 批次id **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "REDISC_BUY_BATCH_ID")
	private String rediscBuyBatchId;
	
	/** 批次号 **/
	@Column(name = "S_BATCH_NO", unique = false, nullable = true, length = 40)
	private String sBatchNo;
	
	/** 当前操作机构id **/
	@Column(name = "S_BRANCH_ID", unique = false, nullable = true, length = 40)
	private String sBranchId;
	
	/** 产品ID **/
	@Column(name = "S_PROD_ID", unique = false, nullable = true, length = 40)
	private String sProdId;
	
	/** 付息方式 **/
	@Column(name = "S_INT_PAYWAY", unique = false, nullable = true, length = 10)
	private String sIntPayway;
	
	/** 操作员id **/
	@Column(name = "S_OPERATOR_ID", unique = false, nullable = true, length = 40)
	private String sOperatorId;
	
	/** 票据状态 **/
	@Column(name = "S_BILL_STATUS", unique = false, nullable = true, length = 10)
	private String sBillStatus;
	
	/** 创建时间 **/
	@Column(name = "D_CREATE_DT", unique = false, nullable = true, length = 20)
	private String dCreateDt;
	
	/** 票据类型 **/
	@Column(name = "S_BILL_TYPE", unique = false, nullable = true, length = 10)
	private String sBillType;
	
	/** 票据介质 **/
	@Column(name = "S_BILL_MEDIA", unique = false, nullable = true, length = 1)
	private String sBillMedia;
	
	/** 工作流caseID **/
	@Column(name = "CASEID", unique = false, nullable = true, length = 10)
	private Integer caseid;
	
	/** 工作任务ID **/
	@Column(name = "WORKITEM_ID", unique = false, nullable = true, length = 10)
	private Integer workitemId;
	
	/** BUYRATE **/
	@Column(name = "BUYRATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal buyrate;
	
	/** 贴入方名称 **/
	@Column(name = "REDISCOUNTINNAME", unique = false, nullable = true, length = 150)
	private String rediscountinname;
	
	/** 贴入方大额行号 **/
	@Column(name = "REDISCOUNTINBANKCODE", unique = false, nullable = true, length = 20)
	private String rediscountinbankcode;
	
	/** 贴入方账号 **/
	@Column(name = "REDISCOUNTINACCOUNT", unique = false, nullable = true, length = 40)
	private String rediscountinaccount;
	
	/** 贴入方组织机构代码号 **/
	@Column(name = "REDISCOUNTINORGCODE", unique = false, nullable = true, length = 20)
	private String rediscountinorgcode;
	
	/** 贴出方名称 **/
	@Column(name = "REDISCOUNTOUTNAME", unique = false, nullable = true, length = 150)
	private String rediscountoutname;
	
	/** 贴出方大额行号 **/
	@Column(name = "REDISCOUNTOUTCODE", unique = false, nullable = true, length = 20)
	private String rediscountoutcode;
	
	/** 贴出方账号 **/
	@Column(name = "REDISCOUNTOUTACCOUNT", unique = false, nullable = true, length = 40)
	private String rediscountoutaccount;
	
	/** 贴出方组织机构代码 **/
	@Column(name = "REDISCOUNTOUTORGCODE", unique = false, nullable = true, length = 20)
	private String rediscountoutorgcode;
	
	/** 转贴现种类 **/
	@Column(name = "REDISC_TYPE", unique = false, nullable = true, length = 10)
	private String rediscType;
	
	/** 清算方式 **/
	@Column(name = "CLEARING_FLAG", unique = false, nullable = true, length = 10)
	private String clearingFlag;
	
	/** 不得转让标记 **/
	@Column(name = "NOT_ATTORN_FLAG", unique = false, nullable = true, length = 10)
	private String notAttornFlag;
	
	/** 备注（没用到） **/
	@Column(name = "S_REMARK", unique = false, nullable = true, length = 800)
	private String sRemark;
	
	/** 贴入方备注 **/
	@Column(name = "REPLY_REMARK", unique = false, nullable = true, length = 800)
	private String replyRemark;
	
	/** 贴出方备注 **/
	@Column(name = "REDISCOUNTOUTREMARK", unique = false, nullable = true, length = 800)
	private String rediscountoutremark;
	
	/** 是否系统内01系统内02系统外 **/
	@Column(name = "IF_SYSTEM_INNER", unique = false, nullable = true, length = 10)
	private String ifSystemInner;
	
	/** 买入利率 **/
	@Column(name = "F_RATE", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal fRate;
	
	/** 利率类型 **/
	@Column(name = "S_RATE_TYPE", unique = false, nullable = true, length = 10)
	private String sRateType;
	
	/** 是否双向 **/
	@Column(name = "S_IF_BIDIRECT", unique = false, nullable = true, length = 10)
	private String sIfBidirect;
	
	/** 库存状态 **/
	@Column(name = "STORAGE_STS", unique = false, nullable = true, length = 10)
	private String storageSts;
	
	/** 添加状态 **/
	@Column(name = "ADD_STATUS", unique = false, nullable = true, length = 10)
	private String addStatus;
	
	/** 卖出批次id **/
	@Column(name = "RETURN_SALE_BATCH_ID", unique = false, nullable = true, length = 40)
	private String returnSaleBatchId;
	
	/** 双买到期日 **/
	@Column(name = "BIDIRECT_DT", unique = false, nullable = true, length = 10)
	private java.util.Date bidirectDt;
	
	/** 回购到期日 **/
	@Column(name = "RETURN_GOU_DT", unique = false, nullable = true, length = 10)
	private java.util.Date returnGouDt;
	
	/** 批次卖出账号 **/
	@Column(name = "BATCH_REDISCOUNT_OUTACCOUNT", unique = false, nullable = true, length = 40)
	private String batchRediscountOutaccount;
	
	/** 批次买入账号 **/
	@Column(name = "BATCH_REDISCOUNT_INACCOUNT", unique = false, nullable = true, length = 40)
	private String batchRediscountInaccount;
	
	/** 校验位 **/
	@Column(name = "SEQUENCE_ID", unique = false, nullable = true, length = 10)
	private String sequenceId;
	
	/** 扣减对象 **/
	@Column(name = "CREDITOBJ", unique = false, nullable = true, length = 40)
	private String creditobj;
	
	/** 额度类型 **/
	@Column(name = "CREDITTYPE", unique = false, nullable = true, length = 40)
	private String credittype;
	
	/** 第三方码 **/
	@Column(name = "THIRDCODE", unique = false, nullable = true, length = 40)
	private String thirdcode;
	
	/** 授信明细id **/
	@Column(name = "DETAILID", unique = false, nullable = true, length = 40)
	private String detailid;
	
	/** 贴入方类别 **/
	@Column(name = "REDISCOUNTINROLE", unique = false, nullable = true, length = 10)
	private String rediscountinrole;
	
	/** 总金额 **/
	@Column(name = "R_TOTALAMOUNT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal rTotalamount;
	
	/** 总利息 **/
	@Column(name = "R_TOTALINTEREST", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal rTotalinterest;
	
	/** 总笔数 **/
	@Column(name = "R_TOTALBILL", unique = false, nullable = true, length = 10)
	private Integer rTotalbill;
	
	/** 总实付金额 **/
	@Column(name = "R_TOTALPAYMENT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal rTotalpayment;
	
	/** 贴入方承接行行号 **/
	@Column(name = "REDISCOUNTINAGCYCODE", unique = false, nullable = true, length = 20)
	private String rediscountinagcycode;
	
	/** 设否设置额度扣减规则 **/
	@Column(name = "CREDITRULE", unique = false, nullable = true, length = 10)
	private String creditrule;
	
	/** 额度扣减结果 **/
	@Column(name = "CREDITRESULT", unique = false, nullable = true, length = 10)
	private String creditresult;
	
	/** 赎回开放日 **/
	@Column(name = "OPENdate", unique = false, nullable = true, length = 10)
	private java.util.Date oPENdate;
	
	/** 赎回截止日 **/
	@Column(name = "ENDdate", unique = false, nullable = true, length = 10)
	private java.util.Date eNDdate;
	
	/** D_SELL_DT **/
	@Column(name = "D_SELL_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dSellDt;
	
	/** 协议编号 **/
	@Column(name = "S_REDISCT_AGREE_NO", unique = false, nullable = true, length = 12)
	private String sRedisctAgreeNo;
	
	/** S_BUYER_BANK_CODE **/
	@Column(name = "S_BUYER_BANK_CODE", unique = false, nullable = true, length = 40)
	private String sBuyerBankCode;
	
	/** 往来帐号 **/
	@Column(name = "S_ACCOUNT", unique = false, nullable = true, length = 40)
	private String sAccount;
	
	/** 客户经理id **/
	@Column(name = "S_CUST_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String sCustManagerId;
	
	/** 买入日期 **/
	@Column(name = "D_BUY_DT", unique = false, nullable = true, length = 10)
	private java.util.Date dBuyDt;
	
	/** 对方行号 **/
	@Column(name = "S_SELLER_BANK_CODE", unique = false, nullable = true, length = 40)
	private String sSellerBankCode;
	
	/** 大额行号 **/
	@Column(name = "S_BANKinteger", unique = false, nullable = true, length = 12)
	private String sBANKinteger;
	
	/** 是否回购状态 0=未到期 1=已到期未回购 2=已到期已回购 3=到期未回购转买断 **/
	@Column(name = "BILLDT_ZT", unique = false, nullable = true, length = 2)
	private String billdtZt;
	
	/** AGENT_FLAG **/
	@Column(name = "AGENT_FLAG", unique = false, nullable = true, length = 2)
	private String agentFlag;
	
	/** 隔夜待持 **/
	@Column(name = "OVERNIGHT_FLAG", unique = false, nullable = true, length = 6)
	private String overnightFlag;
	
	/** 贴出方总行行号 **/
	@Column(name = "RDOUTBKCODE", unique = false, nullable = true, length = 100)
	private String rdoutbkcode;
	
	/** 贴出方总行行名 **/
	@Column(name = "RDOUTBKNAME", unique = false, nullable = true, length = 200)
	private String rdoutbkname;
	
	/** 占用额度类型 **/
	@Column(name = "RDOUT0CCUPYTYPE", unique = false, nullable = true, length = 20)
	private String rdout0ccupytype;
	
	/** 数据移植标记 1:移植数据  0：正常数据 **/
	@Column(name = "YZ_FLAG", unique = false, nullable = true, length = 2)
	private String yzFlag;
	
	/** 贴出方机id(系统内有值) **/
	@Column(name = "REDISCOUNT_OUT_BRANCH_ID", unique = false, nullable = true, length = 40)
	private String rediscountOutBranchId;
	
	/** 贴出方机构号(系统内有值) **/
	@Column(name = "REDISCOUNT_OUT_BRANCH_NO", unique = false, nullable = true, length = 40)
	private String rediscountOutBranchNo;
	
	/** 客户号 **/
	@Column(name = "CUST_NO", unique = false, nullable = true, length = 50)
	private String custNo;
	
	/** 报价ID **/
	@Column(name = "QUOTATION_BATCH_ID", unique = false, nullable = true, length = 40)
	private String quotationBatchId;
	
	/** ACCT_date **/
	@Column(name = "ACCT_date", unique = false, nullable = true, length = 10)
	private java.util.Date aCCTDate;
	
	/** ACCT_FLOW_NO **/
	@Column(name = "ACCT_FLOW_NO", unique = false, nullable = true, length = 60)
	private String acctFlowNo;
	
	/** ACCT_USER_NO **/
	@Column(name = "ACCT_USER_NO", unique = false, nullable = true, length = 60)
	private String acctUserNo;
	
	/** ACCT_USER_NAME **/
	@Column(name = "ACCT_USER_NAME", unique = false, nullable = true, length = 60)
	private String acctUserName;
	
	/** ACCT_AUTH_USER_NO **/
	@Column(name = "ACCT_AUTH_USER_NO", unique = false, nullable = true, length = 60)
	private String acctAuthUserNo;
	
	/** ACCT_AUTH_USER_NAME **/
	@Column(name = "ACCT_AUTH_USER_NAME", unique = false, nullable = true, length = 60)
	private String acctAuthUserName;
	
	/** CORE_TRANDT **/
	@Column(name = "CORE_TRANDT", unique = false, nullable = true, length = 20)
	private String coreTrandt;
	
	/** CORE_TRANSQ **/
	@Column(name = "CORE_TRANSQ", unique = false, nullable = true, length = 60)
	private String coreTransq;
	
	
	/**
	 * @param rediscBuyBatchId
	 */
	public void setRediscBuyBatchId(String rediscBuyBatchId) {
		this.rediscBuyBatchId = rediscBuyBatchId;
	}
	
    /**
     * @return rediscBuyBatchId
     */
	public String getRediscBuyBatchId() {
		return this.rediscBuyBatchId;
	}
	
	/**
	 * @param sBatchNo
	 */
	public void setSBatchNo(String sBatchNo) {
		this.sBatchNo = sBatchNo;
	}
	
    /**
     * @return sBatchNo
     */
	public String getSBatchNo() {
		return this.sBatchNo;
	}
	
	/**
	 * @param sBranchId
	 */
	public void setSBranchId(String sBranchId) {
		this.sBranchId = sBranchId;
	}
	
    /**
     * @return sBranchId
     */
	public String getSBranchId() {
		return this.sBranchId;
	}
	
	/**
	 * @param sProdId
	 */
	public void setSProdId(String sProdId) {
		this.sProdId = sProdId;
	}
	
    /**
     * @return sProdId
     */
	public String getSProdId() {
		return this.sProdId;
	}
	
	/**
	 * @param sIntPayway
	 */
	public void setSIntPayway(String sIntPayway) {
		this.sIntPayway = sIntPayway;
	}
	
    /**
     * @return sIntPayway
     */
	public String getSIntPayway() {
		return this.sIntPayway;
	}
	
	/**
	 * @param sOperatorId
	 */
	public void setSOperatorId(String sOperatorId) {
		this.sOperatorId = sOperatorId;
	}
	
    /**
     * @return sOperatorId
     */
	public String getSOperatorId() {
		return this.sOperatorId;
	}
	
	/**
	 * @param sBillStatus
	 */
	public void setSBillStatus(String sBillStatus) {
		this.sBillStatus = sBillStatus;
	}
	
    /**
     * @return sBillStatus
     */
	public String getSBillStatus() {
		return this.sBillStatus;
	}
	
	/**
	 * @param dCreateDt
	 */
	public void setDCreateDt(String dCreateDt) {
		this.dCreateDt = dCreateDt;
	}
	
    /**
     * @return dCreateDt
     */
	public String getDCreateDt() {
		return this.dCreateDt;
	}
	
	/**
	 * @param sBillType
	 */
	public void setSBillType(String sBillType) {
		this.sBillType = sBillType;
	}
	
    /**
     * @return sBillType
     */
	public String getSBillType() {
		return this.sBillType;
	}
	
	/**
	 * @param sBillMedia
	 */
	public void setSBillMedia(String sBillMedia) {
		this.sBillMedia = sBillMedia;
	}
	
    /**
     * @return sBillMedia
     */
	public String getSBillMedia() {
		return this.sBillMedia;
	}
	
	/**
	 * @param caseid
	 */
	public void setCaseid(Integer caseid) {
		this.caseid = caseid;
	}
	
    /**
     * @return caseid
     */
	public Integer getCaseid() {
		return this.caseid;
	}
	
	/**
	 * @param workitemId
	 */
	public void setWorkitemId(Integer workitemId) {
		this.workitemId = workitemId;
	}
	
    /**
     * @return workitemId
     */
	public Integer getWorkitemId() {
		return this.workitemId;
	}
	
	/**
	 * @param buyrate
	 */
	public void setBuyrate(java.math.BigDecimal buyrate) {
		this.buyrate = buyrate;
	}
	
    /**
     * @return buyrate
     */
	public java.math.BigDecimal getBuyrate() {
		return this.buyrate;
	}
	
	/**
	 * @param rediscountinname
	 */
	public void setRediscountinname(String rediscountinname) {
		this.rediscountinname = rediscountinname;
	}
	
    /**
     * @return rediscountinname
     */
	public String getRediscountinname() {
		return this.rediscountinname;
	}
	
	/**
	 * @param rediscountinbankcode
	 */
	public void setRediscountinbankcode(String rediscountinbankcode) {
		this.rediscountinbankcode = rediscountinbankcode;
	}
	
    /**
     * @return rediscountinbankcode
     */
	public String getRediscountinbankcode() {
		return this.rediscountinbankcode;
	}
	
	/**
	 * @param rediscountinaccount
	 */
	public void setRediscountinaccount(String rediscountinaccount) {
		this.rediscountinaccount = rediscountinaccount;
	}
	
    /**
     * @return rediscountinaccount
     */
	public String getRediscountinaccount() {
		return this.rediscountinaccount;
	}
	
	/**
	 * @param rediscountinorgcode
	 */
	public void setRediscountinorgcode(String rediscountinorgcode) {
		this.rediscountinorgcode = rediscountinorgcode;
	}
	
    /**
     * @return rediscountinorgcode
     */
	public String getRediscountinorgcode() {
		return this.rediscountinorgcode;
	}
	
	/**
	 * @param rediscountoutname
	 */
	public void setRediscountoutname(String rediscountoutname) {
		this.rediscountoutname = rediscountoutname;
	}
	
    /**
     * @return rediscountoutname
     */
	public String getRediscountoutname() {
		return this.rediscountoutname;
	}
	
	/**
	 * @param rediscountoutcode
	 */
	public void setRediscountoutcode(String rediscountoutcode) {
		this.rediscountoutcode = rediscountoutcode;
	}
	
    /**
     * @return rediscountoutcode
     */
	public String getRediscountoutcode() {
		return this.rediscountoutcode;
	}
	
	/**
	 * @param rediscountoutaccount
	 */
	public void setRediscountoutaccount(String rediscountoutaccount) {
		this.rediscountoutaccount = rediscountoutaccount;
	}
	
    /**
     * @return rediscountoutaccount
     */
	public String getRediscountoutaccount() {
		return this.rediscountoutaccount;
	}
	
	/**
	 * @param rediscountoutorgcode
	 */
	public void setRediscountoutorgcode(String rediscountoutorgcode) {
		this.rediscountoutorgcode = rediscountoutorgcode;
	}
	
    /**
     * @return rediscountoutorgcode
     */
	public String getRediscountoutorgcode() {
		return this.rediscountoutorgcode;
	}
	
	/**
	 * @param rediscType
	 */
	public void setRediscType(String rediscType) {
		this.rediscType = rediscType;
	}
	
    /**
     * @return rediscType
     */
	public String getRediscType() {
		return this.rediscType;
	}
	
	/**
	 * @param clearingFlag
	 */
	public void setClearingFlag(String clearingFlag) {
		this.clearingFlag = clearingFlag;
	}
	
    /**
     * @return clearingFlag
     */
	public String getClearingFlag() {
		return this.clearingFlag;
	}
	
	/**
	 * @param notAttornFlag
	 */
	public void setNotAttornFlag(String notAttornFlag) {
		this.notAttornFlag = notAttornFlag;
	}
	
    /**
     * @return notAttornFlag
     */
	public String getNotAttornFlag() {
		return this.notAttornFlag;
	}
	
	/**
	 * @param sRemark
	 */
	public void setSRemark(String sRemark) {
		this.sRemark = sRemark;
	}
	
    /**
     * @return sRemark
     */
	public String getSRemark() {
		return this.sRemark;
	}
	
	/**
	 * @param replyRemark
	 */
	public void setReplyRemark(String replyRemark) {
		this.replyRemark = replyRemark;
	}
	
    /**
     * @return replyRemark
     */
	public String getReplyRemark() {
		return this.replyRemark;
	}
	
	/**
	 * @param rediscountoutremark
	 */
	public void setRediscountoutremark(String rediscountoutremark) {
		this.rediscountoutremark = rediscountoutremark;
	}
	
    /**
     * @return rediscountoutremark
     */
	public String getRediscountoutremark() {
		return this.rediscountoutremark;
	}
	
	/**
	 * @param ifSystemInner
	 */
	public void setIfSystemInner(String ifSystemInner) {
		this.ifSystemInner = ifSystemInner;
	}
	
    /**
     * @return ifSystemInner
     */
	public String getIfSystemInner() {
		return this.ifSystemInner;
	}
	
	/**
	 * @param fRate
	 */
	public void setFRate(java.math.BigDecimal fRate) {
		this.fRate = fRate;
	}
	
    /**
     * @return fRate
     */
	public java.math.BigDecimal getFRate() {
		return this.fRate;
	}
	
	/**
	 * @param sRateType
	 */
	public void setSRateType(String sRateType) {
		this.sRateType = sRateType;
	}
	
    /**
     * @return sRateType
     */
	public String getSRateType() {
		return this.sRateType;
	}
	
	/**
	 * @param sIfBidirect
	 */
	public void setSIfBidirect(String sIfBidirect) {
		this.sIfBidirect = sIfBidirect;
	}
	
    /**
     * @return sIfBidirect
     */
	public String getSIfBidirect() {
		return this.sIfBidirect;
	}
	
	/**
	 * @param storageSts
	 */
	public void setStorageSts(String storageSts) {
		this.storageSts = storageSts;
	}
	
    /**
     * @return storageSts
     */
	public String getStorageSts() {
		return this.storageSts;
	}
	
	/**
	 * @param addStatus
	 */
	public void setAddStatus(String addStatus) {
		this.addStatus = addStatus;
	}
	
    /**
     * @return addStatus
     */
	public String getAddStatus() {
		return this.addStatus;
	}
	
	/**
	 * @param returnSaleBatchId
	 */
	public void setReturnSaleBatchId(String returnSaleBatchId) {
		this.returnSaleBatchId = returnSaleBatchId;
	}
	
    /**
     * @return returnSaleBatchId
     */
	public String getReturnSaleBatchId() {
		return this.returnSaleBatchId;
	}
	
	/**
	 * @param bidirectDt
	 */
	public void setBidirectDt(java.util.Date bidirectDt) {
		this.bidirectDt = bidirectDt;
	}
	
    /**
     * @return bidirectDt
     */
	public java.util.Date getBidirectDt() {
		return this.bidirectDt;
	}
	
	/**
	 * @param returnGouDt
	 */
	public void setReturnGouDt(java.util.Date returnGouDt) {
		this.returnGouDt = returnGouDt;
	}
	
    /**
     * @return returnGouDt
     */
	public java.util.Date getReturnGouDt() {
		return this.returnGouDt;
	}
	
	/**
	 * @param batchRediscountOutaccount
	 */
	public void setBatchRediscountOutaccount(String batchRediscountOutaccount) {
		this.batchRediscountOutaccount = batchRediscountOutaccount;
	}
	
    /**
     * @return batchRediscountOutaccount
     */
	public String getBatchRediscountOutaccount() {
		return this.batchRediscountOutaccount;
	}
	
	/**
	 * @param batchRediscountInaccount
	 */
	public void setBatchRediscountInaccount(String batchRediscountInaccount) {
		this.batchRediscountInaccount = batchRediscountInaccount;
	}
	
    /**
     * @return batchRediscountInaccount
     */
	public String getBatchRediscountInaccount() {
		return this.batchRediscountInaccount;
	}
	
	/**
	 * @param sequenceId
	 */
	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}
	
    /**
     * @return sequenceId
     */
	public String getSequenceId() {
		return this.sequenceId;
	}
	
	/**
	 * @param creditobj
	 */
	public void setCreditobj(String creditobj) {
		this.creditobj = creditobj;
	}
	
    /**
     * @return creditobj
     */
	public String getCreditobj() {
		return this.creditobj;
	}
	
	/**
	 * @param credittype
	 */
	public void setCredittype(String credittype) {
		this.credittype = credittype;
	}
	
    /**
     * @return credittype
     */
	public String getCredittype() {
		return this.credittype;
	}
	
	/**
	 * @param thirdcode
	 */
	public void setThirdcode(String thirdcode) {
		this.thirdcode = thirdcode;
	}
	
    /**
     * @return thirdcode
     */
	public String getThirdcode() {
		return this.thirdcode;
	}
	
	/**
	 * @param detailid
	 */
	public void setDetailid(String detailid) {
		this.detailid = detailid;
	}
	
    /**
     * @return detailid
     */
	public String getDetailid() {
		return this.detailid;
	}
	
	/**
	 * @param rediscountinrole
	 */
	public void setRediscountinrole(String rediscountinrole) {
		this.rediscountinrole = rediscountinrole;
	}
	
    /**
     * @return rediscountinrole
     */
	public String getRediscountinrole() {
		return this.rediscountinrole;
	}
	
	/**
	 * @param rTotalamount
	 */
	public void setRTotalamount(java.math.BigDecimal rTotalamount) {
		this.rTotalamount = rTotalamount;
	}
	
    /**
     * @return rTotalamount
     */
	public java.math.BigDecimal getRTotalamount() {
		return this.rTotalamount;
	}
	
	/**
	 * @param rTotalinterest
	 */
	public void setRTotalinterest(java.math.BigDecimal rTotalinterest) {
		this.rTotalinterest = rTotalinterest;
	}
	
    /**
     * @return rTotalinterest
     */
	public java.math.BigDecimal getRTotalinterest() {
		return this.rTotalinterest;
	}
	
	/**
	 * @param rTotalbill
	 */
	public void setRTotalbill(Integer rTotalbill) {
		this.rTotalbill = rTotalbill;
	}
	
    /**
     * @return rTotalbill
     */
	public Integer getRTotalbill() {
		return this.rTotalbill;
	}
	
	/**
	 * @param rTotalpayment
	 */
	public void setRTotalpayment(java.math.BigDecimal rTotalpayment) {
		this.rTotalpayment = rTotalpayment;
	}
	
    /**
     * @return rTotalpayment
     */
	public java.math.BigDecimal getRTotalpayment() {
		return this.rTotalpayment;
	}
	
	/**
	 * @param rediscountinagcycode
	 */
	public void setRediscountinagcycode(String rediscountinagcycode) {
		this.rediscountinagcycode = rediscountinagcycode;
	}
	
    /**
     * @return rediscountinagcycode
     */
	public String getRediscountinagcycode() {
		return this.rediscountinagcycode;
	}
	
	/**
	 * @param creditrule
	 */
	public void setCreditrule(String creditrule) {
		this.creditrule = creditrule;
	}
	
    /**
     * @return creditrule
     */
	public String getCreditrule() {
		return this.creditrule;
	}
	
	/**
	 * @param creditresult
	 */
	public void setCreditresult(String creditresult) {
		this.creditresult = creditresult;
	}
	
    /**
     * @return creditresult
     */
	public String getCreditresult() {
		return this.creditresult;
	}
	
	/**
	 * @param oPENdate
	 */
	public void setOPENdate(java.util.Date oPENdate) {
		this.oPENdate = oPENdate;
	}
	
    /**
     * @return oPENdate
     */
	public java.util.Date getOPENdate() {
		return this.oPENdate;
	}
	
	/**
	 * @param eNDdate
	 */
	public void setENDdate(java.util.Date eNDdate) {
		this.eNDdate = eNDdate;
	}
	
    /**
     * @return eNDdate
     */
	public java.util.Date getENDdate() {
		return this.eNDdate;
	}
	
	/**
	 * @param dSellDt
	 */
	public void setDSellDt(java.util.Date dSellDt) {
		this.dSellDt = dSellDt;
	}
	
    /**
     * @return dSellDt
     */
	public java.util.Date getDSellDt() {
		return this.dSellDt;
	}
	
	/**
	 * @param sRedisctAgreeNo
	 */
	public void setSRedisctAgreeNo(String sRedisctAgreeNo) {
		this.sRedisctAgreeNo = sRedisctAgreeNo;
	}
	
    /**
     * @return sRedisctAgreeNo
     */
	public String getSRedisctAgreeNo() {
		return this.sRedisctAgreeNo;
	}
	
	/**
	 * @param sBuyerBankCode
	 */
	public void setSBuyerBankCode(String sBuyerBankCode) {
		this.sBuyerBankCode = sBuyerBankCode;
	}
	
    /**
     * @return sBuyerBankCode
     */
	public String getSBuyerBankCode() {
		return this.sBuyerBankCode;
	}
	
	/**
	 * @param sAccount
	 */
	public void setSAccount(String sAccount) {
		this.sAccount = sAccount;
	}
	
    /**
     * @return sAccount
     */
	public String getSAccount() {
		return this.sAccount;
	}
	
	/**
	 * @param sCustManagerId
	 */
	public void setSCustManagerId(String sCustManagerId) {
		this.sCustManagerId = sCustManagerId;
	}
	
    /**
     * @return sCustManagerId
     */
	public String getSCustManagerId() {
		return this.sCustManagerId;
	}
	
	/**
	 * @param dBuyDt
	 */
	public void setDBuyDt(java.util.Date dBuyDt) {
		this.dBuyDt = dBuyDt;
	}
	
    /**
     * @return dBuyDt
     */
	public java.util.Date getDBuyDt() {
		return this.dBuyDt;
	}
	
	/**
	 * @param sSellerBankCode
	 */
	public void setSSellerBankCode(String sSellerBankCode) {
		this.sSellerBankCode = sSellerBankCode;
	}
	
    /**
     * @return sSellerBankCode
     */
	public String getSSellerBankCode() {
		return this.sSellerBankCode;
	}
	
	/**
	 * @param sBANKinteger
	 */
	public void setSBANKinteger(String sBANKinteger) {
		this.sBANKinteger = sBANKinteger;
	}
	
    /**
     * @return sBANKinteger
     */
	public String getSBANKinteger() {
		return this.sBANKinteger;
	}
	
	/**
	 * @param billdtZt
	 */
	public void setBilldtZt(String billdtZt) {
		this.billdtZt = billdtZt;
	}
	
    /**
     * @return billdtZt
     */
	public String getBilldtZt() {
		return this.billdtZt;
	}
	
	/**
	 * @param agentFlag
	 */
	public void setAgentFlag(String agentFlag) {
		this.agentFlag = agentFlag;
	}
	
    /**
     * @return agentFlag
     */
	public String getAgentFlag() {
		return this.agentFlag;
	}
	
	/**
	 * @param overnightFlag
	 */
	public void setOvernightFlag(String overnightFlag) {
		this.overnightFlag = overnightFlag;
	}
	
    /**
     * @return overnightFlag
     */
	public String getOvernightFlag() {
		return this.overnightFlag;
	}
	
	/**
	 * @param rdoutbkcode
	 */
	public void setRdoutbkcode(String rdoutbkcode) {
		this.rdoutbkcode = rdoutbkcode;
	}
	
    /**
     * @return rdoutbkcode
     */
	public String getRdoutbkcode() {
		return this.rdoutbkcode;
	}
	
	/**
	 * @param rdoutbkname
	 */
	public void setRdoutbkname(String rdoutbkname) {
		this.rdoutbkname = rdoutbkname;
	}
	
    /**
     * @return rdoutbkname
     */
	public String getRdoutbkname() {
		return this.rdoutbkname;
	}
	
	/**
	 * @param rdout0ccupytype
	 */
	public void setRdout0ccupytype(String rdout0ccupytype) {
		this.rdout0ccupytype = rdout0ccupytype;
	}
	
    /**
     * @return rdout0ccupytype
     */
	public String getRdout0ccupytype() {
		return this.rdout0ccupytype;
	}
	
	/**
	 * @param yzFlag
	 */
	public void setYzFlag(String yzFlag) {
		this.yzFlag = yzFlag;
	}
	
    /**
     * @return yzFlag
     */
	public String getYzFlag() {
		return this.yzFlag;
	}
	
	/**
	 * @param rediscountOutBranchId
	 */
	public void setRediscountOutBranchId(String rediscountOutBranchId) {
		this.rediscountOutBranchId = rediscountOutBranchId;
	}
	
    /**
     * @return rediscountOutBranchId
     */
	public String getRediscountOutBranchId() {
		return this.rediscountOutBranchId;
	}
	
	/**
	 * @param rediscountOutBranchNo
	 */
	public void setRediscountOutBranchNo(String rediscountOutBranchNo) {
		this.rediscountOutBranchNo = rediscountOutBranchNo;
	}
	
    /**
     * @return rediscountOutBranchNo
     */
	public String getRediscountOutBranchNo() {
		return this.rediscountOutBranchNo;
	}
	
	/**
	 * @param custNo
	 */
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	
    /**
     * @return custNo
     */
	public String getCustNo() {
		return this.custNo;
	}
	
	/**
	 * @param quotationBatchId
	 */
	public void setQuotationBatchId(String quotationBatchId) {
		this.quotationBatchId = quotationBatchId;
	}
	
    /**
     * @return quotationBatchId
     */
	public String getQuotationBatchId() {
		return this.quotationBatchId;
	}
	
	/**
	 * @param aCCTDate
	 */
	public void setACCTDate(java.util.Date aCCTDate) {
		this.aCCTDate = aCCTDate;
	}
	
    /**
     * @return aCCTDate
     */
	public java.util.Date getACCTDate() {
		return this.aCCTDate;
	}
	
	/**
	 * @param acctFlowNo
	 */
	public void setAcctFlowNo(String acctFlowNo) {
		this.acctFlowNo = acctFlowNo;
	}
	
    /**
     * @return acctFlowNo
     */
	public String getAcctFlowNo() {
		return this.acctFlowNo;
	}
	
	/**
	 * @param acctUserNo
	 */
	public void setAcctUserNo(String acctUserNo) {
		this.acctUserNo = acctUserNo;
	}
	
    /**
     * @return acctUserNo
     */
	public String getAcctUserNo() {
		return this.acctUserNo;
	}
	
	/**
	 * @param acctUserName
	 */
	public void setAcctUserName(String acctUserName) {
		this.acctUserName = acctUserName;
	}
	
    /**
     * @return acctUserName
     */
	public String getAcctUserName() {
		return this.acctUserName;
	}
	
	/**
	 * @param acctAuthUserNo
	 */
	public void setAcctAuthUserNo(String acctAuthUserNo) {
		this.acctAuthUserNo = acctAuthUserNo;
	}
	
    /**
     * @return acctAuthUserNo
     */
	public String getAcctAuthUserNo() {
		return this.acctAuthUserNo;
	}
	
	/**
	 * @param acctAuthUserName
	 */
	public void setAcctAuthUserName(String acctAuthUserName) {
		this.acctAuthUserName = acctAuthUserName;
	}
	
    /**
     * @return acctAuthUserName
     */
	public String getAcctAuthUserName() {
		return this.acctAuthUserName;
	}
	
	/**
	 * @param coreTrandt
	 */
	public void setCoreTrandt(String coreTrandt) {
		this.coreTrandt = coreTrandt;
	}
	
    /**
     * @return coreTrandt
     */
	public String getCoreTrandt() {
		return this.coreTrandt;
	}
	
	/**
	 * @param coreTransq
	 */
	public void setCoreTransq(String coreTransq) {
		this.coreTransq = coreTransq;
	}
	
    /**
     * @return coreTransq
     */
	public String getCoreTransq() {
		return this.coreTransq;
	}


}