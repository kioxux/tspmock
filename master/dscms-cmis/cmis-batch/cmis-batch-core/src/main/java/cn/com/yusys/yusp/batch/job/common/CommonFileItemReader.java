package cn.com.yusys.yusp.batch.job.common;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.SimpleBinaryBufferedReaderFactory;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.separator.SimpleRecordSeparatorPolicy;
import org.springframework.batch.item.file.transform.DefaultFieldSetFactory;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 *  平台文件公共读取数据
 * @param <T>
 * @date 2020-01-17 15:49
 */
public class CommonFileItemReader<T> extends FlatFileItemReader<T> {

    public CommonFileItemReader() {
    }

    public CommonFileItemReader(Class clz, String filepath, String openDay, String delimiter, String Encoding) {
        this.init(clz, filepath, openDay, delimiter, Encoding, 0);
    }

    /**
     * 使用分隔符读文件
     *
     * @param clz
     * @param filepath
     * @param openDay
     * @param delimiter
     * @param encoding
     * @param skipNum
     */
    public void init(Class<T> clz, String filepath, String openDay, String delimiter, String encoding, int skipNum) {
        //设置源文件路经
        FileSystemResource fileSystemResource = new FileSystemResource(new File(filepath));
        setResource(fileSystemResource);
        setEncoding(encoding);
        setRecordSeparatorPolicy(new SimpleRecordSeparatorPolicy());
        //LineMapper给定当前行和与其关联的行号，映射器应返回结果域对象
        DefaultLineMapper defaultLineMapper = new DefaultLineMapper();

        //将输入行转换为a的抽象
        DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
        delimitedLineTokenizer.setFieldSetFactory(new DefaultFieldSetFactory());

        //读取设置字段
        Field[] fields = clz.getDeclaredFields();
        List<String> list = new ArrayList<>();
        for (Field field : fields) {
            if (!Modifier.isStatic(field.getModifiers())) {
                list.add(field.getName());
            }
        }
        delimitedLineTokenizer.setNames(list.toArray(new String[0]));
        //设置分隔符
        delimitedLineTokenizer.setDelimiter(delimiter);
        defaultLineMapper.setLineTokenizer(delimitedLineTokenizer);
        BeanWrapperFieldSetMapper fieldSetMapper = new BeanWrapperFieldSetMapper();
        fieldSetMapper.setTargetType(clz);
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);
        setLineMapper(defaultLineMapper);
        setLinesToSkip(skipNum);
        SimpleBinaryBufferedReaderFactory simpleBinaryBufferedReaderFactory = new SimpleBinaryBufferedReaderFactory();
        simpleBinaryBufferedReaderFactory.setLineEnding("\r\n");
        setBufferedReaderFactory(simpleBinaryBufferedReaderFactory);
    }


}
