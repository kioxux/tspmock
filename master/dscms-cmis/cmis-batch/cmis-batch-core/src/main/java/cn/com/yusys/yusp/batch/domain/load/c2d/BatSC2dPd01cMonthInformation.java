/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.c2d;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSC2dPd01cMonthInformation
 * @类描述: bat_s_c2d_pd01c_month_information数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 10:02:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_c2d_pd01c_month_information")
public class BatSC2dPd01cMonthInformation extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** id **/
	@Id
	@Column(name = "ID")
	private String id;
	
	/** 报告id **/
	@Id
	@Column(name = "REPORT_ID")
	private String reportId;
	
	/** 月份 **/
	@Column(name = "MONTH", unique = false, nullable = true, length = 32)
	private String month;
	
	/** 账户状态 **/
	@Column(name = "ACCOUNT_STATUS", unique = false, nullable = true, length = 60)
	private String accountStatus;
	
	/** 余额 **/
	@Column(name = "BALANCE", unique = false, nullable = true, length = 22)
	private String balance;
	
	/** 已用额度 **/
	@Column(name = "QUOTA_USED", unique = false, nullable = true, length = 22)
	private String quotaUsed;
	
	/** 未出单的大额专项分期余额 **/
	@Column(name = "INSTALLMENT_BALANCE", unique = false, nullable = true, length = 22)
	private String installmentBalance;
	
	/** 五级分类 **/
	@Column(name = "FIVE_LEVEL_CLASSIFICATION", unique = false, nullable = true, length = 60)
	private String fiveLevelClassification;
	
	/** 剩余还款期数 **/
	@Column(name = "REMAINING_PERIOD", unique = false, nullable = true, length = 8)
	private String remainingPeriod;
	
	/** 结算/应还款日 **/
	@Column(name = "SETTLEMENT_DAY", unique = false, nullable = true, length = 32)
	private String settlementDay;
	
	/** 本月应还款 **/
	@Column(name = "REPAYMENT", unique = false, nullable = true, length = 22)
	private String repayment;
	
	/** 本月实还款 **/
	@Column(name = "ACTUAL_REPAYMENT", unique = false, nullable = true, length = 22)
	private String actualRepayment;
	
	/** 最近一次还款日期 **/
	@Column(name = "DATE_OF_LAST", unique = false, nullable = true, length = 32)
	private String dateOfLast;
	
	/** 当前逾期期数 **/
	@Column(name = "OVERDUE_PERIOD", unique = false, nullable = true, length = 8)
	private String overduePeriod;
	
	/** 当前逾期总额 **/
	@Column(name = "OVERDUE_AMOUNT", unique = false, nullable = true, length = 22)
	private String overdueAmount;
	
	/** 逾期 31—60 天未还本金 **/
	@Column(name = "UNPAID_PRINCIPAL1", unique = false, nullable = true, length = 22)
	private String unpaidPrincipal1;
	
	/** 逾期 61－90 天未还本金 **/
	@Column(name = "UNPAID_PRINCIPAL2", unique = false, nullable = true, length = 22)
	private String unpaidPrincipal2;
	
	/** 逾期 91－180 天未还本金 **/
	@Column(name = "UNPAID_PRINCIPAL3", unique = false, nullable = true, length = 22)
	private String unpaidPrincipal3;
	
	/** 逾期 180 天以上未还本金 **/
	@Column(name = "UNPAID_PRINCIPAL4", unique = false, nullable = true, length = 22)
	private String unpaidPrincipal4;
	
	/** 透支 180 天以上未付余额 **/
	@Column(name = "OUTSTANDING_BALANCE", unique = false, nullable = true, length = 22)
	private String outstandingBalance;
	
	/** 最近 6 个月平均使用额度 **/
	@Column(name = "USAGE_QUOTA", unique = false, nullable = true, length = 22)
	private String usageQuota;
	
	/** 最近 6 个月平均透支余额 **/
	@Column(name = "OVERDRAFT_BALANCE", unique = false, nullable = true, length = 22)
	private String overdraftBalance;
	
	/** 最大使用额度 **/
	@Column(name = "MAX_QUOTA", unique = false, nullable = true, length = 22)
	private String maxQuota;
	
	/** 最大透支余额 **/
	@Column(name = "MAX_BALANCE", unique = false, nullable = true, length = 22)
	private String maxBalance;
	
	/** 信息报告日期 **/
	@Column(name = "INFORMATION_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date informationDate;
	
	/** 关联ID **/
	@Column(name = "UNION_ID", unique = false, nullable = true, length = 32)
	private String unionId;
	
	/** 数据日期 **/
	@Column(name = "DATA_DATE", unique = false, nullable = true, length = 20)
	private String dataDate;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param reportId
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	
    /**
     * @return reportId
     */
	public String getReportId() {
		return this.reportId;
	}
	
	/**
	 * @param month
	 */
	public void setMonth(String month) {
		this.month = month;
	}
	
    /**
     * @return month
     */
	public String getMonth() {
		return this.month;
	}
	
	/**
	 * @param accountStatus
	 */
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	
    /**
     * @return accountStatus
     */
	public String getAccountStatus() {
		return this.accountStatus;
	}
	
	/**
	 * @param balance
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}
	
    /**
     * @return balance
     */
	public String getBalance() {
		return this.balance;
	}
	
	/**
	 * @param quotaUsed
	 */
	public void setQuotaUsed(String quotaUsed) {
		this.quotaUsed = quotaUsed;
	}
	
    /**
     * @return quotaUsed
     */
	public String getQuotaUsed() {
		return this.quotaUsed;
	}
	
	/**
	 * @param installmentBalance
	 */
	public void setInstallmentBalance(String installmentBalance) {
		this.installmentBalance = installmentBalance;
	}
	
    /**
     * @return installmentBalance
     */
	public String getInstallmentBalance() {
		return this.installmentBalance;
	}
	
	/**
	 * @param fiveLevelClassification
	 */
	public void setFiveLevelClassification(String fiveLevelClassification) {
		this.fiveLevelClassification = fiveLevelClassification;
	}
	
    /**
     * @return fiveLevelClassification
     */
	public String getFiveLevelClassification() {
		return this.fiveLevelClassification;
	}
	
	/**
	 * @param remainingPeriod
	 */
	public void setRemainingPeriod(String remainingPeriod) {
		this.remainingPeriod = remainingPeriod;
	}
	
    /**
     * @return remainingPeriod
     */
	public String getRemainingPeriod() {
		return this.remainingPeriod;
	}
	
	/**
	 * @param settlementDay
	 */
	public void setSettlementDay(String settlementDay) {
		this.settlementDay = settlementDay;
	}
	
    /**
     * @return settlementDay
     */
	public String getSettlementDay() {
		return this.settlementDay;
	}
	
	/**
	 * @param repayment
	 */
	public void setRepayment(String repayment) {
		this.repayment = repayment;
	}
	
    /**
     * @return repayment
     */
	public String getRepayment() {
		return this.repayment;
	}
	
	/**
	 * @param actualRepayment
	 */
	public void setActualRepayment(String actualRepayment) {
		this.actualRepayment = actualRepayment;
	}
	
    /**
     * @return actualRepayment
     */
	public String getActualRepayment() {
		return this.actualRepayment;
	}
	
	/**
	 * @param dateOfLast
	 */
	public void setDateOfLast(String dateOfLast) {
		this.dateOfLast = dateOfLast;
	}
	
    /**
     * @return dateOfLast
     */
	public String getDateOfLast() {
		return this.dateOfLast;
	}
	
	/**
	 * @param overduePeriod
	 */
	public void setOverduePeriod(String overduePeriod) {
		this.overduePeriod = overduePeriod;
	}
	
    /**
     * @return overduePeriod
     */
	public String getOverduePeriod() {
		return this.overduePeriod;
	}
	
	/**
	 * @param overdueAmount
	 */
	public void setOverdueAmount(String overdueAmount) {
		this.overdueAmount = overdueAmount;
	}
	
    /**
     * @return overdueAmount
     */
	public String getOverdueAmount() {
		return this.overdueAmount;
	}
	
	/**
	 * @param unpaidPrincipal1
	 */
	public void setUnpaidPrincipal1(String unpaidPrincipal1) {
		this.unpaidPrincipal1 = unpaidPrincipal1;
	}
	
    /**
     * @return unpaidPrincipal1
     */
	public String getUnpaidPrincipal1() {
		return this.unpaidPrincipal1;
	}
	
	/**
	 * @param unpaidPrincipal2
	 */
	public void setUnpaidPrincipal2(String unpaidPrincipal2) {
		this.unpaidPrincipal2 = unpaidPrincipal2;
	}
	
    /**
     * @return unpaidPrincipal2
     */
	public String getUnpaidPrincipal2() {
		return this.unpaidPrincipal2;
	}
	
	/**
	 * @param unpaidPrincipal3
	 */
	public void setUnpaidPrincipal3(String unpaidPrincipal3) {
		this.unpaidPrincipal3 = unpaidPrincipal3;
	}
	
    /**
     * @return unpaidPrincipal3
     */
	public String getUnpaidPrincipal3() {
		return this.unpaidPrincipal3;
	}
	
	/**
	 * @param unpaidPrincipal4
	 */
	public void setUnpaidPrincipal4(String unpaidPrincipal4) {
		this.unpaidPrincipal4 = unpaidPrincipal4;
	}
	
    /**
     * @return unpaidPrincipal4
     */
	public String getUnpaidPrincipal4() {
		return this.unpaidPrincipal4;
	}
	
	/**
	 * @param outstandingBalance
	 */
	public void setOutstandingBalance(String outstandingBalance) {
		this.outstandingBalance = outstandingBalance;
	}
	
    /**
     * @return outstandingBalance
     */
	public String getOutstandingBalance() {
		return this.outstandingBalance;
	}
	
	/**
	 * @param usageQuota
	 */
	public void setUsageQuota(String usageQuota) {
		this.usageQuota = usageQuota;
	}
	
    /**
     * @return usageQuota
     */
	public String getUsageQuota() {
		return this.usageQuota;
	}
	
	/**
	 * @param overdraftBalance
	 */
	public void setOverdraftBalance(String overdraftBalance) {
		this.overdraftBalance = overdraftBalance;
	}
	
    /**
     * @return overdraftBalance
     */
	public String getOverdraftBalance() {
		return this.overdraftBalance;
	}
	
	/**
	 * @param maxQuota
	 */
	public void setMaxQuota(String maxQuota) {
		this.maxQuota = maxQuota;
	}
	
    /**
     * @return maxQuota
     */
	public String getMaxQuota() {
		return this.maxQuota;
	}
	
	/**
	 * @param maxBalance
	 */
	public void setMaxBalance(String maxBalance) {
		this.maxBalance = maxBalance;
	}
	
    /**
     * @return maxBalance
     */
	public String getMaxBalance() {
		return this.maxBalance;
	}
	
	/**
	 * @param informationDate
	 */
	public void setInformationDate(java.util.Date informationDate) {
		this.informationDate = informationDate;
	}
	
    /**
     * @return informationDate
     */
	public java.util.Date getInformationDate() {
		return this.informationDate;
	}
	
	/**
	 * @param unionId
	 */
	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}
	
    /**
     * @return unionId
     */
	public String getUnionId() {
		return this.unionId;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate;
	}
	
    /**
     * @return dataDate
     */
	public String getDataDate() {
		return this.dataDate;
	}


}