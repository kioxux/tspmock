package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS020B</br>
 * 任务名称：加工任务-额度处理-更新台账占用合同关系处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
public interface Cmis020BMapper {

    /**
     * 删除贷款台账信息表020B
     */
    void deleteTempAccLoan020B();//add 20211019

    /**
     * 插入贷款台账信息表020B
     */
    int insertTempAccLoan020B(@Param("openDay") String openDay);//add 20211019

    /**
     * 删除合同占用关系信息加工表020B
     */
    void deleteTmpDealContAccRel020B();//add 20211019


    /**
     * 插入合同占用关系信息加工表020B
     *
     * @param openDay
     * @return
     */
    int insertTmpDealContAccRel020B(@Param("openDay") String openDay);//add 20211019

    /**
     * insertTdcarYcht- 银承合同产生 垫款 插入 tmp_deal_cont_acc_rel 每天执行
     *
     * @param openDay
     * @return
     */
    int insertTdcarYcht(@Param("openDay") String openDay);


    /**
     * insertTdcarZgesxxy- 最高额授信协议下银承产生垫款 插入 tmp_deal_cont_acc_rel
     *
     * @param openDay
     * @return
     */
    int insertTdcarZgesxxy(@Param("openDay") String openDay);


    /**
     * insertTdcarZgeYc-最高额授信协议下银承产生垫款 插入 tmp_deal_lmt_cont_rel
     *
     * @param openDay
     * @return
     */
    int insertTdcarZgeYc(@Param("openDay") String openDay);


    /**
     * 清空临时表-更新cont_acc_rel贷款与垫款占用金额和余额
     */
    void deleteLmtContAccRel4A();

    /**
     * 插入临时表-更新cont_acc_rel贷款与垫款占用金额和余额
     *
     * @param openDay
     * @return
     */
    int insertLmtContAccRel4A(@Param("openDay") String openDay);

    /**
     * 更新cont_acc_rel贷款占用金额和余额
     *
     * @param openDay
     * @return
     */
    int updateLmtContAccRel4A(@Param("openDay") String openDay);

    /**
     * 清空临时表 更新cont_acc_rel在途贷款占用金额和余额
     */
    void truncateTdcarPla();

    /**
     *
     * 删除贷款出账申请表
     */

    void deleteTempPvpLoanApp020B();//add 20211019


    /**
     *
     * 插入贷款出账申请表
     * @param openDay
     * @return
     */
    int insertTempPvpLoanApp020B(@Param("openDay") String openDay);//add 20211019


    /**
     * 插入临时表 更新cont_acc_rel在途贷款占用金额和余额
     *
     * @param openDay
     * @return
     */
    int insertTdcarPla(@Param("openDay") String openDay);

    /**
     * 更新cont_acc_rel在途贷款占用金额和余额
     *
     * @param openDay
     * @return
     */
    int updateTdcarPla(@Param("openDay") String openDay);


    /**
     * 清空临时表 更新cont_acc_rel 委托贷款占用金额和余额
     */
    void truncateTdcarAcl();

    /**
     * 插入临时表 更新cont_acc_rel 委托贷款占用金额和余额
     *
     * @param openDay
     * @return
     */
    int insertTdcarAcl(@Param("openDay") String openDay);

    /**
     * 更新cont_acc_rel 委托贷款占用金额和余额
     *
     * @param openDay
     * @return
     */
    int updateTdcarAcl(@Param("openDay") String openDay);

    /**
     * 清空临时表 更新cont_acc_rel 在途委托贷款申请占用金额和余额
     */
    void truncateTdcarPela();

    /**
     * 插入临时表 更新cont_acc_rel 在途委托贷款申请占用金额和余额
     *
     * @param openDay
     * @return
     */
    int insertTdcarPela(@Param("openDay") String openDay);

    /**
     * 更新cont_acc_rel 在途委托贷款申请占用金额和余额
     *
     * @param openDay
     * @return
     */
    int updateTdcarPela(@Param("openDay") String openDay);

    /**
     * 清空临时表 更新cont_acc_rel 银承占用金额和余额
     */
    void truncateTdcarAa();

    /**
     *
     * 删除银承台账票据明细020b
     */
    void deleteTempAccAccpDrftSub020b();//add 20211019


    /**
     *
     * 插入银承台账票据明细020b
     * @param openDay
     * @return
     */
    int insertTempAccAccpDrftSub020b(@Param("openDay") String openDay);//add 20211019

    /**
     * 插入临时表 更新cont_acc_rel 银承占用金额和余额
     *
     * @param openDay
     * @return
     */
    int insertTdcarAa(@Param("openDay") String openDay);

    /**
     * 更新cont_acc_rel  银承占用金额和余额
     *
     * @param openDay
     * @return
     */
    int updateTdcarAa(@Param("openDay") String openDay);

    /**
     * 更新cont_acc_rel  银承占用总金额和总敞口金额，是剔除作废与未用退回的票据
     *
     * @param openDay
     * @return
     */
    int updateContAccRelAccTotalAmtAndSpanAmt(@Param("openDay") String openDay);


    /**
     * 清空临时表 更新cont_acc_rel 在途银承申请占用金额和余额
     */
    void deleteTdcarPaa();

    /**
     * 插入临时表 更新cont_acc_rel 在途银承申请占用金额和余额
     *
     * @param openDay
     * @return
     */
    int insertTdcarPaa(@Param("openDay") String openDay);

    /**
     * 更新cont_acc_rel  在途银承申请占用金额和余额
     *
     * @param openDay
     * @return
     */
    int updateTdcarPaa(@Param("openDay") String openDay);

    /**
     * 清空临时表 更新cont_acc_rel 保函占用金额和余额
     */
    void truncateTdcarAc();

    /**
     * 插入临时表 更新cont_acc_rel 保函占用金额和余额
     *
     * @param openDay
     * @return
     */
    int insertTdcarAc(@Param("openDay") String openDay);

    /**
     * 更新cont_acc_rel  保函占用金额和余额
     *
     * @param openDay
     * @return
     */
    int updateTdcarAc(@Param("openDay") String openDay);

    /**
     * 清空临时表 更新cont_acc_rel 信用证占用金额和余额
     */
    void deleteTdcarAtl();

    /**
     * 插入临时表 更新cont_acc_rel 信用证占用金额和余额
     *
     * @param openDay
     * @return
     */
    int insertTdcarAtl(@Param("openDay") String openDay);

    /**
     * 更新cont_acc_rel  信用证占用金额和余额
     *
     * @param openDay
     * @return
     */
    int updateTdcarAtl(@Param("openDay") String openDay);

    /**
     * 清空临时表 更新cont_acc_rel 贴现占用金额和余额
     */
    void truncateTdcarAd();

    /**
     * 插入临时表 更新cont_acc_rel 贴现占用金额和余额
     *
     * @param openDay
     * @return
     */
    int insertTdcarAd(@Param("openDay") String openDay);


    /**
     * 更新cont_acc_rel  贴现占用金额和余额
     *
     * @param openDay
     * @return
     */
    int updateTdcarAd(@Param("openDay") String openDay);

    /**
     * 更新cont_acc_rel  注销和作废的占用关系占用余额为0
     *
     * @param openDay
     * @return
     */
    int updateTdcar(@Param("openDay") String openDay);

    /**
     * 若担保方式为低风险担保，则更新占用敞口为0
     *
     * @param openDay
     * @return
     */
    int updateTdcarGuarMode(@Param("openDay") String openDay);

    /**
     * 已到期的台账占用关系，若占用状态还是生效，则占用状态改为到期未结清
     *
     * @param openDay
     * @return
     */
    int updateContARToSX(@Param("openDay") String openDay);
}
