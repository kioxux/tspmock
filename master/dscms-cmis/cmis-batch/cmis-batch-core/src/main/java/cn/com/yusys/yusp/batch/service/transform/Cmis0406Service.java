package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0406Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0406</br>
 * 任务名称：加工任务-贷后管理-首次检查[贷后临时表数据回插]  </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年09月17日 下午2:56:22
 */
@Service
@Transactional
public class Cmis0406Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0406Service.class);

    @Autowired
    private Cmis0406Mapper cmis0406Mapper;

    /**
     * 首次检查 贷后临时表数据回插
     * @param openDay
     */
    public void cmis0406InsertPspTaskList(String openDay) {
        logger.info("首次检查 贷后临时表数据回插开始,请求参数为:[{}]", openDay);
        int insertPspTaskList = cmis0406Mapper.insertPspTaskList(openDay);
        logger.info("首次检查 贷后临时表数据回插结束,返回参数为:[{}]", insertPspTaskList);
    }
}
