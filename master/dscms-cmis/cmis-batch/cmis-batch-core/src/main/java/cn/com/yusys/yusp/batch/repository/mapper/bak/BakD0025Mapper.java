package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKD0025</br>
 * 任务名称：批前备份日表任务-备份担保合同表[GRT_GUAR_CONT]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakD0025Mapper {

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertCurrent(@Param("openDay") String openDay);

    /**
     * 删除当天数据
     *
     * @return
     */
    void truncateCurrent();

    /**
     * 删除当天的数据
     *
     * @param openDay
     * @return
     */
    int deleteCurrent(@Param("openDay") String openDay);

    /**
     * 查询备份表当天的[担保合同表 [TMP_D_GRT_GUAR_CONT]]数据总条数
     *
     * @param openDay
     * @return
     */
    int bakD0025DeleteOpenDayTmpDGrtGuarContCounts(@Param("openDay") String openDay);


    /**
     *
     * 查询原表当天的[担保合同表 [GRT_GUAR_CONT]]数据总条数
     * @param openDay
     * @return
     */
    int queryGrtGuarContCounts(@Param("openDay") String openDay);
}
