package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0304Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0304</br>
 * 任务名称：加工任务-客户处理-唤醒贷名单规则  </br>
 *
 * @author guyang
 * @version 1.0
 * @since 2021年9月13日 下午14:56:54
 */
@Service
@Transactional
public class Cmis0304Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0304Service.class);

    @Autowired
    private Cmis0304Mapper cmis0304Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 加工符合业务规则数据
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0304InsertTmpCtrLoanCont(String openDay){
        logger.info("重命名创建和删除业务规则临时表数据开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_hxd_ctr");
        logger.info("重命名创建和删除业务规则临时表数据结束");

        logger.info("重命名创建和删除不准入规则临时表数据开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_hxd_acc");
        logger.info("重命名创建和删除不准入规则临时表数据结束");

        logger.info("重命名创建和删除不准入唤醒贷表数据开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","bat_hxd_cus_info");
        logger.info("重命名创建和删除不准入唤醒贷表数据结束");

        logger.info("加工符合业务规则数据开始,请求参数为:[{}]", openDay);
        int insertTmpCtr = cmis0304Mapper.insertTmpCtr(openDay);
        logger.info("加工符合业务规则数据结束,返回参数为:[{}]", insertTmpCtr);
    }

    /**
     * 加工不准入规则数据
     * @param openDay
     */
    public void cmis0304InsertTmpAccLoan(String openDay){
        logger.info("加工不准入规则:借据状态：逾期、核销、未出账、作废数据开始,请求参数为:[{}]", openDay);
        int insertTmpAcc = cmis0304Mapper.insertTmpAcc1(openDay);
        logger.info("加工不准入规则:借据状态：逾期、核销、未出账、作废数据结束,返回参数为:[{}]", insertTmpAcc);

        logger.info("加工不准入规则:五级分类非正常数据开始,请求参数为:[{}]", openDay);
        int insertTmpAcc2 = cmis0304Mapper.insertTmpAcc2(openDay);
        logger.info("加工不准入规则:五级分类非正常数据结束,返回参数为:[{}]", insertTmpAcc2);

        logger.info("加工不准入规则:命中行内黑名单数据开始,请求参数为:[{}]", openDay);
        int insertTmpAcc3 = cmis0304Mapper.insertTmpAcc3(openDay);
        logger.info("加工不准入规则:命中行内黑名单数据结束,返回参数为:[{}]", insertTmpAcc3);

        logger.info("加工不准入规则:加工不准入规则数据: 命中小微黑名单数据开始,请求参数为:[{}]", openDay);
        int insertTmpAcc4 = cmis0304Mapper.insertTmpAcc4(openDay);
        logger.info("加工不准入规则:加工不准入规则数据: 命中小微黑名单数据结束,返回参数为:[{}]", insertTmpAcc4);

        logger.info("加工不准入规则:加工不准入规则数据: 行内贷款本金有逾期 展期、借新还旧 记录数据开始,请求参数为:[{}]", openDay);
        int insertTmpAcc5 = cmis0304Mapper.insertTmpAcc5(openDay);
        logger.info("加工不准入规则:加工不准入规则数据: 行内贷款本金有逾期展期、借新还旧 记录数据结束,返回参数为:[{}]", insertTmpAcc5);


        logger.info("加工不准入规则:存在对公存量余额数据开始,请求参数为:[{}]", openDay);
        int insertTmpAcc8 = cmis0304Mapper.insertTmpAcc8(openDay);
        logger.info("加工不准入规则:存在对公存量余额数据结束,返回参数为:[{}]", insertTmpAcc8);

//        logger.info("加工不准入规则:行内贷款历史还款记录逾期次数＞3 数据开始,请求参数为:[{}]", openDay);
//        int insertTmpAcc9 = cmis0304Mapper.insertTmpAcc9(openDay);
//        logger.info("加工不准入规则:行内贷款历史还款记录逾期次数＞3 数据结束,返回参数为:[{}]", insertTmpAcc9);


    }

    public void cmis0304InsertHXD(String openDay){
        logger.info("加工唤醒贷名单 数据开始,请求参数为:[{}]", openDay);
        int insertHXD = cmis0304Mapper.insertHXD(openDay);
        logger.info("加工唤醒贷名单 数据结束,返回参数为:[{}]", insertHXD);
    }

    /**
     * 加工授信调查任务表
     * @param openDay
     */
    public void cmis0304InsertLmt(String openDay) {
        //
        logger.info("插入唤醒贷名单开始,请求参数为:[{}]", openDay);
        int insertLmt1 = cmis0304Mapper.insertLmt1(openDay);
        logger.info("插入唤醒贷名单结束,返回参数为:[{}]", insertLmt1);


        logger.info("插入客户授信调查主表 开始,请求参数为:[{}]", openDay);
        int insertLmtsurvey = cmis0304Mapper.insertLmtsurvey(openDay);
        logger.info(" 插入客户授信调查主表 结束,返回参数为:[{}]", insertLmtsurvey);
    }


}
