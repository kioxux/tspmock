package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKD0000</br>
 * 任务名称：批前备份日表任务-删除N天前数据</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
@Deprecated
public interface BakD0000Mapper {

    /**
     * 查询待删除3天前的[银承台账[ACC_ACCP]]数据总次数
     *
     * @param openDay
     */
    int bakD0001Delete3DaysAccAccpCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[银承台账[ACC_ACCP]]数据
     *
     * @param openDay
     */
    int bakD0001Delete3DaysAccAccp(@Param("openDay") String openDay);

    /**
     * 查询待删除3天前的[银承台账票据明细 [ACC_ACCP_DRFT_SUB]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0002Delete3DaysAccAccpDrftSubCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[银承台账票据明细 [ACC_ACCP_DRFT_SUB]]数据
     *
     * @param openDay
     */
    int bakD0002Delete3DaysAccAccpDrftSub(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[保函台账[ACC_CVRS]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0003Delete3DaysAccCvrsCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[保函台账[ACC_CVRS]]数据
     *
     * @param openDay
     */
    int bakD0003Delete3DaysAccCvrs(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[贴现台账[ACC_DISC]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0004Delete3DaysAccDiscCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[贴现台账[ACC_DISC]]数据
     *
     * @param openDay
     */
    int bakD0004Delete3DaysAccDisc(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[委托贷款台账[ACC_ENTRUST_LOAN]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0005Delete3DaysccEntrustLoanCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[委托贷款台账[ACC_ENTRUST_LOAN]]数据
     *
     * @param openDay
     */
    int bakD0005Delete3DaysAccEntrustLoan(@Param("openDay") String openDay);

    /**
     * 查询待删除3天前的[贷款台账信息表[ACC_LOAN]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0006Delete3DaysAccLoanCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[贷款台账信息表[ACC_LOAN]]数据
     *
     * @param openDay
     */
    int bakD0006Delete3DaysAccLoan(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[开证台账[ACC_TF_LOC]]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0007Delete3DaysAccTfLocCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[开证台账[ACC_TF_LOC]]数据
     *
     * @param openDay
     */
    int bakD0007Delete3DaysAccTfLoc(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0008Delete3DaysApprCoopInfoCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据
     *
     * @param openDay
     */
    int bakD0008Delete3DaysApprCoopInfo(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[合作方授信台账信息[APPR_COOP_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0009Delete3DaysApprCoopSubCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[合作方授信分项信息[APPR_COOP_SUB_INFO]]数据
     *
     * @param openDay
     */
    int bakD0009Delete3DaysApprCoopSub(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[批复额度分项基础信息[APPR_LMT_SUB_BASIC_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0010Delete3DaysApprLmtSubBasicInfoCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[批复额度分项基础信息[APPR_LMT_SUB_BASIC_INFO]]数据
     *
     * @param openDay
     */
    int bakD0010Delete3DaysApprLmtSubBasicInfo(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[批复主信息[APPR_STR_MTABLE_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0011Delete3DaysApprStrMtableInfoCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[批复主信息[APPR_STR_MTABLE_INFO]]数据
     *
     * @param openDay
     */
    int bakD0011Delete3DaysApprStrMtableInfo(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[批复额度分项基础信息[APPR_LMT_SUB_BASIC_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0012Delete3DaysApprStrOrgInfoCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[批复适用机构[APPR_STR_ORG_INFO]]数据
     *
     * @param openDay
     */
    int bakD0012Delete3DaysApprStrOrgInfo(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[合同占用关系信息[CONT_ACC_REL]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0013Delete3DaysContAccRelCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[合同占用关系信息[CONT_ACC_REL]]数据
     *
     * @param openDay
     */
    int bakD0013Delete3DaysContAccRel(@Param("openDay") String openDay);

    /**
     * 查询待删除3天前的[银承合同详情[CTR_ACCP_CONT]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0014Delete3DaysCtrAccpContCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[银承合同详情[CTR_ACCP_CONT]]数据
     *
     * @param openDay
     */
    int bakD0014Delete3DaysCtrAccpCont(@Param("openDay") String openDay);

    /**
     * 查询待删除3天前的[银承合同详情[CTR_ACCP_CONT]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0015Delete3DaysCtrAsplDetailsCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[资产池协议[CTR_ASPL_DETAILS]]数据
     *
     * @param openDay
     */
    int bakD0015Delete3DaysCtrAsplDetails(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[保函协议详情[CTR_CVRG_CONT]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0016Delete3DaysCtrCvrgContCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[保函协议详情[CTR_CVRG_CONT]]数据
     *
     * @param openDay
     */
    int bakD0016Delete3DaysCtrCvrgCont(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[贴现协议详情[CTR_DISC_CONT]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0017Delete3DaysCtrDiscContCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[贴现协议详情[CTR_DISC_CONT]]数据
     *
     * @param openDay
     */
    int bakD0017Delete3DaysCtrDiscCont(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[贴现协议汇票明细[CTR_DISC_PORDER_SUB]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0018Delete3DaysCtrDiscPorderSubCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[贴现协议汇票明细[CTR_DISC_PORDER_SUB]]数据
     *
     * @param openDay
     */
    int bakD0018Delete3DaysCtrDiscPorderSub(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0019Delete3DaysCtrEntrustLoanContCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]]数据
     *
     * @param openDay
     */
    int bakD0019Delete3DaysCtrEntrustLoanCont(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0020Delete3DaysCtrHighAmtAgrContCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[最高额授信协议[CTR_HIGH_AMT_AGR_CONT]]数据
     *
     * @param openDay
     */
    int bakD0020Delete3DaysCtrHighAmtAgrCont(@Param("openDay") String openDay);

    /**
     * 查询待删除3天前的[贷款合同表[CTR_LOAN_CONT]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0021Delete3DaysCtrLoanContCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[贷款合同表[CTR_LOAN_CONT]]数据
     *
     * @param openDay
     */
    int bakD0021Delete3DaysCtrLoanCont(@Param("openDay") String openDay);

    /**
     * 查询待删除3天前的[行名行号对照表[CFG_BANK_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0022Delete3DaysCfgBankInfoCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[行名行号对照表[CFG_BANK_INFO]]数据
     *
     * @param openDay
     */
    int bakD0022Delete3DaysCfgBankInfo(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[行名行号对照表[CFG_BANK_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0023Delete3DaysCtrTfLocContCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[开证合同详情[CTR_TF_LOC_CONT]]数据
     *
     * @param openDay
     */
    int bakD0023Delete3DaysCtrTfLocCont(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[权证台账[GUAR_WARRANT_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0024Delete3DaysGuarWarrantInfoCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[权证台账[GUAR_WARRANT_INFO]]数据
     *
     * @param openDay
     */
    int bakD0024Delete3DaysGuarWarrantInfo(@Param("openDay") String openDay);

    /**
     * 查询待删除3天前的[担保合同表[GRT_GUAR_CONT]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0025Delete3DaysGrtGuarContCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[担保合同表[GRT_GUAR_CONT]]数据
     *
     * @param openDay
     */
    int bakD0025Delete3DaysGrtGuarCont(@Param("openDay") String openDay);

    /**
     * 查询待删除3天前的[分项占用关系信息[LMT_CONT_REL]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0026Delete3DaysLmtContRelCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[分项占用关系信息[LMT_CONT_REL]]数据
     *
     * @param openDay
     */
    int bakD0026Delete3DaysLmtContRel(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[白名单额度信息[LMT_WHITE_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0027Delete3DaysLmtWhiteInfoCounts(@Param("openDay") String openDay);


    /**
     * 删除3天前的[白名单额度信息[LMT_WHITE_INFO]]数据
     *
     * @param openDay
     */
    int bakD0027Delete3DaysLmtWhiteInfo(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0028Delete3DaysLmtWhiteInfoHistoryCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[白名单额度信息历史[LMT_WHITE_INFO_HISTORY]]数据
     *
     * @param openDay
     */
    int bakD0028Delete3DaysLmtWhiteInfoHistory(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[我行代开他行保函[TMP_GJP_LMT_CVRG_RZKZ]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryD0029Delete3DaysTmpGjpLmtCvrgRzkzCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[我行代开他行保函[TMP_GJP_LMT_CVRG_RZKZ]]数据
     *
     * @param openDay
     */
    int bakD0029Delete3DaysTmpGjpLmtCvrgRzkz(@Param("openDay") String openDay);

    /**
     * 查询待删除3天前的[我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryD0030Delete3DaysTmpGjpLmtPeerRzkzCounts(@Param("openDay") String openDay);

    /**
     * 删除3天前的[我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0030Delete3DaysTmpGjpLmtPeerRzkz(@Param("openDay") String openDay);


    /**
     * 查询待删除3天前的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryD0031Delete3DaysTmpGjpLmtFftRzkzCounts(@Param("openDay") String openDay);


    /**
     * 删除3天前的[福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]]数据总次数
     *
     * @param openDay
     * @return
     */
    int bakD0031Delete3DaysTmpGjpLmtFftRzkz(@Param("openDay") String openDay);

}
