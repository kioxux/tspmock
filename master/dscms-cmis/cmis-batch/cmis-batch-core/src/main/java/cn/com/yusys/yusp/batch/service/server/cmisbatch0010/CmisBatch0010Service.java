package cn.com.yusys.yusp.batch.service.server.cmisbatch0010;

import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0010.Cmisbatch0010ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0010.Cmisbatch0010RespDto;
import cn.com.yusys.yusp.batch.service.bat.BatControlRunService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EchEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * 接口处理Service:提供日终调度平台调起批量任务
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class CmisBatch0010Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0010Service.class);
    @Resource
    private ApplicationContext applicationContext;
    @Resource
    private SimpleJobLauncher customJobLauncher;
    @Autowired
    private BatControlRunService batControlRunService;
    @Autowired
    private BatTaskRunService batTaskRunService;

    private static ExecutorService priFlag01PendingTaskThreadPool = Executors.newFixedThreadPool(300);// 并发启动开始任务和任务级别为1的任务的线程池
    private static ExecutorService priFlag02PendingTaskThreadPool = Executors.newFixedThreadPool(300);// 并发启动任务级别为2的任务的线程池
    private static ExecutorService priFlag03PendingTaskThreadPool = Executors.newFixedThreadPool(300);// 并发启动任务级别为3的任务的线程池

    /**
     * 交易码：cmisbatch0010
     * 交易描述：提供日终调度平台调起批量任务
     *
     * @param cmisbatch0010ReqDto
     * @return
     * @throws Exception
     */
    // @Transactional(rollbackFor = {BizException.class, Exception.class})，此处@Transactional请勿添加，否则会报错
    public Cmisbatch0010RespDto cmisBatch0010(Cmisbatch0010ReqDto cmisbatch0010ReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0010.key, DscmsEnum.TRADE_CODE_CMISBATCH0010.value);
        Cmisbatch0010RespDto cmisbatch0010RespDto = null;// 响应Dto:提供日终调度平台调起批量任务
        try {
            String priFlag = cmisbatch0010ReqDto.getPriFlag();//任务级别
            String taskDate = cmisbatch0010ReqDto.getTaskDate();//任务日期,格式YYYY-MM-DD

            if (Objects.equals(BatEnums.PRI_FLAG_01.key, priFlag)) {
                cmisbatch0010RespDto = this.parallelRunByPriFlag01(taskDate);
            } else if (Objects.equals(BatEnums.PRI_FLAG_02.key, priFlag)) {
                cmisbatch0010RespDto = this.parallelRunByPriFlag02(taskDate);
            } else if (Objects.equals(BatEnums.PRI_FLAG_03.key, priFlag)) {
                cmisbatch0010RespDto = this.parallelRunByPriFlag03(taskDate);
            }
            cmisbatch0010RespDto.setPriFlag(priFlag);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0010.key, DscmsEnum.TRADE_CODE_CMISBATCH0010.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0010.key, DscmsEnum.TRADE_CODE_CMISBATCH0010.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0010.key, DscmsEnum.TRADE_CODE_CMISBATCH0010.value);
        return cmisbatch0010RespDto;
    }

    /**
     * 并发启动开始任务和任务级别为1的任务
     *
     * @return
     * @throws Exception
     */
    private Cmisbatch0010RespDto parallelRunByPriFlag01(String taskDate) throws BizException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
        Cmisbatch0010RespDto cmisbatch0010RespDto = new Cmisbatch0010RespDto();
        cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_S.key);//操作成功标志位
        cmisbatch0010RespDto.setOpMessage(DscmsEnum.OP_FLAG_S.value);

        // 根据任务日期和任务级别查询bat_task_run中是否有任务状态为执行失败的，如果有则重新发起；如果没有则直接启动
        boolean isFailedFlag = false;//是否存在失败任务标志
        QueryModel failedQueryModel = new QueryModel();
        failedQueryModel.addCondition("taskDate", taskDate);
        failedQueryModel.addCondition("priFlag", BatEnums.PRI_FLAG_01.key); // 日终一阶段(加载外围系统文件)
        failedQueryModel.addCondition("taskStatus", BatEnums.TASK_STATUS_101.key);//执行失败
        logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表开始,请求参数为:[{}]", taskDate, BatEnums.PRI_FLAG_01.key, JSON.toJSONString(failedQueryModel));
        List<BatTaskRun> failedTaskList = batTaskRunService.selectFailedTaskList(failedQueryModel);
        logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表结束,响应参数为:[{}]", taskDate, BatEnums.PRI_FLAG_01.key, JSON.toJSONString(failedTaskList));
        if (CollectionUtils.nonEmpty(failedTaskList)) {
            isFailedFlag = true;
            logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表不为空，则存在失败任务标志,重新执行错误的任务", taskDate, BatEnums.PRI_FLAG_01.key);
        } else {
            isFailedFlag = false;
            logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表为空，则不存在失败任务标志,正常发起", taskDate, BatEnums.PRI_FLAG_01.key);
        }


        if (isFailedFlag) {
            try {
                // 存在失败任务标志
                List<String> failedTaskNoList = failedTaskList.stream().map(failedTask -> {
                    String failedTaskNo = failedTask.getTaskNo();
                    return failedTaskNo;
                }).collect(Collectors.toList());
                String failedTaskNos = failedTaskNoList.stream().collect(Collectors.joining(","));
                logger.info("任务日期:[{}],任务级别:[{}]中重新执行的任务编号为:[{}]", taskDate, BatEnums.PRI_FLAG_01.key, failedTaskNos);
                CountDownLatch priFlag01FailedTaskCd = new CountDownLatch(failedTaskNoList.size());
                for (BatTaskRun failedTask : failedTaskList) {
                    priFlag01PendingTaskThreadPool.submit(() -> {
                        try {
                            // Job都是小写,除了bak之外
                            String failedTaskNo = StringUtils.EMPTY;
                            if (failedTask.getTaskNo().contains("BAK")) {
                                failedTaskNo = failedTask.getTaskNo().replaceAll("BAK", "bak");
                            } else if (failedTask.getTaskNo().equals("CMIS020A") || failedTask.getTaskNo().equals("CMIS020B")
                                    || failedTask.getTaskNo().equals("CMIS020C") || failedTask.getTaskNo().equals("CMIS020D")
                                    || failedTask.getTaskNo().equals("CMIS020E") || failedTask.getTaskNo().equals("CMIS020F")
                                    || failedTask.getTaskNo().equals("CMIS020G") || failedTask.getTaskNo().equals("CMIS020H")
                                    || failedTask.getTaskNo().contains("CMIS417") || failedTask.getTaskNo().contains("CMIS418")
                                    || failedTask.getTaskNo().contains("CMIS425") || failedTask.getTaskNo().contains("CMIS431")) {
                                failedTaskNo = failedTask.getTaskNo().replaceAll("CMIS", "cmis");
                            } else if (failedTask.getTaskNo().equals("BATEND")) {
                                // 此处对BATEND任务单独处理
                                failedTaskNo = "batEnd";
                            } else {
                                failedTaskNo = failedTask.getTaskNo().toLowerCase();
                            }
                            String failedTaskName = failedTask.getTaskName();
                            try {
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用开始");
                                customJobLauncher.run((Job) applicationContext.getBean(failedTaskNo + "Job"), createJobParams(taskDate, failedTaskNo, false));
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用完成");
                            } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                                logger.error("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用异常，异常信息为:[" + e.getMessage() + "]");
                                throw BizException.error(null, EchEnum.ECH080014.key, "任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用异常，异常信息为:[" + e.getMessage() + "]");
                            }
                        } catch (Exception ex) {
                            logger.error("任务日期:[" + taskDate + "],任务编号:[" + failedTask.getTaskNo() + "],任务名称:[" + failedTask.getTaskName() + "]调用线程池异常，异常信息为:[" + ex.getMessage() + "]");
                        } finally {
                            priFlag01FailedTaskCd.countDown();
                        }
                    });
                }
            } catch (BizException e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage(e.getMessage());//任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]
                return cmisbatch0010RespDto;
            } catch (Exception e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage("任务日期:[" + taskDate + "重新调用异常，异常信息为:[" + e.getMessage() + "]");//"任务日期:[" + taskDate + "重新调用异常，异常信息为:[" + e.getMessage() + "]"
                return cmisbatch0010RespDto;
            }
        } else {
            // 不存在失败任务标志，正常发起
            // 查询批量运行表，获取任务日期，任务日期等于营业日期
            QueryModel queryModel = new QueryModel();
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(queryModel));
            List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunList));
            if (CollectionUtils.isEmpty(batControlRunList)) {
                // 调度运行管理(BAT_CONTROL_RUN)未配置
                logger.error(TradeLogConstants.BATCH_STEP_EXCEPTION_PREFIX_LOGGER, JobStepEnum.BATSTART_RUN_STEP.key, JobStepEnum.BATSTART_RUN_STEP.value, EchEnum.ECH080008.value);
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage(EchEnum.ECH080008.value);
                return cmisbatch0010RespDto;
            }
            String xxdTaskDate = batControlRunList.get(0).getOpenDay();//营业日期
            if (!Objects.equals(taskDate, xxdTaskDate)) {
                // 日终调度平台的任务日期为,新信贷的营业日期为,两者不一致，请核查。
                logger.error(TradeLogConstants.BATCH_STEP_EXCEPTION_PREFIX_LOGGER, JobStepEnum.BATSTART_RUN_STEP.key, JobStepEnum.BATSTART_RUN_STEP.value, "日终调度平台的任务日期为[" + taskDate + "],新信贷的营业日期为[" + xxdTaskDate + "],两者不一致，请核查");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage("日终调度平台的任务日期为[" + taskDate + "],新信贷的营业日期为[" + xxdTaskDate + "],两者不一致，请核查");
                return cmisbatch0010RespDto;
            }
            // 运行 batStart任务
            String taskNo = "batStart";
            // 查询任务运行信息
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, "任务日期为:[" + taskDate + "],任务编号为:[" + taskNo + "]");
            JobExecution batStartExecution = customJobLauncher.run((Job) applicationContext.getBean(taskNo + "Job"), createJobParams(taskDate, taskNo, false));
            if ("COMPLETED".equals(batStartExecution.getExitStatus().getExitCode())) {
                BatTaskRun batStartTask = batTaskRunService.selectByPrimaryKey(taskDate, taskNo.toUpperCase());
                logger.info("任务日期:【" + taskDate + "】,任务编号:【" + taskNo + "】,任务名称:【" + batStartTask.getTaskName() + "】执行成功");
                batStartTask.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                batStartTask.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                batStartTask.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batStartTask));
                int updateTaskStatus100 = batTaskRunService.updateSelective(batStartTask);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
            } else {
                String errorInfo = batStartExecution.getExitStatus().getExitDescription();
                BatTaskRun batStartTask = batTaskRunService.selectByPrimaryKey(taskDate, taskNo.toUpperCase());
                logger.info("任务日期:【" + taskDate + "】,任务编号:【" + taskNo + "】,任务名称:【" + batStartTask.getTaskName() + "】执行失败，异常信息为：【" + errorInfo + "】");
                batStartTask.setTaskStatus(BatEnums.TASK_STATUS_101.key);// 任务状态 STD_TASK_TYPE, 执行失败
                batStartTask.setErrorCode(EchEnum.ECH089999.key);
                if (errorInfo.length() > 500) {
                    errorInfo = errorInfo.substring(0, 499);
                }
                batStartTask.setErrorInfo(errorInfo);
                batStartTask.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                batStartTask.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batStartTask));
                int updateTaskStatus101 = batTaskRunService.updateSelective(batStartTask);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus101));

                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage(EchEnum.ECH080013.value);//批量任务为:[调度运行开始-更新批量相关表]运行失败,请及时检查!
                return cmisbatch0010RespDto;
            }

            try {
                // 查询待运行的任务列表
                QueryModel btrlQm = new QueryModel();
                btrlQm.addCondition("taskDate", taskDate);
                btrlQm.addCondition("priFlag", BatEnums.PRI_FLAG_01.key); // 日终主任务(非零内评之前)
                btrlQm.addCondition("taskStatus", BatEnums.TASK_STATUS_000.key);
                logger.info("查询待运行的任务列表,请求参数为[{}]", JSON.toJSONString(btrlQm));
                List<BatTaskRun> pendingTaskList = batTaskRunService.selectPendingTask(btrlQm);
                logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATSTART_RUN_STEP.key, JobStepEnum.BATSTART_RUN_STEP.value, "查询待运行的任务列表数量为:[" + pendingTaskList.size() + "]");
                CountDownLatch priFlag01PendingTaskCd = new CountDownLatch(pendingTaskList.size());
                for (BatTaskRun pendingTask : pendingTaskList) {
                    priFlag01PendingTaskThreadPool.submit(() -> {
                        try {
                            // Job都是小写,除了bak之外
                            String pendingTaskNo = StringUtils.EMPTY;
                            if (pendingTask.getTaskNo().contains("BAK")) {
                                pendingTaskNo = pendingTask.getTaskNo().replaceAll("BAK", "bak");
                            } else if (pendingTask.getTaskNo().equals("CMIS020A") || pendingTask.getTaskNo().equals("CMIS020B")
                                    || pendingTask.getTaskNo().equals("CMIS020C") || pendingTask.getTaskNo().equals("CMIS020D")
                                    || pendingTask.getTaskNo().equals("CMIS020E") || pendingTask.getTaskNo().equals("CMIS020F")
                                    || pendingTask.getTaskNo().equals("CMIS020G") || pendingTask.getTaskNo().equals("CMIS020H")
                                    || pendingTask.getTaskNo().contains("CMIS417") || pendingTask.getTaskNo().contains("CMIS418")
                                    || pendingTask.getTaskNo().contains("CMIS425") || pendingTask.getTaskNo().contains("CMIS431")) {
                                pendingTaskNo = pendingTask.getTaskNo().replaceAll("CMIS", "cmis");
                            } else if (pendingTask.getTaskNo().equals("BATEND")) {
                                // 此处对BATEND任务单独处理
                                pendingTaskNo = "batEnd";
                            } else {
                                pendingTaskNo = pendingTask.getTaskNo().toLowerCase();
                            }
                            String pendingTaskName = pendingTask.getTaskName();
                            try {
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]开始调用开始开始调用开始");
                                JobExecution pendingExecution = customJobLauncher.run((Job) applicationContext.getBean(pendingTaskNo + "Job"), createJobParams(taskDate, pendingTaskNo, false));
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用完成");
                            } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                                logger.error("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]");
                                throw BizException.error(null, EchEnum.ECH080014.key, "任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]");
                            }
                        } catch (Exception ex) {
                            logger.error("任务日期:[" + taskDate + "],任务编号:[" + pendingTask.getTaskNo() + "],任务名称:[" + pendingTask.getTaskName() + "]调用线程池异常，异常信息为:[" + ex.getMessage() + "]");
                        } finally {
                            priFlag01PendingTaskCd.countDown();
                        }
                    });
                }
            } catch (BizException e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage(e.getMessage());//任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]
                return cmisbatch0010RespDto;
            } catch (Exception e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");//"任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]"
                return cmisbatch0010RespDto;
            }
        }
        return cmisbatch0010RespDto;
    }


    /**
     * 并发启动任务级别为2的任务
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/parallelRunByPriFlag02")
    public @ResponseBody
    Cmisbatch0010RespDto parallelRunByPriFlag02(String taskDate) throws Exception {
        Cmisbatch0010RespDto cmisbatch0010RespDto = new Cmisbatch0010RespDto();
        cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_S.key);//操作成功标志位
        cmisbatch0010RespDto.setOpMessage(DscmsEnum.OP_FLAG_S.value);
        // 根据任务日期和任务级别查询bat_task_run中是否有任务状态为执行失败的，如果有则重新发起；如果没有则直接启动
        boolean isFailedFlag = false;//是否存在失败任务标志
        QueryModel failedQueryModel = new QueryModel();
        failedQueryModel.addCondition("taskDate", taskDate);
        failedQueryModel.addCondition("priFlag", BatEnums.PRI_FLAG_02.key);// 日终二阶段(台账和额度等加工任务)
        failedQueryModel.addCondition("taskStatus", BatEnums.TASK_STATUS_101.key);//执行失败
        logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表开始,请求参数为:[{}]", taskDate, BatEnums.PRI_FLAG_02.key, JSON.toJSONString(failedQueryModel));
        List<BatTaskRun> failedTaskList = batTaskRunService.selectFailedTaskList(failedQueryModel);
        logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表结束,响应参数为:[{}]", taskDate, BatEnums.PRI_FLAG_02.key, JSON.toJSONString(failedTaskList));

        if (CollectionUtils.nonEmpty(failedTaskList)) {
            isFailedFlag = true;
            logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表不为空，则存在失败任务标志,重新执行错误的任务", taskDate, BatEnums.PRI_FLAG_02.key);
        } else {
            isFailedFlag = false;
            logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表为空，则不存在失败任务标志,正常发起", taskDate, BatEnums.PRI_FLAG_02.key);
        }

        if (isFailedFlag) {
            try {
                // 存在失败任务标志
                List<String> failedTaskNoList = failedTaskList.stream().map(failedTask -> {
                    String failedTaskNo = failedTask.getTaskNo();
                    return failedTaskNo;
                }).collect(Collectors.toList());
                String failedTaskNos = failedTaskNoList.stream().collect(Collectors.joining(","));
                logger.info("任务日期:[{}],任务级别:[{}]中重新执行的任务编号为:[{}]", taskDate, BatEnums.PRI_FLAG_01.key, failedTaskNos);
                CountDownLatch priFlag02FailedTaskCd = new CountDownLatch(failedTaskNoList.size());
                for (BatTaskRun failedTask : failedTaskList) {
                    priFlag02PendingTaskThreadPool.submit(() -> {
                        try {
                            // Job都是小写,除了bak之外
                            String failedTaskNo = StringUtils.EMPTY;
                            if (failedTask.getTaskNo().contains("BAK")) {
                                failedTaskNo = failedTask.getTaskNo().replaceAll("BAK", "bak");
                            } else if (failedTask.getTaskNo().equals("CMIS020A") || failedTask.getTaskNo().equals("CMIS020B")
                                    || failedTask.getTaskNo().equals("CMIS020C") || failedTask.getTaskNo().equals("CMIS020D")
                                    || failedTask.getTaskNo().equals("CMIS020E") || failedTask.getTaskNo().equals("CMIS020F")
                                    || failedTask.getTaskNo().equals("CMIS020G") || failedTask.getTaskNo().equals("CMIS020H")
                                    || failedTask.getTaskNo().contains("CMIS417") || failedTask.getTaskNo().contains("CMIS418")
                                    || failedTask.getTaskNo().contains("CMIS425") || failedTask.getTaskNo().contains("CMIS431")) {
                                failedTaskNo = failedTask.getTaskNo().replaceAll("CMIS", "cmis");
                            } else if (failedTask.getTaskNo().equals("BATEND")) {
                                // 此处对BATEND任务单独处理
                                failedTaskNo = "batEnd";
                            } else {
                                failedTaskNo = failedTask.getTaskNo().toLowerCase();
                            }
                            String failedTaskName = failedTask.getTaskName();
                            try {
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用开始");
                                customJobLauncher.run((Job) applicationContext.getBean(failedTaskNo + "Job"), createJobParams(taskDate, failedTaskNo, false));
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用完成");
                            } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                                logger.error("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用异常，异常信息为:[" + e.getMessage() + "]");
                                throw BizException.error(null, EchEnum.ECH080014.key, "任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用异常，异常信息为:[" + e.getMessage() + "]");
                            }
                        } catch (Exception ex) {
                            logger.error("任务日期:[" + taskDate + "],任务编号:[" + failedTask.getTaskNo() + "],任务名称:[" + failedTask.getTaskName() + "]调用线程池异常，异常信息为:[" + ex.getMessage() + "]");
                        } finally {
                            priFlag02FailedTaskCd.countDown();
                        }
                    });
                }
            } catch (BizException e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage(e.getMessage());//任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]
                return cmisbatch0010RespDto;
            } catch (Exception e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage("任务日期:[" + taskDate + "重新调用异常，异常信息为:[" + e.getMessage() + "]");//"任务日期:[" + taskDate + "重新调用异常，异常信息为:[" + e.getMessage() + "]"
                return cmisbatch0010RespDto;
            }
        } else {
            try {
                // 待运行任务标志，当所有任务处理完成，跳出循环
                boolean flag = true;
                // 查询待运行的任务列表
                QueryModel btrlQm = new QueryModel();
                btrlQm.addCondition("taskDate", taskDate);
                btrlQm.addCondition("priFlag", BatEnums.PRI_FLAG_02.key);// 日终主任务(非零内评之后)
                btrlQm.addCondition("taskStatus", BatEnums.TASK_STATUS_000.key);
                logger.info("查询待运行的任务列表,请求参数为[{}]", JSON.toJSONString(btrlQm));
                java.util.List<BatTaskRun> pendingTaskList = batTaskRunService.selectPendingTask(btrlQm);
                logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATSTART_RUN_STEP.key, JobStepEnum.BATSTART_RUN_STEP.value, "查询待运行的任务列表数量为:[" + pendingTaskList.size() + "]");

                CountDownLatch priFlag02PendingTaskCd = new CountDownLatch(pendingTaskList.size());
                for (BatTaskRun pendingTask : pendingTaskList) {
                    priFlag02PendingTaskThreadPool.submit(() -> {
                        try {
                            // Job都是小写,除了bak之外
                            String pendingTaskNo = StringUtils.EMPTY;
                            if (pendingTask.getTaskNo().contains("BAK")) {
                                pendingTaskNo = pendingTask.getTaskNo().replaceAll("BAK", "bak");
                            } else if (pendingTask.getTaskNo().equals("CMIS020A") || pendingTask.getTaskNo().equals("CMIS020B")
                                    || pendingTask.getTaskNo().equals("CMIS020C") || pendingTask.getTaskNo().equals("CMIS020D")
                                    || pendingTask.getTaskNo().equals("CMIS020E") || pendingTask.getTaskNo().equals("CMIS020F")
                                    || pendingTask.getTaskNo().equals("CMIS020G") || pendingTask.getTaskNo().equals("CMIS020H")
                                    || pendingTask.getTaskNo().contains("CMIS417") || pendingTask.getTaskNo().contains("CMIS418")
                                    || pendingTask.getTaskNo().contains("CMIS425") || pendingTask.getTaskNo().contains("CMIS431")) {
                                pendingTaskNo = pendingTask.getTaskNo().replaceAll("CMIS", "cmis");
                            } else if (pendingTask.getTaskNo().equals("BATEND")) {
                                // 此处对BATEND任务单独处理
                                pendingTaskNo = "batEnd";
                            } else {
                                pendingTaskNo = pendingTask.getTaskNo().toLowerCase();
                            }
                            String pendingTaskName = pendingTask.getTaskName();
                            try {
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]开始调用开始开始调用开始");
                                customJobLauncher.run((Job) applicationContext.getBean(pendingTaskNo + "Job"), createJobParams(taskDate, pendingTaskNo, false));
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用完成");
                            } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                                logger.error("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]");
                                throw BizException.error(null, EchEnum.ECH080014.key, "任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]");
                            }
                        } catch (Exception ex) {
                            logger.error("任务日期:[" + taskDate + "],任务编号:[" + pendingTask.getTaskNo() + "],任务名称:[" + pendingTask.getTaskName() + "]调用线程池异常，异常信息为:[" + ex.getMessage() + "]");
                        } finally {
                            priFlag02PendingTaskCd.countDown();
                        }
                    });
                }

            } catch (BizException e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage(e.getMessage());//任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]
                return cmisbatch0010RespDto;
            } catch (Exception e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");//"任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]"
                return cmisbatch0010RespDto;
            }
        }

        return cmisbatch0010RespDto;
    }


    /**
     * 并发启动任务级别为3的任务
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/parallelRunByPriFlag03")
    public @ResponseBody
    Cmisbatch0010RespDto parallelRunByPriFlag03(String taskDate) throws Exception {
        Cmisbatch0010RespDto cmisbatch0010RespDto = new Cmisbatch0010RespDto();
        cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_S.key);//操作成功标志位
        cmisbatch0010RespDto.setOpMessage(DscmsEnum.OP_FLAG_S.value);
        // 根据任务日期和任务级别查询bat_task_run中是否有任务状态为执行失败的，如果有则重新发起；如果没有则直接启动
        boolean isFailedFlag = false;//是否存在失败任务标志
        QueryModel failedQueryModel = new QueryModel();
        failedQueryModel.addCondition("taskDate", taskDate);
        failedQueryModel.addCondition("priFlag", BatEnums.PRI_FLAG_03.key);// 日终三阶段(贷后管理风险分类)
        failedQueryModel.addCondition("taskStatus", BatEnums.TASK_STATUS_101.key);//执行失败
        logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表开始,请求参数为:[{}]", taskDate, BatEnums.PRI_FLAG_03.key, JSON.toJSONString(failedQueryModel));
        List<BatTaskRun> failedTaskList = batTaskRunService.selectFailedTaskList(failedQueryModel);
        logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表结束,响应参数为:[{}]", taskDate, BatEnums.PRI_FLAG_03.key, JSON.toJSONString(failedTaskList));

        if (CollectionUtils.nonEmpty(failedTaskList)) {
            isFailedFlag = true;
            logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表不为空，则存在失败任务标志,重新执行错误的任务", taskDate, BatEnums.PRI_FLAG_03.key);
        } else {
            isFailedFlag = false;
            logger.info("根据任务日期:[{}]和任务级别:[{}]查询bat_task_run中是否有任务状态为执行失败的列表为空，则不存在失败任务标志,正常发起", taskDate, BatEnums.PRI_FLAG_03.key);
        }

        if (isFailedFlag) {
            try {
                // 存在失败任务标志
                List<String> failedTaskNoList = failedTaskList.stream().map(failedTask -> {
                    String failedTaskNo = failedTask.getTaskNo();
                    return failedTaskNo;
                }).collect(Collectors.toList());
                String failedTaskNos = failedTaskNoList.stream().collect(Collectors.joining(","));
                logger.info("任务日期:[{}],任务级别:[{}]中重新执行的任务编号为:[{}]", taskDate, BatEnums.PRI_FLAG_01.key, failedTaskNos);
                CountDownLatch priFlag03FailedTaskCd = new CountDownLatch(failedTaskNoList.size());
                for (BatTaskRun failedTask : failedTaskList) {
                    priFlag03PendingTaskThreadPool.submit(() -> {
                        try {
                            // Job都是小写,除了bak之外
                            String failedTaskNo = StringUtils.EMPTY;
                            if (failedTask.getTaskNo().contains("BAK")) {
                                failedTaskNo = failedTask.getTaskNo().replaceAll("BAK", "bak");
                            } else if (failedTask.getTaskNo().equals("CMIS020A") || failedTask.getTaskNo().equals("CMIS020B")
                                    || failedTask.getTaskNo().equals("CMIS020C") || failedTask.getTaskNo().equals("CMIS020D")
                                    || failedTask.getTaskNo().equals("CMIS020E") || failedTask.getTaskNo().equals("CMIS020F")
                                    || failedTask.getTaskNo().equals("CMIS020G") || failedTask.getTaskNo().equals("CMIS020H")
                                    || failedTask.getTaskNo().contains("CMIS417") || failedTask.getTaskNo().contains("CMIS418")
                                    || failedTask.getTaskNo().contains("CMIS425") || failedTask.getTaskNo().contains("CMIS431")) {
                                failedTaskNo = failedTask.getTaskNo().replaceAll("CMIS", "cmis");
                            } else if (failedTask.getTaskNo().equals("BATEND")) {
                                // 此处对BATEND任务单独处理
                                failedTaskNo = "batEnd";
                            } else {
                                failedTaskNo = failedTask.getTaskNo().toLowerCase();
                            }
                            String failedTaskName = failedTask.getTaskName();
                            try {
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用开始");
                                customJobLauncher.run((Job) applicationContext.getBean(failedTaskNo + "Job"), createJobParams(taskDate, failedTaskNo, false));
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用完成");
                            } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                                logger.error("任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用异常，异常信息为:[" + e.getMessage() + "]");
                                throw BizException.error(null, EchEnum.ECH080014.key, "任务日期:[" + taskDate + "],任务编号:[" + failedTaskNo + "],任务名称:[" + failedTaskName + "]重新调用异常，异常信息为:[" + e.getMessage() + "]");
                            }
                        } catch (Exception ex) {
                            logger.error("任务日期:[" + taskDate + "],任务编号:[" + failedTask.getTaskNo() + "],任务名称:[" + failedTask.getTaskName() + "]调用线程池异常，异常信息为:[" + ex.getMessage() + "]");
                        } finally {
                            priFlag03FailedTaskCd.countDown();
                        }
                    });
                }
            } catch (BizException e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage(e.getMessage());//任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]
                return cmisbatch0010RespDto;
            } catch (Exception e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage("任务日期:[" + taskDate + "重新调用异常，异常信息为:[" + e.getMessage() + "]");//"任务日期:[" + taskDate + "重新调用异常，异常信息为:[" + e.getMessage() + "]"
                return cmisbatch0010RespDto;
            }
        } else {
            try {
                // 待运行任务标志，当所有任务处理完成，跳出循环
                boolean flag = true;
                // 查询待运行的任务列表
                QueryModel btrlQm = new QueryModel();
                btrlQm.addCondition("taskDate", taskDate);
                btrlQm.addCondition("priFlag", BatEnums.PRI_FLAG_03.key);// 日终三阶段(贷后管理风险分类)
                btrlQm.addCondition("taskStatus", BatEnums.TASK_STATUS_000.key);
                logger.info("查询待运行的任务列表,请求参数为[{}]", JSON.toJSONString(btrlQm));
                java.util.List<BatTaskRun> pendingTaskList = batTaskRunService.selectPendingTask(btrlQm);
                logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATSTART_RUN_STEP.key, JobStepEnum.BATSTART_RUN_STEP.value, "查询待运行的任务列表数量为:[" + pendingTaskList.size() + "]");

                CountDownLatch priFlag03PendingTaskCd = new CountDownLatch(pendingTaskList.size());
                for (BatTaskRun pendingTask : pendingTaskList) {
                    priFlag03PendingTaskThreadPool.submit(() -> {
                        try {
                            // Job都是小写,除了bak之外
                            String pendingTaskNo = StringUtils.EMPTY;
                            if (pendingTask.getTaskNo().contains("BAK")) {
                                pendingTaskNo = pendingTask.getTaskNo().replaceAll("BAK", "bak");
                            } else if (pendingTask.getTaskNo().equals("CMIS020A") || pendingTask.getTaskNo().equals("CMIS020B")
                                    || pendingTask.getTaskNo().equals("CMIS020C") || pendingTask.getTaskNo().equals("CMIS020D")
                                    || pendingTask.getTaskNo().equals("CMIS020E") || pendingTask.getTaskNo().equals("CMIS020F")
                                    || pendingTask.getTaskNo().equals("CMIS020G") || pendingTask.getTaskNo().equals("CMIS020H")
                                    || pendingTask.getTaskNo().contains("CMIS417") || pendingTask.getTaskNo().contains("CMIS418")
                                    || pendingTask.getTaskNo().contains("CMIS425") || pendingTask.getTaskNo().contains("CMIS431")) {
                                pendingTaskNo = pendingTask.getTaskNo().replaceAll("CMIS", "cmis");
                            } else if (pendingTask.getTaskNo().equals("BATEND")) {
                                // 此处对BATEND任务单独处理
                                pendingTaskNo = "batEnd";
                            } else {
                                pendingTaskNo = pendingTask.getTaskNo().toLowerCase();
                            }
                            String pendingTaskName = pendingTask.getTaskName();
                            try {
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]开始调用开始开始调用开始");
                                customJobLauncher.run((Job) applicationContext.getBean(pendingTaskNo + "Job"), createJobParams(taskDate, pendingTaskNo, false));
                                logger.info("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用完成");
                            } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
                                logger.error("任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]");
                                throw BizException.error(null, EchEnum.ECH080014.key, "任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]");
                            }
                        } catch (Exception ex) {
                            logger.error("任务日期:[" + taskDate + "],任务编号:[" + pendingTask.getTaskNo() + "],任务名称:[" + pendingTask.getTaskName() + "]调用线程池异常，异常信息为:[" + ex.getMessage() + "]");
                        } finally {
                            priFlag03PendingTaskCd.countDown();
                        }
                    });
                }

            } catch (BizException e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage(e.getMessage());//任务日期:[" + taskDate + "],任务编号:[" + pendingTaskNo + "],任务名称:[" + pendingTaskName + "]调用异常，异常信息为:[" + e.getMessage() + "]
                return cmisbatch0010RespDto;
            } catch (Exception e) {
                logger.error("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");
                cmisbatch0010RespDto.setOpFlag(DscmsEnum.OP_FLAG_F.key);
                cmisbatch0010RespDto.setOpMessage("任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]");//"任务日期:[" + taskDate + "调用异常，异常信息为:[" + e.getMessage() + "]"
                return cmisbatch0010RespDto;
            }
        }
        return cmisbatch0010RespDto;
    }


    /**
     * 生成 JobParameter
     *
     * @param openDay 跑批日期
     * @param restart 是否需要重跑 需要则加时间戳
     * @return
     */
    private static JobParameters createJobParams(String openDay, String taskNo, boolean... restart) {
        JobParametersBuilder builder = new JobParametersBuilder().addString("openDay", openDay).addString("taskNo", taskNo);
        for (boolean arg : restart) {
            if (arg) {
                builder.addString("date", DateUtils.getCurrTimestamp().toString());
            }
        }
        return builder.toJobParameters();
    }
}
