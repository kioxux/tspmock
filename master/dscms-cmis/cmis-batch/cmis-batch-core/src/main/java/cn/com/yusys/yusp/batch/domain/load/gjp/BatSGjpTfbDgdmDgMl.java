/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.gjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGjpTfbDgdmDgMl
 * @类描述: bat_s_gjp_tfb_dgdm_dg_ml数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-06 14:10:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_gjp_tfb_dgdm_dg_ml")
public class BatSGjpTfbDgdmDgMl extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 反担保保函余额币别 **/
	@Column(name = "LG_GURANT_CCY", unique = false, nullable = false, length = 3)
	private String lgGurantCcy;
	
	/** 发送方式 **/
	@Column(name = "LG_ISS_FLG", unique = false, nullable = true, length = 8)
	private String lgIssFlg;
	
	/** 开立模式 **/
	@Column(name = "LG_ISS_TYPE", unique = false, nullable = true, length = 8)
	private String lgIssType;
	
	/** 保函最大索赔金额 **/
	@Column(name = "LG_MAX_CLM_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lgMaxClmAmt;
	
	/** 最大索赔余额 **/
	@Column(name = "LG_MAX_CLM_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lgMaxClmBal;
	
	/** 保函最大索赔币别 **/
	@Column(name = "LG_MAX_CLM_CCY", unique = false, nullable = true, length = 3)
	private String lgMaxClmCcy;
	
	/** 保函格式 **/
	@Column(name = "LG_MODE", unique = false, nullable = true, length = 8)
	private String lgMode;
	
	/** 保函其他编号 **/
	@Column(name = "LG_OTHER", unique = false, nullable = true, length = 35)
	private String lgOther;
	
	/** 他行保函 **/
	@Column(name = "LG_OTH_NO", unique = false, nullable = true, length = 32)
	private String lgOthNo;
	
	/** 保函性质 **/
	@Column(name = "LG_PROP", unique = false, nullable = true, length = 8)
	private String lgProp;
	
	/** 保函业务编号 **/
	@Column(name = "LG_REF_NO", unique = false, nullable = true, length = 32)
	private String lgRefNo;
	
	/** 出具代表人 **/
	@Column(name = "LG_RESP", unique = false, nullable = true, length = 140)
	private String lgResp;
	
	/** 交易类型 **/
	@Column(name = "LG_TX_TYPE", unique = false, nullable = true, length = 8)
	private String lgTxType;
	
	/** 保函类型 **/
	@Column(name = "LG_TYPE", unique = false, nullable = true, length = 8)
	private String lgType;
	
	/** 出具单位 **/
	@Column(name = "LG_UNIT", unique = false, nullable = true, length = 140)
	private String lgUnit;
	
	/** 金额一 **/
	@Column(name = "MIX_AMT1", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal mixAmt1;
	
	/** 金额二 **/
	@Column(name = "MIX_AMT2", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal mixAmt2;
	
	/** 币种一 **/
	@Column(name = "MIX_CCY1", unique = false, nullable = true, length = 3)
	private String mixCcy1;
	
	/** 币种二 **/
	@Column(name = "MIX_CCY2", unique = false, nullable = true, length = 3)
	private String mixCcy2;
	
	/** 混合币别 **/
	@Column(name = "MIX_CCY_FLG", unique = false, nullable = true, length = 3)
	private String mixCcyFlg;
	
	/** 混合方式 **/
	@Column(name = "MIX_TYPE", unique = false, nullable = true, length = 8)
	private String mixType;
	
	/** 短装比例 **/
	@Column(name = "NEG_TLRNC", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal negTlrnc;
	
	/** 操作员备注 **/
	@Column(name = "OPR_DESC", unique = false, nullable = true, length = 256)
	private String oprDesc;
	
	/** 他行开立编号 **/
	@Column(name = "OTH_ISS_NO", unique = false, nullable = true, length = 32)
	private String othIssNo;
	
	/** 已赔付金额 **/
	@Column(name = "PAY_TTL_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal payTtlAmt;
	
	/** 金额浮动比例 **/
	@Column(name = "POS_PCT", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal posPct;
	
	/** 登记日期 **/
	@Column(name = "REG_DT", unique = false, nullable = true, length = 8)
	private String regDt;
	
	/** 循环次数 **/
	@Column(name = "RVL_TIMES", unique = false, nullable = true, length = 34)
	private java.math.BigDecimal rvlTimes;
	
	/** 循环类型 **/
	@Column(name = "RVL_TYPE", unique = false, nullable = true, length = 8)
	private String rvlType;
	
	/** 外汇局备案号码 **/
	@Column(name = "SAFE_NO", unique = false, nullable = true, length = 32)
	private String safeNo;
	
	/** 备案性质 **/
	@Column(name = "SAFE_TYPE", unique = false, nullable = true, length = 140)
	private String safeType;
	
	/** 信贷批准出账日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 8)
	private String startDate;
	
	/** 转递行BIC **/
	@Column(name = "TRF_BK_BIC", unique = false, nullable = true, length = 16)
	private String trfBkBic;
	
	/** 转入行ID **/
	@Column(name = "TRM_TRF_BK_BIC", unique = false, nullable = true, length = 16)
	private String trmTrfBkBic;
	
	/** 撤销申请次数 **/
	@Column(name = "TX_APP_TIMES", unique = false, nullable = true, length = 34)
	private java.math.BigDecimal txAppTimes;
	
	/** 保函档案编号 **/
	@Column(name = "LG_REG_NO", unique = false, nullable = true, length = 32)
	private String lgRegNo;
	
	/** 业务流水号 **/
	@Column(name = "SYS_REF_NO", unique = false, nullable = true, length = 32)
	private String sysRefNo;
	
	/** 关联流水号 **/
	@Column(name = "SYS_REL_NO", unique = false, nullable = true, length = 32)
	private String sysRelNo;
	
	/** 系统实体编号 **/
	@Column(name = "SYS_ENTY_ID", unique = false, nullable = true, length = 32)
	private String sysEntyId;
	
	/** 系统删除标志 **/
	@Column(name = "SYS_DEL_FLG", unique = false, nullable = true, length = 1)
	private String sysDelFlg;
	
	/** 创建人 **/
	@Column(name = "SYS_CRT_USER", unique = false, nullable = true, length = 32)
	private String sysCrtUser;
	
	/** 系统创建时间 **/
	@Column(name = "SYS_CRT_DT", unique = false, nullable = true, length = 20)
	private String sysCrtDt;
	
	/** 系统修改用户 **/
	@Column(name = "SYS_MODIFY_USER", unique = false, nullable = true, length = 32)
	private String sysModifyUser;
	
	/** 系统修改时间 **/
	@Column(name = "SYS_MODIFY_DT", unique = false, nullable = true, length = 20)
	private String sysModifyDt;
	
	/** 业务主关联流水号 **/
	@Column(name = "SYS_MAIN_REL_NO", unique = false, nullable = true, length = 32)
	private String sysMainRelNo;
	
	/** 附加金额 **/
	@Column(name = "ADT_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal adtAmt;
	
	/** 附加币种 **/
	@Column(name = "ADT_CCY", unique = false, nullable = true, length = 3)
	private String adtCcy;
	
	/** 第二通知行BIC **/
	@Column(name = "ADV_BK_2ND_BIC", unique = false, nullable = true, length = 16)
	private String advBk2ndBic;
	
	/** 通知行BIC **/
	@Column(name = "ADV_BK_BIC", unique = false, nullable = true, length = 16)
	private String advBkBic;
	
	/** 客户代码 **/
	@Column(name = "CST_ID", unique = false, nullable = true, length = 32)
	private String cstId;
	
	/** 申请人性质 **/
	@Column(name = "CST_PROP", unique = false, nullable = true, length = 512)
	private String cstProp;
	
	/** 生效事件 **/
	@Column(name = "EFFCT_CASE", unique = false, nullable = true, length = 140)
	private String effctCase;
	
	/** 生效日期 **/
	@Column(name = "EFFCT_DT", unique = false, nullable = true, length = 8)
	private String effctDt;
	
	/** 到期事件 **/
	@Column(name = "EXPIRY_CASE", unique = false, nullable = true, length = 32)
	private String expiryCase;
	
	/** 到期日 **/
	@Column(name = "EXPIRY_DT", unique = false, nullable = true, length = 8)
	private String expiryDt;
	
	/** 费用负担 **/
	@Column(name = "FEE_BY", unique = false, nullable = true, length = 256)
	private String feeBy;
	
	/** 收费起始日 **/
	@Column(name = "FEE_START_DT", unique = false, nullable = true, length = 8)
	private String feeStartDt;
	
	/** 被担保人名称 **/
	@Column(name = "GURANT_CHN_NM", unique = false, nullable = true, length = 140)
	private String gurantChnNm;
	
	/** 被担保人国别 **/
	@Column(name = "GURANT_CNT_CDE", unique = false, nullable = true, length = 8)
	private String gurantCntCde;
	
	/** 开证日期 **/
	@Column(name = "ISS_DT", unique = false, nullable = true, length = 8)
	private String issDt;
	
	/** 保函金额 **/
	@Column(name = "LG_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lgAmt;
	
	/** 保函余额 **/
	@Column(name = "LG_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lgBal;
	
	/** 进口保函撤销标识 **/
	@Column(name = "LG_CAC_FLG", unique = false, nullable = true, length = 1)
	private String lgCacFlg;
	
	/** 进口保函事件步骤 **/
	@Column(name = "LG_CASE_STEP", unique = false, nullable = true, length = 8)
	private String lgCaseStep;
	
	/** 保函币种 **/
	@Column(name = "LG_CCY", unique = false, nullable = true, length = 3)
	private String lgCcy;
	
	/** 国内保函 **/
	@Column(name = "LG_CIVIL_FLG", unique = false, nullable = true, length = 1)
	private String lgCivilFlg;
	
	/** 进口保函闭卷标识 **/
	@Column(name = "LG_CLS_FLG", unique = false, nullable = true, length = 1)
	private String lgClsFlg;
	
	/** 确认 **/
	@Column(name = "LG_CNF_FLG", unique = false, nullable = true, length = 1)
	private String lgCnfFlg;
	
	/** 操作类型 **/
	@Column(name = "LG_COF_PROC_TYPE", unique = false, nullable = true, length = 8)
	private String lgCofProcType;
	
	/** 同业额度占用金额 **/
	@Column(name = "LG_CONSN_LIMIT_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lgConsnLimitAmt;
	
	/** 同业额度占用币别 **/
	@Column(name = "LG_CONSN_LIMIT_CCY", unique = false, nullable = true, length = 3)
	private String lgConsnLimitCcy;
	
	/** 浮附标识 **/
	@Column(name = "LG_FLAG", unique = false, nullable = true, length = 1)
	private String lgFlag;
	
	/** 通知行业务编号 **/
	@Column(name = "ADV_BK_REF_NO", unique = false, nullable = true, length = 32)
	private String advBkRefNo;
	
	/** 修改次数 **/
	@Column(name = "AMD_TIMES", unique = false, nullable = true, length = 34)
	private java.math.BigDecimal amdTimes;
	
	/** 更改次数 **/
	@Column(name = "AMD_TIMES_TTL", unique = false, nullable = true, length = 34)
	private java.math.BigDecimal amdTimesTtl;
	
	/** 适用惯例类型 **/
	@Column(name = "APP_RULES_TYPE", unique = false, nullable = true, length = 8)
	private String appRulesType;
	
	/** 国别/地区 **/
	@Column(name = "BEN_CNT_CDE", unique = false, nullable = true, length = 3)
	private String benCntCde;
	
	/** 受益人英文名 **/
	@Column(name = "BEN_ENG_NM", unique = false, nullable = true, length = 140)
	private String benEngNm;
	
	/** 业务归属 **/
	@Column(name = "BIZ_BR_CDE", unique = false, nullable = true, length = 8)
	private String bizBrCde;
	
	/** 索汇余额 **/
	@Column(name = "CLM_BAL_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal clmBalAmt;
	
	/** 来单索赔余额 **/
	@Column(name = "CLM_TTL_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal clmTtlBal;
	
	/** 保函注销日期 **/
	@Column(name = "CLS_DT", unique = false, nullable = true, length = 8)
	private String clsDt;
	
	/** 信贷出账号 **/
	@Column(name = "CMS_REF_NO", unique = false, nullable = true, length = 100)
	private String cmsRefNo;
	
	/** 合同金额 **/
	@Column(name = "CNTRCT_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal cntrctAmt;
	
	/** 合同币种 **/
	@Column(name = "CNTRCT_CCY", unique = false, nullable = true, length = 3)
	private String cntrctCcy;
	
	/** 合同号 **/
	@Column(name = "CNTRCT_NO", unique = false, nullable = true, length = 100)
	private String cntrctNo;
	
	/** 委托行BIC **/
	@Column(name = "CONSN_BK_BIC", unique = false, nullable = true, length = 16)
	private String consnBkBic;
	
	/** 委托行编号 **/
	@Column(name = "CONSN_BK_NO", unique = false, nullable = true, length = 32)
	private String consnBkNo;
	
	/** 委托行反担保生效日 **/
	@Column(name = "CONSN_EFFCT_DT", unique = false, nullable = true, length = 8)
	private String consnEffctDt;
	
	/** 代付到期日 **/
	@Column(name = "CONSN_EXPIRY_DT", unique = false, nullable = true, length = 8)
	private String consnExpiryDt;
	
	/** 委托行反担保编号 **/
	@Column(name = "CONSN_GURANT_NO", unique = false, nullable = true, length = 32)
	private String consnGurantNo;
	
	/** 委托开立保函金额 **/
	@Column(name = "CONSN_ISS_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal consnIssAmt;
	
	/** 委托开立保函币别 **/
	@Column(name = "CONSN_ISS_CCY", unique = false, nullable = true, length = 3)
	private String consnIssCcy;
	
	/** 最大反担保金额 **/
	@Column(name = "CONSN_MAX_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal consnMaxAmt;
	
	/** 最大反担保币别 **/
	@Column(name = "CONSN_MAX_CCY", unique = false, nullable = true, length = 3)
	private String consnMaxCcy;
	
	/** 更改次数 **/
	@Column(name = "CORR_AMD_TIMES", unique = false, nullable = true, length = 8)
	private String corrAmdTimes;
	
	/** 更正次数 **/
	@Column(name = "CORR_TIMES", unique = false, nullable = true, length = 34)
	private java.math.BigDecimal corrTimes;
	
	/** 授权金额 **/
	@Column(name = "CREDIT_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal creditAmt;
	
	/** 信贷出账币种 **/
	@Column(name = "CREDIT_CCY", unique = false, nullable = true, length = 3)
	private String creditCcy;
	
	/** 信贷出账到期日 **/
	@Column(name = "CREDIT_EXPIRY_DT", unique = false, nullable = true, length = 8)
	private String creditExpiryDt;
	
	/** 信贷保函编号 **/
	@Column(name = "CREDIT_LG_REF_NO", unique = false, nullable = true, length = 32)
	private String creditLgRefNo;
	
	/** 跨境人民币标识 **/
	@Column(name = "CROSS_RMB_FLG", unique = false, nullable = true, length = 1)
	private String crossRmbFlg;
	
	/** 客户中文名（申请人） **/
	@Column(name = "CST_CHN_NM", unique = false, nullable = true, length = 140)
	private String cstChnNm;
	
	/** 申请人英文名称 **/
	@Column(name = "CST_ENG_NM", unique = false, nullable = true, length = 140)
	private String cstEngNm;
	
	/** 反担保保函余额 **/
	@Column(name = "LG_GURANT_BAL", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal lgGurantBal;
	
	/** 开立方式 **/
	@Column(name = "ISS_FLG", unique = false, nullable = true, length = 8)
	private String issFlg;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 256)
	private String remark;
	
	/** 其他原因 **/
	@Column(name = "OTHERS_CAUSE", unique = false, nullable = true, length = 100)
	private String othersCause;
	
	/** 受益人代码 **/
	@Column(name = "BEN_CODE", unique = false, nullable = true, length = 32)
	private String benCode;
	
	/** 被担保人代码 **/
	@Column(name = "GURANT_CODE", unique = false, nullable = true, length = 32)
	private String gurantCode;
	
	/** 受益人类型 **/
	@Column(name = "BEN_TYPE", unique = false, nullable = true, length = 8)
	private String benType;
	
	/** 被担保人类型 **/
	@Column(name = "GURANT_TYPE1", unique = false, nullable = true, length = 8)
	private String gurantType1;
	
	/** 操作类型 **/
	@Column(name = "LG_PROC_TYPE", unique = false, nullable = true, length = 8)
	private String lgProcType;
	
	/** 操作类型 **/
	@Column(name = "DEAL_TYPE", unique = false, nullable = true, length = 8)
	private String dealType;
	
	/** 申请人类型 **/
	@Column(name = "APP_TYPE", unique = false, nullable = true, length = 8)
	private String appType;
	
	/** 是否接受 **/
	@Column(name = "ACK_FLG", unique = false, nullable = true, length = 8)
	private String ackFlg;
	
	/** 其他规则 **/
	@Column(name = "APP_RULES_OTH", unique = false, nullable = true, length = 140)
	private String appRulesOth;
	
	/** 项目名称 **/
	@Column(name = "PROJ_NAME", unique = false, nullable = true, length = 140)
	private String projName;
	
	/** 项目币种 **/
	@Column(name = "PROJ_CCY", unique = false, nullable = true, length = 3)
	private String projCcy;
	
	/** 项目金额 **/
	@Column(name = "PROJ_AMT", unique = false, nullable = true, length = 18)
	private java.math.BigDecimal projAmt;
	
	/** 保函线上线下标志 **/
	@Column(name = "BH_FLAG", unique = false, nullable = true, length = 1)
	private String bhFlag;
	
	
	/**
	 * @param lgGurantCcy
	 */
	public void setLgGurantCcy(String lgGurantCcy) {
		this.lgGurantCcy = lgGurantCcy;
	}
	
    /**
     * @return lgGurantCcy
     */
	public String getLgGurantCcy() {
		return this.lgGurantCcy;
	}
	
	/**
	 * @param lgIssFlg
	 */
	public void setLgIssFlg(String lgIssFlg) {
		this.lgIssFlg = lgIssFlg;
	}
	
    /**
     * @return lgIssFlg
     */
	public String getLgIssFlg() {
		return this.lgIssFlg;
	}
	
	/**
	 * @param lgIssType
	 */
	public void setLgIssType(String lgIssType) {
		this.lgIssType = lgIssType;
	}
	
    /**
     * @return lgIssType
     */
	public String getLgIssType() {
		return this.lgIssType;
	}
	
	/**
	 * @param lgMaxClmAmt
	 */
	public void setLgMaxClmAmt(java.math.BigDecimal lgMaxClmAmt) {
		this.lgMaxClmAmt = lgMaxClmAmt;
	}
	
    /**
     * @return lgMaxClmAmt
     */
	public java.math.BigDecimal getLgMaxClmAmt() {
		return this.lgMaxClmAmt;
	}
	
	/**
	 * @param lgMaxClmBal
	 */
	public void setLgMaxClmBal(java.math.BigDecimal lgMaxClmBal) {
		this.lgMaxClmBal = lgMaxClmBal;
	}
	
    /**
     * @return lgMaxClmBal
     */
	public java.math.BigDecimal getLgMaxClmBal() {
		return this.lgMaxClmBal;
	}
	
	/**
	 * @param lgMaxClmCcy
	 */
	public void setLgMaxClmCcy(String lgMaxClmCcy) {
		this.lgMaxClmCcy = lgMaxClmCcy;
	}
	
    /**
     * @return lgMaxClmCcy
     */
	public String getLgMaxClmCcy() {
		return this.lgMaxClmCcy;
	}
	
	/**
	 * @param lgMode
	 */
	public void setLgMode(String lgMode) {
		this.lgMode = lgMode;
	}
	
    /**
     * @return lgMode
     */
	public String getLgMode() {
		return this.lgMode;
	}
	
	/**
	 * @param lgOther
	 */
	public void setLgOther(String lgOther) {
		this.lgOther = lgOther;
	}
	
    /**
     * @return lgOther
     */
	public String getLgOther() {
		return this.lgOther;
	}
	
	/**
	 * @param lgOthNo
	 */
	public void setLgOthNo(String lgOthNo) {
		this.lgOthNo = lgOthNo;
	}
	
    /**
     * @return lgOthNo
     */
	public String getLgOthNo() {
		return this.lgOthNo;
	}
	
	/**
	 * @param lgProp
	 */
	public void setLgProp(String lgProp) {
		this.lgProp = lgProp;
	}
	
    /**
     * @return lgProp
     */
	public String getLgProp() {
		return this.lgProp;
	}
	
	/**
	 * @param lgRefNo
	 */
	public void setLgRefNo(String lgRefNo) {
		this.lgRefNo = lgRefNo;
	}
	
    /**
     * @return lgRefNo
     */
	public String getLgRefNo() {
		return this.lgRefNo;
	}
	
	/**
	 * @param lgResp
	 */
	public void setLgResp(String lgResp) {
		this.lgResp = lgResp;
	}
	
    /**
     * @return lgResp
     */
	public String getLgResp() {
		return this.lgResp;
	}
	
	/**
	 * @param lgTxType
	 */
	public void setLgTxType(String lgTxType) {
		this.lgTxType = lgTxType;
	}
	
    /**
     * @return lgTxType
     */
	public String getLgTxType() {
		return this.lgTxType;
	}
	
	/**
	 * @param lgType
	 */
	public void setLgType(String lgType) {
		this.lgType = lgType;
	}
	
    /**
     * @return lgType
     */
	public String getLgType() {
		return this.lgType;
	}
	
	/**
	 * @param lgUnit
	 */
	public void setLgUnit(String lgUnit) {
		this.lgUnit = lgUnit;
	}
	
    /**
     * @return lgUnit
     */
	public String getLgUnit() {
		return this.lgUnit;
	}
	
	/**
	 * @param mixAmt1
	 */
	public void setMixAmt1(java.math.BigDecimal mixAmt1) {
		this.mixAmt1 = mixAmt1;
	}
	
    /**
     * @return mixAmt1
     */
	public java.math.BigDecimal getMixAmt1() {
		return this.mixAmt1;
	}
	
	/**
	 * @param mixAmt2
	 */
	public void setMixAmt2(java.math.BigDecimal mixAmt2) {
		this.mixAmt2 = mixAmt2;
	}
	
    /**
     * @return mixAmt2
     */
	public java.math.BigDecimal getMixAmt2() {
		return this.mixAmt2;
	}
	
	/**
	 * @param mixCcy1
	 */
	public void setMixCcy1(String mixCcy1) {
		this.mixCcy1 = mixCcy1;
	}
	
    /**
     * @return mixCcy1
     */
	public String getMixCcy1() {
		return this.mixCcy1;
	}
	
	/**
	 * @param mixCcy2
	 */
	public void setMixCcy2(String mixCcy2) {
		this.mixCcy2 = mixCcy2;
	}
	
    /**
     * @return mixCcy2
     */
	public String getMixCcy2() {
		return this.mixCcy2;
	}
	
	/**
	 * @param mixCcyFlg
	 */
	public void setMixCcyFlg(String mixCcyFlg) {
		this.mixCcyFlg = mixCcyFlg;
	}
	
    /**
     * @return mixCcyFlg
     */
	public String getMixCcyFlg() {
		return this.mixCcyFlg;
	}
	
	/**
	 * @param mixType
	 */
	public void setMixType(String mixType) {
		this.mixType = mixType;
	}
	
    /**
     * @return mixType
     */
	public String getMixType() {
		return this.mixType;
	}
	
	/**
	 * @param negTlrnc
	 */
	public void setNegTlrnc(java.math.BigDecimal negTlrnc) {
		this.negTlrnc = negTlrnc;
	}
	
    /**
     * @return negTlrnc
     */
	public java.math.BigDecimal getNegTlrnc() {
		return this.negTlrnc;
	}
	
	/**
	 * @param oprDesc
	 */
	public void setOprDesc(String oprDesc) {
		this.oprDesc = oprDesc;
	}
	
    /**
     * @return oprDesc
     */
	public String getOprDesc() {
		return this.oprDesc;
	}
	
	/**
	 * @param othIssNo
	 */
	public void setOthIssNo(String othIssNo) {
		this.othIssNo = othIssNo;
	}
	
    /**
     * @return othIssNo
     */
	public String getOthIssNo() {
		return this.othIssNo;
	}
	
	/**
	 * @param payTtlAmt
	 */
	public void setPayTtlAmt(java.math.BigDecimal payTtlAmt) {
		this.payTtlAmt = payTtlAmt;
	}
	
    /**
     * @return payTtlAmt
     */
	public java.math.BigDecimal getPayTtlAmt() {
		return this.payTtlAmt;
	}
	
	/**
	 * @param posPct
	 */
	public void setPosPct(java.math.BigDecimal posPct) {
		this.posPct = posPct;
	}
	
    /**
     * @return posPct
     */
	public java.math.BigDecimal getPosPct() {
		return this.posPct;
	}
	
	/**
	 * @param regDt
	 */
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
    /**
     * @return regDt
     */
	public String getRegDt() {
		return this.regDt;
	}
	
	/**
	 * @param rvlTimes
	 */
	public void setRvlTimes(java.math.BigDecimal rvlTimes) {
		this.rvlTimes = rvlTimes;
	}
	
    /**
     * @return rvlTimes
     */
	public java.math.BigDecimal getRvlTimes() {
		return this.rvlTimes;
	}
	
	/**
	 * @param rvlType
	 */
	public void setRvlType(String rvlType) {
		this.rvlType = rvlType;
	}
	
    /**
     * @return rvlType
     */
	public String getRvlType() {
		return this.rvlType;
	}
	
	/**
	 * @param safeNo
	 */
	public void setSafeNo(String safeNo) {
		this.safeNo = safeNo;
	}
	
    /**
     * @return safeNo
     */
	public String getSafeNo() {
		return this.safeNo;
	}
	
	/**
	 * @param safeType
	 */
	public void setSafeType(String safeType) {
		this.safeType = safeType;
	}
	
    /**
     * @return safeType
     */
	public String getSafeType() {
		return this.safeType;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param trfBkBic
	 */
	public void setTrfBkBic(String trfBkBic) {
		this.trfBkBic = trfBkBic;
	}
	
    /**
     * @return trfBkBic
     */
	public String getTrfBkBic() {
		return this.trfBkBic;
	}
	
	/**
	 * @param trmTrfBkBic
	 */
	public void setTrmTrfBkBic(String trmTrfBkBic) {
		this.trmTrfBkBic = trmTrfBkBic;
	}
	
    /**
     * @return trmTrfBkBic
     */
	public String getTrmTrfBkBic() {
		return this.trmTrfBkBic;
	}
	
	/**
	 * @param txAppTimes
	 */
	public void setTxAppTimes(java.math.BigDecimal txAppTimes) {
		this.txAppTimes = txAppTimes;
	}
	
    /**
     * @return txAppTimes
     */
	public java.math.BigDecimal getTxAppTimes() {
		return this.txAppTimes;
	}
	
	/**
	 * @param lgRegNo
	 */
	public void setLgRegNo(String lgRegNo) {
		this.lgRegNo = lgRegNo;
	}
	
    /**
     * @return lgRegNo
     */
	public String getLgRegNo() {
		return this.lgRegNo;
	}
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param sysRefNo
	 */
	public void setSysRefNo(String sysRefNo) {
		this.sysRefNo = sysRefNo;
	}
	
    /**
     * @return sysRefNo
     */
	public String getSysRefNo() {
		return this.sysRefNo;
	}
	
	/**
	 * @param sysRelNo
	 */
	public void setSysRelNo(String sysRelNo) {
		this.sysRelNo = sysRelNo;
	}
	
    /**
     * @return sysRelNo
     */
	public String getSysRelNo() {
		return this.sysRelNo;
	}
	
	/**
	 * @param sysEntyId
	 */
	public void setSysEntyId(String sysEntyId) {
		this.sysEntyId = sysEntyId;
	}
	
    /**
     * @return sysEntyId
     */
	public String getSysEntyId() {
		return this.sysEntyId;
	}
	
	/**
	 * @param sysDelFlg
	 */
	public void setSysDelFlg(String sysDelFlg) {
		this.sysDelFlg = sysDelFlg;
	}
	
    /**
     * @return sysDelFlg
     */
	public String getSysDelFlg() {
		return this.sysDelFlg;
	}
	
	/**
	 * @param sysCrtUser
	 */
	public void setSysCrtUser(String sysCrtUser) {
		this.sysCrtUser = sysCrtUser;
	}
	
    /**
     * @return sysCrtUser
     */
	public String getSysCrtUser() {
		return this.sysCrtUser;
	}
	
	/**
	 * @param sysCrtDt
	 */
	public void setSysCrtDt(String sysCrtDt) {
		this.sysCrtDt = sysCrtDt;
	}
	
    /**
     * @return sysCrtDt
     */
	public String getSysCrtDt() {
		return this.sysCrtDt;
	}
	
	/**
	 * @param sysModifyUser
	 */
	public void setSysModifyUser(String sysModifyUser) {
		this.sysModifyUser = sysModifyUser;
	}
	
    /**
     * @return sysModifyUser
     */
	public String getSysModifyUser() {
		return this.sysModifyUser;
	}
	
	/**
	 * @param sysModifyDt
	 */
	public void setSysModifyDt(String sysModifyDt) {
		this.sysModifyDt = sysModifyDt;
	}
	
    /**
     * @return sysModifyDt
     */
	public String getSysModifyDt() {
		return this.sysModifyDt;
	}
	
	/**
	 * @param sysMainRelNo
	 */
	public void setSysMainRelNo(String sysMainRelNo) {
		this.sysMainRelNo = sysMainRelNo;
	}
	
    /**
     * @return sysMainRelNo
     */
	public String getSysMainRelNo() {
		return this.sysMainRelNo;
	}
	
	/**
	 * @param adtAmt
	 */
	public void setAdtAmt(java.math.BigDecimal adtAmt) {
		this.adtAmt = adtAmt;
	}
	
    /**
     * @return adtAmt
     */
	public java.math.BigDecimal getAdtAmt() {
		return this.adtAmt;
	}
	
	/**
	 * @param adtCcy
	 */
	public void setAdtCcy(String adtCcy) {
		this.adtCcy = adtCcy;
	}
	
    /**
     * @return adtCcy
     */
	public String getAdtCcy() {
		return this.adtCcy;
	}
	
	/**
	 * @param advBk2ndBic
	 */
	public void setAdvBk2ndBic(String advBk2ndBic) {
		this.advBk2ndBic = advBk2ndBic;
	}
	
    /**
     * @return advBk2ndBic
     */
	public String getAdvBk2ndBic() {
		return this.advBk2ndBic;
	}
	
	/**
	 * @param advBkBic
	 */
	public void setAdvBkBic(String advBkBic) {
		this.advBkBic = advBkBic;
	}
	
    /**
     * @return advBkBic
     */
	public String getAdvBkBic() {
		return this.advBkBic;
	}
	
	/**
	 * @param cstId
	 */
	public void setCstId(String cstId) {
		this.cstId = cstId;
	}
	
    /**
     * @return cstId
     */
	public String getCstId() {
		return this.cstId;
	}
	
	/**
	 * @param cstProp
	 */
	public void setCstProp(String cstProp) {
		this.cstProp = cstProp;
	}
	
    /**
     * @return cstProp
     */
	public String getCstProp() {
		return this.cstProp;
	}
	
	/**
	 * @param effctCase
	 */
	public void setEffctCase(String effctCase) {
		this.effctCase = effctCase;
	}
	
    /**
     * @return effctCase
     */
	public String getEffctCase() {
		return this.effctCase;
	}
	
	/**
	 * @param effctDt
	 */
	public void setEffctDt(String effctDt) {
		this.effctDt = effctDt;
	}
	
    /**
     * @return effctDt
     */
	public String getEffctDt() {
		return this.effctDt;
	}
	
	/**
	 * @param expiryCase
	 */
	public void setExpiryCase(String expiryCase) {
		this.expiryCase = expiryCase;
	}
	
    /**
     * @return expiryCase
     */
	public String getExpiryCase() {
		return this.expiryCase;
	}
	
	/**
	 * @param expiryDt
	 */
	public void setExpiryDt(String expiryDt) {
		this.expiryDt = expiryDt;
	}
	
    /**
     * @return expiryDt
     */
	public String getExpiryDt() {
		return this.expiryDt;
	}
	
	/**
	 * @param feeBy
	 */
	public void setFeeBy(String feeBy) {
		this.feeBy = feeBy;
	}
	
    /**
     * @return feeBy
     */
	public String getFeeBy() {
		return this.feeBy;
	}
	
	/**
	 * @param feeStartDt
	 */
	public void setFeeStartDt(String feeStartDt) {
		this.feeStartDt = feeStartDt;
	}
	
    /**
     * @return feeStartDt
     */
	public String getFeeStartDt() {
		return this.feeStartDt;
	}
	
	/**
	 * @param gurantChnNm
	 */
	public void setGurantChnNm(String gurantChnNm) {
		this.gurantChnNm = gurantChnNm;
	}
	
    /**
     * @return gurantChnNm
     */
	public String getGurantChnNm() {
		return this.gurantChnNm;
	}
	
	/**
	 * @param gurantCntCde
	 */
	public void setGurantCntCde(String gurantCntCde) {
		this.gurantCntCde = gurantCntCde;
	}
	
    /**
     * @return gurantCntCde
     */
	public String getGurantCntCde() {
		return this.gurantCntCde;
	}
	
	/**
	 * @param issDt
	 */
	public void setIssDt(String issDt) {
		this.issDt = issDt;
	}
	
    /**
     * @return issDt
     */
	public String getIssDt() {
		return this.issDt;
	}
	
	/**
	 * @param lgAmt
	 */
	public void setLgAmt(java.math.BigDecimal lgAmt) {
		this.lgAmt = lgAmt;
	}
	
    /**
     * @return lgAmt
     */
	public java.math.BigDecimal getLgAmt() {
		return this.lgAmt;
	}
	
	/**
	 * @param lgBal
	 */
	public void setLgBal(java.math.BigDecimal lgBal) {
		this.lgBal = lgBal;
	}
	
    /**
     * @return lgBal
     */
	public java.math.BigDecimal getLgBal() {
		return this.lgBal;
	}
	
	/**
	 * @param lgCacFlg
	 */
	public void setLgCacFlg(String lgCacFlg) {
		this.lgCacFlg = lgCacFlg;
	}
	
    /**
     * @return lgCacFlg
     */
	public String getLgCacFlg() {
		return this.lgCacFlg;
	}
	
	/**
	 * @param lgCaseStep
	 */
	public void setLgCaseStep(String lgCaseStep) {
		this.lgCaseStep = lgCaseStep;
	}
	
    /**
     * @return lgCaseStep
     */
	public String getLgCaseStep() {
		return this.lgCaseStep;
	}
	
	/**
	 * @param lgCcy
	 */
	public void setLgCcy(String lgCcy) {
		this.lgCcy = lgCcy;
	}
	
    /**
     * @return lgCcy
     */
	public String getLgCcy() {
		return this.lgCcy;
	}
	
	/**
	 * @param lgCivilFlg
	 */
	public void setLgCivilFlg(String lgCivilFlg) {
		this.lgCivilFlg = lgCivilFlg;
	}
	
    /**
     * @return lgCivilFlg
     */
	public String getLgCivilFlg() {
		return this.lgCivilFlg;
	}
	
	/**
	 * @param lgClsFlg
	 */
	public void setLgClsFlg(String lgClsFlg) {
		this.lgClsFlg = lgClsFlg;
	}
	
    /**
     * @return lgClsFlg
     */
	public String getLgClsFlg() {
		return this.lgClsFlg;
	}
	
	/**
	 * @param lgCnfFlg
	 */
	public void setLgCnfFlg(String lgCnfFlg) {
		this.lgCnfFlg = lgCnfFlg;
	}
	
    /**
     * @return lgCnfFlg
     */
	public String getLgCnfFlg() {
		return this.lgCnfFlg;
	}
	
	/**
	 * @param lgCofProcType
	 */
	public void setLgCofProcType(String lgCofProcType) {
		this.lgCofProcType = lgCofProcType;
	}
	
    /**
     * @return lgCofProcType
     */
	public String getLgCofProcType() {
		return this.lgCofProcType;
	}
	
	/**
	 * @param lgConsnLimitAmt
	 */
	public void setLgConsnLimitAmt(java.math.BigDecimal lgConsnLimitAmt) {
		this.lgConsnLimitAmt = lgConsnLimitAmt;
	}
	
    /**
     * @return lgConsnLimitAmt
     */
	public java.math.BigDecimal getLgConsnLimitAmt() {
		return this.lgConsnLimitAmt;
	}
	
	/**
	 * @param lgConsnLimitCcy
	 */
	public void setLgConsnLimitCcy(String lgConsnLimitCcy) {
		this.lgConsnLimitCcy = lgConsnLimitCcy;
	}
	
    /**
     * @return lgConsnLimitCcy
     */
	public String getLgConsnLimitCcy() {
		return this.lgConsnLimitCcy;
	}
	
	/**
	 * @param lgFlag
	 */
	public void setLgFlag(String lgFlag) {
		this.lgFlag = lgFlag;
	}
	
    /**
     * @return lgFlag
     */
	public String getLgFlag() {
		return this.lgFlag;
	}
	
	/**
	 * @param advBkRefNo
	 */
	public void setAdvBkRefNo(String advBkRefNo) {
		this.advBkRefNo = advBkRefNo;
	}
	
    /**
     * @return advBkRefNo
     */
	public String getAdvBkRefNo() {
		return this.advBkRefNo;
	}
	
	/**
	 * @param amdTimes
	 */
	public void setAmdTimes(java.math.BigDecimal amdTimes) {
		this.amdTimes = amdTimes;
	}
	
    /**
     * @return amdTimes
     */
	public java.math.BigDecimal getAmdTimes() {
		return this.amdTimes;
	}
	
	/**
	 * @param amdTimesTtl
	 */
	public void setAmdTimesTtl(java.math.BigDecimal amdTimesTtl) {
		this.amdTimesTtl = amdTimesTtl;
	}
	
    /**
     * @return amdTimesTtl
     */
	public java.math.BigDecimal getAmdTimesTtl() {
		return this.amdTimesTtl;
	}
	
	/**
	 * @param appRulesType
	 */
	public void setAppRulesType(String appRulesType) {
		this.appRulesType = appRulesType;
	}
	
    /**
     * @return appRulesType
     */
	public String getAppRulesType() {
		return this.appRulesType;
	}
	
	/**
	 * @param benCntCde
	 */
	public void setBenCntCde(String benCntCde) {
		this.benCntCde = benCntCde;
	}
	
    /**
     * @return benCntCde
     */
	public String getBenCntCde() {
		return this.benCntCde;
	}
	
	/**
	 * @param benEngNm
	 */
	public void setBenEngNm(String benEngNm) {
		this.benEngNm = benEngNm;
	}
	
    /**
     * @return benEngNm
     */
	public String getBenEngNm() {
		return this.benEngNm;
	}
	
	/**
	 * @param bizBrCde
	 */
	public void setBizBrCde(String bizBrCde) {
		this.bizBrCde = bizBrCde;
	}
	
    /**
     * @return bizBrCde
     */
	public String getBizBrCde() {
		return this.bizBrCde;
	}
	
	/**
	 * @param clmBalAmt
	 */
	public void setClmBalAmt(java.math.BigDecimal clmBalAmt) {
		this.clmBalAmt = clmBalAmt;
	}
	
    /**
     * @return clmBalAmt
     */
	public java.math.BigDecimal getClmBalAmt() {
		return this.clmBalAmt;
	}
	
	/**
	 * @param clmTtlBal
	 */
	public void setClmTtlBal(java.math.BigDecimal clmTtlBal) {
		this.clmTtlBal = clmTtlBal;
	}
	
    /**
     * @return clmTtlBal
     */
	public java.math.BigDecimal getClmTtlBal() {
		return this.clmTtlBal;
	}
	
	/**
	 * @param clsDt
	 */
	public void setClsDt(String clsDt) {
		this.clsDt = clsDt;
	}
	
    /**
     * @return clsDt
     */
	public String getClsDt() {
		return this.clsDt;
	}
	
	/**
	 * @param cmsRefNo
	 */
	public void setCmsRefNo(String cmsRefNo) {
		this.cmsRefNo = cmsRefNo;
	}
	
    /**
     * @return cmsRefNo
     */
	public String getCmsRefNo() {
		return this.cmsRefNo;
	}
	
	/**
	 * @param cntrctAmt
	 */
	public void setCntrctAmt(java.math.BigDecimal cntrctAmt) {
		this.cntrctAmt = cntrctAmt;
	}
	
    /**
     * @return cntrctAmt
     */
	public java.math.BigDecimal getCntrctAmt() {
		return this.cntrctAmt;
	}
	
	/**
	 * @param cntrctCcy
	 */
	public void setCntrctCcy(String cntrctCcy) {
		this.cntrctCcy = cntrctCcy;
	}
	
    /**
     * @return cntrctCcy
     */
	public String getCntrctCcy() {
		return this.cntrctCcy;
	}
	
	/**
	 * @param cntrctNo
	 */
	public void setCntrctNo(String cntrctNo) {
		this.cntrctNo = cntrctNo;
	}
	
    /**
     * @return cntrctNo
     */
	public String getCntrctNo() {
		return this.cntrctNo;
	}
	
	/**
	 * @param consnBkBic
	 */
	public void setConsnBkBic(String consnBkBic) {
		this.consnBkBic = consnBkBic;
	}
	
    /**
     * @return consnBkBic
     */
	public String getConsnBkBic() {
		return this.consnBkBic;
	}
	
	/**
	 * @param consnBkNo
	 */
	public void setConsnBkNo(String consnBkNo) {
		this.consnBkNo = consnBkNo;
	}
	
    /**
     * @return consnBkNo
     */
	public String getConsnBkNo() {
		return this.consnBkNo;
	}
	
	/**
	 * @param consnEffctDt
	 */
	public void setConsnEffctDt(String consnEffctDt) {
		this.consnEffctDt = consnEffctDt;
	}
	
    /**
     * @return consnEffctDt
     */
	public String getConsnEffctDt() {
		return this.consnEffctDt;
	}
	
	/**
	 * @param consnExpiryDt
	 */
	public void setConsnExpiryDt(String consnExpiryDt) {
		this.consnExpiryDt = consnExpiryDt;
	}
	
    /**
     * @return consnExpiryDt
     */
	public String getConsnExpiryDt() {
		return this.consnExpiryDt;
	}
	
	/**
	 * @param consnGurantNo
	 */
	public void setConsnGurantNo(String consnGurantNo) {
		this.consnGurantNo = consnGurantNo;
	}
	
    /**
     * @return consnGurantNo
     */
	public String getConsnGurantNo() {
		return this.consnGurantNo;
	}
	
	/**
	 * @param consnIssAmt
	 */
	public void setConsnIssAmt(java.math.BigDecimal consnIssAmt) {
		this.consnIssAmt = consnIssAmt;
	}
	
    /**
     * @return consnIssAmt
     */
	public java.math.BigDecimal getConsnIssAmt() {
		return this.consnIssAmt;
	}
	
	/**
	 * @param consnIssCcy
	 */
	public void setConsnIssCcy(String consnIssCcy) {
		this.consnIssCcy = consnIssCcy;
	}
	
    /**
     * @return consnIssCcy
     */
	public String getConsnIssCcy() {
		return this.consnIssCcy;
	}
	
	/**
	 * @param consnMaxAmt
	 */
	public void setConsnMaxAmt(java.math.BigDecimal consnMaxAmt) {
		this.consnMaxAmt = consnMaxAmt;
	}
	
    /**
     * @return consnMaxAmt
     */
	public java.math.BigDecimal getConsnMaxAmt() {
		return this.consnMaxAmt;
	}
	
	/**
	 * @param consnMaxCcy
	 */
	public void setConsnMaxCcy(String consnMaxCcy) {
		this.consnMaxCcy = consnMaxCcy;
	}
	
    /**
     * @return consnMaxCcy
     */
	public String getConsnMaxCcy() {
		return this.consnMaxCcy;
	}
	
	/**
	 * @param corrAmdTimes
	 */
	public void setCorrAmdTimes(String corrAmdTimes) {
		this.corrAmdTimes = corrAmdTimes;
	}
	
    /**
     * @return corrAmdTimes
     */
	public String getCorrAmdTimes() {
		return this.corrAmdTimes;
	}
	
	/**
	 * @param corrTimes
	 */
	public void setCorrTimes(java.math.BigDecimal corrTimes) {
		this.corrTimes = corrTimes;
	}
	
    /**
     * @return corrTimes
     */
	public java.math.BigDecimal getCorrTimes() {
		return this.corrTimes;
	}
	
	/**
	 * @param creditAmt
	 */
	public void setCreditAmt(java.math.BigDecimal creditAmt) {
		this.creditAmt = creditAmt;
	}
	
    /**
     * @return creditAmt
     */
	public java.math.BigDecimal getCreditAmt() {
		return this.creditAmt;
	}
	
	/**
	 * @param creditCcy
	 */
	public void setCreditCcy(String creditCcy) {
		this.creditCcy = creditCcy;
	}
	
    /**
     * @return creditCcy
     */
	public String getCreditCcy() {
		return this.creditCcy;
	}
	
	/**
	 * @param creditExpiryDt
	 */
	public void setCreditExpiryDt(String creditExpiryDt) {
		this.creditExpiryDt = creditExpiryDt;
	}
	
    /**
     * @return creditExpiryDt
     */
	public String getCreditExpiryDt() {
		return this.creditExpiryDt;
	}
	
	/**
	 * @param creditLgRefNo
	 */
	public void setCreditLgRefNo(String creditLgRefNo) {
		this.creditLgRefNo = creditLgRefNo;
	}
	
    /**
     * @return creditLgRefNo
     */
	public String getCreditLgRefNo() {
		return this.creditLgRefNo;
	}
	
	/**
	 * @param crossRmbFlg
	 */
	public void setCrossRmbFlg(String crossRmbFlg) {
		this.crossRmbFlg = crossRmbFlg;
	}
	
    /**
     * @return crossRmbFlg
     */
	public String getCrossRmbFlg() {
		return this.crossRmbFlg;
	}
	
	/**
	 * @param cstChnNm
	 */
	public void setCstChnNm(String cstChnNm) {
		this.cstChnNm = cstChnNm;
	}
	
    /**
     * @return cstChnNm
     */
	public String getCstChnNm() {
		return this.cstChnNm;
	}
	
	/**
	 * @param cstEngNm
	 */
	public void setCstEngNm(String cstEngNm) {
		this.cstEngNm = cstEngNm;
	}
	
    /**
     * @return cstEngNm
     */
	public String getCstEngNm() {
		return this.cstEngNm;
	}
	
	/**
	 * @param lgGurantBal
	 */
	public void setLgGurantBal(java.math.BigDecimal lgGurantBal) {
		this.lgGurantBal = lgGurantBal;
	}
	
    /**
     * @return lgGurantBal
     */
	public java.math.BigDecimal getLgGurantBal() {
		return this.lgGurantBal;
	}
	
	/**
	 * @param issFlg
	 */
	public void setIssFlg(String issFlg) {
		this.issFlg = issFlg;
	}
	
    /**
     * @return issFlg
     */
	public String getIssFlg() {
		return this.issFlg;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param othersCause
	 */
	public void setOthersCause(String othersCause) {
		this.othersCause = othersCause;
	}
	
    /**
     * @return othersCause
     */
	public String getOthersCause() {
		return this.othersCause;
	}
	
	/**
	 * @param benCode
	 */
	public void setBenCode(String benCode) {
		this.benCode = benCode;
	}
	
    /**
     * @return benCode
     */
	public String getBenCode() {
		return this.benCode;
	}
	
	/**
	 * @param gurantCode
	 */
	public void setGurantCode(String gurantCode) {
		this.gurantCode = gurantCode;
	}
	
    /**
     * @return gurantCode
     */
	public String getGurantCode() {
		return this.gurantCode;
	}
	
	/**
	 * @param benType
	 */
	public void setBenType(String benType) {
		this.benType = benType;
	}
	
    /**
     * @return benType
     */
	public String getBenType() {
		return this.benType;
	}
	
	/**
	 * @param gurantType1
	 */
	public void setGurantType1(String gurantType1) {
		this.gurantType1 = gurantType1;
	}
	
    /**
     * @return gurantType1
     */
	public String getGurantType1() {
		return this.gurantType1;
	}
	
	/**
	 * @param lgProcType
	 */
	public void setLgProcType(String lgProcType) {
		this.lgProcType = lgProcType;
	}
	
    /**
     * @return lgProcType
     */
	public String getLgProcType() {
		return this.lgProcType;
	}
	
	/**
	 * @param dealType
	 */
	public void setDealType(String dealType) {
		this.dealType = dealType;
	}
	
    /**
     * @return dealType
     */
	public String getDealType() {
		return this.dealType;
	}
	
	/**
	 * @param appType
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}
	
    /**
     * @return appType
     */
	public String getAppType() {
		return this.appType;
	}
	
	/**
	 * @param ackFlg
	 */
	public void setAckFlg(String ackFlg) {
		this.ackFlg = ackFlg;
	}
	
    /**
     * @return ackFlg
     */
	public String getAckFlg() {
		return this.ackFlg;
	}
	
	/**
	 * @param appRulesOth
	 */
	public void setAppRulesOth(String appRulesOth) {
		this.appRulesOth = appRulesOth;
	}
	
    /**
     * @return appRulesOth
     */
	public String getAppRulesOth() {
		return this.appRulesOth;
	}
	
	/**
	 * @param projName
	 */
	public void setProjName(String projName) {
		this.projName = projName;
	}
	
    /**
     * @return projName
     */
	public String getProjName() {
		return this.projName;
	}
	
	/**
	 * @param projCcy
	 */
	public void setProjCcy(String projCcy) {
		this.projCcy = projCcy;
	}
	
    /**
     * @return projCcy
     */
	public String getProjCcy() {
		return this.projCcy;
	}
	
	/**
	 * @param projAmt
	 */
	public void setProjAmt(java.math.BigDecimal projAmt) {
		this.projAmt = projAmt;
	}
	
    /**
     * @return projAmt
     */
	public java.math.BigDecimal getProjAmt() {
		return this.projAmt;
	}
	
	/**
	 * @param bhFlag
	 */
	public void setBhFlag(String bhFlag) {
		this.bhFlag = bhFlag;
	}
	
    /**
     * @return bhFlag
     */
	public String getBhFlag() {
		return this.bhFlag;
	}


}