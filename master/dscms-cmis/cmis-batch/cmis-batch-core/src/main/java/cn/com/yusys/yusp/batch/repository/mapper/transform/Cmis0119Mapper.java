package cn.com.yusys.yusp.batch.repository.mapper.transform;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：CMIS0119</br>
 * 任务名称：加工任务-业务处理-任务加急 </br>
 *
 * @author xuchao
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public interface Cmis0119Mapper {

    /**
     * 删除重复的记录
     *
     * @param openDay
     * @return
     */
    int deleteTaskUrgentDetailInfo01dele(@Param("openDay") String openDay);


    /**
     * 按月生成用户 的加急记录 一次生成连续三个月的数据
     *
     * @param openDay
     * @return
     */
    int insertTaskUrgentDetailInfo01A(@Param("openDay") String openDay);


    /**
     * 按季生成用户 的加急记录 一次生成一个季度的数据
     *
     * @param openDay
     * @return
     */
    int insertTaskUrgentDetailInfo01B(@Param("openDay") String openDay);

    /**
     * 清空临时表-任务加急笔数明细信息
     */
    void truncateTaskUrgentDetailInfo02A();

    /**
     * 插入临时表-任务加急笔数明细信息
     *
     * @param openDay
     * @return
     */
    int insertTaskUrgentDetailInfo02A(@Param("openDay") String openDay);

    /**
     * 每季度末或者每月末执行   历史结余笔数等于上次 剩余笔数,只更新预生效
     *
     * @param openDay
     * @return
     */
    int updateTaskUrgentDetailInfo02A(@Param("openDay") String openDay);


    /**
     * 清空临时表-任务加急笔数明细信息临时表
     *
     */
    void truncateTaskUrgentDetailInfo02();


    /**
     * 插入临时表-任务加急笔数明细信息临时表
     *
     * @param openDay
     * @return
     */
    int insertTaskUrgentDetailInfo02(@Param("openDay") String openDay);


    /**
     * 每季度或者每月执行:计算本次新增笔数,更新任务加急配置
     *
     * @param openDay
     * @return
     */
    int updateTaskUrgentDetailInfo02(@Param("openDay") String openDay);

    /**
     * 每季度或者每月执行:总优先笔数：历史结余笔数+本次新增笔数
     *
     * @param openDay
     * @return
     */
    int updateTaskUrgentDetailInfo03(@Param("openDay") String openDay);

    /**
     * 每季度或者每月执行:当前剩余笔数等于总优先笔数
     *
     * @param openDay
     * @return
     */
    int updateTaskUrgentDetailInfo04(@Param("openDay") String openDay);


    /**
     * 预生效改为生效之前  将之前的加急记录改为失效
     *
     * @param openDay
     * @return
     */
    int updateTaskUrgentDetailInfo05(@Param("openDay") String openDay);

    /**
     * 将当前的加急记录预生效改为生效
     *
     * @param openDay
     * @return
     */
    int updateTaskUrgentDetailInfo06(@Param("openDay") String openDay);
}
