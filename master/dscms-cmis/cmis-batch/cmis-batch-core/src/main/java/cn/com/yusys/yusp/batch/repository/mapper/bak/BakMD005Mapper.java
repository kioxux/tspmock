package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKMD005</br>
 * 任务名称：批后备份月表日表任务-备份委托贷款台账[ACC_ENTRUST_LOAN]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakMD005Mapper {

    /**
     * 查询待删除当天的[委托贷款台账[ACC_ENTRUST_LOAN]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryMD005DeleteOpenDayAccEntrustLoanCounts(@Param("openDay") String openDay);


    /**
     * 查询待删除当月的[委托贷款台账[ACC_ENTRUST_LOAN]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryMD005DeleteMAccEntrustLoanCounts(@Param("openDay") String openDay);

    /**
     * 删除当天的[委托贷款台账[ACC_ENTRUST_LOAN]]数据
     *
     * @param openDay
     */
    int bakMD005DeleteDAccEntrustLoan(@Param("openDay") String openDay);

    /**
     * 删除当月的[委托贷款台账[ACC_ENTRUST_LOAN]]数据
     *
     * @param openDay
     */
    int bakMD005DeleteMAccEntrustLoan(@Param("openDay") String openDay);

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertD(@Param("openDay") String openDay);

    /**
     * 备份当月的数据
     *
     * @param openDay
     * @return
     */
    int insertM(@Param("openDay") String openDay);

    /**
     *
     * 查询当天备份的[委托贷款台账 [ACC_ENTRUST_LOAN]]数据总条数
     * @param openDay
     * @return
     */
    int queryOriginalCounts(@Param("openDay") String openDay);

    /**
     *
     * 查询当月备份的[委托贷款台账 [TMP_M_ACC_ENTRUST_LOAN]]数据总条数
     * @param openDay
     * @return
     */
    int queryBakMCounts(@Param("openDay") String openDay);
}
