/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.gaps;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.gaps.BatSGapsCcmsPartyinf;
import cn.com.yusys.yusp.batch.service.load.gaps.BatSGapsCcmsPartyinfService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGapsCcmsPartyinfResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 14:06:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsgapsccmspartyinf")
public class BatSGapsCcmsPartyinfResource {
    @Autowired
    private BatSGapsCcmsPartyinfService batSGapsCcmsPartyinfService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSGapsCcmsPartyinf>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSGapsCcmsPartyinf> list = batSGapsCcmsPartyinfService.selectAll(queryModel);
        return new ResultDto<List<BatSGapsCcmsPartyinf>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSGapsCcmsPartyinf>> index(QueryModel queryModel) {
        List<BatSGapsCcmsPartyinf> list = batSGapsCcmsPartyinfService.selectByModel(queryModel);
        return new ResultDto<List<BatSGapsCcmsPartyinf>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{prcptcd}")
    protected ResultDto<BatSGapsCcmsPartyinf> show(@PathVariable("prcptcd") String prcptcd) {
        BatSGapsCcmsPartyinf batSGapsCcmsPartyinf = batSGapsCcmsPartyinfService.selectByPrimaryKey(prcptcd);
        return new ResultDto<BatSGapsCcmsPartyinf>(batSGapsCcmsPartyinf);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSGapsCcmsPartyinf> create(@RequestBody BatSGapsCcmsPartyinf batSGapsCcmsPartyinf) throws URISyntaxException {
        batSGapsCcmsPartyinfService.insert(batSGapsCcmsPartyinf);
        return new ResultDto<BatSGapsCcmsPartyinf>(batSGapsCcmsPartyinf);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSGapsCcmsPartyinf batSGapsCcmsPartyinf) throws URISyntaxException {
        int result = batSGapsCcmsPartyinfService.update(batSGapsCcmsPartyinf);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{prcptcd}")
    protected ResultDto<Integer> delete(@PathVariable("prcptcd") String prcptcd) {
        int result = batSGapsCcmsPartyinfService.deleteByPrimaryKey(prcptcd);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batSGapsCcmsPartyinfService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
