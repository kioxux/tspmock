/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.rcp;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.batch.domain.load.rcp.BatSRcpSnRepayPlanFileTemp;
import cn.com.yusys.yusp.batch.service.load.rcp.BatSRcpSnRepayPlanFileTempService;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSRcpSnRepayPlanFileTempResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 15:42:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batsrcpsnrepayplanfiletemp")
public class BatSRcpSnRepayPlanFileTempResource {
    @Autowired
    private BatSRcpSnRepayPlanFileTempService batSRcpSnRepayPlanFileTempService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatSRcpSnRepayPlanFileTemp>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatSRcpSnRepayPlanFileTemp> list = batSRcpSnRepayPlanFileTempService.selectAll(queryModel);
        return new ResultDto<List<BatSRcpSnRepayPlanFileTemp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatSRcpSnRepayPlanFileTemp>> index(QueryModel queryModel) {
        List<BatSRcpSnRepayPlanFileTemp> list = batSRcpSnRepayPlanFileTempService.selectByModel(queryModel);
        return new ResultDto<List<BatSRcpSnRepayPlanFileTemp>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatSRcpSnRepayPlanFileTemp> create(@RequestBody BatSRcpSnRepayPlanFileTemp batSRcpSnRepayPlanFileTemp) throws URISyntaxException {
        batSRcpSnRepayPlanFileTempService.insert(batSRcpSnRepayPlanFileTemp);
        return new ResultDto<BatSRcpSnRepayPlanFileTemp>(batSRcpSnRepayPlanFileTemp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatSRcpSnRepayPlanFileTemp batSRcpSnRepayPlanFileTemp) throws URISyntaxException {
        int result = batSRcpSnRepayPlanFileTempService.update(batSRcpSnRepayPlanFileTemp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String prdCode, String contractNo, String period) {
        int result = batSRcpSnRepayPlanFileTempService.deleteByPrimaryKey(prdCode, contractNo, period);
        return new ResultDto<Integer>(result);
    }

}
