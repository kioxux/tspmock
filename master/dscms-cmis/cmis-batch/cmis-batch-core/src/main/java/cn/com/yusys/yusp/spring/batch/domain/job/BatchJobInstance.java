/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.spring.batch.domain.job;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatchJobInstance
 * @类描述: batch_job_instance数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 20:01:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "batch_job_instance")
public class BatchJobInstance extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 作业实例ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "JOB_INSTANCE_ID")
	private Long jobInstanceId;
	
	/** 版本号 **/
	@Column(name = "VERSION", unique = false, nullable = true, length = 19)
	private Long version;
	
	/** 作业名称 **/
	@Column(name = "JOB_NAME", unique = false, nullable = false, length = 100)
	private String jobName;
	
	/** 作业标识 **/
	@Column(name = "JOB_KEY", unique = false, nullable = false, length = 32)
	private String jobKey;
	
	
	/**
	 * @param jobInstanceId
	 */
	public void setJobInstanceId(Long jobInstanceId) {
		this.jobInstanceId = jobInstanceId;
	}
	
    /**
     * @return jobInstanceId
     */
	public Long getJobInstanceId() {
		return this.jobInstanceId;
	}
	
	/**
	 * @param version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}
	
    /**
     * @return version
     */
	public Long getVersion() {
		return this.version;
	}
	
	/**
	 * @param jobName
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
    /**
     * @return jobName
     */
	public String getJobName() {
		return this.jobName;
	}
	
	/**
	 * @param jobKey
	 */
	public void setJobKey(String jobKey) {
		this.jobKey = jobKey;
	}
	
    /**
     * @return jobKey
     */
	public String getJobKey() {
		return this.jobKey;
	}


}