/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.gjp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatSGjpTfbParCust
 * @类描述: bat_s_gjp_tfb_par_cust数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-03 22:11:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_s_gjp_tfb_par_cust")
public class BatSGjpTfbParCust extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ID")
	private String id;
	
	/** 核心编号 **/
	@Column(name = "KERNEL_NO", unique = false, nullable = true, length = 32)
	private String kernelNo;
	
	/** 中文名称 **/
	@Column(name = "CHN_NAME", unique = false, nullable = true, length = 280)
	private String chnName;
	
	/** 客户分类 01:对公 02:对私居民 03:对私非居民 04:同业 **/
	@Column(name = "CUST_TYPE", unique = false, nullable = true, length = 8)
	private String custType;
	
	
	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
    /**
     * @return id
     */
	public String getId() {
		return this.id;
	}
	
	/**
	 * @param kernelNo
	 */
	public void setKernelNo(String kernelNo) {
		this.kernelNo = kernelNo;
	}
	
    /**
     * @return kernelNo
     */
	public String getKernelNo() {
		return this.kernelNo;
	}
	
	/**
	 * @param chnName
	 */
	public void setChnName(String chnName) {
		this.chnName = chnName;
	}
	
    /**
     * @return chnName
     */
	public String getChnName() {
		return this.chnName;
	}
	
	/**
	 * @param custType
	 */
	public void setCustType(String custType) {
		this.custType = custType;
	}
	
    /**
     * @return custType
     */
	public String getCustType() {
		return this.custType;
	}


}