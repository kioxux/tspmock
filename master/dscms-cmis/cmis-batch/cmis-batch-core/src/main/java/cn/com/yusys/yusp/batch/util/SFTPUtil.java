package cn.com.yusys.yusp.batch.util;

import com.jcraft.jsch.*;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.provider.sftp.SftpClientFactory;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.FileSystemException;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * SFTP工具类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
public class SFTPUtil {
    private static final Logger logger = LoggerFactory.getLogger(SFTPUtil.class);
    /**
     * 规避多线程并发不断开问题
     */
    private static ThreadLocal<SFTPUtil> sftpLocal = new ThreadLocal<>();


    private ChannelSftp channel;

    private Session session;

    /**
     * sftp用户名
     */
    private String userName;
    /**
     * sftp密码
     */
    private String password;
    /**
     * sftp主机ip
     */
    private String ftpHost;
    /**
     * sftp主机端口
     */
    private int ftpPort;


    /**
     * 默认构造方法
     */
    public SFTPUtil() {
    }

    /**
     * 连接SFTP服务器
     *
     * @param userName 用户名
     * @param password 密码
     * @param ftpHost  IP地址
     * @param ftpPort  端口
     */
    public SFTPUtil(String userName, String password, String ftpHost, int ftpPort) throws JSchException, FileSystemException, org.apache.commons.vfs2.FileSystemException {
        this.userName = userName;
        this.password = password;
        this.ftpHost = ftpHost;
        this.ftpPort = ftpPort;
        logger.info("SFTP连接信息: " + "userName=" + userName + ", " + "password=" + password + ", " + "ftpHost=" + ftpHost + ", " + "ftpPort=" + ftpPort);
        connectServer();

    }

    /**
     * 连接SFTP服务器
     *
     * @throws JSchException
     * @throws FileSystemException
     */
    private void connectServer() throws JSchException, FileSystemException, org.apache.commons.vfs2.FileSystemException {
        if (this.channel != null) {
            disconnect();
        }
        FileSystemOptions fso = new FileSystemOptions();
        SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(fso, "no");

        logger.info("SFTP连接正在创建Session... ...");
        this.session = SftpClientFactory.createConnection(this.ftpHost, this.ftpPort, this.userName.toCharArray(), this.password.toCharArray(), fso);
        logger.info("SFTP连接Session创建成功");

        logger.info("SFTP连接正在打开SFTP通道... ...");
        Channel _channel = this.session.openChannel("sftp");
        logger.info("SFTP连接通道打开成功");

        logger.info("SFTP连接中... ...");
        _channel.connect();
        logger.info("SFTP连接成功");

        this.channel = ((ChannelSftp) _channel);
    }


    /**
     * 是否已连接
     *
     * @return
     */
    private boolean isConnected() {
        return null != channel && channel.isConnected();
    }

    /**
     * 关闭通道
     */
    public void closeChannel() {
        if (null != channel) {
            try {
                channel.disconnect();
            } catch (Exception e) {
                logger.error("关闭SFTP通道发生异常:", e);
            }
        }
        if (null != session) {
            try {
                session.disconnect();
            } catch (Exception e) {
                logger.error("SFTP关闭 session异常:", e);
            }
        }
    }

    /**
     * 断开连接
     *
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.disconnect();
        super.finalize();
    }

    /**
     * sftp连接断开
     */
    public void closeSFTP() {
        this.disconnect();
    }

    /**
     * 从SFTP上下载文件到本地
     *
     * @param sftp       sftp工具类
     * @param remotePath 远程服务器文件路径
     * @param remoteFile sftp服务器文件名
     * @param localFile  下载到本地的文件路径和名称
     * @param closeFlag  rue 表示关闭连接 false 表示不关闭连接
     * @return flag 下载是否成功, true-下载成功, false-下载失败
     * @throws Exception
     */
    public boolean downFile(SFTPUtil sftp, String remotePath, String remoteFile, String localFile, boolean closeFlag) throws Exception {
        boolean flag = false;
        try {
            this.channel.cd(remotePath);
            InputStream input = this.channel.get(remoteFile);

            // 判断本地目录是否存在, 若不存在就创建文件夹
            if (localFile != null && !"".equals(localFile)) {
                File checkFileTemp = new File(localFile);
                if (!checkFileTemp.getParentFile().exists()) {
                    // 创建文件夹, 如：在f盘创建/TXT文件夹/testTXT/两个文件夹
                    checkFileTemp.getParentFile().mkdirs();
                }
            }

            FileOutputStream out = new FileOutputStream(new File(localFile));
            byte[] bt = new byte[1024];
            int length = -1;
            while ((length = input.read(bt)) != -1) {
                out.write(bt, 0, length);
            }
            input.close();
            out.close();
            if (closeFlag) {
                sftp.disconnect();
            }
            flag = true;
        } catch (SftpException e) {
            logger.error("文件下载失败！", e);
            throw new RuntimeException("文件下载失败！");
        } catch (FileNotFoundException e) {
            logger.error("下载文件到本地的路径有误！", e);
            throw new RuntimeException("下载文件到本地的路径有误！");
        } catch (IOException e) {
            logger.error("文件写入有误！", e);
            throw new RuntimeException("文件写入有误！");
        }

        return flag;
    }

    /**
     * 下载文件
     *
     * @param sftp           sftp工具类
     * @param remotePath     远程服务器文件路径
     * @param remoteFileName sftp服务器文件名
     * @param localFilePath  下载到本地的文件路径
     * @param localFileName  下载到本地的文件名称
     * @param closeFlag      true 表示关闭连接 false 表示不关闭连接
     * @return 下载是否成功, true-下载成功, false-下载失败
     * @throws Exception
     */
    public boolean downFile(SFTPUtil sftp, String remotePath, String remoteFileName, String localFilePath, String localFileName, boolean closeFlag) throws Exception {
        boolean flag = false;
        try {
            this.channel.cd(remotePath);
            String sftpRemoteFileName = remotePath + remoteFileName;
            logger.info("远程服务器文件路径为:[{}],sftp服务器文件名为:[{}],下载到本地的文件路径为:[{}],下载到本地的文件名称为:[{}]", remotePath, remoteFileName, localFilePath, localFileName);
            InputStream input = this.channel.get(sftpRemoteFileName);
            String localRemoteFile = localFilePath + remoteFileName;
            File checkFileTemp = null;
            // 判断本地目录是否存在, 若不存在就创建文件夹
            if (localRemoteFile != null && !"".equals(localRemoteFile)) {
                checkFileTemp = new File(localRemoteFile);
                if (!checkFileTemp.getParentFile().exists()) {
                    // 创建文件夹, 如：在f盘创建/TXT文件夹/testTXT/两个文件夹
                    checkFileTemp.getParentFile().mkdirs();
                }
            }

            FileOutputStream out = new FileOutputStream(new File(localRemoteFile));
            byte[] bt = new byte[1024];
            int length = -1;
            while ((length = input.read(bt)) != -1) {
                out.write(bt, 0, length);
            }
            input.close();
            out.close();
            if (closeFlag) {
                sftp.disconnect();
            }
            flag = true;
        } catch (SftpException e) {
            logger.error("文件下载失败！", e);
            throw new RuntimeException("文件下载失败！");
        } catch (FileNotFoundException e) {
            logger.error("下载文件到本地的路径有误！", e);
            throw new RuntimeException("下载文件到本地的路径有误！");
        } catch (IOException e) {
            logger.error("文件写入有误！", e);
            throw new RuntimeException("文件写入有误！");
        }
        return flag;
    }


    /**
     * 关闭连接
     */
    private void disconnect() {
        logger.info("SFTP连接正在关闭SFTP通道... ...");
        if (this.channel != null) {
            this.channel.exit();
        }
        logger.info("SFTP连接通道关闭成功");
        logger.info("SFTP连接正在退出Session... ...");
        if (this.session != null) {
            this.session.disconnect();
        }
        logger.info("SFTP连接退出Session成功");
        this.channel = null;
    }

    /**
     * 下载文件
     *
     * @param remotePath sftp服务器路径
     * @param remoteFile sftp服务器文件名
     * @return
     * @throws Exception
     */
    public InputStream downFile(String remotePath, String remoteFile) throws Exception {
        try {
            this.channel.cd(remotePath);
            return this.channel.get(remoteFile);
        } catch (SftpException e) {
            logger.error("文件下载失败！", e);
            throw new Exception("文件下载失败", e);
        }
    }

    /**
     * 打开指定目录
     *
     * @param directory
     * @return
     */
    public boolean openDirs(String directory) {
        try {
            this.channel.cd(directory);
            return true;
        } catch (SftpException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 检查远程SFTP服务端文件是否存在
     *
     * @param remoteFolder   远程文件路径
     * @param remoteFileName 文件名
     * @param closeFlag      是否关闭SFTP连接
     * @return
     * @throws Exception
     */
    public boolean checkSFTPFile(String remoteFolder, String remoteFileName, Boolean closeFlag) throws Exception {
        boolean flag = false;
        try {
            if (!this.openDirs(remoteFolder)) {// 判断是否存在远程文件目录
                logger.info("SFTP远程文件目录[" + remoteFolder + "]不存在！！！");
                return flag;
            }
            //信号文件匹配
            Pattern pattern = Pattern.compile(remoteFileName);
            // 检查文件是否存在
            Vector<ChannelSftp.LsEntry> files = this.ls(remoteFolder);
            logger.info("SFTP远程文件目录下文件数量为:[{}],获取该文件夹下文件列表开始", files.size());
            for (ChannelSftp.LsEntry file : files) {
                if (file.getFilename().equals(remoteFileName)) {// 文件名相同或匹配到
//              if (file.getFilename().equals(remoteFileName) || pattern.matcher(file.getFilename()).find()) {// 文件名相同或匹配到
                    logger.info("SFTP远程文件目录[{}]下文件名:[{}]，待获取的文件名为[{}]", remoteFolder, file.getFilename(), remoteFileName);
                    flag = true;
                    break;
//                    /*
//                     * 此处防止此种情况:需要下载Name_*数据文件,但是会匹配到SomeString_Name_*文件,导致数据文件无法下载
//                     * */
//                    int count = (int) pattern.splitAsStream(file.getFilename()).filter(new Predicate<String>() {// 过滤掉空字符串
//                        @Override
//                        public boolean test(String arg0) {
//                            return !"".equals(arg0);
//                        }
//                    }).count();
//                    logger.info("匹配的次数为:[{}]", count);
//                    if (count != SFTPUtil.countForStr(remoteFileName, "[*]")) {
//                        flag = false;
//                        continue;
//                    } else {
//                        flag = true;
//                    }
                } else {
                    flag = false;
                    continue;
                }
            }
            logger.info("SFTP远程文件[" + remoteFolder + "],[" + remoteFileName + "]," + (flag ? "[存在]" : "[不存在]"));
        } catch (Exception e) {
            throw e;
        } finally {
            if (closeFlag) {
                this.disconnect();
            }
        }
        return flag;
    }

    /**
     * 查看指定目录下文件
     *
     * @param directory
     * @return
     * @throws Exception
     */
    public Vector<ChannelSftp.LsEntry> ls(String directory) throws Exception {
        try {
            return this.channel.ls(directory);
        } catch (SftpException e) {
            logger.error("查看指定目录" + directory + "下文件失败");
            throw new Exception(e.getMessage(), e);
        }
    }

    /**
     * 计算子字符串出现次数<br>
     *
     * @param src
     * @param regx
     * @return
     */
    public static int countForStr(String src, String regx) {
        logger.info("计算子字符串出现次数开始，待获取的文件名为[{}],匹配规则为[{}]", src, regx);
        Pattern p = Pattern.compile(regx);
        Matcher matcher = p.matcher(src);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        logger.info("计算子字符串出现次数结束，出现的次数为[{}]", count);
        return count;
    }

    /**
     * 根据用户名,主机名判断是否已经存在连接
     *
     * @param userName
     * @param ftpHost
     * @return
     * @throws FileSystemException
     * @throws JSchException
     */
    public boolean isNew(String userName, String ftpHost) throws JSchException, FileSystemException, org.apache.commons.vfs2.FileSystemException {
        if (userName.equals(this.userName) && ftpHost.equals(this.ftpHost)) {
            if (this.channel == null || this.channel.isClosed()) {// 如果已经关闭连接,则重新连接
                this.connectServer();
            }
            return true;
        } else {
            return false;
        }
    }


}
