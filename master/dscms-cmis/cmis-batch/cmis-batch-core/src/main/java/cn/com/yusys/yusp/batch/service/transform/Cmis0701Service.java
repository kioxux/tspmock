package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0701Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0701</br>
 * 任务名称：加工任务-配置管理-更新行名行号对照表  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年8月18日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0701Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0701Service.class);

    @Autowired
    private Cmis0701Mapper cmis0701Mapper;

    /**
     * 更新行名行号对照表
     *
     * @param openDay
     */
    public void cmis0701UpdateCfgBankInfo(String openDay) {
        logger.info("更新行名行号对照表开始,请求参数为:[{}]", openDay);
        int updateCfgBankInfo = cmis0701Mapper.updateCfgBankInfo(openDay);
        logger.info("更新行名行号对照表结束,返回参数为:[{}]", updateCfgBankInfo);
        logger.info("插入行名行号对照表开始,请求参数为:[{}]", openDay);
        int insertCfgBankInfo = cmis0701Mapper.insertCfgBankInfo(openDay);
        logger.info("插入行名行号对照表结束,返回参数为:[{}]", insertCfgBankInfo);
    }
}
