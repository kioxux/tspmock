/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.domain.load.rcp;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTRcpMsCusInfoHis
 * @类描述: bat_t_rcp_ms_cus_info_his数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-08 14:04:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_t_rcp_ms_cus_info_his")
public class BatTRcpMsCusInfoHis extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	/** 业务日期 **/
	@Id
	@Column(name = "biz_date", unique = false, nullable = false, length = 10)
	private String bizDate;
	/** 客户编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "uuid")
	private String uuid;
	
	/** 客户名称 **/
	@Column(name = "name", unique = false, nullable = false, length = 300)
	private String name;
	
	/** 性别 **/
	@Column(name = "gender", unique = false, nullable = false, length = 1)
	private String gender;
	
	/** 出生日期 **/
	@Column(name = "birthday", unique = false, nullable = false, length = 8)
	private String birthday;
	
	/** 居民标志 **/
	@Column(name = "resi_flag", unique = false, nullable = false, length = 1)
	private String resiFlag;
	
	/** 境内境外标志 **/
	@Column(name = "dome_flag", unique = false, nullable = false, length = 1)
	private String domeFlag;
	
	/** 证件类型 10100、身份证 **/
	@Column(name = "id_type", unique = false, nullable = false, length = 5)
	private String idType;
	
	/** 证件号码 **/
	@Column(name = "id_no", unique = false, nullable = false, length = 60)
	private String idNo;
	
	/** 手机号码/联系电话 **/
	@Column(name = "mobile_no", unique = false, nullable = false, length = 20)
	private String mobileNo;
	
	/** 国籍 默认CHN **/
	@Column(name = "nationality", unique = false, nullable = false, length = 3)
	private String nationality;
	
	/** 录入日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 录入时间 **/
	@Column(name = "input_time", unique = false, nullable = true, length = 20)
	private String inputTime;
	
	/** 最后更新日期 **/
	@Column(name = "last_update_date", unique = false, nullable = true, length = 10)
	private String lastUpdateDate;
	
	/** 最后更新时间 **/
	@Column(name = "last_update_time", unique = false, nullable = true, length = 20)
	private String lastUpdateTime;
	
	
	/**
	 * @param bizDate
	 */
	public void setBizDate(String bizDate) {
		this.bizDate = bizDate;
	}
	
    /**
     * @return bizDate
     */
	public String getBizDate() {
		return this.bizDate;
	}
	
	/**
	 * @param uuid
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
    /**
     * @return uuid
     */
	public String getUuid() {
		return this.uuid;
	}
	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
    /**
     * @return name
     */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @param gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	
    /**
     * @return gender
     */
	public String getGender() {
		return this.gender;
	}
	
	/**
	 * @param birthday
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
    /**
     * @return birthday
     */
	public String getBirthday() {
		return this.birthday;
	}
	
	/**
	 * @param resiFlag
	 */
	public void setResiFlag(String resiFlag) {
		this.resiFlag = resiFlag;
	}
	
    /**
     * @return resiFlag
     */
	public String getResiFlag() {
		return this.resiFlag;
	}
	
	/**
	 * @param domeFlag
	 */
	public void setDomeFlag(String domeFlag) {
		this.domeFlag = domeFlag;
	}
	
    /**
     * @return domeFlag
     */
	public String getDomeFlag() {
		return this.domeFlag;
	}
	
	/**
	 * @param idType
	 */
	public void setIdType(String idType) {
		this.idType = idType;
	}
	
    /**
     * @return idType
     */
	public String getIdType() {
		return this.idType;
	}
	
	/**
	 * @param idNo
	 */
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	
    /**
     * @return idNo
     */
	public String getIdNo() {
		return this.idNo;
	}
	
	/**
	 * @param mobileNo
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
    /**
     * @return mobileNo
     */
	public String getMobileNo() {
		return this.mobileNo;
	}
	
	/**
	 * @param nationality
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
    /**
     * @return nationality
     */
	public String getNationality() {
		return this.nationality;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
    /**
     * @return lastUpdateDate
     */
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param lastUpdateTime
	 */
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
    /**
     * @return lastUpdateTime
     */
	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}


}