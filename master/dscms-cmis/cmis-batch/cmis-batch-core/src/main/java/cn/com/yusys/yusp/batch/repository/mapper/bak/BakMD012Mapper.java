package cn.com.yusys.yusp.batch.repository.mapper.bak;

import org.apache.ibatis.annotations.Param;

/**
 * 业务逻辑Dao类：</br>
 * 任务编号：BAKMD012</br>
 * 任务名称：批后备份月表日表任务-备份批复适用机构[APPR_STR_ORG_INFO]  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年7月11日 下午9:56:54
 */
public interface BakMD012Mapper {

    /**
     * 查询待删除当天的[批复适用机构[APPR_STR_ORG_INFO]]数据总次数
     *
     * @param openDay
     * @return
     */
    int queryMD012DeleteOpenDayApprStrOrgInfoCounts(@Param("openDay") String openDay);

    /**
     * 查询待删除当月的[批复适用机构[APPR_STR_ORG_INFO]]数据
     *
     * @param openDay
     */
    int queryMD012DeleteDApprStrOrgInfo(@Param("openDay") String openDay);

    /**
     * 删除当天的[批复适用机构[APPR_STR_ORG_INFO]]数据
     *
     * @param openDay
     */
    int bakMD012DeleteDApprStrOrgInfo(@Param("openDay") String openDay);

    /**
     * 删除当月的[批复适用机构[APPR_STR_ORG_INFO]]数据
     *
     * @param openDay
     */
    int bakMD012DeleteMApprStrOrgInfo(@Param("openDay") String openDay);

    /**
     * 备份当天的数据
     *
     * @param openDay
     * @return
     */
    int insertD(@Param("openDay") String openDay);

    /**
     * 备份当月的数据
     *
     * @param openDay
     * @return
     */
    int insertM(@Param("openDay") String openDay);


    /**
     * 查询当天备份的[批复适用机构 [APPR_STR_ORG_INFO]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryOriginalCounts(@Param("openDay") String openDay);


    /**
     * 查询当月备份的[批复适用机构 [TMP_m_APPR_STR_ORG_INFO]]数据总条数
     *
     * @param openDay
     * @return
     */
    int queryBakMCounts(@Param("openDay") String openDay);
}
