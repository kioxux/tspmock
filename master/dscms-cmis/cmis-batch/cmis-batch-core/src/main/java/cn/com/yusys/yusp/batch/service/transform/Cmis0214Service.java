package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0214Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0214</br>
 * 任务名称：加工任务-额度处理-授信分项计算可出账金额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0214Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0214Service.class);
    @Autowired
    private Cmis0214Mapper cmis0214Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新批复额度分项基础信息-计算可出账金额，对应SQL为
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0214UpdateApprLmtSubBasicInfo(String openDay) {
        logger.info("重命名创建和删除临时表-批复主信息开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_lmt","tmp_appr_str_mtable_info_cmis0214");
        logger.info("重命名创建和删除临时表-批复主信息结束");

        logger.info("插入临时表-批复主信息开始,请求参数为:[{}]", openDay);
        int insertTmpApprStrMtableInfo = cmis0214Mapper.insertTmpApprStrMtableInfo(openDay);
        logger.info("插入临时表-批复主信息结束,返回参数为:[{}]", insertTmpApprStrMtableInfo);
        tableUtilsService.analyzeTable("cmis_lmt","tmp_appr_str_mtable_info_cmis0214");//分析表

        logger.info("重命名创建和删除临时表-分项占用关系信息加工表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_lmt","tmp_deal_lmt_cont_rel_cmis0214");
        logger.info("重命名创建和删除临时表-分项占用关系信息加工表结束");

        logger.info("插入临时表-分项占用关系信息加工表开始,请求参数为:[{}]", openDay);
        int insertTmpDealLmtContRel = cmis0214Mapper.insertTmpDealLmtContRel(openDay);
        logger.info("插入临时表-分项占用关系信息加工表结束,返回参数为:[{}]", insertTmpDealLmtContRel);
        tableUtilsService.analyzeTable("cmis_lmt","tmp_deal_lmt_cont_rel_cmis0214");

        logger.info("重命名创建和删除临时表-合同占用关系信息加工表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_lmt","tmp_deal_cont_acc_rel_cmis0214");
        logger.info("重命名创建和删除临时表-合同占用关系信息加工表结束");

        logger.info("插入临时表-合同占用关系信息加工表开始,请求参数为:[{}]", openDay);
        int insertTmpDealContAccRel = cmis0214Mapper.insertTmpDealContAccRel(openDay);
        logger.info("插入临时表-合同占用关系信息加工表结束,返回参数为:[{}]", insertTmpDealContAccRel);
        tableUtilsService.analyzeTable("cmis_lmt","tmp_deal_cont_acc_rel_cmis0214");

        logger.info("重命名创建和删除临时表-合同占用关系信息加工表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_lmt_amt_cmis0214");
        logger.info("重命名创建和删除临时表-合同占用关系信息加工表结束");

        logger.info("插入临时表-合同占用关系信息加工表合同开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtCont = cmis0214Mapper.insertTmpLmtAmtCont(openDay);
        logger.info("插入临时表-合同占用关系信息加工表合同结束,返回参数为:[{}]", insertTmpLmtAmtCont);

        logger.info("插入临时表-合同占用关系信息加工表台账开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtAcc = cmis0214Mapper.insertTmpLmtAmtAcc(openDay);
        logger.info("插入临时表-合同占用关系信息加工表台账结束,返回参数为:[{}]", insertTmpLmtAmtAcc);

        tableUtilsService.analyzeTable("cmis_batch","tmp_lmt_amt_cmis0214");

        logger.info("重命名创建和删除关系表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_lmt_amt");
        logger.info("重命名创建和删除关系表结束");

        logger.info("插入关系表开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfoB1 = cmis0214Mapper.insertApprLmtSubBasicInfoB1(openDay);
        logger.info("插入关系表结束,返回参数为:[{}]", insertApprLmtSubBasicInfoB1);

        tableUtilsService.analyzeTable("cmis_batch","tmp_lmt_amt");

        logger.info("计算普通贷款台账 垫款台账  已出账金额 （授信可循环 则取贷款余额，不循环 则取 贷款金额 ）， 可出账金额=（授信总额（低风险授信）/授信敞口金额  减去  已出账金额）开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfoB1 = cmis0214Mapper.updateApprLmtSubBasicInfoB1(openDay);
        logger.info("计算普通贷款台账 垫款台账  已出账金额 （授信可循环 则取贷款余额，不循环 则取 贷款金额 ）， 可出账金额=（授信总额（低风险授信）/授信敞口金额  减去  已出账金额）结束,返回参数为:[{}]", updateApprLmtSubBasicInfoB1);

        logger.info("重命名创建和删除临时表-合同占用关系信息加工表2开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_lmt_amt_cmis0214");
        logger.info("重命名创建和删除临时表-合同占用关系信息加工表2结束");

        logger.info("插入临时表-分项项下关联的非最高额授信协议的合同下的已出账金额之和开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtCont2 = cmis0214Mapper.insertTmpLmtAmtCont2(openDay);
        logger.info("插入临时表-分项项下关联的非最高额授信协议的合同下的已出账金额之和结束,返回参数为:[{}]", insertTmpLmtAmtCont2);

        logger.info("插入二级分项项下的已出账金额之和开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtB = cmis0214Mapper.insertTmpLmtAmtB(openDay);
        logger.info("插入二级分项项下的已出账金额之和结束,返回参数为:[{}]", insertTmpLmtAmtB);

        logger.info("插入分项项下关联的台账已出账金额之和开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtAcc2 = cmis0214Mapper.insertTmpLmtAmtAcc2(openDay);
        logger.info("插入分项项下关联的台账已出账金额之和结束,返回参数为:[{}]", insertTmpLmtAmtAcc2);

        tableUtilsService.analyzeTable("cmis_batch","tmp_lmt_amt_cmis0214");

        logger.info("重命名创建和删除关系表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_lmt_amt");
        logger.info("重命名创建和删除关系表结束");

        logger.info("插入关系表开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfoB2 = cmis0214Mapper.insertApprLmtSubBasicInfoB2(openDay);
        logger.info("插入关系表结束,返回参数为:[{}]", insertApprLmtSubBasicInfoB2);

        tableUtilsService.analyzeTable("cmis_batch","tmp_lmt_amt");

        logger.info("含有二级分项的一级分项已出账金额汇总开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfoB2 = cmis0214Mapper.updateApprLmtSubBasicInfoB2(openDay);
        logger.info("含有二级分项的一级分项已出账金额汇总结束,返回参数为:[{}]", updateApprLmtSubBasicInfoB2);


        logger.info("重命名创建和删除关系表开始,请求参数为:[{}]", openDay);
        tableUtilsService.renameCreateDropTable("cmis_batch","tmp_lmt_amt");
        logger.info("重命名创建和删除关系表结束");

        logger.info("插入关系表开始(同业票据类、同业贸融类),请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfoC1 = cmis0214Mapper.insertApprLmtSubBasicInfoC1(openDay);
        logger.info("插入关系表结束(同业票据类、同业贸融类),返回参数为:[{}]", insertApprLmtSubBasicInfoC1);

        tableUtilsService.analyzeTable("cmis_batch","tmp_lmt_amt");

        logger.info("同业票据类、同业贸融类已出账金额汇总开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfoC1 = cmis0214Mapper.updateApprLmtSubBasicInfoC1(openDay);
        logger.info("同业票据类、同业贸融类已出账金额汇总结束,返回参数为:[{}]", updateApprLmtSubBasicInfoC1);

    }
}
