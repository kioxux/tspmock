package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0113Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0113</br>
 * 任务名称：加工任务-业务处理-汇率同步 </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0113Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0113Service.class);

    @Autowired
    private Cmis0113Mapper cmis0113Mapper;


    /**
     * 插入汇率表
     *
     * @param openDay
     */
    public void cmis0113InsertCfgExchgRate(String openDay) {
        logger.info("清空汇率表开始,请求参数为:[{}]", openDay);
        cmis0113Mapper.truncateCfgExchgRate(openDay);
        logger.info("清空汇率表结束");
        logger.info("插入汇率表开始,请求参数为:[{}]", openDay);
        int insertCfgExchgRate = cmis0113Mapper.insertCfgExchgRate(openDay);
        logger.info("插入汇率表结束,返回参数为:[{}]", insertCfgExchgRate);
    }
}
