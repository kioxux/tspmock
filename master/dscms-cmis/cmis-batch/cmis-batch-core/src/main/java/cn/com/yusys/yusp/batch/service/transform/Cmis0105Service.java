package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0105Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0105</br>
 * 任务名称：加工任务-业务处理-信用证台账处理  </br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午11:56:54
 */
@Service
@Transactional
public class Cmis0105Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0105Service.class);

    @Autowired
    private Cmis0105Mapper cmis0105Mapper;


    /**
     * 清空和插入 国结国内证业务发信贷台账
     *
     * @param openDay
     */
    public void cmis0105TruncateInsertTmpGjpAccTfLoc(String openDay) {
        logger.info("清空国结国内证业务发信贷台账开始");
        cmis0105Mapper.truncateTmpGjpAccTfLoc();
        logger.info("清空国结国内证业务发信贷台账表结束");
        logger.info("国内信用证 获取国结最新的台账数据开始,请求参数为:[{}]", openDay);
        int insertTmpGjpAccTfLoc01 = cmis0105Mapper.insertTmpGjpAccTfLoc01(openDay);
        logger.info("国内信用证 获取国结最新的台账数据结束,返回参数为:[{}]", insertTmpGjpAccTfLoc01);
        logger.info("进口信用证获取同一借据最新的台账数据开始,请求参数为:[{}]", openDay);
        int insertTmpGjpAccTfLoc02 = cmis0105Mapper.insertTmpGjpAccTfLoc02(openDay);
        logger.info("进口信用证获取同一借据最新的台账数据结束,返回参数为:[{}]", insertTmpGjpAccTfLoc02);
    }

    /**
     * 清空和插入 国结保证金台账
     *
     * @param openDay
     */
    public void cmis0105TruncateInsertTmpGjpSecurityAmt(String openDay) {
        logger.info("清空票据加工临时表开始");
        cmis0105Mapper.truncateTmpGjpSecurityAmt();
        logger.info("清空票据加工临时表结束");
        logger.info("插入票据加工临时表开始,请求参数为:[{}]", openDay);
        int insertTmpGjpSecurityAmt = cmis0105Mapper.insertTmpGjpSecurityAmt(openDay);
        logger.info("插入票据加工临时表结束,返回参数为:[{}]", insertTmpGjpSecurityAmt);
    }

    /**
     * 更新信用证台账
     *
     * @param openDay
     */
    public void cmis0105UpdateAccTfLoc(String openDay) {
        logger.info("更新信用证台账原始敞口金额开始,请求参数为:[{}]", openDay);
        int updateAccTfLoc01 = cmis0105Mapper.updateAccTfLoc01(openDay);
        logger.info("更新信用证台账原始敞口金额结束,返回参数为:[{}]", updateAccTfLoc01);

        logger.info("加工信用证台账开始,请求参数为:[{}]", openDay);
        int updateAccTfLoc02 = cmis0105Mapper.updateAccTfLoc02(openDay);
        logger.info("加工信用证台账结束,返回参数为:[{}]", updateAccTfLoc02);
    }
}
