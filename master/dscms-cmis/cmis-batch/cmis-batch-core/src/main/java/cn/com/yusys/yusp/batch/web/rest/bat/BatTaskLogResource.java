/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.bat;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskLog;
import cn.com.yusys.yusp.batch.service.bat.BatTaskLogService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTaskLogResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 16:29:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battasklog")
public class BatTaskLogResource {
    @Autowired
    private BatTaskLogService batTaskLogService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTaskLog>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTaskLog> list = batTaskLogService.selectAll(queryModel);
        return new ResultDto<List<BatTaskLog>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTaskLog>> index(QueryModel queryModel) {
        List<BatTaskLog> list = batTaskLogService.selectByModel(queryModel);
        return new ResultDto<List<BatTaskLog>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<BatTaskLog>> query(@RequestBody QueryModel queryModel) {
        List<BatTaskLog> list = batTaskLogService.selectByModel(queryModel);
        return new ResultDto<List<BatTaskLog>>(list);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTaskLog> create(@RequestBody BatTaskLog batTaskLog) throws URISyntaxException {
        batTaskLogService.insert(batTaskLog);
        return new ResultDto<BatTaskLog>(batTaskLog);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTaskLog batTaskLog) throws URISyntaxException {
        int result = batTaskLogService.update(batTaskLog);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String taskDate, String taskNo) {
        int result = batTaskLogService.deleteByPrimaryKey(taskDate, taskNo);
        return new ResultDto<Integer>(result);
    }

}
