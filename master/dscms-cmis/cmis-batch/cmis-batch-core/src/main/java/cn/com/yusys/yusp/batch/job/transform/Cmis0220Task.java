package cn.com.yusys.yusp.batch.job.transform;

import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRelService;
import cn.com.yusys.yusp.batch.service.bat.BatTaskRunService;
import cn.com.yusys.yusp.batch.service.transform.Cmis0220Service;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepLmtEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0220</br>
 * 任务名称：加工任务-额度处理-批后额度比对结果处理</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class Cmis0220Task extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0220Task.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private Cmis0220Service cmis0220Service;
    @Autowired
    private BatTaskRunService batTaskRunService;//任务运行管理
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息


    @Bean
    public Job cmis0220Job() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_JOB.key, JobStepLmtEnum.CMIS0220_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Job cmis0220Job = this.jobBuilderFactory.get(JobStepLmtEnum.CMIS0220_JOB.key)
                .start(cmis0220UpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(cmis0220CheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                 .next(cmis0220InsertTmpCompareLmtDetailsStep(WILL_BE_INJECTED))//加工授信分项额度比对表
                 .next(cmis0220InsertTmpCompareLmtContRelStep(WILL_BE_INJECTED))//加工授信分项占用关系额度比对表
                 .next(cmis0220InsertTmpCompareContAccRelStep(WILL_BE_INJECTED))//加工台账占用合同关系额度比对表
                 .next(cmis0220InsertTmpCompareLmtWhiteInfoStep(WILL_BE_INJECTED))//加工白名单额度比对表
                 .next(cmis0220InsertTmpCompareApprCoopSubInfoStep(WILL_BE_INJECTED))//加工合作方额度比对表
                .next(cmis0220UpdateTask100Step(WILL_BE_INJECTED))// 更新任务状态为执行成功
                .listener(new BatchJobListener())
                .build();
        return cmis0220Job;
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0220UpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_UPDATE_TASK010_STEP.key, JobStepLmtEnum.CMIS0220_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0220UpdateTask010Step = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0220_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0220_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_UPDATE_TASK010_STEP.key, JobStepLmtEnum.CMIS0220_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0220UpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0220CheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0220_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0220CheckRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0220_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0220_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag)
                        return RepeatStatus.FINISHED;
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0220_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_CHECK_REL_STEP.key, JobStepLmtEnum.CMIS0220_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return cmis0220CheckRelStep;
    }

    /**
     * 加工授信分项额度比对表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0220InsertTmpCompareLmtDetailsStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_DETAILS_STEP.key, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_DETAILS_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0220InsertTmpCompareLmtDetailsStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_DETAILS_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0220Service.insertTmpCompareLmtDetails(openDay);// 加工授信分项额度比对表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_DETAILS_STEP.key, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_DETAILS_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0220InsertTmpCompareLmtDetailsStep;
    }

    /**
     * 加工授信分项占用关系额度比对表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0220InsertTmpCompareLmtContRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_CONT_REL_STEP.key, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_CONT_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0220InsertTmpCompareLmtContRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_CONT_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0220Service.insertTmpCompareLmtContRel(openDay);//加工授信分项占用关系额度比对表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_CONT_REL_STEP.key, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_CONT_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0220InsertTmpCompareLmtContRelStep;
    }

    /**
     * 加工台账占用合同关系额度比对表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0220InsertTmpCompareContAccRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_CONT_ACC_REL_STEP.key, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_CONT_ACC_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0220InsertTmpCompareContAccRelStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_CONT_ACC_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0220Service.insertTmpCompareContAccRel(openDay);// 加工台账占用合同关系额度比对表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_CONT_ACC_REL_STEP.key, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_CONT_ACC_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0220InsertTmpCompareContAccRelStep;
    }

    /**
     * 加工白名单额度比对表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0220InsertTmpCompareLmtWhiteInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_WHITE_INFO_STEP.key, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_WHITE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0220InsertTmpCompareLmtWhiteInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_WHITE_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0220Service.insertTmpCompareLmtWhiteInfo(openDay);//加工白名单额度比对表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_WHITE_INFO_STEP.key, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_LMT_WHITE_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0220InsertTmpCompareLmtWhiteInfoStep;
    }

    /**
     * 加工合作方额度比对表
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0220InsertTmpCompareApprCoopSubInfoStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0220InsertTmpCompareApprCoopSubInfoStep = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_APPR_COOP_SUB_INFO_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    cmis0220Service.insertTmpCompareApprCoopSubInfo(openDay);//加工合作方额度比对表
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_APPR_COOP_SUB_INFO_STEP.key, JobStepLmtEnum.CMIS0220_INSERT_TMP_COMPARE_APPR_COOP_SUB_INFO_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0220InsertTmpCompareApprCoopSubInfoStep;
    }

    /**
     * 更新任务状态为执行成功
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step cmis0220UpdateTask100Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_UPDATE_TASK100_STEP.key, JobStepLmtEnum.CMIS0220_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step cmis0220UpdateTask100Step = this.stepBuilderFactory.get(JobStepLmtEnum.CMIS0220_UPDATE_TASK100_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.CMIS0220_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_100.key);// 任务状态 STD_TASK_TYPE, 执行成功
                    batTaskRun.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                    batTaskRun.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepLmtEnum.CMIS0220_UPDATE_TASK100_STEP.key, JobStepLmtEnum.CMIS0220_UPDATE_TASK100_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return cmis0220UpdateTask100Step;
    }


}
