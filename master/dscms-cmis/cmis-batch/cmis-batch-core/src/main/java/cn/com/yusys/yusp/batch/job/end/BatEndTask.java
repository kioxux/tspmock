package cn.com.yusys.yusp.batch.job.end;


import cn.com.yusys.yusp.batch.domain.bat.BatControlHis;
import cn.com.yusys.yusp.batch.domain.bat.BatControlRun;
import cn.com.yusys.yusp.batch.domain.bat.BatTaskRun;
import cn.com.yusys.yusp.batch.domain.bat.BatTransferCfg;
import cn.com.yusys.yusp.batch.job.config.JobCommonConfig;
import cn.com.yusys.yusp.batch.job.listener.BatchJobListener;
import cn.com.yusys.yusp.batch.service.bat.*;
import cn.com.yusys.yusp.batch.util.FileUtils;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.batch.BatEnums;
import cn.com.yusys.yusp.enums.batch.JobStepEnum;
import cn.com.yusys.yusp.enums.batch.TaskEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 批量任务处理类：</br>
 * 任务编号：BATEND</br>
 * 任务名称：调度运行结束-更新批量相关表 </br>
 *
 * @author zhoufeifei
 * @author leehuang
 * @version 1.0
 * @since 2020年6月12日 下午9:56:54
 */
@Configuration
@EnableBatchProcessing
@Lazy
public class BatEndTask extends JobCommonConfig {
    private static final Logger logger = LoggerFactory.getLogger(BatEndTask.class);
    private static final String WILL_BE_INJECTED = null;
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS");
    @Autowired
    private BatControlRunService batControlRunService;
    @Autowired
    private BatControlHisService batControlHisService;
    @Autowired
    private BatTaskRunService batTaskRunService;
    @Autowired
    private BatTaskLogService batTaskLogService;
    @Autowired
    private BatTransferCfgService batTransferCfgService;//文件传输配置信息
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private BatTaskRelService batTaskRelService;//任务依赖信息

    @Bean
    public Job batEndJob() {
        logger.info(TradeLogConstants.BATCH_JOB_BEGIN_PREFIX_LOGGER, JobStepEnum.BATEND_JOB.key, JobStepEnum.BATEND_JOB.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        return this.jobBuilderFactory.get(JobStepEnum.BATEND_JOB.key)
                .start(batEndUpdateTask010Step(WILL_BE_INJECTED))// 更新任务状态为执行中
                .next(batEndCheckRelStep(WILL_BE_INJECTED))//检查依赖任务是否已经完成
                .next(batEndDelete3DaysBeforeDirectoryStep(WILL_BE_INJECTED))  // 删除3日前文件夹
                .next(batEndUpdateRun2hisStep()) // 插入调度运行管理历史表
                .listener(new BatchJobListener())
                .build();
    }

    /**
     * 更新任务状态为执行中
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step batEndUpdateTask010Step(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATEND_UPDATE_TASK010_STEP.key, JobStepEnum.BATEND_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step batEndUpdateTask010Step = this.stepBuilderFactory.get(JobStepEnum.BATEND_UPDATE_TASK010_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BATEND_TASK.key);
                    batTaskRun.setTaskStatus(BatEnums.TASK_STATUS_010.key);// 任务状态 STD_TASK_TYPE, 执行中
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus010 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus010));
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.BATEND_UPDATE_TASK010_STEP.key, JobStepEnum.BATEND_UPDATE_TASK010_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return batEndUpdateTask010Step;
    }

    /**
     * 检查依赖任务是否已经完成
     *
     * @param openDay 营业日期
     * @return
     */
    @Bean
    @JobScope
    public Step batEndCheckRelStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATEND_CHECK_REL_STEP.key, JobStepEnum.BATEND_CHECK_REL_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step batEndCheckRelStep = this.stepBuilderFactory.get(JobStepEnum.BATEND_CHECK_REL_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(openDay, TaskEnum.BATEND_TASK.key);
                    boolean relFlag = batTaskRelService.checkRel(batTaskRun);
                    if (relFlag) {
                        return RepeatStatus.FINISHED;
                    }
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATEND_CHECK_REL_STEP.key, JobStepEnum.BATEND_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟开始");
                    TimeUnit.SECONDS.sleep(10);// sleep 10秒钟
                    logger.info(TradeLogConstants.BATCH_STEP_INFO_PREFIX_LOGGER, JobStepEnum.BATEND_CHECK_REL_STEP.key, JobStepEnum.BATEND_CHECK_REL_STEP.value, "检查依赖任务未完成，该线程休眠10秒钟结束");
                    return RepeatStatus.CONTINUABLE;
                }).build();
        return batEndCheckRelStep;
    }

    /**
     * 删除3日前文件夹
     *
     * @return
     */
    @Bean
    @JobScope
    public Step batEndDelete3DaysBeforeDirectoryStep(@Value("#{jobParameters[openDay]}") String openDay) {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATEND_DELETE_3DAYS_BEFORE_DIRECTORY_STEP.key, JobStepEnum.BATEND_DELETE_3DAYS_BEFORE_DIRECTORY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step batEndDelete3DaysBeforeDirectoryStep = this.stepBuilderFactory.get(JobStepEnum.BATEND_DELETE_3DAYS_BEFORE_DIRECTORY_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    Date openDayDate = DateUtils.parseDateByDef(openDay);//使用默认的格式yyyy-MM-dd转换已格式化的日期
                    BatTransferCfg batTransferCfg = batTransferCfgService.selectByPrimaryKey(TaskEnum.CORE0001_TASK.key);
                    FileUtils.delete3DaysBeforeDirectory(openDay, batTransferCfg);
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.BATEND_DELETE_3DAYS_BEFORE_DIRECTORY_STEP.key, JobStepEnum.BATEND_DELETE_3DAYS_BEFORE_DIRECTORY_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();

        return batEndDelete3DaysBeforeDirectoryStep;
    }


    /**
     * 插入数据至【调度运行历史(BAT_CONTROL_HIS)】表中
     *
     * @return
     */
    @Bean
    @JobScope
    public Step batEndUpdateRun2hisStep() {
        logger.info(TradeLogConstants.BATCH_STEP_BEGIN_PREFIX_LOGGER, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.key, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
        Step batEndUpdateRun2HisStep = this.stepBuilderFactory.get(JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.key)
                .tasklet((stepContribution, chunkContext) -> {
                    // 查询批量运行表
                    QueryModel queryModel = new QueryModel();
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    logger.info("从Redis中获取营业日期是[{}]", openDay);
                    queryModel.addCondition("openDay", openDay);
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(queryModel));
                    List<BatControlRun> batControlRunList = batControlRunService.selectAll(queryModel);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunList));

                    BatControlRun batControlRun = batControlRunList.get(0);
                    batControlRun.setEndTime(DateUtils.getCurrDateTimeStr()); //调度结束时间
                    batControlRun.setControlStatus(BatEnums.CONTROL_STATUS_100.key);//执行成功
                    batControlRun.setUpdDate(DateUtils.getCurrDateStr());// 最近修改日期
                    batControlRun.setUpdateTime(DateUtils.getCurrTimestamp());// 修改时间
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRun));
                    int batControlRunUpdate = batControlRunService.updateSelective(batControlRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_RUN_MODUEL.key, BatEnums.BAT_CONTROL_RUN_MODUEL.value, JSON.toJSONString(batControlRunUpdate));

                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_HIS_MODUEL.key, BatEnums.BAT_CONTROL_HIS_MODUEL.value, JSON.toJSONString(queryModel));
                    List<BatControlHis> batControlHisList = batControlHisService.selectAll(queryModel);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_HIS_MODUEL.key, BatEnums.BAT_CONTROL_HIS_MODUEL.value, JSON.toJSONString(batControlHisList));

                    BatControlHis batControlHis = batControlHisList.get(0);
                    BeanUtils.copyProperties(batControlRun, batControlHis);
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_CONTROL_HIS_MODUEL.key, BatEnums.BAT_CONTROL_HIS_MODUEL.value, JSON.toJSONString(batControlHis));
                    int batControlHisUpdate = batControlHisService.updateSelective(batControlHis);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_CONTROL_HIS_MODUEL.key, BatEnums.BAT_CONTROL_HIS_MODUEL.value, JSON.toJSONString(batControlHisUpdate));

                    String lastOpenDay = batControlRunList.get(0).getLastOpenDay();//上一营业日期
                    String taskDate = lastOpenDay;
                    String taskNo = TaskEnum.BATEND_TASK.key;
                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, taskNo);
                    BatTaskRun batTaskRun = batTaskRunService.selectByPrimaryKey(taskDate, taskNo);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));

                    logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(batTaskRun));
                    int updateTaskStatus100 = batTaskRunService.updateSelective(batTaskRun);
                    logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, BatEnums.BAT_TASK_RUN_MODUEL.key, BatEnums.BAT_TASK_RUN_MODUEL.value, JSON.toJSONString(updateTaskStatus100));

                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.key, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.value, "将任务运行管理数据保存到任务运行日志中开始,请求参数为:[{}]", lastOpenDay);
                    int insertByOpenDay = batTaskLogService.insertByOpenDay(lastOpenDay);// 任务日期为上一营业日期
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.key, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.value, "将任务运行管理数据保存到任务运行日志中结束,影响条数为:[" + insertByOpenDay + "]");

                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.key, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.value, "根据营业日期将任务运行日志中数据删除开始,请求参数为:[{}]", lastOpenDay);
                    int deleteByOpenDay = batTaskRunService.deleteByOpenDay(lastOpenDay);// 任务日期为上一营业日期
                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.key, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.value, "根据营业日期将任务运行日志中数据删除结束,影响条数为:[" + deleteByOpenDay + "]");

                    logger.info(TradeLogConstants.BATCH_STEP_END_PREFIX_LOGGER, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.key, JobStepEnum.BATEND_UPDATE_RUN2HIS_STEP.value, tranDateTimestampFormtter.format(LocalDateTime.now()));
                    return RepeatStatus.FINISHED;
                }).build();
        return batEndUpdateRun2HisStep;
    }
}
