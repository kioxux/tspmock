/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.batch.web.rest.load.gjp ;

import cn.com.yusys.yusp.batch.domain.load.gjp.BatTGjpTfbCmsLgInfoLog;
import cn.com.yusys.yusp.batch.service.load.gjp.BatTGjpTfbCmsLgInfoLogService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-batch-core模块
 * @类名称: BatTGjpTfbCmsLgInfoLogResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 21:47:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/battgjptfbcmslginfolog")
public class BatTGjpTfbCmsLgInfoLogResource {
    @Autowired
    private BatTGjpTfbCmsLgInfoLogService batTGjpTfbCmsLgInfoLogService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatTGjpTfbCmsLgInfoLog>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatTGjpTfbCmsLgInfoLog> list = batTGjpTfbCmsLgInfoLogService.selectAll(queryModel);
        return new ResultDto<List<BatTGjpTfbCmsLgInfoLog>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BatTGjpTfbCmsLgInfoLog>> index(QueryModel queryModel) {
        List<BatTGjpTfbCmsLgInfoLog> list = batTGjpTfbCmsLgInfoLogService.selectByModel(queryModel);
        return new ResultDto<List<BatTGjpTfbCmsLgInfoLog>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{id}")
    protected ResultDto<BatTGjpTfbCmsLgInfoLog> show(@PathVariable("id") String id) {
        BatTGjpTfbCmsLgInfoLog batTGjpTfbCmsLgInfoLog = batTGjpTfbCmsLgInfoLogService.selectByPrimaryKey(id);
        return new ResultDto<BatTGjpTfbCmsLgInfoLog>(batTGjpTfbCmsLgInfoLog);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BatTGjpTfbCmsLgInfoLog> create(@RequestBody BatTGjpTfbCmsLgInfoLog batTGjpTfbCmsLgInfoLog) throws URISyntaxException {
        batTGjpTfbCmsLgInfoLogService.insert(batTGjpTfbCmsLgInfoLog);
        return new ResultDto<BatTGjpTfbCmsLgInfoLog>(batTGjpTfbCmsLgInfoLog);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatTGjpTfbCmsLgInfoLog batTGjpTfbCmsLgInfoLog) throws URISyntaxException {
        int result = batTGjpTfbCmsLgInfoLogService.update(batTGjpTfbCmsLgInfoLog);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{id}")
    protected ResultDto<Integer> delete(@PathVariable("id") String id) {
        int result = batTGjpTfbCmsLgInfoLogService.deleteByPrimaryKey(id);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batTGjpTfbCmsLgInfoLogService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
