package cn.com.yusys.yusp.batch.service.transform;

import cn.com.yusys.yusp.batch.repository.mapper.transform.Cmis0213Mapper;
import cn.com.yusys.yusp.batch.service.load.TableUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量任务处理类：</br>
 * 任务编号：CMIS0213</br>
 * 任务名称：加工任务-额度处理-授信分项计算用信金额</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年9月14日 下午9:56:54
 */
@Service
@Transactional
public class Cmis0213Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmis0213Service.class);
    @Autowired
    private Cmis0213Mapper cmis0213Mapper;
    @Autowired
    private TableUtilsService tableUtilsService;

    /**
     * 更新批复额度分项基础信息-计算用信金额，对应SQL为CMIS0213-04appr_lmt_sub_basic_info计算用信金额.sql
     *
     * @param openDay
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void cmis0213UpdateApprLmtSubBasicInfo(String openDay) {
        logger.info("truncateApprLmtSubBasicInfoA1清理关系表开始,请求参数为:[{}]", openDay);
        // cmis0213Mapper.truncateApprLmtSubBasicInfoA1();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateApprLmtSubBasicInfoA1清理关系表结束");

        logger.info("插入关系表开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfoA1 = cmis0213Mapper.insertApprLmtSubBasicInfoA1(openDay);
        logger.info("插入关系表结束,返回参数为:[{}]", insertApprLmtSubBasicInfoA1);

        logger.info("计算普通贷款台账 垫款台账 最高额授信协议对应的台账 占用授信的 用信余额 、用信敞口余额开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfoA1 = cmis0213Mapper.updateApprLmtSubBasicInfoA1(openDay);
        logger.info("计算普通贷款台账 垫款台账 最高额授信协议对应的台账 占用授信的 用信余额 、用信敞口余额结束,返回参数为:[{}]", updateApprLmtSubBasicInfoA1);

        logger.info("truncateApprLmtSubBasicInfoA2清理关系表开始,请求参数为:[{}]", openDay);
        // cmis0213Mapper.truncateApprLmtSubBasicInfoA2();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateApprLmtSubBasicInfoA2清理关系表结束");

        logger.info("插入关系表开始,请求参数为:[{}]", openDay);
        int insertApprLmtSubBasicInfoA2 = cmis0213Mapper.insertApprLmtSubBasicInfoA2(openDay);
        logger.info("插入关系表结束,返回参数为:[{}]", insertApprLmtSubBasicInfoA2);

        logger.info("一级分项用信余额汇总至二级分项  额度分项项下关联的合同下的台账总余额、总敞口余额汇总 + 额度分项项下关联的非最高额授信协议项下的台账金额之和开始,请求参数为:[{}]", openDay);
        int updateApprLmtSubBasicInfoA2 = cmis0213Mapper.updateApprLmtSubBasicInfoA2(openDay);
        logger.info("一级分项用信余额汇总至二级分项  额度分项项下关联的合同下的台账总余额、总敞口余额汇总 + 额度分项项下关联的非最高额授信协议项下的台账金额之和结束,返回参数为:[{}]", updateApprLmtSubBasicInfoA2);


        logger.info("truncateTmpLmtAmtTy清空加工占用授信表[同业额度分项项下用信金额汇总]开始,请求参数为:[{}]", openDay);
        // cmis0213Mapper.truncateTmpLmtAmtTy();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("truncateTmpLmtAmtTy清空加工占用授信表[同业额度分项项下用信金额汇总]结束");

        logger.info("插入加工占用授信表[同业额度分项项下用信金额汇总]开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmtTy = cmis0213Mapper.insertTmpLmtAmtTy(openDay);
        logger.info("插入加工占用授信表[同业额度分项项下用信金额汇总]结束,返回参数为:[{}]", insertTmpLmtAmtTy);

        logger.info("更新批复额度分项基础信息[同业额度分项项下用信金额汇总]开始,请求参数为:[{}]", openDay);
        int updateTmpLmtAmtTy = cmis0213Mapper.updateTmpLmtAmtTy(openDay);
        logger.info("更新批复额度分项基础信息[同业额度分项项下用信金额汇总]结束,返回参数为:[{}]", updateTmpLmtAmtTy);

        logger.info("deleteTmpLmtAmt清空新信贷-临时表-加工占用授信表开始");
        // cmis0213Mapper.deleteTmpLmtAmt();// cmis_batch.tmp_lmt_amt
        tableUtilsService.renameCreateDropTable("cmis_batch", "tmp_lmt_amt");// 20211027 调整为调用重命名创建和删除相关表的公共方法
        logger.info("deleteTmpLmtAmt清空新信贷-临时表-加工占用授信表结束");


        logger.info("插入新信贷-临时表-[加工占用授信表]开始,请求参数为:[{}]", openDay);
        int insertTmpLmtAmt = cmis0213Mapper.insertTmpLmtAmt(openDay);
        logger.info("插入新信贷-临时表-[加工占用授信表]结束,返回参数为:[{}]", insertTmpLmtAmt);


        logger.info("更新合作方授信分项信息加工表开始,请求参数为:[{}]", openDay);
        int updateTmpDealApprCoopSubInfo = cmis0213Mapper.updateTmpDealApprCoopSubInfo(openDay);
        logger.info("更新合作方授信分项信息加工表结束,返回参数为:[{}]", updateTmpDealApprCoopSubInfo);


    }
}
