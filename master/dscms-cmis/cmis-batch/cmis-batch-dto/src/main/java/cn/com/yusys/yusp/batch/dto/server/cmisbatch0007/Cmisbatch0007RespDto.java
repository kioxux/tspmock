package cn.com.yusys.yusp.batch.dto.server.cmisbatch0007;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Cmisbatch0007RespDto implements Serializable {
    private static final long serialVersionUID = 1L;


    @JsonProperty(value = "opFlag")
    private String opFlag;    //  操作标志
    @JsonProperty(value = "opMessage")
    private String opMessage;//   操作消息

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMessage() {
        return opMessage;
    }

    public void setOpMessage(String opMessage) {
        this.opMessage = opMessage;
    }

    @Override
    public String toString() {
        return "Cmisbatch0006RespDto{" +
                "opFlag='" + opFlag + '\'' +
                ", opMessage='" + opMessage + '\'' +
                '}';
    }
}
