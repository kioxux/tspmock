package cn.com.yusys.yusp.batch.dto.batch;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：运行多个任务
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ParallelRunReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "singleRunReqDtos")
    private java.util.List<SingleRunReqDto> singleRunReqDtos;

    public List<SingleRunReqDto> getSingleRunReqDtos() {
        return singleRunReqDtos;
    }

    public void setSingleRunReqDtos(List<SingleRunReqDto> singleRunReqDtos) {
        this.singleRunReqDtos = singleRunReqDtos;
    }

    @Override
    public String toString() {
        return "ParallelRunReqDto{" +
                "singleRunReqDtos=" + singleRunReqDtos +
                '}';
    }
}
