package cn.com.yusys.yusp.batch.dto.server.cmisbatch0003;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询[贷款还款计划表]和[贷款期供交易明细]关联信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0003ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    @Override
    public String toString() {
        return "Yuspbatch0003ReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                '}';
    }
}  
