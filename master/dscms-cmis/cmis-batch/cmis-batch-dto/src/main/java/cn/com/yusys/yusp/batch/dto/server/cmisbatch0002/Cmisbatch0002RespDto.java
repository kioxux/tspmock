package cn.com.yusys.yusp.batch.dto.server.cmisbatch0002;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：查询[核心系统-历史表-贷款账户主表]信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0002RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cmisbatch0002List")
    private List<Cmisbatch0002Resp> cmisbatch0002List;

    public List<Cmisbatch0002Resp> getCmisbatch0002List() {
        return cmisbatch0002List;
    }

    public void setCmisbatch0002List(List<Cmisbatch0002Resp> cmisbatch0002List) {
        this.cmisbatch0002List = cmisbatch0002List;
    }

    @Override
    public String toString() {
        return "Cmisbatch0002RespDto{" +
                "cmisbatch0002List=" + cmisbatch0002List +
                '}';
    }
}  
