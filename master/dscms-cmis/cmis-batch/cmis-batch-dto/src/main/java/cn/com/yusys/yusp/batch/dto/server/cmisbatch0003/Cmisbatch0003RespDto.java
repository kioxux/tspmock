package cn.com.yusys.yusp.batch.dto.server.cmisbatch0003;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询[贷款还款计划表]和[贷款期供交易明细]关联信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0003RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "benqqish")
    private Integer benqqish;//本期期数
    @JsonProperty(value = "benqizqs")
    private Integer benqizqs;//本期子期数
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi;//终止日期
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "chushibj")
    private BigDecimal chushibj;//初始本金
    @JsonProperty(value = "chushilx")
    private BigDecimal chushilx;//初始利息
    @JsonProperty(value = "faxi")
    private BigDecimal faxi;//罚息=应收应计罚息（ysyjfaxi）+催收应计罚息（csyjfaxi）+应收罚息（yshofaxi）+催收罚息（cshofaxi）+罚息发生额(faxifash)(罚息发生额为空用0   罚息发生额：应收应计罚息发生额(ysyjfxfs) + 应收罚金发生额(yshfajfs))
    @JsonProperty(value = "fuxi")
    private BigDecimal fuxi;//复息=应计复息(yingjifx)+复息(fuxiiiii)+复息发生额(fuxifash)(复息发生额为空用0  复息发生额：应计复息发生额(yjifxifs) + 复息发生额(fuxiiifs))
    @JsonProperty(value = "zhanghye")
    private BigDecimal zhanghye;//账户余额
    @JsonProperty(value = "dqyhzclx")
    private BigDecimal dqyhzclx;//初始利息(chushilx)-应收应计利息(ysyjlixi)-催收应计利息(csyjlixi) - 应收欠息(ysqianxi) - 催收欠息(csqianxi)
    @JsonProperty(value = "dqyhfax")
    private BigDecimal dqyhfax;//罚息发生额（fuxifash）为空用0   罚息发生额：应收应计罚息发生额(ysyjfxfs) + 应收罚金发生额(yshfajfs)
    @JsonProperty(value = "dqyhfux")
    private BigDecimal dqyhfux;//复息发生额（fuxifash）为空用0  复息发生额：应计复息发生额(yjifxifs) + 复息发生额(fuxiiifs)
    @JsonProperty(value = "dqyhbj")
    private BigDecimal dqyhbj;//初始本金(chushibj) - 本金(benjinnn)

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public Integer getBenqizqs() {
        return benqizqs;
    }

    public void setBenqizqs(Integer benqizqs) {
        this.benqizqs = benqizqs;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getChushibj() {
        return chushibj;
    }

    public void setChushibj(BigDecimal chushibj) {
        this.chushibj = chushibj;
    }

    public BigDecimal getChushilx() {
        return chushilx;
    }

    public void setChushilx(BigDecimal chushilx) {
        this.chushilx = chushilx;
    }

    public BigDecimal getFaxi() {
        return faxi;
    }

    public void setFaxi(BigDecimal faxi) {
        this.faxi = faxi;
    }

    public BigDecimal getFuxi() {
        return fuxi;
    }

    public void setFuxi(BigDecimal fuxi) {
        this.fuxi = fuxi;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public BigDecimal getDqyhzclx() {
        return dqyhzclx;
    }

    public void setDqyhzclx(BigDecimal dqyhzclx) {
        this.dqyhzclx = dqyhzclx;
    }

    public BigDecimal getDqyhfax() {
        return dqyhfax;
    }

    public void setDqyhfax(BigDecimal dqyhfax) {
        this.dqyhfax = dqyhfax;
    }

    public BigDecimal getDqyhfux() {
        return dqyhfux;
    }

    public void setDqyhfux(BigDecimal dqyhfux) {
        this.dqyhfux = dqyhfux;
    }

    public BigDecimal getDqyhbj() {
        return dqyhbj;
    }

    public void setDqyhbj(BigDecimal dqyhbj) {
        this.dqyhbj = dqyhbj;
    }

    @Override
    public String toString() {
        return "Yuspbatch0003RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "benqqish='" + benqqish + '\'' +
                "benqizqs='" + benqizqs + '\'' +
                "qishriqi='" + qishriqi + '\'' +
                "zhzhriqi='" + zhzhriqi + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "chushibj='" + chushibj + '\'' +
                "chushilx='" + chushilx + '\'' +
                "faxi='" + faxi + '\'' +
                "fuxi='" + fuxi + '\'' +
                "zhanghye='" + zhanghye + '\'' +
                "dqyhzclx='" + dqyhzclx + '\'' +
                "dqyhfax='" + dqyhfax + '\'' +
                "dqyhfux='" + dqyhfux + '\'' +
                "dqyhbj='" + dqyhbj + '\'' +
                '}';
    }
}  
