package cn.com.yusys.yusp.batch.dto.server.cmisbatch0005;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：更新客户移交相关表
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0005RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cmisbatch0005RespList")
    private List<Cmisbatch0005Resp> cmisbatch0005RespList;

    public List<Cmisbatch0005Resp> getCmisbatch0005RespList() {
        return cmisbatch0005RespList;
    }

    public void setCmisbatch0005RespList(List<Cmisbatch0005Resp> cmisbatch0005RespList) {
        this.cmisbatch0005RespList = cmisbatch0005RespList;
    }

    @Override
    public String toString() {
        return "Cmisbatch0005RespDto{" +
                "cmisbatch0005RespList=" + cmisbatch0005RespList +
                '}';
    }
}
