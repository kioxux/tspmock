package cn.com.yusys.yusp.batch.dto.server.cmisbatch0010;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：提供日终调度平台调起批量任务
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0010RespDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "priFlag")
    private String priFlag;//任务级别
    @JsonProperty(value = "opFlag")
    private String opFlag;//操作标志
    @JsonProperty(value = "opMessage")
    private String opMessage;//操作消息

    public String getPriFlag() {
        return priFlag;
    }

    public void setPriFlag(String priFlag) {
        this.priFlag = priFlag;
    }

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMessage() {
        return opMessage;
    }

    public void setOpMessage(String opMessage) {
        this.opMessage = opMessage;
    }

    @Override
    public String toString() {
        return "Cmisbatch0010RespDto{" +
                "priFlag='" + priFlag + '\'' +
                ", opFlag='" + opFlag + '\'' +
                ", opMessage='" + opMessage + '\'' +
                '}';
    }
}
