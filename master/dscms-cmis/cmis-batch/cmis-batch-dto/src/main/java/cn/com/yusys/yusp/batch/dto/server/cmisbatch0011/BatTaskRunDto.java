package cn.com.yusys.yusp.batch.dto.server.cmisbatch0011;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：提供日终调度平台查询批量任务状态
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class BatTaskRunDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "taskDate")
    private String taskDate;//任务日期,格式YYYY-MM-DD
    @JsonProperty(value = "taskNo")
    private String taskNo;//任务编号
    @JsonProperty(value = "taskName")
    private String taskName;//任务名称
    @JsonProperty(value = "taskStatus")
    private String taskStatus;//任务状态
    @JsonProperty(value = "taskBeginTime")
    private String taskBeginTime;//任务开始时间,格式 YYYY-MM-DD HH:mm:ss
    @JsonProperty(value = "taskEndTime")
    private String taskEndTime;//任务结束时间,格式 YYYY-MM-DD HH:mm:ss
    @JsonProperty(value = "errorCode")
    private String errorCode;//错误码
    @JsonProperty(value = "errorInfo")
    private String errorInfo;//错误信息


    public String getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskBeginTime() {
        return taskBeginTime;
    }

    public void setTaskBeginTime(String taskBeginTime) {
        this.taskBeginTime = taskBeginTime;
    }

    public String getTaskEndTime() {
        return taskEndTime;
    }

    public void setTaskEndTime(String taskEndTime) {
        this.taskEndTime = taskEndTime;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }

    @Override
    public String toString() {
        return "BatTaskRunDto{" +
                "taskDate='" + taskDate + '\'' +
                ", taskNo='" + taskNo + '\'' +
                ", taskName='" + taskName + '\'' +
                ", taskStatus='" + taskStatus + '\'' +
                ", taskBeginTime='" + taskBeginTime + '\'' +
                ", taskEndTime='" + taskEndTime + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorInfo='" + errorInfo + '\'' +
                '}';
    }
}
