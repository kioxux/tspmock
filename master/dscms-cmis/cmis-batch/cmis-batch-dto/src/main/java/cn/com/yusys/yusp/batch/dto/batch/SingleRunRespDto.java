package cn.com.yusys.yusp.batch.dto.batch;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：运行单个任务
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class SingleRunRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "taskNo")
    private String taskNo;//任务编号
    @JsonProperty(value = "openDay")
    private String openDay;//营业日期
    @JsonProperty(value = "result")
    private Integer result;//返回结果
    @JsonProperty(value = "msg")
    private String msg;//返回原因

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getOpenDay() {
        return openDay;
    }

    public void setOpenDay(String openDay) {
        this.openDay = openDay;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ParallelRunRespDto{" +
                "taskNo='" + taskNo + '\'' +
                ", openDay='" + openDay + '\'' +
                ", result=" + result +
                ", msg='" + msg + '\'' +
                '}';
    }
}
