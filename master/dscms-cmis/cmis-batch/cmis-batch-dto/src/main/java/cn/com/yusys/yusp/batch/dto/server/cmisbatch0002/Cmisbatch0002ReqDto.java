package cn.com.yusys.yusp.batch.dto.server.cmisbatch0002;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询[核心系统-历史表-贷款账户主表]信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0002ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    @Override
    public String toString() {
        return "Yuspbatch0002ReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                '}';
    }
}  
