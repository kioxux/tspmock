package cn.com.yusys.yusp.batch.dto.server.cmisbatch0008;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto： 不定期分类任务信息生成
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0008ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "priDivisPerc")
    private String priDivisPerc;    //  优先分配比例
    @JsonProperty(value = "updPeriod")
    private String updPeriod;//   更新周期
    @JsonProperty(value = "bizType")
    private String bizType;// 业务类型
    @JsonProperty(value = "inputId")
    private String inputId;// 登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期

    public String getPriDivisPerc() {
        return priDivisPerc;
    }

    public void setPriDivisPerc(String priDivisPerc) {
        this.priDivisPerc = priDivisPerc;
    }

    public String getUpdPeriod() {
        return updPeriod;
    }

    public void setUpdPeriod(String updPeriod) {
        this.updPeriod = updPeriod;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    @Override
    public String toString() {
        return "Cmisbatch0008ReqDto{" +
                "priDivisPerc='" + priDivisPerc + '\'' +
                ", updPeriod='" + updPeriod + '\'' +
                ", bizType='" + bizType + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                '}';
    }
}
