package cn.com.yusys.yusp.batch.dto.batch;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：运行单个任务
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class SingleRunReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "taskNo")
    private String taskNo;//任务编号
    @JsonProperty(value = "openDay")
    private String openDay;//营业日期

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getOpenDay() {
        return openDay;
    }

    public void setOpenDay(String openDay) {
        this.openDay = openDay;
    }

    @Override
    public String toString() {
        return "SingleRunReqDto{" +
                "taskNo='" + taskNo + '\'' +
                ", openDay='" + openDay + '\'' +
                '}';
    }
}
