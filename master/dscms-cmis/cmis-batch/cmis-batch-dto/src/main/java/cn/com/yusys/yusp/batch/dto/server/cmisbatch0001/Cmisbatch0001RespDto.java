package cn.com.yusys.yusp.batch.dto.server.cmisbatch0001;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.Date;

/**
 * 响应Dto：调度运行管理信息查詢
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0001RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "openDay")
    private String openDay;//营业日期 T日
    @JsonProperty(value = "lastOpenDay")
    private String lastOpenDay;//上一营业日期
    @JsonProperty(value = "threadNum")
    private Integer threadNum;//最大线程数
    @JsonProperty(value = "warnTime")
    private String warnTime;//调度预警时间 预计调度作业结束时间，超过此时间发送短信报警。格式HH24：MM
    @JsonProperty(value = "exitTime")
    private String exitTime;//强制退出时间 调度作业强制退出的时间。格式HH24：MM
    @JsonProperty(value = "waitTime")
    private Integer waitTime;//等待时间（秒） 调度作业循环处理任务的等待时间
    @JsonProperty(value = "smsFlag")
    private String smsFlag;//是否已发送短信
    @JsonProperty(value = "beginTime")
    private String beginTime;//调度开始时间 格式 YYYY-MM-DD HH:mm:ss
    @JsonProperty(value = "endTime")
    private String endTime;//调度结束时间 格式 YYYY-MM-DD HH:mm:ss
    @JsonProperty(value = "controlStatus")
    private String controlStatus;//调度状态 010-批量运行中 100-批量运行完成 101-批量运行异常
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期
    @JsonProperty(value = "updId")
    private String updId;//最近修改人
    @JsonProperty(value = "updBrId")
    private String updBrId;//最近修改机构
    @JsonProperty(value = "updDate")
    private String updDate;//最近修改日期
    @JsonProperty(value = "createTime")
    private java.util.Date createTime;//创建时间
    @JsonProperty(value = "updateTime")
    private java.util.Date updateTime;//修改时间

    public String getOpenDay() {
        return openDay;
    }

    public void setOpenDay(String openDay) {
        this.openDay = openDay;
    }

    public String getLastOpenDay() {
        return lastOpenDay;
    }

    public void setLastOpenDay(String lastOpenDay) {
        this.lastOpenDay = lastOpenDay;
    }

    public Integer getThreadNum() {
        return threadNum;
    }

    public void setThreadNum(Integer threadNum) {
        this.threadNum = threadNum;
    }

    public String getWarnTime() {
        return warnTime;
    }

    public void setWarnTime(String warnTime) {
        this.warnTime = warnTime;
    }

    public String getExitTime() {
        return exitTime;
    }

    public void setExitTime(String exitTime) {
        this.exitTime = exitTime;
    }

    public Integer getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(Integer waitTime) {
        this.waitTime = waitTime;
    }

    public String getSmsFlag() {
        return smsFlag;
    }

    public void setSmsFlag(String smsFlag) {
        this.smsFlag = smsFlag;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getControlStatus() {
        return controlStatus;
    }

    public void setControlStatus(String controlStatus) {
        this.controlStatus = controlStatus;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Yuspbatch0001RespDto{" +
                "openDay='" + openDay + '\'' +
                "lastOpenDay='" + lastOpenDay + '\'' +
                "threadNum='" + threadNum + '\'' +
                "warnTime='" + warnTime + '\'' +
                "exitTime='" + exitTime + '\'' +
                "waitTime='" + waitTime + '\'' +
                "smsFlag='" + smsFlag + '\'' +
                "beginTime='" + beginTime + '\'' +
                "endTime='" + endTime + '\'' +
                "controlStatus='" + controlStatus + '\'' +
                "inputId='" + inputId + '\'' +
                "inputBrId='" + inputBrId + '\'' +
                "inputDate='" + inputDate + '\'' +
                "updId='" + updId + '\'' +
                "updBrId='" + updBrId + '\'' +
                "updDate='" + updDate + '\'' +
                "createTime='" + createTime + '\'' +
                "updateTime='" + updateTime + '\'' +
                '}';
    }
}  
