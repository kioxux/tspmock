package cn.com.yusys.yusp.batch.dto.server.cmisbatch0009;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询[贷款还款计划表]信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0009Resp implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "farendma")
    private String farendma;//法人代码
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "benqqish")
    private Integer benqqish;//本期期数
    @JsonProperty(value = "benqizqs")
    private Integer benqizqs;//本期子期数
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "kxqdqirq")
    private String kxqdqirq;//宽限期到期日
    @JsonProperty(value = "schkriqi")
    private String schkriqi;//上次还款日
    @JsonProperty(value = "xchkriqi")
    private String xchkriqi;//下次还款日
    @JsonProperty(value = "chushibj")
    private BigDecimal chushibj;//初始本金
    @JsonProperty(value = "chushilx")
    private BigDecimal chushilx;//初始利息
    @JsonProperty(value = "benjinnn")
    private BigDecimal benjinnn;//本金
    @JsonProperty(value = "ysyjlixi")
    private BigDecimal ysyjlixi;//应收应计利息
    @JsonProperty(value = "csyjlixi")
    private BigDecimal csyjlixi;//催收应计利息
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi;//应收欠息
    @JsonProperty(value = "csqianxi")
    private BigDecimal csqianxi;//催收欠息
    @JsonProperty(value = "ysyjfaxi")
    private BigDecimal ysyjfaxi;//应收应计罚息
    @JsonProperty(value = "csyjfaxi")
    private BigDecimal csyjfaxi;//催收应计罚息
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi;//应收罚息
    @JsonProperty(value = "cshofaxi")
    private BigDecimal cshofaxi;//催收罚息
    @JsonProperty(value = "yingjifx")
    private BigDecimal yingjifx;//应计复息
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii;//复息
    @JsonProperty(value = "hexiaolx")
    private BigDecimal hexiaolx;//核销利息
    @JsonProperty(value = "sjlllxsr")
    private BigDecimal sjlllxsr;//实际利率利息收入
    @JsonProperty(value = "lxtiaozh")
    private BigDecimal lxtiaozh;//利息调整
    @JsonProperty(value = "sjyjlixi")
    private BigDecimal sjyjlixi;//实际应计利息
    @JsonProperty(value = "sjyjfaxi")
    private BigDecimal sjyjfaxi;//实际应计罚息
    @JsonProperty(value = "sjyjfuxi")
    private BigDecimal sjyjfuxi;//实际应计复息
    @JsonProperty(value = "sjllsjsr")
    private BigDecimal sjllsjsr;//实际利率实际利息收入
    @JsonProperty(value = "yingjitx")
    private BigDecimal yingjitx;//应计贴息
    @JsonProperty(value = "yingshtx")
    private BigDecimal yingshtx;//应收贴息
    @JsonProperty(value = "yingshfy")
        private BigDecimal yingshfy;//应收费用
    @JsonProperty(value = "yingshfj")
    private BigDecimal yingshfj;//应收罚金
    @JsonProperty(value = "yihxbjlx")
    private BigDecimal yihxbjlx;//已核销本金利息
    @JsonProperty(value = "sjhxbjlx")
    private BigDecimal sjhxbjlx;//实际核销本金利息
    @JsonProperty(value = "benqizht")
    private String benqizht;//本期状态 STD_BENQIZHT
    @JsonProperty(value = "yjfyjzht")
    private String yjfyjzht;//应计非应计状态 STD_YJFYJZHT
    @JsonProperty(value = "qigengzl")
    private String qigengzl;//期供种类 STD_QIGENGZL
    @JsonProperty(value = "jzhqshrq")
    private String jzhqshrq;//基准起始日
    @JsonProperty(value = "jzhdqirq")
    private String jzhdqirq;//基准到期日
    @JsonProperty(value = "mingxixh")
    private String mingxixh;//明细序号
    @JsonProperty(value = "qgyqtnsh")
    private String qgyqtnsh;//期供逾期天数
    @JsonProperty(value = "fenhbios")
    private String fenhbios;//分行标识
    @JsonProperty(value = "weihguiy")
    private String weihguiy;//维护柜员
    @JsonProperty(value = "weihjigo")
    private String weihjigo;//维护机构
    @JsonProperty(value = "weihriqi")
    private String weihriqi;//维护日期
    @JsonProperty(value = "weihshij")
    private String weihshij;//维护时间
    @JsonProperty(value = "shijchuo")
    private String shijchuo;//时间戳
    @JsonProperty(value = "jiluztai")
    private String jiluztai;//记录状态 STD_JILUZTAI
    @JsonProperty(value = "dataDate")
    private String dataDate;//数据日期

    public String getFarendma() {
        return farendma;
    }

    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public Integer getBenqizqs() {
        return benqizqs;
    }

    public void setBenqizqs(Integer benqizqs) {
        this.benqizqs = benqizqs;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getKxqdqirq() {
        return kxqdqirq;
    }

    public void setKxqdqirq(String kxqdqirq) {
        this.kxqdqirq = kxqdqirq;
    }

    public String getSchkriqi() {
        return schkriqi;
    }

    public void setSchkriqi(String schkriqi) {
        this.schkriqi = schkriqi;
    }

    public String getXchkriqi() {
        return xchkriqi;
    }

    public void setXchkriqi(String xchkriqi) {
        this.xchkriqi = xchkriqi;
    }

    public BigDecimal getChushibj() {
        return chushibj;
    }

    public void setChushibj(BigDecimal chushibj) {
        this.chushibj = chushibj;
    }

    public BigDecimal getChushilx() {
        return chushilx;
    }

    public void setChushilx(BigDecimal chushilx) {
        this.chushilx = chushilx;
    }

    public BigDecimal getBenjinnn() {
        return benjinnn;
    }

    public void setBenjinnn(BigDecimal benjinnn) {
        this.benjinnn = benjinnn;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getSjlllxsr() {
        return sjlllxsr;
    }

    public void setSjlllxsr(BigDecimal sjlllxsr) {
        this.sjlllxsr = sjlllxsr;
    }

    public BigDecimal getLxtiaozh() {
        return lxtiaozh;
    }

    public void setLxtiaozh(BigDecimal lxtiaozh) {
        this.lxtiaozh = lxtiaozh;
    }

    public BigDecimal getSjyjlixi() {
        return sjyjlixi;
    }

    public void setSjyjlixi(BigDecimal sjyjlixi) {
        this.sjyjlixi = sjyjlixi;
    }

    public BigDecimal getSjyjfaxi() {
        return sjyjfaxi;
    }

    public void setSjyjfaxi(BigDecimal sjyjfaxi) {
        this.sjyjfaxi = sjyjfaxi;
    }

    public BigDecimal getSjyjfuxi() {
        return sjyjfuxi;
    }

    public void setSjyjfuxi(BigDecimal sjyjfuxi) {
        this.sjyjfuxi = sjyjfuxi;
    }

    public BigDecimal getSjllsjsr() {
        return sjllsjsr;
    }

    public void setSjllsjsr(BigDecimal sjllsjsr) {
        this.sjllsjsr = sjllsjsr;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public BigDecimal getYihxbjlx() {
        return yihxbjlx;
    }

    public void setYihxbjlx(BigDecimal yihxbjlx) {
        this.yihxbjlx = yihxbjlx;
    }

    public BigDecimal getSjhxbjlx() {
        return sjhxbjlx;
    }

    public void setSjhxbjlx(BigDecimal sjhxbjlx) {
        this.sjhxbjlx = sjhxbjlx;
    }

    public String getBenqizht() {
        return benqizht;
    }

    public void setBenqizht(String benqizht) {
        this.benqizht = benqizht;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public String getQigengzl() {
        return qigengzl;
    }

    public void setQigengzl(String qigengzl) {
        this.qigengzl = qigengzl;
    }

    public String getJzhqshrq() {
        return jzhqshrq;
    }

    public void setJzhqshrq(String jzhqshrq) {
        this.jzhqshrq = jzhqshrq;
    }

    public String getJzhdqirq() {
        return jzhdqirq;
    }

    public void setJzhdqirq(String jzhdqirq) {
        this.jzhdqirq = jzhdqirq;
    }

    public String getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(String mingxixh) {
        this.mingxixh = mingxixh;
    }

    public String getQgyqtnsh() {
        return qgyqtnsh;
    }

    public void setQgyqtnsh(String qgyqtnsh) {
        this.qgyqtnsh = qgyqtnsh;
    }

    public String getFenhbios() {
        return fenhbios;
    }

    public void setFenhbios(String fenhbios) {
        this.fenhbios = fenhbios;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public String getWeihshij() {
        return weihshij;
    }

    public void setWeihshij(String weihshij) {
        this.weihshij = weihshij;
    }

    public String getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(String shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    public String getDataDate() {
        return dataDate;
    }

    public void setDataDate(String dataDate) {
        this.dataDate = dataDate;
    }

    @Override
    public String toString() {
        return "Cmisbatch0009RespDto{" +
                "farendma='" + farendma + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", benqqish=" + benqqish +
                ", benqizqs=" + benqizqs +
                ", qishriqi='" + qishriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", kxqdqirq='" + kxqdqirq + '\'' +
                ", schkriqi='" + schkriqi + '\'' +
                ", xchkriqi='" + xchkriqi + '\'' +
                ", chushibj=" + chushibj +
                ", chushilx=" + chushilx +
                ", benjinnn=" + benjinnn +
                ", ysyjlixi=" + ysyjlixi +
                ", csyjlixi=" + csyjlixi +
                ", ysqianxi=" + ysqianxi +
                ", csqianxi=" + csqianxi +
                ", ysyjfaxi=" + ysyjfaxi +
                ", csyjfaxi=" + csyjfaxi +
                ", yshofaxi=" + yshofaxi +
                ", cshofaxi=" + cshofaxi +
                ", yingjifx=" + yingjifx +
                ", fuxiiiii=" + fuxiiiii +
                ", hexiaolx=" + hexiaolx +
                ", sjlllxsr=" + sjlllxsr +
                ", lxtiaozh=" + lxtiaozh +
                ", sjyjlixi=" + sjyjlixi +
                ", sjyjfaxi=" + sjyjfaxi +
                ", sjyjfuxi=" + sjyjfuxi +
                ", sjllsjsr=" + sjllsjsr +
                ", yingjitx=" + yingjitx +
                ", yingshtx=" + yingshtx +
                ", yingshfy=" + yingshfy +
                ", yingshfj=" + yingshfj +
                ", yihxbjlx=" + yihxbjlx +
                ", sjhxbjlx=" + sjhxbjlx +
                ", benqizht='" + benqizht + '\'' +
                ", yjfyjzht='" + yjfyjzht + '\'' +
                ", qigengzl='" + qigengzl + '\'' +
                ", jzhqshrq='" + jzhqshrq + '\'' +
                ", jzhdqirq='" + jzhdqirq + '\'' +
                ", mingxixh='" + mingxixh + '\'' +
                ", qgyqtnsh='" + qgyqtnsh + '\'' +
                ", fenhbios='" + fenhbios + '\'' +
                ", weihguiy='" + weihguiy + '\'' +
                ", weihjigo='" + weihjigo + '\'' +
                ", weihriqi='" + weihriqi + '\'' +
                ", weihshij='" + weihshij + '\'' +
                ", shijchuo='" + shijchuo + '\'' +
                ", jiluztai='" + jiluztai + '\'' +
                ", dataDate='" + dataDate + '\'' +
                '}';
    }
}
