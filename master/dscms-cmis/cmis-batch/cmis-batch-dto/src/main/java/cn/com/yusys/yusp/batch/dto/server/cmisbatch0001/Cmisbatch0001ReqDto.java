package cn.com.yusys.yusp.batch.dto.server.cmisbatch0001;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：调度运行管理信息查詢
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0001ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "openDay")
    private String openDay;//营业日期,T日

    public String getOpenDay() {
        return openDay;
    }

    public void setOpenDay(String openDay) {
        this.openDay = openDay;
    }

    @Override
    public String toString() {
        return "Yuspbatch0001ReqDto{" +
                "openDay='" + openDay + '\'' +
                '}';
    }
}  
