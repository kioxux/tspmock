package cn.com.yusys.yusp.batch.dto.server.cmisbatch0005;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：客户移交表清单
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0005Req implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "handoverType")
    private String handoverType;//移交方式
    @JsonProperty(value = "managerId")
    private String managerId;//    管户客户经理ID
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//    管户机构ID
    @JsonProperty(value = "conditionValue")
    private String conditionValue;//    条件对应值，包括客户号和合同号

    public String getHandoverType() {
        return handoverType;
    }

    public void setHandoverType(String handoverType) {
        this.handoverType = handoverType;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getConditionValue() {
        return conditionValue;
    }

    public void setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
    }

    @Override
    public String toString() {
        return "Cmisbatch0005Req{" +
                "handoverType='" + handoverType + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", conditionValue='" + conditionValue + '\'' +
                '}';
    }
}
