package cn.com.yusys.yusp.batch.dto.server.cmisbatch0009;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询[贷款还款计划表]信息
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0009ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "benqqish")
    private String benqqish;//本期期数

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(String benqqish) {
        this.benqqish = benqqish;
    }

    @Override
    public String toString() {
        return "Cmisbatch0009ReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "benqqish='" + benqqish + '\'' +
                '}';
    }
}  
