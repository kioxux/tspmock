package cn.com.yusys.yusp.batch.dto.server.cmisbatch0005;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：更新客户移交相关表
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0005ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cmisbatch0005ReqList")
    private java.util.List<Cmisbatch0005Req> cmisbatch0005ReqList;

    public List<Cmisbatch0005Req> getCmisbatch0005ReqList() {
        return cmisbatch0005ReqList;
    }

    public void setCmisbatch0005ReqList(List<Cmisbatch0005Req> cmisbatch0005ReqList) {
        this.cmisbatch0005ReqList = cmisbatch0005ReqList;
    }

    @Override
    public String toString() {
        return "Cmisbatch0005ReqDto{" +
                "cmisbatch0005ReqList=" + cmisbatch0005ReqList +
                '}';
    }
}
