package cn.com.yusys.yusp.batch.dto.server.cmisbatch0002;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 响应Dto：查询[核心系统-历史表-贷款账户主表]信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0002Resp implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "farendma")
    private String farendma;//法人代码
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "kuaijilb")
    private String kuaijilb;//会计类别
    @JsonProperty(value = "kaihriqi")
    private String kaihriqi;//开户日期
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "dkqixian")
    private String dkqixian;//贷款期限
    @JsonProperty(value = "dktiansh")
    private Long dktiansh;//贷款天数
    @JsonProperty(value = "daikxtai")
    private String daikxtai;//贷款形态
    @JsonProperty(value = "yjfyjzht")
    private String yjfyjzht;//应计非应计状态
    @JsonProperty(value = "dkzhhzht")
    private String dkzhhzht;//贷款账户状态
    @JsonProperty(value = "dbdkkksx")
    private Long dbdkkksx;//多笔贷款扣款顺序
    @JsonProperty(value = "dkyqkksx")
    private Long dkyqkksx;//多笔贷款逾期扣款顺序
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "hetongje")
    private BigDecimal hetongje;//合同金额
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "fafangje")
    private BigDecimal fafangje;//已发放金额
    @JsonProperty(value = "djiekfje")
    private BigDecimal djiekfje;//冻结可放金额
    @JsonProperty(value = "kffangje")
    private BigDecimal kffangje;//可发放金额
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "dzhibjin")
    private BigDecimal dzhibjin;//呆滞本金
    @JsonProperty(value = "daizbjin")
    private BigDecimal daizbjin;//呆账本金
    @JsonProperty(value = "dkuanjij")
    private BigDecimal dkuanjij;//贷款基金
    @JsonProperty(value = "ysyjlixi")
    private BigDecimal ysyjlixi;//应收应计利息
    @JsonProperty(value = "csyjlixi")
    private BigDecimal csyjlixi;//催收应计利息
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi;//应收欠息
    @JsonProperty(value = "csqianxi")
    private BigDecimal csqianxi;//催收欠息
    @JsonProperty(value = "ysyjfaxi")
    private BigDecimal ysyjfaxi;//应收应计罚息
    @JsonProperty(value = "csyjfaxi")
    private BigDecimal csyjfaxi;//催收应计罚息
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi;//应收罚息
    @JsonProperty(value = "cshofaxi")
    private BigDecimal cshofaxi;//催收罚息
    @JsonProperty(value = "yingjifx")
    private BigDecimal yingjifx;//应计复息
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii;//复息
    @JsonProperty(value = "yingjitx")
    private BigDecimal yingjitx;//应计贴息
    @JsonProperty(value = "yingshtx")
    private BigDecimal yingshtx;//应收贴息
    @JsonProperty(value = "tiexfuli")
    private BigDecimal tiexfuli;//贴息复利
    @JsonProperty(value = "ysyjxxsh")
    private BigDecimal ysyjxxsh;//应收应计销项税
    @JsonProperty(value = "csyjxxsh")
    private BigDecimal csyjxxsh;//催收应计销项税
    @JsonProperty(value = "xxiangsh")
    private BigDecimal xxiangsh;//销项税
    @JsonProperty(value = "daitanlx")
    private BigDecimal daitanlx;//待摊利息
    @JsonProperty(value = "hexiaobj")
    private BigDecimal hexiaobj;//核销本金
    @JsonProperty(value = "hexiaolx")
    private BigDecimal hexiaolx;//核销利息
    @JsonProperty(value = "lixishru")
    private BigDecimal lixishru;//利息收入
    @JsonProperty(value = "yingshfy")
    private BigDecimal yingshfy;//应收费用
    @JsonProperty(value = "feiyshru")
    private BigDecimal feiyshru;//费用收入
    @JsonProperty(value = "yingshfj")
    private BigDecimal yingshfj;//应收罚金
    @JsonProperty(value = "fjinshru")
    private BigDecimal fjinshru;//罚金收入
    @JsonProperty(value = "zhunbeij")
    private BigDecimal zhunbeij;//准备金
    @JsonProperty(value = "yijtdklx")
    private BigDecimal yijtdklx;//已计提贷款利息
    @JsonProperty(value = "yihxbjlx")
    private BigDecimal yihxbjlx;//已核销本金利息
    @JsonProperty(value = "zhcwjyrq")
    private String zhcwjyrq;//最后财务交易日
    @JsonProperty(value = "kshchpdm")
    private String kshchpdm;//可售产品代码
    @JsonProperty(value = "kshchpmc")
    private String kshchpmc;//可售产品名称
    @JsonProperty(value = "xtqudhao")
    private String xtqudhao;//系统渠道号
    @JsonProperty(value = "xtqudmch")
    private String xtqudmch;//系统渠道名称
    @JsonProperty(value = "dkczhzhh")
    private String dkczhzhh;//贷款出账号
    @JsonProperty(value = "dkdbfshi")
    private String dkdbfshi;//贷款担保方式
    @JsonProperty(value = "dkyongtu")
    private String dkyongtu;//贷款用途
    @JsonProperty(value = "jiejuxzh")
    private String jiejuxzh;//借据性质
    @JsonProperty(value = "ziczhtai")
    private String ziczhtai;//资产状态
    @JsonProperty(value = "dkglbumn")
    private String dkglbumn;//贷款管理部门
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注信息
    @JsonProperty(value = "mingxixh")
    private Long mingxixh;//明细序号
    @JsonProperty(value = "kaihujig")
    private String kaihujig;//开户机构
    @JsonProperty(value = "kaihguiy")
    private String kaihguiy;//开户柜员
    @JsonProperty(value = "xiaohugy")
    private String xiaohugy;//销户柜员
    @JsonProperty(value = "xiaohurq")
    private String xiaohurq;//销户日期
    @JsonProperty(value = "plfzuhao")
    private String plfzuhao;//批量分组号
    @JsonProperty(value = "shlxsfhq")
    private String shlxsfhq;//赎回时点利息还清标志(1-是,0-否)
    @JsonProperty(value = "tshudklx")
    private String tshudklx;//特殊贷款类型
    @JsonProperty(value = "gljgleib")
    private String gljgleib;//管理机构类别
    @JsonProperty(value = "fenhbios")
    private String fenhbios;//分行标识
    @JsonProperty(value = "weihguiy")
    private String weihguiy;//维护柜员
    @JsonProperty(value = "weihjigo")
    private String weihjigo;//维护机构
    @JsonProperty(value = "weihriqi")
    private String weihriqi;//维护日期
    @JsonProperty(value = "weihshij")
    private String weihshij;//维护时间
    @JsonProperty(value = "shijchuo")
    private Long shijchuo;//时间戳
    @JsonProperty(value = "jiluztai")
    private String jiluztai;//记录状态(0-正常,1-删除)
    @JsonProperty(value = "dataDate")
    private Date dataDate;//数据日期

    public String getFarendma() {
        return farendma;
    }

    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public Long getDktiansh() {
        return dktiansh;
    }

    public void setDktiansh(Long dktiansh) {
        this.dktiansh = dktiansh;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public Long getDbdkkksx() {
        return dbdkkksx;
    }

    public void setDbdkkksx(Long dbdkkksx) {
        this.dbdkkksx = dbdkkksx;
    }

    public Long getDkyqkksx() {
        return dkyqkksx;
    }

    public void setDkyqkksx(Long dkyqkksx) {
        this.dkyqkksx = dkyqkksx;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getFafangje() {
        return fafangje;
    }

    public void setFafangje(BigDecimal fafangje) {
        this.fafangje = fafangje;
    }

    public BigDecimal getDjiekfje() {
        return djiekfje;
    }

    public void setDjiekfje(BigDecimal djiekfje) {
        this.djiekfje = djiekfje;
    }

    public BigDecimal getKffangje() {
        return kffangje;
    }

    public void setKffangje(BigDecimal kffangje) {
        this.kffangje = kffangje;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getDkuanjij() {
        return dkuanjij;
    }

    public void setDkuanjij(BigDecimal dkuanjij) {
        this.dkuanjij = dkuanjij;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public BigDecimal getTiexfuli() {
        return tiexfuli;
    }

    public void setTiexfuli(BigDecimal tiexfuli) {
        this.tiexfuli = tiexfuli;
    }

    public BigDecimal getYsyjxxsh() {
        return ysyjxxsh;
    }

    public void setYsyjxxsh(BigDecimal ysyjxxsh) {
        this.ysyjxxsh = ysyjxxsh;
    }

    public BigDecimal getCsyjxxsh() {
        return csyjxxsh;
    }

    public void setCsyjxxsh(BigDecimal csyjxxsh) {
        this.csyjxxsh = csyjxxsh;
    }

    public BigDecimal getXxiangsh() {
        return xxiangsh;
    }

    public void setXxiangsh(BigDecimal xxiangsh) {
        this.xxiangsh = xxiangsh;
    }

    public BigDecimal getDaitanlx() {
        return daitanlx;
    }

    public void setDaitanlx(BigDecimal daitanlx) {
        this.daitanlx = daitanlx;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getLixishru() {
        return lixishru;
    }

    public void setLixishru(BigDecimal lixishru) {
        this.lixishru = lixishru;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public BigDecimal getFeiyshru() {
        return feiyshru;
    }

    public void setFeiyshru(BigDecimal feiyshru) {
        this.feiyshru = feiyshru;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public BigDecimal getFjinshru() {
        return fjinshru;
    }

    public void setFjinshru(BigDecimal fjinshru) {
        this.fjinshru = fjinshru;
    }

    public BigDecimal getZhunbeij() {
        return zhunbeij;
    }

    public void setZhunbeij(BigDecimal zhunbeij) {
        this.zhunbeij = zhunbeij;
    }

    public BigDecimal getYijtdklx() {
        return yijtdklx;
    }

    public void setYijtdklx(BigDecimal yijtdklx) {
        this.yijtdklx = yijtdklx;
    }

    public BigDecimal getYihxbjlx() {
        return yihxbjlx;
    }

    public void setYihxbjlx(BigDecimal yihxbjlx) {
        this.yihxbjlx = yihxbjlx;
    }

    public String getZhcwjyrq() {
        return zhcwjyrq;
    }

    public void setZhcwjyrq(String zhcwjyrq) {
        this.zhcwjyrq = zhcwjyrq;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getKshchpmc() {
        return kshchpmc;
    }

    public void setKshchpmc(String kshchpmc) {
        this.kshchpmc = kshchpmc;
    }

    public String getXtqudhao() {
        return xtqudhao;
    }

    public void setXtqudhao(String xtqudhao) {
        this.xtqudhao = xtqudhao;
    }

    public String getXtqudmch() {
        return xtqudmch;
    }

    public void setXtqudmch(String xtqudmch) {
        this.xtqudmch = xtqudmch;
    }

    public String getDkczhzhh() {
        return dkczhzhh;
    }

    public void setDkczhzhh(String dkczhzhh) {
        this.dkczhzhh = dkczhzhh;
    }

    public String getDkdbfshi() {
        return dkdbfshi;
    }

    public void setDkdbfshi(String dkdbfshi) {
        this.dkdbfshi = dkdbfshi;
    }

    public String getDkyongtu() {
        return dkyongtu;
    }

    public void setDkyongtu(String dkyongtu) {
        this.dkyongtu = dkyongtu;
    }

    public String getJiejuxzh() {
        return jiejuxzh;
    }

    public void setJiejuxzh(String jiejuxzh) {
        this.jiejuxzh = jiejuxzh;
    }

    public String getZiczhtai() {
        return ziczhtai;
    }

    public void setZiczhtai(String ziczhtai) {
        this.ziczhtai = ziczhtai;
    }

    public String getDkglbumn() {
        return dkglbumn;
    }

    public void setDkglbumn(String dkglbumn) {
        this.dkglbumn = dkglbumn;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public Long getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(Long mingxixh) {
        this.mingxixh = mingxixh;
    }

    public String getKaihujig() {
        return kaihujig;
    }

    public void setKaihujig(String kaihujig) {
        this.kaihujig = kaihujig;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getXiaohugy() {
        return xiaohugy;
    }

    public void setXiaohugy(String xiaohugy) {
        this.xiaohugy = xiaohugy;
    }

    public String getXiaohurq() {
        return xiaohurq;
    }

    public void setXiaohurq(String xiaohurq) {
        this.xiaohurq = xiaohurq;
    }

    public String getPlfzuhao() {
        return plfzuhao;
    }

    public void setPlfzuhao(String plfzuhao) {
        this.plfzuhao = plfzuhao;
    }

    public String getShlxsfhq() {
        return shlxsfhq;
    }

    public void setShlxsfhq(String shlxsfhq) {
        this.shlxsfhq = shlxsfhq;
    }

    public String getTshudklx() {
        return tshudklx;
    }

    public void setTshudklx(String tshudklx) {
        this.tshudklx = tshudklx;
    }

    public String getGljgleib() {
        return gljgleib;
    }

    public void setGljgleib(String gljgleib) {
        this.gljgleib = gljgleib;
    }

    public String getFenhbios() {
        return fenhbios;
    }

    public void setFenhbios(String fenhbios) {
        this.fenhbios = fenhbios;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public String getWeihshij() {
        return weihshij;
    }

    public void setWeihshij(String weihshij) {
        this.weihshij = weihshij;
    }

    public Long getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(Long shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    public Date getDataDate() {
        return dataDate;
    }

    public void setDataDate(Date dataDate) {
        this.dataDate = dataDate;
    }

    @Override
    public String toString() {
        return "Cmisbatch0002Resp{" +
                "farendma='" + farendma + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", kuaijilb='" + kuaijilb + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", dkqixian='" + dkqixian + '\'' +
                ", dktiansh=" + dktiansh +
                ", daikxtai='" + daikxtai + '\'' +
                ", yjfyjzht='" + yjfyjzht + '\'' +
                ", dkzhhzht='" + dkzhhzht + '\'' +
                ", dbdkkksx=" + dbdkkksx +
                ", dkyqkksx=" + dkyqkksx +
                ", huobdhao='" + huobdhao + '\'' +
                ", hetongje=" + hetongje +
                ", jiejuuje=" + jiejuuje +
                ", fafangje=" + fafangje +
                ", djiekfje=" + djiekfje +
                ", kffangje=" + kffangje +
                ", zhchbjin=" + zhchbjin +
                ", yuqibjin=" + yuqibjin +
                ", dzhibjin=" + dzhibjin +
                ", daizbjin=" + daizbjin +
                ", dkuanjij=" + dkuanjij +
                ", ysyjlixi=" + ysyjlixi +
                ", csyjlixi=" + csyjlixi +
                ", ysqianxi=" + ysqianxi +
                ", csqianxi=" + csqianxi +
                ", ysyjfaxi=" + ysyjfaxi +
                ", csyjfaxi=" + csyjfaxi +
                ", yshofaxi=" + yshofaxi +
                ", cshofaxi=" + cshofaxi +
                ", yingjifx=" + yingjifx +
                ", fuxiiiii=" + fuxiiiii +
                ", yingjitx=" + yingjitx +
                ", yingshtx=" + yingshtx +
                ", tiexfuli=" + tiexfuli +
                ", ysyjxxsh=" + ysyjxxsh +
                ", csyjxxsh=" + csyjxxsh +
                ", xxiangsh=" + xxiangsh +
                ", daitanlx=" + daitanlx +
                ", hexiaobj=" + hexiaobj +
                ", hexiaolx=" + hexiaolx +
                ", lixishru=" + lixishru +
                ", yingshfy=" + yingshfy +
                ", feiyshru=" + feiyshru +
                ", yingshfj=" + yingshfj +
                ", fjinshru=" + fjinshru +
                ", zhunbeij=" + zhunbeij +
                ", yijtdklx=" + yijtdklx +
                ", yihxbjlx=" + yihxbjlx +
                ", zhcwjyrq='" + zhcwjyrq + '\'' +
                ", kshchpdm='" + kshchpdm + '\'' +
                ", kshchpmc='" + kshchpmc + '\'' +
                ", xtqudhao='" + xtqudhao + '\'' +
                ", xtqudmch='" + xtqudmch + '\'' +
                ", dkczhzhh='" + dkczhzhh + '\'' +
                ", dkdbfshi='" + dkdbfshi + '\'' +
                ", dkyongtu='" + dkyongtu + '\'' +
                ", jiejuxzh='" + jiejuxzh + '\'' +
                ", ziczhtai='" + ziczhtai + '\'' +
                ", dkglbumn='" + dkglbumn + '\'' +
                ", beizhuuu='" + beizhuuu + '\'' +
                ", mingxixh=" + mingxixh +
                ", kaihujig='" + kaihujig + '\'' +
                ", kaihguiy='" + kaihguiy + '\'' +
                ", xiaohugy='" + xiaohugy + '\'' +
                ", xiaohurq='" + xiaohurq + '\'' +
                ", plfzuhao='" + plfzuhao + '\'' +
                ", shlxsfhq='" + shlxsfhq + '\'' +
                ", tshudklx='" + tshudklx + '\'' +
                ", gljgleib='" + gljgleib + '\'' +
                ", fenhbios='" + fenhbios + '\'' +
                ", weihguiy='" + weihguiy + '\'' +
                ", weihjigo='" + weihjigo + '\'' +
                ", weihriqi='" + weihriqi + '\'' +
                ", weihshij='" + weihshij + '\'' +
                ", shijchuo=" + shijchuo +
                ", jiluztai='" + jiluztai + '\'' +
                ", dataDate=" + dataDate +
                '}';
    }
}
