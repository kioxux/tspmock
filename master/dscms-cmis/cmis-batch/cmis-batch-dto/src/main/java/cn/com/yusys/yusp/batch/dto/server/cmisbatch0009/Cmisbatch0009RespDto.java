package cn.com.yusys.yusp.batch.dto.server.cmisbatch0009;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：查询[贷款还款计划表]信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0009RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cmisbatch0009List")
    private List<Cmisbatch0009Resp> cmisbatch0009List;

    public List<Cmisbatch0009Resp> getCmisbatch0009List() {
        return cmisbatch0009List;
    }

    public void setCmisbatch0009List(List<Cmisbatch0009Resp> cmisbatch0009List) {
        this.cmisbatch0009List = cmisbatch0009List;
    }

    @Override
    public String toString() {
        return "Cmisbatch0009RespDto{" +
                "cmisbatch0009List=" + cmisbatch0009List +
                '}';
    }
}  
