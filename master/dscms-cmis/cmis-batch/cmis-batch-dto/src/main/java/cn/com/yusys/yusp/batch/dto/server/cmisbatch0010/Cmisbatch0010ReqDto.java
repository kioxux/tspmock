package cn.com.yusys.yusp.batch.dto.server.cmisbatch0010;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：提供日终调度平台调起批量任务
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0010ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "priFlag")
    private String priFlag;//任务级别
    @JsonProperty(value = "taskDate")
    private String taskDate;//任务日期,格式YYYY-MM-DD

    public String getPriFlag() {
        return priFlag;
    }

    public void setPriFlag(String priFlag) {
        this.priFlag = priFlag;
    }

    public String getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }

    @Override
    public String toString() {
        return "Cmisbatch0010ReqDto{" +
                "priFlag='" + priFlag + '\'' +
                ", taskDate='" + taskDate + '\'' +
                '}';
    }
}
