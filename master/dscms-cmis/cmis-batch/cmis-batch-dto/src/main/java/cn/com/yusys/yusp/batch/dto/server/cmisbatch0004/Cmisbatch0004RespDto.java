package cn.com.yusys.yusp.batch.dto.server.cmisbatch0004;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询[贷款账户主表]和[贷款账户还款表]关联信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0004RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    /**
     * 查询类型:
     * 10:查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>10
     * 05:查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>5
     * 0:无查询条件
     */
    @JsonProperty(value = "queryType")
    private String queryType;
    @JsonProperty(value = "countNum")
    private Integer countNum;//返回条数

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public Integer getCountNum() {
        return countNum;
    }

    public void setCountNum(Integer countNum) {
        this.countNum = countNum;
    }

    @Override
    public String toString() {
        return "Yuspbatch0004RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "queryType='" + queryType + '\'' +
                "countNum='" + countNum + '\'' +
                '}';
    }
}  
