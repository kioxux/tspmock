package cn.com.yusys.yusp.batch.dto.batch;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：运行多个任务
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ParallelRunRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "singleRunRespDtos")
    private java.util.List<SingleRunRespDto> singleRunRespDtos;

    public List<SingleRunRespDto> getSingleRunRespDtos() {
        return singleRunRespDtos;
    }

    public void setSingleRunRespDtos(List<SingleRunRespDto> singleRunRespDtos) {
        this.singleRunRespDtos = singleRunRespDtos;
    }

    @Override
    public String toString() {
        return "ParallelRunRespDto{" +
                "singleRunRespDtos=" + singleRunRespDtos +
                '}';
    }
}
