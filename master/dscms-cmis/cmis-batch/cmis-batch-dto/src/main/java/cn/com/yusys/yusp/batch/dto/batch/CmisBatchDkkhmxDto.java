package cn.com.yusys.yusp.batch.dto.batch;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 贷款客户还款明细
 * @author jijian_yx
 * @date 2021/10/20 21:50
 **/
public class CmisBatchDkkhmxDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 交易流水号 **/
    private String serno;

    /** 借据编号 **/
    private String billNo;

    /** 还款模式 **/
    private String repayMode;

    /** 还款类型 **/
    private String repayWay;

    /** 收到金额 **/
    private BigDecimal repayAmt;

    /** 归还本金 **/
    private BigDecimal hasbcCap;

    /** 归还利息 **/
    private BigDecimal hasbcInt;

    /** 归还罚息 **/
    private BigDecimal hasbcOdint;

    /** 归还复利 **/
    private BigDecimal hasbcCi;

    /** 还款日期 **/
    private String repayTime;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getRepayWay() {
        return repayWay;
    }

    public void setRepayWay(String repayWay) {
        this.repayWay = repayWay;
    }

    public BigDecimal getRepayAmt() {
        return repayAmt;
    }

    public void setRepayAmt(BigDecimal repayAmt) {
        this.repayAmt = repayAmt;
    }

    public BigDecimal getHasbcCap() {
        return hasbcCap;
    }

    public void setHasbcCap(BigDecimal hasbcCap) {
        this.hasbcCap = hasbcCap;
    }

    public BigDecimal getHasbcInt() {
        return hasbcInt;
    }

    public void setHasbcInt(BigDecimal hasbcInt) {
        this.hasbcInt = hasbcInt;
    }

    public BigDecimal getHasbcOdint() {
        return hasbcOdint;
    }

    public void setHasbcOdint(BigDecimal hasbcOdint) {
        this.hasbcOdint = hasbcOdint;
    }

    public BigDecimal getHasbcCi() {
        return hasbcCi;
    }

    public void setHasbcCi(BigDecimal hasbcCi) {
        this.hasbcCi = hasbcCi;
    }

    public String getRepayTime() {
        return repayTime;
    }

    public void setRepayTime(String repayTime) {
        this.repayTime = repayTime;
    }
}
