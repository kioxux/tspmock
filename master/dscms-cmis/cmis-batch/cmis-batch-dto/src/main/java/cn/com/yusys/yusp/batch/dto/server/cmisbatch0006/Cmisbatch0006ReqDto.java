package cn.com.yusys.yusp.batch.dto.server.cmisbatch0006;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
/**
 * 请求Dto： 不定期检查任务信息生成
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cmisbatch0006ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "taskNo")
    private String taskNo;    //  任务编号
    @JsonProperty(value = "checkType")
    private String checkType;//   检查类型
    @JsonProperty(value = "cusId")
    private String cusId;// 客户号
    @JsonProperty(value = "cusName")
    private String cusName;// 客户名称
    @JsonProperty(value = "taskStartDt")
    private String taskStartDt;//任务起始日
    @JsonProperty(value = "rptType")
    private String rptType;// 报告类型 正常 瑕疵 不良
    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getTaskStartDt() {
        return taskStartDt;
    }

    public void setTaskStartDt(String taskStartDt) {
        this.taskStartDt = taskStartDt;
    }

    public String getRptType() {
        return rptType;
    }

    public void setRptType(String rptType) {
        this.rptType = rptType;
    }

    @Override
    public String toString() {
        return "Cmisbatch0006ReqDto{" +
                "taskNo='" + taskNo + '\'' +
                ", checkType='" + checkType + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", taskStartDt='" + taskStartDt + '\'' +
                ", rptType='" + rptType + '\'' +
                '}';
    }
}
