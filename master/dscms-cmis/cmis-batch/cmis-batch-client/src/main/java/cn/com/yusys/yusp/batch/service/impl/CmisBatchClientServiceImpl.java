package cn.com.yusys.yusp.batch.service.impl;


import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0008.Cmisbatch0008ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0008.Cmisbatch0008RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0010.Cmisbatch0010ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0010.Cmisbatch0010RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.Cmisbatch0011ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.Cmisbatch0011RespDto;
import cn.com.yusys.yusp.batch.dto.batch.CmisBatchDkkhmxDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 封装的接口实现类: 批量服务接口
 *
 * @author leehuang
 * @version 1.0
 */
@Component
public class CmisBatchClientServiceImpl implements CmisBatchClientService {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatchClientServiceImpl.class);

    /**
     * 交易码：cmisbatch0001
     * 交易描述：调度运行管理信息查詢
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0001RespDto> cmisbatch0001(Cmisbatch0001ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0001.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0001.value));
        return null;
    }

    /**
     * 交易码：cmisbatch0002
     * 交易描述：查询[核心系统-历史表-贷款账户主表]信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0002RespDto> cmisbatch0002(Cmisbatch0002ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0002.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0002.value));
        return null;
    }

    /**
     * 交易码：cmisbatch0003
     * 交易描述：查询[贷款还款计划表]和[贷款期供交易明细]关联信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0003RespDto> cmisbatch0003(Cmisbatch0003ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0003.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0003.value));
        return null;
    }

    /**
     * 交易码：cmisbatch0004
     * 交易描述：查询[贷款账户主表]和[贷款账户还款表]关联信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0004RespDto> cmisbatch0004(Cmisbatch0004ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0004.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0004.value));
        return null;
    }

    /**
     * 交易码：cmisbatch0005
     * 交易描述：更新客户移交相关表
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0005RespDto> cmisbatch0005(Cmisbatch0005ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0005.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0005.value));
        return null;
    }

    /**
     * 交易码：cmisbatch0006
     * 交易描述：不定期检查任务信息生成
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0006RespDto> cmisbatch0006(Cmisbatch0006ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0006.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0006.value));
        return null;
    }

    /**
     * 交易码：cmisbatch0007
     * 交易描述：不定期分类任务信息生成
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0007RespDto> cmisbatch0007(Cmisbatch0007ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0007.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0007.value));
        return null;
    }

    /**
     * 交易码：cmisbatch0008
     * 交易描述：任务加急初始化数据
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0008RespDto> cmisbatch0008(Cmisbatch0008ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0008.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0008.value));
        return null;
    }

    /**
     * 交易码：cmisbatch0009
     * 交易描述：查询[贷款账户期供表]信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0009RespDto> cmisbatch0009(Cmisbatch0009ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0009.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0009.value));
        return null;
    }

    /**
     * 交易码：cmisbatch0010
     * 交易描述：提供日终调度平台调起批量任务
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0010RespDto> cmisbatch0010(Cmisbatch0010ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0010.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0010.value));
        return null;
    }

    /**
     * 交易码：cmisbatch0011
     * 交易描述：提供日终调度平台查询批量任务状态
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cmisbatch0011RespDto> cmisbatch0011(Cmisbatch0011ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_CMISBATCH0011.key.concat("|").concat(DscmsEnum.TRADE_CODE_CMISBATCH0011.value));
        return null;
    }

    /**
     * 根据借据号获取还款明细
     * @author jijian_yx
     * @date 2021/10/20 22:22
     **/
    @Override
    public ResultDto<List<CmisBatchDkkhmxDto>> getRepayListFromBatch(QueryModel model) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

}
