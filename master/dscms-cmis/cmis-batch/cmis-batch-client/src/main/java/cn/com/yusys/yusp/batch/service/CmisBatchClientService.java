package cn.com.yusys.yusp.batch.service;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0008.Cmisbatch0008ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0008.Cmisbatch0008RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0010.Cmisbatch0010ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0010.Cmisbatch0010RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.Cmisbatch0011ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.Cmisbatch0011RespDto;
import cn.com.yusys.yusp.batch.dto.batch.CmisBatchDkkhmxDto;
import cn.com.yusys.yusp.batch.service.impl.CmisBatchClientServiceImpl;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 封装的接口类: 批量服务接口
 *
 * @author leehuang
 * @version 1.0
 */
@FeignClient(name = "cmis-batch", path = "/api", fallback = CmisBatchClientServiceImpl.class)
public interface CmisBatchClientService {
    /**
     * 交易码：cmisbatch0001
     * 交易描述：调度运行管理信息查詢
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0001")
    public ResultDto<Cmisbatch0001RespDto> cmisbatch0001(Cmisbatch0001ReqDto reqDto);

    /**
     * 交易码：cmisbatch0002
     * 交易描述：查询[核心系统-历史表-贷款账户主表]信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0002")
    public ResultDto<Cmisbatch0002RespDto> cmisbatch0002(Cmisbatch0002ReqDto reqDto);

    /**
     * 交易码：cmisbatch0003
     * 交易描述：查询[贷款还款计划表]和[贷款期供交易明细]关联信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0003")
    public ResultDto<Cmisbatch0003RespDto> cmisbatch0003(Cmisbatch0003ReqDto reqDto);

    /**
     * 交易码：cmisbatch0004
     * 交易描述：查询[贷款账户主表]和[贷款账户还款表]关联信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0004")
    public ResultDto<Cmisbatch0004RespDto> cmisbatch0004(Cmisbatch0004ReqDto reqDto);

    /**
     * 交易码：cmisbatch0005
     * 交易描述：更新客户移交相关表
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0005")
    public ResultDto<Cmisbatch0005RespDto> cmisbatch0005(Cmisbatch0005ReqDto reqDto);

    /**
     * 交易码：cmisbatch0006
     * 交易描述：不定期检查任务信息生成
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0006")
    public ResultDto<Cmisbatch0006RespDto> cmisbatch0006(Cmisbatch0006ReqDto reqDto);

    /**
     * 交易码：cmisbatch0007
     * 交易描述：不定期分类任务信息生成
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0007")
    public ResultDto<Cmisbatch0007RespDto> cmisbatch0007(Cmisbatch0007ReqDto reqDto);

    /**
     * 交易码：cmisbatch0008
     * 交易描述：任务加急
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0008")
    public ResultDto<Cmisbatch0008RespDto> cmisbatch0008(Cmisbatch0008ReqDto reqDto);

    /**
     * 交易码：cmisbatch0009
     * 交易描述：查询[贷款账户期供表]信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0009")
    public ResultDto<Cmisbatch0009RespDto> cmisbatch0009(Cmisbatch0009ReqDto reqDto);

    /**
     * 交易码：cmisbatch0010
     * 交易描述：提供日终调度平台调起批量任务
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0010")
    public ResultDto<Cmisbatch0010RespDto> cmisbatch0010(Cmisbatch0010ReqDto reqDto);

    /**
     * 交易码：cmisbatch0011
     * 交易描述：提供日终调度平台查询批量任务状态
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/batch4inner/cmisbatch0011")
    public ResultDto<Cmisbatch0011RespDto> cmisbatch0011(Cmisbatch0011ReqDto reqDto);

    /**
     * 根据借据号获取还款明细
     * @author jijian_yx
     * @date 2021/10/20 22:18
     **/
    @PostMapping("/batchdkkhmx/getrepaylistfrombatch")
    public ResultDto<List<CmisBatchDkkhmxDto>> getRepayListFromBatch(@RequestBody QueryModel model);
}
