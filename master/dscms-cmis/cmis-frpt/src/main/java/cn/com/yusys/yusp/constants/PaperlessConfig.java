package cn.com.yusys.yusp.constants;

import org.springframework.stereotype.Component;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/6/2823:48
 * @desc 签章属性配置
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Component
public class PaperlessConfig {

    /* 机构编码，需要预先在管理系统中添加此机构的信息，登录管理系统->选择系统管理->选择组织 机构管理->添加机构信息，   */
//    public static final String operatorCode = new String ("019801");// 生产
    public static final String operatorCode = new String ("zjgrcb");// 测试

    /* 渠道编码，需要在无纸化管理系统中添加此渠道的信息，登录管理系统->选择系统管理->选择渠道管理->添加渠道信息，  */
    public static final String channelCode = new String (""); 
    
    /**
     * 接口中的密码是否密文传输，true-密文传输，false或者其他-明文传输，不上送默认为 false
     */
    public static final String isEncrypt = new String ("false");

    // 无纸化基本接口的访问URL
    public static final String url = new String ("/PaperlessServer/PaperlessServlet");
    // 无纸化辅助接口的访问URL
    public static final String assistUrl = new String ("/PaperlessServer/PaperlessAssistServlet");
    // 无纸化扩展接口的访问URL
    public static final String extUrl = new String ("/PaperlessServer/PaperlessExtServlet");
    // 无纸化管理接口的访问URL
    public static final String managerUrl = new String ("/PaperlessServer/PaperlessManagerServlet");
    // 无纸化查询接口的访问URL
    public static final String queryUrl = new String ("/PaperlessServer/PaperlessQueryServlet");
    // 无纸化联合接口的访问URL
    public static final String uniteUrl = new String ("/PaperlessServer/PaperlessUniteServlet");
    // 无纸化云平台的访问URL
    public static final String proxyUrl = new String ("/Proxy/PaperlessServer");
    // 无纸化UA认证访问URL
    public static final String uaUrl = new String ( "/PaperlessServer/IdentityAuthentiServlet");
    //无纸化心跳检测
    public static final String heartUrl = new String ( "/PaperlessServer/PaperlessHeartBeatServlet");
 
    // 
    public static final String keyStorePath = new String ("./TestData/cert/client.jks");
    public static final String keyStorePassword = new String ("11111111");
    public static final String trustStorePath = new String ("./TestData/cert/client.jks");
    public static final String trustStorePassword = new String ("11111111");
    
    public static final String PDF_FILE_SUFFIX = new String ("pdf");
    
    public static final String HTML_FILE_SUFFIX = new String ("html");

    public static final String XML_FILE_SUFFIX = new String ("xml");
    
    public static final String FTL_FILE_SUFFIX = new String ("ftl");
    
    public static final String PNG_FILE_SUFFIX = new String ("png");
    
    public static final String DOCX_FILE_SUFFIX = new String ("docx");
    
    public static final String DOC_FILE_SUFFIX = new String ("doc");
    
    public static final String DEFAULT_CHARSET = new String ("UTF-8");
    
    public static final String HASH_ALG_SHA1= new String ("sha1");
    
    public static final String HASH_ALG_SHA256 = new String ("sha256");
    
    public static final String HASH_ALG_SM3 = new String ("sm3");
    
}
