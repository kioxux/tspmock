package cn.com.yusys.yusp;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * created by Harrison on 2019/07/09
 **/
@SpringBootConfiguration
public class SpringbootConfiguration {
    @Bean("listener")
    public FineServletContextInitializer createServletListenerRegistrationBean() {
        return (new FineServletContextInitializer());
    }
}
