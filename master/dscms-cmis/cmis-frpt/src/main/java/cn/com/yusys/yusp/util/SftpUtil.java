package cn.com.yusys.yusp.util;

import com.fr.ssh.jsch.*;
import org.omg.CORBA.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Properties;


/**
 * SFTP工具类
 */
@Component
public class SftpUtil {
	
    private static final Logger log = LoggerFactory.getLogger(SftpUtil.class);
    
   
    private static  String sftpIp = getSftpIp();
    
  
    private static  Integer sftpPort = getSftpPort();
    
    private static  String sftpUsername = getSftpUsername();
    
    private static  String sftpPassword = getSftpPassword();
    
    private static String getSftpIp() {
    	return sftpIp;
    }
    //@Value("${application.cont.ip}")
    private void setSftpIp(String sftpIp) {
    	this.sftpIp = sftpIp;
    }
    
    private static Integer getSftpPort() {
    	return sftpPort;
    }
    //@Value("${application.cont.port}")
    private void setSftpPort(int sftpPort) {
    	this.sftpPort = sftpPort;
    }
    
    private static String getSftpUsername() {
    	return sftpUsername;
    }
    //@Value("${application.cont.user}")
    private void setSftpUsername(String sftpUsername) {
    	this.sftpUsername = sftpUsername;
    }
    
    private static String getSftpPassword() {
    	return sftpPassword;
    }
    //@Value("${application.cont.password}")
    private void setSftpPassword(String sftpPassword) {
    	this.sftpPassword = sftpPassword;
    }
    
    private Session session;

    private ChannelSftp channel;

    /**
     * 规避多线程并发不断开问题
     */
    private static ThreadLocal<SftpUtil> sftpLocal = new ThreadLocal<>();


    public SftpUtil() {

    }

    public SftpUtil(String host, Integer port, String username, String password) {
        super();
        init(host, port, username, password);
    }

    /**
     * 获取本地线程存储的sftp客户端，使用玩必须调用 release()释放连接
     *
     * @return
     * @throws Exception
     */
    public static SftpUtil getSftpUtil() {
        SftpUtil sftpUtil = sftpLocal.get();
        if (null == sftpUtil || !sftpUtil.isConnected()) {
            sftpLocal.set(new SftpUtil(sftpIp, sftpPort, sftpUsername, sftpPassword));
        }
        return sftpLocal.get();
    }

    /**
     * 获取本地线程存储的sftp客户端，使用玩必须调用 release()释放连接
     *
     * @param host
     * @param port
     * @param username
     * @param password
     * @return
     */
    public static SftpUtil getSftpUtil(String host, Integer port, String username, String password) {
        SftpUtil sftpUtil = sftpLocal.get();
        if (null == sftpUtil || !sftpUtil.isConnected()) {
            log.info("**********建立连接*********host:"+host+"***port:"+port+"**username:"+username+"***password:"+password);
            sftpLocal.set(new SftpUtil(host, port, username, password));
            log.info("**********连接成功*********");
        } else {
            log.info("连接已经存在");
        }
        return sftpLocal.get();
    }

    /**
     * @param file       上传文件
     * @param remoteName 上传文件名字
     * @param remotePath 服务器存放路径，支持多级目录
     * @throws SystemException
     */
    public boolean upload(File file, String remoteName, String remotePath) {
        log.info("上传文件：" +file);
        log.info("上传文件名字：" +remoteName);
        log.info("服务器存放路径：" +remotePath);

        if (channel == null) {
            System.out.println("get sftp connect fail，please reboot sftp client");
            //log.error("获取sftp连接失败，请检查" + sftpIp + +sftpPort + "@" + sftpUsername + "" + sftpPassword + "是否可以访问");
        } else {

            FileInputStream fileInputStream = null;
            try {
                log.info("文件是否存在：" + file.isFile());
                log.info("文件地址：" + file.getAbsolutePath());
                if (file.isFile()) {
                    //服务器要创建的目录
                    String rpath = remotePath;
                    createDir(rpath);
                    channel.cd(remotePath);
                    log.error(remotePath + "  " + remoteName);
                    fileInputStream = new FileInputStream(file);
                    channel.put(fileInputStream, remoteName);
                    return true;
                }
            } catch (FileNotFoundException e) {
                log.error("上传文件没有找到", e.getMessage());
                return false;
            } catch (SftpException e) {
                log.error("upload" + remotePath + e);
                return false;
            } finally {
                try {
                    if (fileInputStream != null) {
                        fileInputStream.close();//这里要关闭文件流
                    } else {
                        log.error("流不存在" + remotePath + "  " + remoteName);
                    }
                    this.closeChannel();
                } catch (IOException e) {
                    log.error("上传文件异常", e);
                }
            }
        }
        return false;
    }

//    public boolean downFile(SftpUtil sftp, String remotePath, String remoteFile, String localFile, boolean closeFlag) throws Exception {
//        boolean flag = false;
//        try {
//            this.channel.cd(remotePath);
//            InputStream input = this.channel.get(remoteFile);
//            this.channel.get
//
//            // 判断本地目录是否存在, 若不存在就创建文件夹
//            if (localFile != null && !"".equals(localFile)) {
//                File checkFileTemp = new File(localFile);
//                if (!checkFileTemp.getParentFile().exists()) {
//                    // 创建文件夹, 如：在f盘创建/TXT文件夹/testTXT/两个文件夹
//                    checkFileTemp.getParentFile().mkdirs();
//                }
//            }
//
//            FileOutputStream out = new FileOutputStream(new File(localFile));
//            byte[] bt = new byte[1024];
//            int length = -1;
//            while ((length = input.read(bt)) != -1) {
//                out.write(bt, 0, length);
//            }
//            input.close();
//            out.close();
//            if (closeFlag) {
//                sftp.disconnect();
//            }
//            flag = true;
//        } catch (SftpException e) {
//            logger.error("文件下载失败！", e);
//            throw new RuntimeException("文件下载失败！");
//        } catch (FileNotFoundException e) {
//            logger.error("下载文件到本地的路径有误！", e);
//            throw new RuntimeException("下载文件到本地的路径有误！");
//        } catch (IOException e) {
//            logger.error("文件写入有误！", e);
//            throw new RuntimeException("文件写入有误！");
//        }
//
//        return flag;
//    }

    /**
     * 下载文件
     *
     * @param sftp           sftp工具类
     * @param remotePath     远程服务器文件路径
     * @param localFilePath  下载到本地的文件路径
     * @param localFileName  下载到本地的文件名称
     * @param closeFlag      true 表示关闭连接 false 表示不关闭连接
     * @return 下载是否成功, true-下载成功, false-下载失败
     * @throws Exception
     */
    public boolean downFile(SftpUtil sftp, String remotePath, String localFilePath, String localFileName, boolean closeFlag) throws Exception {
        boolean flag = false;
        try {
            String sftpRemoteFileName = remotePath;
            log.info("远程服务器文件路径为:[{}],下载到本地的文件路径为:[{}],下载到本地的文件名称为:[{}]", remotePath, localFilePath, localFileName);
            InputStream input = this.channel.get(sftpRemoteFileName);
            String localRemoteFile = localFilePath + localFileName;
            File checkFileTemp = null;
            // 判断本地目录是否存在, 若不存在就创建文件夹
            if (localRemoteFile != null && !"".equals(localRemoteFile)) {
                checkFileTemp = new File(localRemoteFile);
                if (!checkFileTemp.getParentFile().exists()) {
                    // 创建文件夹, 如：在f盘创建/TXT文件夹/testTXT/两个文件夹
                    checkFileTemp.getParentFile().mkdirs();
                }
            }

            FileOutputStream out = new FileOutputStream(new File(localRemoteFile));
            byte[] bt = new byte[1024];
            int length = -1;
            while ((length = input.read(bt)) != -1) {
                out.write(bt, 0, length);
            }
            input.close();
            out.close();
            flag = true;
        } catch (SftpException e) {
            log.error("文件下载失败！", e);
            throw new RuntimeException("文件下载失败！");
        } catch (FileNotFoundException e) {
            log.error("下载文件到本地的路径有误！", e);
            throw new RuntimeException("下载文件到本地的路径有误！");
        } catch (IOException e) {
            log.error("文件写入有误！", e);
            throw new RuntimeException("文件写入有误！");
        } finally {
            sftp.closeChannel();
        }
        return flag;
    }

    /**
     * 创建一个文件目录
     */
    public void createDir(String createpath) {
        try {
            if (isDirExist(createpath)) {
                this.channel.cd(createpath);
                return;
            }
            String pathArry[] = createpath.split("/");
            StringBuffer filePath = new StringBuffer("/");
            for (String path : pathArry) {
                if (path.equals("")) {
                    continue;
                }
                filePath.append(path + "/");
                if (isDirExist(filePath.toString())) {
                    channel.cd(filePath.toString());
                } else {
                    // 建立目录
                    channel.mkdir(filePath.toString());
                    // 进入并设置为当前目录
                    channel.cd(filePath.toString());
                }
            }
            this.channel.cd(createpath);
        } catch (SftpException e) {
            log.error("createDir" + createpath + e);
        }
    }

    /**
     * 判断目录是否存在
     */
    public boolean isDirExist(String directory) {
        boolean isDirExistFlag = false;
        try {
            SftpATTRS sftpATTRS = channel.lstat(directory);
            isDirExistFlag = true;
            return sftpATTRS.isDir();
        } catch (Exception e) {
            if (e.getMessage().toLowerCase().equals("no such file")) {
                isDirExistFlag = false;
            }
        }
        return isDirExistFlag;
    }

    /**
     * 初始化 创建一个新的 SFTP 通道
     *
     * @param host
     * @param port
     * @param username
     * @param password
     */
    private void init(String host, Integer port, String username, String password) {
        try {
            //场景JSch对象
            JSch jSch = new JSch();
            // jsch.addIdentity(); 私钥
            session = jSch.getSession(username, host, port);

            // 第一次登陆时候提示, (ask|yes|no)
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            config.put("compression.s2c", "zlib,none");
            config.put("compression.c2s", "zlib,none");
            session.setConfig(config);
            //设置超时
//            session.setTimeout(10*1000);
            //设置密码
            session.setPassword(password);
            session.connect();


            //打开SFTP通道
            channel = (ChannelSftp) session.openChannel("sftp");
            //建立SFTP通道的连接
            channel.connect();
            // 失败重试2次  失败不管了，只发送一次 失败回复  并行调用所有节点
        } catch (JSchException e) {
            log.error("init话sftp异常，可能是获得连接错误，请检查用户名密码或者重启sftp服务" + e);
        }
    }

    /**
     * 是否已连接
     *
     * @return
     */
    private boolean isConnected() {
        return null != channel && channel.isConnected();
    }

    /**
     * 关闭通道
     */
    public void closeChannel() {
        if (null != channel) {
            try {
                channel.disconnect();
            } catch (Exception e) {
                log.error("关闭SFTP通道发生异常:", e);
            }
        }
        if (null != session) {
            try {
                session.disconnect();
            } catch (Exception e) {
                log.error("SFTP关闭 session异常:", e);
            }
        }
    }

    /**
     * 每次连接必须释放资源，类似OSS服务
     * 释放本地线程存储的sftp客户端
     */
    public static void release() {
        if (null != sftpLocal.get()) {
            sftpLocal.get().closeChannel();
            sftpLocal.set(null);
        }
    }

    /**
     * 删除指定目录文件
     *
     * @param filePath 删除文件路径
     * @return
     */
    public Boolean del(String filePath) {
        boolean flag = false;
        try {
            channel.rm(filePath);
            flag = true;
        } catch (SftpException e) {
            flag = false;
            log.error("删除文件错误报告： " + e);
        }
        return flag;
    }


}

