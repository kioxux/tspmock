package cn.com.yusys.yusp.service;


import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import cn.com.yusys.yusp.util.SftpUtil;
import com.finebi.third.jooq.tools.json.JSONObject;
import com.fr.general.ModuleContext;
import com.fr.io.TemplateWorkBookIO;
import com.fr.io.exporter.PDFExporter;
import com.fr.main.impl.WorkBook;
import com.fr.main.workbook.ResultWorkBook;
import com.fr.report.module.EngineModule;
import com.fr.stable.WriteActor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;


/**
 * @author xll
 * @version 1.0.0
 * @date 2021/7/15 23:48
 * @desc 生成合同PDF工具类
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CreateFrptPdfService {

    private static final Logger log = LoggerFactory.getLogger(CreateFrptPdfService.class);

    public JSONObject createFrptPdf(FrptPdfArgsDto frptPdfArgsDto) {
        JSONObject result = new JSONObject();
        try {
            log.info("++++++++++++++++++++++++++生成合同PDF开始++START+++++++++++++++++++++++++++");
            // 公共参数
            String TempleteName = frptPdfArgsDto.getPdfFileName();// 合同模板名字
            String saveFileName = frptPdfArgsDto.getNewFileName();//另保存名字 （模板名字+流水号命名,保证唯一性）
            String ip = frptPdfArgsDto.getIp();//IP地址
            int port = frptPdfArgsDto.getPort();//端口
            String username = frptPdfArgsDto.getUserName();//用户
            String password = frptPdfArgsDto.getPassWord();//密码
            String filePath = frptPdfArgsDto.getPath();//上传目标地址路径
            String localPath = frptPdfArgsDto.getLocalPath();//本地服务生成的路径

            //************************************开始生成PDF*********************************************
            log.info("+++++++++++++++++调用生成pdf方法+++++++++++++++++++++++");
            if ("10.28.110.45".equals(ip)) {//测试环境
                localPath = "/home/dscms/upload/";
            } else {
                localPath = filePath;
            }

            // 检查文件路径地址是否存在，不存在则生成地址
            File ff = new File(filePath);
            if (!ff.exists()) {
                ff.mkdirs();
            }
            //报表查询参数
            HashMap<String, Object> parameterMap = new HashMap<String, Object>();
            parameterMap = frptPdfArgsDto.getMap();
            createPDF(TempleteName, saveFileName, parameterMap, localPath);

            //****************************上传文件开始**************************
            SftpUtil sftpUtil = new SftpUtil();
            port = 22;//sftp
            sftpUtil = sftpUtil.getSftpUtil(ip, port, username, password);
            log.info("**********连接成功，开始上传*********");
            String localFile = localPath + saveFileName + ".pdf";
            log.info("**********本地文件地址*********" + localFile);
            log.info("**********上传目标地址*********" + filePath);
            boolean falg = sftpUtil.upload(new File(localFile), saveFileName + ".pdf", filePath);
            if (falg) {
                log.info("**********上传成功,开始删除本地文件*********");
                boolean delFalg = false;
                File file = new File(localFile);
                if (file.exists()) {//存在帆软本地,不用删除
                 //   delFalg = file.delete();
                }
                if (delFalg) {
                    log.info("**********上传成功,删除本地服务器存储的文件成功*********");
                } else {
                    log.info("**********上传成功,删除本地服务器存储的文件失败*********");
                }
            } else {
                log.info("**********上传失败*********");
            }
            //*************************调用成功，上传成功，返回结果*******************
            result.put("code", 0);
            result.put("msg", "SUCCESS");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 帆软报表转pdf
     *
     * @param pdfId        模板文件名称(含路径)
     * @param cptId        需要生成的PDF文件名称
     * @param parameterMap 报表参数信息
     * @param filePath     PDF生成路径地址
     */
    public void createPDF(String pdfId, String cptId, HashMap<String, Object> parameterMap, String filePath) throws Exception {
        // 设置环境
        ModuleContext.startModule(EngineModule.class.getName());

        //1、生成本地文件
        File ff = new File(filePath);
        ff.mkdirs();

        //2、调用帆软的生成pdf的方法生成PDF
        WorkBook workBook = (WorkBook) TemplateWorkBookIO.readTemplateWorkBook(pdfId);

        String fileName = cptId + ".pdf";
        File file1 = new File(filePath);
        file1.mkdirs();
        File file2 = new File(filePath + fileName);
        if (file2.exists())
            file2.delete();

        FileOutputStream outputStream = null;
        try {
            file2.createNewFile();
            outputStream = new FileOutputStream(file2);
            PDFExporter pe = new PDFExporter();
            ResultWorkBook execute = workBook.execute(parameterMap, new WriteActor());
            pe.export(outputStream, execute);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            outputStream.flush();
            outputStream.close();
        }
    }
}
