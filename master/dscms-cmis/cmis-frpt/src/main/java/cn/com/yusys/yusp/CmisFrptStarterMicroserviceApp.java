package cn.com.yusys.yusp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Description:帆软报表工程启动类
 * @Auth：顾银华
 * @Create Date:2021-05-10 13:52:57
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@EnableFeignClients("cn.com.yusys.yusp")
public class CmisFrptStarterMicroserviceApp {
    public static void main(String[] args) {
        SpringApplication.run(CmisFrptStarterMicroserviceApp.class, args);
    }
}