package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.CfcaArgsDto;
import cn.com.yusys.yusp.service.CfcaElectronicSealService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/6/2823:48
 * @desc 签章服务
 * @修改历史: 修改时间    修改人员    修改原因
 */
@RestController
@RequestMapping("/api/frpt")
public class CfcaElectronicSealResource {
    private static Logger logger = LoggerFactory.getLogger(CfcaElectronicSealResource.class);

    @Autowired
    private CfcaElectronicSealService cfcaElectronicSealService;


    /**
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Boolean>
     * @author 王玉坤
     * @date 2021/7/10 17:52
     * @version 1.0.0
     * @desc 调用签章系统
     * 1、根据报表模板生成pdf文件
     * 2、将生成的pdf文件前往签章系统盖章
     * 3、生成盖章后的文件
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addElectronicSeal")
    public ResultDto<String> addElectronicSeal(@RequestBody CfcaArgsDto cfcaArgsDto) {
        return cfcaElectronicSealService.pdfSignSeal(cfcaArgsDto);
    }
    /**
     * @param map（key:path;value:服务器路径）
     * @author cqs
     * @date 2021-8-18 15:06:46
     * @desc 获取服务器文件
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/downloadPdf")
    public void downloadPdf(HttpServletResponse response,@RequestBody Map<String, String> map) {
        String path = map.get("path");
        cfcaElectronicSealService.download(response,path);
    }

    /**
     * 获取机构印章配置,获取到返 Y,未获取返 N
     * @author jijian_yx
     * @date 2021/11/7 14:21
     **/
    @PostMapping("/getOrgElecSealFlag")
    public ResultDto<String> getOrgElecSealFlag(@RequestBody CfcaArgsDto cfcaArgsDto){
        String result = cfcaElectronicSealService.getOrgElecSealFlag(cfcaArgsDto);
        return new ResultDto<>(result);
    }
}
