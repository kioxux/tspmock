package cn.com.yusys.yusp.web.rest;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import cn.com.yusys.yusp.service.CreateFrptPdfService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller("createFrptFdfResource")
@RequestMapping("/api/frpt")
public class CreateFrptPdfResource {

    private static final Logger log = LoggerFactory.getLogger(CreateFrptPdfResource.class);

    @Autowired
    private CreateFrptPdfService cfcaElectronicSealService;

    /**
     * @param
     * @return ResultDto<java.lang.Boolean>
     * @author xll
     * @date 2021/7/16 17:52
     * @version 1.0.0
     * @desc 调用帆软提供方法生成pdf文件
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/createFrptPdf")
    public ResultDto<Boolean> createFrptPdf(HttpServletRequest request, @RequestBody FrptPdfArgsDto frptPdfArgsDto) {
        cfcaElectronicSealService.createFrptPdf(frptPdfArgsDto);
        return new ResultDto<Boolean>();
    }
}
