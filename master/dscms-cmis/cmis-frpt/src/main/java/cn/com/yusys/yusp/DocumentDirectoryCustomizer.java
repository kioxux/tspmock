package cn.com.yusys.yusp;

import com.fr.log.FineLoggerFactory;
import com.fr.third.org.apache.commons.lang3.StringUtils;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

/**
 * @Description: 内嵌Tomcat运行项目目录, 解决帆软报表以SpringBoot集成并以jar包运行时，无法保存数据库连接问题
 * @Author: 顾银华
 * @Create Date: 2021-05-10 23:35:34
 */
@Configuration
public class DocumentDirectoryCustomizer implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {
    @Override
    public void customize(ConfigurableServletWebServerFactory factory) {
        // 获取当前应用启动位置
        ApplicationHome h = new ApplicationHome(getClass());
        // 资源：获取当前启动的jar包
        File jarF = h.getSource();
        // 当前jar包位置为, 开发环境下为：target结尾
        String jarPath = jarF.getParentFile().toString();
        // 因为是jar包启动，无法在内部写入配置文件，因此改成挂载外部资[系统设置为与Jar包同目录]，否则无法保持数据库相关配置
        File file;
        // 定义：当前jar包位置是否在开发工程中或手动添加的特殊目录，即不允许启动jar包生成的文件在target目录下启动
        final String isDevPath = "target";
        // 如果当前启动jar包是在maven构建或是手动新建的target目录下，将配置文件提升一层与cmis-frpt目录齐平，一般出现的会在target目录下，手动通过java -jar启动jar包发生
        if (jarPath.endsWith(isDevPath)) {
            // 在target上一层创建frpt目录
            file = new File(jarF.getParentFile().getParentFile().getPath()+ File.separator + "frpt");
        } else {
            // 如果是自定义存放位置，那么与当前jar包齐平，用于生产部署
            file = new File(jarPath+ File.separator + "frpt");
        }
        // 如果文件夹不存在，那么创建，系统首次启动时
        if (!file.exists()) {
            // 创建文件夹
            file.mkdirs();
        }
        // 将配置文件夹资源位置指定为上文创建
        factory.setDocumentRoot(file);
    }
}