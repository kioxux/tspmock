package cn.com.yusys.yusp.dto;

import java.util.HashMap;

/**
 * @author xll
 * @version 1.0.0
 * @date 2021/7/19 19:27
 * @desc 帆软生成PDF参数Dto
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class FrptPdfArgsDto {
    // 模板文件名称
    private String pdfFileName;
    // 生成新的PDF文件名称
    private String newFileName;
    // 业务流水号
    private String serno;
    // ip地址
    private String ip;
    // 端口号
    private int port;
    // 用户名
    private String userName;
    // 密码
    private String passWord;
    // 文件存储路径（提供给客户端）
    private String path;
    // 本地文件生成路径
    private String localPath;
    // 报表标识
    private String templateId;
    // 报表参数
    private HashMap<String, Object> map;

    public String getPdfFileName() {
        return pdfFileName;
    }

    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public HashMap<String, Object> getMap() {
        return map;
    }

    public void setMap(HashMap<String, Object> map) {
        this.map = map;
    }
}
