package cn.com.yusys.yusp.service;

import cfca.paperless.client.bean.ImageBean;
import cfca.paperless.client.bean.QRCodeBean;
import cfca.paperless.client.bean.SealUserInfo;
import cfca.paperless.client.bean.SignLocation;
import cfca.paperless.client.servlet.PaperlessClient;
import cfca.paperless.client.util.Base64;
import cfca.paperless.client.util.*;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.PaperlessConfig;
import cn.com.yusys.yusp.dto.CfcaArgsDto;
import cn.com.yusys.yusp.dto.CfgOrgElecSeal;
import cn.com.yusys.yusp.util.SftpUtil;
import com.alibaba.fastjson.JSON;
import com.fr.general.ModuleContext;
import com.fr.io.TemplateWorkBookIO;
import com.fr.io.exporter.PDFExporter;
import com.fr.main.impl.WorkBook;
import com.fr.main.workbook.ResultWorkBook;
import com.fr.report.module.EngineModule;
import com.fr.ssh.jsch.SftpException;
import com.fr.stable.WriteActor;
import com.lowagie2.text.pdf.PdfReader;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/6/2823:48
 * @desc 签章工具类
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CfcaElectronicSealService {
    private static Logger logger = LoggerFactory.getLogger(CfcaElectronicSealService.class);

    // PDF存储服务器路径
    private static String filePath;
    private static String unfilePath;

    @Value("${application.qzxt.url}")
    public String protocol;
    @Value("${application.qzxt.operatorCode}")
    public String operatorCode;
    @Value("${sftp.host}")
    private String sftpHost;
    @Value("${sftp.port}")
    private Integer sftpPort;
    @Value("${sftp.username}")
    private String sftpUserName;
    @Value("${sftp.password}")
    private String sftpPassWord;
    @Value("${sftp.home-path}")
    private String sftpHomePath;
    @Value("${sftp.default-path}")
    private String sftpDeaultPath;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * @param cfcaArgsDto
     * @return boolean
     * @author 王玉坤
     * @date 2021/7/10 21:04
     * @version 1.0.0
     * @desc pdf签章
     * 1、根据报表模板生成pdf文件
     * 2、将生成的pdf文件前往签章系统盖章
     * 3、生成盖章后的文件
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<String> pdfSignSeal(CfcaArgsDto cfcaArgsDto) {
        ResultDto<String> resultDto = new ResultDto<>();
        //**********************查询配置文件信息************************
        // 1、查询机构配置文件
        Map<String, String> map = new HashMap<>();
        map.put("orgCode", cfcaArgsDto.getManagerBrId());
        map.put("isBegin", "1");// 1--启用
        ResultDto<List<CfgOrgElecSeal>> listResultDto = iCmisCfgClientService.selectByCondition(map);
        if (Objects.isNull(listResultDto) || Objects.isNull(listResultDto.getData()) || listResultDto.getData().size() < 1) {
            return resultDto.data("false").code(9999).message("未查询到对应机构的专用章！");
        }
        // 获取机构配置
        // 通过fein接口获取的数据为linkhashmap，无法强转为CfgOrgElecSeal对象，通过转义完成对象装载
        List<CfgOrgElecSeal> list = JSON.parseArray(JSON.toJSONString(listResultDto.getData()), CfgOrgElecSeal.class);
        CfgOrgElecSeal cfgOrgElecSeal = list.get(0);

        // 2、帆软生成PDF文件
        // 定义变量
        String file = cfcaArgsDto.getTemplateId();//模板文件名称
        String newFile = cfcaArgsDto.getPdfFileName();//需要生成的文件名称

        /********************获取服务器配置文件路径************************/
        logger.info("***获取服务器配置文件路径IP地址:" + sftpHost + "**用户名:" + sftpUserName + "**密码:" + sftpPassWord + "**上传路径:" + sftpHomePath + sftpDeaultPath);
        //filePath = "F:/";//生成后文件的存放路径
        filePath = sftpHomePath + sftpDeaultPath + "/signfiles/";
        unfilePath = sftpHomePath + sftpDeaultPath + "/unsignfiles/";
        // 生成未签章本地文件夹
        File ff = new File(filePath);
        if (!ff.exists()) {
            ff.mkdirs();
        }
        /*********************删除***********************/

        try {
            this.createPDF(file, newFile, cfcaArgsDto.getMap(), unfilePath);
        } catch (Exception e) {
            return resultDto.data("false").code(9999).message("生成PDF文件失败！");
        }

        // 3、查询签章配置信息获取签章模板编号 暂时挡板
        // 判断是否是合同章还是抵押登记章 01--借款合同 02--抵押登记
        String sealCode = Objects.equals("1", cfcaArgsDto.getContType()) ? cfgOrgElecSeal.getSealCode() : cfgOrgElecSeal.getGaurSealCode();
        String pawd = Objects.equals("1", cfcaArgsDto.getContType()) ? cfgOrgElecSeal.getSealPassword() : cfgOrgElecSeal.getGaurSealPassword();
        cfcaArgsDto.setSealCode(sealCode);
        cfcaArgsDto.setSealPassword(pawd);

        // 4、前往签章系统生成签章文件
        Boolean result = this.addElectronicSeal(cfcaArgsDto);

        // 5、上传签章后的文件至共享目录
        // 将本地文件上传至帆软文件服务器
        logger.info("**********将签章文件上传至帆软文件服务器开始*********");
        SftpUtil sftpUtil = new SftpUtil();
        sftpUtil = sftpUtil.getSftpUtil(sftpHost, sftpPort, sftpUserName, sftpPassWord);
        boolean falg = sftpUtil.upload(new File(filePath + cfcaArgsDto.getPdfFileName()), cfcaArgsDto.getPdfFileName(), sftpHomePath + sftpDeaultPath + "/");
        if (falg) {
            logger.info("**********文件上传成功*********");
        } else {
            logger.info("**********文件上传失败*********");
        }

        return resultDto.data(sftpHomePath + sftpDeaultPath + "/" + newFile).code(result ? 0 : 9999).message(result ? "获取签章成功！" : "获取签章失败！");
    }

    /**
     * @param cfcaArgsDto
     * @return java.lang.String
     * @author 王玉坤
     * @date 2021/7/10 19:56
     * @version 1.0.0
     * @desc 调用电子签章系统对信贷电子合同加盖电子签章
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Boolean addElectronicSeal(CfcaArgsDto cfcaArgsDto) {
        // 待盖章pdf文件名称
        String pdfFileName = cfcaArgsDto.getPdfFileName();
        // 盖章客户经理
        String managerId = cfcaArgsDto.getManagerId();
        // 盖章机构
        String managerBrId = cfcaArgsDto.getManagerBrId();
        // 印章编码
        String sealCode = cfcaArgsDto.getSealCode();
        // 印章密码
        String sealPassword = cfcaArgsDto.getSealPassword();
        // 业务流水号
        String sealserno = cfcaArgsDto.getSealserno();
        // 骑缝章标识
        boolean crossSealFlag = cfcaArgsDto.getCrossSealFlag();
        // 专用章标识
        boolean keySealFlag = cfcaArgsDto.getKeySealFlag();
        // 盖章关键字，用于此处盖章
        String keyWord = cfcaArgsDto.getKeyWord();//领导意见

        logger.info("电子印章签订START，印章编码【{}】，骑缝章标识【{}】，银行公章标识【{}】", sealCode, crossSealFlag, keySealFlag);

        // 成功标识
        Boolean flag = false;
        // 待签章文件
        String pdfFilePath = unfilePath + pdfFileName;
        try {
            // 判断文件格式是否正确
            String pdfSuffix = IoUtil.getFileNameSuffix(pdfFilePath);
            if (!PaperlessConfig.PDF_FILE_SUFFIX.equals(pdfSuffix)) {
                throw new Exception("所选文件不是pdf文件");
            }

            // 读取pdf流
            byte[] pdfFileData = FileUtils.readFileToByteArray(new File(pdfFilePath));

            //防伪图片合成
            //byte[] pngPdfDataTemp = synthesizeAutoBusinessPdf(pdfFileData, sealCode, sealPassword, managerId, managerBrId);

            byte[] pdfDataTemp = null;
            if (keySealFlag) {
                //关键字盖章
                pdfDataTemp = sealAutoPdf(pdfFileData, sealCode, sealPassword, keyWord, managerId, managerBrId, sealserno);
            }
            if (crossSealFlag) {
                //自动骑缝章
                //pdfDataTemp = sealAutoCrossPdf(keySealFlag ? pdfDataTemp : pngPdfDataTemp, sealCode, sealPassword, managerId, managerBrId);
                pdfDataTemp = sealAutoCrossPdf(pdfFileData, sealCode, sealPassword, managerId, managerBrId);
            }

            String errorRsString = isError(pdfDataTemp);
            // 处理结果为正常，保存签章后的PDF文件到本地目录下
            if ("".equals(errorRsString)) {
                logger.info("电子印章签订END OK，印章编码【{}】，骑缝章标识【{}】，银行公章标识【{}】", sealCode, crossSealFlag, keySealFlag);

                // 删除文件
                //(new File(pdfFilePath)).delete();

                // 填充结果文件
                IoUtil.write(filePath + pdfFileName, pdfDataTemp);
                flag = true;
            } else {
                // 处理结果为异常，打印出错误码和错误信息
                System.out.println("addElectronicSeal END NG");
                logger.info("电子印章签订失败END ，印章编码【{}】，骑缝章标识【{}】，银行公章标识【{}】", sealCode, crossSealFlag, keySealFlag);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * pdf获取关键字所在坐标
     *
     * @param pdfFilePath 目标pdf路径地址
     * @param Keyword     关键字
     */
    public void GetKeywordLocationsInPdf(String pdfFilePath, String Keyword) throws Exception {
        try {

            PaperlessClient clientBean = new PaperlessClient(protocol + PaperlessConfig.assistUrl, 10000, 60000);// 无纸化辅助接口的访问URL
            byte[] pdf = FileUtils.readFileToByteArray(new File(pdfFilePath));

            // 取得转换后的PDF文件数据
            String result = clientBean.getKeywordLocationsInPdf(pdf, Keyword, operatorCode);

            String code = StringUtil.getNodeText(result, "Code");
            if (!code.equals("200")) {
                System.out.println("getKeywordLocationsInPdf");
                System.out.println(result);
            } else {
                Object[] locationArr = StringUtil.getXmlFieldArr(result, "Location");
                for (Object obj : locationArr) {
                    System.out.println("Location:" + obj.toString());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 关键字盖章
     * Pdf自动化签章接口。
     * 1，操作对象为本地待处理的单个PDF文件。
     * 2，使用服务端管理页面中预先制作的印章进行签章。
     * 3，签章类型支持4种，1=空白标签签章,2=坐标签章,3=关键字签章，4=位置标识。
     * 4，返回成功签章的PDF文件。
     *
     * @param pdfFileData   pdf文件流
     * @param sealCode      签章编号
     * @param sealPassword  签章密码
     * @param keyWord       关键字
     * @param manager_id
     * @param manager_br_id
     * @param Sealserno     业务码
     * @return
     */
    public byte[] sealAutoPdf(byte[] pdfFileData, String sealCode, String sealPassword, String keyWord, String manager_id, String manager_br_id, String Sealserno) throws Exception {
        byte[] sealedPdfData = null;
        try {
            long timeStart = System.currentTimeMillis();// 开始时间
            logger.info("Pdf自动化关键字签章SealAutoPdfTest01 START");
            PaperlessClient clientBean = new PaperlessClient(protocol + PaperlessConfig.url, 10000, 60000);// 无纸化基本接口的访问URL

            String savedPdfId = "";
            if (StringUtil.isNotEmpty(savedPdfId) && (savedPdfId.length() < 18 || savedPdfId.length() > 32)) {
                throw new Exception("savedPdfId 的长度必须在18-32之间，否则pdf参数不被认为是临时ID");
            }

            // 印章编码和印章密码，需要在无纸化管理系统中添加此印章的信息，登录管理页面->选择电子印章管理->选择印章管理->添加印章信息（需要先添加相应的印模信息和印章证书信息）
            // 签章杂项信息
            // ************************************************************
            SealUserInfo sealUserInfo = new SealUserInfo(manager_id, manager_br_id, "银行盖章");// 签章人，签章地点，签章理由
            // 透明度0-1.0f，默认为1.0f，可以为空
            //sealUserInfo.setFillOpacity(1.0f);
            // 是否显示，true or false，默认为true，可以为空
            sealUserInfo.setVisible(true);
            // 业务码：可以是验证码、查询码，可以为空
            sealUserInfo.setBusinessCode(Sealserno);
            // 业务码显示样式，可以为空
            sealUserInfo.setBusinessCodeStyle("font-size:4;x-ratio:0.255;y-ratio:0.9");
            // 设置印章显示大小，可以不设置。比如：100，代表100px。比如需要把章显示为4cm（圆章且DPI为96）时，传入151即可。（并且imageScale配置为0.87）
            sealUserInfo.setSealImageSize(117);
            // ************************************************************

            // 签章类型（不能为空），1=空白标签签章,2=坐标签章,3=关键字签章，4=位置标识
            //************************************************************
            // 1=按空白标签域签章
            //SignLocation signLocation = new SignLocation();
            //signLocation.setType("1");

            // 2=按坐标签章
            // 页数，按坐标签章时不能为空；
            // 左侧的x坐标（单位：像素）；左侧的y坐标（单位：像素）；
            //SignLocation signLocation = new SignLocation(1, 110, 10);

            // 3=按关键字签章
            // 关键字，按关键字签章时不能为空；
            // 位置风格：（上:U；下:D；左:L；右:R；中:C）；默认：C；
            // 横轴偏移，默认为0（单位：像素）；纵轴偏移，默认为0（单位：像素）
            SignLocation signLocation = new SignLocation(keyWord, "R", 10, -10);
            // 关键字位置索引（1：第一个位置；2：第二个位置；0：默认全部位置盖章，支持1、2、1-3、3-9格式，如果输入非法数字或者负数当做0处理，如果输入的数字大于关键字数量时就在最后一个位置盖章处理）
            signLocation.setKeywordPositionIndex("0");

            // 4=按位置标识签章（需要提前在服务端管理页面上登录业务模板和签章位置标识信息）
            // SignLocation signLocation = new SignLocation("BizLoc001");// 签章位置标识编码
            // ************************************************************

            // 生成签章策略文件
            String sealStrategyXml = StrategyUtil.createSealStrategyXml(sealCode, sealPassword, sealUserInfo, signLocation);

            // 渠道编码，可以为空
            String channelCode = PaperlessConfig.channelCode;

            // 获取时间戳的方式。默认值为0。0：实时访问CFCA 时间戳服务；1：使用从CFCA购置并在本地部署的时间戳服务器产品；
            String timestampChannel = "1";
            //密码是否密文传输，默认为false-原文传输，true-密文传输
            String isEncrypt = PaperlessConfig.isEncrypt;

            // 取得签章后的PDF文件数据
            sealedPdfData = clientBean.sealAutoPdf(pdfFileData, savedPdfId, sealStrategyXml, operatorCode, channelCode, timestampChannel, isEncrypt);
            String errorRsString = PaperlessUtil.isError(sealedPdfData);

            // 处理结果为正常，保存签章后的PDF文件到本地目录下
            if ("".equals(errorRsString)) {
                logger.info("Pdf自动化关键字签章SealAutoPdfTest01 END OK");
            } else {
                // 处理结果为异常，打印出错误码和错误信息
                logger.info("Pdf自动化关键字签章SealAutoPdfTest01 END 失败");
            }

            long timeEnd = System.currentTimeMillis();// 结束时间
            logger.info("##########生成电子签章结束，耗时time used:【{}】", timeEnd - timeStart);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sealedPdfData;
    }

    /**
     * Pdf自动化骑缝章签章接口
     *
     * @param pdfFileData  pdf文件流
     * @param sealCode     签章编号
     * @param sealPassword 签章密码
     * @param managerId
     * @param managerBrId
     * @return
     */
    public byte[] sealAutoCrossPdf(byte[] pdfFileData, String sealCode, String sealPassword, String managerId, String managerBrId) throws Exception {
        logger.info("PPdf自动化骑缝章签章sealAutoCrossPdf START");
        byte[] sealedPdfData = null;
        try {
            // 无纸化辅助接口的访问URL
            PaperlessClient clientBean = new PaperlessClient(protocol + PaperlessConfig.assistUrl, 10000, 60000);

            //由于cfca骑缝章单双页加盖暂不支持图片大小,所有先获取章图片信息，再调整大小发送cfca加盖骑缝章
            byte[] sealImage = imageData(sealCode);
            sealImage = ImageUtil.resizeImage(sealImage, 150, 150);

            int fromPage = 1;
            int toPage = 2;
            PdfReader pdfReader = null;
            try {
                pdfReader = new PdfReader(pdfFileData);
            } catch (Exception e) {
                throw new Exception("pdf文档有误");
            }
            int pageCount = pdfReader.getNumberOfPages();

            if (fromPage > pageCount || toPage > pageCount) {
                throw new Exception("签章页码不能大于pdf文档总页数");
            }
            if (fromPage >= toPage) {
                throw new Exception("骑缝签章起始页码不能小于等于结束页码");
            }

            // 印章编码和印章密码，需要在无纸化管理系统中添加此印章的信息，登录管理页面->选择电子印章管理->选择印章管理->添加印章信息（需要先添加相应的印模信息和印章证书信息）
            //String sealCode = "yz202011468636865";
            //String sealPassword =  "123456";//PwdEncryptUtil.encrypto("cfca1234");
            // 签章的用户信息 <!-- 签章人，签章地点，签章理由，可以为空 -->
            SealUserInfo sealUserInfo = new SealUserInfo(managerId, managerBrId, "骑缝章");
            // 签章的位置信息，骑缝章签章起始页、结束页设置
            SignLocation signLocation = new SignLocation(fromPage, pageCount);

            // 骑缝章风格
            // 1：上下各一半		2：左右各一半		3：上下骑缝		4：左右骑缝		5：左骑缝		6：右骑缝
            String crossStyle = "6";

            //只针对于签章风格5:(左骑缝)和6:(右骑缝)生效
            // 0:全部指定页范围；1：指定页范围中的奇数页；2：指定页范围中的偶数页
            String oddEvenOption = "0";

            // 渠道编码，可以为空
            String channelCode = PaperlessConfig.channelCode;

            // 获取时间戳的方式。默认值为0。0：实时访问CFCA 时间戳服务；1：使用从CFCA购置并在本地部署的时间戳服务器产品；
            String timestampChannel = "0";

            //版本号（不为空时版本号必须与当前服务一致，密码须上送密文，如果为空，则密码须是原文）
            String isEncrypt = PaperlessConfig.isEncrypt;

            // 设置印章显示大小，可以不设置。比如：100，代表100px。比如需要把章显示为4cm（圆章且DPI为96）时，传入151即可。（并且imageScale配置为0.87）
            //sealUserInfo.setSealImageSize(100);
            String crossSealStrategyXml = StrategyUtil.createCrossSealStrategyXml(sealCode, sealPassword, sealImage, sealUserInfo, signLocation, crossStyle, -1, -1, oddEvenOption);

            // 取得骑缝章签章后的PDF文件数据
            sealedPdfData = clientBean.sealAutoCrossPdfNew(pdfFileData, "", crossSealStrategyXml, operatorCode, channelCode, timestampChannel, isEncrypt);

            String errorRsString = PaperlessUtil.isError(sealedPdfData);
            // 处理结果为正常，保存骑缝章签章后的PDF文件到本地目录下
            if ("".equals(errorRsString)) {
                logger.info("Pdf自动化骑缝章签章sealAutoCrossPdf END OK");
            } else {
                // 处理结果为异常，打印出错误码和错误信息
                logger.info("Pdf自动化骑缝章签章sealAutoCrossPdf END 失败，失败信息【{}】", new String(sealedPdfData, "UTF-8"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sealedPdfData;
    }

    /**
     * @param sealCode
     * @return byte[]
     * @author 王玉坤
     * @date 2021/7/10 16:24
     * @version 1.0.0
     * @desc 获取骑缝章信息并设置大小
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private byte[] imageData(String sealCode) throws Exception {
        byte[] sealImageData = null;
        try {
            PaperlessClient clientBean = new PaperlessClient(protocol + PaperlessConfig.assistUrl, 10000, 60000);// 无纸化辅助接口的访问URL
            String result = clientBean.getSealInfo(sealCode, operatorCode);

            String code = StringUtil.getNodeText(result, "Code");
            if (!code.equals("200")) {
                logger.info("获取骑缝章信息ok,result【{}】", result);
            } else {
                String sealIData = StringUtil.getNodeText(result, "Image");
                BASE64Decoder decoder = new BASE64Decoder();
                sealImageData = decoder.decodeBuffer(sealIData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sealImageData;
    }


    /**
     * Pdf自动化合成业务数据接口
     * 1，操作对象为本地待处理的单个PDF文件。
     * 2，合成内容包含：文本域内容、文本、图片和二维码。
     * 3，返回成功合成内容的PDF文件
     *
     * @param pdfFileData  pdf文件流
     * @param sealCode     签章编号
     * @param sealPassword 签章密码
     * @param managerId
     * @param managerBrId
     * @return
     */
    public byte[] synthesizeAutoBusinessPdf(byte[] pdfFileData, String sealCode, String sealPassword, String managerId, String managerBrId) throws Exception {
        byte[] synthesizeResultData = null;
        logger.info("Pdf自动化合成防伪图片SynthesizeAutoBusinessPdfTest01 START");
        try {
            // 开始时间
            long timeStart = System.currentTimeMillis();

            // 获取请求实例
            PaperlessClient clientBean = new PaperlessClient(protocol + PaperlessConfig.url, 10000, 60000);

            String savedPdfId = "";//GUID.generateId();

            // 需要合成到PDF中的图片和二维码，可为空
            List<ImageBean> imageBeanList = getImageBeanList();
            String imageBeanListXml = buildImageBeanListXml(imageBeanList, null);

            //合成二维码后的PDF文件数据
            synthesizeResultData = clientBean.synthesizeAutoBusinessPdf(pdfFileData, savedPdfId, "", "", imageBeanListXml,
                    operatorCode, "");

            String errorRsString = PaperlessUtil.isError(synthesizeResultData);
            if ("".equals(errorRsString)) {
                logger.info("Pdf自动化合成防伪图片SynthesizeAutoBusinessPdfTest01 END OK");
            } else {
                logger.info("Pdf自动化合成防伪图片失败，返回信息【{}】", new String(synthesizeResultData, "UTF-8"));
            }

            long timeEnd = System.currentTimeMillis();// 结束时间
            logger.info("##########自动化合成防伪图片结束，耗时time used:【{}】", timeEnd - timeStart);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return synthesizeResultData;
    }

    /**
     * @param
     * @return java.util.List<cfca.paperless.client.bean.ImageBean>
     * @author 王玉坤
     * @date 2021/7/10 16:31
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private List<ImageBean> getImageBeanList() throws Exception {
        //String cmisWebRoot = System.getProperty("ROOTPATH").replace(File.separatorChar, '/');// 获取此应用根路径, 如E:\workspace\EMP\cmis-main\WebContent\
        String cmisWebRoot = "F:/sys3/dscms-cmis/cmis-frpt/src/main/webapp/help/picture/";
        String path = cmisWebRoot + "chart/AxisCustom2.png";
        List<ImageBean> imageBeanList = new ArrayList<ImageBean>();

        byte[] imageData = FileUtils.readFileToByteArray(new File(path));// 图片数据
        String imagePath = Base64.toBase64String(path.getBytes("UTF-8"));// 服务器端存储的图片文件路径

        ImageBean imageBean1 = new ImageBean("", "", 0, "30", "30");// 传入图片数据，根据坐标定位进行合成
        imageBean1.setImageData(imageData);
        imageBeanList.add(imageBean1);

        return imageBeanList;
    }

    /**
     * @param rs
     * @return java.lang.String
     * @author 王玉坤
     * @date 2021/7/10 16:42
     * @version 1.0.0
     * @desc 处理错误信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private String isError(byte[] rs) throws UnsupportedEncodingException {
        if ((null == rs) || (rs.length == 0)) {
            logger.info("PDF文件数据为空");
            return "数据为空错误";
        }
        if (rs.length < 16) {
            String errorInfo = new String(rs, "UTF-8");
            logger.info("PDF文件数据长度：{}", errorInfo);
            return new String(rs, "UTF-8");
        }

        byte[] successHeader = new byte[16];
        System.arraycopy(rs, 0, successHeader, 0, 16);
        if ("<Code>200</Code>".equals(new String(successHeader, "UTF-8"))) {
            return "";
        }

        if (rs.length < 24) {
            String errorInfo = new String(rs, "UTF-8");
            logger.info("PDF文件数据长度：{}", errorInfo);
            return new String(rs, "UTF-8");
        }
        byte[] header = new byte[24];
        System.arraycopy(rs, 0, header, 0, 24);
        String headerStr = new String(header, "UTF-8");
        if ("<Result><Code>200</Code>".equals(headerStr)) {
            return "";
        }

        if ((headerStr.startsWith("<Error>"))
                || (headerStr.startsWith("<Result><Code>"))) {
            String errorInfo = new String(rs, "UTF-8");
            logger.info("PDF文件数据处理：{}", errorInfo);
            return new String(rs, "UTF-8");
        }

        boolean result = isImageOrPdf(rs);
        if (result) {
            return "";
        }
        logger.info("PDF文件数据处理：{}", getResultData(rs));
        return "不是有效的PDF或图片文件";
    }

    /**
     * @param src
     * @return boolean
     * @author 王玉坤
     * @date 2021/7/10 16:43
     * @version 1.0.0
     * @desc 校验文件属性
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private boolean isImageOrPdf(byte[] src) {
        if (src.length < 4) {
            return false;
        }

        PdfReader reader = checkPdf(src);
        if (null != reader) {
            return true;
        }

        byte[] header = new byte[4];
        System.arraycopy(src, 0, header, 0, 4);
        String headerStr = bytesToHexString(header);
        boolean isImage = false;
        if (headerStr != null) {
            if (headerStr.startsWith("FFD8FF"))
                isImage = true;
            else if (headerStr.startsWith("89504E47"))
                isImage = true;
            else if (headerStr.startsWith("47494638"))
                isImage = true;
            else if (headerStr.startsWith("49492A00"))
                isImage = true;
            else if (headerStr.startsWith("424D")) {
                isImage = true;
            }
        }
        return isImage;
    }

    private PdfReader checkPdf(byte[] pdf) {
        return checkPdf(pdf, null);
    }

    private PdfReader checkPdf(byte[] pdf, byte[] pdfOwnerPassword) {
        try {
            if ((pdfOwnerPassword == null) || (pdfOwnerPassword.length == 0)) {
                return new PdfReader(pdf);
            }
            return new PdfReader(pdf, pdfOwnerPassword);
        } catch (Exception e) {
        }
        return null;
    }

    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder();
        if ((src == null) || (src.length <= 0)) {
            return null;
        }
        for (int i = 0; i < src.length; ++i) {
            String hv = Integer.toHexString(src[i] & 0xFF).toUpperCase();
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    private String getResultData(byte[] data) throws UnsupportedEncodingException {
        if (data.length > 1024) {
            return new String(data, 0, 1024, "UTF-8");
        }
        return new String(data, "UTF-8");
    }

    private String buildImageBeanListXml(List<ImageBean> imageBeanList,
                                         List<QRCodeBean> qrCodeBeanList) {
        return buildImageBeanListXml(imageBeanList, qrCodeBeanList, "", "");
    }

    private String buildImageBeanListXml(List<ImageBean> imageBeanList,
                                         List<QRCodeBean> qrCodeBeanList, String savedBizXmlId,
                                         String savedTimeIncrement) {
        StringBuilder imageListXml = new StringBuilder(
                "<ImageList hashAlg=\"sha1\" savedBizXmlId=\"" + savedBizXmlId
                        + "\"  savedTimeIncrement=\"" + savedTimeIncrement
                        + "\">");

        if (imageBeanList != null) {
            for (int i = 0; i < imageBeanList.size(); ++i) {
                ImageBean imageBean = (ImageBean) imageBeanList.get(i);
                imageListXml.append(imageBean.toString());
            }

        }

        if (null != qrCodeBeanList) {
            for (QRCodeBean qrCodeBean : qrCodeBeanList) {
                imageListXml.append("<QRCode>");
                imageListXml.append("<QRCodeContent>"
                        + qrCodeBean.getQrCodeContent() + "</QRCodeContent>");
                imageListXml.append("<Width>" + qrCodeBean.getWidth()
                        + "</Width>");
                imageListXml.append("<Height>" + qrCodeBean.getHeight()
                        + "</Height>");
                imageListXml.append("<PageNo>" + qrCodeBean.getPageNo()
                        + "</PageNo>");
                imageListXml.append("<LX>" + qrCodeBean.getLx() + "</LX>");
                imageListXml.append("<LY>" + qrCodeBean.getLy() + "</LY>");
                imageListXml.append("<TemplateIndex>"
                        + qrCodeBean.getTemplateIndex() + "</TemplateIndex>");
                imageListXml.append("</QRCode>");
            }
        }
        imageListXml.append("</ImageList>");
        return imageListXml.toString();
    }

    /**
     * 帆软报表转pdf
     *
     * @param cptId        模板文件名称(含路径)
     * @param pdfId        需要生成的PDF文件名称
     * @param parameterMap 报表参数信息
     * @param filePath     PDF生成路径地址
     */
    public void createPDF(String cptId, String pdfId, Map<String, Object> parameterMap, String filePath) throws Exception {
        // 1、设置环境
        ModuleContext.startModule(EngineModule.class.getName());

        // 2、生成本地文件夹
        File ff = new File(filePath);
        if (!ff.exists()) {
            ff.mkdirs();
        }

        //3、调用帆软的生成pdf的方法生成PDF
        WorkBook workBook = (WorkBook) TemplateWorkBookIO.readTemplateWorkBook(cptId);
        String fileName = pdfId.toLowerCase().contains(".pdf") ? pdfId : pdfId + ".pdf";
        File file = new File(filePath + fileName);
        if (file.exists())
            file.delete();

        FileOutputStream outputStream = null;
        try {
            logger.info("***************IP地址" + sftpHost + "**用户名" + sftpUserName + "**密码" + sftpPassWord + "**上传路径" + sftpHomePath + sftpDeaultPath);
            file.createNewFile();
            outputStream = new FileOutputStream(file);
            PDFExporter pe = new PDFExporter();
            ResultWorkBook execute = workBook.execute(parameterMap, new WriteActor());
            pe.export(outputStream, execute);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            outputStream.flush();
            outputStream.close();
        }
    }

    /**
     * @param response,path
     * @return void
     * @author cqs
     * @date 2021-8-18 15:06:46
     * @version 1.0.0
     * @desc 获取服务器文件
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void download(HttpServletResponse response, String path) {
        logger.info("pdf文件下载路径: " + path);
        OutputStream toClient = null;
        InputStream fis = null;
        try {
            // path是指欲下载的文件的路径。
            File file = new File(path);
            // 取得文件名。
            String filename = file.getName();

            InputStream fileInputStream = null;
            if (file.exists()) {
                logger.info("当前文件存在该服务器上，无需连接SFTP");
            } else {
                logger.info("通过SFTP获取文件开始");
                SftpUtil sftpUtil = new SftpUtil();
                sftpUtil = sftpUtil.getSftpUtil(sftpHost, sftpPort, sftpUserName, sftpPassWord);
                sftpUtil.downFile(sftpUtil, path, filePath, path.substring(path.lastIndexOf("/"), path.length()),true);
                path = filePath + path.substring(path.lastIndexOf("/"), path.length());
            }

            // 以流的形式下载文件。
            fis = new BufferedInputStream(new FileInputStream(path));
            byte[] buffer = new byte[fis.available()];
            if (fis.read(buffer) > 0) {
                fis.close();
                response.reset();
                response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
                toClient = new BufferedOutputStream(response.getOutputStream());
                response.setContentType("application/pdf;charset=utf-8");
                toClient.write(buffer);
                toClient.flush();
                toClient.close();
            }
        } catch (IOException | SftpException e) {
            logger.error("文件下载异常", e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (toClient != null) {
                try {
                    toClient.close();
                } catch (IOException e) {
                    logger.error("关闭OutputStream异常", e);
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    logger.error("关闭InputStream异常", e);
                }
            }
        }

    }

    /**
     * 获取机构印章配置,获取到返 Y,未获取返 N
     * @author jijian_yx
     * @date 2021/11/7 14:21
     **/
    public String getOrgElecSealFlag(CfcaArgsDto cfcaArgsDto) {
        // 1、查询机构配置文件
        Map<String, String> map = new HashMap<>();
        map.put("orgCode", cfcaArgsDto.getManagerBrId());
        map.put("isBegin", "1");// 1--启用
        ResultDto<List<CfgOrgElecSeal>> listResultDto = iCmisCfgClientService.selectByCondition(map);
        if (Objects.isNull(listResultDto) || Objects.isNull(listResultDto.getData()) || listResultDto.getData().size() < 1) {
            return "N";
        } else {
            return "Y";
        }
    }
}