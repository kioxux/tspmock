package cn.com.yusys.yusp;

import com.fr.startup.FineWebApplicationInitializer;
import org.springframework.boot.web.servlet.ServletContextInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * created by Harrison on 2019/07/09
 **/
public class FineServletContextInitializer implements ServletContextInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        FineWebApplicationInitializer initializer = new FineWebApplicationInitializer();
        initializer.onStartup(servletContext);
    }
}