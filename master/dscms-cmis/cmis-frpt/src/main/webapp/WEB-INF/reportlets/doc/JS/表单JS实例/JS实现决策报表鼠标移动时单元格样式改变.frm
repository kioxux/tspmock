<?xml version="1.0" encoding="UTF-8"?>
<Form xmlVersion="20170720" releaseVersion="10.0.0">
<TableDataMap>
<TableData name="ds1" class="com.fr.data.impl.DBTableData">
<Parameters/>
<Attributes maxMemRowCount="-1"/>
<Connection class="com.fr.data.impl.NameDatabaseConnection">
<DatabaseName>
<![CDATA[FRDemo]]></DatabaseName>
</Connection>
<Query>
<![CDATA[SELECT * FROM 区域销售]]></Query>
<PageQuery>
<![CDATA[]]></PageQuery>
</TableData>
</TableDataMap>
<FormMobileAttr>
<FormMobileAttr refresh="false" isUseHTML="false" isMobileOnly="false" isAdaptivePropertyAutoMatch="false" appearRefresh="false" promptWhenLeaveWithoutSubmit="false" allowDoubleClickOrZoom="true"/>
</FormMobileAttr>
<Parameters/>
<Layout class="com.fr.form.ui.container.WBorderLayout">
<WidgetName name="form"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<ShowBookmarks showBookmarks="false"/>
<Center class="com.fr.form.ui.container.WFitLayout">
<WidgetName name="body"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WAbsoluteBodyLayout">
<WidgetName name="body"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WTitleLayout">
<Listener event="afterinit">
<JavaScript class="com.fr.js.JavaScriptImpl">
<Parameters/>
<Content>
<![CDATA[setTimeout(function() {
//鼠标经过时
$(".x-table.REPORT0table td span.linkspan").mousemove(function() {
//所在单元格字体颜色为红色
$(this).css("color", "red");
//所在单元格背景为蓝色
$(this).css("background-color", "blue");
//所在单元格字体加粗
$(this).css("font-weight", "bold");
//所在单元格添加下划线
$(this).css("text-decoration", "underline");
//所在行单元格字体:11px  
$(this).find("td").css("font-size", "11px");
});
//鼠标点击时
$(".x-table.REPORT0table td span.linkspan").mousedown(function() {
//所在单元格字体颜色为黄色
$(this).css("color", "yellow");
//所在单元格背景为黑色
$(this).css("background-color", "black");
//所在单元格字体加粗
$(this).css("font-weight", "bold");
//所在单元格添加上划线
$(this).css("text-decoration", "overline");
//所在行单元格字体:13px  
$(this).find("td").css("font-size", "13px");
});
//鼠标离开
$(".x-table.REPORT0table td span.linkspan").mouseout(function() {
//所在单元格字体颜色为黑色
$(this).css("color", "black");
//所在单元格背景为白色
$(this).css("background-color", "white");
//所在单元格字体正常
$(this).css("font-weight", "normal");
//所在单元格无下划线
$(this).css("text-decoration", "none");
//所在行单元格字体:9px  
$(this).find("td").css("font-size", "9px");
});
}, 100);]]></Content>
</JavaScript>
</Listener>
<WidgetName name="report0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="report0" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.ElementCaseEditor">
<WidgetName name="report0"/>
<WidgetID widgetID="23ac97c1-cbe0-4a51-af09-0138aa02f059"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<Refresh class="com.fr.plugin.reportRefresh.ReportExtraRefreshAttr" pluginID="com.fr.plugin.reportRefresh.v10">
<Refresh state="0" interval="0.0" refreshArea="" customClass="false"/>
</Refresh>
<FormElementCase>
<ReportPageAttr>
<HR/>
<FR/>
<HC/>
<FC/>
</ReportPageAttr>
<ColumnPrivilegeControl/>
<RowPrivilegeControl/>
<RowHeight defaultValue="723900">
<![CDATA[723900,723900,723900,723900,723900,723900,723900,723900,723900,723900,723900]]></RowHeight>
<ColumnWidth defaultValue="2743200">
<![CDATA[2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200]]></ColumnWidth>
<CellElementList>
<C c="0" r="0">
<O>
<![CDATA[销售区]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="1" r="0">
<O>
<![CDATA[金额]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="0" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="销售区"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<NameJavaScriptGroup>
<NameJavaScript name="动态参数1">
<JavaScript class="com.fr.js.ParameterJavaScript">
<Parameters>
<Parameter>
<Attributes name="p1"/>
<O>
<![CDATA[111]]></O>
</Parameter>
</Parameters>
</JavaScript>
</NameJavaScript>
</NameJavaScriptGroup>
<Expand dir="0"/>
</C>
<C c="1" r="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="金额"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
</CellElementList>
<ReportAttrSet>
<ReportSettings headerHeight="0" footerHeight="0">
<PaperSetting/>
<Background name="ColorBackground" color="-1"/>
</ReportSettings>
</ReportAttrSet>
</FormElementCase>
<StyleList>
<Style imageLayout="1">
<FRFont name="SimSun" style="0" size="72" foreground="-16776961" underline="1"/>
<Background name="NullBackground"/>
<Border/>
</Style>
</StyleList>
<heightRestrict heightrestrict="false"/>
<heightPercent heightpercent="0.75"/>
<IM>
<![CDATA[]Aiobf;cgE9eH+FqYdSUu):JMcf/h@nnVof^1d7s2Hq_6-6E7"g+$`[H&3.eagkrK@j7Rth8s
66<?n3+P$5etk>"u'9:cX?E&iDuuJHQBQaRdUca[tKmSU:CXJ(_q(H`q@iDlr]A7%:>6!/Id+
gqW2_W`e?$P[V3pqEA+C42t"Z^'8+(Zj.MHUj2SdGDttU:]A"Dqd]A."!1CH&X1Vp%+oIeDL7&
$$J\>WLcD")f`*rd*79S#<ZCp>6lt+9-l$Fr;f>nLKe>++,;AADS3pj9`?@Ip:K2/35IFIaW
#_l'%-qI[Y?t^@7b9;NAuC350Ga>9n,*\1$2_qjo._Sob\+LH[Z%05f5XTKS83D5O1iWQmE?
Gu<-8a"`=nVioTnjD)=fk#THmefr/Up7:@Dh8U:djX):I1S,]A<b,Hf.b;cPC6d?:-^\kQ*AK
2gZq>t4/<8qjpUYtC9'd)2l1EUSaEuOW0*9sJ=Z@6-P[>E7nN'5cPdI>c,Rd)G]Ap!Bd2ULQ_
L=+755AZK]AlX3`JC@Rk;l_cYLdi1.9RN\kQ!ZbP`61Ud<.)Kr>AC!L8ME6+P@[,P]Aj5pCCQm
%e6!eomA<VhSG`2En`i,#fQho.'@T&7+cWS3m_cjh0[-7Mf[de`!/4N@G,r),/#TXRPO,]A"*
.U)'Som:b+1&e(aGIafp2qi+-bpF1$B>CZ2%e9f&,8VU"<G_6A*E;n:6P,lE?p@43,/MTq\9
4o$3L_2Pq]ARe7?33X>KcT.u-41B',9h6eAs3eU:;O6uW[YJ4XZ43k[iffhB/IJN3HOqIZ(U:
AShUm`VJaa,@+D[:"S2R8kfXs&YZa_p.sD;g2k2FJA;9N'nD1#"q?I%f0Y264VJjpctpC#Oa
mo"uIUp#X9l6K`Sb@^NMC=J=$ah.qofKbb.ds!GFc$1$=7C7&TBNF.4K;%TN9e&nLmXi6p8B
A;e?[#:'4'"cf(QO%3NS!]Au!/"Q;5N.sB>='LZV0+eNE]A<_c$Vi7To$ql*GA.I+o,+7=8\c4
-[-.GR(R2m=08o2eaj.e-)866UB,dqsLAh!?Il78[khVkbF%4?>mRAkuO]A6WNtpqa;AE11+7
Z]Ahs0@E&s;_c"]A5M#F7<)b><$SDVr(*s7iS6pZ86?-hRa[E$1!,n6Yf1NQi`Z%nYaP=rpDj,
EL^<FM=5k'.\nnFkg3BDBA!!%IGc1K6h/rY0-.0ZBI`;`D-#NK=%:<9X-T.#&/Y<?dSs05*<
I@B#2mUoVbZIEN2,H-;RhfQET6mKdrVSCa.J`TdP,1P8Hs[sX!cpIHXd3*W2Xq0%I&02&^'a
.BGG]AR"ilGUkX[]Au$J<$*6[/Bk&[!X?Dhs.@8j/A3ALH0B9ug"Zp?:,[rY`:a":NUrs`tcPW
@M0m-a`=5ke%d_=Bf]A)mX)jJ3Ul>e?osLg\gsA3aL=eQ]AY1MOO7U+YtEdki"tiQ%VL_!2'cr
\1T"`3fA5mW93ZI@<*]AH/!m)A'#]AqT+CUR9<A)0mWb&jIML*e\S`Pi6FPD#\UI5;1<7.2Hq^
0eee5/lX-/&p=__PH==Jb-%VhlL8Bd0E+ENY'K1*lG#2,[@?)S&c_CUm+&_X$9No8pfAjYFN
?/aLY"*1b9->.,>;PXKkp.9+!8``oX?o%A^[b$Fjj8CMP//g?c;NF==VQ$\dJNeO7#`^u7]A=
c@C2b$H$4W%p5B2d'M9#;%\(IWB"u>Y-LuaWJ<(@FSJjK!*LL9\TZL64>/4p8Tk^:`7]AoX?m
k<XA0Kp3k\K)b1K]A*N3E<cAKe.ar)I,@%:\[-Yg-ub1a8g#_abdof$T4kMYXN3f(Hm&F7Vf^
rB]AP!r,%/8eW$2(09e35KUR(2X8_N>YtCAurgT._\^jYA.+Bq@46Ml%L6#i3bu6:gUi]A5rep
Yt5XXTjiD9+<:]AQ.nNJh%nM`>Ym@)bja9*6*Am@b5cMN\YB</49TS+ohiZL41?*KuJ?"0IK&
,:9D69?t/cogRPsj^R!o6k0dNf>5U^?qG-7K@<S!DDT'r5g2RJA0.9e)EL9Y1P^c''S'kh>H
`P=$dt>rW;N#A)`,U&#]A^ka[an%q=[M'@/+a<0^&nK6=AR:WKR3L(ALUSs\9#:K$/>`,@S*j
TehX]A]Ak%L+[jJ2Ffm]AK[1?Q1S,k7@-ShVOV(N6.-k\[9P&`(R<1.(9ss-kL>e/qHiQ:1`IQ=
XXBL[LWrDN\U(i7:g.NA"]A%.\40o>VTfmCq.u!G$\e=f)4>Xci"AV2hGNRkTY@Vi,)C:do7V
WJJjARLSGUUa=3CjTX2A-pKJrtO2e]An+n0OcHd%&?i-I5GOT"thq>Cie)7105%*U@A^j.KNJ
a7f$;m9VmS?;1s)X*I#50N>1CYNgeD5rZ<Gli4@?gRp-=`,sk9IDBS5-4H!nacC@_7`b>?!2
cQ!0afO#;5F8q%?9^@t9>Rk&#_n7f/F-SrWP5IWBkiB=VKbCNUZ;YWJRfq9_0D\,")GE'F5B
+*A5i5`Qo[`a#0k:`k.rH)CU"6tKgE^t7V8dA^t_q*Gm,Xuc0qN@c;PYLn-DciQ=?3P#$^'A
kA8F<@Z9*Z53f6dd0jbhQ'Z<O5(\#lXlWSapFbtORt:'"5U5p$#T)4^"3#;DaC6)JFQ%e8/C
7+c#[,Bf@LsRH>sN*/0KB`4/?)Rio1%n):C^:OKe.o/-L"7R2^J"JN2oIuZlutH8#l>l810L
$%!AgeUV8N5s!l@]A4;mF%(,S#Z=gMV$^-LMk`'6q:mac?IG>4e);#qmH6q^mXLtJ'"SY<gOn
5^@'ZN-8U'$'Kh,s>*cqIk14eohl8SRu1;G?+a&,YCKMP6tV9m[e1&?knPgrFdRQ7MnR?1(F
(<Vi;h'S&1BCm6bWp,^HJtKkY.8K8EECo7&2Wg*AIDK=,n83l!X4!JC)r,Ni_(mkM4N.<A%W
'T-!d#P^J?'4_e_Fl**tk>[X.b_\o%8<n^79Gd*=6)$1,JIsh5U`Y4C%U1X\(8e[4`\gf!'f
)<2j<R7-/;DiU8jl#,Y0'[>'l:N5C!JrR.Uf-u?=nuoa*(?mZsnYVNdC$S5K^4=k!WdO72"s
7bJh\I%C'R9<$XH88XcYZnYK(&\sN]At`0'FQ3Kn:pY8upKCS)DFR]Ab:C/G^%scL8WrQ+aX-q
)Iab7*mNqV+NY,-MA65N8t`:R<%^K?B7e96@_*XTNcYigh:;]AEIlToinA>%R!\o1C'd]AF/\E
M,f08]A;otmTa*;,HafZ$-e\b3op7,tO3p$#FW^Y1mUNaNhRVR^8-%%Sr\.7jROV6K-BE:CqX
*BD"8@#3so0hKg`&!dEDp=@Zs,=3(=D;c_of5BW[2Zm3m]A<V9R`U\Dg>(_f?\:)!dbMN)CMK
FKZSg9abG4MJ7/;q7,fe:TY/W=_>BBNAU3k+aBmDKHO4`^Cie0e[c8O1THD[%/-%sV162dO;
j<tm&.\i-:SP]Ada`68pjK]Aif#(5Q(Kso>aV]A8tQ$l/"TdSA!+q=@!_j6?lj@PQ`:J^9gAq02
;CiM$I"uc*@9J#>p*ad[VNU-6MD_!i+m`^UobJn"oW8Em_eLks+gKf]A0tlMLb&FbSc*q<+$j
GbKS-9.[q82dRP3KW]A4d+U?YT=g^k-O44i2J,R)kn,"P9N`lX`cdM`lc3^GidSGn1f*T]AFX0
*a'B6:D*u0^N$o"_Mo"2O)R<)Wr.L*TLNN1M8mYUQ`(\TS3l7+\DOlmUQTUIT,eNI='VJ_E`
<#(lJpao8!74=EGijk"F`eZNX_'H_*sgj)Z1[<)W8)"MTbl!DgL<UAED0FH<m!<m?Wo'1^6%
dnL0jhqnPY!FQ^PoIWVJ>!fl(7q"(.?$*\hlcIum>]A>MIpKmhl&^Peds8E$o3T^r,tl/0((Z
_0_ZcCQY+q?]A+@KV_(4hE7ErJG[^*8A:GW[4/"GT)]Ak6JWtdD1fmp_k#%J!C>=>$rom%!^N;
2NEQp>TA.gWbNQ5tbGl@;2QB`GG0SO8A"\OXL-F_iqrLn<._BfDk>bOZ_!-?Qp5B866rOY8U
#3@7Y9.^s[HG&R1!WtkBBI!.a7g4?DC_buVmS7-;nDH#X(sReb[iiZV!!VVAV*D1\FsL!4Jf
_cXe-'i5r!*cGk=KT"LXGrbOR[ZV=,'iL!I:unK@'6n;!VmIEMP=YOq/1hF'&2X6K"?.>5jC
G+G&aSQ<q!rV1kA5..bN]Ab7aqRYO;^I9`2?c=[!&BJqI5d(%+#D^EFNUOBHJ]A:.N-"VS\NT^
>_Z@44e^,@KrIWD=1bjce+A1Y1(,S@!d@%p25!o%sl4&lm^[aWkpk7,!bgSGbG"1?2^;$OWr
0@[&!9JFX\6+^\E+iaU]AQ@8>:^f=k3UfLo0&K5D(Q/\9H/[_:9]Atm^6_lfoH;]AF]A#l_?dub"
KE;IVpPR9!;#<#iadmCb2ZqbtM)BsfS_r?uZF97jr,$)ehN#:hM7)T<Imu"r)UC?'KRRlVc7
:<lL00nK*5AH&XML(J8l/!Ah$\`;lJh#DFGOs9iXM)(7TN:JD6I2e)NCLM3:E]A=[C=0d.c'A
&X6ta;D[-==E78Yil"2&uBMV2JmMO.#J]A:Z.h+<l2.+.b*-RNc&)62AM-90iOpW/WjGrZ6S[
j>&Z@^d5$DLhnP2HHpib)$IE@aY(MOi+g]AF9e47grn4uH_/o_@MiXairmV)`^M',OXhNJO4o
/X?uC2rPn!beL<B6Y&er=,J^AWrlf:M,FOZR`'/e+V\4@G_g^\@=I[rE\UI<0kPr$"a`UP'J
,aSo;b'X+OZ%%2bQG&Wh5JX*9<:K%Y7g6Mc[Wl=@&-TBko<3Kf-Qc9V3!q]A,.JOl;\JVG*U1
Hr(V,:uoA*?W9.%I=]A)DQFGs5n/4>^*;Nm3BVOEnQ'FU_h7cdl)_6jOi(2X'-32s0.C'NpQ;
Y\'pIYRSkS'_fgV[1;9H3`:h&!L&SP'o"H1T8#NRDLS.D!NF(jT_*C=,20eYp#m<&Z@>Ieh>
$9Nm'HSN<Ah(:SBMUN*@+"f.;a]AG(]AHIUc?;85/[T>#]Ao_6e@lQZZ5\>Yrfesp$:)u'F0]ACa
me!D3r-3l7)'?B)?AaEWK*H`5@s9D6EAUso9N`]A;nMg)C6kM&8:C04d!9QCt1PCOYDk8'ore
("iV$eN:k2OA,">N`uO[muC.M\mbUp!f+%.!=?HfE'?LA[tF#p?a@XMp88op$E$$@SuMFIei
D2Fqi:7HH8#?4^dT0D&ZCg6h!:@ZqMc^AGjDu&_GTemi<O(_E%e[cb2q62rMG+T@6`>S$Hq;
#bO9[EJ1[t)PT(G&fP,Pu^m$he`O8YCQ]A(]A8Tq?R*IrWX9?U`p]ABP/#BPl3!s>'C]ApD[ATn<
OTIeG0^aWj(>SRe&Z_q>`N-GgUEXakht]ALR`nS_''JHQ)YmaKOAe:DUt'T6e`)pFZ$A1;G=o
"c-?!&O5MXN^-HJ]Amj>eUcUriG^dTQL;blOu:eFXQ(-M\!>Nq*IbbImbP_d0q*&f!Q_(f[cZ
d^/I#50p8piWX[QTO%Y0VGb-n5?:ro]AI"s%b4F<_-ZnP?6p]AjB_%^VWnH9j>l$Tt)j'h6E_[
_\1i9Xc"7G6D)cN$sA_*q,GCh[S1Nrcqh6_Z4gqTl;l,^X0t?pSbkEVgefq$g(E5g9j)0?sj
=aA!Rg>QhD;e#tiSqG8)Z)&s*4/uW%L,s7aFB(.#kjF!#C?:XfO6\@IY-<h1cA\2i]A]Aa1M>-
=Fj(2JF<=q6SD6(G#K^H0dSfM'F)<YlLDiSF+qFBbpLOZmj)$I::->Sg[U3Xb6cO^E6_fR('
o!F"3hm%d^cbpF.?W[^+*Qg/["/F>b5-eBFoe&hT-tZiVAc<r'RXWD``p1G;<b.sIS=ht/Es
``1dX#eumkgVd=X2tmMeqY%ZCg#O&!^F/gg%dfI(GVA>eK3t)LN20d_)M0W+P$`L?2m-Z9\/
9sT592bJ>.u]A<gD1a#]ANf_G76e_B-LtP18SqCLOXFJMqgTmJ$4EZYaBY5033q.?]ALT\C;__j
A\!TPWpB>hE#6queqL2YGZdqpPaKbEf7A8oK$;fJDSq$O<^gAD)^-6"sY)7idRTTZ_Jo]AT!Z
iuer[Wq1!3rPH@=.c.3*$i3Nbt7ca=#\[g?$hIE=N^6l0@pc*LO@@:5!\od$?#,"JM\0l&,Y
'I+KLIr37AhXo@o:JQj;+<A:Huu(uT>GO2ap#<(pMq?KIq6Sp@O!9k,0Kh)@G.`21f0/RtW!
o%O0uV:*5)-.A2-'VcDH'`)bQM]AV7e7$DUEm/UnmkH'[>4#&,emfs^^j7S_1Dp.I=qAUB@Q2
_m7Dt+41C@18IJ907c!jo_7IGF0I&$.KQG@pVE6]A\Xk-[J0%29?7A7%d?5GPtsp1T,j;96,,
aO^G0IV4;kC<F![X,N7"HmE;QU&_FSm-ZStA<K!m7s5]A)f!<~
]]></IM>
<ElementCaseMobileAttrProvider horizontal="1" vertical="1" zoom="true" refresh="false" isUseHTML="false" isMobileCanvasSize="false" appearRefresh="false" allowFullScreen="false" allowDoubleClickOrZoom="true" functionalWhenUnactivated="false"/>
<MobileFormCollapsedStyle class="com.fr.form.ui.mobile.MobileFormCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
<lineAttr number="1"/>
</MobileFormCollapsedStyle>
</InnerWidget>
<BoundsAttr x="0" y="0" width="250" height="150"/>
</Widget>
<ShowBookmarks showBookmarks="false"/>
<body class="com.fr.form.ui.ElementCaseEditor">
<WidgetName name="report0"/>
<WidgetID widgetID="23ac97c1-cbe0-4a51-af09-0138aa02f059"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<Refresh class="com.fr.plugin.reportRefresh.ReportExtraRefreshAttr" pluginID="com.fr.plugin.reportRefresh.v10">
<Refresh state="0" interval="0.0" refreshArea="" customClass="false"/>
</Refresh>
<FormElementCase>
<ReportPageAttr>
<HR/>
<FR/>
<HC/>
<FC/>
</ReportPageAttr>
<ColumnPrivilegeControl/>
<RowPrivilegeControl/>
<RowHeight defaultValue="723900">
<![CDATA[723900,723900,723900,723900,723900,723900,723900,723900,723900,723900,723900]]></RowHeight>
<ColumnWidth defaultValue="2743200">
<![CDATA[2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200]]></ColumnWidth>
<CellElementList>
<C c="0" r="0">
<O>
<![CDATA[销售区]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="1" r="0">
<O>
<![CDATA[金额]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="0" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="销售区"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<NameJavaScriptGroup>
<NameJavaScript name="动态参数1">
<JavaScript class="com.fr.js.ParameterJavaScript">
<Parameters>
<Parameter>
<Attributes name="p1"/>
<O>
<![CDATA[111]]></O>
</Parameter>
</Parameters>
</JavaScript>
</NameJavaScript>
</NameJavaScriptGroup>
<Expand dir="0"/>
</C>
<C c="1" r="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="金额"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
</CellElementList>
<ReportAttrSet>
<ReportSettings headerHeight="0" footerHeight="0">
<PaperSetting/>
<Background name="ColorBackground" color="-1"/>
</ReportSettings>
</ReportAttrSet>
</FormElementCase>
<StyleList>
<Style imageLayout="1">
<FRFont name="SimSun" style="0" size="72" foreground="-16776961" underline="1"/>
<Background name="NullBackground"/>
<Border/>
</Style>
</StyleList>
<heightRestrict heightrestrict="false"/>
<heightPercent heightpercent="0.75"/>
<IM>
<![CDATA[m99lrP?6HJ:s)HLV%s4L+gtFRb#/VQP@.[p77<&ia:]A4jPg'aF>8G_f)bXqFT^7O;Je6TY+f
&.0e?B7bL*TBA!$MXlO:XaA5IAn-rZ+SoS29?3rTiEe^;Kp;rU,+&qn18H^@u[8n0+\En#_'
M!!')[nAX7f!.]A8+!!)\mIsG,cS%n="YQ=e1Yrf16pX,A08Xdp`K2K]A=5/V]A;q'DS^O7jD/X
`?FEGXM2rX.MI*G[+;,oZ8#ZRj?>c7ce9,H[1<-\>m#RWc&1<W,sCY0DNV8mLPN4b7i`J[[8
CG\7GfT4sBq(l&EHM+1@3.Dm.)$0RYhl-E<%IN#*_#`'@-KJOZIj#Rq^$:iq*Z^kKh<3;)g+
>NV0Q8CZ:_j>Q&lNfJ]A]ArN:]AUZ>W5;FP]A4NYD7_O2Kp![C6#j(daBtg)fX9C,qr9oX-\9_Gg
;jZ0DYmQ2QU5,n+YLQ5GC=GbE[;W:S>?^e]A5fd7]AX,8jWMAg?g0Y`]A5o`DTW_+!P:Ak<d1"6
O(<qm"$cC:.aaQ8\)fm9q<b+;gVh7*kM.5L'4>BpXI\tFp;NZ7YRAa%PWN[`<me2oA#.:[N7
?X^]Am>1nN*SCsL9HD?)OaUukWL\b(Ie%kJX1p&m6.ulTnQg3aGNXN8B*m"+ep.D7s-T(]Apdc
9;?nj9j$7(3".hQXGZ]A`4GgCn!u<F+SoE[D>0>:;[CRNThM0_dpCV78u9q8e58d$+854]AVtI
bD-qU6KD;31h@_NE1^hGC?g),YB4=B>,0#4*SLbpL?Cp\R%fa=CJ8si&faZ1SKAQ\cksoHS_
"tN59!:q(\\F"O3uSQ2@f,9K(@'UO#E4ha32hSe&H`dadLr"rD>@0U#\B4r4,IZ'NhWV4E@X
O9i>dudB'\up)u8YS6nlC+O;8CcT3lL?aQM(+^EEo85BdIRmG^P"XTR7Qjj,=>^=O_;k(=@A
m2L@:m-@`SdDG`qbESl_Ur7;;G-Y\U=G;1ULgRcX&bpH7O<@Mmm@;bf^=ckHB,+?;1`(oD\3
LsfY'Y$LIZM,;BH9Q_%_ICHe4L4#Aa:Xmi/aD$5\MF+Q;1kS\9L422J^!BUK=HO/PuZ12S8>
D6*"R'l9!>`50BpTAQ!/GuF9V+>JQ!Wc40\_&;=7DtTS%.ms1ub(]ALffZ6RT-#8/1"P54KC/
lJN0jup7Gq5._Q5kq<_hG-oA8:<#ZoC%d:p`AVE6\^bNl2`4a?#(,c:\o'<`FY',I0VH%Zam
:l".t/1*_PhR(MUf4KLlrrObe4G'dh4qE%5@4L_P.roT,U>?D%k2([B@N%MUpF4cZKclXT-T
AX%XopJbi.MfLqMO.ZF@>C[U)Y9dY+LH(PjPM$(JWCA>$6"Juf<k--AJB$RQ#$jgXN([(QYH
.9DNDFq3i%I,=:M\`kYHtBND;%NaaN]A48SOl>3iTG5A;n2X8hm)k7eNf/R8EMAgRm/$+[(to
-7^#V0Ut1drE5D"Xl=-lT_CnKd=)_Q%d_QNPL6'PCYr6ScmM!NWp"-k3PtV:#2+Dk]Ah$AY]AV
;gW`j\mbg]AgcJiEt_'-BZOup3b@;%QBmCqfC+#I&M/s%9RX`kdkRONa6i$%WOi),1>KSl\n'
8Z0]At,+J,/aWgI$kMcl:5`K#8[BhH2!_8(YTD0AGkB$Vm"SoLS_8t6VZ4#S:jqo7hm<^S,^9
@?&4`\EaDEl!.%iJP\n_6pJ[DU)P.O[*j9!!XL8@V%UcK_<$.$&ih7:2;&i"cAkn:N*d%E=k
a1h[4;%\KWg1nU'=1U(=$SoiThI/tbu:/^.LP1$]AuuH(n;9md""1+[[l-9Ko6Y9JGn"S#!OI
YY2@7X;R.5NYXBNMc,_ZI6G3^AY5f_@`\dfS5"1me&f!tVVm2VL6apKU.s5_D;)s!@"S\pd"
Des;\Ps3O,"o'4.qZXMe?AF)@@gLd<TSam':Co6.WcB/aSa)?')UD"$$"\Co:0'FJoUpc0b$
`degaMJl&mk8rGCPb(%($;6+2I`mGb!qAOIY+dV$MQJR;BQ&_^GG\0C>_<lE0<_O!"HAi03X
FEF`^qahV8!Sdh)7N%mK0A\i+0nOXgkB3d;nl6pQ-+2#4#CGK\Hu2+I+P;#U7ifj.HW@!r_U
1V@:84KC/7_9b0*n#+4YlRQuG2hh>'Co`B*%`^;JLl>uS]A-KaIah`nG^%WM<Xi=BkCeQ@%bn
D$Lk!^2"M]ABP"Bb,\'@c*+%:7Ao>3`C$h,@C_L`&mN2m>48Q;o&>=:>X'bVd!S^h]Ab^l<ANf
i=hQ,3&_$^urJs*.Ar*bLQH!;#<e0Ni*q3uF+$ZqgGmV[__<.bru@:O$75DNIH5!*`Ptrd=p
gr+PE/!<&*+l;r1#F:`.ZEg))<U&OTjmGIoCs,R]AhffjpDhe6pZRflN4rqc\d\$t<H[I_lme
=(8I[6W#U?43+"?;t/lY#F*ol@qEch9M,%dLN/5498<2.0%WaT$&s22uai7.ip2,^nH(7>pb
[;?Ct<Xj]A.D(45.e9m5u:TIPBUJG.PH!ajM[Ym+(G>*dKlL\IUpaC8M7G;$W*cWkD)DFrU0<
d>r`Vq<o*2BC5t:%;=q`/K&>M;0">R33Op'Y?o^9Ph-[j2cF`nQ?>7o-&aD>;D7$q']AJ=Cq-
@l.KYTV.2qu)2;6tmFILn+\m=?ek<WFI9g`9:<<-J1EjGB(5I*P`;!Ep`F/SVOOl2d0Zhe[a
(82I&,&]Adl]AQ.q@ZG(3t]Ae3$FB<*FCE#??H:2gaQ!p;HD=N!-#)/")0\@K>4jmuLd!FbdgVC
)18`MfVerSB4>t)CUJ?nt;sKrdE^U2p3c('$Uoqnu$+Hc#@Z_Q"O@,^11oh6^J,6)pf[Is,A
oF7&[f[>27l0F8fGrTNJE]As+@\d>p]Akt;$.FKa#*a*Dl=<GQg$M"K4ZT[+@9OilrdhYHB1<u
Z=CnqZ)bKZgW;&iQMT&1]A/P(01uIVG]Ah(fc%7uOmG4hWbNEV)J'OHY0G^YW?]A"^@rA"!s,e+
H0PL@l</7+l/;kn4]Afj(l!sOk:pOdNL1">jYd9n;D9s!m4f*Ru3ri?F"cC"Q0:797?IRZgDT
9$4gQoG>u\Un.^M5p`Wf*Q<dPg;7;eHl:"Zg20U!upgmq-"(`P^<BK7+_io57c\h#=-j>CC.
8.F7#CK*em0sMCeIPk$Dt;4".,!iKocR,M&$ZcOrU`EuI7U57ah'd;p"%m\G/(KCe[7*"ac.
N42uNT;7<<O;koees*>a-NDtpDFpT,)3!CQTffBceQ;neKfYA==M84T.Q`>hLr6#UuJW3tOd
!'0cVa;*49f.2\SJOm28n)-/_e=dgV7V>H(A=tNj22)2j9&Ac;c=@58.GBIF:gfg%FF`)Kk6
MhfF$2+H*[ts_.qg"NpQlTC__NTI'*qRrQVf0d9r4CbKt[f\SMM=eck[8SVE4m!?aiI%ZJd"
arL((r9"*/D4)]A5q!=teNpQ0s+Zaguse'G3E[Z-EuIBbhmgP8mKh@/keaW(@?F.s[-i`JURL
I5<PQ3L,mLmj$YLWj%W7BJp\$tZ.pQlZN(dh(R9/b%>bX1`j!\4u#AfcIUs+S,g5Cj&EffD$
dRRd?ui?(6^Gcf;rr^>&P(j57Q'd#pl63Q&O2kWgssKMC^OS*-s6hr3r1Tc3I;<dX4B3-'LU
9T%@G3m%jmE>u_BSF6ZkG+pmkd#?D$VR"O&dg7&(a$1u0V%'%3Ao!k_;TTTf0SR/C![9u(Z,
]Au-Vha9(m=p)i+dn!Z.qLNdMu&KE[.>]A87_K*jG>Vl&KI_UTFLB_S5EQoCAo_d`Z.IO+2p.-
HEPeXQ@oHUHa"9<hgNY`"Y=_'G,MV.Y>Ke8q9J.$LR$o:Zh3RFr)$/d0[@"mR7SkCE<D;N5"
K_M@D1:lhCS6CJ"(Z&-Smr)JJ=hu(;9g_><?NK90g_IYIT;lO(>9#j-qF4)l6_<>oQao>oot
Tq(2!`3G\21*0R"ShILa1G[`#\6>d=^MKH4.pDAoPl27l$3Kp.g3c_amU/+<0VHPP:YidCj.
-#o,%m"Aj.ph(CeNo_1+h5>9U-#-)VX(569]Ak'#:cE8AC7k:Jo$=/s($FU;@/*-#W_r;@3SV
Z=`_d_oj5<8\(T.]All@Zj!+=bgZ#?0/JD'AR'hEVr=@-5f2.`ubO'G1X,df``/QO]AZ&.0Zd2
Z4Bu?]AX?im37]AP,b-q)<$A9"fAkOR:5B:o:)2<!0KorfD2/JAaUoE5JD2suNW)u\lCX<$8P+
<6kCphWW9FVNP!Wc=kQ=2Nk$nUStHN&e5JKZfBUS"YmL%.$[c>U[M9YqT7]AjX$t%)84mu<Ao
cd]AjeemN;3OWcl2l<gOihOUUGUb/EHLlC71n,oq&HE4^1/1(eQAXCjrst4($tf`FIB!7geM.
8#mN^i:1C[[b74`BG]A"R2S5..*aaZcG<9kkF.38t%Qa@uJC*=s>2=k$gbRdP6<I_d^pGIK<G
m\gUjlp4E,O*!8W%40Dj%r>]A,Xl@Dk^_@>C;!`f]AG)t%I:S>*.<9[dfJ'Qo&JjjRN[t!EW`8
Z0r3/miD2JFmIRZ5bbXqDHJnSD`p#,>@fsgK.h7,cliitK4f?<IQe!D+&1VH1c3I_iMNGpeq
corOF">l*;Rrju^+&lbcG8\g"$HB23PCG7RF6Ool0),::uib&;sLkS^sjcP:Q?Vlm%'%NeSY
CG7A>XSjLK`#osM`%f/7fNb!1eDes'b>&s4JC?^km_okU+$rMUg7PK329DCk359>k2jmh[b"
Jl2[7ea8o$QXkuo@#\kp')/D7rY,8]A#:ji654iLJep+g-q<N)Fo%!MEc8IZ,Lck%]A*f6H"ND
cf,L!k,`k&+JY/\L,oWZmn<nS)C%-D0qsDLqXd\S,=X1;'S,R+U$#lZBHKgsdrmmTCJD'-#Z
T)SG<TXV8;^=UM-oIs,GdF`oDnlA5qtYr*gR7T`P24r:nV"20`2YV^s&Ys1rKf0ZJKK"7rV]A
0`rCadqn_Cce&U0s3]A0B[poh'b51l45,6UR`,iTAo8isr2aZk)m0tP"]AHPd3aHN"ehI'X7t?
^E\R+TCm:NiD]A+cK/*=rC_UKh0681,ae(;@#eEbSRVep!A"7cMBY..'3mmfrbJCS*bjGnjN*
=Ok,>HSq)9+K3t@`@209dA3=#Z`^W+#%Nh^6?lR@:_JiP(Aa'Y[W1nN0ie/3JO!N?,fpRQqP
qX^4BQ6nOlT$=>;LMbKu06'A7-MPbE(Wh^mPRQ,L.?)?\dB<qOSdX`m;IdC)IQ?1t)TV0SD!
tD:4Spe"Zq.]A9_Pa/lrQE^Um^[k[/cud6$MZXM(c>X1L$FUHXq)-rC=8`4ndmVA^5hXrl;TY
*4'Bdl!""KaCXaVOGl*4%q]A!pt^j2IO0/g;<0h3li=Sg[Q>q]A]AfF8P'#-(e\<C^;X4XON<pc
/HIoP<J[ucp?kZS?2LcMrA&<>0V>@=f[=>DK(HpFsZqOJf3,:+.BWCO(UfcNSo,*^q3Y5ka6
TqPYA_$3qBg=doRng;dr5A@C'72T4T,]As12!n[PumM(:.d64Jp,f_B>V//E2&T3PQ;Slt!HU
pBU:NH$$T]ALMU2g<7i*!R#a]A2!]AU(u[4XXVF]AY1+Ghe#[_NW7C*60$`JS7:XL-e^s-uH@o6'
2b8'N>)#[E+>Qmee'B\`%L]Aog)g10rfnV=.39Fmg"N2eS(LcP0*<@T=mhd3VV88BchiDj7H:
k]A^5Z@OU[8n>:dL=aXSbKjJCDEG&fAjd[r!@ClW`!lX@NE5Z:X\qs5C^p4llt+9MdL!4cFW9
,\/Ad^WrIG*G5r:G&cQfL<Rlm&"^jZg0R]AfJj?=Yh+"ae#"osG\t\r-3*E"5Gp-3VB%Vs`(1
bOo^W]AU%XMIa,bP^8^4,FeM?fKW@=GHOI`\N8OQ)K$,',&o\gPrOmh+iG(dPf4e-8SSsI'5I
1"Zo`M7I[GD?$c#??.X9W90benL1K%:sd#(?"<]An6S-=^mlkMpsdOTZ?`Q3RG07e=26k;.Ji
PI_EZ<rfPf^nJ6:Kcp6A?Knn'0=FaK,:UcX^M3;b)]A(3Hl$J0dg9H;:eBQnkbP^;++0"P7dX
ODg539g4X.`"@ADQcBNGZKRlhKQP8I-@9rSc0!39'4'_E(GL,Kr&$.BYWgZj1Z?A4]A=Nf&+.
$him[PSc@-uqo;Z+tbl4RDgQ*m2q_9Hjnnl]AtEgEt<G"F9BFJCr&60g'O2@g#\L?BF)n`aGf
"I-EaJaq422@r+t:3]AQ-3RtYZm1-&-q'/*m9S1i(1OlOihnIbtbCG`d@*S/Go.L.?9ACPNP<
=;l;uU]AY`d7eg1k"a4*NdJfWEJb;*_5JrE@2*X5a^3h#t),QYkIBE\ooOmY?eOo--QRJ9G18
<X/1'-U?(.q)OkCG_Z+P<NL.$D$N6<O0<>i:XAO]A<bjlMH[6hc\M%K0OYbt?-i?;VVBVYPOE
`3\ri3Z\K$]AlQ"H8%rf2<p3T\5/=kb;A0@,pOr^e,:m\GB\-C*4Pgp3k4LHL^h*IZclW2ra)
N#?gOjb!$^7Sp[-ZPpi76oK:Jm5kGX3G$J!CQoMThX-k3_F-%MJm&0Ql2hq\o0^9.<ls)Js^
(Fs4gX$@RB&Hio.MD<;BF/[D/7CjIY^TnhG22(b+ha8\!Q$4V<(P=gq.aY(IhosPTqP7N4%I
X[P,>TkLct7?!)i0SfpkeUsW,""m[VR"pnCTO<b;[0YS<PWMJ'B'4a!pK$1=^Gn_GAb)qS9'
V'52!u,7TI#.sGF$Ad90_*X8XuI[RR8M=YAE+_t*S,jhft^Y*9<\AOUIcS\LB0ct+;Ot($Uj
-M",*kSWGT^aHcduS_./X@+XQ\!nmip.5\oU>aH%bP@**^ncISe]A/gk7>mKSD9s7.OQNRW*:
HnZI"erTs'IjP.%*Fa]A5#GXnL1=3VC(fM8Z$:qc2mGkO*d5+4tbugbP%H2W=jfD5^7c`uniB
C`/gj;Y5+ADftQLUKHWFN#!?mB);$\E-NblF23,'BX#P]AT6/\'PQq')A%G<7%"#S++g!5I=h
"60BK$@'MTVZo!Oa[bYj1u$><6H^D^QL'E0bQ26#I2Bf4J$T<O_d8H6b@ur"6c=\g4PP0J8(
YE)D7Un:^d)O*J*J3Pg0Apc>Z7r8[`<Y7KE$mAcKrDLa%PF(+F\b=0AaPo2`SW7?G<JI_$#9
ud#RI?!@cQD4MRJoauaE$G;OlJW6=ET^'>R?_s;G0=(<d_t-)-^>,$Tq+\Y]ACsqdM&^QC)o%
n66UK&eA%F'=YLf8smkd24o9`//.!Ndk2uYVLqVrr*BXJ^#n62jkLA/(]AViQ)WH-94N'NJ/P
6['X0kd,YI?(AX16D3:=mcDtrBAJSkHS,O4XCL[SaiBqB=lJKfmd6jNC57_`_\WJpLnnfBB@
(_HUYQ,(ToGD.SBML>4N@o5#!8cR:ouGaPS4^]A7'OG\mY_EY:))&n7ic9JS!pW()-X@fTNP8
GPIQ%%HV4D&]AjaFookKIZQ'O$AIfZJWKkTV/59a]A=F>hWi/7H!Cgi9q*DS=!?6'aaUA?NQt-
@;p"pUuZj;B_]A.WE!*eRG*-mI#d0#!33`Xnr\D6,,H[DNKa"a?rZ?s'86`F^&[4W5/5]A"rKp
"jrZ2;IWVh*Kl9W%Mbgo[)N+JAXLTjWEM=.HKjBRmpO>F6*$'fD)gg;'q&kkY)$ctR+8%5#l
L;brJBK^9!1?ROuPD!Er0Ws85%?IYVBl&`>ANH8s#`G%Mk^+:W&nHUN&jRlr+er,6,6es%Tj
-NEU\l%$Di@ZKXS&1UdIDbS+"n)s`O!VBAS7&eHSSB<^45h&7JH%"?qDDPbe<a4Saq981[L!
2a$^qajSJD8TWhJ*a@s/9.SuZ!p\G-N]A>\"$"-L8ZL^K90X_p_B9@iIngA6<*puoOq8W2eh;
kU$)LPeoZj%"V5*@I]Ap[\OpIf$[H]A8$07#r;9,k9+mT#SaZr>ftsu%?>2PYG8qe)K'K-uoI'
\tFM)MVmmY2d0IOKE\ibptgqICMeY&5tJ?qh$ror*Z10*3df2e,n2dN[=bRV6)k.5n.O2ULp
J"`%6OD7e>Pe$kb5`2L?q'%NHGu8tC)KrJ`A"h$o5h.DN'a<-<'>*Ts:tdF$*]A7^1=e33`(\
/LNbDq5IM;"`Q\ts<%#CN?fWGh%CBZZf,0'2SdkBdR83rFm[,',(NnV945<cMsT6p2r5YWcN
gZ'Rn$DD4mL/R^R%[ph0J#ahkEm5G>B]A/!S@b:nQ:1=u83*Tp<Kn3[qYc69U>#/EJm!3OH_@
hE<S^'kPgor#H9h0GU&%f2^S0VB&8Z"bp2_LB)(+]AO.a=7*X>nNGqI%Yd[Bp2Gn8>#D&pMh6
NCSn@Vn#B`he2+:rYo^rD`U`k2OZU&GO^JM?Y<g5%80tULFSO`mHNGP^dM8N>9_\+F6o_BSq
-iY`Tr#.r2>jqO.9@7q-Tc29Vh6H24Pc%h<%C2GGcbKJF&XW'nqEX8IOkE]Akb(),:RX&QK*m
K;uCZcER;)5`1+uTCFM^+&!/i6RJV8t3=gGnqTmB*)XE>8_-^"Rb,,L9b;R?@R3oZU8F6C`$
%ITC_H#/N!DLP5KET;Dl_m4&je73mObkqALUkJ3lGTgm325R79N.f86JQ*&Q]Al\;^:Y:1!-0
o.A*4=Iq5EQ`_p<J\%=:=(6cnFb?^4bn:0"3Q;>d^LDGa/VSNU/]AuqE%jlQNKq^Sl,!3j%I#
kp3$51EL+=?.rWE3M`Q=<;W8sWgG%O$m_AbAU@'nA?*Y,@<!L.mXPaA@3VPIngnSdbDea+]A5
c+h7n_3u_.$2*?Q9f+4r6+tGbZU_^]Aq!^^1"%BSGc-ObdC22,AejIJI9E%n[J#^bC;k'>as8
0Ndb'Kfa`BTKVHRai8X-7?%4L&LZ?Ts]AT&RLCn]A8BMf>Kr7Dos)[c1?>/(*L13VR0aLoGT7I
-!WM?gZj.QhY,\!?WWLi(nbMV#$_QN'VQQ+Ld4^hbh)rVd0&@YS%#akV/s@""WYD6gKSq#rK
asAW8?$DZ]AoEb$V1VcKR']AXcEj,.;YU<b`LDKf0;D^)9]A0Q9diA0]A2J(?1NT@GL'TO&>@q!N
Y7%.mH-bTu[QoiOFbJAk[YpI[BUerDmDs,2bu[&E`qL,D',k@udkcLlW#1gcr^"r"Lke\mcI
i[fVQd%iS.Z8.TCKMnHa2"g'hD2t]A%`G(^5^*@\tn-SJEpErXk\%H=I^09&-Zphn>(N?+rOF
A&.[K,=+<d!!8?N'*]AGCNqRr5/h9_dD:Y;-BMjn0HN946H.0h2Su73%"5=[n]A!M37.iOW=6_
Fc'4F5N)6-tN]APJXEeNHB>5L\Wc1h%i-1-O6`#uF%^0<6;V[iNY5Xm&6)qJprSmBq3hl.\ns
4HXQ$2M?<hWY,@S'fj?ZnkH0%iuFNeLW'cm$S>G>r1.6T6(-DX9b#8[j43%1@MC9>?5>#cc#
>Q)c!Xn%&W9Wp<'G&I*4`'1CR2N?]A-qj#&X=/pYDlqKdNK!`/@]A(KGY]AdcqVo^/e=b??SInE
V9YLpCU:X>,8\7b9T2?06Oga!*g@"9cB7n`oNXbiCs`dE;BT_YNg$P"$d@">e4jg;.B]AkV-E
'I,m7\0a:-DU'N[:%6>KcO8i`U_/.\#[&Ga(G[7:e#SO9u39EhqULMLP4:qRh*ol)H,BW_[Q
W\,CSXm#PKkKr\ASl<NgmNWBI")N>Th]Ai0AQS#KsXTW0J/2,0`jqA?GPP+2E%AJW5u0"S_W\
R*O41A&UE.,MZ_"Cs2deI=%/Ak-:!FL]AP?cOkAeMmrURnTAZMBk`YfqLH`&";)b/kgTfYX0+
)%[&CKQ&2t8E[/4<,ZjK]ACBm-<)%:tJ$Ms;[eY%`'T'brs,dN);]AMEQXMdDNjFg7,+kO1?M+
UE7@ff69Yp6rM\%?$%']Abh87,bsen+AtNb'=AQ4YUDs4oo>Bl4<L:.0(B6X#Qsh*Wr)AtHQc
^9o0DNp(X>%%qV>'Xnk]Ai6hKF$6=?&7CKp7;0\qb3J;rp@q$,B<Ma5J=d.pYE-?!=e@dmF1s
nh$mGHmZ+r,Z/dR(rYKFB[]A5b.>b,I)bqZR!/gaTqV4INn@nath8>_*:b@J7A'cAdSJ$oDRK
]AUJNUDiN@VCk5l@$LF@F1SnH]A[@9Ll'05-XOLq^J@25u,qIHu$5tfZfMm>0VYegq!hq7IV'!
IWakEb[)Zi!=i/LIXYRF]A"T^(8j2@U+!i%*?02SZMrWW"G2:o!'o2`=roF\T<XRs4I`9tIEX
rp4_:Vh@'PT+X=Q;ibUu@:d9(r%FQeelL@=L#\1P2CX)PBkNNHlts%hJQ?+N<\BKX"kg(9['
;cYMapY3>VTj3fB)qb(]AoY)G0;QfD94F=88;X/do1\))KT8$1uigaHrrLH`Yu7X%meaeJa^(
NZuB>l9sMp#YgHkg4LTuLOVPpIZGJ=YF6P>@7VRh31RVbOq@LatN=P6>,n8NM^BbK8J-t`j@
c]A)rCqj)_b6U9,SBKL'8ft$lL$C;$GP&IJhPf\n1_2*7V+sc+PJHbnbF\lKR_t989`5ag$O&
p&^P\R>,&jhVK49-[HA]A5]AY/>M+4_%c\raj(l#bL5T++!Y\Ko<tEdi3-uI;oVu1m<n`A#[^B
e<<W)h')g5A=j&S?m'V`#+ag=@<UGIB?o#5,p5#f5V$?NZ9G_Yp@^@TS0\%kL/lqu%PjC7X#
/&KXT@*5ipXE(S9!XV+]A9)R'P??g7(&FK5'1Bl;%&BS;EciSJ.o]A)Q?4rj!m;OJcat2ooeUc
u@UoS@(5Z642TK=FOog4.jZGhIYYsXbKI),o0YET?p#<dPdW[L3`Mg8jLI/j$LAGLGW%P702
m@e;;EVQ#noh;(Ir6?^IrjtMEbi2K20A3L$g@NH/:l>(VhHMa2"50P+.i*Os&l$1qXL@kjjN
%q>3WOSm0^D%3h-/%=@0#)X;!5hVPQ%leE3FCIV)tJ+W9jMdO:Wed;_6JB<lNmL_=]Af-gkg^
k;:3#7TdI?3DH>(!qh6L*24#a*$dn+QnT4)R+]AR_I0Q/3@._^Wke@\5Zs@oa!"Ep#$TU*Z%'
DBuQMKV\60`4'9]A`2^":VBPJDpL%frk^9]Ak6-eX<@,O\`[8^SS>n8()$J..^2cT6'[T>;N1+
MDL=PBPdm34PAS@9HK?VW*jQ2tA"uP,-QI*"Kp#^*/75iB[t,pelQe:I[HWu*jkHRUgscq-V
pm,5RAQ\aL##RSkD0j#9^SIHZE:j,]A:etH$TA#2q^`qh9im#<MZt)*8_IKRf^9u&N&F>KQq;
Ntb$DRG<Ru.7,HkE58Xm[TTL?.+npIMF%1=b<)>MVq#kpO?QA%:Il4tF:F2Kn&ATWA.#1WpF
Z+Re]A+5iqp6B3'7,mO_kJTHbW;<-VG==kp(&4^#P+%24^@A$nCTL^IA9O,B.6)I30PBZV5?'
^48Y%D(G]Ar^ZLX]AJjVf:YfM8>\$>-:C<^`X7=&8`4d3i.@gU@6bL(K.qCU><%72B3?f'W/+@
h8)a]AKbl*jtMA_]AE6#Lj@B2mhIL]A=>>=`g0!rst@%.CG4gpZu,i_'q3ObQcgXa=L.EOsak<q
]A!S6DCgG1Y)U(HNsZ>43%o%u2\o+KJ-BI8q*h.M3IF32^X*eSc9i)V;Xp;NdW[c\AFbXN1Tl
u(.OWh8-Wgdumeer5'N`d5JQ&4>0e!>qrfl;/gWu(SAAJQ9rG6qgR?VeU%6QVj=&-!^JC%g^
&76TKs+0#AT#2C17R\1nDC!DT:o,MeEtJGImUZ=XV6.TSTTAGM#\+o:<p5G2(UKH:fEedQ-H
V<ecNQu:Hl12LJfDb?638?@"j6Z6Xd$?027;)$`,nr;fkBcNrus`q'[,hR"?!%L;4mu^;hs4
[QV:OP%Zaa;\auLW7.<MZ%rm&,b_Kg6Nh(\<&5T<8!aWq^5P@?Hbj,G.T-lR@2ApB1)4c9%/
;9;496((?ba'u<q,UlFZJ$I=j<\*iPjXSD-N"X0[[U1WGZR0(W*pd>+]A0XC$XFTJ$5O4-i?s
rPV`7sZHu@pE2T7p6.J34"_3j<!jee=5Y$dbSZerY:#rAIBKJ2QhQ5ihV?l,^10;.F&3p;AM
WFB:hUp?:$jI&L!^@5OSok\*c76i*SHo]A+N/^2I=@::b0W\LOaV'^K2"IstSA>1\gHo_pIS+
&'L*V>5TWP;HdoRCbN7<`-FPE<uX3g`'*.4JQ(17j-2f7+f0`&5b=PlWM'B`>K$V$l;!ZXBX
N[NQ-gcmir3D7ShX*at)jb#u7o>c3hCjM6M+Zuc)_"X&?Thgp32p6L,Uo&BZL~
]]></IM>
<ElementCaseMobileAttrProvider horizontal="1" vertical="1" zoom="true" refresh="false" isUseHTML="false" isMobileCanvasSize="false" appearRefresh="false" allowFullScreen="false" allowDoubleClickOrZoom="true" functionalWhenUnactivated="false"/>
<MobileFormCollapsedStyle class="com.fr.form.ui.mobile.MobileFormCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
<lineAttr number="1"/>
</MobileFormCollapsedStyle>
</body>
</InnerWidget>
<BoundsAttr x="83" y="78" width="486" height="368"/>
</Widget>
<ShowBookmarks showBookmarks="true"/>
<Sorted sorted="true"/>
<MobileWidgetList>
<Widget widgetName="report0"/>
</MobileWidgetList>
<FrozenWidgets/>
<MobileBookMarkStyle class="com.fr.form.ui.mobile.impl.DefaultMobileBookMarkStyle"/>
<WidgetScalingAttr compState="1"/>
<DesignResolution absoluteResolutionScaleW="1440" absoluteResolutionScaleH="960"/>
<AppRelayout appRelayout="true"/>
</InnerWidget>
<BoundsAttr x="0" y="0" width="960" height="540"/>
</Widget>
<ShowBookmarks showBookmarks="true"/>
<Sorted sorted="false"/>
<MobileWidgetList/>
<FrozenWidgets/>
<MobileBookMarkStyle class="com.fr.form.ui.mobile.impl.DefaultMobileBookMarkStyle"/>
<WidgetZoomAttr compState="0"/>
<AppRelayout appRelayout="true"/>
<Size width="960" height="540"/>
<ResolutionScalingAttr percent="1.0"/>
<BodyLayoutType type="1"/>
</Center>
</Layout>
<DesignerVersion DesignerVersion="KAA"/>
<PreviewType PreviewType="5"/>
<WatermarkAttr class="com.fr.base.iofile.attr.WatermarkAttr">
<WatermarkAttr fontSize="20" color="-6710887" horizontalGap="200" verticalGap="100" valid="false">
<Text>
<![CDATA[]]></Text>
</WatermarkAttr>
</WatermarkAttr>
<TemplateIdAttMark class="com.fr.base.iofile.attr.TemplateIdAttrMark">
<TemplateIdAttMark TemplateId="267965a0-bd97-4393-9a3d-97685aae8b35"/>
</TemplateIdAttMark>
</Form>
