<?xml version="1.0" encoding="UTF-8"?>
<Form xmlVersion="20170720" releaseVersion="10.0.0">
<TableDataMap>
<TableData name="ds1" class="com.fr.data.impl.DBTableData">
<Parameters/>
<Attributes maxMemRowCount="-1"/>
<Connection class="com.fr.data.impl.NameDatabaseConnection">
<DatabaseName>
<![CDATA[FRDemo]]></DatabaseName>
</Connection>
<Query>
<![CDATA[SELECT * FROM 销量]]></Query>
<PageQuery>
<![CDATA[]]></PageQuery>
</TableData>
</TableDataMap>
<FormMobileAttr>
<FormMobileAttr refresh="false" isUseHTML="false" isMobileOnly="false" isAdaptivePropertyAutoMatch="false" appearRefresh="false" promptWhenLeaveWithoutSubmit="false" allowDoubleClickOrZoom="true"/>
</FormMobileAttr>
<Parameters/>
<Layout class="com.fr.form.ui.container.WBorderLayout">
<WidgetName name="form"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="form" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<ShowBookmarks showBookmarks="false"/>
<Center class="com.fr.form.ui.container.WFitLayout">
<WidgetName name="body"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WAbsoluteBodyLayout">
<WidgetName name="body"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.FreeButton">
<Listener event="click">
<JavaScript class="com.fr.js.JavaScriptImpl">
<Parameters/>
<Content>
<![CDATA[this.options.form.getWidgetByName('REPORT0').setVisible(true);
]]></Content>
</JavaScript>
</Listener>
<WidgetName name="button0_c"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="button0_c" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Text>
<![CDATA[显示隐藏报表块]]></Text>
<initial>
<Background name="ColorBackground" color="-14701083"/>
</initial>
<FRFont name="微软雅黑" style="0" size="72" foreground="-1"/>
<isCustomType isCustomType="true"/>
</InnerWidget>
<BoundsAttr x="0" y="0" width="102" height="33"/>
</Widget>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WTitleLayout">
<Listener event="afterinit">
<JavaScript class="com.fr.js.JavaScriptImpl">
<Parameters/>
<Content>
<![CDATA[setTimeout(function() {
       //隐藏报表块report0的滚动条（此报表块名为report0，根据具体情况修改）
    $("div[widgetname=REPORT0]A").find(".frozen-north").css({
        'overflow-x':'hidden',
        'overflow-y':'hidden'
    });
    $("div[widgetname=REPORT0]A").find(".frozen-center").css({
        'overflow-x':'hidden',
        'overflow-y':'hidden'
    });

},1000);

window.flag1 = true;
var self1 = this;
//鼠标悬停，滚动停止
setTimeout(function() {
    $("div[widgetname=REPORT0]A").find(".frozen-center").mouseover(function() {
        window.flag1 = false;
    });

    //鼠标离开，继续滚动
    $("div[widgetname=REPORT0]A").find(".frozen-center").mouseleave(function() {
        window.flag1 = true;
    });

    var old = -1;
    var interval = setInterval(function() {
        if (!self1.isVisible()) {
            return;
        }
        if (window.flag1) {
            currentpos1 = $("div[widgetname=REPORT0]A").find(".frozen-center")[0]A.scrollTop;
            if (currentpos1 == old) {
                $("div[widgetname=REPORT0]A").find(".frozen-center")[0]A.scrollTop = 0;
            } else {
                old = currentpos1;
                //以25ms的速度每次滚动1.5PX
                $("div[widgetname=REPORT0]A").find(".frozen-center")[0]A.scrollTop = currentpos1 + 1.5;
            }
        }
    },
    25);
},
1000);]]></Content>
</JavaScript>
</Listener>
<WidgetName name="report0"/>
<WidgetAttr invisible="true" description="">
<MobileBookMark useBookMark="false" bookMarkName="report0" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.ElementCaseEditor">
<WidgetName name="report0"/>
<WidgetAttr invisible="true" description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<Refresh class="com.fr.plugin.reportRefresh.ReportExtraRefreshAttr" pluginID="com.fr.plugin.reportRefresh.v10">
<Refresh state="0" interval="0.0" refreshArea="" customClass="false"/>
</Refresh>
<FormElementCase>
<ReportPageAttr>
<HR F="0" T="0"/>
<FR/>
<HC/>
<FC/>
<UPFCR COLUMN="false" ROW="true"/>
</ReportPageAttr>
<ColumnPrivilegeControl/>
<RowPrivilegeControl/>
<RowHeight defaultValue="723900">
<![CDATA[723900,723900,723900,723900,723900,723900,723900,723900,723900,723900,723900]]></RowHeight>
<ColumnWidth defaultValue="2743200">
<![CDATA[2743200,3314700,3733800,2705100,2743200,2743200,2743200,2743200,2743200,2743200,2743200]]></ColumnWidth>
<CellElementList>
<C c="0" r="0" s="0">
<O>
<![CDATA[地区]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="1" r="0" s="0">
<O>
<![CDATA[销售员]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="2" r="0" s="0">
<O>
<![CDATA[产品类型]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="3" r="0" s="0">
<O>
<![CDATA[产品]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="4" r="0" s="0">
<O>
<![CDATA[销量]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="0" r="1" s="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="地区"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper">
<Attr divideMode="1"/>
</RG>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="1" r="1" s="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="销售员"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="2" r="1" s="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="产品类型"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="3" r="1" s="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="产品"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="4" r="1" s="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="销量"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
</CellElementList>
<ReportAttrSet>
<ReportSettings headerHeight="0" footerHeight="0">
<PaperSetting/>
<Background name="ColorBackground" color="-1"/>
</ReportSettings>
</ReportAttrSet>
<WorkSheetAttr/>
</FormElementCase>
<StyleList>
<Style horizontal_alignment="0" imageLayout="1">
<FRFont name="SimSun" style="0" size="72"/>
<Background name="ImageBackground" layout="2">
<FineImage fm="png">
<IM>
<![CDATA[!Q"h"s+sQI7h#eD$31&+%7s)Y;?-[t%fcS0'F4mC!!&3S\C(?W!/X985u`*!mF8rZ!FZ[mdJ
No=a%1@h_DK4?Th+3iLAPnbcJn3*eJG^HS,cRZlN#j-R0V*W4Fed@!AlMATS[po:!<]AQ''&q
S!*(c/johW?rq]AB?Wn:%l"K74Fl&bsGYo0\N:_i;F8q<r*qCr#N@^/-F5ZK?G@ffUuGFT;=.
TZKo!NNm.OB:@7P:"i\!!!!j78?7R6=>B~
]]></IM>
</FineImage>
</Background>
<Border>
<Top style="1" color="-6697729"/>
<Bottom style="1" color="-6697729"/>
<Left style="1" color="-6697729"/>
<Right style="1" color="-6697729"/>
</Border>
</Style>
<Style horizontal_alignment="0" imageLayout="1">
<FRFont name="SimSun" style="0" size="72"/>
<Background name="NullBackground"/>
<Border>
<Top style="1" color="-6697729"/>
<Bottom style="1" color="-6697729"/>
<Left style="1" color="-6697729"/>
<Right style="1" color="-6697729"/>
</Border>
</Style>
</StyleList>
<heightRestrict heightrestrict="false"/>
<heightPercent heightpercent="0.75"/>
<IM>
<![CDATA[mCRjQP@`K5DHU+VE-GNL3T)5711:PhEPP\27]A&M0JI(!u"MEj:A7OO7@huP:+[\\d$>_!p,!
RE5h_:?.$lL`a@Goe8^c#S:PDsGGkOk6HcVokeS)6F(dp8SIr5Zd!S`b!aa)ShSp\QpF:4=,
>s3ch/^URt)U6i<:]AOgi37>q*\/UCUkr,^F:-aT(PM\b\GDqhRCcd;g)!0L3W%g=%$'f##=p
1!kNa`us3/^M&M<\7MGnX&;i#I[=bT'pu6aiBDh>E4,WWVtqn6P"$^A+f5OXq-,n`I83,oYT
a%0&FW,10dV[lOfCk$nSsZ=ouL5aE4s!D]Ap6gqeuhcI%r]Ah&`Q'B:8s/Abo&0>m#i+29\hn=
Z\6u.O!pBVJjY1[BDdk0BMFtqSn)rG)W.1%+`DMM\8E]AD)_=0Q?JTTWGm.MQ"hJqs$(pVNiL
'dD8+5M5mdBIi2msh]Ak"LRekKM*@lbo4Q=S;U\nQGn6hRJPu3T[rf%ppKN.eW=r<j?=S.b;h
.q`\)!rIkkP`D-kU4iT`h32N.V)_!rF/d%#/i*S7saXarfYM#ud\$*su*jG"moX=ndLTtAj4
aYY!1Bt3I]A(aA@TSp02L%O`i*i*nAoOp+e[s&:po_>,4l0b)@OB0nW0utK[.r)D+Orh;D$OS
2lq.H-Jq/);aVOFnRluZIl6!Qb:$qF@&Qb9KZHY.#!dIR4,Ybg..]A<T^tlh_"XkBsYJe*Eh4
DfKq0'a0)I.PKBniqLANkull3M2;M.lUIl8aJG%8&AImtGg_qlLdA?#PugpK>>XBJ`L`!T^6
_fp`c1fNArA$bfJ*?<,,<WHD@;[1%OG1L$8<\=*Y_d"Fbnp7;7kDEe%Zkg/l!8=<584>\.`o
pg19&^Iotbj:p?KgN.KU(NaZiI%RE]AG8hnl?BP?"[NL^!hhF?ZfmVLKk<FE;JIh#^E8+7SSH
R+,i9OXIq?e\NiXcH*jir\$6TuGPhKonm18Y_4*:re(5<1!ujeY6ZRjh8"H1_QmU9.a40a*(
1\H2)^L(gpWLkGWX?ES+88R:Rm!VHuc7XUL3SBe54<H/gcgf=g$"g&*]Ah=72rSNeFQNHg&*F
-^%&GWNuOr_3rW(2N@@C62/W.rL6"2Lu6%XW`Zsf*Z_FQmSKAW;<IHkJI0^a7*Z<l-0]AY>%-
[)FY>_RXOLkJ,cV79PoMic(;9c<QChMCILna:`eRdiBTVs?e:JkqJl5n8k]A("a-HcafbabIL
ime[mm8<UYsf&(qsO7@)C(H6i<Rt().%;chqM0cmL8TARpR[A`j2rk^ajV@a$3)Kn:eA(e0L
8K%\=*M@'fsQF<bBbX=pBIo'U$6t^.DaH-"D7L5PVWh_\)U"7R#Pg$=1CC;_ora9NlA)j8N/
&g]A1%<HI0j<'co9F0b&(VQSW%sY?;n&rJ`OYaS@Va<+:EDC6)iP!PGYfBX9$5%1Cr&'nu:Xb
eHOGpOt8u9BIVO:BQ]A(<*M<oppSXb7[;p[hlWf2)OtiL1bp43GbiY\'EJC`rR2]Aq&oLR?IWV
(hi!AM)3X_.PV2`g976l"09]AFq4iak#tdWu4AU_Ni!:-n#\Y[>_sh]A'GLQXCF^KEm+s\&gu$
k[\s,ufK+(pCG.hZ)*VUPX8V)Zh\1V'nO,R1_igo7c>ge>HDsds3T*[e0MEoWh,*n]Adc'!?.
r<2hE]A]A;kQ#2Ng=KY.40/QhipI&RPjMZ2&#2b2=TAPht'NO9Pb0<fgW(02'%s;]AX*SYL3e>M
2jgcbKSV?[&d!%j!GqeeP+8M)S+r1s>j:j9*I\j^$'#9rHI":,nm4N_U)jPCNZes7m@&eMNH
h^m=Fa7$0.Xq9-Y_50c.<tLk;9oERq]A+>#rfYLUq9\l:#*&3%5k&"0_<M+e:DXi]ApUm)I+7:
GHQ]AHXcMFEqk_Z2k,?1*]AR%>,7?R,l/)O9H3-&'Zo%ifYYPs#NeeE5e5mrSlb[O0RK,TK1K'
*8u8rOPH&7B"Ib2t`e[708i=?+`P!kHlkG8<7@ul/+e&ch#cE/7ks47i\\?kUhW=[,YX2+m`
;h_F8qkbmnKC$(h(W(IWV<FK2Dn<gq`+?dS=_as.fRkG5BIfjNaTghF>O35O-UJ$G0Y<8n9#
fmL*.SLQ;+#Q3hDL7:[_*ZT)6rBk;[*!TclU\>;_rG7;0acgts&t&*j1<V(gseV>bp,fY?*R
2krrj'9i'*5e(A0mB^UiN1sMZ3\C%5LkFS.5^qX4aFY1''iVb>+F.Bt_Ha'.qO3R)J5"TOk_
GiCCI*Z1b^bKLBNaH,-,3^7/<7NJb'?@XbCoMn]A<X]AQ_^"@:C\`.IA>e^B'WQ$rj$.,7TW&(
lLI"RNb&\,!/aV^Wf=;#+l$cdR`Y+mV.W7?PC(<Io!]ANSqKS&*C/amV(p;g@rl_<lj'7hBV.
Lrc@e*4+)MV5^#N+cQ_MOge%Ga5'eRm3E_PD5*td<Go&(n'<#"g2tkp*6FPE>p(SkET.U*-a
6[^5N(B,[!p,V:!oEJ8utFX1rUJ(qu3DqG(R]Apmiu+<UOSk+^U>D5MX*XPi#)H(@`"ph0bCj
OY3J9k^cqP1bP;4&ik.<;aia(1XJime!W\!knX`C&X[bWk*'m89;EVi13U]A@VVq<@<>:uHL#
rlsh;MZ$1n$m4J[f\n!'\2N^'`0OP-/<BRE_3`$=N\*HUZ4te`?d0s1Y^aCc\/`&t5+u\WmH
d]A2B*ge?4C&qrHhZeJs2sO<if5M;9V"_[QbcTX6(qh,L-dP.7?+*6]A-oC8]A*FJ:i\?oXS3)*
CO^D"/D]ACgV^V:8M\b"4t*h5<,Gb`=6!MQDBB;:BuFsB(B?<=IH#Kp0Gs^#gPfG2&D:`U#3h
rTMBsQ7KllekC*6@oUNSQA/Q"%Ie4dOiY1AVM):&j5dq^j!UsE$(,U31$[ur_GNCoBAlD;d7
DZ5#?MJ*kP19@f$DWT>F^;?o>biZ'JJ!"I;ZN(J0o*1*\8KXZ-P7fM)S*`n>]A4!0&R795r^7
Kks;\kEH#0VU_$)SE]AJamm%XW8]A<6A-aQ;o'q77pPOk6]A+Hu/<M@S@_?q-g_ae7':BC;ggl/
R,hZ."l(->'9[q.8<ZqgIVl'itAGJ?.=r*_G5Vtp]AH\tonIEBKld47NRe)cjGlpMIX&]AC'Od
#MlLE<R<UFC<D%@rAV-8M_4knb;=cSFl,Ro*NnlT-WVajK@?If?MJ\XJcNqmP%=5ptcrY*ib
G1\8p`bJlIm+k%&q$#eR5W#8NNPS*-@+XSA[#&#RP.=#\0cC^`EqEU?js%(4`m1rATl;l4,6
$FO.H%W!'+g4`0p4Q)'#B.f^g:ZdW2hW+KJh&uCOd&`a8C+-X%a=dd!EaeJ&\4'VU(L8B#)[
Ymm@"j:LS:2'C"*=dEB%4d@\3:kICIE,?5(j<qY'"X0")&n8E\Gc`SR>MkgE9n9CFE,&/D=c
cPV>1WLA4r:''Jn,?Ja+]AgJ[=-!Xquu^A7aXCV$8+%gWrO`U*Yu1?G5\dX.6s(0r)ad2K!tZ
P$bo]AVX51JWt[r\Yp9:SW5QLX3P]A\\$'9I2&i\Y0K<INXD:Q!)Vs5uiQ?^L\^5F]A*Q;M1USY
X/9[)uO9&QAYN)%o]Ap"iiRE?n'mQ_ru<(>VdVoTck%jjk#;('b(t-oHsNi#"I;SCY[)$W7Ct
-;Eoj]AY\n62bJsqD4[dpkuA`FUfib_`kjfuP!(3#g;GQ^KH5^ai"j!S0\J4,MkPn;c(o<bNa
u*c,]AMQRS=g'SL&k4JX3n#PG@6f=A9tA1Y6WD[8L)>2n3hPt/2EY+fM++ZHbt>TP2#9!Xsk6
jO:0(rZflF3E'R`:9PTiD=Q]A-&SU"TO;_1Tk;9"R6mQoA849j+:(&Z7sc27?P?3V\GZ=gneJ
;.:0B(YkE"%CLd/.uqKBhecVVaAgp/_DU,+MXn.1ki/hZ8i-VQs:n14>j>(Ks3566Ds00WU)
/&?5'Y2K-bUdP)Mdm>>dL#&H@R&37WN^;?*.Ec-+(i,_-'>%nc4\<`j+GJlrtQDW\2d!=6kJ
K!kYq'&#YQk*bW'6&`BK%6(O`bf#bl.j;G':depS1cE\5=JciOA2<<Z1f0;:$2>;R@b`39Y2
4]A<bgMWnd)tu]Ale>oAbEU*i3#\9h$\;\i/dgHHTaEA'g%l%Tjf+I]A?1D)@<GidRgkZFH_#HR
j]A&7W2U16h]A5/h2S#*LN?@4Y]AA^?L"PECL/A0NTD9A8]AX#%1WPTFNI_(*m(tTF+#[7nm7`L4
UfW5-Gl.g;h.ugD`H:mSn[DBT7=fTTO!p1b&OCReuf%t*!N7?hXm@-4FsJ']AV<ZeobI8Y6\%
D<Xn9%P`o1&ja':^-H"N.r@@s1%rPIV:EfaUF9W]A[cB8[;*H/US=.jc`3WI7d%SCBbK)lmNl
jhcG+CeckZ]AN<=r\tZNO=X2DA;1an-!ueY8#[KdWc/nb3UW05=$Yr/.7,Wn;FpX#8e'8;q7I
L.D`E"1`E4-UmoqKkk=_cIaE1DUdf<jj2>1L/m^.%@I$9.33PLBW$J:fN5ab*.7kT2uP`7O^
=/<a`R.8d8TL_#$aA)V9H935;&k7hG1?sH;+3`IhgHtPdI-%@qF(KKg<*&Bn/f:1ZQ^WGL47
Pc%,PYiBNj)iJ\=Y74SV&DaWMp2_UIE0TGg/%u:mBGaOoM9PDKHS<N"aQ'Pe7AK_H+l'hd^#
+BQ/:e;[d^sIqF%)[C?MO-'D>^J(W&gTGOr$i7W;lPi%%Mbq4N]A[:Uk"M3de[nScRk3k.i$X
k*RW[),11BXrBdE4e<AG`)XK;<%V+fkO5a&,2GD;'kkCUi)]A&41Y.>s]AmQt[SKcjE(c%iDLX
O4a25&)W/oW`_mb>]AY\mW:in!)V:PYcIW.iAPL4j+ecDEg@::-5J9nN&lYEDmOHblj?Z`L10
)[G9"q,@l5&G\((D/1b$fdDgr4X/*32f1h:Bgo5W7/Am[8JC%OG^WIdT=>Ks[U'qW[P1Up%h
-?5PDIi6+'MEF&Zmj+)9o;/SRq<UA0I(jDKE<lun)Qrq[m"a^@kZLZ'*7\<&jfWR@"RjOKTA
G7Zul1[BD&&n(W9HBDKZK7VYd_m?YN",8L9]A,"T#MU,IsY6>*s@=UN7t&`UAtNX&6ZpeJ'%i
4[@m>rrtj90a)+M%Z3eBXLFE!Dk+S(;XDX^CXOeHJ\YY'XRo,R-:0Rb\!<`96b:.\d(V3LSB
\S[g"*N2A;l<GC1@F@XX#gN,0WH/iU.K0jYQgJqnq"kZ5iJl[P4mZrO9_OLUFJ!o%U"_;gZ1
7/0eJSbqoDZin<o^AY6Gi#G^;-"@4Pfe2NGG"j22+N0^fI$-i$^4nVG9(Tsk\74diJcdC'5Y
]A&F3:M2C#<8lY:Se*!DFLG/F^39comLDAOnl>6n`If$VeeN[1MkdRCpd0e>huG'r_QRL@22)
?&c::@_R6R;$/Fd6_TVh,?m,Zrnr;7oKq1t;=iD!inV8n#Z&[M2,a+R[c!N:PZ2oY\m-9b%Z
OoMa[]AZ=nM$4EXJ`#Gf+o99J9aE=l-V\Y^i+:k;A;pqMK6TaYkJ:-J.\!;R0I$YM+/84]A+/]A
e]AVq9?k/rl%Tq]ABqq[rlak"`7"bq.NkYNS^\/i&7$o"DZ=hdc?jt@R<e,#ru*?PiJMXk)CJm
>F]A0C4(VabR(qAh#?Z>4i$Tnh<S%G)2GtCp6'i9&))t\oSZj;S\4^^"(W:cZ3[cO8nfMi4qW
TXqT[B&s/.7BW8<JRi<mH\mk4)e%#E=+leahIPY;I1V?nR)f1mQd!=Cr2mC1rL]Ao#:)D(RuG
8A$<F[s33%*8`o3`eCV>^GWaC^n&rR[uToJl(D"4;)3HX;1#G9^;@gsg2LP9[<,pZ]A;VDtX@
'Ptl^CsZ'dS7SS2j/KE(mmra$UNa&;;-LU%4MQ:q`]A"u/;JRf6p7qbKZLHM/&a]A&-9iVAe;B
JV=e$mQ"#4[U44X3SX3Vkp9>;D;FDXdE>:W'7:QF0D1HldpdkfgWKbe\E`[ht&]AaACaJb'Xe
kL=T[c^=SE=[;uQab;m4=:<JTT8_)rmIq53B=mNS/9FAD1XG,/l**`*XXK]ALdXeE*PY=O+io
"rfU(KQFrV>faRpcAMc3E!"qi"i+AXbZ<m?*U3U5uo92SC=ZEXCAJQGQU$lR-QHZ-o`o*aS"
:!J7Ljcfc%b\*QqX@(DF`LM_!G\d"Ekj%fu4<AhT3,!X]AUlk\l;P@1L*D_;V/V1*n/0CH>*l
[d#&:fqQN.\)$nNS\_R$c5kJ*7uZe%k%.[AkRk21`E-e^!U\ENLb"j'hN3r2US"tu,:2AYV<
*t/+`<NN5aCe$i2MCdWHuQ]ADn*`GJPNsE&V8,>"sM.V?=Sq)jF,l11KnJVM0R^<6!2Tb1>_P
2S`"TdrNL\K$VsAlp=egFn^[8BU\!C3a/)^8I;9'0VLFo*k2\F@7M!*fb(E1l)_1tR5aJ1OR
>-qlIL*[\k!@8L)SH<ZlY2hpL35,o/(+gTW+Q$S73aEO]AajVEe'kd@5s9=PWhlt3o\L+6;kZ
oUZ"\E^d[9kG9fZtt$rNJp951(DK<s7n9X)8m&rFK>BFSgY<SCqbKMR3e=6#J`qaP`*S'CCZ
A5OIpPXa[tC/Zdm<MfnNrFMR$SVmWG?_A<8:e#i&:n;.B#[X6;euqoe)hlC3_D3L"qEYlNOr
kGL!=/\s4`:bDIKkWk6O,)#@0%gQTX$7W(o7LH?Q(Cjf)S]AH]AnrZ&?6O,QCml22,)&[No]AZW
H/jG8ngYrmDSWU_56LnBFGlcf@"A@LgIRc^)(7@^5]Aqcg'Y(I^*(G<T3i:5&3brn1jZ2>*b.
&194B9VFOBnm@M'q,Rl*6`9a,cAE,7li*+?X4Eo8UtYU0ZNk=8AE19Ff@i(:",@_]A<e*CaZS
EdheuDs0AK.tZH&rT[H!b^Vn^3i\f=iXY;314`>"V)U-"T""C=VVW]Ad\+@U9P@r<nR=1%B@:
lC1e]AiIPkW-1P)Ck&#<5+oq4iTGo\ll.-Sm\GN=nbJ'TY-NMYektHX6+bKNV"n'9&l,A*cMK
q.Rk:P\8"Kig*c\<32EdO(/)eS;[HHH!/fSRDrGLC++8$COE5H5SkT[XEOLQS&kNWbEpX$Q.
Z@3JJn4s\B9=Q5g14+L2K,"<NG[!DiF)t==&Z+Knuqh@:1*^=i?5,KZLHr,\/(EQ<jIOV0T&
6&=$(cfq,1gfobo"4Q/msl-%if0a=DMOj*n90gR%6OQj"1!<nGt1pp?*j1g%7_.J<>3Hen7.
5aKI*'Zn_=dq4O'dQb*(T88E'D29f->>CY%<fQcI5?)Z7r;<[S%oi%?TR9[]Ao]AY:MI3IC-uY
eD6fX7pWI]AXS'e*=[DFu8=?s\5ReXce<#unSG$WKjOIU,43(KXFa4%0"LhG#QWH$=`O[b^!3
,SC;9N&,d4;.u@_]AId^a<7M?@dsWkoZ/,q2*Eu[Ip[cQ_32!9\kih:$"+*bEF6ggL]Ac,F``a
*AcDU\.Umb=ZIqo'MLoNdX2#%Da4bi,<]A,diMVAjD2hBf!/9#n#<5*bl+jn!;+q?r7c_#Bom
<l(7IAj5skF>!m?CEc>XR30088R3mDBH,m_WPOpHh<JBT5bK811IpQh0qQ/.@\@.GTbdIkeW
j)cr@^C:>8?Z-1WR=TrjLsD_A\a<Cf?V^+MUFJV?/pdpRRi>8uLahpip3K#)A(-VSsJ8_(r+
SE4*jE0K[MT'Y?0Yci+2a"LCW_ap_<KhI=I\X<An.r%"?jBe1iPTJgaF=a?&YOV6Y@(iSPDr
N'FiJn##CB8>&gS&'AV<uAZ?lJDnm]A;He(SY,g!5,!D7[aRr]AP/5_1g^-_;DK2dW?-"t.'&T
A5&:(drJJ-_T!_lT<K/Mu6%r1PO^t5":/C"??P9X,k(<e^#?(_/9dqQ'0+3H<-26"taFs>DT
T\tP7]A+g6UPPip,&GLTe=G&QSTiE'V5^0MH`ss(e'MOgmY'AWZOX;&bpQed'[9[8re43s2GF
nZ2uD+]ArkZ\7'H^^Iqn-22Jh5?p?TN3i>F/5ARZJ`,ZYas1gWNn&ks3XANLGsFSnD]AIM.7`s
k/@Ur9V60n*]Al&.4G<c=1ED,1UdN:?hnZ?681C/&@D%U@ASKVrE!YM-<_+SbWR?*=::0@$$k
p_!Q7\9ZdcE&`RZlcH^6<<1*e['#&,OD3RCe+*0QS_orcF%m;!i$(i'IWG;^+sj/Eb)9.`NX
+Q5Z07/Ik(J#YTZM_OeJU!ch"W?n<(XFEnl:>/BI<Qu]AC^?>sn7!Kn,*h&WI'nH`dq'1ei%:
YtL3IKrkEp@f8uf48&3\aiCX+p,D^dEU=!MS>@s&lVldQ[+?hWC<Hp^/IH)]AXKiB9+qn^^E>
+UNZ(r[5Zjq?d"*[nKnU;gGi1Bs'RYDQh6fVX4u^u4R/F*(?.fICpYVLDbREa%;XE!lM?go=
K=!@=.B>f#D?2,dkc8hc\4I,N*LGd^GXqYdJM9]A3l]AS:?9)I$)4b:6)Hr$G.LNc@r#5I3/;P
@[Oq!?.0T<^aa:Z(`cHdpT-c6IHBps9W:^;<'rWD0gsV.Q`me_G*"/L2UOGg0N.Tf`4D+gq/
9Gu*5-QBp6NK1^*N7Xc(a\Hdc"P^9hOZ,;`QPpkW!-smlqL\kB@P,(:/D\sM$6*m9'l#;&"E
2A9C]A[WPB<b7>4C9Nm(E%4omfVNL_Sn-q1WLm%doFunoS2T`=Q"4O')@T_uU@M<M(ULV@+a+
CjV.Ma9SE3;V<j.i3?),>%J=7.Ar\WDWGNQf7EhMK,$KHR&*Q:+-ZPM#MVH6oX.Qg/ml'c&^
]A>Kh3UV7rS%ia1@;Li&4AZfSN^A([\X-\`K!Vs'"7EG.6A?)U>*o)9#/mVQJ8V0VRUQ-n&Dt
<O[!4ZO=:TCC!9q(4mBML(63RuukX\8SD_FnV4qki'BBq.%"fF^$6:sXMbbe';CFonqVmY?<
.+1:uh`Saspq$-<=R^jNY!r:\W%fdq-8LqL36JWFt6IGIc>MA9<p7m?9O7:?<D7$0XbQBM+T
pN92-X'1>aN1$HpI;'dn9QA4@Pj%96jq%8VN.bFENIc1ad;M9=?I<e=ujB"A7$%qYHFT&Skf
%14<rrdMJf`WF]APX1%9YGI1>t/NNDX)l&nThfZqh>par$O+OobqbjLcu.?/%"rYE4\R3THg4
l8*!K7&0`%:?KrCm6NYc:oU?ScKuCeN#Zao+#gu'4hDETIE_ZGF!?!A@SukYU+%Si///nXYQ
E%);,F$MK(6(&Ea]A>]Aej*-Bks4:3W:a56L.eusPgSbUrQp:%T<AY$V&EqW2qXB-`j"r>k#%!
=UfjI`kcT<F0FunW)fO)YAI@:%[1:KACl.:QE.!W7J<H>D@gNkaMFcQG5hASjYF58K/gY!'<
ImK:b,IpNF;C<S`%91+g+8WW;$q#b;D3^(%)km8XsFS$m7[4'1@=_lX0.b@a!/,R\;FtV'$[
uPI=t1-19QPBlEBZ&*-TUcj"(_q5->V$+5*%tPD]AW`hM0RGWI&.:IA=WSfEhisU\F[glhcD\
O%%`PJp\BQ)q#.1;EkrPdlgus9Umh*NOi;=G(2u(s)CCNm5@@L\C,s1bqHWIUX^6N2jm+qaK
D):UGTi@k$W=qG[*NOAHDIe'\Sr@%iT7*kPe]AMW#c8I"7Q/sDOEKaY]Afl\VlIq(:EbnPA<p;
@Y-h+iB0mZ8X-'\=SpI+FKsZ`ANn8/oj1^TC%a#lM7*!r0#!@t2r425mg!$c;C#+PU5>tW8e
X^80e)5(:Ib:qgERnR!rT*)-d"Bnh672tS/8d3/-,9-[neKg/=^^dacW8u+K0?0s0LfXaPII
ZqI`se<\Xk4$MjNL_DjQ/A2ZHD"FYD%`Z+i?21Y6f$UY$qJbF'EqIf-=C]AeB9=r2#5\Zi]A"5
]ARSi43-/our&=K=iLPjq5uI)6eEe1l1<G4,fn(U*((_((WJ>;;l^UW!A"WW+_@LumK;uBYWm
01bSg`_OU#b/kIq^6Rf1&BYnP7:=Da1\)4.[.3k.h$?oNN%`F@B0l)iq+fU5JsC?/bKUMB[[
J\G_1's6klcpBe@_<3"8WcTT49aA_lU#3kSl_%U\/K0$tB>]AM*8X"DQFL_%1.lV?2_[";%Oj
DGpoJ]AqLL/'R'<[Qm&)+<#Q6kHC==*eCX,P.DBicL5(bCfMKLM>#.TIOkD:1%8]ApQl0_,iDk
5*5)L!aaZ&8059_HNLQS>L,D2ZEFU-7jdQ$Jk:B0okVmej]A^uF3$>#BCp-4*f9Pr1YiG\iJ/
:7+7-r3S&?4W+lIT`4`edETZ<B9MY]A>H,6QpV6X(9hr,d`Fu0i1d:\Mb9!L\f@55*gcloRd^
0N7]AK:J@CsVC';=$Rf:nSOhX<DTK/";&n-ie3gW:n#u-OCmZ+[5)4p8bq?0NKrHJBg967=WD
.eVuu(:gF1Lhfoh`O\T5?5@X3]AYntdIZ,XdTjQa`EaHk_D'FU#Qhi,#POD&ZUj0GlgJsJWg>
/IjqBbsT=:]Ak=C7TB.%,AA$Eo5h?DCDhR1g@hM2WN11Mj(sg5$[mHSPAH.r0qE11$`rG#a`J
=Q.[gi:EcXmbp90ZNdrhf6UDMR?.hD-H4#PaF4:u'<ihU3f%_9#RDZnncCU!.&R8o+0\hAoT
"BR5Tfm4i##+gWZba1040$7DSoZR!05ZQ4Yb?;,\5I9N1WsAW`0A^FI%;o*Ff$i<>\b2ZW]Aa
R_e477`n2YWhPjFjc0+g_kr`M?2tQQ%n;,Jod@r"PPO3`%r>B;#j<?9.]ARldl#Og\[II>Zh;
hDr$21nq'Venl?:3q'RILT)Qh\VS\%RQL,C:)$)nIKA#8"Hg57[n=Il1el[k.I9m=A&Seo]A\
Ih1UiP7)J4i5IY4C3jBG.Gr6Ic8ldo&U,+F3u0)G?]ARZMB?V!lRn^E5Q&8!7bNqE_9RfCs3a
#@=d_,oSW#dlqH.@,T,OR(@_d3W&^\_3Oe;QL:=L;A<Mo#h.L75mD,.`BGCEoQ&=0&CS[2lS
Yl?[JggX87/,lRboY1da2%XO)SD-*CkM<Gg)n5*)B>W*_6plt6Y`54f!c/K&#C>f08F8/Hfg
e+!ceQRHeKj98o<hY:C'ED,O>?:fC`f8b:[ZK]A[EGFVYUk`<=-@-q09;u(/c/2ErJ1#KCM0Y
%eS>34fm'+0iG/0p%WC'Ipc<u-0R.-rIZf<>D_4E,>W?@_as)'/qJ]AL)m/N%jn#p-prt?r2d
[>D.:hdPnnbJfe_dnbta9;Y0K[?at5C@:S#Yr-#Sl\mse&LSL'#'ib@/f3:.29><q3)Y/lCG
ug\s3m33rI!tFJ'5PrmIE>ln@:mT,>q9o"WrJ%mU[)#IZE5YpQf2dQpJ&p6#4I,d8BJ2U:"j
:bUQ="LlIM]AJ=[3ObKbhVfSQ29om=g/'.6PdEB6Tno<tk\GILHJ,ZqeZcG\Shk$X6reJ^55@
.iJ2rdbt]A#,o[akDH`o-l)5ck.d_<!up0!O`a-\t.hAE=$'u52/QUZ:=*Ya@Ud%aYj/WMS6\
7Q[5qMU7n69k_qqn;6_#hN/b1DMu<L.aH'_8=ZU-+qu>aG<`IE6,u_X@QiGLEr<Nua`7D=(F
9n)'e&Wo;DHhaT>dXRJI^0u%O6h:4pRek$qI`E'EJ?D_EnSsO$cL40XZN&6Q7Z(Ep/=5W5uh
k'lAB0]AD!h8)>=$4\,Tr#rr)oMfs6jR$.$9C\Y>[Q!/*c#aTGGZ86Z#-QM2]A?EF#\Cb"@Lfc
c,.?BWr-)Hl#+35\BpFk*66GL>A+?HlIK89X&&H(>ZkhDogCUY)EC9b%i6_l`'IeUQ=e"r"Y
OH/,B3HOqr4r"q)q:ooYEBs5N`-]Ac*jscK&imP-htE=<a07$dt:R[q<n65PfMl(TsdOqHVgb
d[>c.DV?YU&'J7\SW<k"X[Im>M(&@IXrM\ZHa),Y,KMX.V(W^I0FUkc"TJc_@WR'<kYC<BWc
e@PLaesX%Kc^Iu's(/Z>jW-@7d:fCHs^>(S9@e5N1V^6fn7`\UIWHA>>#Rh\sVa'V?NF2g;C
Fp^MXV]A5#KhbU3'P:6I_hP@-C$KU$gA5mLm@Y'1>V<[$Bna>uV.WJ)pUVeqK28H_K1>%mij]A
DTuEGPjF2M5ML_A7k'Tg-]A%K)5mA&J]AJA@>Mb!a64:5`_;:-NtRUH/!TO\`nJkXu+&4B!c:D
QJOR=k(7@rA_hC4XPqmr6QC5ufh"NmmI6aW]A;WYrenA9(f^M2[eDm.iX]AdB]A@,_gP+FV>*8O
5"[C,Cr)Y-1J%:[GIX%&E9AHcP2;o?Q1ZrqkDm"9pcn4>-[V7[MS"#DTQl6@$dr3-pXdk8^?
c<#]AibbMo:t?NSQQ'an.:G8$kgn;gR1Wkjm:[Ho<f6*s;4\QX2!q)okeWitrDhYPh9XE]A#F0
o%%F%4HNFd[:;\QnmBPDsm%^uFPO,p[e]A@F>iio.7+A:]A43#UO9B5G2KKC2Va96H98Q=9%/k
A_^f*ijWaG]A(3?ub<Hb,DUpeR6i2olWIt.lIK0>!q>p;GV,p#o1AA^<ZMn[p)R-(/_u3s?*r
~
]]></IM>
<ElementCaseMobileAttrProvider horizontal="1" vertical="1" zoom="true" refresh="false" isUseHTML="false" isMobileCanvasSize="false" appearRefresh="false" allowFullScreen="false" allowDoubleClickOrZoom="true" functionalWhenUnactivated="false"/>
<MobileFormCollapsedStyle class="com.fr.form.ui.mobile.MobileFormCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
<lineAttr number="1"/>
</MobileFormCollapsedStyle>
</InnerWidget>
<BoundsAttr x="0" y="0" width="250" height="150"/>
</Widget>
<ShowBookmarks showBookmarks="false"/>
<body class="com.fr.form.ui.ElementCaseEditor">
<WidgetName name="report0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<FormElementCase>
<ReportPageAttr>
<HR F="0" T="0"/>
<FR/>
<HC/>
<FC/>
<UPFCR COLUMN="false" ROW="true"/>
</ReportPageAttr>
<ColumnPrivilegeControl/>
<RowPrivilegeControl/>
<RowHeight defaultValue="723900">
<![CDATA[723900,723900,723900,723900,723900,723900,723900,723900,723900,723900,723900]]></RowHeight>
<ColumnWidth defaultValue="2743200">
<![CDATA[2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200]]></ColumnWidth>
<CellElementList>
<C c="0" r="0" s="0">
<O>
<![CDATA[地区]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="1" r="0" s="0">
<O>
<![CDATA[销售员]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="2" r="0" s="0">
<O>
<![CDATA[产品类型]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="3" r="0" s="0">
<O>
<![CDATA[产品]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="4" r="0" s="0">
<O>
<![CDATA[销量]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="0" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="地区"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper">
<Attr divideMode="1"/>
</RG>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="1" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="销售员"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="2" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="产品类型"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="3" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="产品"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="4" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="销量"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
</CellElementList>
<ReportAttrSet>
<ReportSettings headerHeight="0" footerHeight="0">
<PaperSetting/>
<Background name="ColorBackground" color="-1"/>
</ReportSettings>
</ReportAttrSet>
<WorkSheetAttr/>
</FormElementCase>
<StyleList>
<Style horizontal_alignment="0" imageLayout="1">
<FRFont name="SimSun" style="0" size="72"/>
<Background name="NullBackground"/>
<Border>
<Top style="1"/>
<Bottom style="1"/>
<Left style="1"/>
<Right style="1"/>
</Border>
</Style>
</StyleList>
<heightRestrict heightrestrict="false"/>
<heightPercent heightpercent="0.75"/>
<IM>
<![CDATA[m94:-;cgQ-[53']A.@*^Z[;p7":Ml.tC;#4\fG]A]A3UXDo8.Ktu9KjYK(,1kVig6O-X9PpII\l
GhR6qBrX0our.MF8&bPY8rJ6qK^uOG"l)+nsZgX[.0ndAPNdkHt0@cd(B_IHu6ppO0fa2S7j
MLOK$@(SC?inTjuUeX4,Ipe4'MpcLGkrK#;G7SQjcO3c842_hN94hjlN&<jeQCY>$rN4uhTF
#C^S_3T,Kr@L,.$1<@/%l8H5cXf?8qVoAQH=),8CiJm=CO,8Aj/P`=gkd6GT%27p:[P4.PgO
aR6C/\Zic#]Aq-cW<qJ&$)jgDqiSK/Rh#_DXN]AKA?)+WnN7NG<5f6]Afp5WG5$mg9f^B*A(T&,
)Gj.`@a?2+CWKAOJ`f@WKoc$mYo[8*S.U>sj4%)gGYBLe4BF-'?E=*l*dI)"^;t/c44F=u)P
*U?(L0@=7ia(s(T$pk?GehYYKU=6#HFkVrAUUKYRQ^B6"W^QfNrCupib-tM!SQYXXS4i^NF!
?nMoh_dMcdSkBpP2iJo8c`_3D<*s7s&--iV^:6<N"Ffuuf_Z8c-IYHbBTO`Oi+t=:Xc;UrtW
Z?0(/YrfK2K]Aa8?XV<XorM\jk%6+043rkaa"iGhYXSd"?s%R\`)\#hJiZl3Qfq8u)[:"$1?[
\"`TlHW=N.bGK1,+lE*^foQ)(IVFO3!6"557',7QR:d;F9B3Lt#F$LChoFYS0gA3FCaJ(3#6
#Uu82Laj2:$U27:!:^l1$D3fU^m63X"b!RX*K2#JVXBhpJoYm?BL"F<&m:jp+*lCm.5dTRHE
_#W*fu<@o((oE7-qdh4lBr\&[_-u.7)Mma.Q:u_$2a^0QJB,Jt.i.fitJt_#VQ:Y*Q5`g8Zi
[#hMW)LhZM*L&UYBJACE#_aj\,l7(-Nc36b'TF?LjId@IL]A+KZ&&"1NBfQ;tq,AagJ_?d7u<
[!O]ALHP3VjC]ASkYuG/u8i>]Au<\RW@)nC(6NfPu?@*.0Y,]A#S4)R'_ip(uoo7F_MNV[]Ah!@<m
W[>#eW)`oX\OiIVK.Ids/+U[ctZ'NFkt_c>]AhXo=H82esHr(-2/P=mAe#5s1C[['h+d(V+/%
/"90I[!bNPF(bs-^Tb98dVuDOoH?NPE%Wt8&5X:s<kY$3^IJBgnFhrr2_QP.>,kitIceo@N'
3-P/Qk<OEA2ab2bS'Pdb9GS?Zu<)1i;;0=(^?9AC,Q=OmP2<\tKnlAe-_KH&Bg8f'*SQ.YUd
@?o@-;7kV#rIKG4))koA9&.oOD=S2EQ/.HC=;:>#fW_5HN2X`Ff'@JJXs7f"?o[1@]A-I>9&1
i;4!G4Beeq'>(e:":'%-Bkch@<H-MTO"s=HjdHu.&O]AL0Zkf)@B\A(jcO*dc,rA45ZG0UG]Ah
3EN,8A^XF_lBi/9g\h*QD-mg8T2BP@A6M2BBdYWl>QGOu;UBhWbU5a,W[bM^qTKi3!]A=kBU`
pFT(8`aHZXOnFA[$1XHOpfa!OaLJHV\=nGA1*6t23Qm>72U?@>GH=\mhp?^Z^6h-[XMqWbE&
F$qS4HAG0;+=BC,9nMX>ODl;FN`3\hM3!FcosM]A$[,6-!1`3O\iU>;2d*h%WXYHi]ABopZMo*
^m/,LNPpFPL5#o>]AqecK2'\5]A!\^joj<iI7#p/S2gpFst/LIOY=n4^B_6.qdfeXh#;J@S)#Y
`!-M[TT+JEI+cGCmIUQ`,$HYk+Jk$%!jpP`2:Xs:T3b)5UodL+MZVmdcf=.*iNMPW>QLrSrq
k^)$$d)A6W&9^R6$[Z45kf0pYJ\AfSN-::i2*XaMR7?rp6O-9$QV=j!N2Xr?14=_eAh\X&05
Y1c^j^us0io(BIDTP>g[(/bQ**(hf\7#s30$:5r_nB;7W4SgHpS3UY=VD<,P0tp"WkBkK=J`
)M+1+oKX^.2HZSl0f%5Xe'2/F,^,#`$"4cr%fD>jG>KqWFWh.a'-&;:"2]AemO[qNT$:5pcJa
U[ir`*ho/ksM2ABk4mn_c<Li`]AVpc^QDuQ2LOp/r8YCl5sc"cgWHsJ;)[0R&8I`jqg0Yu#MG
S@R7hb%/7RGa@L_Sar_$_L&\bAI^s,q$$nnnCJGhVr;h^i,JB5"1=Lf$#"qIXf&7r@jkdEkK
2V80l0IRp"m*U!BPBQr9&ZDJ0ciMBVnuIcNs)dQ0Vfjai*:bf_8cZ7s]A!"(l9o<i51,NH(7U
QM\0b=<(B$3)Dja?s8<7f+4fU!E0d\TEiKP;KfBrI,ODH&0!`'8$T\eRP0W:Q-Y[G3)A+)$M
nC$\h\d!Y9;ZK/o\2tl]ACtdS"W.`nJ-\VFfbN\B6')j*Uf(W&]AM43a\d94@!O.ekN`f@`b,X
=K=/>6UV=031tDH*&0(e9)31*.%_fp]A"U,\FF<Z1$JoEIGdj]A@ZYjPANhJ2#j>kUj[>:(+R>
jhc*;VR(@^XKPlJ<:mu7Eh&C[C@P[BnP73R&3r4+",kN.j2r__D.5,,GqupAe?Wa)n/C/CD+
a$luCW-2,]Aip_\)A^StPUZ31=-4QW!4:,u'AaM'QkDZH1TiQElHL`M2B(eHieQ.!8P`BUasX
gE\JpeUe-(%N?RC4KSdHJP<GXfI;3l\CpVL=;rU6&Q)V5npI)l<\Z1>Md$LD1mrq@mU9ci0q
R*hX)VJQ'+k'k>\d0Lls_-I9%FVmQ`r20NnlU&#*O6OThDRg\X'Fe_T9.(_D\6\R=&8d`P6s
CD'sb<'Uj58aaDd$#e.V<M'YaD!XJPOYq'\ifghQ!nY*AYeEshh<;;E*Z=7bjZG+nC.]A;oU8
dOHTSFV[X\Rh`aNVWgC.472?P%ut_[8RI(eIMICQ=iNs7NbYY4)WOrG3R=h=7`V7AhojU%Er
*bdG1/rCLj<qU:,Wb)JCcb1op]A7/D;l<`2./P5aKjtL0:./P*ZG32>Nq2K#mo'0*P.X(G6eh
5=XV92VY!`AU:mml`q)1HcgK<,ERa6gBT'5:j,QfXIZ4Km'<!UCBagTX<RVPcCTh*[0mV;0p
[gsODU6GCJ=m,W>K1>)PGZ*2Zr5(%3AqfRPXGJ8u%"`a?+Wh4S*SQd!qC?)/_S6=g>2:gIc>
.hfs%Fn$*#b,I.F"9n#u2O,O8sFDgAeL]A.]AJpNh%9V3uLP7kGLPP<3uW$\<D^eL["u;m$ZGn
]A-#5n@lpA9njVO,@+^%<)Od.=+bDe=G"HK$6m]A2(2g9m9*CuZ->fJMRRX,'b)Kdf/Tdcb4.F
56\C*'JD!u]A#[!Glr=fU86MCqbJ/OY-L20Oh&ENua'NCh40[uL-#b#.IC2!S+p+k8AaUP5O2
A$L!eY-e`r8PMWn4ckT)Qm;B,7nGjVkAt<g:*$%TV*_fl!dn@K/__\u*A_lJ$&7hao1#Z!I#
_.O_RZStDGGACNB%8=B&GRb:Jq^]A5;,WcOa-0B_#Ph"\<VS13H:eqg8HQ-3^S:m5Zun]AD:i<
tffhd/ghL24rsB%>jgbQTN#2<Eg?\'i-G`9XcRcE76&V4ZJ?ZX_el&M&T)?MUJkBFQk@2O2&
W7Y/%"QL4W3*j0WH4,-!bd-7rj5OOnO9"i'PEL\e3V[:)9a0U&ah1i\$ZX0#fUH/Ql4<f^JS
JEe79Vtd9>fu/%I;A;051P&rF)ikNDX7pM"0ADr1bdoYXK:mh0eBHc70F]A7e#LMG3OqcdeTr
@+K%U-.r\qRD@)d2*S2cVZ$U(U&I*%8VQJ!&nZ,g@O&]A#hs:*d9P3U-l%DIPK>s+gfQpaY0l
CNSWE5m#LluVjWK9kMe>C?[4\*#ia3"lEG[]Aceo3\d5)4)t2e2a@jkKnZfSCaV5WM@)%m!"<
W*@lZOc-SN_rR_9KD=N'Sq5LLUSUm+mAW';a1/G!um_*fm6MGeSQ8O08X94N)QejeNO>N`-S
kd;]A2<s6@B7qF[.nnO+6Co_/8?_GG]A]AaT4(<kuAZ4?;:`uAYq@:n;NR<dE,mW$PpnXS;mhjM
h)oK*.V[SYb*o>Se901,G"mJ8-j""is(K*i:r0Y<+Tc1;!4;<&_XZ^^lp&ief*T@Z4pO&Zb[
3s;K!ZFFFY.gA(8-U"`@&&*qTi]A>qK8MsTdHYsT4/;AEn@Su.2`TBHO#D^9aOA&^:_W-h.6;
T"p/ppcSK*T*i4*p#"C*4hiSN3#-pP-pFnF%/X-fj1Z3n`ennVfndaNfI\=7a"nr)05gotIb
2Vh-hmW!F6i4(`u:U@V]AC=hN\`p4n9cQQirAWL`p(!Vc^NZL_L5jb4_pan$c-J?=@binB&dV
;VN$/dC>i_<F`FUi8Q3LP]A?52Sta'j>JO'&&'M_$WG5X'un)e/n'Je#qjp+eC0IXcal+tZkO
<J=FEh=9=TP+LF(e`LpPVro$BF>LDMD/6s0U=Gepm<Ua"J"_H6ab39cFJ[6#\@+/,k9#2q)"
KMMRliA1q7W#_*AoYT3dK9o(&WUa!bi:9nDo+qGln?:HaLn,Np*_;FN.*TKaj+>;H*@&2_po
Wl<SCc%JRYWl7gZjNd<Q,slh\i+'FkZf5.M:&lEd%P(Ldq[A>X5s\mLZpaVJ/XgVkiGB8d%#
&WHi$Vb1q@<=u0,.RcQpoLZP59d$o5!D!D,Y#fAeg.ri,geKDZQV<$EqKuhHk#mmQ0\\$K?S
NCoI<7K[5\]AY)QNn6u'E/N@E=hO:q/:T'Mpsi'ch5RCDFAPu?hfm7@Q3Y[@h.hS[4W]AfL\+T
*IgfmGCX;"[;aKMk$-N1(T]AY--t?bOB]A<&8$9&]ANG3hJIhDU4QK\3^Il4D>L6S\-!u)>ZI>'
Z^mBU7Pm5i#U8QOnE=58lbkoAlW!.uA$V-9H=\0)7_C@T*P>RJV,K.^aMD!\-56JC8kB4_!#
7NYehK/k4V("Im0Cu)J&E]A(lSlT6AuPM?aV90GOG!&B.XA#"rHuL1niilUhKhO1[Ef7_Vbe#
$16STDn<Ko$lKNgh)%b2-G.*7jX8]AL!QLc^WQ^9f[QhQ*62B5TM%;'4]AN)I$@iU)p-An&Ui/
`6(MI*N'G9"\t2qb<DQKs=1:n3(f0"<sOU+489_aaRYD_+Rp2"3>:3b0`HQ%Qh=ndGEaDQnJ
7Via3&PrgZsqVC2p\NO?%S<jGToIO;\CZc2Q^27*dZi!rXLlt]AFh-":70q9<bZ<Ec>d)PHLO
*$E(=BE,eh"^S#F[e6T:]AeCjkJ<<=*l-U[K^$6IIM"6?)b[C_"k/oH;g&B;&4%"Vde9!.Bdu
,aS2Bb4O/[W\#mKM$lEt,Bpl(J#?&JQ8feSnC.5an$L$61C[id?h$.cWL.o"Q8MiOuYNM".>
re>(V;WT^IJ[CGg"b0%Kc]Aqr;-fp*(@iOoNN5ceFMc+:?iS.G2LTp`B3G?XlhAc#/u$Jnj<A
P.6]A(W?,kBS3?H#6RA_It?U*4FA:ia&fS*nbWiVYPNSC@AJG_P9u$*qPg.FB'3A-i*=HOiOP
_NR?cWORV\6QS"8MlGpm4:.7^kuS#(":p\O/.E8\&<]Ai]A^ULCa<bQ+qg/J^VqN/0#]Ai*it;C
!@2nXi\I)?Q0fkdd'e4Re&quckXB3)WV?W*Ar3kW<*FX2MG@I_1P'-%mWn<is8Vj6nQ.\SLd
P[u+Uj3H+)GOEYrK+WO#\>r7igQb41iU0iu=!b.Cf9LYA=b\OHE!m;$l1_0:traP.>^D+Z\+
W028A]A&Hr'CM+cdCShtBMi!95biG`fL[pG/?+BH=4,:+8/kYjg&4p\b1Gp6sQn*QnKra'!u_
ijrtX'JWYjPG"2_DO9+Gp-]AgqOmagQISg4<4qDUX<>c[CGs[g9C%c,8)makE)$(iXL.<N^rp
@3E2l?l;sks/+Vd/oB!ZaHl2m=]AU*dqOqr[%=Huom[gUD/C?LT0PpRc=KpF&$:&)pY`@N_Pu
6eI!p7^,*AjhE"+_Q,<mY"S!G9G6Q>F<Su"Q:uo-::]A^YGb4p.>,Od4]A'=/R>A6Y"7UO5T%<
$U<(pjCqnd!m]AEL2K\$_&/A@>]A_:rIMN5Yh>T>kQ!U@Sl[.XqTNJN_ru'S,ES<iSq&h+r&]A2
mPJ!&FV\e)c%DAR`\o?A.+02W_*V+q.FL4NiP:#dPMCs2CJ"b[H4Fs\HV]A>Ftd7FSgZ1@).$
bS1k+FpQ&UjJplM1DVu00m?2"``jB-f@),LXtA<rkEC2Vi,Gdr+16+W\':=bd`l6=dePiEX4
kTbEien2jU!/WYF@-aX;"RO=mC0]A6AU&ZFq7djXo:_CB%6::L^f:l6>KKa-TA(p4<5"N*V4<
HfP'crXA~
]]></IM>
<ElementCaseMobileAttrProvider horizontal="1" vertical="1" zoom="true" refresh="false" isUseHTML="false" isMobileCanvasSize="false" appearRefresh="false" allowFullScreen="false" allowDoubleClickOrZoom="true" functionalWhenUnactivated="false"/>
<MobileFormCollapsedStyle class="com.fr.form.ui.mobile.MobileFormCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
<lineAttr number="1"/>
</MobileFormCollapsedStyle>
</body>
</InnerWidget>
<BoundsAttr x="0" y="39" width="442" height="231"/>
</Widget>
<ShowBookmarks showBookmarks="true"/>
<Sorted sorted="false"/>
<MobileWidgetList>
<Widget widgetName="button0_c"/>
<Widget widgetName="report0"/>
</MobileWidgetList>
<FrozenWidgets/>
<MobileBookMarkStyle class="com.fr.form.ui.mobile.impl.DefaultMobileBookMarkStyle"/>
<WidgetScalingAttr compState="1"/>
<DesignResolution absoluteResolutionScaleW="1440" absoluteResolutionScaleH="960"/>
<AppRelayout appRelayout="true"/>
</InnerWidget>
<BoundsAttr x="0" y="0" width="960" height="540"/>
</Widget>
<ShowBookmarks showBookmarks="true"/>
<Sorted sorted="false"/>
<MobileWidgetList/>
<FrozenWidgets/>
<MobileBookMarkStyle class="com.fr.form.ui.mobile.impl.DefaultMobileBookMarkStyle"/>
<WidgetZoomAttr compState="0"/>
<AppRelayout appRelayout="true"/>
<Size width="960" height="540"/>
<ResolutionScalingAttr percent="1.0"/>
<BodyLayoutType type="1"/>
</Center>
</Layout>
<DesignerVersion DesignerVersion="KAA"/>
<PreviewType PreviewType="5"/>
<TemplateIdAttMark class="com.fr.base.iofile.attr.TemplateIdAttrMark">
<TemplateIdAttMark TemplateId="12143fba-e91b-44e8-a587-f5a8700cbf7e"/>
</TemplateIdAttMark>
</Form>
