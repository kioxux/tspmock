<?xml version="1.0" encoding="UTF-8"?>
<Form xmlVersion="20170720" releaseVersion="10.0.0">
<TableDataMap>
<TableData name="ds1" class="com.fr.data.impl.DBTableData">
<Parameters/>
<Attributes maxMemRowCount="-1"/>
<Connection class="com.fr.data.impl.NameDatabaseConnection">
<DatabaseName>
<![CDATA[FRDemo]]></DatabaseName>
</Connection>
<Query>
<![CDATA[SELECT * FROM 销量]]></Query>
<PageQuery>
<![CDATA[]]></PageQuery>
</TableData>
</TableDataMap>
<ReportFitAttr fitStateInPC="1" fitFont="false"/>
<FormMobileAttr>
<FormMobileAttr refresh="false" isUseHTML="false" isMobileOnly="false" isAdaptivePropertyAutoMatch="false" appearRefresh="false" promptWhenLeaveWithoutSubmit="false" allowDoubleClickOrZoom="true"/>
</FormMobileAttr>
<Parameters/>
<Layout class="com.fr.form.ui.container.WBorderLayout">
<WidgetName name="form"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<ShowBookmarks showBookmarks="false"/>
<Center class="com.fr.form.ui.container.WFitLayout">
<WidgetName name="body"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WAbsoluteBodyLayout">
<WidgetName name="body"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WTitleLayout">
<Listener event="afterinit">
<JavaScript class="com.fr.js.JavaScriptImpl">
<Parameters/>
<Content>
<![CDATA[setTimeout(function() {
       //隐藏报表块report0的滚动条（此报表块名为report0，根据具体情况修改）
    $("div[widgetname=REPORT0]A").find(".frozen-north").css({
        'overflow-x':'hidden',
        'overflow-y':'hidden'
    });
    $("div[widgetname=REPORT0]A").find(".frozen-center").css({
        'overflow-x':'hidden',
        'overflow-y':'hidden'
    });

},1000);

window.flag1 = true;
var self1 = this;
//鼠标悬停，滚动停止
setTimeout(function() {
    $("div[widgetname=REPORT0]A").find(".frozen-center").mouseover(function() {
        window.flag1 = false;
    });

    //鼠标离开，继续滚动
    $("div[widgetname=REPORT0]A").find(".frozen-center").mouseleave(function() {
        window.flag1 = true;
    });

    var old = -1;
    var interval = setInterval(function() {
        if (!self1.isVisible()) {
            return;
        }
        if (window.flag1) {
            currentpos1 = $("div[widgetname=REPORT0]A").find(".frozen-center")[0]A.scrollTop;
            if (currentpos1 == old) {
                $("div[widgetname=REPORT0]A").find(".frozen-center")[0]A.scrollTop = 0;
            } else {
                old = currentpos1;
                //以25ms的速度每次滚动1.5PX
                $("div[widgetname=REPORT0]A").find(".frozen-center")[0]A.scrollTop = currentpos1 + 1.5;
            }
        }
    },
    25);
},
1000);]]></Content>
</JavaScript>
</Listener>
<WidgetName name="report0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="report0_c" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.ElementCaseEditor">
<WidgetName name="report0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<Refresh class="com.fr.plugin.reportRefresh.ReportExtraRefreshAttr" pluginID="com.fr.plugin.reportRefresh.v10">
<Refresh state="0" interval="0.0" refreshArea="" customClass="false"/>
</Refresh>
<FormElementCase>
<ReportPageAttr>
<HR F="0" T="0"/>
<FR/>
<HC/>
<FC/>
<UPFCR COLUMN="false" ROW="true"/>
</ReportPageAttr>
<ColumnPrivilegeControl/>
<RowPrivilegeControl/>
<RowHeight defaultValue="723900">
<![CDATA[723900,723900,723900,723900,723900,723900,723900,723900,723900,723900,723900]]></RowHeight>
<ColumnWidth defaultValue="2743200">
<![CDATA[2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200]]></ColumnWidth>
<CellElementList>
<C c="0" r="0" s="0">
<O>
<![CDATA[地区]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="1" r="0" s="0">
<O>
<![CDATA[销售员]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="2" r="0" s="0">
<O>
<![CDATA[产品类型]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="3" r="0" s="0">
<O>
<![CDATA[产品]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="4" r="0" s="0">
<O>
<![CDATA[销量]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="0" r="1" s="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="地区"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper">
<Attr divideMode="1"/>
</RG>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="1" r="1" s="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="销售员"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="2" r="1" s="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="产品类型"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="3" r="1" s="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="产品"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="4" r="1" s="1">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="销量"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
</CellElementList>
<ReportAttrSet>
<ReportSettings headerHeight="0" footerHeight="0">
<PaperSetting/>
<Background name="ColorBackground" color="-1"/>
</ReportSettings>
</ReportAttrSet>
<WorkSheetAttr/>
</FormElementCase>
<StyleList>
<Style horizontal_alignment="0" imageLayout="1">
<FRFont name="SimSun" style="0" size="72"/>
<Background name="ImageBackground" layout="2">
<FineImage fm="png">
<IM>
<![CDATA[!Q"h"s+sQI7h#eD$31&+%7s)Y;?-[t%fcS0'F4mC!!&3S\C(?W!/X985u`*!mF8rZ!FZ[mdJ
No=a%1@h_DK4?Th+3iLAPnbcJn3*eJG^HS,cRZlN#j-R0V*W4Fed@!AlMATS[po:!<]AQ''&q
S!*(c/johW?rq]AB?Wn:%l"K74Fl&bsGYo0\N:_i;F8q<r*qCr#N@^/-F5ZK?G@ffUuGFT;=.
TZKo!NNm.OB:@7P:"i\!!!!j78?7R6=>B~
]]></IM>
</FineImage>
</Background>
<Border>
<Top style="1" color="-6697729"/>
<Bottom style="1" color="-6697729"/>
<Left style="1" color="-6697729"/>
<Right style="1" color="-6697729"/>
</Border>
</Style>
<Style horizontal_alignment="0" imageLayout="1">
<FRFont name="SimSun" style="0" size="72"/>
<Background name="NullBackground"/>
<Border>
<Top style="1" color="-6697729"/>
<Bottom style="1" color="-6697729"/>
<Left style="1" color="-6697729"/>
<Right style="1" color="-6697729"/>
</Border>
</Style>
</StyleList>
<heightRestrict heightrestrict="false"/>
<heightPercent heightpercent="0.75"/>
<IM>
<![CDATA[m<a7dPNTKk4aZF`[B-\bD$@5).6f2AU)"/U3M!V+WNbY;^-JCr70Si@J.P&1?`P\uBW.a?2'
c=aQ<D.*&-<0[UIBtU+cCpa6AP\7J@n07ng&LD4=>r5RDdGAoQ.Ioo?k;q\Tse4ja2K\mrdH
qL]A<Hf=npHIIl_N)]AS5\'Xkc*Xh@D=3k5C)'-U7<SEqlT0Cu2gKD!T]Aen]A-e2PPX[h;/#hI=
".b[TpTA>GfAC<6ST\:SmN<4lSi=fZp&PlGk01=UHcnlT=O/UU!KX=]AD#mSTsSTNZ'2AMTDt
_U6KZhWT<3AnG!g=l*MrD7g0.pU^9E[E-$8IjP8[=jWC:i0V7+j,^5PP5=MIhs/cuEo.r4l/
%HV9LL./;%GOX%$V@RcU#S&G'g,RL0:ON[1!FF-_NNlt=l`[LY2SI)E4k=glI0:_SZS^>QCi
stLC'D\(0$u<-2'&l"e@f1R1:2?P^O!RQ'%:!FAPRQJA>8Y;=O@<PK9rmunkWYgW_0`VNbGU
*;H?Kb+q?(2knsEeNUc(lf>Kb:g)E@bOWA3@ZgODPs-T^#AROh"(0WeDcWILY[A#^PB^*+@L
&Tk3Ht'%!a3*r"U[?M8:A2g;<Y`W[\uY&@<V)l"jd=>3&+T7kpM511Vt?J(TjJEs*uD-9Wp_
h+m>i8028/@$4X!-VA/Lh^lDptN'adq43dpOUpG?u0Hd*>?i7%\VklKo@i"#Gr5./!Mhf!gS
4,U@;;j\bP7'5"s->_i9+u+V+"cuSI?@qO2Ft?$?R!OU]AqI>OIF7hVB@L[=i.'8Ms;\C\M\_
*GrmceItVe_1g^Y+[=<m>m#OC^]AVLJ6[;(t,2fJSK4eZF:-'os7<%mr:;UM3c#kbt)I(6C)A
;U,D5MKfr$SN]ARpCpMOA;]AeO$jQEC#<PNen]A!Yf<.m#O?jMC%>+USoMnJ'3.4n\rJ/U<[^O4
\D0E+-760ZA:NZ1d8/E+/'CQ2t09+m%(pcXq'mMUuk0VLsgn3;a_l#K%SHb0?&V=S5j0CpZ1
.Ss+1!.(dmV7r83Jq&,#"s&[/BaT9tF<?&L#1BdA^Sq^5!m%'%2d+/(>?TW1i^!Th7mHddZA
)"1Noc$s8t:8.;:Wn2\aM7#fC0;8MP^6kHi8"?eBY@85MMk?L;6V"de'/qV/frR`'DWlO!O'
qYNV+l;TFtG<gmosdl.@^*>LX@>RVUQCZOi)>8!ARY\h/H6:OdOEM3jbe)hK8s4lVFmCc`^m
2-?mFKF+"PN`Ei>/khAm1<\W%hUI2(Sg"ch\]Ap_sWpF_3Q41f\4pY&J\]Ag)N9j83t+biE2Ns
#*@2aCtP=<qC7AV<hIHSXGJgQlCkok?Dl>H@M,J@\Tl#RSgVbOg8KOA#jlBX3!C$8,5)O26V
=SKIS8@*)h29Ej6]Aa2/T\3p/Tf#Z85L+ca3Ou:*6d$=K$QhQeZZ2_9tU1&b*622msh`B]A%1C
@$ndiF!L;'hEM0$CV3Jj'$UM&E]A#%p=\=I_-48%D]AtZ#0aBIqFRQMiA-nQXZ&6MMZmr<ui@o
U`Z7o"^e<UDr9?-:Khb1f%c<eCWD$mm\4p>]A.PEG/WskZ>bS%dR7Wk7*1Un9G\k-'AK$FS><
p=3>>OOs",W^:q7"B1o<U'm>Y99I,ZHg'ae.'dG=/l&i%-Pql9i:Mi@Dg\UabLg\FLq[O\)U
]AMKG.^=GEq!S"Pi+-<3p"ZPC7']A@cf>\4e774kG(FnmAbFc,.P!0;78EZ/LZ'J9I/p04D6G%
G\Va\?u"e*J:C<ESZT@aa4M^&52%I$3dG,(nt]A]ACEFhQ_s8bps"A0tu)dD^+3`UaI@=&^*58
*F$<7l030Mr94^"Mupe$-L&Q:gDK^ZB9)<(0mTiKR`BKhi'-4Dj@aY13maL_a?Q:Z8R5`.EB
hg7oM_+hSL!VFE9t2O05@fE<(XJ5P7*7)W=6/YIG-^RVEbT>/;JYIkc>Qu<Pt`E9V_rTNJnQ
*RBed=c_-U)/oibT1/<Nrg>P%EX0h'W?n<VOYFcdgPVU$"U2j]Ah.C(*,JSGJ4:RoZ0^FBj@9
"8EA(o##*YXA:"'\%ZEFe^3Z]A8Xif/34#BTRYhLT,r0VNer*gR,"HI?5%PQhFaiX?p?>l>D>
mga-m"<A%j]ASF]ATmbj"ai@160IU_gp^"]A;e=%8m(fHW&r&?d+80OO=iloD)<(%>#&+RF"T\-
@Q5]Ad,F0eF?&I_Y2kOZURtaJKp0:DDitJQDf@b1cF"mt?i<_XumAZ!:>7)^Qi(9a1U.'87,@
tD+0:QL;,;N:q'GN%C^6)HaaS$js@t=lN]AE>qj`M$p3jO0LSd(pmL.R+\RG0CJJ!cgS`D66q
W*c=ONcJ'^S9A&Yj@B\KZlmu7]A)Z&OUTOF=%--kJ]Ark<1MTDm+M=Gc(6p/Nf\g/VWaM'XSE%
IIT#3TZi"KpA,]A5o43i?(I:j#)+5t-5-1_$B*%HE(W1q=42C=4o3dQLas\;]A4P-m0`kD89oG
liml>i0)`V%1d,63=Bo)KW0ans@UiFVng"k&PdTTs!F:YCN;2L"M&fp;?'sa[OUh@ZE$./rc
%R.H3MQU_'kr/BE>hPp3O^sr:*4pb-OhUa;,gX,[F++=#0qQ@!?YYX7Bu_oL&3bCQn>RrA^d
k*5LVgUJFZUd(cp<jZ)?q?"i%q8gUKoL$0&R[@CZ[sT";TO0cMsq>!8FSqWn6CR1sJ@R'N6i
!$X8@?Y![E_4Z9['imi1?2R6I@`Q^*5e3gCu"/Pk>6?4u+ej>!bbKY,K)BW$pWF1&N:W<^]A)
D+Bl?AE,C\N&RR)r-(H3?m*7W/'g_(V"c^!$1.;WX:pS6Z/"(ktg%[>=k:<IJU#DQOi93_<B
t0k[]AF,YEu4nB2-T`8[Uud?\a@pW;(o7'[9AmSfgc3ETkL^C0J:`<D<GJ%srdQj`t3S-,4F]A
_e0urCq]A*PkkNTWW:?+AK'48Z8H?8\S+pR/Q,+&J&a9pLGq`q]Af0,\UXRT#YgT8X$_MH?$h$
]AdSd#L+sI%R=S&.;b6%TMFqi[p_(i6nGPPVfAG>iRg`/G3/:)67#_.n7>9)QNOl)K0E#d(\u
Z`521/%CH)\Em:90W&<QDfc&8&7W^^%T;dC3r=t5Sii@cLV=(XgVSF8rWME`)%gD!Fq&t?\d
UEd;(aZFhpskZ:k7pLq`VF8,g%c5p11PtfSoT6fZ"YR@qhl^ERJgT&0mt_=LdM8hM9fU&2&2
P0gk*ilnX\SI87c40#[3c3G>jYIC!X8gLuOpNMh=^Qprjl<S;ZQ`,Sd)-M5WN#m.2QjhoTH[
4PAD%.>_g[_X>=uTE'\3:p39d:4@cVEe$%51dX+:11]AX23[\GJ&/S9WK40dY-JYuo\u)n$*C
oiZEJqBAlT#o+&s1O&SaPa/<=''Sq`lnks+;*1<KB`tk9]A'ZNl*(grjYXuVBT5:=;_</*pQ]A
Xoui?7V?r.*AB=kI6sD_P!TIcp$'Rj%m.r]A,S2N]APF'9-HEC);3DF:dV@'8ss8`8(FNDu1ZF
dn4EYEa?aq/+,al*fMX!dq:?!*>NjP,SfNJt!Bp_L?khVJ\qC?;3:.7cXV;(=Eih^?]AG9$nq
<=h*#b53]A0S9S@*`t5a@,$X-%Y"KIW-tPBLu[eiG'5_5-8k@DY7-nsW2`U>%4^(BN60YP[D]A
0<S<)8$ZI#1=<gdi`gd4DH9Ri=7$lKeBo4D3t`Q(bCho+p'T0sF_CbOokEh;Z-Z+&E$#f=Nn
<58c8(Nq6-WD.9kF\m%*ES&..4'icCDP2dg+_p@`oOQ:$CgdSY43!iBI1T2rZ!m0uT!#[2)F
HJW=[@Acn,!k5ji8*^5-=9S@gt(nUI$#UO.`^8[,YRQj[p88k[V68C.L.=ekrgJ$bB=;H+r=
(c7=l"b3ZCWD_M_0!.)R2[NWoP3X[(e$m&O*d]Am@5e8E#9/'h*@<""]ApP6Z)3UGFGDco0Dd'
V1lgA[Xc)[=d,Y+%lG_\3mm06S_B%N@Z!W<XI<skg(!+Z,l9Z\+-_\HjPmu[<>_r_b7MZn9<
0G5EuO>qV_&ms(DUU]AGZ[\bM_K>Q,:?"-=&86R?Yo*SJ9V5&k^qm?]A-Y%R<S6)6D`!E]AHeNI
#Kco:"(8"?JLNTRBg"Q*6&QOcV8el5O\AqBk:N6[6G?a)@#W_L`LC7D&Hp?<?*uNHJp/Wu:]A
LbG8>i<$n[W8[f0O1:csRC]A++>V%'T'l[uI@j><j@f)L=_(_?!:LcI;=0LnAD9Ac_ch1>7o)
h=hD6Qi,1).=/m/5`&Y1'4<O]Aj>KG4H.UN?PJYl#e1CAq/4^RCZEKd-rWH0q4Fb,)Q#\Cp02
E%?>$:afH[TW0-9u6>aJa3Vb5361jPQ09?IKs+D=12*B:(in!jOP%r./=s0h*&=)@[Z^Z^ZL
8k3@SQP-l_/SeTRJ&OR>?j-bal]A#SB,lL9=+CE9WRN46@iKVfI'jI%fC'4MG(ip$@:r7<*57
0bmP8b:jCKKu,>tn)XC`m#e)H@*^d-drfV"`_b4&aDY5/s;Y\#hhVR-sq7aINJn8#o>DC@8]A
,_#]A]ARGoXJj<d"I[I6NG#?0bKnDMV_pq+^+T#Vb=?o)PZsNlV#DF)XL\l.T5Sh:(Tf8LNFDG
G8LN'Bt,bPC+b.#.cn@2r1BP*D[us;tWh,*8T5,<r;:koV#9kR6\XlE^H:gLP([PYa2Ftb`n
.JasM#*kSNpH15ao$9EEu&mR7uDS-g]AVZ0M/,e_Q6h^_,`7=JmARE#]A<n#Z%HW,n6B4?)uAp
Z/-`oT9UtMOK'r@CFVflA7(O+O7mZVC?'p;J7dG?/l\0oT]AFV,'a2!/TjF_0l!;X!\r1,lD*
QFLKKUfr]A:n8`.i8_Tf0PrhOfC\d'murZdig`ak=1m$=[;&e`<L6WAG]A#Y5iYnpnY+^]ABWOu
tK)>Lmf7);(nD)Q7Tn>m#+Z2pJU7bG,?b^pK*@`*ScmO*=*cLWE?oip]A7[AcU\L?nqV(`)?l
u?uaK*p7#&CYA674fqM-Adp\d:h)>l.dLY!S9EHS/]A[?6GrZ`j:Co1TZC9+&i12PfK7Tq6"2
m,QG=;mN6l:&3G5XYs#u>O2"mi0U`MUi>usUKQ.)A6)JjU:VA9]AuT"kssJ@2<#M6X!gN1323
Qm&)#>[\dUHIGa[HnCh#Le.r#G]A2?BK/J:S`&$2DAKk%k@_(nZ'7+&_.,f8l8*GqFd8!%_?$
ENIC6\/S"0g@(DLnFX;l>"VkA\J+f;KrO3P.sU\tS_iDh?XN(Fn<:/QW8FQ-h%Q^ti]A):%,?
!,DU@U&3:(-!<.b0co>BV2Er)$)(+bqXa"dRa'=M1KRU9#!8)#f>4ZrqpoO`Oh/Q7S"hU+JU
N(B)jD-#;]Aq;b="12k`/#p,,5kd/]AapaX0=#fd21<U21gr'<;9J@MEV^?8SLlIE1b=P*XgXc
L)`]A?d?]AV-!JYP<nHN)bdIi8rZhD`04>+`pk\'O>^s6;l;_UddkMB_IU<m??M_<^5&R6=k^o
<:5TdNT1037[64CJ:Cik=-?/>UaV\t5@mHc4!?h^QqoObmpG>k$FJcTCejjl;ueBRHZE/Qc,
@kRnO30]A(Ot6\J(&uBlp\H(@0:,FEs=ntba9N=qHL(B-L/Cg_M7L7Z^iY!5)J%(.iK<)b%1L
pih$(Yh/(\N=mrrAT"3+F;THsM&7gcd1TDCle=kJMOE$gEBE#=oiVf%42./&]Afa\Ekld)DR]A
'o=dK3#RM&).o:\c7/=T.Q>WVc*=DM0Ikumn.3HlC!4OT+b&^*(A\Qr0P.cF-ctP-HYric+-
+^:=hfth9:_qiM&r6`qnQj`fH.399#Z'X^Sf11Ogq30n&!7*^lT"''75f1oHJ"#/FgURtMh&
Z7V%dCSAU]A&n,aXd0*StFGgJ($'nQMn2]A"WTd,sas1sX3H4)#fg;HM#ab/rfRaV1):t(Jd[7
JGFom?CbDUK(]A]A7TKic$nD1WAa"n+HG`P0MV?4KrI,F:mJ?u-d`C,er]Alg*gBJRhN5PIhR"_
Smk1AHC\!X,W:&/Pr:\mj=,6+71Lc@n,t(*J6"['dp.+u@EXT><LDJK3kV?$\'m)\'F<-t(r
bgH]A-[m=q=j\H<-2p+.ibGa#V^&sAqQ\Xnm-%n("]Ah\94:)]AY8(;/F5B,d<Nng]AIPD89JXT`
o?SQedq\4*nu%#+Mf8K5Cs@bcD4E?/l1+2nKTh9P6K2A^<G;U9<;+q/YT`7jJ76T-j"-J&n/
%]A-YbXh?-CT2HV8qKP`rCW<S#h1;0\V_Bq-mEFAr9b(j=4$0V<pJZhP(FL4OF:H:Xr4m\`Bi
:3V%L4d+>HS\TrdG<>4K,GB(1#Y`(Nc.Nb]As5?9o%nk81\#_=M=5@Yn%\[^S$\d4)7tEL-$g
::f\PZ+o0^chN(2p8-"S-6]Al9G2`<Qb(u1Z:GXW%/=ZDK0"I>*3i:qq<_hk)<`4,!?0UqQ@>
D9(FYN7nlLEWur,V2&cGsSD/NMg'si/1fM\%ZfJK`7GI&lldD1<&e'EVi6-B[Xk((Z[3*_Jj
kjC^B(XKm$hVO'F$Z)of6T`?hR4I8#8Vp;@%iAtan;T<Sd\.(pBtf@YCLh]AX5?J)!G^3pQ=B
,2Ni6h<N:C\A5$AP#/Saaiu.$B`n"L;(#)&ko5/-]AO"5=>mt:BMs[;2+fs!@X(kHaH-_W`s)
Hchp'E$S@@P<tSFjr>26.pWr/:`NO+l=>i$j'Uoq:eQZ,L1T2#@GTUPlS7oW`KM'qNF_,-eN
H)u`1(+)9%_-?`ZnK]A8jZ/AO0\XKZ@VD%P910UARHgPj\h(D<uEPqh']An>TMs3=(XnR5:To9
]A8O$.$Fceq3bfUr+2qAW[8N`9#J0jqjcBXDTSX!e=i'D$iV2^0j#Yq>(?Z;[HUSFF!X'8hNq
%W(JU>V-PmKZXlG?/YV$,n4(@CUM^K6p74AliDD)fe3]A6/MO4)6G%"k7#XnMPK,or6gN96XK
N3p>6M+Q`0J'M%KY9^*[QfsF2HC!+)F;cqP(N>Si:J]AlsmR`%Y6i<$<<b8#XrDjgC$Y!O!Zj
f<*!A5Zeb;gIr<rFP!mM_:X!82BZA&!56lG?]AY$P=kO#:iTEhsFQg5"J&=-&>N3Y7J)uGr"d
o9pti!L(VZnEc4fL+^bpi801^+iq"gpO;-ag6=E%R-fc5Z7FI?2+k/8.`DMomnk*\'[$"NGU
Nla%*C<IY=g(b('!82G`$L9_0P;2c[gXBS(G_P6;r&QDiU@b=iLY4/,(_"2plfa>E<2U#e8"
!O[a]A#sad4OTr-\j8kj6^*PAmX>k`0)5KCth_[+[*.U<$rK8u4CAafk?AC2u27CE5nYoEj&1
1%9"aL,Ya_^8>#&bQLp\6`0r>[BIWZQu$^)!XU,%m5-8iX7R0pi/bDVB.`q;V-L2X]AZba89t
>'(OsH6!RVohWG^9%"#)[;5a*K6M+)Ao7L=?,ge[%u0#sDKDV*DWE^K`Qe8nC[+O4rBYlTrs
NT"9_B#%8AcYg-[f6nr&8'B(98p@5,XCX>_kEc,?R.gtY26PcFsUh+SH9[Ttf7[WrX%CXiF7
,7)@`L\9&S,!qOJ;2[9F@XNr,)WtM)cP![;/n3s^Pt:e:ibO3;4-L)9t-F9?n'E&(q)/+7&]A
77`j@s<cnk6pUkt*dM0@d9--<0u7FH)@I,c.Ch8tqGD<W@:%,W5#p#C*mAm_.l4]AL[1?IK6o
j_MBe]A.11m8#s&`q%?`V6@Pq5P>kXb8NqF3*6AHL[/PCVMrF5+ac/GP!\A"#1R!t>=9riE.N
.`j$#CF5qcFbCL/*tB0s,)Y[gdS,-Q_:8[pMVfj\J7bcQ7$n'Y.AFPpu^DTaX$H[YOQ(=9aW
12X92[-4GCPGGJQM\CiuC`&7gT4,-JdJ+]AI>(A>^gM'QSr.@;d*rnQL1*u]A^.Vp2NAYfU,3b
`&Y*FY_a<o\TEso*ZQQmPA!ZEAMgrHLfseD+u/]Ah)&ba]A%))N+/o1/mNim$\t$F9,#^jil$*
cA/=FA^]Aes>an>a3*[Ubnj_o7n;#LQ#]A3k;3T"bl[AV'<%@hj,rsc=Vbs4k^4(N<_#\HHhf0
&YE%jGNN'Em2Q8,6JA$[D*o[JY<f"/<CWQ8'=hK6MFm(c=>^TE;rr4_N(=MMD#WbE(BH&YHT
;-Bn+k^.g9L9EZ_b5oEMn*__t_U)50CT.9*VAbSPJQS]A:O[%1k]AC-]Au?=[+67+U?=YZ[O3U5
C=1F6mcuGVF.??.I[s3OW4JS*$Ba\0bWbt11f]A5*(IuD3/oQ29\i)c@-h"2k*EoLr=UYo%5!
DBXL:m"_c>>o7+`*IDAcq#d6In_:2?+)_]A>h0:kYI#!@=W$+CoDbj"Bg$C[I:2$\KQaH7OA^
7(5#caSm$b-SF@/E_j-mgpe6deZKb3ZR)E>g#2#3"PAU6YUP2p+RmLmFWM+e6Of3_?\1_0[J
EMJ2E.A`WKgJ,+<PK0sWOSa2#Z7eOG^B)um4P!B5Y3X=fe,UkO+%Q-36V0HV;*):,#]A+D^7]A
5+o"NfD_I>HtC4[ka$d;+Y@eC6^UPZOS6\"]Ad(7n+jcMh]AB<O+WF<TZgp,a<iO457Fj)26+@
K#=6]A)?o&H8jQT53`Iut3:)h4#YMW9Urq6DF9$Sd+pRI<Y%.#&9-d(c^(k59D\RNEVqP5W]A6
T=m[]AaS"!c.RKj>+\l]AH@F"]AH%eNKnT+n_2QnC?(SQn_Np["Ch9SQcgWHPF5THKc)tR%@REe
4CP\#-0Q14VmV4eT.P)Q31^eTm_3/6CbrIg5:9qZ@K<urDb"2Ea(S=j<60%eeNhNldPIS0?P
0tNG]AR]A;2N*#Unf9d+g`Z^30R.^eJI\p=s/ek+LUkoN,>2hY1+H7+bM`%>FIG-`P4#"[52E(
mh2R9U<$c[ET?*1<Prp6&r5+L@L%2jB"jDRlk0ZR&=!MES8LFG9n@U/(.V"(CCcXoj$><:hd
P`bGB/`%O5[PT[3;`9bpTOc/tMbiC/,p2;CqQ%Y`0fm`^;1XMon8i&5J`6!QVgIXbj1XP[cS
j)*"_OM!QrK/o`:B^^>(?_2(KHpq?Lj\3M1`6XJQ/9KdPk.uN/a8?]AT97n5QI@CpW5$8bpt0
E"=$qL>9qO2IP2=MS8/t/\qC3AA8!c-aOSDnEM/HLTF<%j82i9SK,oli_7JZQ.klZgHFRdt?
DVOVm5FD8DqV+Y=p>s(*EZ5SlhdDE;$KtWPV>5V\qAoT*:\P`rTJn_B:;=cZ^E6,SI.R.ogo
MOo\nmT"iqYLQq:u=Xn(/[DF^%^X4"HT^Z0LgnhrJFZ9(UHf1*SB^e1oLD-mEL#[gbi:C,tS
T2)2\NCg4*C5L\]AjH/Vl_l*LfaIBe9Jp8#:rCOn]AcpO.2ac'jOCPNd#_p<r;)il+^-[:]A+<n
_Id6=u1MIG]AiVeer7kD:]A=kr'Qna\o;eT>XDV<6c-8$L]A66EG?F`7cWEPrs(49`mZW6B's5a
l*hf_K4BE,ZYeqCSOO[nT%Js<sQ@Jlf9EU"E_Q3"oaN[[2Y9r.-jH"HWUmhCF?DuS@$kGneD
iP^-EFhV\(1Ni(V0J">_OFGjKf5q-`9cR+1DAHu3l!sMD(!AG(]AiK)p;ZQ$d(*(g&RJ$'=H%
]Ai3nn_f,Iapg2'K'RE*&Z=M=N%;KmX:T.piYp=<oIhmNW6r<r>Vu,@oG3b$FKc*)t1Zoo*F#
O1A[eaK([[,m_-&[J9b+;<V,'M)S5J7>Q;B6;1>)-8YEI?^7SD?k(5Ceb>+uG^[N'P8DbnfM
k&pr;QjA$Ie[EHIPCS"!:G4i@f;\XA6!ZKD,G?FY#f8pF!FU%r^7S=+CZeE+aL>o@,gXUSmP
\#nG7qL`mte([Wd,NNuJVJ%%Iau<I_hu6bGeVctfWp$OX*;kdqWNI[.*,d1%HF%tkD%B<J9>
%M;nAR)jb41@]AUD%?sR+8+Oa?ceb*BH'r(I@8AAZ(/8T=2>/!B>64h:L\7+N=QK[c.+)rZ"@
?T@oEnf<8co/MO?F&M4WWut=uj<ti5Bj"g/AB%LVE5MqFYoQgco#t@Oa_qT2`2pg#Q%\0)!;
j:%9St*Z!&f/7RrD60kN=Y!=OZQYDM/=).o[G'5T[ql]ACuc47e5h&(G.:j"\\[A1&NdTmhQp
8$:"Ln0K,<d+`4CnRZ,.Q9C@q",?_\psqqa)H:>QpBk^4?o9(kJ%Xd8\[-\*$,n!,erhJD>,
#]AhaQ#_lN$JX'%b@sI`n_Z\HHi^J!8]A'I/^HT0Qe]AX-U+O,__<E9ltqEBP)oYk1Jm#i#<jZj
kA$j(/j<_O:`6")S^`[fri'1S^U;NJp4_?1eD/IEF8%I$M5gC?0Rnt]As".an_+d:i0_I\]AP%
!"@\rNCtji!;3R=S7)INhrpK2Zpkh8bau:TSAi73k/F4#uRVX(G4M6H[)+W>d"FMAjQYBL^6
M-#mFf^gg>n2".7!!TT"?!G@]A-(rjpE?J'hL;^k>>p6m9qXF?O%cH6h;K6cfKN-A=:Q1WNjQ
>NS_D2ATNp1;c:'eqJj2mnlmg#)Z=_,JSV:GBi="E<o$7OgJ<6HIFAF/DpDs.^n0HT*!ZKpN
"#I@C;W4Z%(MIoMSo30qSj1cWVq&'%Y)5FfYT150!LKd"CZR8t(;g@eM;"K[B\Y\M2uB+"fh
e!,b'-:q8A&Dc&]AD;-/]A_5=,(p$ZNC/C:5ZiT1k7#"7.!<l,PC\"Z^l#bjrE'1e(aO02.N/>
0_D.83?ibnDcQ``!bTFA.tP)b?+sE1,B<=3]Am%XG<B'Nm4PZ%+C\Aa&;rSZ';lY;SR2U<.+Y
$5_3Q)T*s=<@u+L9?j.U".aG^A==`d$*Ks4QH$F_^)*Q!_N*c87ATa6(c%Cam<Il&hCOdR1O
\gN4S+Ul@ol&YG(u3?j.L-_&(]Ac(!hp1PhaQ2Ws9N,JmO]AQ_Cj%B0;g9E^SMOqj5VO8(//22
AlpWTW,@cXQX4G8317/T7j"t>qPm_/*:1R==P%2AT39:N:$Xkk42[/MhB&rRrp?7WP^qEIuY
#OkJ]A$;A,8O'+c@Sf.QLp(9Y)Or"K<$:(Wm'i4u%npUs9U#U+2`dgRn3F1S\!H'QjPk*YYPL
R!R`AY^I2SdOXP=G:\TRpF.`,MkHVE)Hcla/78`L)0##cVG%9<>%TombfYn8C+HF`Io,.[6N
L`*f,khJ*$?`;s,!D/9ch(B"-[Nq&LW'Z98g^ER#_/:B)]A-VeHt/js6QSrVn:;X@!Z\).$5U
``&VSo^WjZr:g>WlpibJ^4QJ$486E;K#6:+8R@nSpYjZlJGbthju9`[Wh[1\eoZEZ*RHs*;!
@aCq,b@nI\$^<d^_Aa^a/`$3uAg1ZmQI>7O.$kM5S3&%to4M1sQ"Hj"VUa&R!>!`F%q7Za`L
K`l44]A/]AKYj%jT!r<k-SnG]AO?gJU5sa<M%jJB;69</i%P-]AmuH@eQc)o<ussJcY=87:PO'd;
JN:R,sBrg<W#9Jum7F'\("T6Ko`j/^4'I0@ajt.lKRSBD`h#=dK=,RkFuY&6c_P9jR89iuMR
KG86("(bHI'b3Q6AQ'_P,DPj_2+FlZaGTh^f/;17\!MgqeK7TA%nBh7?ktF\JmF+GEJ)T%f#
W7\_Mpt<$O6dGU"@q9%UXlFHK!9`#;0DZhH_aLU-(K`?(!Qd!G(s.A[AREDpt"5<`O,5-J4h
j1?Z$I/D19EB,tkre]AHa*Im1I;oLg50V^roUq\OCX:q<a$s2j1[E@mFQg@WEgke(=h8_##iB
GLL+V5\"k2Upm.<('M4%<V3P>9_+s/a5[(")mm:0&jnq0\a:Qt9"H1=r:s3;73t'Jk00(K+/
N;UbkSq4ZMe,.QQ?u`LN+[+2Kt^/T)[^E2cJ:6pt`(`rX/~
]]></IM>
<ElementCaseMobileAttrProvider horizontal="1" vertical="1" zoom="true" refresh="false" isUseHTML="false" isMobileCanvasSize="false" appearRefresh="false" allowFullScreen="false" allowDoubleClickOrZoom="true" functionalWhenUnactivated="false"/>
<MobileFormCollapsedStyle class="com.fr.form.ui.mobile.MobileFormCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
<lineAttr number="1"/>
</MobileFormCollapsedStyle>
</InnerWidget>
<BoundsAttr x="0" y="0" width="250" height="150"/>
</Widget>
<ShowBookmarks showBookmarks="false"/>
<body class="com.fr.form.ui.ElementCaseEditor">
<WidgetName name="report0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<FormElementCase>
<ReportPageAttr>
<HR F="0" T="0"/>
<FR/>
<HC/>
<FC/>
<UPFCR COLUMN="false" ROW="true"/>
</ReportPageAttr>
<ColumnPrivilegeControl/>
<RowPrivilegeControl/>
<RowHeight defaultValue="723900">
<![CDATA[723900,723900,723900,723900,723900,723900,723900,723900,723900,723900,723900]]></RowHeight>
<ColumnWidth defaultValue="2743200">
<![CDATA[2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200]]></ColumnWidth>
<CellElementList>
<C c="0" r="0" s="0">
<O>
<![CDATA[地区]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="1" r="0" s="0">
<O>
<![CDATA[销售员]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="2" r="0" s="0">
<O>
<![CDATA[产品类型]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="3" r="0" s="0">
<O>
<![CDATA[产品]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="4" r="0" s="0">
<O>
<![CDATA[销量]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="0" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="地区"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper">
<Attr divideMode="1"/>
</RG>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="1" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="销售员"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="2" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="产品类型"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="3" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="产品"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="4" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="ds1" columnName="销量"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
</CellElementList>
<ReportAttrSet>
<ReportSettings headerHeight="0" footerHeight="0">
<PaperSetting/>
<Background name="ColorBackground" color="-1"/>
</ReportSettings>
</ReportAttrSet>
<WorkSheetAttr/>
</FormElementCase>
<StyleList>
<Style horizontal_alignment="0" imageLayout="1">
<FRFont name="SimSun" style="0" size="72"/>
<Background name="NullBackground"/>
<Border>
<Top style="1"/>
<Bottom style="1"/>
<Left style="1"/>
<Right style="1"/>
</Border>
</Style>
</StyleList>
<heightRestrict heightrestrict="false"/>
<heightPercent heightpercent="0.75"/>
<IM>
<![CDATA[m94:-;cgQ-[53']A.@*^Z[;p7":Ml.tC;#4\fG]A]A3UXDo8.Ktu9KjYK(,1kVig6O-X9PpII\l
GhR6qBrX0our.MF8&bPY8rJ6qK^uOG"l)+nsZgX[.0ndAPNdkHt0@cd(B_IHu6ppO0fa2S7j
MLOK$@(SC?inTjuUeX4,Ipe4'MpcLGkrK#;G7SQjcO3c842_hN94hjlN&<jeQCY>$rN4uhTF
#C^S_3T,Kr@L,.$1<@/%l8H5cXf?8qVoAQH=),8CiJm=CO,8Aj/P`=gkd6GT%27p:[P4.PgO
aR6C/\Zic#]Aq-cW<qJ&$)jgDqiSK/Rh#_DXN]AKA?)+WnN7NG<5f6]Afp5WG5$mg9f^B*A(T&,
)Gj.`@a?2+CWKAOJ`f@WKoc$mYo[8*S.U>sj4%)gGYBLe4BF-'?E=*l*dI)"^;t/c44F=u)P
*U?(L0@=7ia(s(T$pk?GehYYKU=6#HFkVrAUUKYRQ^B6"W^QfNrCupib-tM!SQYXXS4i^NF!
?nMoh_dMcdSkBpP2iJo8c`_3D<*s7s&--iV^:6<N"Ffuuf_Z8c-IYHbBTO`Oi+t=:Xc;UrtW
Z?0(/YrfK2K]Aa8?XV<XorM\jk%6+043rkaa"iGhYXSd"?s%R\`)\#hJiZl3Qfq8u)[:"$1?[
\"`TlHW=N.bGK1,+lE*^foQ)(IVFO3!6"557',7QR:d;F9B3Lt#F$LChoFYS0gA3FCaJ(3#6
#Uu82Laj2:$U27:!:^l1$D3fU^m63X"b!RX*K2#JVXBhpJoYm?BL"F<&m:jp+*lCm.5dTRHE
_#W*fu<@o((oE7-qdh4lBr\&[_-u.7)Mma.Q:u_$2a^0QJB,Jt.i.fitJt_#VQ:Y*Q5`g8Zi
[#hMW)LhZM*L&UYBJACE#_aj\,l7(-Nc36b'TF?LjId@IL]A+KZ&&"1NBfQ;tq,AagJ_?d7u<
[!O]ALHP3VjC]ASkYuG/u8i>]Au<\RW@)nC(6NfPu?@*.0Y,]A#S4)R'_ip(uoo7F_MNV[]Ah!@<m
W[>#eW)`oX\OiIVK.Ids/+U[ctZ'NFkt_c>]AhXo=H82esHr(-2/P=mAe#5s1C[['h+d(V+/%
/"90I[!bNPF(bs-^Tb98dVuDOoH?NPE%Wt8&5X:s<kY$3^IJBgnFhrr2_QP.>,kitIceo@N'
3-P/Qk<OEA2ab2bS'Pdb9GS?Zu<)1i;;0=(^?9AC,Q=OmP2<\tKnlAe-_KH&Bg8f'*SQ.YUd
@?o@-;7kV#rIKG4))koA9&.oOD=S2EQ/.HC=;:>#fW_5HN2X`Ff'@JJXs7f"?o[1@]A-I>9&1
i;4!G4Beeq'>(e:":'%-Bkch@<H-MTO"s=HjdHu.&O]AL0Zkf)@B\A(jcO*dc,rA45ZG0UG]Ah
3EN,8A^XF_lBi/9g\h*QD-mg8T2BP@A6M2BBdYWl>QGOu;UBhWbU5a,W[bM^qTKi3!]A=kBU`
pFT(8`aHZXOnFA[$1XHOpfa!OaLJHV\=nGA1*6t23Qm>72U?@>GH=\mhp?^Z^6h-[XMqWbE&
F$qS4HAG0;+=BC,9nMX>ODl;FN`3\hM3!FcosM]A$[,6-!1`3O\iU>;2d*h%WXYHi]ABopZMo*
^m/,LNPpFPL5#o>]AqecK2'\5]A!\^joj<iI7#p/S2gpFst/LIOY=n4^B_6.qdfeXh#;J@S)#Y
`!-M[TT+JEI+cGCmIUQ`,$HYk+Jk$%!jpP`2:Xs:T3b)5UodL+MZVmdcf=.*iNMPW>QLrSrq
k^)$$d)A6W&9^R6$[Z45kf0pYJ\AfSN-::i2*XaMR7?rp6O-9$QV=j!N2Xr?14=_eAh\X&05
Y1c^j^us0io(BIDTP>g[(/bQ**(hf\7#s30$:5r_nB;7W4SgHpS3UY=VD<,P0tp"WkBkK=J`
)M+1+oKX^.2HZSl0f%5Xe'2/F,^,#`$"4cr%fD>jG>KqWFWh.a'-&;:"2]AemO[qNT$:5pcJa
U[ir`*ho/ksM2ABk4mn_c<Li`]AVpc^QDuQ2LOp/r8YCl5sc"cgWHsJ;)[0R&8I`jqg0Yu#MG
S@R7hb%/7RGa@L_Sar_$_L&\bAI^s,q$$nnnCJGhVr;h^i,JB5"1=Lf$#"qIXf&7r@jkdEkK
2V80l0IRp"m*U!BPBQr9&ZDJ0ciMBVnuIcNs)dQ0Vfjai*:bf_8cZ7s]A!"(l9o<i51,NH(7U
QM\0b=<(B$3)Dja?s8<7f+4fU!E0d\TEiKP;KfBrI,ODH&0!`'8$T\eRP0W:Q-Y[G3)A+)$M
nC$\h\d!Y9;ZK/o\2tl]ACtdS"W.`nJ-\VFfbN\B6')j*Uf(W&]AM43a\d94@!O.ekN`f@`b,X
=K=/>6UV=031tDH*&0(e9)31*.%_fp]A"U,\FF<Z1$JoEIGdj]A@ZYjPANhJ2#j>kUj[>:(+R>
jhc*;VR(@^XKPlJ<:mu7Eh&C[C@P[BnP73R&3r4+",kN.j2r__D.5,,GqupAe?Wa)n/C/CD+
a$luCW-2,]Aip_\)A^StPUZ31=-4QW!4:,u'AaM'QkDZH1TiQElHL`M2B(eHieQ.!8P`BUasX
gE\JpeUe-(%N?RC4KSdHJP<GXfI;3l\CpVL=;rU6&Q)V5npI)l<\Z1>Md$LD1mrq@mU9ci0q
R*hX)VJQ'+k'k>\d0Lls_-I9%FVmQ`r20NnlU&#*O6OThDRg\X'Fe_T9.(_D\6\R=&8d`P6s
CD'sb<'Uj58aaDd$#e.V<M'YaD!XJPOYq'\ifghQ!nY*AYeEshh<;;E*Z=7bjZG+nC.]A;oU8
dOHTSFV[X\Rh`aNVWgC.472?P%ut_[8RI(eIMICQ=iNs7NbYY4)WOrG3R=h=7`V7AhojU%Er
*bdG1/rCLj<qU:,Wb)JCcb1op]A7/D;l<`2./P5aKjtL0:./P*ZG32>Nq2K#mo'0*P.X(G6eh
5=XV92VY!`AU:mml`q)1HcgK<,ERa6gBT'5:j,QfXIZ4Km'<!UCBagTX<RVPcCTh*[0mV;0p
[gsODU6GCJ=m,W>K1>)PGZ*2Zr5(%3AqfRPXGJ8u%"`a?+Wh4S*SQd!qC?)/_S6=g>2:gIc>
.hfs%Fn$*#b,I.F"9n#u2O,O8sFDgAeL]A.]AJpNh%9V3uLP7kGLPP<3uW$\<D^eL["u;m$ZGn
]A-#5n@lpA9njVO,@+^%<)Od.=+bDe=G"HK$6m]A2(2g9m9*CuZ->fJMRRX,'b)Kdf/Tdcb4.F
56\C*'JD!u]A#[!Glr=fU86MCqbJ/OY-L20Oh&ENua'NCh40[uL-#b#.IC2!S+p+k8AaUP5O2
A$L!eY-e`r8PMWn4ckT)Qm;B,7nGjVkAt<g:*$%TV*_fl!dn@K/__\u*A_lJ$&7hao1#Z!I#
_.O_RZStDGGACNB%8=B&GRb:Jq^]A5;,WcOa-0B_#Ph"\<VS13H:eqg8HQ-3^S:m5Zun]AD:i<
tffhd/ghL24rsB%>jgbQTN#2<Eg?\'i-G`9XcRcE76&V4ZJ?ZX_el&M&T)?MUJkBFQk@2O2&
W7Y/%"QL4W3*j0WH4,-!bd-7rj5OOnO9"i'PEL\e3V[:)9a0U&ah1i\$ZX0#fUH/Ql4<f^JS
JEe79Vtd9>fu/%I;A;051P&rF)ikNDX7pM"0ADr1bdoYXK:mh0eBHc70F]A7e#LMG3OqcdeTr
@+K%U-.r\qRD@)d2*S2cVZ$U(U&I*%8VQJ!&nZ,g@O&]A#hs:*d9P3U-l%DIPK>s+gfQpaY0l
CNSWE5m#LluVjWK9kMe>C?[4\*#ia3"lEG[]Aceo3\d5)4)t2e2a@jkKnZfSCaV5WM@)%m!"<
W*@lZOc-SN_rR_9KD=N'Sq5LLUSUm+mAW';a1/G!um_*fm6MGeSQ8O08X94N)QejeNO>N`-S
kd;]A2<s6@B7qF[.nnO+6Co_/8?_GG]A]AaT4(<kuAZ4?;:`uAYq@:n;NR<dE,mW$PpnXS;mhjM
h)oK*.V[SYb*o>Se901,G"mJ8-j""is(K*i:r0Y<+Tc1;!4;<&_XZ^^lp&ief*T@Z4pO&Zb[
3s;K!ZFFFY.gA(8-U"`@&&*qTi]A>qK8MsTdHYsT4/;AEn@Su.2`TBHO#D^9aOA&^:_W-h.6;
T"p/ppcSK*T*i4*p#"C*4hiSN3#-pP-pFnF%/X-fj1Z3n`ennVfndaNfI\=7a"nr)05gotIb
2Vh-hmW!F6i4(`u:U@V]AC=hN\`p4n9cQQirAWL`p(!Vc^NZL_L5jb4_pan$c-J?=@binB&dV
;VN$/dC>i_<F`FUi8Q3LP]A?52Sta'j>JO'&&'M_$WG5X'un)e/n'Je#qjp+eC0IXcal+tZkO
<J=FEh=9=TP+LF(e`LpPVro$BF>LDMD/6s0U=Gepm<Ua"J"_H6ab39cFJ[6#\@+/,k9#2q)"
KMMRliA1q7W#_*AoYT3dK9o(&WUa!bi:9nDo+qGln?:HaLn,Np*_;FN.*TKaj+>;H*@&2_po
Wl<SCc%JRYWl7gZjNd<Q,slh\i+'FkZf5.M:&lEd%P(Ldq[A>X5s\mLZpaVJ/XgVkiGB8d%#
&WHi$Vb1q@<=u0,.RcQpoLZP59d$o5!D!D,Y#fAeg.ri,geKDZQV<$EqKuhHk#mmQ0\\$K?S
NCoI<7K[5\]AY)QNn6u'E/N@E=hO:q/:T'Mpsi'ch5RCDFAPu?hfm7@Q3Y[@h.hS[4W]AfL\+T
*IgfmGCX;"[;aKMk$-N1(T]AY--t?bOB]A<&8$9&]ANG3hJIhDU4QK\3^Il4D>L6S\-!u)>ZI>'
Z^mBU7Pm5i#U8QOnE=58lbkoAlW!.uA$V-9H=\0)7_C@T*P>RJV,K.^aMD!\-56JC8kB4_!#
7NYehK/k4V("Im0Cu)J&E]A(lSlT6AuPM?aV90GOG!&B.XA#"rHuL1niilUhKhO1[Ef7_Vbe#
$16STDn<Ko$lKNgh)%b2-G.*7jX8]AL!QLc^WQ^9f[QhQ*62B5TM%;'4]AN)I$@iU)p-An&Ui/
`6(MI*N'G9"\t2qb<DQKs=1:n3(f0"<sOU+489_aaRYD_+Rp2"3>:3b0`HQ%Qh=ndGEaDQnJ
7Via3&PrgZsqVC2p\NO?%S<jGToIO;\CZc2Q^27*dZi!rXLlt]AFh-":70q9<bZ<Ec>d)PHLO
*$E(=BE,eh"^S#F[e6T:]AeCjkJ<<=*l-U[K^$6IIM"6?)b[C_"k/oH;g&B;&4%"Vde9!.Bdu
,aS2Bb4O/[W\#mKM$lEt,Bpl(J#?&JQ8feSnC.5an$L$61C[id?h$.cWL.o"Q8MiOuYNM".>
re>(V;WT^IJ[CGg"b0%Kc]Aqr;-fp*(@iOoNN5ceFMc+:?iS.G2LTp`B3G?XlhAc#/u$Jnj<A
P.6]A(W?,kBS3?H#6RA_It?U*4FA:ia&fS*nbWiVYPNSC@AJG_P9u$*qPg.FB'3A-i*=HOiOP
_NR?cWORV\6QS"8MlGpm4:.7^kuS#(":p\O/.E8\&<]Ai]A^ULCa<bQ+qg/J^VqN/0#]Ai*it;C
!@2nXi\I)?Q0fkdd'e4Re&quckXB3)WV?W*Ar3kW<*FX2MG@I_1P'-%mWn<is8Vj6nQ.\SLd
P[u+Uj3H+)GOEYrK+WO#\>r7igQb41iU0iu=!b.Cf9LYA=b\OHE!m;$l1_0:traP.>^D+Z\+
W028A]A&Hr'CM+cdCShtBMi!95biG`fL[pG/?+BH=4,:+8/kYjg&4p\b1Gp6sQn*QnKra'!u_
ijrtX'JWYjPG"2_DO9+Gp-]AgqOmagQISg4<4qDUX<>c[CGs[g9C%c,8)makE)$(iXL.<N^rp
@3E2l?l;sks/+Vd/oB!ZaHl2m=]AU*dqOqr[%=Huom[gUD/C?LT0PpRc=KpF&$:&)pY`@N_Pu
6eI!p7^,*AjhE"+_Q,<mY"S!G9G6Q>F<Su"Q:uo-::]A^YGb4p.>,Od4]A'=/R>A6Y"7UO5T%<
$U<(pjCqnd!m]AEL2K\$_&/A@>]A_:rIMN5Yh>T>kQ!U@Sl[.XqTNJN_ru'S,ES<iSq&h+r&]A2
mPJ!&FV\e)c%DAR`\o?A.+02W_*V+q.FL4NiP:#dPMCs2CJ"b[H4Fs\HV]A>Ftd7FSgZ1@).$
bS1k+FpQ&UjJplM1DVu00m?2"``jB-f@),LXtA<rkEC2Vi,Gdr+16+W\':=bd`l6=dePiEX4
kTbEien2jU!/WYF@-aX;"RO=mC0]A6AU&ZFq7djXo:_CB%6::L^f:l6>KKa-TA(p4<5"N*V4<
HfP'crXA~
]]></IM>
<ElementCaseMobileAttrProvider horizontal="1" vertical="1" zoom="true" refresh="false" isUseHTML="false" isMobileCanvasSize="false" appearRefresh="false" allowFullScreen="false" allowDoubleClickOrZoom="true" functionalWhenUnactivated="false"/>
<MobileFormCollapsedStyle class="com.fr.form.ui.mobile.MobileFormCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
<lineAttr number="1"/>
</MobileFormCollapsedStyle>
</body>
</InnerWidget>
<BoundsAttr x="0" y="0" width="403" height="216"/>
</Widget>
<ShowBookmarks showBookmarks="true"/>
<Sorted sorted="false"/>
<MobileWidgetList>
<Widget widgetName="report0"/>
</MobileWidgetList>
<FrozenWidgets/>
<MobileBookMarkStyle class="com.fr.form.ui.mobile.impl.DefaultMobileBookMarkStyle"/>
<WidgetScalingAttr compState="1"/>
<DesignResolution absoluteResolutionScaleW="1440" absoluteResolutionScaleH="960"/>
<AppRelayout appRelayout="true"/>
</InnerWidget>
<BoundsAttr x="0" y="0" width="650" height="450"/>
</Widget>
<ShowBookmarks showBookmarks="true"/>
<Sorted sorted="false"/>
<MobileWidgetList/>
<FrozenWidgets/>
<MobileBookMarkStyle class="com.fr.form.ui.mobile.impl.DefaultMobileBookMarkStyle"/>
<WidgetZoomAttr compState="0"/>
<AppRelayout appRelayout="true"/>
<Size width="650" height="450"/>
<ResolutionScalingAttr percent="1.0"/>
<BodyLayoutType type="1"/>
</Center>
</Layout>
<DesignerVersion DesignerVersion="KAA"/>
<PreviewType PreviewType="5"/>
<TemplateIdAttMark class="com.fr.base.iofile.attr.TemplateIdAttrMark">
<TemplateIdAttMark TemplateId="717e7086-fb88-41de-bd23-239feafa3f03"/>
</TemplateIdAttMark>
</Form>
