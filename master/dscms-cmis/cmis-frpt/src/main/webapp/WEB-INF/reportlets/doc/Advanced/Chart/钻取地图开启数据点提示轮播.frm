<?xml version="1.0" encoding="UTF-8"?>
<Form xmlVersion="20170720" releaseVersion="10.0.0">
<TableDataMap>
<TableData name="销额" class="com.fr.data.impl.EmbeddedTableData">
<Parameters/>
<DSName>
<![CDATA[]]></DSName>
<ColumnNames>
<![CDATA[市,,.,,县,,.,,农商行,,.,,类型,,.,,笔数]]></ColumnNames>
<ColumnTypes>
<![CDATA[java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String]]></ColumnTypes>
<RowData ColumnTypes="java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String">
<![CDATA[HeLR[fo2N#4.8.$_HG3nG/%-Dp"@h(c[+2'Ek\'ln8co]A:*TiC/gMal2E]Ag/lY4Zt'ZEg$`P
s<`?gOE`@`sq4IJZkU42Vc%J/#2Vo5)EEYfSEX5?ffRh&.h!`MP&@TBt5"FFG=C@TSq+1BR^
Z~
]]></RowData>
</TableData>
<TableData name="销量" class="com.fr.data.impl.EmbeddedTableData">
<Parameters/>
<DSName>
<![CDATA[]]></DSName>
<ColumnNames>
<![CDATA[市,,.,,县,,.,,农商行,,.,,类型,,.,,笔数]]></ColumnNames>
<ColumnTypes>
<![CDATA[java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String]]></ColumnTypes>
<RowData ColumnTypes="java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String">
<![CDATA[HeLR[fo2N#4.8.$_HG3nG/%-Dp"@h(c[+2'Ek\'ln?Ta"e&[%I8]AA%G:Nu8Rg9V/UXV4C10s
]ALLqe*[rZK,`WIMB380XW5m[Hls;S$<ZDmY^hd(/.,?K*tAT.C.YT4#T3lhhSAc'rKR+!!!
~
]]></RowData>
</TableData>
</TableDataMap>
<FormMobileAttr>
<FormMobileAttr refresh="false" isUseHTML="false" isMobileOnly="false" isAdaptivePropertyAutoMatch="false" appearRefresh="false" promptWhenLeaveWithoutSubmit="false" allowDoubleClickOrZoom="true"/>
</FormMobileAttr>
<Parameters/>
<Layout class="com.fr.form.ui.container.WBorderLayout">
<WidgetName name="form"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<ShowBookmarks showBookmarks="false"/>
<Center class="com.fr.form.ui.container.WFitLayout">
<WidgetName name="body"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WAbsoluteBodyLayout">
<Listener event="afterinit">
<JavaScript class="com.fr.js.JavaScriptImpl">
<Parameters/>
<Content>
<![CDATA[setTimeout(function(){
var vanchart =FR.Chart.WebUtils.getChart("chart0").getChartWithIndex(0);
vanchart.openAutoTooltip();
},500) ]]></Content>
</JavaScript>
</Listener>
<WidgetName name="body"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WTitleLayout">
<Listener event="click">
<JavaScript class="com.fr.js.JavaScriptImpl">
<Parameters/>
<Content>
<![CDATA[FR.Chart.WebUtils.getChart("chart0").getChartWithIndex(0).openAutoTooltip();]]></Content>
</JavaScript>
</Listener>
<WidgetName name="chart0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="chart0" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.ChartEditor">
<WidgetName name="chart0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LayoutAttr selectedIndex="0"/>
<ChangeAttr enable="false" changeType="button" timeInterval="5" buttonColor="-8421505" carouselColor="-8421505" showArrow="true">
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
</ChangeAttr>
<Chart name="默认" chartClass="com.fr.plugin.chart.vanchart.VanChart">
<Chart class="com.fr.plugin.chart.vanchart.VanChart">
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-3355444" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<ChartAttr isJSDraw="true" isStyleGlobal="false"/>
<Title4VanChart>
<Title>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-1"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-6908266" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.75"/>
</AttrAlpha>
</GI>
<O>
<![CDATA[新建图表标题]]></O>
<TextAttr>
<Attr alignText="0" predefinedStyle="false">
<FRFont name="Microsoft YaHei" style="0" size="128" foreground="-13421773"/>
</Attr>
</TextAttr>
<TitleVisible value="false" position="0"/>
</Title>
<Attr4VanChart useHtml="false" floating="false" x="0.0" y="0.0" limitSize="false" maxHeight="15.0"/>
</Title4VanChart>
<Plot class="com.fr.plugin.chart.drillmap.VanChartDrillMapPlot">
<VanChartPlotVersion version="20170715"/>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<Attr isNullValueBreak="true" autoRefreshPerSecond="0" seriesDragEnable="false" plotStyle="0" combinedSize="50.0"/>
<newHotTooltipStyle>
<AttrContents>
<Attr showLine="false" position="1" isWhiteBackground="true" isShowMutiSeries="false" seriesLabel="${VALUE}"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##]]></Format>
<PercentFormat>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#0.##%]]></Format>
</PercentFormat>
</AttrContents>
</newHotTooltipStyle>
<ConditionCollection>
<DefaultAttr class="com.fr.chart.chartglyph.ConditionAttr">
<ConditionAttr name="">
<AttrList>
<Attr class="com.fr.chart.base.AttrAlpha">
<AttrAlpha>
<Attr alpha="0.75"/>
</AttrAlpha>
</Attr>
<Attr class="com.fr.plugin.chart.base.AttrEffect">
<AttrEffect>
<attr enabled="false" period="3.2"/>
</AttrEffect>
</Attr>
<Attr class="com.fr.plugin.chart.map.line.condition.AttrCurve">
<AttrCurve>
<attr lineWidth="0.5" bending="30.0" alpha="100.0"/>
</AttrCurve>
</Attr>
<Attr class="com.fr.plugin.chart.map.line.condition.AttrLineEffect">
<AttrEffect>
<attr enabled="true" period="30.0"/>
<lineEffectAttr animationType="default"/>
<marker>
<VanAttrMarker>
<Attr isCommon="true" markerType="NullMarker" radius="3.5" width="30.0" height="30.0"/>
<Background name="NullBackground"/>
</VanAttrMarker>
</marker>
</AttrEffect>
</Attr>
<Attr class="com.fr.plugin.chart.map.attr.AttrMapLabel">
<AttrMapLabel>
<areaLabel class="com.fr.plugin.chart.base.AttrLabel">
<AttrLabel>
<labelAttr enable="false"/>
<labelDetail class="com.fr.plugin.chart.base.AttrLabelDetail">
<AttrBorderWithShape>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216" autoColor="true" predefinedStyle="false"/>
<shapeAttr isAutoColor="true" shapeType="RectangularMarker"/>
</AttrBorderWithShape>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.8"/>
</AttrAlpha>
</GI>
<Attr showLine="false" isHorizontal="true" autoAdjust="false" position="5" align="9"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="center"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="false"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="false"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
</labelDetail>
</AttrLabel>
</areaLabel>
<pointLabel class="com.fr.plugin.chart.base.AttrLabel">
<AttrLabel>
<labelAttr enable="false"/>
<labelDetail class="com.fr.plugin.chart.base.AttrLabelDetail">
<AttrBorderWithShape>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216" autoColor="true" predefinedStyle="false"/>
<shapeAttr isAutoColor="true" shapeType="RectangularMarker"/>
</AttrBorderWithShape>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.8"/>
</AttrAlpha>
</GI>
<Attr showLine="false" isHorizontal="true" autoAdjust="false" position="5" align="9"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="center"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="false"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="false"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
</labelDetail>
</AttrLabel>
</pointLabel>
</AttrMapLabel>
</Attr>
<Attr class="com.fr.plugin.chart.map.attr.AttrMapTooltip">
<AttrMapTooltip>
<areaTooltip class="com.fr.plugin.chart.base.AttrTooltip">
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="true"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-16777216"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="true" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.5"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</areaTooltip>
<pointTooltip class="com.fr.plugin.chart.base.AttrTooltip">
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="true"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-16777216"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="true" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.5"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</pointTooltip>
<lineTooltip class="com.fr.plugin.chart.base.AttrTooltip">
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="true"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipStartAndEndNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipStartAndEndNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-16777216"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="true" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.5"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</lineTooltip>
</AttrMapTooltip>
</Attr>
<Attr class="com.fr.plugin.chart.base.AttrBorderWithAlpha">
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-1" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AlphaAttr alpha="1.0"/>
</Attr>
</AttrList>
</ConditionAttr>
</DefaultAttr>
</ConditionCollection>
<Legend4VanChart>
<Legend>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-1"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-3355444" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.9"/>
</AttrAlpha>
</GI>
<Attr position="4" visible="true" predefinedStyle="false"/>
<FRFont name="Microsoft YaHei" style="0" size="88" foreground="-10066330"/>
</Legend>
<Attr4VanChart floating="false" x="0.0" y="0.0" layout="aligned" customSize="true" maxHeight="100.0" isHighlight="true"/>
<Attr4VanChartScatter>
<Type rangeLegendType="1"/>
<GradualLegend>
<GradualIntervalConfig>
<IntervalConfigAttr subColor="-14374913" divStage="2.0"/>
<ValueRange IsCustomMin="false" IsCustomMax="false"/>
<ColorDistList>
<ColorDist color="-4791553" dist="0.0"/>
<ColorDist color="-9583361" dist="0.5"/>
<ColorDist color="-14374913" dist="1.0"/>
</ColorDistList>
</GradualIntervalConfig>
<LegendLabelFormat>
<IsCommon commonValueFormat="true"/>
</LegendLabelFormat>
</GradualLegend>
</Attr4VanChartScatter>
</Legend4VanChart>
<DataSheet>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<Attr isVisible="false" predefinedStyle="false"/>
<FRFont name="SimSun" style="0" size="72"/>
</DataSheet>
<NameJavaScriptGroup>
<NameJavaScript name="JavaScript1">
<JavaScript class="com.fr.js.JavaScriptImpl">
<Parameters/>
<Content>
<![CDATA[FR.Chart.WebUtils.getChart("chart0").getChartWithIndex(0).openAutoTooltip(); ]]></Content>
</JavaScript>
</NameJavaScript>
</NameJavaScriptGroup>
<DataProcessor class="com.fr.base.chart.chartdata.model.NormalDataModel"/>
<newPlotFillStyle>
<AttrFillStyle>
<AFStyle colorStyle="0"/>
<FillStyleName fillStyleName=""/>
<isCustomFillStyle isCustomFillStyle="false"/>
<PredefinedStyle predefinedStyle="false"/>
</AttrFillStyle>
</newPlotFillStyle>
<VanChartPlotAttr isAxisRotation="false" categoryNum="1"/>
<GradientStyle>
<Attr gradientType="normal" startColor="-12146441" endColor="-9378161"/>
</GradientStyle>
<VanChartMapPlotAttr mapType="area" geourl="assets/map/geographic/world/中国/江西省.json" zoomlevel="0" mapmarkertype="0" autoNullValue="false" nullvaluecolor="-3355444"/>
<lineMapDataProcessor>
<DataProcessor class="com.fr.base.chart.chartdata.model.NormalDataModel"/>
</lineMapDataProcessor>
<GisLayer>
<Attr gislayer="predefined_layer" layerName="深蓝"/>
</GisLayer>
<ViewCenter auto="true" longitude="0.0" latitude="0.0"/>
<pointConditionCollection>
<ConditionCollection>
<DefaultAttr class="com.fr.chart.chartglyph.ConditionAttr">
<ConditionAttr name=""/>
</DefaultAttr>
</ConditionCollection>
</pointConditionCollection>
<lineConditionCollection>
<ConditionCollection>
<DefaultAttr class="com.fr.chart.chartglyph.ConditionAttr">
<ConditionAttr name=""/>
</DefaultAttr>
</ConditionCollection>
</lineConditionCollection>
<matchResult/>
<layerMapTypeList>
<single type="area"/>
<single type="area"/>
</layerMapTypeList>
<layerLevelList>
<single level="0"/>
<single level="0"/>
</layerLevelList>
<drillUpHyperLink/>
<DrillMapTools>
<drillAttr enable="true"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false">
<FRFont name="微软雅黑" style="0" size="96" foreground="-5066062"/>
</Attr>
</TextAttr>
<backgroundinfo>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
</backgroundinfo>
<selectbackgroundinfo>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
</selectbackgroundinfo>
</DrillMapTools>
<matchResultList>
<matchResult/>
<matchResult/>
<matchResult/>
</matchResultList>
</Plot>
<ChartDefinition>
<DillMapDefinition>
<Top topCate="-1" topValue="-1" isDiscardOtherCate="false" isDiscardOtherSeries="false" isDiscardNullCate="false" isDiscardNullSeries="false"/>
<Attr fromBottomData="false"/>
<eachLayerDataDefinitionList>
<SingleLayerDataDefinition class="com.fr.plugin.chart.map.data.VanMapReportDefinition">
<VanMapReportDefinition>
<Category>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=REPORT0~A2]]></Attributes>
</O>
</Category>
<Top topCate="-1" topValue="-1" isDiscardOtherCate="false" isDiscardOtherSeries="false" isDiscardNullCate="false" isDiscardNullSeries="false"/>
<Attr useAreaName="true"/>
<longitude/>
<latitude/>
<endLongitude/>
<endLatitude/>
<endArea/>
<DefinitionList>
<SeriesDefinition>
<SeriesName>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[='销额']]></Attributes>
</O>
</SeriesName>
<SeriesValue>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=REPORT0~I2]]></Attributes>
</O>
</SeriesValue>
</SeriesDefinition>
<SeriesDefinition>
<SeriesName>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[='销量']]></Attributes>
</O>
</SeriesName>
<SeriesValue>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=REPORT0~J2]]></Attributes>
</O>
</SeriesValue>
</SeriesDefinition>
<SeriesDefinition>
<SeriesName>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[='合计']]></Attributes>
</O>
</SeriesName>
<SeriesValue>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=REPORT0~K2]]></Attributes>
</O>
</SeriesValue>
</SeriesDefinition>
</DefinitionList>
</VanMapReportDefinition>
</SingleLayerDataDefinition>
<SingleLayerDataDefinition class="com.fr.plugin.chart.map.data.VanMapReportDefinition">
<VanMapReportDefinition>
<Category>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=REPORT0~B2]]></Attributes>
</O>
</Category>
<Top topCate="-1" topValue="-1" isDiscardOtherCate="false" isDiscardOtherSeries="false" isDiscardNullCate="false" isDiscardNullSeries="false"/>
<Attr useAreaName="true"/>
<longitude/>
<latitude/>
<endLongitude/>
<endLatitude/>
<endArea/>
<DefinitionList>
<SeriesDefinition>
<SeriesName>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[='销量']]></Attributes>
</O>
</SeriesName>
<SeriesValue>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=REPORT0~F2]]></Attributes>
</O>
</SeriesValue>
</SeriesDefinition>
<SeriesDefinition>
<SeriesName>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[='销额']]></Attributes>
</O>
</SeriesName>
<SeriesValue>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=REPORT0~G2]]></Attributes>
</O>
</SeriesValue>
</SeriesDefinition>
<SeriesDefinition>
<SeriesName>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[='合计']]></Attributes>
</O>
</SeriesName>
<SeriesValue>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=REPORT0~H2]]></Attributes>
</O>
</SeriesValue>
</SeriesDefinition>
</DefinitionList>
</VanMapReportDefinition>
</SingleLayerDataDefinition>
</eachLayerDataDefinitionList>
</DillMapDefinition>
</ChartDefinition>
</Chart>
<UUID uuid="e255a73f-a4d5-424d-b1e5-3b3d3b3edcd4"/>
<tools hidden="true" sort="false" export="false" fullScreen="false"/>
<VanChartZoom>
<zoomAttr zoomVisible="false" zoomGesture="true" zoomResize="true" zoomType="xy"/>
<from>
<![CDATA[]]></from>
<to>
<![CDATA[]]></to>
</VanChartZoom>
<refreshMoreLabel>
<attr moreLabel="false" autoTooltip="true"/>
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="true"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat">
<AttrToolTipCategoryFormat>
<Attr enable="false"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="true"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-1"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="4"/>
<newColor borderColor="-15395563" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.8"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</refreshMoreLabel>
<ThemeAttr>
<Attr darkTheme="false" predefinedStyle="false"/>
</ThemeAttr>
</Chart>
<ChartMobileAttrProvider zoomOut="0" zoomIn="2" allowFullScreen="true" functionalWhenUnactivated="false"/>
<MobileChartCollapsedStyle class="com.fr.form.ui.mobile.MobileChartCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
</MobileChartCollapsedStyle>
</InnerWidget>
<BoundsAttr x="0" y="0" width="250" height="150"/>
</Widget>
<ShowBookmarks showBookmarks="false"/>
<body class="com.fr.form.ui.ChartEditor">
<WidgetName name="chart0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LayoutAttr selectedIndex="0"/>
<ChangeAttr enable="false" changeType="button" timeInterval="5" buttonColor="-8421505" carouselColor="-8421505" showArrow="true">
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
</ChangeAttr>
<Chart name="默认" chartClass="com.fr.plugin.chart.vanchart.VanChart">
<Chart class="com.fr.plugin.chart.vanchart.VanChart">
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-3355444" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<ChartAttr isJSDraw="true" isStyleGlobal="false"/>
<Title4VanChart>
<Title>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-1"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-6908266" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.75"/>
</AttrAlpha>
</GI>
<O>
<![CDATA[新建图表标题]]></O>
<TextAttr>
<Attr alignText="0" predefinedStyle="false">
<FRFont name="Microsoft YaHei" style="0" size="128" foreground="-13421773"/>
</Attr>
</TextAttr>
<TitleVisible value="false" position="0"/>
</Title>
<Attr4VanChart useHtml="false" floating="false" x="0.0" y="0.0" limitSize="false" maxHeight="15.0"/>
</Title4VanChart>
<Plot class="com.fr.plugin.chart.drillmap.VanChartDrillMapPlot">
<VanChartPlotVersion version="20170715"/>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<Attr isNullValueBreak="true" autoRefreshPerSecond="0" seriesDragEnable="false" plotStyle="0" combinedSize="50.0"/>
<newHotTooltipStyle>
<AttrContents>
<Attr showLine="false" position="1" isWhiteBackground="true" isShowMutiSeries="false" seriesLabel="${VALUE}"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##]]></Format>
<PercentFormat>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#0.##%]]></Format>
</PercentFormat>
</AttrContents>
</newHotTooltipStyle>
<ConditionCollection>
<DefaultAttr class="com.fr.chart.chartglyph.ConditionAttr">
<ConditionAttr name="">
<AttrList>
<Attr class="com.fr.plugin.chart.base.AttrBorderWithAlpha">
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-1" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AlphaAttr alpha="1.0"/>
</Attr>
<Attr class="com.fr.chart.base.AttrAlpha">
<AttrAlpha>
<Attr alpha="0.75"/>
</AttrAlpha>
</Attr>
<Attr class="com.fr.plugin.chart.base.AttrEffect">
<AttrEffect>
<attr enabled="false" period="3.2"/>
</AttrEffect>
</Attr>
<Attr class="com.fr.plugin.chart.map.line.condition.AttrCurve">
<AttrCurve>
<attr lineWidth="0.5" bending="30.0" alpha="100.0"/>
</AttrCurve>
</Attr>
<Attr class="com.fr.plugin.chart.map.line.condition.AttrLineEffect">
<AttrEffect>
<attr enabled="true" period="30.0"/>
<lineEffectAttr animationType="default"/>
<marker>
<VanAttrMarker>
<Attr isCommon="true" markerType="NullMarker" radius="3.5" width="30.0" height="30.0"/>
<Background name="NullBackground"/>
</VanAttrMarker>
</marker>
</AttrEffect>
</Attr>
<Attr class="com.fr.plugin.chart.map.attr.AttrMapLabel">
<AttrMapLabel>
<areaLabel class="com.fr.plugin.chart.base.AttrLabel">
<AttrLabel>
<labelAttr enable="false"/>
<labelDetail class="com.fr.plugin.chart.base.AttrLabelDetail">
<AttrBorderWithShape>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216" autoColor="true" predefinedStyle="false"/>
<shapeAttr isAutoColor="true" shapeType="RectangularMarker"/>
</AttrBorderWithShape>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.8"/>
</AttrAlpha>
</GI>
<Attr showLine="false" isHorizontal="true" autoAdjust="false" position="5" align="9"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="center"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="false"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="false"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
</labelDetail>
</AttrLabel>
</areaLabel>
<pointLabel class="com.fr.plugin.chart.base.AttrLabel">
<AttrLabel>
<labelAttr enable="false"/>
<labelDetail class="com.fr.plugin.chart.base.AttrLabelDetail">
<AttrBorderWithShape>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216" autoColor="true" predefinedStyle="false"/>
<shapeAttr isAutoColor="true" shapeType="RectangularMarker"/>
</AttrBorderWithShape>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.8"/>
</AttrAlpha>
</GI>
<Attr showLine="false" isHorizontal="true" autoAdjust="false" position="5" align="9"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="center"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="false"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="false"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
</labelDetail>
</AttrLabel>
</pointLabel>
</AttrMapLabel>
</Attr>
<Attr class="com.fr.plugin.chart.map.attr.AttrMapTooltip">
<AttrMapTooltip>
<areaTooltip class="com.fr.plugin.chart.base.AttrTooltip">
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="true"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-16777216"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="true" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.5"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</areaTooltip>
<pointTooltip class="com.fr.plugin.chart.base.AttrTooltip">
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="true"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-16777216"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="true" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.5"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</pointTooltip>
<lineTooltip class="com.fr.plugin.chart.base.AttrTooltip">
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="true"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipStartAndEndNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipStartAndEndNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-16777216"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="true" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.5"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</lineTooltip>
</AttrMapTooltip>
</Attr>
</AttrList>
</ConditionAttr>
</DefaultAttr>
</ConditionCollection>
<Legend4VanChart>
<Legend>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-1"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-3355444" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.9"/>
</AttrAlpha>
</GI>
<Attr position="4" visible="true" predefinedStyle="false"/>
<FRFont name="Microsoft YaHei" style="0" size="88" foreground="-10066330"/>
</Legend>
<Attr4VanChart floating="false" x="0.0" y="0.0" layout="aligned" customSize="true" maxHeight="100.0" isHighlight="true"/>
<Attr4VanChartScatter>
<Type rangeLegendType="1"/>
<GradualLegend>
<GradualIntervalConfig>
<IntervalConfigAttr subColor="-14374913" divStage="2.0"/>
<ValueRange IsCustomMin="false" IsCustomMax="false"/>
<ColorDistList>
<ColorDist color="-4791553" dist="0.0"/>
<ColorDist color="-9583361" dist="0.5"/>
<ColorDist color="-14374913" dist="1.0"/>
</ColorDistList>
</GradualIntervalConfig>
<LegendLabelFormat>
<IsCommon commonValueFormat="true"/>
</LegendLabelFormat>
</GradualLegend>
</Attr4VanChartScatter>
</Legend4VanChart>
<DataSheet>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<Attr isVisible="false" predefinedStyle="false"/>
<FRFont name="SimSun" style="0" size="72"/>
</DataSheet>
<DataProcessor class="com.fr.base.chart.chartdata.model.NormalDataModel"/>
<newPlotFillStyle>
<AttrFillStyle>
<AFStyle colorStyle="0"/>
<FillStyleName fillStyleName=""/>
<isCustomFillStyle isCustomFillStyle="false"/>
<PredefinedStyle predefinedStyle="false"/>
</AttrFillStyle>
</newPlotFillStyle>
<VanChartPlotAttr isAxisRotation="false" categoryNum="1"/>
<GradientStyle>
<Attr gradientType="normal" startColor="-12146441" endColor="-9378161"/>
</GradientStyle>
<VanChartMapPlotAttr mapType="area" geourl="assets/map/geographic/world/中国.json" zoomlevel="0" mapmarkertype="0" autoNullValue="false" nullvaluecolor="-3355444"/>
<lineMapDataProcessor>
<DataProcessor class="com.fr.base.chart.chartdata.model.NormalDataModel"/>
</lineMapDataProcessor>
<GisLayer>
<Attr gislayer="predefined_layer" layerName="高德地图"/>
</GisLayer>
<ViewCenter auto="true" longitude="0.0" latitude="0.0"/>
<pointConditionCollection>
<ConditionCollection>
<DefaultAttr class="com.fr.chart.chartglyph.ConditionAttr">
<ConditionAttr name=""/>
</DefaultAttr>
</ConditionCollection>
</pointConditionCollection>
<lineConditionCollection>
<ConditionCollection>
<DefaultAttr class="com.fr.chart.chartglyph.ConditionAttr">
<ConditionAttr name=""/>
</DefaultAttr>
</ConditionCollection>
</lineConditionCollection>
<matchResult/>
<layerMapTypeList/>
<layerLevelList/>
<DrillMapTools>
<drillAttr enable="true"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false">
<FRFont name="Microsoft YaHei" style="0" size="96" foreground="-5066062"/>
</Attr>
</TextAttr>
<backgroundinfo>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
</backgroundinfo>
<selectbackgroundinfo>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
</selectbackgroundinfo>
</DrillMapTools>
<matchResultList/>
</Plot>
</Chart>
<UUID uuid="82c2617c-2a40-4265-9f86-498f59279b9d"/>
<tools hidden="true" sort="false" export="false" fullScreen="false"/>
<VanChartZoom>
<zoomAttr zoomVisible="false" zoomGesture="true" zoomResize="true" zoomType="xy"/>
<from>
<![CDATA[]]></from>
<to>
<![CDATA[]]></to>
</VanChartZoom>
<refreshMoreLabel>
<attr moreLabel="false" autoTooltip="true"/>
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="true"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0" predefinedStyle="false"/>
</TextAttr>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
</AttrTooltipRichText>
</richText>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat">
<AttrToolTipCategoryFormat>
<Attr enable="false"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<value class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="true"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-1"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false" autoBackground="false" predefinedStyle="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="4"/>
<newColor borderColor="-15395563" autoColor="false" predefinedStyle="false"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.8"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</refreshMoreLabel>
<ThemeAttr>
<Attr darkTheme="false" predefinedStyle="false"/>
</ThemeAttr>
</Chart>
<ChartMobileAttrProvider zoomOut="0" zoomIn="2" allowFullScreen="true" functionalWhenUnactivated="false"/>
<MobileChartCollapsedStyle class="com.fr.form.ui.mobile.MobileChartCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
</MobileChartCollapsedStyle>
</body>
</InnerWidget>
<BoundsAttr x="0" y="125" width="569" height="388"/>
</Widget>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WTitleLayout">
<WidgetName name="report0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="report0" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.ElementCaseEditor">
<WidgetName name="report0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<FileAttrErrorMarker class="com.fr.base.io.FileAttrErrorMarker" pluginID="com.fr.plugin.reportRefresh.v10" oriClass="com.fr.plugin.reportRefresh.ReportExtraRefreshAttr">
<Refresh customClass="false" interval="0.0" state="0" refreshArea=""/>
</FileAttrErrorMarker>
<FormElementCase>
<ReportPageAttr>
<HR/>
<FR/>
<HC/>
<FC/>
</ReportPageAttr>
<ColumnPrivilegeControl/>
<RowPrivilegeControl/>
<RowHeight defaultValue="723900">
<![CDATA[1257300,723900,723900,723900,723900,723900,723900,723900,723900,723900,723900]]></RowHeight>
<ColumnWidth defaultValue="2743200">
<![CDATA[2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200]]></ColumnWidth>
<CellElementList>
<C c="0" r="0" s="0">
<O>
<![CDATA[市]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="1" r="0" s="0">
<O>
<![CDATA[县]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="2" r="0" s="0">
<O>
<![CDATA[银行]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="3" r="0" s="0">
<O>
<![CDATA[类型]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="4" r="0" s="0">
<O>
<![CDATA[笔数]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="5" r="0" s="0">
<O>
<![CDATA[县销量合计]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="6" r="0" s="0">
<O>
<![CDATA[县销额合计]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="7" r="0" s="0">
<O>
<![CDATA[县合计]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="8" r="0" s="0">
<O>
<![CDATA[市销额合计]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="9" r="0" s="0">
<O>
<![CDATA[市销量合计]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="10" r="0" s="0">
<O>
<![CDATA[市合计]]></O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="0" r="1" rs="2" s="0">
<O t="DSColumn">
<Attributes dsName="销量" columnName="市"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand dir="0"/>
</C>
<C c="1" r="1" rs="2" s="0">
<O t="DSColumn">
<Attributes dsName="销量" columnName="县"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand dir="0"/>
</C>
<C c="2" r="1" rs="2" s="0">
<O t="DSColumn">
<Attributes dsName="销量" columnName="农商行"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand dir="0"/>
</C>
<C c="3" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="销量" columnName="类型"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand dir="0"/>
</C>
<C c="4" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="销量" columnName="笔数"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.SummaryGrouper">
<FN>
<![CDATA[com.fr.data.util.function.SumFunction]]></FN>
</RG>
<Parameters/>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
<C c="5" r="1" rs="2" s="0">
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=SUM(E2)]]></Attributes>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand leftParentDefault="false" left="B2"/>
</C>
<C c="6" r="1" rs="2" s="0">
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=SUM(E3)]]></Attributes>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand leftParentDefault="false" left="B2"/>
</C>
<C c="7" r="1" rs="2" s="0">
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=F2+G2]]></Attributes>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand leftParentDefault="false" left="B2"/>
</C>
<C c="8" r="1" rs="2" s="0">
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=SUM(E3)]]></Attributes>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand leftParentDefault="false" left="A2"/>
</C>
<C c="9" r="1" rs="2" s="0">
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=SUM(E2)]]></Attributes>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand leftParentDefault="false" left="A2"/>
</C>
<C c="10" r="1" rs="2" s="0">
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=I2 + J2]]></Attributes>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand leftParentDefault="false" left="A2"/>
</C>
<C c="3" r="2" s="0">
<O t="DSColumn">
<Attributes dsName="销额" columnName="类型"/>
<Condition class="com.fr.data.condition.ListCondition">
<JoinCondition join="0">
<Condition class="com.fr.data.condition.CommonCondition">
<CNUMBER>
<![CDATA[0]]></CNUMBER>
<CNAME>
<![CDATA[市]]></CNAME>
<Compare op="0">
<SimpleDSColumn dsName="销量" columnName="市"/>
</Compare>
</Condition>
</JoinCondition>
<JoinCondition join="0">
<Condition class="com.fr.data.condition.CommonCondition">
<CNUMBER>
<![CDATA[0]]></CNUMBER>
<CNAME>
<![CDATA[县]]></CNAME>
<Compare op="0">
<SimpleDSColumn dsName="销量" columnName="县"/>
</Compare>
</Condition>
</JoinCondition>
<JoinCondition join="0">
<Condition class="com.fr.data.condition.CommonCondition">
<CNUMBER>
<![CDATA[0]]></CNUMBER>
<CNAME>
<![CDATA[农商行]]></CNAME>
<Compare op="0">
<SimpleDSColumn dsName="销量" columnName="农商行"/>
</Compare>
</Condition>
</JoinCondition>
</Condition>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Result>
<![CDATA[$$$]]></Result>
<Parameters/>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand dir="0"/>
</C>
<C c="4" r="2" s="0">
<O t="DSColumn">
<Attributes dsName="销额" columnName="笔数"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.SummaryGrouper">
<FN>
<![CDATA[com.fr.data.util.function.SumFunction]]></FN>
</RG>
<Parameters/>
</O>
<PrivilegeControl/>
<CellGUIAttr adjustmode="2"/>
<CellPageAttr/>
<Expand/>
</C>
</CellElementList>
<ReportAttrSet>
<ReportSettings headerHeight="0" footerHeight="0">
<PaperSetting/>
<Background name="ColorBackground" color="-1"/>
</ReportSettings>
</ReportAttrSet>
</FormElementCase>
<StyleList>
<Style horizontal_alignment="0" imageLayout="1">
<FRFont name="微软雅黑" style="0" size="72"/>
<Background name="NullBackground"/>
<Border>
<Top style="1"/>
<Bottom style="1"/>
<Left style="1"/>
<Right style="1"/>
</Border>
</Style>
</StyleList>
<heightRestrict heightrestrict="false"/>
<heightPercent heightpercent="0.75"/>
<IM>
<![CDATA[m<]A4?;eN\M*F!K_&H?aITKqL1>GW>p[4:nr1:GONX6A3e)^Ik1e:u$j5R?%7WbG0Z9%q-*.a
#cZb-Y?H,(s`077\ANL^4SZ#U+qQ+UP^:P7t%C[);#Ws#IDjk=ipMMj8\Irq<hYbO[pdo50M
*h3&KAoWkCXc'oO=qJ3;4H/?2@$%;h`(AR^>C&Bg;3cA?a+'sFCiQ]A6g)C[-p,kY+O+nOO!f
5*J:<86SKG:X.e0+@""-(/5i9*t8Gj,JCMF=lUE3L[;60Y$)*kN8nn^+l`/I8(]AI1Icfccd-
1;n3m1i.t5sAj'oCZ+udPXk't"O(^>"$/s4aS+#o\Vga/^Aif;s%J8JIr-c+X2)X/:`o@mQo
1W.1+[L:Ajn3db3O>alXHeIsg5mT;T$SEZWmqDUU0t&Q.k]ALjW7Jm?#\NGGePm$RmM&Xc9V$
;f)\qgKr3&<4<@JIE\k(E-3W1JO=7a;7Nc6VDlnV3G(9e376dkU0W<`J!/1$`0VQYaXt8La%
2Dej4V9fXYk82KAo`&bQEl=CE4+(9Pq_0>340E`<4k=)=F!Q#.DXD)LLEcZ!QOg1)J(#ANp$
ptB>:f#^HeB1`XDq&s2DGQd;XmJpoV:>4i*:o[c$+1k5WUZTLB<?<h>irZ9mAa2R8?R$X\@Z
54Z)I0P[5fDTc[\U;pHK#"YKGWkQKN-\'TWkc_`+9iRG/P_$XmMCiH_e4=-r*ue46Dl7%a5Q
pqYq#a3j,u6"lC.WQGkCTnls)hrCVNnl+QBgtrb4feneV:S4*4("HQ\^NjFkAfIosi/#J@9Q
AAHT@j`/(n!mULaK^=#qNqtio[_$n`coA2W"'m2.l=.!lkW)D=6P(7b8nd,J1[]A0iFTa.@".
2fSYYEhji&(R*loeffo#i0;HP:Fpf#0Jnk5FVA8%/RLYYX"XZ,r4k?e4f(UH'G\dIq^43tD#
87<a,T0ljcRoi"QCZ%t"4T_@[CA(-UV=L0mUDgEq-6cW9&l6Z,('cT<mP(QJc>,XX:.Hom8l
T?GF#/qo)0!hF(!pK\nCON<jJAfdANVDjT2XM0_"!V#_ld[_2"0YA,Gq7_I4bjgF(_0rKLH\
W"pWbQ^*4S;0=pt2W)u:AC5'2NT`![7Eo^9Z9gU_($W[PgQ/d77oZDJ!XX"j6Hh1b&pjYa0J
9d_oo.:40P_'/9b`/SKnH-`$VEB(*7dOZS+q/RI?YHI7,H%[>?<,G_7jO/7ogI\l/iq7+N60
9_s$,<=4K@C7:r)'a2]A<$c<!Q5c]A%2;Um7*oV4<TgFrOH>8/[T+=_NQm4-Ya8))&rC3UI2>J
e^Yf;[$8N@B\UM>KNS)El[FOF,j)$4\c/M?0<c'I.-SU1gFKI'AL58%F_\qF`clO;(s/p!6:
kB[%Nj"Z$0pbZ\\P$W`Ie/-+\<aKVXRL9[oe3Qlcg8]Aajmu-bh<;hWX[S/U*[c\\3=6\H^&E
@V(2-*LT-@a>6LD;>lb24ZW;`4TV1R\Y+8[l&$;[VfnN`XqXglp?e+,,U\.&qS#/&kZ*ARo,
lfS_YM">-gTY/CFVo[Maj]AI-a>.uk]A0T?b<V903Z!(-K[dV$Fka.':G<YjBg]Ajc@k7ga28t\
4HJS"Y=6FcHk25_$:ui(>j%nHG/!?UO#q-Z,/kb.&(T*PMicXKa20_d(f+EdcK?^_rbO1BkD
mY2X`eiP*-e384AZ"NXpGKq:(R&dj;MO[)]ABUBgh=_O>kh7<p,rn=k`W)n\(opoMTT!GnCa!
]ANUA%h[+>:ic\&:Hbr;e.i&rscGKi,]AU)4;?8E10u;H7MSW;,M^4'p+?*e&1Oj&;S*S9H+D(
jWH)meWMEt%0&!8;&3mS^i'K<*25f&`:iHJ&[0Q-UL8BR-"J=j%>'(_!'eOMIfR6RAD[d#2_
$pNE'tH+IV.pfYZc*B:n>hjYm\9@mTJ3COt9>a5>]AM(KVD6S2Q*erm<dc<dtnP1@&5!!E,Zn
nPsEl'N.iW?Y3)KNmQf@lS5O,^fFi-m)?MngU.uuO8^L&)iX5FYX6]A259*I@j6UJ6"h<oR*f
%!ju,m`h$@H"UqFS\Au02RLm#`hG+VV<c5-@$qo?B)$!]ASJo/;sg/N"4^@&Qmp!3J.c;"4tW
1V,MaTBU=f]A?<@XMTdiN#J]A4h'S0$(=,!aap077-.kaNDh0Ki/m<O'=4_fltq"%.+W3'FUPf
Jh`JbosR2k*Oco?P>qj=&L.N;fHQoHa7$#0L("L"Onb%=(!H.]A_=f33TBL3tQ4QBS6oNJg6`
6\9]AeT509s9t>#*f(!aOf"u$W_FJ"5u37VCLUbk[E!nZqWc)>\q+(Yip%&pIb9&Mc#P$B^(G
.oVnkAK$59%Hopd(b8M&fIN\%_(Uu26%%'d"/n9*Bgp4n+;2mjUi]A?"GV"Z2<al__A97KCf7
3,M*WlUSpo1ir#UqEV5mdG[u@1HD*="\8%m927(g#.]AQ-aAZjibs&dnHc$?pL_l1i$O=NdAt
.,2GljKds8/(n=gl3+rYFs+:8\s%d*:;ZDU/+?_P(f%^6@>:.TlDip5HR3mlIL9@$V\N-\*H
OH'+p9Zm]A4NFWAWLO#3HMnE'$DVP"862E"crhi\rbW=X-9UMomeMVMN`BgS05l3fgX2$EkB0
*duWX+3e`"U&k'aPT/l2qbni8J4XLXCXFP[Rm+(s9+Ki%utPI/D"g]A8K&NQ6hX=3`/Oh4n'Z
5o^hFKh`2V&m2>W[:58'QGr'>1hY9fsrNML=kPreGF^7A!WfY]AYQVQt@DqD$Z8;gd4TYai7I
:t`U6)!/XkuHPC9oaE+X!n8bkQRPtM+HRGjEl=I.>T"s<UG:Dm\[bWm?5g"8^>o,m6s$>4^d
,$"/epK*07%AjXo]AGRZ;Xk",/9tB/,EES+S=\(p87dPauqI\$a64CXn?hMY403?#qc@YW$Q&
CC?/^pkp:=c[9A?L[0@m#PLe@3mh7C?p0W%H2^71g8(7oBi0C@"CKAJ5&cI+H/V&r]AtFGWR<
Q1O(&O)932@f<24V+:s#.%D85/_PcRP%0G1^0PQ?og`C[8[`T[LQ,es5>>FMs*X:PTAaeiIf
B!\=#+YnlVAc1sV"o^G,=k?=5Pg#u`qV'!tP"`:JtAXs*lTfmRCJDa)eAV'\IaDY9OoBcU+4
TCH3?.N]A`[=VYI>),'3E&A\3ebX6-'D63B.0aaSO^PGSm^\Kg*ub#\Tpr#u'chn+pun=a3o=
'2/msg'U%lA^0uN:b#c(&"h7Bcq]A7/fbi[\[dbCK-H=bfD[Y$HC(.VYWGrSU<4.^Yi=l@Ut%
+")1n\3ler&Gg?o*mK8"l?=9AX\Qu0!h<k>(RmeOP%kQ">cUacRA@_3Xl`@Gh:ckNA72*.1G
7"@6FIEqXW$T:a%:(#di.%,AsLg?7N&qqs-mj9GE4)T%%aNFSB2VToP;iqh"/YQgF=6"c-Ba
VQI>nnR2\Z,(%&cf^AF64n'FR1=84VM8.%?Jm35eJ?ej:'-IM/0lmIpSV?"$Q.`+)ZEKqMJ]A
/dGWKdG='1AIuC."BIJ8*>&CkK-0>29B-oKh)&Pn,.8@pJ#lZX=QrT'\Jr"OI!dcWt"3)m;3
TYCVZXcMrXE.Fu-IQ\.>CI,r9Xnnn1/E/g+$^lE(>q)$LBRnl*-EZ0UFpc_>\$S/c8bfr!dj
70rA9[Y$.dTth0.dEQQ#W*9lmi`GC7eqj2[HbDV/ULR&H,jAJ9HaA%bJ\<kuAMDr2Y\FtK5I
lITpY+sJ5@j6b*W7E.iAF,k=#RFuiF8&IQq6B4Lm(2tUo2A+C9l#'k[34FQi(/9c`pi`R%[q
*jR4q^-!j0KNZ+"Y3ZkL*k;N,b<F1Wt)&0-ldpdUJ>TtmUb^&iu/J!9#\Z@?MWRu;\\SHJ"7
noMACf@8h9**fCTYM(Rg>tQ?rBpoBmhiD0L3uDe(76*8A6.&3>X@]As!qf@<Cq>D]ATh'r7a-a
m''tMYJB]A39M$0D%"E)+tq56Y]AnDd'9T`UBA!1bgk_KiQR&Pql3K"\bY/9!Fdk^T+fnX_+V@
Z=87un'\Hj5WN2SYB&T=mTf.WT;jm3m[#+R(T.[ZG6)RBQ=(,KIuY1Eg@GHaF1NG>6mBUh'(
P%G;uN0T]A>*e$D'3O5@G(S-/m%'CP5^5#7TT[akYE(*e#15U!TB:+YG%Nh*&B^lnT"[KKpLS
qMAL+Xb`2&'C2MtF+Y#ADDD:0@?gA`s/Nomi-E7A3]AFpVLkLmfe/$i$i!A@4NWF4Z\BW?I@l
GD:%fQl%&R6C",4FB-sI-pRU,q5R9Z)$,f4\s_6(jr%JaB]A9LQ6hmZH?D'=;+WrKRpaQX8TA
^SdXtHdKoQCK]ABR#2]AY)i?_`<=;4i>gP]A(c0,Q1A/=69h,Q.lf;p[np^6CbXu9bf8Y#"9(ob
5E\sQJUA>*h(!n;&RGpb#(u-0SY6Enh?]Aur)m/ci9QVY$$]AK1qA/2-H_-ncX;8AIbX>T+I?l
_/s7qCj*,PDo-=,22F+B7YS"0!%hV7!aN3A@hF.!OiuA(ekg(/q'>OU?8kYp^N.*H/B>K.)[
R5*'fIAT5SN:'gOjael6;eg,;)8o@'\@JjD&PEcnTN]A)(EomO>B%,(:3UnkV.9VASO?1Jo>2
@na&S+rHp>42Wm1CR#c"@e+*F;kR^QUc]Ai!QX0Q)UJlYBD)KH;#uNDV.CKOECt&.j^rg"9CK
lXN:Wn/g@o*ToJf5V+0CgfVTJNrF=Fd$b>.)Fn>ksT&T4RW@s5DTdO<5Tp8k*\::?1kGlJ,$
(&l?>Z^,$E;q7#AFPc9hX+",@1Y-<r6L>'nmT@*RPK/^bIP6o&q\ag[2*J)1iT6ORZ"q8AVW
EN791B75Ld%KQ#Fa$k03Tt'rZ5;^XmV:/BX'^rafQk^=:`Pb"[fr:/skOqUg-X*8hLKt@0!;
'C%_Wr+XIrqqgCPNEFcuDS?,pBBa?D9^VSFi50$9=-l1'oIjRCZ,8H1[+gTWW[cR`IhJ(&Xq
F[;P4o2J)f9t#jV@?L0*6XGOF!QSr5m9lrO5UB3Q&?U1j;2-#iI5PikW;lj1$(Y?3[SukEf%
Ft@nkmjM0dWT>\JoP+Dr@W3B\+bd!ICgl.O"a;q.P"mL/?AMt.Yj`Dl2CER1WK.li9K*-WMd
),.2H#H`R@*O1Mm.VY<!Q);F'gh^S,SmZF4En`k56+gBFUS_rt]A=m?a`3UUoCH@nX;2@]AC[j
OO^n:UCRZ286QoRta]ADORc5]Ab(dtM7#`pMe=i4VQL_S8[bJnbAL7,7AoE>5:%A'eD;Cg:qgj
,jq#-Fl9_6leo2&+L1G,s*0";HT=G%$J,79LWDY'+P?q6'*HL2k,q$]AfP)>pe4BM9M_qdPS*
6^m>P3f)lg%Mg=:P1YDN*Bg0hDbAFUh`N.;e.U`%.e5_[T:OT)_5U^o(3M-Z%3J#p-(=O3F"
emGR9FC$?dP,<@bI7Pe=etd6mXRf<V!$,77TC&!D("e\H31@E.tPR%kA2!aa;sN4<$=fgRL6
d[a&S.hk,Ui2'&*.]AS>og0WkZ]A7uN5F-<Y*]AK=ZHZ)?%?WYk"(8`*Lg1/OLD6Z00\!tKF%V&
W?9lLS>pc:gtfD(loHI`u1Cg89k,QYPubjk//Dii?g*rb`+thl82=`W/LsH]A^j7Z#"NiVZQ:
+Y>mI<W.;gkG+d.J>/B=UfB[G`P'od0N5fYQ0e>AU03]A)?eej29I?9,L+%<AFMG=Y?CQ,J3?
%:`;==mjH1;1M/fK.`(2j`H+o,C=m4$mgi5cilFF=n#2>%ro(\Af2MFK6!%;0PkhZ97r:KVX
TnEWNWjbXlIJATo_<N7[9Y;XY)Ib3;`F:i.!EpMf4(7m)3=!4RB\g,4rt"4p"1E!r2J^&=r5
9`EKhj)X>8N]A)BW;,+tTbF4!(QIoNT6o,VW"';Bb,ulDCj'$T^7b$Cu(t0;XbolE-aaIaUGT
O]A7<'<dIPtK!nJ\iabaX@O`O85ll'LiH3>LjuPPZ&s2(An$>YU@,1(4?e`(%Hc#OP?L?]AOsc
nYI?(Q1hXd:(.XnOmd-<Zn(\^3f'\G9^2ID`OhV5@\1*e#d8A]A\%a=^mDP(KG%kW\M.H?e")
`.DW`#;pDXtDW/8Q6[MV\jWV]A5XEF&18YKa.j*5#m3A)$TWdj*En2n.4SOoj'6Afb6[/r2\h
"4&0'Q_qAh138aj%ad3;LknsdTJdsh&DbtEr(;aIYRa<!=gTJh2*6]A'=<M`0R3eSRlcp=mg_
Aoc2TC-KgN8g%n[V?n7Jr)J2!ebIb3hL@boPJGndp,4G39NO<GdC&KB5jN4X%n-Cu$9n9gSK
i.L]Ar2m705j7C=Odq=R$OddO.o(Thb$<H`\stl-='On(:3t(Rmn;Plse[8h3GjXq&%P6]A7q)
:KQkc,QL/d76NsV>1:Ad]Aa0he1o^8rR=rYuV>/AhLT<kA1hX7m@_rdX0L>Q-gN+96\EQ6LVr
&R]A$]A?YHIMqYM=bj.N?Q)4I]AqpGtc_cF\=Y?Ku(0qSE4P"7H)\>+$1fq#rjakgoOTMWGLf,U
]A%@Hidra+r.(gAQ%jED7."-l@c=>uIBcq0^L`N8#l'0<+#UZ&r,/mgV6BDIjjHMA&iCJ_KBp
e4bgOE1_:KqDS6)Z+f8I1%6!S!l#+o4\li1n\<6@\iF0Z!ft6[;s5l2$rFJg@86;fllojL5=
a52nXtU(28+s!EV&'+S$)#&oeR"G0*k*;l?oB'i^N%a]A;OKXeP<@JYEd+'fdF7WSjn1k@il"
OFMnEU[/6s@D-+]AeRdXPM$p;br1DJ/JI;#/0<:E-6&W,O,M]A$5hgT'pL_aM^q,Mj2s.jdkR\
Vf<\LT=Sd01`7"?i^]A+j,WgZ'h!db12BT[BTb:t.p-/"r;i</pRh,`<,6Joe;]AaGm!>=)f?b
m^bLl$.FbfF'mcUQ<8q#SRE,Z6QME+H]AA+=9q%\R9(?/\GOoVTj7B\mH$Z60=g?VkF&7W:6>
Y`b,hIoTk-jA-'2]A'q]A.Qs7rSTI2Cn>4&B;hIu"R`b:-F['3DD)Dk5f5[Lm:oVqSKO<7;./q
JpQ72Ve?ajF!c6c_fnC?!0F%5ANUYdVK),Dr;$+)hM#4!1S09!MDr+T7_3?*=;LYG=t:LA"m
8a4tTAU5E*P)>8N,RN260OmZ,j#YG@l0Am_b<o\`.YSdXuE808Y9qEUbMUY:ZI^S]AG31B%'L
bL^c8fZS68&A'>[I<Cf*)W)(_m`i2;*k[h2J0+2?O-^cImui$_1hk\;Z,kKJJf!g(lIsSLj8
5%>]AUicAIV-jmOgN"OSpNU4d@9i&F1QEF$pPkNn4WteiBW!-+Fni.6c\L7[_?mLE\6h4<tV'
GUgQ/FAJ$*I>quDDEC;0#$eXmfL_n;aaJRFkD<A0hR$on'UA$.G@)7>.I6pP>7"=u;6V!'lX
QLE]Aqt*gYe/?7;Ccl^NAV6dF+OpjSALNXZVIO!$?1-j()VC7`c)?h9XOF`((E>?J.3NSZ]A`[
(P1p,^L)nia_(K7@h(f[RNDKB6UQrTNMF&Le8"s&PNf=1sNmqmTo$pO[Fmh0dNA?K(^RGZh0
*Q+PBC&f_5c_X(30p1m3:eZFPVoo>(u6g,b#SVg+*;bX2GX<A*A[Xa[WC-MTD,\3[brpE+Fe
T>rX$]A.,L/9ZNlu^Xs*\,1TjW_urf"V_O(%-Al-<H&F:Y?YCZHt`i19H>HM2LC7hX;gdEk;X
6/8R((tj1KQ26F37EaEinpC)"phX<4]AO(#nGhn)D-no=)dMlUN9-hBg'K>aprcF!PNOUCg:I
uD!`BF^L"EO4^jRS_/#VP`KRBAa:`0C%9*U\d$fBY=7qrI4BGd-tuLgYRUb,n[%Wuk'ILeM\
rc+fcVVp6%_qJK6+6UT",;6qqHi>NaJ[/iZb>Au"cZQfODq`.)\5a5(]A1i)#u>l;L*GiX]A[4
IN+pq0bEtii90)^7Pdcjf$3EYk1r'8i50.hff$7c&5D1j:L`Hqp19u*_,8K)_oo#pA+\T_7u
I&1/pCO4G2:9*t:<K[\mu=nRilf>Y9T%;J'KK=LDqhXN`-pP5P=$rd^FcdQ07e(01cR_0m-%
l8h?NXF;_Cf]Am$ZU_]ArAq9/YM`nt]AMr+g*A#2ZsR/"bPmk%pNX.RZmeJNH]AmVYVKFn8+0r1!
<i;rom*-nA_QK>4:9PGe]Aet,S\<====I<dq9+([XWJnd/rU,OS98[XE2g[$LP0/ppD3WFa=!
.b@<PL"M5P+KA<umHRb[.deZ/N#A3DWc593YJ,Xf#m#c:-or0-Gf]AQ;V]A`"IV?qAD6a'g<o<
Iu>:ROjAHe9/sd9.hTS*s^G1_Iq;rDiSp#pU)k3HEM@A]AZ3M&D"sDYn\!uFXC$5N0=.aB1XY
`K[M1uTR2,\kirGVO=VcG<fA^[qp^TQ$(L6c<&sp7R6rT2KL+Xbek=0pKEBMB,Jm^o'S@/XE
'2>oCV8+T#JjdYg1>6%AkMr43Y^"VGaP<A.jBiu"q@gqTMZ%fJ$WUCAl*m&%eH,&^3Z=/V&G
>N1S[@uC'_-6efMqs(/MJ^OQcC?-p/aisg,J\$?!!\l06p3K1Tg/^EjSjH'r2?^;_2e2BT!A
t6pq3mL>!q(.Wjo>3GfldrqJ*RY3,ICB$r/V6TXP<]AU6&VbZCCg\BE_jMVWNUCagsdhKl0OU
?[AQ;NPd#A2ld]A&tC.28g`&F`d8$QY"r9.TZ;YC"N[Z%NpeJ36c*,@XL^cj%G3:%e9kNkD`m
st`^AcF-m9P&c/GZ?d_=ca:X5iSXd6X_lZ&l9gUXE(f4+\FTi(,bhc@Q:j7Vi,/Vjp+!U]A$H
G3B-SUcNQI%@h!FEds@t[YA4P.%pA7[)c%bSSa&OI`fC6+F3p"oA4<)A8>)-BsuD;^W\g([+
UD%h`thBEco/T4@=Su;N'8&fuAY^!\.Sp47U[bLD#nhU[<UF]Al4HCAALSul7T]A1pS*r"f^LO
I3p$5UIQ@F6YFZ$>A^404RaQ$$qs`tW88^VG'(3%&!>,W,lH/1gWLLlXUU/bs+:N':S1QX]AN
AK`(gddPcmUo`c1:5bHRC/.UaKps!V'siK5;pgOh$m<;i/`L(SS&cL=q-rPLq&[u.)2jb)3/
*09X#0Q/(u&Op!+f*)d$'af$aSt]AFfG/4?!,S=Mm1u3;MZ\Mir[IZt&O+e7D.g*aiZa*:m+S
V?mMTG.'L9mXPVgG3IWs#UQuJ<ftK36(?R]Am44C&S@-mJq=fr&HOj$MmMtP*GZ`[P[ghXI.0
"DEKk*CAW`g]AcY0Yo*\9KrCYr\Gr4sI2NJ<6/M<60YR,#N2h(s:TqOFG</9sbG3?ur92>BtF
POSai#'O;W-$CI6E<dmFSZ-gAa7J:R_bYSo$[h/o$qiD^dJM5bMe-aa*7hle.kOt1WQt]AC6a
@s*3#)AGbSsc/BaRMF='2TNNB+J#U&Q.NbLtP>''qMoBg]AL,nroL\?mX;b\5=sW4l'g(6$Bk
ALm2.OQP'q)*AAL<c9lnC@WD"k)1@(A:>?Y!TH[W'1523KaG-#\D5,3_;NFt#PT[E\p5/_XO
fQ6i7LRnj.%GFD#`Q4Asm2%]A*8)U^PXAlgpaB^8@4SB!t;[mg3B0PcmSTF-@-60+XCOcBLrP
6Pn?.+%q!J"5K7"_\,%RepS5WlDs>h*T2YCSmP%]AaJQ[j]ANY7\*heno(@Z*77$ZEYisJZHOK
.A[:2[Gn=i;nb)K`&C8^)Dec<P,s/>d9)EcA#?p^7?la@rG0QcsnVWKHkbrX<q?E44#;s+;_
tg7a)@l0>f5t^:kp-m=m2FuSKG8X1;CEj*8-"-b/OE.@Y+VVghIe]AC"QW]As9UrCDn2hC+)fd
L?imEV+fA+/%"I-LMa\#,FTG9b*m#2Gq!K2V6Z]AVY"&.U`?TnlDl_pG4Sm)Qjtq.1=*orr1=
Y/6OL*;,F1V\cok"i9>%;?ka@=)/EU-9@f%b*O\]A11ld7TRScEJ!q#SaVYY41HngiZ7[uWh@
+hpPI7Jc4;T0o/-tDTA3os#4tet?kBQ5:_qY93m)*P!Mu%H+kHG?V?o$^DUPjd:7hl!c1&h]A
DRB$hodSBl)\<_8q&@<Bq#i0NuK,?lBh[2Yd1=<h-?P"@(Rf+^BBN_Y9ef6+/J+OQYoHJUaT
.?t>h(>,ZqtKrf>@+<-dlW?b$N~
]]></IM>
<ElementCaseMobileAttrProvider horizontal="1" vertical="1" zoom="true" refresh="false" isUseHTML="false" isMobileCanvasSize="false" appearRefresh="false" allowFullScreen="false" allowDoubleClickOrZoom="true" functionalWhenUnactivated="false"/>
<MobileFormCollapsedStyle class="com.fr.form.ui.mobile.MobileFormCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
<lineAttr number="1"/>
</MobileFormCollapsedStyle>
</InnerWidget>
<BoundsAttr x="0" y="0" width="250" height="150"/>
</Widget>
<ShowBookmarks showBookmarks="false"/>
<body class="com.fr.form.ui.ElementCaseEditor">
<WidgetName name="report0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<FileAttrErrorMarker class="com.fr.base.io.FileAttrErrorMarker" pluginID="com.fr.plugin.reportRefresh.v10" oriClass="com.fr.plugin.reportRefresh.ReportExtraRefreshAttr">
<Refresh customClass="false" interval="0.0" state="0" refreshArea=""/>
</FileAttrErrorMarker>
<FormElementCase>
<ReportPageAttr>
<HR/>
<FR/>
<HC/>
<FC/>
</ReportPageAttr>
<ColumnPrivilegeControl/>
<RowPrivilegeControl/>
<RowHeight defaultValue="723900">
<![CDATA[723900,723900,723900,723900,723900,723900,723900,723900,723900,723900,723900]]></RowHeight>
<ColumnWidth defaultValue="2743200">
<![CDATA[2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200,2743200]]></ColumnWidth>
<CellElementList>
<C c="0" r="0" s="0">
<O>
<![CDATA[市]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="1" r="0" s="0">
<O>
<![CDATA[县]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="2" r="0" s="0">
<O>
<![CDATA[笔数]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="0" r="1" rs="2" s="0">
<O t="DSColumn">
<Attributes dsName="File1" columnName="市"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="1" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="File1" columnName="县"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="2" r="1" s="0">
<O t="DSColumn">
<Attributes dsName="File1" columnName="笔数"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="1" r="2" s="0">
<O t="DSColumn">
<Attributes dsName="File2" columnName="县"/>
<Condition class="com.fr.data.condition.CommonCondition">
<CNUMBER>
<![CDATA[0]]></CNUMBER>
<CNAME>
<![CDATA[市]]></CNAME>
<Compare op="0">
<ColumnRow column="1" row="1"/>
</Compare>
</Condition>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Result>
<![CDATA[$$$]]></Result>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="2" r="2" s="0">
<O t="DSColumn">
<Attributes dsName="File2" columnName="笔数"/>
<Complex/>
<RG class="com.fr.report.cell.cellattr.core.group.FunctionGrouper"/>
<Parameters/>
</O>
<PrivilegeControl/>
<Expand dir="0"/>
</C>
<C c="0" r="3" cs="2" s="0">
<O>
<![CDATA[合计：]]></O>
<PrivilegeControl/>
<Expand/>
</C>
<C c="2" r="3" s="0">
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=C2+C3]]></Attributes>
</O>
<PrivilegeControl/>
<Expand/>
</C>
</CellElementList>
<ReportAttrSet>
<ReportSettings headerHeight="0" footerHeight="0">
<PaperSetting/>
<Background name="ColorBackground" color="-1"/>
</ReportSettings>
</ReportAttrSet>
</FormElementCase>
<StyleList>
<Style horizontal_alignment="0" imageLayout="1">
<FRFont name="SimSun" style="0" size="72"/>
<Background name="NullBackground"/>
<Border>
<Top style="1"/>
<Bottom style="1"/>
<Left style="1"/>
<Right style="1"/>
</Border>
</Style>
</StyleList>
<heightRestrict heightrestrict="false"/>
<heightPercent heightpercent="0.75"/>
<IM>
<![CDATA[m<s=`;eNPJe"6C3II/sSF#o1Y+DktQaR(rn&;?qQL4fmKO$k8/e/KS9'TcO:BA0W?U<+5RYo
ohXO_GC_Je2fkAr\)j,"+[i,p<j6$o(#kS_VO\O&"#BC>SnO_bk`7r.0]ABp0R36J*XRmCTTO
V-U3WJr5Itp?Cs/phCkV3D2t)En)qc;o3U8BJ,^&O2k<]ABke)25c&^TViP4NI)]At*5029Obq
NFQ@PIpSeji@\:=A%2-NJ[C1m(^Bho\$4IffJ6cXrX"@<tE$YU\2qJg\u>h(9&a-QeHo:[Ya
mD;LFEXDgi%S9ISCmC$mGLK._)S_bP2kg/_*S'8duj1!R.q<0#\X\emM*Wuh&j8(#u:;u.3;
aXoRe=PCX11jMq"BSoddeB$>c]Amfh<W^[3\\)4>8kW&E]Aj;&XXj1eslo]A*5jiQo\r&iZqqM\
]A3l9St]AUdV3)J1;^`f%qr.)fCY,2P@*adhW1@4Jrq3LH['4h5]Ah[9O)Ngs.UAbiL`ah>7Wb0
24oXr93.sPMY?SX%]A]Ad/#UoM@iZXKOJUAB;;V=@9'Dfl`-3S6"#R%U3n@\@)>!+"lB/u-/n=
F)[>'iQYWGG>H6lc?oLMGe3^_O/6'bimsO)LGrMcjCCCraAi;*34C)8U\?=;-/6O.=Ia$T6(
?9InOoiTkWcb[@QkXF2H!Mof8&X]A%Ti$DQ+muHL7^2D'\@?hLadJ9>@SX)/:+t?+TuB)lmNp
CU!_Aid&ohK;bWRrS47(W^G$8l\rm*)0O!;'$es@PV(]AqlZPX6qU`<LfbKBD#t?'&BT(>Hft
@XjQRi#SqUJ<oiYj+=Oo"["b1jW&[o8dPDkm2FlYiUX^TW5)H"C[Pa"d,Llmm)"P#@cnrLH<
PW4Ie>R.uG2h^l/%`agn\3d]Al7,[\,1ZEJ(tgt,Fr76KD^g)=NYKbY3XNNVkU`>",7%n&`#r
h\O>/7eD:^@Y<Irjk8=2.&1d\LF\!U!5Z-89XLW/,5QIK+q7.>Pd*U_r2lQ*qSK!Mi.;Z6/t
`\o]AN`f+jZ`IiD6+OeV>[Sl`G;.LBH+\EAg-!ilgO*X-SHhr0c+&Nc\^pVq9\lk4(M7OP%M/
,[al2dDA1!CZ2+[\.,jNM3gYuq!k5kJ03Oip2uGp]Ap?[P(n:<&Y\g'C8%@3\ipe$(R\W2Z#b
-]AM*u6I[UGK/fH?n_c+JaheeLkTj6'9mLX[kkbQkR4$4%tNMg;kaH\.leJ@lL7tEq=^uOV9-
Cr*d[[XE8:SV>di6BEu*Y>2"!mcr$MqHFi/kXfc3.mF'@66RP]AmO5YH<D0KPjn[K)_rJ?(Un
4$%R$TQH6"`d=&'(T]ASoZ+<-)*1GP(a9El^Q\NcVhgB-'o:)FX8$>4#&W(<fH&f[2@fZ2a1X
(\hcc>Jg%+h#Q0_Lg)r#f9eB4r5qkei[U3fApfuj!(gKU:Tm1Ys8rNVhcIlnI%oR$-e`qJGR
)DJmjJ;JRBp7/DIq*]A>mUW7;je=k2C44Vt[7Lp>JQ7SQ]AaZXq0#hjgGfb5"GDS6,-EVFTJA3
b<fk4<9aO>p"m3mV!PUdn1;S<cr?mH/Z#-L2[=H5+2l96WJ3Z@!tg?sc"3^L5@s#:<X,rlWX
:J6/te=4D^alJO[@Z??ukD!d&nEdE8RVuC9Bs1-m:`Pf.]A]A<ab\`Q4KkGN.UnHg\GaCF0(J?
]AWBF>W>:_5\@UJ%j09q%l<%E4-h[oN3uA6klL#.p)Nfm!q`iLEc76&^Ac.FFH67u",s>,]A?H
?T<%NDk^sB5EBs6p,aj)en*nj#G%sllsrVgZk4"J`olrj0bg&]Alm,@'ho)W_SB2@__p,m:E7
-Kb2:FA"rK[r;KdPJQYPYmPg5el@o_*"PiWU;7*[&"<Cq)Q23?s.h"S-<RjaFA0!`hMgUK/9
FbL+'aml0;\Dc,\)/BgqO>\c8lD:;M9#)R5h3=hGBNsPL@7^I9&[`?A*1W46,sTH]Ar-u*bsT
(gCSHt9=eOI[GND&Zl+RC&C!A[;h!$,.uo"%7&S^?Q7Y^9Zt,d^96%9#8]A3q[*am/oKf)K09
1M'X(U'8+AJJo6SB).-<d7_$q>^ZqpQJi.I\a@]A1'rYXl9l>APsT2%g"i'?pQ5D\:G>J[@XP
k`C.DST%4OiH#Z<Y0@;rh"=_S6lr:p\PKQ8d_JIZnhXB%HIrMouo9=ebOb31-H?W'7K1G?Fq
B?$@@7brIoL"Ps1ViE#^0No6Md(16R@SsVj+F]A<(_F\fo^V;&9\BHql_5kIY7X0e@J%JP4`"
oV=S)e@hRJb&t*G4V$QGJn<m<#I%AD*!>8/*Pr]A&=hiV70!,;71;%E3BR>%+h]A.L_b;'>>hi
4H]A,2pg4buoNcDr@S22h=>tV<.Y6nST/,(8)o-mkWgSu=ai=F#h/S#ZU&rNW.cY'ed']AYWRG
FJbYghpqlH?dAr)WgsHX;(P!IS%uJ?p?f#:tgZ$BWk1aQ.l0$_..=?Yo"8sWPs(K$H-V`[bn
`R`qEs8UmOF^%WL8C:<'euYjMg8>[pp>KJ."NF-"K`L<2&7Ec<cX!TV@ibP>?-7LN5NPkUkT
2s;^KRi0Y75?qVfF%QA^FD)2gfJ,^r+Ja]ANIchFJnZ@2*0%-e#+WfEa43?Bkf$g0`>=/^'oJ
4!g/Xt.%]A,[rh3O.4RAj-,ZS:9r&.BDFXqPrJR:W]A@MQXd$D4]A/_H_!7K[BO0TL)IsC77E"S
2F+%DOF]AV2+(.@<<Cca:!3/TBh2Dppb^`f+1O-ndQ;7,*W?%5eOThS$lP]Arte<`t/.7^FG3=
7-+&Gp16?6(Iq!kAX[lCQQEo-i1J!3&+;GHII2q<8j*Q-e8/a.om\b7""^W))>2@S`:G2iQJ
9`\(MIZga#ZsR$V9WmT?QU'5`I"#kQlQ*XMaK)-sLk>gSNJF[Yg?]A[0laS<W`'Pg2ieK[>RH
YQ#$]Aj5IIM4GLbPg\[]A/84$YZLWp0KQ^lLiA3m3ZCA6r5[>hc3d&_0WP0',(8$DWlaO_[$]Ao
Vb<cr#.NPtFd;]A=-JTmrX1+1aQ+,>e;#mr*sDZQ=I5Zg8u^l6)h7pHPZHX:RM)rNDf1f(tI-
`&Qo)H6-<+V:7S#:PF2sKM`-FfbaQ&!]A...N_m[5$Y-m,":Q)m.ABV#a=UcOq"cC,p"Xb"^8
+0SIZeC4jNSJ2U]Ar6Rq9kq1$_'Fg&4&#<OqilcGcDIC#8F\>EdiT#nW3B^PW!OCWSBZ=AT(Z
So4^(N_n!6ik$:KSZMVD4%KYFcFX@h1B>\;\bhCaa6PgNK5S?L)/TV4mZ7sfREa+df<qQ:8t
'AM-#b%p]AtO:Gc,adD/)?_]A(2HPg1.ETl"kqVr9qj6F$s4_0o)LWAP+d]A:QOm;D0ckVnmd9)
2.FUs%1Ep;Jn8P$oY1]A"%$2-V)AR\"^5'h"+qiW&U#h=2Sb[s.XO%YZKFf&T/KRg5qee*7P0
!-cHFd5njnn'Q\Btc170i8+DTdaB#@&&%ClL`/N[1diMS1khf%tr,5P:cJUp"iSHG7XE+2-$
T2&\E-XT.!_YkB3i?M_7=5S@9<C9$@RBO[-6sd&b,VT0<Z@.tLNC)8\uEOU^`:_CYLu\_C0S
)]AhPAZQ\JsrT$7hJp[(.d'qO?:YamS^NS^Z+qSu^47=kTtPUV*h7682F0+%4'$jm-,]A<tuXL
-OH4/?dSM#[-?qBeS.\i\Z!do_\\N@(mf:m0&]A>9:PAJj.UD=jhE7=`:lq'-B>#-.=A9*OoH
;k>"sni79sO7)"u4!*o`MpEOQ+m>^^,.U4.YQTf5F72rg@RL.+<Z!4(KN]A&4g5`\=:UQOhN)
dSjHil`b.VlRg8+Mm(jCibaK"o_pQ\O8I.3NK-jbMXRm5(fS2:fZ$>8]AEn%r`Ii`*>qFi/_>
Y&,[+lmYB@<YF'8iFd:n!a_NVEjsmE&mHJRt_%>BJ]AkhT8&Q=q'H7e'sMl$m`;G+8re35`]AX
>n2L2rb4k;d+ONo.QZnXUc0.4@O@Vsta*7lXQ$@=P]AIRY(L+^^iNjaq^Z2caM`\CuQ"$K5Mn
Y=W]A2_kpF/o%j+CcUfZ$=]A%(i9LRmX9&;s7k]Adq^ofC6r?h8R;)Wd6hf6RPmY[D#BL`CZ?Fl
d_RMN)Z>b/PuL2Q^</URp\X*uBbSjgLc?"t+?YSjd.&pR&&3Otu5Jp,G54RW:Fo1s%V!ZikO
'0"Qm-:*h_r&SWLH^iud*6Ag1m@KbSS.G$s^o!A=G!0qP>"-Jh[f9B%:pR>.Gmf?E]A?*l<S:
M2bW$=N*WrZJ@nY<HJ3NFeITZ'Ih%h=9jQU6$(<"F!g[Y^P5J<hRUm]A;k1r'Xj<NQ-ob]AFO6
;)&oZ(eCZVM##?)a>Skop(I=E0^'DbV>PK,u"AF##GR3&OpA:^c1J%EkfS2>D^)UB%+/'YP<
4;n4!WpnpW!g2gZ7tei-e(+pQUMIJA[:kd`%j<U#Z!9CAX`cuBK#>`m3?b\M*0VYi<FfiuB1
3]A"J%Bef^F4gpoi;^JXW(,E7BkRMJ_F9G_Nf$air6o.d'7=9BZ8S:Yp.@_$@21p?Ha4b=`Ul
QIJ"2o=4N`D+P`W$5tK/0TJWeBS=si-JL$.#pM(e=aBHP5\jtf7H^"aL1s,9$6u<IcWEV_1$
B^.g45,PgJBICjdlYgM?u;51Jc1u1WML.RmIlXf4.mQs]AId=>f/hL.-&.Ug<U;%:0j7h*\p@
o+ob$RNX30PM(WfSaje?1(,=?e7"47TGK)RNXGV6A:9eK$-Deo?N'=TQ]A//f)C37=<8V.eA2
A+SdBoBeoc6p$keQ5Ef!4]AjGc^d:n'XsJR*TmSWthfIN^Th'?^*To%fAkP:\e$lQ-.?lrTMU
F5bp'JdRjIQ-js3)I8V=^!;+pp?cBj5VT6E[nL3eIc>"`EW4RHE/,&.^_D:%>]A!0.l9E77-f
53OFnrd)pHA/->phc3b)FMHEj+YsHWehroeG"ZC^=#@$J>*H]AUmABOQGNk?pqUNV:Zp-qGNN
A;EKjn@7&r;i8XS(A-\4.sn$/=8cHMuPkm4Qm)c>4;<aLL/`a[R\tOW98a0=]A8)Zi>2I$62(
42`:Tcdgu:nO#str.$8$ICVT6_CgG51U6",^.n]AV.K,f5/I$@ieiM]A*F%..'I/4?p/jPGih3
`IR<qJc/p2_'t+*!^AU$dJKsO*Va76H\F\dSc2FSH<2bJ<dQCSG;B[eX,JU)Q`'r1T?^d"o-
li:hh2'IS[hk@4@<4M!om1kQR1=_+9i,@6fPWT!DWt0#ULh2MW\4PnlpdpLlW=/X$OHML#El
tU]Aa#f8c9;jml,+t`AJbI_l7a1*p0J`7Q$]A>cFJ?`fqa6D2KI&DcFa7/"*8MFI(,$qFrJQ0*
UhoQ$%8+i?\f)GCHRS3cX$"C=#]Ae#lCMo:P\E%BASc#<I8`8T9-DZd0dke#`%Z>dUJK4c,S$
VN4?pqMJPOVCOh84[]AQ<"h+dTdX+SpTtbRHjA!oFAP<WMRpD<o<ZiGt.c'%A=*-/N1\[QU"c
d@?,ha&=l,;>k)nq\iAUMqX(oIc'B.V<7?mm)Kc`["OQ)VO-QGB66mjT&.Tj/TYalIZHkHZh
BVRs$#i*T?:gYNK;5sbHC.KkD/tp]A++)4o/61j?.%'Y-ia+&AUmK';.it]A-qW9/cc^Q!`%We
+^$!R;@46\e[ATD2f-?0JJ=",uE.Q9H+!_VB+^*m50*4?k<aPG25bLgFg-O'N]AEl6kN*hNgH
VS[q'[\Wub$F2T'>Y8$pr+C_n3dL$[DQQHmD$pHa2fsh^VFG)S<SR>a5h=e#V<#djg%TG3Ab
on2H0J_&=(7>"!#K;\Y.J:S*9m#d53X)p9^Ylm5t7T\u/Rm=PY[#N._ikpgE07,2H!DVt+6c
fQMf04#.SWK7#[3@MDoO>>",kP+mG(c5>B;K*"<Nk_qKR^D4CL6J^/2)%#@?#OS$1]Ah'u@fX
nHf#I*SVf2Xl$N._jo4Z1BEUX#DJ_dkG:Q\Q=aXsof0(%i45YCJa!26.i+,E7[?jN2\W%qP(
Q-/_a1&'cF!Cdt5KMbC;?M,rAa3_6Z'Gju</6Att%RuE"4*dnRYoLB^m8+S$@/RJL!nS#RQk
l*;rL^@t6jehgNq-2T^oFiPWW8)U5)>R."^?UCB$nrjs?-UU8%KW?MMgnU7#_nY3OmL"n'p'
`dGq'lF$ZSVi.!Q3faOu8uO`#/NChB&Blj*I#3-tsT2AlgVa4F'F,,Y8AP*i*u?j>b@.:,D8
UqkV^j/l3?!>Q"JUXQob+qTs_OCLA>FP5<!j1nh`1X'84^!"*feD(,!Qb/1Nc5[M44=4rgqO
9+P$"bLJ$=>-FZ\W5)S./MG$Q`7:H6<r0@>#.5oXAE5WBCA5EZ]AjPU$j$rEXZ(+kU.Z&E'Tr
8D7C.-9Sid1dTf-KR@AB4UhL&Dj#b"h7)[jeQ3^XDq07$toC"bL1,(\iEi7EM7.>hmM)?*f%
,tJF#aTuFe%QG!eI7oo-)PiH%-,Re;Y-sIb@[*rIl?Nc!I@OqSIV#]A7`%H97C;RLLoo5iP7N
RSgNI]AtkD/[!kpu_Kb0Z.\&fR_-IPW8pWf,#s7s9T*Jte2F1(tnE)lN/&rN%,iV0WRZ)Wl&9
ao;R0la*YHo!]Ad^%rf1ep=N0Cr]AZS6?a/r4/$!`Cpa(k7g97mjlh=e3VdLKakZ$s@RumnR@D
75BBVU$GnO9k(+3WTOK95$,ZEGhiR@@Ho6ij!3(jJEo+uhVC3AEY6Br-I:!g<Z6?[cD.^jrN
TC:)>G(&#M!D3.N@1bY?$htlL#\%,8fo@!O[Lei6'D?6eF@l^*4,0.KdQ.7:`BZ2d(]A!L\m$
d6f>f9?kc#a`Bk$I=k(98!,$kt/l/as&.("qtcJrVbk1qhN;41SXOh$b$KQf;`VNRXPh$[5"
J(2StS.4e_7egO%rP#CogC'+0^-%ZJi?NR<#(*?bIXX`MERLHBuM-\GbubV"Bm1<kicIie^R
Vi6R'+\\nn_JB-\k>k^GfSl-6!PlOc!KNZW"DQ3\qU'$4".Q[KXe@T2pc3;(%^Z,g/.*57*B
oqRUXV;)r^UI02kR]Aj)r@LfS)rO17K;qj;ofVjGb]AN^AU=TO]AmGcg]ApJ<GjU/3'G>d1TQ1W.
l8;*L<$#o6]AHC(f,1F78dC1C*FfjOCTl@jdd/L-eDquV+5Ii=kY-`-njg/hh.pPLn\[.+`n?
4>Pt0MF/+,0K"aB)sM6WE/Ej)']A`l`0LjX`>JoQ^2dA*82!fPAhe6IGq7[4[1\#KpEPCD5Yg
tV)fqcXfn)_<ki$@SaKB%,?RDuhi$!a%[_0DWo`^pGq^Rt4X-AV9#rqA!@=WV#d9)W0%/L#p
V<_0CgAU1TH6d/+RJ`WPZ/^u)>E8-u+!YGL_$a&ESZfh\dI'/e:sqZDZ*ek@MhZ.6I>t"2?W
[la9LLZ\Km3nafB@Mg%ZGPAH`\&Y[8Jd_qt-81?c,,O!\L0&H(jA2'V#OS!6`f)!C/)RZ!6X
7*Tr&`3P(ujl3lVUV>M*J24Hqci1lZJZO%[t"=lpDPm4XGp%3Bn<&L.)(.K]A>GuE)M4JH[.=
Nk1LQlklF:!dI<Vab!R"G-d%,(c!oCD;q2E;luY?E<dHTB7>MVhjZ0k:6bl^T1C)".r>f<VC
.*FES`c=nPoAD)"@@BYIsB@.f^J"j3$3Kn2)XPf#skAb>XX,N;;4Kbq_$1DI"r$u*mQC*]A<S
l'&akZ%2+_Oc4;^(jUKC-FmNt4p=W;d9^*`^L\[b4`_%UaHij47bn1Y:H>jXTrIB4$u[Xe6L
44s/pk2PdLE3BGPWKq*/b4p_U=T%pA[&rEW3gsa2J@=6"@+bfFkd\l)R0N17Ne$,AK\lA4]As
,5$Z^(C`*XaYm"%G"f(XEIg.*hS&5NhYYFfp/:$"b)[59E)"s;4n+SSG0K;,6E0K2a)k#cE&
^#A5n,sb@AVI=X^Cf,;adHh4H3NNR(FL,0+DJWmZ^RDt_r"EU',SQdW*'gJE2$[R]AF?M+20A
C5_-</=9=eR'*DPURPi>9CQmdWXMu]A!Z5i$kALe]AQ4pU;aU?AiJ-,P:?LYF-p/rkXK(;,[TW
q-SiKoM/mO)VeVPYm9QC.%(b>M2k7H0WOb6>,YE0mkPtq$62''l[C]Aha;fiK_Ha@pB%DL-gt
F6^1k:LnUlI^s#KEAM!j7W@A.h+r1\Rp!G0j+u2iG%)*(^j)acMmZ!n^[Ydb$>TN61cD(nFc
4jou6t#hV;pU"AZNU=(K`0B<FP8>R&4/-606iqlLJHdG(cE.!i0RPHhHFbjC&LKHmi^(*L4+
408lVR5p8U)Per\f2f2=EX\Wo9IFUTPBJp>"cC*N9j]AKo2^oG%RU42,]Alpd9%f!rLET0G_,?
jkZmX'4EA+;I9.D(3^uMUghmMTpE='q#$JJ4r%XeJa.\kBc/+IRJFDM^;gh$d%eC<AV.gKM-
njXdr]AF8&X]AqKs9(r;p[EA5BnPEuH%Eru,dM#_F;kU*;Ee^_fa(bgt<5c*rQs#eA>1K,7Y2h
chcH]A-1g4[UXO-MP!(cEZj)DP9q9_J).E_q.UD-^Iug_b^iR%,q=JI0H1e"_Z7RG$\#9MmgH
hc:&^c^u<EAkGHTs2N28+kcW*/T>R,B61LCiEfPQT%f<\:L(jb-F,D0FCP$sAVm^<-&k:>po
F6V+$/dh_@M+Jf!lpC"?UErC"S]Ap5LFFK]A%#k'8(g[FgHME8C)(G;WDZC;2k%5JaR>CIm'!]A
Vtl(c$FbFMT-U[%.Z0K6t(S>%/2>aBMIKnKoh]Au#P%;$a^J").?NO0pqao<.[X@kk,/cu+%[
QS%t7iAJiLXS<$jbY=iMOppAh(P#/Vp"U:+,-Uba;eeTLC+Q&gkL2e2"=R7]AN$F$A+jOJ%ZW
j/#p19qkYSo7WZFRjc0Bo\D2H^LlCn3('IGWMAQGM9L=Q6je!\KV^K[H0sjHb?+eE\H3>OT)
P"89isE80Gq9p[e/^2k=&+)=;1$A&\%,3Q=%$1/Ep7$n_e]Ak?Bm*Vp_D4E,Bsa^\WC9KqYJq
>T_6/[R@:cZ49.172aH1N;bq`!`bEK7/h$r)/ClN[uf9A2.iIB6j#@ABMqJP=hZeJYV6&*T?
<CYD49+__J)]AQVG;OEGg"dbAas'"`\iNcp@)(4c3&-\n2e9-TW$\gGh,oIc3jE3`:d;E$*Ic
_e9PkFPi^XR`bXh.:E;7Yeb#\J2I[$q"c,pOhc@k32Q$.gS$"243[cc.([q3efBf;/8/P*?K
$Fd4J\X)baj96iBUK=:Wj*9K`piY\j2sNrN:-0Al7$t&FSZCj;W4T@Mf>+2!XDW@MjmmV\)U
bGC]A]ApS/c\KeG+fF(`CCuPi6YGL"1Xq/T]AgJS#tj"g2ipe%dtO^@@Z[bn'^Ht>4]AH<";>td!
bmdNJ?O=$.gXV]A$["$)\_I(h:KF"1rT;Xk?C_>7U:/!VDV4a=:]ACL~
]]></IM>
<ElementCaseMobileAttrProvider horizontal="1" vertical="1" zoom="true" refresh="false" isUseHTML="false" isMobileCanvasSize="false" appearRefresh="false" allowFullScreen="false" allowDoubleClickOrZoom="true" functionalWhenUnactivated="false"/>
<MobileFormCollapsedStyle class="com.fr.form.ui.mobile.MobileFormCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
<lineAttr number="1"/>
</MobileFormCollapsedStyle>
</body>
</InnerWidget>
<BoundsAttr x="0" y="0" width="569" height="156"/>
</Widget>
<ShowBookmarks showBookmarks="true"/>
<Sorted sorted="true"/>
<MobileWidgetList>
<Widget widgetName="report0"/>
<Widget widgetName="chart0"/>
</MobileWidgetList>
<FrozenWidgets/>
<MobileBookMarkStyle class="com.fr.form.ui.mobile.impl.DefaultMobileBookMarkStyle"/>
<WidgetScalingAttr compState="0"/>
<DesignResolution absoluteResolutionScaleW="1920" absoluteResolutionScaleH="1080"/>
<AppRelayout appRelayout="true"/>
</InnerWidget>
<BoundsAttr x="0" y="0" width="800" height="430"/>
</Widget>
<ShowBookmarks showBookmarks="true"/>
<Sorted sorted="false"/>
<MobileWidgetList/>
<FrozenWidgets/>
<MobileBookMarkStyle class="com.fr.form.ui.mobile.impl.DefaultMobileBookMarkStyle"/>
<WidgetZoomAttr compState="0"/>
<AppRelayout appRelayout="true"/>
<Size width="800" height="430"/>
<ResolutionScalingAttr percent="1.2"/>
<BodyLayoutType type="1"/>
</Center>
</Layout>
<DesignerVersion DesignerVersion="KAA"/>
<PreviewType PreviewType="5"/>
<WatermarkAttr class="com.fr.base.iofile.attr.WatermarkAttr">
<WatermarkAttr fontSize="20" color="-6710887" horizontalGap="200" verticalGap="100" valid="false">
<Text>
<![CDATA[]]></Text>
</WatermarkAttr>
</WatermarkAttr>
<TemplateIdAttMark class="com.fr.base.iofile.attr.TemplateIdAttrMark">
<TemplateIdAttMark TemplateId="01ff7adc-1ff4-4a10-9ea5-ddd132449d21"/>
</TemplateIdAttMark>
</Form>
