<?xml version="1.0" encoding="UTF-8"?>
<Form xmlVersion="20170720" releaseVersion="10.0.0">
<TableDataMap>
<TableData name="长三角各省份销量数据" class="com.fr.data.impl.EmbeddedTableData">
<Parameters/>
<DSName>
<![CDATA[]]></DSName>
<ColumnNames>
<![CDATA[省份,,.,,销量]]></ColumnNames>
<ColumnTypes>
<![CDATA[java.lang.String,java.lang.String]]></ColumnTypes>
<RowData ColumnTypes="java.lang.String,java.lang.String">
<![CDATA[Ha9F4g",OdG;O:X)&O304khsr7<ee'3K9P`@.)U65S5^%'Zb^6Y4Lnl]AcaJ3JH5`~
]]></RowData>
</TableData>
<TableData name="城市销量数据" class="com.fr.data.impl.EmbeddedTableData">
<Parameters/>
<DSName>
<![CDATA[]]></DSName>
<ColumnNames>
<![CDATA[城市,,.,,销量]]></ColumnNames>
<ColumnTypes>
<![CDATA[java.lang.String,java.lang.String]]></ColumnTypes>
<RowData ColumnTypes="java.lang.String,java.lang.String">
<![CDATA[9oZsrJj:#=fCh(&]A'/V*72jZ0[A]AcU_+4iH85'q6;*X)f3IfF=(o-3O*:CeCCfQAI4"Rcunb
iJN@MB7'gDa9Nf`mq/>S((rf2,I1_.qTe/#^MsD#$)1a;NBe<aHc;7j;-AV4$b.`g/<j3>[j
B1TZL`"kCRP._?t(5=`MQ/ir_QaOB%=&ZHB8mnYQ$c^jq7~
]]></RowData>
</TableData>
</TableDataMap>
<FormMobileAttr>
<FormMobileAttr refresh="false" isUseHTML="false" isMobileOnly="false" isAdaptivePropertyAutoMatch="false" appearRefresh="false" promptWhenLeaveWithoutSubmit="false" allowDoubleClickOrZoom="true"/>
</FormMobileAttr>
<Parameters/>
<Layout class="com.fr.form.ui.container.WBorderLayout">
<WidgetName name="form"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<ShowBookmarks showBookmarks="false"/>
<Center class="com.fr.form.ui.container.WFitLayout">
<WidgetName name="body"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WTitleLayout">
<WidgetName name="chart0"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="chart0" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.ChartEditor">
<WidgetName name="chart0"/>
<WidgetID widgetID="94ad46a3-7f3e-4e17-aabe-06f268b20629"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="宋体" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Background name="ColorBackground" color="-14867386"/>
<Alpha alpha="1.0"/>
</Border>
<Background name="ColorBackground" color="-14867386"/>
<LayoutAttr selectedIndex="0"/>
<ChangeAttr enable="false" changeType="button" timeInterval="5" buttonColor="-8421505" carouselColor="-8421505" showArrow="true">
<TextAttr>
<Attr alignText="0"/>
</TextAttr>
</ChangeAttr>
<Chart name="默认" chartClass="com.fr.plugin.chart.vanchart.VanChart">
<Chart class="com.fr.plugin.chart.vanchart.VanChart">
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-1118482"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<ChartAttr isJSDraw="true" isStyleGlobal="false"/>
<Title4VanChart>
<Title>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-6908266"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<O>
<![CDATA[新建图表标题]]></O>
<TextAttr>
<Attr alignText="0">
<FRFont name="微软雅黑" style="0" size="128" foreground="-13421773"/>
</Attr>
</TextAttr>
<TitleVisible value="false" position="0"/>
</Title>
<Attr4VanChart useHtml="false" floating="false" x="0.0" y="0.0" limitSize="false" maxHeight="15.0"/>
</Title4VanChart>
<Plot class="com.fr.plugin.chart.column.VanChartColumnPlot">
<VanChartPlotVersion version="20170715"/>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<Attr isNullValueBreak="true" autoRefreshPerSecond="6" seriesDragEnable="false" plotStyle="0" combinedSize="50.0"/>
<newHotTooltipStyle>
<AttrContents>
<Attr showLine="false" position="1" isWhiteBackground="true" isShowMutiSeries="false" seriesLabel="${VALUE}"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##]]></Format>
<PercentFormat>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#0.##%]]></Format>
</PercentFormat>
</AttrContents>
</newHotTooltipStyle>
<ConditionCollection>
<DefaultAttr class="com.fr.chart.chartglyph.ConditionAttr">
<ConditionAttr name="">
<AttrList>
<Attr class="com.fr.plugin.chart.base.AttrTooltip">
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="false"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0"/>
</TextAttr>
<value class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
<params>
<![CDATA[{}]]></params>
</AttrTooltipRichText>
</richText>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-16777216"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="true"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.5"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</Attr>
<Attr class="com.fr.chart.base.AttrBorder">
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-1"/>
</AttrBorder>
</Attr>
<Attr class="com.fr.chart.base.AttrAlpha">
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</Attr>
</AttrList>
</ConditionAttr>
</DefaultAttr>
</ConditionCollection>
<Legend4VanChart>
<Legend>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-3355444"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<Attr position="4" visible="false"/>
<FRFont name="微软雅黑" style="0" size="88" foreground="-10066330"/>
</Legend>
<Attr4VanChart floating="false" x="0.0" y="0.0" layout="aligned" customSize="false" maxHeight="30.0" isHighlight="true"/>
</Legend4VanChart>
<DataSheet>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<Attr isVisible="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##]]></Format>
</DataSheet>
<NameJavaScriptGroup>
<NameJavaScript name="JavaScript1">
<JavaScript class="com.fr.js.JavaScriptImpl">
<Parameters>
<Parameter>
<Attributes name="p1"/>
<O t="XMLable" class="com.fr.base.Formula">
<Attributes>
<![CDATA[=CATEGORY]]></Attributes>
</O>
</Parameter>
</Parameters>
<Content>
<![CDATA[//获取场景切换地图chart1
var chartBridge = Van.Utils.getChartBridge({name:'chart1'});
//切换到场景名称为参数p1的场景
chartBridge.switchScene({name:p1});]]></Content>
</JavaScript>
</NameJavaScript>
</NameJavaScriptGroup>
<DataProcessor class="com.fr.base.chart.chartdata.model.NormalDataModel"/>
<newPlotFillStyle>
<AttrFillStyle>
<AFStyle colorStyle="0"/>
<FillStyleName fillStyleName=""/>
<isCustomFillStyle isCustomFillStyle="false"/>
</AttrFillStyle>
</newPlotFillStyle>
<VanChartPlotAttr isAxisRotation="false" categoryNum="1"/>
<GradientStyle>
<Attr gradientType="gradual" startColor="-12146441" endColor="-9378161"/>
</GradientStyle>
<VanChartRectanglePlotAttr vanChartPlotType="normal" isDefaultIntervalBackground="true"/>
<XAxisList>
<VanChartAxis class="com.fr.plugin.chart.attr.axis.VanChartAxis">
<Title>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<O>
<![CDATA[]]></O>
<TextAttr>
<Attr alignText="0">
<FRFont name="Verdana" style="0" size="88" foreground="-10066330"/>
</Attr>
</TextAttr>
<TitleVisible value="true" position="0"/>
</Title>
<newAxisAttr isShowAxisLabel="true"/>
<AxisLineStyle AxisStyle="1" MainGridStyle="1"/>
<newLineColor lineColor="-5197648"/>
<AxisPosition value="3"/>
<TickLine201106 type="2" secType="0"/>
<ArrowShow arrowShow="false"/>
<TextAttr>
<Attr alignText="0">
<FRFont name="Verdana" style="0" size="88" foreground="-1"/>
</Attr>
</TextAttr>
<AxisLabelCount value="=1"/>
<AxisRange/>
<AxisUnit201106 isCustomMainUnit="false" isCustomSecUnit="false" mainUnit="=0" secUnit="=0"/>
<ZoomAxisAttr isZoom="false"/>
<axisReversed axisReversed="false"/>
<VanChartAxisAttr mainTickLine="2" secTickLine="0" axisName="X轴" titleUseHtml="false" autoLabelGap="true" limitSize="false" maxHeight="15.0" commonValueFormat="true" isRotation="false" isShowAxisTitle="false" gridLineType="NONE"/>
<HtmlLabel customText="function(){ return this; }" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
<alertList/>
<customBackgroundList/>
</VanChartAxis>
</XAxisList>
<YAxisList>
<VanChartAxis class="com.fr.plugin.chart.attr.axis.VanChartValueAxis">
<Title>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<O>
<![CDATA[]]></O>
<TextAttr>
<Attr rotation="-90" alignText="0">
<FRFont name="Verdana" style="0" size="88" foreground="-10066330"/>
</Attr>
</TextAttr>
<TitleVisible value="true" position="0"/>
</Title>
<newAxisAttr isShowAxisLabel="true"/>
<AxisLineStyle AxisStyle="0" MainGridStyle="1"/>
<newLineColor mainGridColor="-3881788" lineColor="-5197648"/>
<AxisPosition value="2"/>
<TickLine201106 type="2" secType="0"/>
<ArrowShow arrowShow="false"/>
<TextAttr>
<Attr alignText="0">
<FRFont name="Verdana" style="0" size="88" foreground="-10066330"/>
</Attr>
</TextAttr>
<AxisLabelCount value="=1"/>
<AxisRange/>
<AxisUnit201106 isCustomMainUnit="false" isCustomSecUnit="false" mainUnit="=0" secUnit="=0"/>
<ZoomAxisAttr isZoom="false"/>
<axisReversed axisReversed="false"/>
<VanChartAxisAttr mainTickLine="0" secTickLine="0" axisName="Y轴" titleUseHtml="false" autoLabelGap="true" limitSize="false" maxHeight="15.0" commonValueFormat="true" isRotation="false" isShowAxisTitle="false" gridLineType="solid"/>
<HtmlLabel customText="function(){ return this; }" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
<alertList/>
<customBackgroundList/>
<VanChartValueAxisAttr isLog="false" valueStyle="false" baseLog="=10"/>
<ds>
<RadarYAxisTableDefinition>
<Top topCate="-1" topValue="-1" isDiscardOtherCate="false" isDiscardOtherSeries="false" isDiscardNullCate="false" isDiscardNullSeries="false"/>
<attr/>
</RadarYAxisTableDefinition>
</ds>
</VanChartAxis>
</YAxisList>
<stackAndAxisCondition>
<ConditionCollection>
<DefaultAttr class="com.fr.chart.chartglyph.ConditionAttr">
<ConditionAttr name=""/>
</DefaultAttr>
</ConditionCollection>
</stackAndAxisCondition>
<VanChartColumnPlotAttr seriesOverlapPercent="20.0" categoryIntervalPercent="20.0" fixedWidth="false" columnWidth="0" filledWithImage="false" isBar="false"/>
</Plot>
<ChartDefinition>
<MoreNameCDDefinition>
<Top topCate="-1" topValue="-1" isDiscardOtherCate="false" isDiscardOtherSeries="false" isDiscardNullCate="false" isDiscardNullSeries="false"/>
<TableData class="com.fr.data.impl.NameTableData">
<Name>
<![CDATA[长三角各省份销量数据]]></Name>
</TableData>
<CategoryName value="省份"/>
<ChartSummaryColumn name="销量" function="com.fr.data.util.function.NoneFunction" customName="销量"/>
</MoreNameCDDefinition>
</ChartDefinition>
</Chart>
<UUID uuid="c9878f5f-629a-4eb6-bcbe-4bae4863fb85"/>
<tools hidden="true" sort="false" export="false" fullScreen="false"/>
<VanChartZoom>
<zoomAttr zoomVisible="false" zoomGesture="true" zoomResize="true" zoomType="xy"/>
<from>
<![CDATA[]]></from>
<to>
<![CDATA[]]></to>
</VanChartZoom>
<refreshMoreLabel>
<attr moreLabel="false" autoTooltip="true"/>
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="false"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0"/>
</TextAttr>
<value class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="true"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat">
<AttrToolTipCategoryFormat>
<Attr enable="false"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
<params>
<![CDATA[{}]]></params>
</AttrTooltipRichText>
</richText>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-1"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="4"/>
<newColor borderColor="-15395563"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.8"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</refreshMoreLabel>
</Chart>
<ChartMobileAttrProvider zoomOut="0" zoomIn="2" allowFullScreen="true" functionalWhenUnactivated="false"/>
<MobileChartCollapsedStyle class="com.fr.form.ui.mobile.MobileChartCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
</MobileChartCollapsedStyle>
</InnerWidget>
<BoundsAttr x="0" y="0" width="295" height="540"/>
</Widget>
<ShowBookmarks showBookmarks="false"/>
<body class="com.fr.form.ui.ChartEditor">
<WidgetName name="chart0"/>
<WidgetID widgetID="94ad46a3-7f3e-4e17-aabe-06f268b20629"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LayoutAttr selectedIndex="0"/>
<ChangeAttr enable="false" changeType="button" timeInterval="5" buttonColor="-8421505" carouselColor="-8421505" showArrow="true">
<TextAttr>
<Attr alignText="0"/>
</TextAttr>
</ChangeAttr>
<Chart name="默认" chartClass="com.fr.plugin.chart.vanchart.VanChart">
<Chart class="com.fr.plugin.chart.vanchart.VanChart">
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-1118482"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<ChartAttr isJSDraw="true" isStyleGlobal="false"/>
<Title4VanChart>
<Title>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-6908266"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<O>
<![CDATA[新建图表标题]]></O>
<TextAttr>
<Attr alignText="0">
<FRFont name="Microsoft YaHei" style="0" size="128" foreground="-13421773"/>
</Attr>
</TextAttr>
<TitleVisible value="true" position="0"/>
</Title>
<Attr4VanChart useHtml="false" floating="false" x="0.0" y="0.0" limitSize="false" maxHeight="15.0"/>
</Title4VanChart>
<Plot class="com.fr.plugin.chart.column.VanChartColumnPlot">
<VanChartPlotVersion version="20170715"/>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<Attr isNullValueBreak="true" autoRefreshPerSecond="6" seriesDragEnable="false" plotStyle="0" combinedSize="50.0"/>
<newHotTooltipStyle>
<AttrContents>
<Attr showLine="false" position="1" isWhiteBackground="true" isShowMutiSeries="false" seriesLabel="${VALUE}"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##]]></Format>
<PercentFormat>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#0.##%]]></Format>
</PercentFormat>
</AttrContents>
</newHotTooltipStyle>
<ConditionCollection>
<DefaultAttr class="com.fr.chart.chartglyph.ConditionAttr">
<ConditionAttr name="">
<AttrList>
<Attr class="com.fr.plugin.chart.base.AttrTooltip">
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="false"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0"/>
</TextAttr>
<value class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
<params>
<![CDATA[{}]]></params>
</AttrTooltipRichText>
</richText>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-16777216"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="true"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="2"/>
<newColor borderColor="-16777216"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.5"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</Attr>
<Attr class="com.fr.chart.base.AttrBorder">
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-1"/>
</AttrBorder>
</Attr>
<Attr class="com.fr.chart.base.AttrAlpha">
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</Attr>
</AttrList>
</ConditionAttr>
</DefaultAttr>
</ConditionCollection>
<Legend4VanChart>
<Legend>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-3355444"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<Attr position="4" visible="true"/>
<FRFont name="Microsoft YaHei" style="0" size="88" foreground="-10066330"/>
</Legend>
<Attr4VanChart floating="false" x="0.0" y="0.0" layout="aligned" customSize="false" maxHeight="30.0" isHighlight="true"/>
</Legend4VanChart>
<DataSheet>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<Attr isVisible="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##]]></Format>
</DataSheet>
<DataProcessor class="com.fr.base.chart.chartdata.model.NormalDataModel"/>
<newPlotFillStyle>
<AttrFillStyle>
<AFStyle colorStyle="0"/>
<FillStyleName fillStyleName=""/>
<isCustomFillStyle isCustomFillStyle="false"/>
</AttrFillStyle>
</newPlotFillStyle>
<VanChartPlotAttr isAxisRotation="false" categoryNum="1"/>
<GradientStyle>
<Attr gradientType="gradual" startColor="-12146441" endColor="-9378161"/>
</GradientStyle>
<VanChartRectanglePlotAttr vanChartPlotType="normal" isDefaultIntervalBackground="true"/>
<XAxisList>
<VanChartAxis class="com.fr.plugin.chart.attr.axis.VanChartAxis">
<Title>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<O>
<![CDATA[]]></O>
<TextAttr>
<Attr alignText="0">
<FRFont name="verdana" style="0" size="88" foreground="-10066330"/>
</Attr>
</TextAttr>
<TitleVisible value="true" position="0"/>
</Title>
<newAxisAttr isShowAxisLabel="true"/>
<AxisLineStyle AxisStyle="1" MainGridStyle="1"/>
<newLineColor lineColor="-5197648"/>
<AxisPosition value="3"/>
<TickLine201106 type="2" secType="0"/>
<ArrowShow arrowShow="false"/>
<TextAttr>
<Attr alignText="0">
<FRFont name="verdana" style="0" size="88" foreground="-10066330"/>
</Attr>
</TextAttr>
<AxisLabelCount value="=1"/>
<AxisRange/>
<AxisUnit201106 isCustomMainUnit="false" isCustomSecUnit="false" mainUnit="=0" secUnit="=0"/>
<ZoomAxisAttr isZoom="false"/>
<axisReversed axisReversed="false"/>
<VanChartAxisAttr mainTickLine="2" secTickLine="0" axisName="X轴" titleUseHtml="false" autoLabelGap="true" limitSize="false" maxHeight="15.0" commonValueFormat="true" isRotation="false" isShowAxisTitle="false" gridLineType="NONE"/>
<HtmlLabel customText="function(){ return this; }" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
<alertList/>
<customBackgroundList/>
</VanChartAxis>
</XAxisList>
<YAxisList>
<VanChartAxis class="com.fr.plugin.chart.attr.axis.VanChartValueAxis">
<Title>
<GI>
<AttrBackground>
<Background name="NullBackground"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="0" isRoundBorder="false" roundRadius="0"/>
<newColor borderColor="-16777216"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="1.0"/>
</AttrAlpha>
</GI>
<O>
<![CDATA[]]></O>
<TextAttr>
<Attr rotation="-90" alignText="0">
<FRFont name="verdana" style="0" size="88" foreground="-10066330"/>
</Attr>
</TextAttr>
<TitleVisible value="true" position="0"/>
</Title>
<newAxisAttr isShowAxisLabel="true"/>
<AxisLineStyle AxisStyle="0" MainGridStyle="1"/>
<newLineColor mainGridColor="-3881788" lineColor="-5197648"/>
<AxisPosition value="2"/>
<TickLine201106 type="2" secType="0"/>
<ArrowShow arrowShow="false"/>
<TextAttr>
<Attr alignText="0">
<FRFont name="verdana" style="0" size="88" foreground="-10066330"/>
</Attr>
</TextAttr>
<AxisLabelCount value="=1"/>
<AxisRange/>
<AxisUnit201106 isCustomMainUnit="false" isCustomSecUnit="false" mainUnit="=0" secUnit="=0"/>
<ZoomAxisAttr isZoom="false"/>
<axisReversed axisReversed="false"/>
<VanChartAxisAttr mainTickLine="0" secTickLine="0" axisName="Y轴" titleUseHtml="false" autoLabelGap="true" limitSize="false" maxHeight="15.0" commonValueFormat="true" isRotation="false" isShowAxisTitle="false" gridLineType="solid"/>
<HtmlLabel customText="function(){ return this; }" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
<alertList/>
<customBackgroundList/>
<VanChartValueAxisAttr isLog="false" valueStyle="false" baseLog="=10"/>
<ds>
<RadarYAxisTableDefinition>
<Top topCate="-1" topValue="-1" isDiscardOtherCate="false" isDiscardOtherSeries="false" isDiscardNullCate="false" isDiscardNullSeries="false"/>
<attr/>
</RadarYAxisTableDefinition>
</ds>
</VanChartAxis>
</YAxisList>
<stackAndAxisCondition>
<ConditionCollection>
<DefaultAttr class="com.fr.chart.chartglyph.ConditionAttr">
<ConditionAttr name=""/>
</DefaultAttr>
</ConditionCollection>
</stackAndAxisCondition>
<VanChartColumnPlotAttr seriesOverlapPercent="20.0" categoryIntervalPercent="20.0" fixedWidth="false" columnWidth="0" filledWithImage="false" isBar="false"/>
</Plot>
<ChartDefinition>
<MoreNameCDDefinition>
<Top topCate="-1" topValue="-1" isDiscardOtherCate="false" isDiscardOtherSeries="false" isDiscardNullCate="false" isDiscardNullSeries="false"/>
<TableData class="com.fr.data.impl.NameTableData">
<Name>
<![CDATA[长三角各省份销量数据]]></Name>
</TableData>
<CategoryName value="省份"/>
<ChartSummaryColumn name="销量" function="com.fr.data.util.function.NoneFunction" customName="销量"/>
</MoreNameCDDefinition>
</ChartDefinition>
</Chart>
<UUID uuid="2e311c06-3024-42f3-83f4-892ab1cbf378"/>
<tools hidden="true" sort="false" export="false" fullScreen="false"/>
<VanChartZoom>
<zoomAttr zoomVisible="false" zoomGesture="true" zoomResize="true" zoomType="xy"/>
<from>
<![CDATA[]]></from>
<to>
<![CDATA[]]></to>
</VanChartZoom>
<refreshMoreLabel>
<attr moreLabel="false" autoTooltip="true"/>
<AttrTooltip>
<Attr enable="true" duration="4" followMouse="false" showMutiSeries="false"/>
<AttrToolTipContent>
<Attr isCommon="true" isCustom="false" isRichText="false" richTextAlign="left"/>
<TextAttr>
<Attr alignText="0"/>
</TextAttr>
<value class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</value>
<percent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</percent>
<category class="com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat">
<AttrToolTipCategoryFormat>
<Attr enable="true"/>
</AttrToolTipCategoryFormat>
</category>
<series class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="true"/>
</AttrTooltipSeriesFormat>
</series>
<changedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</changedPercent>
<changedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="true"/>
</AttrTooltipChangedValueFormat>
</changedValue>
<richTextValue class="com.fr.plugin.chart.base.format.AttrTooltipValueFormat">
<AttrTooltipValueFormat>
<Attr enable="true"/>
</AttrTooltipValueFormat>
</richTextValue>
<richTextPercent class="com.fr.plugin.chart.base.format.AttrTooltipPercentFormat">
<AttrTooltipPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipPercentFormat>
</richTextPercent>
<richTextCategory class="com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat">
<AttrToolTipCategoryFormat>
<Attr enable="false"/>
</AttrToolTipCategoryFormat>
</richTextCategory>
<richTextSeries class="com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat">
<AttrTooltipSeriesFormat>
<Attr enable="false"/>
</AttrTooltipSeriesFormat>
</richTextSeries>
<richTextChangedPercent class="com.fr.plugin.chart.base.format.AttrTooltipChangedPercentFormat">
<AttrTooltipChangedPercentFormat>
<Attr enable="false"/>
<Format class="com.fr.base.CoreDecimalFormat" roundingMode="6">
<![CDATA[#.##%]]></Format>
</AttrTooltipChangedPercentFormat>
</richTextChangedPercent>
<richTextChangedValue class="com.fr.plugin.chart.base.format.AttrTooltipChangedValueFormat">
<AttrTooltipChangedValueFormat>
<Attr enable="false"/>
</AttrTooltipChangedValueFormat>
</richTextChangedValue>
<richText class="com.fr.plugin.chart.base.AttrTooltipRichText">
<AttrTooltipRichText>
<Attr content="" isAuto="true" initParamsContent=""/>
<params>
<![CDATA[{}]]></params>
</AttrTooltipRichText>
</richText>
<HtmlLabel customText="" useHtml="false" isCustomWidth="false" isCustomHeight="false" width="50" height="50"/>
</AttrToolTipContent>
<GI>
<AttrBackground>
<Background name="ColorBackground" color="-1"/>
<Attr gradientType="normal" gradientStartColor="-12146441" gradientEndColor="-9378161" shadow="false"/>
</AttrBackground>
<AttrBorder>
<Attr lineStyle="1" isRoundBorder="false" roundRadius="4"/>
<newColor borderColor="-15395563"/>
</AttrBorder>
<AttrAlpha>
<Attr alpha="0.8"/>
</AttrAlpha>
</GI>
</AttrTooltip>
</refreshMoreLabel>
</Chart>
<ChartMobileAttrProvider zoomOut="0" zoomIn="2" allowFullScreen="true" functionalWhenUnactivated="false"/>
<MobileChartCollapsedStyle class="com.fr.form.ui.mobile.MobileChartCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
</MobileChartCollapsedStyle>
</body>
</InnerWidget>
<BoundsAttr x="0" y="0" width="182" height="444"/>
</Widget>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.container.WTitleLayout">
<WidgetName name="chart1"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="chart1" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LCAttr vgap="0" hgap="0" compInterval="0"/>
<Widget class="com.fr.form.ui.container.WAbsoluteLayout$BoundsWidget">
<InnerWidget class="com.fr.form.ui.ChartEditor">
<WidgetName name="chart1"/>
<WidgetID widgetID="ac26633f-6872-4b02-a1f7-c4d48c607986"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LayoutAttr selectedIndex="0"/>
<ChangeAttr enable="false" changeType="button" timeInterval="5" buttonColor="-8421505" carouselColor="-8421505" showArrow="true">
<TextAttr>
<Attr alignText="0"/>
</TextAttr>
</ChangeAttr>
<Chart name="默认" chartClass="com.fr.plugin.chart.u3d.geoland.UGeoLandChart">
<attr refreshEnabled="false" refreshTime="10.0"/>
<SceneList>
<SingleScene name="上海市">
<UGeoLandScene geourl="assets/map/geographic/world/中国/上海市.json" simplifyModel="true" turnInterval="6.0" callLink="true" openTurn="false" auto="false" number="60.0">
<GroupDataDefinition>
<dataMap>
<SingleDefinition key="area">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.AreaGeoLandDataDefinition" pluginID="com.fr.plugin.bigScreen.v10" fromBottomData="true" drill="false">
<GroupDataDefinition>
<dataMap>
<SingleDefinition key="normal">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="bottom">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="0">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
</dataMap>
</GroupDataDefinition>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="point">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.PointMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<lng type="ColumnField">
<ColumnField>
<attr/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lng>
<areaName type="ColumnField">
<ColumnField>
<attr fieldName="城市"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<lat type="ColumnField">
<ColumnField>
<attr/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lat>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="true"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="销量"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="销量" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
<TableData class="com.fr.data.impl.NameTableData">
<Name>
<![CDATA[城市销量数据]]></Name>
</TableData>
</dataDefinition>
<matchResult>
<CustomResult>
<ResultMap key="上海市" value="金山区"/>
</CustomResult>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="line">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.LineMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<fromAreaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromAreaName>
<toLat type="ColumnField">
<ColumnField>
<attr/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toLat>
<fromLat type="ColumnField">
<ColumnField>
<attr/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromLat>
<toAreaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toAreaName>
<fromLng type="ColumnField">
<ColumnField>
<attr/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromLng>
<lineName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lineName>
<toLng type="ColumnField">
<ColumnField>
<attr/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toLng>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="cylinder">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.PointMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<lng type="ColumnField">
<ColumnField>
<attr/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lng>
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<lat type="ColumnField">
<ColumnField>
<attr/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lat>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
</dataMap>
</GroupDataDefinition>
<GeoLandStyle theme="0">
<PointGraphic type="1" bottomCircles="true" radiusScale="1.3000000000000003" auto="true" color="-6750208">
<gradientColor auto="true" startColor="-1" endColor="-16777216"/>
<GeoLandLabel fontScaleSize="3.500000000000002" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</PointGraphic>
<LineGraphic alpha="20.0" toCircles="true" fromCircles="true" auto="true" color="-9176074">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</LineGraphic>
<CylinderGraphic bottomCircles="true" widthScale="1.0" heightScale="1.0" auto="true" startColor="-14929473" endColor="-4251969">
<GeoLandLabel fontScaleSize="3.0000000000000018" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</CylinderGraphic>
<AreaGraphic>
<fill auto="true" color="-10971649"/>
<border auto="true" color="-16589569"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</AreaGraphic>
<GeoLandBackground particles="true" meteor="true" gear="true" flowEffect="true" backLand="true" skyBox="0"/>
</GeoLandStyle>
<PositionAndRotation>
<position x="96.91002" y="53.824333" z="13.357698"/>
<rotation x="61.500294" y="39.000206" z="1.32E-4"/>
</PositionAndRotation>
<pointhyper/>
<linehyper/>
<cylinderhyper/>
<areahyper/>
<drillhyper/>
</UGeoLandScene>
</SingleScene>
<SingleScene name="江苏省">
<UGeoLandScene geourl="assets/map/geographic/world/中国/江苏省.json" simplifyModel="true" turnInterval="6.0" callLink="true" openTurn="false" auto="false" number="60.0">
<GroupDataDefinition>
<dataMap>
<SingleDefinition key="area">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.AreaGeoLandDataDefinition" pluginID="com.fr.plugin.bigScreen.v10" fromBottomData="true" drill="false">
<GroupDataDefinition>
<dataMap>
<SingleDefinition key="normal">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="1">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="bottom">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="0">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
</dataMap>
</GroupDataDefinition>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="point">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.PointMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<lng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lng>
<areaName type="ColumnField">
<ColumnField>
<attr fieldName="城市"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<lat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lat>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="true"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="销量"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="销量" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
<TableData class="com.fr.data.impl.NameTableData">
<Name>
<![CDATA[城市销量数据]]></Name>
</TableData>
</dataDefinition>
<matchResult>
<CustomResult>
<ResultMap key="江苏省" value="南京市"/>
</CustomResult>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="line">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.LineMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<fromAreaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromAreaName>
<toLat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toLat>
<fromLat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromLat>
<toAreaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toAreaName>
<fromLng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromLng>
<lineName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lineName>
<toLng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toLng>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="cylinder">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.PointMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<lng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lng>
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<lat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lat>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
</dataMap>
</GroupDataDefinition>
<GeoLandStyle theme="0">
<PointGraphic type="1" bottomCircles="true" radiusScale="1.3000000000000003" auto="true" color="-6750208">
<gradientColor auto="true" startColor="-1" endColor="-16777216"/>
<GeoLandLabel fontScaleSize="3.500000000000002" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</PointGraphic>
<LineGraphic alpha="20.0" toCircles="true" fromCircles="true" auto="true" color="-9176074">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</LineGraphic>
<CylinderGraphic bottomCircles="true" widthScale="1.0" heightScale="1.0" auto="true" startColor="-14929473" endColor="-4251969">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</CylinderGraphic>
<AreaGraphic>
<fill auto="true" color="-10971649"/>
<border auto="true" color="-16589569"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</AreaGraphic>
<GeoLandBackground particles="true" meteor="true" gear="true" flowEffect="true" backLand="true" skyBox="0"/>
</GeoLandStyle>
<PositionAndRotation>
<position x="71.91771" y="42.545235" z="31.990883"/>
<rotation x="44.000034" y="69.50002" z="1.9E-5"/>
</PositionAndRotation>
</UGeoLandScene>
</SingleScene>
<SingleScene name="浙江省">
<UGeoLandScene geourl="assets/map/geographic/world/中国/浙江省.json" simplifyModel="true" turnInterval="6.0" callLink="true" openTurn="false" auto="false" number="60.0">
<GroupDataDefinition>
<dataMap>
<SingleDefinition key="area">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.AreaGeoLandDataDefinition" pluginID="com.fr.plugin.bigScreen.v10" fromBottomData="true" drill="false">
<GroupDataDefinition>
<dataMap>
<SingleDefinition key="normal">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="1">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="bottom">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="0">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
</dataMap>
</GroupDataDefinition>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="point">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.PointMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<lng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lng>
<areaName type="ColumnField">
<ColumnField>
<attr fieldName="城市"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<lat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lat>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="true"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="销量"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="销量" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
<TableData class="com.fr.data.impl.NameTableData">
<Name>
<![CDATA[城市销量数据]]></Name>
</TableData>
</dataDefinition>
<matchResult>
<CustomResult>
<ResultMap key="浙江省" value="杭州市"/>
</CustomResult>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="line">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.LineMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<fromAreaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromAreaName>
<toLat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toLat>
<fromLat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromLat>
<toAreaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toAreaName>
<fromLng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromLng>
<lineName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lineName>
<toLng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toLng>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="cylinder">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.PointMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<lng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lng>
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<lat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lat>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
</dataMap>
</GroupDataDefinition>
<GeoLandStyle theme="0">
<PointGraphic type="1" bottomCircles="true" radiusScale="1.3000000000000003" auto="true" color="-6750208">
<gradientColor auto="true" startColor="-1" endColor="-16777216"/>
<GeoLandLabel fontScaleSize="3.500000000000002" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</PointGraphic>
<LineGraphic alpha="20.0" toCircles="true" fromCircles="true" auto="true" color="-9176074">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</LineGraphic>
<CylinderGraphic bottomCircles="true" widthScale="1.0" heightScale="1.0" auto="true" startColor="-14929473" endColor="-4251969">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</CylinderGraphic>
<AreaGraphic>
<fill auto="true" color="-10971649"/>
<border auto="true" color="-16589569"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</AreaGraphic>
<GeoLandBackground particles="true" meteor="true" gear="true" flowEffect="true" backLand="true" skyBox="0"/>
</GeoLandStyle>
<PositionAndRotation>
<position x="95.048195" y="44.079735" z="85.94821"/>
<rotation x="42.500042" y="150.999908" z="-3.9E-5"/>
</PositionAndRotation>
</UGeoLandScene>
</SingleScene>
<SingleScene name="安徽省">
<UGeoLandScene geourl="assets/map/geographic/world/中国/安徽省.json" simplifyModel="true" turnInterval="6.0" callLink="true" openTurn="false" auto="false" number="60.0">
<GroupDataDefinition>
<dataMap>
<SingleDefinition key="area">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.AreaGeoLandDataDefinition" pluginID="com.fr.plugin.bigScreen.v10" fromBottomData="true" drill="false">
<GroupDataDefinition>
<dataMap>
<SingleDefinition key="normal">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="1">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="bottom">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="0">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.AreaMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10">
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
</dataMap>
</GroupDataDefinition>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="point">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.PointMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<lng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lng>
<areaName type="ColumnField">
<ColumnField>
<attr fieldName="城市"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<lat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lat>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="true"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="销量"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="销量" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
<TableData class="com.fr.data.impl.NameTableData">
<Name>
<![CDATA[城市销量数据]]></Name>
</TableData>
</dataDefinition>
<matchResult>
<CustomResult>
<ResultMap key="安徽省" value="合肥市"/>
</CustomResult>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="line">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.LineMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<fromAreaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromAreaName>
<toLat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toLat>
<fromLat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromLat>
<toAreaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toAreaName>
<fromLng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</fromLng>
<lineName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lineName>
<toLng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</toLng>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
<SingleDefinition key="cylinder">
<DataDefinition class="com.fr.plugin.chart.u3d.geoland.data.DataSetDefinitionWithMapMatch" pluginID="com.fr.plugin.bigScreen.v10">
<dataDefinition>
<AbstractColumnFieldCollection class="com.fr.plugin.chart.u3d.geoland.data.compatible.PointMapColumnFieldCollection" pluginID="com.fr.plugin.bigScreen.v10" useAreaName="true">
<lng type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lng>
<areaName type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</areaName>
<lat type="ColumnField">
<ColumnField>
<attr fieldName=""/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</lat>
<SeriesValueCorrelationDefinition>
<attr customFieldValue="false"/>
<seriesValueFieldList>
<SeriesValueField>
<series>
<ColumnField>
<attr fieldName="无"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</series>
<value>
<ColumnField>
<attr fieldName="" function="com.fr.data.util.function.NoneFunction"/>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</ColumnField>
</value>
</SeriesValueField>
</seriesValueFieldList>
<DataFilterProperties>
<attr useTop="false" top="0" merge="false" hideNull="false"/>
</DataFilterProperties>
</SeriesValueCorrelationDefinition>
</AbstractColumnFieldCollection>
</dataDefinition>
<matchResult>
<CustomResult/>
</matchResult>
</DataDefinition>
</SingleDefinition>
</dataMap>
</GroupDataDefinition>
<GeoLandStyle theme="0">
<PointGraphic type="1" bottomCircles="true" radiusScale="1.3000000000000003" auto="true" color="-6750208">
<gradientColor auto="true" startColor="-1" endColor="-16777216"/>
<GeoLandLabel fontScaleSize="3.500000000000002" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</PointGraphic>
<LineGraphic alpha="20.0" toCircles="true" fromCircles="true" auto="true" color="-9176074">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</LineGraphic>
<CylinderGraphic bottomCircles="true" widthScale="1.0" heightScale="1.0" auto="true" startColor="-14929473" endColor="-4251969">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</CylinderGraphic>
<AreaGraphic>
<fill auto="true" color="-10971649"/>
<border auto="true" color="-16589569"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</AreaGraphic>
<GeoLandBackground particles="true" meteor="true" gear="true" flowEffect="true" backLand="true" skyBox="0"/>
</GeoLandStyle>
<PositionAndRotation>
<position x="124.059265" y="44.97686" z="7.753414"/>
<rotation x="54.49999" y="343.5" z="1.2E-5"/>
</PositionAndRotation>
</UGeoLandScene>
</SingleScene>
</SceneList>
<SwitchSceneAttr>
<buttonOrCarouselInterval auto="true" number="30.0"/>
</SwitchSceneAttr>
</Chart>
<ChartMobileAttrProvider zoomOut="0" zoomIn="2" allowFullScreen="true" functionalWhenUnactivated="false"/>
<MobileChartCollapsedStyle class="com.fr.form.ui.mobile.MobileChartCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
</MobileChartCollapsedStyle>
</InnerWidget>
<BoundsAttr x="295" y="0" width="665" height="540"/>
</Widget>
<ShowBookmarks showBookmarks="false"/>
<body class="com.fr.form.ui.ChartEditor">
<WidgetName name="chart1"/>
<WidgetID widgetID="ac26633f-6872-4b02-a1f7-c4d48c607986"/>
<WidgetAttr description="">
<MobileBookMark useBookMark="false" bookMarkName="" frozen="false"/>
<PrivilegeControl/>
</WidgetAttr>
<Margin top="0" left="0" bottom="0" right="0"/>
<Border>
<border style="0" color="-723724" borderRadius="0" type="0" borderStyle="0"/>
<WidgetTitle>
<O>
<![CDATA[新建标题]]></O>
<FRFont name="SimSun" style="0" size="72"/>
<Position pos="0"/>
</WidgetTitle>
<Alpha alpha="1.0"/>
</Border>
<LayoutAttr selectedIndex="0"/>
<ChangeAttr enable="false" changeType="button" timeInterval="5" buttonColor="-8421505" carouselColor="-8421505" showArrow="true">
<TextAttr>
<Attr alignText="0"/>
</TextAttr>
</ChangeAttr>
<Chart name="默认" chartClass="com.fr.plugin.chart.u3d.geoland.UGeoLandChart">
<attr refreshEnabled="false" refreshTime="10.0"/>
<SceneList>
<SingleScene name="场景1">
<UGeoLandScene geourl="assets/map/geographic/world/中国.json" simplifyModel="true" turnInterval="6.0" callLink="true" openTurn="false" auto="false" number="60.0">
<GeoLandStyle theme="0">
<PointGraphic type="1" bottomCircles="true" radiusScale="1.0" auto="true" color="-6750208">
<gradientColor auto="true" startColor="-1" endColor="-16777216"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</PointGraphic>
<LineGraphic alpha="20.0" toCircles="true" fromCircles="true" auto="true" color="-9176074">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</LineGraphic>
<CylinderGraphic bottomCircles="true" widthScale="1.0" heightScale="1.0" auto="true" startColor="-14929473" endColor="-4251969">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</CylinderGraphic>
<AreaGraphic>
<fill auto="true" color="-10971649"/>
<border auto="true" color="-16589569"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</AreaGraphic>
<GeoLandBackground particles="true" meteor="true" gear="true" flowEffect="true" backLand="true" skyBox="0"/>
</GeoLandStyle>
<PositionAndRotation>
<position x="123.0" y="29.0" z="-7.0"/>
<rotation x="33.0" y="351.0" z="0.0"/>
</PositionAndRotation>
</UGeoLandScene>
</SingleScene>
<SingleScene name="场景2">
<UGeoLandScene geourl="assets/map/geographic/world/中国.json" simplifyModel="true" turnInterval="6.0" callLink="true" openTurn="false" auto="false" number="60.0">
<GeoLandStyle theme="0">
<PointGraphic type="1" bottomCircles="true" radiusScale="1.0" auto="true" color="-6750208">
<gradientColor auto="true" startColor="-1" endColor="-16777216"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</PointGraphic>
<LineGraphic alpha="20.0" toCircles="true" fromCircles="true" auto="true" color="-9176074">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</LineGraphic>
<CylinderGraphic bottomCircles="true" widthScale="1.0" heightScale="1.0" auto="true" startColor="-14929473" endColor="-4251969">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</CylinderGraphic>
<AreaGraphic>
<fill auto="true" color="-10971649"/>
<border auto="true" color="-16589569"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</AreaGraphic>
<GeoLandBackground particles="true" meteor="true" gear="true" flowEffect="true" backLand="true" skyBox="0"/>
</GeoLandStyle>
<PositionAndRotation>
<position x="123.0" y="29.0" z="-7.0"/>
<rotation x="33.0" y="351.0" z="0.0"/>
</PositionAndRotation>
</UGeoLandScene>
</SingleScene>
<SingleScene name="场景3">
<UGeoLandScene geourl="assets/map/geographic/world/中国.json" simplifyModel="true" turnInterval="6.0" callLink="true" openTurn="false" auto="false" number="60.0">
<GeoLandStyle theme="0">
<PointGraphic type="1" bottomCircles="true" radiusScale="1.0" auto="true" color="-6750208">
<gradientColor auto="true" startColor="-1" endColor="-16777216"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</PointGraphic>
<LineGraphic alpha="20.0" toCircles="true" fromCircles="true" auto="true" color="-9176074">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</LineGraphic>
<CylinderGraphic bottomCircles="true" widthScale="1.0" heightScale="1.0" auto="true" startColor="-14929473" endColor="-4251969">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</CylinderGraphic>
<AreaGraphic>
<fill auto="true" color="-10971649"/>
<border auto="true" color="-16589569"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</AreaGraphic>
<GeoLandBackground particles="true" meteor="true" gear="true" flowEffect="true" backLand="true" skyBox="0"/>
</GeoLandStyle>
<PositionAndRotation>
<position x="123.0" y="29.0" z="-7.0"/>
<rotation x="33.0" y="351.0" z="0.0"/>
</PositionAndRotation>
</UGeoLandScene>
</SingleScene>
<SingleScene name="场景4">
<UGeoLandScene geourl="assets/map/geographic/world/中国.json" simplifyModel="true" turnInterval="6.0" callLink="true" openTurn="false" auto="false" number="60.0">
<GeoLandStyle theme="0">
<PointGraphic type="1" bottomCircles="true" radiusScale="1.0" auto="true" color="-6750208">
<gradientColor auto="true" startColor="-1" endColor="-16777216"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</PointGraphic>
<LineGraphic alpha="20.0" toCircles="true" fromCircles="true" auto="true" color="-9176074">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</LineGraphic>
<CylinderGraphic bottomCircles="true" widthScale="1.0" heightScale="1.0" auto="true" startColor="-14929473" endColor="-4251969">
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</CylinderGraphic>
<AreaGraphic>
<fill auto="true" color="-10971649"/>
<border auto="true" color="-16589569"/>
<GeoLandLabel fontScaleSize="1.0" alwaysShow="true" fontColor="-1">
<LabelItems>
<itemList>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
<item>
<ETooltipItem>
<Attr enable="true"/>
</ETooltipItem>
</item>
</itemList>
</LabelItems>
</GeoLandLabel>
</AreaGraphic>
<GeoLandBackground particles="true" meteor="true" gear="true" flowEffect="true" backLand="true" skyBox="0"/>
</GeoLandStyle>
<PositionAndRotation>
<position x="123.0" y="29.0" z="-7.0"/>
<rotation x="33.0" y="351.0" z="0.0"/>
</PositionAndRotation>
</UGeoLandScene>
</SingleScene>
</SceneList>
<SwitchSceneAttr>
<buttonOrCarouselInterval auto="true" number="30.0"/>
</SwitchSceneAttr>
</Chart>
<ChartMobileAttrProvider zoomOut="0" zoomIn="2" allowFullScreen="true" functionalWhenUnactivated="false"/>
<MobileChartCollapsedStyle class="com.fr.form.ui.mobile.MobileChartCollapsedStyle">
<collapseButton showButton="true" color="-6710887" foldedHint="" unfoldedHint="" defaultState="0"/>
<collapsedWork value="false"/>
</MobileChartCollapsedStyle>
</body>
</InnerWidget>
<BoundsAttr x="182" y="0" width="408" height="444"/>
</Widget>
<ShowBookmarks showBookmarks="true"/>
<Sorted sorted="true"/>
<MobileWidgetList>
<Widget widgetName="chart1"/>
<Widget widgetName="chart0"/>
</MobileWidgetList>
<FrozenWidgets/>
<MobileBookMarkStyle class="com.fr.form.ui.mobile.impl.DefaultMobileBookMarkStyle"/>
<WidgetZoomAttr compState="0"/>
<AppRelayout appRelayout="true"/>
<Size width="590" height="444"/>
<ResolutionScalingAttr percent="1.2"/>
<BodyLayoutType type="0"/>
</Center>
</Layout>
<DesignerVersion DesignerVersion="KAA"/>
<PreviewType PreviewType="0"/>
<WatermarkAttr class="com.fr.base.iofile.attr.WatermarkAttr">
<WatermarkAttr fontSize="20" color="-6710887" horizontalGap="200" verticalGap="100" valid="false">
<Text>
<![CDATA[]]></Text>
</WatermarkAttr>
</WatermarkAttr>
<FileAttrErrorMarker class="com.fr.base.io.FileAttrErrorMarker" pluginID="com.fr.plugin.adaptive" oriClass="com.fr.plugin.adaptive.designer.form.NewFormMarkAttr">
<NewFormMarkAttr bodyHeight="540" type="0" bodyWidth="960"/>
</FileAttrErrorMarker>
<TemplateIdAttMark class="com.fr.base.iofile.attr.TemplateIdAttrMark">
<TemplateIdAttMark TemplateId="7c0c7c37-5693-470d-932c-daaf2838276e"/>
</TemplateIdAttMark>
</Form>
