/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.PspCusFinAndGuarDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspCusFinAnaly;
import cn.com.yusys.yusp.service.PspCusFinAnalyService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspCusFinAnalyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pspcusfinanaly")
public class PspCusFinAnalyResource {
    @Autowired
    private PspCusFinAnalyService pspCusFinAnalyService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspCusFinAnaly>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspCusFinAnaly> list = pspCusFinAnalyService.selectAll(queryModel);
        return new ResultDto<List<PspCusFinAnaly>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PspCusFinAnaly>> index(QueryModel queryModel) {
        List<PspCusFinAnaly> list = pspCusFinAnalyService.selectByModel(queryModel);
        return new ResultDto<List<PspCusFinAnaly>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @GetMapping("/queryList")
    protected ResultDto<List<PspCusFinAnaly>> queryList(QueryModel queryModel) {
        List<PspCusFinAnaly> list = pspCusFinAnalyService.selectByModel(queryModel);
        return new ResultDto<List<PspCusFinAnaly>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspCusFinAnaly> show(@PathVariable("pkId") String pkId) {
        PspCusFinAnaly pspCusFinAnaly = pspCusFinAnalyService.selectByPrimaryKey(pkId);
        return new ResultDto<PspCusFinAnaly>(pspCusFinAnaly);
    }
    /**
     * @函数名称:queryPspCusFinAnaly
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<PspCusFinAnaly> queryPspCusFinAnaly(@RequestBody PspCusFinAnaly pspCusFinAnaly) {
        pspCusFinAnaly = pspCusFinAnalyService.queryPspCusFinAnaly(pspCusFinAnaly);
        return new ResultDto<PspCusFinAnaly>(pspCusFinAnaly);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PspCusFinAnaly> create(@RequestBody PspCusFinAnaly pspCusFinAnaly) throws URISyntaxException {
        pspCusFinAnalyService.insert(pspCusFinAnaly);
        return new ResultDto<PspCusFinAnaly>(pspCusFinAnaly);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspCusFinAnaly pspCusFinAnaly) throws URISyntaxException {
        int result = pspCusFinAnalyService.update(pspCusFinAnaly);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspCusFinAnalyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspCusFinAnalyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addFinAndGuar")
    protected ResultDto<PspCusFinAndGuarDto> addFinAndGuar(@RequestBody PspCusFinAndGuarDto pspCusFinAndGuarDto) throws URISyntaxException {
        pspCusFinAnalyService.addFinAndGuar(pspCusFinAndGuarDto);
        return new ResultDto<PspCusFinAndGuarDto>(pspCusFinAndGuarDto);
    }

    /**
     * @函数名称:updateFinAndGuar
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateFinAndGuar")
    protected ResultDto<Integer> updateFinAndGuar(@RequestBody PspCusFinAndGuarDto pspCusFinAndGuarDto) throws URISyntaxException {
        int result = pspCusFinAnalyService.updateFinAndGuar(pspCusFinAndGuarDto);
        return new ResultDto<Integer>(result);
    }
}
