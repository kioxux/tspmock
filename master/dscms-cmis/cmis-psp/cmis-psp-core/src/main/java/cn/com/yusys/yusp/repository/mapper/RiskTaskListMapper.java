/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.PspTaskList;
import cn.com.yusys.yusp.dto.PspKHYJDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.RiskTaskList;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskTaskListMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-25 03:38:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface RiskTaskListMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    RiskTaskList selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: queryRiskTaskList
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    RiskTaskList queryRiskTaskList(RiskTaskList riskTaskList);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<RiskTaskList> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(RiskTaskList record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(RiskTaskList record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(RiskTaskList record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(RiskTaskList record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据任务编号更新
     * @author jijian_yx
     * @date 2021/7/3 18:23
     **/
    int updateByTaskNoSelective(RiskTaskList record);


    /**
     * @方法名称: queryRiskTaskListByCusId
     * @方法描述: 根据客户编号查询风险分类信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    RiskTaskList queryRiskTaskListByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: queryRiskTaskListByCusId
     * @方法描述: 根据客户编号查询贷后检查信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    RiskTaskList queryPspCheckRstByCusId(@Param("cusId") String cusId);

    /**
     * 获取用户上次已分析的分类信息
     * @author jijian_yx
     * @date 2021/9/25 15:20
     **/
    RiskTaskList queryLastRiskTask(@Param("checkType")String checkType,@Param("cusId")String cusId);

    /**
     * 获取子表任务生成状态
     * @author jijian_yx
     * @date 2021/9/30 0:48
     **/
    String getTaskStatus(@Param("pkId") String pkId);

    /**
     * 更新子表数据生成状态
     * @author jijian_yx
     * @date 2021/9/30 1:04
     **/
    int updateTaskStatus(@Param("pkId") String pkId, @Param("taskStatus") String taskStatus);

    /**
     * 获取所有页签保存情况
     * @author jijian_yx
     * @date 2021/10/8 17:09
     **/
    String getSaveInfo(@Param("tableName") String tableName, @Param("taskNo") String taskNo);
    /**
     * 判断是否有在途任务
     * @param pspKHYJDto
     * @return
     */
    RiskTaskList queryCountBycusId(PspKHYJDto pspKHYJDto);
}