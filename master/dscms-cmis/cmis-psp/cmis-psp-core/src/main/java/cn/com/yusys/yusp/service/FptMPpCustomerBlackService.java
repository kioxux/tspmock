/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.FptMPpCustomerBlack;
import cn.com.yusys.yusp.repository.mapper.FptMPpCustomerBlackMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: FptMPpCustomerBlackService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-01 21:24:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class FptMPpCustomerBlackService {

    @Autowired
    private FptMPpCustomerBlackMapper fptMPpCustomerBlackMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public FptMPpCustomerBlack selectByPrimaryKey(String custNo, String custName, String remark, String inputDate) {
        return fptMPpCustomerBlackMapper.selectByPrimaryKey(custNo, custName, remark, inputDate);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<FptMPpCustomerBlack> selectAll(QueryModel model) {
        List<FptMPpCustomerBlack> records = (List<FptMPpCustomerBlack>) fptMPpCustomerBlackMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<FptMPpCustomerBlack> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<FptMPpCustomerBlack> list = fptMPpCustomerBlackMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(FptMPpCustomerBlack record) {
        return fptMPpCustomerBlackMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(FptMPpCustomerBlack record) {
        return fptMPpCustomerBlackMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(FptMPpCustomerBlack record) {
        return fptMPpCustomerBlackMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(FptMPpCustomerBlack record) {
        return fptMPpCustomerBlackMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String custNo, String custName, String remark, String inputDate) {
        return fptMPpCustomerBlackMapper.deleteByPrimaryKey(custNo, custName, remark, inputDate);
    }

    /**
     * @方法名称: selectBycusId
     * @方法描述: 根据客户号查询查询
     * @参数与返回说明:
     * @算法描述: 无
     * @修改人
     */
    public FptMPpCustomerBlack selectBycusId(String cusId) {
        QueryModel model = new QueryModel();
        model.addCondition("custNo",cusId);
        model.setSort("createTime desc");
        List<FptMPpCustomerBlack> list = selectAll(model);
        if(CollectionUtils.nonEmpty(list)) {
            return list.get(0);
        } else {
            return null;
        }
    }
}
