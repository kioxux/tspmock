/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckProperty
 * @类描述: psp_oper_status_check_property数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_oper_status_check_property")
public class PspOperStatusCheckProperty extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 贷款金额（元） **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 本次贷后检查时的出租率 **/
	@Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanBalance;
	
	/** 贷款余额（元） **/
	@Column(name = "RENT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rentRate;
	
	/** 年租金（万元） **/
	@Column(name = "RENT_AMT_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rentAmtYear;
	
	/** 贷款期限 **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 10)
	private Integer loanTerm;
	
	/** 我行账户当年回笼租金（万元） **/
	@Column(name = "RENT_AMT_BACK", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rentAmtBack;
	
	/** 租金归集账户是否为我行账户 **/
	@Column(name = "IS_ACCT", unique = false, nullable = true, length = 5)
	private String isAcct;
	
	/** 归集账户说明 **/
	@Column(name = "ACCT_EXPL", unique = false, nullable = true, length = 65535)
	private String acctExpl;
	
	/** 账号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 40)
	private String acctNo;
	
	/** 账户名称 **/
	@Column(name = "ACCT_NAME", unique = false, nullable = true, length = 80)
	private String acctName;
	
	/** 账户序号 **/
	@Column(name = "ACCT_SEQ_NO", unique = false, nullable = true, length = 40)
	private String acctSeqNo;
	
	/** 开户行 **/
	@Column(name = "ACCTB", unique = false, nullable = true, length = 40)
	private String acctb;
	
	/** 是否按我行还款计划按时足额还款 **/
	@Column(name = "IS_REPAY", unique = false, nullable = true, length = 5)
	private String isRepay;
	
	/** 按时还款说明 **/
	@Column(name = "REPAY_EXPL", unique = false, nullable = true, length = 65535)
	private String repayExpl;
	
	/** 是否收集全部承租户承诺书 **/
	@Column(name = "IS_SIGN", unique = false, nullable = true, length = 5)
	private String isSign;
	
	/** 承诺书收集说明 **/
	@Column(name = "SIGN_EXPL", unique = false, nullable = true, length = 65535)
	private String signExpl;
	
	/** 租户的租金是否按时支付 **/
	@Column(name = "IS_DEFRAY_ONTIME", unique = false, nullable = true, length = 5)
	private String isDefrayOntime;
	
	/** 租金按时交付说明 **/
	@Column(name = "DEFRAY_EXPL", unique = false, nullable = true, length = 65535)
	private String defrayExpl;
	
	/** 租金是否能覆盖我行贷款本息 **/
	@Column(name = "IS_COVER", unique = false, nullable = true, length = 5)
	private String isCover;
	
	/** 本息覆盖说明 **/
	@Column(name = "COVER_EXPL", unique = false, nullable = true, length = 65535)
	private String coverExpl;
	
	/** 租金来源是否与租户一致 **/
	@Column(name = "IS_IDENTICAL", unique = false, nullable = true, length = 5)
	private String isIdentical;
	
	/** 租金来源说明 **/
	@Column(name = "IDENTICAL_EXPL", unique = false, nullable = true, length = 65535)
	private String identicalExpl;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return loanBalance
     */
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param rentRate
	 */
	public void setRentRate(java.math.BigDecimal rentRate) {
		this.rentRate = rentRate;
	}
	
    /**
     * @return rentRate
     */
	public java.math.BigDecimal getRentRate() {
		return this.rentRate;
	}
	
	/**
	 * @param rentAmtYear
	 */
	public void setRentAmtYear(java.math.BigDecimal rentAmtYear) {
		this.rentAmtYear = rentAmtYear;
	}
	
    /**
     * @return rentAmtYear
     */
	public java.math.BigDecimal getRentAmtYear() {
		return this.rentAmtYear;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(Integer loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return loanTerm
     */
	public Integer getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param rentAmtBack
	 */
	public void setRentAmtBack(java.math.BigDecimal rentAmtBack) {
		this.rentAmtBack = rentAmtBack;
	}
	
    /**
     * @return rentAmtBack
     */
	public java.math.BigDecimal getRentAmtBack() {
		return this.rentAmtBack;
	}
	
	/**
	 * @param isAcct
	 */
	public void setIsAcct(String isAcct) {
		this.isAcct = isAcct;
	}
	
    /**
     * @return isAcct
     */
	public String getIsAcct() {
		return this.isAcct;
	}
	
	/**
	 * @param acctExpl
	 */
	public void setAcctExpl(String acctExpl) {
		this.acctExpl = acctExpl;
	}
	
    /**
     * @return acctExpl
     */
	public String getAcctExpl() {
		return this.acctExpl;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	
    /**
     * @return acctName
     */
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param acctSeqNo
	 */
	public void setAcctSeqNo(String acctSeqNo) {
		this.acctSeqNo = acctSeqNo;
	}
	
    /**
     * @return acctSeqNo
     */
	public String getAcctSeqNo() {
		return this.acctSeqNo;
	}
	
	/**
	 * @param acctb
	 */
	public void setAcctb(String acctb) {
		this.acctb = acctb;
	}
	
    /**
     * @return acctb
     */
	public String getAcctb() {
		return this.acctb;
	}
	
	/**
	 * @param isRepay
	 */
	public void setIsRepay(String isRepay) {
		this.isRepay = isRepay;
	}
	
    /**
     * @return isRepay
     */
	public String getIsRepay() {
		return this.isRepay;
	}
	
	/**
	 * @param repayExpl
	 */
	public void setRepayExpl(String repayExpl) {
		this.repayExpl = repayExpl;
	}
	
    /**
     * @return repayExpl
     */
	public String getRepayExpl() {
		return this.repayExpl;
	}
	
	/**
	 * @param isSign
	 */
	public void setIsSign(String isSign) {
		this.isSign = isSign;
	}
	
    /**
     * @return isSign
     */
	public String getIsSign() {
		return this.isSign;
	}
	
	/**
	 * @param signExpl
	 */
	public void setSignExpl(String signExpl) {
		this.signExpl = signExpl;
	}
	
    /**
     * @return signExpl
     */
	public String getSignExpl() {
		return this.signExpl;
	}
	
	/**
	 * @param isDefrayOntime
	 */
	public void setIsDefrayOntime(String isDefrayOntime) {
		this.isDefrayOntime = isDefrayOntime;
	}
	
    /**
     * @return isDefrayOntime
     */
	public String getIsDefrayOntime() {
		return this.isDefrayOntime;
	}
	
	/**
	 * @param defrayExpl
	 */
	public void setDefrayExpl(String defrayExpl) {
		this.defrayExpl = defrayExpl;
	}
	
    /**
     * @return defrayExpl
     */
	public String getDefrayExpl() {
		return this.defrayExpl;
	}
	
	/**
	 * @param isCover
	 */
	public void setIsCover(String isCover) {
		this.isCover = isCover;
	}
	
    /**
     * @return isCover
     */
	public String getIsCover() {
		return this.isCover;
	}
	
	/**
	 * @param coverExpl
	 */
	public void setCoverExpl(String coverExpl) {
		this.coverExpl = coverExpl;
	}
	
    /**
     * @return coverExpl
     */
	public String getCoverExpl() {
		return this.coverExpl;
	}
	
	/**
	 * @param isIdentical
	 */
	public void setIsIdentical(String isIdentical) {
		this.isIdentical = isIdentical;
	}
	
    /**
     * @return isIdentical
     */
	public String getIsIdentical() {
		return this.isIdentical;
	}
	
	/**
	 * @param identicalExpl
	 */
	public void setIdenticalExpl(String identicalExpl) {
		this.identicalExpl = identicalExpl;
	}
	
    /**
     * @return identicalExpl
     */
	public String getIdenticalExpl() {
		return this.identicalExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}