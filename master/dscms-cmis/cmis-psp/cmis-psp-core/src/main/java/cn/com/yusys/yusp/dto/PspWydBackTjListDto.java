package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/9/89:21
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class PspWydBackTjListDto {
    private static final long serialVersionUID = 1L;

    /** 支行名称 **/
    @RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="relBrIdName" )
    private String relBrId;
    /** 退回总计数量 **/
    private String backNum;
    /** 退回原因1的数量 **/
    private String backOneNum;
    /** 退回原因2的数量 **/
    private String backTwoNum;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getRelBrId() {
        return relBrId;
    }

    public void setRelBrId(String relBrId) {
        this.relBrId = relBrId;
    }

    public String getBackNum() {
        return backNum;
    }

    public void setBackNum(String backNum) {
        this.backNum = backNum;
    }

    public String getBackOneNum() {
        return backOneNum;
    }

    public void setBackOneNum(String backOneNum) {
        this.backOneNum = backOneNum;
    }

    public String getBackTwoNum() {
        return backTwoNum;
    }

    public void setBackTwoNum(String backTwoNum) {
        this.backTwoNum = backTwoNum;
    }
}
