package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007RespDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RiskTaskList;
import cn.com.yusys.yusp.dto.RiskTaskAndAdjustDto;
import cn.com.yusys.yusp.service.RiskTaskListService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskTaskListResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-25 03:38:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/risktasklist")
public class RiskTaskListResource {

    @Autowired
    private RiskTaskListService riskTaskListService;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    public ResultDto<List<RiskTaskList>> query() {
        QueryModel queryModel = new QueryModel();
        List<RiskTaskList> list = riskTaskListService.selectAll(queryModel);
        return new ResultDto<List<RiskTaskList>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    public ResultDto<List<RiskTaskList>> index(QueryModel queryModel) {
        List<RiskTaskList> list = riskTaskListService.selectByModel(queryModel);
        return new ResultDto<List<RiskTaskList>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryList")
    public ResultDto<List<RiskTaskList>> queryList(@RequestBody QueryModel queryModel) {
        List<RiskTaskList> list = riskTaskListService.selectByModel(queryModel);
        return new ResultDto<List<RiskTaskList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    public ResultDto<RiskTaskList> show(@PathVariable("pkId") String pkId) {
        RiskTaskList riskTaskList = riskTaskListService.selectByPrimaryKey(pkId);
        return new ResultDto<RiskTaskList>(riskTaskList);
    }

    /**
     * @函数名称:queryRiskTaskList
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    public ResultDto<RiskTaskList> queryRiskTaskList(@RequestBody RiskTaskList riskTaskList) {
        riskTaskList = riskTaskListService.queryRiskTaskList(riskTaskList);
        return new ResultDto<RiskTaskList>(riskTaskList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    public ResultDto<RiskTaskList> create(@RequestBody RiskTaskList riskTaskList) {
        riskTaskListService.insert(riskTaskList);
        return new ResultDto<RiskTaskList>(riskTaskList);
    }

    /**
     * 生成风险分类子表数据
     *
     * @author jijian_yx
     * @date 2021/7/15 21:41
     **/
    @PostMapping("/insertSubData")
    public ResultDto<Integer> insertSubData(@RequestBody RiskTaskList riskTaskList) {
        int result = riskTaskListService.insertSubData(riskTaskList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    public ResultDto<Integer> update(@RequestBody RiskTaskList riskTaskList) {
        int result = riskTaskListService.update(riskTaskList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updatebycondition
     * @函数描述:用于更新检查状态和审批状态
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatebycondition")
    public ResultDto<Integer> updateSelective(@RequestBody RiskTaskList riskTaskList) {
        riskTaskList.setUpdateTime(new Date());
        int result = riskTaskListService.updateSelective(riskTaskList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    public ResultDto<Integer> delete(@RequestBody String pkId) {
        int result = riskTaskListService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    public ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = riskTaskListService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryRiskTaskList
     * @函数描述:根据客户号查询风险分类信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryRiskClassByCusId")
    public ResultDto<HashMap<String, String>> queryRiskClassByCusId(@RequestBody String cusId) {
        HashMap<String, String> classMap = riskTaskListService.queryRiskClassByCusId(cusId);
        return new ResultDto<HashMap<String, String>>(classMap);
    }

    /**
     * @函数名称:queryTaskAndAdjust
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryTaskAndAdjust")
    public ResultDto<RiskTaskAndAdjustDto> queryTaskAndAdjust(@RequestBody RiskTaskAndAdjustDto riskTaskAndAdjustDto) {
        riskTaskAndAdjustDto = riskTaskListService.queryTaskAndAdjust(riskTaskAndAdjustDto);
        return new ResultDto<RiskTaskAndAdjustDto>(riskTaskAndAdjustDto);
    }

    /**
     * @函数名称:getSequences
     * @函数描述:用于获取序列号
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getSequences")
    public ResultDto<String> getSequences(@RequestBody String type) {
        String taskNo = riskTaskListService.getSequences(type);
        return new ResultDto<String>(taskNo);
    }

    /**
     * @函数名称:show
     * @函数描述: 根据客户编号查询风险分类信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryRiskTaskListByCusId")
    public ResultDto<RiskTaskList> queryRiskTaskListByCusId(@RequestBody String cusId) {
        RiskTaskList riskTaskList = riskTaskListService.queryRiskTaskListByCusId(cusId);
        return new ResultDto<RiskTaskList>(riskTaskList);
    }

    /**
     * @函数名称:show
     * @函数描述: 根据客户编号查询贷后检查信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryPspCheckRstByCusId")
    public ResultDto<RiskTaskList> queryPspCheckRstByCusId(@RequestBody String cusId) {
        RiskTaskList riskTaskList = riskTaskListService.queryPspCheckRstByCusId(cusId);
        return new ResultDto<RiskTaskList>(riskTaskList);
    }

    /**
     * 机评计算分值
     *
     * @author jijian_yx
     * @date 2021/8/6 21:43
     **/
    @PostMapping("/calculatePoint")
    protected ResultDto<JSONObject> calculatePoint(@RequestBody String pkId) {
        return new ResultDto<JSONObject>(riskTaskListService.calculatePoint(pkId));
    }

    /**
     * 获取子表任务生成状态
     * @author jijian_yx
     * @date 2021/9/30 0:47
     **/
    @PostMapping("/getTaskStatus")
    protected ResultDto<String> getTaskStatus(@RequestBody String pkId){
        String result = riskTaskListService.getTaskStatus(pkId);
        return new ResultDto<>(result);
    }

    /**
     * 获取所有页签保存情况
     * @author jijian_yx
     * @date 2021/10/8 17:09
     **/
    @PostMapping("/getSaveInfo")
    protected ResultDto<String> getSaveInfo(@RequestBody String taskNo){
        String result = riskTaskListService.getSaveInfo(taskNo);
        return new ResultDto<String>(result);
    }
}
