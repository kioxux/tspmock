/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskRepayAnaly
 * @类描述: risk_repay_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:37:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_repay_analy")
public class RiskRepayAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 还款意愿 **/
	@Column(name = "REPAY_WISH", unique = false, nullable = true, length = 5)
	private String repayWish;
	
	/** 还本金能力 **/
	@Column(name = "REPAY_CAP_ABILITY", unique = false, nullable = true, length = 5)
	private String repayCapAbility;
	
	/** 还息能力 **/
	@Column(name = "REPAY_INTEREST_ABILITY", unique = false, nullable = true, length = 5)
	private String repayInterestAbility;
	
	/** 欠息说明 **/
	@Column(name = "DEBIT_INTEREST_DESC", unique = false, nullable = true, length = 65535)
	private String debitInterestDesc;
	
	/** 银行对客户贷款管理 **/
	@Column(name = "CUS_LOAN_MANAGE", unique = false, nullable = true, length = 5)
	private String cusLoanManage;
	
	/** 客户是否存在违约行为 **/
	@Column(name = "IS_EXISTS_PENALTY", unique = false, nullable = true, length = 5)
	private String isExistsPenalty;
	
	/** 授信业务本金逾期时间T **/
	@Column(name = "CAP_OVERDUE_DAY", unique = false, nullable = true, length = 10)
	private String capOverdueDay;
	
	/** 授信业务利息逾期时间T **/
	@Column(name = "INT_OVERDUE_DAY", unique = false, nullable = true, length = 10)
	private String intOverdueDay;
	
	/** 实际控制人(法人代表) **/
	@Column(name = "INFACT_CTRL", unique = false, nullable = true, length = 5)
	private String infactCtrl;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param repayWish
	 */
	public void setRepayWish(String repayWish) {
		this.repayWish = repayWish;
	}
	
    /**
     * @return repayWish
     */
	public String getRepayWish() {
		return this.repayWish;
	}
	
	/**
	 * @param repayCapAbility
	 */
	public void setRepayCapAbility(String repayCapAbility) {
		this.repayCapAbility = repayCapAbility;
	}
	
    /**
     * @return repayCapAbility
     */
	public String getRepayCapAbility() {
		return this.repayCapAbility;
	}
	
	/**
	 * @param repayInterestAbility
	 */
	public void setRepayInterestAbility(String repayInterestAbility) {
		this.repayInterestAbility = repayInterestAbility;
	}
	
    /**
     * @return repayInterestAbility
     */
	public String getRepayInterestAbility() {
		return this.repayInterestAbility;
	}
	
	/**
	 * @param debitInterestDesc
	 */
	public void setDebitInterestDesc(String debitInterestDesc) {
		this.debitInterestDesc = debitInterestDesc;
	}
	
    /**
     * @return debitInterestDesc
     */
	public String getDebitInterestDesc() {
		return this.debitInterestDesc;
	}
	
	/**
	 * @param cusLoanManage
	 */
	public void setCusLoanManage(String cusLoanManage) {
		this.cusLoanManage = cusLoanManage;
	}
	
    /**
     * @return cusLoanManage
     */
	public String getCusLoanManage() {
		return this.cusLoanManage;
	}
	
	/**
	 * @param isExistsPenalty
	 */
	public void setIsExistsPenalty(String isExistsPenalty) {
		this.isExistsPenalty = isExistsPenalty;
	}
	
    /**
     * @return isExistsPenalty
     */
	public String getIsExistsPenalty() {
		return this.isExistsPenalty;
	}
	
	/**
	 * @param capOverdueDay
	 */
	public void setCapOverdueDay(String capOverdueDay) {
		this.capOverdueDay = capOverdueDay;
	}
	
    /**
     * @return capOverdueDay
     */
	public String getCapOverdueDay() {
		return this.capOverdueDay;
	}
	
	/**
	 * @param intOverdueDay
	 */
	public void setIntOverdueDay(String intOverdueDay) {
		this.intOverdueDay = intOverdueDay;
	}
	
    /**
     * @return intOverdueDay
     */
	public String getIntOverdueDay() {
		return this.intOverdueDay;
	}
	
	/**
	 * @param infactCtrl
	 */
	public void setInfactCtrl(String infactCtrl) {
		this.infactCtrl = infactCtrl;
	}
	
    /**
     * @return infactCtrl
     */
	public String getInfactCtrl() {
		return this.infactCtrl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}