/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import cn.com.yusys.yusp.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PspFinSituCheck;
import cn.com.yusys.yusp.repository.mapper.PspFinSituCheckMapper;

import static java.util.Comparator.comparing;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspFinSituCheckService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-28 13:16:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspFinSituCheckService {

    @Autowired
    private PspFinSituCheckMapper pspFinSituCheckMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspFinSituCheck selectByPrimaryKey(String pkId) {
        return pspFinSituCheckMapper.selectByPrimaryKey(pkId);
    }
    /**
     * @方法名称: queryPspFinSituCheck
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspFinSituCheck queryPspFinSituCheck(PspFinSituCheck pspFinSituCheck) {
        return pspFinSituCheckMapper.queryPspFinSituCheck(pspFinSituCheck);
    }
    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PspFinSituCheck> selectAll(QueryModel model) {
        List<PspFinSituCheck> records = (List<PspFinSituCheck>) pspFinSituCheckMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PspFinSituCheck> selectByModel(QueryModel model) {
        //PageHelper.startPage(model.getPage(), model.getSize());
        List<PspFinSituCheck> lists = pspFinSituCheckMapper.selectByModel(model);
        List<PspFinSituCheck> list = new ArrayList<>();
        String cusType = model.getCondition().get("cusType").toString();
        List<PspFinSituCheck> retList = new ArrayList<PspFinSituCheck>();
        if("1".equals(cusType)){
            list=changeGRLists(lists);
            retList = dealIndivFinaData();
        }else{
            list=changeLists(lists);
            retList = dealCorpFinaData();
        }
        // 根据报表编号统一名称
        list.addAll(retList);
        List<PspFinSituCheck> ret = list.stream()
                .collect(Collectors.toMap(PspFinSituCheck::getIdxName, a -> a, (o1,o2)-> {
                    o1.setHintInfo(o2.getHintInfo());
                    o1.setIdxOrder(o2.getIdxOrder());
                    o1.setHintValue(o2.getHintValue());
                    return o1;
                })).values().stream().collect(Collectors.toList());
        for (int i = ret.size() - 1; i >= 0; i--) {
            if(ret.get(i).getIdxOrder() == null){
                ret.remove(i);
            }else{
                if("1".equals(cusType)){
                    double riseRate = Double.parseDouble(ret.get(i).getL1yRiseRate() != null && ret.get(i).getL1yRiseRate() != "" ? ret.get(i).getL1yRiseRate() : "0");
                    if(Math.abs(riseRate) > 0.2){
                        ret.get(i).setHintValue("1");
                    }else{
                        ret.get(i).setHintValue("0");
                    }
                }else{
                    BigDecimal curtValue = new BigDecimal(StringUtils.nonBlank(ret.get(i).getCurtValue()) ? ret.get(i).getCurtValue():"0");
                    BigDecimal l1yCurtValue = new BigDecimal(StringUtils.nonBlank(ret.get(i).getL1yCurtValue()) ? ret.get(i).getL1yCurtValue():"0");
                    BigDecimal hintValue = new BigDecimal(StringUtils.nonBlank(ret.get(i).getHintValue())? ret.get(i).getHintValue():"0");
                    BigDecimal curtValueTmp = l1yCurtValue.add(l1yCurtValue.abs().multiply(hintValue));

                    //增加需要说明
                    if (hintValue.compareTo(BigDecimal.ZERO) > 0) {
                        if (curtValue.compareTo(curtValueTmp) > 0)
                            ret.get(i).setHintValue("1");
                        else
                            ret.get(i).setHintValue("0");
                    }
                    //减少需要说明
                    else {
                        if (curtValue.compareTo(curtValueTmp) < 0)
                            ret.get(i).setHintValue("1");
                        else
                            ret.get(i).setHintValue("0");
                    }
                }
            }
        }
        ret.sort(comparing(PspFinSituCheck::getIdxOrder));
        // PageHelper.clearPage();
        return ret;
    }

    private List<PspFinSituCheck> dealIndivFinaData(){
        List<PspFinSituCheck> retList = new ArrayList<PspFinSituCheck>();
        PspFinSituCheck pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("家庭工资收入");
        pspFinSituCheck.setHintInfo("如有大额变化20%以上客户经理必须说明");
        pspFinSituCheck.setIdxOrder("1");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("应收账款");
        pspFinSituCheck.setHintInfo("如有大额变化20%以上客户经理必须说明");
        pspFinSituCheck.setIdxOrder("2");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("存货");
        pspFinSituCheck.setHintInfo("如有大额变化20%以上客户经理必须说明");
        pspFinSituCheck.setIdxOrder("3");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("固定资产");
        pspFinSituCheck.setHintInfo("如有大额变化20%以上客户经理必须说明");
        pspFinSituCheck.setIdxOrder("4");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("负债总额");
        pspFinSituCheck.setHintInfo("如有大额变化20%以上客户经理必须说明");
        pspFinSituCheck.setIdxOrder("5");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("资产总额");
        pspFinSituCheck.setHintInfo("如有大额变化20%以上客户经理必须说明");
        pspFinSituCheck.setIdxOrder("6");
        retList.add(pspFinSituCheck);
        return retList;
    }

    private List<PspFinSituCheck> dealCorpFinaData(){
        List<PspFinSituCheck> retList = new ArrayList<PspFinSituCheck>();
        PspFinSituCheck pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("总资产");
        pspFinSituCheck.setHintInfo("如减少10%以上客户经理必须说明");
        pspFinSituCheck.setHintValue("-0.1");
        pspFinSituCheck.setIdxOrder("1");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("负债合计");
        pspFinSituCheck.setHintInfo("如增加10%以上客户经理必须说明");
        pspFinSituCheck.setHintValue("0.1");
        pspFinSituCheck.setIdxOrder("2");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("所有者权益");
        pspFinSituCheck.setHintInfo("如减少10%以上客户经理必须说明");
        pspFinSituCheck.setHintValue("-0.1");
        pspFinSituCheck.setIdxOrder("3");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("资产负债率(%)");
        pspFinSituCheck.setHintInfo("如增加5%以上客户经理必须说明");
        pspFinSituCheck.setHintValue("0.05");
        pspFinSituCheck.setIdxOrder("4");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("应收账款");
        pspFinSituCheck.setHintInfo("如增加20%以上客户经理必须说明");
        pspFinSituCheck.setHintValue("0.2");
        pspFinSituCheck.setIdxOrder("5");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("存货");
        pspFinSituCheck.setHintInfo("如增加20%以上客户经理必须说明");
        pspFinSituCheck.setHintValue("0.2");
        pspFinSituCheck.setIdxOrder("6");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("主营业务收入");
        pspFinSituCheck.setHintInfo("如减少20%以上客户经理必须说明");
        pspFinSituCheck.setHintValue("-0.2");
        pspFinSituCheck.setIdxOrder("7");
        retList.add(pspFinSituCheck);
        pspFinSituCheck = new PspFinSituCheck();
        pspFinSituCheck.setIdxName("利润总额");
        pspFinSituCheck.setHintInfo("如减少20%以上客户经理必须说明");
        pspFinSituCheck.setHintValue("-0.2");
        pspFinSituCheck.setIdxOrder("9");
        retList.add(pspFinSituCheck);
        return retList;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PspFinSituCheck record) {
        return pspFinSituCheckMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PspFinSituCheck record) {
        return pspFinSituCheckMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PspFinSituCheck record) {
        return pspFinSituCheckMapper.updateByPrimaryKey(record);
    }

    public int updateList(List<PspFinSituCheck> pspFinSituCheckList){
        int retValue = 0;
        for (PspFinSituCheck pspFinSituCheck : pspFinSituCheckList) {
            retValue += pspFinSituCheckMapper.updateByPrimaryKey(pspFinSituCheck);
        }
        return retValue;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PspFinSituCheck record) {
        return pspFinSituCheckMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pspFinSituCheckMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pspFinSituCheckMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByTaskNo
     * @方法描述: 根据任务编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByTaskNo(String taskNo) {
        return pspFinSituCheckMapper.deleteByTaskNo(taskNo);
    }

    /**
     * 修改表中报表名称
     * @param lists
     * @return
     */
    public List<PspFinSituCheck> changeLists(List<PspFinSituCheck> lists){
        List<PspFinSituCheck> list = new ArrayList<>();
        // 总资产
        String totalZCStr="ACC090123,Z01000001,XFZ0035,GC0000014,BS010120";
        // 负责合计
        String totalFZStr="ACC090213,XFZ0060,Z02000001,BS010221,GC0000015";
        // 所有者权益
        String totalQYStr = "Z04000000,GC0000025,ACC090214,Z04000001";
        // 应收账款
        String totalZKStr = "XFZ0005,Z01010610,ZRC0006,Z01010600,BS010104";
        // 存货
        String chStr = "Z01011200,XFZ0010,GC0000005,Z01011210,ACC090106";
        // 主营业务收入
        String zyStr = "L01000000,L02010000";
        // 利润总额
        String totalLRStr = "L05010000,XSL0015";
        // 资产负债率
        String fzStr = "XSC0003,C03010000";
        for(PspFinSituCheck pspFinSituCheck: lists){
            if(totalZCStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("总资产");
            }
            if(totalFZStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("负债合计");
            }
            if(totalQYStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("所有者权益");
            }
            if(totalZKStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("应收账款");
            }
            if(chStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("存货");
            }
            if(zyStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("主营业务收入");
            }
            if(totalLRStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("利润总额");
            }
            if(fzStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("资产负债率(%)");
            }
            list.add(pspFinSituCheck);
        }
        return list;
    }
    /**
     * 修改表中报表名称
     * @param lists
     * @return
     */
    public List<PspFinSituCheck> changeGRLists(List<PspFinSituCheck> lists){
        List<PspFinSituCheck> list = new ArrayList<>();
        // 家庭工资收入
        String totalSRStr="GC0000031";
        // 应收账款
        String totalYSStr="GC0000006";
        // 存货
        String chStr = "GC0000005";
        // 固定资产
        String totalGDStr = "GC0000013";
        // 负债总额
        String  totalFZStr = "GC0000015";
        // 资产总额
        String totalZCStr = "GC0000014";
        for(PspFinSituCheck pspFinSituCheck: lists){
            if(totalSRStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("家庭工资收入");
            }
            if(totalYSStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("应收账款");
            }
            if(chStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("存货");
            }
            if(totalGDStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("固定资产");
            }
            if(totalFZStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("负债总额");
            }
            if(totalZCStr.contains(pspFinSituCheck.getIdxKey())){
                pspFinSituCheck.setIdxName("资产总额");
            }
            list.add(pspFinSituCheck);
        }
        return list;
    }
}
