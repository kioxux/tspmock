/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspImplementOpt;
import cn.com.yusys.yusp.service.PspImplementOptService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspImplementOptResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pspimplementopt")
public class PspImplementOptResource {
    @Autowired
    private PspImplementOptService pspImplementOptService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspImplementOpt>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspImplementOpt> list = pspImplementOptService.selectAll(queryModel);
        return new ResultDto<List<PspImplementOpt>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PspImplementOpt>> index(QueryModel queryModel) {
        List<PspImplementOpt> list = pspImplementOptService.selectByModel(queryModel);
        return new ResultDto<List<PspImplementOpt>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @GetMapping("/queryList")
    protected ResultDto<List<PspImplementOpt>> queryList(QueryModel queryModel) {
        List<PspImplementOpt> list = pspImplementOptService.selectByModel(queryModel);
        return new ResultDto<List<PspImplementOpt>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspImplementOpt> show(@PathVariable("pkId") String pkId) {
        PspImplementOpt pspImplementOpt = pspImplementOptService.selectByPrimaryKey(pkId);
        return new ResultDto<PspImplementOpt>(pspImplementOpt);
    }
    /**
     * @函数名称:queryPspImplementOpt
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<PspImplementOpt> queryPspImplementOpt(@RequestBody PspImplementOpt pspImplementOpt) {
        pspImplementOpt = pspImplementOptService.queryPspImplementOpt(pspImplementOpt);
        return new ResultDto<PspImplementOpt>(pspImplementOpt);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PspImplementOpt> create(@RequestBody PspImplementOpt pspImplementOpt) throws URISyntaxException {
        pspImplementOptService.insert(pspImplementOpt);
        return new ResultDto<PspImplementOpt>(pspImplementOpt);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspImplementOpt pspImplementOpt) throws URISyntaxException {
        int result = pspImplementOptService.update(pspImplementOpt);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/updateList")
    protected ResultDto<Integer> updateList(@RequestBody List<PspImplementOpt> pspImplementOptList) throws URISyntaxException {
        int result = 0;
        for(int i=0; i<pspImplementOptList.size(); i++) {
            result += pspImplementOptService.update(pspImplementOptList.get(i));
        }
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspImplementOptService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspImplementOptService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByLastLmtAdvice")
    protected ResultDto<List<PspImplementOpt>> selectByLastLmtAdvice(@RequestBody QueryModel queryModel) {
        List<PspImplementOpt> list = pspImplementOptService.selectByModel(queryModel);
        return new ResultDto<List<PspImplementOpt>>(list);
    }
}
