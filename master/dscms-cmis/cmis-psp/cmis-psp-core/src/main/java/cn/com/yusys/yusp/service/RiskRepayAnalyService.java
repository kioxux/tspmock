/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RiskRepayAnaly;
import cn.com.yusys.yusp.repository.mapper.RiskRepayAnalyMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskRepayAnalyService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-20 09:37:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RiskRepayAnalyService {

    @Autowired
    private RiskRepayAnalyMapper riskRepayAnalyMapper;
    @Autowired
    private RiskDebitInfoService riskDebitInfoService;
    @Autowired
    private CmisBizClientService cmisBizClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskRepayAnaly selectByPrimaryKey(String pkId) {
        return riskRepayAnalyMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: queryRiskRepayAnaly
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskRepayAnaly queryRiskRepayAnaly(RiskRepayAnaly riskRepayAnaly) {
        RiskRepayAnaly repayAnaly = riskRepayAnalyMapper.queryRiskRepayAnaly(riskRepayAnaly);
        List<String> billNos = riskDebitInfoService.getBillNoByTaskNo(riskRepayAnaly.getTaskNo());
        if (null != billNos && billNos.size() > 0) {
            ResultDto<Integer> resultDto = cmisBizClientService.getMaxOverdueDay(billNos);
            if (null != resultDto && null != resultDto.getData()) {
                int overdueDay = resultDto.getData();
                String result = "";
                if (0 == overdueDay) {
                    // 1 授信业务本金未逾期
                    result = "1";
                } else if (0 < overdueDay && overdueDay <= 30) {
                    // 2 授信业务本金逾期时间T：0小于T小于等于30
                    result = "2";
                } else if (30 < overdueDay && overdueDay <= 90) {
                    // 3 授信业务本金逾期时间T：30小于T小于等于90
                    result = "3";
                } else if (90 < overdueDay && overdueDay <= 180) {
                    // 4 授信业务本金逾期时间T：90小于T小于等于180
                    result = "4";
                } else if (180 < overdueDay) {
                    // 5 授信业务本金逾期时间T：T大于180
                    result = "5";
                }
                if (null != repayAnaly) {
                    boolean saveFlag = false;
                    if (!Objects.equals(repayAnaly.getCapOverdueDay(), result)) {
                        repayAnaly.setCapOverdueDay(result);
                        saveFlag = true;
                    }
                    if (!Objects.equals(repayAnaly.getIntOverdueDay(), result)) {
                        repayAnaly.setIntOverdueDay(result);
                        saveFlag = true;
                    }
                    if (saveFlag) {
                        riskRepayAnalyMapper.updateByPrimaryKey(repayAnaly);
                    }
                } else {
                    repayAnaly = new RiskRepayAnaly();
                    repayAnaly.setCapOverdueDay(result);
                    repayAnaly.setIntOverdueDay(result);
                    repayAnaly.setTaskNo(riskRepayAnaly.getTaskNo());
                    riskRepayAnalyMapper.insertSelective(repayAnaly);
                }
            }
        }
        return repayAnaly;
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RiskRepayAnaly> selectAll(QueryModel model) {
        List<RiskRepayAnaly> records = (List<RiskRepayAnaly>) riskRepayAnalyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RiskRepayAnaly> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RiskRepayAnaly> list = riskRepayAnalyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RiskRepayAnaly record) {
        return riskRepayAnalyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RiskRepayAnaly record) {
        return riskRepayAnalyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RiskRepayAnaly record) {
        return riskRepayAnalyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RiskRepayAnaly record) {
        return riskRepayAnalyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return riskRepayAnalyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return riskRepayAnalyMapper.deleteByIds(ids);
    }
}
