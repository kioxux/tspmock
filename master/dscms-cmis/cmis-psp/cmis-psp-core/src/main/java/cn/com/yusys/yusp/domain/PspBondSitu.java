/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspBondSitu
 * @类描述: psp_bond_situ数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_bond_situ")
public class PspBondSitu extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 投资资产名称 **/
	@Column(name = "INVEST_ASSET_NAME", unique = false, nullable = true, length = 200)
	private String investAssetName;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 200)
	private String prdName;
	
	/** 投资金额 **/
	@Column(name = "INVEST_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal investAmt;
	
	/** 投资币种 **/
	@Column(name = "INVEST_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String investCurType;
	
	/** 交易日期 **/
	@Column(name = "TRAN_DATE", unique = false, nullable = true, length = 20)
	private String tranDate;
	
	/** 资产到期日 **/
	@Column(name = "ASSET_END_DATE", unique = false, nullable = true, length = 20)
	private String assetEndDate;
	
	/** 投资部门/分支机构 **/
	@Column(name = "INVEST_BR_ID", unique = false, nullable = true, length = 20)
	private String investBrId;
	
	/** 投资经理/主办客户经理 **/
	@Column(name = "INVEST_ID", unique = false, nullable = true, length = 20)
	private String investId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param investAssetName
	 */
	public void setInvestAssetName(String investAssetName) {
		this.investAssetName = investAssetName;
	}
	
    /**
     * @return investAssetName
     */
	public String getInvestAssetName() {
		return this.investAssetName;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param investAmt
	 */
	public void setInvestAmt(java.math.BigDecimal investAmt) {
		this.investAmt = investAmt;
	}
	
    /**
     * @return investAmt
     */
	public java.math.BigDecimal getInvestAmt() {
		return this.investAmt;
	}
	
	/**
	 * @param investCurType
	 */
	public void setInvestCurType(String investCurType) {
		this.investCurType = investCurType;
	}
	
    /**
     * @return investCurType
     */
	public String getInvestCurType() {
		return this.investCurType;
	}
	
	/**
	 * @param tranDate
	 */
	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}
	
    /**
     * @return tranDate
     */
	public String getTranDate() {
		return this.tranDate;
	}
	
	/**
	 * @param assetEndDate
	 */
	public void setAssetEndDate(String assetEndDate) {
		this.assetEndDate = assetEndDate;
	}
	
    /**
     * @return assetEndDate
     */
	public String getAssetEndDate() {
		return this.assetEndDate;
	}
	
	/**
	 * @param investBrId
	 */
	public void setInvestBrId(String investBrId) {
		this.investBrId = investBrId;
	}
	
    /**
     * @return investBrId
     */
	public String getInvestBrId() {
		return this.investBrId;
	}
	
	/**
	 * @param investId
	 */
	public void setInvestId(String investId) {
		this.investId = investId;
	}
	
    /**
     * @return investId
     */
	public String getInvestId() {
		return this.investId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}