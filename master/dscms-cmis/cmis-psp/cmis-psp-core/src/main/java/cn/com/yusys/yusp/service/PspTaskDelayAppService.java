/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PspTaskList;
import cn.com.yusys.yusp.dto.PspTaskAndDelayDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PspTaskDelayApp;
import cn.com.yusys.yusp.repository.mapper.PspTaskDelayAppMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskDelayAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-25 03:37:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspTaskDelayAppService {

    @Autowired
    private PspTaskDelayAppMapper pspTaskDelayAppMapper;
    @Autowired
    private PspTaskListService pspTaskListService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PspTaskDelayApp selectByPrimaryKey(String pkId) {
        return pspTaskDelayAppMapper.selectByPrimaryKey(pkId);
    }
	/**
     * @方法名称: queryPspTaskDelayApp
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspTaskDelayApp queryPspTaskDelayApp(PspTaskDelayApp pspTaskDelayApp) {
        return pspTaskDelayAppMapper.queryPspTaskDelayApp(pspTaskDelayApp);
    }
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PspTaskAndDelayDto> selectAll(QueryModel model) {
        List<PspTaskAndDelayDto> records = (List<PspTaskAndDelayDto>) pspTaskDelayAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PspTaskAndDelayDto> selectByModel(QueryModel model) {
        // 申请类型
        String applyType = "";
        // 审批状态
        String approveStatus = "";
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PspTaskAndDelayDto> pspTaskDelayAppList = new ArrayList<>();
        if (model.getCondition().get("applyType") != null){
            applyType = model.getCondition().get("applyType").toString();
            List<String> applyTypeList = Arrays.asList(applyType.split(","));
            model.getCondition().put("applyTypeList", applyTypeList);
        }
        if (model.getCondition().get("approveStatus") != null){
            approveStatus = model.getCondition().get("approveStatus").toString();
            // 待检查任务展示待发起、退回、审批中数据
            List<String> approveStatusList = Arrays.asList(approveStatus.split(","));
            model.getCondition().put("approveStatusList", approveStatusList);
        }
        if (model.getCondition().get("cusId") != null||model.getCondition().get("cusName") != null ){
            pspTaskDelayAppList=pspTaskDelayAppMapper.selectByModel(model);
        }else{
            pspTaskDelayAppList = pspTaskDelayAppMapper.selectByQueryModel(model);
            if(CollectionUtils.nonEmpty(pspTaskDelayAppList)){
                for(int i=0;i<pspTaskDelayAppList.size();i++){
                    PspTaskAndDelayDto pspTaskAndDelayDto=pspTaskDelayAppList.get(i);
                    PspTaskList pspTaskList=pspTaskListService.selectByTaskNo(pspTaskAndDelayDto.getTaskNo());
                    if(pspTaskList!=null){
                        pspTaskAndDelayDto.setCheckType(pspTaskList.getCheckType());
                        pspTaskAndDelayDto.setCusId(pspTaskList.getCusId());
                        pspTaskAndDelayDto.setCusName(pspTaskList.getCusName());
                        pspTaskAndDelayDto.setExecId(pspTaskList.getExecId());
                        pspTaskAndDelayDto.setExecBrId(pspTaskList.getExecBrId());
                        pspTaskAndDelayDto.setTaskStartDt(pspTaskList.getTaskStartDt());
                        pspTaskAndDelayDto.setTaskEndDt(pspTaskList.getTaskEndDt());
                    }
                    pspTaskDelayAppList.set(i,pspTaskAndDelayDto);
                }}
        }
        PageHelper.clearPage();
        return pspTaskDelayAppList;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PspTaskDelayApp record) {
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String openDay =stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
        }
        record.setInputId(inputId);
        record.setInputBrId(inputBrId);
        record.setInputDate(openDay);
        record.setUpdId(inputId);
        record.setUpdBrId(inputBrId);
        record.setUpdDate(openDay);
        record.setCreateTime(createTime);
        record.setUpdateTime(createTime);
        return pspTaskDelayAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PspTaskDelayApp record) {
        return pspTaskDelayAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PspTaskDelayApp record) {
        return pspTaskDelayAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PspTaskDelayApp record) {
        return pspTaskDelayAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pspTaskDelayAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pspTaskDelayAppMapper.deleteByIds(ids);
    }
}
