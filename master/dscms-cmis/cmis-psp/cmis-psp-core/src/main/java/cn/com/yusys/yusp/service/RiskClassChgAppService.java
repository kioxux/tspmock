/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RiskClassChgApp;
import cn.com.yusys.yusp.domain.RiskDebitInfo;
import cn.com.yusys.yusp.repository.mapper.RiskClassChgAppMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskClassChgAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-25 03:37:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RiskClassChgAppService {
    private final Logger logger = LoggerFactory.getLogger(RiskClassChgAppService.class);

    @Autowired
    private RiskClassChgAppMapper riskClassChgAppMapper;
    @Autowired
    private RiskDebitInfoService riskDebitInfoService;
    @Autowired
    private CmisBizClientService cmisBizClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskClassChgApp selectByPrimaryKey(String pkId) {
        return riskClassChgAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: queryRiskClassChgApp
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskClassChgApp queryRiskClassChgApp(RiskClassChgApp riskClassChgApp) {
        return riskClassChgAppMapper.queryRiskClassChgApp(riskClassChgApp);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RiskClassChgApp> selectAll(QueryModel model) {
        List<RiskClassChgApp> records = (List<RiskClassChgApp>) riskClassChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RiskClassChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        // 审批状态
        String approveStatus = "";
        if (model.getCondition().get("approveStatus") != null) {
            approveStatus = model.getCondition().get("approveStatus").toString();
            // 待检查任务展示待发起、退回、审批中数据
            List<String> approveStatusList = Arrays.asList(approveStatus.split(","));
            model.getCondition().put("approveStatusList", approveStatusList);
        }
        List<RiskClassChgApp> list = riskClassChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RiskClassChgApp record) {
        return riskClassChgAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RiskClassChgApp record) {
        return riskClassChgAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RiskClassChgApp record) {
        return riskClassChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RiskClassChgApp record) {
        return riskClassChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return riskClassChgAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return riskClassChgAppMapper.deleteByIds(ids);
    }

    /**
     * 根据流水号更新-只更新非空字段
     *
     * @author jijian_yx
     * @date 2021/7/8 22:33
     **/
    public int updateBySernoSelective(RiskClassChgApp riskClassChgApp) {
        return riskClassChgAppMapper.updateBySernoSelective(riskClassChgApp);
    }

    /**
     * 更新台账五级分类结果
     *
     * @author jijian_yx
     * @date 2021/8/10 23:08
     **/
    public void updateAccLoanFiveClass(RiskClassChgApp riskClassChgApp) throws Exception {
        try {
            RiskClassChgApp riskClass = riskClassChgAppMapper.queryRiskClassChgApp(riskClassChgApp);
            if (null == riskClass) {
                logger.info("风险分类数据不存在,风险分类调整流水号[{}]", riskClassChgApp.getSerno());
                return;
            }
            String appAdjClass = riskClass.getAppAdjClass();// 调整后分类
            String appAdjTenClass = riskClass.getAppAdjTenClass();// 调整后十级分类
            if (!StringUtils.isEmpty(appAdjClass)) {
                QueryModel queryModel = new QueryModel();
                queryModel.getCondition().put("taskNo", riskClass.getTaskNo());
                List<RiskDebitInfo> riskDebitInfoList = riskDebitInfoService.selectAll(queryModel);
                if (null != riskDebitInfoList && riskDebitInfoList.size() > 0) {
                    for (RiskDebitInfo riskDebitInfo : riskDebitInfoList) {
                        String billNo = riskDebitInfo.getBillNo();
                        if (!StringUtils.isEmpty(billNo)) {
                            Map<String, String> map = new HashMap<>();
                            map.put("billNo", billNo);
                            map.put("fiveClass", appAdjClass);
                            if (!StringUtils.isEmpty(appAdjTenClass)) {
                                map.put("tenClass", appAdjTenClass);
                            }
                            ResultDto<Integer> resultDto = cmisBizClientService.updateAccLoanByBillNo(map);
                            if (null == resultDto) {
                                logger.info("更新台账五级/十级分类结果失败,参数:[{}]", map);
                                throw new Exception("更新台账五级/十级分类结果失败,biz服务异常");
                            } else if (resultDto.getData() < 1) {
                                logger.info("更新台账五级/十级分类结果失败,未更新到数据,参数:[{}]", map);
                                // throw new Exception("更新台账五级/十级分类结果失败,未更新到数据");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("更新台账五级分类结果异常,原因:[{}]", e.getMessage());
            throw e;
        }
    }
}
