/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspRelCorpAnaly
 * @类描述: psp_rel_corp_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_rel_corp_analy")
public class PspRelCorpAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 所属集团/关联企业情况 **/
	@Column(name = "REL_SITU", unique = false, nullable = true, length = 65535)
	private String relSitu;
	
	/** 关联关系及内部资金往来情况 **/
	@Column(name = "CASH_FLOW_SITU", unique = false, nullable = true, length = 65535)
	private String cashFlowSitu;
	
	/** 整个集团客户授信规模及风险评估 **/
	@Column(name = "GRP_RISK_EVAL", unique = false, nullable = true, length = 65535)
	private String grpRiskEval;
	
	/** 目前采取的贷后管理策略及效果 **/
	@Column(name = "STRATY_EFFECT", unique = false, nullable = true, length = 65535)
	private String stratyEffect;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param relSitu
	 */
	public void setRelSitu(String relSitu) {
		this.relSitu = relSitu;
	}
	
    /**
     * @return relSitu
     */
	public String getRelSitu() {
		return this.relSitu;
	}
	
	/**
	 * @param cashFlowSitu
	 */
	public void setCashFlowSitu(String cashFlowSitu) {
		this.cashFlowSitu = cashFlowSitu;
	}
	
    /**
     * @return cashFlowSitu
     */
	public String getCashFlowSitu() {
		return this.cashFlowSitu;
	}
	
	/**
	 * @param grpRiskEval
	 */
	public void setGrpRiskEval(String grpRiskEval) {
		this.grpRiskEval = grpRiskEval;
	}
	
    /**
     * @return grpRiskEval
     */
	public String getGrpRiskEval() {
		return this.grpRiskEval;
	}
	
	/**
	 * @param stratyEffect
	 */
	public void setStratyEffect(String stratyEffect) {
		this.stratyEffect = stratyEffect;
	}
	
    /**
     * @return stratyEffect
     */
	public String getStratyEffect() {
		return this.stratyEffect;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}