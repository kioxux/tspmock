/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AutoPspWhiteInfoCorp;
import cn.com.yusys.yusp.service.AutoPspWhiteInfoCorpService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: AutoPspWhiteInfoCorpResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 09:45:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/autopspwhiteinfocorp")
public class AutoPspWhiteInfoCorpResource {
    @Autowired
    private AutoPspWhiteInfoCorpService autoPspWhiteInfoCorpService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AutoPspWhiteInfoCorp>> query() {
        QueryModel queryModel = new QueryModel();
        List<AutoPspWhiteInfoCorp> list = autoPspWhiteInfoCorpService.selectAll(queryModel);
        return new ResultDto<List<AutoPspWhiteInfoCorp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AutoPspWhiteInfoCorp>> index(QueryModel queryModel) {
        List<AutoPspWhiteInfoCorp> list = autoPspWhiteInfoCorpService.selectByModel(queryModel);
        return new ResultDto<List<AutoPspWhiteInfoCorp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{autoSerno}")
    protected ResultDto<AutoPspWhiteInfoCorp> show(@PathVariable("autoSerno") String autoSerno) {
        AutoPspWhiteInfoCorp autoPspWhiteInfoCorp = autoPspWhiteInfoCorpService.selectByPrimaryKey(autoSerno);
        return new ResultDto<AutoPspWhiteInfoCorp>(autoPspWhiteInfoCorp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AutoPspWhiteInfoCorp> create(@RequestBody AutoPspWhiteInfoCorp autoPspWhiteInfoCorp) throws URISyntaxException {
        autoPspWhiteInfoCorpService.insert(autoPspWhiteInfoCorp);
        return new ResultDto<AutoPspWhiteInfoCorp>(autoPspWhiteInfoCorp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AutoPspWhiteInfoCorp autoPspWhiteInfoCorp) throws URISyntaxException {
        int result = autoPspWhiteInfoCorpService.update(autoPspWhiteInfoCorp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{autoSerno}")
    protected ResultDto<Integer> delete(@PathVariable("autoSerno") String autoSerno) {
        int result = autoPspWhiteInfoCorpService.deleteByPrimaryKey(autoSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = autoPspWhiteInfoCorpService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
