/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskFinaAnaly
 * @类描述: risk_fina_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_fina_analy")
public class RiskFinaAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 客户现金流量分析 **/
	@Column(name = "CASH_ANALY", unique = false, nullable = true, length = 5)
	private String cashAnaly;
	
	/** 现金流量分析说明 **/
	@Column(name = "CASH_ANALY_EXPL", unique = false, nullable = true, length = 65535)
	private String cashAnalyExpl;
	
	/** 客户财务状况分析 **/
	@Column(name = "FINA_ANALY", unique = false, nullable = true, length = 5)
	private String finaAnaly;
	
	/** 财务状况说明 **/
	@Column(name = "FINA_ANALY_EXPL", unique = false, nullable = true, length = 65535)
	private String finaAnalyExpl;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param cashAnaly
	 */
	public void setCashAnaly(String cashAnaly) {
		this.cashAnaly = cashAnaly;
	}
	
    /**
     * @return cashAnaly
     */
	public String getCashAnaly() {
		return this.cashAnaly;
	}
	
	/**
	 * @param cashAnalyExpl
	 */
	public void setCashAnalyExpl(String cashAnalyExpl) {
		this.cashAnalyExpl = cashAnalyExpl;
	}
	
    /**
     * @return cashAnalyExpl
     */
	public String getCashAnalyExpl() {
		return this.cashAnalyExpl;
	}
	
	/**
	 * @param finaAnaly
	 */
	public void setFinaAnaly(String finaAnaly) {
		this.finaAnaly = finaAnaly;
	}
	
    /**
     * @return finaAnaly
     */
	public String getFinaAnaly() {
		return this.finaAnaly;
	}
	
	/**
	 * @param finaAnalyExpl
	 */
	public void setFinaAnalyExpl(String finaAnalyExpl) {
		this.finaAnalyExpl = finaAnalyExpl;
	}
	
    /**
     * @return finaAnalyExpl
     */
	public String getFinaAnalyExpl() {
		return this.finaAnalyExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}