/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPromiseAnaly
 * @类描述: psp_promise_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-25 03:37:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_promise_analy")
public class PspPromiseAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 生产状况 **/
	@Column(name = "PRODUCE_STATUS", unique = false, nullable = true, length = 65535)
	private String produceStatus;
	
	/** 企业经营状况 **/
	@Column(name = "OPER_STATUS", unique = false, nullable = true, length = 65535)
	private String operStatus;
	
	/** 担保人经营情况 **/
	@Column(name = "GUAR_OPER_STATUS", unique = false, nullable = true, length = 65535)
	private String guarOperStatus;
	
	/** 担保意愿 **/
	@Column(name = "GUAR_WISH_CASE", unique = false, nullable = true, length = 65535)
	private String guarWishCase;
	
	/** 担保实力 **/
	@Column(name = "GUAR_ABI", unique = false, nullable = true, length = 65535)
	private String guarAbi;
	
	/** 抵押物情况 **/
	@Column(name = "GUAR_SITU", unique = false, nullable = true, length = 65535)
	private String guarSitu;
	
	/** 借款人或担保人名下有效资产分析 **/
	@Column(name = "VALID_ASSET_ANALY", unique = false, nullable = true, length = 65535)
	private String validAssetAnaly;
	
	/** 工作收入来源分析 **/
	@Column(name = "INCOME_ANALY", unique = false, nullable = true, length = 65535)
	private String incomeAnaly;
	
	/** 是否有其它收入来源 **/
	@Column(name = "IS_OTHER_INCOME", unique = false, nullable = true, length = 5)
	private String isOtherIncome;
	
	/** 有关利害关系方情况 **/
	@Column(name = "INTERESTED_SITU", unique = false, nullable = true, length = 65535)
	private String interestedSitu;
	
	/** 抵押物是否被查封 **/
	@Column(name = "IS_CLOSE", unique = false, nullable = true, length = 5)
	private String isClose;
	
	/** 借款人或家庭名下有效资产 **/
	@Column(name = "FAM_VALID_ASSET_ANALY", unique = false, nullable = true, length = 65535)
	private String famValidAssetAnaly;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param produceStatus
	 */
	public void setProduceStatus(String produceStatus) {
		this.produceStatus = produceStatus;
	}
	
    /**
     * @return produceStatus
     */
	public String getProduceStatus() {
		return this.produceStatus;
	}
	
	/**
	 * @param operStatus
	 */
	public void setOperStatus(String operStatus) {
		this.operStatus = operStatus;
	}
	
    /**
     * @return operStatus
     */
	public String getOperStatus() {
		return this.operStatus;
	}
	
	/**
	 * @param guarOperStatus
	 */
	public void setGuarOperStatus(String guarOperStatus) {
		this.guarOperStatus = guarOperStatus;
	}
	
    /**
     * @return guarOperStatus
     */
	public String getGuarOperStatus() {
		return this.guarOperStatus;
	}
	
	/**
	 * @param guarWishCase
	 */
	public void setGuarWishCase(String guarWishCase) {
		this.guarWishCase = guarWishCase;
	}
	
    /**
     * @return guarWishCase
     */
	public String getGuarWishCase() {
		return this.guarWishCase;
	}
	
	/**
	 * @param guarAbi
	 */
	public void setGuarAbi(String guarAbi) {
		this.guarAbi = guarAbi;
	}
	
    /**
     * @return guarAbi
     */
	public String getGuarAbi() {
		return this.guarAbi;
	}
	
	/**
	 * @param guarSitu
	 */
	public void setGuarSitu(String guarSitu) {
		this.guarSitu = guarSitu;
	}
	
    /**
     * @return guarSitu
     */
	public String getGuarSitu() {
		return this.guarSitu;
	}
	
	/**
	 * @param validAssetAnaly
	 */
	public void setValidAssetAnaly(String validAssetAnaly) {
		this.validAssetAnaly = validAssetAnaly;
	}
	
    /**
     * @return validAssetAnaly
     */
	public String getValidAssetAnaly() {
		return this.validAssetAnaly;
	}
	
	/**
	 * @param incomeAnaly
	 */
	public void setIncomeAnaly(String incomeAnaly) {
		this.incomeAnaly = incomeAnaly;
	}
	
    /**
     * @return incomeAnaly
     */
	public String getIncomeAnaly() {
		return this.incomeAnaly;
	}
	
	/**
	 * @param isOtherIncome
	 */
	public void setIsOtherIncome(String isOtherIncome) {
		this.isOtherIncome = isOtherIncome;
	}
	
    /**
     * @return isOtherIncome
     */
	public String getIsOtherIncome() {
		return this.isOtherIncome;
	}
	
	/**
	 * @param interestedSitu
	 */
	public void setInterestedSitu(String interestedSitu) {
		this.interestedSitu = interestedSitu;
	}
	
    /**
     * @return interestedSitu
     */
	public String getInterestedSitu() {
		return this.interestedSitu;
	}
	
	/**
	 * @param isClose
	 */
	public void setIsClose(String isClose) {
		this.isClose = isClose;
	}
	
    /**
     * @return isClose
     */
	public String getIsClose() {
		return this.isClose;
	}
	
	/**
	 * @param famValidAssetAnaly
	 */
	public void setFamValidAssetAnaly(String famValidAssetAnaly) {
		this.famValidAssetAnaly = famValidAssetAnaly;
	}
	
    /**
     * @return famValidAssetAnaly
     */
	public String getFamValidAssetAnaly() {
		return this.famValidAssetAnaly;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}