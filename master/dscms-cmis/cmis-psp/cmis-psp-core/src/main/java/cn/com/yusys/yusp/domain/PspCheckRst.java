/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspCheckRst
 * @类描述: psp_check_rst数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-26 06:58:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_check_rst")
public class PspCheckRst extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 是否受托支付 **/
	@Column(name = "IS_UNTRU_PAY_TYPE", unique = false, nullable = true, length = 5)
	private String isUntruPayType;
	
	/** 购销合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;
	
	/** 交易对手名称 **/
	@Column(name = "CONT_TRAN_NAME", unique = false, nullable = true, length = 80)
	private String contTranName;
	
	/** 贷款用途 **/
	@Column(name = "CONT_AGREED_USE", unique = false, nullable = true, length = 65535)
	private String contAgreedUse;
	
	/** 贷款资金是否存在回流 **/
	@Column(name = "IS_CAP_BACK", unique = false, nullable = true, length = 5)
	private String isCapBack;
	
	/** 与授信用途是否一致 **/
	@Column(name = "IS_LMT_USE", unique = false, nullable = true, length = 5)
	private String isLmtUse;
	
	/** 贷款资金流向说明 **/
	@Column(name = "CAP_FLOW_REMARK", unique = false, nullable = true, length = 65535)
	private String capFlowRemark;
	
	/** 检查结果及检查人意见 **/
	@Column(name = "CHECK_COMMENT", unique = false, nullable = true, length = 65535)
	private String checkComment;
	
	/** 协查人意见 **/
	@Column(name = "ASSIST_COMMENT", unique = false, nullable = true, length = 65535)
	private String assistComment;
	
	/** 协查人 **/
	@Column(name = "ASSIST_ID", unique = false, nullable = true, length = 40)
	private String assistId;
	
	/** 协查机构 **/
	@Column(name = "ASSIST_ORG_ID", unique = false, nullable = true, length = 40)
	private String assistOrgId;
	
	/** 协查日期 **/
	@Column(name = "ASSIST_DATE", unique = false, nullable = true, length = 40)
	private String assistDate;
	
	/** 约见人 **/
	@Column(name = "RELATED_PERSONNEL", unique = false, nullable = true, length = 80)
	private String relatedPersonnel;
	
	/** 约见人职务 **/
	@Column(name = "RELATED_JOB_TTL", unique = false, nullable = true, length = 5)
	private String relatedJobTtl;
	
	/** 检查方式 **/
	@Column(name = "CHECK_MODE", unique = false, nullable = true, length = 5)
	private String checkMode;
	
	/** 是否落实抵质押或担保 **/
	@Column(name = "IS_GUAR_IMPLE", unique = false, nullable = true, length = 5)
	private String isGuarImple;
	
	/** 未落实说明 **/
	@Column(name = "IMPLE_REMARK", unique = false, nullable = true, length = 65535)
	private String impleRemark;
	
	/** 检查地点 **/
	@Column(name = "CHECK_PLACE", unique = false, nullable = true, length = 80)
	private String checkPlace;
	
	/** 管户客户经理授信建议 **/
	@Column(name = "CHECK_ADVICE_TYPE", unique = false, nullable = true, length = 5)
	private String checkAdviceType;
	
	/** 管户客户经理授信建议理由 **/
	@Column(name = "CHECK_ADVICE_REASON", unique = false, nullable = true, length = 65535)
	private String checkAdviceReason;
	
	/** 协办客户经理授信建议 **/
	@Column(name = "ASSIST_ADVICE_TYPE", unique = false, nullable = true, length = 5)
	private String assistAdviceType;
	
	/** 协办客户经理授信建议理由 **/
	@Column(name = "ASSIST_ADVICE_REASON", unique = false, nullable = true, length = 65535)
	private String assistAdviceReason;
	
	/** 客户逾期行为恶意还是非恶意 **/
	@Column(name = "IS_BAD_OVERDUE", unique = false, nullable = true, length = 5)
	private String isBadOverdue;
	
	/** 有无偿还能力 **/
	@Column(name = "REPAY_ABI", unique = false, nullable = true, length = 5)
	private String repayAbi;
	
	/** 催收方式 **/
	@Column(name = "COLLECT_WAY", unique = false, nullable = true, length = 200)
	private String collectWay;
	
	/** 有无向客户发送催收通知书 **/
	@Column(name = "IS_COLLECT", unique = false, nullable = true, length = 5)
	private String isCollect;
	
	/** 有无催收回执 **/
	@Column(name = "IS_RECEIPT", unique = false, nullable = true, length = 5)
	private String isReceipt;
	
	/** 是否准备起诉 **/
	@Column(name = "IS_SUE", unique = false, nullable = true, length = 5)
	private String isSue;
	
	/** 投后检查结论 **/
	@Column(name = "CAP_CHECK_RESULT", unique = false, nullable = true, length = 5)
	private String capCheckResult;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isUntruPayType
	 */
	public void setIsUntruPayType(String isUntruPayType) {
		this.isUntruPayType = isUntruPayType;
	}
	
    /**
     * @return isUntruPayType
     */
	public String getIsUntruPayType() {
		return this.isUntruPayType;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param contTranName
	 */
	public void setContTranName(String contTranName) {
		this.contTranName = contTranName;
	}
	
    /**
     * @return contTranName
     */
	public String getContTranName() {
		return this.contTranName;
	}
	
	/**
	 * @param contAgreedUse
	 */
	public void setContAgreedUse(String contAgreedUse) {
		this.contAgreedUse = contAgreedUse;
	}
	
    /**
     * @return contAgreedUse
     */
	public String getContAgreedUse() {
		return this.contAgreedUse;
	}
	
	/**
	 * @param isCapBack
	 */
	public void setIsCapBack(String isCapBack) {
		this.isCapBack = isCapBack;
	}
	
    /**
     * @return isCapBack
     */
	public String getIsCapBack() {
		return this.isCapBack;
	}
	
	/**
	 * @param isLmtUse
	 */
	public void setIsLmtUse(String isLmtUse) {
		this.isLmtUse = isLmtUse;
	}
	
    /**
     * @return isLmtUse
     */
	public String getIsLmtUse() {
		return this.isLmtUse;
	}
	
	/**
	 * @param capFlowRemark
	 */
	public void setCapFlowRemark(String capFlowRemark) {
		this.capFlowRemark = capFlowRemark;
	}
	
    /**
     * @return capFlowRemark
     */
	public String getCapFlowRemark() {
		return this.capFlowRemark;
	}
	
	/**
	 * @param checkComment
	 */
	public void setCheckComment(String checkComment) {
		this.checkComment = checkComment;
	}
	
    /**
     * @return checkComment
     */
	public String getCheckComment() {
		return this.checkComment;
	}
	
	/**
	 * @param assistComment
	 */
	public void setAssistComment(String assistComment) {
		this.assistComment = assistComment;
	}
	
    /**
     * @return assistComment
     */
	public String getAssistComment() {
		return this.assistComment;
	}
	
	/**
	 * @param assistId
	 */
	public void setAssistId(String assistId) {
		this.assistId = assistId;
	}
	
    /**
     * @return assistId
     */
	public String getAssistId() {
		return this.assistId;
	}
	
	/**
	 * @param assistOrgId
	 */
	public void setAssistOrgId(String assistOrgId) {
		this.assistOrgId = assistOrgId;
	}
	
    /**
     * @return assistOrgId
     */
	public String getAssistOrgId() {
		return this.assistOrgId;
	}
	
	/**
	 * @param assistDate
	 */
	public void setAssistDate(String assistDate) {
		this.assistDate = assistDate;
	}
	
    /**
     * @return assistDate
     */
	public String getAssistDate() {
		return this.assistDate;
	}
	
	/**
	 * @param relatedPersonnel
	 */
	public void setRelatedPersonnel(String relatedPersonnel) {
		this.relatedPersonnel = relatedPersonnel;
	}
	
    /**
     * @return relatedPersonnel
     */
	public String getRelatedPersonnel() {
		return this.relatedPersonnel;
	}
	
	/**
	 * @param relatedJobTtl
	 */
	public void setRelatedJobTtl(String relatedJobTtl) {
		this.relatedJobTtl = relatedJobTtl;
	}
	
    /**
     * @return relatedJobTtl
     */
	public String getRelatedJobTtl() {
		return this.relatedJobTtl;
	}
	
	/**
	 * @param checkMode
	 */
	public void setCheckMode(String checkMode) {
		this.checkMode = checkMode;
	}
	
    /**
     * @return checkMode
     */
	public String getCheckMode() {
		return this.checkMode;
	}
	
	/**
	 * @param isGuarImple
	 */
	public void setIsGuarImple(String isGuarImple) {
		this.isGuarImple = isGuarImple;
	}
	
    /**
     * @return isGuarImple
     */
	public String getIsGuarImple() {
		return this.isGuarImple;
	}
	
	/**
	 * @param impleRemark
	 */
	public void setImpleRemark(String impleRemark) {
		this.impleRemark = impleRemark;
	}
	
    /**
     * @return impleRemark
     */
	public String getImpleRemark() {
		return this.impleRemark;
	}
	
	/**
	 * @param checkPlace
	 */
	public void setCheckPlace(String checkPlace) {
		this.checkPlace = checkPlace;
	}
	
    /**
     * @return checkPlace
     */
	public String getCheckPlace() {
		return this.checkPlace;
	}
	
	/**
	 * @param checkAdviceType
	 */
	public void setCheckAdviceType(String checkAdviceType) {
		this.checkAdviceType = checkAdviceType;
	}
	
    /**
     * @return checkAdviceType
     */
	public String getCheckAdviceType() {
		return this.checkAdviceType;
	}
	
	/**
	 * @param checkAdviceReason
	 */
	public void setCheckAdviceReason(String checkAdviceReason) {
		this.checkAdviceReason = checkAdviceReason;
	}
	
    /**
     * @return checkAdviceReason
     */
	public String getCheckAdviceReason() {
		return this.checkAdviceReason;
	}
	
	/**
	 * @param assistAdviceType
	 */
	public void setAssistAdviceType(String assistAdviceType) {
		this.assistAdviceType = assistAdviceType;
	}
	
    /**
     * @return assistAdviceType
     */
	public String getAssistAdviceType() {
		return this.assistAdviceType;
	}
	
	/**
	 * @param assistAdviceReason
	 */
	public void setAssistAdviceReason(String assistAdviceReason) {
		this.assistAdviceReason = assistAdviceReason;
	}
	
    /**
     * @return assistAdviceReason
     */
	public String getAssistAdviceReason() {
		return this.assistAdviceReason;
	}
	
	/**
	 * @param isBadOverdue
	 */
	public void setIsBadOverdue(String isBadOverdue) {
		this.isBadOverdue = isBadOverdue;
	}
	
    /**
     * @return isBadOverdue
     */
	public String getIsBadOverdue() {
		return this.isBadOverdue;
	}
	
	/**
	 * @param repayAbi
	 */
	public void setRepayAbi(String repayAbi) {
		this.repayAbi = repayAbi;
	}
	
    /**
     * @return repayAbi
     */
	public String getRepayAbi() {
		return this.repayAbi;
	}
	
	/**
	 * @param collectWay
	 */
	public void setCollectWay(String collectWay) {
		this.collectWay = collectWay;
	}
	
    /**
     * @return collectWay
     */
	public String getCollectWay() {
		return this.collectWay;
	}
	
	/**
	 * @param isCollect
	 */
	public void setIsCollect(String isCollect) {
		this.isCollect = isCollect;
	}
	
    /**
     * @return isCollect
     */
	public String getIsCollect() {
		return this.isCollect;
	}
	
	/**
	 * @param isReceipt
	 */
	public void setIsReceipt(String isReceipt) {
		this.isReceipt = isReceipt;
	}
	
    /**
     * @return isReceipt
     */
	public String getIsReceipt() {
		return this.isReceipt;
	}
	
	/**
	 * @param isSue
	 */
	public void setIsSue(String isSue) {
		this.isSue = isSue;
	}
	
    /**
     * @return isSue
     */
	public String getIsSue() {
		return this.isSue;
	}
	
	/**
	 * @param capCheckResult
	 */
	public void setCapCheckResult(String capCheckResult) {
		this.capCheckResult = capCheckResult;
	}
	
    /**
     * @return capCheckResult
     */
	public String getCapCheckResult() {
		return this.capCheckResult;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}