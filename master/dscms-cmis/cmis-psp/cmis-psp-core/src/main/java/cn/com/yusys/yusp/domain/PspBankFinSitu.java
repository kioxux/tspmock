/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspBankFinSitu
 * @类描述: psp_bank_fin_situ数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_bank_fin_situ")
public class PspBankFinSitu extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 融资品种 **/
	@Column(name = "FIN_VARIET", unique = false, nullable = true, length = 5)
	private String finVariet;
	
	/** 融资金额（元） **/
	@Column(name = "FIN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal finAmt;
	
	/** 融资余额（元） **/
	@Column(name = "FIN_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal finLmt;
	
	/** 逾期本金（元） **/
	@Column(name = "OVERDUE_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueCapAmt;
	
	/** 拖欠利息（元） **/
	@Column(name = "DEBIT_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debitInt;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 融资品种名称 **/
	@Column(name = "FIN_VARIET_NAME", unique = false, nullable = true, length = 80)
	private String finVarietName;

	public String getFinVarietName() {
		return finVarietName;
	}

	public void setFinVarietName(String finVarietName) {
		this.finVarietName = finVarietName;
	}

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param finVariet
	 */
	public void setFinVariet(String finVariet) {
		this.finVariet = finVariet;
	}
	
    /**
     * @return finVariet
     */
	public String getFinVariet() {
		return this.finVariet;
	}
	
	/**
	 * @param finAmt
	 */
	public void setFinAmt(java.math.BigDecimal finAmt) {
		this.finAmt = finAmt;
	}
	
    /**
     * @return finAmt
     */
	public java.math.BigDecimal getFinAmt() {
		return this.finAmt;
	}
	
	/**
	 * @param finLmt
	 */
	public void setFinLmt(java.math.BigDecimal finLmt) {
		this.finLmt = finLmt;
	}
	
    /**
     * @return finLmt
     */
	public java.math.BigDecimal getFinLmt() {
		return this.finLmt;
	}
	
	/**
	 * @param overdueCapAmt
	 */
	public void setOverdueCapAmt(java.math.BigDecimal overdueCapAmt) {
		this.overdueCapAmt = overdueCapAmt;
	}
	
    /**
     * @return overdueCapAmt
     */
	public java.math.BigDecimal getOverdueCapAmt() {
		return this.overdueCapAmt;
	}
	
	/**
	 * @param debitInt
	 */
	public void setDebitInt(java.math.BigDecimal debitInt) {
		this.debitInt = debitInt;
	}
	
    /**
     * @return debitInt
     */
	public java.math.BigDecimal getDebitInt() {
		return this.debitInt;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}