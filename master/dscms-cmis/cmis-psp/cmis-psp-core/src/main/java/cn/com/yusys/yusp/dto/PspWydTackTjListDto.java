package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/9/916:01
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class PspWydTackTjListDto {
    private static final long serialVersionUID = 1L;

    /** 支行名称 **/
    @RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="relBrIdName" )
    private String relBrId;
    /** 累计分配数量（退回的剔除） **/
    private String distributeNum;
    /** 累计完成数量（过程营销平台操作完成）**/
    private String completeNum;
    /** 未完成贷后任务数量 **/
    private String incompleteNum;
    /** 正常时间处理贷后数量 **/
    private String normalNum;
    /** 未完成贷后任务数量 **/
    private String abnormalNum;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getRelBrId() {
        return relBrId;
    }

    public void setRelBrId(String relBrId) {
        this.relBrId = relBrId;
    }

    public String getDistributeNum() {
        return distributeNum;
    }

    public void setDistributeNum(String distributeNum) {
        this.distributeNum = distributeNum;
    }

    public String getCompleteNum() {
        return completeNum;
    }

    public void setCompleteNum(String completeNum) {
        this.completeNum = completeNum;
    }

    public String getIncompleteNum() {
        return incompleteNum;
    }

    public void setIncompleteNum(String incompleteNum) {
        this.incompleteNum = incompleteNum;
    }

    public String getNormalNum() {
        return normalNum;
    }

    public void setNormalNum(String normalNum) {
        this.normalNum = normalNum;
    }

    public String getAbnormalNum() {
        return abnormalNum;
    }

    public void setAbnormalNum(String abnormalNum) {
        this.abnormalNum = abnormalNum;
    }
}
