/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspIssueRiskInfo;
import cn.com.yusys.yusp.service.PspIssueRiskInfoService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspIssueRiskInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pspissueriskinfo")
public class PspIssueRiskInfoResource {
    @Autowired
    private PspIssueRiskInfoService pspIssueRiskInfoService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspIssueRiskInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspIssueRiskInfo> list = pspIssueRiskInfoService.selectAll(queryModel);
        return new ResultDto<List<PspIssueRiskInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PspIssueRiskInfo>> index(QueryModel queryModel) {
        List<PspIssueRiskInfo> list = pspIssueRiskInfoService.selectByModel(queryModel);
        return new ResultDto<List<PspIssueRiskInfo>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<PspIssueRiskInfo>> queryList(@RequestBody QueryModel queryModel) {
        List<PspIssueRiskInfo> list = pspIssueRiskInfoService.selectByModel(queryModel);
        return new ResultDto<List<PspIssueRiskInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspIssueRiskInfo> show(@PathVariable("pkId") String pkId) {
        PspIssueRiskInfo pspIssueRiskInfo = pspIssueRiskInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<PspIssueRiskInfo>(pspIssueRiskInfo);
    }
    /**
     * @函数名称:queryPspIssueRiskInfo
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<PspIssueRiskInfo> queryPspIssueRiskInfo(@RequestBody PspIssueRiskInfo pspIssueRiskInfo) {
        pspIssueRiskInfo = pspIssueRiskInfoService.queryPspIssueRiskInfo(pspIssueRiskInfo);
        return new ResultDto<PspIssueRiskInfo>(pspIssueRiskInfo);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PspIssueRiskInfo> create(@RequestBody PspIssueRiskInfo pspIssueRiskInfo) throws URISyntaxException {
        pspIssueRiskInfoService.insert(pspIssueRiskInfo);
        return new ResultDto<PspIssueRiskInfo>(pspIssueRiskInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspIssueRiskInfo pspIssueRiskInfo) throws URISyntaxException {
        int result = pspIssueRiskInfoService.update(pspIssueRiskInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspIssueRiskInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspIssueRiskInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deletepspIssueRiskInfo
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/deletepspIssueRiskInfo")
    protected ResultDto<Integer> deletepspIssueRiskInfo(@RequestBody PspIssueRiskInfo pspIssueRiskInfo) {
        int result = pspIssueRiskInfoService.deleteByPrimaryKey(pspIssueRiskInfo.getPkId());
        return new ResultDto<Integer>(result);
    }
}
