/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspGuarCheck
 * @类描述: psp_guar_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_guar_check")
public class PspGuarCheck extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 上期贷后检查时担保总额（万元） **/
	@Column(name = "PRE_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preGuarAmt;
	
	/** 本期贷后检查时担保总额（万元） **/
	@Column(name = "CURT_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtGuarAmt;
	
	/** 对外担保变化（万元） **/
	@Column(name = "GUAR_CHANGE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarChange;
	
	/** 其中对关联企业担保总额（万元） **/
	@Column(name = "CORRE_CON_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal correConGuarAmt;
	
	/** 对外担保变化情况分析 **/
	@Column(name = "GUAR_CHANGE_ANALY", unique = false, nullable = true, length = 65535)
	private String guarChangeAnaly;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param preGuarAmt
	 */
	public void setPreGuarAmt(java.math.BigDecimal preGuarAmt) {
		this.preGuarAmt = preGuarAmt;
	}
	
    /**
     * @return preGuarAmt
     */
	public java.math.BigDecimal getPreGuarAmt() {
		return this.preGuarAmt;
	}
	
	/**
	 * @param curtGuarAmt
	 */
	public void setCurtGuarAmt(java.math.BigDecimal curtGuarAmt) {
		this.curtGuarAmt = curtGuarAmt;
	}
	
    /**
     * @return curtGuarAmt
     */
	public java.math.BigDecimal getCurtGuarAmt() {
		return this.curtGuarAmt;
	}
	
	/**
	 * @param guarChange
	 */
	public void setGuarChange(java.math.BigDecimal guarChange) {
		this.guarChange = guarChange;
	}
	
    /**
     * @return guarChange
     */
	public java.math.BigDecimal getGuarChange() {
		return this.guarChange;
	}
	
	/**
	 * @param correConGuarAmt
	 */
	public void setCorreConGuarAmt(java.math.BigDecimal correConGuarAmt) {
		this.correConGuarAmt = correConGuarAmt;
	}
	
    /**
     * @return correConGuarAmt
     */
	public java.math.BigDecimal getCorreConGuarAmt() {
		return this.correConGuarAmt;
	}
	
	/**
	 * @param guarChangeAnaly
	 */
	public void setGuarChangeAnaly(String guarChangeAnaly) {
		this.guarChangeAnaly = guarChangeAnaly;
	}
	
    /**
     * @return guarChangeAnaly
     */
	public String getGuarChangeAnaly() {
		return this.guarChangeAnaly;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}