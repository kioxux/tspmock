/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspDebitNotFinaAnaly
 * @类描述: psp_debit_not_fina_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_debit_not_fina_analy")
public class PspDebitNotFinaAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 借款人及其家庭是否发生意外 **/
	@Column(name = "IS_ACCIDENT", unique = false, nullable = true, length = 5)
	private String isAccident;
	
	/** 意外说明 **/
	@Column(name = "ACCIDENT_REMARK", unique = false, nullable = true, length = 65535)
	private String accidentRemark;
	
	/** 借款人是否面临重大诉讼 **/
	@Column(name = "IS_INVOLVE_LAWSUIT", unique = false, nullable = true, length = 5)
	private String isInvolveLawsuit;
	
	/** 重大诉讼说明 **/
	@Column(name = "INVOLVE_LAWSUIT_REMARK", unique = false, nullable = true, length = 65535)
	private String involveLawsuitRemark;
	
	/** 借款人对外是否提供过多担保或大量资产被抵押 **/
	@Column(name = "IS_OUTGUAR", unique = false, nullable = true, length = 5)
	private String isOutguar;
	
	/** 过量担保、抵押说明 **/
	@Column(name = "OUTGUAR_REMARK", unique = false, nullable = true, length = 65535)
	private String outguarRemark;
	
	/** 相关法律文本是否合法有效 **/
	@Column(name = "IS_VLD", unique = false, nullable = true, length = 5)
	private String isVld;
	
	/** 法律文本无效说明 **/
	@Column(name = "VLD_REMARK", unique = false, nullable = true, length = 65535)
	private String vldRemark;
	
	/** 对借款人的总体评价 **/
	@Column(name = "TOTL_EVLU", unique = false, nullable = true, length = 5)
	private String totlEvlu;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isAccident
	 */
	public void setIsAccident(String isAccident) {
		this.isAccident = isAccident;
	}
	
    /**
     * @return isAccident
     */
	public String getIsAccident() {
		return this.isAccident;
	}
	
	/**
	 * @param accidentRemark
	 */
	public void setAccidentRemark(String accidentRemark) {
		this.accidentRemark = accidentRemark;
	}
	
    /**
     * @return accidentRemark
     */
	public String getAccidentRemark() {
		return this.accidentRemark;
	}
	
	/**
	 * @param isInvolveLawsuit
	 */
	public void setIsInvolveLawsuit(String isInvolveLawsuit) {
		this.isInvolveLawsuit = isInvolveLawsuit;
	}
	
    /**
     * @return isInvolveLawsuit
     */
	public String getIsInvolveLawsuit() {
		return this.isInvolveLawsuit;
	}
	
	/**
	 * @param involveLawsuitRemark
	 */
	public void setInvolveLawsuitRemark(String involveLawsuitRemark) {
		this.involveLawsuitRemark = involveLawsuitRemark;
	}
	
    /**
     * @return involveLawsuitRemark
     */
	public String getInvolveLawsuitRemark() {
		return this.involveLawsuitRemark;
	}
	
	/**
	 * @param isOutguar
	 */
	public void setIsOutguar(String isOutguar) {
		this.isOutguar = isOutguar;
	}
	
    /**
     * @return isOutguar
     */
	public String getIsOutguar() {
		return this.isOutguar;
	}
	
	/**
	 * @param outguarRemark
	 */
	public void setOutguarRemark(String outguarRemark) {
		this.outguarRemark = outguarRemark;
	}
	
    /**
     * @return outguarRemark
     */
	public String getOutguarRemark() {
		return this.outguarRemark;
	}
	
	/**
	 * @param isVld
	 */
	public void setIsVld(String isVld) {
		this.isVld = isVld;
	}
	
    /**
     * @return isVld
     */
	public String getIsVld() {
		return this.isVld;
	}
	
	/**
	 * @param vldRemark
	 */
	public void setVldRemark(String vldRemark) {
		this.vldRemark = vldRemark;
	}
	
    /**
     * @return vldRemark
     */
	public String getVldRemark() {
		return this.vldRemark;
	}
	
	/**
	 * @param totlEvlu
	 */
	public void setTotlEvlu(String totlEvlu) {
		this.totlEvlu = totlEvlu;
	}
	
    /**
     * @return totlEvlu
     */
	public String getTotlEvlu() {
		return this.totlEvlu;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}