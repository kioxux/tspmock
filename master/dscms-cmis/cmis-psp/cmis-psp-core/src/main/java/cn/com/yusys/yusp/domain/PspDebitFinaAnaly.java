/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspDebitFinaAnaly
 * @类描述: psp_debit_fina_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_debit_fina_analy")
public class PspDebitFinaAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 借款人销售同比是否下降 **/
	@Column(name = "IS_CHANGE_SALE", unique = false, nullable = true, length = 5)
	private String isChangeSale;
	
	/** 销售同比下降说明 **/
	@Column(name = "CHANGE_SALE_REMARK", unique = false, nullable = true, length = 65535)
	private String changeSaleRemark;
	
	/** 应收款是否明显增加 **/
	@Column(name = "IS_GROW_REC", unique = false, nullable = true, length = 5)
	private String isGrowRec;
	
	/** 应收款明显增加说明 **/
	@Column(name = "GROW_REC_REARK", unique = false, nullable = true, length = 65535)
	private String growRecReark;
	
	/** 借款人负债是否明显增加 **/
	@Column(name = "IS_GROW_DEBT", unique = false, nullable = true, length = 5)
	private String isGrowDebt;
	
	/** 负债明显增加说明 **/
	@Column(name = "GROW_DEBT_REMARK", unique = false, nullable = true, length = 65535)
	private String growDebtRemark;
	
	/** 是否涉及民间借贷 **/
	@Column(name = "IS_DC", unique = false, nullable = true, length = 5)
	private String isDc;
	
	/** 涉及民间借贷说明 **/
	@Column(name = "DC_REMARK", unique = false, nullable = true, length = 65535)
	private String dcRemark;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isChangeSale
	 */
	public void setIsChangeSale(String isChangeSale) {
		this.isChangeSale = isChangeSale;
	}
	
    /**
     * @return isChangeSale
     */
	public String getIsChangeSale() {
		return this.isChangeSale;
	}
	
	/**
	 * @param changeSaleRemark
	 */
	public void setChangeSaleRemark(String changeSaleRemark) {
		this.changeSaleRemark = changeSaleRemark;
	}
	
    /**
     * @return changeSaleRemark
     */
	public String getChangeSaleRemark() {
		return this.changeSaleRemark;
	}
	
	/**
	 * @param isGrowRec
	 */
	public void setIsGrowRec(String isGrowRec) {
		this.isGrowRec = isGrowRec;
	}
	
    /**
     * @return isGrowRec
     */
	public String getIsGrowRec() {
		return this.isGrowRec;
	}
	
	/**
	 * @param growRecReark
	 */
	public void setGrowRecReark(String growRecReark) {
		this.growRecReark = growRecReark;
	}
	
    /**
     * @return growRecReark
     */
	public String getGrowRecReark() {
		return this.growRecReark;
	}
	
	/**
	 * @param isGrowDebt
	 */
	public void setIsGrowDebt(String isGrowDebt) {
		this.isGrowDebt = isGrowDebt;
	}
	
    /**
     * @return isGrowDebt
     */
	public String getIsGrowDebt() {
		return this.isGrowDebt;
	}
	
	/**
	 * @param growDebtRemark
	 */
	public void setGrowDebtRemark(String growDebtRemark) {
		this.growDebtRemark = growDebtRemark;
	}
	
    /**
     * @return growDebtRemark
     */
	public String getGrowDebtRemark() {
		return this.growDebtRemark;
	}
	
	/**
	 * @param isDc
	 */
	public void setIsDc(String isDc) {
		this.isDc = isDc;
	}
	
    /**
     * @return isDc
     */
	public String getIsDc() {
		return this.isDc;
	}
	
	/**
	 * @param dcRemark
	 */
	public void setDcRemark(String dcRemark) {
		this.dcRemark = dcRemark;
	}
	
    /**
     * @return dcRemark
     */
	public String getDcRemark() {
		return this.dcRemark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}