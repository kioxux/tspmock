/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOtherCaseCheckBlemish
 * @类描述: psp_other_case_check_blemish数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_other_case_check_blemish")
public class PspOtherCaseCheckBlemish extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 瑕疵贷款形成原因 **/
	@Column(name = "BLEMISH_REASON", unique = false, nullable = true, length = 65535)
	private String blemishReason;
	
	/** 是否会形成不良 **/
	@Column(name = "IS_NOT_FINE", unique = false, nullable = true, length = 5)
	private String isNotFine;
	
	/** 形成不良说明 **/
	@Column(name = "NOT_FINE_REMARK", unique = false, nullable = true, length = 65535)
	private String notFineRemark;
	
	/** 重要法律文本是否有效 **/
	@Column(name = "IS_VLD", unique = false, nullable = true, length = 5)
	private String isVld;
	
	/** 法律文本无效说明 **/
	@Column(name = "VLD_REMARK", unique = false, nullable = true, length = 65535)
	private String vldRemark;
	
	/** 主从合同能否衔接，无法律瑕疵 **/
	@Column(name = "IS_LNK", unique = false, nullable = true, length = 5)
	private String isLnk;
	
	/** 合同不能衔接说明 **/
	@Column(name = "LNK_REMARK", unique = false, nullable = true, length = 65535)
	private String lnkRemark;
	
	/** 本次检查后拟采取的措施等情况说明 **/
	@Column(name = "CASE_REMARK", unique = false, nullable = true, length = 65535)
	private String caseRemark;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param blemishReason
	 */
	public void setBlemishReason(String blemishReason) {
		this.blemishReason = blemishReason;
	}
	
    /**
     * @return blemishReason
     */
	public String getBlemishReason() {
		return this.blemishReason;
	}
	
	/**
	 * @param isNotFine
	 */
	public void setIsNotFine(String isNotFine) {
		this.isNotFine = isNotFine;
	}
	
    /**
     * @return isNotFine
     */
	public String getIsNotFine() {
		return this.isNotFine;
	}
	
	/**
	 * @param notFineRemark
	 */
	public void setNotFineRemark(String notFineRemark) {
		this.notFineRemark = notFineRemark;
	}
	
    /**
     * @return notFineRemark
     */
	public String getNotFineRemark() {
		return this.notFineRemark;
	}
	
	/**
	 * @param isVld
	 */
	public void setIsVld(String isVld) {
		this.isVld = isVld;
	}
	
    /**
     * @return isVld
     */
	public String getIsVld() {
		return this.isVld;
	}
	
	/**
	 * @param vldRemark
	 */
	public void setVldRemark(String vldRemark) {
		this.vldRemark = vldRemark;
	}
	
    /**
     * @return vldRemark
     */
	public String getVldRemark() {
		return this.vldRemark;
	}
	
	/**
	 * @param isLnk
	 */
	public void setIsLnk(String isLnk) {
		this.isLnk = isLnk;
	}
	
    /**
     * @return isLnk
     */
	public String getIsLnk() {
		return this.isLnk;
	}
	
	/**
	 * @param lnkRemark
	 */
	public void setLnkRemark(String lnkRemark) {
		this.lnkRemark = lnkRemark;
	}
	
    /**
     * @return lnkRemark
     */
	public String getLnkRemark() {
		return this.lnkRemark;
	}
	
	/**
	 * @param caseRemark
	 */
	public void setCaseRemark(String caseRemark) {
		this.caseRemark = caseRemark;
	}
	
    /**
     * @return caseRemark
     */
	public String getCaseRemark() {
		return this.caseRemark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}