/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskPldimnList
 * @类描述: risk_pldimn_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_pldimn_list")
public class RiskPldimnList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 押品编号 **/
	@Column(name = "PLDIMN_NO", unique = false, nullable = true, length = 40)
	private String pldimnNo;
	
	/** 押品类型 **/
	@Column(name = "PLDIMN_TYPE", unique = false, nullable = true, length = 5)
	private String pldimnType;
	
	/** 抵质押类别 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String guarType;
	
	/** 押品名称 **/
	@Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 100)
	private String pldimnMemo;
	
	/** 所有权人 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 40)
	private String guarCusId;
	
	/** 所有权人名称 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 100)
	private String guarCusName;
	
	/** 评估价值 **/
	@Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalAmt;
	
	/** 认定价值 **/
	@Column(name = "CONFIRM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal confirmAmt;
	
	/** 抵（质）押品可执行能力 **/
	@Column(name = "PLDIMN_EXE_ABI", unique = false, nullable = true, length = 5)
	private String pldimnExeAbi;
	
	/** 抵质押品价值(元) **/
	@Column(name = "PLDIMN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pldimnAmt;
	
	/** 抵质押率 **/
	@Column(name = "MORTAGAGE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mortagageRate;
	
	/** 抵质押品价值情况 **/
	@Column(name = "GUAR_VALUE_SITU", unique = false, nullable = true, length = 5)
	private String guarValueSitu;
	
	/** 抵（质）押品的价值评估方式 **/
	@Column(name = "GUAR_EVAL_TYPE", unique = false, nullable = true, length = 5)
	private String guarEvalType;
	
	/** 抵押/质押品情况备注 **/
	@Column(name = "PLDIMN_REMARK", unique = false, nullable = true, length = 65535)
	private String pldimnRemark;

	/** 抵（质）押品的价值评估方式 **/
	@Column(name = "分析状态", unique = false, nullable = true, length = 5)
	private String analyStatus;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param pldimnNo
	 */
	public void setPldimnNo(String pldimnNo) {
		this.pldimnNo = pldimnNo;
	}
	
    /**
     * @return pldimnNo
     */
	public String getPldimnNo() {
		return this.pldimnNo;
	}
	
	/**
	 * @param pldimnType
	 */
	public void setPldimnType(String pldimnType) {
		this.pldimnType = pldimnType;
	}
	
    /**
     * @return pldimnType
     */
	public String getPldimnType() {
		return this.pldimnType;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}
	
    /**
     * @return pldimnMemo
     */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}
	
    /**
     * @return guarCusId
     */
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}
	
    /**
     * @return guarCusName
     */
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return evalAmt
     */
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param confirmAmt
	 */
	public void setConfirmAmt(java.math.BigDecimal confirmAmt) {
		this.confirmAmt = confirmAmt;
	}
	
    /**
     * @return confirmAmt
     */
	public java.math.BigDecimal getConfirmAmt() {
		return this.confirmAmt;
	}
	
	/**
	 * @param pldimnExeAbi
	 */
	public void setPldimnExeAbi(String pldimnExeAbi) {
		this.pldimnExeAbi = pldimnExeAbi;
	}
	
    /**
     * @return pldimnExeAbi
     */
	public String getPldimnExeAbi() {
		return this.pldimnExeAbi;
	}
	
	/**
	 * @param pldimnAmt
	 */
	public void setPldimnAmt(java.math.BigDecimal pldimnAmt) {
		this.pldimnAmt = pldimnAmt;
	}
	
    /**
     * @return pldimnAmt
     */
	public java.math.BigDecimal getPldimnAmt() {
		return this.pldimnAmt;
	}
	
	/**
	 * @param mortagageRate
	 */
	public void setMortagageRate(java.math.BigDecimal mortagageRate) {
		this.mortagageRate = mortagageRate;
	}
	
    /**
     * @return mortagageRate
     */
	public java.math.BigDecimal getMortagageRate() {
		return this.mortagageRate;
	}
	
	/**
	 * @param guarValueSitu
	 */
	public void setGuarValueSitu(String guarValueSitu) {
		this.guarValueSitu = guarValueSitu;
	}
	
    /**
     * @return guarValueSitu
     */
	public String getGuarValueSitu() {
		return this.guarValueSitu;
	}
	
	/**
	 * @param guarEvalType
	 */
	public void setGuarEvalType(String guarEvalType) {
		this.guarEvalType = guarEvalType;
	}
	
    /**
     * @return guarEvalType
     */
	public String getGuarEvalType() {
		return this.guarEvalType;
	}
	
	/**
	 * @param pldimnRemark
	 */
	public void setPldimnRemark(String pldimnRemark) {
		this.pldimnRemark = pldimnRemark;
	}
	
    /**
     * @return pldimnRemark
     */
	public String getPldimnRemark() {
		return this.pldimnRemark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


	public String getAnalyStatus() {
		return analyStatus;
	}

	public void setAnalyStatus(String analyStatus) {
		this.analyStatus = analyStatus;
	}
}