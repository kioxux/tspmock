package cn.com.yusys.yusp.service.server.xdqt0006;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0001.Cmisbatch0001RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdqt0006.resp.Xdqt0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp模块
 * @类名称: Xdqt0006Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-06-05 09:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdqt0006Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdqt0006Service.class);

    @Autowired
    private CmisBatchClientService cmisBatchClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * 跑批状态时间查询
     *
     * @param
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    public Xdqt0006DataRespDto getProgressStatus() throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value);
        Xdqt0006DataRespDto xdqt0006DataRespDto = new Xdqt0006DataRespDto();
        try{
            String openday = stringRedisTemplate.opsForValue().get("openDay");//当前日期
            Cmisbatch0001ReqDto cmisbatch0001ReqDto = new Cmisbatch0001ReqDto();
            cmisbatch0001ReqDto.setOpenDay(openday);
            String progress="0";
            ResultDto<Cmisbatch0001RespDto> resultDto = cmisBatchClientService.cmisbatch0001(cmisbatch0001ReqDto);
            if(ResultDto.success().getCode().equals(resultDto.getCode())){
                Cmisbatch0001RespDto cmisbatch0001RespDto = resultDto.getData();
                if(Objects.nonNull(cmisbatch0001RespDto)){
                    if("100".equals(cmisbatch0001RespDto.getControlStatus())){
                        progress = "1";
                    }
                }
            }
            xdqt0006DataRespDto.setProgress(progress);
        }catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
            //throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value, JSON.toJSONString(xdqt0006DataRespDto));
        return xdqt0006DataRespDto;
    }
}
