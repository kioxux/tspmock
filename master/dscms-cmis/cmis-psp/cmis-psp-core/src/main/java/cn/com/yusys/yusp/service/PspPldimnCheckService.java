/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PspPldimnCheck;
import cn.com.yusys.yusp.repository.mapper.PspPldimnCheckMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPldimnCheckService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspPldimnCheckService {

    @Autowired
    private PspPldimnCheckMapper pspPldimnCheckMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PspPldimnCheck selectByPrimaryKey(String pkId) {
        return pspPldimnCheckMapper.selectByPrimaryKey(pkId);
    }
	/**
     * @方法名称: queryPspPldimnCheck
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspPldimnCheck queryPspPldimnCheck(PspPldimnCheck pspPldimnCheck) {
        return pspPldimnCheckMapper.queryPspPldimnCheck(pspPldimnCheck);
    }
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PspPldimnCheck> selectAll(QueryModel model) {
        List<PspPldimnCheck> records = (List<PspPldimnCheck>) pspPldimnCheckMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PspPldimnCheck> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PspPldimnCheck> list = pspPldimnCheckMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PspPldimnCheck record) {
        return pspPldimnCheckMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PspPldimnCheck record) {
        return pspPldimnCheckMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PspPldimnCheck record) {
        int count = 0;
        PspPldimnCheck pspPldimnCheck = pspPldimnCheckMapper.selectByPrimaryKey(record.getPkId());
        if (pspPldimnCheck != null ){
            pspPldimnCheckMapper.updateByPrimaryKey(record);
        }else {
            pspPldimnCheckMapper.insert(record);
        }
        return count;
    }

    /**
     * @方法名称: updateByTaskNo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateByTaskNo(PspPldimnCheck record) {
        return pspPldimnCheckMapper.updateByTaskNo(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PspPldimnCheck record) {
        return pspPldimnCheckMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pspPldimnCheckMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pspPldimnCheckMapper.deleteByIds(ids);
    }
}
