package cn.com.yusys.yusp.workFlow.listener;

import cn.com.yusys.yusp.flow.client.ClientBizFactory;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 投贷后管理业务监听（客户端）
 */
@Service
public class PspBizMessageListener {
    private final Logger log = LoggerFactory.getLogger(PspBizMessageListener.class);

    @Autowired
    private ClientBizFactory clientBizFactory;

    @RabbitListener(queuesToDeclare =
            {
                    @Queue("yusp-flow.DHGL01"),
                    @Queue("yusp-flow.DHGL02"),
                    @Queue("yusp-flow.DHGL03"),
                    @Queue("yusp-flow.DHGL04"),
                    @Queue("yusp-flow.DHGL05"),
                    @Queue("yusp-flow.DHGL06"),
                    @Queue("yusp-flow.DHGL07"),
                    @Queue("yusp-flow.DHGL08"),
                    @Queue("yusp-flow.DHGL09"),
                    @Queue("yusp-flow.DHGL10"),
                    @Queue("yusp-flow.DHGL11"),
                    @Queue("yusp-flow.DHGL12"),
                    @Queue("yusp-flow.DHGL13"),
                    @Queue("yusp-flow.DHGL14"),
                    @Queue("yusp-flow.DHGL15"),
                    @Queue("yusp-flow.DHGL16"),
                    @Queue("yusp-flow.DHGL17"),
                    @Queue("yusp-flow.DHGL18"),
                    @Queue("yusp-flow.DHGL19"),
                    @Queue("yusp-flow.DHGL20"),
                    @Queue("yusp-flow.DHGL21"),
                    @Queue("yusp-flow.SGCZ22"), // 贷后检查审批流程（寿光）
                    @Queue("yusp-flow.SGCZ23"), // 总行下发不定期检查审批流程（寿光）
                    @Queue("yusp-flow.SGCZ24"), // 贷后检查延期终止审批流程（寿光）
                    @Queue("yusp-flow.SGCZ25"), // 风险分类审批流程（寿光）
                    @Queue("yusp-flow.DHCZ22"), //贷后检查审批流程（东海）
                    @Queue("yusp-flow.DHCZ23"), //总行下发不定期检查审批流程（东海）
                    @Queue("yusp-flow.DHCZ24"), //贷后检查延期终止审批流程（东海）
                    @Queue("yusp-flow.DHCZ25") // 风险分类审批流程（东海）
            })// 队列名称为【yusp-flow.流程申请类型】,可以添加多个
    @RabbitHandler
    public void receiveQueue(ResultInstanceDto message) throws Exception {
        log.info("客户端（投贷后管理）业务监听:" + message);
        clientBizFactory.processBiz(message);
    }
}