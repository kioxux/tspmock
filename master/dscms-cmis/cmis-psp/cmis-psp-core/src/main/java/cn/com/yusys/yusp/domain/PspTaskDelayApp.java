/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskDelayApp
 * @类描述: psp_task_delay_app数据实体类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-25 03:37:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_task_delay_app")
public class PspTaskDelayApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;

	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;

	/** 申请延期完成日期 **/
	@Column(name = "DELAY_DATE", unique = false, nullable = true, length = 20)
	private String delayDate;

	/**原任务完成日期完成日期 **/
	@Column(name = "TASK_END_DT_OLD", unique = false, nullable = true, length = 20)
	private String taskEndDtOld;

	/** 申请理由 **/
	@Column(name = "APPLY_REASON", unique = false, nullable = true, length = 65535)
	private String applyReason;

	/** 申请类型 **/
	@Column(name = "APPLY_TYPE", unique = false, nullable = true, length = 5)
	private String applyType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}

    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param delayDate
	 */
	public void setDelayDate(String delayDate) {
		this.delayDate = delayDate;
	}

    /**
     * @return delayDate
     */
	public String getDelayDate() {
		return this.delayDate;
	}

	/**
	 * @param applyReason
	 */
	public void setApplyReason(String applyReason) {
		this.applyReason = applyReason;
	}

    /**
     * @return applyReason
     */
	public String getApplyReason() {
		return this.applyReason;
	}

	/**
	 * @param applyType
	 */
	public void setApplyType(String applyType) {
		this.applyType = applyType;
	}

    /**
     * @return applyType
     */
	public String getApplyType() {
		return this.applyType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getTaskEndDtOld() {
		return taskEndDtOld;
	}

	public void setTaskEndDtOld(String taskEndDtOld) {
		this.taskEndDtOld = taskEndDtOld;
	}

	public String getUpdId() {
		return updId;
	}

	public void setUpdId(String updId) {
		this.updId = updId;
	}

	public String getUpdBrId() {
		return updBrId;
	}

	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
}