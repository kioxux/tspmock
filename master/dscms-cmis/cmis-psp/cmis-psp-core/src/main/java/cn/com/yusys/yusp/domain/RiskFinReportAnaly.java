/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskFinReportAnaly
 * @类描述: risk_fin_report_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_fin_report_analy")
public class RiskFinReportAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 指标编号 **/
	@Column(name = "IDX_KEY", unique = false, nullable = true, length = 40)
	private String idxKey;
	
	/** 指标名称 **/
	@Column(name = "IDX_NAME", unique = false, nullable = true, length = 80)
	private String idxName;
	
	/** 本期数据 **/
	@Column(name = "CURT_VALUE", unique = false, nullable = true, length = 40)
	private String curtValue;
	
	/** 上一年数据 **/
	@Column(name = "L1Y_VALUE", unique = false, nullable = true, length = 40)
	private String l1yValue;
	
	/** 上两年数据 **/
	@Column(name = "L2Y_VALUE", unique = false, nullable = true, length = 40)
	private String l2yValue;
	
	/** 比上一年增长 **/
	@Column(name = "L1Y_RISE_RATE", unique = false, nullable = true, length = 40)
	private String l1yRiseRate;
	
	/** 比上两年增长 **/
	@Column(name = "L2Y_RISE_RATE", unique = false, nullable = true, length = 40)
	private String l2yRiseRate;

	private String idxOrder;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param idxKey
	 */
	public void setIdxKey(String idxKey) {
		this.idxKey = idxKey;
	}
	
    /**
     * @return idxKey
     */
	public String getIdxKey() {
		return this.idxKey;
	}
	
	/**
	 * @param idxName
	 */
	public void setIdxName(String idxName) {
		this.idxName = idxName;
	}
	
    /**
     * @return idxName
     */
	public String getIdxName() {
		return this.idxName;
	}
	
	/**
	 * @param curtValue
	 */
	public void setCurtValue(String curtValue) {
		this.curtValue = curtValue;
	}
	
    /**
     * @return curtValue
     */
	public String getCurtValue() {
		return this.curtValue;
	}
	
	/**
	 * @param l1yValue
	 */
	public void setL1yValue(String l1yValue) {
		this.l1yValue = l1yValue;
	}
	
    /**
     * @return l1yValue
     */
	public String getL1yValue() {
		return this.l1yValue;
	}
	
	/**
	 * @param l2yValue
	 */
	public void setL2yValue(String l2yValue) {
		this.l2yValue = l2yValue;
	}
	
    /**
     * @return l2yValue
     */
	public String getL2yValue() {
		return this.l2yValue;
	}
	
	/**
	 * @param l1yRiseRate
	 */
	public void setL1yRiseRate(String l1yRiseRate) {
		this.l1yRiseRate = l1yRiseRate;
	}
	
    /**
     * @return l1yRiseRate
     */
	public String getL1yRiseRate() {
		return this.l1yRiseRate;
	}
	
	/**
	 * @param l2yRiseRate
	 */
	public void setL2yRiseRate(String l2yRiseRate) {
		this.l2yRiseRate = l2yRiseRate;
	}
	
    /**
     * @return l2yRiseRate
     */
	public String getL2yRiseRate() {
		return this.l2yRiseRate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


	public String getIdxOrder() {
		return idxOrder;
	}

	public void setIdxOrder(String idxOrder) {
		this.idxOrder = idxOrder;
	}
}
