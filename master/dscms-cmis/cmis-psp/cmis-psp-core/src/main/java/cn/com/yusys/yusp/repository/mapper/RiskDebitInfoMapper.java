/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.RiskDebitAndLoanDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.RiskDebitInfo;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskDebitInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-18 02:45:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface RiskDebitInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    RiskDebitInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: queryRiskDebitInfo
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    RiskDebitInfo queryRiskDebitInfo(RiskDebitInfo riskDebitInfo);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<RiskDebitInfo> selectByModel(QueryModel model);

    /**
     * @方法名称: selectRiskDebitAndLoan
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<RiskDebitAndLoanDto> selectRiskDebitAndLoan(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(RiskDebitInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(RiskDebitInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(RiskDebitInfo record);

    /**
     * @方法名称: updateCheckStatus
     * @方法描述: 更新解析状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateCheckStatus(RiskDebitInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(RiskDebitInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据任务编号获取借据号
     * @author jijian_yx
     * @date 2021/10/13 21:28
     **/
    List<String> getBillNoByTaskNo(@Param("taskNo") String taskNo);

    /**
     * 取借据中最低五级分类结果
     * @author jijian_yx
     * @date 2021/10/22 0:03
     **/
    String getLowFiveClassByTaskNo(@Param("taskNo") String taskNo);
}