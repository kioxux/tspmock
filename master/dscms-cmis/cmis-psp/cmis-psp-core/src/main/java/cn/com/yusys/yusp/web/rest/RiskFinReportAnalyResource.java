/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RiskFinReportAnaly;
import cn.com.yusys.yusp.service.RiskFinReportAnalyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskFinReportAnalyResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/riskfinreportanaly")
public class RiskFinReportAnalyResource {
    @Autowired
    private RiskFinReportAnalyService riskFinReportAnalyService;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RiskFinReportAnaly>> query() {
        QueryModel queryModel = new QueryModel();
        List<RiskFinReportAnaly> list = riskFinReportAnalyService.selectAll(queryModel);
        return new ResultDto<List<RiskFinReportAnaly>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RiskFinReportAnaly>> index(QueryModel queryModel) {
        List<RiskFinReportAnaly> list = riskFinReportAnalyService.selectByModel(queryModel);
        return new ResultDto<List<RiskFinReportAnaly>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<RiskFinReportAnaly>> queryList(@RequestBody QueryModel queryModel) throws URISyntaxException {
        List<RiskFinReportAnaly> lists = riskFinReportAnalyService.selectByModel(queryModel);
        if (null != lists && lists.size() > 0) {
            for (RiskFinReportAnaly riskFinReportAnaly : lists) {
                boolean saveFlag = false;
                if (Objects.isNull(riskFinReportAnaly.getL1yRiseRate())) {
                    BigDecimal curtValue = new BigDecimal(riskFinReportAnaly.getCurtValue());// 本期数据
                    BigDecimal l1yValue = new BigDecimal(riskFinReportAnaly.getL1yValue());// 上一年数据
                    riskFinReportAnaly.setL1yRiseRate(curtValue.subtract(l1yValue).toPlainString());
                    saveFlag = true;
                }
                if (Objects.isNull(riskFinReportAnaly.getL2yRiseRate())) {
                    BigDecimal curtValue = new BigDecimal(riskFinReportAnaly.getCurtValue());// 本期数据
                    BigDecimal l2yValue = new BigDecimal(riskFinReportAnaly.getL2yValue());// 上两年数据
                    riskFinReportAnaly.setL2yRiseRate(curtValue.subtract(l2yValue).toPlainString());
                    saveFlag = true;
                }
                if (saveFlag) {
                    update(riskFinReportAnaly);
                }
            }
        }
        List<RiskFinReportAnaly> list = riskFinReportAnalyService.changeLists(lists);
        List<RiskFinReportAnaly> retList = riskFinReportAnalyService.dealFinaData();
        list.addAll(retList);
        List<RiskFinReportAnaly> ret = list.stream()
                .collect(Collectors.toMap(RiskFinReportAnaly::getIdxName, a -> a, (o1, o2) -> {
                    o1.setIdxOrder(o2.getIdxOrder());
                    return o1;
                })).values().stream().collect(Collectors.toList());
        for (int i = ret.size() - 1; i >= 0; i--) {
            if (ret.get(i).getIdxOrder() == null) {
                ret.remove(i);
            }
        }
        ret.sort(comparing(RiskFinReportAnaly::getIdxOrder));
        return new ResultDto<List<RiskFinReportAnaly>>(ret);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RiskFinReportAnaly> show(@PathVariable("pkId") String pkId) {
        RiskFinReportAnaly riskFinReportAnaly = riskFinReportAnalyService.selectByPrimaryKey(pkId);
        return new ResultDto<RiskFinReportAnaly>(riskFinReportAnaly);
    }

    /**
     * @函数名称:queryRiskFinReportAnaly
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<RiskFinReportAnaly> queryRiskFinReportAnaly(@RequestBody RiskFinReportAnaly riskFinReportAnaly) {
        riskFinReportAnaly = riskFinReportAnalyService.queryRiskFinReportAnaly(riskFinReportAnaly);
        return new ResultDto<RiskFinReportAnaly>(riskFinReportAnaly);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<RiskFinReportAnaly> create(@RequestBody RiskFinReportAnaly riskFinReportAnaly) throws URISyntaxException {
        riskFinReportAnalyService.insert(riskFinReportAnaly);
        return new ResultDto<RiskFinReportAnaly>(riskFinReportAnaly);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RiskFinReportAnaly riskFinReportAnaly) throws URISyntaxException {
        int result = riskFinReportAnalyService.update(riskFinReportAnaly);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = riskFinReportAnalyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = riskFinReportAnalyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
