/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskNonFinaAnaly
 * @类描述: risk_non_fina_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_non_fina_analy")
public class RiskNonFinaAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 外部宏观经济环境发生变化情况 **/
	@Column(name = "ECONOMY_CHANGE_CASE", unique = false, nullable = true, length = 5)
	private String economyChangeCase;
	
	/** 外部宏观经济环境说明 **/
	@Column(name = "CHANGE_CASE_EXPL", unique = false, nullable = true, length = 65535)
	private String changeCaseExpl;
	
	/** 行业风险 **/
	@Column(name = "TRADE_RISK", unique = false, nullable = true, length = 5)
	private String tradeRisk;
	
	/** 行业风险说明 **/
	@Column(name = "TRADE_RISK_EXPL", unique = false, nullable = true, length = 65535)
	private String tradeRiskExpl;
	
	/** 主要股东、关联公司或母子公司发生重大变化 **/
	@Column(name = "SHAREHOLDER_RELA_CHANGE", unique = false, nullable = true, length = 5)
	private String shareholderRelaChange;
	
	/** 主要股东、关联公司或母子公司变化说明 **/
	@Column(name = "RELA_CHANGE_EXPL", unique = false, nullable = true, length = 65535)
	private String relaChangeExpl;
	
	/** 借款人内部管理情况 **/
	@Column(name = "MANA_CASE", unique = false, nullable = true, length = 5)
	private String manaCase;
	
	/** 借款人内部管理情况说明 **/
	@Column(name = "MANA_CASE_EXPL", unique = false, nullable = true, length = 65535)
	private String manaCaseExpl;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param economyChangeCase
	 */
	public void setEconomyChangeCase(String economyChangeCase) {
		this.economyChangeCase = economyChangeCase;
	}
	
    /**
     * @return economyChangeCase
     */
	public String getEconomyChangeCase() {
		return this.economyChangeCase;
	}
	
	/**
	 * @param changeCaseExpl
	 */
	public void setChangeCaseExpl(String changeCaseExpl) {
		this.changeCaseExpl = changeCaseExpl;
	}
	
    /**
     * @return changeCaseExpl
     */
	public String getChangeCaseExpl() {
		return this.changeCaseExpl;
	}
	
	/**
	 * @param tradeRisk
	 */
	public void setTradeRisk(String tradeRisk) {
		this.tradeRisk = tradeRisk;
	}
	
    /**
     * @return tradeRisk
     */
	public String getTradeRisk() {
		return this.tradeRisk;
	}
	
	/**
	 * @param tradeRiskExpl
	 */
	public void setTradeRiskExpl(String tradeRiskExpl) {
		this.tradeRiskExpl = tradeRiskExpl;
	}
	
    /**
     * @return tradeRiskExpl
     */
	public String getTradeRiskExpl() {
		return this.tradeRiskExpl;
	}
	
	/**
	 * @param shareholderRelaChange
	 */
	public void setShareholderRelaChange(String shareholderRelaChange) {
		this.shareholderRelaChange = shareholderRelaChange;
	}
	
    /**
     * @return shareholderRelaChange
     */
	public String getShareholderRelaChange() {
		return this.shareholderRelaChange;
	}
	
	/**
	 * @param relaChangeExpl
	 */
	public void setRelaChangeExpl(String relaChangeExpl) {
		this.relaChangeExpl = relaChangeExpl;
	}
	
    /**
     * @return relaChangeExpl
     */
	public String getRelaChangeExpl() {
		return this.relaChangeExpl;
	}
	
	/**
	 * @param manaCase
	 */
	public void setManaCase(String manaCase) {
		this.manaCase = manaCase;
	}
	
    /**
     * @return manaCase
     */
	public String getManaCase() {
		return this.manaCase;
	}
	
	/**
	 * @param manaCaseExpl
	 */
	public void setManaCaseExpl(String manaCaseExpl) {
		this.manaCaseExpl = manaCaseExpl;
	}
	
    /**
     * @return manaCaseExpl
     */
	public String getManaCaseExpl() {
		return this.manaCaseExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}