/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspCusFinAnaly
 * @类描述: psp_cus_fin_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_cus_fin_analy")
public class PspCusFinAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 融资总额（万元） **/
	@Column(name = "TOTAL_FIN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalFinAmt;
	
	/** 支行催收情况 **/
	@Column(name = "COLLECT_SITU", unique = false, nullable = true, length = 65535)
	private String collectSitu;
	
	/** 借款人还款意愿 **/
	@Column(name = "REPAY_WISH", unique = false, nullable = true, length = 65535)
	private String repayWish;
	
	/** 整体融资分析 **/
	@Column(name = "ENTIRE_FIN_ANALY", unique = false, nullable = true, length = 65535)
	private String entireFinAnaly;
	
	/** 上期贷后检查时融资总额（万元） **/
	@Column(name = "PRE_FIN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preFinAmt;
	
	/** 本期贷后检查时融资总额（万元） **/
	@Column(name = "CURT_FIN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtFinAmt;
	
	/** 融资变化（万元） **/
	@Column(name = "FIN_CHANGE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal finChange;
	
	/** 融资变化情况分析 **/
	@Column(name = "FIN_CHANGE_ANALY", unique = false, nullable = true, length = 65535)
	private String finChangeAnaly;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param totalFinAmt
	 */
	public void setTotalFinAmt(java.math.BigDecimal totalFinAmt) {
		this.totalFinAmt = totalFinAmt;
	}
	
    /**
     * @return totalFinAmt
     */
	public java.math.BigDecimal getTotalFinAmt() {
		return this.totalFinAmt;
	}
	
	/**
	 * @param collectSitu
	 */
	public void setCollectSitu(String collectSitu) {
		this.collectSitu = collectSitu;
	}
	
    /**
     * @return collectSitu
     */
	public String getCollectSitu() {
		return this.collectSitu;
	}
	
	/**
	 * @param repayWish
	 */
	public void setRepayWish(String repayWish) {
		this.repayWish = repayWish;
	}
	
    /**
     * @return repayWish
     */
	public String getRepayWish() {
		return this.repayWish;
	}
	
	/**
	 * @param entireFinAnaly
	 */
	public void setEntireFinAnaly(String entireFinAnaly) {
		this.entireFinAnaly = entireFinAnaly;
	}
	
    /**
     * @return entireFinAnaly
     */
	public String getEntireFinAnaly() {
		return this.entireFinAnaly;
	}
	
	/**
	 * @param preFinAmt
	 */
	public void setPreFinAmt(java.math.BigDecimal preFinAmt) {
		this.preFinAmt = preFinAmt;
	}
	
    /**
     * @return preFinAmt
     */
	public java.math.BigDecimal getPreFinAmt() {
		return this.preFinAmt;
	}
	
	/**
	 * @param curtFinAmt
	 */
	public void setCurtFinAmt(java.math.BigDecimal curtFinAmt) {
		this.curtFinAmt = curtFinAmt;
	}
	
    /**
     * @return curtFinAmt
     */
	public java.math.BigDecimal getCurtFinAmt() {
		return this.curtFinAmt;
	}
	
	/**
	 * @param finChange
	 */
	public void setFinChange(java.math.BigDecimal finChange) {
		this.finChange = finChange;
	}
	
    /**
     * @return finChange
     */
	public java.math.BigDecimal getFinChange() {
		return this.finChange;
	}
	
	/**
	 * @param finChangeAnaly
	 */
	public void setFinChangeAnaly(String finChangeAnaly) {
		this.finChangeAnaly = finChangeAnaly;
	}
	
    /**
     * @return finChangeAnaly
     */
	public String getFinChangeAnaly() {
		return this.finChangeAnaly;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}