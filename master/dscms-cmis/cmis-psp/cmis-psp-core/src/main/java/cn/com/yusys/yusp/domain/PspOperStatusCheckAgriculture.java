/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckAgriculture
 * @类描述: psp_oper_status_check_agriculture数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_oper_status_check_agriculture")
public class PspOperStatusCheckAgriculture extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 是否受疫情影响 **/
	@Column(name = "IS_EPIDEMIC_IMP", unique = false, nullable = true, length = 5)
	private String isEpidemicImp;
	
	/** 疫情影响说明 **/
	@Column(name = "EPIDEMIC_EXPL", unique = false, nullable = true, length = 65535)
	private String epidemicExpl;
	
	/** 是否受天灾影响 **/
	@Column(name = "IS_WEATHER_IMP", unique = false, nullable = true, length = 5)
	private String isWeatherImp;
	
	/** 天灾影响说明 **/
	@Column(name = "WEATHER_EXPL", unique = false, nullable = true, length = 65535)
	private String weatherExpl;
	
	/** 主要产品价格是否异常波动 **/
	@Column(name = "PRICE_ABN", unique = false, nullable = true, length = 5)
	private String priceAbn;
	
	/** 价格异常波动说明 **/
	@Column(name = "PRICE_ABN_EXPL", unique = false, nullable = true, length = 65535)
	private String priceAbnExpl;
	
	/** 上期主要产品1 **/
	@Column(name = "PRE_PRD_ONE", unique = false, nullable = true, length = 80)
	private String prePrdOne;
	
	/** 上期产品价格1 **/
	@Column(name = "PRE_PRICE_ONE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal prePriceOne;
	
	/** 上期产品规模1 **/
	@Column(name = "PRE_SCALE_ONE", unique = false, nullable = true, length = 80)
	private String preScaleOne;
	
	/** 上期主要产品2 **/
	@Column(name = "PRE_PRD_TWO", unique = false, nullable = true, length = 80)
	private String prePrdTwo;
	
	/** 上期产品价格2 **/
	@Column(name = "PRE_PRICE_TWO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal prePriceTwo;
	
	/** 上期产品规模2 **/
	@Column(name = "PRE_SCALE_TWO", unique = false, nullable = true, length = 80)
	private String preScaleTwo;
	
	/** 上期主要产品3 **/
	@Column(name = "PRE_PRD_THREE", unique = false, nullable = true, length = 80)
	private String prePrdThree;
	
	/** 上期产品价格3 **/
	@Column(name = "PRE_PRICE_THREE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal prePriceThree;
	
	/** 上期产品规模3 **/
	@Column(name = "PRE_SCALE_THREE", unique = false, nullable = true, length = 80)
	private String preScaleThree;
	
	/** 本期主要产品1 **/
	@Column(name = "CURT_PRD_ONE", unique = false, nullable = true, length = 80)
	private String curtPrdOne;
	
	/** 本期产品价格1 **/
	@Column(name = "CURT_SCALE_ONE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtScaleOne;
	
	/** 本期产品规模1 **/
	@Column(name = "CURT_PRICE_ONE", unique = false, nullable = true, length = 80)
	private String curtPriceOne;
	
	/** 本期主要产品2 **/
	@Column(name = "CURT_PRD_TWO", unique = false, nullable = true, length = 80)
	private String curtPrdTwo;
	
	/** 本期产品价格2 **/
	@Column(name = "CURT_SCALE_TWO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtScaleTwo;
	
	/** 本期产品规模2 **/
	@Column(name = "CURT_PRICE_TWO", unique = false, nullable = true, length = 80)
	private String curtPriceTwo;
	
	/** 本期主要产品3 **/
	@Column(name = "CURT_PRD_THREE", unique = false, nullable = true, length = 80)
	private String curtPrdThree;
	
	/** 本期产品价格3 **/
	@Column(name = "CURT_SCALE_THREE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtScaleThree;
	
	/** 本期产品规模3 **/
	@Column(name = "CURT_PRICE_THREE", unique = false, nullable = true, length = 80)
	private String curtPriceThree;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isEpidemicImp
	 */
	public void setIsEpidemicImp(String isEpidemicImp) {
		this.isEpidemicImp = isEpidemicImp;
	}
	
    /**
     * @return isEpidemicImp
     */
	public String getIsEpidemicImp() {
		return this.isEpidemicImp;
	}
	
	/**
	 * @param epidemicExpl
	 */
	public void setEpidemicExpl(String epidemicExpl) {
		this.epidemicExpl = epidemicExpl;
	}
	
    /**
     * @return epidemicExpl
     */
	public String getEpidemicExpl() {
		return this.epidemicExpl;
	}
	
	/**
	 * @param isWeatherImp
	 */
	public void setIsWeatherImp(String isWeatherImp) {
		this.isWeatherImp = isWeatherImp;
	}
	
    /**
     * @return isWeatherImp
     */
	public String getIsWeatherImp() {
		return this.isWeatherImp;
	}
	
	/**
	 * @param weatherExpl
	 */
	public void setWeatherExpl(String weatherExpl) {
		this.weatherExpl = weatherExpl;
	}
	
    /**
     * @return weatherExpl
     */
	public String getWeatherExpl() {
		return this.weatherExpl;
	}
	
	/**
	 * @param priceAbn
	 */
	public void setPriceAbn(String priceAbn) {
		this.priceAbn = priceAbn;
	}
	
    /**
     * @return priceAbn
     */
	public String getPriceAbn() {
		return this.priceAbn;
	}
	
	/**
	 * @param priceAbnExpl
	 */
	public void setPriceAbnExpl(String priceAbnExpl) {
		this.priceAbnExpl = priceAbnExpl;
	}
	
    /**
     * @return priceAbnExpl
     */
	public String getPriceAbnExpl() {
		return this.priceAbnExpl;
	}
	
	/**
	 * @param prePrdOne
	 */
	public void setPrePrdOne(String prePrdOne) {
		this.prePrdOne = prePrdOne;
	}
	
    /**
     * @return prePrdOne
     */
	public String getPrePrdOne() {
		return this.prePrdOne;
	}
	
	/**
	 * @param prePriceOne
	 */
	public void setPrePriceOne(java.math.BigDecimal prePriceOne) {
		this.prePriceOne = prePriceOne;
	}
	
    /**
     * @return prePriceOne
     */
	public java.math.BigDecimal getPrePriceOne() {
		return this.prePriceOne;
	}
	
	/**
	 * @param preScaleOne
	 */
	public void setPreScaleOne(String preScaleOne) {
		this.preScaleOne = preScaleOne;
	}
	
    /**
     * @return preScaleOne
     */
	public String getPreScaleOne() {
		return this.preScaleOne;
	}
	
	/**
	 * @param prePrdTwo
	 */
	public void setPrePrdTwo(String prePrdTwo) {
		this.prePrdTwo = prePrdTwo;
	}
	
    /**
     * @return prePrdTwo
     */
	public String getPrePrdTwo() {
		return this.prePrdTwo;
	}
	
	/**
	 * @param prePriceTwo
	 */
	public void setPrePriceTwo(java.math.BigDecimal prePriceTwo) {
		this.prePriceTwo = prePriceTwo;
	}
	
    /**
     * @return prePriceTwo
     */
	public java.math.BigDecimal getPrePriceTwo() {
		return this.prePriceTwo;
	}
	
	/**
	 * @param preScaleTwo
	 */
	public void setPreScaleTwo(String preScaleTwo) {
		this.preScaleTwo = preScaleTwo;
	}
	
    /**
     * @return preScaleTwo
     */
	public String getPreScaleTwo() {
		return this.preScaleTwo;
	}
	
	/**
	 * @param prePrdThree
	 */
	public void setPrePrdThree(String prePrdThree) {
		this.prePrdThree = prePrdThree;
	}
	
    /**
     * @return prePrdThree
     */
	public String getPrePrdThree() {
		return this.prePrdThree;
	}
	
	/**
	 * @param prePriceThree
	 */
	public void setPrePriceThree(java.math.BigDecimal prePriceThree) {
		this.prePriceThree = prePriceThree;
	}
	
    /**
     * @return prePriceThree
     */
	public java.math.BigDecimal getPrePriceThree() {
		return this.prePriceThree;
	}
	
	/**
	 * @param preScaleThree
	 */
	public void setPreScaleThree(String preScaleThree) {
		this.preScaleThree = preScaleThree;
	}
	
    /**
     * @return preScaleThree
     */
	public String getPreScaleThree() {
		return this.preScaleThree;
	}
	
	/**
	 * @param curtPrdOne
	 */
	public void setCurtPrdOne(String curtPrdOne) {
		this.curtPrdOne = curtPrdOne;
	}
	
    /**
     * @return curtPrdOne
     */
	public String getCurtPrdOne() {
		return this.curtPrdOne;
	}
	
	/**
	 * @param curtScaleOne
	 */
	public void setCurtScaleOne(java.math.BigDecimal curtScaleOne) {
		this.curtScaleOne = curtScaleOne;
	}
	
    /**
     * @return curtScaleOne
     */
	public java.math.BigDecimal getCurtScaleOne() {
		return this.curtScaleOne;
	}
	
	/**
	 * @param curtPriceOne
	 */
	public void setCurtPriceOne(String curtPriceOne) {
		this.curtPriceOne = curtPriceOne;
	}
	
    /**
     * @return curtPriceOne
     */
	public String getCurtPriceOne() {
		return this.curtPriceOne;
	}
	
	/**
	 * @param curtPrdTwo
	 */
	public void setCurtPrdTwo(String curtPrdTwo) {
		this.curtPrdTwo = curtPrdTwo;
	}
	
    /**
     * @return curtPrdTwo
     */
	public String getCurtPrdTwo() {
		return this.curtPrdTwo;
	}
	
	/**
	 * @param curtScaleTwo
	 */
	public void setCurtScaleTwo(java.math.BigDecimal curtScaleTwo) {
		this.curtScaleTwo = curtScaleTwo;
	}
	
    /**
     * @return curtScaleTwo
     */
	public java.math.BigDecimal getCurtScaleTwo() {
		return this.curtScaleTwo;
	}
	
	/**
	 * @param curtPriceTwo
	 */
	public void setCurtPriceTwo(String curtPriceTwo) {
		this.curtPriceTwo = curtPriceTwo;
	}
	
    /**
     * @return curtPriceTwo
     */
	public String getCurtPriceTwo() {
		return this.curtPriceTwo;
	}
	
	/**
	 * @param curtPrdThree
	 */
	public void setCurtPrdThree(String curtPrdThree) {
		this.curtPrdThree = curtPrdThree;
	}
	
    /**
     * @return curtPrdThree
     */
	public String getCurtPrdThree() {
		return this.curtPrdThree;
	}
	
	/**
	 * @param curtScaleThree
	 */
	public void setCurtScaleThree(java.math.BigDecimal curtScaleThree) {
		this.curtScaleThree = curtScaleThree;
	}
	
    /**
     * @return curtScaleThree
     */
	public java.math.BigDecimal getCurtScaleThree() {
		return this.curtScaleThree;
	}
	
	/**
	 * @param curtPriceThree
	 */
	public void setCurtPriceThree(String curtPriceThree) {
		this.curtPriceThree = curtPriceThree;
	}
	
    /**
     * @return curtPriceThree
     */
	public String getCurtPriceThree() {
		return this.curtPriceThree;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}