package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskList
 * @类描述: psp_task_list数据实体类
 * @功能描述: 
 * @创建人: hanl
 * @创建时间: 2021-06-13 19:39:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspKHYJDto implements Serializable{
	private static final long serialVersionUID = 1L;
	

	/** 客户编号 **/
	private String cusId;
	
	/** 任务执行人 **/
	private String execId;
	
	/** 任务执行机构 **/
	private String execBrId;


	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getExecId() {
		return execId;
	}

	public void setExecId(String execId) {
		this.execId = execId;
	}

	public String getExecBrId() {
		return execBrId;
	}

	public void setExecBrId(String execBrId) {
		this.execBrId = execBrId;
	}
}