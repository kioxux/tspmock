/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;


import cn.com.yusys.yusp.domain.PspCusBaseCase;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.*;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.PspCusBaseCaseMapper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp-core模块
 * @类名称: PspCusBaseCaseService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspCusBaseCaseService {

    @Autowired
    private PspCusBaseCaseMapper pspCusBaseCaseMapper;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private Dscms2OuterdataClientService dscms2ImageClientService;

    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspCusBaseCase selectByPrimaryKey(String pkId) {
        return pspCusBaseCaseMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: queryPspCusBaseCase
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspCusBaseCase queryPspCusBaseCase(PspCusBaseCase pspCusBaseCase) {
        return pspCusBaseCaseMapper.queryPspCusBaseCase(pspCusBaseCase);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PspCusBaseCase> selectAll(QueryModel model) {
        List<PspCusBaseCase> records = (List<PspCusBaseCase>) pspCusBaseCaseMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     * @author: cainingbo
     */

    public List<PspCusBaseCase> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PspCusBaseCase> list = pspCusBaseCaseMapper.selectByModel(model);
        if (list != null && list.size() > 0) {
            for (PspCusBaseCase pspCusBaseCases : list) {
                String cusId = pspCusBaseCases.getCusId();
                ResultDto<CusIndivDto> cusIndivDto = cmisCusClientService.queryCusindivByCusid(cusId);
                if (cusIndivDto != null) {
                    pspCusBaseCases.setIsCom("1");//当为1是，为个人客户
                } else {
                    pspCusBaseCases.setIsCom("2");//当为2时，为对公客户
                }
            }
        }
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PspCusBaseCase record) {
        return pspCusBaseCaseMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PspCusBaseCase record) {
        return pspCusBaseCaseMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PspCusBaseCase record) {
        return pspCusBaseCaseMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PspCusBaseCase record) {
        int count = 0;
        count = pspCusBaseCaseMapper.queryPspCusByTaskNoCount(record.getTaskNo());
        if (count >0 ){
            pspCusBaseCaseMapper.updateByPrimaryKeySelective(record);
        }else {
            pspCusBaseCaseMapper.insertSelective(record);
        }
        return count;
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pspCusBaseCaseMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pspCusBaseCaseMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByTaskNo
     * @方法描述: 根据任务编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByTaskNo(String taskNo) {
        return pspCusBaseCaseMapper.deleteByTaskNo(taskNo);
    }

    /**
     * 定期贷后检查对公
     *
     * @param pspCusBaseCase
     * @return
     */
    public String checkInfo(PspCusBaseCase pspCusBaseCase) throws Exception {
        String result = "failure";
        ZsnewReqDto zsnewReqDto = new ZsnewReqDto();
        QueryModel model = new QueryModel();
        model.addCondition("cusId", pspCusBaseCase.getCusId());
        ResultDto<List<CusCorpDto>> cusCorpDtoList = cmisCusClientService.selectCusCorpDtoList(model);
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cusCorpDtoList.getCode())) {
            if (cusCorpDtoList.getData().size() > 0) {
                CusCorpDto cusCorpDto = new CusCorpDto();
                cusCorpDto = JSON.parseObject(JSON.toJSONString(cusCorpDtoList.getData().get(0)), new TypeReference<CusCorpDto>(){});
                String codeType = cusCorpDto.getCertType();
                if ("Q".equals(codeType)) {
                    zsnewReqDto.setOrgcode(cusCorpDto.getInsCode());     //组织机构代码 Q
                } else if ("R".equals(codeType)) {
                    zsnewReqDto.setCreditcode(cusCorpDto.getCertCode()); //统一信用代码 R
                }
                zsnewReqDto.setEntstatus(cusCorpDto.getOperStatus());  //企业经营状态，1：在营，2：非在营
                zsnewReqDto.setRegno(cusCorpDto.getRegiCode());    //企业注册号
            }
        }
        zsnewReqDto.setId(StringUtils.EMPTY);         //中数企业ID
        zsnewReqDto.setVersion(StringUtils.EMPTY);  //高管识别码版本号,返回高管识别码时生效
        zsnewReqDto.setName(pspCusBaseCase.getCusName());
        ResultDto<ZsnewRespDto> zsnewResultDto = dscms2OuterdataClientService.zsnew(zsnewReqDto);
        String zsnewCode = Optional.ofNullable(zsnewResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String zsnewMeesage = Optional.ofNullable(zsnewResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        ZsnewRespDto zsnewRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, zsnewResultDto.getCode())) {
            //  获取相关的值并解析
            zsnewRespDto = zsnewResultDto.getData();
            if (null != zsnewRespDto && null != zsnewRespDto.getData()) {
                Data data = zsnewRespDto.getData();
                if (null != data.getCODE() && "200".equals(data.getCODE())) {
                    //  TODO 校验客户名称、经营地址、股权结构、主营业务、注册资本(万元)、法人代表
                    Map<String, Object> mapCusBase = new HashMap<>();
                    mapCusBase.put("operAddrAct", pspCusBaseCase.getOperAddrAct());
                    //mapCusBase.put("ownshipStr", pspCusBaseCase.getOwnshipStr());
                    //mapCusBase.put("mainOptScp", pspCusBaseCase.getMainOptScp());
                    mapCusBase.put("regiCapAmt", pspCusBaseCase.getRegiCapAmt().toString());
                    mapCusBase.put("legalRepresent", pspCusBaseCase.getLegalRepresent());
                    ENT_INFO entInfo = (ENT_INFO) data.getENT_INFO();
                    BASIC basic = entInfo.getBASIC();
                    Map<String, Object> mapCheck = new HashMap<>();
                    mapCheck.put("operAddrAct", basic.getDOM());
//                mapCheck.put("ownshipStr", basic.getOwnshipStr());
//                mapCheck.put("mainOptScp", basic.getMainOptScp());
                    mapCheck.put("regiCapAmt", basic.getREGCAP());
                    mapCheck.put("legalRepresent", basic.getFRNAME());
                    if (!mapCusBase.equals(mapCheck)) {
                        pspCusBaseCase.setIsDataChg("1");
                        pspCusBaseCaseMapper.updateByPrimaryKeySelective(pspCusBaseCase);
                        result = "isChange";
                    }
                }
            }
        }
        return result;
    }

    /**
     * 定期贷后检查对个
     *
     * @param pspCusBaseCase
     * @return
     */
    public String checkCusIndiv(PspCusBaseCase pspCusBaseCase) {
        String result = "failure";
        // 第二步 校验客户信息  发送身份校验
        IdCheckReqDto idCheckReqDto = new IdCheckReqDto();
        // 证件类型 0101身份证
        idCheckReqDto.setCert_type("0101");//(cusIndivDto.getCertType()
        // 用户名称
        idCheckReqDto.setUser_name(pspCusBaseCase.getCusName());
        // 证件号码
        idCheckReqDto.setId_number(pspCusBaseCase.getCertCode());
        // 机构类型 2：银行营业厅
        idCheckReqDto.setSource_type("2");
        // 网上营业厅申请人IP地址  StringUtils.EMPTY 待确认
        idCheckReqDto.setIp("10.87.3.37");//StringUtils.EMPTY
        ResultDto<IdCheckRespDto> idCheckResultDto = dscms2ImageClientService.idCheck(idCheckReqDto);
        String idCheckCode = Optional.ofNullable(idCheckResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String idCheckMeesage = Optional.ofNullable(idCheckResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        IdCheckRespDto idCheckRespDto = null;
        if (Objects.equals(idCheckCode, SuccessEnum.CMIS_SUCCSESS.key)) {
            //  获取相关的值并解析
            idCheckRespDto = idCheckResultDto.getData();
            if (!EcsEnum.CUS_INDIV_CHECK_RESULT00.key.equals(idCheckRespDto.getCheckresult())) {
                pspCusBaseCase.setIsDataChg("1");
                pspCusBaseCaseMapper.updateByPrimaryKeySelective(pspCusBaseCase);
                result = "isChange";
            }
        }
        return result;
    }

    /**
     * @方法名称: queryPspCusBaseCase
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspCusBaseCase queryPspCusByTaskNo(String taskNo) {

        return pspCusBaseCaseMapper.queryPspCusByTaskNo(taskNo);
    }
}
