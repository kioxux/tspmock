/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPldimnList
 * @类描述: psp_pldimn_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_pldimn_list")
public class PspPldimnList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;

	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = false, length = 40)
	private String guarNo;

	/** 押品所有人编号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = false, length = 40)
	private String guarCusId;

	/** 押品所有人名称 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = false, length = 100)
	private String guarCusName;

	/** 押品所有人证件类型 **/
	@Column(name = "GUAR_CERT_TYPE", unique = false, nullable = false, length = 10)
	private String guarCertType;

	/** 押品所有人证件号码 **/
	@Column(name = "GUAR_CERT_CODE", unique = false, nullable = false, length = 40)
	private String guarCertCode;
	
	/** 抵/质押物名称 **/
	@Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 80)
	private String pldimnMemo;
	
	/** 抵/质押物位置 **/
	@Column(name = "GUAR_ADDR", unique = false, nullable = true, length = 80)
	private String guarAddr;
	
	/** 权利金额 **/
	@Column(name = "CONFIRM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal confirmAmt;
	
	/** 评估金额 **/
	@Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalAmt;
	
	/** 担保标志 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String guarType;
	
	/** 抵/质押物余值 **/
	@Column(name = "PLDIMN_REMAIN_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pldimnRemainValue;
	
	/** 是否被有关机关查封、冻结、抵押 **/
	@Column(name = "IS_CLOSE", unique = false, nullable = true, length = 5)
	private String isClose;
	
	/** 抵/质押物说明 **/
	@Column(name = "GUAR_REMARK", unique = false, nullable = true, length = 65535)
	private String guarRemark;
	
	/** 押品状态说明 **/
	@Column(name = "GUAR_STS_REMARK", unique = false, nullable = true, length = 65535)
	private String guarStsRemark;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}
	
    /**
     * @return pldimnMemo
     */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarAddr
	 */
	public void setGuarAddr(String guarAddr) {
		this.guarAddr = guarAddr;
	}
	
    /**
     * @return guarAddr
     */
	public String getGuarAddr() {
		return this.guarAddr;
	}
	
	/**
	 * @param confirmAmt
	 */
	public void setConfirmAmt(java.math.BigDecimal confirmAmt) {
		this.confirmAmt = confirmAmt;
	}
	
    /**
     * @return confirmAmt
     */
	public java.math.BigDecimal getConfirmAmt() {
		return this.confirmAmt;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return evalAmt
     */
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param pldimnRemainValue
	 */
	public void setPldimnRemainValue(java.math.BigDecimal pldimnRemainValue) {
		this.pldimnRemainValue = pldimnRemainValue;
	}
	
    /**
     * @return pldimnRemainValue
     */
	public java.math.BigDecimal getPldimnRemainValue() {
		return this.pldimnRemainValue;
	}
	
	/**
	 * @param isClose
	 */
	public void setIsClose(String isClose) {
		this.isClose = isClose;
	}
	
    /**
     * @return isClose
     */
	public String getIsClose() {
		return this.isClose;
	}
	
	/**
	 * @param guarRemark
	 */
	public void setGuarRemark(String guarRemark) {
		this.guarRemark = guarRemark;
	}
	
    /**
     * @return guarRemark
     */
	public String getGuarRemark() {
		return this.guarRemark;
	}
	
	/**
	 * @param guarStsRemark
	 */
	public void setGuarStsRemark(String guarStsRemark) {
		this.guarStsRemark = guarStsRemark;
	}
	
    /**
     * @return guarStsRemark
     */
	public String getGuarStsRemark() {
		return this.guarStsRemark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getGuarNo() {
		return guarNo;
	}

	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}

	public String getGuarCusId() {
		return guarCusId;
	}

	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}

	public String getGuarCusName() {
		return guarCusName;
	}

	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}

	public String getGuarCertType() {
		return guarCertType;
	}

	public void setGuarCertType(String guarCertType) {
		this.guarCertType = guarCertType;
	}

	public String getGuarCertCode() {
		return guarCertCode;
	}

	public void setGuarCertCode(String guarCertCode) {
		this.guarCertCode = guarCertCode;
	}
}