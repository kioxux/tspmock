/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskDebitAnaly
 * @类描述: risk_debit_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_debit_analy")
public class RiskDebitAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 销售收入变化趋势 **/
	@Column(name = "INCOME_CHANGE", unique = false, nullable = true, length = 5)
	private String incomeChange;
	
	/** 利润变动趋势 **/
	@Column(name = "PROFIT_CHANGE", unique = false, nullable = true, length = 5)
	private String profitChange;
	
	/** 现金流量变动趋势 **/
	@Column(name = "CASH_CHANGE", unique = false, nullable = true, length = 5)
	private String cashChange;
	
	/** 经营活动现金流量变动趋势 **/
	@Column(name = "OPER_CASH_CHANGE", unique = false, nullable = true, length = 5)
	private String operCashChange;
	
	/** 是否按约定用途使用贷款 **/
	@Column(name = "IS_RIGHT_PURP", unique = false, nullable = true, length = 5)
	private String isRightPurp;
	
	/** 贷款用途使用说明 **/
	@Column(name = "LOAN_PURP_DESC", unique = false, nullable = true, length = 65535)
	private String loanPurpDesc;
	
	/** 有无不良行为、不良嗜好 **/
	@Column(name = "IS_BAD_ACTION", unique = false, nullable = true, length = 5)
	private String isBadAction;
	
	/** 不良行为嗜好说明 **/
	@Column(name = "BAD_ACTION_DESC", unique = false, nullable = true, length = 65535)
	private String badActionDesc;
	
	/** 有无不良信用记录 **/
	@Column(name = "IS_BAD_CDT_RECORD", unique = false, nullable = true, length = 5)
	private String isBadCdtRecord;
	
	/** 家庭状况是否正确 **/
	@Column(name = "IS_FAMILY_STATUS_NORMAL", unique = false, nullable = true, length = 5)
	private String isFamilyStatusNormal;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param incomeChange
	 */
	public void setIncomeChange(String incomeChange) {
		this.incomeChange = incomeChange;
	}
	
    /**
     * @return incomeChange
     */
	public String getIncomeChange() {
		return this.incomeChange;
	}
	
	/**
	 * @param profitChange
	 */
	public void setProfitChange(String profitChange) {
		this.profitChange = profitChange;
	}
	
    /**
     * @return profitChange
     */
	public String getProfitChange() {
		return this.profitChange;
	}
	
	/**
	 * @param cashChange
	 */
	public void setCashChange(String cashChange) {
		this.cashChange = cashChange;
	}
	
    /**
     * @return cashChange
     */
	public String getCashChange() {
		return this.cashChange;
	}
	
	/**
	 * @param operCashChange
	 */
	public void setOperCashChange(String operCashChange) {
		this.operCashChange = operCashChange;
	}
	
    /**
     * @return operCashChange
     */
	public String getOperCashChange() {
		return this.operCashChange;
	}
	
	/**
	 * @param isRightPurp
	 */
	public void setIsRightPurp(String isRightPurp) {
		this.isRightPurp = isRightPurp;
	}
	
    /**
     * @return isRightPurp
     */
	public String getIsRightPurp() {
		return this.isRightPurp;
	}
	
	/**
	 * @param loanPurpDesc
	 */
	public void setLoanPurpDesc(String loanPurpDesc) {
		this.loanPurpDesc = loanPurpDesc;
	}
	
    /**
     * @return loanPurpDesc
     */
	public String getLoanPurpDesc() {
		return this.loanPurpDesc;
	}
	
	/**
	 * @param isBadAction
	 */
	public void setIsBadAction(String isBadAction) {
		this.isBadAction = isBadAction;
	}
	
    /**
     * @return isBadAction
     */
	public String getIsBadAction() {
		return this.isBadAction;
	}
	
	/**
	 * @param badActionDesc
	 */
	public void setBadActionDesc(String badActionDesc) {
		this.badActionDesc = badActionDesc;
	}
	
    /**
     * @return badActionDesc
     */
	public String getBadActionDesc() {
		return this.badActionDesc;
	}
	
	/**
	 * @param isBadCdtRecord
	 */
	public void setIsBadCdtRecord(String isBadCdtRecord) {
		this.isBadCdtRecord = isBadCdtRecord;
	}
	
    /**
     * @return isBadCdtRecord
     */
	public String getIsBadCdtRecord() {
		return this.isBadCdtRecord;
	}
	
	/**
	 * @param isFamilyStatusNormal
	 */
	public void setIsFamilyStatusNormal(String isFamilyStatusNormal) {
		this.isFamilyStatusNormal = isFamilyStatusNormal;
	}
	
    /**
     * @return isFamilyStatusNormal
     */
	public String getIsFamilyStatusNormal() {
		return this.isFamilyStatusNormal;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}