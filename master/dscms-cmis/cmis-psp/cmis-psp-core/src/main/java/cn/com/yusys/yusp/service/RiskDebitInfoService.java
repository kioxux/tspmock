/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.RiskGuarContAnaly;
import cn.com.yusys.yusp.domain.RiskPldimnList;
import cn.com.yusys.yusp.domain.RiskTaskList;
//import cn.com.yusys.yusp.dto.GrtGuarContClientDto;
import cn.com.yusys.yusp.dto.RiskDebitAndLoanDto;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RiskDebitInfo;
import cn.com.yusys.yusp.repository.mapper.RiskDebitInfoMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskDebitInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-18 01:58:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RiskDebitInfoService {
    private static final Logger logger = LoggerFactory.getLogger(RiskDebitInfoService.class);
    @Autowired
    private RiskDebitInfoMapper riskDebitInfoMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
//    @Resource
//    private CmisBizClientService cmisBizClientService;
    @Autowired
    private RiskGuarContAnalyService riskGuarContAnalyService;
    @Autowired
    private RiskTaskListService riskTaskListService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskDebitInfo selectByPrimaryKey(String pkId) {
        return riskDebitInfoMapper.selectByPrimaryKey(pkId);
    }
    /**
     * @方法名称: queryRiskDebitInfo
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskDebitInfo queryRiskDebitInfo(RiskDebitInfo riskDebitInfo) {
        return riskDebitInfoMapper.queryRiskDebitInfo(riskDebitInfo);
    }
    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RiskDebitInfo> selectAll(QueryModel model) {
        List<RiskDebitInfo> records = (List<RiskDebitInfo>) riskDebitInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RiskDebitInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RiskDebitInfo> list = riskDebitInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RiskDebitAndLoanDto> selectRiskDebitAndLoan(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RiskDebitAndLoanDto> list = riskDebitInfoMapper.selectRiskDebitAndLoan(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RiskDebitInfo record) {
        return riskDebitInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RiskDebitInfo record) {
        return riskDebitInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RiskDebitInfo record) {
        return riskDebitInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateCheckStatus
     * @方法描述: 更新解析状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateCheckStatus(RiskDebitInfo record) {
        return riskDebitInfoMapper.updateCheckStatus(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RiskDebitInfo record) {
        return riskDebitInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return riskDebitInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return riskDebitInfoMapper.deleteByIds(ids);
    }

    /**
     * 根据任务编号获取借据号
     * @author jijian_yx
     * @date 2021/10/13 21:28
     **/
    public List<String> getBillNoByTaskNo(String taskNo) {
        return riskDebitInfoMapper.getBillNoByTaskNo(taskNo);
    }

    /**
     * 取借据中最低五级分类结果
     * @author jijian_yx
     * @date 2021/10/22 0:02
     **/
    public String getLowFiveClassByTaskNo(String taskNo) {
        return riskDebitInfoMapper.getLowFiveClassByTaskNo(taskNo);
    }

//    public int process(String billNo) {
//        try {
//            RiskDebitInfo riskDebitInfo = new RiskDebitInfo();
//            riskDebitInfo.setBillNo(billNo);
//            riskDebitInfo = riskDebitInfoMapper.queryRiskDebitInfo(riskDebitInfo);
//            String cusId = riskDebitInfo.getCusId();
//            String cusName = riskDebitInfo.getCusName();
//            String loanType = riskDebitInfo.getLoanType();
//            String isLowRisk = riskDebitInfo.getIsLowRisk();
//            String taskNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, new HashMap<>());
//            //TODO 系统日期获取
//            String openday = stringRedisTemplate.opsForValue().get("openDay");
//            //判断是否对公客户
//            if(cusId.startsWith("8"))
//            {
//                List<String> contList = new ArrayList<>();
//                if("Y".equals(isLowRisk)){
//                    riskDebitInfo = new RiskDebitInfo();
//                    riskDebitInfo.setCusId(cusId);
//                    riskDebitInfo.setIsLowRisk(isLowRisk);
//                    riskDebitInfo.setLoanType(loanType);
//                    riskDebitInfo = riskDebitInfoMapper.queryRiskDebitInfo(riskDebitInfo);
//                    riskDebitInfo.setRiskDate(openday);
//                    riskDebitInfo.setTaskNo(taskNo);
//                    riskDebitInfo.setPkId(null);
//                    contList.add(riskDebitInfo.getContNo());
//                    int count = riskDebitInfoMapper.insert(riskDebitInfo);
//                    if(count >= 1){
//                        ResultDto<List<GrtGuarContClientDto>> grtMap = cmisBizClientService.queryGrtGuarContByContNo(contList);
//                        List<GrtGuarContClientDto> grtList = new ArrayList<>();
//                        if("0".equals(grtMap.getCode())){
//                            grtList  = grtMap.getData();
//                        }
//                        if(grtList.size()>0){
//                            for (GrtGuarContClientDto grtGuarContClientDto: grtList) {
//                                RiskGuarContAnaly riskGuarContAnaly = new RiskGuarContAnaly();
//                                riskGuarContAnaly.setTaskNo(taskNo);
//                                riskGuarContAnaly.setGuarContNo(grtGuarContClientDto.getGuarContNo());
//                                riskGuarContAnaly.setGuarMode(grtGuarContClientDto.getGuarWay());
//                                riskGuarContAnaly.setGuarAmt(grtGuarContClientDto.getGuarAmt());
//                                riskGuarContAnalyService.insert(riskGuarContAnaly);
//                                RiskPldimnList riskPldimnList = new RiskPldimnList();
//                                riskPldimnList.setPldimnNo(grtGuarContClientDto.getGuarNo());
//                                //TODO 等待cmis-biz提供根据押品编号查询押品信息api，对应/api/guarbaseinfo/getGuarBaseInfo
//                            }
//                        }
//                        Date date = DateUtils.parseDate(openday, "yyyy-MM-dd");
//                        Date startDate = DateUtils.addMonth(date,-6);
//                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");//日期格式
//                        String bdate = format.format(startDate);
//                        //从分类历史表中找出该客户最近6个月内的分类
//                        RiskTaskList riskTaskList = new RiskTaskList();
//                        riskTaskList.setCusId(cusId);
//                        riskTaskList.setCheckType("1");
//                        riskTaskList.setCheckDate(bdate);
//                        riskTaskList = riskTaskListService.queryRiskTaskList(riskTaskList);
//                        //参考的上次分类的任务号
//                        String referenceTaskNo = riskTaskList.getTaskNo();
//                        String lastClaTen = riskTaskList.getManualTenClass();
//                        //如果此客户存在最近6个月内的低风险分类，则直接使用其作为本次分类结果；否则，默认为"正常1"
//                        String thisClaTen = lastClaTen != null?lastClaTen:"11";
//                        //此处需翻译字典项
//                        String autoRateReason = lastClaTen != null?"参考上一次的分类结果为【" + lastClaTen + "】" :
//                                "未能从历史表中找出其上次分类结果，默认为【正常1】";
//                    }
//                }else{
//
//                }
//            }else{
//
//            }
//        }catch (YuspException e) {
//            logger.error("风险分类接口出错：", e);
//        }
//        return 0;
//    }

}
