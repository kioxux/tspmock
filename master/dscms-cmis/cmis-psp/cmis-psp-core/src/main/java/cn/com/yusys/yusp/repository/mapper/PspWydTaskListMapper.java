/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PspWydTaskList;
import cn.com.yusys.yusp.dto.PspWydBackTjListDto;
import cn.com.yusys.yusp.dto.PspWydTackTjListDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWydTaskListMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: likc
 * @创建时间: 2021-06-11 14:17:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PspWydTaskListMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询pspwydtasklist
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    PspWydTaskList selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByTaskNo
     * @方法描述: 根据主键查询pspwydtasklist
     * @参数与返回说明:
     * @算法描述: 无
     */

    PspWydTaskList selectByTaskNo(@Param("taskNo") String taskNo);

    /**
     * @方法名称: queryPspWydTaskList
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    PspWydTaskList queryPspWydTaskList(PspWydTaskList record);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<PspWydTaskList> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(PspWydTaskList record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(PspWydTaskList record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(PspWydTaskList record);

    /**
     * @方法名称: updateByTaskNo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByTaskNo(PspWydTaskList record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(PspWydTaskList record);

    /**
     * @方法名称: updateByTaskNoKey
     * @方法描述: 根据任务编号更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int  updateByTaskNoKey(PspWydTaskList pspWydTaskList);

    /**
     * @方法名称: pspWydTackTjList
     * @方法描述: 贷后正常任务统计查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PspWydTackTjListDto> pspWydTackTjList(QueryModel model);

    /**
     * @函数名称:queryTackCompleteNum
     * @函数描述:查询该机构号累计完成数量（过程营销平台操作完成）
     * @参数与返回说明:
     * @算法描述:
     */
    String queryTackCompleteNum(QueryModel queryModel);

    /**
     * @函数名称:queryTackIncompleteNum
     * @函数描述:查询未完成贷后任务数量
     * @参数与返回说明:
     * @算法描述:
     */
    String queryTackIncompleteNum(QueryModel queryModel);

    /**
     * @函数名称:queryTackNormalNum
     * @函数描述:查询正常时间处理贷后数量
     * @参数与返回说明:
     * @算法描述:
     */
    String queryTackNormalNum(QueryModel queryModel);

    /**
     * @函数名称:queryTackAbnormalNum
     * @函数描述:查询逾期时间处理贷后数量
     * @参数与返回说明:
     * @算法描述:
     */
    String queryTackAbnormalNum(QueryModel queryModel);
}