/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskTaskList
 * @类描述: risk_task_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-30 00:14:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_task_list")
public class RiskTaskList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 任务类型 **/
	@Column(name = "TASK_TYPE", unique = false, nullable = true, length = 5)
	private String taskType;
	
	/** 分类模型 **/
	@Column(name = "CHECK_TYPE", unique = false, nullable = true, length = 5)
	private String checkType;
	
	/** 客户类型 **/
	@Column(name = "CUS_CATALOG", unique = false, nullable = true, length = 5)
	private String cusCatalog;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 手工分类 **/
	@Column(name = "MANUAL_CLASS", unique = false, nullable = true, length = 5)
	private String manualClass;
	
	/** 手工十级分类结果 **/
	@Column(name = "MANUAL_TEN_CLASS", unique = false, nullable = true, length = 5)
	private String manualTenClass;
	
	/** 人工分类理由 **/
	@Column(name = "MANUAL_CLASS_REASON", unique = false, nullable = true, length = 65535)
	private String manualClassReason;
	
	/** 机评分类 **/
	@Column(name = "AUTO_CLASS", unique = false, nullable = true, length = 5)
	private String autoClass;
	
	/** 机评十级分类结果 **/
	@Column(name = "AUTO_TEN_CLASS", unique = false, nullable = true, length = 5)
	private String autoTenClass;
	
	/** 机评分类理由 **/
	@Column(name = "AUTO_CLASS_REASON", unique = false, nullable = true, length = 65535)
	private String autoClassReason;
	
	/** 实际分类日期 **/
	@Column(name = "CHECK_DATE", unique = false, nullable = true, length = 20)
	private String checkDate;
	
	/** 上次分类结果 **/
	@Column(name = "LAST_CLASS_RST", unique = false, nullable = true, length = 5)
	private String lastClassRst;
	
	/** 上次十级分类结果 **/
	@Column(name = "LAST_TEN_CLASS_RST", unique = false, nullable = true, length = 5)
	private String lastTenClassRst;
	
	/** 上次分类任务编号 **/
	@Column(name = "LAST_TASK_NO", unique = false, nullable = true, length = 40)
	private String lastTaskNo;
	
	/** 上次分类日期 **/
	@Column(name = "LAST_CHECK_DATE", unique = false, nullable = true, length = 20)
	private String lastCheckDate;
	
	/** 风险分类得分 **/
	@Column(name = "RISK_SCORE", unique = false, nullable = true, length = 10)
	private Integer riskScore;
	
	/** 任务生成日期 **/
	@Column(name = "TASK_START_DT", unique = false, nullable = true, length = 20)
	private String taskStartDt;
	
	/** 要求完成日期 **/
	@Column(name = "TASK_END_DT", unique = false, nullable = true, length = 20)
	private String taskEndDt;
	
	/** 任务说明 **/
	@Column(name = "TASK_REMARKS", unique = false, nullable = true, length = 255)
	private String taskRemarks;
	
	/** 任务执行人 **/
	@Column(name = "EXEC_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="execIdName")
	private String execId;
	
	/** 任务执行机构 **/
	@Column(name = "EXEC_BR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName" ,refFieldName="execBrIdName")
	private String execBrId;
	
	/** 任务状态 **/
	@Column(name = "CHECK_STATUS", unique = false, nullable = true, length = 5)
	private String checkStatus;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 生成原因 **/
	@Column(name = "CHECK_REASON", unique = false, nullable = true, length = 65535)
	private String checkReason;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 任务状态(0:未生成;1:生成中;2:已生成) **/
	@Column(name = "task_status", unique = false, nullable = true, length = 5)
	private String taskStatus;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	
    /**
     * @return taskType
     */
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param checkType
	 */
	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}
	
    /**
     * @return checkType
     */
	public String getCheckType() {
		return this.checkType;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog;
	}
	
    /**
     * @return cusCatalog
     */
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param manualClass
	 */
	public void setManualClass(String manualClass) {
		this.manualClass = manualClass;
	}
	
    /**
     * @return manualClass
     */
	public String getManualClass() {
		return this.manualClass;
	}
	
	/**
	 * @param manualTenClass
	 */
	public void setManualTenClass(String manualTenClass) {
		this.manualTenClass = manualTenClass;
	}
	
    /**
     * @return manualTenClass
     */
	public String getManualTenClass() {
		return this.manualTenClass;
	}
	
	/**
	 * @param manualClassReason
	 */
	public void setManualClassReason(String manualClassReason) {
		this.manualClassReason = manualClassReason;
	}
	
    /**
     * @return manualClassReason
     */
	public String getManualClassReason() {
		return this.manualClassReason;
	}
	
	/**
	 * @param autoClass
	 */
	public void setAutoClass(String autoClass) {
		this.autoClass = autoClass;
	}
	
    /**
     * @return autoClass
     */
	public String getAutoClass() {
		return this.autoClass;
	}
	
	/**
	 * @param autoTenClass
	 */
	public void setAutoTenClass(String autoTenClass) {
		this.autoTenClass = autoTenClass;
	}
	
    /**
     * @return autoTenClass
     */
	public String getAutoTenClass() {
		return this.autoTenClass;
	}
	
	/**
	 * @param autoClassReason
	 */
	public void setAutoClassReason(String autoClassReason) {
		this.autoClassReason = autoClassReason;
	}
	
    /**
     * @return autoClassReason
     */
	public String getAutoClassReason() {
		return this.autoClassReason;
	}
	
	/**
	 * @param checkDate
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	
    /**
     * @return checkDate
     */
	public String getCheckDate() {
		return this.checkDate;
	}
	
	/**
	 * @param lastClassRst
	 */
	public void setLastClassRst(String lastClassRst) {
		this.lastClassRst = lastClassRst;
	}
	
    /**
     * @return lastClassRst
     */
	public String getLastClassRst() {
		return this.lastClassRst;
	}
	
	/**
	 * @param lastTenClassRst
	 */
	public void setLastTenClassRst(String lastTenClassRst) {
		this.lastTenClassRst = lastTenClassRst;
	}
	
    /**
     * @return lastTenClassRst
     */
	public String getLastTenClassRst() {
		return this.lastTenClassRst;
	}
	
	/**
	 * @param lastTaskNo
	 */
	public void setLastTaskNo(String lastTaskNo) {
		this.lastTaskNo = lastTaskNo;
	}
	
    /**
     * @return lastTaskNo
     */
	public String getLastTaskNo() {
		return this.lastTaskNo;
	}
	
	/**
	 * @param lastCheckDate
	 */
	public void setLastCheckDate(String lastCheckDate) {
		this.lastCheckDate = lastCheckDate;
	}
	
    /**
     * @return lastCheckDate
     */
	public String getLastCheckDate() {
		return this.lastCheckDate;
	}
	
	/**
	 * @param riskScore
	 */
	public void setRiskScore(Integer riskScore) {
		this.riskScore = riskScore;
	}
	
    /**
     * @return riskScore
     */
	public Integer getRiskScore() {
		return this.riskScore;
	}
	
	/**
	 * @param taskStartDt
	 */
	public void setTaskStartDt(String taskStartDt) {
		this.taskStartDt = taskStartDt;
	}
	
    /**
     * @return taskStartDt
     */
	public String getTaskStartDt() {
		return this.taskStartDt;
	}
	
	/**
	 * @param taskEndDt
	 */
	public void setTaskEndDt(String taskEndDt) {
		this.taskEndDt = taskEndDt;
	}
	
    /**
     * @return taskEndDt
     */
	public String getTaskEndDt() {
		return this.taskEndDt;
	}
	
	/**
	 * @param taskRemarks
	 */
	public void setTaskRemarks(String taskRemarks) {
		this.taskRemarks = taskRemarks;
	}
	
    /**
     * @return taskRemarks
     */
	public String getTaskRemarks() {
		return this.taskRemarks;
	}
	
	/**
	 * @param execId
	 */
	public void setExecId(String execId) {
		this.execId = execId;
	}
	
    /**
     * @return execId
     */
	public String getExecId() {
		return this.execId;
	}
	
	/**
	 * @param execBrId
	 */
	public void setExecBrId(String execBrId) {
		this.execBrId = execBrId;
	}
	
    /**
     * @return execBrId
     */
	public String getExecBrId() {
		return this.execBrId;
	}
	
	/**
	 * @param checkStatus
	 */
	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}
	
    /**
     * @return checkStatus
     */
	public String getCheckStatus() {
		return this.checkStatus;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param checkReason
	 */
	public void setCheckReason(String checkReason) {
		this.checkReason = checkReason;
	}
	
    /**
     * @return checkReason
     */
	public String getCheckReason() {
		return this.checkReason;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param taskStatus
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	
    /**
     * @return taskStatus
     */
	public String getTaskStatus() {
		return this.taskStatus;
	}


}