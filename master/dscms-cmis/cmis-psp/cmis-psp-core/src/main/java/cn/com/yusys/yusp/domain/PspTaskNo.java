package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称:
 * @类名称:
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: zhaoyue
 * @创建时间: 2021/6/4 10:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspTaskNo {
    private static final long serialVersionUID = 1L;

    /** 任务编号 **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
    private String taskNo;

    /** 客户名称 **/
    @Column(name = "CUS_NAME", unique = false, nullable = false, length = 100)
    private String cusName;

    /** 客户编号 **/
    @Column(name = "CUS_ID", unique = false, nullable = false, length = 40)
    private String cusId;

    /** 模型编号 **/
    @Column(name = "MODE_ID", unique = false, nullable = true, length = 5)
    private String modeId;

    /** 检查类型 **/
    @Column(name = "CHEK_TYPE", unique = false, nullable = true, length = 10)
    private String chekType;

    /** 授信品种 **/
    @Column(name = "LMT_TYPE", unique = false, nullable = true, length = 10)
    private String lmtType;

    /** 合同编号 **/
    @Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
    private String contNo;

    /** 借据编号 **/
    @Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
    private String billNo;

    /** 检查数量 **/
    @Column(name = "CHECK_NUM", unique = false, nullable = true, length = 18)
    private BigDecimal checkNum;

    /** 检查金额 **/
    @Column(name = "CHECK_AMT", unique = false, nullable = true, length = 18)
    private BigDecimal checkAmt;

    /** 任务执行人 **/
    @Column(name = "TSK_EXE_ID", unique = false, nullable = true, length = 20)
    private String tskExeId;

    /** 任务执行机构 **/
    @Column(name = "TSK_EXE_BR_ID", unique = false, nullable = true, length = 20)
    private String tskExeBrId;

    /** 任务生成日期 **/
    @Column(name = "TSK_CREATE_DATE", unique = false, nullable = true, length = 20)
    private String tskCreateDate;

    /** 要求完成日期 **/
    @Column(name = "TSK_FINISH_DATE", unique = false, nullable = true, length = 20)
    private String tskFinishDate;

    /** 计划完成日期 **/
    @Column(name = "TSK_PLAN_DATE", unique = false, nullable = true, length = 20)
    private String tskPlanDate;

    /** 检查日期 **/
    @Column(name = "CHEK_DATE", unique = false, nullable = true, length = 20)
    private String chekDate;

    /** 检查方式 **/
    @Column(name = "CHEK_MODE", unique = false, nullable = true, length = 5)
    private String chekMode;

    /** 检查状态 **/
    @Column(name = "CHEK_STATUS", unique = false, nullable = true, length = 5)
    private Integer chekStatus;

    /** 审批状态 **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /** 资金流向是否异常 **/
    @Column(name = "IS_ABN_CURFUND", unique = false, nullable = true, length = 5)
    private String isAbnCurfund;

    /** 是否自动化 **/
    @Column(name = "IS_AUTO", unique = false, nullable = true, length = 5)
    private String isAuto;

    /** 业务流水号 **/
    @Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 40)
    private String bizSerno;

    /** 放款时间 **/
    @Column(name = "DISB_TIME", unique = false, nullable = true, length = 20)
    private String disbTime;

    /** 放款金额（元） **/
    @Column(name = "DISB_AMT", unique = false, nullable = true, length = 18)
    private BigDecimal disbAmt;

    /** 是否总行分配 **/
    @Column(name = "IS_HO_DIVIS", unique = false, nullable = true, length = 5)
    private String isHoDivis;

    /** 是否总行分配 **/
    @Column(name = "COUNTNUM", unique = false, nullable = true, length = 100)
    private String countnum;

    public String getCountnum() {
        return countnum;
    }

    public void setCountnum(String countnum) {
        this.countnum = countnum;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getModeId() {
        return modeId;
    }

    public void setModeId(String modeId) {
        this.modeId = modeId;
    }

    public String getChekType() {
        return chekType;
    }

    public void setChekType(String chekType) {
        this.chekType = chekType;
    }

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public BigDecimal getCheckNum() {
        return checkNum;
    }

    public void setCheckNum(BigDecimal checkNum) {
        this.checkNum = checkNum;
    }

    public BigDecimal getCheckAmt() {
        return checkAmt;
    }

    public void setCheckAmt(BigDecimal checkAmt) {
        this.checkAmt = checkAmt;
    }

    public String getTskExeId() {
        return tskExeId;
    }

    public void setTskExeId(String tskExeId) {
        this.tskExeId = tskExeId;
    }

    public String getTskExeBrId() {
        return tskExeBrId;
    }

    public void setTskExeBrId(String tskExeBrId) {
        this.tskExeBrId = tskExeBrId;
    }

    public String getTskCreateDate() {
        return tskCreateDate;
    }

    public void setTskCreateDate(String tskCreateDate) {
        this.tskCreateDate = tskCreateDate;
    }

    public String getTskFinishDate() {
        return tskFinishDate;
    }

    public void setTskFinishDate(String tskFinishDate) {
        this.tskFinishDate = tskFinishDate;
    }

    public String getTskPlanDate() {
        return tskPlanDate;
    }

    public void setTskPlanDate(String tskPlanDate) {
        this.tskPlanDate = tskPlanDate;
    }

    public String getChekDate() {
        return chekDate;
    }

    public void setChekDate(String chekDate) {
        this.chekDate = chekDate;
    }

    public String getChekMode() {
        return chekMode;
    }

    public void setChekMode(String chekMode) {
        this.chekMode = chekMode;
    }

    public Integer getChekStatus() {
        return chekStatus;
    }

    public void setChekStatus(Integer chekStatus) {
        this.chekStatus = chekStatus;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getIsAbnCurfund() {
        return isAbnCurfund;
    }

    public void setIsAbnCurfund(String isAbnCurfund) {
        this.isAbnCurfund = isAbnCurfund;
    }

    public String getIsAuto() {
        return isAuto;
    }

    public void setIsAuto(String isAuto) {
        this.isAuto = isAuto;
    }

    public String getBizSerno() {
        return bizSerno;
    }

    public void setBizSerno(String bizSerno) {
        this.bizSerno = bizSerno;
    }

    public String getDisbTime() {
        return disbTime;
    }

    public void setDisbTime(String disbTime) {
        this.disbTime = disbTime;
    }

    public BigDecimal getDisbAmt() {
        return disbAmt;
    }

    public void setDisbAmt(BigDecimal disbAmt) {
        this.disbAmt = disbAmt;
    }

    public String getIsHoDivis() {
        return isHoDivis;
    }

    public void setIsHoDivis(String isHoDivis) {
        this.isHoDivis = isHoDivis;
    }

    @Override
    public String toString() {
        return "PspTaskNo{" +
                "taskNo='" + taskNo + '\'' +
                ", cusName='" + cusName + '\'' +
                ", cusId='" + cusId + '\'' +
                ", modeId='" + modeId + '\'' +
                ", chekType='" + chekType + '\'' +
                ", lmtType='" + lmtType + '\'' +
                ", contNo='" + contNo + '\'' +
                ", billNo='" + billNo + '\'' +
                ", checkNum=" + checkNum +
                ", checkAmt=" + checkAmt +
                ", tskExeId='" + tskExeId + '\'' +
                ", tskExeBrId='" + tskExeBrId + '\'' +
                ", tskCreateDate='" + tskCreateDate + '\'' +
                ", tskFinishDate='" + tskFinishDate + '\'' +
                ", tskPlanDate='" + tskPlanDate + '\'' +
                ", chekDate='" + chekDate + '\'' +
                ", chekMode='" + chekMode + '\'' +
                ", chekStatus=" + chekStatus +
                ", approveStatus='" + approveStatus + '\'' +
                ", isAbnCurfund='" + isAbnCurfund + '\'' +
                ", isAuto='" + isAuto + '\'' +
                ", bizSerno='" + bizSerno + '\'' +
                ", disbTime='" + disbTime + '\'' +
                ", disbAmt=" + disbAmt +
                ", isHoDivis='" + isHoDivis + '\'' +
                '}';
    }
}
