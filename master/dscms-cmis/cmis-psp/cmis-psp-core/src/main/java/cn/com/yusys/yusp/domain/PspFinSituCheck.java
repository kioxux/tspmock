/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspFinSituCheck
 * @类描述: psp_fin_situ_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-28 13:16:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_fin_situ_check")
public class PspFinSituCheck extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 科目编号 **/
	@Column(name = "IDX_KEY", unique = false, nullable = true, length = 40)
	private String idxKey;
	
	/** 科目名称 **/
	@Column(name = "IDX_NAME", unique = false, nullable = true, length = 80)
	private String idxName;
	
	/** 上年末数据 **/
	@Column(name = "L1Y_END_VALUE", unique = false, nullable = true, length = 40)
	private String l1yEndValue;
	
	/** 本期数据 **/
	@Column(name = "CURT_VALUE", unique = false, nullable = true, length = 40)
	private String curtValue;
	
	/** 上年同期数据 **/
	@Column(name = "L1Y_CURT_VALUE", unique = false, nullable = true, length = 40)
	private String l1yCurtValue;
	
	/** 本期数据和上年同期数变化 **/
	@Column(name = "L1Y_RISE_RATE", unique = false, nullable = true, length = 40)
	private String l1yRiseRate;
	
	/** 说明 **/
	@Column(name = "IDX_REMARK", unique = false, nullable = true, length = 65535)
	private String idxRemark;
	
	/** 提示信息 **/
	@Column(name = "HINT_INFO", unique = false, nullable = true, length = 200)
	private String hintInfo;

	/** 提示值 **/
	private String hintValue;

	/** 页面展示排序 **/
	private String idxOrder;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param idxKey
	 */
	public void setIdxKey(String idxKey) {
		this.idxKey = idxKey;
	}
	
    /**
     * @return idxKey
     */
	public String getIdxKey() {
		return this.idxKey;
	}
	
	/**
	 * @param idxName
	 */
	public void setIdxName(String idxName) {
		this.idxName = idxName;
	}
	
    /**
     * @return idxName
     */
	public String getIdxName() {
		return this.idxName;
	}
	
	/**
	 * @param l1yEndValue
	 */
	public void setL1yEndValue(String l1yEndValue) {
		this.l1yEndValue = l1yEndValue;
	}
	
    /**
     * @return l1yEndValue
     */
	public String getL1yEndValue() {
		return this.l1yEndValue;
	}
	
	/**
	 * @param curtValue
	 */
	public void setCurtValue(String curtValue) {
		this.curtValue = curtValue;
	}
	
    /**
     * @return curtValue
     */
	public String getCurtValue() {
		return this.curtValue;
	}
	
	/**
	 * @param l1yCurtValue
	 */
	public void setL1yCurtValue(String l1yCurtValue) {
		this.l1yCurtValue = l1yCurtValue;
	}
	
    /**
     * @return l1yCurtValue
     */
	public String getL1yCurtValue() {
		return this.l1yCurtValue;
	}
	
	/**
	 * @param l1yRiseRate
	 */
	public void setL1yRiseRate(String l1yRiseRate) {
		this.l1yRiseRate = l1yRiseRate;
	}
	
    /**
     * @return l1yRiseRate
     */
	public String getL1yRiseRate() {
		return this.l1yRiseRate;
	}
	
	/**
	 * @param idxRemark
	 */
	public void setIdxRemark(String idxRemark) {
		this.idxRemark = idxRemark;
	}
	
    /**
     * @return idxRemark
     */
	public String getIdxRemark() {
		return this.idxRemark;
	}
	
	/**
	 * @param hintValue
	 */
	public void setHintValue(String hintValue) {
		this.hintValue = hintValue;
	}
	
    /**
     * @return hintValue
     */
	public String getHintValue() {
		return this.hintValue;
	}

	/**
	 * @param idxOrder
	 */
	public void setIdxOrder(String idxOrder) {
		this.idxOrder = idxOrder;
	}

	/**
	 * @return idxOrder
	 */
	public String getIdxOrder() {
		return this.idxOrder;
	}

	/**
	 * @param hintInfo
	 */
	public void setHintInfo(String hintInfo) {
		this.hintInfo = hintInfo;
	}

	/**
	 * @return hintInfo
	 */
	public String getHintInfo() {
		return this.hintInfo;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}
