package cn.com.yusys.yusp.service.client.batch.cmisbatch0007;


import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：不定期分类任务信息生成
 *
 * @author 茂茂
 * @version 1.0
 * @since   2021年7月13日21:54:58
 */
@Service
public class CmisBatch0007Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0007Service.class);

    // 1）注入：封装的接口类:批量日终管理模块
    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    /**
     * 业务逻辑处理方法：查詢[调度运行管理]信息
     *
     * @param cmisbatch0007ReqDto
     * @return
     */
    @Async
    public Cmisbatch0007RespDto cmisbatch0007(Cmisbatch0007ReqDto cmisbatch0007ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value, JSON.toJSONString(cmisbatch0007ReqDto));
        ResultDto<Cmisbatch0007RespDto> cmisbatch0007ResultDto = cmisBatchClientService.cmisbatch0007(cmisbatch0007ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value, JSON.toJSONString(cmisbatch0007ResultDto));
        Cmisbatch0007RespDto cmisbatch0007RespDto = null;
        if(cmisbatch0007ResultDto !=null ){
            String cmisbatch0007Code = Optional.ofNullable(cmisbatch0007ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String cmisbatch0007Meesage = Optional.ofNullable(cmisbatch0007ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisbatch0007ResultDto.getCode())) {
                //  获取相关的值并解析
                cmisbatch0007RespDto = cmisbatch0007ResultDto.getData();
            } else {
                //  抛出错误异常
                throw BizException.error(null, cmisbatch0007Code, cmisbatch0007Meesage);
            }
        }else{
            throw BizException.error(null, TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0007.key, DscmsEnum.TRADE_CODE_CMISBATCH0007.value);
        return cmisbatch0007RespDto;
    }
}
