/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskDebitInfo
 * @类描述: risk_debit_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-08 10:22:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_debit_info")
public class RiskDebitInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 200)
	private String prdName;
	
	/** 借据金额（元） **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 借据余额（元） **/
	@Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanBalance;
	
	/** 拖欠利息（元） **/
	@Column(name = "DEBIT_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debitInt;
	
	/** 贷款起始日 **/
	@Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
	private String loanStartDate;
	
	/** 贷款到期日 **/
	@Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
	private String loanEndDate;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 执行年利率 **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;
	
	/** 五级分类 **/
	@Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String fiveClass;
	
	/** 上次分类结果 **/
	@Column(name = "LAST_CLASS_RST", unique = false, nullable = true, length = 5)
	private String lastClassRst;
	
	/** 分析状态 **/
	@Column(name = "ANALY_STATUS", unique = false, nullable = true, length = 5)
	private String analyStatus;
	
	/** 是否低风险业务 **/
	@Column(name = "IS_LOW_RISK", unique = false, nullable = true, length = 5)
	private String isLowRisk;
	
	/** 贷款类型 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 5)
	private String loanType;
	
	/** 风险分类日期 **/
	@Column(name = "RISK_DATE", unique = false, nullable = true, length = 20)
	private String riskDate;
	
	/** 矩阵分类结果 **/
	@Column(name = "CROSS_RESULT", unique = false, nullable = true, length = 5)
	private String crossResult;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 十级分类 **/
	@Column(name = "TEN_CLASS", unique = false, nullable = true, length = 5)
	private String tenClass;
	
	/** 上次十级分类结果 **/
	@Column(name = "LAST_TEN_CLASS_RST", unique = false, nullable = true, length = 5)
	private String lastTenClassRst;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return loanBalance
     */
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param debitInt
	 */
	public void setDebitInt(java.math.BigDecimal debitInt) {
		this.debitInt = debitInt;
	}
	
    /**
     * @return debitInt
     */
	public java.math.BigDecimal getDebitInt() {
		return this.debitInt;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}
	
    /**
     * @return loanStartDate
     */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}
	
    /**
     * @return loanEndDate
     */
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return execRateYear
     */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
	
    /**
     * @return fiveClass
     */
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param lastClassRst
	 */
	public void setLastClassRst(String lastClassRst) {
		this.lastClassRst = lastClassRst;
	}
	
    /**
     * @return lastClassRst
     */
	public String getLastClassRst() {
		return this.lastClassRst;
	}
	
	/**
	 * @param analyStatus
	 */
	public void setAnalyStatus(String analyStatus) {
		this.analyStatus = analyStatus;
	}
	
    /**
     * @return analyStatus
     */
	public String getAnalyStatus() {
		return this.analyStatus;
	}
	
	/**
	 * @param isLowRisk
	 */
	public void setIsLowRisk(String isLowRisk) {
		this.isLowRisk = isLowRisk;
	}
	
    /**
     * @return isLowRisk
     */
	public String getIsLowRisk() {
		return this.isLowRisk;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
    /**
     * @return loanType
     */
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param riskDate
	 */
	public void setRiskDate(String riskDate) {
		this.riskDate = riskDate;
	}
	
    /**
     * @return riskDate
     */
	public String getRiskDate() {
		return this.riskDate;
	}
	
	/**
	 * @param crossResult
	 */
	public void setCrossResult(String crossResult) {
		this.crossResult = crossResult;
	}
	
    /**
     * @return crossResult
     */
	public String getCrossResult() {
		return this.crossResult;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param tenClass
	 */
	public void setTenClass(String tenClass) {
		this.tenClass = tenClass;
	}
	
    /**
     * @return tenClass
     */
	public String getTenClass() {
		return this.tenClass;
	}
	
	/**
	 * @param lastTenClassRst
	 */
	public void setLastTenClassRst(String lastTenClassRst) {
		this.lastTenClassRst = lastTenClassRst;
	}
	
    /**
     * @return lastTenClassRst
     */
	public String getLastTenClassRst() {
		return this.lastTenClassRst;
	}


}