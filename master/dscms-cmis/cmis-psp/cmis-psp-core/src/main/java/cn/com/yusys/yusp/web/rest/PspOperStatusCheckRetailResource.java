/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspOperStatusCheckRetail;
import cn.com.yusys.yusp.service.PspOperStatusCheckRetailService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckRetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pspoperstatuscheckretail")
public class PspOperStatusCheckRetailResource {
    @Autowired
    private PspOperStatusCheckRetailService pspOperStatusCheckRetailService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspOperStatusCheckRetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspOperStatusCheckRetail> list = pspOperStatusCheckRetailService.selectAll(queryModel);
        return new ResultDto<List<PspOperStatusCheckRetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PspOperStatusCheckRetail>> index(QueryModel queryModel) {
        List<PspOperStatusCheckRetail> list = pspOperStatusCheckRetailService.selectByModel(queryModel);
        return new ResultDto<List<PspOperStatusCheckRetail>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @GetMapping("/queryList")
    protected ResultDto<List<PspOperStatusCheckRetail>> queryList(QueryModel queryModel) {
        List<PspOperStatusCheckRetail> list = pspOperStatusCheckRetailService.selectByModel(queryModel);
        return new ResultDto<List<PspOperStatusCheckRetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspOperStatusCheckRetail> show(@PathVariable("pkId") String pkId) {
        PspOperStatusCheckRetail pspOperStatusCheckRetail = pspOperStatusCheckRetailService.selectByPrimaryKey(pkId);
        return new ResultDto<PspOperStatusCheckRetail>(pspOperStatusCheckRetail);
    }
    /**
     * @函数名称:queryPspOperStatusCheckRetail
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<PspOperStatusCheckRetail> queryPspOperStatusCheckRetail(@RequestBody PspOperStatusCheckRetail pspOperStatusCheckRetail) {
        pspOperStatusCheckRetail = pspOperStatusCheckRetailService.queryPspOperStatusCheckRetail(pspOperStatusCheckRetail);
        return new ResultDto<PspOperStatusCheckRetail>(pspOperStatusCheckRetail);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PspOperStatusCheckRetail> create(@RequestBody PspOperStatusCheckRetail pspOperStatusCheckRetail) throws URISyntaxException {
        pspOperStatusCheckRetailService.insert(pspOperStatusCheckRetail);
        return new ResultDto<PspOperStatusCheckRetail>(pspOperStatusCheckRetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspOperStatusCheckRetail pspOperStatusCheckRetail) throws URISyntaxException {
        int result = pspOperStatusCheckRetailService.update(pspOperStatusCheckRetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspOperStatusCheckRetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspOperStatusCheckRetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
