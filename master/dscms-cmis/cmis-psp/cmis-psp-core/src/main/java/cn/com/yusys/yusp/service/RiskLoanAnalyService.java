/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.RiskDebitInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RiskLoanAnaly;
import cn.com.yusys.yusp.repository.mapper.RiskLoanAnalyMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskLoanAnalyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RiskLoanAnalyService {

    @Autowired
    private RiskLoanAnalyMapper riskLoanAnalyMapper;
    @Autowired
    private RiskDebitInfoService riskDebitInfoService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RiskLoanAnaly selectByPrimaryKey(String pkId) {
        return riskLoanAnalyMapper.selectByPrimaryKey(pkId);
    }
	/**
     * @方法名称: queryRiskLoanAnaly
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskLoanAnaly queryRiskLoanAnaly(RiskLoanAnaly riskLoanAnaly) {
        return riskLoanAnalyMapper.queryRiskLoanAnaly(riskLoanAnaly);
    }
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RiskLoanAnaly> selectAll(QueryModel model) {
        List<RiskLoanAnaly> records = (List<RiskLoanAnaly>) riskLoanAnalyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RiskLoanAnaly> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RiskLoanAnaly> list = riskLoanAnalyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RiskLoanAnaly record) {
        RiskDebitInfo riskDebitInfo = new RiskDebitInfo();
        riskDebitInfo.setTaskNo(record.getTaskNo());
        riskDebitInfo.setBillNo(record.getBillNo());
        int result = riskLoanAnalyMapper.insert(record) + riskDebitInfoService.update(riskDebitInfo);
        return result;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RiskLoanAnaly record) {
        return riskLoanAnalyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RiskLoanAnaly record) {
        RiskDebitInfo riskDebitInfo = new RiskDebitInfo();
        riskDebitInfo.setTaskNo(record.getTaskNo());
        riskDebitInfo.setBillNo(record.getBillNo());
        if(riskLoanAnalyMapper.updateByPrimaryKeySelective(record) == 0){
            riskLoanAnalyMapper.insert(record);
        }
        int result = riskDebitInfoService.updateCheckStatus(riskDebitInfo);
        return result;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RiskLoanAnaly record) {
        return riskLoanAnalyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return riskLoanAnalyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return riskLoanAnalyMapper.deleteByIds(ids);
    }
}
