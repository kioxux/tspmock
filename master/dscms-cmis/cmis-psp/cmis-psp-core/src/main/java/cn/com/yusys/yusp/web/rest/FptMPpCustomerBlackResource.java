/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.FptMPpCustomerBlack;
import cn.com.yusys.yusp.service.FptMPpCustomerBlackService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: FptMPpCustomerBlackResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-01 21:24:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/fptmppcustomerblack")
public class FptMPpCustomerBlackResource {
    @Autowired
    private FptMPpCustomerBlackService fptMPpCustomerBlackService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<FptMPpCustomerBlack>> query() {
        QueryModel queryModel = new QueryModel();
        List<FptMPpCustomerBlack> list = fptMPpCustomerBlackService.selectAll(queryModel);
        return new ResultDto<List<FptMPpCustomerBlack>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<FptMPpCustomerBlack>> index(QueryModel queryModel) {
        List<FptMPpCustomerBlack> list = fptMPpCustomerBlackService.selectByModel(queryModel);
        return new ResultDto<List<FptMPpCustomerBlack>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<FptMPpCustomerBlack> create(@RequestBody FptMPpCustomerBlack fptMPpCustomerBlack) throws URISyntaxException {
        fptMPpCustomerBlackService.insert(fptMPpCustomerBlack);
        return new ResultDto<FptMPpCustomerBlack>(fptMPpCustomerBlack);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody FptMPpCustomerBlack fptMPpCustomerBlack) throws URISyntaxException {
        int result = fptMPpCustomerBlackService.update(fptMPpCustomerBlack);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String custNo, String custName, String remark, String inputDate) {
        int result = fptMPpCustomerBlackService.deleteByPrimaryKey(custNo, custName, remark, inputDate);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:通过客户号查询工作信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByCusid")
    protected ResultDto<FptMPpCustomerBlack> selectByCusId(@RequestBody String cusId) {
        FptMPpCustomerBlack fptMPpCustomerBlack = fptMPpCustomerBlackService.selectBycusId(cusId);
        return new ResultDto<FptMPpCustomerBlack>(fptMPpCustomerBlack);
    }
}
