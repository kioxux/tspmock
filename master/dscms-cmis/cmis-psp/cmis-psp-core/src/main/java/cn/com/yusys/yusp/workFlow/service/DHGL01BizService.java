package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.PspTaskList;
import cn.com.yusys.yusp.dto.CusMgrDividePercDto;
import cn.com.yusys.yusp.flow.ClientCons;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.WFException;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.PspTaskListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 对公首次贷后检查流程
 */
@Service
public class  DHGL01BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DHGL01BizService.class);

    @Autowired
    private PspTaskListService pspTaskListService;
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String bizType = resultInstanceDto.getBizType();
        log.info("{}进入流程处理",bizType);
        dealBizOp(resultInstanceDto);
    }

    private void dealBizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        PspTaskList pspTaskList = new PspTaskList();
        pspTaskList.setTaskNo(serno);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + resultInstanceDto);
                pspTaskList.setApproveStatus("111");
                pspTaskList.setCheckStatus("3");
                pspTaskListService.update(pspTaskList);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                //审批通过
                log.info("结束操作:" + resultInstanceDto);
                pspTaskList.setApproveStatus("997");
                //获取当前日期
                pspTaskList.setCheckDate(openDay);
                pspTaskListService.update(pspTaskList);
                //
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                pspTaskList.setApproveStatus("992");
                pspTaskList.setCheckStatus("2");
                pspTaskListService.update(pspTaskList);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                pspTaskList.setApproveStatus("992");
                pspTaskList.setCheckStatus("2");
                pspTaskListService.update(pspTaskList);
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             态改为追回 991
                log.info("拿回操作:" + resultInstanceDto);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                pspTaskList.setApproveStatus("998");
                //插入检查时间
                pspTaskList.setCheckDate(openDay);
                pspTaskListService.update(pspTaskList);
                log.info("否决操作结束:" + resultInstanceDto);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }

        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                WFException exception = new WFException();
                exception.setBizId(resultInstanceDto.getBizId());
                exception.setBizType(resultInstanceDto.getBizType());
                exception.setFlowName(resultInstanceDto.getFlowName());
                exception.setInstanceId(resultInstanceDto.getInstanceId());
                exception.setNodeId(resultInstanceDto.getNodeId());
                exception.setNodeName(resultInstanceDto.getNodeName());
                exception.setUserId(resultInstanceDto.getCurrentUserId());
                exception.setOpType(currentOpType);
                // 后业务处理失败时，将异常信息保存到异常表中
                amqpTemplate.convertAndSend(ClientCons.queue_exception, exception);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DHGL01.equals(flowCode);
    }

}
