/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspLmtSitu;
import cn.com.yusys.yusp.service.PspLmtSituService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspLmtSituResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/psplmtsitu")
public class PspLmtSituResource {
    @Autowired
    private PspLmtSituService pspLmtSituService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspLmtSitu>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspLmtSitu> list = pspLmtSituService.selectAll(queryModel);
        return new ResultDto<List<PspLmtSitu>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PspLmtSitu>> index(QueryModel queryModel) {
        List<PspLmtSitu> list = pspLmtSituService.selectByModel(queryModel);
        return new ResultDto<List<PspLmtSitu>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<PspLmtSitu>> queryList(@RequestBody QueryModel queryModel) {
        List<PspLmtSitu> list = pspLmtSituService.selectByModel(queryModel);
        return new ResultDto<List<PspLmtSitu>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspLmtSitu> show(@PathVariable("pkId") String pkId) {
        PspLmtSitu pspLmtSitu = pspLmtSituService.selectByPrimaryKey(pkId);
        return new ResultDto<PspLmtSitu>(pspLmtSitu);
    }
    /**
     * @函数名称:queryPspLmtSitu
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<PspLmtSitu> queryPspLmtSitu(@RequestBody PspLmtSitu pspLmtSitu) {
        pspLmtSitu = pspLmtSituService.queryPspLmtSitu(pspLmtSitu);
        return new ResultDto<PspLmtSitu>(pspLmtSitu);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PspLmtSitu> create(@RequestBody PspLmtSitu pspLmtSitu) throws URISyntaxException {
        pspLmtSituService.insert(pspLmtSitu);
        return new ResultDto<PspLmtSitu>(pspLmtSitu);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspLmtSitu pspLmtSitu) throws URISyntaxException {
        int result = pspLmtSituService.update(pspLmtSitu);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspLmtSituService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspLmtSituService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
