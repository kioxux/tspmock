/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;

import cn.com.yusys.yusp.dto.PspWydBackTjListDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PspWydTaskBack;
import cn.com.yusys.yusp.repository.mapper.PspWydTaskBackMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWydTaskBackService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-30 21:46:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspWydTaskBackService {

    @Autowired
    private PspWydTaskBackMapper pspWydTaskBackMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PspWydTaskBack selectByPrimaryKey(String pkId) {
        return pspWydTaskBackMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PspWydTaskBack> selectAll(QueryModel model) {
        List<PspWydTaskBack> records = (List<PspWydTaskBack>) pspWydTaskBackMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PspWydTaskBack> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PspWydTaskBack> list = pspWydTaskBackMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PspWydTaskBack record) {
        return pspWydTaskBackMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PspWydTaskBack record) {
        return pspWydTaskBackMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PspWydTaskBack record) {
        return pspWydTaskBackMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PspWydTaskBack record) {
        return pspWydTaskBackMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pspWydTaskBackMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pspWydTaskBackMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: pspWydBackTjList
     * @方法描述: 贷后正常任务统计查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PspWydBackTjListDto> pspWydBackTjList(QueryModel model)  {
        //QueryModel model = new QueryModel();
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PspWydBackTjListDto> pspwydBackTjListDto = new ArrayList<PspWydBackTjListDto>();
        List<PspWydBackTjListDto> pspWydBackTjListDto = pspWydTaskBackMapper.pspWydBackTjList(model);
        if(pspWydBackTjListDto.size()>0){
            for(int i=0;i<pspWydBackTjListDto.size();i++){
                PspWydBackTjListDto pspWydbackTjListDto = new PspWydBackTjListDto();
                String relBrId = pspWydBackTjListDto.get(i).getRelBrId();
                String backNum = pspWydBackTjListDto.get(i).getBackNum();
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("inputBrId",relBrId);
                queryModel.addCondition("backReason","01");
                String backOneNum = pspWydTaskBackMapper.queryBackReasonNum(queryModel);
                queryModel.addCondition("inputBrId",relBrId);
                queryModel.addCondition("backReason","02");
                String backTwoNum = pspWydTaskBackMapper.queryBackReasonNum(queryModel);
                pspWydbackTjListDto.setRelBrId(relBrId);
                pspWydbackTjListDto.setBackNum(backNum);
                pspWydbackTjListDto.setBackOneNum(backOneNum);
                pspWydbackTjListDto.setBackTwoNum(backTwoNum);
                pspwydBackTjListDto.add(pspWydbackTjListDto);
            }
        }
        PageHelper.clearPage();
        return pspwydBackTjListDto;
    }
}
