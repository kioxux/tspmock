/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspCusBaseCase;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PspPropertyCheck;
import cn.com.yusys.yusp.repository.mapper.PspPropertyCheckMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPropertyCheckService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspPropertyCheckService {

    @Autowired
    private PspPropertyCheckMapper pspPropertyCheckMapper;

    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PspPropertyCheck selectByPrimaryKey(String pkId) {
        return pspPropertyCheckMapper.selectByPrimaryKey(pkId);
    }
	/**
     * @方法名称: queryPspPropertyCheck
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspPropertyCheck queryPspPropertyCheck(PspPropertyCheck pspPropertyCheck) {
        return pspPropertyCheckMapper.queryPspPropertyCheck(pspPropertyCheck);
    }
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PspPropertyCheck> selectAll(QueryModel model) {
        List<PspPropertyCheck> records = (List<PspPropertyCheck>) pspPropertyCheckMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PspPropertyCheck> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PspPropertyCheck> list = pspPropertyCheckMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PspPropertyCheck record) {
        return pspPropertyCheckMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PspPropertyCheck record) {
        return pspPropertyCheckMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PspPropertyCheck record) {
        int count = 0;
        PspPropertyCheck pspPropertyCheck = pspPropertyCheckMapper.selectByPrimaryKey(record.getPkId());
        if (pspPropertyCheck != null){
            pspPropertyCheckMapper.updateByPrimaryKeySelective(record);
        }else {
            pspPropertyCheckMapper.insert(record);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PspPropertyCheck record) {
        return pspPropertyCheckMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pspPropertyCheckMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pspPropertyCheckMapper.deleteByIds(ids);
    }

    /**
     * 调用ib1253查询子序号
     *
     * @author 刘权
     **/
    public ResultDto<Ib1253RespDto> sendToIb1253(Map map) {
        Ib1253ReqDto ib1253ReqDto = new Ib1253ReqDto();
        //客户账号
        ib1253ReqDto.setKehuzhao((String) map.get("acctNo"));
        //子账户序号
        ib1253ReqDto.setZhhaoxuh("");
        //密码校验方式不校验1 0----校验查询密码 2--校验交易密码
        ib1253ReqDto.setYanmbzhi("0");
        //密码
        ib1253ReqDto.setMimammmm("");
        //客户账号2
        ib1253ReqDto.setKehzhao2("");
        //是否标志 1--是 0--否
        ib1253ReqDto.setShifoubz("0");
        //账户分类代码1
        ib1253ReqDto.setZhufldm1("");
        //账户分类代码2
        ib1253ReqDto.setZhufldm2("");

        ResultDto<Ib1253RespDto> resultDto = dscms2CoreIbClientService.ib1253(ib1253ReqDto);
        String ib1253Code = Optional.ofNullable(resultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ib1253Meesage = Optional.ofNullable(resultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(ib1253Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            return resultDto;
        } else {
            resultDto.setMessage(ib1253Meesage);
            return resultDto;
        }
    }
}
