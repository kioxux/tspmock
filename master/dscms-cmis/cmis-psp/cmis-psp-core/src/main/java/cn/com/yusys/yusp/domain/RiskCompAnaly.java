/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskCompAnaly
 * @类描述: risk_comp_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_comp_analy")
public class RiskCompAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 综合分析 **/
	@Column(name = "INTE_ANALY", unique = false, nullable = true, length = 65535)
	private String inteAnaly;
	
	/** 影响偿还的各类风险因素 **/
	@Column(name = "RISK_RESN", unique = false, nullable = true, length = 65535)
	private String riskResn;
	
	/** 防范风险的具体措施 **/
	@Column(name = "RISK_MODE", unique = false, nullable = true, length = 65535)
	private String riskMode;
	
	/** 风险防范化解措施的落实情况 **/
	@Column(name = "ACT_PACT", unique = false, nullable = true, length = 65535)
	private String actPact;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param inteAnaly
	 */
	public void setInteAnaly(String inteAnaly) {
		this.inteAnaly = inteAnaly;
	}
	
    /**
     * @return inteAnaly
     */
	public String getInteAnaly() {
		return this.inteAnaly;
	}
	
	/**
	 * @param riskResn
	 */
	public void setRiskResn(String riskResn) {
		this.riskResn = riskResn;
	}
	
    /**
     * @return riskResn
     */
	public String getRiskResn() {
		return this.riskResn;
	}

	public String getRiskMode() {
		return riskMode;
	}

	public void setRiskMode(String riskMode) {
		this.riskMode = riskMode;
	}

	/**
	 * @param actPact
	 */
	public void setActPact(String actPact) {
		this.actPact = actPact;
	}
	
    /**
     * @return actPact
     */
	public String getActPact() {
		return this.actPact;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}