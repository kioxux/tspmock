/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspDebitOperCaseAnaly
 * @类描述: psp_debit_oper_case_analy数据实体类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_debit_oper_case_analy")
public class PspDebitOperCaseAnaly extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;

	/** 产品库存是否大幅增加 **/
	@Column(name = "IS_BIG_GROW_STORAGE", unique = false, nullable = true, length = 5)
	private String isBigGrowStorage;

	/** 库存大幅增加说明 **/
	@Column(name = "BIG_GROW_STORAGE_REMARK", unique = false, nullable = true, length = 65535)
	private String bigGrowStorageRemark;

	/** 借款人是否存在过度对外投资 **/
	@Column(name = "IS_BIG_OUTER_INVEST", unique = false, nullable = true, length = 5)
	private String isBigOuterInvest;

	/** 过度对外投资说明 **/
	@Column(name = "BIG_OUTER_INVEST_REMARK", unique = false, nullable = true, length = 65535)
	private String bigOuterInvestRemark;

	/** 借款人经营所有权是否发生重大变化 **/
	@Column(name = "IS_BIG_CHANGE_OWNER", unique = false, nullable = true, length = 5)
	private String isBigChangeOwner;

	/** 经营所有权重大变化说明 **/
	@Column(name = "BIG_CHANGE_OWNER_REMARK", unique = false, nullable = true, length = 65535)
	private String bigChangeOwnerRemark;

	/** 贷款用途是否符合约定 **/
	@Column(name = "IS_SUIT_AGREED", unique = false, nullable = true, length = 5)
	private String isSuitAgreed;

	/** 用途不符说明 **/
	@Column(name = "SUIT_AGREED_REMARK", unique = false, nullable = true, length = 65535)
	private String suitAgreedRemark;

	/** 借款人经营是否正常 **/
	@Column(name = "IS_NORMAL_OPER", unique = false, nullable = true, length = 5)
	private String isNormalOper;

	/** 经营异常说明 **/
	@Column(name = "NORMAL_OPER_REMARK", unique = false, nullable = true, length = 65535)
	private String normalOperRemark;

	/** 生产经营情况 **/
	@Column(name = "INDIV_OPER_SITU", unique = false, nullable = true, length = 5)
	private String indivOperSitu;

	/** 销售收入变化趋势 **/
	@Column(name = "INCOME_CHANGE", unique = false, nullable = true, length = 5)
	private String incomeChange;

	/** 利润变动趋势 **/
	@Column(name = "PROFIT_CHANGE", unique = false, nullable = true, length = 5)
	private String profitChange;

	/** 现金流量变动趋势 **/
	@Column(name = "CASH_CHANGE", unique = false, nullable = true, length = 5)
	private String cashChange;

	/** 第一还款来源是否充足 **/
	@Column(name = "IS_INCOME_SUFFICE", unique = false, nullable = true, length = 5)
	private String isIncomeSuffice;

	/** 借款人收入显著变化 **/
	@Column(name = "IS_BIG_CHANGE_INCOME", unique = false, nullable = true, length = 65535)
	private String isBigChangeIncome;

	/** 收入显著变化说明 **/
	@Column(name = "BIG_CHANGE_INCOME_REMARK", unique = false, nullable = true, length = 65535)
	private String bigChangeIncomeRemark;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	public String getIsBigChangeIncome() {
		return isBigChangeIncome;
	}

	public void setIsBigChangeIncome(String isBigChangeIncome) {
		this.isBigChangeIncome = isBigChangeIncome;
	}

	public String getBigChangeIncomeRemark() {
		return bigChangeIncomeRemark;
	}

	public void setBigChangeIncomeRemark(String bigChangeIncomeRemark) {
		this.bigChangeIncomeRemark = bigChangeIncomeRemark;
	}

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}

	/**
	 * @return taskNo
	 */
	public String getTaskNo() {
		return this.taskNo;
	}

	/**
	 * @param isBigGrowStorage
	 */
	public void setIsBigGrowStorage(String isBigGrowStorage) {
		this.isBigGrowStorage = isBigGrowStorage;
	}

	/**
	 * @return isBigGrowStorage
	 */
	public String getIsBigGrowStorage() {
		return this.isBigGrowStorage;
	}

	/**
	 * @param bigGrowStorageRemark
	 */
	public void setBigGrowStorageRemark(String bigGrowStorageRemark) {
		this.bigGrowStorageRemark = bigGrowStorageRemark;
	}

	/**
	 * @return bigGrowStorageRemark
	 */
	public String getBigGrowStorageRemark() {
		return this.bigGrowStorageRemark;
	}

	/**
	 * @param isBigOuterInvest
	 */
	public void setIsBigOuterInvest(String isBigOuterInvest) {
		this.isBigOuterInvest = isBigOuterInvest;
	}

	/**
	 * @return isBigOuterInvest
	 */
	public String getIsBigOuterInvest() {
		return this.isBigOuterInvest;
	}

	/**
	 * @param bigOuterInvestRemark
	 */
	public void setBigOuterInvestRemark(String bigOuterInvestRemark) {
		this.bigOuterInvestRemark = bigOuterInvestRemark;
	}

	/**
	 * @return bigOuterInvestRemark
	 */
	public String getBigOuterInvestRemark() {
		return this.bigOuterInvestRemark;
	}

	/**
	 * @param isBigChangeOwner
	 */
	public void setIsBigChangeOwner(String isBigChangeOwner) {
		this.isBigChangeOwner = isBigChangeOwner;
	}

	/**
	 * @return isBigChangeOwner
	 */
	public String getIsBigChangeOwner() {
		return this.isBigChangeOwner;
	}

	/**
	 * @param bigChangeOwnerRemark
	 */
	public void setBigChangeOwnerRemark(String bigChangeOwnerRemark) {
		this.bigChangeOwnerRemark = bigChangeOwnerRemark;
	}

	/**
	 * @return bigChangeOwnerRemark
	 */
	public String getBigChangeOwnerRemark() {
		return this.bigChangeOwnerRemark;
	}

	/**
	 * @param isSuitAgreed
	 */
	public void setIsSuitAgreed(String isSuitAgreed) {
		this.isSuitAgreed = isSuitAgreed;
	}

	/**
	 * @return isSuitAgreed
	 */
	public String getIsSuitAgreed() {
		return this.isSuitAgreed;
	}

	/**
	 * @param suitAgreedRemark
	 */
	public void setSuitAgreedRemark(String suitAgreedRemark) {
		this.suitAgreedRemark = suitAgreedRemark;
	}

	/**
	 * @return suitAgreedRemark
	 */
	public String getSuitAgreedRemark() {
		return this.suitAgreedRemark;
	}

	/**
	 * @param isNormalOper
	 */
	public void setIsNormalOper(String isNormalOper) {
		this.isNormalOper = isNormalOper;
	}

	/**
	 * @return isNormalOper
	 */
	public String getIsNormalOper() {
		return this.isNormalOper;
	}

	/**
	 * @param normalOperRemark
	 */
	public void setNormalOperRemark(String normalOperRemark) {
		this.normalOperRemark = normalOperRemark;
	}

	/**
	 * @return normalOperRemark
	 */
	public String getNormalOperRemark() {
		return this.normalOperRemark;
	}

	/**
	 * @param indivOperSitu
	 */
	public void setIndivOperSitu(String indivOperSitu) {
		this.indivOperSitu = indivOperSitu;
	}

	/**
	 * @return indivOperSitu
	 */
	public String getIndivOperSitu() {
		return this.indivOperSitu;
	}

	/**
	 * @param incomeChange
	 */
	public void setIncomeChange(String incomeChange) {
		this.incomeChange = incomeChange;
	}

	/**
	 * @return incomeChange
	 */
	public String getIncomeChange() {
		return this.incomeChange;
	}

	/**
	 * @param profitChange
	 */
	public void setProfitChange(String profitChange) {
		this.profitChange = profitChange;
	}

	/**
	 * @return profitChange
	 */
	public String getProfitChange() {
		return this.profitChange;
	}

	/**
	 * @param cashChange
	 */
	public void setCashChange(String cashChange) {
		this.cashChange = cashChange;
	}

	/**
	 * @return cashChange
	 */
	public String getCashChange() {
		return this.cashChange;
	}

	/**
	 * @param isIncomeSuffice
	 */
	public void setIsIncomeSuffice(String isIncomeSuffice) {
		this.isIncomeSuffice = isIncomeSuffice;
	}

	/**
	 * @return isIncomeSuffice
	 */
	public String getIsIncomeSuffice() {
		return this.isIncomeSuffice;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}