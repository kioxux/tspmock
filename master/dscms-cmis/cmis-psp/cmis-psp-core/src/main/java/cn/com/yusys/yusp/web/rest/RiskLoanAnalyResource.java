/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RiskLoanAnaly;
import cn.com.yusys.yusp.service.RiskLoanAnalyService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskLoanAnalyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/riskloananaly")
public class RiskLoanAnalyResource {
    @Autowired
    private RiskLoanAnalyService riskLoanAnalyService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RiskLoanAnaly>> query() {
        QueryModel queryModel = new QueryModel();
        List<RiskLoanAnaly> list = riskLoanAnalyService.selectAll(queryModel);
        return new ResultDto<List<RiskLoanAnaly>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RiskLoanAnaly>> index(QueryModel queryModel) {
        List<RiskLoanAnaly> list = riskLoanAnalyService.selectByModel(queryModel);
        return new ResultDto<List<RiskLoanAnaly>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @GetMapping("/queryList")
    protected ResultDto<List<RiskLoanAnaly>> queryList(QueryModel queryModel) {
        List<RiskLoanAnaly> list = riskLoanAnalyService.selectByModel(queryModel);
        return new ResultDto<List<RiskLoanAnaly>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RiskLoanAnaly> show(@PathVariable("pkId") String pkId) {
        RiskLoanAnaly riskLoanAnaly = riskLoanAnalyService.selectByPrimaryKey(pkId);
        return new ResultDto<RiskLoanAnaly>(riskLoanAnaly);
    }
    /**
     * @函数名称:queryRiskLoanAnaly
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<RiskLoanAnaly> queryRiskLoanAnaly(@RequestBody RiskLoanAnaly riskLoanAnaly) {
        riskLoanAnaly = riskLoanAnalyService.queryRiskLoanAnaly(riskLoanAnaly);
        return new ResultDto<RiskLoanAnaly>(riskLoanAnaly);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<RiskLoanAnaly> create(@RequestBody RiskLoanAnaly riskLoanAnaly) throws URISyntaxException {
        riskLoanAnalyService.insert(riskLoanAnaly);
        return new ResultDto<RiskLoanAnaly>(riskLoanAnaly);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RiskLoanAnaly riskLoanAnaly) throws URISyntaxException {
        int result = riskLoanAnalyService.update(riskLoanAnaly);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = riskLoanAnalyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = riskLoanAnalyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
