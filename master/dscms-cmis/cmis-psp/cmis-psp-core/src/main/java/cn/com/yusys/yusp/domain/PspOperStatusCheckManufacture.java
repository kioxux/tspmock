/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckManufacture
 * @类描述: psp_oper_status_check_manufacture数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_oper_status_check_manufacture")
public class PspOperStatusCheckManufacture extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 库存是否积压 **/
	@Column(name = "IS_OVERSTOCKED_PRODUCTS", unique = false, nullable = true, length = 5)
	private String isOverstockedProducts;
	
	/** 库存积压说明 **/
	@Column(name = "OVERSTOCKED_EXPL", unique = false, nullable = true, length = 65535)
	private String overstockedExpl;
	
	/** 订单量是否下降 **/
	@Column(name = "IS_ORDER_DOWEN", unique = false, nullable = true, length = 5)
	private String isOrderDowen;
	
	/** 订单量下降说明 **/
	@Column(name = "DOWEN_EXPL", unique = false, nullable = true, length = 65535)
	private String dowenExpl;
	
	/** 产量是否下降 **/
	@Column(name = "IS_OUTPUT_DOWEN", unique = false, nullable = true, length = 5)
	private String isOutputDowen;
	
	/** 产量下降说明 **/
	@Column(name = "OUTPUT_DOWEN_EXPL", unique = false, nullable = true, length = 65535)
	private String outputDowenExpl;
	
	/** 是否新增对外投资和固定资产 **/
	@Column(name = "IS_ADD_INVEST", unique = false, nullable = true, length = 5)
	private String isAddInvest;
	
	/** 新增投资说明 **/
	@Column(name = "ADD_INVEST_EXPL", unique = false, nullable = true, length = 65535)
	private String addInvestExpl;
	
	/** 借款人融资与销售是否匹配 **/
	@Column(name = "IS_FIN_MATCHING", unique = false, nullable = true, length = 5)
	private String isFinMatching;
	
	/** 融资销售匹配说明 **/
	@Column(name = "MATCHING_EXPL", unique = false, nullable = true, length = 65535)
	private String matchingExpl;
	
	/** 借款人上期开台率情况 **/
	@Column(name = "PRE_RST", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst;
	
	/** 借款人本期开台率情况 **/
	@Column(name = "CURT_RST", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst;
	
	/** 借款人开台率情况说明 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 65535)
	private String remark;
	
	/** 借款人上期水费（万元） **/
	@Column(name = "PRE_RST1", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst1;
	
	/** 借款人本期水费（万元） **/
	@Column(name = "CURT_RST1", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst1;
	
	/** 借款人水费说明 **/
	@Column(name = "REMARK1", unique = false, nullable = true, length = 65535)
	private String remark1;
	
	/** 借款人上期电费（万元） **/
	@Column(name = "PRE_RST2", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst2;
	
	/** 借款人本期电费（万元） **/
	@Column(name = "CURT_RST2", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst2;
	
	/** 借款人电费说明 **/
	@Column(name = "REMARK2", unique = false, nullable = true, length = 65535)
	private String remark2;
	
	/** 借款人上期气费（万元） **/
	@Column(name = "PRE_RST3", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst3;
	
	/** 借款人本期气费（万元） **/
	@Column(name = "CURT_RST3", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst3;
	
	/** 借款人气费说明 **/
	@Column(name = "REMARK3", unique = false, nullable = true, length = 65535)
	private String remark3;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isOverstockedProducts
	 */
	public void setIsOverstockedProducts(String isOverstockedProducts) {
		this.isOverstockedProducts = isOverstockedProducts;
	}
	
    /**
     * @return isOverstockedProducts
     */
	public String getIsOverstockedProducts() {
		return this.isOverstockedProducts;
	}
	
	/**
	 * @param overstockedExpl
	 */
	public void setOverstockedExpl(String overstockedExpl) {
		this.overstockedExpl = overstockedExpl;
	}
	
    /**
     * @return overstockedExpl
     */
	public String getOverstockedExpl() {
		return this.overstockedExpl;
	}
	
	/**
	 * @param isOrderDowen
	 */
	public void setIsOrderDowen(String isOrderDowen) {
		this.isOrderDowen = isOrderDowen;
	}
	
    /**
     * @return isOrderDowen
     */
	public String getIsOrderDowen() {
		return this.isOrderDowen;
	}
	
	/**
	 * @param dowenExpl
	 */
	public void setDowenExpl(String dowenExpl) {
		this.dowenExpl = dowenExpl;
	}
	
    /**
     * @return dowenExpl
     */
	public String getDowenExpl() {
		return this.dowenExpl;
	}
	
	/**
	 * @param isOutputDowen
	 */
	public void setIsOutputDowen(String isOutputDowen) {
		this.isOutputDowen = isOutputDowen;
	}
	
    /**
     * @return isOutputDowen
     */
	public String getIsOutputDowen() {
		return this.isOutputDowen;
	}
	
	/**
	 * @param outputDowenExpl
	 */
	public void setOutputDowenExpl(String outputDowenExpl) {
		this.outputDowenExpl = outputDowenExpl;
	}
	
    /**
     * @return outputDowenExpl
     */
	public String getOutputDowenExpl() {
		return this.outputDowenExpl;
	}
	
	/**
	 * @param isAddInvest
	 */
	public void setIsAddInvest(String isAddInvest) {
		this.isAddInvest = isAddInvest;
	}
	
    /**
     * @return isAddInvest
     */
	public String getIsAddInvest() {
		return this.isAddInvest;
	}
	
	/**
	 * @param addInvestExpl
	 */
	public void setAddInvestExpl(String addInvestExpl) {
		this.addInvestExpl = addInvestExpl;
	}
	
    /**
     * @return addInvestExpl
     */
	public String getAddInvestExpl() {
		return this.addInvestExpl;
	}
	
	/**
	 * @param isFinMatching
	 */
	public void setIsFinMatching(String isFinMatching) {
		this.isFinMatching = isFinMatching;
	}
	
    /**
     * @return isFinMatching
     */
	public String getIsFinMatching() {
		return this.isFinMatching;
	}
	
	/**
	 * @param matchingExpl
	 */
	public void setMatchingExpl(String matchingExpl) {
		this.matchingExpl = matchingExpl;
	}
	
    /**
     * @return matchingExpl
     */
	public String getMatchingExpl() {
		return this.matchingExpl;
	}
	
	/**
	 * @param preRst
	 */
	public void setPreRst(java.math.BigDecimal preRst) {
		this.preRst = preRst;
	}
	
    /**
     * @return preRst
     */
	public java.math.BigDecimal getPreRst() {
		return this.preRst;
	}
	
	/**
	 * @param curtRst
	 */
	public void setCurtRst(java.math.BigDecimal curtRst) {
		this.curtRst = curtRst;
	}
	
    /**
     * @return curtRst
     */
	public java.math.BigDecimal getCurtRst() {
		return this.curtRst;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param preRst1
	 */
	public void setPreRst1(java.math.BigDecimal preRst1) {
		this.preRst1 = preRst1;
	}
	
    /**
     * @return preRst1
     */
	public java.math.BigDecimal getPreRst1() {
		return this.preRst1;
	}
	
	/**
	 * @param curtRst1
	 */
	public void setCurtRst1(java.math.BigDecimal curtRst1) {
		this.curtRst1 = curtRst1;
	}
	
    /**
     * @return curtRst1
     */
	public java.math.BigDecimal getCurtRst1() {
		return this.curtRst1;
	}
	
	/**
	 * @param remark1
	 */
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	
    /**
     * @return remark1
     */
	public String getRemark1() {
		return this.remark1;
	}
	
	/**
	 * @param preRst2
	 */
	public void setPreRst2(java.math.BigDecimal preRst2) {
		this.preRst2 = preRst2;
	}
	
    /**
     * @return preRst2
     */
	public java.math.BigDecimal getPreRst2() {
		return this.preRst2;
	}
	
	/**
	 * @param curtRst2
	 */
	public void setCurtRst2(java.math.BigDecimal curtRst2) {
		this.curtRst2 = curtRst2;
	}
	
    /**
     * @return curtRst2
     */
	public java.math.BigDecimal getCurtRst2() {
		return this.curtRst2;
	}
	
	/**
	 * @param remark2
	 */
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	
    /**
     * @return remark2
     */
	public String getRemark2() {
		return this.remark2;
	}
	
	/**
	 * @param preRst3
	 */
	public void setPreRst3(java.math.BigDecimal preRst3) {
		this.preRst3 = preRst3;
	}
	
    /**
     * @return preRst3
     */
	public java.math.BigDecimal getPreRst3() {
		return this.preRst3;
	}
	
	/**
	 * @param curtRst3
	 */
	public void setCurtRst3(java.math.BigDecimal curtRst3) {
		this.curtRst3 = curtRst3;
	}
	
    /**
     * @return curtRst3
     */
	public java.math.BigDecimal getCurtRst3() {
		return this.curtRst3;
	}
	
	/**
	 * @param remark3
	 */
	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}
	
    /**
     * @return remark3
     */
	public String getRemark3() {
		return this.remark3;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}