/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWydTaskBack
 * @类描述: psp_wyd_task_back数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-30 21:46:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_wyd_task_back")
public class PspWydTaskBack extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 退回原因 **/
	@Column(name = "BACK_REASON", unique = false, nullable = false, length = 100)
	private String backReason;
	
	/** 退回日期 **/
	@Column(name = "BACK_DATE", unique = false, nullable = true, length = 20)
	private String backDate;
	
	/** 实际经营地址 **/
	@Column(name = "REL_ARRDESS", unique = false, nullable = true, length = 200)
	private String relArrdess;
	
	/** 对应支行 **/
	@Column(name = "REL_BR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="relBrIdName" )
	private String relBrId;
	
	/** 联系客户时间1 **/
	@Column(name = "CONTACT_TIME_1", unique = false, nullable = true, length = 20)
	private String contactTime1;
	
	/** 联系客户时间2 **/
	@Column(name = "CONTACT_TIME_2", unique = false, nullable = true, length = 20)
	private String contactTime2;
	
	/** 联系客户时间3 **/
	@Column(name = "CONTACT_TIME_3", unique = false, nullable = true, length = 20)
	private String contactTime3;
	
	/** 联系情况 **/
	@Column(name = "CONTACT_INFO", unique = false, nullable = true, length = 200)
	private String contactInfo;
	
	/** 联系客户手机（座机）号 **/
	@Column(name = "CONTACT_PHONE", unique = false, nullable = true, length = 20)
	private String contactPhone;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param backReason
	 */
	public void setBackReason(String backReason) {
		this.backReason = backReason;
	}
	
    /**
     * @return backReason
     */
	public String getBackReason() {
		return this.backReason;
	}
	
	/**
	 * @param backDate
	 */
	public void setBackDate(String backDate) {
		this.backDate = backDate;
	}
	
    /**
     * @return backDate
     */
	public String getBackDate() {
		return this.backDate;
	}
	
	/**
	 * @param relArrdess
	 */
	public void setRelArrdess(String relArrdess) {
		this.relArrdess = relArrdess;
	}
	
    /**
     * @return relArrdess
     */
	public String getRelArrdess() {
		return this.relArrdess;
	}
	
	/**
	 * @param relBrId
	 */
	public void setRelBrId(String relBrId) {
		this.relBrId = relBrId;
	}
	
    /**
     * @return relBrId
     */
	public String getRelBrId() {
		return this.relBrId;
	}
	
	/**
	 * @param contactTime1
	 */
	public void setContactTime1(String contactTime1) {
		this.contactTime1 = contactTime1;
	}
	
    /**
     * @return contactTime1
     */
	public String getContactTime1() {
		return this.contactTime1;
	}
	
	/**
	 * @param contactTime2
	 */
	public void setContactTime2(String contactTime2) {
		this.contactTime2 = contactTime2;
	}
	
    /**
     * @return contactTime2
     */
	public String getContactTime2() {
		return this.contactTime2;
	}
	
	/**
	 * @param contactTime3
	 */
	public void setContactTime3(String contactTime3) {
		this.contactTime3 = contactTime3;
	}
	
    /**
     * @return contactTime3
     */
	public String getContactTime3() {
		return this.contactTime3;
	}
	
	/**
	 * @param contactInfo
	 */
	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}
	
    /**
     * @return contactInfo
     */
	public String getContactInfo() {
		return this.contactInfo;
	}
	
	/**
	 * @param contactPhone
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	
    /**
     * @return contactPhone
     */
	public String getContactPhone() {
		return this.contactPhone;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}