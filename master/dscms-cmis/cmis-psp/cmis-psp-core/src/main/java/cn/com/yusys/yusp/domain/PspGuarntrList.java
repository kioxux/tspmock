/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspGuarntrList
 * @类描述: psp_guarntr_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_guarntr_list")
public class PspGuarntrList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;

	/** 担保人编号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 40)
	private String guarCusId;

	/** 担保人名称 **/
	@Column(name = "GUAR_NAME", unique = false, nullable = true, length = 80)
	private String guarName;
	
	/** 担保标志 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String guarType;
	
	/** 担保金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 报告期净资产 **/
	@Column(name = "NET_ASSETS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal netAssets;
	
	/** 已对外担保金额 **/
	@Column(name = "CURT_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtGuarAmt;
	
	/** 测算担保人可担保金额 **/
	@Column(name = "EVAL_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalGuarAmt;
	
	/** 是否具担保能力 **/
	@Column(name = "GUAR_ABI", unique = false, nullable = true, length = 5)
	private String guarAbi;
	
	/** 担保人说明 **/
	@Column(name = "GUAR_ABI_EXPL", unique = false, nullable = true, length = 65535)
	private String guarAbiExpl;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param guarName
	 */
	public void setGuarName(String guarName) {
		this.guarName = guarName;
	}
	
    /**
     * @return guarName
     */
	public String getGuarName() {
		return this.guarName;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param netAssets
	 */
	public void setNetAssets(java.math.BigDecimal netAssets) {
		this.netAssets = netAssets;
	}
	
    /**
     * @return netAssets
     */
	public java.math.BigDecimal getNetAssets() {
		return this.netAssets;
	}
	
	/**
	 * @param curtGuarAmt
	 */
	public void setCurtGuarAmt(java.math.BigDecimal curtGuarAmt) {
		this.curtGuarAmt = curtGuarAmt;
	}
	
    /**
     * @return curtGuarAmt
     */
	public java.math.BigDecimal getCurtGuarAmt() {
		return this.curtGuarAmt;
	}
	
	/**
	 * @param evalGuarAmt
	 */
	public void setEvalGuarAmt(java.math.BigDecimal evalGuarAmt) {
		this.evalGuarAmt = evalGuarAmt;
	}
	
    /**
     * @return evalGuarAmt
     */
	public java.math.BigDecimal getEvalGuarAmt() {
		return this.evalGuarAmt;
	}
	
	/**
	 * @param guarAbi
	 */
	public void setGuarAbi(String guarAbi) {
		this.guarAbi = guarAbi;
	}
	
    /**
     * @return guarAbi
     */
	public String getGuarAbi() {
		return this.guarAbi;
	}
	
	/**
	 * @param guarAbiExpl
	 */
	public void setGuarAbiExpl(String guarAbiExpl) {
		this.guarAbiExpl = guarAbiExpl;
	}
	
    /**
     * @return guarAbiExpl
     */
	public String getGuarAbiExpl() {
		return this.guarAbiExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getGuarCusId() {
		return guarCusId;
	}

	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}
}