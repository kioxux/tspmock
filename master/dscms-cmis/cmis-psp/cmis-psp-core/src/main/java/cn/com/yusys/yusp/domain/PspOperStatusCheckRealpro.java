/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckRealpro
 * @类描述: psp_oper_status_check_realpro数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_oper_status_check_realpro")
public class PspOperStatusCheckRealpro extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 项目总投资是否超预算 **/
	@Column(name = "IS_OVER_INVEST", unique = false, nullable = true, length = 5)
	private String isOverInvest;
	
	/** 预算超支说明 **/
	@Column(name = "OVER_EXPL", unique = false, nullable = true, length = 65535)
	private String overExpl;
	
	/** 房地产开发进展是否存在非正常延期情况 **/
	@Column(name = "IS_NORMAL_DELAY", unique = false, nullable = true, length = 5)
	private String isNormalDelay;
	
	/** 非正常延期说明 **/
	@Column(name = "DELAY_EXPL", unique = false, nullable = true, length = 65535)
	private String delayExpl;
	
	/** 房地产开发资质是否过期 **/
	@Column(name = "IS_APTI_TERM", unique = false, nullable = true, length = 5)
	private String isAptiTerm;
	
	/** 开发资质过期说明 **/
	@Column(name = "OVER_TERM_EXPL", unique = false, nullable = true, length = 65535)
	private String overTermExpl;
	
	/** 是否取得预售许可证 **/
	@Column(name = "PRESELL_LIC", unique = false, nullable = true, length = 5)
	private String presellLic;
	
	/** 预售许可证说明 **/
	@Column(name = "PRESELL_EXPL", unique = false, nullable = true, length = 65535)
	private String presellExpl;
	
	/** 房产销售是否正常 **/
	@Column(name = "IS_SALE_NORMAL", unique = false, nullable = true, length = 5)
	private String isSaleNormal;
	
	/** 销售情况说明 **/
	@Column(name = "SALE_NORMAL_EXPL", unique = false, nullable = true, length = 65535)
	private String saleNormalExpl;
	
	/** 贷款使用是否与项目进度相匹配 **/
	@Column(name = "IS_SCHEDULE_MATCHING", unique = false, nullable = true, length = 5)
	private String isScheduleMatching;
	
	/** 贷款匹配说明 **/
	@Column(name = "SCHEDULE_EXPL", unique = false, nullable = true, length = 65535)
	private String scheduleExpl;
	
	/** 是否拖欠工程款 **/
	@Column(name = "IS_DELAY_CAP", unique = false, nullable = true, length = 5)
	private String isDelayCap;
	
	/** 拖欠工程款说明 **/
	@Column(name = "DELAY_REMARK", unique = false, nullable = true, length = 65535)
	private String delayRemark;
	
	/** 是否按我行还款计划按时足额还款 **/
	@Column(name = "IS_REPAY", unique = false, nullable = true, length = 5)
	private String isRepay;
	
	/** 按时还款说明 **/
	@Column(name = "REPAY_EXPL", unique = false, nullable = true, length = 65535)
	private String repayExpl;
	
	/** 是否在我行开立专户 **/
	@Column(name = "IS_ACCT", unique = false, nullable = true, length = 5)
	private String isAcct;
	
	/** 账号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 40)
	private String acctNo;
	
	/** 账户名称 **/
	@Column(name = "ACCT_NAME", unique = false, nullable = true, length = 80)
	private String acctName;
	
	/** 账户序号 **/
	@Column(name = "ACCT_SEQ_NO", unique = false, nullable = true, length = 40)
	private String acctSeqNo;
	
	/** 开户行 **/
	@Column(name = "ACCTB", unique = false, nullable = true, length = 40)
	private String acctb;
	
	/** 未开立专户说明 **/
	@Column(name = "NO_ACCT_EXPL", unique = false, nullable = true, length = 65535)
	private String noAcctExpl;
	
	/** 上期项目预计总投入（万元） **/
	@Column(name = "PRE_RST1", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst1;
	
	/** 本期项目预计总投入（万元） **/
	@Column(name = "CURT_RST1", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst1;
	
	/** 项目预计总投入说明 **/
	@Column(name = "REMARK1", unique = false, nullable = true, length = 65535)
	private String remark1;
	
	/** 上期项目实际累计总投入（万元） **/
	@Column(name = "PRE_RST2", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst2;
	
	/** 本期项目实际累计总投入（万元） **/
	@Column(name = "CURT_RST2", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst2;
	
	/** 项目实际累计总投入说明 **/
	@Column(name = "REMARK2", unique = false, nullable = true, length = 65535)
	private String remark2;
	
	/** 上期我行已投入贷款（万元） **/
	@Column(name = "PRE_RST3", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst3;
	
	/** 本期我行已投入贷款（万元） **/
	@Column(name = "CURT_RST3", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst3;
	
	/** 我行已投入贷款说明 **/
	@Column(name = "REMARK3", unique = false, nullable = true, length = 65535)
	private String remark3;
	
	/** 上期他行已投入贷款（万元） **/
	@Column(name = "PRE_RST4", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst4;
	
	/** 本期他行已投入贷款（万元） **/
	@Column(name = "CURT_RST4", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst4;
	
	/** 他行已投入贷款说明 **/
	@Column(name = "REMARK4", unique = false, nullable = true, length = 65535)
	private String remark4;
	
	/** 上期借款人已投入自有资金（万元） **/
	@Column(name = "PRE_RST5", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst5;
	
	/** 本期借款人已投入自有资金（万元） **/
	@Column(name = "CURT_RST5", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst5;
	
	/** 借款人已投入自有资金说明 **/
	@Column(name = "REMARK5", unique = false, nullable = true, length = 65535)
	private String remark5;
	
	/** 上期项目开发进度 **/
	@Column(name = "PRE_RST6", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst6;
	
	/** 本期项目开发进度 **/
	@Column(name = "CURT_RST6", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst6;
	
	/** 项目开发进度说明 **/
	@Column(name = "REMARK6", unique = false, nullable = true, length = 65535)
	private String remark6;
	
	/** 上期可销售面积（平方米） **/
	@Column(name = "PRE_RST7", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst7;
	
	/** 本期可销售面积（平方米） **/
	@Column(name = "CURT_RST7", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst7;
	
	/** 可销售面积说明 **/
	@Column(name = "REMARK7", unique = false, nullable = true, length = 65535)
	private String remark7;
	
	/** 上期已销售面积（平方米） **/
	@Column(name = "PRE_RST8", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst8;
	
	/** 本期已销售面积（平方米） **/
	@Column(name = "CURT_RST8", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst8;
	
	/** 已销售面积说明 **/
	@Column(name = "REMARK8", unique = false, nullable = true, length = 65535)
	private String remark8;
	
	/** 上期已销售总价（万元） **/
	@Column(name = "PRE_RST9", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst9;
	
	/** 本期已销售总价（万元） **/
	@Column(name = "CURT_RST9", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst9;
	
	/** 已销售总价说明 **/
	@Column(name = "REMARK9", unique = false, nullable = true, length = 65535)
	private String remark9;
	
	/** 上期已预售面积（平方米） **/
	@Column(name = "PRE_RST10", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst10;
	
	/** 本期已预售面积（平方米） **/
	@Column(name = "CURT_RST10", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst10;
	
	/** 已预售面积说明 **/
	@Column(name = "REMARK10", unique = false, nullable = true, length = 65535)
	private String remark10;
	
	/** 上期已预售总价（万元） **/
	@Column(name = "PRE_RST11", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst11;
	
	/** 本期已预售总价（万元） **/
	@Column(name = "CURT_RST11", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst11;
	
	/** 已预售总价说明 **/
	@Column(name = "REMARK11", unique = false, nullable = true, length = 65535)
	private String remark11;
	
	/** 上期销售均价（万元） **/
	@Column(name = "PRE_RST12", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst12;
	
	/** 本期销售均价（万元） **/
	@Column(name = "CURT_RST12", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst12;
	
	/** 销售均价说明 **/
	@Column(name = "REMARK12", unique = false, nullable = true, length = 65535)
	private String remark12;
	
	/** 上期回笼我行资金（万元） **/
	@Column(name = "PRE_RST13", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preRst13;
	
	/** 本期回笼我行资金（万元） **/
	@Column(name = "CURT_RST13", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtRst13;
	
	/** 回笼我行资金说明 **/
	@Column(name = "REMARK13", unique = false, nullable = true, length = 65535)
	private String remark13;
	
	/** 开发销售情况描述 **/
	@Column(name = "REMARK14", unique = false, nullable = true, length = 65535)
	private String remark14;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isOverInvest
	 */
	public void setIsOverInvest(String isOverInvest) {
		this.isOverInvest = isOverInvest;
	}
	
    /**
     * @return isOverInvest
     */
	public String getIsOverInvest() {
		return this.isOverInvest;
	}
	
	/**
	 * @param overExpl
	 */
	public void setOverExpl(String overExpl) {
		this.overExpl = overExpl;
	}
	
    /**
     * @return overExpl
     */
	public String getOverExpl() {
		return this.overExpl;
	}
	
	/**
	 * @param isNormalDelay
	 */
	public void setIsNormalDelay(String isNormalDelay) {
		this.isNormalDelay = isNormalDelay;
	}
	
    /**
     * @return isNormalDelay
     */
	public String getIsNormalDelay() {
		return this.isNormalDelay;
	}
	
	/**
	 * @param delayExpl
	 */
	public void setDelayExpl(String delayExpl) {
		this.delayExpl = delayExpl;
	}
	
    /**
     * @return delayExpl
     */
	public String getDelayExpl() {
		return this.delayExpl;
	}
	
	/**
	 * @param isAptiTerm
	 */
	public void setIsAptiTerm(String isAptiTerm) {
		this.isAptiTerm = isAptiTerm;
	}
	
    /**
     * @return isAptiTerm
     */
	public String getIsAptiTerm() {
		return this.isAptiTerm;
	}
	
	/**
	 * @param overTermExpl
	 */
	public void setOverTermExpl(String overTermExpl) {
		this.overTermExpl = overTermExpl;
	}
	
    /**
     * @return overTermExpl
     */
	public String getOverTermExpl() {
		return this.overTermExpl;
	}
	
	/**
	 * @param presellLic
	 */
	public void setPresellLic(String presellLic) {
		this.presellLic = presellLic;
	}
	
    /**
     * @return presellLic
     */
	public String getPresellLic() {
		return this.presellLic;
	}
	
	/**
	 * @param presellExpl
	 */
	public void setPresellExpl(String presellExpl) {
		this.presellExpl = presellExpl;
	}
	
    /**
     * @return presellExpl
     */
	public String getPresellExpl() {
		return this.presellExpl;
	}
	
	/**
	 * @param isSaleNormal
	 */
	public void setIsSaleNormal(String isSaleNormal) {
		this.isSaleNormal = isSaleNormal;
	}
	
    /**
     * @return isSaleNormal
     */
	public String getIsSaleNormal() {
		return this.isSaleNormal;
	}
	
	/**
	 * @param saleNormalExpl
	 */
	public void setSaleNormalExpl(String saleNormalExpl) {
		this.saleNormalExpl = saleNormalExpl;
	}
	
    /**
     * @return saleNormalExpl
     */
	public String getSaleNormalExpl() {
		return this.saleNormalExpl;
	}
	
	/**
	 * @param isScheduleMatching
	 */
	public void setIsScheduleMatching(String isScheduleMatching) {
		this.isScheduleMatching = isScheduleMatching;
	}
	
    /**
     * @return isScheduleMatching
     */
	public String getIsScheduleMatching() {
		return this.isScheduleMatching;
	}
	
	/**
	 * @param scheduleExpl
	 */
	public void setScheduleExpl(String scheduleExpl) {
		this.scheduleExpl = scheduleExpl;
	}
	
    /**
     * @return scheduleExpl
     */
	public String getScheduleExpl() {
		return this.scheduleExpl;
	}
	
	/**
	 * @param isDelayCap
	 */
	public void setIsDelayCap(String isDelayCap) {
		this.isDelayCap = isDelayCap;
	}
	
    /**
     * @return isDelayCap
     */
	public String getIsDelayCap() {
		return this.isDelayCap;
	}
	
	/**
	 * @param delayRemark
	 */
	public void setDelayRemark(String delayRemark) {
		this.delayRemark = delayRemark;
	}
	
    /**
     * @return delayRemark
     */
	public String getDelayRemark() {
		return this.delayRemark;
	}
	
	/**
	 * @param isRepay
	 */
	public void setIsRepay(String isRepay) {
		this.isRepay = isRepay;
	}
	
    /**
     * @return isRepay
     */
	public String getIsRepay() {
		return this.isRepay;
	}
	
	/**
	 * @param repayExpl
	 */
	public void setRepayExpl(String repayExpl) {
		this.repayExpl = repayExpl;
	}
	
    /**
     * @return repayExpl
     */
	public String getRepayExpl() {
		return this.repayExpl;
	}
	
	/**
	 * @param isAcct
	 */
	public void setIsAcct(String isAcct) {
		this.isAcct = isAcct;
	}
	
    /**
     * @return isAcct
     */
	public String getIsAcct() {
		return this.isAcct;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	
    /**
     * @return acctName
     */
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param acctSeqNo
	 */
	public void setAcctSeqNo(String acctSeqNo) {
		this.acctSeqNo = acctSeqNo;
	}
	
    /**
     * @return acctSeqNo
     */
	public String getAcctSeqNo() {
		return this.acctSeqNo;
	}
	
	/**
	 * @param acctb
	 */
	public void setAcctb(String acctb) {
		this.acctb = acctb;
	}
	
    /**
     * @return acctb
     */
	public String getAcctb() {
		return this.acctb;
	}
	
	/**
	 * @param noAcctExpl
	 */
	public void setNoAcctExpl(String noAcctExpl) {
		this.noAcctExpl = noAcctExpl;
	}
	
    /**
     * @return noAcctExpl
     */
	public String getNoAcctExpl() {
		return this.noAcctExpl;
	}
	
	/**
	 * @param preRst1
	 */
	public void setPreRst1(java.math.BigDecimal preRst1) {
		this.preRst1 = preRst1;
	}
	
    /**
     * @return preRst1
     */
	public java.math.BigDecimal getPreRst1() {
		return this.preRst1;
	}
	
	/**
	 * @param curtRst1
	 */
	public void setCurtRst1(java.math.BigDecimal curtRst1) {
		this.curtRst1 = curtRst1;
	}
	
    /**
     * @return curtRst1
     */
	public java.math.BigDecimal getCurtRst1() {
		return this.curtRst1;
	}
	
	/**
	 * @param remark1
	 */
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	
    /**
     * @return remark1
     */
	public String getRemark1() {
		return this.remark1;
	}
	
	/**
	 * @param preRst2
	 */
	public void setPreRst2(java.math.BigDecimal preRst2) {
		this.preRst2 = preRst2;
	}
	
    /**
     * @return preRst2
     */
	public java.math.BigDecimal getPreRst2() {
		return this.preRst2;
	}
	
	/**
	 * @param curtRst2
	 */
	public void setCurtRst2(java.math.BigDecimal curtRst2) {
		this.curtRst2 = curtRst2;
	}
	
    /**
     * @return curtRst2
     */
	public java.math.BigDecimal getCurtRst2() {
		return this.curtRst2;
	}
	
	/**
	 * @param remark2
	 */
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	
    /**
     * @return remark2
     */
	public String getRemark2() {
		return this.remark2;
	}
	
	/**
	 * @param preRst3
	 */
	public void setPreRst3(java.math.BigDecimal preRst3) {
		this.preRst3 = preRst3;
	}
	
    /**
     * @return preRst3
     */
	public java.math.BigDecimal getPreRst3() {
		return this.preRst3;
	}
	
	/**
	 * @param curtRst3
	 */
	public void setCurtRst3(java.math.BigDecimal curtRst3) {
		this.curtRst3 = curtRst3;
	}
	
    /**
     * @return curtRst3
     */
	public java.math.BigDecimal getCurtRst3() {
		return this.curtRst3;
	}
	
	/**
	 * @param remark3
	 */
	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}
	
    /**
     * @return remark3
     */
	public String getRemark3() {
		return this.remark3;
	}
	
	/**
	 * @param preRst4
	 */
	public void setPreRst4(java.math.BigDecimal preRst4) {
		this.preRst4 = preRst4;
	}
	
    /**
     * @return preRst4
     */
	public java.math.BigDecimal getPreRst4() {
		return this.preRst4;
	}
	
	/**
	 * @param curtRst4
	 */
	public void setCurtRst4(java.math.BigDecimal curtRst4) {
		this.curtRst4 = curtRst4;
	}
	
    /**
     * @return curtRst4
     */
	public java.math.BigDecimal getCurtRst4() {
		return this.curtRst4;
	}
	
	/**
	 * @param remark4
	 */
	public void setRemark4(String remark4) {
		this.remark4 = remark4;
	}
	
    /**
     * @return remark4
     */
	public String getRemark4() {
		return this.remark4;
	}
	
	/**
	 * @param preRst5
	 */
	public void setPreRst5(java.math.BigDecimal preRst5) {
		this.preRst5 = preRst5;
	}
	
    /**
     * @return preRst5
     */
	public java.math.BigDecimal getPreRst5() {
		return this.preRst5;
	}
	
	/**
	 * @param curtRst5
	 */
	public void setCurtRst5(java.math.BigDecimal curtRst5) {
		this.curtRst5 = curtRst5;
	}
	
    /**
     * @return curtRst5
     */
	public java.math.BigDecimal getCurtRst5() {
		return this.curtRst5;
	}
	
	/**
	 * @param remark5
	 */
	public void setRemark5(String remark5) {
		this.remark5 = remark5;
	}
	
    /**
     * @return remark5
     */
	public String getRemark5() {
		return this.remark5;
	}
	
	/**
	 * @param preRst6
	 */
	public void setPreRst6(java.math.BigDecimal preRst6) {
		this.preRst6 = preRst6;
	}
	
    /**
     * @return preRst6
     */
	public java.math.BigDecimal getPreRst6() {
		return this.preRst6;
	}
	
	/**
	 * @param curtRst6
	 */
	public void setCurtRst6(java.math.BigDecimal curtRst6) {
		this.curtRst6 = curtRst6;
	}
	
    /**
     * @return curtRst6
     */
	public java.math.BigDecimal getCurtRst6() {
		return this.curtRst6;
	}
	
	/**
	 * @param remark6
	 */
	public void setRemark6(String remark6) {
		this.remark6 = remark6;
	}
	
    /**
     * @return remark6
     */
	public String getRemark6() {
		return this.remark6;
	}
	
	/**
	 * @param preRst7
	 */
	public void setPreRst7(java.math.BigDecimal preRst7) {
		this.preRst7 = preRst7;
	}
	
    /**
     * @return preRst7
     */
	public java.math.BigDecimal getPreRst7() {
		return this.preRst7;
	}
	
	/**
	 * @param curtRst7
	 */
	public void setCurtRst7(java.math.BigDecimal curtRst7) {
		this.curtRst7 = curtRst7;
	}
	
    /**
     * @return curtRst7
     */
	public java.math.BigDecimal getCurtRst7() {
		return this.curtRst7;
	}
	
	/**
	 * @param remark7
	 */
	public void setRemark7(String remark7) {
		this.remark7 = remark7;
	}
	
    /**
     * @return remark7
     */
	public String getRemark7() {
		return this.remark7;
	}
	
	/**
	 * @param preRst8
	 */
	public void setPreRst8(java.math.BigDecimal preRst8) {
		this.preRst8 = preRst8;
	}
	
    /**
     * @return preRst8
     */
	public java.math.BigDecimal getPreRst8() {
		return this.preRst8;
	}
	
	/**
	 * @param curtRst8
	 */
	public void setCurtRst8(java.math.BigDecimal curtRst8) {
		this.curtRst8 = curtRst8;
	}
	
    /**
     * @return curtRst8
     */
	public java.math.BigDecimal getCurtRst8() {
		return this.curtRst8;
	}
	
	/**
	 * @param remark8
	 */
	public void setRemark8(String remark8) {
		this.remark8 = remark8;
	}
	
    /**
     * @return remark8
     */
	public String getRemark8() {
		return this.remark8;
	}
	
	/**
	 * @param preRst9
	 */
	public void setPreRst9(java.math.BigDecimal preRst9) {
		this.preRst9 = preRst9;
	}
	
    /**
     * @return preRst9
     */
	public java.math.BigDecimal getPreRst9() {
		return this.preRst9;
	}
	
	/**
	 * @param curtRst9
	 */
	public void setCurtRst9(java.math.BigDecimal curtRst9) {
		this.curtRst9 = curtRst9;
	}
	
    /**
     * @return curtRst9
     */
	public java.math.BigDecimal getCurtRst9() {
		return this.curtRst9;
	}
	
	/**
	 * @param remark9
	 */
	public void setRemark9(String remark9) {
		this.remark9 = remark9;
	}
	
    /**
     * @return remark9
     */
	public String getRemark9() {
		return this.remark9;
	}
	
	/**
	 * @param preRst10
	 */
	public void setPreRst10(java.math.BigDecimal preRst10) {
		this.preRst10 = preRst10;
	}
	
    /**
     * @return preRst10
     */
	public java.math.BigDecimal getPreRst10() {
		return this.preRst10;
	}
	
	/**
	 * @param curtRst10
	 */
	public void setCurtRst10(java.math.BigDecimal curtRst10) {
		this.curtRst10 = curtRst10;
	}
	
    /**
     * @return curtRst10
     */
	public java.math.BigDecimal getCurtRst10() {
		return this.curtRst10;
	}
	
	/**
	 * @param remark10
	 */
	public void setRemark10(String remark10) {
		this.remark10 = remark10;
	}
	
    /**
     * @return remark10
     */
	public String getRemark10() {
		return this.remark10;
	}
	
	/**
	 * @param preRst11
	 */
	public void setPreRst11(java.math.BigDecimal preRst11) {
		this.preRst11 = preRst11;
	}
	
    /**
     * @return preRst11
     */
	public java.math.BigDecimal getPreRst11() {
		return this.preRst11;
	}
	
	/**
	 * @param curtRst11
	 */
	public void setCurtRst11(java.math.BigDecimal curtRst11) {
		this.curtRst11 = curtRst11;
	}
	
    /**
     * @return curtRst11
     */
	public java.math.BigDecimal getCurtRst11() {
		return this.curtRst11;
	}
	
	/**
	 * @param remark11
	 */
	public void setRemark11(String remark11) {
		this.remark11 = remark11;
	}
	
    /**
     * @return remark11
     */
	public String getRemark11() {
		return this.remark11;
	}
	
	/**
	 * @param preRst12
	 */
	public void setPreRst12(java.math.BigDecimal preRst12) {
		this.preRst12 = preRst12;
	}
	
    /**
     * @return preRst12
     */
	public java.math.BigDecimal getPreRst12() {
		return this.preRst12;
	}
	
	/**
	 * @param curtRst12
	 */
	public void setCurtRst12(java.math.BigDecimal curtRst12) {
		this.curtRst12 = curtRst12;
	}
	
    /**
     * @return curtRst12
     */
	public java.math.BigDecimal getCurtRst12() {
		return this.curtRst12;
	}
	
	/**
	 * @param remark12
	 */
	public void setRemark12(String remark12) {
		this.remark12 = remark12;
	}
	
    /**
     * @return remark12
     */
	public String getRemark12() {
		return this.remark12;
	}
	
	/**
	 * @param preRst13
	 */
	public void setPreRst13(java.math.BigDecimal preRst13) {
		this.preRst13 = preRst13;
	}
	
    /**
     * @return preRst13
     */
	public java.math.BigDecimal getPreRst13() {
		return this.preRst13;
	}
	
	/**
	 * @param curtRst13
	 */
	public void setCurtRst13(java.math.BigDecimal curtRst13) {
		this.curtRst13 = curtRst13;
	}
	
    /**
     * @return curtRst13
     */
	public java.math.BigDecimal getCurtRst13() {
		return this.curtRst13;
	}
	
	/**
	 * @param remark13
	 */
	public void setRemark13(String remark13) {
		this.remark13 = remark13;
	}
	
    /**
     * @return remark13
     */
	public String getRemark13() {
		return this.remark13;
	}
	
	/**
	 * @param remark14
	 */
	public void setRemark14(String remark14) {
		this.remark14 = remark14;
	}
	
    /**
     * @return remark14
     */
	public String getRemark14() {
		return this.remark14;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}