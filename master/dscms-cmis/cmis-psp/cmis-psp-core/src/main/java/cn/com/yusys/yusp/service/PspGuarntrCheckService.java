/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PspPldimnCheck;
import cn.com.yusys.yusp.dto.PspGuarntrAndPldimnDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PspGuarntrCheck;
import cn.com.yusys.yusp.repository.mapper.PspGuarntrCheckMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspGuarntrCheckService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspGuarntrCheckService {

    @Autowired
    private PspGuarntrCheckMapper pspGuarntrCheckMapper;
    @Autowired
    private PspPldimnCheckService pspPldimnCheckService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PspGuarntrCheck selectByPrimaryKey(String pkId) {
        return pspGuarntrCheckMapper.selectByPrimaryKey(pkId);
    }
	/**
     * @方法名称: queryPspGuarntrCheck
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspGuarntrCheck queryPspGuarntrCheck(PspGuarntrCheck pspGuarntrCheck) {
        return pspGuarntrCheckMapper.queryPspGuarntrCheck(pspGuarntrCheck);
    }
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PspGuarntrCheck> selectAll(QueryModel model) {
        List<PspGuarntrCheck> records = (List<PspGuarntrCheck>) pspGuarntrCheckMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PspGuarntrCheck> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PspGuarntrCheck> list = pspGuarntrCheckMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PspGuarntrCheck record) {
        return pspGuarntrCheckMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PspGuarntrCheck record) {
        return pspGuarntrCheckMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PspGuarntrCheck record) {
        int count = 0;
        PspGuarntrCheck pspGuarntrCheck = pspGuarntrCheckMapper.selectByPrimaryKey(record.getPkId());
        if (pspGuarntrCheck != null ){
            pspGuarntrCheckMapper.updateByPrimaryKey(record);
        }else {
            pspGuarntrCheckMapper.insert(record);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PspGuarntrCheck record) {
        return pspGuarntrCheckMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pspGuarntrCheckMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pspGuarntrCheckMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateGuarntrAndPldimn
     * @方法描述: 根据任务编号更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateGuarntrAndPldimn(PspGuarntrAndPldimnDto record) {
        PspGuarntrCheck pspGuarntrCheck = new PspGuarntrCheck();
        PspPldimnCheck pspPldimnCheck = new PspPldimnCheck();
        record.setUpdateTime(DateUtils.getCurrDate());
        org.springframework.beans.BeanUtils.copyProperties(record,pspGuarntrCheck);
        org.springframework.beans.BeanUtils.copyProperties(record,pspPldimnCheck);
        pspPldimnCheck.setIsOtherRiskEvent(record.getIsOtherRiskEvent4Pldimn());
        pspPldimnCheck.setRiskEventExpl(record.getRiskEventExpl4Pldimn());
        pspPldimnCheck.setTotlEvlu(record.getTotlEvlu4Pldimn());
        if(pspGuarntrCheckMapper.updateByTaskNo(pspGuarntrCheck) == 0){
            pspGuarntrCheck.setCreateTime(DateUtils.getCurrDate());
            pspGuarntrCheckMapper.insert(pspGuarntrCheck);
        }
        if(pspPldimnCheckService.updateByTaskNo(pspPldimnCheck) == 0){
            pspGuarntrCheck.setCreateTime(DateUtils.getCurrDate());
            pspPldimnCheckService.insert(pspPldimnCheck);
        }
        return 0;
    }
}
