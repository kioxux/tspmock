/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspPropertyCheck;
import cn.com.yusys.yusp.service.PspPropertyCheckService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPropertyCheckResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/psppropertycheck")
public class PspPropertyCheckResource {
    @Autowired
    private PspPropertyCheckService pspPropertyCheckService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspPropertyCheck>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspPropertyCheck> list = pspPropertyCheckService.selectAll(queryModel);
        return new ResultDto<List<PspPropertyCheck>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PspPropertyCheck>> index(QueryModel queryModel) {
        List<PspPropertyCheck> list = pspPropertyCheckService.selectByModel(queryModel);
        return new ResultDto<List<PspPropertyCheck>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @GetMapping("/queryList")
    protected ResultDto<List<PspPropertyCheck>> queryList(QueryModel queryModel) {
        List<PspPropertyCheck> list = pspPropertyCheckService.selectByModel(queryModel);
        return new ResultDto<List<PspPropertyCheck>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspPropertyCheck> show(@PathVariable("pkId") String pkId) {
        PspPropertyCheck pspPropertyCheck = pspPropertyCheckService.selectByPrimaryKey(pkId);
        return new ResultDto<PspPropertyCheck>(pspPropertyCheck);
    }
    /**
     * @函数名称:queryPspPropertyCheck
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<PspPropertyCheck> queryPspPropertyCheck(@RequestBody PspPropertyCheck pspPropertyCheck) {
        pspPropertyCheck = pspPropertyCheckService.queryPspPropertyCheck(pspPropertyCheck);
        return new ResultDto<PspPropertyCheck>(pspPropertyCheck);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PspPropertyCheck> create(@RequestBody PspPropertyCheck pspPropertyCheck) throws URISyntaxException {
        pspPropertyCheckService.insert(pspPropertyCheck);
        return new ResultDto<PspPropertyCheck>(pspPropertyCheck);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspPropertyCheck pspPropertyCheck) throws URISyntaxException {
        int result = pspPropertyCheckService.update(pspPropertyCheck);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspPropertyCheckService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspPropertyCheckService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: sendToIb1253
     * @函数描述: 调用ib1253查询子序号
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据账号自动回显名称")
    @PostMapping("/sendToIb1253")
    protected ResultDto<Ib1253RespDto> sendToIb1253(@RequestBody Map map) {
        ResultDto<Ib1253RespDto> resultDto = pspPropertyCheckService.sendToIb1253(map);
        return resultDto;
    }
}
