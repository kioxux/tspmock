package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.RiskClassChgApp;
import cn.com.yusys.yusp.domain.RiskTaskList;
import cn.com.yusys.yusp.flow.ClientCons;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFException;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.RiskClassChgAppService;
import cn.com.yusys.yusp.service.RiskTaskListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

/**
 * @author xcc
 * @version 1.0.0
 * @date 2021/06/20
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
class DHGL15BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(DHGL15BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private RiskTaskListService riskTaskListService;
    @Autowired
    private RiskClassChgAppService riskClassChgAppService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("后业务处理类型:" + currentOpType);
        Map<String, Object> param = resultInstanceDto.getParam();
        String bizType = Optional.ofNullable((String) param.get("bizType")).orElse("");
        try {

            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + resultInstanceDto);
                updateApproveStatus(bizType, serno, "111");
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                //审批通过
                log.info("结束操作:" + resultInstanceDto);
                // 更新台账五级/十级分类结果
                updateAccLoanFiveClass(bizType, serno);
                // 更新审批状态通过 997
                updateApproveStatus(bizType, serno, "997");
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                updateApproveStatus(bizType, serno, "992");
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                updateApproveStatus(bizType, serno, "992");
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             态改为追回 991
                log.info("拿回操作:" + resultInstanceDto);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateApproveStatus(bizType, serno, "998");
                log.info("否决操作结束:" + resultInstanceDto);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                WFException exception = new WFException();
                exception.setBizId(resultInstanceDto.getBizId());
                exception.setBizType(resultInstanceDto.getBizType());
                exception.setFlowName(resultInstanceDto.getFlowName());
                exception.setInstanceId(resultInstanceDto.getInstanceId());
                exception.setNodeId(resultInstanceDto.getNodeId());
                exception.setNodeName(resultInstanceDto.getNodeName());
                exception.setUserId(resultInstanceDto.getCurrentUserId());
                exception.setOpType(currentOpType);
                // 后业务处理失败时，将异常信息保存到异常表中
                amqpTemplate.convertAndSend(ClientCons.queue_exception, exception);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 根据业务场景更新审批状态
     *
     * @author jijian_yx
     * @date 2021/8/12 17:40
     **/
    private void updateApproveStatus(String bizType, String serno, String approveStatus) {
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        if ("task".equals(bizType)) {
            // 风险分类初分认定
            RiskTaskList riskTaskList = new RiskTaskList();
            riskTaskList.setTaskNo(serno);
            riskTaskList.setApproveStatus(approveStatus);
            if ("997".equals(approveStatus)) {
                riskTaskList.setCheckStatus("3");
                riskTaskList.setCheckDate(openDay);
            }
            riskTaskListService.updateByTaskNoSelective(riskTaskList);
        } else if ("adjust".equals(bizType)) {
            // 风险分类调整
            RiskClassChgApp riskClassChgApp = new RiskClassChgApp();
            riskClassChgApp.setSerno(serno);
            riskClassChgApp.setApproveStatus(approveStatus);
            riskClassChgAppService.updateBySernoSelective(riskClassChgApp);
        }
    }

    /**
     * 根据业务场景更新台账五级/十级分类
     *
     * @author jijian_yx
     * @date 2021/8/12 19:40
     **/
    private void updateAccLoanFiveClass(String bizType, String serno) throws Exception {
        try {
            if ("task".equals(bizType)) {
                // 风险分类初分认定
                RiskTaskList riskTaskList = new RiskTaskList();
                riskTaskList.setTaskNo(serno);
                riskTaskListService.updateAccLoanFiveClass(riskTaskList);
            } else if ("adjust".equals(bizType)) {
                // 风险分类调整
                RiskClassChgApp riskClassChgApp = new RiskClassChgApp();
                riskClassChgApp.setSerno(serno);
                riskClassChgAppService.updateAccLoanFiveClass(riskClassChgApp);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DHGL15.equals(flowCode);
    }
}
