package cn.com.yusys.yusp.service.server.xdxw0037;


import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0037.req.Xdxw0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.resp.Xdxw0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.PspTaskListMapper;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp模块
 * @类名称: Xdxw0037Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zhaoyue
 * @创建时间: 2021-06-04 09:38:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0037Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0037Service.class);

    @Resource
    private PspTaskListMapper pspTaskListMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * 交易码：Xdxw0037
     * 交易描述：查询借款人是否存在未完成的贷后检查任务
     *
     * @param xdxw0037DataReqDto
     * @return
     */
    @Transactional
    public Xdxw0037DataRespDto xdxw0037(Xdxw0037DataReqDto xdxw0037DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, JSON.toJSONString(xdxw0037DataReqDto));
        //定义返回信息
        Xdxw0037DataRespDto xdxw0037DataRespDto = new Xdxw0037DataRespDto();
        try {
            //请求字段
            String cusId = xdxw0037DataReqDto.getCusId();
            String managerId = xdxw0037DataReqDto.getManagerId();

            //返回字段
            String isExist = StringUtils.EMPTY;// 是否存在未完成的贷后检查任务  字典项：Y是；N否
            int firstCheckNum = 0;// 首检笔数
            int regCheckNum = 0;// 定检笔数
            int noRegCheckNum = 0;// 不定期检查笔数

            //查询借款人是否存在未完成的贷后检查任务
            List<java.util.Map<String, String>> modelList = pspTaskListMapper.selectPspTaskNoListByCusId(cusId);
            if (StringUtil.isNotEmpty(managerId)) {//根据客户经理查询
                String openday = stringRedisTemplate.opsForValue().get("openDay");//当前日期
                modelList = pspTaskListMapper.selectPspTaskNoListByManagerId(managerId,openday);
            }


            if (CollectionUtils.nonNull(modelList)) {
                if (modelList.size() > 0) {
                    isExist = "Y";
                } else {
                    isExist = "N";
                }

                for (int i = 0; i < modelList.size(); i++) {
                    Map<String, String> map = modelList.get(i);
                    //                STD_ZB_CHECK_TYPE
                    //                11-首次检查（对公）
                    //                12-首次检查（个人经营性）
                    //                13-首次检查（个人消费性）
                    //                14-首次检查（小微经营性）
                    //                15-首次检查（小微消费性）
                    //                21-定期检查（对公）
                    //                22-定期检查（个人经营性）
                    //                23-定期检查（个人消费性）
                    //                24-定期检查（小微经营性）
                    //                25-定期检查（小微消费性）
                    //                26-定期检查（同业授信）
                    //                27-定期检查（主体授信）
                    //                28-定期检查（产品授信）
                    String checkType = "";
                    //pspTaskNo.getChekType();//检查类型
                    if (map.containsKey("checktype")) {
                        checkType = map.get("checktype");
                    }
                    Integer num = 0;
                    if (map.containsKey("countnum")) {
                        String count = String.valueOf(map.get("countnum"));//map.get("countnum").toString();
                        num = Integer.valueOf(count);
                    }
                    if ("11".equals(checkType) || "12".equals(checkType) || "13".equals(checkType) || "14".equals(checkType) || "15".equals(checkType)) {
                        firstCheckNum += num;
                    } else if ("21".equals(checkType) || "22".equals(checkType) || "23".equals(checkType) || "24".equals(checkType) || "25".equals(checkType)
                            || "26".equals(checkType) || "27".equals(checkType) || "28".equals(checkType)) {
                        regCheckNum += num;
                    } else if ("31".equals(checkType) || "32".equals(checkType) || "33".equals(checkType) || "34".equals(checkType) || "35".equals(checkType)
                            || "36".equals(checkType) || "37".equals(checkType) || "38".equals(checkType) || "41".equals(checkType)) {
                        noRegCheckNum += num;
                    }
                }
            }

            xdxw0037DataRespDto.setIsExist(isExist);// 是否存在未完成的贷后检查任务  字典项：Y是；N否
            xdxw0037DataRespDto.setFirstCheckNum(firstCheckNum);// 首检笔数
            xdxw0037DataRespDto.setRegCheckNum(regCheckNum);// 定检笔数
            xdxw0037DataRespDto.setNoRegCheckNum(noRegCheckNum);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, JSON.toJSONString(xdxw0037DataRespDto));
        return xdxw0037DataRespDto;
    }


}
