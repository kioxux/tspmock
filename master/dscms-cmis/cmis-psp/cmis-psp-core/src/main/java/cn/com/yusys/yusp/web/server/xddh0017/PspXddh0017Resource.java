package cn.com.yusys.yusp.web.server.xddh0017;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0017.req.Xddh0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0017.resp.Xddh0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddh0017.Xddh0017Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:贷后任务完成接收接口
 *
 * @author wrw
 * @version 1.0
 */
@Api(tags = "XDDH0017:贷后任务完成接收接口")
@RestController
@RequestMapping("/api/pspqt4bsp")
public class PspXddh0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(PspXddh0017Resource.class);

    @Autowired
    private Xddh0017Service xddh0017Service;
    /**
     * 交易码：xddh0017
     * 交易描述：贷后任务完成接收接口
     *
     * @param xddh0017DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贷后任务完成接收接口")
    @PostMapping("/xddh0017")
    protected @ResponseBody
    ResultDto<Xddh0017DataRespDto> xddh0017(@Validated @RequestBody Xddh0017DataReqDto xddh0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value, JSON.toJSONString(xddh0017DataReqDto));
        Xddh0017DataRespDto xddh0017DataRespDto = new Xddh0017DataRespDto();// 响应Dto:贷后任务完成接收接口
        ResultDto<Xddh0017DataRespDto> xddh0017DataResultDto = new ResultDto<>();
        try {
            // 从xddh0017DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value, JSON.toJSONString(xddh0017DataReqDto));
            xddh0017DataRespDto = xddh0017Service.xddh0017(xddh0017DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value, JSON.toJSONString(xddh0017DataReqDto));
            // 封装xddh0017DataResultDto中正确的返回码和返回信息
            xddh0017DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0017DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value, e.getMessage());
            // 封装xddh0017DataResultDto中异常返回码和返回信息
            xddh0017DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0017DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0017DataRespDto到xddh0017DataResultDto中
        xddh0017DataResultDto.setData(xddh0017DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value, JSON.toJSONString(xddh0017DataResultDto));
        return xddh0017DataResultDto;
    }
}

