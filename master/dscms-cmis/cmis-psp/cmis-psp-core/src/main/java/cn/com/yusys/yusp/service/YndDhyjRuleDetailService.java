/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.YndDhyjRuleDetail;
import cn.com.yusys.yusp.repository.mapper.YndDhyjRuleDetailMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: YndDhyjRuleDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 21:58:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class YndDhyjRuleDetailService {

    @Autowired
    private YndDhyjRuleDetailMapper yndDhyjRuleDetailMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public YndDhyjRuleDetail selectByPrimaryKey(String pkId) {
        return yndDhyjRuleDetailMapper.selectByPrimaryKey(pkId);
    }
	/**
     * @方法名称: queryYndDhyjRuleDetail
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public YndDhyjRuleDetail queryYndDhyjRuleDetail(YndDhyjRuleDetail yndDhyjRuleDetail) {
        return yndDhyjRuleDetailMapper.queryYndDhyjRuleDetail(yndDhyjRuleDetail);
    }
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<YndDhyjRuleDetail> selectAll(QueryModel model) {
        List<YndDhyjRuleDetail> records = (List<YndDhyjRuleDetail>) yndDhyjRuleDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<YndDhyjRuleDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<YndDhyjRuleDetail> list = yndDhyjRuleDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(YndDhyjRuleDetail record) {
        return yndDhyjRuleDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(YndDhyjRuleDetail record) {
        return yndDhyjRuleDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(YndDhyjRuleDetail record) {
        return yndDhyjRuleDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(YndDhyjRuleDetail record) {
        return yndDhyjRuleDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return yndDhyjRuleDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return yndDhyjRuleDetailMapper.deleteByIds(ids);
    }
}
