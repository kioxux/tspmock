package cn.com.yusys.yusp.web.server.xdqt0006;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdqt0006.resp.Xdqt0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdqt0006.Xdqt0006Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:当天跑P状态查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0006:当天跑P状态查询")
@RestController
@RequestMapping("/api/pspqt4bsp")
public class PspXdqt0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(PspXdqt0006Resource.class);

    @Autowired
    private Xdqt0006Service xdqt0006Service;
    /**
     * 交易码：xdqt0006
     * 交易描述：当天跑P状态查询
     *
     * @return
     * @throws Exception
     */
    @ApiOperation("当天跑P状态查询")
    @PostMapping("/xdqt0006")
    protected @ResponseBody
    ResultDto<Xdqt0006DataRespDto> xdqt0006() throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value);
        Xdqt0006DataRespDto xdqt0006DataRespDto = new Xdqt0006DataRespDto();// 响应Dto:当天跑P状态查询
        ResultDto<Xdqt0006DataRespDto> xdqt0006DataResultDto = new ResultDto<>();
        try {
            // 从xdqt0006DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value);
            xdqt0006DataRespDto = xdqt0006Service.getProgressStatus();
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value);
            // 封装xdqt0006DataResultDto中正确的返回码和返回信息
            xdqt0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdqt0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value, e.getMessage());
            // 封装xdqt0006DataResultDto中异常返回码和返回信息
            xdqt0006DataResultDto.setCode(EcbEnum.ECB019999.key);
            xdqt0006DataResultDto.setMessage(EcbEnum.ECB019999.value);
        }
        // 封装xdqt0006DataRespDto到xdqt0006DataResultDto中
        xdqt0006DataResultDto.setData(xdqt0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value, JSON.toJSONString(xdqt0006DataResultDto));
        return xdqt0006DataResultDto;
    }
}
