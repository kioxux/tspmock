/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspGuarntrCheck
 * @类描述: psp_guarntr_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_guarntr_check")
public class PspGuarntrCheck extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 目前保证人经营是否正常 **/
	@Column(name = "IS_OPER_NORMAL", unique = false, nullable = true, length = 5)
	private String isOperNormal;
	
	/** 保证人经营异常说明 **/
	@Column(name = "OPER_EXPL", unique = false, nullable = true, length = 65535)
	private String operExpl;
	
	/** 保证人担保能力 **/
	@Column(name = "GUAR_ABI", unique = false, nullable = true, length = 5)
	private String guarAbi;
	
	/** 保证人担保能力说明 **/
	@Column(name = "GUAR_ABI_EXPL", unique = false, nullable = true, length = 65535)
	private String guarAbiExpl;
	
	/** 保证人担保意愿 **/
	@Column(name = "GUAR_WISH", unique = false, nullable = true, length = 5)
	private String guarWish;
	
	/** 保证人担保意愿说明 **/
	@Column(name = "GUAR_WISH_EXPL", unique = false, nullable = true, length = 65535)
	private String guarWishExpl;
	
	/** 是否有其他重要风险事项 **/
	@Column(name = "IS_OTHER_RISK_EVENT", unique = false, nullable = true, length = 5)
	private String isOtherRiskEvent;
	
	/** 其他风险说明 **/
	@Column(name = "RISK_EVENT_EXPL", unique = false, nullable = true, length = 65535)
	private String riskEventExpl;
	
	/** 担保人经营（工作）是否正常 **/
	@Column(name = "IS_GUAR_OPER_NORMAL", unique = false, nullable = true, length = 5)
	private String isGuarOperNormal;
	
	/** 担保人经营异常说明 **/
	@Column(name = "UNNORMAL_DESC", unique = false, nullable = true, length = 65535)
	private String unnormalDesc;
	
	/** 担保人担保能力是否下降 **/
	@Column(name = "IS_CHANGE_ABI", unique = false, nullable = true, length = 5)
	private String isChangeAbi;
	
	/** 担保人担保能力下降说明 **/
	@Column(name = "CHANGE_ABI_REMARK", unique = false, nullable = true, length = 65535)
	private String changeAbiRemark;
	
	/** 是否超过担保人实际担保能力 **/
	@Column(name = "IS_ADD_ABI", unique = false, nullable = true, length = 5)
	private String isAddAbi;
	
	/** 超过担保能力说明 **/
	@Column(name = "ADD_ABI_REMARK", unique = false, nullable = true, length = 65535)
	private String addAbiRemark;
	
	/** 担保人及其家庭是否发生意外 **/
	@Column(name = "IS_ACCIDENT", unique = false, nullable = true, length = 5)
	private String isAccident;
	
	/** 意外说明 **/
	@Column(name = "ACCIDENT_REMARK", unique = false, nullable = true, length = 65535)
	private String accidentRemark;
	
	/** 担保人是否面临重大诉讼 **/
	@Column(name = "IS_INVOLVE_LAWSUIT", unique = false, nullable = true, length = 5)
	private String isInvolveLawsuit;
	
	/** 重大诉讼说明 **/
	@Column(name = "INVOLVE_LAWSUIT_REMARK", unique = false, nullable = true, length = 65535)
	private String involveLawsuitRemark;
	
	/** 是否具备担保资格和能力 **/
	@Column(name = "IS_OWN_GUAR_ABI", unique = false, nullable = true, length = 5)
	private String isOwnGuarAbi;
	
	/** 担保资格说明 **/
	@Column(name = "OWN_GUAR_ABI_REMARK", unique = false, nullable = true, length = 65535)
	private String ownGuarAbiRemark;
	
	/** 对担保人的总体评价 **/
	@Column(name = "TOTL_EVLU", unique = false, nullable = true, length = 5)
	private String totlEvlu;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isOperNormal
	 */
	public void setIsOperNormal(String isOperNormal) {
		this.isOperNormal = isOperNormal;
	}
	
    /**
     * @return isOperNormal
     */
	public String getIsOperNormal() {
		return this.isOperNormal;
	}
	
	/**
	 * @param operExpl
	 */
	public void setOperExpl(String operExpl) {
		this.operExpl = operExpl;
	}
	
    /**
     * @return operExpl
     */
	public String getOperExpl() {
		return this.operExpl;
	}
	
	/**
	 * @param guarAbi
	 */
	public void setGuarAbi(String guarAbi) {
		this.guarAbi = guarAbi;
	}
	
    /**
     * @return guarAbi
     */
	public String getGuarAbi() {
		return this.guarAbi;
	}
	
	/**
	 * @param guarAbiExpl
	 */
	public void setGuarAbiExpl(String guarAbiExpl) {
		this.guarAbiExpl = guarAbiExpl;
	}
	
    /**
     * @return guarAbiExpl
     */
	public String getGuarAbiExpl() {
		return this.guarAbiExpl;
	}
	
	/**
	 * @param guarWish
	 */
	public void setGuarWish(String guarWish) {
		this.guarWish = guarWish;
	}
	
    /**
     * @return guarWish
     */
	public String getGuarWish() {
		return this.guarWish;
	}
	
	/**
	 * @param guarWishExpl
	 */
	public void setGuarWishExpl(String guarWishExpl) {
		this.guarWishExpl = guarWishExpl;
	}
	
    /**
     * @return guarWishExpl
     */
	public String getGuarWishExpl() {
		return this.guarWishExpl;
	}
	
	/**
	 * @param isOtherRiskEvent
	 */
	public void setIsOtherRiskEvent(String isOtherRiskEvent) {
		this.isOtherRiskEvent = isOtherRiskEvent;
	}
	
    /**
     * @return isOtherRiskEvent
     */
	public String getIsOtherRiskEvent() {
		return this.isOtherRiskEvent;
	}
	
	/**
	 * @param riskEventExpl
	 */
	public void setRiskEventExpl(String riskEventExpl) {
		this.riskEventExpl = riskEventExpl;
	}
	
    /**
     * @return riskEventExpl
     */
	public String getRiskEventExpl() {
		return this.riskEventExpl;
	}
	
	/**
	 * @param isGuarOperNormal
	 */
	public void setIsGuarOperNormal(String isGuarOperNormal) {
		this.isGuarOperNormal = isGuarOperNormal;
	}
	
    /**
     * @return isGuarOperNormal
     */
	public String getIsGuarOperNormal() {
		return this.isGuarOperNormal;
	}
	
	/**
	 * @param unnormalDesc
	 */
	public void setUnnormalDesc(String unnormalDesc) {
		this.unnormalDesc = unnormalDesc;
	}
	
    /**
     * @return unnormalDesc
     */
	public String getUnnormalDesc() {
		return this.unnormalDesc;
	}
	
	/**
	 * @param isChangeAbi
	 */
	public void setIsChangeAbi(String isChangeAbi) {
		this.isChangeAbi = isChangeAbi;
	}
	
    /**
     * @return isChangeAbi
     */
	public String getIsChangeAbi() {
		return this.isChangeAbi;
	}
	
	/**
	 * @param changeAbiRemark
	 */
	public void setChangeAbiRemark(String changeAbiRemark) {
		this.changeAbiRemark = changeAbiRemark;
	}
	
    /**
     * @return changeAbiRemark
     */
	public String getChangeAbiRemark() {
		return this.changeAbiRemark;
	}
	
	/**
	 * @param isAddAbi
	 */
	public void setIsAddAbi(String isAddAbi) {
		this.isAddAbi = isAddAbi;
	}
	
    /**
     * @return isAddAbi
     */
	public String getIsAddAbi() {
		return this.isAddAbi;
	}
	
	/**
	 * @param addAbiRemark
	 */
	public void setAddAbiRemark(String addAbiRemark) {
		this.addAbiRemark = addAbiRemark;
	}
	
    /**
     * @return addAbiRemark
     */
	public String getAddAbiRemark() {
		return this.addAbiRemark;
	}
	
	/**
	 * @param isAccident
	 */
	public void setIsAccident(String isAccident) {
		this.isAccident = isAccident;
	}
	
    /**
     * @return isAccident
     */
	public String getIsAccident() {
		return this.isAccident;
	}
	
	/**
	 * @param accidentRemark
	 */
	public void setAccidentRemark(String accidentRemark) {
		this.accidentRemark = accidentRemark;
	}
	
    /**
     * @return accidentRemark
     */
	public String getAccidentRemark() {
		return this.accidentRemark;
	}
	
	/**
	 * @param isInvolveLawsuit
	 */
	public void setIsInvolveLawsuit(String isInvolveLawsuit) {
		this.isInvolveLawsuit = isInvolveLawsuit;
	}
	
    /**
     * @return isInvolveLawsuit
     */
	public String getIsInvolveLawsuit() {
		return this.isInvolveLawsuit;
	}
	
	/**
	 * @param involveLawsuitRemark
	 */
	public void setInvolveLawsuitRemark(String involveLawsuitRemark) {
		this.involveLawsuitRemark = involveLawsuitRemark;
	}
	
    /**
     * @return involveLawsuitRemark
     */
	public String getInvolveLawsuitRemark() {
		return this.involveLawsuitRemark;
	}
	
	/**
	 * @param isOwnGuarAbi
	 */
	public void setIsOwnGuarAbi(String isOwnGuarAbi) {
		this.isOwnGuarAbi = isOwnGuarAbi;
	}
	
    /**
     * @return isOwnGuarAbi
     */
	public String getIsOwnGuarAbi() {
		return this.isOwnGuarAbi;
	}
	
	/**
	 * @param ownGuarAbiRemark
	 */
	public void setOwnGuarAbiRemark(String ownGuarAbiRemark) {
		this.ownGuarAbiRemark = ownGuarAbiRemark;
	}
	
    /**
     * @return ownGuarAbiRemark
     */
	public String getOwnGuarAbiRemark() {
		return this.ownGuarAbiRemark;
	}
	
	/**
	 * @param totlEvlu
	 */
	public void setTotlEvlu(String totlEvlu) {
		this.totlEvlu = totlEvlu;
	}
	
    /**
     * @return totlEvlu
     */
	public String getTotlEvlu() {
		return this.totlEvlu;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}