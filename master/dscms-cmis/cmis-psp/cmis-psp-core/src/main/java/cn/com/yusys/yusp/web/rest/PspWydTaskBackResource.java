/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.PspWydBackTjListDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspWydTaskBack;
import cn.com.yusys.yusp.service.PspWydTaskBackService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWydTaskBackResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-30 21:46:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pspwydtaskback")
public class PspWydTaskBackResource {
    @Autowired
    private PspWydTaskBackService pspWydTaskBackService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspWydTaskBack>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspWydTaskBack> list = pspWydTaskBackService.selectAll(queryModel);
        return new ResultDto<List<PspWydTaskBack>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<PspWydTaskBack>> index(@RequestBody QueryModel queryModel) {
        List<PspWydTaskBack> list = pspWydTaskBackService.selectByModel(queryModel);
        return new ResultDto<List<PspWydTaskBack>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspWydTaskBack> show(@PathVariable("pkId") String pkId) {
        PspWydTaskBack pspWydTaskBack = pspWydTaskBackService.selectByPrimaryKey(pkId);
        return new ResultDto<PspWydTaskBack>(pspWydTaskBack);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<PspWydTaskBack> create(@RequestBody PspWydTaskBack pspWydTaskBack) throws URISyntaxException {
        pspWydTaskBackService.insert(pspWydTaskBack);
        return new ResultDto<PspWydTaskBack>(pspWydTaskBack);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspWydTaskBack pspWydTaskBack) throws URISyntaxException {
        int result = pspWydTaskBackService.updateSelective(pspWydTaskBack);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspWydTaskBackService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspWydTaskBackService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 微业贷贷后退回任务统计查询
     * @函数名称:pspWydBackTjList
     * @函数描述:微业贷贷后退回任务统计查询
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/pspWydBackTjList")
    protected ResultDto<List<PspWydBackTjListDto>> pspWydBackTjList(){
        QueryModel queryModel = new QueryModel();
        List<PspWydBackTjListDto> list = pspWydTaskBackService.pspWydBackTjList(queryModel);
        return new ResultDto<List<PspWydBackTjListDto>>(list);
    }
}
