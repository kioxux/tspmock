/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskGuarContAnaly
 * @类描述: risk_guar_cont_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_guar_cont_analy")
public class RiskGuarContAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 担保合同编号 **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
	private String guarContNo;
	
	/** 担保合同类型 **/
	@Column(name = "GUAR_CONT_TYPE", unique = false, nullable = true, length = 5)
	private String guarContType;
	
	/** 是否授信项下 **/
	@Column(name = "IS_UNDER_LMT", unique = false, nullable = true, length = 5)
	private String isUnderLmt;
	
	/** 授信额度编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 担保合同金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 借款人名称 **/
	@Column(name = "BORROWER_NAME", unique = false, nullable = true, length = 80)
	private String borrowerName;
	
	/** 担保合同状态 **/
	@Column(name = "GUAR_CONT_STATE", unique = false, nullable = true, length = 5)
	private String guarContState;
	
	/** 担保合同有效性 **/
	@Column(name = "GUAR_CONT_VALID_FLAG", unique = false, nullable = true, length = 5)
	private String guarContValidFlag;
	
	/** 担保合同限制性条款 **/
	@Column(name = "GUAR_CONT_LIMIT_FLAG", unique = false, nullable = true, length = 5)
	private String guarContLimitFlag;
	
	/** 担保合同代偿能力说明 **/
	@Column(name = "GUAR_REPAY_ABI_REMARK", unique = false, nullable = true, length = 65535)
	private String guarRepayAbiRemark;
	
	/** 分析状态 **/
	@Column(name = "ANALY_STATUS", unique = false, nullable = true, length = 5)
	private String analyStatus;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}
	
    /**
     * @return guarContNo
     */
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param guarContType
	 */
	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType;
	}
	
    /**
     * @return guarContType
     */
	public String getGuarContType() {
		return this.guarContType;
	}
	
	/**
	 * @param isUnderLmt
	 */
	public void setIsUnderLmt(String isUnderLmt) {
		this.isUnderLmt = isUnderLmt;
	}
	
    /**
     * @return isUnderLmt
     */
	public String getIsUnderLmt() {
		return this.isUnderLmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param borrowerName
	 */
	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}
	
    /**
     * @return borrowerName
     */
	public String getBorrowerName() {
		return this.borrowerName;
	}
	
	/**
	 * @param guarContState
	 */
	public void setGuarContState(String guarContState) {
		this.guarContState = guarContState;
	}
	
    /**
     * @return guarContState
     */
	public String getGuarContState() {
		return this.guarContState;
	}
	
	/**
	 * @param guarContValidFlag
	 */
	public void setGuarContValidFlag(String guarContValidFlag) {
		this.guarContValidFlag = guarContValidFlag;
	}
	
    /**
     * @return guarContValidFlag
     */
	public String getGuarContValidFlag() {
		return this.guarContValidFlag;
	}
	
	/**
	 * @param guarContLimitFlag
	 */
	public void setGuarContLimitFlag(String guarContLimitFlag) {
		this.guarContLimitFlag = guarContLimitFlag;
	}
	
    /**
     * @return guarContLimitFlag
     */
	public String getGuarContLimitFlag() {
		return this.guarContLimitFlag;
	}
	
	/**
	 * @param guarRepayAbiRemark
	 */
	public void setGuarRepayAbiRemark(String guarRepayAbiRemark) {
		this.guarRepayAbiRemark = guarRepayAbiRemark;
	}
	
    /**
     * @return guarRepayAbiRemark
     */
	public String getGuarRepayAbiRemark() {
		return this.guarRepayAbiRemark;
	}
	
	/**
	 * @param analyStatus
	 */
	public void setAnalyStatus(String analyStatus) {
		this.analyStatus = analyStatus;
	}
	
    /**
     * @return analyStatus
     */
	public String getAnalyStatus() {
		return this.analyStatus;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}