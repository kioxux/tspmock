/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PubSysInfo
 * @类描述: PUB_SYS_INFO数据实体类
 * @功能描述: 
 * @创建人: sunbin
 * @创建时间: 2021-06-05 22:45:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "PUB_SYS_INFO")
public class PubSysInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 系统编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SYS_ID")
	private String sysId;
	
	/** 系统名称 **/
	@Column(name = "SYS_NAME", unique = false, nullable = false, length = 100)
	private String sysName;
	
	/** 未知 **/
	@Column(name = "HEADOFFICE", unique = false, nullable = false, length = 20)
	private String headoffice;
	
	/** 进程 **/
	@Column(name = "PROGRESS", unique = false, nullable = false, length = 2)
	private String progress;
	
	/** 当前日期 **/
	@Column(name = "OPENDAY", unique = false, nullable = false, length = 10)
	private String openday;
	
	/** 昨日日期 **/
	@Column(name = "LAST_OPENDAY", unique = false, nullable = false, length = 10)
	private String lastOpenday;
	
	/** 频率 **/
	@Column(name = "EFFECTIVEDAYS", unique = false, nullable = true, length = 38)
	private java.math.BigDecimal effectivedays;
	
	/** 切换生产日期 **/
	@Column(name = "TURNDAY", unique = false, nullable = true, length = 10)
	private String turnday;
	
	/** 废弃 **/
	@Column(name = "SERVICEPASS", unique = false, nullable = true, length = 10)
	private String servicepass;
	
	/** 废弃 **/
	@Column(name = "TURNDAYHS", unique = false, nullable = true, length = 10)
	private String turndayhs;
	
	
	/**
	 * @param sysId
	 */
	public void setSysId(String sysId) {
		this.sysId = sysId;
	}
	
    /**
     * @return sysId
     */
	public String getSysId() {
		return this.sysId;
	}
	
	/**
	 * @param sysName
	 */
	public void setSysName(String sysName) {
		this.sysName = sysName;
	}
	
    /**
     * @return sysName
     */
	public String getSysName() {
		return this.sysName;
	}
	
	/**
	 * @param headoffice
	 */
	public void setHeadoffice(String headoffice) {
		this.headoffice = headoffice;
	}
	
    /**
     * @return headoffice
     */
	public String getHeadoffice() {
		return this.headoffice;
	}
	
	/**
	 * @param progress
	 */
	public void setProgress(String progress) {
		this.progress = progress;
	}
	
    /**
     * @return progress
     */
	public String getProgress() {
		return this.progress;
	}
	
	/**
	 * @param openday
	 */
	public void setOpenday(String openday) {
		this.openday = openday;
	}
	
    /**
     * @return openday
     */
	public String getOpenday() {
		return this.openday;
	}
	
	/**
	 * @param lastOpenday
	 */
	public void setLastOpenday(String lastOpenday) {
		this.lastOpenday = lastOpenday;
	}
	
    /**
     * @return lastOpenday
     */
	public String getLastOpenday() {
		return this.lastOpenday;
	}
	
	/**
	 * @param effectivedays
	 */
	public void setEffectivedays(java.math.BigDecimal effectivedays) {
		this.effectivedays = effectivedays;
	}
	
    /**
     * @return effectivedays
     */
	public java.math.BigDecimal getEffectivedays() {
		return this.effectivedays;
	}
	
	/**
	 * @param turnday
	 */
	public void setTurnday(String turnday) {
		this.turnday = turnday;
	}
	
    /**
     * @return turnday
     */
	public String getTurnday() {
		return this.turnday;
	}
	
	/**
	 * @param servicepass
	 */
	public void setServicepass(String servicepass) {
		this.servicepass = servicepass;
	}
	
    /**
     * @return servicepass
     */
	public String getServicepass() {
		return this.servicepass;
	}
	
	/**
	 * @param turndayhs
	 */
	public void setTurndayhs(String turndayhs) {
		this.turndayhs = turndayhs;
	}
	
    /**
     * @return turndayhs
     */
	public String getTurndayhs() {
		return this.turndayhs;
	}


}