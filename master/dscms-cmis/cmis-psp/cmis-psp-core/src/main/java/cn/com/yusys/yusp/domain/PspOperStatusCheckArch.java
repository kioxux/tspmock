/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckArch
 * @类描述: psp_oper_status_check_arch数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_oper_status_check_arch")
public class PspOperStatusCheckArch extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 工程款是否及时回笼 **/
	@Column(name = "IS_CAP_BACK", unique = false, nullable = true, length = 5)
	private String isCapBack;
	
	/** 工程款回笼说明 **/
	@Column(name = "BACK_EXPL", unique = false, nullable = true, length = 65535)
	private String backExpl;
	
	/** 企业垫资是否严重 **/
	@Column(name = "IS_MAT_ENDOWMENT", unique = false, nullable = true, length = 5)
	private String isMatEndowment;
	
	/** 企业垫资说明 **/
	@Column(name = "MAT_ENDOWMENT_EXPL", unique = false, nullable = true, length = 65535)
	private String matEndowmentExpl;
	
	/** 企业是否存在重大质量问题的工程 **/
	@Column(name = "QUALITY_PROBLEM", unique = false, nullable = true, length = 5)
	private String qualityProblem;
	
	/** 重大质量问题说明 **/
	@Column(name = "PROBLEM_EXPL", unique = false, nullable = true, length = 65535)
	private String problemExpl;
	
	/** 借款人当前承接的工程量与授信时相比是否下降 **/
	@Column(name = "IS_DOWN", unique = false, nullable = true, length = 5)
	private String isDown;
	
	/** 工程量下降说明 **/
	@Column(name = "DOWN_EXPL", unique = false, nullable = true, length = 65535)
	private String downExpl;
	
	/** 主要原材料价格波动是否较大 **/
	@Column(name = "PRICE_CHANGE", unique = false, nullable = true, length = 5)
	private String priceChange;
	
	/** 原材料价格波动说明 **/
	@Column(name = "CHANGE_EXPL", unique = false, nullable = true, length = 65535)
	private String changeExpl;
	
	/** 市政工程占比 **/
	@Column(name = "CITY_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cityPerc;
	
	/** 房地产工程占比 **/
	@Column(name = "HOUSE_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal housePerc;
	
	/** 其他工程占比 **/
	@Column(name = "OTHER_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherPerc;
	
	/** 上期工程主要地点 **/
	@Column(name = "PRE_RST", unique = false, nullable = true, length = 80)
	private String preRst;
	
	/** 本期工程主要地点 **/
	@Column(name = "CURT_RST", unique = false, nullable = true, length = 80)
	private String curtRst;
	
	/** 工程主要地点变化说明 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 65535)
	private String remark;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isCapBack
	 */
	public void setIsCapBack(String isCapBack) {
		this.isCapBack = isCapBack;
	}
	
    /**
     * @return isCapBack
     */
	public String getIsCapBack() {
		return this.isCapBack;
	}
	
	/**
	 * @param backExpl
	 */
	public void setBackExpl(String backExpl) {
		this.backExpl = backExpl;
	}
	
    /**
     * @return backExpl
     */
	public String getBackExpl() {
		return this.backExpl;
	}
	
	/**
	 * @param isMatEndowment
	 */
	public void setIsMatEndowment(String isMatEndowment) {
		this.isMatEndowment = isMatEndowment;
	}
	
    /**
     * @return isMatEndowment
     */
	public String getIsMatEndowment() {
		return this.isMatEndowment;
	}
	
	/**
	 * @param matEndowmentExpl
	 */
	public void setMatEndowmentExpl(String matEndowmentExpl) {
		this.matEndowmentExpl = matEndowmentExpl;
	}
	
    /**
     * @return matEndowmentExpl
     */
	public String getMatEndowmentExpl() {
		return this.matEndowmentExpl;
	}
	
	/**
	 * @param qualityProblem
	 */
	public void setQualityProblem(String qualityProblem) {
		this.qualityProblem = qualityProblem;
	}
	
    /**
     * @return qualityProblem
     */
	public String getQualityProblem() {
		return this.qualityProblem;
	}
	
	/**
	 * @param problemExpl
	 */
	public void setProblemExpl(String problemExpl) {
		this.problemExpl = problemExpl;
	}
	
    /**
     * @return problemExpl
     */
	public String getProblemExpl() {
		return this.problemExpl;
	}
	
	/**
	 * @param isDown
	 */
	public void setIsDown(String isDown) {
		this.isDown = isDown;
	}
	
    /**
     * @return isDown
     */
	public String getIsDown() {
		return this.isDown;
	}
	
	/**
	 * @param downExpl
	 */
	public void setDownExpl(String downExpl) {
		this.downExpl = downExpl;
	}
	
    /**
     * @return downExpl
     */
	public String getDownExpl() {
		return this.downExpl;
	}
	
	/**
	 * @param priceChange
	 */
	public void setPriceChange(String priceChange) {
		this.priceChange = priceChange;
	}
	
    /**
     * @return priceChange
     */
	public String getPriceChange() {
		return this.priceChange;
	}
	
	/**
	 * @param changeExpl
	 */
	public void setChangeExpl(String changeExpl) {
		this.changeExpl = changeExpl;
	}
	
    /**
     * @return changeExpl
     */
	public String getChangeExpl() {
		return this.changeExpl;
	}
	
	/**
	 * @param cityPerc
	 */
	public void setCityPerc(java.math.BigDecimal cityPerc) {
		this.cityPerc = cityPerc;
	}
	
    /**
     * @return cityPerc
     */
	public java.math.BigDecimal getCityPerc() {
		return this.cityPerc;
	}
	
	/**
	 * @param housePerc
	 */
	public void setHousePerc(java.math.BigDecimal housePerc) {
		this.housePerc = housePerc;
	}
	
    /**
     * @return housePerc
     */
	public java.math.BigDecimal getHousePerc() {
		return this.housePerc;
	}
	
	/**
	 * @param otherPerc
	 */
	public void setOtherPerc(java.math.BigDecimal otherPerc) {
		this.otherPerc = otherPerc;
	}
	
    /**
     * @return otherPerc
     */
	public java.math.BigDecimal getOtherPerc() {
		return this.otherPerc;
	}
	
	/**
	 * @param preRst
	 */
	public void setPreRst(String preRst) {
		this.preRst = preRst;
	}
	
    /**
     * @return preRst
     */
	public String getPreRst() {
		return this.preRst;
	}
	
	/**
	 * @param curtRst
	 */
	public void setCurtRst(String curtRst) {
		this.curtRst = curtRst;
	}
	
    /**
     * @return curtRst
     */
	public String getCurtRst() {
		return this.curtRst;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}