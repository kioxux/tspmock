/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspDealSitu
 * @类描述: psp_deal_situ数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_deal_situ")
public class PspDealSitu extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 出险成因 **/
	@Column(name = "RISK_REASON", unique = false, nullable = true, length = 80)
	private String riskReason;
	
	/** 是否寄送催收通知书 **/
	@Column(name = "IS_COLLECT", unique = false, nullable = true, length = 5)
	private String isCollect;
	
	/** 是否有回执 **/
	@Column(name = "IS_RECEIPT", unique = false, nullable = true, length = 5)
	private String isReceipt;
	
	/** 当前处置措施 **/
	@Column(name = "PRESENT_ACTION_DESC", unique = false, nullable = true, length = 65535)
	private String presentActionDesc;
	
	/** 下一步处置措施或建议 **/
	@Column(name = "NEXT_STEP_ACTION", unique = false, nullable = true, length = 65535)
	private String nextStepAction;

	/** 逾期原因 **/
	@Column(name = "OVERDUE_RESN", unique = false, nullable = true, length = 65535)
	private String overdueResn;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param riskReason
	 */
	public void setRiskReason(String riskReason) {
		this.riskReason = riskReason;
	}
	
    /**
     * @return riskReason
     */
	public String getRiskReason() {
		return this.riskReason;
	}
	
	/**
	 * @param isCollect
	 */
	public void setIsCollect(String isCollect) {
		this.isCollect = isCollect;
	}
	
    /**
     * @return isCollect
     */
	public String getIsCollect() {
		return this.isCollect;
	}
	
	/**
	 * @param isReceipt
	 */
	public void setIsReceipt(String isReceipt) {
		this.isReceipt = isReceipt;
	}
	
    /**
     * @return isReceipt
     */
	public String getIsReceipt() {
		return this.isReceipt;
	}
	
	/**
	 * @param presentActionDesc
	 */
	public void setPresentActionDesc(String presentActionDesc) {
		this.presentActionDesc = presentActionDesc;
	}
	
    /**
     * @return presentActionDesc
     */
	public String getPresentActionDesc() {
		return this.presentActionDesc;
	}
	
	/**
	 * @param nextStepAction
	 */
	public void setNextStepAction(String nextStepAction) {
		this.nextStepAction = nextStepAction;
	}
	
    /**
     * @return nextStepAction
     */
	public String getNextStepAction() {
		return this.nextStepAction;
	}

	/**
	 * @param overdueResn
	 */
	public void setOverdueResn(String overdueResn) {
		this.overdueResn = overdueResn;
	}

	/**
	 * @return overdueResn
	 */
	public String getOverdueResn() {
		return this.overdueResn;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}