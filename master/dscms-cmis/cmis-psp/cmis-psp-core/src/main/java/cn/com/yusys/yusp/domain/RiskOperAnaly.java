/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskOperAnaly
 * @类描述: risk_oper_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_oper_analy")
public class RiskOperAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 经营情况 **/
	@Column(name = "CORP_OPER_SITU", unique = false, nullable = true, length = 100)
	private String corpOperSitu;
	
	/** 经营情况说明 **/
	@Column(name = "OPER_SITU_REMARK", unique = false, nullable = true, length = 65535)
	private String operSituRemark;
	
	/** 预测以后1年内经营趋势 **/
	@Column(name = "N1Y_OPER_TREND", unique = false, nullable = true, length = 5)
	private String n1yOperTrend;
	
	/** 生产经营情况 **/
	@Column(name = "INDIV_OPER_SITU", unique = false, nullable = true, length = 5)
	private String indivOperSitu;
	
	/** 销售收入变化趋势 **/
	@Column(name = "INCOME_CHANGE", unique = false, nullable = true, length = 5)
	private String incomeChange;
	
	/** 利润变动趋势 **/
	@Column(name = "PROFIT_CHANGE", unique = false, nullable = true, length = 5)
	private String profitChange;
	
	/** 现金流量变动趋势 **/
	@Column(name = "CASH_CHANGE", unique = false, nullable = true, length = 5)
	private String cashChange;
	
	/** 第一还款来源是否充足 **/
	@Column(name = "IS_INCOME_SUFFICE", unique = false, nullable = true, length = 5)
	private String isIncomeSuffice;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param corpOperSitu
	 */
	public void setCorpOperSitu(String corpOperSitu) {
		this.corpOperSitu = corpOperSitu;
	}
	
    /**
     * @return corpOperSitu
     */
	public String getCorpOperSitu() {
		return this.corpOperSitu;
	}
	
	/**
	 * @param operSituRemark
	 */
	public void setOperSituRemark(String operSituRemark) {
		this.operSituRemark = operSituRemark;
	}
	
    /**
     * @return operSituRemark
     */
	public String getOperSituRemark() {
		return this.operSituRemark;
	}
	
	/**
	 * @param n1yOperTrend
	 */
	public void setN1yOperTrend(String n1yOperTrend) {
		this.n1yOperTrend = n1yOperTrend;
	}
	
    /**
     * @return n1yOperTrend
     */
	public String getN1yOperTrend() {
		return this.n1yOperTrend;
	}
	
	/**
	 * @param indivOperSitu
	 */
	public void setIndivOperSitu(String indivOperSitu) {
		this.indivOperSitu = indivOperSitu;
	}
	
    /**
     * @return indivOperSitu
     */
	public String getIndivOperSitu() {
		return this.indivOperSitu;
	}
	
	/**
	 * @param incomeChange
	 */
	public void setIncomeChange(String incomeChange) {
		this.incomeChange = incomeChange;
	}
	
    /**
     * @return incomeChange
     */
	public String getIncomeChange() {
		return this.incomeChange;
	}
	
	/**
	 * @param profitChange
	 */
	public void setProfitChange(String profitChange) {
		this.profitChange = profitChange;
	}
	
    /**
     * @return profitChange
     */
	public String getProfitChange() {
		return this.profitChange;
	}
	
	/**
	 * @param cashChange
	 */
	public void setCashChange(String cashChange) {
		this.cashChange = cashChange;
	}
	
    /**
     * @return cashChange
     */
	public String getCashChange() {
		return this.cashChange;
	}
	
	/**
	 * @param isIncomeSuffice
	 */
	public void setIsIncomeSuffice(String isIncomeSuffice) {
		this.isIncomeSuffice = isIncomeSuffice;
	}
	
    /**
     * @return isIncomeSuffice
     */
	public String getIsIncomeSuffice() {
		return this.isIncomeSuffice;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}