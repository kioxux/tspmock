/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.PspCusBaseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PspOperStatusCheckArch;
import cn.com.yusys.yusp.repository.mapper.PspOperStatusCheckArchMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckArchService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspOperStatusCheckArchService {

    @Autowired
    private PspOperStatusCheckArchMapper pspOperStatusCheckArchMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PspOperStatusCheckArch selectByPrimaryKey(String pkId) {
        return pspOperStatusCheckArchMapper.selectByPrimaryKey(pkId);
    }
	/**
     * @方法名称: queryPspOperStatusCheckArch
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspOperStatusCheckArch queryPspOperStatusCheckArch(PspOperStatusCheckArch pspOperStatusCheckArch) {
        return pspOperStatusCheckArchMapper.queryPspOperStatusCheckArch(pspOperStatusCheckArch);
    }
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PspOperStatusCheckArch> selectAll(QueryModel model) {
        List<PspOperStatusCheckArch> records = (List<PspOperStatusCheckArch>) pspOperStatusCheckArchMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PspOperStatusCheckArch> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PspOperStatusCheckArch> list = pspOperStatusCheckArchMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PspOperStatusCheckArch record) {
        return pspOperStatusCheckArchMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PspOperStatusCheckArch record) {
        return pspOperStatusCheckArchMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PspOperStatusCheckArch record) {
        int count = 0;
        PspOperStatusCheckArch pspOperStatusCheckArch = pspOperStatusCheckArchMapper.selectByPrimaryKey(record.getPkId());
        if (pspOperStatusCheckArch != null){
            pspOperStatusCheckArchMapper.updateByPrimaryKeySelective(record);
        }else {
            pspOperStatusCheckArchMapper.insert(record);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PspOperStatusCheckArch record) {
        return pspOperStatusCheckArchMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pspOperStatusCheckArchMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pspOperStatusCheckArchMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByTaskNo
     * @方法描述: 根据任务编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByTaskNo(String taskNo) {
        return pspOperStatusCheckArchMapper.deleteByTaskNo(taskNo);
    }
}
