/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPldimnCheck
 * @类描述: psp_pldimn_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_pldimn_check")
public class PspPldimnCheck extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 是否对抵（质）押物进行核实并拍照确认 **/
	@Column(name = "IS_PHOTOGRAPH", unique = false, nullable = true, length = 5)
	private String isPhotograph;
	
	/** 未核实说明 **/
	@Column(name = "NO_PHOTO_EXPL", unique = false, nullable = true, length = 65535)
	private String noPhotoExpl;
	
	/** 抵（质）押人的担保意愿情况 **/
	@Column(name = "GUAR_WISH_CASE", unique = false, nullable = true, length = 5)
	private String guarWishCase;
	
	/** 抵（质）押人的担保意愿说明 **/
	@Column(name = "WISH_CASE_EXPL", unique = false, nullable = true, length = 65535)
	private String wishCaseExpl;
	
	/** 抵/质押物是否被有关机关查封、冻结、再次抵押 **/
	@Column(name = "IS_CLOSE", unique = false, nullable = true, length = 5)
	private String isClose;
	
	/** 抵质押物查封说明 **/
	@Column(name = "CLOSE_EXPL", unique = false, nullable = true, length = 65535)
	private String closeExpl;
	
	/** 是否书面承诺放弃优先购买权及优先租赁权 **/
	@Column(name = "IS_PRI_RENT", unique = false, nullable = true, length = 5)
	private String isPriRent;
	
	/** 放弃优先权说明 **/
	@Column(name = "RENT_EXPL", unique = false, nullable = true, length = 65535)
	private String rentExpl;
	
	/** 是否有其他重要风险事项 **/
	@Column(name = "IS_OTHER_RISK_EVENT", unique = false, nullable = true, length = 5)
	private String isOtherRiskEvent;
	
	/** 其他风险说明 **/
	@Column(name = "RISK_EVENT_EXPL", unique = false, nullable = true, length = 65535)
	private String riskEventExpl;
	
	/** 抵/质押物是否损毁或大幅贬值 **/
	@Column(name = "IS_RUIN", unique = false, nullable = true, length = 5)
	private String isRuin;
	
	/** 抵/质押物贬值说明 **/
	@Column(name = "RUIN_EXPL", unique = false, nullable = true, length = 65535)
	private String ruinExpl;
	
	/** 抵/质押物变现能力与审批贷款时有无较大差异 **/
	@Column(name = "IS_CHANGE_GUAR", unique = false, nullable = true, length = 5)
	private String isChangeGuar;
	
	/** 变现能力差异说明 **/
	@Column(name = "CHANGE_GUAR_REMARK", unique = false, nullable = true, length = 65535)
	private String changeGuarRemark;
	
	/** 该抵/质押物是否能迅速变现并足额抵偿相关债务 **/
	@Column(name = "IS_TO_ENTER_GUAR", unique = false, nullable = true, length = 5)
	private String isToEnterGuar;
	
	/** 快速变现说明 **/
	@Column(name = "TO_ENTER_GUAR_REMARK", unique = false, nullable = true, length = 65535)
	private String toEnterGuarRemark;
	
	/** 对抵/质押物的总体评价 **/
	@Column(name = "TOTL_EVLU", unique = false, nullable = true, length = 5)
	private String totlEvlu;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isPhotograph
	 */
	public void setIsPhotograph(String isPhotograph) {
		this.isPhotograph = isPhotograph;
	}
	
    /**
     * @return isPhotograph
     */
	public String getIsPhotograph() {
		return this.isPhotograph;
	}
	
	/**
	 * @param noPhotoExpl
	 */
	public void setNoPhotoExpl(String noPhotoExpl) {
		this.noPhotoExpl = noPhotoExpl;
	}
	
    /**
     * @return noPhotoExpl
     */
	public String getNoPhotoExpl() {
		return this.noPhotoExpl;
	}
	
	/**
	 * @param guarWishCase
	 */
	public void setGuarWishCase(String guarWishCase) {
		this.guarWishCase = guarWishCase;
	}
	
    /**
     * @return guarWishCase
     */
	public String getGuarWishCase() {
		return this.guarWishCase;
	}
	
	/**
	 * @param wishCaseExpl
	 */
	public void setWishCaseExpl(String wishCaseExpl) {
		this.wishCaseExpl = wishCaseExpl;
	}
	
    /**
     * @return wishCaseExpl
     */
	public String getWishCaseExpl() {
		return this.wishCaseExpl;
	}
	
	/**
	 * @param isClose
	 */
	public void setIsClose(String isClose) {
		this.isClose = isClose;
	}
	
    /**
     * @return isClose
     */
	public String getIsClose() {
		return this.isClose;
	}
	
	/**
	 * @param closeExpl
	 */
	public void setCloseExpl(String closeExpl) {
		this.closeExpl = closeExpl;
	}
	
    /**
     * @return closeExpl
     */
	public String getCloseExpl() {
		return this.closeExpl;
	}
	
	/**
	 * @param isPriRent
	 */
	public void setIsPriRent(String isPriRent) {
		this.isPriRent = isPriRent;
	}
	
    /**
     * @return isPriRent
     */
	public String getIsPriRent() {
		return this.isPriRent;
	}
	
	/**
	 * @param rentExpl
	 */
	public void setRentExpl(String rentExpl) {
		this.rentExpl = rentExpl;
	}
	
    /**
     * @return rentExpl
     */
	public String getRentExpl() {
		return this.rentExpl;
	}
	
	/**
	 * @param isOtherRiskEvent
	 */
	public void setIsOtherRiskEvent(String isOtherRiskEvent) {
		this.isOtherRiskEvent = isOtherRiskEvent;
	}
	
    /**
     * @return isOtherRiskEvent
     */
	public String getIsOtherRiskEvent() {
		return this.isOtherRiskEvent;
	}
	
	/**
	 * @param riskEventExpl
	 */
	public void setRiskEventExpl(String riskEventExpl) {
		this.riskEventExpl = riskEventExpl;
	}
	
    /**
     * @return riskEventExpl
     */
	public String getRiskEventExpl() {
		return this.riskEventExpl;
	}
	
	/**
	 * @param isRuin
	 */
	public void setIsRuin(String isRuin) {
		this.isRuin = isRuin;
	}
	
    /**
     * @return isRuin
     */
	public String getIsRuin() {
		return this.isRuin;
	}
	
	/**
	 * @param ruinExpl
	 */
	public void setRuinExpl(String ruinExpl) {
		this.ruinExpl = ruinExpl;
	}
	
    /**
     * @return ruinExpl
     */
	public String getRuinExpl() {
		return this.ruinExpl;
	}
	
	/**
	 * @param isChangeGuar
	 */
	public void setIsChangeGuar(String isChangeGuar) {
		this.isChangeGuar = isChangeGuar;
	}
	
    /**
     * @return isChangeGuar
     */
	public String getIsChangeGuar() {
		return this.isChangeGuar;
	}
	
	/**
	 * @param changeGuarRemark
	 */
	public void setChangeGuarRemark(String changeGuarRemark) {
		this.changeGuarRemark = changeGuarRemark;
	}
	
    /**
     * @return changeGuarRemark
     */
	public String getChangeGuarRemark() {
		return this.changeGuarRemark;
	}
	
	/**
	 * @param isToEnterGuar
	 */
	public void setIsToEnterGuar(String isToEnterGuar) {
		this.isToEnterGuar = isToEnterGuar;
	}
	
    /**
     * @return isToEnterGuar
     */
	public String getIsToEnterGuar() {
		return this.isToEnterGuar;
	}
	
	/**
	 * @param toEnterGuarRemark
	 */
	public void setToEnterGuarRemark(String toEnterGuarRemark) {
		this.toEnterGuarRemark = toEnterGuarRemark;
	}
	
    /**
     * @return toEnterGuarRemark
     */
	public String getToEnterGuarRemark() {
		return this.toEnterGuarRemark;
	}
	
	/**
	 * @param totlEvlu
	 */
	public void setTotlEvlu(String totlEvlu) {
		this.totlEvlu = totlEvlu;
	}
	
    /**
     * @return totlEvlu
     */
	public String getTotlEvlu() {
		return this.totlEvlu;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}