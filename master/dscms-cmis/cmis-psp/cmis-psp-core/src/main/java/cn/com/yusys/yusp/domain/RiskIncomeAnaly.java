/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskIncomeAnaly
 * @类描述: risk_income_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_income_analy")
public class RiskIncomeAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 家庭年总收入 **/
	@Column(name = "FAM_TOTAL_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal famTotalIncome;
	
	/** 家庭年总支出 **/
	@Column(name = "FAM_TOTAL_PAY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal famTotalPay;
	
	/** 年收入与年支出对比 **/
	@Column(name = "INCOME_BALANCE", unique = false, nullable = true, length = 5)
	private String incomeBalance;
	
	/** 家庭总资产 **/
	@Column(name = "FAM_TOTAL_ASSET", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal famTotalAsset;
	
	/** 家庭总负债 **/
	@Column(name = "FAM_TOTAL_DEBT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal famTotalDebt;
	
	/** 家庭总资产与总负债对比 **/
	@Column(name = "FAM_ASSET_BALANCE", unique = false, nullable = true, length = 5)
	private String famAssetBalance;
	
	/** 工作稳定性 **/
	@Column(name = "IS_WORK_STABLE", unique = false, nullable = true, length = 5)
	private String isWorkStable;
	
	/** 第一还款来源是否充足 **/
	@Column(name = "IS_INCOME_SUFFICE", unique = false, nullable = true, length = 5)
	private String isIncomeSuffice;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param famTotalIncome
	 */
	public void setFamTotalIncome(java.math.BigDecimal famTotalIncome) {
		this.famTotalIncome = famTotalIncome;
	}
	
    /**
     * @return famTotalIncome
     */
	public java.math.BigDecimal getFamTotalIncome() {
		return this.famTotalIncome;
	}
	
	/**
	 * @param famTotalPay
	 */
	public void setFamTotalPay(java.math.BigDecimal famTotalPay) {
		this.famTotalPay = famTotalPay;
	}
	
    /**
     * @return famTotalPay
     */
	public java.math.BigDecimal getFamTotalPay() {
		return this.famTotalPay;
	}
	
	/**
	 * @param incomeBalance
	 */
	public void setIncomeBalance(String incomeBalance) {
		this.incomeBalance = incomeBalance;
	}
	
    /**
     * @return incomeBalance
     */
	public String getIncomeBalance() {
		return this.incomeBalance;
	}
	
	/**
	 * @param famTotalAsset
	 */
	public void setFamTotalAsset(java.math.BigDecimal famTotalAsset) {
		this.famTotalAsset = famTotalAsset;
	}
	
    /**
     * @return famTotalAsset
     */
	public java.math.BigDecimal getFamTotalAsset() {
		return this.famTotalAsset;
	}
	
	/**
	 * @param famTotalDebt
	 */
	public void setFamTotalDebt(java.math.BigDecimal famTotalDebt) {
		this.famTotalDebt = famTotalDebt;
	}
	
    /**
     * @return famTotalDebt
     */
	public java.math.BigDecimal getFamTotalDebt() {
		return this.famTotalDebt;
	}
	
	/**
	 * @param famAssetBalance
	 */
	public void setFamAssetBalance(String famAssetBalance) {
		this.famAssetBalance = famAssetBalance;
	}
	
    /**
     * @return famAssetBalance
     */
	public String getFamAssetBalance() {
		return this.famAssetBalance;
	}
	
	/**
	 * @param isWorkStable
	 */
	public void setIsWorkStable(String isWorkStable) {
		this.isWorkStable = isWorkStable;
	}
	
    /**
     * @return isWorkStable
     */
	public String getIsWorkStable() {
		return this.isWorkStable;
	}
	
	/**
	 * @param isIncomeSuffice
	 */
	public void setIsIncomeSuffice(String isIncomeSuffice) {
		this.isIncomeSuffice = isIncomeSuffice;
	}
	
    /**
     * @return isIncomeSuffice
     */
	public String getIsIncomeSuffice() {
		return this.isIncomeSuffice;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}