/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspRiskFactor
 * @类描述: psp_risk_factor数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_risk_factor")
public class PspRiskFactor extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 账户是否查封 **/
	@Column(name = "IS_ACCT_ATTACHMENT", unique = false, nullable = true, length = 5)
	private String isAcctAttachment;
	
	/** 查封原因 **/
	@Column(name = "ACCT_ATTACHMENT_REMARK", unique = false, nullable = true, length = 65535)
	private String acctAttachmentRemark;
	
	/** 是否已向总行提交人工风险预警 **/
	@Column(name = "IS_SUBMIT_MANUAL_ALT", unique = false, nullable = true, length = 5)
	private String isSubmitManualAlt;
	
	/** 提交时间 **/
	@Column(name = "SUBMIT_DATE", unique = false, nullable = true, length = 20)
	private String submitDate;
	
	/** 涉诉情况 **/
	@Column(name = "LAWSUIT_SITU", unique = false, nullable = true, length = 65535)
	private String lawsuitSitu;
	
	/** 不良成因 **/
	@Column(name = "BAD_REASON", unique = false, nullable = true, length = 65535)
	private String badReason;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isAcctAttachment
	 */
	public void setIsAcctAttachment(String isAcctAttachment) {
		this.isAcctAttachment = isAcctAttachment;
	}
	
    /**
     * @return isAcctAttachment
     */
	public String getIsAcctAttachment() {
		return this.isAcctAttachment;
	}
	
	/**
	 * @param acctAttachmentRemark
	 */
	public void setAcctAttachmentRemark(String acctAttachmentRemark) {
		this.acctAttachmentRemark = acctAttachmentRemark;
	}
	
    /**
     * @return acctAttachmentRemark
     */
	public String getAcctAttachmentRemark() {
		return this.acctAttachmentRemark;
	}
	
	/**
	 * @param isSubmitManualAlt
	 */
	public void setIsSubmitManualAlt(String isSubmitManualAlt) {
		this.isSubmitManualAlt = isSubmitManualAlt;
	}
	
    /**
     * @return isSubmitManualAlt
     */
	public String getIsSubmitManualAlt() {
		return this.isSubmitManualAlt;
	}
	
	/**
	 * @param submitDate
	 */
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}
	
    /**
     * @return submitDate
     */
	public String getSubmitDate() {
		return this.submitDate;
	}
	
	/**
	 * @param lawsuitSitu
	 */
	public void setLawsuitSitu(String lawsuitSitu) {
		this.lawsuitSitu = lawsuitSitu;
	}
	
    /**
     * @return lawsuitSitu
     */
	public String getLawsuitSitu() {
		return this.lawsuitSitu;
	}
	
	/**
	 * @param badReason
	 */
	public void setBadReason(String badReason) {
		this.badReason = badReason;
	}
	
    /**
     * @return badReason
     */
	public String getBadReason() {
		return this.badReason;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}