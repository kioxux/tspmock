package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.PspTaskDelayApp;
import cn.com.yusys.yusp.domain.PspTaskList;
import cn.com.yusys.yusp.enums.returncode.EpsEnum;
import cn.com.yusys.yusp.flow.ClientCons;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFException;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.PspTaskDelayAppService;
import cn.com.yusys.yusp.service.PspTaskListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author likc
 * @version 1.0.0
 * @date 2021/06/16
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class DHGL10BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(DHGL10BizService.class);

    @Autowired
    private PspTaskDelayAppService pspTaskDelayAppService;

    @Autowired
    private PspTaskListService pspTaskListService;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();

        String bizType = resultInstanceDto.getBizType();
        Map param = resultInstanceDto.getParam();
        Map pspTask = new HashMap();
        String delayDate = "";
        String taskNo = "";
        try {
            // 判断传递参数是否为空
            if (CollectionUtils.isEmpty(param)) {
                throw new Exception(EpsEnum.EPS030003.value + "param");
            } else if (param.containsKey("pspTask")) {
                pspTask = (Map) param.get("pspTask");
                if (CollectionUtils.isEmpty(pspTask)) {
                    throw new Exception(EpsEnum.EPS030003.value + "pspTask");
                } else {
                    if (pspTask.containsKey("delayDate") && pspTask.get("delayDate") != null) {
                        delayDate = (String) pspTask.get("delayDate");//延期完成时间
                    }
                    if (pspTask.containsKey("taskNo") && pspTask.get("taskNo") != null) {
                        taskNo = (String) pspTask.get("taskNo"); //任务编号
                    } else {
                        throw new Exception(EpsEnum.EPS030003.value + "taskNo");
                    }
                }
            }
            PspTaskList pspTaskList = new PspTaskList();
            pspTaskList.setTaskNo(taskNo);

            log.info("后业务处理类型:" + currentOpType);
            //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
            // N-> 不做任何处理,一直为审批中的状态
            PspTaskDelayApp pspTaskDelayApp = new PspTaskDelayApp();
            pspTaskDelayApp.setPkId(serno);

            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + resultInstanceDto);
                pspTaskDelayApp.setApproveStatus("111");
                pspTaskDelayAppService.updateSelective(pspTaskDelayApp);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                //审批通过
                log.info("结束操作:" + resultInstanceDto);
                pspTaskDelayApp.setApproveStatus("997");
                pspTaskDelayAppService.updateSelective(pspTaskDelayApp);

                if (CmisFlowConstants.DH045.equals(bizType)) {//贷后终止流程
                    //修改任务状态
                    pspTaskList.setApproveStatus("990");
                    pspTaskListService.update(pspTaskList);
                } else if (CmisFlowConstants.DH041.equals(bizType)) {//延期流程
                    pspTaskList.setTaskEndDt(delayDate); //最后完成时间
                    pspTaskListService.update(pspTaskList);
                }

                //
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                pspTaskDelayApp.setApproveStatus("992");
                pspTaskDelayAppService.updateSelective(pspTaskDelayApp);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                pspTaskDelayApp.setApproveStatus("992");
                pspTaskDelayAppService.updateSelective(pspTaskDelayApp);
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             态改为追回 991
                log.info("拿回操作:" + resultInstanceDto);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                pspTaskDelayApp.setApproveStatus("998");
                pspTaskDelayAppService.updateSelective(pspTaskDelayApp);
                log.info("否决操作结束:" + resultInstanceDto);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }

        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                WFException exception = new WFException();
                exception.setBizId(resultInstanceDto.getBizId());
                exception.setBizType(resultInstanceDto.getBizType());
                exception.setFlowName(resultInstanceDto.getFlowName());
                exception.setInstanceId(resultInstanceDto.getInstanceId());
                exception.setNodeId(resultInstanceDto.getNodeId());
                exception.setNodeName(resultInstanceDto.getNodeName());
                exception.setUserId(resultInstanceDto.getCurrentUserId());
                exception.setOpType(currentOpType);
                // 后业务处理失败时，将异常信息保存到异常表中
                amqpTemplate.convertAndSend(ClientCons.queue_exception, exception);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DHGL10.equals(flowCode);
    }
}
