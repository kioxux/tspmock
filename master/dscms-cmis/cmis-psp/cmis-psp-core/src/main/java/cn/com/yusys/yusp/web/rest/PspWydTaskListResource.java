package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspWydTaskList;
import cn.com.yusys.yusp.dto.PspTaskAndRstDto;
import cn.com.yusys.yusp.dto.PspWydBackTjListDto;
import cn.com.yusys.yusp.dto.PspWydTackTjListDto;
import cn.com.yusys.yusp.service.PspWydTaskListService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWydTaskListResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: likc
 * @创建时间: 2021-06-11 14:17:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pspwydtasklist")
public class PspWydTaskListResource {

    @Autowired
    private PspWydTaskListService pspWydTaskListService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspWydTaskList>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspWydTaskList> list = pspWydTaskListService.selectAll(queryModel);
        return new ResultDto<List<PspWydTaskList>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:getPspWydTaskList
     * @函数描述:查询待检查任务列表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getPspWydTaskList")
    protected ResultDto<List<PspWydTaskList>> getPspWydTaskList(@RequestBody QueryModel queryModel) throws Exception {
        List<PspWydTaskList> list = pspWydTaskListService.getPspWydTaskList(queryModel);
        return new ResultDto<List<PspWydTaskList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspWydTaskList> show(@PathVariable("pkId") String pkId) {
        PspWydTaskList PspWydTaskList = pspWydTaskListService.selectByPrimaryKey(pkId);
        return new ResultDto<PspWydTaskList>(PspWydTaskList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PspWydTaskList> create(@RequestBody PspWydTaskList PspWydTaskList) {
        PspWydTaskList.setCreateTime(new Date());
        pspWydTaskListService.insert(PspWydTaskList);
        return new ResultDto<PspWydTaskList>(PspWydTaskList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspWydTaskList pspWydTaskList) throws Exception {
        int result = pspWydTaskListService.update(pspWydTaskList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updatebycondition
     * @函数描述:用于更新检查状态和审批状态
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatebycondition")
    protected ResultDto<Integer> updateSelective(@RequestBody PspWydTaskList PspWydTaskList) {
        PspWydTaskList.setUpdateTime(new Date());
        int result = pspWydTaskListService.updateSelective(PspWydTaskList);
        return new ResultDto<Integer>(result);
    }

    /**
     * 贷后正常任务统计查询
     * @函数名称:pspWydBackTjList
     * @函数描述:贷后正常任务统计查询
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/pspWydTackTjList")
    protected ResultDto<List<PspWydTackTjListDto>> pspWydTackTTjList() {
        QueryModel queryModel = new QueryModel();
        List<PspWydTackTjListDto> list = pspWydTaskListService.pspWydTackTjList(queryModel);
        return new ResultDto<List<PspWydTackTjListDto>>(list);
    }

}
