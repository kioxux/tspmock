/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPropertyCheck
 * @类描述: psp_property_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_property_check")
public class PspPropertyCheck extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 是我在我行开立专户 **/
	@Column(name = "IS_ACCT", unique = false, nullable = true, length = 5)
	private String isAcct;
	
	/** 专户开立说明 **/
	@Column(name = "ACCT_EXPL", unique = false, nullable = true, length = 65535)
	private String acctExpl;
	
	/** 账号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 40)
	private String acctNo;
	
	/** 账户名称 **/
	@Column(name = "ACCT_NAME", unique = false, nullable = true, length = 80)
	private String acctName;
	
	/** 账户序号 **/
	@Column(name = "ACCT_SEQ_NO", unique = false, nullable = true, length = 40)
	private String acctSeqNo;
	
	/** 开户行 **/
	@Column(name = "ACCTB", unique = false, nullable = true, length = 40)
	private String acctb;
	
	/** 项目预计总投入（万元） **/
	@Column(name = "TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalAmt;
	
	/** 他行贷款已投入（万元） **/
	@Column(name = "OTHER_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherAmt;
	
	/** 项目实际累计总投入（万元） **/
	@Column(name = "USE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal useAmt;
	
	/** 借款人自有资金已投入（万元） **/
	@Column(name = "LOCALITY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal localityAmt;
	
	/** 我行贷款已投入（万元） **/
	@Column(name = "BANK_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bankAmt;
	
	/** 借款人资本金已投入（万元） **/
	@Column(name = "OTHER_LOCALITY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherLocalityAmt;
	
	/** 目前项目进度是否与可研报告一致 **/
	@Column(name = "IS_IDENTICAL", unique = false, nullable = true, length = 5)
	private String isIdentical;
	
	/** 项目进度说明 **/
	@Column(name = "IDENTICAL_EXPL", unique = false, nullable = true, length = 65535)
	private String identicalExpl;
	
	/** 自筹资金是否能按期、足额到位 **/
	@Column(name = "IS_ONTIME_AMT", unique = false, nullable = true, length = 5)
	private String isOntimeAmt;
	
	/** 自筹资金到位说明 **/
	@Column(name = "ONTIME_AMT_EXPL", unique = false, nullable = true, length = 65535)
	private String ontimeAmtExpl;
	
	/** 是否按时竣工 **/
	@Column(name = "IS_COMPLETION", unique = false, nullable = true, length = 5)
	private String isCompletion;
	
	/** 未按时竣工说明 **/
	@Column(name = "COMPLETION_EXPL", unique = false, nullable = true, length = 65535)
	private String completionExpl;
	
	/** 消防是否验收合格 **/
	@Column(name = "IS_CERTIFICATE", unique = false, nullable = true, length = 5)
	private String isCertificate;
	
	/** 消防不合格说明 **/
	@Column(name = "CERTIFICATE_EXPL", unique = false, nullable = true, length = 65535)
	private String certificateExpl;
	
	/** 实际投资与预期投资是否存在差异 **/
	@Column(name = "IS_DISCREPANCY", unique = false, nullable = true, length = 5)
	private String isDiscrepancy;
	
	/** 投资差异说明 **/
	@Column(name = "DISCREPANCY_EXPL", unique = false, nullable = true, length = 65535)
	private String discrepancyExpl;
	
	/** 项目实际生产能力是否达到预期要求 **/
	@Column(name = "IS_ARRIVE_NEED", unique = false, nullable = true, length = 5)
	private String isArriveNeed;
	
	/** 生产能力说明 **/
	@Column(name = "ARRIVE_NEED_EXPL", unique = false, nullable = true, length = 65535)
	private String arriveNeedExpl;
	
	/** 产品质量是否有问题 **/
	@Column(name = "IS_QUALITY", unique = false, nullable = true, length = 5)
	private String isQuality;
	
	/** 质量问题说明 **/
	@Column(name = "QUALITY_EXPL", unique = false, nullable = true, length = 65535)
	private String qualityExpl;
	
	/** 生产许可证等相关证书是否齐全 **/
	@Column(name = "IS_COMP", unique = false, nullable = true, length = 5)
	private String isComp;
	
	/** 证书齐全说明 **/
	@Column(name = "COMP_EXPL", unique = false, nullable = true, length = 65535)
	private String compExpl;
	
	/** 排污许可证是否取得 **/
	@Column(name = "IS_ROW_LICE", unique = false, nullable = true, length = 5)
	private String isRowLice;
	
	/** 排污许可证说明 **/
	@Column(name = "ROW_LICE_EXPL", unique = false, nullable = true, length = 65535)
	private String rowLiceExpl;
	
	/** 是否能按计划进入批量生产 **/
	@Column(name = "IS_BATCH_PRODUCE", unique = false, nullable = true, length = 5)
	private String isBatchProduce;
	
	/** 按时批量生产说明 **/
	@Column(name = "BATCH_PRODUCE_EXPL", unique = false, nullable = true, length = 65535)
	private String batchProduceExpl;
	
	/** 能否按预期及时回笼货款 **/
	@Column(name = "IS_AMT_BACK", unique = false, nullable = true, length = 5)
	private String isAmtBack;
	
	/** 回笼货款说明 **/
	@Column(name = "AMT_BACK_EXPL", unique = false, nullable = true, length = 65535)
	private String amtBackExpl;
	
	/** 是否按我行还款计划按时足额还款 **/
	@Column(name = "IS_REPAY_ONTIME", unique = false, nullable = true, length = 5)
	private String isRepayOntime;
	
	/** 按时还款说明 **/
	@Column(name = "REPAY_ONTIME_EXPL", unique = false, nullable = true, length = 65535)
	private String repayOntimeExpl;
	
	/** 贷款使用是否与项目进度相匹配 **/
	@Column(name = "IS_SCHEDULE_MATCHING", unique = false, nullable = true, length = 5)
	private String isScheduleMatching;
	
	/** 进度匹配说明 **/
	@Column(name = "SCHEDULE_EXPL", unique = false, nullable = true, length = 65535)
	private String scheduleExpl;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isAcct
	 */
	public void setIsAcct(String isAcct) {
		this.isAcct = isAcct;
	}
	
    /**
     * @return isAcct
     */
	public String getIsAcct() {
		return this.isAcct;
	}
	
	/**
	 * @param acctExpl
	 */
	public void setAcctExpl(String acctExpl) {
		this.acctExpl = acctExpl;
	}
	
    /**
     * @return acctExpl
     */
	public String getAcctExpl() {
		return this.acctExpl;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	
    /**
     * @return acctName
     */
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param acctSeqNo
	 */
	public void setAcctSeqNo(String acctSeqNo) {
		this.acctSeqNo = acctSeqNo;
	}
	
    /**
     * @return acctSeqNo
     */
	public String getAcctSeqNo() {
		return this.acctSeqNo;
	}
	
	/**
	 * @param acctb
	 */
	public void setAcctb(String acctb) {
		this.acctb = acctb;
	}
	
    /**
     * @return acctb
     */
	public String getAcctb() {
		return this.acctb;
	}
	
	/**
	 * @param totalAmt
	 */
	public void setTotalAmt(java.math.BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	
    /**
     * @return totalAmt
     */
	public java.math.BigDecimal getTotalAmt() {
		return this.totalAmt;
	}
	
	/**
	 * @param otherAmt
	 */
	public void setOtherAmt(java.math.BigDecimal otherAmt) {
		this.otherAmt = otherAmt;
	}
	
    /**
     * @return otherAmt
     */
	public java.math.BigDecimal getOtherAmt() {
		return this.otherAmt;
	}
	
	/**
	 * @param useAmt
	 */
	public void setUseAmt(java.math.BigDecimal useAmt) {
		this.useAmt = useAmt;
	}
	
    /**
     * @return useAmt
     */
	public java.math.BigDecimal getUseAmt() {
		return this.useAmt;
	}
	
	/**
	 * @param localityAmt
	 */
	public void setLocalityAmt(java.math.BigDecimal localityAmt) {
		this.localityAmt = localityAmt;
	}
	
    /**
     * @return localityAmt
     */
	public java.math.BigDecimal getLocalityAmt() {
		return this.localityAmt;
	}
	
	/**
	 * @param bankAmt
	 */
	public void setBankAmt(java.math.BigDecimal bankAmt) {
		this.bankAmt = bankAmt;
	}
	
    /**
     * @return bankAmt
     */
	public java.math.BigDecimal getBankAmt() {
		return this.bankAmt;
	}
	
	/**
	 * @param otherLocalityAmt
	 */
	public void setOtherLocalityAmt(java.math.BigDecimal otherLocalityAmt) {
		this.otherLocalityAmt = otherLocalityAmt;
	}
	
    /**
     * @return otherLocalityAmt
     */
	public java.math.BigDecimal getOtherLocalityAmt() {
		return this.otherLocalityAmt;
	}
	
	/**
	 * @param isIdentical
	 */
	public void setIsIdentical(String isIdentical) {
		this.isIdentical = isIdentical;
	}
	
    /**
     * @return isIdentical
     */
	public String getIsIdentical() {
		return this.isIdentical;
	}
	
	/**
	 * @param identicalExpl
	 */
	public void setIdenticalExpl(String identicalExpl) {
		this.identicalExpl = identicalExpl;
	}
	
    /**
     * @return identicalExpl
     */
	public String getIdenticalExpl() {
		return this.identicalExpl;
	}
	
	/**
	 * @param isOntimeAmt
	 */
	public void setIsOntimeAmt(String isOntimeAmt) {
		this.isOntimeAmt = isOntimeAmt;
	}
	
    /**
     * @return isOntimeAmt
     */
	public String getIsOntimeAmt() {
		return this.isOntimeAmt;
	}
	
	/**
	 * @param ontimeAmtExpl
	 */
	public void setOntimeAmtExpl(String ontimeAmtExpl) {
		this.ontimeAmtExpl = ontimeAmtExpl;
	}
	
    /**
     * @return ontimeAmtExpl
     */
	public String getOntimeAmtExpl() {
		return this.ontimeAmtExpl;
	}
	
	/**
	 * @param isCompletion
	 */
	public void setIsCompletion(String isCompletion) {
		this.isCompletion = isCompletion;
	}
	
    /**
     * @return isCompletion
     */
	public String getIsCompletion() {
		return this.isCompletion;
	}
	
	/**
	 * @param completionExpl
	 */
	public void setCompletionExpl(String completionExpl) {
		this.completionExpl = completionExpl;
	}
	
    /**
     * @return completionExpl
     */
	public String getCompletionExpl() {
		return this.completionExpl;
	}
	
	/**
	 * @param isCertificate
	 */
	public void setIsCertificate(String isCertificate) {
		this.isCertificate = isCertificate;
	}
	
    /**
     * @return isCertificate
     */
	public String getIsCertificate() {
		return this.isCertificate;
	}
	
	/**
	 * @param certificateExpl
	 */
	public void setCertificateExpl(String certificateExpl) {
		this.certificateExpl = certificateExpl;
	}
	
    /**
     * @return certificateExpl
     */
	public String getCertificateExpl() {
		return this.certificateExpl;
	}
	
	/**
	 * @param isDiscrepancy
	 */
	public void setIsDiscrepancy(String isDiscrepancy) {
		this.isDiscrepancy = isDiscrepancy;
	}
	
    /**
     * @return isDiscrepancy
     */
	public String getIsDiscrepancy() {
		return this.isDiscrepancy;
	}
	
	/**
	 * @param discrepancyExpl
	 */
	public void setDiscrepancyExpl(String discrepancyExpl) {
		this.discrepancyExpl = discrepancyExpl;
	}
	
    /**
     * @return discrepancyExpl
     */
	public String getDiscrepancyExpl() {
		return this.discrepancyExpl;
	}
	
	/**
	 * @param isArriveNeed
	 */
	public void setIsArriveNeed(String isArriveNeed) {
		this.isArriveNeed = isArriveNeed;
	}
	
    /**
     * @return isArriveNeed
     */
	public String getIsArriveNeed() {
		return this.isArriveNeed;
	}
	
	/**
	 * @param arriveNeedExpl
	 */
	public void setArriveNeedExpl(String arriveNeedExpl) {
		this.arriveNeedExpl = arriveNeedExpl;
	}
	
    /**
     * @return arriveNeedExpl
     */
	public String getArriveNeedExpl() {
		return this.arriveNeedExpl;
	}
	
	/**
	 * @param isQuality
	 */
	public void setIsQuality(String isQuality) {
		this.isQuality = isQuality;
	}
	
    /**
     * @return isQuality
     */
	public String getIsQuality() {
		return this.isQuality;
	}
	
	/**
	 * @param qualityExpl
	 */
	public void setQualityExpl(String qualityExpl) {
		this.qualityExpl = qualityExpl;
	}
	
    /**
     * @return qualityExpl
     */
	public String getQualityExpl() {
		return this.qualityExpl;
	}
	
	/**
	 * @param isComp
	 */
	public void setIsComp(String isComp) {
		this.isComp = isComp;
	}
	
    /**
     * @return isComp
     */
	public String getIsComp() {
		return this.isComp;
	}
	
	/**
	 * @param compExpl
	 */
	public void setCompExpl(String compExpl) {
		this.compExpl = compExpl;
	}
	
    /**
     * @return compExpl
     */
	public String getCompExpl() {
		return this.compExpl;
	}
	
	/**
	 * @param isRowLice
	 */
	public void setIsRowLice(String isRowLice) {
		this.isRowLice = isRowLice;
	}
	
    /**
     * @return isRowLice
     */
	public String getIsRowLice() {
		return this.isRowLice;
	}
	
	/**
	 * @param rowLiceExpl
	 */
	public void setRowLiceExpl(String rowLiceExpl) {
		this.rowLiceExpl = rowLiceExpl;
	}
	
    /**
     * @return rowLiceExpl
     */
	public String getRowLiceExpl() {
		return this.rowLiceExpl;
	}
	
	/**
	 * @param isBatchProduce
	 */
	public void setIsBatchProduce(String isBatchProduce) {
		this.isBatchProduce = isBatchProduce;
	}
	
    /**
     * @return isBatchProduce
     */
	public String getIsBatchProduce() {
		return this.isBatchProduce;
	}
	
	/**
	 * @param batchProduceExpl
	 */
	public void setBatchProduceExpl(String batchProduceExpl) {
		this.batchProduceExpl = batchProduceExpl;
	}
	
    /**
     * @return batchProduceExpl
     */
	public String getBatchProduceExpl() {
		return this.batchProduceExpl;
	}
	
	/**
	 * @param isAmtBack
	 */
	public void setIsAmtBack(String isAmtBack) {
		this.isAmtBack = isAmtBack;
	}
	
    /**
     * @return isAmtBack
     */
	public String getIsAmtBack() {
		return this.isAmtBack;
	}
	
	/**
	 * @param amtBackExpl
	 */
	public void setAmtBackExpl(String amtBackExpl) {
		this.amtBackExpl = amtBackExpl;
	}
	
    /**
     * @return amtBackExpl
     */
	public String getAmtBackExpl() {
		return this.amtBackExpl;
	}
	
	/**
	 * @param isRepayOntime
	 */
	public void setIsRepayOntime(String isRepayOntime) {
		this.isRepayOntime = isRepayOntime;
	}
	
    /**
     * @return isRepayOntime
     */
	public String getIsRepayOntime() {
		return this.isRepayOntime;
	}
	
	/**
	 * @param repayOntimeExpl
	 */
	public void setRepayOntimeExpl(String repayOntimeExpl) {
		this.repayOntimeExpl = repayOntimeExpl;
	}
	
    /**
     * @return repayOntimeExpl
     */
	public String getRepayOntimeExpl() {
		return this.repayOntimeExpl;
	}
	
	/**
	 * @param isScheduleMatching
	 */
	public void setIsScheduleMatching(String isScheduleMatching) {
		this.isScheduleMatching = isScheduleMatching;
	}
	
    /**
     * @return isScheduleMatching
     */
	public String getIsScheduleMatching() {
		return this.isScheduleMatching;
	}
	
	/**
	 * @param scheduleExpl
	 */
	public void setScheduleExpl(String scheduleExpl) {
		this.scheduleExpl = scheduleExpl;
	}
	
    /**
     * @return scheduleExpl
     */
	public String getScheduleExpl() {
		return this.scheduleExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}