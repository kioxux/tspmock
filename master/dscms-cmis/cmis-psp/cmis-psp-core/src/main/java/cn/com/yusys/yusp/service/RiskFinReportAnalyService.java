/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RiskFinReportAnaly;
import cn.com.yusys.yusp.repository.mapper.RiskFinReportAnalyMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskFinReportAnalyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RiskFinReportAnalyService {

    @Autowired
    private RiskFinReportAnalyMapper riskFinReportAnalyMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RiskFinReportAnaly selectByPrimaryKey(String pkId) {
        return riskFinReportAnalyMapper.selectByPrimaryKey(pkId);
    }
	/**
     * @方法名称: queryRiskFinReportAnaly
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskFinReportAnaly queryRiskFinReportAnaly(RiskFinReportAnaly riskFinReportAnaly) {
        return riskFinReportAnalyMapper.queryRiskFinReportAnaly(riskFinReportAnaly);
    }
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RiskFinReportAnaly> selectAll(QueryModel model) {
        List<RiskFinReportAnaly> records = (List<RiskFinReportAnaly>) riskFinReportAnalyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RiskFinReportAnaly> selectByModel(QueryModel model) {
        List<RiskFinReportAnaly> list = riskFinReportAnalyMapper.selectByModel(model);
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RiskFinReportAnaly record) {
        return riskFinReportAnalyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RiskFinReportAnaly record) {
        return riskFinReportAnalyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RiskFinReportAnaly record) {
        return riskFinReportAnalyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RiskFinReportAnaly record) {
        return riskFinReportAnalyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return riskFinReportAnalyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return riskFinReportAnalyMapper.deleteByIds(ids);
    }
    public List<RiskFinReportAnaly> dealFinaData() {
        List<RiskFinReportAnaly> retList = new ArrayList<>();
        RiskFinReportAnaly riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("资产总计");
        riskFinReportAnaly.setIdxOrder("1");
        retList.add(riskFinReportAnaly);
//        riskFinReportAnaly = new RiskFinReportAnaly();
//        riskFinReportAnaly.setIdxName("流动资产合计");
//        riskFinReportAnaly.setIdxOrder("");
//        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("货币资金");
        riskFinReportAnaly.setIdxOrder("2");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("存货");
        riskFinReportAnaly.setIdxOrder("3");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("应收账款");
        riskFinReportAnaly.setIdxOrder("4");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("其他应收款");
        riskFinReportAnaly.setIdxOrder("5");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("长期投资");
        riskFinReportAnaly.setIdxOrder("6");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("固定资产净额");
        riskFinReportAnaly.setIdxOrder("7");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("流动负债合计");
        riskFinReportAnaly.setIdxOrder("8");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("短期借款");
        riskFinReportAnaly.setIdxOrder("9");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("长期借款");
        riskFinReportAnaly.setIdxOrder("10");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("实收资本");
        riskFinReportAnaly.setIdxOrder("11");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("资本公积");
        riskFinReportAnaly.setIdxOrder("12");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("未分配利润");
        riskFinReportAnaly.setIdxOrder("13");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("主营业务收入");
        riskFinReportAnaly.setIdxOrder("14");
        retList.add(riskFinReportAnaly);
//        riskFinReportAnaly = new RiskFinReportAnaly();
//        riskFinReportAnaly.setIdxName("投资收益");
//        riskFinReportAnaly.setIdxOrder("");
//        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("利润总额");
        riskFinReportAnaly.setIdxOrder("15");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("经营活动产生的现金流量净额");
        riskFinReportAnaly.setIdxOrder("16");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("投资活动产生的现金流量净额");
        riskFinReportAnaly.setIdxOrder("17");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("筹资活动产生的现金流量净额");
        riskFinReportAnaly.setIdxOrder("18");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("现金及现金等价物净增加额");
        riskFinReportAnaly.setIdxOrder("19");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("销售净利润率");
        riskFinReportAnaly.setIdxOrder("20");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("营业利润率");
        riskFinReportAnaly.setIdxOrder("21");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("资产收益率");
        riskFinReportAnaly.setIdxOrder("22");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("净资产收益率");
        riskFinReportAnaly.setIdxOrder("23");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("投资收益率");
        riskFinReportAnaly.setIdxOrder("24");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("投资收益现金率");
        riskFinReportAnaly.setIdxOrder("25");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("营运资金");
        riskFinReportAnaly.setIdxOrder("26");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("净利润增长率");
        riskFinReportAnaly.setIdxOrder("27");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("总资产增长率");
        riskFinReportAnaly.setIdxOrder("28");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("总负债增长率");
        riskFinReportAnaly.setIdxOrder("29");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("资产净值增长率");
        riskFinReportAnaly.setIdxOrder("30");
        retList.add(riskFinReportAnaly);
        riskFinReportAnaly = new RiskFinReportAnaly();
        riskFinReportAnaly.setIdxName("营业收入增长率");
        riskFinReportAnaly.setIdxOrder("31");
        retList.add(riskFinReportAnaly);
//        riskFinReportAnaly = new RiskFinReportAnaly();
//        riskFinReportAnaly.setIdxName("利润");
//        riskFinReportAnaly.setIdxOrder("34");
//        retList.add(riskFinReportAnaly);
        return retList;
    }

    /**
     * 修改表中报表名称
     * @param lists
     * @return
     */
    public List<RiskFinReportAnaly> changeLists(List<RiskFinReportAnaly> lists){
        List<RiskFinReportAnaly> list = new ArrayList<>();
        // 资产总计
        String zczjStr="ACC090123,Z01000001,XFZ0035,GC0000014,BS010120";
        // 短期借款
        String dqjkStr="ACC090203,XFZ0037,ACC070221,Z02010100";
        // 长期借款
        String cqjkStr="ACC070226,XFZ0052,ACC090209,Z02020100";
        // 实收资本
        String sszbStr="Z04010000";
        // 资本公积
        String zbgjStr="Z04020000,XFZ0063";
        // 未分配利润
        String wfplrStr="L09010000,L06020000,XFZ0066,Z04050000";
        // 主营业务收入
        String zyywsrStr = "L01000000,L02010000,PL010114";
        // 利润总额
        String lrzeStr="L05010000,XSL0015";
        // 经营活动产生的现金流量净额
        String jyStr="XSX0011,X01100000";
        // 投资活动产生的现金流量净额
        String tzStr="X02100000,XSX0024";
        // 筹资活动产生的现金流量净额
        String czStr="XSX0034,X03090000";
        // 现金及现金等价物净增加额
        String xjStr="X05000000,XSX0036";
        // 销售净利润率
        String xsStr="C01040000,XSC0022";
        // 营业利润率
        String yylrlStr="XSC0023,C01020000";
        // 资产收益率
        String zcsylStr="C01060000";
        // 净资产收益率
        String jzcsylStr="XSC0026,C01070000";
        // 投资收益率
        String tzsylStr="C01110000,XSC0019";
        // 投资收益现金率
        String tzsyxjlStr="C01140000";
        // 营运资金
        String yyzjStr="C04040000";
        // 净利润增长率
        String jlrzzlStr="C05010000,XSC0030";
        // 货币资金
        String hbzjStr="ZRC0002,ACC090103,XFZ0002,Z01010100";
        // 总资产增长率
        String zzczzlStr="C05020000";
        // 总负债增长率
        String zfzzzlStr="C05030000";
        // 资产净值增长率
        String zcjzzzlStr="C05040000";
        // 营业收入增长率
        String yysrzzlStr="XSC0032,C05050000";
        // 存货
        String chStr="Z01011200,XFZ0010,GC0000005,Z01011210,ACC090106";
        // 应收账款
        String yszkStr="XFZ0005,Z01010610,ZRC0006,Z01010600,BS010104";
        // 其他应收款
        String qtyskStr="XFZ0009,BS010104,Z01011100";
        // 长期投资
        String cqtzStr="Z01020100,ACC090113";
        // 固定资产净额
        String gdzcjeStr="Z01030100,ACC090117,XFZ0021,BS010110";
        // 流动负债合计
        String ldfzhjStr="BS010221,ACC090207,Z02010001,XFZ0050";

        for(RiskFinReportAnaly riskFinReportAnaly: lists){
            if(zczjStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("资产总计");
            }
            if(dqjkStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("短期借款");
            }
            if(cqjkStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("长期借款");
            }
            if(sszbStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("实收资本");
            }
            if(zbgjStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("资本公积");
            }
            if(wfplrStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("未分配利润");
            }
            if(zyywsrStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("主营业务收入");
            }
            if(lrzeStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("利润总额");
            }
            if(jyStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("经营活动产生的现金流量净额");
            }
            if(tzStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("投资活动产生的现金流量净额");
            }
            if(czStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("筹资活动产生的现金流量净额");
            }
            if(xjStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("现金及现金等价物净增加额");
            }
            if(xsStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("销售净利润率");
            }
            if(yylrlStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("营业利润率");
            }
            if(zcsylStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("资产收益率");
            }
            if(jzcsylStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("净资产收益率");
            }
            if(tzsylStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("投资收益率");
            }
            if(tzsyxjlStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("投资收益现金率");
            }
            if(yyzjStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("营运资金");
            }
            if(jlrzzlStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("净利润增长率");
            }
            if(hbzjStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("货币资金");
            }
            if(zzczzlStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("总资产增长率");
            }
            if(zfzzzlStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("总负债增长率");
            }
            if(zcjzzzlStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("资产净值增长率");
            }
            if(yysrzzlStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("营业收入增长率");
            }
            if(chStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("存货");
            }
            if(yszkStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("应收账款");
            }
            if(qtyskStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("其他应收款");
            }
            if(cqtzStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("长期投资");
            }
            if(gdzcjeStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("固定资产净额");
            }
            if(ldfzhjStr.contains(riskFinReportAnaly.getIdxKey())){
                riskFinReportAnaly.setIdxName("流动负债合计");
            }
            list.add(riskFinReportAnaly);
        }
        return list;
    }
}

