/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspLegalCheck
 * @类描述: psp_legal_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_legal_check")
public class PspLegalCheck extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 重要法律文本是否合法、有效 **/
	@Column(name = "IS_VLD", unique = false, nullable = true, length = 5)
	private String isVld;
	
	/** 主从合同能否衔接，无法律瑕疵 **/
	@Column(name = "IS_LNK", unique = false, nullable = true, length = 5)
	private String isLnk;
	
	/** 重要法律文本是否具有时效性 **/
	@Column(name = "IS_TIME_EFFECT", unique = false, nullable = true, length = 5)
	private String isTimeEffect;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isVld
	 */
	public void setIsVld(String isVld) {
		this.isVld = isVld;
	}
	
    /**
     * @return isVld
     */
	public String getIsVld() {
		return this.isVld;
	}
	
	/**
	 * @param isLnk
	 */
	public void setIsLnk(String isLnk) {
		this.isLnk = isLnk;
	}
	
    /**
     * @return isLnk
     */
	public String getIsLnk() {
		return this.isLnk;
	}
	
	/**
	 * @param isTimeEffect
	 */
	public void setIsTimeEffect(String isTimeEffect) {
		this.isTimeEffect = isTimeEffect;
	}
	
    /**
     * @return isTimeEffect
     */
	public String getIsTimeEffect() {
		return this.isTimeEffect;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}