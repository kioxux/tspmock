package cn.com.yusys.yusp.service.client.batch.cmisbatch0006;


import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：不定期检查任务信息生成
 *
 * @author 茂茂
 * @version 1.0
 * @since   2021年7月13日21:54:58
 */
@Service
public class CmisBatch0006Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0006Service.class);

    // 1）注入：封装的接口类:批量日终管理模块
    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    /**
     * 业务逻辑处理方法：查詢[调度运行管理]信息
     *
     * @param cmisbatch0006ReqDto
     * @return
     */
    @Async
    public void cmisbatch0006(Cmisbatch0006ReqDto cmisbatch0006ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, JSON.toJSONString(cmisbatch0006ReqDto));
        ResultDto<Cmisbatch0006RespDto> cmisbatch0006ResultDto = cmisBatchClientService.cmisbatch0006(cmisbatch0006ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value, JSON.toJSONString(cmisbatch0006ResultDto));
        Cmisbatch0006RespDto cmisbatch0006RespDto = null;
        if(cmisbatch0006ResultDto !=null){
            String cmisbatch0006Code = Optional.ofNullable(cmisbatch0006ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String cmisbatch0006Meesage = Optional.ofNullable(cmisbatch0006ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisbatch0006ResultDto.getCode())) {
                //  获取相关的值并解析
                cmisbatch0006RespDto = cmisbatch0006ResultDto.getData();
            } else {
                //  抛出错误异常
                throw BizException.error(null, cmisbatch0006Code, cmisbatch0006Meesage);
            }
        }else{
            throw BizException.error(null, TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0006.key, DscmsEnum.TRADE_CODE_CMISBATCH0006.value);
    }
}
