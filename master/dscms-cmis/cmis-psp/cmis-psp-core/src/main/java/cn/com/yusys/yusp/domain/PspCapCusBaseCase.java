/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspCapCusBaseCase
 * @类描述: psp_cap_cus_base_case数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_cap_cus_base_case")
public class PspCapCusBaseCase extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 所属集团 **/
	@Column(name = "BELONG_GRP_ID", unique = false, nullable = true, length = 80)
	private String belongGrpId;
	
	/** 准入时内部主体评级 **/
	@Column(name = "EVAL_RESULT_INNER", unique = false, nullable = true, length = 5)
	private String evalResultInner;
	
	/** 准入时外部主体评级 **/
	@Column(name = "EVAL_RESULT_OUTER", unique = false, nullable = true, length = 5)
	private String evalResultOuter;
	
	/** 最新内部主体评级 **/
	@Column(name = "CURT_EVAL_RESULT_INNER", unique = false, nullable = true, length = 5)
	private String curtEvalResultInner;
	
	/** 最新外部主体评级 **/
	@Column(name = "CURT_EVAL_RESULT_OUTER", unique = false, nullable = true, length = 5)
	private String curtEvalResultOuter;
	
	/** 授信批复编号 **/
	@Column(name = "LMT_REPLY_NO", unique = false, nullable = true, length = 40)
	private String lmtReplyNo;
	
	/** 项目名称 **/
	@Column(name = "PRO_NAME", unique = false, nullable = true, length = 200)
	private String proName;
	
	/** 授信金额 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 已用额度 **/
	@Column(name = "OUTSTND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outstndAmt;
	
	/** 业务起始日 **/
	@Column(name = "BUSI_START_DATE", unique = false, nullable = true, length = 20)
	private String busiStartDate;
	
	/** 业务到期日 **/
	@Column(name = "BUSI_END_DATE", unique = false, nullable = true, length = 20)
	private String busiEndDate;
	
	/** 业务类型 **/
	@Column(name = "BUSI_TYPE", unique = false, nullable = true, length = 5)
	private String busiType;
	
	/** 业务管辖机构 **/
	@Column(name = "BUSI_ORG_ID", unique = false, nullable = true, length = 20)
	private String busiOrgId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param belongGrpId
	 */
	public void setBelongGrpId(String belongGrpId) {
		this.belongGrpId = belongGrpId;
	}
	
    /**
     * @return belongGrpId
     */
	public String getBelongGrpId() {
		return this.belongGrpId;
	}
	
	/**
	 * @param evalResultInner
	 */
	public void setEvalResultInner(String evalResultInner) {
		this.evalResultInner = evalResultInner;
	}
	
    /**
     * @return evalResultInner
     */
	public String getEvalResultInner() {
		return this.evalResultInner;
	}
	
	/**
	 * @param evalResultOuter
	 */
	public void setEvalResultOuter(String evalResultOuter) {
		this.evalResultOuter = evalResultOuter;
	}
	
    /**
     * @return evalResultOuter
     */
	public String getEvalResultOuter() {
		return this.evalResultOuter;
	}
	
	/**
	 * @param curtEvalResultInner
	 */
	public void setCurtEvalResultInner(String curtEvalResultInner) {
		this.curtEvalResultInner = curtEvalResultInner;
	}
	
    /**
     * @return curtEvalResultInner
     */
	public String getCurtEvalResultInner() {
		return this.curtEvalResultInner;
	}
	
	/**
	 * @param curtEvalResultOuter
	 */
	public void setCurtEvalResultOuter(String curtEvalResultOuter) {
		this.curtEvalResultOuter = curtEvalResultOuter;
	}
	
    /**
     * @return curtEvalResultOuter
     */
	public String getCurtEvalResultOuter() {
		return this.curtEvalResultOuter;
	}
	
	/**
	 * @param lmtReplyNo
	 */
	public void setLmtReplyNo(String lmtReplyNo) {
		this.lmtReplyNo = lmtReplyNo;
	}
	
    /**
     * @return lmtReplyNo
     */
	public String getLmtReplyNo() {
		return this.lmtReplyNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param outstndAmt
	 */
	public void setOutstndAmt(java.math.BigDecimal outstndAmt) {
		this.outstndAmt = outstndAmt;
	}
	
    /**
     * @return outstndAmt
     */
	public java.math.BigDecimal getOutstndAmt() {
		return this.outstndAmt;
	}
	
	/**
	 * @param busiStartDate
	 */
	public void setBusiStartDate(String busiStartDate) {
		this.busiStartDate = busiStartDate;
	}
	
    /**
     * @return busiStartDate
     */
	public String getBusiStartDate() {
		return this.busiStartDate;
	}
	
	/**
	 * @param busiEndDate
	 */
	public void setBusiEndDate(String busiEndDate) {
		this.busiEndDate = busiEndDate;
	}
	
    /**
     * @return busiEndDate
     */
	public String getBusiEndDate() {
		return this.busiEndDate;
	}
	
	/**
	 * @param busiType
	 */
	public void setBusiType(String busiType) {
		this.busiType = busiType;
	}
	
    /**
     * @return busiType
     */
	public String getBusiType() {
		return this.busiType;
	}
	
	/**
	 * @param busiOrgId
	 */
	public void setBusiOrgId(String busiOrgId) {
		this.busiOrgId = busiOrgId;
	}
	
    /**
     * @return busiOrgId
     */
	public String getBusiOrgId() {
		return this.busiOrgId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}