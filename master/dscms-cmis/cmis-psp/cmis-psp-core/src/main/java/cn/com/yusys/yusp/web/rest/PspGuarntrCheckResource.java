/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.PspGuarntrAndPldimnDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspGuarntrCheck;
import cn.com.yusys.yusp.service.PspGuarntrCheckService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspGuarntrCheckResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pspguarntrcheck")
public class PspGuarntrCheckResource {
    @Autowired
    private PspGuarntrCheckService pspGuarntrCheckService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspGuarntrCheck>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspGuarntrCheck> list = pspGuarntrCheckService.selectAll(queryModel);
        return new ResultDto<List<PspGuarntrCheck>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PspGuarntrCheck>> index(QueryModel queryModel) {
        List<PspGuarntrCheck> list = pspGuarntrCheckService.selectByModel(queryModel);
        return new ResultDto<List<PspGuarntrCheck>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @GetMapping("/queryList")
    protected ResultDto<List<PspGuarntrCheck>> queryList(QueryModel queryModel) {
        List<PspGuarntrCheck> list = pspGuarntrCheckService.selectByModel(queryModel);
        return new ResultDto<List<PspGuarntrCheck>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspGuarntrCheck> show(@PathVariable("pkId") String pkId) {
        PspGuarntrCheck pspGuarntrCheck = pspGuarntrCheckService.selectByPrimaryKey(pkId);
        return new ResultDto<PspGuarntrCheck>(pspGuarntrCheck);
    }
    /**
     * @函数名称:queryPspGuarntrCheck
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<PspGuarntrCheck> queryPspGuarntrCheck(@RequestBody PspGuarntrCheck pspGuarntrCheck) {
        pspGuarntrCheck = pspGuarntrCheckService.queryPspGuarntrCheck(pspGuarntrCheck);
        return new ResultDto<PspGuarntrCheck>(pspGuarntrCheck);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PspGuarntrCheck> create(@RequestBody PspGuarntrCheck pspGuarntrCheck) throws URISyntaxException {
        pspGuarntrCheckService.insert(pspGuarntrCheck);
        return new ResultDto<PspGuarntrCheck>(pspGuarntrCheck);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspGuarntrCheck pspGuarntrCheck) throws URISyntaxException {
        int result = pspGuarntrCheckService.update(pspGuarntrCheck);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspGuarntrCheckService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspGuarntrCheckService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateGuarntrAndPldimn
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateGuarntrAndPldimn")
    protected ResultDto<Integer> updateGuarntrAndPldimn(@RequestBody PspGuarntrAndPldimnDto pspGuarntrAndPldimnDto) throws URISyntaxException {
        int result = pspGuarntrCheckService.updateGuarntrAndPldimn(pspGuarntrAndPldimnDto);
        return new ResultDto<Integer>(result);
    }
}
