/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWarningInfoAnaly
 * @类描述: psp_warning_info_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_warning_info_analy")
public class PspWarningInfoAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 预警分析 **/
	@Column(name = "ALT_ANALY", unique = false, nullable = true, length = 65535)
	private String altAnaly;
	
	/** 是否有其他负面信息 **/
	@Column(name = "IS_NEGATIVE_INFO", unique = false, nullable = true, length = 5)
	private String isNegativeInfo;
	
	/** 分析说明 **/
	@Column(name = "ANALY", unique = false, nullable = true, length = 65535)
	private String analy;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param altAnaly
	 */
	public void setAltAnaly(String altAnaly) {
		this.altAnaly = altAnaly;
	}
	
    /**
     * @return altAnaly
     */
	public String getAltAnaly() {
		return this.altAnaly;
	}
	
	/**
	 * @param isNegativeInfo
	 */
	public void setIsNegativeInfo(String isNegativeInfo) {
		this.isNegativeInfo = isNegativeInfo;
	}
	
    /**
     * @return isNegativeInfo
     */
	public String getIsNegativeInfo() {
		return this.isNegativeInfo;
	}
	
	/**
	 * @param analy
	 */
	public void setAnaly(String analy) {
		this.analy = analy;
	}
	
    /**
     * @return analy
     */
	public String getAnaly() {
		return this.analy;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}