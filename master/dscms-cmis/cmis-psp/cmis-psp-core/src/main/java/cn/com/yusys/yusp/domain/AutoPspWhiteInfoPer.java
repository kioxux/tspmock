/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: AutoPspWhiteInfoPer
 * @类描述: auto_psp_white_info_per数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 09:45:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "auto_psp_white_info_per")
public class AutoPspWhiteInfoPer extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 自动化编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "AUTO_SERNO")
	private String autoSerno;
	
	/** 个人或者实际控制人(1或者0) **/
	@Column(name = "AUTO_TYPE", unique = false, nullable = false, length = 40)
	private String autoType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = false, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = false, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = false, length = 40)
	private String certCode;
	
	/** 通过状态 **/
	@Column(name = "AUTO_STATUS", unique = false, nullable = true, length = 2)
	private String autoStatus;
	
	/** 报告编号 **/
	@Column(name = "REPORT_ID", unique = false, nullable = true, length = 80)
	private String reportId;
	
	/** 查询人行征信是否成功 **/
	@Column(name = "QUERY_CRDREPORT_FLAG", unique = false, nullable = true, length = 2)
	private String queryCrdreportFlag;
	
	/** 个人或者实际控制人是否需要查询人行征信 **/
	@Column(name = "PER_NEED_QUERY_CREREPORT", unique = false, nullable = true, length = 2)
	private String perNeedQueryCrereport;
	
	/** 查询小安涉诉是否成功 **/
	@Column(name = "QUERY_XA_CASE_FLAG", unique = false, nullable = true, length = 2)
	private String queryXaCaseFlag;
	
	/** 存在小安涉诉 **/
	@Column(name = "EXIST_XA_CASE_INFO", unique = false, nullable = true, length = 2)
	private String existXaCaseInfo;
	
	/** 存在失信被执行人 **/
	@Column(name = "EXIST_LOST_EXECUTOR", unique = false, nullable = true, length = 2)
	private String existLostExecutor;
	
	/** 个人或者实际控制人近一年经营性贷款逾期次数（>=2 不符合) **/
	@Column(name = "JYX_OVER_COUNT", unique = false, nullable = true, length = 10)
	private Integer jyxOverCount;
	
	/** 个人或者实际控制人经营性连续逾期期数（>=0 不符合) **/
	@Column(name = "JYX_CONTI_OVERDUE", unique = false, nullable = true, length = 10)
	private Integer jyxContiOverdue;
	
	/** 个人或者实际控制人近一年消费贷款逾期次数（>=3 不符合) **/
	@Column(name = "XF_OVER_COUNT", unique = false, nullable = true, length = 10)
	private Integer xfOverCount;
	
	/** 个人或者实际控制人消费贷款连续逾期次数  **/
	@Column(name = "XF_CONTI_OVERDUE", unique = false, nullable = true, length = 10)
	private Integer xfContiOverdue;
	
	/** 个人或者实际控制人消费贷款当前逾期次数 **/
	@Column(name = "PER_LOAN_OVERDUE", unique = false, nullable = true, length = 10)
	private Integer perLoanOverdue;
	
	/** 个人或者实际控制人五级分类是否存在正常以下 **/
	@Column(name = "EXIST_PER_BAD_FIVECLASS", unique = false, nullable = true, length = 2)
	private String existPerBadFiveclass;
	
	/** 台账逾期天数 **/
	@Column(name = "ACC_OVERDUE_DAYS", unique = false, nullable = true, length = 10)
	private Integer accOverdueDays;
	
	/** 存在红黑预警等级 **/
	@Column(name = "EXIST_HIGH_RISK_LEVEL", unique = false, nullable = true, length = 2)
	private String existHighRiskLevel;
	
	/** 存在黑灰名单 **/
	@Column(name = "EXIST_BLACK_LIST", unique = false, nullable = true, length = 2)
	private String existBlackList;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 操作类型    **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param autoSerno
	 */
	public void setAutoSerno(String autoSerno) {
		this.autoSerno = autoSerno;
	}
	
    /**
     * @return autoSerno
     */
	public String getAutoSerno() {
		return this.autoSerno;
	}
	
	/**
	 * @param autoType
	 */
	public void setAutoType(String autoType) {
		this.autoType = autoType;
	}
	
    /**
     * @return autoType
     */
	public String getAutoType() {
		return this.autoType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param autoStatus
	 */
	public void setAutoStatus(String autoStatus) {
		this.autoStatus = autoStatus;
	}
	
    /**
     * @return autoStatus
     */
	public String getAutoStatus() {
		return this.autoStatus;
	}
	
	/**
	 * @param reportId
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	
    /**
     * @return reportId
     */
	public String getReportId() {
		return this.reportId;
	}
	
	/**
	 * @param queryCrdreportFlag
	 */
	public void setQueryCrdreportFlag(String queryCrdreportFlag) {
		this.queryCrdreportFlag = queryCrdreportFlag;
	}
	
    /**
     * @return queryCrdreportFlag
     */
	public String getQueryCrdreportFlag() {
		return this.queryCrdreportFlag;
	}
	
	/**
	 * @param perNeedQueryCrereport
	 */
	public void setPerNeedQueryCrereport(String perNeedQueryCrereport) {
		this.perNeedQueryCrereport = perNeedQueryCrereport;
	}
	
    /**
     * @return perNeedQueryCrereport
     */
	public String getPerNeedQueryCrereport() {
		return this.perNeedQueryCrereport;
	}
	
	/**
	 * @param queryXaCaseFlag
	 */
	public void setQueryXaCaseFlag(String queryXaCaseFlag) {
		this.queryXaCaseFlag = queryXaCaseFlag;
	}
	
    /**
     * @return queryXaCaseFlag
     */
	public String getQueryXaCaseFlag() {
		return this.queryXaCaseFlag;
	}
	
	/**
	 * @param existXaCaseInfo
	 */
	public void setExistXaCaseInfo(String existXaCaseInfo) {
		this.existXaCaseInfo = existXaCaseInfo;
	}
	
    /**
     * @return existXaCaseInfo
     */
	public String getExistXaCaseInfo() {
		return this.existXaCaseInfo;
	}
	
	/**
	 * @param existLostExecutor
	 */
	public void setExistLostExecutor(String existLostExecutor) {
		this.existLostExecutor = existLostExecutor;
	}
	
    /**
     * @return existLostExecutor
     */
	public String getExistLostExecutor() {
		return this.existLostExecutor;
	}
	
	/**
	 * @param jyxOverCount
	 */
	public void setJyxOverCount(Integer jyxOverCount) {
		this.jyxOverCount = jyxOverCount;
	}
	
    /**
     * @return jyxOverCount
     */
	public Integer getJyxOverCount() {
		return this.jyxOverCount;
	}
	
	/**
	 * @param jyxContiOverdue
	 */
	public void setJyxContiOverdue(Integer jyxContiOverdue) {
		this.jyxContiOverdue = jyxContiOverdue;
	}
	
    /**
     * @return jyxContiOverdue
     */
	public Integer getJyxContiOverdue() {
		return this.jyxContiOverdue;
	}
	
	/**
	 * @param xfOverCount
	 */
	public void setXfOverCount(Integer xfOverCount) {
		this.xfOverCount = xfOverCount;
	}
	
    /**
     * @return xfOverCount
     */
	public Integer getXfOverCount() {
		return this.xfOverCount;
	}
	
	/**
	 * @param xfContiOverdue
	 */
	public void setXfContiOverdue(Integer xfContiOverdue) {
		this.xfContiOverdue = xfContiOverdue;
	}
	
    /**
     * @return xfContiOverdue
     */
	public Integer getXfContiOverdue() {
		return this.xfContiOverdue;
	}
	
	/**
	 * @param perLoanOverdue
	 */
	public void setPerLoanOverdue(Integer perLoanOverdue) {
		this.perLoanOverdue = perLoanOverdue;
	}
	
    /**
     * @return perLoanOverdue
     */
	public Integer getPerLoanOverdue() {
		return this.perLoanOverdue;
	}
	
	/**
	 * @param existPerBadFiveclass
	 */
	public void setExistPerBadFiveclass(String existPerBadFiveclass) {
		this.existPerBadFiveclass = existPerBadFiveclass;
	}
	
    /**
     * @return existPerBadFiveclass
     */
	public String getExistPerBadFiveclass() {
		return this.existPerBadFiveclass;
	}
	
	/**
	 * @param accOverdueDays
	 */
	public void setAccOverdueDays(Integer accOverdueDays) {
		this.accOverdueDays = accOverdueDays;
	}
	
    /**
     * @return accOverdueDays
     */
	public Integer getAccOverdueDays() {
		return this.accOverdueDays;
	}
	
	/**
	 * @param existHighRiskLevel
	 */
	public void setExistHighRiskLevel(String existHighRiskLevel) {
		this.existHighRiskLevel = existHighRiskLevel;
	}
	
    /**
     * @return existHighRiskLevel
     */
	public String getExistHighRiskLevel() {
		return this.existHighRiskLevel;
	}
	
	/**
	 * @param existBlackList
	 */
	public void setExistBlackList(String existBlackList) {
		this.existBlackList = existBlackList;
	}
	
    /**
     * @return existBlackList
     */
	public String getExistBlackList() {
		return this.existBlackList;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}