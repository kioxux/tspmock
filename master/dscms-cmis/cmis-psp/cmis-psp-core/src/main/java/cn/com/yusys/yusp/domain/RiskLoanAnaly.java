/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskLoanAnaly
 * @类描述: risk_loan_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_loan_analy")
public class RiskLoanAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 授信发生后用途是否正常 **/
	@Column(name = "IS_NORMAL_AFT_LMT", unique = false, nullable = true, length = 5)
	private String isNormalAftLmt;
	
	/** 实际借款用途 **/
	@Column(name = "FACT_LOAN_US", unique = false, nullable = true, length = 65535)
	private String factLoanUs;
	
	/** 是否重组贷款 **/
	@Column(name = "IS_REC_LOAN", unique = false, nullable = true, length = 5)
	private String isRecLoan;
	
	/** 是否展期 **/
	@Column(name = "IS_EXT", unique = false, nullable = true, length = 5)
	private String isExt;
	
	/** 是否借新还旧 **/
	@Column(name = "IS_REFINANCE", unique = false, nullable = true, length = 5)
	private String isRefinance;
	
	/** 保全状态 **/
	@Column(name = "PRESERVE_STATE", unique = false, nullable = true, length = 5)
	private String preserveState;
	
	/** 保全情况说明 **/
	@Column(name = "PRESERVE_CASE_DESC", unique = false, nullable = true, length = 65535)
	private String preserveCaseDesc;
	
	/** 有无影响该笔授信偿还的不利因素 **/
	@Column(name = "IS_NEG_FACTOR", unique = false, nullable = true, length = 5)
	private String isNegFactor;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param isNormalAftLmt
	 */
	public void setIsNormalAftLmt(String isNormalAftLmt) {
		this.isNormalAftLmt = isNormalAftLmt;
	}
	
    /**
     * @return isNormalAftLmt
     */
	public String getIsNormalAftLmt() {
		return this.isNormalAftLmt;
	}
	
	/**
	 * @param factLoanUs
	 */
	public void setFactLoanUs(String factLoanUs) {
		this.factLoanUs = factLoanUs;
	}
	
    /**
     * @return factLoanUs
     */
	public String getFactLoanUs() {
		return this.factLoanUs;
	}
	
	/**
	 * @param isRecLoan
	 */
	public void setIsRecLoan(String isRecLoan) {
		this.isRecLoan = isRecLoan;
	}
	
    /**
     * @return isRecLoan
     */
	public String getIsRecLoan() {
		return this.isRecLoan;
	}
	
	/**
	 * @param isExt
	 */
	public void setIsExt(String isExt) {
		this.isExt = isExt;
	}
	
    /**
     * @return isExt
     */
	public String getIsExt() {
		return this.isExt;
	}
	
	/**
	 * @param isRefinance
	 */
	public void setIsRefinance(String isRefinance) {
		this.isRefinance = isRefinance;
	}
	
    /**
     * @return isRefinance
     */
	public String getIsRefinance() {
		return this.isRefinance;
	}
	
	/**
	 * @param preserveState
	 */
	public void setPreserveState(String preserveState) {
		this.preserveState = preserveState;
	}
	
    /**
     * @return preserveState
     */
	public String getPreserveState() {
		return this.preserveState;
	}
	
	/**
	 * @param preserveCaseDesc
	 */
	public void setPreserveCaseDesc(String preserveCaseDesc) {
		this.preserveCaseDesc = preserveCaseDesc;
	}
	
    /**
     * @return preserveCaseDesc
     */
	public String getPreserveCaseDesc() {
		return this.preserveCaseDesc;
	}
	
	/**
	 * @param isNegFactor
	 */
	public void setIsNegFactor(String isNegFactor) {
		this.isNegFactor = isNegFactor;
	}
	
    /**
     * @return isNegFactor
     */
	public String getIsNegFactor() {
		return this.isNegFactor;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}