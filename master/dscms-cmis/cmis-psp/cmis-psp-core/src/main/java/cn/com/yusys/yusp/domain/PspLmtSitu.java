/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspLmtSitu
 * @类描述: psp_lmt_situ数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_lmt_situ")
public class PspLmtSitu extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 授信分项流水号 **/
	@Column(name = "SUB_LMT_NO", unique = false, nullable = true, length = 40)
	private String subLmtNo;
	
	/** 授信分项额度名称 **/
	@Column(name = "SUB_LMT_NAME", unique = false, nullable = true, length = 80)
	private String subLmtName;
	
	/** 授信额度 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 授信余额 **/
	@Column(name = "LMT_BALANCE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtBalanceAmt;
	
	/** 已用额度 **/
	@Column(name = "OUTSTND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outstndAmt;
	
	/** 可用额度 **/
	@Column(name = "AVL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal avlLmtAmt;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 是否循环 **/
	@Column(name = "IS_REVOLV", unique = false, nullable = true, length = 5)
	private String isRevolv;
	
	/** 额度到期日 **/
	@Column(name = "LMT_END_DATE", unique = false, nullable = true, length = 20)
	private String lmtEndDate;
	
	/** 业务大类 **/
	@Column(name = "PRD_CATALOG", unique = false, nullable = true, length = 5)
	private String prdCatalog;
	
	/** 存量类型 **/
	@Column(name = "BUSI_TYPE", unique = false, nullable = true, length = 5)
	private String busiType;
	
	/** 授信类型 **/
	@Column(name = "LMT_TYPE", unique = false, nullable = true, length = 5)
	private String lmtType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param subLmtNo
	 */
	public void setSubLmtNo(String subLmtNo) {
		this.subLmtNo = subLmtNo;
	}
	
    /**
     * @return subLmtNo
     */
	public String getSubLmtNo() {
		return this.subLmtNo;
	}
	
	/**
	 * @param subLmtName
	 */
	public void setSubLmtName(String subLmtName) {
		this.subLmtName = subLmtName;
	}
	
    /**
     * @return subLmtName
     */
	public String getSubLmtName() {
		return this.subLmtName;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param lmtBalanceAmt
	 */
	public void setLmtBalanceAmt(java.math.BigDecimal lmtBalanceAmt) {
		this.lmtBalanceAmt = lmtBalanceAmt;
	}
	
    /**
     * @return lmtBalanceAmt
     */
	public java.math.BigDecimal getLmtBalanceAmt() {
		return this.lmtBalanceAmt;
	}
	
	/**
	 * @param outstndAmt
	 */
	public void setOutstndAmt(java.math.BigDecimal outstndAmt) {
		this.outstndAmt = outstndAmt;
	}
	
    /**
     * @return outstndAmt
     */
	public java.math.BigDecimal getOutstndAmt() {
		return this.outstndAmt;
	}
	
	/**
	 * @param avlLmtAmt
	 */
	public void setAvlLmtAmt(java.math.BigDecimal avlLmtAmt) {
		this.avlLmtAmt = avlLmtAmt;
	}
	
    /**
     * @return avlLmtAmt
     */
	public java.math.BigDecimal getAvlLmtAmt() {
		return this.avlLmtAmt;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv;
	}
	
    /**
     * @return isRevolv
     */
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate;
	}
	
    /**
     * @return lmtEndDate
     */
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param prdCatalog
	 */
	public void setPrdCatalog(String prdCatalog) {
		this.prdCatalog = prdCatalog;
	}
	
    /**
     * @return prdCatalog
     */
	public String getPrdCatalog() {
		return this.prdCatalog;
	}
	
	/**
	 * @param busiType
	 */
	public void setBusiType(String busiType) {
		this.busiType = busiType;
	}
	
    /**
     * @return busiType
     */
	public String getBusiType() {
		return this.busiType;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}
	
    /**
     * @return lmtType
     */
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}