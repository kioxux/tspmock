/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWarningInfoList
 * @类描述: psp_warning_info_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_warning_info_list")
public class PspWarningInfoList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 预警单编号 **/
	@Column(name = "ALT_SIN_NO", unique = false, nullable = true, length = 40)
	private String altSinNo;
	
	/** 预警日期 **/
	@Column(name = "ALT_DATE", unique = false, nullable = true, length = 20)
	private String altDate;
	
	/** 信息来源 **/
	@Column(name = "INFO_SOURCE", unique = false, nullable = true, length = 5)
	private String infoSource;
	
	/** 预警大类 **/
	@Column(name = "ALT_TYPE_MAX", unique = false, nullable = true, length = 5)
	private String altTypeMax;
	
	/** 预警种类 **/
	@Column(name = "ALT_TYPE", unique = false, nullable = true, length = 5)
	private String altType;
	
	/** 预警子项 **/
	@Column(name = "ALT_SUB_TYPE", unique = false, nullable = true, length = 80)
	private String altSubType;
	
	/** 预警输出描述 **/
	@Column(name = "ALT_DESC", unique = false, nullable = true, length = 500)
	private String altDesc;
	
	/** 指标等级 **/
	@Column(name = "ALT_LVL", unique = false, nullable = true, length = 5)
	private String altLvl;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param altSinNo
	 */
	public void setAltSinNo(String altSinNo) {
		this.altSinNo = altSinNo;
	}
	
    /**
     * @return altSinNo
     */
	public String getAltSinNo() {
		return this.altSinNo;
	}
	
	/**
	 * @param altDate
	 */
	public void setAltDate(String altDate) {
		this.altDate = altDate;
	}
	
    /**
     * @return altDate
     */
	public String getAltDate() {
		return this.altDate;
	}
	
	/**
	 * @param infoSource
	 */
	public void setInfoSource(String infoSource) {
		this.infoSource = infoSource;
	}
	
    /**
     * @return infoSource
     */
	public String getInfoSource() {
		return this.infoSource;
	}
	
	/**
	 * @param altTypeMax
	 */
	public void setAltTypeMax(String altTypeMax) {
		this.altTypeMax = altTypeMax;
	}
	
    /**
     * @return altTypeMax
     */
	public String getAltTypeMax() {
		return this.altTypeMax;
	}
	
	/**
	 * @param altType
	 */
	public void setAltType(String altType) {
		this.altType = altType;
	}
	
    /**
     * @return altType
     */
	public String getAltType() {
		return this.altType;
	}
	
	/**
	 * @param altSubType
	 */
	public void setAltSubType(String altSubType) {
		this.altSubType = altSubType;
	}
	
    /**
     * @return altSubType
     */
	public String getAltSubType() {
		return this.altSubType;
	}
	
	/**
	 * @param altDesc
	 */
	public void setAltDesc(String altDesc) {
		this.altDesc = altDesc;
	}
	
    /**
     * @return altDesc
     */
	public String getAltDesc() {
		return this.altDesc;
	}
	
	/**
	 * @param altLvl
	 */
	public void setAltLvl(String altLvl) {
		this.altLvl = altLvl;
	}
	
    /**
     * @return altLvl
     */
	public String getAltLvl() {
		return this.altLvl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}