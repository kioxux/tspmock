package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.RiskClassChgApp;
import cn.com.yusys.yusp.domain.RiskTaskList;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.RiskClassChgAppService;
import cn.com.yusys.yusp.service.RiskTaskListService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class DHCZ25BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DHCZ25BizService.class);

    @Autowired
    private RiskTaskListService riskTaskListService;
    @Autowired
    private RiskClassChgAppService riskClassChgAppService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String bizType = resultInstanceDto.getBizType();
        if ("DHK22".equals(bizType) || "DHK23".equals(bizType) || "DHK24".equals(bizType)) {
            // 公司客户风险分类（东海）、个人经营性风险分类（东海）、个人消费性风险分类（东海）
            handlerTask(resultInstanceDto);
        } else if ("DHK25".equals(bizType)) {
            // 风险分类调整申请（东海）
            handlerAdjust(resultInstanceDto);
        } else {
            log.info("[{}]流程业务类型为空", CmisFlowConstants.DHCZ25);
        }
    }

    /**
     * 公司客户风险分类（东海）
     * 人经营性风险分类（东海）
     * 个人消费性风险分类（东海）
     *
     * @author jijian_yx
     * @date 2021/8/31 0:49
     **/
    private void handlerTask(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        log.info("风险分类（东海）后业务处理类型:" + currentOpType);
        RiskTaskList riskTaskList = new RiskTaskList();
        riskTaskList.setTaskNo(serno);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("风险分类（东海）发起操作:" + resultInstanceDto);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + resultInstanceDto);
                riskTaskList.setApproveStatus("111");
                riskTaskListService.updateByTaskNoSelective(riskTaskList);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("风险分类（东海）跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                //审批通过
                log.info("风险分类（东海）结束操作:" + resultInstanceDto);
                // 更新台账五级/十级分类结果
                riskTaskListService.updateAccLoanFiveClass(riskTaskList);
                // 更新审批状态通过 997
                riskTaskList.setApproveStatus("997");
                riskTaskList.setCheckStatus("3");
                riskTaskList.setCheckDate(openDay);
                riskTaskListService.updateByTaskNoSelective(riskTaskList);
                log.info("风险分类（东海）结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("风险分类（东海）退回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                riskTaskList.setApproveStatus("992");
                riskTaskListService.updateByTaskNoSelective(riskTaskList);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("风险分类（东海）打回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                riskTaskList.setApproveStatus("992");
                riskTaskListService.updateByTaskNoSelective(riskTaskList);
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回状态                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        态改为追回 991
                log.info("风险分类（东海）拿回操作:" + resultInstanceDto);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("风险分类（东海）否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                riskTaskList.setApproveStatus("998");
                riskTaskListService.updateByTaskNoSelective(riskTaskList);
                log.info("风险分类（东海）否决操作结束:" + resultInstanceDto);
            } else {
                log.warn("风险分类（东海）未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("风险分类（东海）后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("风险分类（东海）发送异常消息失败", e1);
            }
        }

    }

    /**
     * 风险分类调整申请（东海）
     *
     * @author jijian_yx
     * @date 2021/8/31 0:50
     **/
    private void handlerAdjust(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("风险分类调整（东海）后业务处理类型:" + currentOpType);
        RiskClassChgApp riskClassChgApp = new RiskClassChgApp();
        riskClassChgApp.setSerno(serno);
        try {

            if (OpType.STRAT.equals(currentOpType)) {
                log.info("风险分类调整（东海）发起操作:" + resultInstanceDto);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + resultInstanceDto);
                riskClassChgApp.setApproveStatus("111");
                riskClassChgAppService.updateBySernoSelective(riskClassChgApp);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("风险分类调整（东海）跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                //审批通过
                log.info("风险分类调整（东海）结束操作:" + resultInstanceDto);
                // 更新台账五级/十级分类结果
                riskClassChgAppService.updateAccLoanFiveClass(riskClassChgApp);
                // 更新审批状态通过 997
                riskClassChgApp.setApproveStatus("997");
                riskClassChgAppService.updateBySernoSelective(riskClassChgApp);
                log.info("风险分类调整（东海）结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("风险分类调整（东海）退回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                riskClassChgApp.setApproveStatus("992");
                riskClassChgAppService.updateBySernoSelective(riskClassChgApp);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("风险分类调整（东海）打回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                riskClassChgApp.setApproveStatus("992");
                riskClassChgAppService.updateBySernoSelective(riskClassChgApp);
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回状态                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            态改为追回 991
                log.info("风险分类调整（东海）拿回操作:" + resultInstanceDto);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("风险分类调整（东海）否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                riskClassChgApp.setApproveStatus("998");
                riskClassChgAppService.updateBySernoSelective(riskClassChgApp);
                log.info("风险分类调整（东海）否决操作结束:" + resultInstanceDto);
            } else {
                log.warn("风险分类调整（东海）未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("风险分类调整（东海）后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("风险分类调整（东海）发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DHCZ25.equals(flowCode);
    }
}
