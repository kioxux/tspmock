package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspTaskList;
import cn.com.yusys.yusp.dto.DocAccSearchDto;
import cn.com.yusys.yusp.dto.PspKHYJDto;
import cn.com.yusys.yusp.dto.PspTaskAndRstDto;
import cn.com.yusys.yusp.service.PspTaskListService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskListResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: likc
 * @创建时间: 2021-06-11 14:17:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/psptasklist")
public class PspTaskListResource {

    @Autowired
    private PspTaskListService pspTaskListService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspTaskList>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspTaskList> list = pspTaskListService.selectAll(queryModel);
        return new ResultDto<List<PspTaskList>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:getPspTaskList
     * @函数描述:查询待检查任务列表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getPspTaskList")
    protected ResultDto<List<PspTaskList>> getPspTaskList(@RequestBody QueryModel queryModel) throws Exception {
        List<PspTaskList> list = pspTaskListService.getPspTaskList(queryModel);
        return new ResultDto<List<PspTaskList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspTaskList> show(@PathVariable("pkId") String pkId) {
        PspTaskList pspTaskList = pspTaskListService.selectByPrimaryKey(pkId);
        return new ResultDto<PspTaskList>(pspTaskList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<PspTaskList> create(@RequestBody PspTaskList pspTaskList) {
        pspTaskList.setCreateTime(new Date());
        pspTaskListService.insert(pspTaskList);
        return new ResultDto<PspTaskList>(pspTaskList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspTaskList pspTaskList) {
        pspTaskList.setUpdateTime(new Date());
        int result = pspTaskListService.update(pspTaskList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updatebycondition
     * @函数描述:用于更新检查状态和审批状态
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatebycondition")
    protected ResultDto<Integer> updateSelective(@RequestBody PspTaskList pspTaskList) {
        pspTaskList.setUpdateTime(new Date());
        int result = pspTaskListService.updateSelective(pspTaskList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getSequences
     * @函数描述:用于获取序列号
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getSequences")
    protected ResultDto<String> getSequences() {
        String taskNo = pspTaskListService.getSequences();
        return new ResultDto<String>(taskNo);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspTaskListService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspTaskListService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateTaskAndRst
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateTaskAndRst")
    protected ResultDto<Integer> updateTaskAndRst(@RequestBody PspTaskAndRstDto pspTaskAndRstDto) throws URISyntaxException {
        int result = pspTaskListService.updateTaskAndRst(pspTaskAndRstDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateTaskAndRst
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectTaskAndRst")
    protected ResultDto<PspTaskAndRstDto> selectTaskAndRst(@RequestBody PspTaskAndRstDto pspTaskAndRstDto) throws URISyntaxException {
        pspTaskAndRstDto = pspTaskListService.selectTaskAndRst(pspTaskAndRstDto);
        return new ResultDto<PspTaskAndRstDto>(pspTaskAndRstDto);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectDetailById")
    protected ResultDto<PspTaskList> selectDetailById(@RequestBody String pkId) {
        PspTaskList pspTaskList = pspTaskListService.selectByPrimaryKey(pkId);
        return new ResultDto<PspTaskList>(pspTaskList);
    }

    /**
     * @函数名称:selectNumByInputId
     * @函数描述:根据客户经理工号查询待处理数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据客户经理工号查询待处理数据")
    @PostMapping("/selectNumByInputId")
    protected ResultDto<List<Map<String, Object>>> selectNumByInputId(@RequestBody QueryModel queryModel) {
        List<Map<String, Object>> list = pspTaskListService.selectNumByInputId(queryModel);
        return new ResultDto<List<Map<String, Object>>>(list);
    }

    /**
     * @函数名称:delete
     * @函数描述:删除检查，并且删除其子表信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByTaskNo")
    protected ResultDto<Integer> deleteByTaskNo(@RequestBody PspTaskList pspTaskList) {
        int result = pspTaskListService.deleteByTaskNo(pspTaskList);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:cmisBatch0006Service
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/cmisBatch0006Service")
    protected ResultDto<Integer> cmisBatch0006Service(@RequestBody PspTaskAndRstDto pspTaskAndRstDto) throws URISyntaxException {
        int result = pspTaskListService.cmisBatch0006Services(pspTaskAndRstDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * 贷后征信查询
     * @param taskNo
     * @return
     * @throws Exception
     */
    @PostMapping("/createcreditcus")
    protected ResultDto<Integer> createCreditCus(@RequestBody String taskNo) throws Exception {
        ResultDto<Integer> result = pspTaskListService.createCreditCus(taskNo);
        return result;
    }

    /**
     * 根据客户号获取年份集合
     * @author jijian_yx
     * @date 2021/9/27 15:36
     **/
    @PostMapping("/getacctimelist")
    protected ResultDto<List<String>> getAccTimeList(@RequestBody String cusId){
        List<String> result = pspTaskListService.getAccTimeList(cusId);
        return new ResultDto<>(result);
    }

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     * @author jijian_yx
     * @date 2021/9/28 0:05
     **/
    @PostMapping("/queryacclistbydoctypeandyear")
    protected ResultDto<List<DocAccSearchDto>> queryAccListByDocTypeAndYear(@RequestBody QueryModel queryModel){
        List<DocAccSearchDto> result = pspTaskListService.queryAccListByDocTypeAndYear(queryModel);
        return new ResultDto<>(result);
    }

    /**
     * 根据传入的pspTaskList对象，更新指定对象的属性
     * @param paramMap
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/updatePspTaskListInfo")
    protected ResultDto<Boolean> updatePspTaskListInfo(@RequestBody Map<String,String> paramMap)  {
        boolean insertFlag  = pspTaskListService.updatePspTaskList(paramMap);
        return new ResultDto<>(insertFlag);
    }
    /**
     * @param queryModel 分页查询类
     * @函数名称:getPspTaskList
     * @函数描述:查询待检查任务列表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryPspTaskList")
    protected ResultDto<List<PspTaskAndRstDto>> queryPspTaskList(@RequestBody QueryModel queryModel) throws Exception {
        List<PspTaskAndRstDto> list = pspTaskListService.queryPspTaskList(queryModel);
        return new ResultDto<List<PspTaskAndRstDto>>(list);
    }

    /**
     * @param pspKHYJDto 分页查询类
     * @函数名称:getPspTaskList
     * @函数描述:查询待检查任务列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "客户移交贷后在途任务判断")
    @PostMapping("/querypsptasklist")
    protected ResultDto<String> queryCountBycusId(@RequestBody PspKHYJDto pspKHYJDto) throws Exception {
        ResultDto<String> resultDto = pspTaskListService.queryCountBycusId(pspKHYJDto);
        return resultDto;
    }
}
