/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskList
 * @类描述: psp_task_list数据实体类
 * @功能描述: 
 * @创建人: hanl
 * @创建时间: 2021-06-13 19:39:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_wyd_task_list")
public class PspWydTaskList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 任务类型 **/
	@Column(name = "TASK_TYPE", unique = false, nullable = true, length = 5)
	private String taskType;
	
	/** 检查类型 **/
	@Column(name = "CHECK_TYPE", unique = false, nullable = true, length = 5)
	private String checkType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 企业联系电话 **/
	@Column(name = "PHONE_NO", unique = false, nullable = true, length = 20)
	private String phoneNo;

	/** 企业联系地址 **/
	@Column(name = "ADDRESS", unique = false, nullable = true, length = 500)
	private String address;
	
	/** 任务生成日期 **/
	@Column(name = "TASK_START_DT", unique = false, nullable = true, length = 20)
	private String taskStartDt;
	
	/** 任务要求完成日期 **/
	@Column(name = "TASK_END_DT", unique = false, nullable = true, length = 20)
	private String taskEndDt;
	
	/** 任务执行人 **/
	@Column(name = "EXEC_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName",refFieldName = "execIdName")
	private String execId;
	
	/** 任务执行机构 **/
	@Column(name = "EXEC_BR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName = "execBrIdName")
	private String execBrId;
	
	/** 检查状态 **/
	@Column(name = "CHECK_STATUS", unique = false, nullable = true, length = 5)
	private String checkStatus;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 报告类型 **/
	@Column(name = "RPT_TYPE", unique = false, nullable = true, length = 5)
	private String rptType;
	
	/** 检查日期 **/
	@Column(name = "CHECK_DATE", unique = false, nullable = true, length = 20)
	private String checkDate;
	
	/** 借据起始日 **/
	@Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
	private String loanStartDate;
	
	/** 借据到期日 **/
	@Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
	private String loanEndDate;
	
	/** 风险等级 **/
	@Column(name = "RISK_LVL", unique = false, nullable = true, length = 5)
	private String riskLvl;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 任务派发人员 **/
	@Column(name = "ISSUE_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName",refFieldName = "issueIdName")
	private String issueId;
	
	/** 任务派发人员所属机构 **/
	@Column(name = "ISSUE_BR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName = "issueBrIdName")
	private String issueBrId;
	
	/** 任务下发日期 **/
	@Column(name = "ISSUE_DATE", unique = false, nullable = true, length = 20)
	private String issueDate;
	
	/** 贷款金额（元）  **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;

	/** 申请延期完成日期 **/
	@Column(name = "DELAY_DATE", unique = false, nullable = true, length = 20)
	private String delayDate;

	/** 微众银行客户号 **/
	@Column(name = "WEBANK_ID", unique = false, nullable = true, length = 100)
	private String webankId;
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 20)
	private Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 20)
	private Date updateTime;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	
    /**
     * @return taskType
     */
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param checkType
	 */
	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}
	
    /**
     * @return checkType
     */
	public String getCheckType() {
		return this.checkType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param phoneNo
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * @return phoneNo
	 */
	public String getPhoneNo() {
		return this.phoneNo;
	}

	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return address
	 */
	public String getAddress() {
		return this.address;
	}
	
	/**
	 * @param taskStartDt
	 */
	public void setTaskStartDt(String taskStartDt) {
		this.taskStartDt = taskStartDt;
	}
	
    /**
     * @return taskStartDt
     */
	public String getTaskStartDt() {
		return this.taskStartDt;
	}
	
	/**
	 * @param taskEndDt
	 */
	public void setTaskEndDt(String taskEndDt) {
		this.taskEndDt = taskEndDt;
	}
	
    /**
     * @return taskEndDt
     */
	public String getTaskEndDt() {
		return this.taskEndDt;
	}

	/**
	 * @param delayDate
	 */
	public void setDelayDate(String delayDate) {
		this.delayDate = delayDate;
	}

	/**
	 * @return phoneNo
	 */
	public String getDelayDate() {
		return this.delayDate;
	}

	/**
	 * @param webankId
	 */
	public void setWebankId(String webankId) {
		this.webankId = webankId;
	}

	/**
	 * @return webankId
	 */
	public String getWebankId() {
		return this.webankId;
	}
	
	/**
	 * @param execId
	 */
	public void setExecId(String execId) {
		this.execId = execId;
	}
	
    /**
     * @return execId
     */
	public String getExecId() {
		return this.execId;
	}
	
	/**
	 * @param execBrId
	 */
	public void setExecBrId(String execBrId) {
		this.execBrId = execBrId;
	}
	
    /**
     * @return execBrId
     */
	public String getExecBrId() {
		return this.execBrId;
	}
	
	/**
	 * @param checkStatus
	 */
	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}
	
    /**
     * @return checkStatus
     */
	public String getCheckStatus() {
		return this.checkStatus;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param rptType
	 */
	public void setRptType(String rptType) {
		this.rptType = rptType;
	}
	
    /**
     * @return rptType
     */
	public String getRptType() {
		return this.rptType;
	}
	
	/**
	 * @param checkDate
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	
    /**
     * @return checkDate
     */
	public String getCheckDate() {
		return this.checkDate;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}
	
    /**
     * @return loanStartDate
     */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}
	
    /**
     * @return loanEndDate
     */
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param riskLvl
	 */
	public void setRiskLvl(String riskLvl) {
		this.riskLvl = riskLvl;
	}
	
    /**
     * @return riskLvl
     */
	public String getRiskLvl() {
		return this.riskLvl;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param issueId
	 */
	public void setIssueId(String issueId) {
		this.issueId = issueId;
	}
	
    /**
     * @return issueId
     */
	public String getIssueId() {
		return this.issueId;
	}
	
	/**
	 * @param issueBrId
	 */
	public void setIssueBrId(String issueBrId) {
		this.issueBrId = issueBrId;
	}
	
    /**
     * @return issueBrId
     */
	public String getIssueBrId() {
		return this.issueBrId;
	}
	
	/**
	 * @param issueDate
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	
    /**
     * @return issueDate
     */
	public String getIssueDate() {
		return this.issueDate;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	public String getInputId() {
		return inputId;
	}

	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	public String getInputBrId() {
		return inputBrId;
	}

	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getUpdId() {
		return updId;
	}

	public void setUpdId(String updId) {
		this.updId = updId;
	}

	public String getUpdBrId() {
		return updBrId;
	}

	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
}