package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.PspWydTaskBack;
import cn.com.yusys.yusp.domain.PspWydTaskList;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByRoleCodeDto;
import cn.com.yusys.yusp.dto.client.esb.gcyxpt.yxgc01.Yxgc01ReqDto;
import cn.com.yusys.yusp.flow.ClientCons;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFException;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.Dscms2GcyxptClientService;
import cn.com.yusys.yusp.service.PspWydTaskBackService;
import cn.com.yusys.yusp.service.PspWydTaskListService;
import cn.com.yusys.yusp.service.AdminSmRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 个人消费性首次贷后检查流程
 */
@Service
public class DHGL21BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DHGL21BizService.class);

    @Autowired
    private PspWydTaskBackService pspWydTaskBackService;
    @Autowired
    private PspWydTaskListService pspWydTaskListService;
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private AdminSmRoleService adminSmRoleService;
    @Autowired
    private Dscms2GcyxptClientService dscms2GcyxptClientService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String bizType = resultInstanceDto.getBizType();
        log.info("{}进入流程处理",bizType);
        dealBizOp(resultInstanceDto);
    }

    private void dealBizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String pkId = resultInstanceDto.getBizId();
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        PspWydTaskBack pspWydTaskBack =pspWydTaskBackService.selectByPrimaryKey(pkId);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + resultInstanceDto);
                pspWydTaskBack.setApproveStatus("111");
                pspWydTaskBackService.updateSelective(pspWydTaskBack);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                //审批通过
                log.info("结束操作:" + resultInstanceDto);
                pspWydTaskBack.setApproveStatus("997");
                pspWydTaskBackService.updateSelective(pspWydTaskBack);
                PspWydTaskList pspWydTaskList =pspWydTaskListService.selectByTaskNo(pspWydTaskBack.getTaskNo());
                //  4.根据角色1128取中台审核人员，选取规则为当天，当前业务类型数量最少的人，相同则随机
                GetUserInfoByRoleCodeDto userInfoByRoleCodeDto = new GetUserInfoByRoleCodeDto();
                userInfoByRoleCodeDto.setRoleCode("R1045");
                userInfoByRoleCodeDto.setPageNum(1);
                userInfoByRoleCodeDto.setPageSize(100);
                ResultDto<List<AdminSmUserDto>> resultDto = adminSmRoleService.getuserlist(userInfoByRoleCodeDto);
                if (CollectionUtils.nonNull(resultDto)) {
                    List<AdminSmUserDto> adminSmUserDtos = resultDto.getData();
                    if(adminSmUserDtos.get(0) instanceof AdminSmUserDto) {
                        pspWydTaskList.setExecId(adminSmUserDtos.get(0).getUserCode());
                        pspWydTaskList.setExecBrId(adminSmUserDtos.get(0).getOrgId());
                    }else {
                        //UserAndOrgNameAspect.listTrans()会将CusIndivDto转换成LinkedHashMap
                        pspWydTaskList.setExecId((String)((Map)resultDto.getData().get(0)).get("userCode"));
                        pspWydTaskList.setExecBrId((String)((Map)resultDto.getData().get(0)).get("orgId"));
                    }
                }
                pspWydTaskList.setIssueId("");
                pspWydTaskList.setIssueBrId("");
                pspWydTaskList.setInputDate("");
                pspWydTaskList.setApproveStatus("992");//退回流程通过后贷后任务退回
                pspWydTaskListService.updateSelective(pspWydTaskList);
                //通知过程营销平台退回
                try {
                    Yxgc01ReqDto yxgc01ReqDto=new Yxgc01ReqDto();
                    yxgc01ReqDto.setSerno(pspWydTaskList.getTaskNo());
                    yxgc01ReqDto.setCus_id(pspWydTaskList.getCusId());
                    yxgc01ReqDto.setCus_name("");
                    yxgc01ReqDto.setPhone("");
                    yxgc01ReqDto.setAddress("");
                    yxgc01ReqDto.setCus_mng_id("");
                    yxgc01ReqDto.setCus_mng_name("");
                    yxgc01ReqDto.setMng_br_id("");
                    yxgc01ReqDto.setMng_br_name("");
                    yxgc01ReqDto.setCrt_date("");
                    yxgc01ReqDto.setRqr_fin_date("");
                    yxgc01ReqDto.setPsp_state("7"); //7-微业贷退回
                    dscms2GcyxptClientService.yxgc01(yxgc01ReqDto);
                } catch (Exception e) {
                    throw new Exception("发送过程营销平台异常");
                }
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                pspWydTaskBack.setApproveStatus("992");
                pspWydTaskBackService.updateSelective(pspWydTaskBack);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111 -> 打回 992
                pspWydTaskBack.setApproveStatus("992");
                pspWydTaskBackService.updateSelective(pspWydTaskBack);
            }else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             态改为追回 991
                log.info("拿回操作:" + resultInstanceDto);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                pspWydTaskBack.setApproveStatus("998");
                pspWydTaskBackService.updateSelective(pspWydTaskBack);
                log.info("否决操作结束:" + resultInstanceDto);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }

        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                WFException exception = new WFException();
                exception.setBizId(resultInstanceDto.getBizId());
                exception.setBizType(resultInstanceDto.getBizType());
                exception.setFlowName(resultInstanceDto.getFlowName());
                exception.setInstanceId(resultInstanceDto.getInstanceId());
                exception.setNodeId(resultInstanceDto.getNodeId());
                exception.setNodeName(resultInstanceDto.getNodeName());
                exception.setUserId(resultInstanceDto.getCurrentUserId());
                exception.setOpType(currentOpType);
                // 后业务处理失败时，将异常信息保存到异常表中
                amqpTemplate.convertAndSend(ClientCons.queue_exception, exception);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DHGL21.equals(flowCode);
    }

}
