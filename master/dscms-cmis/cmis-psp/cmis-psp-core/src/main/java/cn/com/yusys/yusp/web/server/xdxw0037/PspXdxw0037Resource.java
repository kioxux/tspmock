package cn.com.yusys.yusp.web.server.xdxw0037;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0037.req.Xdxw0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.resp.Xdxw0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0037.Xdxw0037Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询借款人是否存在未完成的贷后检查任务
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0037:查询借款人是否存在未完成的贷后检查任务")
@RestController
@RequestMapping("/api/pspqt4bsp")
public class PspXdxw0037Resource {
    private static final Logger logger = LoggerFactory.getLogger(PspXdxw0037Resource.class);

    @Autowired
    private Xdxw0037Service xdxw0037Service;
    /**
     * 交易码：xdxw0037
     * 交易描述：查询借款人是否存在未完成的贷后检查任务
     *
     * @param xdxw0037DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询借款人是否存在未完成的贷后检查任务")
    @PostMapping("/xdxw0037")
    protected @ResponseBody
    ResultDto<Xdxw0037DataRespDto> xdxw0037(@Validated @RequestBody Xdxw0037DataReqDto xdxw0037DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, JSON.toJSONString(xdxw0037DataReqDto));
        Xdxw0037DataRespDto xdxw0037DataRespDto = new Xdxw0037DataRespDto();// 响应Dto:查询借款人是否存在未完成的贷后检查任务
        ResultDto<Xdxw0037DataRespDto> xdxw0037DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0037DataReqDto获取业务值进行业务逻辑处理
            String cusId = xdxw0037DataReqDto.getCusId();//客户号
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdxw0037DataReqDto));
            xdxw0037DataRespDto = xdxw0037Service.xdxw0037(xdxw0037DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdxw0037DataReqDto));
            // 封装xdxw0037DataResultDto中正确的返回码和返回信息
            xdxw0037DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0037DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, e.getMessage());
            // 封装xdxw0037DataResultDto中异常返回码和返回信息
            xdxw0037DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0037DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0037DataRespDto到xdxw0037DataResultDto中
        xdxw0037DataResultDto.setData(xdxw0037DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, JSON.toJSONString(xdxw0037DataResultDto));
        return xdxw0037DataResultDto;
    }
}
