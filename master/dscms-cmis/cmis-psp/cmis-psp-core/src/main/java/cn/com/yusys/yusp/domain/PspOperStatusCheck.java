/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheck
 * @类描述: psp_oper_status_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_oper_status_check")
public class PspOperStatusCheck extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 是否对经营场所现场检查 **/
	@Column(name = "IS_PHOTOGRAPH", unique = false, nullable = true, length = 5)
	private String isPhotograph;
	
	/** 确认说明 **/
	@Column(name = "NOT_PHOTOGRAPH_REMARK", unique = false, nullable = true, length = 65535)
	private String notPhotographRemark;
	
	/** 目前借款人经营是否正常 **/
	@Column(name = "IS_OPER_NORMAL", unique = false, nullable = true, length = 5)
	private String isOperNormal;
	
	/** 经营异常说明 **/
	@Column(name = "NOT_OPER_NORMAL_REMARK", unique = false, nullable = true, length = 65535)
	private String notOperNormalRemark;
	
	/** 是否有其他重要风险事项 **/
	@Column(name = "IS_OTHER_REMARK", unique = false, nullable = true, length = 5)
	private String isOtherRemark;
	
	/** 其他风险说明 **/
	@Column(name = "OTHER_REMARK", unique = false, nullable = true, length = 65535)
	private String otherRemark;
	
	/** 借款人具体经营内容 **/
	@Column(name = "OPER_CONTENT", unique = false, nullable = true, length = 65535)
	private String operContent;
	
	/** 借款人身体是否健康 **/
	@Column(name = "IS_HEALTH", unique = false, nullable = true, length = 5)
	private String isHealth;
	
	/** 借款人身体状况说明 **/
	@Column(name = "NOT_HEALTH_REMARK", unique = false, nullable = true, length = 65535)
	private String notHealthRemark;
	
	/** 上期借款人环评颜色 **/
	@Column(name = "PRE_EVLU_RST", unique = false, nullable = true, length = 5)
	private String preEvluRst;
	
	/** 本期借款人环评颜色 **/
	@Column(name = "CURT_EVLU_RST", unique = false, nullable = true, length = 5)
	private String curtEvluRst;
	
	/** 环评变化说明 **/
	@Column(name = "EVLU_CHANGE_REMARK", unique = false, nullable = true, length = 65535)
	private String evluChangeRemark;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isPhotograph
	 */
	public void setIsPhotograph(String isPhotograph) {
		this.isPhotograph = isPhotograph;
	}
	
    /**
     * @return isPhotograph
     */
	public String getIsPhotograph() {
		return this.isPhotograph;
	}
	
	/**
	 * @param notPhotographRemark
	 */
	public void setNotPhotographRemark(String notPhotographRemark) {
		this.notPhotographRemark = notPhotographRemark;
	}
	
    /**
     * @return notPhotographRemark
     */
	public String getNotPhotographRemark() {
		return this.notPhotographRemark;
	}
	
	/**
	 * @param isOperNormal
	 */
	public void setIsOperNormal(String isOperNormal) {
		this.isOperNormal = isOperNormal;
	}
	
    /**
     * @return isOperNormal
     */
	public String getIsOperNormal() {
		return this.isOperNormal;
	}
	
	/**
	 * @param notOperNormalRemark
	 */
	public void setNotOperNormalRemark(String notOperNormalRemark) {
		this.notOperNormalRemark = notOperNormalRemark;
	}
	
    /**
     * @return notOperNormalRemark
     */
	public String getNotOperNormalRemark() {
		return this.notOperNormalRemark;
	}
	
	/**
	 * @param isOtherRemark
	 */
	public void setIsOtherRemark(String isOtherRemark) {
		this.isOtherRemark = isOtherRemark;
	}
	
    /**
     * @return isOtherRemark
     */
	public String getIsOtherRemark() {
		return this.isOtherRemark;
	}
	
	/**
	 * @param otherRemark
	 */
	public void setOtherRemark(String otherRemark) {
		this.otherRemark = otherRemark;
	}
	
    /**
     * @return otherRemark
     */
	public String getOtherRemark() {
		return this.otherRemark;
	}
	
	/**
	 * @param operContent
	 */
	public void setOperContent(String operContent) {
		this.operContent = operContent;
	}
	
    /**
     * @return operContent
     */
	public String getOperContent() {
		return this.operContent;
	}
	
	/**
	 * @param isHealth
	 */
	public void setIsHealth(String isHealth) {
		this.isHealth = isHealth;
	}
	
    /**
     * @return isHealth
     */
	public String getIsHealth() {
		return this.isHealth;
	}
	
	/**
	 * @param notHealthRemark
	 */
	public void setNotHealthRemark(String notHealthRemark) {
		this.notHealthRemark = notHealthRemark;
	}
	
    /**
     * @return notHealthRemark
     */
	public String getNotHealthRemark() {
		return this.notHealthRemark;
	}
	
	/**
	 * @param preEvluRst
	 */
	public void setPreEvluRst(String preEvluRst) {
		this.preEvluRst = preEvluRst;
	}
	
    /**
     * @return preEvluRst
     */
	public String getPreEvluRst() {
		return this.preEvluRst;
	}
	
	/**
	 * @param curtEvluRst
	 */
	public void setCurtEvluRst(String curtEvluRst) {
		this.curtEvluRst = curtEvluRst;
	}
	
    /**
     * @return curtEvluRst
     */
	public String getCurtEvluRst() {
		return this.curtEvluRst;
	}
	
	/**
	 * @param evluChangeRemark
	 */
	public void setEvluChangeRemark(String evluChangeRemark) {
		this.evluChangeRemark = evluChangeRemark;
	}
	
    /**
     * @return evluChangeRemark
     */
	public String getEvluChangeRemark() {
		return this.evluChangeRemark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}