/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: YndDhyjRuleDetail
 * @类描述: ynd_dhyj_rule_detail数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 21:58:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "ynd_dhyj_rule_detail")
public class YndDhyjRuleDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 贷后预警流水号 **/
	@Column(name = "WARN_SERNO", unique = false, nullable = true, length = 40)
	private String warnSerno;
	
	/** 预警规则 **/
	@Column(name = "WARN_RULE", unique = false, nullable = true, length = 20)
	private String warnRule;
	
	/** 预警等级 **/
	@Column(name = "WARN_RESULT", unique = false, nullable = true, length = 5)
	private String warnResult;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param warnSerno
	 */
	public void setWarnSerno(String warnSerno) {
		this.warnSerno = warnSerno;
	}
	
    /**
     * @return warnSerno
     */
	public String getWarnSerno() {
		return this.warnSerno;
	}
	
	/**
	 * @param warnRule
	 */
	public void setWarnRule(String warnRule) {
		this.warnRule = warnRule;
	}
	
    /**
     * @return warnRule
     */
	public String getWarnRule() {
		return this.warnRule;
	}
	
	/**
	 * @param warnResult
	 */
	public void setWarnResult(String warnResult) {
		this.warnResult = warnResult;
	}
	
    /**
     * @return warnResult
     */
	public String getWarnResult() {
		return this.warnResult;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}