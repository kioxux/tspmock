/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.data.authority.annotation.IgnoredDataAuthority;
import cn.com.yusys.yusp.domain.PspTaskList;
import cn.com.yusys.yusp.dto.DocAccSearchDto;
import cn.com.yusys.yusp.dto.PspKHYJDto;
import cn.com.yusys.yusp.dto.PspTaskAndRstDto;
import org.apache.ibatis.annotations.Param;

import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskListMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: likc
 * @创建时间: 2021-06-11 14:17:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PspTaskListMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    PspTaskList selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: queryPspTaskList
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    PspTaskList queryPspTaskList(PspTaskList record);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<PspTaskList> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(PspTaskList record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(PspTaskList record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(PspTaskList record);

    /**
     * @方法名称: updateByTaskNo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByTaskNo(PspTaskList record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(PspTaskList record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectPspTaskNoListByCusId
     * @方法描述: 通过客户号查询未完成的贷后检查任务
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<java.util.Map<String,String>> selectPspTaskNoListByCusId(@Param("cusId")String cusId);

    /**
     * @方法名称: selectPspTaskNoListByManagerId
     * @方法描述: 通过客户经理号查询未完成的贷后检查任务
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<java.util.Map<String,String>> selectPspTaskNoListByManagerId(@Param("managerId")String managerId,@Param("openDay")String openDay);

    /**
     * @方法名称: selectByModel
     * @方法描述: 查询待检查任务列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PspTaskList> selectMhByModel(QueryModel model);

    /**
     * @函数名称:selectNumByInputId
     * @函数描述:根据客户经理工号查询待处理数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    List<Map<String, Object>> selectNumByInputId(QueryModel model);
    /**
     * @方法名称: deleteByTaskNo
     * @方法描述: 根据任务编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByTaskNo(@Param("taskNo") String taskNo);

    /**
     * 根据客户号获取年份集合
     * @author jijian_yx
     * @date 2021/9/27 16:29
     **/
    List<String> getAccTimeList(@Param("cusId") String cusId);

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     * @author jijian_yx
     * @date 2021/9/28 0:06
     **/
    List<DocAccSearchDto> queryAccListByDocTypeAndYear(QueryModel queryModel);

    /**
     * 根据传入的pspTaskList对象，更新指定对象的属性
     * @param pspTaskList
     * @return
     */
    int updatePspTaskList(Map<String, String> pspTaskList);


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据任务编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @IgnoredDataAuthority
    PspTaskList selectByTaskNo(@Param("taskNo") String taskNo);


    /**
     * @方法名称: selectByModel
     * @方法描述: 查询待检查任务列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PspTaskAndRstDto> selectPspTaskAndRst(QueryModel model);

    /**
     * 判断是否有在途任务
     * @param pspKHYJDto
     * @return
     */
    PspTaskList queryCountBycusId(PspKHYJDto pspKHYJDto);
}