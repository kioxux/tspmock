/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006RespDto;
import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisPspConstants;
import cn.com.yusys.yusp.domain.PspCheckRst;
import cn.com.yusys.yusp.domain.PspTaskList;
import cn.com.yusys.yusp.domain.PspWydTaskList;
import cn.com.yusys.yusp.dto.PspTaskAndRstDto;
import cn.com.yusys.yusp.dto.PspWydBackTjListDto;
import cn.com.yusys.yusp.dto.PspWydTackTjListDto;
import cn.com.yusys.yusp.dto.client.esb.gcyxpt.yxgc01.Yxgc01ReqDto;
import cn.com.yusys.yusp.repository.mapper.PspTaskListMapper;
import cn.com.yusys.yusp.repository.mapper.PspWydTaskListMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.batch.cmisbatch0006.CmisBatch0006Service;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Supplier;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskListService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: likc
 * @创建时间: 2021-06-11 14:17:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspWydTaskListService {
    private static final Logger log = LoggerFactory.getLogger(PspWydTaskListService.class);

    @Autowired
    private PspWydTaskListMapper pspWydTaskListMapper;

    @Autowired
    private Dscms2GcyxptClientService dscms2GcyxptClientService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public PspWydTaskList selectByPrimaryKey(String pkId) {
        return pspWydTaskListMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public PspWydTaskList selectByTaskNo(String pkId) {
        return pspWydTaskListMapper.selectByTaskNo(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PspWydTaskList> selectAll(QueryModel model) {
        List<PspWydTaskList> records = (List<PspWydTaskList>) pspWydTaskListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 查询待检查任务列表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PspWydTaskList> getPspWydTaskList(QueryModel model) throws Exception {
        List<PspWydTaskList> pspWydTaskList = new ArrayList<PspWydTaskList>();
        PageHelper.startPage(model.getPage(), model.getSize());
        pspWydTaskList = pspWydTaskListMapper.selectByModel(model);
        PageHelper.clearPage();
        return pspWydTaskList;
    }



    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int insert(PspWydTaskList record) {
        return pspWydTaskListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(PspWydTaskList record) {
        return pspWydTaskListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int update(PspWydTaskList pspWydTaskList) throws Exception {
        pspWydTaskList.setUpdateTime(new Date());
        if(pspWydTaskList.getApproveStatus().equals("997")){//贷后任务发送过程营销平台
            try {
            Yxgc01ReqDto yxgc01ReqDto=new Yxgc01ReqDto();
            yxgc01ReqDto.setSerno(pspWydTaskList.getTaskNo());
            yxgc01ReqDto.setCus_id(pspWydTaskList.getCusId());
            yxgc01ReqDto.setCus_name(pspWydTaskList.getCusName());
            yxgc01ReqDto.setPhone(pspWydTaskList.getPhoneNo());
            yxgc01ReqDto.setAddress(pspWydTaskList.getAddress());
            yxgc01ReqDto.setCus_mng_id(pspWydTaskList.getExecId());
            String execIdName = OcaTranslatorUtils.getUserName(pspWydTaskList.getExecId());// 客户经理名称
            yxgc01ReqDto.setCus_mng_name(execIdName);
            yxgc01ReqDto.setMng_br_id(pspWydTaskList.getExecBrId());
            String execBrIdName = OcaTranslatorUtils.getOrgName(pspWydTaskList.getExecBrId());// 客户经理机构
            yxgc01ReqDto.setMng_br_name(execBrIdName);
            yxgc01ReqDto.setCrt_date(pspWydTaskList.getTaskStartDt().replaceAll("-",""));
            yxgc01ReqDto.setRqr_fin_date(pspWydTaskList.getTaskEndDt().replaceAll("-",""));
            yxgc01ReqDto.setPsp_state("6"); //6-信贷发起微业贷
            dscms2GcyxptClientService.yxgc01(yxgc01ReqDto);
            } catch (Exception e) {
                throw new Exception("发送过程营销平台异常");
            }
        }
        return pspWydTaskListMapper.updateByTaskNo(pspWydTaskList);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int updateSelective(PspWydTaskList record) {
        return pspWydTaskListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: pspWydBackTjList
     * @方法描述: 贷后正常任务统计查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PspWydTackTjListDto> pspWydTackTjList(QueryModel model) {
        List<PspWydTackTjListDto> pspwydTackTjListDto = new ArrayList<PspWydTackTjListDto>();
        List<PspWydTackTjListDto> pspWydTackTjListDto = pspWydTaskListMapper.pspWydTackTjList(model);
        if(pspWydTackTjListDto.size()>0){
            for(int i=0;i<pspWydTackTjListDto.size();i++){
                PspWydTackTjListDto spWydTackTjListDto = new PspWydTackTjListDto();
                String relBrId = pspWydTackTjListDto.get(i).getRelBrId();
                String distributeNum = pspWydTackTjListDto.get(i).getDistributeNum();
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("execBrId",relBrId);
                String completeNum = pspWydTaskListMapper.queryTackCompleteNum(queryModel);
                String incompleteNum = pspWydTaskListMapper.queryTackIncompleteNum(queryModel);
                String normalNum = pspWydTaskListMapper.queryTackNormalNum(queryModel);
                String abnormalNum = pspWydTaskListMapper.queryTackAbnormalNum(queryModel);
                spWydTackTjListDto.setRelBrId(relBrId);
                spWydTackTjListDto.setDistributeNum(distributeNum);
                spWydTackTjListDto.setCompleteNum(completeNum);
                spWydTackTjListDto.setIncompleteNum(incompleteNum);
                spWydTackTjListDto.setNormalNum(normalNum);
                spWydTackTjListDto.setAbnormalNum(abnormalNum);
                pspwydTackTjListDto.add(spWydTackTjListDto);
            }
        }
        return pspwydTackTjListDto;
    }

}
