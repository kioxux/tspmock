/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspCusBaseCase;
import cn.com.yusys.yusp.service.PspCusBaseCaseService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspCusBaseCaseResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pspcusbasecase")
public class PspCusBaseCaseResource {
    @Autowired
    private PspCusBaseCaseService pspCusBaseCaseService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspCusBaseCase>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspCusBaseCase> list = pspCusBaseCaseService.selectAll(queryModel);
        return new ResultDto<List<PspCusBaseCase>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PspCusBaseCase>> index(QueryModel queryModel) {
        List<PspCusBaseCase> list = pspCusBaseCaseService.selectByModel(queryModel);
        return new ResultDto<List<PspCusBaseCase>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @GetMapping("/queryList")
    protected ResultDto<List<PspCusBaseCase>> queryList(QueryModel queryModel) {
        List<PspCusBaseCase> list = pspCusBaseCaseService.selectByModel(queryModel);
        return new ResultDto<List<PspCusBaseCase>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspCusBaseCase> show(@PathVariable("pkId") String pkId) {
        PspCusBaseCase pspCusBaseCase = pspCusBaseCaseService.selectByPrimaryKey(pkId);
        return new ResultDto<PspCusBaseCase>(pspCusBaseCase);
    }
    /**
     * @函数名称:queryPspCusBaseCase
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<PspCusBaseCase> queryPspCusBaseCase(@RequestBody PspCusBaseCase pspCusBaseCase) {
        pspCusBaseCase = pspCusBaseCaseService.queryPspCusBaseCase(pspCusBaseCase);
        return new ResultDto<PspCusBaseCase>(pspCusBaseCase);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PspCusBaseCase> create(@RequestBody PspCusBaseCase pspCusBaseCase) throws URISyntaxException {
        pspCusBaseCaseService.insert(pspCusBaseCase);
        return new ResultDto<PspCusBaseCase>(pspCusBaseCase);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspCusBaseCase pspCusBaseCase) throws URISyntaxException {
        int result = pspCusBaseCaseService.update(pspCusBaseCase);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody PspCusBaseCase pspCusBaseCase) throws URISyntaxException {
        int result = pspCusBaseCaseService.updateSelective(pspCusBaseCase);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspCusBaseCaseService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspCusBaseCaseService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByTaskNo")
    protected ResultDto<List<PspCusBaseCase>> selectByTaskNo(@RequestBody QueryModel queryModel) {
        List<PspCusBaseCase> list = pspCusBaseCaseService.selectByModel(queryModel);
        return new ResultDto<List<PspCusBaseCase>>(list);
    }

    /**
     * @函数名称:insert
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<PspCusBaseCase> insert(@RequestBody PspCusBaseCase pspCusBaseCase) throws URISyntaxException {
        pspCusBaseCaseService.insert(pspCusBaseCase);
        return new ResultDto<PspCusBaseCase>(pspCusBaseCase);
    }
    /**
     * 定期贷后检查对公
     *
     * @author
     **/
    @ApiOperation("定期贷后检查对公")
    @PostMapping("/checkInfo")
    protected ResultDto<String> checkInfo(@RequestBody PspCusBaseCase pspCusBaseCase) throws Exception {
        String result= pspCusBaseCaseService.checkInfo(pspCusBaseCase);
        return new ResultDto<String>(result);
    }

    /**
     * 定期贷后检查对公
     *
     * @author
     **/
    @ApiOperation("定期贷后检查对个")
    @PostMapping("/checkCusIndiv")
    protected ResultDto<String> checkCusIndiv(@RequestBody PspCusBaseCase pspCusBaseCase) throws Exception {
        String result= pspCusBaseCaseService.checkCusIndiv(pspCusBaseCase);
        return new ResultDto<String>(result);
    }


}
