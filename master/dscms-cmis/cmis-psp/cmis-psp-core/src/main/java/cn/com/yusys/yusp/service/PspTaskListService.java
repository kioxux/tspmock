/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0006.Cmisbatch0006RespDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisPspConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmislmt0022.req.CmisLmt0022CusListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.req.CmisLmt0022ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022LmtListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022RespDto;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.EpsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.server.cmiscfg0001.req.CmisCfg0001ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0001.resp.CmisCfg0001RespDto;
import cn.com.yusys.yusp.service.client.batch.cmisbatch0006.CmisBatch0006Service;
import org.slf4j.Logger;
import com.alibaba.fastjson.JSON;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.PspTaskListMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskListService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: likc
 * @创建时间: 2021-06-11 14:17:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspTaskListService {
    private static final Logger log = LoggerFactory.getLogger(PspTaskListService.class);

    @Autowired
    private PspTaskListMapper pspTaskListMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private PspCheckRstService pspCheckRstService;
    @Autowired
    private CmisBatch0006Service cmisBatch0006Service;
    @Autowired
    private PspDebitInfoService pspDebitInfoService;
    @Autowired
    private PspGuarntrListService pspGuarntrListService;
    @Autowired
    private PspFinSituCheckService pspFinSituCheckService;
    @Autowired
    private PspImplementOptService pspImplementOptService;
    @Autowired
    private PspPldimnListService pspPldimnListService;
    @Autowired
    private PspOperStatusCheckService pspOperStatusCheckService;
    @Autowired
    private PspCusFinAnalyService pspCusFinAnalyService;
    @Autowired
    private PspWarningInfoListService pspWarningInfoListService;
    @Autowired
    private PspBankFinSituService pspBankFinSituService;
    @Autowired
    private PspGuarCheckService pspGuarCheckService;
    @Autowired
    private PspRelCorpService pspRelCorpService;
    @Autowired
    private PspOperStatusCheckPropertyService pspOperStatusCheckPropertyService;
    @Autowired
    private PspOperStatusCheckRealproService pspOperStatusCheckRealproService;
    @Autowired
    private PspOperStatusCheckArchService pspOperStatusCheckArchService;
    @Autowired
    private PspOperStatusCheckManufactureService pspOperStatusCheckManufactureService;
    @Autowired
    private PspCusBaseCaseService pspCusBaseCaseService;
    @Autowired
    private PspLmtSituService pspLmtSituService;
    @Autowired
    private PspBondSituService pspBondSituService;
    @Autowired
    private PspCapCusBaseCaseService pspCapCusBaseCaseService;
    @Autowired
    private  ICusClientService iCusClientService;

    @Autowired
    private RiskTaskListService riskTaskListService;
    /**
     * 征信接口
     */
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;
    /**
     * 统一额度服务接口
     */
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public PspTaskList selectByPrimaryKey(String pkId) {
        return pspTaskListMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<PspTaskList> selectAll(QueryModel model) {
        List<PspTaskList> records = (List<PspTaskList>) pspTaskListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 查询待检查任务列表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PspTaskList> getPspTaskList(QueryModel model) throws Exception {
        log.info("******************查询待(历史)检查任务列表开始******************");
        List<PspTaskList> pspTaskListList = new ArrayList<PspTaskList>();
        try {
            // 检查类型
            String checkType = "";
            // 审批状态
            String approveStatus = "";
            if (model.getCondition().get("checkType") != null) {
                checkType = model.getCondition().get("checkType").toString();
                List<String> checkTypeList = Arrays.asList(checkType.split(","));
                model.getCondition().put("checkTypeList", checkTypeList);
            }
            if (model.getCondition().get("approveStatus") != null) {
                approveStatus = model.getCondition().get("approveStatus").toString();
                // 待检查任务展示待发起、退回、审批中数据
                List<String> approveStatusList = Arrays.asList(approveStatus.split(","));
                model.getCondition().put("approveStatusList", approveStatusList);
            }
            PageHelper.startPage(model.getPage(), model.getSize());
            if (model.getCondition().get("cusName") != null && model.getCondition().get("cusName") != "") {
                String cusName = model.getCondition().get("cusName").toString();
                model.getCondition().put("cusName", "%" + cusName + "%");
            }
            pspTaskListList = pspTaskListMapper.selectMhByModel(model);
            PageHelper.clearPage();
        } catch (Exception e) {
            log.error("******************查询待(历史)检查任务列表异常******************");
            throw new Exception("查询待(历史)检查任务列表异常");
        }
        log.info("******************查询待(历史)检查任务列表结束******************");
        return pspTaskListList;
    }

    /**
     * 获取sequence流水号
     **/
    public String getSequences() {
        //TODO 后续改为getSequenceTemplate获取
        String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.PSP_TASK_NO, new HashMap<>());
//        String serno = sequenceTemplateClient.getSequenceTemplate("DHJC", new HashMap<>());
        return serno;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int insert(PspTaskList record) {
        // 贷后检查任务表，新增成功，则生成首页提示
        int insertCount = pspTaskListMapper.insert(record);
        if( insertCount > 0) {
            CmisCfg0001ReqDto cmisCfg0001ReqDto = new CmisCfg0001ReqDto();
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.PK_VALUE, new HashMap<>());
            cmisCfg0001ReqDto.setSerno(serno);
            // 工作日历待办提醒
            cmisCfg0001ReqDto.setMessageType("5");
            // 提醒内容
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            cmisCfg0001ReqDto.setContent("您收到了总行下发的关于您的客户"+record.getCusId()+record.getCusName()+"的不定期贷后检查任务，请登录系统查看。");
            cmisCfg0001ReqDto.setPubTime(DateUtils.getCurrTime());
            cmisCfg0001ReqDto.setReceiverType(null);
            cmisCfg0001ReqDto.setInputDate(openDay);
            cmisCfg0001ReqDto.setInputBrId(record.getExecBrId());
            cmisCfg0001ReqDto.setInputId(record.getExecId());
            log.info("根据任务编号【{}】，审批操作生成首页提醒！", record.getTaskNo());
            ResultDto<CmisCfg0001RespDto> cmisCfg0001RespDto = dscmsCfgQtClientService.cmisCfg0001(cmisCfg0001ReqDto);
            log.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0001.key,
                    DscmsCfgEnum.TRADE_CODE_CMISCFG0001.value, JSON.toJSONString(cmisCfg0001RespDto));
            if (cmisCfg0001RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisCfg0001RespDto.getData().getErrorCode())) {
                log.info("根据任务编号【{}】,生成首页提醒成功！", serno);
            } else {
                log.info("根据任务编号【{}】,生成首页提醒成功失败！", serno);
            }
        }
        return insertCount;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int insertSelective(PspTaskList record) {
        return pspTaskListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int update(PspTaskList record) {
        return pspTaskListMapper.updateByTaskNo(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int updateSelective(PspTaskList record) {
        return pspTaskListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(String pkId) {
        return pspTaskListMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int deleteByIds(String ids) {
        return pspTaskListMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateTaskAndRst(PspTaskAndRstDto record) {
        PspTaskList pspTaskList = new PspTaskList();
        PspCheckRst pspCheckRst = new PspCheckRst();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        record.setUpdateTime(DateUtils.parseDateByDef(openDay));
        org.springframework.beans.BeanUtils.copyProperties(record, pspTaskList);
        org.springframework.beans.BeanUtils.copyProperties(record, pspCheckRst);
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
        }
        pspTaskList.setUpdId(inputId);
        pspTaskList.setUpdBrId(inputBrId);
        pspTaskList.setUpdDate(openDay);
        pspTaskList.setUpdateTime(createTime);
        pspCheckRst.setUpdateTime(createTime);
        if (pspTaskListMapper.updateByTaskNo(pspTaskList) == 0) {
            pspTaskList.setCreateTime(DateUtils.parseDateByDef(openDay));
            pspTaskList.setPkId(StringUtils.getUUID());
            pspTaskList.setTaskStatus("1");// 任务生成中
            int result = insertSelectiveData(pspTaskList);
        }
        if (pspCheckRstService.updateByTaskNo(pspCheckRst) == 0) {
            pspCheckRst.setCreateTime(DateUtils.parseDateByDef(openDay));
            pspCheckRstService.insert(pspCheckRst);
        }
        return 0;
    }

    private int insertSelectiveData(PspTaskList record) {
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String openDay =stringRedisTemplate.opsForValue().get("openDay");
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
        }
        record.setInputId(inputId);
        record.setInputBrId(inputBrId);
        record.setInputDate(openDay);
        record.setUpdId(inputId);
        record.setUpdBrId(inputBrId);
        record.setUpdDate(openDay);
        record.setCreateTime(createTime);
        record.setUpdateTime(createTime);
        return pspTaskListMapper.insertSelective(record);
    }

    /**
     * @方法名称: selectTaskAndRst
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspTaskAndRstDto selectTaskAndRst(PspTaskAndRstDto pspTaskAndRstDto) {
        PspTaskList pspTaskList = new PspTaskList();
        PspCheckRst pspCheckRst = new PspCheckRst();
        pspTaskList.setTaskNo(pspTaskAndRstDto.getTaskNo());
        pspTaskList.setPkId(pspTaskAndRstDto.getPkId());
        pspCheckRst.setTaskNo(pspTaskAndRstDto.getTaskNo());
        pspCheckRst.setPkId(pspTaskAndRstDto.getPkId());
        pspTaskList = pspTaskListMapper.queryPspTaskList(pspTaskList);
        pspCheckRst = pspCheckRstService.queryPspCheckRst(pspCheckRst);
        if (null == pspCheckRst) {
            pspTaskAndRstDto.setIsSaved("0");
        } else {
            org.springframework.beans.BeanUtils.copyProperties(pspCheckRst, pspTaskAndRstDto);
            pspTaskAndRstDto.setIsSaved("1");
        }
        org.springframework.beans.BeanUtils.copyProperties(pspTaskList, pspTaskAndRstDto);
        return pspTaskAndRstDto;
    }

    /**
     * @函数名称:selectNumByInputId
     * @函数描述:根据客户经理工号查询待处理数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    public List<Map<String, Object>> selectNumByInputId(QueryModel queryModel) {
        return pspTaskListMapper.selectNumByInputId(queryModel);
    }

    /**
     * @方法名称: deleteByTaskNo
     * @方法描述: 删除检查，并且删除其子表信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int deleteByTaskNo(PspTaskList pspTaskList) {
        // 1、先删除子表信息\
        String taskNo = pspTaskList.getTaskNo();
        // psp_debit_info  借据信息
        pspDebitInfoService.deleteByTaskNo(taskNo);
        // psp_check_rst   贷后检查结果表
        pspCheckRstService.deleteByTaskNo(taskNo);
        // psp_guarntr_list 保证人检查
        pspGuarntrListService.deleteByTaskNo(taskNo);
        // psp_fin_situ_check 财务状况检查
        pspFinSituCheckService.deleteByTaskNo(taskNo);
        // psp_implement_opt 贷后管理建议落实情况
        pspImplementOptService.deleteByTaskNo(taskNo);
        // psp_pldimn_list 抵质押物检查
        pspPldimnListService.deleteByTaskNo(taskNo);
        // psp_oper_status_check 经营状况检查
        pspOperStatusCheckService.deleteByTaskNo(taskNo);
        // psp_cus_fin_analy 融资情况分析
        pspCusFinAnalyService.deleteByTaskNo(taskNo);
        // psp_warning_info_list 预警信号
        pspWarningInfoListService.deleteByTaskNo(taskNo);
        // psp_bank_fin_situ 正常类本行融资情况
        pspBankFinSituService.deleteByTaskNo(taskNo);
        // psp_guar_check  对外担保分析
        pspGuarCheckService.deleteByTaskNo(taskNo);
        // psp_rel_corp 不良类关联企业表
        pspRelCorpService.deleteByTaskNo(taskNo);
        // psp_oper_status_check_property 经营性物业贷款检查
        pspOperStatusCheckPropertyService.deleteByTaskNo(taskNo);
        // psp_oper_status_check_realpro 经营状况检查（房地产开发贷）
        pspOperStatusCheckRealproService.deleteByTaskNo(taskNo);
        // PspOperStatusCheckArch// 插入对公定期检查经营状况检查（建筑业）
        pspOperStatusCheckArchService.deleteByTaskNo(taskNo);
        // PspOperStatusCheckManufacture// 插入对公定期检查经营状况检查（制造业）
        pspOperStatusCheckManufactureService.deleteByTaskNo(taskNo);
        // PspCusBaseCase// 插入对公、个人客户基本信息分析表
        pspCusBaseCaseService.deleteByTaskNo(taskNo);
        // PspLmtSitu// 插入投后定期检查存量授信情况
        pspLmtSituService.deleteByTaskNo(taskNo);
        // PspBondSitu// 插入投后定期检查债券池用信业务情况
        pspBondSituService.deleteByTaskNo(taskNo);
        // PspCapCusBaseCase// 插入资金业务客户基本信息分析表
        pspCapCusBaseCaseService.deleteByTaskNo(taskNo);
        // 2、再删除主表数据
        return pspTaskListMapper.deleteByTaskNo(taskNo);
    }

    /**
     * 调用批量接口
     *
     * @param record
     * @return
     */
    public int cmisBatch0006Services(PspTaskAndRstDto record) {
        if (record != null) {
            log.info("开始下发任务");
            Supplier<Cmisbatch0006ReqDto> supplier = Cmisbatch0006ReqDto::new;
            Cmisbatch0006ReqDto cmisbatch0006ReqDto = supplier.get();
            cmisbatch0006ReqDto.setTaskNo(record.getTaskNo());
            cmisbatch0006ReqDto.setRptType(record.getRptType());
            cmisbatch0006ReqDto.setCheckType(record.getCheckType());
            cmisbatch0006ReqDto.setCusId(record.getCusId());
            cmisbatch0006ReqDto.setCusName(record.getCusName());
            cmisbatch0006ReqDto.setTaskStartDt(record.getTaskStartDt());
            cmisBatch0006Service.cmisbatch0006(cmisbatch0006ReqDto);
            // 投后存量信息插入
            String checkTypes="26,27,28,36,37,38";
            if (checkTypes.indexOf(record.getCheckType())>=0) {
                // 客户号
                String cusId = record.getCusId();
                Supplier<PspBondSitu> pspBondSitusupplier = PspBondSitu::new;
                Supplier<PspLmtSitu> pspLmtSitusupplier = PspLmtSitu::new;
                CmisLmt0022ReqDto cmisLmt0022ReqDto = new CmisLmt0022ReqDto();
                ArrayList<CmisLmt0022CusListReqDto> cmisLmt0022CusListReqDtos = new ArrayList<>();
                CmisLmt0022CusListReqDto cmisLmt0022CusListReqDto = new CmisLmt0022CusListReqDto();
                cmisLmt0022CusListReqDto.setCusId(cusId);
                cmisLmt0022CusListReqDtos.add(cmisLmt0022CusListReqDto);
                cmisLmt0022ReqDto.setCusList(cmisLmt0022CusListReqDtos);
                cmisLmt0022ReqDto.setInstuCde(getCurrentUser().getInstuOrg().getCode());
                // 调用额度接口
                log.info("调用额度接口，请求参数" + cmisLmt0022ReqDto);
                ResultDto<CmisLmt0022RespDto> cmisLmt0022RespDto = cmisLmtClientService.cmisLmt0022(cmisLmt0022ReqDto);
                if (cmisLmt0022RespDto.getData() != null) {
                    CmisLmt0022RespDto data = cmisLmt0022RespDto.getData();
                    List<CmisLmt0022LmtListRespDto> lmtList = data.getLmtList();
                    for (CmisLmt0022LmtListRespDto cmisLmt0022LmtListRespDto : lmtList) {
                        // 判断是否是债卷池用信
                        if ("13".equals(cmisLmt0022LmtListRespDto.getLmtCatalog())) {
                            PspBondSitu pspBondSitu = pspBondSitusupplier.get();
                            pspBondSitu.setPkId(StringUtils.getUUID());
                            pspBondSitu.setTaskNo(record.getTaskNo());
                            // 投资资产名称
                            pspBondSitu.setInvestAssetName(cmisLmt0022LmtListRespDto.getInvestAssetName());
                            // 产品名称
                            pspBondSitu.setPrdName("");
                            // 投资金额
                            pspBondSitu.setInvestAmt(cmisLmt0022LmtListRespDto.getLmtAmt());
                            // 投资币种
                            pspBondSitu.setInvestCurType("");
                            // 交易日期
                            pspBondSitu.setTranDate(cmisLmt0022LmtListRespDto.getStartDate());
                            // 资产到期日
                            pspBondSitu.setAssetEndDate(cmisLmt0022LmtListRespDto.getEndDate());
                            // 投资部门
                            // 投资经理
                            pspBondSituService.insert(pspBondSitu);
                        } else {
                            PspLmtSitu pspLmtSitu = pspLmtSitusupplier.get();
                            // 授信分项流水号
                            pspLmtSitu.setSubLmtNo(cmisLmt0022LmtListRespDto.getApprSubSerno());
                            // 授信分项额度名称
                            pspLmtSitu.setSubLmtName(cmisLmt0022LmtListRespDto.getLmtBizTypeName());
                            // 授信额度
                            pspLmtSitu.setLmtAmt(cmisLmt0022LmtListRespDto.getLmtAmt());
                            //  授信余额
                            pspLmtSitu.setLmtBalanceAmt(cmisLmt0022LmtListRespDto.getLmtBalanceAmt());
                            //  已用额度
                            pspLmtSitu.setOutstndAmt(cmisLmt0022LmtListRespDto.getOutstandAmt());
                            //  可用额度
                            pspLmtSitu.setAvlLmtAmt(cmisLmt0022LmtListRespDto.getLmtAmt().subtract(cmisLmt0022LmtListRespDto.getOutstandAmt()));
                            // 担保方式
                            pspLmtSitu.setGuarMode(cmisLmt0022LmtListRespDto.getGuarMode());
                            //  是否循环
                            pspLmtSitu.setIsRevolv(cmisLmt0022LmtListRespDto.getIsRevolv());
                            //  额度到期日
                            pspLmtSitu.setLmtEndDate(cmisLmt0022LmtListRespDto.getEndDate());
                            // 授信类型
                            pspLmtSitu.setLmtType(cmisLmt0022LmtListRespDto.getLmtType());
                            pspLmtSitu.setPkId(StringUtils.getUUID());
                            pspLmtSitu.setTaskNo(record.getTaskNo());
                            // 11 资金业务存量授信
                            if ("11".equals(cmisLmt0022LmtListRespDto.getLmtCatalog())) {
                                pspLmtSitu.setPrdCatalog("1");
                                // 21 敞口存量授信、同业综合授信
                            }else if("21".equals(cmisLmt0022LmtListRespDto.getLmtCatalog())){
                                pspLmtSitu.setPrdCatalog("2");// 信贷业务
                                pspLmtSitu.setBusiType("1"); // 敞口存量授信
                            }else if("22".equals(cmisLmt0022LmtListRespDto.getLmtCatalog())){
                                pspLmtSitu.setPrdCatalog("2");// 信贷业务
                                pspLmtSitu.setBusiType("2"); // 低分险存量授信
                            }
                            pspLmtSituService.insert(pspLmtSitu);
                        }
                    }
                }
            }
        }
        return 0;
    }

    /**
     * 获取当前登陆人员
     *
     * @return
     */
    public User getCurrentUser() {
        return SessionUtils.getUserInformation();
    }





    /**
     * 贷后征信查询
     * @param taskNo
     * @return
     */
    public ResultDto<Integer> createCreditCus(String taskNo){
        // 先查询对公客户信息
        PspCusBaseCase pspCusBaseCase=pspCusBaseCaseService.queryPspCusByTaskNo(taskNo);
        ResultDto<Integer> doCreateCreditAuto = new ResultDto<>();
        if(pspCusBaseCase !=null) {

            // 主借款人客户号
            String cusId = pspCusBaseCase.getCusId();
            String borrowRel= "001";
            doCreateCreditAuto= createCredit(cusId,taskNo,borrowRel,cusId);
            // 保证人客户号
            String grtCusId = "";
            List<PspGuarntrList> pspGuarntrLists = pspGuarntrListService.selectByTaskNo(taskNo);
            // 如果担保扔不为空则查询担保征信信息
            if(CollectionUtils.nonEmpty(pspGuarntrLists)){
                for(PspGuarntrList pspGuarntrList:pspGuarntrLists){
                    grtCusId=pspGuarntrList.getGuarCusId();
                    borrowRel="007";
                    doCreateCreditAuto= createCredit(grtCusId,taskNo,borrowRel,cusId);
                }
            }

            List<PspPldimnList> pspPldimnLists = pspPldimnListService.selectByTaskNo(taskNo);
            // 如果抵质押物所有权人不为空则查询担保征信信息
            if(CollectionUtils.nonEmpty(pspPldimnLists)){
                for(PspPldimnList pspPldimnList:pspPldimnLists){
                    grtCusId = pspPldimnList.getGuarCusId();
                    borrowRel="007";
                    doCreateCreditAuto= createCredit(grtCusId,taskNo,borrowRel,cusId);
                }
            }
        }
        return doCreateCreditAuto;
    }


    /**
     * 对公不定期贷后检查征信信息
     * @param taskNo
     * @return
     */
    public ResultDto<Integer> createCredit(String cusId,String taskNo,String borrowRel,String borrowerCusId){
        ResultDto<Integer> doCreateCreditAuto = new ResultDto<>();
        if(StringUtils.nonBlank(cusId)){
               //拼装请求参数
               CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
               CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
               CusBaseClientDto cusBaseBorrowerClientDto = iCusClientService.queryCus(borrowerCusId);
               creditReportQryLstAndRealDto.setBorrowerCusId(borrowerCusId);
               creditReportQryLstAndRealDto.setBorrowerCusName(cusBaseBorrowerClientDto.getCusName());
               creditReportQryLstAndRealDto.setBorrowerCertCode(cusBaseBorrowerClientDto.getCertCode());
               creditReportQryLstAndRealDto.setCusId(cusId);
               creditReportQryLstAndRealDto.setCusName(cusBaseClientDto.getCusName());
               creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
               creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
               // 与主借款人关系   001 主借款人  007 担保人
               creditReportQryLstAndRealDto.setBorrowRel(borrowRel);
               // 客户大类
               String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
               creditReportQryLstAndRealDto.setQryCls(qryCls);
               creditReportQryLstAndRealDto.setApproveStatus("000");
               // 是否成功发起
               creditReportQryLstAndRealDto.setIsSuccssInit("0");
               // 征信查询标识
               creditReportQryLstAndRealDto.setQryFlag("02");
               // 征信查询状态
               creditReportQryLstAndRealDto.setQryStatus("001");
               //生成关联征信数据
               creditReportQryLstAndRealDto.setBizSerno(taskNo);
               // 贷后检查
               creditReportQryLstAndRealDto.setScene("04");
               doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
               if (!doCreateCreditAuto.getCode().equals("0")) {
                   log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
               }
           }else {
            doCreateCreditAuto.setMessage("客户号不存在");
        }
        return doCreateCreditAuto;
    }

    /**
     * 根据客户号获取年份集合
     * @author jijian_yx
     * @date 2021/9/27 16:29
     **/
    public List<String> getAccTimeList(String cusId) {
        return pspTaskListMapper.getAccTimeList(cusId);
    }

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     * @author jijian_yx
     * @date 2021/9/28 0:06
     **/
    public List<DocAccSearchDto> queryAccListByDocTypeAndYear(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<DocAccSearchDto> list = pspTaskListMapper.queryAccListByDocTypeAndYear(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据传入的pspTaskList对象，更新指定对象的属性
     * @param paramMap
     * @return
     */
    public boolean updatePspTaskList(Map<String, String> paramMap) {
        int i=pspTaskListMapper.updatePspTaskList(paramMap);
        return i==1?true:false;
    }


    /**
     * 根据任务编号查询
     * @param taskNo
     * @return
     */
    public PspTaskList selectByTaskNo(String taskNo) {
        return pspTaskListMapper.selectByTaskNo(taskNo);
    }
    /**
     * @方法名称: queryPspTaskList
     * @方法描述: 查询待检查任务列表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PspTaskAndRstDto> queryPspTaskList(QueryModel model) throws Exception {
        log.info("******************查询待(历史)检查任务列表开始******************");
        List<PspTaskAndRstDto> pspTaskListList = new ArrayList<PspTaskAndRstDto>();
        try {
            // 检查类型
            String checkType = "";
            // 审批状态
            String approveStatus = "";
            if (model.getCondition().get("checkType") != null) {
                checkType = model.getCondition().get("checkType").toString();
                List<String> checkTypeList = Arrays.asList(checkType.split(","));
                model.getCondition().put("checkTypeList", checkTypeList);
            }
            if (model.getCondition().get("approveStatus") != null) {
                approveStatus = model.getCondition().get("approveStatus").toString();
                // 待检查任务展示待发起、退回、审批中数据
                List<String> approveStatusList = Arrays.asList(approveStatus.split(","));
                model.getCondition().put("approveStatusList", approveStatusList);
            }
            PageHelper.startPage(model.getPage(), model.getSize());
            if (model.getCondition().get("cusName") != null && model.getCondition().get("cusName") != "") {
                String cusName = model.getCondition().get("cusName").toString();
                model.getCondition().put("cusName", "%" + cusName + "%");
            }
            List<PspTaskAndRstDto> pspTaskListLists = pspTaskListMapper.selectPspTaskAndRst(model);
            // 查询贷后检查结论
            if(CollectionUtils.nonEmpty(pspTaskListLists)){
                for (PspTaskAndRstDto pspTaskAndRstDto:pspTaskListLists){
                    String taskNo=pspTaskAndRstDto.getTaskNo();
                    PspCheckRst pspCheckRst =pspCheckRstService.queryByTaskNo(taskNo);
                    if(pspCheckRst!=null){
                        pspTaskAndRstDto.setCheckComment(pspCheckRst.getCheckComment());
                    }
                    pspTaskListList.add(pspTaskAndRstDto);
                }
            }
            PageHelper.clearPage();
        } catch (Exception e) {
            log.error("******************查询待(历史)检查任务列表异常******************");
            throw new Exception("查询待(历史)检查任务列表异常");
        }
        log.info("******************查询待(历史)检查任务列表结束******************");
        return pspTaskListList;
    }


    /**
     *客户移交判断是否有在途任务
     * @param pspKHYJDto
     * @return
     */
    public ResultDto<String> queryCountBycusId(PspKHYJDto pspKHYJDto) {
        ResultDto<String> resultDto= new ResultDto<>();
        // 判断贷后数据是否存在
        PspTaskList pspTaskList= pspTaskListMapper.queryCountBycusId(pspKHYJDto);
        RiskTaskList riskTaskList=riskTaskListService.queryCountBycusId(pspKHYJDto);
        if(pspTaskList !=null && riskTaskList !=null){
            resultDto.setData(pspKHYJDto.getCusId());
            resultDto.setCode(EpsEnum.EPS039999.key);
            resultDto.setMessage(EpsEnum.EPS030006.value);
        }else if(riskTaskList !=null){
            resultDto.setData(pspKHYJDto.getCusId());
            resultDto.setCode(EpsEnum.EPS039999.key);
            resultDto.setMessage(EpsEnum.EPS030005.value);
        }else if(pspTaskList !=null){
            resultDto.setData(pspKHYJDto.getCusId());
            resultDto.setCode(EpsEnum.EPS039999.key);
            resultDto.setMessage(EpsEnum.EPS030004.value);
        }else{
            resultDto.setData(pspKHYJDto.getCusId());
            resultDto.setMessage(EpsEnum.EPS030000.value);
        }
        return resultDto;
    }
}
