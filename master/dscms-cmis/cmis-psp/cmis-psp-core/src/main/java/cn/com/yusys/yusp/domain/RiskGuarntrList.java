/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskGuarntrList
 * @类描述: risk_guarntr_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-20 09:36:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "risk_guarntr_list")
public class RiskGuarntrList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 保证编号 **/
	@Column(name = "GUARANTY_ID", unique = false, nullable = true, length = 40)
	private String guarantyId;
	
	/** 保证人客户编号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 40)
	private String guarCusId;
	
	/** 保证人客户名称 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 80)
	private String guarCusName;
	
	/** 保证方式 **/
	@Column(name = "GUARANTEE_TYPE", unique = false, nullable = true, length = 5)
	private String guaranteeType;
	
	/** 保证金额（元） **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 保证人代偿意愿 **/
	@Column(name = "SUBPAY_WISH", unique = false, nullable = true, length = 5)
	private String subpayWish;
	
	/** 代偿能力 **/
	@Column(name = "SUBPAY_ABI", unique = false, nullable = true, length = 5)
	private String subpayAbi;
	
	/** 分析状态 **/
	@Column(name = "ANALY_STATUS", unique = false, nullable = true, length = 5)
	private String analyStatus;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param guarantyId
	 */
	public void setGuarantyId(String guarantyId) {
		this.guarantyId = guarantyId;
	}
	
    /**
     * @return guarantyId
     */
	public String getGuarantyId() {
		return this.guarantyId;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}
	
    /**
     * @return guarCusId
     */
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}
	
    /**
     * @return guarCusName
     */
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param guaranteeType
	 */
	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}
	
    /**
     * @return guaranteeType
     */
	public String getGuaranteeType() {
		return this.guaranteeType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param subpayWish
	 */
	public void setSubpayWish(String subpayWish) {
		this.subpayWish = subpayWish;
	}
	
    /**
     * @return subpayWish
     */
	public String getSubpayWish() {
		return this.subpayWish;
	}
	
	/**
	 * @param subpayAbi
	 */
	public void setSubpayAbi(String subpayAbi) {
		this.subpayAbi = subpayAbi;
	}
	
    /**
     * @return subpayAbi
     */
	public String getSubpayAbi() {
		return this.subpayAbi;
	}
	
	/**
	 * @param analyStatus
	 */
	public void setAnalyStatus(String analyStatus) {
		this.analyStatus = analyStatus;
	}
	
    /**
     * @return analyStatus
     */
	public String getAnalyStatus() {
		return this.analyStatus;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}