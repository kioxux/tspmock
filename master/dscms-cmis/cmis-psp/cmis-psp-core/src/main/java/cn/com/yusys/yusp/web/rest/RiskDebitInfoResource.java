/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.RiskDebitAndLoanDto;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RiskDebitInfo;
import cn.com.yusys.yusp.service.RiskDebitInfoService;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskDebitInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-18 01:58:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/riskdebitinfo")
public class RiskDebitInfoResource {
    @Autowired
    private RiskDebitInfoService riskDebitInfoService;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RiskDebitInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<RiskDebitInfo> list = riskDebitInfoService.selectAll(queryModel);
        return new ResultDto<List<RiskDebitInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RiskDebitInfo>> index(QueryModel queryModel) {
        List<RiskDebitInfo> list = riskDebitInfoService.selectByModel(queryModel);
        return new ResultDto<List<RiskDebitInfo>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<RiskDebitInfo>> queryList(@RequestBody QueryModel queryModel) {
        List<RiskDebitInfo> list = riskDebitInfoService.selectByModel(queryModel);
        return new ResultDto<List<RiskDebitInfo>>(list);
    }

    /**
     * @函数名称:selectRiskDebitAndLoanList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @PostMapping("/selectRiskDebitAndLoanList")
    protected ResultDto<List<RiskDebitAndLoanDto>> selectRiskDebitAndLoan(@RequestBody QueryModel queryModel) {
        List<RiskDebitAndLoanDto> list = riskDebitInfoService.selectRiskDebitAndLoan(queryModel);
        return new ResultDto<List<RiskDebitAndLoanDto>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RiskDebitInfo> show(@PathVariable("pkId") String pkId) {
        RiskDebitInfo riskDebitInfo = riskDebitInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<RiskDebitInfo>(riskDebitInfo);
    }
    /**
     * @函数名称:queryRiskDebitInfo
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<RiskDebitInfo> queryRiskDebitInfo(@RequestBody RiskDebitInfo riskDebitInfo) {
        riskDebitInfo = riskDebitInfoService.queryRiskDebitInfo(riskDebitInfo);
        return new ResultDto<RiskDebitInfo>(riskDebitInfo);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<RiskDebitInfo> create(@RequestBody RiskDebitInfo riskDebitInfo) throws URISyntaxException {
        riskDebitInfoService.insert(riskDebitInfo);
        return new ResultDto<RiskDebitInfo>(riskDebitInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RiskDebitInfo riskDebitInfo) throws URISyntaxException {
        int result = riskDebitInfoService.update(riskDebitInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = riskDebitInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = riskDebitInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

//    @PostMapping("/process")
//    protected ResultDto<Integer> process(@Parameter String billNo) {
//        int result = riskDebitInfoService.process(billNo);
//        return new ResultDto<Integer>(result);
//    }
}
