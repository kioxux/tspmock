/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWarnList
 * @类描述: psp_warn_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 21:58:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_warn_list")
public class PspWarnList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 贷后预警流水号 **/
	@Column(name = "WARN_SERNO", unique = false, nullable = true, length = 40)
	private String warnSerno;
	
	/** 贷后预警类型 **/
	@Column(name = "WARN_TYPE", unique = false, nullable = true, length = 5)
	private String warnType;
	
	/** 借据号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 模型结果 **/
	@Column(name = "MODEL_RESULT", unique = false, nullable = true, length = 5)
	private String modelResult;
	
	/** 预警日期 **/
	@Column(name = "WARN_DATE", unique = false, nullable = true, length = 20)
	private String warnDate;
	
	/** 逾期本金 **/
	@Column(name = "OVERDUE_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueCapAmt;
	
	/** 逾期利息 **/
	@Column(name = "OVERDUE_INT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueIntAmt;
	
	/** 期望完成日期 **/
	@Column(name = "EXPERT_DATE", unique = false, nullable = true, length = 20)
	private String expertDate;
	
	/** 完成日期 **/
	@Column(name = "COMPLETE_DATE", unique = false, nullable = true, length = 20)
	private String completeDate;
	
	/** 办理人工号 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 办理人 **/
	@Column(name = "MANAGER_NAME", unique = false, nullable = true, length = 80)
	private String managerName;
	
	/** 借款人客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 借款人名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;
	
	/** 合同起始日 **/
	@Column(name = "CONT_START_DATE", unique = false, nullable = true, length = 20)
	private String contStartDate;
	
	/** 借据金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 借据起始日 **/
	@Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
	private String loanStartDate;
	
	/** 核验方式 **/
	@Column(name = "CHECK_MODE", unique = false, nullable = true, length = 5)
	private String checkMode;
	
	/** 核验地点 **/
	@Column(name = "CHECK_ADDR", unique = false, nullable = true, length = 200)
	private String checkAddr;
	
	/** 出险原因 **/
	@Column(name = "RISK_REASON", unique = false, nullable = true, length = 65535)
	private String riskReason;
	
	/** 检查意见 **/
	@Column(name = "CHECK_ADVICE", unique = false, nullable = true, length = 65535)
	private String checkAdvice;
	
	/** 审批人工号 **/
	@Column(name = "APPROVE_ID", unique = false, nullable = true, length = 20)
	private String approveId;
	
	/** 审批人名称 **/
	@Column(name = "APPROVE_NAME", unique = false, nullable = true, length = 80)
	private String approveName;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 合同号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 借款人身份证号 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param warnSerno
	 */
	public void setWarnSerno(String warnSerno) {
		this.warnSerno = warnSerno;
	}
	
    /**
     * @return warnSerno
     */
	public String getWarnSerno() {
		return this.warnSerno;
	}
	
	/**
	 * @param warnType
	 */
	public void setWarnType(String warnType) {
		this.warnType = warnType;
	}
	
    /**
     * @return warnType
     */
	public String getWarnType() {
		return this.warnType;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param modelResult
	 */
	public void setModelResult(String modelResult) {
		this.modelResult = modelResult;
	}
	
    /**
     * @return modelResult
     */
	public String getModelResult() {
		return this.modelResult;
	}
	
	/**
	 * @param warnDate
	 */
	public void setWarnDate(String warnDate) {
		this.warnDate = warnDate;
	}
	
    /**
     * @return warnDate
     */
	public String getWarnDate() {
		return this.warnDate;
	}
	
	/**
	 * @param overdueCapAmt
	 */
	public void setOverdueCapAmt(java.math.BigDecimal overdueCapAmt) {
		this.overdueCapAmt = overdueCapAmt;
	}
	
    /**
     * @return overdueCapAmt
     */
	public java.math.BigDecimal getOverdueCapAmt() {
		return this.overdueCapAmt;
	}
	
	/**
	 * @param overdueIntAmt
	 */
	public void setOverdueIntAmt(java.math.BigDecimal overdueIntAmt) {
		this.overdueIntAmt = overdueIntAmt;
	}
	
    /**
     * @return overdueIntAmt
     */
	public java.math.BigDecimal getOverdueIntAmt() {
		return this.overdueIntAmt;
	}
	
	/**
	 * @param expertDate
	 */
	public void setExpertDate(String expertDate) {
		this.expertDate = expertDate;
	}
	
    /**
     * @return expertDate
     */
	public String getExpertDate() {
		return this.expertDate;
	}
	
	/**
	 * @param completeDate
	 */
	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate;
	}
	
    /**
     * @return completeDate
     */
	public String getCompleteDate() {
		return this.completeDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	
    /**
     * @return managerName
     */
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param contStartDate
	 */
	public void setContStartDate(String contStartDate) {
		this.contStartDate = contStartDate;
	}
	
    /**
     * @return contStartDate
     */
	public String getContStartDate() {
		return this.contStartDate;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}
	
    /**
     * @return loanStartDate
     */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param checkMode
	 */
	public void setCheckMode(String checkMode) {
		this.checkMode = checkMode;
	}
	
    /**
     * @return checkMode
     */
	public String getCheckMode() {
		return this.checkMode;
	}
	
	/**
	 * @param checkAddr
	 */
	public void setCheckAddr(String checkAddr) {
		this.checkAddr = checkAddr;
	}
	
    /**
     * @return checkAddr
     */
	public String getCheckAddr() {
		return this.checkAddr;
	}
	
	/**
	 * @param riskReason
	 */
	public void setRiskReason(String riskReason) {
		this.riskReason = riskReason;
	}
	
    /**
     * @return riskReason
     */
	public String getRiskReason() {
		return this.riskReason;
	}
	
	/**
	 * @param checkAdvice
	 */
	public void setCheckAdvice(String checkAdvice) {
		this.checkAdvice = checkAdvice;
	}
	
    /**
     * @return checkAdvice
     */
	public String getCheckAdvice() {
		return this.checkAdvice;
	}
	
	/**
	 * @param approveId
	 */
	public void setApproveId(String approveId) {
		this.approveId = approveId;
	}
	
    /**
     * @return approveId
     */
	public String getApproveId() {
		return this.approveId;
	}
	
	/**
	 * @param approveName
	 */
	public void setApproveName(String approveName) {
		this.approveName = approveName;
	}
	
    /**
     * @return approveName
     */
	public String getApproveName() {
		return this.approveName;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}


}