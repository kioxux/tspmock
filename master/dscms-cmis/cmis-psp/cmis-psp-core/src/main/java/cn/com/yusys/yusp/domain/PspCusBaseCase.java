/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspCusBaseCase
 * @类描述: psp_cus_base_case数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_cus_base_case")
public class PspCusBaseCase extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 经营地址 **/
	@Column(name = "OPER_ADDR_ACT", unique = false, nullable = true, length = 200)
	private String operAddrAct;
	
	/** 股权结构 **/
	@Column(name = "OWNSHIP_STR", unique = false, nullable = true, length = 65535)
	private String ownshipStr;
	
	/** 主营业务 **/
	@Column(name = "MAIN_OPT_SCP", unique = false, nullable = true, length = 500)
	private String mainOptScp;
	
	/** 成立日期 **/
	@Column(name = "BUILD_DATE", unique = false, nullable = true, length = 20)
	private String buildDate;
	
	/** 员工人数 **/
	@Column(name = "FJOB_NUM", unique = false, nullable = true, length = 10)
	private Integer fjobNum;
	
	/** 注册资本（万元） **/
	@Column(name = "REGI_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regiCapAmt;
	
	/** 实收资本（万元） **/
	@Column(name = "PAID_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidCapAmt;
	
	/** 行业分类 **/
	@Column(name = "TRADE_CLASS", unique = false, nullable = true, length = 5)
	private String tradeClass;
	
	/** 法人代表 **/
	@Column(name = "LEGAL_REPRESENT", unique = false, nullable = true, length = 80)
	private String legalRepresent;
	
	/** 实际控制人 **/
	@Column(name = "REAL_OPER_CUS_NAME", unique = false, nullable = true, length = 80)
	private String realOperCusName;
	
	/** 企业性质 **/
	@Column(name = "CORP_QLTY", unique = false, nullable = true, length = 5)
	private String corpQlty;
	
	/** 内外部数据系统校验是否有变化 **/
	@Column(name = "IS_DATA_CHG", unique = false, nullable = true, length = 5)
	private String isDataChg;
	
	/** 校验变化说明 **/
	@Column(name = "DATA_CHG_EXPL", unique = false, nullable = true, length = 65535)
	private String dataChgExpl;
	
	/** 房地产开发贷检查 **/
	@Column(name = "IS_REALPRO", unique = false, nullable = true, length = 5)
	private String isRealpro;
	
	/** 经营性物业贷检查 **/
	@Column(name = "IS_PROPERTY", unique = false, nullable = true, length = 5)
	private String isProperty;
	
	/** 固定资产贷款、项目贷款检查 **/
	@Column(name = "IS_FIXED", unique = false, nullable = true, length = 5)
	private String isFixed;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 借款人身份证号 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 借款人电话号码 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;
	
	/** 工作单位名称 **/
	@Column(name = "WORK_UNIT", unique = false, nullable = true, length = 80)
	private String workUnit;
	
	/** 经营范围 **/
	@Column(name = "NAT_BUSI", unique = false, nullable = true, length = 500)
	private String natBusi;
	
	/** 风险分类 **/
	@Column(name = "RISK_CLASS", unique = false, nullable = true, length = 5)
	private String riskClass;
	
	/** 风险预警等级 **/
	@Column(name = "RISK_LEVEL", unique = false, nullable = true, length = 5)
	private String riskLevel;
	
	/** 客户信用评级 **/
	@Column(name = "CREDIT_RANK", unique = false, nullable = true, length = 5)
	private String creditRank;
	
	/** 具体说明 **/
	@Column(name = "CREDIT_RANK_REMARK", unique = false, nullable = true, length = 65535)
	private String creditRankRemark;
	
	/** 频率修改信息 **/
	@Column(name = "FREQ_MODIFY_INFO", unique = false, nullable = true, length = 65535)
	private String freqModifyInfo;
	
	/** 性别 **/
	@Column(name = "SEX", unique = false, nullable = true, length = 5)
	private String sex;
	
	/** 婚姻状况 **/
	@Column(name = "MAR_STATUS", unique = false, nullable = true, length = 5)
	private String marStatus;
	
	/** 现居地址 **/
	@Column(name = "RESI_ADDR", unique = false, nullable = true, length = 200)
	private String resiAddr;
	
	/** 年收入 **/
	@Column(name = "YEARN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearn;
	
	/** 职务 **/
	@Column(name = "DUTY", unique = false, nullable = true, length = 5)
	private String duty;
	
	/** 单位具体地址 **/
	@Column(name = "UNIT_ADDR", unique = false, nullable = true, length = 200)
	private String unitAddr;
	
	/** 单位电话 **/
	@Column(name = "UNIT_PHONE", unique = false, nullable = true, length = 20)
	private String unitPhone;
	
	/** 配偶客户编号 **/
	@Column(name = "SPOUSE_CUS_ID", unique = false, nullable = true, length = 40)
	private String spouseCusId;
	
	/** 配偶客户名称 **/
	@Column(name = "SPOUSE_CUS_NAME", unique = false, nullable = true, length = 80)
	private String spouseCusName;
	
	/** 配偶身份证号 **/
	@Column(name = "SPOUSE_CERT_CODE", unique = false, nullable = true, length = 20)
	private String spouseCertCode;
	
	/** 配偶联系电话 **/
	@Column(name = "SPOUSE_PHONE", unique = false, nullable = true, length = 20)
	private String spousePhone;
	
	/** 配偶现居地址 **/
	@Column(name = "SPOUSE_RESI_ADDR", unique = false, nullable = true, length = 80)
	private String spouseResiAddr;
	
	/** 配偶年收入 **/
	@Column(name = "SPOUSE_YEARN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal spouseYearn;
	
	/** 配偶工作单位名称 **/
	@Column(name = "SPOUSE_WORK_UNIT", unique = false, nullable = true, length = 200)
	private String spouseWorkUnit;
	
	/** 配偶职务 **/
	@Column(name = "SPOUSE_DUTY", unique = false, nullable = true, length = 5)
	private String spouseDuty;
	
	/** 配偶单位具体地址 **/
	@Column(name = "SPOUSE_UNIT_ADDR", unique = false, nullable = true, length = 200)
	private String spouseUnitAddr;
	
	/** 配偶单位电话 **/
	@Column(name = "SPOUSE_UNIT_PHONE", unique = false, nullable = true, length = 20)
	private String spouseUnitPhone;

	/** 借款人综合财务状况评价 **/
	@Column(name = "FINA_ANALY", unique = false, nullable = true, length = 65535)
	private String finaAnaly;

	/** 是否对公客户 **/
	@Column(name = "IS_COM", unique = false, nullable = true, length = 5)
	private String isCom;

	/**
	 * @param isCom
	 */
	public void setIsCom(String isCom) {
		this.isCom = isCom;
	}

	/**
	 * @return isCom
	 */
	public String getIsCom() {
		return this.isCom;
	}

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param operAddrAct
	 */
	public void setOperAddrAct(String operAddrAct) {
		this.operAddrAct = operAddrAct;
	}
	
    /**
     * @return operAddrAct
     */
	public String getOperAddrAct() {
		return this.operAddrAct;
	}
	
	/**
	 * @param ownshipStr
	 */
	public void setOwnshipStr(String ownshipStr) {
		this.ownshipStr = ownshipStr;
	}
	
    /**
     * @return ownshipStr
     */
	public String getOwnshipStr() {
		return this.ownshipStr;
	}
	
	/**
	 * @param mainOptScp
	 */
	public void setMainOptScp(String mainOptScp) {
		this.mainOptScp = mainOptScp;
	}
	
    /**
     * @return mainOptScp
     */
	public String getMainOptScp() {
		return this.mainOptScp;
	}
	
	/**
	 * @param buildDate
	 */
	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate;
	}
	
    /**
     * @return buildDate
     */
	public String getBuildDate() {
		return this.buildDate;
	}
	
	/**
	 * @param fjobNum
	 */
	public void setFjobNum(Integer fjobNum) {
		this.fjobNum = fjobNum;
	}
	
    /**
     * @return fjobNum
     */
	public Integer getFjobNum() {
		return this.fjobNum;
	}
	
	/**
	 * @param regiCapAmt
	 */
	public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
		this.regiCapAmt = regiCapAmt;
	}
	
    /**
     * @return regiCapAmt
     */
	public java.math.BigDecimal getRegiCapAmt() {
		return this.regiCapAmt;
	}
	
	/**
	 * @param paidCapAmt
	 */
	public void setPaidCapAmt(java.math.BigDecimal paidCapAmt) {
		this.paidCapAmt = paidCapAmt;
	}
	
    /**
     * @return paidCapAmt
     */
	public java.math.BigDecimal getPaidCapAmt() {
		return this.paidCapAmt;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass;
	}
	
    /**
     * @return tradeClass
     */
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param legalRepresent
	 */
	public void setLegalRepresent(String legalRepresent) {
		this.legalRepresent = legalRepresent;
	}
	
    /**
     * @return legalRepresent
     */
	public String getLegalRepresent() {
		return this.legalRepresent;
	}
	
	/**
	 * @param realOperCusName
	 */
	public void setRealOperCusName(String realOperCusName) {
		this.realOperCusName = realOperCusName;
	}
	
    /**
     * @return realOperCusName
     */
	public String getRealOperCusName() {
		return this.realOperCusName;
	}
	
	/**
	 * @param corpQlty
	 */
	public void setCorpQlty(String corpQlty) {
		this.corpQlty = corpQlty;
	}
	
    /**
     * @return corpQlty
     */
	public String getCorpQlty() {
		return this.corpQlty;
	}
	
	/**
	 * @param isDataChg
	 */
	public void setIsDataChg(String isDataChg) {
		this.isDataChg = isDataChg;
	}
	
    /**
     * @return isDataChg
     */
	public String getIsDataChg() {
		return this.isDataChg;
	}
	
	/**
	 * @param dataChgExpl
	 */
	public void setDataChgExpl(String dataChgExpl) {
		this.dataChgExpl = dataChgExpl;
	}
	
    /**
     * @return dataChgExpl
     */
	public String getDataChgExpl() {
		return this.dataChgExpl;
	}
	
	/**
	 * @param isRealpro
	 */
	public void setIsRealpro(String isRealpro) {
		this.isRealpro = isRealpro;
	}
	
    /**
     * @return isRealpro
     */
	public String getIsRealpro() {
		return this.isRealpro;
	}
	
	/**
	 * @param isProperty
	 */
	public void setIsProperty(String isProperty) {
		this.isProperty = isProperty;
	}
	
    /**
     * @return isProperty
     */
	public String getIsProperty() {
		return this.isProperty;
	}
	
	/**
	 * @param isFixed
	 */
	public void setIsFixed(String isFixed) {
		this.isFixed = isFixed;
	}
	
    /**
     * @return isFixed
     */
	public String getIsFixed() {
		return this.isFixed;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param workUnit
	 */
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}
	
    /**
     * @return workUnit
     */
	public String getWorkUnit() {
		return this.workUnit;
	}
	
	/**
	 * @param natBusi
	 */
	public void setNatBusi(String natBusi) {
		this.natBusi = natBusi;
	}
	
    /**
     * @return natBusi
     */
	public String getNatBusi() {
		return this.natBusi;
	}
	
	/**
	 * @param riskClass
	 */
	public void setRiskClass(String riskClass) {
		this.riskClass = riskClass;
	}
	
    /**
     * @return riskClass
     */
	public String getRiskClass() {
		return this.riskClass;
	}
	
	/**
	 * @param riskLevel
	 */
	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel;
	}
	
    /**
     * @return riskLevel
     */
	public String getRiskLevel() {
		return this.riskLevel;
	}
	
	/**
	 * @param creditRank
	 */
	public void setCreditRank(String creditRank) {
		this.creditRank = creditRank;
	}
	
    /**
     * @return creditRank
     */
	public String getCreditRank() {
		return this.creditRank;
	}
	
	/**
	 * @param creditRankRemark
	 */
	public void setCreditRankRemark(String creditRankRemark) {
		this.creditRankRemark = creditRankRemark;
	}
	
    /**
     * @return creditRankRemark
     */
	public String getCreditRankRemark() {
		return this.creditRankRemark;
	}
	
	/**
	 * @param freqModifyInfo
	 */
	public void setFreqModifyInfo(String freqModifyInfo) {
		this.freqModifyInfo = freqModifyInfo;
	}
	
    /**
     * @return freqModifyInfo
     */
	public String getFreqModifyInfo() {
		return this.freqModifyInfo;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
    /**
     * @return sex
     */
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}
	
    /**
     * @return marStatus
     */
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param resiAddr
	 */
	public void setResiAddr(String resiAddr) {
		this.resiAddr = resiAddr;
	}
	
    /**
     * @return resiAddr
     */
	public String getResiAddr() {
		return this.resiAddr;
	}
	
	/**
	 * @param yearn
	 */
	public void setYearn(java.math.BigDecimal yearn) {
		this.yearn = yearn;
	}
	
    /**
     * @return yearn
     */
	public java.math.BigDecimal getYearn() {
		return this.yearn;
	}
	
	/**
	 * @param duty
	 */
	public void setDuty(String duty) {
		this.duty = duty;
	}
	
    /**
     * @return duty
     */
	public String getDuty() {
		return this.duty;
	}
	
	/**
	 * @param unitAddr
	 */
	public void setUnitAddr(String unitAddr) {
		this.unitAddr = unitAddr;
	}
	
    /**
     * @return unitAddr
     */
	public String getUnitAddr() {
		return this.unitAddr;
	}
	
	/**
	 * @param unitPhone
	 */
	public void setUnitPhone(String unitPhone) {
		this.unitPhone = unitPhone;
	}
	
    /**
     * @return unitPhone
     */
	public String getUnitPhone() {
		return this.unitPhone;
	}
	
	/**
	 * @param spouseCusId
	 */
	public void setSpouseCusId(String spouseCusId) {
		this.spouseCusId = spouseCusId;
	}
	
    /**
     * @return spouseCusId
     */
	public String getSpouseCusId() {
		return this.spouseCusId;
	}
	
	/**
	 * @param spouseCusName
	 */
	public void setSpouseCusName(String spouseCusName) {
		this.spouseCusName = spouseCusName;
	}
	
    /**
     * @return spouseCusName
     */
	public String getSpouseCusName() {
		return this.spouseCusName;
	}
	
	/**
	 * @param spouseCertCode
	 */
	public void setSpouseCertCode(String spouseCertCode) {
		this.spouseCertCode = spouseCertCode;
	}
	
    /**
     * @return spouseCertCode
     */
	public String getSpouseCertCode() {
		return this.spouseCertCode;
	}
	
	/**
	 * @param spousePhone
	 */
	public void setSpousePhone(String spousePhone) {
		this.spousePhone = spousePhone;
	}
	
    /**
     * @return spousePhone
     */
	public String getSpousePhone() {
		return this.spousePhone;
	}
	
	/**
	 * @param spouseResiAddr
	 */
	public void setSpouseResiAddr(String spouseResiAddr) {
		this.spouseResiAddr = spouseResiAddr;
	}
	
    /**
     * @return spouseResiAddr
     */
	public String getSpouseResiAddr() {
		return this.spouseResiAddr;
	}
	
	/**
	 * @param spouseYearn
	 */
	public void setSpouseYearn(java.math.BigDecimal spouseYearn) {
		this.spouseYearn = spouseYearn;
	}
	
    /**
     * @return spouseYearn
     */
	public java.math.BigDecimal getSpouseYearn() {
		return this.spouseYearn;
	}
	
	/**
	 * @param spouseWorkUnit
	 */
	public void setSpouseWorkUnit(String spouseWorkUnit) {
		this.spouseWorkUnit = spouseWorkUnit;
	}
	
    /**
     * @return spouseWorkUnit
     */
	public String getSpouseWorkUnit() {
		return this.spouseWorkUnit;
	}
	
	/**
	 * @param spouseDuty
	 */
	public void setSpouseDuty(String spouseDuty) {
		this.spouseDuty = spouseDuty;
	}
	
    /**
     * @return spouseDuty
     */
	public String getSpouseDuty() {
		return this.spouseDuty;
	}
	
	/**
	 * @param spouseUnitAddr
	 */
	public void setSpouseUnitAddr(String spouseUnitAddr) {
		this.spouseUnitAddr = spouseUnitAddr;
	}
	
    /**
     * @return spouseUnitAddr
     */
	public String getSpouseUnitAddr() {
		return this.spouseUnitAddr;
	}
	
	/**
	 * @param spouseUnitPhone
	 */
	public void setSpouseUnitPhone(String spouseUnitPhone) {
		this.spouseUnitPhone = spouseUnitPhone;
	}
	
    /**
     * @return spouseUnitPhone
     */
	public String getSpouseUnitPhone() {
		return this.spouseUnitPhone;
	}


	public String getFinaAnaly() {
		return finaAnaly;
	}

	public void setFinaAnaly(String finaAnaly) {
		this.finaAnaly = finaAnaly;
	}
}
