/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0007.Cmisbatch0007ReqDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.PspKHYJDto;
import cn.com.yusys.yusp.dto.RiskTaskAndAdjustDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.repository.mapper.RiskTaskListMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.batch.cmisbatch0007.CmisBatch0007Service;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.function.Supplier;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskTaskListService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-25 03:38:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RiskTaskListService {
    private final Logger log = LoggerFactory.getLogger(RiskTaskListService.class);

    @Autowired
    private RiskTaskListMapper riskTaskListMapper;
    @Autowired
    private RiskClassChgAppService riskClassChgAppService;
    @Autowired
    private CmisBatch0007Service cmisBatch0007Service;
    @Autowired
    private RiskDebitInfoService riskDebitInfoService;
    @Autowired
    private RiskDebitAnalyService riskDebitAnalyService;
    @Autowired
    private RiskGuarntrListService riskGuarntrListService;
    @Autowired
    private RiskPldimnListService riskPldimnListService;
    @Autowired
    private RiskRepayAnalyService riskRepayAnalyService;
    @Autowired
    private RiskLoanAnalyService riskLoanAnalyService;
    @Autowired
    private RiskOperAnalyService riskOperAnalyService;
    @Autowired
    private RiskIncomeAnalyService riskIncomeAnalyService;
    @Autowired
    private RiskFinaAnalyService riskFinaAnalyService;
    @Autowired
    private RiskNonFinaAnalyService riskNonFinaAnalyService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskTaskList selectByPrimaryKey(String pkId) {
        return riskTaskListMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: queryRiskTaskList
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskTaskList queryRiskTaskList(RiskTaskList riskTaskList) {
        RiskTaskList riskTaskListNew = riskTaskListMapper.queryRiskTaskList(riskTaskList);
        RiskTaskList lastTask = riskTaskListMapper.queryLastRiskTask(riskTaskListNew.getCheckType(), riskTaskListNew.getCusId());
        if (null != lastTask) {
            riskTaskListNew.setLastCheckDate(lastTask.getCheckDate());// 上次分类时间
            riskTaskListNew.setLastTaskNo(lastTask.getTaskNo());// 上次分类编号
            riskTaskListNew.setLastClassRst(lastTask.getManualClass());// 上级五级分类
            riskTaskListNew.setLastTenClassRst(lastTask.getManualTenClass());// 上次十级分类
        }
        if (Objects.equals(riskTaskListNew.getCheckType(), "3") && StringUtils.isEmpty(riskTaskListNew.getLastClassRst())) {
            // 个人消费性风险分类
            User userInfo = SessionUtils.getUserInformation();
            if (null != userInfo && Objects.equals(userInfo.getOrg().getCode(), "005004")) {
                // 网络金融总部 新发风险分类任务无上次分类结果时取借据中最低五级分类结果
                String lowFiveClass = riskDebitInfoService.getLowFiveClassByTaskNo(riskTaskListNew.getTaskNo());
                riskTaskListNew.setLastClassRst(lowFiveClass);
            }
        }
        return riskTaskListNew;
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RiskTaskList> selectAll(QueryModel model) {
        List<RiskTaskList> records = (List<RiskTaskList>) riskTaskListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RiskTaskList record) {
        // 获取用户上次已分析的分类信息
        RiskTaskList lastTask = riskTaskListMapper.queryLastRiskTask(record.getCheckType(), record.getCusId());
        if (null != lastTask) {
            record.setLastCheckDate(lastTask.getCheckDate());// 上次分类时间
            record.setLastTaskNo(lastTask.getTaskNo());// 上次分类编号
            record.setLastClassRst(lastTask.getManualClass());// 上级五级分类
            record.setLastTenClassRst(lastTask.getManualTenClass());// 上次十级分类
        }
        // 登记人，登记机构，登记时间
        String inputId = "", inputBrId = "", inputDate = "";
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            inputId = userInfo.getLoginCode();// 登记人
            inputBrId = userInfo.getOrg().getCode();// 登记机构
            inputDate = stringRedisTemplate.opsForValue().get("openDay");// 登记时间
        }
        record.setInputId(inputId);
        record.setInputBrId(inputBrId);
        record.setInputDate(inputDate);
        return riskTaskListMapper.insert(record);
    }

    /**
     * 调用加工任务,生成风险分类子表数据
     *
     * @author jijian_yx
     * @date 2021/7/15 21:20
     **/
    public Integer insertSubData(RiskTaskList record) {
        log.info("风险分类新增向导异步生成子表数据开始,任务编号[{}]", record.getTaskNo());
        Supplier<Cmisbatch0007ReqDto> supplier = Cmisbatch0007ReqDto::new;
        Cmisbatch0007ReqDto cmisbatch0007ReqDto = supplier.get();
        cmisbatch0007ReqDto.setTaskNo(record.getTaskNo());
        cmisbatch0007ReqDto.setCheckType(record.getCheckType());
        cmisbatch0007ReqDto.setCusId(record.getCusId());
        cmisbatch0007ReqDto.setCusName(record.getCusName());
        cmisbatch0007ReqDto.setTaskStartDt(record.getTaskStartDt());
        cmisBatch0007Service.cmisbatch0007(cmisbatch0007ReqDto);
        return 1;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RiskTaskList record) {
        return riskTaskListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RiskTaskList record) {
        // 更新人，更新机构，更新时间
        String updId = "", updBrId = "", updDate = "";
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 更新人
            updId = userInfo.getLoginCode();
            // 更新机构
            updBrId = userInfo.getOrg().getCode();
            // 更新时间
            updDate = stringRedisTemplate.opsForValue().get("openDay");
        }
        record.setUpdId(updId);
        record.setUpdBrId(updBrId);
        record.setUpdDate(updDate);
        return riskTaskListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RiskTaskList record) {
        // 更新人，更新机构，更新时间
        String updId = "", updBrId = "", updDate = "";
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 更新人
            updId = userInfo.getLoginCode();
            // 更新机构
            updBrId = userInfo.getOrg().getCode();
            // 更新时间
            updDate = stringRedisTemplate.opsForValue().get("openDay");
        }
        record.setUpdId(updId);
        record.setUpdBrId(updBrId);
        record.setUpdDate(updDate);
        return riskTaskListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        int result = 0;
        RiskTaskList riskTaskList = riskTaskListMapper.selectByPrimaryKey(pkId);
        if (null != riskTaskList) {
            if (Objects.equals(riskTaskList.getApproveStatus(), CmisCommonConstants.WF_STATUS_992)) {
                riskTaskList.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                riskTaskListMapper.updateByPrimaryKeySelective(riskTaskList);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(riskTaskList.getTaskNo());
                result = 1;
            } else if (Objects.equals(riskTaskList.getApproveStatus(), CmisCommonConstants.WF_STATUS_000)) {
                result = riskTaskListMapper.deleteByPrimaryKey(pkId);
            }
        }
        return result;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return riskTaskListMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RiskTaskList> selectByModel(QueryModel model) {
        List<RiskTaskList> riskTaskListList = null;
        // 检查类型
        String checkType = "";
        // 审批状态
        String approveStatus = "";
        if (!StringUtils.isEmpty(model.getCondition().get("checkType"))) {
            checkType = model.getCondition().get("checkType").toString();
            List<String> checkTypeList = Arrays.asList(checkType.split(","));
            model.getCondition().put("checkTypeList", checkTypeList);
        }
        if (!StringUtils.isEmpty(model.getCondition().get("approveStatus"))) {
            approveStatus = model.getCondition().get("approveStatus").toString();
            // 待检查任务展示待发起、退回、审批中数据
            List<String> approveStatusList = Arrays.asList(approveStatus.split(","));
            model.getCondition().put("approveStatusList", approveStatusList);
        }
        PageHelper.startPage(model.getPage(), model.getSize());
        riskTaskListList = riskTaskListMapper.selectByModel(model);
        PageHelper.clearPage();
        return riskTaskListList;
    }

    /**
     * @方法名称: queryRiskClassByCusId
     * @方法描述: 根据客户号查询风险分类信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public HashMap<String, String> queryRiskClassByCusId(String cusId) {
        RiskTaskList riskTaskList = new RiskTaskList();
        riskTaskList.setCusId(cusId);
        //审批通过
        riskTaskList.setApproveStatus("997");
        riskTaskList = riskTaskListMapper.queryRiskTaskList(riskTaskList);
        HashMap<String, String> classMap = new HashMap<String, String>();
        classMap.put("cusId", cusId);
        if (riskTaskList == null) {
            classMap.put("riskClass", "");
            return classMap;
        }
        String riskClass;
        //对公客户取十级分类
        if ("1".equals(riskTaskList.getCusCatalog())) {
            //系统自动
            if ("3".equals(riskTaskList.getTaskType())) {
                classMap.put("riskClass", riskTaskList.getAutoTenClass());
            } else { //人工新增、系统生成
                classMap.put("riskClass", riskTaskList.getManualTenClass());
            }
        } else { //个人客户取五级分类
            //系统自动
            if ("3".equals(riskTaskList.getTaskType())) {
                classMap.put("riskClass", riskTaskList.getAutoClass());
            } else { //人工新增、系统生成
                classMap.put("riskClass", riskTaskList.getManualClass());
            }
        }
        return classMap;
    }

    /**
     * @方法名称: queryTaskAndAdjust
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskTaskAndAdjustDto queryTaskAndAdjust(RiskTaskAndAdjustDto riskTaskAndAdjustDto) {

        RiskClassChgApp riskClassChgApp = new RiskClassChgApp();
        riskClassChgApp.setSerno(riskTaskAndAdjustDto.getSerno());
        riskClassChgApp = riskClassChgAppService.queryRiskClassChgApp(riskClassChgApp);
        if (null != riskClassChgApp) {
            org.springframework.beans.BeanUtils.copyProperties(riskClassChgApp, riskTaskAndAdjustDto);
        }
        RiskTaskList riskTaskList = new RiskTaskList();
        riskTaskList.setTaskNo(riskTaskAndAdjustDto.getTaskNo());
        riskTaskList = riskTaskListMapper.queryRiskTaskList(riskTaskList);
        if (null != riskTaskList) {
            org.springframework.beans.BeanUtils.copyProperties(riskTaskList, riskTaskAndAdjustDto);
            if (null != riskClassChgApp) {
                riskTaskAndAdjustDto.setPkId(riskClassChgApp.getPkId());
                riskTaskAndAdjustDto.setInputId(riskClassChgApp.getInputId());
                riskTaskAndAdjustDto.setInputBrId(riskClassChgApp.getInputBrId());
                riskTaskAndAdjustDto.setInputDate(riskClassChgApp.getInputDate());
            }
        }
        riskTaskAndAdjustDto.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        return riskTaskAndAdjustDto;
    }

    /**
     * 获取sequence流水号
     **/
    public String getSequences(String type) {
        //TODO 后续改为getSequenceTemplate获取
        String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.PSP_TASK_NO, new HashMap<>());
//        String serno = sequenceTemplateClient.getSequenceTemplate(type, new HashMap<>());
        return serno;
    }

    /**
     * 根据任务编号更新
     *
     * @author jijian_yx
     * @date 2021/7/3 18:23
     **/
    public int updateByTaskNoSelective(RiskTaskList record) {
        // 更新人，更新机构，更新时间
        String updId = "", updBrId = "", updDate = "";
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 更新人
            updId = userInfo.getLoginCode();
            // 更新机构
            updBrId = userInfo.getOrg().getCode();
            // 更新时间
            updDate = stringRedisTemplate.opsForValue().get("openDay");
        }
        record.setUpdId(updId);
        record.setUpdBrId(updBrId);
        record.setUpdDate(updDate);
        return riskTaskListMapper.updateByTaskNoSelective(record);
    }

    /**
     * @方法名称: queryRiskTaskListByCusId
     * @方法描述: 根据客户编号查询风险分类信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskTaskList queryRiskTaskListByCusId(String cusId) {
        return riskTaskListMapper.queryRiskTaskListByCusId(cusId);
    }

    /**
     * @方法名称: queryRiskTaskListByCusId
     * @方法描述: 根据客户编号查询贷后检查信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RiskTaskList queryPspCheckRstByCusId(String cusId) {
        return riskTaskListMapper.queryPspCheckRstByCusId(cusId);
    }

    /**
     * 机评计算分值
     *
     * @author jijian_yx
     * @date 2021/8/6 14:07
     **/
    public JSONObject calculatePoint(String pkId) {
        JSONObject jsonObject = new JSONObject();
        RiskTaskList riskTaskList = riskTaskListMapper.selectByPrimaryKey(pkId);
        if (null != riskTaskList) {
            jsonObject = calculate(riskTaskList.getTaskNo(), riskTaskList.getCheckType());
        }
        return jsonObject;
    }

    private JSONObject calculate(String taskNo, String checkType) {
        int point = 0;// 机评评分
        int guarPoint = 0;// 担保评分
        String reason = "";
        // 授信资料
        int point1 = calculateRiskDebitInfo(taskNo, 0);
        log.info("任务编号:[{}]，授信资料得分:[{}]", taskNo, point1);
        point = comparePoint(point, point1);
        // 担保人分析
        int point2 = calculateRiskGuarntrList(taskNo, 0);
        log.info("任务编号:[{}]，担保人分析得分:[{}]", taskNo, point2);
        guarPoint = comparePoint(guarPoint, point2);
        // 担保品信息
        int point3 = calculateRiskPldimnList(taskNo, 0);
        log.info("任务编号:[{}]，担保品信息得分:[{}]", taskNo, point3);
        guarPoint = comparePoint(guarPoint, point3);
        if ("1".equals(checkType)) {
            /** 公司客户 **/
            // 借款人分析
            int point4 = calculateRiskDebitAnaly(taskNo, 0);
            log.info("任务编号:[{}]，借款人分析得分:[{}]", taskNo, point4);
            point = comparePoint(point, point4);
            // 客户信息
            int point5 = calculateRiskOperAnaly(taskNo, 0, checkType);
            log.info("任务编号:[{}]，客户信息得分:[{}]", taskNo, point5);
            point = comparePoint(point, point5);
            // 影响偿还因素分析
            int point6 = calculateRiskRepayAnaly(taskNo, 0);
            log.info("任务编号:[{}]，影响偿还因素分析得分:[{}]", taskNo, point6);
            point = comparePoint(point, point6);
            // 财务分析
            int point7 = calculateRiskFinaAnaly(taskNo, 0);
            log.info("任务编号:[{}]，财务分析得分:[{}]", taskNo, point7);
            point = comparePoint(point, point7);
            // 非财务分析
            int point8 = calculateRiskNonFinaAnaly(taskNo, 0);
            log.info("任务编号:[{}]，非财务分析得分:[{}]", taskNo, point8);
            point = comparePoint(point, point8);
        } else if ("2".equals(checkType)) {
            /** 个人经营性 **/
            // 经营情况分析
            int point9 = calculateRiskOperAnaly(taskNo, 0, checkType);
            log.info("任务编号:[{}]，经营情况分析得分:[{}]", taskNo, point9);
            point = comparePoint(point, point9);
            // 自然人其他分析
            int point10 = calculateRiskDebitAnaly2(taskNo, 0);
            log.info("任务编号:[{}]，自然人其他分析得分:[{}]", taskNo, point10);
            point = comparePoint(point, point10);
        } else if ("3".equals(checkType)) {
            /** 个人消费性 **/
            // 借款人收入情况分析
            int point11 = calculateRiskIncomeAnaly(taskNo, 0);
            log.info("任务编号:[{}]，借款人收入情况分析得分:[{}]", taskNo, point11);
            point = comparePoint(point, point11);
            // 自然人其他分析
            int point12 = calculateRiskDebitAnaly2(taskNo, 0);
            log.info("任务编号:[{}]，自然人其他分析得分:[{}]", taskNo, point12);
            point = comparePoint(point, point12);
        }
        log.info("任务编号:[{}]，担保初分得分:[{}]", taskNo, guarPoint);
        log.info("任务编号:[{}]，机评初分得分:[{}]", taskNo, point);
        switch (point) {
            case 10:
                reason = "分类结果为正常";
                break;
            case 20:
                reason = "分类结果为关注";
                break;
            case 30:
                reason = "分类结果为次级";
                break;
            case 40:
                reason = "分类结果为可疑";
                break;
            case 50:
                reason = "分类结果为损失";
                break;
        }
        if (guarPoint > 0) {
            if (guarPoint == 10) {
                // 担保评分为正常10,机评结果上调一级
                reason += "，担保较好，上调一级";
                point = point == 10 ? point : point - 10;
            } else if (guarPoint == 20) {
                // 担保评分为关注20,机评结果不调整
                reason += "，担保一般，不调级";
            } else if (guarPoint == 30 || guarPoint == 40 || guarPoint == 50) {
                // 担保评分为次级30、可疑40、损失50，机评结果下调一级
                reason += "，担保较差，下调一级";
                point = point == 50 ? point : point + 10;
            }
        } else if (!StringUtils.isEmpty(reason)) {
            reason += "，担保一般，不调级";
        }
        log.info("任务编号:[{}]，机评调整理由:[{}]", taskNo, reason);
        log.info("任务编号:[{}]，担保最终得分:[{}]", taskNo, guarPoint);
        log.info("任务编号:[{}]，机评最终得分:[{}]", taskNo, point);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("point", point);
        jsonObject.put("reason", reason);
        return jsonObject;
    }

    /**
     * 授信资料
     **/
    private int calculateRiskDebitInfo(String taskNo, int point) {
        QueryModel queryModel = new QueryModel();
        queryModel.getCondition().put("taskNo", taskNo);
        List<RiskDebitInfo> list = riskDebitInfoService.selectByModel(queryModel);
        if (null != list && list.size() > 0) {
            for (RiskDebitInfo riskDebitInfo : list) {
                String billNo = riskDebitInfo.getBillNo();
                RiskLoanAnaly riskLoanAnaly = new RiskLoanAnaly();
                riskLoanAnaly.setTaskNo(taskNo);
                riskLoanAnaly.setBillNo(billNo);
                riskLoanAnaly = riskLoanAnalyService.queryRiskLoanAnaly(riskLoanAnaly);
                if (null != riskLoanAnaly) {
                    String isNormalAftLmt = riskLoanAnaly.getIsNormalAftLmt();// 授信发生后用途是否正常 STD_RISK_USE_NEG
                    if ("1".equals(isNormalAftLmt)) {
                        point = comparePoint(point, 10);
                    } else if ("2".equals(isNormalAftLmt)) {
                        point = comparePoint(point, 20);
                    } else if ("3".equals(isNormalAftLmt)) {
                        point = comparePoint(point, 30);
                    }
                    String isRecLoan = riskLoanAnaly.getIsRecLoan();// 是否重组贷款 STD_RISK_REC_LOAN
                    if ("1".equals(isRecLoan)) {
                        point = comparePoint(point, 10);
                    } else if ("2".equals(isRecLoan)) {
                        point = comparePoint(point, 30);
                    } else if ("3".equals(isRecLoan)) {
                        point = comparePoint(point, 40);
                    }
                    String preserveState = riskLoanAnaly.getPreserveState();// 保全状态 STD_RISK_PRESERVE_STATE
                    if ("1".equals(preserveState)) {
                        point = comparePoint(point, 10);
                    } else if ("2".equals(preserveState)) {
                        point = comparePoint(point, 30);
                    } else if ("3".equals(preserveState)) {
                        point = comparePoint(point, 40);
                    } else if ("4".equals(preserveState)) {
                        point = comparePoint(point, 40);
                    } else if ("5".equals(preserveState)) {
                        point = comparePoint(point, 40);
                    } else if ("6".equals(preserveState)) {
                        point = comparePoint(point, 50);
                    }
                    String isExt = riskLoanAnaly.getIsExt();// 是否展期 STD_RISK_EXT_LOAN
                    if ("1".equals(isExt)) {
                        point = comparePoint(point, 10);
                    } else if ("2".equals(isExt)) {
                        point = comparePoint(point, 20);
                    } else if ("3".equals(isExt)) {
                        point = comparePoint(point, 30);
                    }
                    String isRefinance = riskLoanAnaly.getIsRefinance();// 是否借新还旧 STD_RISK_REFINANCE
                    if ("1".equals(isRefinance)) {
                        point = comparePoint(point, 10);
                    } else if ("2".equals(isRefinance)) {
                        point = comparePoint(point, 10);
                    } else if ("3".equals(isRefinance)) {
                        point = comparePoint(point, 30);
                    }
                    String isNegFactor = riskLoanAnaly.getIsNegFactor();// 有无影响该笔授信偿还的不利因素 STD_RISK_REPAY_NEG
                    if ("1".equals(isNegFactor)) {
                        point = comparePoint(point, 10);
                    } else if ("2".equals(isNegFactor)) {
                        // point = comparePoint(point, 0);
                    } else if ("3".equals(isNegFactor)) {
                        point = comparePoint(point, 30);
                    } else if ("4".equals(isNegFactor)) {
                        point = comparePoint(point, 30);
                    } else if ("5".equals(isNegFactor)) {
                        point = comparePoint(point, 30);
                    } else if ("6".equals(isNegFactor)) {
                        point = comparePoint(point, 30);
                    } else if ("7".equals(isNegFactor)) {
                        point = comparePoint(point, 20);
                    }
                }
            }
        }
        return point;
    }

    /**
     * 客户信息/经营情况分析
     **/
    private int calculateRiskOperAnaly(String taskNo, int point, String checkType) {
        RiskOperAnaly riskOperAnaly = new RiskOperAnaly();
        riskOperAnaly.setTaskNo(taskNo);
        riskOperAnaly = riskOperAnalyService.queryRiskOperAnaly(riskOperAnaly);
        if (null != riskOperAnaly) {
            if (!"3".equals(checkType)) {
                // 对公客户 或 个人经营性
                String corpOperSitu = Optional.ofNullable(riskOperAnaly.getCorpOperSitu()).orElse("");// 经营情况 STD_RISK_CORP_OPER_SITU
                if (corpOperSitu.contains("01")) {
                    point = comparePoint(point, 10);
                }
                if (corpOperSitu.contains("02")) {
                    point = comparePoint(point, 10);
                }
                if (corpOperSitu.contains("03")) {
                    point = comparePoint(point, 10);
                }
                if (corpOperSitu.contains("04")) {
                    point = comparePoint(point, 20);
                }
                if (corpOperSitu.contains("05")) {
                    point = comparePoint(point, 20);
                }
                if (corpOperSitu.contains("06")) {
                    point = comparePoint(point, 20);
                }
                if (corpOperSitu.contains("07")) {
                    // point = comparePoint(point, 0);
                }
                if (corpOperSitu.contains("08")) {
                    point = comparePoint(point, 20);
                }
                if (corpOperSitu.contains("09")) {
                    point = comparePoint(point, 20);
                }
                if (corpOperSitu.contains("10")) {
                    point = comparePoint(point, 20);
                }
                if (corpOperSitu.contains("11")) {
                    point = comparePoint(point, 30);
                }
                if (corpOperSitu.contains("12")) {
                    point = comparePoint(point, 30);
                }
                if (corpOperSitu.contains("13")) {
                    point = comparePoint(point, 40);
                }
                if (corpOperSitu.contains("14")) {
                    point = comparePoint(point, 30);
                }
                if (corpOperSitu.contains("15")) {
                    point = comparePoint(point, 40);
                }
                if (corpOperSitu.contains("16")) {
                    point = comparePoint(point, 30);
                }
                if (corpOperSitu.contains("17")) {
                    point = comparePoint(point, 30);
                }
                if (corpOperSitu.contains("18")) {
                    point = comparePoint(point, 40);
                }
                if (corpOperSitu.contains("19")) {
                    point = comparePoint(point, 40);
                }
                if (corpOperSitu.contains("20")) {
                    point = comparePoint(point, 40);
                }
                if (corpOperSitu.contains("21")) {
                    point = comparePoint(point, 40);
                }
                if (corpOperSitu.contains("22")) {
                    point = comparePoint(point, 50);
                }
                if (corpOperSitu.contains("23")) {
                    point = comparePoint(point, 50);
                }
            }

            if ("1".equals(checkType)) {
                // 对公客户
                String n1yOperTrend = riskOperAnaly.getN1yOperTrend();// 预测以后1年内经营趋势 STD_RISK_OPER_TREND
                if ("1".equals(n1yOperTrend)) {
                    point = comparePoint(point, 10);
                } else if ("2".equals(n1yOperTrend)) {
                    point = comparePoint(point, 10);
                } else if ("3".equals(n1yOperTrend)) {
                    point = comparePoint(point, 20);
                } else if ("4".equals(n1yOperTrend)) {
                    point = comparePoint(point, 10);
                } else if ("5".equals(n1yOperTrend)) {
                    point = comparePoint(point, 20);
                } else if ("6".equals(n1yOperTrend)) {
                    point = comparePoint(point, 20);
                } else if ("7".equals(n1yOperTrend)) {
                    point = comparePoint(point, 30);
                }
            } else if ("2".equals(checkType)) {
                // 个人经营性
                String incomeChange = riskOperAnaly.getIncomeChange();// 销售收入变化趋势    STD_RISK_CHG_TREND
                if ("1".equals(incomeChange)) {
                    point = comparePoint(point, 10);
                } else if ("2".equals(incomeChange)) {
                    point = comparePoint(point, 10);
                } else if ("3".equals(incomeChange)) {
                    point = comparePoint(point, 20);
                } else if ("4".equals(incomeChange)) {
                    point = comparePoint(point, 10);
                }
                String profitChange = riskOperAnaly.getProfitChange();// 利润变动趋势  STD_RISK_CHG_TREND
                if ("1".equals(profitChange)) {
                    point = comparePoint(point, 10);
                } else if ("2".equals(profitChange)) {
                    point = comparePoint(point, 10);
                } else if ("3".equals(profitChange)) {
                    point = comparePoint(point, 20);
                } else if ("4".equals(profitChange)) {
                    point = comparePoint(point, 10);
                }
                String cashChange = riskOperAnaly.getCashChange();// 现金流量变动趋势    STD_RISK_CHG_TREND
                if ("1".equals(cashChange)) {
                    point = comparePoint(point, 10);
                } else if ("2".equals(cashChange)) {
                    point = comparePoint(point, 10);
                } else if ("3".equals(cashChange)) {
                    point = comparePoint(point, 20);
                } else if ("4".equals(cashChange)) {
                    point = comparePoint(point, 10);
                }
                String isIncomeSuffice = riskOperAnaly.getIsIncomeSuffice();// 第一还款来源是否充足  STD_RISK_INCOME_SUFFICE
                if ("1".equals(isIncomeSuffice)) {
                    point = comparePoint(point, 10);
                } else if ("2".equals(isIncomeSuffice)) {
                    point = comparePoint(point, 20);
                } else if ("3".equals(isIncomeSuffice)) {
                    point = comparePoint(point, 30);
                } else if ("4".equals(isIncomeSuffice)) {
                    point = comparePoint(point, 40);
                }
            }
        }
        return point;
    }

    /**
     * 借款人分析(对公客户)
     **/
    private int calculateRiskDebitAnaly(String taskNo, int point) {
        RiskDebitAnaly riskDebitAnaly = new RiskDebitAnaly();
        riskDebitAnaly.setTaskNo(taskNo);
        riskDebitAnaly = riskDebitAnalyService.queryRiskDebitAnaly(riskDebitAnaly);
        if (null != riskDebitAnaly) {
            String incomeChange = riskDebitAnaly.getIncomeChange();// 销售收入变化趋势 STD_RISK_CHG_TREND
            if ("1".equals(incomeChange) || "2".equals(incomeChange)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(incomeChange)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(incomeChange)) {
                point = comparePoint(point, 30);
            }
            String profitChange = riskDebitAnaly.getProfitChange();// 利润变动趋势 STD_RISK_CHG_TREND
            if ("1".equals(profitChange)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(profitChange)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(profitChange)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(profitChange)) {
                point = comparePoint(point, 30);
            }
            String cashChange = riskDebitAnaly.getCashChange();// 现金流量变动趋势 STD_RISK_CHG_TREND
            if ("1".equals(cashChange)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(cashChange)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(cashChange)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(cashChange)) {
                point = comparePoint(point, 30);
            }
            String operCashChange = riskDebitAnaly.getOperCashChange();// 经营活动现金流量变动趋势 STD_RISK_CHG_TREND
            if ("1".equals(operCashChange)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(operCashChange)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(operCashChange)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(operCashChange)) {
                point = comparePoint(point, 30);
            }
//            String isIncomeSuffice = riskDebitAnaly.getIncomeChange();// 第一还款来源是否充足 STD_RISK_INCOME_SUFFICE
//            if ("1".equals(isIncomeSuffice)) {
//                point = comparePoint(point, 10);
//            } else if ("2".equals(isIncomeSuffice)) {
//                point = comparePoint(point, 20);
//            } else if ("3".equals(isIncomeSuffice)) {
//                point = comparePoint(point, 30);
//            } else if ("4".equals(isIncomeSuffice)) {
//                point = comparePoint(point, 40);
//            }
        }
        return point;
    }

    /**
     * 自然人其他分析
     **/
    private int calculateRiskDebitAnaly2(String taskNo, int point) {
        RiskDebitAnaly riskDebitAnaly = new RiskDebitAnaly();
        riskDebitAnaly.setTaskNo(taskNo);
        riskDebitAnaly = riskDebitAnalyService.queryRiskDebitAnaly(riskDebitAnaly);
        if (null != riskDebitAnaly) {
            String isRightPurp = riskDebitAnaly.getIsRightPurp();// 是否按约定用途使用贷款 STD_ZB_YES_NO
            if ("0".equals(isRightPurp)) {
                point = comparePoint(point, 30);
            } else if ("1".equals(isRightPurp)) {
                point = comparePoint(point, 10);
            }
            String isBadAction = riskDebitAnaly.getIsBadAction();// 有无不良行为、不良嗜好 STD_ZB_YES_NO
            if ("0".equals(isBadAction)) {
                point = comparePoint(point, 10);
            } else if ("1".equals(isBadAction)) {
                point = comparePoint(point, 20);
            }
            String isBadCdtRecord = riskDebitAnaly.getIsBadCdtRecord();// 有无不良信用记录（含他行信用）
            if ("10".equals(isBadCdtRecord)) {
                point = comparePoint(point, 10);
            } else if ("20".equals(isBadCdtRecord)) {
                point = comparePoint(point, 20);
            } else if ("30".equals(isBadCdtRecord)) {
                point = comparePoint(point, 30);
            } else if ("40".equals(isBadCdtRecord)) {
                point = comparePoint(point, 40);
            } else if ("50".equals(isBadCdtRecord)) {
                point = comparePoint(point, 50);
            }
            String isFamilyStatusNormal = riskDebitAnaly.getIsFamilyStatusNormal();// 家庭状况是否正常 STD_ZB_YES_NO
            if ("0".equals(isFamilyStatusNormal)) {
                point = comparePoint(point, 20);
            } else if ("1".equals(isFamilyStatusNormal)) {
                point = comparePoint(point, 10);
            }
        }
        return point;
    }

    /**
     * 担保人分析
     **/
    private int calculateRiskGuarntrList(String taskNo, int point) {
        QueryModel queryModel = new QueryModel();
        queryModel.getCondition().put("taskNo", taskNo);
        List<RiskGuarntrList> list = riskGuarntrListService.selectByModel(queryModel);
        if (null != list && list.size() > 0) {
            for (RiskGuarntrList riskGuarntrList : list) {
                String subpayWish = riskGuarntrList.getSubpayWish();// 保证人代偿意愿 STD_RISK_SUBPAY_WISH
                if ("1".equals(subpayWish)) {
                    point = comparePoint(point, 10);
                } else if ("2".equals(subpayWish)) {
                    point = comparePoint(point, 20);
                } else if ("3".equals(subpayWish)) {
                    point = comparePoint(point, 30);
                } else if ("4".equals(subpayWish)) {
                    point = comparePoint(point, 40);
                }

                String subpayAbi = riskGuarntrList.getSubpayAbi();// 代偿能力 STD_RISK_SUBPAY_ABI
                if ("1".equals(subpayAbi)) {
                    point = comparePoint(point, 10);
                } else if ("2".equals(subpayAbi)) {
                    point = comparePoint(point, 20);
                } else if ("3".equals(subpayAbi)) {
                    point = comparePoint(point, 30);
                } else if ("4".equals(subpayAbi)) {
                    point = comparePoint(point, 40);
                }
            }
        }
        return point;
    }

    /**
     * 担保品分析
     **/
    private int calculateRiskPldimnList(String taskNo, int point) {
        QueryModel queryModel = new QueryModel();
        queryModel.getCondition().put("taskNo", taskNo);
        List<RiskPldimnList> list = riskPldimnListService.selectByModel(queryModel);
        if (null != list && list.size() > 0) {
            for (RiskPldimnList riskPldimnList : list) {
                String pldimnExeAbi = riskPldimnList.getPldimnExeAbi();// 抵（质）押品可执行能力 STD_RISK_PLDIMN_EXE_ABI
                if ("1".equals(pldimnExeAbi)) {
                    point = comparePoint(point, 10);
                } else if ("2".equals(pldimnExeAbi)) {
                    point = comparePoint(point, 20);
                } else if ("3".equals(pldimnExeAbi)) {
                    point = comparePoint(point, 30);
                } else if ("4".equals(pldimnExeAbi)) {
                    point = comparePoint(point, 40);
                } else if ("5".equals(pldimnExeAbi)) {
                    point = comparePoint(point, 40);
                }

                String guarValueSitu = riskPldimnList.getGuarValueSitu();// 抵质押品价值(元) STD_RISK_GUAR_VALUE_SITU
                if ("1".equals(guarValueSitu)) {
                    point = comparePoint(point, 10);
                } else if ("2".equals(guarValueSitu)) {
                    point = comparePoint(point, 30);
                }

                String guarEvalType = riskPldimnList.getGuarEvalType();// 抵（质）押品的实际价值（采用以下三种方式之一） STD_RISK_GUAR_EVAL
                if ("1".equals(guarEvalType)) {
                    point = comparePoint(point, 0);
                } else if ("2".equals(guarEvalType)) {
                    point = comparePoint(point, 0);
                } else if ("3".equals(guarEvalType)) {
                    point = comparePoint(point, 0);
                }
            }
        }
        return point;
    }

    /**
     * 影响偿还因素分析
     **/
    private int calculateRiskRepayAnaly(String taskNo, int point) {
        RiskRepayAnaly riskRepayAnaly = new RiskRepayAnaly();
        riskRepayAnaly.setTaskNo(taskNo);
        riskRepayAnaly = riskRepayAnalyService.queryRiskRepayAnaly(riskRepayAnaly);
        if (null != riskRepayAnaly) {
            String repayWish = riskRepayAnaly.getRepayWish();// 还款意愿 STD_RISK_REPAY_WISH
            if ("1".equals(repayWish)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(repayWish)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(repayWish)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(repayWish)) {
                point = comparePoint(point, 30);
            } else if ("5".equals(repayWish)) {
                point = comparePoint(point, 30);
            }
            String repayCapAbility = riskRepayAnaly.getRepayCapAbility();// 还本金能力 STD_RISK_REPAY_CAP_ABI
            if ("1".equals(repayCapAbility)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(repayCapAbility)) {
                point = comparePoint(point, 30);
            } else if ("3".equals(repayCapAbility)) {
                point = comparePoint(point, 40);
            }
            String repayInterestAbility = riskRepayAnaly.getRepayInterestAbility();// 还息能力 STD_RISK_REPAY_INT_ABI
            if ("1".equals(repayInterestAbility)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(repayInterestAbility)) {
                point = comparePoint(point, 30);
            } else if ("3".equals(repayInterestAbility)) {
                point = comparePoint(point, 40);
            }
            String cusLoanManage = riskRepayAnaly.getCusLoanManage();// 银行对客户贷款管理 STD_RISK_LOAN_MANAGE
            if ("1".equals(cusLoanManage)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(cusLoanManage)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(cusLoanManage)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(cusLoanManage)) {
                point = comparePoint(point, 30);
            } else if ("5".equals(cusLoanManage)) {
                point = comparePoint(point, 40);
            }
            String isExistsPenalty = riskRepayAnaly.getIsExistsPenalty();// 客户是否存在违约行为 STD_RISK_CUS_DEFAULT
            if ("1".equals(isExistsPenalty)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(isExistsPenalty)) {
                point = comparePoint(point, 30);
            } else if ("3".equals(isExistsPenalty)) {
                point = comparePoint(point, 20);
            }
            String capOverdueDay = riskRepayAnaly.getCapOverdueDay();// 授信业务本金逾期时间T STD_RISK_CAP_OVERDAY
            if ("1".equals(capOverdueDay)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(capOverdueDay)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(capOverdueDay)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(capOverdueDay)) {
                point = comparePoint(point, 30);
            } else if ("5".equals(capOverdueDay)) {
                point = comparePoint(point, 30);
            }
            String intOverdueDay = riskRepayAnaly.getIntOverdueDay();// 授信业务利息逾期时间T STD_RISK_INT_OVERDAY
            if ("1".equals(intOverdueDay)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(intOverdueDay)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(intOverdueDay)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(intOverdueDay)) {
                point = comparePoint(point, 30);
            } else if ("5".equals(intOverdueDay)) {
                point = comparePoint(point, 30);
            }
            String infactCtrl = riskRepayAnaly.getInfactCtrl();// 实际控制人(法人代表) STD_RISK_INFACT_CTRL
            if ("1".equals(infactCtrl)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(infactCtrl)) {
                point = comparePoint(point, 20);
            }
        }
        return point;
    }

    /**
     * 借款人收入情况分析
     **/
    private int calculateRiskIncomeAnaly(String taskNo, int point) {
        RiskIncomeAnaly riskIncomeAnaly = new RiskIncomeAnaly();
        riskIncomeAnaly.setTaskNo(taskNo);
        riskIncomeAnaly = riskIncomeAnalyService.queryRiskIncomeAnaly(riskIncomeAnaly);
        if (null != riskIncomeAnaly) {
            String incomeBalance = riskIncomeAnaly.getIncomeBalance();// 家庭资产情况 STD_RISK_INCOME_BALANCE
            if ("1".equals(incomeBalance)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(incomeBalance)) {
                point = comparePoint(point, 20);
            }
            String famAssetBalance = riskIncomeAnaly.getFamAssetBalance();// 家庭负债情况 STD_RISK_ASSET_BALANCE
            if ("1".equals(famAssetBalance)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(famAssetBalance)) {
                point = comparePoint(point, 20);
            }
            String isWorkStable = riskIncomeAnaly.getIsWorkStable();// 工作稳定性 STD_RISK_WORK_STABLE
            if ("1".equals(isWorkStable)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(isWorkStable)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(isWorkStable)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(isWorkStable)) {
                point = comparePoint(point, 30);
            }
            String isIncomeSuffice = riskIncomeAnaly.getIsIncomeSuffice();// 第一还款来源是否充足 STD_RISK_INCOME_SUFFICE
            if ("1".equals(isIncomeSuffice)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(isIncomeSuffice)) {
                point = comparePoint(point, 20);
            } else if ("3".equals(isIncomeSuffice)) {
                point = comparePoint(point, 30);
            } else if ("4".equals(isIncomeSuffice)) {
                point = comparePoint(point, 40);
            }
        }
        return point;
    }

    /**
     * 财务分析
     **/
    private int calculateRiskFinaAnaly(String taskNo, int point) {

        RiskFinaAnaly riskFinaAnaly = new RiskFinaAnaly();
        riskFinaAnaly.setTaskNo(taskNo);
        riskFinaAnaly = riskFinaAnalyService.queryRiskFinaAnaly(riskFinaAnaly);
        if (null != riskFinaAnaly) {
            String cashAnaly = riskFinaAnaly.getCashAnaly();// 客户现金流量分析 STD_RISK_CASH_ANALY
            if ("1".equals(cashAnaly)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(cashAnaly)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(cashAnaly)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(cashAnaly)) {
                point = comparePoint(point, 20);
            } else if ("5".equals(cashAnaly)) {
                point = comparePoint(point, 20);
            }
            String finaAnaly = riskFinaAnaly.getFinaAnaly();// 客户财务状况分析 STD_RISK_FINA_ANALY
            if ("1".equals(finaAnaly)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(finaAnaly)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(finaAnaly)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(finaAnaly)) {
                point = comparePoint(point, 30);
            } else if ("5".equals(finaAnaly)) {
                point = comparePoint(point, 40);
            }
        }
        return point;
    }

    /**
     * 非财务分析
     **/
    private int calculateRiskNonFinaAnaly(String taskNo, int point) {
        RiskNonFinaAnaly riskNonFinaAnaly = new RiskNonFinaAnaly();
        riskNonFinaAnaly.setTaskNo(taskNo);
        riskNonFinaAnaly = riskNonFinaAnalyService.queryRiskNonFinaAnaly(riskNonFinaAnaly);
        if (null != riskNonFinaAnaly) {
            String economyChangeCase = riskNonFinaAnaly.getEconomyChangeCase();// 外部宏观经济环境发生变化情况 STD_RISK_ECONOMY_EFFECT
            if ("1".equals(economyChangeCase)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(economyChangeCase)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(economyChangeCase)) {
                point = comparePoint(point, 20);
            }
            String tradeRisk = riskNonFinaAnaly.getTradeRisk();// 行业风险 STD_RISK_TRADE_EFFECT
            if ("1".equals(tradeRisk)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(tradeRisk)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(tradeRisk)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(tradeRisk)) {
                point = comparePoint(point, 20);
            }
            String shareholderRelaChange = riskNonFinaAnaly.getShareholderRelaChange();// 主要股东、关联公司或母子公司发生重大变化 STD_RISK_RELA_EFFECT
            if ("1".equals(shareholderRelaChange)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(shareholderRelaChange)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(shareholderRelaChange)) {
                point = comparePoint(point, 20);
            }
            String manaCase = riskNonFinaAnaly.getManaCase();// 借款人内部管理情况 STD_RISK_MANA_EFFECT
            if ("1".equals(manaCase)) {
                point = comparePoint(point, 10);
            } else if ("2".equals(manaCase)) {
                point = comparePoint(point, 10);
            } else if ("3".equals(manaCase)) {
                point = comparePoint(point, 20);
            } else if ("4".equals(manaCase)) {
                point = comparePoint(point, 30);
            }
        }
        return point;
    }

    private int comparePoint(int p1, int p2) {
        if (p1 > p2) {
            return p1;
        } else {
            return p2;
        }
    }

    /**
     * 更新台账五级/十级分类结果
     *
     * @author jijian_yx
     * @date 2021/8/10 23:08
     **/
    public void updateAccLoanFiveClass(RiskTaskList riskTaskList) throws Exception {
        try {
            RiskTaskList riskTask = riskTaskListMapper.queryRiskTaskList(riskTaskList);
            if (null == riskTask) {
                log.info("风险分类数据不存在,任务编号[{}]", riskTaskList.getTaskNo());
                return;
            }
            String cusId = riskTask.getCusId();// 客户号
            String manualClass = riskTask.getManualClass();// 手工五级分类结果
            String manualTenClass = riskTask.getManualTenClass();// 手工十级分类结果
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("taskNo", riskTask.getTaskNo());
            List<RiskDebitInfo> riskDebitInfoList = riskDebitInfoService.selectAll(queryModel);
            if (null != riskDebitInfoList && riskDebitInfoList.size() > 0) {
                for (RiskDebitInfo riskDebitInfo : riskDebitInfoList) {
                    String billNo = riskDebitInfo.getBillNo();
                    if (!StringUtils.isEmpty(billNo)) {
                        Map<String, String> map = new HashMap<>();
                        map.put("billNo", billNo);
                        map.put("fiveClass", manualClass);
                        if (!StringUtils.isEmpty(manualTenClass)) {
                            map.put("tenClass", manualTenClass);
                        }
                        ResultDto<Integer> resultDto = cmisBizClientService.updateAccLoanByBillNo(map);
                        if (null == resultDto) {
                            log.info("更新台账五级/十级分类结果失败,参数:[{}]", map);
                            throw new Exception("更新台账五级/十级分类结果失败,biz服务异常");
                        } else if (resultDto.getData() < 1) {
                            log.info("更新台账五级/十级分类结果完成,未更新到数据,参数:[{}]", map);
                            // throw new Exception("更新台账五级/十级分类结果失败,未更新到数据");
                        }
                    }
                }

            }
            if (!StringUtils.isEmpty(cusId) && !StringUtils.isEmpty(manualClass)) {
                // 更新客户名下未结清的银承、保函、开证、委托贷款台账五十级分类结果
                Map<String, String> otherMap = new HashMap<>();
                otherMap.put("cusId", cusId);
                otherMap.put("fiveClass", manualClass);
                if (!StringUtils.isEmpty(manualTenClass)) {
                    otherMap.put("tenClass", manualTenClass);
                }
                ResultDto<Integer> loanDto = cmisBizClientService.updateOtherLoanByCusId(otherMap);
                if (null == loanDto) {
                    log.info("风险分类审批结束更新银承、保函、开证、委托贷款台账五十级分类失败,参数:[{}]", otherMap);
                    throw new Exception("风险分类审批结束更新银承、保函、开证、委托贷款台账五十级分类失败,biz服务异常");
                } else if (loanDto.getData() < 1) {
                    log.info("风险分类审批结束更新银承、保函、开证、委托贷款台账五十级分类完成,未更新到数据,参数:[{}]", otherMap);
                }
            }
        } catch (Exception e) {
            log.error("更新台账五级/十级分类结果异常,原因:[{}]", e.getMessage());
            throw e;
        }
    }

    /**
     * 获取子表任务生成状态
     *
     * @author jijian_yx
     * @date 2021/9/30 0:47
     **/
    public String getTaskStatus(String pkId) {
        return riskTaskListMapper.getTaskStatus(pkId);
    }

    /**
     * 获取所有页签保存情况
     *
     * @author jijian_yx
     * @date 2021/10/8 15:40
     **/
    public String getSaveInfo(String taskNo) {
        String result = "0";// 保存结果,通过为0
        try {
            log.info("风险分类初分校验页签保存情况开始");
            RiskTaskList riskTaskList = new RiskTaskList();
            riskTaskList.setTaskNo(taskNo);
            riskTaskList = riskTaskListMapper.queryRiskTaskList(riskTaskList);
            if (null == riskTaskList) {
                result = "风险分类数据异常";
                return result;
            }
            Map<String, String> tableNameMap = new LinkedHashMap<>();
            String checkType = riskTaskList.getCheckType();// 分类模型：1 公司客户风险分类；2 个人经营性风险分类；3 个人消费性风险分类
            if (Objects.equals(checkType, "1")) {
                // 1 公司客户风险分类
                tableNameMap.put("RISK_DEBIT_ANALY", "借款人情况分析");
                tableNameMap.put("RISK_OPER_ANALY", "经营情况分析");
                tableNameMap.put("RISK_DEBIT_INFO", "贷款情况分析");
                tableNameMap.put("RISK_REPAY_ANALY", "影响偿还因素分析");
                tableNameMap.put("RISK_GUAR_CONT_ANALY", "担保合同分析");
                tableNameMap.put("RISK_PLDIMN_LIST", "抵质押情况分析");
                tableNameMap.put("RISK_GUARNTR_LIST", "保证人情况分析");
                tableNameMap.put("RISK_FINA_ANALY", "财务情况分析");
                tableNameMap.put("RISK_NON_FINA_ANALY", "非财务情况分析");
                tableNameMap.put("RISK_COMP_ANALY", "综合分析");
            } else if (Objects.equals(checkType, "2")) {
                // 2 个人经营性风险分类
                tableNameMap.put("RISK_DEBIT_ANALY", "借款人情况分析");
                tableNameMap.put("RISK_OPER_ANALY", "经营情况分析");
                tableNameMap.put("RISK_DEBIT_INFO", "贷款情况分析");
                tableNameMap.put("RISK_GUAR_CONT_ANALY", "担保合同分析");
                tableNameMap.put("RISK_PLDIMN_LIST", "抵押物情况分析");
                tableNameMap.put("RISK_GUARNTR_LIST", "保证人情况分析");
            } else if (Objects.equals(checkType, "3")) {
                // 3 个人消费性风险分类
                tableNameMap.put("RISK_DEBIT_ANALY", "借款人情况分析");
                tableNameMap.put("RISK_INCOME_ANALY", "借款人收入情况分析");
                tableNameMap.put("RISK_DEBIT_INFO", "贷款情况分析");
                tableNameMap.put("RISK_GUAR_CONT_ANALY", "担保合同分析");
                tableNameMap.put("RISK_PLDIMN_LIST", "抵押物情况分析");
                tableNameMap.put("RISK_GUARNTR_LIST", "保证人情况分析");
            }
            if (tableNameMap.size() > 0) {
                for (Map.Entry<String, String> entry : tableNameMap.entrySet()) {
                    String tableName = entry.getKey();
                    String tableNameCN = entry.getValue();
                    if (Objects.equals(tableName, "RISK_DEBIT_INFO") || Objects.equals(tableName, "RISK_GUAR_CONT_ANALY")
                            || Objects.equals(tableName, "RISK_PLDIMN_LIST") || Objects.equals(tableName, "RISK_GUARNTR_LIST")) {
                        String count = riskTaskListMapper.getSaveInfo(tableName, taskNo);
                        if (null != count && Integer.parseInt(count) > 0) {
                            result = tableNameCN + "页签存在未分析项";
                            break;
                        }
                    } else {
                        String count = riskTaskListMapper.getSaveInfo(tableName, taskNo);
                        if (null == count) {
                            result = tableNameCN + "页签未分析";
                            break;
                        }
                    }
                }
            }
            log.info("风险分类初分校验页签保存情况结束");
        } catch (Exception e) {
            e.printStackTrace();
            result = "0";// 后台校验异常不影响后续获取机评结果,code返回成功0,异常捕捉掉
            log.info("风险分类初分校验页签保存情况异常,原因[{}]", e.getMessage());
        }
        return result;
    }
    /**
     *客户移交判断是否有在途任务
     * @param pspKHYJDto
     * @return
     */
    public RiskTaskList queryCountBycusId(PspKHYJDto pspKHYJDto) {
        // 判断数据是否存在
        RiskTaskList riskTaskList= riskTaskListMapper.queryCountBycusId(pspKHYJDto);
        return riskTaskList;
    }
}
