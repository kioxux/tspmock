package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: FptMPpCustomerBlack
 * @类描述: fpt_m_pp_customer_black数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-01 21:24:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class FptMPpCustomerBlackDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 客户号 **/
	private String custNo;
	/** 客户名称 **/
	private String custName;
	/** 黑名单描述 **/
	private String remark;
	/** 下发日期 **/
	private String inputDate;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certNo;
	
	/** 黑名单等级 **/
	private String levelType;
	
	/** 黑名单类型(01黑名单 02灰名单 03信用卡黑名单) **/
	private String listType;
	
	/** 流程编号 **/
	private String dealId;
	
	/** 流程状态 **/
	private String wfApprSts;
	
	/** 解除原因 **/
	private String removeReason;
	
	/** 解除时间 **/
	private String shutDate;
	
	/** 当前逾期金额 **/
	private java.math.BigDecimal currentBalance;
	
	/** 信用卡号 **/
	private String cardNo;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 数据日期 **/
	private String dataDate;
	
	
	/**
	 * @param custNo
	 */
	public void setCustNo(String custNo) {
		this.custNo = custNo == null ? null : custNo.trim();
	}
	
    /**
     * @return CustNo
     */	
	public String getCustNo() {
		return this.custNo;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName == null ? null : custName.trim();
	}
	
    /**
     * @return CustName
     */	
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo == null ? null : certNo.trim();
	}
	
    /**
     * @return CertNo
     */	
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param levelType
	 */
	public void setLevelType(String levelType) {
		this.levelType = levelType == null ? null : levelType.trim();
	}
	
    /**
     * @return LevelType
     */	
	public String getLevelType() {
		return this.levelType;
	}
	
	/**
	 * @param listType
	 */
	public void setListType(String listType) {
		this.listType = listType == null ? null : listType.trim();
	}
	
    /**
     * @return ListType
     */	
	public String getListType() {
		return this.listType;
	}
	
	/**
	 * @param dealId
	 */
	public void setDealId(String dealId) {
		this.dealId = dealId == null ? null : dealId.trim();
	}
	
    /**
     * @return DealId
     */	
	public String getDealId() {
		return this.dealId;
	}
	
	/**
	 * @param wfApprSts
	 */
	public void setWfApprSts(String wfApprSts) {
		this.wfApprSts = wfApprSts == null ? null : wfApprSts.trim();
	}
	
    /**
     * @return WfApprSts
     */	
	public String getWfApprSts() {
		return this.wfApprSts;
	}
	
	/**
	 * @param removeReason
	 */
	public void setRemoveReason(String removeReason) {
		this.removeReason = removeReason == null ? null : removeReason.trim();
	}
	
    /**
     * @return RemoveReason
     */	
	public String getRemoveReason() {
		return this.removeReason;
	}
	
	/**
	 * @param shutDate
	 */
	public void setShutDate(String shutDate) {
		this.shutDate = shutDate == null ? null : shutDate.trim();
	}
	
    /**
     * @return ShutDate
     */	
	public String getShutDate() {
		return this.shutDate;
	}
	
	/**
	 * @param currentBalance
	 */
	public void setCurrentBalance(java.math.BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}
	
    /**
     * @return CurrentBalance
     */	
	public java.math.BigDecimal getCurrentBalance() {
		return this.currentBalance;
	}
	
	/**
	 * @param cardNo
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo == null ? null : cardNo.trim();
	}
	
    /**
     * @return CardNo
     */	
	public String getCardNo() {
		return this.cardNo;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param dataDate
	 */
	public void setDataDate(String dataDate) {
		this.dataDate = dataDate == null ? null : dataDate.trim();
	}
	
    /**
     * @return DataDate
     */	
	public String getDataDate() {
		return this.dataDate;
	}


}