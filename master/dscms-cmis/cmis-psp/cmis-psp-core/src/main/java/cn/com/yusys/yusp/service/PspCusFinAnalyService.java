/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.PspGuarCheck;
import cn.com.yusys.yusp.dto.PspCusFinAndGuarDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PspCusFinAnaly;
import cn.com.yusys.yusp.repository.mapper.PspCusFinAnalyMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspCusFinAnalyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspCusFinAnalyService {

    @Autowired
    private PspCusFinAnalyMapper pspCusFinAnalyMapper;
    @Autowired
    private PspGuarCheckService pspGuarCheckService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PspCusFinAnaly selectByPrimaryKey(String pkId) {
        return pspCusFinAnalyMapper.selectByPrimaryKey(pkId);
    }
	/**
     * @方法名称: queryPspCusFinAnaly
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspCusFinAnaly queryPspCusFinAnaly(PspCusFinAnaly pspCusFinAnaly) {
        return pspCusFinAnalyMapper.queryPspCusFinAnaly(pspCusFinAnaly);
    }
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PspCusFinAnaly> selectAll(QueryModel model) {
        List<PspCusFinAnaly> records = (List<PspCusFinAnaly>) pspCusFinAnalyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PspCusFinAnaly> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PspCusFinAnaly> list = pspCusFinAnalyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PspCusFinAnaly record) {
        return pspCusFinAnalyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    /**
     * @方法名称: addFinAndGuar
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int addFinAndGuar(PspCusFinAndGuarDto record) {
        PspCusFinAnaly pspCusFinAnaly = new PspCusFinAnaly();
        PspGuarCheck pspGuarCheck = new PspGuarCheck();
        record.setCreateTime(DateUtils.getCurrDate());
        record.setUpdateTime(DateUtils.getCurrDate());
        org.springframework.beans.BeanUtils.copyProperties(record,pspCusFinAnaly);
        org.springframework.beans.BeanUtils.copyProperties(record,pspGuarCheck);
        pspCusFinAnalyMapper.insert(pspCusFinAnaly);
        pspGuarCheckService.insert(pspGuarCheck);
        return pspCusFinAnalyMapper.insert(pspCusFinAnaly) + pspGuarCheckService.insert(pspGuarCheck);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PspCusFinAnaly record) {
        return pspCusFinAnalyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PspCusFinAnaly record) {
        int count = 0;
        PspCusFinAnaly pspCusFinAnaly = pspCusFinAnalyMapper.selectByPrimaryKey(record.getPkId());
        if (pspCusFinAnaly != null){
            pspCusFinAnalyMapper.updateByPrimaryKey(record);
        }else {
            pspCusFinAnalyMapper.insert(record);
        }
        return count;
    }

    /**
     * @方法名称: updateFinAndGuar
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateFinAndGuar(PspCusFinAndGuarDto record) {
        PspCusFinAnaly pspCusFinAnaly = new PspCusFinAnaly();
        PspGuarCheck pspGuarCheck = new PspGuarCheck();
        record.setUpdateTime(DateUtils.getCurrDate());
        org.springframework.beans.BeanUtils.copyProperties(record,pspCusFinAnaly);
        org.springframework.beans.BeanUtils.copyProperties(record,pspGuarCheck);
        if(pspCusFinAnalyMapper.updateByTaskNo(pspCusFinAnaly) == 0){
            pspCusFinAnaly.setCreateTime(DateUtils.getCurrDate());
            pspCusFinAnalyMapper.insert(pspCusFinAnaly);
        }
        if(pspGuarCheckService.updateByTaskNo(pspGuarCheck) == 0){
            pspGuarCheck.setCreateTime(DateUtils.getCurrDate());
            pspGuarCheckService.insert(pspGuarCheck);
        }
        return 0;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PspCusFinAnaly record) {
        return pspCusFinAnalyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pspCusFinAnalyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pspCusFinAnalyMapper.deleteByIds(ids);
    }
    /**
     * @方法名称: deleteByTaskNo
     * @方法描述: 根据任务编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByTaskNo(String taskNo) {
        return pspCusFinAnalyMapper.deleteByTaskNo(taskNo);
    }
}
