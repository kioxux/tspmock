
/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PspDebitOperCaseAnaly;
import cn.com.yusys.yusp.repository.mapper.PspDebitOperCaseAnalyMapper;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspDebitOperCaseAnalyService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PspDebitOperCaseAnalyService {

    @Autowired
    private PspDebitOperCaseAnalyMapper pspDebitOperCaseAnalyMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspDebitOperCaseAnaly selectByPrimaryKey(String pkId) {
        return pspDebitOperCaseAnalyMapper.selectByPrimaryKey(pkId);
    }
    /**
     * @方法名称: queryPspDebitOperCaseAnaly
     * @方法描述: 根据实体类查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PspDebitOperCaseAnaly queryPspDebitOperCaseAnaly(PspDebitOperCaseAnaly pspDebitOperCaseAnaly) {
        return pspDebitOperCaseAnalyMapper.queryPspDebitOperCaseAnaly(pspDebitOperCaseAnaly);
    }
    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PspDebitOperCaseAnaly> selectAll(QueryModel model) {
        List<PspDebitOperCaseAnaly> records = (List<PspDebitOperCaseAnaly>) pspDebitOperCaseAnalyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PspDebitOperCaseAnaly> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PspDebitOperCaseAnaly> list = pspDebitOperCaseAnalyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PspDebitOperCaseAnaly record) {
        return pspDebitOperCaseAnalyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PspDebitOperCaseAnaly record) {
        return pspDebitOperCaseAnalyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PspDebitOperCaseAnaly record) {
        int count = 0;
        if (record != null) {
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            if(record != null){
                record.setUpdateTime(createTime);
            }
            count = pspDebitOperCaseAnalyMapper.updateByPrimaryKeySelective(record);
        }
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PspDebitOperCaseAnaly record) {
        return pspDebitOperCaseAnalyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return pspDebitOperCaseAnalyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pspDebitOperCaseAnalyMapper.deleteByIds(ids);
    }
}