/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PspOperStatusCheckProperty;
import cn.com.yusys.yusp.service.PspOperStatusCheckPropertyService;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;

/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckPropertyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pspoperstatuscheckproperty")
public class PspOperStatusCheckPropertyResource {
    @Autowired
    private PspOperStatusCheckPropertyService pspOperStatusCheckPropertyService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PspOperStatusCheckProperty>> query() {
        QueryModel queryModel = new QueryModel();
        List<PspOperStatusCheckProperty> list = pspOperStatusCheckPropertyService.selectAll(queryModel);
        return new ResultDto<List<PspOperStatusCheckProperty>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PspOperStatusCheckProperty>> index(QueryModel queryModel) {
        List<PspOperStatusCheckProperty> list = pspOperStatusCheckPropertyService.selectByModel(queryModel);
        return new ResultDto<List<PspOperStatusCheckProperty>>(list);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     */
    @GetMapping("/queryList")
    protected ResultDto<List<PspOperStatusCheckProperty>> queryList(QueryModel queryModel) {
        List<PspOperStatusCheckProperty> list = pspOperStatusCheckPropertyService.selectByModel(queryModel);
        return new ResultDto<List<PspOperStatusCheckProperty>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PspOperStatusCheckProperty> show(@PathVariable("pkId") String pkId) {
        PspOperStatusCheckProperty pspOperStatusCheckProperty = pspOperStatusCheckPropertyService.selectByPrimaryKey(pkId);
        return new ResultDto<PspOperStatusCheckProperty>(pspOperStatusCheckProperty);
    }
    /**
     * @函数名称:queryPspOperStatusCheckProperty
     * @函数描述:根据实体类查询单个对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<PspOperStatusCheckProperty> queryPspOperStatusCheckProperty(@RequestBody PspOperStatusCheckProperty pspOperStatusCheckProperty) {
        pspOperStatusCheckProperty = pspOperStatusCheckPropertyService.queryPspOperStatusCheckProperty(pspOperStatusCheckProperty);
        return new ResultDto<PspOperStatusCheckProperty>(pspOperStatusCheckProperty);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PspOperStatusCheckProperty> create(@RequestBody PspOperStatusCheckProperty pspOperStatusCheckProperty) throws URISyntaxException {
        pspOperStatusCheckPropertyService.insert(pspOperStatusCheckProperty);
        return new ResultDto<PspOperStatusCheckProperty>(pspOperStatusCheckProperty);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PspOperStatusCheckProperty pspOperStatusCheckProperty) throws URISyntaxException {
        int result = pspOperStatusCheckPropertyService.update(pspOperStatusCheckProperty);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pspOperStatusCheckPropertyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pspOperStatusCheckPropertyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: sendToIb1253
     * @函数描述: 调用ib1253查询子序号
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据账号自动回显名称")
    @PostMapping("/sendToIb1253")
    protected ResultDto<Ib1253RespDto> sendToIb1253(@RequestBody Map map) {
        ResultDto<Ib1253RespDto> resultDto = pspOperStatusCheckPropertyService.sendToIb1253(map);
        return resultDto;
    }
}
