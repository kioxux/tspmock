/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-psp-starter模块
 * @类名称: PspCheckInfo
 * @类描述: psp_check_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-29 00:10:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "psp_check_info")
public class PspCheckInfo extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 任务编号 **/
	@Column(name = "TASK_NO", unique = false, nullable = false, length = 40)
	private String taskNo;

	/** 支付方式 **/
	@Column(name = "DEFRAY_MODE", unique = false, nullable = true, length = 5)
	private String defrayMode;

	/** 受托对象 **/
	@Column(name = "ENTRUSTED_CUS_NAME", unique = false, nullable = true, length = 80)
	private String entrustedCusName;

	/** 工作单位是否变化 **/
	@Column(name = "IS_CHANGE_UNIT", unique = false, nullable = true, length = 5)
	private String isChangeUnit;

	/** 现工作单位 **/
	@Column(name = "NOW_UNIT", unique = false, nullable = true, length = 80)
	private String nowUnit;

	/** 家庭住址是否变化 **/
	@Column(name = "IS_CHANGE_ADDR", unique = false, nullable = true, length = 5)
	private String isChangeAddr;

	/** 现家庭住址 **/
	@Column(name = "NOW_ADDR", unique = false, nullable = true, length = 80)
	private String nowAddr;

	/** 婚姻状况是否变化 **/
	@Column(name = "IS_CHANGE_MAR", unique = false, nullable = true, length = 5)
	private String isChangeMar;

	/** 婚姻状况说明 **/
	@Column(name = "MAR_REMARK", unique = false, nullable = true, length = 65535)
	private String marRemark;

	/** 联系方式是否变化 **/
	@Column(name = "IS_CHANGE_PHONE", unique = false, nullable = true, length = 5)
	private String isChangePhone;

	/** 现联系方式 **/
	@Column(name = "NOW_REMARK", unique = false, nullable = true, length = 65535)
	private String nowRemark;

	/** 收入及负债是否变化 **/
	@Column(name = "IS_CHANGE_EARNING", unique = false, nullable = true, length = 5)
	private String isChangeEarning;

	/** 现收入负债情况 **/
	@Column(name = "EARNING_REMARK", unique = false, nullable = true, length = 65535)
	private String earningRemark;

	/** 担保情况是否变化 **/
	@Column(name = "IS_CHANGE_GUAR", unique = false, nullable = true, length = 5)
	private String isChangeGuar;

	/** 现担保情况 **/
	@Column(name = "GUAR_REMARK", unique = false, nullable = true, length = 65535)
	private String guarRemark;

	/** 贷款半年逾期次数 **/
	@Column(name = "OVERDUE_TIMES", unique = false, nullable = true, length = 10)
	private Integer overdueTimes;

	/** 逾期原因 **/
	@Column(name = "OVERDUE_RESN", unique = false, nullable = true, length = 65535)
	private String overdueResn;

	/** 贷后方式 **/
	@Column(name = "CHECK_MODE", unique = false, nullable = true, length = 5)
	private String checkMode;

	/** 贷后方式说明 **/
	@Column(name = "CHECK_MODE_REMARK", unique = false, nullable = true, length = 65535)
	private String checkModeRemark;

	/** 拟采取措施 **/
	@Column(name = "CHECK_MEASURE", unique = false, nullable = true, length = 5)
	private String checkMeasure;

	/** 其他措施 **/
	@Column(name = "CHECK_MEASURE_REMARK", unique = false, nullable = true, length = 65535)
	private String checkMeasureRemark;

	/** 超期未办理抵押原因 **/
	@Column(name = "OVERDUE_REASON", unique = false, nullable = true, length = 5)
	private String overdueReason;

	/** 其他超期未办理抵押原因 **/
	@Column(name = "OTHER_OVERDUE_REASON", unique = false, nullable = true, length = 65535)
	private String otherOverdueReason;

	/** 超期未抵押拟采取措施 **/
	@Column(name = "OVERDUE_MEASURE", unique = false, nullable = true, length = 5)
	private String overdueMeasure;

	/** 其他超期未抵押措施 **/
	@Column(name = "OTHER_MEASURE_REMARK", unique = false, nullable = true, length = 65535)
	private String otherMeasureRemark;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}

	/**
	 * @return taskNo
	 */
	public String getTaskNo() {
		return this.taskNo;
	}

	/**
	 * @param defrayMode
	 */
	public void setDefrayMode(String defrayMode) {
		this.defrayMode = defrayMode;
	}

	/**
	 * @return defrayMode
	 */
	public String getDefrayMode() {
		return this.defrayMode;
	}

	/**
	 * @param entrustedCusName
	 */
	public void setEntrustedCusName(String entrustedCusName) {
		this.entrustedCusName = entrustedCusName;
	}

	/**
	 * @return entrustedCusName
	 */
	public String getEntrustedCusName() {
		return this.entrustedCusName;
	}

	/**
	 * @param isChangeUnit
	 */
	public void setIsChangeUnit(String isChangeUnit) {
		this.isChangeUnit = isChangeUnit;
	}

	/**
	 * @return isChangeUnit
	 */
	public String getIsChangeUnit() {
		return this.isChangeUnit;
	}

	/**
	 * @param nowUnit
	 */
	public void setNowUnit(String nowUnit) {
		this.nowUnit = nowUnit;
	}

	/**
	 * @return nowUnit
	 */
	public String getNowUnit() {
		return this.nowUnit;
	}

	/**
	 * @param isChangeAddr
	 */
	public void setIsChangeAddr(String isChangeAddr) {
		this.isChangeAddr = isChangeAddr;
	}

	/**
	 * @return isChangeAddr
	 */
	public String getIsChangeAddr() {
		return this.isChangeAddr;
	}

	/**
	 * @param nowAddr
	 */
	public void setNowAddr(String nowAddr) {
		this.nowAddr = nowAddr;
	}

	/**
	 * @return nowAddr
	 */
	public String getNowAddr() {
		return this.nowAddr;
	}

	/**
	 * @param isChangeMar
	 */
	public void setIsChangeMar(String isChangeMar) {
		this.isChangeMar = isChangeMar;
	}

	/**
	 * @return isChangeMar
	 */
	public String getIsChangeMar() {
		return this.isChangeMar;
	}

	/**
	 * @param marRemark
	 */
	public void setMarRemark(String marRemark) {
		this.marRemark = marRemark;
	}

	/**
	 * @return marRemark
	 */
	public String getMarRemark() {
		return this.marRemark;
	}

	/**
	 * @param isChangePhone
	 */
	public void setIsChangePhone(String isChangePhone) {
		this.isChangePhone = isChangePhone;
	}

	/**
	 * @return isChangePhone
	 */
	public String getIsChangePhone() {
		return this.isChangePhone;
	}

	/**
	 * @param nowRemark
	 */
	public void setNowRemark(String nowRemark) {
		this.nowRemark = nowRemark;
	}

	/**
	 * @return nowRemark
	 */
	public String getNowRemark() {
		return this.nowRemark;
	}

	/**
	 * @param isChangeEarning
	 */
	public void setIsChangeEarning(String isChangeEarning) {
		this.isChangeEarning = isChangeEarning;
	}

	/**
	 * @return isChangeEarning
	 */
	public String getIsChangeEarning() {
		return this.isChangeEarning;
	}

	/**
	 * @param earningRemark
	 */
	public void setEarningRemark(String earningRemark) {
		this.earningRemark = earningRemark;
	}

	/**
	 * @return earningRemark
	 */
	public String getEarningRemark() {
		return this.earningRemark;
	}

	/**
	 * @param isChangeGuar
	 */
	public void setIsChangeGuar(String isChangeGuar) {
		this.isChangeGuar = isChangeGuar;
	}

	/**
	 * @return isChangeGuar
	 */
	public String getIsChangeGuar() {
		return this.isChangeGuar;
	}

	/**
	 * @param guarRemark
	 */
	public void setGuarRemark(String guarRemark) {
		this.guarRemark = guarRemark;
	}

	/**
	 * @return guarRemark
	 */
	public String getGuarRemark() {
		return this.guarRemark;
	}

	/**
	 * @param overdueTimes
	 */
	public void setOverdueTimes(Integer overdueTimes) {
		this.overdueTimes = overdueTimes;
	}

	/**
	 * @return overdueTimes
	 */
	public Integer getOverdueTimes() {
		return this.overdueTimes;
	}

	/**
	 * @param overdueResn
	 */
	public void setOverdueResn(String overdueResn) {
		this.overdueResn = overdueResn;
	}

	/**
	 * @return overdueResn
	 */
	public String getOverdueResn() {
		return this.overdueResn;
	}

	/**
	 * @param checkMode
	 */
	public void setCheckMode(String checkMode) {
		this.checkMode = checkMode;
	}

	/**
	 * @return checkMode
	 */
	public String getCheckMode() {
		return this.checkMode;
	}

	/**
	 * @param checkModeRemark
	 */
	public void setCheckModeRemark(String checkModeRemark) {
		this.checkModeRemark = checkModeRemark;
	}

	/**
	 * @return checkModeRemark
	 */
	public String getCheckModeRemark() {
		return this.checkModeRemark;
	}

	/**
	 * @param checkMeasure
	 */
	public void setCheckMeasure(String checkMeasure) {
		this.checkMeasure = checkMeasure;
	}

	/**
	 * @return checkMeasure
	 */
	public String getCheckMeasure() {
		return this.checkMeasure;
	}

	/**
	 * @param checkMeasureRemark
	 */
	public void setCheckMeasureRemark(String checkMeasureRemark) {
		this.checkMeasureRemark = checkMeasureRemark;
	}

	/**
	 * @return checkMeasureRemark
	 */
	public String getCheckMeasureRemark() {
		return this.checkMeasureRemark;
	}

	/**
	 * @param overdueReason
	 */
	public void setOverdueReason(String overdueReason) {
		this.overdueReason = overdueReason;
	}

	/**
	 * @return overdueReason
	 */
	public String getOverdueReason() {
		return this.overdueReason;
	}

	/**
	 * @param otherOverdueReason
	 */
	public void setOtherOverdueReason(String otherOverdueReason) {
		this.otherOverdueReason = otherOverdueReason;
	}

	/**
	 * @return otherOverdueReason
	 */
	public String getOtherOverdueReason() {
		return this.otherOverdueReason;
	}

	/**
	 * @param overdueMeasure
	 */
	public void setOverdueMeasure(String overdueMeasure) {
		this.overdueMeasure = overdueMeasure;
	}

	/**
	 * @return overdueMeasure
	 */
	public String getOverdueMeasure() {
		return this.overdueMeasure;
	}

	/**
	 * @param otherMeasureRemark
	 */
	public void setOtherMeasureRemark(String otherMeasureRemark) {
		this.otherMeasureRemark = otherMeasureRemark;
	}

	/**
	 * @return otherMeasureRemark
	 */
	public String getOtherMeasureRemark() {
		return this.otherMeasureRemark;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}