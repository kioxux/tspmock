package cn.com.yusys.yusp.service.server.xddh0017;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.PspWydTaskList;
import cn.com.yusys.yusp.dto.server.xddh0017.req.Xddh0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0017.resp.Xddh0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.PspWydTaskListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-psp模块
 * @类名称: Xddh0017Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: wrw
 * @创建时间: 2021-08-30 09:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xddh0017Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0017Service.class);

    @Resource
    private PspWydTaskListMapper pspWydTaskListMapper;

    /**
     * 贷后任务完成接收接口
     *
     * @param
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddh0017DataRespDto xddh0017(Xddh0017DataReqDto Xddh0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value);
        Xddh0017DataRespDto xddh0017DataRespDto = new Xddh0017DataRespDto();
        String serno = Xddh0017DataReqDto.getSerno();//贷后任务流水号
        String CusId = Xddh0017DataReqDto.getCusId();//客户号
        String ChkState = Xddh0017DataReqDto.getChkState();//检查状态
        String ChkDate = Xddh0017DataReqDto.getChkDate();//完成日期
        String ChkOpt = Xddh0017DataReqDto.getChkOpt();//检查意见
        String ManagerId = Xddh0017DataReqDto.getManagerId();//客户经理编号
        String ManagerName = Xddh0017DataReqDto.getManagerName();//客户经理名称
        int result = 0;
        try {

            if (StringUtil.isNotEmpty(serno) && StringUtil.isNotEmpty(ChkState)) {
                PspWydTaskList pspWydTaskList = new PspWydTaskList();
                pspWydTaskList.setTaskNo(serno);
                pspWydTaskList.setCusId(CusId);
                pspWydTaskList.setCheckStatus(ChkState);
                pspWydTaskList.setCheckDate(ChkDate);
                pspWydTaskList.setInputId(ManagerId);
                result = pspWydTaskListMapper.updateByTaskNoKey(pspWydTaskList);

                if (result > 0) {
                    xddh0017DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                    xddh0017DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
                }else{
                    xddh0017DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                    xddh0017DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
                }
            }

        }catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value);
        return xddh0017DataRespDto;
    }
}

