import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class test {
    public static void main(String[] args) {
        String letterOrg = "bill_dispupe_opt,arbitrate_addr,other_opt,cont_qnt,pund_cont_no,sppl_clause,sign_addr,busi_network,main_busi_palce,cont_template,cont_print_num"/* +
                "UPD_BR_ID\n" +
                "UPD_DATE\n"*/;
        letterOrg = letterOrg.toLowerCase();
        String[] letterAry = letterOrg.split(",");
        for(String letter:letterAry){
            String replaceLetter = "";
            String resultString = "";

            if(letter.length()>0){
                String[] letters = letter.split("");
                boolean replaceFlag = false;
                for (String idxStr:letters){
                    if(replaceFlag){
                        idxStr = idxStr.toUpperCase();
                    }
                    if("_".equals(idxStr)){
                        replaceFlag = true;
                    }else{
                        replaceLetter +=idxStr;
                        replaceFlag = false;
                    }
                }

//                System.out.println("private String " + replaceLetter + ";"+"\n");

                String firstLetter = replaceLetter.substring(0,1);
                resultString +="public String get";
                resultString = resultString + replaceLetter.replaceFirst(firstLetter,firstLetter.toUpperCase());
                resultString = resultString +  "() {" +" return " + replaceLetter + ";" +" }";
                resultString = resultString +"\n" +"public void set";
                resultString = resultString + replaceLetter.replaceFirst(firstLetter,firstLetter.toUpperCase());
                resultString = resultString +"(String "+replaceLetter+") {" +" this.";
                resultString = resultString + replaceLetter +" = "+replaceLetter+";" + " }"+"\n";

                System.out.println(resultString);
            }
        }
    }
}
