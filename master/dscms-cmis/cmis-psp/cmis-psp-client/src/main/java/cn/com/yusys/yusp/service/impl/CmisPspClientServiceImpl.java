package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.DocAccSearchDto;
import cn.com.yusys.yusp.dto.FptMPpCustomerBlackDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.req.Xdxw0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.resp.Xdxw0037DataRespDto;
import cn.com.yusys.yusp.service.CmisPspClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * cmis-psp服务对外提供服务接口实现类
 *
 * @author monchi
 * @since 2020-12-19 11:50:00
 */
@Component
public class CmisPspClientServiceImpl implements CmisPspClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CmisPspClientServiceImpl.class);

    @Override
    public ResultDto<Xdxw0037DataRespDto> xdxw0037(Xdxw0037DataReqDto reqDto) {
        LOGGER.error("xdxw0037访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Boolean> updatePspTaskListInfo(Map<String, String> paramMap) {
        LOGGER.error("updatePspTaskListInfo访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<HashMap<String, String>> queryRiskClassByCusId(String cusId) {
        LOGGER.error("queryRiskClassByCusId访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<FptMPpCustomerBlackDto> queryFptMPpCustomerBlackByCusId(String cusId) {
        LOGGER.error("queryFptMPpCustomerBlackByCusId访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> createCreditCus(String taskNo) {
        LOGGER.error("createCreditCus访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<String>> getAccTimeList(String cusId) {
        LOGGER.error("getAccTimeList访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<DocAccSearchDto>> queryAccListByDocTypeAndYear(QueryModel queryModel) {
        LOGGER.error("queryAccListByDocTypeAndYear访问失败，触发熔断。");
        return null;
    }
}