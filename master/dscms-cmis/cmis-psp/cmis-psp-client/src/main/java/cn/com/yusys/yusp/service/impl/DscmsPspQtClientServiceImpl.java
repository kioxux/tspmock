package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xddh0017.req.Xddh0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0017.resp.Xddh0017DataRespDto;
import cn.com.yusys.yusp.dto.server.xdqt0006.resp.Xdqt0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.req.Xdxw0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.resp.Xdxw0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsPspQtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsPspQtClientServiceImpl implements DscmsPspQtClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DscmsPspQtClientServiceImpl.class);

    /**
     * 交易码：xdqt0006
     * 交易描述：当天跑P状态查询
     *
     * @return
     */
    @Override
    public ResultDto<Xdqt0006DataRespDto> xdqt0006() {
        LOGGER.error("访问{}|{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value);
        return null;
    }

    /**
     * 交易码：xdxw0037
     * 交易描述：查询借款人是否存在未完成的贷后检查任务
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxw0037DataRespDto> xdxw0037(Xdxw0037DataReqDto reqDto) {
        LOGGER.error("访问{}|{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value);
        return null;
    }

    /**
     * 交易码：xddh0017
     * 交易描述：贷后任务完成接收接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0017DataRespDto> xddh0017(Xddh0017DataReqDto reqDto) {
        LOGGER.error("访问{}|{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value);
        return null;
    }
}
