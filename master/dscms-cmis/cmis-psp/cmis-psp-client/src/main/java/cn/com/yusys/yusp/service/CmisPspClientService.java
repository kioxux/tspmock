package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.DocAccSearchDto;
import cn.com.yusys.yusp.dto.FptMPpCustomerBlackDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.req.Xdxw0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.resp.Xdxw0037DataRespDto;
import cn.com.yusys.yusp.service.impl.CmisPspClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * cmis-psp服务对外提供服务接口
 *
 * @author leehuang
 * @since 2021-03-31 11:50:00
 */
@FeignClient(name = "cmis-psp", path = "/api", fallback = CmisPspClientServiceImpl.class)
public interface CmisPspClientService {
    /**
     * 交易码：xdxw0037
     * 交易描述：查询借款人是否存在未完成的贷后检查任务
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/pspqt4bsp/xdxw0037")
    ResultDto<Xdxw0037DataRespDto> xdxw0037(Xdxw0037DataReqDto reqDto);

    @PostMapping("/psptasklist/updatePspTaskListInfo")
    ResultDto<Boolean> updatePspTaskListInfo(@RequestBody Map<String, String> paramMap);

    /**
     * 交易码：queryRiskClassByCusId
     * 交易描述：根据客户号查询风险分类信息
     *
     * @param cusId
     * @return
     */
    @PostMapping("/risktasklist/queryRiskClassByCusId")
    ResultDto<HashMap<String, String>> queryRiskClassByCusId(String cusId);

    /**
     * 查询黑名单客户表
     *
     * @param cusId
     * @return
     */
    @PostMapping("/fptmppcustomerblack/selectByCusid")
    ResultDto<FptMPpCustomerBlackDto> queryFptMPpCustomerBlackByCusId(String cusId);

    /**
     * 贷后征信查询
     *
     * @param taskNo
     * @return
     */
    @PostMapping("/psptasklist/createcreditcus")
    ResultDto<Integer> createCreditCus(String taskNo);

    /**
     * 根据客户号获取年份集合
     *
     * @author jijian_yx
     * @date 2021/9/27 16:29
     **/
    @PostMapping("/psptasklist/getacctimelist")
    ResultDto<List<String>> getAccTimeList(String cusId);

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     *
     * @author jijian_yx
     * @date 2021/9/28 0:03
     **/
    @PostMapping("/psptasklist/queryacclistbydoctypeandyear")
    public ResultDto<List<DocAccSearchDto>> queryAccListByDocTypeAndYear(@RequestBody QueryModel queryModel);
}