package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xddh0017.req.Xddh0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0017.resp.Xddh0017DataRespDto;
import cn.com.yusys.yusp.dto.server.xdqt0006.resp.Xdqt0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.req.Xdxw0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.resp.Xdxw0037DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsPspQtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类
 *
 * @author lihh
 * @version 1.0
 */
@FeignClient(name = "cmis-psp", path = "/api", fallback = DscmsPspQtClientServiceImpl.class)
public interface DscmsPspQtClientService {

    /**
     * 交易码：xdqt0006
     * 交易描述：当天跑P状态查询
     *
     * @return
     */
    @PostMapping("/pspqt4bsp/xdqt0006")
    ResultDto<Xdqt0006DataRespDto> xdqt0006();

    /**
     * 交易码：xdxw0037
     * 交易描述：查询借款人是否存在未完成的贷后检查任务
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/pspqt4bsp/xdxw0037")
    ResultDto<Xdxw0037DataRespDto> xdxw0037(Xdxw0037DataReqDto reqDto);

    /**
     * 交易码：xddh0017
     * 交易描述：贷后任务完成接收接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/pspqt4bsp/xddh0017")
    ResultDto<Xddh0017DataRespDto> xddh0017(Xddh0017DataReqDto reqDto);

}
