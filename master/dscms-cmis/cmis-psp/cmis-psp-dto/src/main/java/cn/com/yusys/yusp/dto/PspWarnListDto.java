package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWarnList
 * @类描述: psp_warn_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 21:58:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspWarnListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 贷后预警流水号 **/
	private String warnSerno;
	
	/** 贷后预警类型 **/
	private String warnType;
	
	/** 借据号 **/
	private String billNo;
	
	/** 模型结果 **/
	private String modelResult;
	
	/** 预警日期 **/
	private String warnDate;
	
	/** 逾期本金 **/
	private java.math.BigDecimal overdueCapAmt;
	
	/** 逾期利息 **/
	private java.math.BigDecimal overdueIntAmt;
	
	/** 期望完成日期 **/
	private String expertDate;
	
	/** 完成日期 **/
	private String completeDate;
	
	/** 办理人工号 **/
	private String managerId;
	
	/** 办理人 **/
	private String managerName;
	
	/** 借款人客户编号 **/
	private String cusId;
	
	/** 借款人名称 **/
	private String cusName;
	
	/** 合同金额 **/
	private java.math.BigDecimal contAmt;
	
	/** 合同起始日 **/
	private String contStartDate;
	
	/** 借据金额 **/
	private java.math.BigDecimal loanAmt;
	
	/** 借据起始日 **/
	private String loanStartDate;
	
	/** 核验方式 **/
	private String checkMode;
	
	/** 核验地点 **/
	private String checkAddr;
	
	/** 出险原因 **/
	private String riskReason;
	
	/** 检查意见 **/
	private String checkAdvice;
	
	/** 审批人工号 **/
	private String approveId;
	
	/** 审批人名称 **/
	private String approveName;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 合同号 **/
	private String contNo;
	
	/** 借款人身份证号 **/
	private String certCode;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param warnSerno
	 */
	public void setWarnSerno(String warnSerno) {
		this.warnSerno = warnSerno == null ? null : warnSerno.trim();
	}
	
    /**
     * @return WarnSerno
     */	
	public String getWarnSerno() {
		return this.warnSerno;
	}
	
	/**
	 * @param warnType
	 */
	public void setWarnType(String warnType) {
		this.warnType = warnType == null ? null : warnType.trim();
	}
	
    /**
     * @return WarnType
     */	
	public String getWarnType() {
		return this.warnType;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param modelResult
	 */
	public void setModelResult(String modelResult) {
		this.modelResult = modelResult == null ? null : modelResult.trim();
	}
	
    /**
     * @return ModelResult
     */	
	public String getModelResult() {
		return this.modelResult;
	}
	
	/**
	 * @param warnDate
	 */
	public void setWarnDate(String warnDate) {
		this.warnDate = warnDate == null ? null : warnDate.trim();
	}
	
    /**
     * @return WarnDate
     */	
	public String getWarnDate() {
		return this.warnDate;
	}
	
	/**
	 * @param overdueCapAmt
	 */
	public void setOverdueCapAmt(java.math.BigDecimal overdueCapAmt) {
		this.overdueCapAmt = overdueCapAmt;
	}
	
    /**
     * @return OverdueCapAmt
     */	
	public java.math.BigDecimal getOverdueCapAmt() {
		return this.overdueCapAmt;
	}
	
	/**
	 * @param overdueIntAmt
	 */
	public void setOverdueIntAmt(java.math.BigDecimal overdueIntAmt) {
		this.overdueIntAmt = overdueIntAmt;
	}
	
    /**
     * @return OverdueIntAmt
     */	
	public java.math.BigDecimal getOverdueIntAmt() {
		return this.overdueIntAmt;
	}
	
	/**
	 * @param expertDate
	 */
	public void setExpertDate(String expertDate) {
		this.expertDate = expertDate == null ? null : expertDate.trim();
	}
	
    /**
     * @return ExpertDate
     */	
	public String getExpertDate() {
		return this.expertDate;
	}
	
	/**
	 * @param completeDate
	 */
	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate == null ? null : completeDate.trim();
	}
	
    /**
     * @return CompleteDate
     */	
	public String getCompleteDate() {
		return this.completeDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName == null ? null : managerName.trim();
	}
	
    /**
     * @return ManagerName
     */	
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return ContAmt
     */	
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param contStartDate
	 */
	public void setContStartDate(String contStartDate) {
		this.contStartDate = contStartDate == null ? null : contStartDate.trim();
	}
	
    /**
     * @return ContStartDate
     */	
	public String getContStartDate() {
		return this.contStartDate;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate == null ? null : loanStartDate.trim();
	}
	
    /**
     * @return LoanStartDate
     */	
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param checkMode
	 */
	public void setCheckMode(String checkMode) {
		this.checkMode = checkMode == null ? null : checkMode.trim();
	}
	
    /**
     * @return CheckMode
     */	
	public String getCheckMode() {
		return this.checkMode;
	}
	
	/**
	 * @param checkAddr
	 */
	public void setCheckAddr(String checkAddr) {
		this.checkAddr = checkAddr == null ? null : checkAddr.trim();
	}
	
    /**
     * @return CheckAddr
     */	
	public String getCheckAddr() {
		return this.checkAddr;
	}
	
	/**
	 * @param riskReason
	 */
	public void setRiskReason(String riskReason) {
		this.riskReason = riskReason == null ? null : riskReason.trim();
	}
	
    /**
     * @return RiskReason
     */	
	public String getRiskReason() {
		return this.riskReason;
	}
	
	/**
	 * @param checkAdvice
	 */
	public void setCheckAdvice(String checkAdvice) {
		this.checkAdvice = checkAdvice == null ? null : checkAdvice.trim();
	}
	
    /**
     * @return CheckAdvice
     */	
	public String getCheckAdvice() {
		return this.checkAdvice;
	}
	
	/**
	 * @param approveId
	 */
	public void setApproveId(String approveId) {
		this.approveId = approveId == null ? null : approveId.trim();
	}
	
    /**
     * @return ApproveId
     */	
	public String getApproveId() {
		return this.approveId;
	}
	
	/**
	 * @param approveName
	 */
	public void setApproveName(String approveName) {
		this.approveName = approveName == null ? null : approveName.trim();
	}
	
    /**
     * @return ApproveName
     */	
	public String getApproveName() {
		return this.approveName;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}


}