package cn.com.yusys.yusp.dto.server.xddh0017.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：贷后任务完成接收接口
 *
 * @author wrw
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0017DataReqDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;//贷后任务流水号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "chkState")
    private String chkState;//检查状态  0-待安排|1-待执行|2-执行中|3-已提交补全|4-提交审核|5-已办结|6-已过期
    @JsonProperty(value = "chkDate")
    private String chkDate;//完成日期
    @JsonProperty(value = "chkOpt")
    private String chkOpt;//检查意见
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理编号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名称

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getChkState() {
        return chkState;
    }

    public void setChkState(String chkState) {
        this.chkState = chkState;
    }

    public String getChkDate() {
        return chkDate;
    }

    public void setChkDate(String chkDate) {
        this.chkDate = chkDate;
    }

    public String getChkOpt() {
        return chkOpt;
    }

    public void setChkOpt(String chkOpt) {
        this.chkOpt = chkOpt;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    @Override
    public String toString() {
        return "Xddh0017DataReqDto{" +
                "serno='" + serno + '\'' +
                ", cusId='" + cusId + '\'' +
                ", chkState='" + chkState + '\'' +
                ", chkDate='" + chkDate + '\'' +
                ", chkOpt='" + chkOpt + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                '}';
    }
}

