package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * DocAccSearchDto
 * @author jijian_yx
 * @date 2021/9/28 0:00
 **/
public class DocAccSearchDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 业务发生时间 **/
	private String inputDate;

	/** 业务流水号 **/
	private String bizSerno;

	/** 产品名称 **/
	private String prdName;

	/** 档案编号 **/
	private String docNo;

	/** 业务条线(放款/合同) **/
	private String belgLine;

	/** 检查类型(贷后) **/
	private String checkType;

	/** 合同号(放款/合同) **/
	private String contNo;

	/** 借据号(放款) **/
	private String billNo;

	/** 业务类型标志 **/
	private String bizType;

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getBizSerno() {
		return bizSerno;
	}

	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getBelgLine() {
		return belgLine;
	}

	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}

	public String getCheckType() {
		return checkType;
	}

	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
}