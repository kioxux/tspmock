package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskNo
 * @类描述: psp_task_no数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 20:08:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspTaskNoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 模型编号 **/
	private String modeId;
	
	/** 检查类型 **/
	private String chekType;
	
	/** 授信品种 **/
	private String lmtType;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 检查数量 **/
	private java.math.BigDecimal checkNum;
	
	/** 检查金额 **/
	private java.math.BigDecimal checkAmt;
	
	/** 任务执行人 **/
	private String tskExeId;
	
	/** 任务执行机构 **/
	private String tskExeBrId;
	
	/** 任务生成日期 **/
	private String tskCreateDate;
	
	/** 要求完成日期 **/
	private String tskFinishDate;
	
	/** 计划完成日期 **/
	private String tskPlanDate;
	
	/** 检查日期 **/
	private String chekDate;
	
	/** 检查方式 **/
	private String chekMode;
	
	/** 检查状态 **/
	private String chekStatus;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 资金流向是否异常 **/
	private String isAbnCurfund;
	
	/** 是否自动化 **/
	private String isAuto;
	
	/** 业务流水号 **/
	private String bizSerno;
	
	/** 放款时间 **/
	private String disbTime;
	
	/** 放款金额（元） **/
	private java.math.BigDecimal disbAmt;
	
	/** 是否总行分配 **/
	private String isHoDivis;
	
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param modeId
	 */
	public void setModeId(String modeId) {
		this.modeId = modeId == null ? null : modeId.trim();
	}
	
    /**
     * @return ModeId
     */	
	public String getModeId() {
		return this.modeId;
	}
	
	/**
	 * @param chekType
	 */
	public void setChekType(String chekType) {
		this.chekType = chekType == null ? null : chekType.trim();
	}
	
    /**
     * @return ChekType
     */	
	public String getChekType() {
		return this.chekType;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType == null ? null : lmtType.trim();
	}
	
    /**
     * @return LmtType
     */	
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param checkNum
	 */
	public void setCheckNum(java.math.BigDecimal checkNum) {
		this.checkNum = checkNum;
	}
	
    /**
     * @return CheckNum
     */	
	public java.math.BigDecimal getCheckNum() {
		return this.checkNum;
	}
	
	/**
	 * @param checkAmt
	 */
	public void setCheckAmt(java.math.BigDecimal checkAmt) {
		this.checkAmt = checkAmt;
	}
	
    /**
     * @return CheckAmt
     */	
	public java.math.BigDecimal getCheckAmt() {
		return this.checkAmt;
	}
	
	/**
	 * @param tskExeId
	 */
	public void setTskExeId(String tskExeId) {
		this.tskExeId = tskExeId == null ? null : tskExeId.trim();
	}
	
    /**
     * @return TskExeId
     */	
	public String getTskExeId() {
		return this.tskExeId;
	}
	
	/**
	 * @param tskExeBrId
	 */
	public void setTskExeBrId(String tskExeBrId) {
		this.tskExeBrId = tskExeBrId == null ? null : tskExeBrId.trim();
	}
	
    /**
     * @return TskExeBrId
     */	
	public String getTskExeBrId() {
		return this.tskExeBrId;
	}
	
	/**
	 * @param tskCreateDate
	 */
	public void setTskCreateDate(String tskCreateDate) {
		this.tskCreateDate = tskCreateDate == null ? null : tskCreateDate.trim();
	}
	
    /**
     * @return TskCreateDate
     */	
	public String getTskCreateDate() {
		return this.tskCreateDate;
	}
	
	/**
	 * @param tskFinishDate
	 */
	public void setTskFinishDate(String tskFinishDate) {
		this.tskFinishDate = tskFinishDate == null ? null : tskFinishDate.trim();
	}
	
    /**
     * @return TskFinishDate
     */	
	public String getTskFinishDate() {
		return this.tskFinishDate;
	}
	
	/**
	 * @param tskPlanDate
	 */
	public void setTskPlanDate(String tskPlanDate) {
		this.tskPlanDate = tskPlanDate == null ? null : tskPlanDate.trim();
	}
	
    /**
     * @return TskPlanDate
     */	
	public String getTskPlanDate() {
		return this.tskPlanDate;
	}
	
	/**
	 * @param chekDate
	 */
	public void setChekDate(String chekDate) {
		this.chekDate = chekDate == null ? null : chekDate.trim();
	}
	
    /**
     * @return ChekDate
     */	
	public String getChekDate() {
		return this.chekDate;
	}
	
	/**
	 * @param chekMode
	 */
	public void setChekMode(String chekMode) {
		this.chekMode = chekMode == null ? null : chekMode.trim();
	}
	
    /**
     * @return ChekMode
     */	
	public String getChekMode() {
		return this.chekMode;
	}
	
	/**
	 * @param chekStatus
	 */
	public void setChekStatus(String chekStatus) {
		this.chekStatus = chekStatus == null ? null : chekStatus.trim();
	}
	
    /**
     * @return ChekStatus
     */	
	public String getChekStatus() {
		return this.chekStatus;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param isAbnCurfund
	 */
	public void setIsAbnCurfund(String isAbnCurfund) {
		this.isAbnCurfund = isAbnCurfund == null ? null : isAbnCurfund.trim();
	}
	
    /**
     * @return IsAbnCurfund
     */	
	public String getIsAbnCurfund() {
		return this.isAbnCurfund;
	}
	
	/**
	 * @param isAuto
	 */
	public void setIsAuto(String isAuto) {
		this.isAuto = isAuto == null ? null : isAuto.trim();
	}
	
    /**
     * @return IsAuto
     */	
	public String getIsAuto() {
		return this.isAuto;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}
	
    /**
     * @return BizSerno
     */	
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param disbTime
	 */
	public void setDisbTime(String disbTime) {
		this.disbTime = disbTime == null ? null : disbTime.trim();
	}
	
    /**
     * @return DisbTime
     */	
	public String getDisbTime() {
		return this.disbTime;
	}
	
	/**
	 * @param disbAmt
	 */
	public void setDisbAmt(java.math.BigDecimal disbAmt) {
		this.disbAmt = disbAmt;
	}
	
    /**
     * @return DisbAmt
     */	
	public java.math.BigDecimal getDisbAmt() {
		return this.disbAmt;
	}
	
	/**
	 * @param isHoDivis
	 */
	public void setIsHoDivis(String isHoDivis) {
		this.isHoDivis = isHoDivis == null ? null : isHoDivis.trim();
	}
	
    /**
     * @return IsHoDivis
     */	
	public String getIsHoDivis() {
		return this.isHoDivis;
	}


}