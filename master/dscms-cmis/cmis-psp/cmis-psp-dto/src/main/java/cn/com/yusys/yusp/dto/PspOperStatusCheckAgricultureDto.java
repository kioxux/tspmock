package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckAgriculture
 * @类描述: psp_oper_status_check_agriculture数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspOperStatusCheckAgricultureDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 是否受疫情影响 **/
	private String isEpidemicImp;
	
	/** 疫情影响说明 **/
	private String epidemicExpl;
	
	/** 是否受天灾影响 **/
	private String isWeatherImp;
	
	/** 天灾影响说明 **/
	private String weatherExpl;
	
	/** 主要产品价格是否异常波动 **/
	private String priceAbn;
	
	/** 价格异常波动说明 **/
	private String priceAbnExpl;
	
	/** 上期主要产品1 **/
	private String prePrdOne;
	
	/** 上期产品价格1 **/
	private java.math.BigDecimal prePriceOne;
	
	/** 上期产品规模1 **/
	private String preScaleOne;
	
	/** 上期主要产品2 **/
	private String prePrdTwo;
	
	/** 上期产品价格2 **/
	private java.math.BigDecimal prePriceTwo;
	
	/** 上期产品规模2 **/
	private String preScaleTwo;
	
	/** 上期主要产品3 **/
	private String prePrdThree;
	
	/** 上期产品价格3 **/
	private java.math.BigDecimal prePriceThree;
	
	/** 上期产品规模3 **/
	private String preScaleThree;
	
	/** 本期主要产品1 **/
	private String curtPrdOne;
	
	/** 本期产品价格1 **/
	private java.math.BigDecimal curtScaleOne;
	
	/** 本期产品规模1 **/
	private String curtPriceOne;
	
	/** 本期主要产品2 **/
	private String curtPrdTwo;
	
	/** 本期产品价格2 **/
	private java.math.BigDecimal curtScaleTwo;
	
	/** 本期产品规模2 **/
	private String curtPriceTwo;
	
	/** 本期主要产品3 **/
	private String curtPrdThree;
	
	/** 本期产品价格3 **/
	private java.math.BigDecimal curtScaleThree;
	
	/** 本期产品规模3 **/
	private String curtPriceThree;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isEpidemicImp
	 */
	public void setIsEpidemicImp(String isEpidemicImp) {
		this.isEpidemicImp = isEpidemicImp == null ? null : isEpidemicImp.trim();
	}
	
    /**
     * @return IsEpidemicImp
     */	
	public String getIsEpidemicImp() {
		return this.isEpidemicImp;
	}
	
	/**
	 * @param epidemicExpl
	 */
	public void setEpidemicExpl(String epidemicExpl) {
		this.epidemicExpl = epidemicExpl == null ? null : epidemicExpl.trim();
	}
	
    /**
     * @return EpidemicExpl
     */	
	public String getEpidemicExpl() {
		return this.epidemicExpl;
	}
	
	/**
	 * @param isWeatherImp
	 */
	public void setIsWeatherImp(String isWeatherImp) {
		this.isWeatherImp = isWeatherImp == null ? null : isWeatherImp.trim();
	}
	
    /**
     * @return IsWeatherImp
     */	
	public String getIsWeatherImp() {
		return this.isWeatherImp;
	}
	
	/**
	 * @param weatherExpl
	 */
	public void setWeatherExpl(String weatherExpl) {
		this.weatherExpl = weatherExpl == null ? null : weatherExpl.trim();
	}
	
    /**
     * @return WeatherExpl
     */	
	public String getWeatherExpl() {
		return this.weatherExpl;
	}
	
	/**
	 * @param priceAbn
	 */
	public void setPriceAbn(String priceAbn) {
		this.priceAbn = priceAbn == null ? null : priceAbn.trim();
	}
	
    /**
     * @return PriceAbn
     */	
	public String getPriceAbn() {
		return this.priceAbn;
	}
	
	/**
	 * @param priceAbnExpl
	 */
	public void setPriceAbnExpl(String priceAbnExpl) {
		this.priceAbnExpl = priceAbnExpl == null ? null : priceAbnExpl.trim();
	}
	
    /**
     * @return PriceAbnExpl
     */	
	public String getPriceAbnExpl() {
		return this.priceAbnExpl;
	}
	
	/**
	 * @param prePrdOne
	 */
	public void setPrePrdOne(String prePrdOne) {
		this.prePrdOne = prePrdOne == null ? null : prePrdOne.trim();
	}
	
    /**
     * @return PrePrdOne
     */	
	public String getPrePrdOne() {
		return this.prePrdOne;
	}
	
	/**
	 * @param prePriceOne
	 */
	public void setPrePriceOne(java.math.BigDecimal prePriceOne) {
		this.prePriceOne = prePriceOne;
	}
	
    /**
     * @return PrePriceOne
     */	
	public java.math.BigDecimal getPrePriceOne() {
		return this.prePriceOne;
	}
	
	/**
	 * @param preScaleOne
	 */
	public void setPreScaleOne(String preScaleOne) {
		this.preScaleOne = preScaleOne == null ? null : preScaleOne.trim();
	}
	
    /**
     * @return PreScaleOne
     */	
	public String getPreScaleOne() {
		return this.preScaleOne;
	}
	
	/**
	 * @param prePrdTwo
	 */
	public void setPrePrdTwo(String prePrdTwo) {
		this.prePrdTwo = prePrdTwo == null ? null : prePrdTwo.trim();
	}
	
    /**
     * @return PrePrdTwo
     */	
	public String getPrePrdTwo() {
		return this.prePrdTwo;
	}
	
	/**
	 * @param prePriceTwo
	 */
	public void setPrePriceTwo(java.math.BigDecimal prePriceTwo) {
		this.prePriceTwo = prePriceTwo;
	}
	
    /**
     * @return PrePriceTwo
     */	
	public java.math.BigDecimal getPrePriceTwo() {
		return this.prePriceTwo;
	}
	
	/**
	 * @param preScaleTwo
	 */
	public void setPreScaleTwo(String preScaleTwo) {
		this.preScaleTwo = preScaleTwo == null ? null : preScaleTwo.trim();
	}
	
    /**
     * @return PreScaleTwo
     */	
	public String getPreScaleTwo() {
		return this.preScaleTwo;
	}
	
	/**
	 * @param prePrdThree
	 */
	public void setPrePrdThree(String prePrdThree) {
		this.prePrdThree = prePrdThree == null ? null : prePrdThree.trim();
	}
	
    /**
     * @return PrePrdThree
     */	
	public String getPrePrdThree() {
		return this.prePrdThree;
	}
	
	/**
	 * @param prePriceThree
	 */
	public void setPrePriceThree(java.math.BigDecimal prePriceThree) {
		this.prePriceThree = prePriceThree;
	}
	
    /**
     * @return PrePriceThree
     */	
	public java.math.BigDecimal getPrePriceThree() {
		return this.prePriceThree;
	}
	
	/**
	 * @param preScaleThree
	 */
	public void setPreScaleThree(String preScaleThree) {
		this.preScaleThree = preScaleThree == null ? null : preScaleThree.trim();
	}
	
    /**
     * @return PreScaleThree
     */	
	public String getPreScaleThree() {
		return this.preScaleThree;
	}
	
	/**
	 * @param curtPrdOne
	 */
	public void setCurtPrdOne(String curtPrdOne) {
		this.curtPrdOne = curtPrdOne == null ? null : curtPrdOne.trim();
	}
	
    /**
     * @return CurtPrdOne
     */	
	public String getCurtPrdOne() {
		return this.curtPrdOne;
	}
	
	/**
	 * @param curtScaleOne
	 */
	public void setCurtScaleOne(java.math.BigDecimal curtScaleOne) {
		this.curtScaleOne = curtScaleOne;
	}
	
    /**
     * @return CurtScaleOne
     */	
	public java.math.BigDecimal getCurtScaleOne() {
		return this.curtScaleOne;
	}
	
	/**
	 * @param curtPriceOne
	 */
	public void setCurtPriceOne(String curtPriceOne) {
		this.curtPriceOne = curtPriceOne == null ? null : curtPriceOne.trim();
	}
	
    /**
     * @return CurtPriceOne
     */	
	public String getCurtPriceOne() {
		return this.curtPriceOne;
	}
	
	/**
	 * @param curtPrdTwo
	 */
	public void setCurtPrdTwo(String curtPrdTwo) {
		this.curtPrdTwo = curtPrdTwo == null ? null : curtPrdTwo.trim();
	}
	
    /**
     * @return CurtPrdTwo
     */	
	public String getCurtPrdTwo() {
		return this.curtPrdTwo;
	}
	
	/**
	 * @param curtScaleTwo
	 */
	public void setCurtScaleTwo(java.math.BigDecimal curtScaleTwo) {
		this.curtScaleTwo = curtScaleTwo;
	}
	
    /**
     * @return CurtScaleTwo
     */	
	public java.math.BigDecimal getCurtScaleTwo() {
		return this.curtScaleTwo;
	}
	
	/**
	 * @param curtPriceTwo
	 */
	public void setCurtPriceTwo(String curtPriceTwo) {
		this.curtPriceTwo = curtPriceTwo == null ? null : curtPriceTwo.trim();
	}
	
    /**
     * @return CurtPriceTwo
     */	
	public String getCurtPriceTwo() {
		return this.curtPriceTwo;
	}
	
	/**
	 * @param curtPrdThree
	 */
	public void setCurtPrdThree(String curtPrdThree) {
		this.curtPrdThree = curtPrdThree == null ? null : curtPrdThree.trim();
	}
	
    /**
     * @return CurtPrdThree
     */	
	public String getCurtPrdThree() {
		return this.curtPrdThree;
	}
	
	/**
	 * @param curtScaleThree
	 */
	public void setCurtScaleThree(java.math.BigDecimal curtScaleThree) {
		this.curtScaleThree = curtScaleThree;
	}
	
    /**
     * @return CurtScaleThree
     */	
	public java.math.BigDecimal getCurtScaleThree() {
		return this.curtScaleThree;
	}
	
	/**
	 * @param curtPriceThree
	 */
	public void setCurtPriceThree(String curtPriceThree) {
		this.curtPriceThree = curtPriceThree == null ? null : curtPriceThree.trim();
	}
	
    /**
     * @return CurtPriceThree
     */	
	public String getCurtPriceThree() {
		return this.curtPriceThree;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}