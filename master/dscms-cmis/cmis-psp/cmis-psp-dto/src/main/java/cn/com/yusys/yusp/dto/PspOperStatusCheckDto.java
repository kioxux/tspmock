package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheck
 * @类描述: psp_oper_status_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspOperStatusCheckDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 是否对经营场所现场检查 **/
	private String isPhotograph;
	
	/** 确认说明 **/
	private String notPhotographRemark;
	
	/** 目前借款人经营是否正常 **/
	private String isOperNormal;
	
	/** 经营异常说明 **/
	private String notOperNormalRemark;
	
	/** 是否有其他重要风险事项 **/
	private String isOtherRemark;
	
	/** 其他风险说明 **/
	private String otherRemark;
	
	/** 借款人具体经营内容 **/
	private String operContent;
	
	/** 借款人身体是否健康 **/
	private String isHealth;
	
	/** 借款人身体状况说明 **/
	private String notHealthRemark;
	
	/** 上期借款人环评颜色 **/
	private String preEvluRst;
	
	/** 本期借款人环评颜色 **/
	private String curtEvluRst;
	
	/** 环评变化说明 **/
	private String evluChangeRemark;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isPhotograph
	 */
	public void setIsPhotograph(String isPhotograph) {
		this.isPhotograph = isPhotograph == null ? null : isPhotograph.trim();
	}
	
    /**
     * @return IsPhotograph
     */	
	public String getIsPhotograph() {
		return this.isPhotograph;
	}
	
	/**
	 * @param notPhotographRemark
	 */
	public void setNotPhotographRemark(String notPhotographRemark) {
		this.notPhotographRemark = notPhotographRemark == null ? null : notPhotographRemark.trim();
	}
	
    /**
     * @return NotPhotographRemark
     */	
	public String getNotPhotographRemark() {
		return this.notPhotographRemark;
	}
	
	/**
	 * @param isOperNormal
	 */
	public void setIsOperNormal(String isOperNormal) {
		this.isOperNormal = isOperNormal == null ? null : isOperNormal.trim();
	}
	
    /**
     * @return IsOperNormal
     */	
	public String getIsOperNormal() {
		return this.isOperNormal;
	}
	
	/**
	 * @param notOperNormalRemark
	 */
	public void setNotOperNormalRemark(String notOperNormalRemark) {
		this.notOperNormalRemark = notOperNormalRemark == null ? null : notOperNormalRemark.trim();
	}
	
    /**
     * @return NotOperNormalRemark
     */	
	public String getNotOperNormalRemark() {
		return this.notOperNormalRemark;
	}
	
	/**
	 * @param isOtherRemark
	 */
	public void setIsOtherRemark(String isOtherRemark) {
		this.isOtherRemark = isOtherRemark == null ? null : isOtherRemark.trim();
	}
	
    /**
     * @return IsOtherRemark
     */	
	public String getIsOtherRemark() {
		return this.isOtherRemark;
	}
	
	/**
	 * @param otherRemark
	 */
	public void setOtherRemark(String otherRemark) {
		this.otherRemark = otherRemark == null ? null : otherRemark.trim();
	}
	
    /**
     * @return OtherRemark
     */	
	public String getOtherRemark() {
		return this.otherRemark;
	}
	
	/**
	 * @param operContent
	 */
	public void setOperContent(String operContent) {
		this.operContent = operContent == null ? null : operContent.trim();
	}
	
    /**
     * @return OperContent
     */	
	public String getOperContent() {
		return this.operContent;
	}
	
	/**
	 * @param isHealth
	 */
	public void setIsHealth(String isHealth) {
		this.isHealth = isHealth == null ? null : isHealth.trim();
	}
	
    /**
     * @return IsHealth
     */	
	public String getIsHealth() {
		return this.isHealth;
	}
	
	/**
	 * @param notHealthRemark
	 */
	public void setNotHealthRemark(String notHealthRemark) {
		this.notHealthRemark = notHealthRemark == null ? null : notHealthRemark.trim();
	}
	
    /**
     * @return NotHealthRemark
     */	
	public String getNotHealthRemark() {
		return this.notHealthRemark;
	}
	
	/**
	 * @param preEvluRst
	 */
	public void setPreEvluRst(String preEvluRst) {
		this.preEvluRst = preEvluRst == null ? null : preEvluRst.trim();
	}
	
    /**
     * @return PreEvluRst
     */	
	public String getPreEvluRst() {
		return this.preEvluRst;
	}
	
	/**
	 * @param curtEvluRst
	 */
	public void setCurtEvluRst(String curtEvluRst) {
		this.curtEvluRst = curtEvluRst == null ? null : curtEvluRst.trim();
	}
	
    /**
     * @return CurtEvluRst
     */	
	public String getCurtEvluRst() {
		return this.curtEvluRst;
	}
	
	/**
	 * @param evluChangeRemark
	 */
	public void setEvluChangeRemark(String evluChangeRemark) {
		this.evluChangeRemark = evluChangeRemark == null ? null : evluChangeRemark.trim();
	}
	
    /**
     * @return EvluChangeRemark
     */	
	public String getEvluChangeRemark() {
		return this.evluChangeRemark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}