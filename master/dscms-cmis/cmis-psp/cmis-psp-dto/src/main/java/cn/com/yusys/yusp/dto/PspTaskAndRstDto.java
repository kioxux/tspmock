package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskList
 * @类描述: psp_task_list数据实体类
 * @功能描述: 
 * @创建人: hanl
 * @创建时间: 2021-06-13 19:39:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspTaskAndRstDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 任务类型 **/
	private String taskType;
	
	/** 检查类型 **/
	private String checkType;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 任务生成日期 **/
	private String taskStartDt;
	
	/** 任务要求完成日期 **/
	private String taskEndDt;
	
	/** 任务执行人 **/
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="execIdName")
	private String execId;
	
	/** 任务执行机构 **/
	@RedisCacheTranslator(redisCacheKey = "orgName" ,refFieldName="execBrIdName")
	private String execBrId;
	
	/** 检查状态 **/
	private String checkStatus;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 报告类型 **/
	private String rptType;
	
	/** 检查日期 **/
	private String checkDate;
	
	/** 借据起始日 **/
	private String loanStartDate;
	
	/** 借据到期日 **/
	private String loanEndDate;
	
	/** 风险等级 **/
	private String riskLvl;
	
	/** 担保方式 **/
	private String guarMode;
	
	/** 任务派发人员 **/
	private String issueId;
	
	/** 任务派发人员所属机构 **/
	private String issueBrId;
	
	/** 任务下发日期 **/
	private String issueDate;
	
	/** 贷款金额（元）  **/
	private java.math.BigDecimal loanAmt;

	/** 是否受托支付 **/
	private String isUntruPayType;

	/** 购销合同金额 **/
	private java.math.BigDecimal contAmt;

	/** 交易对手名称 **/
	private String contTranName;

	/** 贷款用途 **/
	private String contAgreedUse;

	/** 贷款资金是否存在回流 **/
	private String isCapBack;

	/** 与授信用途是否一致 **/
	private String isLmtUse;

	/** 贷款资金流向说明 **/
	private String capFlowRemark;

	/** 检查结果及检查人意见 **/
	private String checkComment;

	/** 协查人意见 **/
	private String assistComment;

	/** 协查人 **/
	private String assistId;

	/** 协查机构 **/
	private String assistOrgId;

	/** 协查日期 **/
	private String assistDate;

	/** 约见人 **/
	private String relatedPersonnel;

	/** 约见人职务 **/
	private String relatedJobTtl;

	/** 检查方式 **/
	private String checkMode;

	/** 是否落实抵质押或担保 **/
	private String isGuarImple;

	/** 未落实说明 **/
	private String impleRemark;

	/** 检查地点 **/
	private String checkPlace;

	/** 管户客户经理授信建议 **/
	private String checkAdviceType;

	/** 管户客户经理授信建议理由 **/
	private String checkAdviceReason;

	/** 协办客户经理授信建议 **/
	private String assistAdviceType;

	/** 协办客户经理授信建议理由 **/
	private String assistAdviceReason;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;

	private String isSaved;
	/** 任务状态**/
    private String taskStatus;
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}

	/**
	 * @return PkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}

	/**
	 * @return TaskNo
	 */
	public String getTaskNo() {
		return this.taskNo;
	}

	/**
	 * @param isUntruPayType
	 */
	public void setIsUntruPayType(String isUntruPayType) {
		this.isUntruPayType = isUntruPayType == null ? null : isUntruPayType.trim();
	}

	/**
	 * @return IsUntruPayType
	 */
	public String getIsUntruPayType() {
		return this.isUntruPayType;
	}

	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	/**
	 * @return ContAmt
	 */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}

	/**
	 * @param contTranName
	 */
	public void setContTranName(String contTranName) {
		this.contTranName = contTranName == null ? null : contTranName.trim();
	}

	/**
	 * @return ContTranName
	 */
	public String getContTranName() {
		return this.contTranName;
	}

	/**
	 * @param contAgreedUse
	 */
	public void setContAgreedUse(String contAgreedUse) {
		this.contAgreedUse = contAgreedUse == null ? null : contAgreedUse.trim();
	}

	/**
	 * @return ContAgreedUse
	 */
	public String getContAgreedUse() {
		return this.contAgreedUse;
	}

	/**
	 * @param isCapBack
	 */
	public void setIsCapBack(String isCapBack) {
		this.isCapBack = isCapBack == null ? null : isCapBack.trim();
	}

	/**
	 * @return IsCapBack
	 */
	public String getIsCapBack() {
		return this.isCapBack;
	}

	/**
	 * @param isLmtUse
	 */
	public void setIsLmtUse(String isLmtUse) {
		this.isLmtUse = isLmtUse == null ? null : isLmtUse.trim();
	}

	/**
	 * @return IsLmtUse
	 */
	public String getIsLmtUse() {
		return this.isLmtUse;
	}

	/**
	 * @param capFlowRemark
	 */
	public void setCapFlowRemark(String capFlowRemark) {
		this.capFlowRemark = capFlowRemark == null ? null : capFlowRemark.trim();
	}

	/**
	 * @return CapFlowRemark
	 */
	public String getCapFlowRemark() {
		return this.capFlowRemark;
	}

	/**
	 * @param checkComment
	 */
	public void setCheckComment(String checkComment) {
		this.checkComment = checkComment == null ? null : checkComment.trim();
	}

	/**
	 * @return CheckComment
	 */
	public String getCheckComment() {
		return this.checkComment;
	}

	/**
	 * @param assistComment
	 */
	public void setAssistComment(String assistComment) {
		this.assistComment = assistComment == null ? null : assistComment.trim();
	}

	/**
	 * @return AssistComment
	 */
	public String getAssistComment() {
		return this.assistComment;
	}

	/**
	 * @param assistId
	 */
	public void setAssistId(String assistId) {
		this.assistId = assistId == null ? null : assistId.trim();
	}

	/**
	 * @return AssistId
	 */
	public String getAssistId() {
		return this.assistId;
	}

	/**
	 * @param assistOrgId
	 */
	public void setAssistOrgId(String assistOrgId) {
		this.assistOrgId = assistOrgId == null ? null : assistOrgId.trim();
	}

	/**
	 * @return AssistOrgId
	 */
	public String getAssistOrgId() {
		return this.assistOrgId;
	}

	/**
	 * @param assistDate
	 */
	public void setAssistDate(String assistDate) {
		this.assistDate = assistDate == null ? null : assistDate.trim();
	}

	/**
	 * @return AssistDate
	 */
	public String getAssistDate() {
		return this.assistDate;
	}

	/**
	 * @param relatedPersonnel
	 */
	public void setRelatedPersonnel(String relatedPersonnel) {
		this.relatedPersonnel = relatedPersonnel == null ? null : relatedPersonnel.trim();
	}

	/**
	 * @return RelatedPersonnel
	 */
	public String getRelatedPersonnel() {
		return this.relatedPersonnel;
	}

	/**
	 * @param relatedJobTtl
	 */
	public void setRelatedJobTtl(String relatedJobTtl) {
		this.relatedJobTtl = relatedJobTtl == null ? null : relatedJobTtl.trim();
	}

	/**
	 * @return RelatedJobTtl
	 */
	public String getRelatedJobTtl() {
		return this.relatedJobTtl;
	}

	/**
	 * @param checkMode
	 */
	public void setCheckMode(String checkMode) {
		this.checkMode = checkMode == null ? null : checkMode.trim();
	}

	/**
	 * @return CheckMode
	 */
	public String getCheckMode() {
		return this.checkMode;
	}

	/**
	 * @param isGuarImple
	 */
	public void setIsGuarImple(String isGuarImple) {
		this.isGuarImple = isGuarImple == null ? null : isGuarImple.trim();
	}

	/**
	 * @return IsGuarImple
	 */
	public String getIsGuarImple() {
		return this.isGuarImple;
	}

	/**
	 * @param impleRemark
	 */
	public void setImpleRemark(String impleRemark) {
		this.impleRemark = impleRemark == null ? null : impleRemark.trim();
	}

	/**
	 * @return ImpleRemark
	 */
	public String getImpleRemark() {
		return this.impleRemark;
	}

	/**
	 * @param checkPlace
	 */
	public void setCheckPlace(String checkPlace) {
		this.checkPlace = checkPlace == null ? null : checkPlace.trim();
	}

	/**
	 * @return CheckPlace
	 */
	public String getCheckPlace() {
		return this.checkPlace;
	}

	/**
	 * @param checkAdviceType
	 */
	public void setCheckAdviceType(String checkAdviceType) {
		this.checkAdviceType = checkAdviceType == null ? null : checkAdviceType.trim();
	}

	/**
	 * @return CheckAdviceType
	 */
	public String getCheckAdviceType() {
		return this.checkAdviceType;
	}

	/**
	 * @param checkAdviceReason
	 */
	public void setCheckAdviceReason(String checkAdviceReason) {
		this.checkAdviceReason = checkAdviceReason == null ? null : checkAdviceReason.trim();
	}

	/**
	 * @return CheckAdviceReason
	 */
	public String getCheckAdviceReason() {
		return this.checkAdviceReason;
	}

	/**
	 * @param assistAdviceType
	 */
	public void setAssistAdviceType(String assistAdviceType) {
		this.assistAdviceType = assistAdviceType == null ? null : assistAdviceType.trim();
	}

	/**
	 * @return AssistAdviceType
	 */
	public String getAssistAdviceType() {
		return this.assistAdviceType;
	}

	/**
	 * @param assistAdviceReason
	 */
	public void setAssistAdviceReason(String assistAdviceReason) {
		this.assistAdviceReason = assistAdviceReason == null ? null : assistAdviceReason.trim();
	}

	/**
	 * @return AssistAdviceReason
	 */
	public String getAssistAdviceReason() {
		return this.assistAdviceReason;
	}
	
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType == null ? null : taskType.trim();
	}
	
    /**
     * @return TaskType
     */	
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param checkType
	 */
	public void setCheckType(String checkType) {
		this.checkType = checkType == null ? null : checkType.trim();
	}
	
    /**
     * @return CheckType
     */	
	public String getCheckType() {
		return this.checkType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param taskStartDt
	 */
	public void setTaskStartDt(String taskStartDt) {
		this.taskStartDt = taskStartDt == null ? null : taskStartDt.trim();
	}
	
    /**
     * @return TaskStartDt
     */	
	public String getTaskStartDt() {
		return this.taskStartDt;
	}
	
	/**
	 * @param taskEndDt
	 */
	public void setTaskEndDt(String taskEndDt) {
		this.taskEndDt = taskEndDt == null ? null : taskEndDt.trim();
	}
	
    /**
     * @return TaskEndDt
     */	
	public String getTaskEndDt() {
		return this.taskEndDt;
	}
	
	/**
	 * @param execId
	 */
	public void setExecId(String execId) {
		this.execId = execId == null ? null : execId.trim();
	}
	
    /**
     * @return ExecId
     */	
	public String getExecId() {
		return this.execId;
	}
	
	/**
	 * @param execBrId
	 */
	public void setExecBrId(String execBrId) {
		this.execBrId = execBrId == null ? null : execBrId.trim();
	}
	
    /**
     * @return ExecBrId
     */	
	public String getExecBrId() {
		return this.execBrId;
	}
	
	/**
	 * @param checkStatus
	 */
	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus == null ? null : checkStatus.trim();
	}
	
    /**
     * @return CheckStatus
     */	
	public String getCheckStatus() {
		return this.checkStatus;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param rptType
	 */
	public void setRptType(String rptType) {
		this.rptType = rptType == null ? null : rptType.trim();
	}
	
    /**
     * @return RptType
     */	
	public String getRptType() {
		return this.rptType;
	}
	
	/**
	 * @param checkDate
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate == null ? null : checkDate.trim();
	}
	
    /**
     * @return CheckDate
     */	
	public String getCheckDate() {
		return this.checkDate;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate == null ? null : loanStartDate.trim();
	}
	
    /**
     * @return LoanStartDate
     */	
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate == null ? null : loanEndDate.trim();
	}
	
    /**
     * @return LoanEndDate
     */	
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param riskLvl
	 */
	public void setRiskLvl(String riskLvl) {
		this.riskLvl = riskLvl == null ? null : riskLvl.trim();
	}
	
    /**
     * @return RiskLvl
     */	
	public String getRiskLvl() {
		return this.riskLvl;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param issueId
	 */
	public void setIssueId(String issueId) {
		this.issueId = issueId == null ? null : issueId.trim();
	}
	
    /**
     * @return IssueId
     */	
	public String getIssueId() {
		return this.issueId;
	}
	
	/**
	 * @param issueBrId
	 */
	public void setIssueBrId(String issueBrId) {
		this.issueBrId = issueBrId == null ? null : issueBrId.trim();
	}
	
    /**
     * @return IssueBrId
     */	
	public String getIssueBrId() {
		return this.issueBrId;
	}
	
	/**
	 * @param issueDate
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate == null ? null : issueDate.trim();
	}
	
    /**
     * @return IssueDate
     */	
	public String getIssueDate() {
		return this.issueDate;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return CreateTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return UpdateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


	public String getIsSaved() {
		return isSaved;
	}

	public void setIsSaved(String isSaved) {
		this.isSaved = isSaved;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
}