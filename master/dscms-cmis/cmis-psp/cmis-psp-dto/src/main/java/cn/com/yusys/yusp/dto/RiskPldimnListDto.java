package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskPldimnList
 * @类描述: risk_pldimn_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskPldimnListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 押品编号 **/
	private String pldimnNo;
	
	/** 押品类型 **/
	private String pldimnType;
	
	/** 抵质押类别 **/
	private String guarType;
	
	/** 押品名称 **/
	private String pldimnMemo;
	
	/** 所有权人 **/
	private String guarCusId;
	
	/** 所有权人名称 **/
	private String guarCusName;
	
	/** 评估价值 **/
	private java.math.BigDecimal evalAmt;
	
	/** 认定价值 **/
	private java.math.BigDecimal confirmAmt;
	
	/** 抵（质）押品可执行能力 **/
	private String pldimnExeAbi;
	
	/** 抵质押品价值(元) **/
	private java.math.BigDecimal pldimnAmt;
	
	/** 抵质押率 **/
	private java.math.BigDecimal mortagageRate;
	
	/** 抵质押品价值情况 **/
	private String guarValueSitu;
	
	/** 抵（质）押品的价值评估方式 **/
	private String guarEvalType;
	
	/** 抵押/质押品情况备注 **/
	private String pldimnRemark;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param pldimnNo
	 */
	public void setPldimnNo(String pldimnNo) {
		this.pldimnNo = pldimnNo == null ? null : pldimnNo.trim();
	}
	
    /**
     * @return PldimnNo
     */	
	public String getPldimnNo() {
		return this.pldimnNo;
	}
	
	/**
	 * @param pldimnType
	 */
	public void setPldimnType(String pldimnType) {
		this.pldimnType = pldimnType == null ? null : pldimnType.trim();
	}
	
    /**
     * @return PldimnType
     */	
	public String getPldimnType() {
		return this.pldimnType;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType == null ? null : guarType.trim();
	}
	
    /**
     * @return GuarType
     */	
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo == null ? null : pldimnMemo.trim();
	}
	
    /**
     * @return PldimnMemo
     */	
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId == null ? null : guarCusId.trim();
	}
	
    /**
     * @return GuarCusId
     */	
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName == null ? null : guarCusName.trim();
	}
	
    /**
     * @return GuarCusName
     */	
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return EvalAmt
     */	
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param confirmAmt
	 */
	public void setConfirmAmt(java.math.BigDecimal confirmAmt) {
		this.confirmAmt = confirmAmt;
	}
	
    /**
     * @return ConfirmAmt
     */	
	public java.math.BigDecimal getConfirmAmt() {
		return this.confirmAmt;
	}
	
	/**
	 * @param pldimnExeAbi
	 */
	public void setPldimnExeAbi(String pldimnExeAbi) {
		this.pldimnExeAbi = pldimnExeAbi == null ? null : pldimnExeAbi.trim();
	}
	
    /**
     * @return PldimnExeAbi
     */	
	public String getPldimnExeAbi() {
		return this.pldimnExeAbi;
	}
	
	/**
	 * @param pldimnAmt
	 */
	public void setPldimnAmt(java.math.BigDecimal pldimnAmt) {
		this.pldimnAmt = pldimnAmt;
	}
	
    /**
     * @return PldimnAmt
     */	
	public java.math.BigDecimal getPldimnAmt() {
		return this.pldimnAmt;
	}
	
	/**
	 * @param mortagageRate
	 */
	public void setMortagageRate(java.math.BigDecimal mortagageRate) {
		this.mortagageRate = mortagageRate;
	}
	
    /**
     * @return MortagageRate
     */	
	public java.math.BigDecimal getMortagageRate() {
		return this.mortagageRate;
	}
	
	/**
	 * @param guarValueSitu
	 */
	public void setGuarValueSitu(String guarValueSitu) {
		this.guarValueSitu = guarValueSitu == null ? null : guarValueSitu.trim();
	}
	
    /**
     * @return GuarValueSitu
     */	
	public String getGuarValueSitu() {
		return this.guarValueSitu;
	}
	
	/**
	 * @param guarEvalType
	 */
	public void setGuarEvalType(String guarEvalType) {
		this.guarEvalType = guarEvalType == null ? null : guarEvalType.trim();
	}
	
    /**
     * @return GuarEvalType
     */	
	public String getGuarEvalType() {
		return this.guarEvalType;
	}
	
	/**
	 * @param pldimnRemark
	 */
	public void setPldimnRemark(String pldimnRemark) {
		this.pldimnRemark = pldimnRemark == null ? null : pldimnRemark.trim();
	}
	
    /**
     * @return PldimnRemark
     */	
	public String getPldimnRemark() {
		return this.pldimnRemark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}