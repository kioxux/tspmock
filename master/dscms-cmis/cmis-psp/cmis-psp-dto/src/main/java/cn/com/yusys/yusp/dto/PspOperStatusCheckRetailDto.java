package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckRetail
 * @类描述: psp_oper_status_check_retail数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspOperStatusCheckRetailDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 上期主要产品1 **/
	private String prePrd1;
	
	/** 上期主要产品1市场情况 **/
	private String preSitu1;
	
	/** 上期主要产品2 **/
	private String prePrd2;
	
	/** 上期主要产品2市场情况 **/
	private String preSitu2;
	
	/** 上期主要产品3 **/
	private String prePrd3;
	
	/** 上期主要产品3市场情况 **/
	private String preSitu3;
	
	/** 本期主要产品1 **/
	private String curtPrd1;
	
	/** 本期主要产品1市场情况 **/
	private String curtSitu1;
	
	/** 本期主要产品2 **/
	private String curtPrd2;
	
	/** 本期主要产品2市场情况 **/
	private String curtSitu2;
	
	/** 本期主要产品3 **/
	private String curtPrd3;
	
	/** 本期主要产品3市场情况 **/
	private String curtSitu3;
	
	/** 说明 **/
	private String remark;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param prePrd1
	 */
	public void setPrePrd1(String prePrd1) {
		this.prePrd1 = prePrd1 == null ? null : prePrd1.trim();
	}
	
    /**
     * @return PrePrd1
     */	
	public String getPrePrd1() {
		return this.prePrd1;
	}
	
	/**
	 * @param preSitu1
	 */
	public void setPreSitu1(String preSitu1) {
		this.preSitu1 = preSitu1 == null ? null : preSitu1.trim();
	}
	
    /**
     * @return PreSitu1
     */	
	public String getPreSitu1() {
		return this.preSitu1;
	}
	
	/**
	 * @param prePrd2
	 */
	public void setPrePrd2(String prePrd2) {
		this.prePrd2 = prePrd2 == null ? null : prePrd2.trim();
	}
	
    /**
     * @return PrePrd2
     */	
	public String getPrePrd2() {
		return this.prePrd2;
	}
	
	/**
	 * @param preSitu2
	 */
	public void setPreSitu2(String preSitu2) {
		this.preSitu2 = preSitu2 == null ? null : preSitu2.trim();
	}
	
    /**
     * @return PreSitu2
     */	
	public String getPreSitu2() {
		return this.preSitu2;
	}
	
	/**
	 * @param prePrd3
	 */
	public void setPrePrd3(String prePrd3) {
		this.prePrd3 = prePrd3 == null ? null : prePrd3.trim();
	}
	
    /**
     * @return PrePrd3
     */	
	public String getPrePrd3() {
		return this.prePrd3;
	}
	
	/**
	 * @param preSitu3
	 */
	public void setPreSitu3(String preSitu3) {
		this.preSitu3 = preSitu3 == null ? null : preSitu3.trim();
	}
	
    /**
     * @return PreSitu3
     */	
	public String getPreSitu3() {
		return this.preSitu3;
	}
	
	/**
	 * @param curtPrd1
	 */
	public void setCurtPrd1(String curtPrd1) {
		this.curtPrd1 = curtPrd1 == null ? null : curtPrd1.trim();
	}
	
    /**
     * @return CurtPrd1
     */	
	public String getCurtPrd1() {
		return this.curtPrd1;
	}
	
	/**
	 * @param curtSitu1
	 */
	public void setCurtSitu1(String curtSitu1) {
		this.curtSitu1 = curtSitu1 == null ? null : curtSitu1.trim();
	}
	
    /**
     * @return CurtSitu1
     */	
	public String getCurtSitu1() {
		return this.curtSitu1;
	}
	
	/**
	 * @param curtPrd2
	 */
	public void setCurtPrd2(String curtPrd2) {
		this.curtPrd2 = curtPrd2 == null ? null : curtPrd2.trim();
	}
	
    /**
     * @return CurtPrd2
     */	
	public String getCurtPrd2() {
		return this.curtPrd2;
	}
	
	/**
	 * @param curtSitu2
	 */
	public void setCurtSitu2(String curtSitu2) {
		this.curtSitu2 = curtSitu2 == null ? null : curtSitu2.trim();
	}
	
    /**
     * @return CurtSitu2
     */	
	public String getCurtSitu2() {
		return this.curtSitu2;
	}
	
	/**
	 * @param curtPrd3
	 */
	public void setCurtPrd3(String curtPrd3) {
		this.curtPrd3 = curtPrd3 == null ? null : curtPrd3.trim();
	}
	
    /**
     * @return CurtPrd3
     */	
	public String getCurtPrd3() {
		return this.curtPrd3;
	}
	
	/**
	 * @param curtSitu3
	 */
	public void setCurtSitu3(String curtSitu3) {
		this.curtSitu3 = curtSitu3 == null ? null : curtSitu3.trim();
	}
	
    /**
     * @return CurtSitu3
     */	
	public String getCurtSitu3() {
		return this.curtSitu3;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}