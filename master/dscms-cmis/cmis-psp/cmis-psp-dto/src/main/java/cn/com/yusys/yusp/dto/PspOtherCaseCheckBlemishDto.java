package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOtherCaseCheckBlemish
 * @类描述: psp_other_case_check_blemish数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspOtherCaseCheckBlemishDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 瑕疵贷款形成原因 **/
	private String blemishReason;
	
	/** 是否会形成不良 **/
	private String isNotFine;
	
	/** 形成不良说明 **/
	private String notFineRemark;
	
	/** 重要法律文本是否有效 **/
	private String isVld;
	
	/** 法律文本无效说明 **/
	private String vldRemark;
	
	/** 主从合同能否衔接，无法律瑕疵 **/
	private String isLnk;
	
	/** 合同不能衔接说明 **/
	private String lnkRemark;
	
	/** 本次检查后拟采取的措施等情况说明 **/
	private String caseRemark;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param blemishReason
	 */
	public void setBlemishReason(String blemishReason) {
		this.blemishReason = blemishReason == null ? null : blemishReason.trim();
	}
	
    /**
     * @return BlemishReason
     */	
	public String getBlemishReason() {
		return this.blemishReason;
	}
	
	/**
	 * @param isNotFine
	 */
	public void setIsNotFine(String isNotFine) {
		this.isNotFine = isNotFine == null ? null : isNotFine.trim();
	}
	
    /**
     * @return IsNotFine
     */	
	public String getIsNotFine() {
		return this.isNotFine;
	}
	
	/**
	 * @param notFineRemark
	 */
	public void setNotFineRemark(String notFineRemark) {
		this.notFineRemark = notFineRemark == null ? null : notFineRemark.trim();
	}
	
    /**
     * @return NotFineRemark
     */	
	public String getNotFineRemark() {
		return this.notFineRemark;
	}
	
	/**
	 * @param isVld
	 */
	public void setIsVld(String isVld) {
		this.isVld = isVld == null ? null : isVld.trim();
	}
	
    /**
     * @return IsVld
     */	
	public String getIsVld() {
		return this.isVld;
	}
	
	/**
	 * @param vldRemark
	 */
	public void setVldRemark(String vldRemark) {
		this.vldRemark = vldRemark == null ? null : vldRemark.trim();
	}
	
    /**
     * @return VldRemark
     */	
	public String getVldRemark() {
		return this.vldRemark;
	}
	
	/**
	 * @param isLnk
	 */
	public void setIsLnk(String isLnk) {
		this.isLnk = isLnk == null ? null : isLnk.trim();
	}
	
    /**
     * @return IsLnk
     */	
	public String getIsLnk() {
		return this.isLnk;
	}
	
	/**
	 * @param lnkRemark
	 */
	public void setLnkRemark(String lnkRemark) {
		this.lnkRemark = lnkRemark == null ? null : lnkRemark.trim();
	}
	
    /**
     * @return LnkRemark
     */	
	public String getLnkRemark() {
		return this.lnkRemark;
	}
	
	/**
	 * @param caseRemark
	 */
	public void setCaseRemark(String caseRemark) {
		this.caseRemark = caseRemark == null ? null : caseRemark.trim();
	}
	
    /**
     * @return CaseRemark
     */	
	public String getCaseRemark() {
		return this.caseRemark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}