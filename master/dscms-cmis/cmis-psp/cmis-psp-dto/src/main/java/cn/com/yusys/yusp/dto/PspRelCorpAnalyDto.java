package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspRelCorpAnaly
 * @类描述: psp_rel_corp_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspRelCorpAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 所属集团/关联企业情况 **/
	private String relSitu;
	
	/** 关联关系及内部资金往来情况 **/
	private String cashFlowSitu;
	
	/** 整个集团客户授信规模及风险评估 **/
	private String grpRiskEval;
	
	/** 目前采取的贷后管理策略及效果 **/
	private String stratyEffect;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param relSitu
	 */
	public void setRelSitu(String relSitu) {
		this.relSitu = relSitu == null ? null : relSitu.trim();
	}
	
    /**
     * @return RelSitu
     */	
	public String getRelSitu() {
		return this.relSitu;
	}
	
	/**
	 * @param cashFlowSitu
	 */
	public void setCashFlowSitu(String cashFlowSitu) {
		this.cashFlowSitu = cashFlowSitu == null ? null : cashFlowSitu.trim();
	}
	
    /**
     * @return CashFlowSitu
     */	
	public String getCashFlowSitu() {
		return this.cashFlowSitu;
	}
	
	/**
	 * @param grpRiskEval
	 */
	public void setGrpRiskEval(String grpRiskEval) {
		this.grpRiskEval = grpRiskEval == null ? null : grpRiskEval.trim();
	}
	
    /**
     * @return GrpRiskEval
     */	
	public String getGrpRiskEval() {
		return this.grpRiskEval;
	}
	
	/**
	 * @param stratyEffect
	 */
	public void setStratyEffect(String stratyEffect) {
		this.stratyEffect = stratyEffect == null ? null : stratyEffect.trim();
	}
	
    /**
     * @return StratyEffect
     */	
	public String getStratyEffect() {
		return this.stratyEffect;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}