package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskLoanAnaly
 * @类描述: risk_loan_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskLoanAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 授信发生后用途是否正常 **/
	private String isNormalAftLmt;
	
	/** 实际借款用途 **/
	private String factLoanUs;
	
	/** 是否重组贷款 **/
	private String isRecLoan;
	
	/** 是否展期 **/
	private String isExt;
	
	/** 是否借新还旧 **/
	private String isRefinance;
	
	/** 保全状态 **/
	private String preserveState;
	
	/** 保全情况说明 **/
	private String preserveCaseDesc;
	
	/** 有无影响该笔授信偿还的不利因素 **/
	private String isNegFactor;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param isNormalAftLmt
	 */
	public void setIsNormalAftLmt(String isNormalAftLmt) {
		this.isNormalAftLmt = isNormalAftLmt == null ? null : isNormalAftLmt.trim();
	}
	
    /**
     * @return IsNormalAftLmt
     */	
	public String getIsNormalAftLmt() {
		return this.isNormalAftLmt;
	}
	
	/**
	 * @param factLoanUs
	 */
	public void setFactLoanUs(String factLoanUs) {
		this.factLoanUs = factLoanUs == null ? null : factLoanUs.trim();
	}
	
    /**
     * @return FactLoanUs
     */	
	public String getFactLoanUs() {
		return this.factLoanUs;
	}
	
	/**
	 * @param isRecLoan
	 */
	public void setIsRecLoan(String isRecLoan) {
		this.isRecLoan = isRecLoan == null ? null : isRecLoan.trim();
	}
	
    /**
     * @return IsRecLoan
     */	
	public String getIsRecLoan() {
		return this.isRecLoan;
	}
	
	/**
	 * @param isExt
	 */
	public void setIsExt(String isExt) {
		this.isExt = isExt == null ? null : isExt.trim();
	}
	
    /**
     * @return IsExt
     */	
	public String getIsExt() {
		return this.isExt;
	}
	
	/**
	 * @param isRefinance
	 */
	public void setIsRefinance(String isRefinance) {
		this.isRefinance = isRefinance == null ? null : isRefinance.trim();
	}
	
    /**
     * @return IsRefinance
     */	
	public String getIsRefinance() {
		return this.isRefinance;
	}
	
	/**
	 * @param preserveState
	 */
	public void setPreserveState(String preserveState) {
		this.preserveState = preserveState == null ? null : preserveState.trim();
	}
	
    /**
     * @return PreserveState
     */	
	public String getPreserveState() {
		return this.preserveState;
	}
	
	/**
	 * @param preserveCaseDesc
	 */
	public void setPreserveCaseDesc(String preserveCaseDesc) {
		this.preserveCaseDesc = preserveCaseDesc == null ? null : preserveCaseDesc.trim();
	}
	
    /**
     * @return PreserveCaseDesc
     */	
	public String getPreserveCaseDesc() {
		return this.preserveCaseDesc;
	}
	
	/**
	 * @param isNegFactor
	 */
	public void setIsNegFactor(String isNegFactor) {
		this.isNegFactor = isNegFactor == null ? null : isNegFactor.trim();
	}
	
    /**
     * @return IsNegFactor
     */	
	public String getIsNegFactor() {
		return this.isNegFactor;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}