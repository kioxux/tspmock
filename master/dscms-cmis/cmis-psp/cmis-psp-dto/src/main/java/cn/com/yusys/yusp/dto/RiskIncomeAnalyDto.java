package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskIncomeAnaly
 * @类描述: risk_income_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskIncomeAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 家庭年总收入 **/
	private java.math.BigDecimal famTotalIncome;
	
	/** 家庭年总支出 **/
	private java.math.BigDecimal famTotalPay;
	
	/** 年收入与年支出对比 **/
	private String incomeBalance;
	
	/** 家庭总资产 **/
	private java.math.BigDecimal famTotalAsset;
	
	/** 家庭总负债 **/
	private java.math.BigDecimal famTotalDebt;
	
	/** 家庭总资产与总负债对比 **/
	private String famAssetBalance;
	
	/** 工作稳定性 **/
	private String isWorkStable;
	
	/** 第一还款来源是否充足 **/
	private String isIncomeSuffice;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param famTotalIncome
	 */
	public void setFamTotalIncome(java.math.BigDecimal famTotalIncome) {
		this.famTotalIncome = famTotalIncome;
	}
	
    /**
     * @return FamTotalIncome
     */	
	public java.math.BigDecimal getFamTotalIncome() {
		return this.famTotalIncome;
	}
	
	/**
	 * @param famTotalPay
	 */
	public void setFamTotalPay(java.math.BigDecimal famTotalPay) {
		this.famTotalPay = famTotalPay;
	}
	
    /**
     * @return FamTotalPay
     */	
	public java.math.BigDecimal getFamTotalPay() {
		return this.famTotalPay;
	}
	
	/**
	 * @param incomeBalance
	 */
	public void setIncomeBalance(String incomeBalance) {
		this.incomeBalance = incomeBalance == null ? null : incomeBalance.trim();
	}
	
    /**
     * @return IncomeBalance
     */	
	public String getIncomeBalance() {
		return this.incomeBalance;
	}
	
	/**
	 * @param famTotalAsset
	 */
	public void setFamTotalAsset(java.math.BigDecimal famTotalAsset) {
		this.famTotalAsset = famTotalAsset;
	}
	
    /**
     * @return FamTotalAsset
     */	
	public java.math.BigDecimal getFamTotalAsset() {
		return this.famTotalAsset;
	}
	
	/**
	 * @param famTotalDebt
	 */
	public void setFamTotalDebt(java.math.BigDecimal famTotalDebt) {
		this.famTotalDebt = famTotalDebt;
	}
	
    /**
     * @return FamTotalDebt
     */	
	public java.math.BigDecimal getFamTotalDebt() {
		return this.famTotalDebt;
	}
	
	/**
	 * @param famAssetBalance
	 */
	public void setFamAssetBalance(String famAssetBalance) {
		this.famAssetBalance = famAssetBalance == null ? null : famAssetBalance.trim();
	}
	
    /**
     * @return FamAssetBalance
     */	
	public String getFamAssetBalance() {
		return this.famAssetBalance;
	}
	
	/**
	 * @param isWorkStable
	 */
	public void setIsWorkStable(String isWorkStable) {
		this.isWorkStable = isWorkStable == null ? null : isWorkStable.trim();
	}
	
    /**
     * @return IsWorkStable
     */	
	public String getIsWorkStable() {
		return this.isWorkStable;
	}
	
	/**
	 * @param isIncomeSuffice
	 */
	public void setIsIncomeSuffice(String isIncomeSuffice) {
		this.isIncomeSuffice = isIncomeSuffice == null ? null : isIncomeSuffice.trim();
	}
	
    /**
     * @return IsIncomeSuffice
     */	
	public String getIsIncomeSuffice() {
		return this.isIncomeSuffice;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}