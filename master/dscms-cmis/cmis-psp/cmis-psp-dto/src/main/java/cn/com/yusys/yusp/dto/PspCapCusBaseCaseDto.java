package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * psp_cap_cus_base_case数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:23
 */
public class PspCapCusBaseCaseDto {

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 客户编号
     **/
    private String cusId;

    /**
     * 客户名称
     **/
    private String cusName;

    /**
     * 所属集团
     **/
    private String belongGrpId;

    /**
     * 准入时内部主体评级
     **/
    private String evalResultInner;

    /**
     * 准入时外部主体评级
     **/
    private String evalResultOuter;

    /**
     * 最新内部主体评级
     **/
    private String curtEvalResultInner;

    /**
     * 最新外部主体评级
     **/
    private String curtEvalResultOuter;

    /**
     * 授信批复编号
     **/
    private String lmtReplyNo;

    /**
     * 项目名称
     **/
    private String proName;

    /**
     * 授信金额
     **/
    private BigDecimal lmtAmt;

    /**
     * 已用额度
     **/
    private BigDecimal outstndAmt;

    /**
     * 业务起始日
     **/
    private String busiStartDate;

    /**
     * 业务到期日
     **/
    private String busiEndDate;

    /**
     * 业务类型
     **/
    private String busiType;

    /**
     * 业务管辖机构
     **/
    private String busiOrgId;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspCapCusBaseCaseDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBelongGrpId() {
        return belongGrpId;
    }

    public void setBelongGrpId(String belongGrpId) {
        this.belongGrpId = belongGrpId;
    }

    public String getEvalResultInner() {
        return evalResultInner;
    }

    public void setEvalResultInner(String evalResultInner) {
        this.evalResultInner = evalResultInner;
    }

    public String getEvalResultOuter() {
        return evalResultOuter;
    }

    public void setEvalResultOuter(String evalResultOuter) {
        this.evalResultOuter = evalResultOuter;
    }

    public String getCurtEvalResultInner() {
        return curtEvalResultInner;
    }

    public void setCurtEvalResultInner(String curtEvalResultInner) {
        this.curtEvalResultInner = curtEvalResultInner;
    }

    public String getCurtEvalResultOuter() {
        return curtEvalResultOuter;
    }

    public void setCurtEvalResultOuter(String curtEvalResultOuter) {
        this.curtEvalResultOuter = curtEvalResultOuter;
    }

    public String getLmtReplyNo() {
        return lmtReplyNo;
    }

    public void setLmtReplyNo(String lmtReplyNo) {
        this.lmtReplyNo = lmtReplyNo;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public BigDecimal getOutstndAmt() {
        return outstndAmt;
    }

    public void setOutstndAmt(BigDecimal outstndAmt) {
        this.outstndAmt = outstndAmt;
    }

    public String getBusiStartDate() {
        return busiStartDate;
    }

    public void setBusiStartDate(String busiStartDate) {
        this.busiStartDate = busiStartDate;
    }

    public String getBusiEndDate() {
        return busiEndDate;
    }

    public void setBusiEndDate(String busiEndDate) {
        this.busiEndDate = busiEndDate;
    }

    public String getBusiType() {
        return busiType;
    }

    public void setBusiType(String busiType) {
        this.busiType = busiType;
    }

    public String getBusiOrgId() {
        return busiOrgId;
    }

    public void setBusiOrgId(String busiOrgId) {
        this.busiOrgId = busiOrgId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}