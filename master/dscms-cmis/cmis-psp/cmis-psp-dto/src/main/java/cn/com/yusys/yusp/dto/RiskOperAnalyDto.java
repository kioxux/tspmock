package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskOperAnaly
 * @类描述: risk_oper_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskOperAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 经营情况 **/
	private String corpOperSitu;
	
	/** 经营情况说明 **/
	private String operSituRemark;
	
	/** 预测以后1年内经营趋势 **/
	private String n1yOperTrend;
	
	/** 生产经营情况 **/
	private String indivOperSitu;
	
	/** 销售收入变化趋势 **/
	private String incomeChange;
	
	/** 利润变动趋势 **/
	private String profitChange;
	
	/** 现金流量变动趋势 **/
	private String cashChange;
	
	/** 第一还款来源是否充足 **/
	private String isIncomeSuffice;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param corpOperSitu
	 */
	public void setCorpOperSitu(String corpOperSitu) {
		this.corpOperSitu = corpOperSitu == null ? null : corpOperSitu.trim();
	}
	
    /**
     * @return CorpOperSitu
     */	
	public String getCorpOperSitu() {
		return this.corpOperSitu;
	}
	
	/**
	 * @param operSituRemark
	 */
	public void setOperSituRemark(String operSituRemark) {
		this.operSituRemark = operSituRemark == null ? null : operSituRemark.trim();
	}
	
    /**
     * @return OperSituRemark
     */	
	public String getOperSituRemark() {
		return this.operSituRemark;
	}
	
	/**
	 * @param n1yOperTrend
	 */
	public void setN1yOperTrend(String n1yOperTrend) {
		this.n1yOperTrend = n1yOperTrend == null ? null : n1yOperTrend.trim();
	}
	
    /**
     * @return N1yOperTrend
     */	
	public String getN1yOperTrend() {
		return this.n1yOperTrend;
	}
	
	/**
	 * @param indivOperSitu
	 */
	public void setIndivOperSitu(String indivOperSitu) {
		this.indivOperSitu = indivOperSitu == null ? null : indivOperSitu.trim();
	}
	
    /**
     * @return IndivOperSitu
     */	
	public String getIndivOperSitu() {
		return this.indivOperSitu;
	}
	
	/**
	 * @param incomeChange
	 */
	public void setIncomeChange(String incomeChange) {
		this.incomeChange = incomeChange == null ? null : incomeChange.trim();
	}
	
    /**
     * @return IncomeChange
     */	
	public String getIncomeChange() {
		return this.incomeChange;
	}
	
	/**
	 * @param profitChange
	 */
	public void setProfitChange(String profitChange) {
		this.profitChange = profitChange == null ? null : profitChange.trim();
	}
	
    /**
     * @return ProfitChange
     */	
	public String getProfitChange() {
		return this.profitChange;
	}
	
	/**
	 * @param cashChange
	 */
	public void setCashChange(String cashChange) {
		this.cashChange = cashChange == null ? null : cashChange.trim();
	}
	
    /**
     * @return CashChange
     */	
	public String getCashChange() {
		return this.cashChange;
	}
	
	/**
	 * @param isIncomeSuffice
	 */
	public void setIsIncomeSuffice(String isIncomeSuffice) {
		this.isIncomeSuffice = isIncomeSuffice == null ? null : isIncomeSuffice.trim();
	}
	
    /**
     * @return IsIncomeSuffice
     */	
	public String getIsIncomeSuffice() {
		return this.isIncomeSuffice;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}