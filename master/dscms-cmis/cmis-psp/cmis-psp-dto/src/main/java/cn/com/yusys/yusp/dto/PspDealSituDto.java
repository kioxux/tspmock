package cn.com.yusys.yusp.dto;

import java.util.Date;

/**
 * psp_deal_situ数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:24
 */
public class PspDealSituDto {

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 出险成因
     **/
    private String riskReason;

    /**
     * 是否寄送催收通知书
     **/
    private String isCollect;

    /**
     * 是否有回执
     **/
    private String isReceipt;

    /**
     * 当前处置措施
     **/
    private String presentActionDesc;

    /**
     * 下一步处置措施或建议
     **/
    private String nextStepAction;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspDealSituDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getRiskReason() {
        return riskReason;
    }

    public void setRiskReason(String riskReason) {
        this.riskReason = riskReason;
    }

    public String getIsCollect() {
        return isCollect;
    }

    public void setIsCollect(String isCollect) {
        this.isCollect = isCollect;
    }

    public String getIsReceipt() {
        return isReceipt;
    }

    public void setIsReceipt(String isReceipt) {
        this.isReceipt = isReceipt;
    }

    public String getPresentActionDesc() {
        return presentActionDesc;
    }

    public void setPresentActionDesc(String presentActionDesc) {
        this.presentActionDesc = presentActionDesc;
    }

    public String getNextStepAction() {
        return nextStepAction;
    }

    public void setNextStepAction(String nextStepAction) {
        this.nextStepAction = nextStepAction;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}