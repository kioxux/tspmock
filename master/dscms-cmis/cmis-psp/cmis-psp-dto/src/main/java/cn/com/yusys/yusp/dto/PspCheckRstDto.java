package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * psp_check_rst数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-26 06:58:58
 */
public class PspCheckRstDto {

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 是否受托支付
     **/
    private String isUntruPayType;

    /**
     * 购销合同金额
     **/
    private BigDecimal contAmt;

    /**
     * 交易对手名称
     **/
    private String contTranName;

    /**
     * 贷款用途
     **/
    private String contAgreedUse;

    /**
     * 贷款资金是否存在回流
     **/
    private String isCapBack;

    /**
     * 与授信用途是否一致
     **/
    private String isLmtUse;

    /**
     * 贷款资金流向说明
     **/
    private String capFlowRemark;

    /**
     * 检查结果及检查人意见
     **/
    private String checkComment;

    /**
     * 协查人意见
     **/
    private String assistComment;

    /**
     * 协查人
     **/
    private String assistId;

    /**
     * 协查机构
     **/
    private String assistOrgId;

    /**
     * 协查日期
     **/
    private String assistDate;

    /**
     * 约见人
     **/
    private String relatedPersonnel;

    /**
     * 约见人职务
     **/
    private String relatedJobTtl;

    /**
     * 检查方式
     **/
    private String checkMode;

    /**
     * 是否落实抵质押或担保
     **/
    private String isGuarImple;

    /**
     * 未落实说明
     **/
    private String impleRemark;

    /**
     * 检查地点
     **/
    private String checkPlace;

    /**
     * 管户客户经理授信建议
     **/
    private String checkAdviceType;

    /**
     * 管户客户经理授信建议理由
     **/
    private String checkAdviceReason;

    /**
     * 协办客户经理授信建议
     **/
    private String assistAdviceType;

    /**
     * 协办客户经理授信建议理由
     **/
    private String assistAdviceReason;

    /**
     * 客户逾期行为恶意还是非恶意
     **/
    private String isBadOverdue;

    /**
     * 有无偿还能力
     **/
    private String repayAbi;

    /**
     * 催收方式
     **/
    private String collectWay;

    /**
     * 有无向客户发送催收通知书
     **/
    private String isCollect;

    /**
     * 有无催收回执
     **/
    private String isReceipt;

    /**
     * 是否准备起诉
     **/
    private String isSue;

    /**
     * 投后检查结论
     **/
    private String capCheckResult;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspCheckRstDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getIsUntruPayType() {
        return isUntruPayType;
    }

    public void setIsUntruPayType(String isUntruPayType) {
        this.isUntruPayType = isUntruPayType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public String getContTranName() {
        return contTranName;
    }

    public void setContTranName(String contTranName) {
        this.contTranName = contTranName;
    }

    public String getContAgreedUse() {
        return contAgreedUse;
    }

    public void setContAgreedUse(String contAgreedUse) {
        this.contAgreedUse = contAgreedUse;
    }

    public String getIsCapBack() {
        return isCapBack;
    }

    public void setIsCapBack(String isCapBack) {
        this.isCapBack = isCapBack;
    }

    public String getIsLmtUse() {
        return isLmtUse;
    }

    public void setIsLmtUse(String isLmtUse) {
        this.isLmtUse = isLmtUse;
    }

    public String getCapFlowRemark() {
        return capFlowRemark;
    }

    public void setCapFlowRemark(String capFlowRemark) {
        this.capFlowRemark = capFlowRemark;
    }

    public String getCheckComment() {
        return checkComment;
    }

    public void setCheckComment(String checkComment) {
        this.checkComment = checkComment;
    }

    public String getAssistComment() {
        return assistComment;
    }

    public void setAssistComment(String assistComment) {
        this.assistComment = assistComment;
    }

    public String getAssistId() {
        return assistId;
    }

    public void setAssistId(String assistId) {
        this.assistId = assistId;
    }

    public String getAssistOrgId() {
        return assistOrgId;
    }

    public void setAssistOrgId(String assistOrgId) {
        this.assistOrgId = assistOrgId;
    }

    public String getAssistDate() {
        return assistDate;
    }

    public void setAssistDate(String assistDate) {
        this.assistDate = assistDate;
    }

    public String getRelatedPersonnel() {
        return relatedPersonnel;
    }

    public void setRelatedPersonnel(String relatedPersonnel) {
        this.relatedPersonnel = relatedPersonnel;
    }

    public String getRelatedJobTtl() {
        return relatedJobTtl;
    }

    public void setRelatedJobTtl(String relatedJobTtl) {
        this.relatedJobTtl = relatedJobTtl;
    }

    public String getCheckMode() {
        return checkMode;
    }

    public void setCheckMode(String checkMode) {
        this.checkMode = checkMode;
    }

    public String getIsGuarImple() {
        return isGuarImple;
    }

    public void setIsGuarImple(String isGuarImple) {
        this.isGuarImple = isGuarImple;
    }

    public String getImpleRemark() {
        return impleRemark;
    }

    public void setImpleRemark(String impleRemark) {
        this.impleRemark = impleRemark;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getCheckAdviceType() {
        return checkAdviceType;
    }

    public void setCheckAdviceType(String checkAdviceType) {
        this.checkAdviceType = checkAdviceType;
    }

    public String getCheckAdviceReason() {
        return checkAdviceReason;
    }

    public void setCheckAdviceReason(String checkAdviceReason) {
        this.checkAdviceReason = checkAdviceReason;
    }

    public String getAssistAdviceType() {
        return assistAdviceType;
    }

    public void setAssistAdviceType(String assistAdviceType) {
        this.assistAdviceType = assistAdviceType;
    }

    public String getAssistAdviceReason() {
        return assistAdviceReason;
    }

    public void setAssistAdviceReason(String assistAdviceReason) {
        this.assistAdviceReason = assistAdviceReason;
    }

    public String getIsBadOverdue() {
        return isBadOverdue;
    }

    public void setIsBadOverdue(String isBadOverdue) {
        this.isBadOverdue = isBadOverdue;
    }

    public String getRepayAbi() {
        return repayAbi;
    }

    public void setRepayAbi(String repayAbi) {
        this.repayAbi = repayAbi;
    }

    public String getCollectWay() {
        return collectWay;
    }

    public void setCollectWay(String collectWay) {
        this.collectWay = collectWay;
    }

    public String getIsCollect() {
        return isCollect;
    }

    public void setIsCollect(String isCollect) {
        this.isCollect = isCollect;
    }

    public String getIsReceipt() {
        return isReceipt;
    }

    public void setIsReceipt(String isReceipt) {
        this.isReceipt = isReceipt;
    }

    public String getIsSue() {
        return isSue;
    }

    public void setIsSue(String isSue) {
        this.isSue = isSue;
    }

    public String getCapCheckResult() {
        return capCheckResult;
    }

    public void setCapCheckResult(String capCheckResult) {
        this.capCheckResult = capCheckResult;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}