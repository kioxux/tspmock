package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskTaskList
 * @类描述: risk_task_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-25 03:38:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskTaskListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 任务类型 **/
	private String taskType;
	
	/** 分类模型 **/
	private String checkType;
	
	/** 客户类型 **/
	private String cusCatalog;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 手工分类 **/
	private String manualClass;
	
	/** 手工十级分类结果 **/
	private String manualTenClass;
	
	/** 人工分类理由 **/
	private String manualClassReason;
	
	/** 机评分类 **/
	private String autoClass;
	
	/** 机评十级分类结果 **/
	private String autoTenClass;
	
	/** 机评分类理由 **/
	private String autoClassReason;
	
	/** 实际分类日期 **/
	private String checkDate;
	
	/** 上次分类结果 **/
	private String lastClassRst;
	
	/** 上次十级分类结果 **/
	private String lastTenClassRst;
	
	/** 上次分类任务编号 **/
	private String lastTaskNo;
	
	/** 上次分类日期 **/
	private String lastCheckDate;
	
	/** 风险分类得分 **/
	private Integer riskScore;
	
	/** 任务生成日期 **/
	private String taskStartDt;
	
	/** 要求完成日期 **/
	private String taskEndDt;
	
	/** 任务执行人 **/
	private String execId;
	
	/** 任务执行机构 **/
	private String execBrId;
	
	/** 任务状态 **/
	private String checkStatus;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 生成原因 **/
	private String checkReason;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType == null ? null : taskType.trim();
	}
	
    /**
     * @return TaskType
     */	
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param checkType
	 */
	public void setCheckType(String checkType) {
		this.checkType = checkType == null ? null : checkType.trim();
	}
	
    /**
     * @return CheckType
     */	
	public String getCheckType() {
		return this.checkType;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog == null ? null : cusCatalog.trim();
	}
	
    /**
     * @return CusCatalog
     */	
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param manualClass
	 */
	public void setManualClass(String manualClass) {
		this.manualClass = manualClass == null ? null : manualClass.trim();
	}
	
    /**
     * @return ManualClass
     */	
	public String getManualClass() {
		return this.manualClass;
	}
	
	/**
	 * @param manualTenClass
	 */
	public void setManualTenClass(String manualTenClass) {
		this.manualTenClass = manualTenClass == null ? null : manualTenClass.trim();
	}
	
    /**
     * @return ManualTenClass
     */	
	public String getManualTenClass() {
		return this.manualTenClass;
	}
	
	/**
	 * @param manualClassReason
	 */
	public void setManualClassReason(String manualClassReason) {
		this.manualClassReason = manualClassReason == null ? null : manualClassReason.trim();
	}
	
    /**
     * @return ManualClassReason
     */	
	public String getManualClassReason() {
		return this.manualClassReason;
	}
	
	/**
	 * @param autoClass
	 */
	public void setAutoClass(String autoClass) {
		this.autoClass = autoClass == null ? null : autoClass.trim();
	}
	
    /**
     * @return AutoClass
     */	
	public String getAutoClass() {
		return this.autoClass;
	}
	
	/**
	 * @param autoTenClass
	 */
	public void setAutoTenClass(String autoTenClass) {
		this.autoTenClass = autoTenClass == null ? null : autoTenClass.trim();
	}
	
    /**
     * @return AutoTenClass
     */	
	public String getAutoTenClass() {
		return this.autoTenClass;
	}
	
	/**
	 * @param autoClassReason
	 */
	public void setAutoClassReason(String autoClassReason) {
		this.autoClassReason = autoClassReason == null ? null : autoClassReason.trim();
	}
	
    /**
     * @return AutoClassReason
     */	
	public String getAutoClassReason() {
		return this.autoClassReason;
	}
	
	/**
	 * @param checkDate
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate == null ? null : checkDate.trim();
	}
	
    /**
     * @return CheckDate
     */	
	public String getCheckDate() {
		return this.checkDate;
	}
	
	/**
	 * @param lastClassRst
	 */
	public void setLastClassRst(String lastClassRst) {
		this.lastClassRst = lastClassRst == null ? null : lastClassRst.trim();
	}
	
    /**
     * @return LastClassRst
     */	
	public String getLastClassRst() {
		return this.lastClassRst;
	}
	
	/**
	 * @param lastTenClassRst
	 */
	public void setLastTenClassRst(String lastTenClassRst) {
		this.lastTenClassRst = lastTenClassRst == null ? null : lastTenClassRst.trim();
	}
	
    /**
     * @return LastTenClassRst
     */	
	public String getLastTenClassRst() {
		return this.lastTenClassRst;
	}
	
	/**
	 * @param lastTaskNo
	 */
	public void setLastTaskNo(String lastTaskNo) {
		this.lastTaskNo = lastTaskNo == null ? null : lastTaskNo.trim();
	}
	
    /**
     * @return LastTaskNo
     */	
	public String getLastTaskNo() {
		return this.lastTaskNo;
	}
	
	/**
	 * @param lastCheckDate
	 */
	public void setLastCheckDate(String lastCheckDate) {
		this.lastCheckDate = lastCheckDate == null ? null : lastCheckDate.trim();
	}
	
    /**
     * @return LastCheckDate
     */	
	public String getLastCheckDate() {
		return this.lastCheckDate;
	}
	
	/**
	 * @param riskScore
	 */
	public void setRiskScore(Integer riskScore) {
		this.riskScore = riskScore;
	}
	
    /**
     * @return RiskScore
     */	
	public Integer getRiskScore() {
		return this.riskScore;
	}
	
	/**
	 * @param taskStartDt
	 */
	public void setTaskStartDt(String taskStartDt) {
		this.taskStartDt = taskStartDt == null ? null : taskStartDt.trim();
	}
	
    /**
     * @return TaskStartDt
     */	
	public String getTaskStartDt() {
		return this.taskStartDt;
	}
	
	/**
	 * @param taskEndDt
	 */
	public void setTaskEndDt(String taskEndDt) {
		this.taskEndDt = taskEndDt == null ? null : taskEndDt.trim();
	}
	
    /**
     * @return TaskEndDt
     */	
	public String getTaskEndDt() {
		return this.taskEndDt;
	}
	
	/**
	 * @param execId
	 */
	public void setExecId(String execId) {
		this.execId = execId == null ? null : execId.trim();
	}
	
    /**
     * @return ExecId
     */	
	public String getExecId() {
		return this.execId;
	}
	
	/**
	 * @param execBrId
	 */
	public void setExecBrId(String execBrId) {
		this.execBrId = execBrId == null ? null : execBrId.trim();
	}
	
    /**
     * @return ExecBrId
     */	
	public String getExecBrId() {
		return this.execBrId;
	}
	
	/**
	 * @param checkStatus
	 */
	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus == null ? null : checkStatus.trim();
	}
	
    /**
     * @return CheckStatus
     */	
	public String getCheckStatus() {
		return this.checkStatus;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param checkReason
	 */
	public void setCheckReason(String checkReason) {
		this.checkReason = checkReason == null ? null : checkReason.trim();
	}
	
    /**
     * @return CheckReason
     */	
	public String getCheckReason() {
		return this.checkReason;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}