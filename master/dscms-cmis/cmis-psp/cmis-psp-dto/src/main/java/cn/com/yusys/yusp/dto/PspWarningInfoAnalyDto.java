package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWarningInfoAnaly
 * @类描述: psp_warning_info_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspWarningInfoAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 预警分析 **/
	private String altAnaly;
	
	/** 是否有其他负面信息 **/
	private String isNegativeInfo;
	
	/** 分析说明 **/
	private String analy;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param altAnaly
	 */
	public void setAltAnaly(String altAnaly) {
		this.altAnaly = altAnaly == null ? null : altAnaly.trim();
	}
	
    /**
     * @return AltAnaly
     */	
	public String getAltAnaly() {
		return this.altAnaly;
	}
	
	/**
	 * @param isNegativeInfo
	 */
	public void setIsNegativeInfo(String isNegativeInfo) {
		this.isNegativeInfo = isNegativeInfo == null ? null : isNegativeInfo.trim();
	}
	
    /**
     * @return IsNegativeInfo
     */	
	public String getIsNegativeInfo() {
		return this.isNegativeInfo;
	}
	
	/**
	 * @param analy
	 */
	public void setAnaly(String analy) {
		this.analy = analy == null ? null : analy.trim();
	}
	
    /**
     * @return Analy
     */	
	public String getAnaly() {
		return this.analy;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}