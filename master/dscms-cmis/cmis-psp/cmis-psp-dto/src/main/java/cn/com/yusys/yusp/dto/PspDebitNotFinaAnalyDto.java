package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * psp_debit_not_fina_analy数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:24
 */
public class PspDebitNotFinaAnalyDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 借款人及其家庭是否发生意外
     **/
    private String isAccident;

    /**
     * 意外说明
     **/
    private String accidentRemark;

    /**
     * 借款人是否面临重大诉讼
     **/
    private String isInvolveLawsuit;

    /**
     * 重大诉讼说明
     **/
    private String involveLawsuitRemark;

    /**
     * 借款人对外是否提供过多担保或大量资产被抵押
     **/
    private String isOutguar;

    /**
     * 过量担保、抵押说明
     **/
    private String outguarRemark;

    /**
     * 相关法律文本是否合法有效
     **/
    private String isVld;

    /**
     * 法律文本无效说明
     **/
    private String vldRemark;

    /**
     * 对借款人的总体评价
     **/
    private String totlEvlu;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspDebitNotFinaAnalyDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getIsAccident() {
        return isAccident;
    }

    public void setIsAccident(String isAccident) {
        this.isAccident = isAccident;
    }

    public String getAccidentRemark() {
        return accidentRemark;
    }

    public void setAccidentRemark(String accidentRemark) {
        this.accidentRemark = accidentRemark;
    }

    public String getIsInvolveLawsuit() {
        return isInvolveLawsuit;
    }

    public void setIsInvolveLawsuit(String isInvolveLawsuit) {
        this.isInvolveLawsuit = isInvolveLawsuit;
    }

    public String getInvolveLawsuitRemark() {
        return involveLawsuitRemark;
    }

    public void setInvolveLawsuitRemark(String involveLawsuitRemark) {
        this.involveLawsuitRemark = involveLawsuitRemark;
    }

    public String getIsOutguar() {
        return isOutguar;
    }

    public void setIsOutguar(String isOutguar) {
        this.isOutguar = isOutguar;
    }

    public String getOutguarRemark() {
        return outguarRemark;
    }

    public void setOutguarRemark(String outguarRemark) {
        this.outguarRemark = outguarRemark;
    }

    public String getIsVld() {
        return isVld;
    }

    public void setIsVld(String isVld) {
        this.isVld = isVld;
    }

    public String getVldRemark() {
        return vldRemark;
    }

    public void setVldRemark(String vldRemark) {
        this.vldRemark = vldRemark;
    }

    public String getTotlEvlu() {
        return totlEvlu;
    }

    public void setTotlEvlu(String totlEvlu) {
        this.totlEvlu = totlEvlu;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}