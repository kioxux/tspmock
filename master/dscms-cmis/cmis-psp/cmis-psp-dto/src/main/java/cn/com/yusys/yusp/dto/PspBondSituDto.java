package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * psp_bond_situ数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:23
 */
public class PspBondSituDto {

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 投资资产名称
     **/
    private String investAssetName;

    /**
     * 产品名称
     **/
    private String prdName;

    /**
     * 投资金额
     **/
    private BigDecimal investAmt;

    /**
     * 投资币种
     **/
    private String investCurType;

    /**
     * 交易日期
     **/
    private String tranDate;

    /**
     * 资产到期日
     **/
    private String assetEndDate;

    /**
     * 投资部门/分支机构
     **/
    private String investBrId;

    /**
     * 投资经理/主办客户经理
     **/
    private String investId;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspBondSituDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getInvestAssetName() {
        return investAssetName;
    }

    public void setInvestAssetName(String investAssetName) {
        this.investAssetName = investAssetName;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public BigDecimal getInvestAmt() {
        return investAmt;
    }

    public void setInvestAmt(BigDecimal investAmt) {
        this.investAmt = investAmt;
    }

    public String getInvestCurType() {
        return investCurType;
    }

    public void setInvestCurType(String investCurType) {
        this.investCurType = investCurType;
    }

    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    public String getAssetEndDate() {
        return assetEndDate;
    }

    public void setAssetEndDate(String assetEndDate) {
        this.assetEndDate = assetEndDate;
    }

    public String getInvestBrId() {
        return investBrId;
    }

    public void setInvestBrId(String investBrId) {
        this.investBrId = investBrId;
    }

    public String getInvestId() {
        return investId;
    }

    public void setInvestId(String investId) {
        this.investId = investId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}