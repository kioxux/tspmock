package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPromiseAnaly
 * @类描述: psp_promise_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-25 03:37:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspPromiseAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 生产状况 **/
	private String produceStatus;
	
	/** 企业经营状况 **/
	private String operStatus;
	
	/** 担保人经营情况 **/
	private String guarOperStatus;
	
	/** 担保意愿 **/
	private String guarWishCase;
	
	/** 担保实力 **/
	private String guarAbi;
	
	/** 抵押物情况 **/
	private String guarSitu;
	
	/** 借款人或担保人名下有效资产分析 **/
	private String validAssetAnaly;
	
	/** 工作收入来源分析 **/
	private String incomeAnaly;
	
	/** 是否有其它收入来源 **/
	private String isOtherIncome;
	
	/** 有关利害关系方情况 **/
	private String interestedSitu;
	
	/** 抵押物是否被查封 **/
	private String isClose;
	
	/** 借款人或家庭名下有效资产 **/
	private String famValidAssetAnaly;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param produceStatus
	 */
	public void setProduceStatus(String produceStatus) {
		this.produceStatus = produceStatus == null ? null : produceStatus.trim();
	}
	
    /**
     * @return ProduceStatus
     */	
	public String getProduceStatus() {
		return this.produceStatus;
	}
	
	/**
	 * @param operStatus
	 */
	public void setOperStatus(String operStatus) {
		this.operStatus = operStatus == null ? null : operStatus.trim();
	}
	
    /**
     * @return OperStatus
     */	
	public String getOperStatus() {
		return this.operStatus;
	}
	
	/**
	 * @param guarOperStatus
	 */
	public void setGuarOperStatus(String guarOperStatus) {
		this.guarOperStatus = guarOperStatus == null ? null : guarOperStatus.trim();
	}
	
    /**
     * @return GuarOperStatus
     */	
	public String getGuarOperStatus() {
		return this.guarOperStatus;
	}
	
	/**
	 * @param guarWishCase
	 */
	public void setGuarWishCase(String guarWishCase) {
		this.guarWishCase = guarWishCase == null ? null : guarWishCase.trim();
	}
	
    /**
     * @return GuarWishCase
     */	
	public String getGuarWishCase() {
		return this.guarWishCase;
	}
	
	/**
	 * @param guarAbi
	 */
	public void setGuarAbi(String guarAbi) {
		this.guarAbi = guarAbi == null ? null : guarAbi.trim();
	}
	
    /**
     * @return GuarAbi
     */	
	public String getGuarAbi() {
		return this.guarAbi;
	}
	
	/**
	 * @param guarSitu
	 */
	public void setGuarSitu(String guarSitu) {
		this.guarSitu = guarSitu == null ? null : guarSitu.trim();
	}
	
    /**
     * @return GuarSitu
     */	
	public String getGuarSitu() {
		return this.guarSitu;
	}
	
	/**
	 * @param validAssetAnaly
	 */
	public void setValidAssetAnaly(String validAssetAnaly) {
		this.validAssetAnaly = validAssetAnaly == null ? null : validAssetAnaly.trim();
	}
	
    /**
     * @return ValidAssetAnaly
     */	
	public String getValidAssetAnaly() {
		return this.validAssetAnaly;
	}
	
	/**
	 * @param incomeAnaly
	 */
	public void setIncomeAnaly(String incomeAnaly) {
		this.incomeAnaly = incomeAnaly == null ? null : incomeAnaly.trim();
	}
	
    /**
     * @return IncomeAnaly
     */	
	public String getIncomeAnaly() {
		return this.incomeAnaly;
	}
	
	/**
	 * @param isOtherIncome
	 */
	public void setIsOtherIncome(String isOtherIncome) {
		this.isOtherIncome = isOtherIncome == null ? null : isOtherIncome.trim();
	}
	
    /**
     * @return IsOtherIncome
     */	
	public String getIsOtherIncome() {
		return this.isOtherIncome;
	}
	
	/**
	 * @param interestedSitu
	 */
	public void setInterestedSitu(String interestedSitu) {
		this.interestedSitu = interestedSitu == null ? null : interestedSitu.trim();
	}
	
    /**
     * @return InterestedSitu
     */	
	public String getInterestedSitu() {
		return this.interestedSitu;
	}
	
	/**
	 * @param isClose
	 */
	public void setIsClose(String isClose) {
		this.isClose = isClose == null ? null : isClose.trim();
	}
	
    /**
     * @return IsClose
     */	
	public String getIsClose() {
		return this.isClose;
	}
	
	/**
	 * @param famValidAssetAnaly
	 */
	public void setFamValidAssetAnaly(String famValidAssetAnaly) {
		this.famValidAssetAnaly = famValidAssetAnaly == null ? null : famValidAssetAnaly.trim();
	}
	
    /**
     * @return FamValidAssetAnaly
     */	
	public String getFamValidAssetAnaly() {
		return this.famValidAssetAnaly;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}