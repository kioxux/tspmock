package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * psp_cus_fin_analy数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:23
 */
public class PspCusFinAnalyDto {

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 融资总额（万元）
     **/
    private BigDecimal totalFinAmt;

    /**
     * 支行催收情况
     **/
    private String collectSitu;

    /**
     * 借款人还款意愿
     **/
    private String repayWish;

    /**
     * 整体融资分析
     **/
    private String entireFinAnaly;

    /**
     * 上期贷后检查时融资总额（万元）
     **/
    private BigDecimal preFinAmt;

    /**
     * 本期贷后检查时融资总额（万元）
     **/
    private BigDecimal curtFinAmt;

    /**
     * 融资变化（万元）
     **/
    private BigDecimal finChange;

    /**
     * 融资变化情况分析
     **/
    private String finChangeAnaly;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspCusFinAnalyDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public BigDecimal getTotalFinAmt() {
        return totalFinAmt;
    }

    public void setTotalFinAmt(BigDecimal totalFinAmt) {
        this.totalFinAmt = totalFinAmt;
    }

    public String getCollectSitu() {
        return collectSitu;
    }

    public void setCollectSitu(String collectSitu) {
        this.collectSitu = collectSitu;
    }

    public String getRepayWish() {
        return repayWish;
    }

    public void setRepayWish(String repayWish) {
        this.repayWish = repayWish;
    }

    public String getEntireFinAnaly() {
        return entireFinAnaly;
    }

    public void setEntireFinAnaly(String entireFinAnaly) {
        this.entireFinAnaly = entireFinAnaly;
    }

    public BigDecimal getPreFinAmt() {
        return preFinAmt;
    }

    public void setPreFinAmt(BigDecimal preFinAmt) {
        this.preFinAmt = preFinAmt;
    }

    public BigDecimal getCurtFinAmt() {
        return curtFinAmt;
    }

    public void setCurtFinAmt(BigDecimal curtFinAmt) {
        this.curtFinAmt = curtFinAmt;
    }

    public BigDecimal getFinChange() {
        return finChange;
    }

    public void setFinChange(BigDecimal finChange) {
        this.finChange = finChange;
    }

    public String getFinChangeAnaly() {
        return finChangeAnaly;
    }

    public void setFinChangeAnaly(String finChangeAnaly) {
        this.finChangeAnaly = finChangeAnaly;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}