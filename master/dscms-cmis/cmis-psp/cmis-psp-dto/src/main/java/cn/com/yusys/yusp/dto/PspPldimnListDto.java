package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPldimnList
 * @类描述: psp_pldimn_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspPldimnListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 抵/质押物名称 **/
	private String pldimnMemo;

	/** 押品统一编号 **/
	private String guarNo;

	/** 押品所有人编号 **/
	private String guarCusId;

	/** 押品所有人名称 **/
	private String guarCusName;

	/** 押品所有人证件类型 **/
	private String guarCertType;

	/** 押品所有人证件号码 **/
	private String guarCertCode;


	/** 抵/质押物位置 **/
	private String guarAddr;
	
	/** 权利金额 **/
	private java.math.BigDecimal confirmAmt;
	
	/** 评估金额 **/
	private java.math.BigDecimal evalAmt;
	
	/** 担保标志 **/
	private String guarType;
	
	/** 抵/质押物余值 **/
	private java.math.BigDecimal pldimnRemainValue;
	
	/** 是否被有关机关查封、冻结、抵押 **/
	private String isClose;
	
	/** 抵/质押物说明 **/
	private String guarRemark;
	
	/** 押品状态说明 **/
	private String guarStsRemark;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo == null ? null : pldimnMemo.trim();
	}
	
    /**
     * @return PldimnMemo
     */	
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarAddr
	 */
	public void setGuarAddr(String guarAddr) {
		this.guarAddr = guarAddr == null ? null : guarAddr.trim();
	}
	
    /**
     * @return GuarAddr
     */	
	public String getGuarAddr() {
		return this.guarAddr;
	}
	
	/**
	 * @param confirmAmt
	 */
	public void setConfirmAmt(java.math.BigDecimal confirmAmt) {
		this.confirmAmt = confirmAmt;
	}
	
    /**
     * @return ConfirmAmt
     */	
	public java.math.BigDecimal getConfirmAmt() {
		return this.confirmAmt;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return EvalAmt
     */	
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType == null ? null : guarType.trim();
	}
	
    /**
     * @return GuarType
     */	
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param pldimnRemainValue
	 */
	public void setPldimnRemainValue(java.math.BigDecimal pldimnRemainValue) {
		this.pldimnRemainValue = pldimnRemainValue;
	}
	
    /**
     * @return PldimnRemainValue
     */	
	public java.math.BigDecimal getPldimnRemainValue() {
		return this.pldimnRemainValue;
	}
	
	/**
	 * @param isClose
	 */
	public void setIsClose(String isClose) {
		this.isClose = isClose == null ? null : isClose.trim();
	}
	
    /**
     * @return IsClose
     */	
	public String getIsClose() {
		return this.isClose;
	}
	
	/**
	 * @param guarRemark
	 */
	public void setGuarRemark(String guarRemark) {
		this.guarRemark = guarRemark == null ? null : guarRemark.trim();
	}
	
    /**
     * @return GuarRemark
     */	
	public String getGuarRemark() {
		return this.guarRemark;
	}
	
	/**
	 * @param guarStsRemark
	 */
	public void setGuarStsRemark(String guarStsRemark) {
		this.guarStsRemark = guarStsRemark == null ? null : guarStsRemark.trim();
	}
	
    /**
     * @return GuarStsRemark
     */	
	public String getGuarStsRemark() {
		return this.guarStsRemark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getGuarNo() {
		return guarNo;
	}

	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}

	public String getGuarCusId() {
		return guarCusId;
	}

	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}

	public String getGuarCusName() {
		return guarCusName;
	}

	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}

	public String getGuarCertType() {
		return guarCertType;
	}

	public void setGuarCertType(String guarCertType) {
		this.guarCertType = guarCertType;
	}

	public String getGuarCertCode() {
		return guarCertCode;
	}

	public void setGuarCertCode(String guarCertCode) {
		this.guarCertCode = guarCertCode;
	}
}