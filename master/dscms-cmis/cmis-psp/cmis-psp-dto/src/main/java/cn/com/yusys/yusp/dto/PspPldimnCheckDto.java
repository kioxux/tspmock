package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPldimnCheck
 * @类描述: psp_pldimn_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspPldimnCheckDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 是否对抵（质）押物进行核实并拍照确认 **/
	private String isPhotograph;
	
	/** 未核实说明 **/
	private String noPhotoExpl;
	
	/** 抵（质）押人的担保意愿情况 **/
	private String guarWishCase;
	
	/** 抵（质）押人的担保意愿说明 **/
	private String wishCaseExpl;
	
	/** 抵/质押物是否被有关机关查封、冻结、再次抵押 **/
	private String isClose;
	
	/** 抵质押物查封说明 **/
	private String closeExpl;
	
	/** 是否书面承诺放弃优先购买权及优先租赁权 **/
	private String isPriRent;
	
	/** 放弃优先权说明 **/
	private String rentExpl;
	
	/** 是否有其他重要风险事项 **/
	private String isOtherRiskEvent;
	
	/** 其他风险说明 **/
	private String riskEventExpl;
	
	/** 抵/质押物是否损毁或大幅贬值 **/
	private String isRuin;
	
	/** 抵/质押物贬值说明 **/
	private String ruinExpl;
	
	/** 抵/质押物变现能力与审批贷款时有无较大差异 **/
	private String isChangeGuar;
	
	/** 变现能力差异说明 **/
	private String changeGuarRemark;
	
	/** 该抵/质押物是否能迅速变现并足额抵偿相关债务 **/
	private String isToEnterGuar;
	
	/** 快速变现说明 **/
	private String toEnterGuarRemark;
	
	/** 对抵/质押物的总体评价 **/
	private String totlEvlu;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isPhotograph
	 */
	public void setIsPhotograph(String isPhotograph) {
		this.isPhotograph = isPhotograph == null ? null : isPhotograph.trim();
	}
	
    /**
     * @return IsPhotograph
     */	
	public String getIsPhotograph() {
		return this.isPhotograph;
	}
	
	/**
	 * @param noPhotoExpl
	 */
	public void setNoPhotoExpl(String noPhotoExpl) {
		this.noPhotoExpl = noPhotoExpl == null ? null : noPhotoExpl.trim();
	}
	
    /**
     * @return NoPhotoExpl
     */	
	public String getNoPhotoExpl() {
		return this.noPhotoExpl;
	}
	
	/**
	 * @param guarWishCase
	 */
	public void setGuarWishCase(String guarWishCase) {
		this.guarWishCase = guarWishCase == null ? null : guarWishCase.trim();
	}
	
    /**
     * @return GuarWishCase
     */	
	public String getGuarWishCase() {
		return this.guarWishCase;
	}
	
	/**
	 * @param wishCaseExpl
	 */
	public void setWishCaseExpl(String wishCaseExpl) {
		this.wishCaseExpl = wishCaseExpl == null ? null : wishCaseExpl.trim();
	}
	
    /**
     * @return WishCaseExpl
     */	
	public String getWishCaseExpl() {
		return this.wishCaseExpl;
	}
	
	/**
	 * @param isClose
	 */
	public void setIsClose(String isClose) {
		this.isClose = isClose == null ? null : isClose.trim();
	}
	
    /**
     * @return IsClose
     */	
	public String getIsClose() {
		return this.isClose;
	}
	
	/**
	 * @param closeExpl
	 */
	public void setCloseExpl(String closeExpl) {
		this.closeExpl = closeExpl == null ? null : closeExpl.trim();
	}
	
    /**
     * @return CloseExpl
     */	
	public String getCloseExpl() {
		return this.closeExpl;
	}
	
	/**
	 * @param isPriRent
	 */
	public void setIsPriRent(String isPriRent) {
		this.isPriRent = isPriRent == null ? null : isPriRent.trim();
	}
	
    /**
     * @return IsPriRent
     */	
	public String getIsPriRent() {
		return this.isPriRent;
	}
	
	/**
	 * @param rentExpl
	 */
	public void setRentExpl(String rentExpl) {
		this.rentExpl = rentExpl == null ? null : rentExpl.trim();
	}
	
    /**
     * @return RentExpl
     */	
	public String getRentExpl() {
		return this.rentExpl;
	}
	
	/**
	 * @param isOtherRiskEvent
	 */
	public void setIsOtherRiskEvent(String isOtherRiskEvent) {
		this.isOtherRiskEvent = isOtherRiskEvent == null ? null : isOtherRiskEvent.trim();
	}
	
    /**
     * @return IsOtherRiskEvent
     */	
	public String getIsOtherRiskEvent() {
		return this.isOtherRiskEvent;
	}
	
	/**
	 * @param riskEventExpl
	 */
	public void setRiskEventExpl(String riskEventExpl) {
		this.riskEventExpl = riskEventExpl == null ? null : riskEventExpl.trim();
	}
	
    /**
     * @return RiskEventExpl
     */	
	public String getRiskEventExpl() {
		return this.riskEventExpl;
	}
	
	/**
	 * @param isRuin
	 */
	public void setIsRuin(String isRuin) {
		this.isRuin = isRuin == null ? null : isRuin.trim();
	}
	
    /**
     * @return IsRuin
     */	
	public String getIsRuin() {
		return this.isRuin;
	}
	
	/**
	 * @param ruinExpl
	 */
	public void setRuinExpl(String ruinExpl) {
		this.ruinExpl = ruinExpl == null ? null : ruinExpl.trim();
	}
	
    /**
     * @return RuinExpl
     */	
	public String getRuinExpl() {
		return this.ruinExpl;
	}
	
	/**
	 * @param isChangeGuar
	 */
	public void setIsChangeGuar(String isChangeGuar) {
		this.isChangeGuar = isChangeGuar == null ? null : isChangeGuar.trim();
	}
	
    /**
     * @return IsChangeGuar
     */	
	public String getIsChangeGuar() {
		return this.isChangeGuar;
	}
	
	/**
	 * @param changeGuarRemark
	 */
	public void setChangeGuarRemark(String changeGuarRemark) {
		this.changeGuarRemark = changeGuarRemark == null ? null : changeGuarRemark.trim();
	}
	
    /**
     * @return ChangeGuarRemark
     */	
	public String getChangeGuarRemark() {
		return this.changeGuarRemark;
	}
	
	/**
	 * @param isToEnterGuar
	 */
	public void setIsToEnterGuar(String isToEnterGuar) {
		this.isToEnterGuar = isToEnterGuar == null ? null : isToEnterGuar.trim();
	}
	
    /**
     * @return IsToEnterGuar
     */	
	public String getIsToEnterGuar() {
		return this.isToEnterGuar;
	}
	
	/**
	 * @param toEnterGuarRemark
	 */
	public void setToEnterGuarRemark(String toEnterGuarRemark) {
		this.toEnterGuarRemark = toEnterGuarRemark == null ? null : toEnterGuarRemark.trim();
	}
	
    /**
     * @return ToEnterGuarRemark
     */	
	public String getToEnterGuarRemark() {
		return this.toEnterGuarRemark;
	}
	
	/**
	 * @param totlEvlu
	 */
	public void setTotlEvlu(String totlEvlu) {
		this.totlEvlu = totlEvlu == null ? null : totlEvlu.trim();
	}
	
    /**
     * @return TotlEvlu
     */	
	public String getTotlEvlu() {
		return this.totlEvlu;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}