package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspLmtSitu
 * @类描述: psp_lmt_situ数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspLmtSituDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 授信分项流水号 **/
	private String subLmtNo;
	
	/** 授信分项额度名称 **/
	private String subLmtName;
	
	/** 授信额度 **/
	private java.math.BigDecimal lmtAmt;
	
	/** 授信余额 **/
	private java.math.BigDecimal lmtBalanceAmt;
	
	/** 已用额度 **/
	private java.math.BigDecimal outstndAmt;
	
	/** 可用额度 **/
	private java.math.BigDecimal avlLmtAmt;
	
	/** 担保方式 **/
	private String guarMode;
	
	/** 是否循环 **/
	private String isRevolv;
	
	/** 额度到期日 **/
	private String lmtEndDate;
	
	/** 业务大类 **/
	private String prdCatalog;
	
	/** 存量类型 **/
	private String busiType;
	
	/** 授信类型 **/
	private String lmtType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param subLmtNo
	 */
	public void setSubLmtNo(String subLmtNo) {
		this.subLmtNo = subLmtNo == null ? null : subLmtNo.trim();
	}
	
    /**
     * @return SubLmtNo
     */	
	public String getSubLmtNo() {
		return this.subLmtNo;
	}
	
	/**
	 * @param subLmtName
	 */
	public void setSubLmtName(String subLmtName) {
		this.subLmtName = subLmtName == null ? null : subLmtName.trim();
	}
	
    /**
     * @return SubLmtName
     */	
	public String getSubLmtName() {
		return this.subLmtName;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return LmtAmt
     */	
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param lmtBalanceAmt
	 */
	public void setLmtBalanceAmt(java.math.BigDecimal lmtBalanceAmt) {
		this.lmtBalanceAmt = lmtBalanceAmt;
	}
	
    /**
     * @return LmtBalanceAmt
     */	
	public java.math.BigDecimal getLmtBalanceAmt() {
		return this.lmtBalanceAmt;
	}
	
	/**
	 * @param outstndAmt
	 */
	public void setOutstndAmt(java.math.BigDecimal outstndAmt) {
		this.outstndAmt = outstndAmt;
	}
	
    /**
     * @return OutstndAmt
     */	
	public java.math.BigDecimal getOutstndAmt() {
		return this.outstndAmt;
	}
	
	/**
	 * @param avlLmtAmt
	 */
	public void setAvlLmtAmt(java.math.BigDecimal avlLmtAmt) {
		this.avlLmtAmt = avlLmtAmt;
	}
	
    /**
     * @return AvlLmtAmt
     */	
	public java.math.BigDecimal getAvlLmtAmt() {
		return this.avlLmtAmt;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv == null ? null : isRevolv.trim();
	}
	
    /**
     * @return IsRevolv
     */	
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate == null ? null : lmtEndDate.trim();
	}
	
    /**
     * @return LmtEndDate
     */	
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param prdCatalog
	 */
	public void setPrdCatalog(String prdCatalog) {
		this.prdCatalog = prdCatalog == null ? null : prdCatalog.trim();
	}
	
    /**
     * @return PrdCatalog
     */	
	public String getPrdCatalog() {
		return this.prdCatalog;
	}
	
	/**
	 * @param busiType
	 */
	public void setBusiType(String busiType) {
		this.busiType = busiType == null ? null : busiType.trim();
	}
	
    /**
     * @return BusiType
     */	
	public String getBusiType() {
		return this.busiType;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType == null ? null : lmtType.trim();
	}
	
    /**
     * @return LmtType
     */	
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}