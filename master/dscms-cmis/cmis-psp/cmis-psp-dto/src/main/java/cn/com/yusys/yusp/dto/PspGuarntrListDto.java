package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * psp_guarntr_list数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:24
 */
public class PspGuarntrListDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 担保人名称
     **/
    private String guarName;

    /**
     * 担保标志
     **/
    private String guarType;

    /**
     * 担保金额
     **/
    private BigDecimal guarAmt;

    /**
     * 报告期净资产
     **/
    private BigDecimal netAssets;

    /**
     * 已对外担保金额
     **/
    private BigDecimal curtGuarAmt;

    /**
     * 测算担保人可担保金额
     **/
    private BigDecimal evalGuarAmt;

    /**
     * 是否具担保能力
     **/
    private String guarAbi;

    /**
     * 担保人说明
     **/
    private String guarAbiExpl;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspGuarntrListDto() {
        // do nothing
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getGuarName() {
        return guarName;
    }

    public void setGuarName(String guarName) {
        this.guarName = guarName;
    }

    public String getGuarType() {
        return guarType;
    }

    public void setGuarType(String guarType) {
        this.guarType = guarType;
    }

    public BigDecimal getGuarAmt() {
        return guarAmt;
    }

    public void setGuarAmt(BigDecimal guarAmt) {
        this.guarAmt = guarAmt;
    }

    public BigDecimal getNetAssets() {
        return netAssets;
    }

    public void setNetAssets(BigDecimal netAssets) {
        this.netAssets = netAssets;
    }

    public BigDecimal getCurtGuarAmt() {
        return curtGuarAmt;
    }

    public void setCurtGuarAmt(BigDecimal curtGuarAmt) {
        this.curtGuarAmt = curtGuarAmt;
    }

    public BigDecimal getEvalGuarAmt() {
        return evalGuarAmt;
    }

    public void setEvalGuarAmt(BigDecimal evalGuarAmt) {
        this.evalGuarAmt = evalGuarAmt;
    }

    public String getGuarAbi() {
        return guarAbi;
    }

    public void setGuarAbi(String guarAbi) {
        this.guarAbi = guarAbi;
    }

    public String getGuarAbiExpl() {
        return guarAbiExpl;
    }

    public void setGuarAbiExpl(String guarAbiExpl) {
        this.guarAbiExpl = guarAbiExpl;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}