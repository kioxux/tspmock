package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskRepayAnaly
 * @类描述: risk_repay_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskRepayAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 还款意愿 **/
	private String repayWish;
	
	/** 还本金能力 **/
	private String repayCapAbility;
	
	/** 还息能力 **/
	private String repayInterestAbility;
	
	/** 欠息说明 **/
	private String debitInterestDesc;
	
	/** 银行对客户贷款管理 **/
	private String cusLoanManage;
	
	/** 客户是否存在违约行为 **/
	private String isExistsPenalty;
	
	/** 授信业务本金逾期时间T **/
	private String capOverdueDay;
	
	/** 授信业务利息逾期时间T **/
	private String intOverdueDay;
	
	/** 实际控制人(法人代表) **/
	private String infactCtrl;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param repayWish
	 */
	public void setRepayWish(String repayWish) {
		this.repayWish = repayWish == null ? null : repayWish.trim();
	}
	
    /**
     * @return RepayWish
     */	
	public String getRepayWish() {
		return this.repayWish;
	}
	
	/**
	 * @param repayCapAbility
	 */
	public void setRepayCapAbility(String repayCapAbility) {
		this.repayCapAbility = repayCapAbility == null ? null : repayCapAbility.trim();
	}
	
    /**
     * @return RepayCapAbility
     */	
	public String getRepayCapAbility() {
		return this.repayCapAbility;
	}
	
	/**
	 * @param repayInterestAbility
	 */
	public void setRepayInterestAbility(String repayInterestAbility) {
		this.repayInterestAbility = repayInterestAbility == null ? null : repayInterestAbility.trim();
	}
	
    /**
     * @return RepayInterestAbility
     */	
	public String getRepayInterestAbility() {
		return this.repayInterestAbility;
	}
	
	/**
	 * @param debitInterestDesc
	 */
	public void setDebitInterestDesc(String debitInterestDesc) {
		this.debitInterestDesc = debitInterestDesc == null ? null : debitInterestDesc.trim();
	}
	
    /**
     * @return DebitInterestDesc
     */	
	public String getDebitInterestDesc() {
		return this.debitInterestDesc;
	}
	
	/**
	 * @param cusLoanManage
	 */
	public void setCusLoanManage(String cusLoanManage) {
		this.cusLoanManage = cusLoanManage == null ? null : cusLoanManage.trim();
	}
	
    /**
     * @return CusLoanManage
     */	
	public String getCusLoanManage() {
		return this.cusLoanManage;
	}
	
	/**
	 * @param isExistsPenalty
	 */
	public void setIsExistsPenalty(String isExistsPenalty) {
		this.isExistsPenalty = isExistsPenalty == null ? null : isExistsPenalty.trim();
	}
	
    /**
     * @return IsExistsPenalty
     */	
	public String getIsExistsPenalty() {
		return this.isExistsPenalty;
	}
	
	/**
	 * @param capOverdueDay
	 */
	public void setCapOverdueDay(String capOverdueDay) {
		this.capOverdueDay = capOverdueDay == null ? null : capOverdueDay.trim();
	}
	
    /**
     * @return CapOverdueDay
     */	
	public String getCapOverdueDay() {
		return this.capOverdueDay;
	}
	
	/**
	 * @param intOverdueDay
	 */
	public void setIntOverdueDay(String intOverdueDay) {
		this.intOverdueDay = intOverdueDay == null ? null : intOverdueDay.trim();
	}
	
    /**
     * @return IntOverdueDay
     */	
	public String getIntOverdueDay() {
		return this.intOverdueDay;
	}
	
	/**
	 * @param infactCtrl
	 */
	public void setInfactCtrl(String infactCtrl) {
		this.infactCtrl = infactCtrl == null ? null : infactCtrl.trim();
	}
	
    /**
     * @return InfactCtrl
     */	
	public String getInfactCtrl() {
		return this.infactCtrl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}