package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * psp_guarntr_check数据实体类
 *
 * @since 2021-06-17 22:29:24
 */
public class PspGuarntrAndPldimnDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 目前保证人经营是否正常
     **/
    private String isOperNormal;

    /**
     * 保证人经营异常说明
     **/
    private String operExpl;

    /**
     * 保证人担保能力
     **/
    private String guarAbi;

    /**
     * 保证人担保能力说明
     **/
    private String guarAbiExpl;

    /**
     * 保证人担保意愿
     **/
    private String guarWish;

    /**
     * 保证人担保意愿说明
     **/
    private String guarWishExpl;

    /**
     * 是否有其他重要风险事项
     **/
    private String isOtherRiskEvent;

    /**
     * 其他风险说明
     **/
    private String riskEventExpl;

    /**
     * 担保人经营（工作）是否正常
     **/
    private String isGuarOperNormal;

    /**
     * 担保人经营异常说明
     **/
    private String unnormalDesc;

    /**
     * 担保人担保能力是否下降
     **/
    private String isChangeAbi;

    /**
     * 担保人担保能力下降说明
     **/
    private String changeAbiRemark;

    /**
     * 是否超过担保人实际担保能力
     **/
    private String isAddAbi;

    /**
     * 超过担保能力说明
     **/
    private String addAbiRemark;

    /**
     * 担保人及其家庭是否发生意外
     **/
    private String isAccident;

    /**
     * 意外说明
     **/
    private String accidentRemark;

    /**
     * 担保人是否面临重大诉讼
     **/
    private String isInvolveLawsuit;

    /**
     * 重大诉讼说明
     **/
    private String involveLawsuitRemark;

    /**
     * 是否具备担保资格和能力
     **/
    private String isOwnGuarAbi;

    /**
     * 担保资格说明
     **/
    private String ownGuarAbiRemark;

    /**
     * 对担保人的总体评价
     **/
    private String totlEvlu;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    /**
     * 是否对抵（质）押物进行核实并拍照确认
     **/
    private String isPhotograph;

    /**
     * 未核实说明
     **/
    private String noPhotoExpl;

    /**
     * 抵（质）押人的担保意愿情况
     **/
    private String guarWishCase;

    /**
     * 抵（质）押人的担保意愿说明
     **/
    private String wishCaseExpl;

    /**
     * 抵/质押物是否被有关机关查封、冻结、再次抵押
     **/
    private String isClose;

    /**
     * 抵质押物查封说明
     **/
    private String closeExpl;

    /**
     * 是否书面承诺放弃优先购买权及优先租赁权
     **/
    private String isPriRent;

    /**
     * 放弃优先权说明
     **/
    private String rentExpl;

    /**
     * 是否有其他重要风险事项
     **/
    private String isOtherRiskEvent4Pldimn;

    /**
     * 其他风险说明
     **/
    private String riskEventExpl4Pldimn;

    /**
     * 抵/质押物是否损毁或大幅贬值
     **/
    private String isRuin;

    /**
     * 抵/质押物贬值说明
     **/
    private String ruinExpl;

    /**
     * 抵/质押物变现能力与审批贷款时有无较大差异
     **/
    private String isChangeGuar;

    /**
     * 变现能力差异说明
     **/
    private String changeGuarRemark;

    /**
     * 该抵/质押物是否能迅速变现并足额抵偿相关债务
     **/
    private String isToEnterGuar;

    /**
     * 快速变现说明
     **/
    private String toEnterGuarRemark;

    /**
     * 对抵/质押物的总体评价
     **/
    private String totlEvlu4Pldimn;

    public PspGuarntrAndPldimnDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getIsOperNormal() {
        return isOperNormal;
    }

    public void setIsOperNormal(String isOperNormal) {
        this.isOperNormal = isOperNormal;
    }

    public String getOperExpl() {
        return operExpl;
    }

    public void setOperExpl(String operExpl) {
        this.operExpl = operExpl;
    }

    public String getGuarAbi() {
        return guarAbi;
    }

    public void setGuarAbi(String guarAbi) {
        this.guarAbi = guarAbi;
    }

    public String getGuarAbiExpl() {
        return guarAbiExpl;
    }

    public void setGuarAbiExpl(String guarAbiExpl) {
        this.guarAbiExpl = guarAbiExpl;
    }

    public String getGuarWish() {
        return guarWish;
    }

    public void setGuarWish(String guarWish) {
        this.guarWish = guarWish;
    }

    public String getGuarWishExpl() {
        return guarWishExpl;
    }

    public void setGuarWishExpl(String guarWishExpl) {
        this.guarWishExpl = guarWishExpl;
    }

    public String getIsOtherRiskEvent() {
        return isOtherRiskEvent;
    }

    public void setIsOtherRiskEvent(String isOtherRiskEvent) {
        this.isOtherRiskEvent = isOtherRiskEvent;
    }

    public String getRiskEventExpl() {
        return riskEventExpl;
    }

    public void setRiskEventExpl(String riskEventExpl) {
        this.riskEventExpl = riskEventExpl;
    }

    public String getIsGuarOperNormal() {
        return isGuarOperNormal;
    }

    public void setIsGuarOperNormal(String isGuarOperNormal) {
        this.isGuarOperNormal = isGuarOperNormal;
    }

    public String getUnnormalDesc() {
        return unnormalDesc;
    }

    public void setUnnormalDesc(String unnormalDesc) {
        this.unnormalDesc = unnormalDesc;
    }

    public String getIsChangeAbi() {
        return isChangeAbi;
    }

    public void setIsChangeAbi(String isChangeAbi) {
        this.isChangeAbi = isChangeAbi;
    }

    public String getChangeAbiRemark() {
        return changeAbiRemark;
    }

    public void setChangeAbiRemark(String changeAbiRemark) {
        this.changeAbiRemark = changeAbiRemark;
    }

    public String getIsAddAbi() {
        return isAddAbi;
    }

    public void setIsAddAbi(String isAddAbi) {
        this.isAddAbi = isAddAbi;
    }

    public String getAddAbiRemark() {
        return addAbiRemark;
    }

    public void setAddAbiRemark(String addAbiRemark) {
        this.addAbiRemark = addAbiRemark;
    }

    public String getIsAccident() {
        return isAccident;
    }

    public void setIsAccident(String isAccident) {
        this.isAccident = isAccident;
    }

    public String getAccidentRemark() {
        return accidentRemark;
    }

    public void setAccidentRemark(String accidentRemark) {
        this.accidentRemark = accidentRemark;
    }

    public String getIsInvolveLawsuit() {
        return isInvolveLawsuit;
    }

    public void setIsInvolveLawsuit(String isInvolveLawsuit) {
        this.isInvolveLawsuit = isInvolveLawsuit;
    }

    public String getInvolveLawsuitRemark() {
        return involveLawsuitRemark;
    }

    public void setInvolveLawsuitRemark(String involveLawsuitRemark) {
        this.involveLawsuitRemark = involveLawsuitRemark;
    }

    public String getIsOwnGuarAbi() {
        return isOwnGuarAbi;
    }

    public void setIsOwnGuarAbi(String isOwnGuarAbi) {
        this.isOwnGuarAbi = isOwnGuarAbi;
    }

    public String getOwnGuarAbiRemark() {
        return ownGuarAbiRemark;
    }

    public void setOwnGuarAbiRemark(String ownGuarAbiRemark) {
        this.ownGuarAbiRemark = ownGuarAbiRemark;
    }

    public String getTotlEvlu() {
        return totlEvlu;
    }

    public void setTotlEvlu(String totlEvlu) {
        this.totlEvlu = totlEvlu;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsPhotograph() {
        return isPhotograph;
    }

    public void setIsPhotograph(String isPhotograph) {
        this.isPhotograph = isPhotograph;
    }

    public String getNoPhotoExpl() {
        return noPhotoExpl;
    }

    public void setNoPhotoExpl(String noPhotoExpl) {
        this.noPhotoExpl = noPhotoExpl;
    }

    public String getGuarWishCase() {
        return guarWishCase;
    }

    public void setGuarWishCase(String guarWishCase) {
        this.guarWishCase = guarWishCase;
    }

    public String getWishCaseExpl() {
        return wishCaseExpl;
    }

    public void setWishCaseExpl(String wishCaseExpl) {
        this.wishCaseExpl = wishCaseExpl;
    }

    public String getIsClose() {
        return isClose;
    }

    public void setIsClose(String isClose) {
        this.isClose = isClose;
    }

    public String getCloseExpl() {
        return closeExpl;
    }

    public void setCloseExpl(String closeExpl) {
        this.closeExpl = closeExpl;
    }

    public String getIsPriRent() {
        return isPriRent;
    }

    public void setIsPriRent(String isPriRent) {
        this.isPriRent = isPriRent;
    }

    public String getRentExpl() {
        return rentExpl;
    }

    public void setRentExpl(String rentExpl) {
        this.rentExpl = rentExpl;
    }

    public String getIsOtherRiskEvent4Pldimn() {
        return isOtherRiskEvent4Pldimn;
    }

    public void setIsOtherRiskEvent4Pldimn(String isOtherRiskEvent4Pldimn) {
        this.isOtherRiskEvent4Pldimn = isOtherRiskEvent4Pldimn;
    }

    public String getRiskEventExpl4Pldimn() {
        return riskEventExpl4Pldimn;
    }

    public void setRiskEventExpl4Pldimn(String riskEventExpl4Pldimn) {
        this.riskEventExpl4Pldimn = riskEventExpl4Pldimn;
    }

    public String getIsRuin() {
        return isRuin;
    }

    public void setIsRuin(String isRuin) {
        this.isRuin = isRuin;
    }

    public String getRuinExpl() {
        return ruinExpl;
    }

    public void setRuinExpl(String ruinExpl) {
        this.ruinExpl = ruinExpl;
    }

    public String getIsChangeGuar() {
        return isChangeGuar;
    }

    public void setIsChangeGuar(String isChangeGuar) {
        this.isChangeGuar = isChangeGuar;
    }

    public String getChangeGuarRemark() {
        return changeGuarRemark;
    }

    public void setChangeGuarRemark(String changeGuarRemark) {
        this.changeGuarRemark = changeGuarRemark;
    }

    public String getIsToEnterGuar() {
        return isToEnterGuar;
    }

    public void setIsToEnterGuar(String isToEnterGuar) {
        this.isToEnterGuar = isToEnterGuar;
    }

    public String getToEnterGuarRemark() {
        return toEnterGuarRemark;
    }

    public void setToEnterGuarRemark(String toEnterGuarRemark) {
        this.toEnterGuarRemark = toEnterGuarRemark;
    }

    public String getTotlEvlu4Pldimn() {
        return totlEvlu4Pldimn;
    }

    public void setTotlEvlu4Pldimn(String totlEvlu4Pldimn) {
        this.totlEvlu4Pldimn = totlEvlu4Pldimn;
    }
}