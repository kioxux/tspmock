package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspPropertyCheck
 * @类描述: psp_property_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspPropertyCheckDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 是我在我行开立专户 **/
	private String isAcct;
	
	/** 专户开立说明 **/
	private String acctExpl;
	
	/** 账号 **/
	private String acctNo;
	
	/** 账户名称 **/
	private String acctName;
	
	/** 账户序号 **/
	private String acctSeqNo;
	
	/** 开户行 **/
	private String acctb;
	
	/** 项目预计总投入（万元） **/
	private java.math.BigDecimal totalAmt;
	
	/** 他行贷款已投入（万元） **/
	private java.math.BigDecimal otherAmt;
	
	/** 项目实际累计总投入（万元） **/
	private java.math.BigDecimal useAmt;
	
	/** 借款人自有资金已投入（万元） **/
	private java.math.BigDecimal localityAmt;
	
	/** 我行贷款已投入（万元） **/
	private java.math.BigDecimal bankAmt;
	
	/** 借款人资本金已投入（万元） **/
	private java.math.BigDecimal otherLocalityAmt;
	
	/** 目前项目进度是否与可研报告一致 **/
	private String isIdentical;
	
	/** 项目进度说明 **/
	private String identicalExpl;
	
	/** 自筹资金是否能按期、足额到位 **/
	private String isOntimeAmt;
	
	/** 自筹资金到位说明 **/
	private String ontimeAmtExpl;
	
	/** 是否按时竣工 **/
	private String isCompletion;
	
	/** 未按时竣工说明 **/
	private String completionExpl;
	
	/** 消防是否验收合格 **/
	private String isCertificate;
	
	/** 消防不合格说明 **/
	private String certificateExpl;
	
	/** 实际投资与预期投资是否存在差异 **/
	private String isDiscrepancy;
	
	/** 投资差异说明 **/
	private String discrepancyExpl;
	
	/** 项目实际生产能力是否达到预期要求 **/
	private String isArriveNeed;
	
	/** 生产能力说明 **/
	private String arriveNeedExpl;
	
	/** 产品质量是否有问题 **/
	private String isQuality;
	
	/** 质量问题说明 **/
	private String qualityExpl;
	
	/** 生产许可证等相关证书是否齐全 **/
	private String isComp;
	
	/** 证书齐全说明 **/
	private String compExpl;
	
	/** 排污许可证是否取得 **/
	private String isRowLice;
	
	/** 排污许可证说明 **/
	private String rowLiceExpl;
	
	/** 是否能按计划进入批量生产 **/
	private String isBatchProduce;
	
	/** 按时批量生产说明 **/
	private String batchProduceExpl;
	
	/** 能否按预期及时回笼货款 **/
	private String isAmtBack;
	
	/** 回笼货款说明 **/
	private String amtBackExpl;
	
	/** 是否按我行还款计划按时足额还款 **/
	private String isRepayOntime;
	
	/** 按时还款说明 **/
	private String repayOntimeExpl;
	
	/** 贷款使用是否与项目进度相匹配 **/
	private String isScheduleMatching;
	
	/** 进度匹配说明 **/
	private String scheduleExpl;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isAcct
	 */
	public void setIsAcct(String isAcct) {
		this.isAcct = isAcct == null ? null : isAcct.trim();
	}
	
    /**
     * @return IsAcct
     */	
	public String getIsAcct() {
		return this.isAcct;
	}
	
	/**
	 * @param acctExpl
	 */
	public void setAcctExpl(String acctExpl) {
		this.acctExpl = acctExpl == null ? null : acctExpl.trim();
	}
	
    /**
     * @return AcctExpl
     */	
	public String getAcctExpl() {
		return this.acctExpl;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo == null ? null : acctNo.trim();
	}
	
    /**
     * @return AcctNo
     */	
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName == null ? null : acctName.trim();
	}
	
    /**
     * @return AcctName
     */	
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param acctSeqNo
	 */
	public void setAcctSeqNo(String acctSeqNo) {
		this.acctSeqNo = acctSeqNo == null ? null : acctSeqNo.trim();
	}
	
    /**
     * @return AcctSeqNo
     */	
	public String getAcctSeqNo() {
		return this.acctSeqNo;
	}
	
	/**
	 * @param acctb
	 */
	public void setAcctb(String acctb) {
		this.acctb = acctb == null ? null : acctb.trim();
	}
	
    /**
     * @return Acctb
     */	
	public String getAcctb() {
		return this.acctb;
	}
	
	/**
	 * @param totalAmt
	 */
	public void setTotalAmt(java.math.BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	
    /**
     * @return TotalAmt
     */	
	public java.math.BigDecimal getTotalAmt() {
		return this.totalAmt;
	}
	
	/**
	 * @param otherAmt
	 */
	public void setOtherAmt(java.math.BigDecimal otherAmt) {
		this.otherAmt = otherAmt;
	}
	
    /**
     * @return OtherAmt
     */	
	public java.math.BigDecimal getOtherAmt() {
		return this.otherAmt;
	}
	
	/**
	 * @param useAmt
	 */
	public void setUseAmt(java.math.BigDecimal useAmt) {
		this.useAmt = useAmt;
	}
	
    /**
     * @return UseAmt
     */	
	public java.math.BigDecimal getUseAmt() {
		return this.useAmt;
	}
	
	/**
	 * @param localityAmt
	 */
	public void setLocalityAmt(java.math.BigDecimal localityAmt) {
		this.localityAmt = localityAmt;
	}
	
    /**
     * @return LocalityAmt
     */	
	public java.math.BigDecimal getLocalityAmt() {
		return this.localityAmt;
	}
	
	/**
	 * @param bankAmt
	 */
	public void setBankAmt(java.math.BigDecimal bankAmt) {
		this.bankAmt = bankAmt;
	}
	
    /**
     * @return BankAmt
     */	
	public java.math.BigDecimal getBankAmt() {
		return this.bankAmt;
	}
	
	/**
	 * @param otherLocalityAmt
	 */
	public void setOtherLocalityAmt(java.math.BigDecimal otherLocalityAmt) {
		this.otherLocalityAmt = otherLocalityAmt;
	}
	
    /**
     * @return OtherLocalityAmt
     */	
	public java.math.BigDecimal getOtherLocalityAmt() {
		return this.otherLocalityAmt;
	}
	
	/**
	 * @param isIdentical
	 */
	public void setIsIdentical(String isIdentical) {
		this.isIdentical = isIdentical == null ? null : isIdentical.trim();
	}
	
    /**
     * @return IsIdentical
     */	
	public String getIsIdentical() {
		return this.isIdentical;
	}
	
	/**
	 * @param identicalExpl
	 */
	public void setIdenticalExpl(String identicalExpl) {
		this.identicalExpl = identicalExpl == null ? null : identicalExpl.trim();
	}
	
    /**
     * @return IdenticalExpl
     */	
	public String getIdenticalExpl() {
		return this.identicalExpl;
	}
	
	/**
	 * @param isOntimeAmt
	 */
	public void setIsOntimeAmt(String isOntimeAmt) {
		this.isOntimeAmt = isOntimeAmt == null ? null : isOntimeAmt.trim();
	}
	
    /**
     * @return IsOntimeAmt
     */	
	public String getIsOntimeAmt() {
		return this.isOntimeAmt;
	}
	
	/**
	 * @param ontimeAmtExpl
	 */
	public void setOntimeAmtExpl(String ontimeAmtExpl) {
		this.ontimeAmtExpl = ontimeAmtExpl == null ? null : ontimeAmtExpl.trim();
	}
	
    /**
     * @return OntimeAmtExpl
     */	
	public String getOntimeAmtExpl() {
		return this.ontimeAmtExpl;
	}
	
	/**
	 * @param isCompletion
	 */
	public void setIsCompletion(String isCompletion) {
		this.isCompletion = isCompletion == null ? null : isCompletion.trim();
	}
	
    /**
     * @return IsCompletion
     */	
	public String getIsCompletion() {
		return this.isCompletion;
	}
	
	/**
	 * @param completionExpl
	 */
	public void setCompletionExpl(String completionExpl) {
		this.completionExpl = completionExpl == null ? null : completionExpl.trim();
	}
	
    /**
     * @return CompletionExpl
     */	
	public String getCompletionExpl() {
		return this.completionExpl;
	}
	
	/**
	 * @param isCertificate
	 */
	public void setIsCertificate(String isCertificate) {
		this.isCertificate = isCertificate == null ? null : isCertificate.trim();
	}
	
    /**
     * @return IsCertificate
     */	
	public String getIsCertificate() {
		return this.isCertificate;
	}
	
	/**
	 * @param certificateExpl
	 */
	public void setCertificateExpl(String certificateExpl) {
		this.certificateExpl = certificateExpl == null ? null : certificateExpl.trim();
	}
	
    /**
     * @return CertificateExpl
     */	
	public String getCertificateExpl() {
		return this.certificateExpl;
	}
	
	/**
	 * @param isDiscrepancy
	 */
	public void setIsDiscrepancy(String isDiscrepancy) {
		this.isDiscrepancy = isDiscrepancy == null ? null : isDiscrepancy.trim();
	}
	
    /**
     * @return IsDiscrepancy
     */	
	public String getIsDiscrepancy() {
		return this.isDiscrepancy;
	}
	
	/**
	 * @param discrepancyExpl
	 */
	public void setDiscrepancyExpl(String discrepancyExpl) {
		this.discrepancyExpl = discrepancyExpl == null ? null : discrepancyExpl.trim();
	}
	
    /**
     * @return DiscrepancyExpl
     */	
	public String getDiscrepancyExpl() {
		return this.discrepancyExpl;
	}
	
	/**
	 * @param isArriveNeed
	 */
	public void setIsArriveNeed(String isArriveNeed) {
		this.isArriveNeed = isArriveNeed == null ? null : isArriveNeed.trim();
	}
	
    /**
     * @return IsArriveNeed
     */	
	public String getIsArriveNeed() {
		return this.isArriveNeed;
	}
	
	/**
	 * @param arriveNeedExpl
	 */
	public void setArriveNeedExpl(String arriveNeedExpl) {
		this.arriveNeedExpl = arriveNeedExpl == null ? null : arriveNeedExpl.trim();
	}
	
    /**
     * @return ArriveNeedExpl
     */	
	public String getArriveNeedExpl() {
		return this.arriveNeedExpl;
	}
	
	/**
	 * @param isQuality
	 */
	public void setIsQuality(String isQuality) {
		this.isQuality = isQuality == null ? null : isQuality.trim();
	}
	
    /**
     * @return IsQuality
     */	
	public String getIsQuality() {
		return this.isQuality;
	}
	
	/**
	 * @param qualityExpl
	 */
	public void setQualityExpl(String qualityExpl) {
		this.qualityExpl = qualityExpl == null ? null : qualityExpl.trim();
	}
	
    /**
     * @return QualityExpl
     */	
	public String getQualityExpl() {
		return this.qualityExpl;
	}
	
	/**
	 * @param isComp
	 */
	public void setIsComp(String isComp) {
		this.isComp = isComp == null ? null : isComp.trim();
	}
	
    /**
     * @return IsComp
     */	
	public String getIsComp() {
		return this.isComp;
	}
	
	/**
	 * @param compExpl
	 */
	public void setCompExpl(String compExpl) {
		this.compExpl = compExpl == null ? null : compExpl.trim();
	}
	
    /**
     * @return CompExpl
     */	
	public String getCompExpl() {
		return this.compExpl;
	}
	
	/**
	 * @param isRowLice
	 */
	public void setIsRowLice(String isRowLice) {
		this.isRowLice = isRowLice == null ? null : isRowLice.trim();
	}
	
    /**
     * @return IsRowLice
     */	
	public String getIsRowLice() {
		return this.isRowLice;
	}
	
	/**
	 * @param rowLiceExpl
	 */
	public void setRowLiceExpl(String rowLiceExpl) {
		this.rowLiceExpl = rowLiceExpl == null ? null : rowLiceExpl.trim();
	}
	
    /**
     * @return RowLiceExpl
     */	
	public String getRowLiceExpl() {
		return this.rowLiceExpl;
	}
	
	/**
	 * @param isBatchProduce
	 */
	public void setIsBatchProduce(String isBatchProduce) {
		this.isBatchProduce = isBatchProduce == null ? null : isBatchProduce.trim();
	}
	
    /**
     * @return IsBatchProduce
     */	
	public String getIsBatchProduce() {
		return this.isBatchProduce;
	}
	
	/**
	 * @param batchProduceExpl
	 */
	public void setBatchProduceExpl(String batchProduceExpl) {
		this.batchProduceExpl = batchProduceExpl == null ? null : batchProduceExpl.trim();
	}
	
    /**
     * @return BatchProduceExpl
     */	
	public String getBatchProduceExpl() {
		return this.batchProduceExpl;
	}
	
	/**
	 * @param isAmtBack
	 */
	public void setIsAmtBack(String isAmtBack) {
		this.isAmtBack = isAmtBack == null ? null : isAmtBack.trim();
	}
	
    /**
     * @return IsAmtBack
     */	
	public String getIsAmtBack() {
		return this.isAmtBack;
	}
	
	/**
	 * @param amtBackExpl
	 */
	public void setAmtBackExpl(String amtBackExpl) {
		this.amtBackExpl = amtBackExpl == null ? null : amtBackExpl.trim();
	}
	
    /**
     * @return AmtBackExpl
     */	
	public String getAmtBackExpl() {
		return this.amtBackExpl;
	}
	
	/**
	 * @param isRepayOntime
	 */
	public void setIsRepayOntime(String isRepayOntime) {
		this.isRepayOntime = isRepayOntime == null ? null : isRepayOntime.trim();
	}
	
    /**
     * @return IsRepayOntime
     */	
	public String getIsRepayOntime() {
		return this.isRepayOntime;
	}
	
	/**
	 * @param repayOntimeExpl
	 */
	public void setRepayOntimeExpl(String repayOntimeExpl) {
		this.repayOntimeExpl = repayOntimeExpl == null ? null : repayOntimeExpl.trim();
	}
	
    /**
     * @return RepayOntimeExpl
     */	
	public String getRepayOntimeExpl() {
		return this.repayOntimeExpl;
	}
	
	/**
	 * @param isScheduleMatching
	 */
	public void setIsScheduleMatching(String isScheduleMatching) {
		this.isScheduleMatching = isScheduleMatching == null ? null : isScheduleMatching.trim();
	}
	
    /**
     * @return IsScheduleMatching
     */	
	public String getIsScheduleMatching() {
		return this.isScheduleMatching;
	}
	
	/**
	 * @param scheduleExpl
	 */
	public void setScheduleExpl(String scheduleExpl) {
		this.scheduleExpl = scheduleExpl == null ? null : scheduleExpl.trim();
	}
	
    /**
     * @return ScheduleExpl
     */	
	public String getScheduleExpl() {
		return this.scheduleExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}