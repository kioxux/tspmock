package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspWarningInfoList
 * @类描述: psp_warning_info_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspWarningInfoListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 预警单编号 **/
	private String altSinNo;
	
	/** 预警日期 **/
	private String altDate;
	
	/** 信息来源 **/
	private String infoSource;
	
	/** 预警大类 **/
	private String altTypeMax;
	
	/** 预警种类 **/
	private String altType;
	
	/** 预警子项 **/
	private String altSubType;
	
	/** 预警输出描述 **/
	private String altDesc;
	
	/** 指标等级 **/
	private String altLvl;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param altSinNo
	 */
	public void setAltSinNo(String altSinNo) {
		this.altSinNo = altSinNo == null ? null : altSinNo.trim();
	}
	
    /**
     * @return AltSinNo
     */	
	public String getAltSinNo() {
		return this.altSinNo;
	}
	
	/**
	 * @param altDate
	 */
	public void setAltDate(String altDate) {
		this.altDate = altDate == null ? null : altDate.trim();
	}
	
    /**
     * @return AltDate
     */	
	public String getAltDate() {
		return this.altDate;
	}
	
	/**
	 * @param infoSource
	 */
	public void setInfoSource(String infoSource) {
		this.infoSource = infoSource == null ? null : infoSource.trim();
	}
	
    /**
     * @return InfoSource
     */	
	public String getInfoSource() {
		return this.infoSource;
	}
	
	/**
	 * @param altTypeMax
	 */
	public void setAltTypeMax(String altTypeMax) {
		this.altTypeMax = altTypeMax == null ? null : altTypeMax.trim();
	}
	
    /**
     * @return AltTypeMax
     */	
	public String getAltTypeMax() {
		return this.altTypeMax;
	}
	
	/**
	 * @param altType
	 */
	public void setAltType(String altType) {
		this.altType = altType == null ? null : altType.trim();
	}
	
    /**
     * @return AltType
     */	
	public String getAltType() {
		return this.altType;
	}
	
	/**
	 * @param altSubType
	 */
	public void setAltSubType(String altSubType) {
		this.altSubType = altSubType == null ? null : altSubType.trim();
	}
	
    /**
     * @return AltSubType
     */	
	public String getAltSubType() {
		return this.altSubType;
	}
	
	/**
	 * @param altDesc
	 */
	public void setAltDesc(String altDesc) {
		this.altDesc = altDesc == null ? null : altDesc.trim();
	}
	
    /**
     * @return AltDesc
     */	
	public String getAltDesc() {
		return this.altDesc;
	}
	
	/**
	 * @param altLvl
	 */
	public void setAltLvl(String altLvl) {
		this.altLvl = altLvl == null ? null : altLvl.trim();
	}
	
    /**
     * @return AltLvl
     */	
	public String getAltLvl() {
		return this.altLvl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}