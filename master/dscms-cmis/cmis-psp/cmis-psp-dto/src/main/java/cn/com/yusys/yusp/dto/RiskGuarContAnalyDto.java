package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskGuarContAnaly
 * @类描述: risk_guar_cont_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskGuarContAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 担保合同编号 **/
	private String guarContNo;
	
	/** 担保合同类型 **/
	private String guarContType;
	
	/** 是否授信项下 **/
	private String isUnderLmt;
	
	/** 授信额度编号 **/
	private String lmtAccNo;
	
	/** 担保方式 **/
	private String guarMode;
	
	/** 担保合同金额 **/
	private java.math.BigDecimal guarAmt;
	
	/** 借款人名称 **/
	private String borrowerName;
	
	/** 担保合同状态 **/
	private String guarContState;
	
	/** 担保合同有效性 **/
	private String guarContValidFlag;
	
	/** 担保合同限制性条款 **/
	private String guarContLimitFlag;
	
	/** 担保合同代偿能力说明 **/
	private String guarRepayAbiRemark;
	
	/** 分析状态 **/
	private String analyStatus;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}
	
    /**
     * @return GuarContNo
     */	
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param guarContType
	 */
	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType == null ? null : guarContType.trim();
	}
	
    /**
     * @return GuarContType
     */	
	public String getGuarContType() {
		return this.guarContType;
	}
	
	/**
	 * @param isUnderLmt
	 */
	public void setIsUnderLmt(String isUnderLmt) {
		this.isUnderLmt = isUnderLmt == null ? null : isUnderLmt.trim();
	}
	
    /**
     * @return IsUnderLmt
     */	
	public String getIsUnderLmt() {
		return this.isUnderLmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo == null ? null : lmtAccNo.trim();
	}
	
    /**
     * @return LmtAccNo
     */	
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return GuarAmt
     */	
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param borrowerName
	 */
	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName == null ? null : borrowerName.trim();
	}
	
    /**
     * @return BorrowerName
     */	
	public String getBorrowerName() {
		return this.borrowerName;
	}
	
	/**
	 * @param guarContState
	 */
	public void setGuarContState(String guarContState) {
		this.guarContState = guarContState == null ? null : guarContState.trim();
	}
	
    /**
     * @return GuarContState
     */	
	public String getGuarContState() {
		return this.guarContState;
	}
	
	/**
	 * @param guarContValidFlag
	 */
	public void setGuarContValidFlag(String guarContValidFlag) {
		this.guarContValidFlag = guarContValidFlag == null ? null : guarContValidFlag.trim();
	}
	
    /**
     * @return GuarContValidFlag
     */	
	public String getGuarContValidFlag() {
		return this.guarContValidFlag;
	}
	
	/**
	 * @param guarContLimitFlag
	 */
	public void setGuarContLimitFlag(String guarContLimitFlag) {
		this.guarContLimitFlag = guarContLimitFlag == null ? null : guarContLimitFlag.trim();
	}
	
    /**
     * @return GuarContLimitFlag
     */	
	public String getGuarContLimitFlag() {
		return this.guarContLimitFlag;
	}
	
	/**
	 * @param guarRepayAbiRemark
	 */
	public void setGuarRepayAbiRemark(String guarRepayAbiRemark) {
		this.guarRepayAbiRemark = guarRepayAbiRemark == null ? null : guarRepayAbiRemark.trim();
	}
	
    /**
     * @return GuarRepayAbiRemark
     */	
	public String getGuarRepayAbiRemark() {
		return this.guarRepayAbiRemark;
	}
	
	/**
	 * @param analyStatus
	 */
	public void setAnalyStatus(String analyStatus) {
		this.analyStatus = analyStatus == null ? null : analyStatus.trim();
	}
	
    /**
     * @return AnalyStatus
     */	
	public String getAnalyStatus() {
		return this.analyStatus;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}