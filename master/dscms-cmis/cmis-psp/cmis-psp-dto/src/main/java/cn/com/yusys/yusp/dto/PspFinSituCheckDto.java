package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * psp_fin_situ_check数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-28 13:16:39
 */
public class PspFinSituCheckDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 科目编号
     **/
    private String idxKey;

    /**
     * 科目名称
     **/
    private String idxName;

    /**
     * 上年末数据
     **/
    private String l1yEndValue;

    /**
     * 本期数据
     **/
    private String curtValue;

    /**
     * 上年同期数据
     **/
    private String l1yCurtValue;

    /**
     * 本期数据和上年同期数变化
     **/
    private String l1yRiseRate;

    /**
     * 说明
     **/
    private String idxRemark;

    /**
     * 提示信息
     **/
    private String hintInfo;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspFinSituCheckDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getIdxKey() {
        return idxKey;
    }

    public void setIdxKey(String idxKey) {
        this.idxKey = idxKey;
    }

    public String getIdxName() {
        return idxName;
    }

    public void setIdxName(String idxName) {
        this.idxName = idxName;
    }

    public String getL1yEndValue() {
        return l1yEndValue;
    }

    public void setL1yEndValue(String l1yEndValue) {
        this.l1yEndValue = l1yEndValue;
    }

    public String getCurtValue() {
        return curtValue;
    }

    public void setCurtValue(String curtValue) {
        this.curtValue = curtValue;
    }

    public String getL1yCurtValue() {
        return l1yCurtValue;
    }

    public void setL1yCurtValue(String l1yCurtValue) {
        this.l1yCurtValue = l1yCurtValue;
    }

    public String getL1yRiseRate() {
        return l1yRiseRate;
    }

    public void setL1yRiseRate(String l1yRiseRate) {
        this.l1yRiseRate = l1yRiseRate;
    }

    public String getIdxRemark() {
        return idxRemark;
    }

    public void setIdxRemark(String idxRemark) {
        this.idxRemark = idxRemark;
    }

    public String getHintInfo() {
        return hintInfo;
    }

    public void setHintInfo(String hintInfo) {
        this.hintInfo = hintInfo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}