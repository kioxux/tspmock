package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskDebitAnaly
 * @类描述: risk_debit_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskDebitAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 销售收入变化趋势 **/
	private String incomeChange;
	
	/** 利润变动趋势 **/
	private String profitChange;
	
	/** 现金流量变动趋势 **/
	private String cashChange;
	
	/** 经营活动现金流量变动趋势 **/
	private String operCashChange;
	
	/** 是否按约定用途使用贷款 **/
	private String isRightPurp;
	
	/** 贷款用途使用说明 **/
	private String loanPurpDesc;
	
	/** 有无不良行为、不良嗜好 **/
	private String isBadAction;
	
	/** 不良行为嗜好说明 **/
	private String badActionDesc;
	
	/** 有无不良信用记录 **/
	private String isBadCdtRecord;
	
	/** 家庭状况是否正确 **/
	private String isFamilyStatusNormal;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param incomeChange
	 */
	public void setIncomeChange(String incomeChange) {
		this.incomeChange = incomeChange == null ? null : incomeChange.trim();
	}
	
    /**
     * @return IncomeChange
     */	
	public String getIncomeChange() {
		return this.incomeChange;
	}
	
	/**
	 * @param profitChange
	 */
	public void setProfitChange(String profitChange) {
		this.profitChange = profitChange == null ? null : profitChange.trim();
	}
	
    /**
     * @return ProfitChange
     */	
	public String getProfitChange() {
		return this.profitChange;
	}
	
	/**
	 * @param cashChange
	 */
	public void setCashChange(String cashChange) {
		this.cashChange = cashChange == null ? null : cashChange.trim();
	}
	
    /**
     * @return CashChange
     */	
	public String getCashChange() {
		return this.cashChange;
	}
	
	/**
	 * @param operCashChange
	 */
	public void setOperCashChange(String operCashChange) {
		this.operCashChange = operCashChange == null ? null : operCashChange.trim();
	}
	
    /**
     * @return OperCashChange
     */	
	public String getOperCashChange() {
		return this.operCashChange;
	}
	
	/**
	 * @param isRightPurp
	 */
	public void setIsRightPurp(String isRightPurp) {
		this.isRightPurp = isRightPurp == null ? null : isRightPurp.trim();
	}
	
    /**
     * @return IsRightPurp
     */	
	public String getIsRightPurp() {
		return this.isRightPurp;
	}
	
	/**
	 * @param loanPurpDesc
	 */
	public void setLoanPurpDesc(String loanPurpDesc) {
		this.loanPurpDesc = loanPurpDesc == null ? null : loanPurpDesc.trim();
	}
	
    /**
     * @return LoanPurpDesc
     */	
	public String getLoanPurpDesc() {
		return this.loanPurpDesc;
	}
	
	/**
	 * @param isBadAction
	 */
	public void setIsBadAction(String isBadAction) {
		this.isBadAction = isBadAction == null ? null : isBadAction.trim();
	}
	
    /**
     * @return IsBadAction
     */	
	public String getIsBadAction() {
		return this.isBadAction;
	}
	
	/**
	 * @param badActionDesc
	 */
	public void setBadActionDesc(String badActionDesc) {
		this.badActionDesc = badActionDesc == null ? null : badActionDesc.trim();
	}
	
    /**
     * @return BadActionDesc
     */	
	public String getBadActionDesc() {
		return this.badActionDesc;
	}
	
	/**
	 * @param isBadCdtRecord
	 */
	public void setIsBadCdtRecord(String isBadCdtRecord) {
		this.isBadCdtRecord = isBadCdtRecord == null ? null : isBadCdtRecord.trim();
	}
	
    /**
     * @return IsBadCdtRecord
     */	
	public String getIsBadCdtRecord() {
		return this.isBadCdtRecord;
	}
	
	/**
	 * @param isFamilyStatusNormal
	 */
	public void setIsFamilyStatusNormal(String isFamilyStatusNormal) {
		this.isFamilyStatusNormal = isFamilyStatusNormal == null ? null : isFamilyStatusNormal.trim();
	}
	
    /**
     * @return IsFamilyStatusNormal
     */	
	public String getIsFamilyStatusNormal() {
		return this.isFamilyStatusNormal;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}