package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskNonFinaAnaly
 * @类描述: risk_non_fina_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskNonFinaAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 外部宏观经济环境发生变化情况 **/
	private String economyChangeCase;
	
	/** 外部宏观经济环境说明 **/
	private String changeCaseExpl;
	
	/** 行业风险 **/
	private String tradeRisk;
	
	/** 行业风险说明 **/
	private String tradeRiskExpl;
	
	/** 主要股东、关联公司或母子公司发生重大变化 **/
	private String shareholderRelaChange;
	
	/** 主要股东、关联公司或母子公司变化说明 **/
	private String relaChangeExpl;
	
	/** 借款人内部管理情况 **/
	private String manaCase;
	
	/** 借款人内部管理情况说明 **/
	private String manaCaseExpl;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param economyChangeCase
	 */
	public void setEconomyChangeCase(String economyChangeCase) {
		this.economyChangeCase = economyChangeCase == null ? null : economyChangeCase.trim();
	}
	
    /**
     * @return EconomyChangeCase
     */	
	public String getEconomyChangeCase() {
		return this.economyChangeCase;
	}
	
	/**
	 * @param changeCaseExpl
	 */
	public void setChangeCaseExpl(String changeCaseExpl) {
		this.changeCaseExpl = changeCaseExpl == null ? null : changeCaseExpl.trim();
	}
	
    /**
     * @return ChangeCaseExpl
     */	
	public String getChangeCaseExpl() {
		return this.changeCaseExpl;
	}
	
	/**
	 * @param tradeRisk
	 */
	public void setTradeRisk(String tradeRisk) {
		this.tradeRisk = tradeRisk == null ? null : tradeRisk.trim();
	}
	
    /**
     * @return TradeRisk
     */	
	public String getTradeRisk() {
		return this.tradeRisk;
	}
	
	/**
	 * @param tradeRiskExpl
	 */
	public void setTradeRiskExpl(String tradeRiskExpl) {
		this.tradeRiskExpl = tradeRiskExpl == null ? null : tradeRiskExpl.trim();
	}
	
    /**
     * @return TradeRiskExpl
     */	
	public String getTradeRiskExpl() {
		return this.tradeRiskExpl;
	}
	
	/**
	 * @param shareholderRelaChange
	 */
	public void setShareholderRelaChange(String shareholderRelaChange) {
		this.shareholderRelaChange = shareholderRelaChange == null ? null : shareholderRelaChange.trim();
	}
	
    /**
     * @return ShareholderRelaChange
     */	
	public String getShareholderRelaChange() {
		return this.shareholderRelaChange;
	}
	
	/**
	 * @param relaChangeExpl
	 */
	public void setRelaChangeExpl(String relaChangeExpl) {
		this.relaChangeExpl = relaChangeExpl == null ? null : relaChangeExpl.trim();
	}
	
    /**
     * @return RelaChangeExpl
     */	
	public String getRelaChangeExpl() {
		return this.relaChangeExpl;
	}
	
	/**
	 * @param manaCase
	 */
	public void setManaCase(String manaCase) {
		this.manaCase = manaCase == null ? null : manaCase.trim();
	}
	
    /**
     * @return ManaCase
     */	
	public String getManaCase() {
		return this.manaCase;
	}
	
	/**
	 * @param manaCaseExpl
	 */
	public void setManaCaseExpl(String manaCaseExpl) {
		this.manaCaseExpl = manaCaseExpl == null ? null : manaCaseExpl.trim();
	}
	
    /**
     * @return ManaCaseExpl
     */	
	public String getManaCaseExpl() {
		return this.manaCaseExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}