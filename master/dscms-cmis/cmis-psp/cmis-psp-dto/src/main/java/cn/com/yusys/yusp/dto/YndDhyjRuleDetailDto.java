package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: YndDhyjRuleDetail
 * @类描述: ynd_dhyj_rule_detail数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 21:58:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class YndDhyjRuleDetailDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 贷后预警流水号 **/
	private String warnSerno;
	
	/** 预警规则 **/
	private String warnRule;
	
	/** 预警等级 **/
	private String warnResult;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param warnSerno
	 */
	public void setWarnSerno(String warnSerno) {
		this.warnSerno = warnSerno == null ? null : warnSerno.trim();
	}
	
    /**
     * @return WarnSerno
     */	
	public String getWarnSerno() {
		return this.warnSerno;
	}
	
	/**
	 * @param warnRule
	 */
	public void setWarnRule(String warnRule) {
		this.warnRule = warnRule == null ? null : warnRule.trim();
	}
	
    /**
     * @return WarnRule
     */	
	public String getWarnRule() {
		return this.warnRule;
	}
	
	/**
	 * @param warnResult
	 */
	public void setWarnResult(String warnResult) {
		this.warnResult = warnResult == null ? null : warnResult.trim();
	}
	
    /**
     * @return WarnResult
     */	
	public String getWarnResult() {
		return this.warnResult;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}