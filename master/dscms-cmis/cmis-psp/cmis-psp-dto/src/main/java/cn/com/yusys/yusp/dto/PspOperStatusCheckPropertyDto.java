package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckProperty
 * @类描述: psp_oper_status_check_property数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspOperStatusCheckPropertyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 贷款金额（元） **/
	private java.math.BigDecimal loanAmt;
	
	/** 本次贷后检查时的出租率 **/
	private java.math.BigDecimal loanBalance;
	
	/** 贷款余额（元） **/
	private java.math.BigDecimal rentRate;
	
	/** 年租金（万元） **/
	private java.math.BigDecimal rentAmtYear;
	
	/** 贷款期限 **/
	private Integer loanTerm;
	
	/** 我行账户当年回笼租金（万元） **/
	private java.math.BigDecimal rentAmtBack;
	
	/** 租金归集账户是否为我行账户 **/
	private String isAcct;
	
	/** 归集账户说明 **/
	private String acctExpl;
	
	/** 账号 **/
	private String acctNo;
	
	/** 账户名称 **/
	private String acctName;
	
	/** 账户序号 **/
	private String acctSeqNo;
	
	/** 开户行 **/
	private String acctb;
	
	/** 是否按我行还款计划按时足额还款 **/
	private String isRepay;
	
	/** 按时还款说明 **/
	private String repayExpl;
	
	/** 是否收集全部承租户承诺书 **/
	private String isSign;
	
	/** 承诺书收集说明 **/
	private String signExpl;
	
	/** 租户的租金是否按时支付 **/
	private String isDefrayOntime;
	
	/** 租金按时交付说明 **/
	private String defrayExpl;
	
	/** 租金是否能覆盖我行贷款本息 **/
	private String isCover;
	
	/** 本息覆盖说明 **/
	private String coverExpl;
	
	/** 租金来源是否与租户一致 **/
	private String isIdentical;
	
	/** 租金来源说明 **/
	private String identicalExpl;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return LoanBalance
     */	
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param rentRate
	 */
	public void setRentRate(java.math.BigDecimal rentRate) {
		this.rentRate = rentRate;
	}
	
    /**
     * @return RentRate
     */	
	public java.math.BigDecimal getRentRate() {
		return this.rentRate;
	}
	
	/**
	 * @param rentAmtYear
	 */
	public void setRentAmtYear(java.math.BigDecimal rentAmtYear) {
		this.rentAmtYear = rentAmtYear;
	}
	
    /**
     * @return RentAmtYear
     */	
	public java.math.BigDecimal getRentAmtYear() {
		return this.rentAmtYear;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(Integer loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return LoanTerm
     */	
	public Integer getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param rentAmtBack
	 */
	public void setRentAmtBack(java.math.BigDecimal rentAmtBack) {
		this.rentAmtBack = rentAmtBack;
	}
	
    /**
     * @return RentAmtBack
     */	
	public java.math.BigDecimal getRentAmtBack() {
		return this.rentAmtBack;
	}
	
	/**
	 * @param isAcct
	 */
	public void setIsAcct(String isAcct) {
		this.isAcct = isAcct == null ? null : isAcct.trim();
	}
	
    /**
     * @return IsAcct
     */	
	public String getIsAcct() {
		return this.isAcct;
	}
	
	/**
	 * @param acctExpl
	 */
	public void setAcctExpl(String acctExpl) {
		this.acctExpl = acctExpl == null ? null : acctExpl.trim();
	}
	
    /**
     * @return AcctExpl
     */	
	public String getAcctExpl() {
		return this.acctExpl;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo == null ? null : acctNo.trim();
	}
	
    /**
     * @return AcctNo
     */	
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName == null ? null : acctName.trim();
	}
	
    /**
     * @return AcctName
     */	
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param acctSeqNo
	 */
	public void setAcctSeqNo(String acctSeqNo) {
		this.acctSeqNo = acctSeqNo == null ? null : acctSeqNo.trim();
	}
	
    /**
     * @return AcctSeqNo
     */	
	public String getAcctSeqNo() {
		return this.acctSeqNo;
	}
	
	/**
	 * @param acctb
	 */
	public void setAcctb(String acctb) {
		this.acctb = acctb == null ? null : acctb.trim();
	}
	
    /**
     * @return Acctb
     */	
	public String getAcctb() {
		return this.acctb;
	}
	
	/**
	 * @param isRepay
	 */
	public void setIsRepay(String isRepay) {
		this.isRepay = isRepay == null ? null : isRepay.trim();
	}
	
    /**
     * @return IsRepay
     */	
	public String getIsRepay() {
		return this.isRepay;
	}
	
	/**
	 * @param repayExpl
	 */
	public void setRepayExpl(String repayExpl) {
		this.repayExpl = repayExpl == null ? null : repayExpl.trim();
	}
	
    /**
     * @return RepayExpl
     */	
	public String getRepayExpl() {
		return this.repayExpl;
	}
	
	/**
	 * @param isSign
	 */
	public void setIsSign(String isSign) {
		this.isSign = isSign == null ? null : isSign.trim();
	}
	
    /**
     * @return IsSign
     */	
	public String getIsSign() {
		return this.isSign;
	}
	
	/**
	 * @param signExpl
	 */
	public void setSignExpl(String signExpl) {
		this.signExpl = signExpl == null ? null : signExpl.trim();
	}
	
    /**
     * @return SignExpl
     */	
	public String getSignExpl() {
		return this.signExpl;
	}
	
	/**
	 * @param isDefrayOntime
	 */
	public void setIsDefrayOntime(String isDefrayOntime) {
		this.isDefrayOntime = isDefrayOntime == null ? null : isDefrayOntime.trim();
	}
	
    /**
     * @return IsDefrayOntime
     */	
	public String getIsDefrayOntime() {
		return this.isDefrayOntime;
	}
	
	/**
	 * @param defrayExpl
	 */
	public void setDefrayExpl(String defrayExpl) {
		this.defrayExpl = defrayExpl == null ? null : defrayExpl.trim();
	}
	
    /**
     * @return DefrayExpl
     */	
	public String getDefrayExpl() {
		return this.defrayExpl;
	}
	
	/**
	 * @param isCover
	 */
	public void setIsCover(String isCover) {
		this.isCover = isCover == null ? null : isCover.trim();
	}
	
    /**
     * @return IsCover
     */	
	public String getIsCover() {
		return this.isCover;
	}
	
	/**
	 * @param coverExpl
	 */
	public void setCoverExpl(String coverExpl) {
		this.coverExpl = coverExpl == null ? null : coverExpl.trim();
	}
	
    /**
     * @return CoverExpl
     */	
	public String getCoverExpl() {
		return this.coverExpl;
	}
	
	/**
	 * @param isIdentical
	 */
	public void setIsIdentical(String isIdentical) {
		this.isIdentical = isIdentical == null ? null : isIdentical.trim();
	}
	
    /**
     * @return IsIdentical
     */	
	public String getIsIdentical() {
		return this.isIdentical;
	}
	
	/**
	 * @param identicalExpl
	 */
	public void setIdenticalExpl(String identicalExpl) {
		this.identicalExpl = identicalExpl == null ? null : identicalExpl.trim();
	}
	
    /**
     * @return IdenticalExpl
     */	
	public String getIdenticalExpl() {
		return this.identicalExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}