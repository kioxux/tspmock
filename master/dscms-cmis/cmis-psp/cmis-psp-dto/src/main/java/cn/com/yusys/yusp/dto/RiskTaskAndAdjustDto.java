package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskTaskList
 * @类描述: risk_task_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-18 18:52:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskTaskAndAdjustDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 任务类型 **/
	private String taskType;
	
	/** 分类模型 **/
	private String checkType;
	
	/** 客户类型 **/
	private String cusCatalog;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 手工分类 **/
	private String manualClass;
	
	/** 机评分类 **/
	private String autoClass;
	
	/** 任务生成日期 **/
	private String taskStartDt;
	
	/** 要求完成日期 **/
	private String taskEndDt;
	
	/** 任务执行人 **/
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="execIdName")
	private String execId;
	
	/** 任务执行机构 **/
	@RedisCacheTranslator(redisCacheKey = "orgName" ,refFieldName="execBrIdName")
	private String execBrId;
	
	/** 任务状态 **/
	private String checkStatus;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 实际分类日期 **/
	private String checkDate;
	
	/** 上次分类结果 **/
	private String lastClassRst;
	
	/** 手工十级分类结果 **/
	private String manualTenClass;
	
	/** 自动十级分类结果 **/
	private String autoClassReason;
	
	/** 人工分类理由 **/
	private String manualClassReason;
	
	/** 风险分类得分 **/
	private Integer riskScore;
	
	/** 创建时间 **/
	private Date createTime;
	
	/** 修改时间 **/
	private Date updateTime;

	/** 业务流水号 **/
	private String serno;

	/** 调整前分类 **/
	private String origiClass;

	/** 调整后分类 **/
	private String appAdjClass;

	/** 申请调整理由 **/
	private String appAdjRemark;

	/** 调整前十级分类 **/
	private String origiTenClass;

	/** 调整后十级分类 **/
	private String appAdjTenClass;

	/** 登记人 **/
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="inputIdName")
	private String inputId;

	/** 登记机构 **/
	@RedisCacheTranslator(redisCacheKey = "orgName" ,refFieldName="inputBrIdName")
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}

	/**
	 * @return PkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}

	/**
	 * @return TaskNo
	 */
	public String getTaskNo() {
		return this.taskNo;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}

	/**
	 * @return Serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param origiClass
	 */
	public void setOrigiClass(String origiClass) {
		this.origiClass = origiClass == null ? null : origiClass.trim();
	}

	/**
	 * @return OrigiClass
	 */
	public String getOrigiClass() {
		return this.origiClass;
	}

	/**
	 * @param appAdjClass
	 */
	public void setAppAdjClass(String appAdjClass) {
		this.appAdjClass = appAdjClass == null ? null : appAdjClass.trim();
	}

	/**
	 * @return AppAdjClass
	 */
	public String getAppAdjClass() {
		return this.appAdjClass;
	}

	/**
	 * @param origiTenClass
	 */
	public void setOrigiTenClass(String origiTenClass) {
		this.origiTenClass = origiTenClass == null ? null : origiTenClass.trim();
	}

	/**
	 * @return OrigiTenClass
	 */
	public String getOrigiTenClass() {
		return this.origiTenClass;
	}

	/**
	 * @param appAdjTenClass
	 */
	public void setAppAdjTenClass(String appAdjTenClass) {
		this.appAdjTenClass = appAdjTenClass == null ? null : appAdjTenClass.trim();
	}

	/**
	 * @return AppAdjTenClass
	 */
	public String getAppAdjTenClass() {
		return this.appAdjTenClass;
	}

	/**
	 * @param appAdjRemark
	 */
	public void setAppAdjRemark(String appAdjRemark) {
		this.appAdjRemark = appAdjRemark == null ? null : appAdjRemark.trim();
	}

	/**
	 * @return AppAdjRemark
	 */
	public String getAppAdjRemark() {
		return this.appAdjRemark;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

	/**
	 * @return InputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

	/**
	 * @return InputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

	/**
	 * @return InputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}
	
	

	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType == null ? null : taskType.trim();
	}
	
    /**
     * @return TaskType
     */	
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param checkType
	 */
	public void setCheckType(String checkType) {
		this.checkType = checkType == null ? null : checkType.trim();
	}
	
    /**
     * @return CheckType
     */	
	public String getCheckType() {
		return this.checkType;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog == null ? null : cusCatalog.trim();
	}
	
    /**
     * @return CusCatalog
     */	
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param manualClass
	 */
	public void setManualClass(String manualClass) {
		this.manualClass = manualClass == null ? null : manualClass.trim();
	}
	
    /**
     * @return ManualClass
     */	
	public String getManualClass() {
		return this.manualClass;
	}
	
	/**
	 * @param autoClass
	 */
	public void setAutoClass(String autoClass) {
		this.autoClass = autoClass == null ? null : autoClass.trim();
	}
	
    /**
     * @return AutoClass
     */	
	public String getAutoClass() {
		return this.autoClass;
	}
	
	/**
	 * @param taskStartDt
	 */
	public void setTaskStartDt(String taskStartDt) {
		this.taskStartDt = taskStartDt == null ? null : taskStartDt.trim();
	}
	
    /**
     * @return TaskStartDt
     */	
	public String getTaskStartDt() {
		return this.taskStartDt;
	}
	
	/**
	 * @param taskEndDt
	 */
	public void setTaskEndDt(String taskEndDt) {
		this.taskEndDt = taskEndDt == null ? null : taskEndDt.trim();
	}
	
    /**
     * @return TaskEndDt
     */	
	public String getTaskEndDt() {
		return this.taskEndDt;
	}
	
	/**
	 * @param execId
	 */
	public void setExecId(String execId) {
		this.execId = execId == null ? null : execId.trim();
	}
	
    /**
     * @return ExecId
     */	
	public String getExecId() {
		return this.execId;
	}
	
	/**
	 * @param execBrId
	 */
	public void setExecBrId(String execBrId) {
		this.execBrId = execBrId == null ? null : execBrId.trim();
	}
	
    /**
     * @return ExecBrId
     */	
	public String getExecBrId() {
		return this.execBrId;
	}
	
	/**
	 * @param checkStatus
	 */
	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus == null ? null : checkStatus.trim();
	}
	
    /**
     * @return CheckStatus
     */	
	public String getCheckStatus() {
		return this.checkStatus;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param checkDate
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate == null ? null : checkDate.trim();
	}
	
    /**
     * @return CheckDate
     */	
	public String getCheckDate() {
		return this.checkDate;
	}
	
	/**
	 * @param lastClassRst
	 */
	public void setLastClassRst(String lastClassRst) {
		this.lastClassRst = lastClassRst == null ? null : lastClassRst.trim();
	}
	
    /**
     * @return LastClassRst
     */	
	public String getLastClassRst() {
		return this.lastClassRst;
	}
	
	/**
	 * @param manualTenClass
	 */
	public void setManualTenClass(String manualTenClass) {
		this.manualTenClass = manualTenClass == null ? null : manualTenClass.trim();
	}
	
    /**
     * @return ManualTenClass
     */	
	public String getManualTenClass() {
		return this.manualTenClass;
	}
	
	/**
	 * @param autoClassReason
	 */
	public void setAutoClassReason(String autoClassReason) {
		this.autoClassReason = autoClassReason == null ? null : autoClassReason.trim();
	}
	
    /**
     * @return AutoClassReason
     */	
	public String getAutoClassReason() {
		return this.autoClassReason;
	}
	
	/**
	 * @param manualClassReason
	 */
	public void setManualClassReason(String manualClassReason) {
		this.manualClassReason = manualClassReason == null ? null : manualClassReason.trim();
	}
	
    /**
     * @return ManualClassReason
     */	
	public String getManualClassReason() {
		return this.manualClassReason;
	}
	
	/**
	 * @param riskScore
	 */
	public void setRiskScore(Integer riskScore) {
		this.riskScore = riskScore;
	}
	
    /**
     * @return RiskScore
     */	
	public Integer getRiskScore() {
		return this.riskScore;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public Date getUpdateTime() {
		return this.updateTime;
	}


}