package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * psp_cus_base_case数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:23
 */
public class PspCusBaseCaseDto {

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 客户编号
     **/
    private String cusId;

    /**
     * 客户名称
     **/
    private String cusName;

    /**
     * 经营地址
     **/
    private String operAddrAct;

    /**
     * 股权结构
     **/
    private String ownshipStr;

    /**
     * 主营业务
     **/
    private String mainOptScp;

    /**
     * 成立日期
     **/
    private String buildDate;

    /**
     * 员工人数
     **/
    private Integer fjobNum;

    /**
     * 注册资本（万元）
     **/
    private BigDecimal regiCapAmt;

    /**
     * 实收资本（万元）
     **/
    private BigDecimal paidCapAmt;

    /**
     * 行业分类
     **/
    private String tradeClass;

    /**
     * 法人代表
     **/
    private String legalRepresent;

    /**
     * 实际控制人
     **/
    private String realOperCusName;

    /**
     * 企业性质
     **/
    private String corpQlty;

    /**
     * 内外部数据系统校验是否有变化
     **/
    private String isDataChg;

    /**
     * 校验变化说明
     **/
    private String dataChgExpl;

    /**
     * 房地产开发贷检查
     **/
    private String isRealpro;

    /**
     * 经营性物业贷检查
     **/
    private String isProperty;

    /**
     * 固定资产贷款、项目贷款检查
     **/
    private String isFixed;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    /**
     * 借款人身份证号
     **/
    private String certCode;

    /**
     * 借款人电话号码
     **/
    private String phone;

    /**
     * 工作单位名称
     **/
    private String workUnit;

    /**
     * 经营范围
     **/
    private String natBusi;

    /**
     * 风险分类
     **/
    private String riskClass;

    /**
     * 风险预警等级
     **/
    private String riskLevel;

    /**
     * 客户信用评级
     **/
    private String creditRank;

    /**
     * 具体说明
     **/
    private String creditRankRemark;

    /**
     * 频率修改信息
     **/
    private String freqModifyInfo;

    /**
     * 性别
     **/
    private String sex;

    /**
     * 婚姻状况
     **/
    private String marStatus;

    /**
     * 现居地址
     **/
    private String resiAddr;

    /**
     * 年收入
     **/
    private BigDecimal yearn;

    /**
     * 职务
     **/
    private String duty;

    /**
     * 单位具体地址
     **/
    private String unitAddr;

    /**
     * 单位电话
     **/
    private String unitPhone;

    /**
     * 配偶客户编号
     **/
    private String spouseCusId;

    /**
     * 配偶客户名称
     **/
    private String spouseCusName;

    /**
     * 配偶身份证号
     **/
    private String spouseCertCode;

    /**
     * 配偶联系电话
     **/
    private String spousePhone;

    /**
     * 配偶现居地址
     **/
    private String spouseResiAddr;

    /**
     * 配偶年收入
     **/
    private BigDecimal spouseYearn;

    /**
     * 配偶工作单位名称
     **/
    private String spouseWorkUnit;

    /**
     * 配偶职务
     **/
    private String spouseDuty;

    /**
     * 配偶单位具体地址
     **/
    private String spouseUnitAddr;

    /**
     * 配偶单位电话
     **/
    private String spouseUnitPhone;

    public PspCusBaseCaseDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getOperAddrAct() {
        return operAddrAct;
    }

    public void setOperAddrAct(String operAddrAct) {
        this.operAddrAct = operAddrAct;
    }

    public String getOwnshipStr() {
        return ownshipStr;
    }

    public void setOwnshipStr(String ownshipStr) {
        this.ownshipStr = ownshipStr;
    }

    public String getMainOptScp() {
        return mainOptScp;
    }

    public void setMainOptScp(String mainOptScp) {
        this.mainOptScp = mainOptScp;
    }

    public String getBuildDate() {
        return buildDate;
    }

    public void setBuildDate(String buildDate) {
        this.buildDate = buildDate;
    }

    public Integer getFjobNum() {
        return fjobNum;
    }

    public void setFjobNum(Integer fjobNum) {
        this.fjobNum = fjobNum;
    }

    public BigDecimal getRegiCapAmt() {
        return regiCapAmt;
    }

    public void setRegiCapAmt(BigDecimal regiCapAmt) {
        this.regiCapAmt = regiCapAmt;
    }

    public BigDecimal getPaidCapAmt() {
        return paidCapAmt;
    }

    public void setPaidCapAmt(BigDecimal paidCapAmt) {
        this.paidCapAmt = paidCapAmt;
    }

    public String getTradeClass() {
        return tradeClass;
    }

    public void setTradeClass(String tradeClass) {
        this.tradeClass = tradeClass;
    }

    public String getLegalRepresent() {
        return legalRepresent;
    }

    public void setLegalRepresent(String legalRepresent) {
        this.legalRepresent = legalRepresent;
    }

    public String getRealOperCusName() {
        return realOperCusName;
    }

    public void setRealOperCusName(String realOperCusName) {
        this.realOperCusName = realOperCusName;
    }

    public String getCorpQlty() {
        return corpQlty;
    }

    public void setCorpQlty(String corpQlty) {
        this.corpQlty = corpQlty;
    }

    public String getIsDataChg() {
        return isDataChg;
    }

    public void setIsDataChg(String isDataChg) {
        this.isDataChg = isDataChg;
    }

    public String getDataChgExpl() {
        return dataChgExpl;
    }

    public void setDataChgExpl(String dataChgExpl) {
        this.dataChgExpl = dataChgExpl;
    }

    public String getIsRealpro() {
        return isRealpro;
    }

    public void setIsRealpro(String isRealpro) {
        this.isRealpro = isRealpro;
    }

    public String getIsProperty() {
        return isProperty;
    }

    public void setIsProperty(String isProperty) {
        this.isProperty = isProperty;
    }

    public String getIsFixed() {
        return isFixed;
    }

    public void setIsFixed(String isFixed) {
        this.isFixed = isFixed;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkUnit() {
        return workUnit;
    }

    public void setWorkUnit(String workUnit) {
        this.workUnit = workUnit;
    }

    public String getNatBusi() {
        return natBusi;
    }

    public void setNatBusi(String natBusi) {
        this.natBusi = natBusi;
    }

    public String getRiskClass() {
        return riskClass;
    }

    public void setRiskClass(String riskClass) {
        this.riskClass = riskClass;
    }

    public String getRiskLevel() {
        return riskLevel;
    }

    public void setRiskLevel(String riskLevel) {
        this.riskLevel = riskLevel;
    }

    public String getCreditRank() {
        return creditRank;
    }

    public void setCreditRank(String creditRank) {
        this.creditRank = creditRank;
    }

    public String getCreditRankRemark() {
        return creditRankRemark;
    }

    public void setCreditRankRemark(String creditRankRemark) {
        this.creditRankRemark = creditRankRemark;
    }

    public String getFreqModifyInfo() {
        return freqModifyInfo;
    }

    public void setFreqModifyInfo(String freqModifyInfo) {
        this.freqModifyInfo = freqModifyInfo;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public String getResiAddr() {
        return resiAddr;
    }

    public void setResiAddr(String resiAddr) {
        this.resiAddr = resiAddr;
    }

    public BigDecimal getYearn() {
        return yearn;
    }

    public void setYearn(BigDecimal yearn) {
        this.yearn = yearn;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getUnitAddr() {
        return unitAddr;
    }

    public void setUnitAddr(String unitAddr) {
        this.unitAddr = unitAddr;
    }

    public String getUnitPhone() {
        return unitPhone;
    }

    public void setUnitPhone(String unitPhone) {
        this.unitPhone = unitPhone;
    }

    public String getSpouseCusId() {
        return spouseCusId;
    }

    public void setSpouseCusId(String spouseCusId) {
        this.spouseCusId = spouseCusId;
    }

    public String getSpouseCusName() {
        return spouseCusName;
    }

    public void setSpouseCusName(String spouseCusName) {
        this.spouseCusName = spouseCusName;
    }

    public String getSpouseCertCode() {
        return spouseCertCode;
    }

    public void setSpouseCertCode(String spouseCertCode) {
        this.spouseCertCode = spouseCertCode;
    }

    public String getSpousePhone() {
        return spousePhone;
    }

    public void setSpousePhone(String spousePhone) {
        this.spousePhone = spousePhone;
    }

    public String getSpouseResiAddr() {
        return spouseResiAddr;
    }

    public void setSpouseResiAddr(String spouseResiAddr) {
        this.spouseResiAddr = spouseResiAddr;
    }

    public BigDecimal getSpouseYearn() {
        return spouseYearn;
    }

    public void setSpouseYearn(BigDecimal spouseYearn) {
        this.spouseYearn = spouseYearn;
    }

    public String getSpouseWorkUnit() {
        return spouseWorkUnit;
    }

    public void setSpouseWorkUnit(String spouseWorkUnit) {
        this.spouseWorkUnit = spouseWorkUnit;
    }

    public String getSpouseDuty() {
        return spouseDuty;
    }

    public void setSpouseDuty(String spouseDuty) {
        this.spouseDuty = spouseDuty;
    }

    public String getSpouseUnitAddr() {
        return spouseUnitAddr;
    }

    public void setSpouseUnitAddr(String spouseUnitAddr) {
        this.spouseUnitAddr = spouseUnitAddr;
    }

    public String getSpouseUnitPhone() {
        return spouseUnitPhone;
    }

    public void setSpouseUnitPhone(String spouseUnitPhone) {
        this.spouseUnitPhone = spouseUnitPhone;
    }
}