package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskFinReportAnaly
 * @类描述: risk_fin_report_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskFinReportAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 指标编号 **/
	private String idxKey;
	
	/** 指标名称 **/
	private String idxName;
	
	/** 本期数据 **/
	private String curtValue;
	
	/** 上一年数据 **/
	private String l1yValue;
	
	/** 上两年数据 **/
	private String l2yValue;
	
	/** 比上一年增长 **/
	private String l1yRiseRate;
	
	/** 比上两年增长 **/
	private String l2yRiseRate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param idxKey
	 */
	public void setIdxKey(String idxKey) {
		this.idxKey = idxKey == null ? null : idxKey.trim();
	}
	
    /**
     * @return IdxKey
     */	
	public String getIdxKey() {
		return this.idxKey;
	}
	
	/**
	 * @param idxName
	 */
	public void setIdxName(String idxName) {
		this.idxName = idxName == null ? null : idxName.trim();
	}
	
    /**
     * @return IdxName
     */	
	public String getIdxName() {
		return this.idxName;
	}
	
	/**
	 * @param curtValue
	 */
	public void setCurtValue(String curtValue) {
		this.curtValue = curtValue == null ? null : curtValue.trim();
	}
	
    /**
     * @return CurtValue
     */	
	public String getCurtValue() {
		return this.curtValue;
	}
	
	/**
	 * @param l1yValue
	 */
	public void setL1yValue(String l1yValue) {
		this.l1yValue = l1yValue == null ? null : l1yValue.trim();
	}
	
    /**
     * @return L1yValue
     */	
	public String getL1yValue() {
		return this.l1yValue;
	}
	
	/**
	 * @param l2yValue
	 */
	public void setL2yValue(String l2yValue) {
		this.l2yValue = l2yValue == null ? null : l2yValue.trim();
	}
	
    /**
     * @return L2yValue
     */	
	public String getL2yValue() {
		return this.l2yValue;
	}
	
	/**
	 * @param l1yRiseRate
	 */
	public void setL1yRiseRate(String l1yRiseRate) {
		this.l1yRiseRate = l1yRiseRate == null ? null : l1yRiseRate.trim();
	}
	
    /**
     * @return L1yRiseRate
     */	
	public String getL1yRiseRate() {
		return this.l1yRiseRate;
	}
	
	/**
	 * @param l2yRiseRate
	 */
	public void setL2yRiseRate(String l2yRiseRate) {
		this.l2yRiseRate = l2yRiseRate == null ? null : l2yRiseRate.trim();
	}
	
    /**
     * @return L2yRiseRate
     */	
	public String getL2yRiseRate() {
		return this.l2yRiseRate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}