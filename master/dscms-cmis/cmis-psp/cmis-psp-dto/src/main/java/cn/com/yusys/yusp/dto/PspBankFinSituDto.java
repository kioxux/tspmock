package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * psp_bank_fin_situ数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:23
 */
public class PspBankFinSituDto {

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 融资品种
     **/
    private String finVariet;

    /**
     * 融资金额（元）
     **/
    private BigDecimal finAmt;

    /**
     * 融资余额（元）
     **/
    private BigDecimal finLmt;

    /**
     * 逾期本金（元）
     **/
    private BigDecimal overdueCapAmt;

    /**
     * 拖欠利息（元）
     **/
    private BigDecimal debitInt;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspBankFinSituDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getFinVariet() {
        return finVariet;
    }

    public void setFinVariet(String finVariet) {
        this.finVariet = finVariet;
    }

    public BigDecimal getFinAmt() {
        return finAmt;
    }

    public void setFinAmt(BigDecimal finAmt) {
        this.finAmt = finAmt;
    }

    public BigDecimal getFinLmt() {
        return finLmt;
    }

    public void setFinLmt(BigDecimal finLmt) {
        this.finLmt = finLmt;
    }

    public BigDecimal getOverdueCapAmt() {
        return overdueCapAmt;
    }

    public void setOverdueCapAmt(BigDecimal overdueCapAmt) {
        this.overdueCapAmt = overdueCapAmt;
    }

    public BigDecimal getDebitInt() {
        return debitInt;
    }

    public void setDebitInt(BigDecimal debitInt) {
        this.debitInt = debitInt;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}