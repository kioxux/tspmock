package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckArch
 * @类描述: psp_oper_status_check_arch数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspOperStatusCheckArchDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 工程款是否及时回笼 **/
	private String isCapBack;
	
	/** 工程款回笼说明 **/
	private String backExpl;
	
	/** 企业垫资是否严重 **/
	private String isMatEndowment;
	
	/** 企业垫资说明 **/
	private String matEndowmentExpl;
	
	/** 企业是否存在重大质量问题的工程 **/
	private String qualityProblem;
	
	/** 重大质量问题说明 **/
	private String problemExpl;
	
	/** 借款人当前承接的工程量与授信时相比是否下降 **/
	private String isDown;
	
	/** 工程量下降说明 **/
	private String downExpl;
	
	/** 主要原材料价格波动是否较大 **/
	private String priceChange;
	
	/** 原材料价格波动说明 **/
	private String changeExpl;
	
	/** 市政工程占比 **/
	private java.math.BigDecimal cityPerc;
	
	/** 房地产工程占比 **/
	private java.math.BigDecimal housePerc;
	
	/** 其他工程占比 **/
	private java.math.BigDecimal otherPerc;
	
	/** 上期工程主要地点 **/
	private String preRst;
	
	/** 本期工程主要地点 **/
	private String curtRst;
	
	/** 工程主要地点变化说明 **/
	private String remark;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isCapBack
	 */
	public void setIsCapBack(String isCapBack) {
		this.isCapBack = isCapBack == null ? null : isCapBack.trim();
	}
	
    /**
     * @return IsCapBack
     */	
	public String getIsCapBack() {
		return this.isCapBack;
	}
	
	/**
	 * @param backExpl
	 */
	public void setBackExpl(String backExpl) {
		this.backExpl = backExpl == null ? null : backExpl.trim();
	}
	
    /**
     * @return BackExpl
     */	
	public String getBackExpl() {
		return this.backExpl;
	}
	
	/**
	 * @param isMatEndowment
	 */
	public void setIsMatEndowment(String isMatEndowment) {
		this.isMatEndowment = isMatEndowment == null ? null : isMatEndowment.trim();
	}
	
    /**
     * @return IsMatEndowment
     */	
	public String getIsMatEndowment() {
		return this.isMatEndowment;
	}
	
	/**
	 * @param matEndowmentExpl
	 */
	public void setMatEndowmentExpl(String matEndowmentExpl) {
		this.matEndowmentExpl = matEndowmentExpl == null ? null : matEndowmentExpl.trim();
	}
	
    /**
     * @return MatEndowmentExpl
     */	
	public String getMatEndowmentExpl() {
		return this.matEndowmentExpl;
	}
	
	/**
	 * @param qualityProblem
	 */
	public void setQualityProblem(String qualityProblem) {
		this.qualityProblem = qualityProblem == null ? null : qualityProblem.trim();
	}
	
    /**
     * @return QualityProblem
     */	
	public String getQualityProblem() {
		return this.qualityProblem;
	}
	
	/**
	 * @param problemExpl
	 */
	public void setProblemExpl(String problemExpl) {
		this.problemExpl = problemExpl == null ? null : problemExpl.trim();
	}
	
    /**
     * @return ProblemExpl
     */	
	public String getProblemExpl() {
		return this.problemExpl;
	}
	
	/**
	 * @param isDown
	 */
	public void setIsDown(String isDown) {
		this.isDown = isDown == null ? null : isDown.trim();
	}
	
    /**
     * @return IsDown
     */	
	public String getIsDown() {
		return this.isDown;
	}
	
	/**
	 * @param downExpl
	 */
	public void setDownExpl(String downExpl) {
		this.downExpl = downExpl == null ? null : downExpl.trim();
	}
	
    /**
     * @return DownExpl
     */	
	public String getDownExpl() {
		return this.downExpl;
	}
	
	/**
	 * @param priceChange
	 */
	public void setPriceChange(String priceChange) {
		this.priceChange = priceChange == null ? null : priceChange.trim();
	}
	
    /**
     * @return PriceChange
     */	
	public String getPriceChange() {
		return this.priceChange;
	}
	
	/**
	 * @param changeExpl
	 */
	public void setChangeExpl(String changeExpl) {
		this.changeExpl = changeExpl == null ? null : changeExpl.trim();
	}
	
    /**
     * @return ChangeExpl
     */	
	public String getChangeExpl() {
		return this.changeExpl;
	}
	
	/**
	 * @param cityPerc
	 */
	public void setCityPerc(java.math.BigDecimal cityPerc) {
		this.cityPerc = cityPerc;
	}
	
    /**
     * @return CityPerc
     */	
	public java.math.BigDecimal getCityPerc() {
		return this.cityPerc;
	}
	
	/**
	 * @param housePerc
	 */
	public void setHousePerc(java.math.BigDecimal housePerc) {
		this.housePerc = housePerc;
	}
	
    /**
     * @return HousePerc
     */	
	public java.math.BigDecimal getHousePerc() {
		return this.housePerc;
	}
	
	/**
	 * @param otherPerc
	 */
	public void setOtherPerc(java.math.BigDecimal otherPerc) {
		this.otherPerc = otherPerc;
	}
	
    /**
     * @return OtherPerc
     */	
	public java.math.BigDecimal getOtherPerc() {
		return this.otherPerc;
	}
	
	/**
	 * @param preRst
	 */
	public void setPreRst(String preRst) {
		this.preRst = preRst == null ? null : preRst.trim();
	}
	
    /**
     * @return PreRst
     */	
	public String getPreRst() {
		return this.preRst;
	}
	
	/**
	 * @param curtRst
	 */
	public void setCurtRst(String curtRst) {
		this.curtRst = curtRst == null ? null : curtRst.trim();
	}
	
    /**
     * @return CurtRst
     */	
	public String getCurtRst() {
		return this.curtRst;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}