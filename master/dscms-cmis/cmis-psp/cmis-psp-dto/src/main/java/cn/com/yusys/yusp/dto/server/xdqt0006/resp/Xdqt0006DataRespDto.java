package cn.com.yusys.yusp.dto.server.xdqt0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：当天跑P状态查询
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdqt0006DataRespDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "progress")
    private String progress;//跑P状态

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    @Override
    public String toString() {
        return "Xdqt0006DataRespDto{" +
                "progress='" + progress + '\'' +
                '}';
    }
}
