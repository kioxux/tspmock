package cn.com.yusys.yusp.dto;

import java.util.Date;


/**
 * psp_check_info数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-29 00:10:44
 */
public class PspCheckInfoDto {

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 支付方式
     **/
    private String defrayMode;

    /**
     * 受托对象
     **/
    private String entrustedCusName;

    /**
     * 工作单位是否变化
     **/
    private String isChangeUnit;

    /**
     * 现工作单位
     **/
    private String nowUnit;

    /**
     * 家庭住址是否变化
     **/
    private String isChangeAddr;

    /**
     * 现家庭住址
     **/
    private String nowAddr;

    /**
     * 婚姻状况是否变化
     **/
    private String isChangeMar;

    /**
     * 婚姻状况说明
     **/
    private String marRemark;

    /**
     * 联系方式是否变化
     **/
    private String isChangePhone;

    /**
     * 现联系方式
     **/
    private String nowRemark;

    /**
     * 收入及负债是否变化
     **/
    private String isChangeEarning;

    /**
     * 现收入负债情况
     **/
    private String earningRemark;

    /**
     * 担保情况是否变化
     **/
    private String isChangeGuar;

    /**
     * 现担保情况
     **/
    private String guarRemark;

    /**
     * 贷款半年逾期次数
     **/
    private Integer overdueTimes;

    /**
     * 逾期原因
     **/
    private String overdueResn;

    /**
     * 贷后方式
     **/
    private String checkMode;

    /**
     * 贷后方式说明
     **/
    private String checkModeRemark;

    /**
     * 拟采取措施
     **/
    private String checkMeasure;

    /**
     * 其他措施
     **/
    private String checkMeasureRemark;

    /**
     * 超期未办理抵押原因
     **/
    private String overdueReason;

    /**
     * 其他超期未办理抵押原因
     **/
    private String otherOverdueReason;

    /**
     * 超期未抵押拟采取措施
     **/
    private String overdueMeasure;

    /**
     * 其他超期未抵押措施
     **/
    private String otherMeasureRemark;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspCheckInfoDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getDefrayMode() {
        return defrayMode;
    }

    public void setDefrayMode(String defrayMode) {
        this.defrayMode = defrayMode;
    }

    public String getEntrustedCusName() {
        return entrustedCusName;
    }

    public void setEntrustedCusName(String entrustedCusName) {
        this.entrustedCusName = entrustedCusName;
    }

    public String getIsChangeUnit() {
        return isChangeUnit;
    }

    public void setIsChangeUnit(String isChangeUnit) {
        this.isChangeUnit = isChangeUnit;
    }

    public String getNowUnit() {
        return nowUnit;
    }

    public void setNowUnit(String nowUnit) {
        this.nowUnit = nowUnit;
    }

    public String getIsChangeAddr() {
        return isChangeAddr;
    }

    public void setIsChangeAddr(String isChangeAddr) {
        this.isChangeAddr = isChangeAddr;
    }

    public String getNowAddr() {
        return nowAddr;
    }

    public void setNowAddr(String nowAddr) {
        this.nowAddr = nowAddr;
    }

    public String getIsChangeMar() {
        return isChangeMar;
    }

    public void setIsChangeMar(String isChangeMar) {
        this.isChangeMar = isChangeMar;
    }

    public String getMarRemark() {
        return marRemark;
    }

    public void setMarRemark(String marRemark) {
        this.marRemark = marRemark;
    }

    public String getIsChangePhone() {
        return isChangePhone;
    }

    public void setIsChangePhone(String isChangePhone) {
        this.isChangePhone = isChangePhone;
    }

    public String getNowRemark() {
        return nowRemark;
    }

    public void setNowRemark(String nowRemark) {
        this.nowRemark = nowRemark;
    }

    public String getIsChangeEarning() {
        return isChangeEarning;
    }

    public void setIsChangeEarning(String isChangeEarning) {
        this.isChangeEarning = isChangeEarning;
    }

    public String getEarningRemark() {
        return earningRemark;
    }

    public void setEarningRemark(String earningRemark) {
        this.earningRemark = earningRemark;
    }

    public String getIsChangeGuar() {
        return isChangeGuar;
    }

    public void setIsChangeGuar(String isChangeGuar) {
        this.isChangeGuar = isChangeGuar;
    }

    public String getGuarRemark() {
        return guarRemark;
    }

    public void setGuarRemark(String guarRemark) {
        this.guarRemark = guarRemark;
    }

    public Integer getOverdueTimes() {
        return overdueTimes;
    }

    public void setOverdueTimes(Integer overdueTimes) {
        this.overdueTimes = overdueTimes;
    }

    public String getOverdueResn() {
        return overdueResn;
    }

    public void setOverdueResn(String overdueResn) {
        this.overdueResn = overdueResn;
    }

    public String getCheckMode() {
        return checkMode;
    }

    public void setCheckMode(String checkMode) {
        this.checkMode = checkMode;
    }

    public String getCheckModeRemark() {
        return checkModeRemark;
    }

    public void setCheckModeRemark(String checkModeRemark) {
        this.checkModeRemark = checkModeRemark;
    }

    public String getCheckMeasure() {
        return checkMeasure;
    }

    public void setCheckMeasure(String checkMeasure) {
        this.checkMeasure = checkMeasure;
    }

    public String getCheckMeasureRemark() {
        return checkMeasureRemark;
    }

    public void setCheckMeasureRemark(String checkMeasureRemark) {
        this.checkMeasureRemark = checkMeasureRemark;
    }

    public String getOverdueReason() {
        return overdueReason;
    }

    public void setOverdueReason(String overdueReason) {
        this.overdueReason = overdueReason;
    }

    public String getOtherOverdueReason() {
        return otherOverdueReason;
    }

    public void setOtherOverdueReason(String otherOverdueReason) {
        this.otherOverdueReason = otherOverdueReason;
    }

    public String getOverdueMeasure() {
        return overdueMeasure;
    }

    public void setOverdueMeasure(String overdueMeasure) {
        this.overdueMeasure = overdueMeasure;
    }

    public String getOtherMeasureRemark() {
        return otherMeasureRemark;
    }

    public void setOtherMeasureRemark(String otherMeasureRemark) {
        this.otherMeasureRemark = otherMeasureRemark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}