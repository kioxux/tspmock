package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskDebitInfo
 * @类描述: risk_debit_info数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-18 02:45:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskDebitAndLoanDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 借据金额（元） **/
	private java.math.BigDecimal loanAmt;
	
	/** 借据余额（元） **/
	private java.math.BigDecimal loanBalance;
	
	/** 拖欠利息（元） **/
	private java.math.BigDecimal debitInt;
	
	/** 贷款起始日 **/
	private String loanStartDate;
	
	/** 贷款到期日 **/
	private String loanEndDate;
	
	/** 担保方式 **/
	private String guarMode;
	
	/** 执行年利率 **/
	private java.math.BigDecimal execRateYear;
	
	/** 五级分类 **/
	private String fiveClass;
	
	/** 上次分类结果 **/
	private String lastClassRst;
	
	/** 分析状态 **/
	private String analyStatus;
	
	/** 是否低风险 **/
	private String isLowRisk;
	
	/** 贷款类型 **/
	private String loanType;
	
	/** 风险分类日期 **/
	private String riskDate;
	
	/** 创建时间 **/
	private Date createTime;
	
	/** 修改时间 **/
	private Date updateTime;

	/** 授信发生后用途是否正常 **/
	private String isNormalAftLmt;

	/** 实际借款用途 **/
	private String factLoanUs;

	/** 是否重组贷款 **/
	private String isRecLoan;

	/** 是否展期 **/
	private String isExt;

	/** 是否借新还旧 **/
	private String isRefinance;

	/** 保全状态 **/
	private String preserveState;

	/** 保全情况说明 **/
	private String preserveCaseDesc;

	/** 有无影响该笔授信偿还的不利因素 **/
	private String isNegFactor;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return LoanBalance
     */	
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param debitInt
	 */
	public void setDebitInt(java.math.BigDecimal debitInt) {
		this.debitInt = debitInt;
	}
	
    /**
     * @return DebitInt
     */	
	public java.math.BigDecimal getDebitInt() {
		return this.debitInt;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate == null ? null : loanStartDate.trim();
	}
	
    /**
     * @return LoanStartDate
     */	
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate == null ? null : loanEndDate.trim();
	}
	
    /**
     * @return LoanEndDate
     */	
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return ExecRateYear
     */	
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass == null ? null : fiveClass.trim();
	}
	
    /**
     * @return FiveClass
     */	
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param lastClassRst
	 */
	public void setLastClassRst(String lastClassRst) {
		this.lastClassRst = lastClassRst == null ? null : lastClassRst.trim();
	}
	
    /**
     * @return LastClassRst
     */	
	public String getLastClassRst() {
		return this.lastClassRst;
	}
	
	/**
	 * @param analyStatus
	 */
	public void setAnalyStatus(String analyStatus) {
		this.analyStatus = analyStatus == null ? null : analyStatus.trim();
	}
	
    /**
     * @return AnalyStatus
     */	
	public String getAnalyStatus() {
		return this.analyStatus;
	}
	
	/**
	 * @param isLowRisk
	 */
	public void setIsLowRisk(String isLowRisk) {
		this.isLowRisk = isLowRisk == null ? null : isLowRisk.trim();
	}
	
    /**
     * @return IsLowRisk
     */	
	public String getIsLowRisk() {
		return this.isLowRisk;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType == null ? null : loanType.trim();
	}
	
    /**
     * @return LoanType
     */	
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param riskDate
	 */
	public void setRiskDate(String riskDate) {
		this.riskDate = riskDate == null ? null : riskDate.trim();
	}
	
    /**
     * @return RiskDate
     */	
	public String getRiskDate() {
		return this.riskDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param isNormalAftLmt
	 */
	public void setIsNormalAftLmt(String isNormalAftLmt) {
		this.isNormalAftLmt = isNormalAftLmt == null ? null : isNormalAftLmt.trim();
	}

	/**
	 * @return IsNormalAftLmt
	 */
	public String getIsNormalAftLmt() {
		return this.isNormalAftLmt;
	}

	/**
	 * @param factLoanUs
	 */
	public void setFactLoanUs(String factLoanUs) {
		this.factLoanUs = factLoanUs == null ? null : factLoanUs.trim();
	}

	/**
	 * @return FactLoanUs
	 */
	public String getFactLoanUs() {
		return this.factLoanUs;
	}

	/**
	 * @param isRecLoan
	 */
	public void setIsRecLoan(String isRecLoan) {
		this.isRecLoan = isRecLoan == null ? null : isRecLoan.trim();
	}

	/**
	 * @return IsRecLoan
	 */
	public String getIsRecLoan() {
		return this.isRecLoan;
	}

	/**
	 * @param isExt
	 */
	public void setIsExt(String isExt) {
		this.isExt = isExt == null ? null : isExt.trim();
	}

	/**
	 * @return IsExt
	 */
	public String getIsExt() {
		return this.isExt;
	}

	/**
	 * @param isRefinance
	 */
	public void setIsRefinance(String isRefinance) {
		this.isRefinance = isRefinance == null ? null : isRefinance.trim();
	}

	/**
	 * @return IsRefinance
	 */
	public String getIsRefinance() {
		return this.isRefinance;
	}

	/**
	 * @param preserveState
	 */
	public void setPreserveState(String preserveState) {
		this.preserveState = preserveState == null ? null : preserveState.trim();
	}

	/**
	 * @return PreserveState
	 */
	public String getPreserveState() {
		return this.preserveState;
	}

	/**
	 * @param preserveCaseDesc
	 */
	public void setPreserveCaseDesc(String preserveCaseDesc) {
		this.preserveCaseDesc = preserveCaseDesc == null ? null : preserveCaseDesc.trim();
	}

	/**
	 * @return PreserveCaseDesc
	 */
	public String getPreserveCaseDesc() {
		return this.preserveCaseDesc;
	}

	/**
	 * @param isNegFactor
	 */
	public void setIsNegFactor(String isNegFactor) {
		this.isNegFactor = isNegFactor == null ? null : isNegFactor.trim();
	}

	/**
	 * @return IsNegFactor
	 */
	public String getIsNegFactor() {
		return this.isNegFactor;
	}


}