package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspLegalCheck
 * @类描述: psp_legal_check数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspLegalCheckDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 重要法律文本是否合法、有效 **/
	private String isVld;
	
	/** 主从合同能否衔接，无法律瑕疵 **/
	private String isLnk;
	
	/** 重要法律文本是否具有时效性 **/
	private String isTimeEffect;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isVld
	 */
	public void setIsVld(String isVld) {
		this.isVld = isVld == null ? null : isVld.trim();
	}
	
    /**
     * @return IsVld
     */	
	public String getIsVld() {
		return this.isVld;
	}
	
	/**
	 * @param isLnk
	 */
	public void setIsLnk(String isLnk) {
		this.isLnk = isLnk == null ? null : isLnk.trim();
	}
	
    /**
     * @return IsLnk
     */	
	public String getIsLnk() {
		return this.isLnk;
	}
	
	/**
	 * @param isTimeEffect
	 */
	public void setIsTimeEffect(String isTimeEffect) {
		this.isTimeEffect = isTimeEffect == null ? null : isTimeEffect.trim();
	}
	
    /**
     * @return IsTimeEffect
     */	
	public String getIsTimeEffect() {
		return this.isTimeEffect;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}