package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspRelCorp
 * @类描述: psp_rel_corp数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspRelCorpDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 成员客户编号 **/
	private String memberCusNo;
	
	/** 成员客户名称 **/
	private String memberCusName;
	
	/** 关联（集团）关联关系类型 **/
	private String relType;
	
	/** 主办客户经理 **/
	private String managerId;
	
	/** 主办行 **/
	private String managerBrId;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param memberCusNo
	 */
	public void setMemberCusNo(String memberCusNo) {
		this.memberCusNo = memberCusNo == null ? null : memberCusNo.trim();
	}
	
    /**
     * @return MemberCusNo
     */	
	public String getMemberCusNo() {
		return this.memberCusNo;
	}
	
	/**
	 * @param memberCusName
	 */
	public void setMemberCusName(String memberCusName) {
		this.memberCusName = memberCusName == null ? null : memberCusName.trim();
	}
	
    /**
     * @return MemberCusName
     */	
	public String getMemberCusName() {
		return this.memberCusName;
	}
	
	/**
	 * @param relType
	 */
	public void setRelType(String relType) {
		this.relType = relType == null ? null : relType.trim();
	}
	
    /**
     * @return RelType
     */	
	public String getRelType() {
		return this.relType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}