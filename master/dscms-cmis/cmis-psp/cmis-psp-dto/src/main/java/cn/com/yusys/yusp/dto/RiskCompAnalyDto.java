package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskCompAnaly
 * @类描述: risk_comp_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskCompAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 综合分析 **/
	private String inteAnaly;
	
	/** 影响偿还的各类风险因素 **/
	private String riskResn;
	
	/** 防范风险的具体措施 **/
	private String riskMode;
	
	/** 风险防范化解措施的落实情况 **/
	private String actPact;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param inteAnaly
	 */
	public void setInteAnaly(String inteAnaly) {
		this.inteAnaly = inteAnaly == null ? null : inteAnaly.trim();
	}
	
    /**
     * @return InteAnaly
     */	
	public String getInteAnaly() {
		return this.inteAnaly;
	}
	
	/**
	 * @param riskResn
	 */
	public void setRiskResn(String riskResn) {
		this.riskResn = riskResn == null ? null : riskResn.trim();
	}
	
    /**
     * @return RiskResn
     */	
	public String getRiskResn() {
		return this.riskResn;
	}

	public String getRiskMode() {
		return riskMode;
	}

	public void setRiskMode(String riskMode) {
		this.riskMode = riskMode;
	}

	/**
	 * @param actPact
	 */
	public void setActPact(String actPact) {
		this.actPact = actPact == null ? null : actPact.trim();
	}
	
    /**
     * @return ActPact
     */	
	public String getActPact() {
		return this.actPact;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}