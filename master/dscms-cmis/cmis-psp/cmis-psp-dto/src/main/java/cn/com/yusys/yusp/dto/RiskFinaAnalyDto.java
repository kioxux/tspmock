package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskFinaAnaly
 * @类描述: risk_fina_analy数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskFinaAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 客户现金流量分析 **/
	private String cashAnaly;
	
	/** 现金流量分析说明 **/
	private String cashAnalyExpl;
	
	/** 客户财务状况分析 **/
	private String finaAnaly;
	
	/** 财务状况说明 **/
	private String finaAnalyExpl;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param cashAnaly
	 */
	public void setCashAnaly(String cashAnaly) {
		this.cashAnaly = cashAnaly == null ? null : cashAnaly.trim();
	}
	
    /**
     * @return CashAnaly
     */	
	public String getCashAnaly() {
		return this.cashAnaly;
	}
	
	/**
	 * @param cashAnalyExpl
	 */
	public void setCashAnalyExpl(String cashAnalyExpl) {
		this.cashAnalyExpl = cashAnalyExpl == null ? null : cashAnalyExpl.trim();
	}
	
    /**
     * @return CashAnalyExpl
     */	
	public String getCashAnalyExpl() {
		return this.cashAnalyExpl;
	}
	
	/**
	 * @param finaAnaly
	 */
	public void setFinaAnaly(String finaAnaly) {
		this.finaAnaly = finaAnaly == null ? null : finaAnaly.trim();
	}
	
    /**
     * @return FinaAnaly
     */	
	public String getFinaAnaly() {
		return this.finaAnaly;
	}
	
	/**
	 * @param finaAnalyExpl
	 */
	public void setFinaAnalyExpl(String finaAnalyExpl) {
		this.finaAnalyExpl = finaAnalyExpl == null ? null : finaAnalyExpl.trim();
	}
	
    /**
     * @return FinaAnalyExpl
     */	
	public String getFinaAnalyExpl() {
		return this.finaAnalyExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}