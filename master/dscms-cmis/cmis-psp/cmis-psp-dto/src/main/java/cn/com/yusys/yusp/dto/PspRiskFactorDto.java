package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspRiskFactor
 * @类描述: psp_risk_factor数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspRiskFactorDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 账户是否查封 **/
	private String isAcctAttachment;
	
	/** 查封原因 **/
	private String acctAttachmentRemark;
	
	/** 是否已向总行提交人工风险预警 **/
	private String isSubmitManualAlt;
	
	/** 提交时间 **/
	private String submitDate;
	
	/** 涉诉情况 **/
	private String lawsuitSitu;
	
	/** 不良成因 **/
	private String badReason;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isAcctAttachment
	 */
	public void setIsAcctAttachment(String isAcctAttachment) {
		this.isAcctAttachment = isAcctAttachment == null ? null : isAcctAttachment.trim();
	}
	
    /**
     * @return IsAcctAttachment
     */	
	public String getIsAcctAttachment() {
		return this.isAcctAttachment;
	}
	
	/**
	 * @param acctAttachmentRemark
	 */
	public void setAcctAttachmentRemark(String acctAttachmentRemark) {
		this.acctAttachmentRemark = acctAttachmentRemark == null ? null : acctAttachmentRemark.trim();
	}
	
    /**
     * @return AcctAttachmentRemark
     */	
	public String getAcctAttachmentRemark() {
		return this.acctAttachmentRemark;
	}
	
	/**
	 * @param isSubmitManualAlt
	 */
	public void setIsSubmitManualAlt(String isSubmitManualAlt) {
		this.isSubmitManualAlt = isSubmitManualAlt == null ? null : isSubmitManualAlt.trim();
	}
	
    /**
     * @return IsSubmitManualAlt
     */	
	public String getIsSubmitManualAlt() {
		return this.isSubmitManualAlt;
	}
	
	/**
	 * @param submitDate
	 */
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate == null ? null : submitDate.trim();
	}
	
    /**
     * @return SubmitDate
     */	
	public String getSubmitDate() {
		return this.submitDate;
	}
	
	/**
	 * @param lawsuitSitu
	 */
	public void setLawsuitSitu(String lawsuitSitu) {
		this.lawsuitSitu = lawsuitSitu == null ? null : lawsuitSitu.trim();
	}
	
    /**
     * @return LawsuitSitu
     */	
	public String getLawsuitSitu() {
		return this.lawsuitSitu;
	}
	
	/**
	 * @param badReason
	 */
	public void setBadReason(String badReason) {
		this.badReason = badReason == null ? null : badReason.trim();
	}
	
    /**
     * @return BadReason
     */	
	public String getBadReason() {
		return this.badReason;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}