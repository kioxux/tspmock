package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspOperStatusCheckManufacture
 * @类描述: psp_oper_status_check_manufacture数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspOperStatusCheckManufactureDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 库存是否积压 **/
	private String isOverstockedProducts;
	
	/** 库存积压说明 **/
	private String overstockedExpl;
	
	/** 订单量是否下降 **/
	private String isOrderDowen;
	
	/** 订单量下降说明 **/
	private String dowenExpl;
	
	/** 产量是否下降 **/
	private String isOutputDowen;
	
	/** 产量下降说明 **/
	private String outputDowenExpl;
	
	/** 是否新增对外投资和固定资产 **/
	private String isAddInvest;
	
	/** 新增投资说明 **/
	private String addInvestExpl;
	
	/** 借款人融资与销售是否匹配 **/
	private String isFinMatching;
	
	/** 融资销售匹配说明 **/
	private String matchingExpl;
	
	/** 借款人上期开台率情况 **/
	private java.math.BigDecimal preRst;
	
	/** 借款人本期开台率情况 **/
	private java.math.BigDecimal curtRst;
	
	/** 借款人开台率情况说明 **/
	private String remark;
	
	/** 借款人上期水费（万元） **/
	private java.math.BigDecimal preRst1;
	
	/** 借款人本期水费（万元） **/
	private java.math.BigDecimal curtRst1;
	
	/** 借款人水费说明 **/
	private String remark1;
	
	/** 借款人上期电费（万元） **/
	private java.math.BigDecimal preRst2;
	
	/** 借款人本期电费（万元） **/
	private java.math.BigDecimal curtRst2;
	
	/** 借款人电费说明 **/
	private String remark2;
	
	/** 借款人上期气费（万元） **/
	private java.math.BigDecimal preRst3;
	
	/** 借款人本期气费（万元） **/
	private java.math.BigDecimal curtRst3;
	
	/** 借款人气费说明 **/
	private String remark3;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param isOverstockedProducts
	 */
	public void setIsOverstockedProducts(String isOverstockedProducts) {
		this.isOverstockedProducts = isOverstockedProducts == null ? null : isOverstockedProducts.trim();
	}
	
    /**
     * @return IsOverstockedProducts
     */	
	public String getIsOverstockedProducts() {
		return this.isOverstockedProducts;
	}
	
	/**
	 * @param overstockedExpl
	 */
	public void setOverstockedExpl(String overstockedExpl) {
		this.overstockedExpl = overstockedExpl == null ? null : overstockedExpl.trim();
	}
	
    /**
     * @return OverstockedExpl
     */	
	public String getOverstockedExpl() {
		return this.overstockedExpl;
	}
	
	/**
	 * @param isOrderDowen
	 */
	public void setIsOrderDowen(String isOrderDowen) {
		this.isOrderDowen = isOrderDowen == null ? null : isOrderDowen.trim();
	}
	
    /**
     * @return IsOrderDowen
     */	
	public String getIsOrderDowen() {
		return this.isOrderDowen;
	}
	
	/**
	 * @param dowenExpl
	 */
	public void setDowenExpl(String dowenExpl) {
		this.dowenExpl = dowenExpl == null ? null : dowenExpl.trim();
	}
	
    /**
     * @return DowenExpl
     */	
	public String getDowenExpl() {
		return this.dowenExpl;
	}
	
	/**
	 * @param isOutputDowen
	 */
	public void setIsOutputDowen(String isOutputDowen) {
		this.isOutputDowen = isOutputDowen == null ? null : isOutputDowen.trim();
	}
	
    /**
     * @return IsOutputDowen
     */	
	public String getIsOutputDowen() {
		return this.isOutputDowen;
	}
	
	/**
	 * @param outputDowenExpl
	 */
	public void setOutputDowenExpl(String outputDowenExpl) {
		this.outputDowenExpl = outputDowenExpl == null ? null : outputDowenExpl.trim();
	}
	
    /**
     * @return OutputDowenExpl
     */	
	public String getOutputDowenExpl() {
		return this.outputDowenExpl;
	}
	
	/**
	 * @param isAddInvest
	 */
	public void setIsAddInvest(String isAddInvest) {
		this.isAddInvest = isAddInvest == null ? null : isAddInvest.trim();
	}
	
    /**
     * @return IsAddInvest
     */	
	public String getIsAddInvest() {
		return this.isAddInvest;
	}
	
	/**
	 * @param addInvestExpl
	 */
	public void setAddInvestExpl(String addInvestExpl) {
		this.addInvestExpl = addInvestExpl == null ? null : addInvestExpl.trim();
	}
	
    /**
     * @return AddInvestExpl
     */	
	public String getAddInvestExpl() {
		return this.addInvestExpl;
	}
	
	/**
	 * @param isFinMatching
	 */
	public void setIsFinMatching(String isFinMatching) {
		this.isFinMatching = isFinMatching == null ? null : isFinMatching.trim();
	}
	
    /**
     * @return IsFinMatching
     */	
	public String getIsFinMatching() {
		return this.isFinMatching;
	}
	
	/**
	 * @param matchingExpl
	 */
	public void setMatchingExpl(String matchingExpl) {
		this.matchingExpl = matchingExpl == null ? null : matchingExpl.trim();
	}
	
    /**
     * @return MatchingExpl
     */	
	public String getMatchingExpl() {
		return this.matchingExpl;
	}
	
	/**
	 * @param preRst
	 */
	public void setPreRst(java.math.BigDecimal preRst) {
		this.preRst = preRst;
	}
	
    /**
     * @return PreRst
     */	
	public java.math.BigDecimal getPreRst() {
		return this.preRst;
	}
	
	/**
	 * @param curtRst
	 */
	public void setCurtRst(java.math.BigDecimal curtRst) {
		this.curtRst = curtRst;
	}
	
    /**
     * @return CurtRst
     */	
	public java.math.BigDecimal getCurtRst() {
		return this.curtRst;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param preRst1
	 */
	public void setPreRst1(java.math.BigDecimal preRst1) {
		this.preRst1 = preRst1;
	}
	
    /**
     * @return PreRst1
     */	
	public java.math.BigDecimal getPreRst1() {
		return this.preRst1;
	}
	
	/**
	 * @param curtRst1
	 */
	public void setCurtRst1(java.math.BigDecimal curtRst1) {
		this.curtRst1 = curtRst1;
	}
	
    /**
     * @return CurtRst1
     */	
	public java.math.BigDecimal getCurtRst1() {
		return this.curtRst1;
	}
	
	/**
	 * @param remark1
	 */
	public void setRemark1(String remark1) {
		this.remark1 = remark1 == null ? null : remark1.trim();
	}
	
    /**
     * @return Remark1
     */	
	public String getRemark1() {
		return this.remark1;
	}
	
	/**
	 * @param preRst2
	 */
	public void setPreRst2(java.math.BigDecimal preRst2) {
		this.preRst2 = preRst2;
	}
	
    /**
     * @return PreRst2
     */	
	public java.math.BigDecimal getPreRst2() {
		return this.preRst2;
	}
	
	/**
	 * @param curtRst2
	 */
	public void setCurtRst2(java.math.BigDecimal curtRst2) {
		this.curtRst2 = curtRst2;
	}
	
    /**
     * @return CurtRst2
     */	
	public java.math.BigDecimal getCurtRst2() {
		return this.curtRst2;
	}
	
	/**
	 * @param remark2
	 */
	public void setRemark2(String remark2) {
		this.remark2 = remark2 == null ? null : remark2.trim();
	}
	
    /**
     * @return Remark2
     */	
	public String getRemark2() {
		return this.remark2;
	}
	
	/**
	 * @param preRst3
	 */
	public void setPreRst3(java.math.BigDecimal preRst3) {
		this.preRst3 = preRst3;
	}
	
    /**
     * @return PreRst3
     */	
	public java.math.BigDecimal getPreRst3() {
		return this.preRst3;
	}
	
	/**
	 * @param curtRst3
	 */
	public void setCurtRst3(java.math.BigDecimal curtRst3) {
		this.curtRst3 = curtRst3;
	}
	
    /**
     * @return CurtRst3
     */	
	public java.math.BigDecimal getCurtRst3() {
		return this.curtRst3;
	}
	
	/**
	 * @param remark3
	 */
	public void setRemark3(String remark3) {
		this.remark3 = remark3 == null ? null : remark3.trim();
	}
	
    /**
     * @return Remark3
     */	
	public String getRemark3() {
		return this.remark3;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}