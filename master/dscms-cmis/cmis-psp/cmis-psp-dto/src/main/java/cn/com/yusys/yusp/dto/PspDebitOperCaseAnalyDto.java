package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * psp_debit_oper_case_analy数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:24
 */
public class PspDebitOperCaseAnalyDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 产品库存是否大幅增加
     **/
    private String isBigGrowStorage;

    /**
     * 库存大幅增加说明
     **/
    private String bigGrowStorageRemark;

    /**
     * 借款人是否存在过度对外投资
     **/
    private String isBigOuterInvest;

    /**
     * 过度对外投资说明
     **/
    private String bigOuterInvestRemark;

    /**
     * 借款人经营所有权是否发生重大变化
     **/
    private String isBigChangeOwner;

    /**
     * 经营所有权重大变化说明
     **/
    private String bigChangeOwnerRemark;

    /**
     * 贷款用途是否符合约定
     **/
    private String isSuitAgreed;

    /**
     * 用途不符说明
     **/
    private String suitAgreedRemark;

    /**
     * 借款人经营是否正常
     **/
    private String isNormalOper;

    /**
     * 经营异常说明
     **/
    private String normalOperRemark;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspDebitOperCaseAnalyDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getIsBigGrowStorage() {
        return isBigGrowStorage;
    }

    public void setIsBigGrowStorage(String isBigGrowStorage) {
        this.isBigGrowStorage = isBigGrowStorage;
    }

    public String getBigGrowStorageRemark() {
        return bigGrowStorageRemark;
    }

    public void setBigGrowStorageRemark(String bigGrowStorageRemark) {
        this.bigGrowStorageRemark = bigGrowStorageRemark;
    }

    public String getIsBigOuterInvest() {
        return isBigOuterInvest;
    }

    public void setIsBigOuterInvest(String isBigOuterInvest) {
        this.isBigOuterInvest = isBigOuterInvest;
    }

    public String getBigOuterInvestRemark() {
        return bigOuterInvestRemark;
    }

    public void setBigOuterInvestRemark(String bigOuterInvestRemark) {
        this.bigOuterInvestRemark = bigOuterInvestRemark;
    }

    public String getIsBigChangeOwner() {
        return isBigChangeOwner;
    }

    public void setIsBigChangeOwner(String isBigChangeOwner) {
        this.isBigChangeOwner = isBigChangeOwner;
    }

    public String getBigChangeOwnerRemark() {
        return bigChangeOwnerRemark;
    }

    public void setBigChangeOwnerRemark(String bigChangeOwnerRemark) {
        this.bigChangeOwnerRemark = bigChangeOwnerRemark;
    }

    public String getIsSuitAgreed() {
        return isSuitAgreed;
    }

    public void setIsSuitAgreed(String isSuitAgreed) {
        this.isSuitAgreed = isSuitAgreed;
    }

    public String getSuitAgreedRemark() {
        return suitAgreedRemark;
    }

    public void setSuitAgreedRemark(String suitAgreedRemark) {
        this.suitAgreedRemark = suitAgreedRemark;
    }

    public String getIsNormalOper() {
        return isNormalOper;
    }

    public void setIsNormalOper(String isNormalOper) {
        this.isNormalOper = isNormalOper;
    }

    public String getNormalOperRemark() {
        return normalOperRemark;
    }

    public void setNormalOperRemark(String normalOperRemark) {
        this.normalOperRemark = normalOperRemark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}