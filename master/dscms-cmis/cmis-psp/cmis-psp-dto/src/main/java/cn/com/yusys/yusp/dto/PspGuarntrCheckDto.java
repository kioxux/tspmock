package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * psp_guarntr_check数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:24
 */
public class PspGuarntrCheckDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 目前保证人经营是否正常
     **/
    private String isOperNormal;

    /**
     * 保证人经营异常说明
     **/
    private String operExpl;

    /**
     * 保证人担保能力
     **/
    private String guarAbi;

    /**
     * 保证人担保能力说明
     **/
    private String guarAbiExpl;

    /**
     * 保证人担保意愿
     **/
    private String guarWish;

    /**
     * 保证人担保意愿说明
     **/
    private String guarWishExpl;

    /**
     * 是否有其他重要风险事项
     **/
    private String isOtherRiskEvent;

    /**
     * 其他风险说明
     **/
    private String riskEventExpl;

    /**
     * 担保人经营（工作）是否正常
     **/
    private String isGuarOperNormal;

    /**
     * 担保人经营异常说明
     **/
    private String unnormalDesc;

    /**
     * 担保人担保能力是否下降
     **/
    private String isChangeAbi;

    /**
     * 担保人担保能力下降说明
     **/
    private String changeAbiRemark;

    /**
     * 是否超过担保人实际担保能力
     **/
    private String isAddAbi;

    /**
     * 超过担保能力说明
     **/
    private String addAbiRemark;

    /**
     * 担保人及其家庭是否发生意外
     **/
    private String isAccident;

    /**
     * 意外说明
     **/
    private String accidentRemark;

    /**
     * 担保人是否面临重大诉讼
     **/
    private String isInvolveLawsuit;

    /**
     * 重大诉讼说明
     **/
    private String involveLawsuitRemark;

    /**
     * 是否具备担保资格和能力
     **/
    private String isOwnGuarAbi;

    /**
     * 担保资格说明
     **/
    private String ownGuarAbiRemark;

    /**
     * 对担保人的总体评价
     **/
    private String totlEvlu;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspGuarntrCheckDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getIsOperNormal() {
        return isOperNormal;
    }

    public void setIsOperNormal(String isOperNormal) {
        this.isOperNormal = isOperNormal;
    }

    public String getOperExpl() {
        return operExpl;
    }

    public void setOperExpl(String operExpl) {
        this.operExpl = operExpl;
    }

    public String getGuarAbi() {
        return guarAbi;
    }

    public void setGuarAbi(String guarAbi) {
        this.guarAbi = guarAbi;
    }

    public String getGuarAbiExpl() {
        return guarAbiExpl;
    }

    public void setGuarAbiExpl(String guarAbiExpl) {
        this.guarAbiExpl = guarAbiExpl;
    }

    public String getGuarWish() {
        return guarWish;
    }

    public void setGuarWish(String guarWish) {
        this.guarWish = guarWish;
    }

    public String getGuarWishExpl() {
        return guarWishExpl;
    }

    public void setGuarWishExpl(String guarWishExpl) {
        this.guarWishExpl = guarWishExpl;
    }

    public String getIsOtherRiskEvent() {
        return isOtherRiskEvent;
    }

    public void setIsOtherRiskEvent(String isOtherRiskEvent) {
        this.isOtherRiskEvent = isOtherRiskEvent;
    }

    public String getRiskEventExpl() {
        return riskEventExpl;
    }

    public void setRiskEventExpl(String riskEventExpl) {
        this.riskEventExpl = riskEventExpl;
    }

    public String getIsGuarOperNormal() {
        return isGuarOperNormal;
    }

    public void setIsGuarOperNormal(String isGuarOperNormal) {
        this.isGuarOperNormal = isGuarOperNormal;
    }

    public String getUnnormalDesc() {
        return unnormalDesc;
    }

    public void setUnnormalDesc(String unnormalDesc) {
        this.unnormalDesc = unnormalDesc;
    }

    public String getIsChangeAbi() {
        return isChangeAbi;
    }

    public void setIsChangeAbi(String isChangeAbi) {
        this.isChangeAbi = isChangeAbi;
    }

    public String getChangeAbiRemark() {
        return changeAbiRemark;
    }

    public void setChangeAbiRemark(String changeAbiRemark) {
        this.changeAbiRemark = changeAbiRemark;
    }

    public String getIsAddAbi() {
        return isAddAbi;
    }

    public void setIsAddAbi(String isAddAbi) {
        this.isAddAbi = isAddAbi;
    }

    public String getAddAbiRemark() {
        return addAbiRemark;
    }

    public void setAddAbiRemark(String addAbiRemark) {
        this.addAbiRemark = addAbiRemark;
    }

    public String getIsAccident() {
        return isAccident;
    }

    public void setIsAccident(String isAccident) {
        this.isAccident = isAccident;
    }

    public String getAccidentRemark() {
        return accidentRemark;
    }

    public void setAccidentRemark(String accidentRemark) {
        this.accidentRemark = accidentRemark;
    }

    public String getIsInvolveLawsuit() {
        return isInvolveLawsuit;
    }

    public void setIsInvolveLawsuit(String isInvolveLawsuit) {
        this.isInvolveLawsuit = isInvolveLawsuit;
    }

    public String getInvolveLawsuitRemark() {
        return involveLawsuitRemark;
    }

    public void setInvolveLawsuitRemark(String involveLawsuitRemark) {
        this.involveLawsuitRemark = involveLawsuitRemark;
    }

    public String getIsOwnGuarAbi() {
        return isOwnGuarAbi;
    }

    public void setIsOwnGuarAbi(String isOwnGuarAbi) {
        this.isOwnGuarAbi = isOwnGuarAbi;
    }

    public String getOwnGuarAbiRemark() {
        return ownGuarAbiRemark;
    }

    public void setOwnGuarAbiRemark(String ownGuarAbiRemark) {
        this.ownGuarAbiRemark = ownGuarAbiRemark;
    }

    public String getTotlEvlu() {
        return totlEvlu;
    }

    public void setTotlEvlu(String totlEvlu) {
        this.totlEvlu = totlEvlu;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}