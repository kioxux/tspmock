package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: PspTaskList
 * @类描述: psp_task_list数据实体类
 * @功能描述: 
 * @创建人: hanl
 * @创建时间: 2021-06-13 19:39:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PspTaskListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 任务类型 **/
	private String taskType;
	
	/** 检查类型 **/
	private String checkType;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 任务生成日期 **/
	private String taskStartDt;
	
	/** 任务要求完成日期 **/
	private String taskEndDt;
	
	/** 任务执行人 **/
	private String execId;
	
	/** 任务执行机构 **/
	private String execBrId;
	
	/** 检查状态 **/
	private String checkStatus;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 报告类型 **/
	private String rptType;
	
	/** 检查日期 **/
	private String checkDate;
	
	/** 借据起始日 **/
	private String loanStartDate;
	
	/** 借据到期日 **/
	private String loanEndDate;
	
	/** 风险等级 **/
	private String riskLvl;
	
	/** 担保方式 **/
	private String guarMode;
	
	/** 任务派发人员 **/
	private String issueId;
	
	/** 任务派发人员所属机构 **/
	private String issueBrId;
	
	/** 任务下发日期 **/
	private String issueDate;
	
	/** 贷款金额（元）  **/
	private java.math.BigDecimal loanAmt;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType == null ? null : taskType.trim();
	}
	
    /**
     * @return TaskType
     */	
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param checkType
	 */
	public void setCheckType(String checkType) {
		this.checkType = checkType == null ? null : checkType.trim();
	}
	
    /**
     * @return CheckType
     */	
	public String getCheckType() {
		return this.checkType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param taskStartDt
	 */
	public void setTaskStartDt(String taskStartDt) {
		this.taskStartDt = taskStartDt == null ? null : taskStartDt.trim();
	}
	
    /**
     * @return TaskStartDt
     */	
	public String getTaskStartDt() {
		return this.taskStartDt;
	}
	
	/**
	 * @param taskEndDt
	 */
	public void setTaskEndDt(String taskEndDt) {
		this.taskEndDt = taskEndDt == null ? null : taskEndDt.trim();
	}
	
    /**
     * @return TaskEndDt
     */	
	public String getTaskEndDt() {
		return this.taskEndDt;
	}
	
	/**
	 * @param execId
	 */
	public void setExecId(String execId) {
		this.execId = execId == null ? null : execId.trim();
	}
	
    /**
     * @return ExecId
     */	
	public String getExecId() {
		return this.execId;
	}
	
	/**
	 * @param execBrId
	 */
	public void setExecBrId(String execBrId) {
		this.execBrId = execBrId == null ? null : execBrId.trim();
	}
	
    /**
     * @return ExecBrId
     */	
	public String getExecBrId() {
		return this.execBrId;
	}
	
	/**
	 * @param checkStatus
	 */
	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus == null ? null : checkStatus.trim();
	}
	
    /**
     * @return CheckStatus
     */	
	public String getCheckStatus() {
		return this.checkStatus;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param rptType
	 */
	public void setRptType(String rptType) {
		this.rptType = rptType == null ? null : rptType.trim();
	}
	
    /**
     * @return RptType
     */	
	public String getRptType() {
		return this.rptType;
	}
	
	/**
	 * @param checkDate
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate == null ? null : checkDate.trim();
	}
	
    /**
     * @return CheckDate
     */	
	public String getCheckDate() {
		return this.checkDate;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate == null ? null : loanStartDate.trim();
	}
	
    /**
     * @return LoanStartDate
     */	
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate == null ? null : loanEndDate.trim();
	}
	
    /**
     * @return LoanEndDate
     */	
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param riskLvl
	 */
	public void setRiskLvl(String riskLvl) {
		this.riskLvl = riskLvl == null ? null : riskLvl.trim();
	}
	
    /**
     * @return RiskLvl
     */	
	public String getRiskLvl() {
		return this.riskLvl;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param issueId
	 */
	public void setIssueId(String issueId) {
		this.issueId = issueId == null ? null : issueId.trim();
	}
	
    /**
     * @return IssueId
     */	
	public String getIssueId() {
		return this.issueId;
	}
	
	/**
	 * @param issueBrId
	 */
	public void setIssueBrId(String issueBrId) {
		this.issueBrId = issueBrId == null ? null : issueBrId.trim();
	}
	
    /**
     * @return IssueBrId
     */	
	public String getIssueBrId() {
		return this.issueBrId;
	}
	
	/**
	 * @param issueDate
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate == null ? null : issueDate.trim();
	}
	
    /**
     * @return IssueDate
     */	
	public String getIssueDate() {
		return this.issueDate;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}


}