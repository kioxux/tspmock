package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * psp_guar_check数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:24
 */
public class PspGuarCheckDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 上期贷后检查时担保总额（万元）
     **/
    private BigDecimal preGuarAmt;

    /**
     * 本期贷后检查时担保总额（万元）
     **/
    private BigDecimal curtGuarAmt;

    /**
     * 对外担保变化（万元）
     **/
    private BigDecimal guarChange;

    /**
     * 其中对关联企业担保总额（万元）
     **/
    private BigDecimal correConGuarAmt;

    /**
     * 对外担保变化情况分析
     **/
    private String guarChangeAnaly;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspGuarCheckDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public BigDecimal getPreGuarAmt() {
        return preGuarAmt;
    }

    public void setPreGuarAmt(BigDecimal preGuarAmt) {
        this.preGuarAmt = preGuarAmt;
    }

    public BigDecimal getCurtGuarAmt() {
        return curtGuarAmt;
    }

    public void setCurtGuarAmt(BigDecimal curtGuarAmt) {
        this.curtGuarAmt = curtGuarAmt;
    }

    public BigDecimal getGuarChange() {
        return guarChange;
    }

    public void setGuarChange(BigDecimal guarChange) {
        this.guarChange = guarChange;
    }

    public BigDecimal getCorreConGuarAmt() {
        return correConGuarAmt;
    }

    public void setCorreConGuarAmt(BigDecimal correConGuarAmt) {
        this.correConGuarAmt = correConGuarAmt;
    }

    public String getGuarChangeAnaly() {
        return guarChangeAnaly;
    }

    public void setGuarChangeAnaly(String guarChangeAnaly) {
        this.guarChangeAnaly = guarChangeAnaly;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}