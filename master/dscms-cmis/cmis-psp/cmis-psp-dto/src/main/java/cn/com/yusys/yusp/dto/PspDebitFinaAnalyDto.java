package cn.com.yusys.yusp.dto;

import java.util.Date;

/**
 * psp_debit_fina_analy数据实体类
 *
 * @version 1.0.0
 * @since 2021-06-17 22:29:24
 */
public class PspDebitFinaAnalyDto {

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 任务编号
     **/
    private String taskNo;

    /**
     * 借款人销售同比是否下降
     **/
    private String isChangeSale;

    /**
     * 销售同比下降说明
     **/
    private String changeSaleRemark;

    /**
     * 应收款是否明显增加
     **/
    private String isGrowRec;

    /**
     * 应收款明显增加说明
     **/
    private String growRecReark;

    /**
     * 借款人负债是否明显增加
     **/
    private String isGrowDebt;

    /**
     * 负债明显增加说明
     **/
    private String growDebtRemark;

    /**
     * 是否涉及民间借贷
     **/
    private String isDc;

    /**
     * 涉及民间借贷说明
     **/
    private String dcRemark;

    /**
     * 创建时间
     **/
    private Date createTime;

    /**
     * 修改时间
     **/
    private Date updateTime;

    public PspDebitFinaAnalyDto() {
        // do nothing
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getIsChangeSale() {
        return isChangeSale;
    }

    public void setIsChangeSale(String isChangeSale) {
        this.isChangeSale = isChangeSale;
    }

    public String getChangeSaleRemark() {
        return changeSaleRemark;
    }

    public void setChangeSaleRemark(String changeSaleRemark) {
        this.changeSaleRemark = changeSaleRemark;
    }

    public String getIsGrowRec() {
        return isGrowRec;
    }

    public void setIsGrowRec(String isGrowRec) {
        this.isGrowRec = isGrowRec;
    }

    public String getGrowRecReark() {
        return growRecReark;
    }

    public void setGrowRecReark(String growRecReark) {
        this.growRecReark = growRecReark;
    }

    public String getIsGrowDebt() {
        return isGrowDebt;
    }

    public void setIsGrowDebt(String isGrowDebt) {
        this.isGrowDebt = isGrowDebt;
    }

    public String getGrowDebtRemark() {
        return growDebtRemark;
    }

    public void setGrowDebtRemark(String growDebtRemark) {
        this.growDebtRemark = growDebtRemark;
    }

    public String getIsDc() {
        return isDc;
    }

    public void setIsDc(String isDc) {
        this.isDc = isDc;
    }

    public String getDcRemark() {
        return dcRemark;
    }

    public void setDcRemark(String dcRemark) {
        this.dcRemark = dcRemark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}