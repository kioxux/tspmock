package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-psp-core模块
 * @类名称: RiskGuarntrList
 * @类描述: risk_guarntr_list数据实体类
 * @功能描述: 
 * @创建人: me
 * @创建时间: 2021-06-17 22:29:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RiskGuarntrListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 保证编号 **/
	private String guarantyId;
	
	/** 保证人客户编号 **/
	private String guarCusId;
	
	/** 保证人客户名称 **/
	private String guarCusName;
	
	/** 保证方式 **/
	private String guaranteeType;
	
	/** 保证金额（元） **/
	private java.math.BigDecimal guarAmt;
	
	/** 保证人代偿意愿 **/
	private String subpayWish;
	
	/** 代偿能力 **/
	private String subpayAbi;
	
	/** 分析状态 **/
	private String analyStatus;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param guarantyId
	 */
	public void setGuarantyId(String guarantyId) {
		this.guarantyId = guarantyId == null ? null : guarantyId.trim();
	}
	
    /**
     * @return GuarantyId
     */	
	public String getGuarantyId() {
		return this.guarantyId;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId == null ? null : guarCusId.trim();
	}
	
    /**
     * @return GuarCusId
     */	
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName == null ? null : guarCusName.trim();
	}
	
    /**
     * @return GuarCusName
     */	
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param guaranteeType
	 */
	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType == null ? null : guaranteeType.trim();
	}
	
    /**
     * @return GuaranteeType
     */	
	public String getGuaranteeType() {
		return this.guaranteeType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return GuarAmt
     */	
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param subpayWish
	 */
	public void setSubpayWish(String subpayWish) {
		this.subpayWish = subpayWish == null ? null : subpayWish.trim();
	}
	
    /**
     * @return SubpayWish
     */	
	public String getSubpayWish() {
		return this.subpayWish;
	}
	
	/**
	 * @param subpayAbi
	 */
	public void setSubpayAbi(String subpayAbi) {
		this.subpayAbi = subpayAbi == null ? null : subpayAbi.trim();
	}
	
    /**
     * @return SubpayAbi
     */	
	public String getSubpayAbi() {
		return this.subpayAbi;
	}
	
	/**
	 * @param analyStatus
	 */
	public void setAnalyStatus(String analyStatus) {
		this.analyStatus = analyStatus == null ? null : analyStatus.trim();
	}
	
    /**
     * @return AnalyStatus
     */	
	public String getAnalyStatus() {
		return this.analyStatus;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}