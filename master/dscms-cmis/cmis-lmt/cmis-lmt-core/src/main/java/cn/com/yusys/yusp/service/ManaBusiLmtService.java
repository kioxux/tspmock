/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.ManaBusiLmt;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.req.CmisBiz0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.resp.CmisBiz0003RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ManaBusiLmtMapper;
import cn.com.yusys.yusp.vo.ManaBusiLmtVo;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ManaBusiLmtService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 15:32:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ManaBusiLmtService {

    private static final Logger logger = LoggerFactory.getLogger(ManaBusiLmtService.class);


    @Autowired
    private ManaBusiLmtMapper manaBusiLmtMapper;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CmisBizClientService cmisBizClientService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ManaBusiLmt selectByPrimaryKey(String busiType, String orgAreaType) {
        return manaBusiLmtMapper.selectByPrimaryKey(busiType, orgAreaType);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ManaBusiLmt> selectAll(QueryModel model) {
        List<ManaBusiLmt> records = (List<ManaBusiLmt>) manaBusiLmtMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ManaBusiLmt> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ManaBusiLmt> list = manaBusiLmtMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryByOrgType
     * @方法描述: 根据机构汇总
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map<String, Object>> queryByOrgType() {
        List<Map<String, Object>> list = manaBusiLmtMapper.queryByOrgType();
        return list;
    }

    /**
     * @方法名称: queryByOrgType
     * @方法描述: 根据条线汇总
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map<String, Object>> queryByBusiType(String busiType) {
        List<Map<String, Object>> list = manaBusiLmtMapper.queryByBusiType(busiType);
        return list;
    }

    /**
     * @方法名称: selectListByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map> selectListByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ManaBusiLmt> manaBusiLmts = manaBusiLmtMapper.selectByModel(model);
        List<Map> manaBusiLmtVoList = new ArrayList<>();

        //本地机构
        List<String> localOrgList = adminSmOrgService.getRemoteOrgList("0").getData();
        logger.info("getRemoteOrgList--localOrgs->resp===>{}", JSONObject.toJSON(localOrgList));
        if (CollectionUtils.isEmpty(localOrgList)) {
            throw BizException.error(null, "999999", "本地机构列表获取失败");
        }

        //异地机构
        List<String> otherLocalOrgList = adminSmOrgService.getRemoteOrgList("1").getData();
        logger.info("getRemoteOrgList--otherLocalOrgs->resp===>{}", JSONObject.toJSON(otherLocalOrgList));
        if (CollectionUtils.isEmpty(localOrgList)) {
            throw BizException.error(null, "999999", "异地机构列表获取失败");
        }

        if (CollectionUtils.nonEmpty(manaBusiLmts)) {
            manaBusiLmts.stream().forEach(a -> {
                Map<String, Object> resultMap = new HashMap<>();
                Map<String, Object> newMap = (Map<String, Object>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(a);
                resultMap.putAll(newMap);

                CmisBiz0003ReqDto cmisBiz0003ReqDto = new CmisBiz0003ReqDto();
                //STD_ORG_AREA_TYPE orgAreaType
                //本地机构
                if (CmisBizConstants.STD_ORG_AREA_TYPE_01.equals(a.getOrgAreaType())){
                    cmisBiz0003ReqDto.setOrgIdList(localOrgList);
                }else{
                    //异地机构
                    cmisBiz0003ReqDto.setOrgIdList(otherLocalOrgList);
                }
                cmisBiz0003ReqDto.setBelgLine(a.getBusiType());
                cmisBiz0003ReqDto.setSelectType(1);
                logger.info("cmisBiz0003--->reqDto===>{}",cmisBiz0003ReqDto);
                ResultDto<CmisBiz0003RespDto> cmisBiz0003RespDtoResultDto = cmisBizClientService.cmisBiz0003(cmisBiz0003ReqDto);
                logger.info("cmisBiz0003--->respDto===>{}",cmisBiz0003RespDtoResultDto);
                String code = cmisBiz0003RespDtoResultDto.getCode();
                if (!SuccessEnum.CMIS_SUCCSESS.key.equals(code)){
                    throw BizException.error(null,cmisBiz0003RespDtoResultDto.getCode(),cmisBiz0003RespDtoResultDto.getMessage());
                }
                CmisBiz0003RespDto data = cmisBiz0003RespDtoResultDto.getData();
                if (!SuccessEnum.SUCCESS.key.equals(data.getErrorCode())){
                    throw BizException.error(null,data.getErrorCode(),data.getErrorMsg());
                }
                //当前余额
                BigDecimal totalCurrMonthAllowLoanBalance = data.getTotalCurrMonthAllowLoanBalance().divide(BigDecimal.valueOf(10000L));
                //当月剩余可投放对公贷款余额  = 上月末贷款余额 + 当月可净新增贷款投放金额 - 当前余额
                BigDecimal currMonthAllowLoanBalance = a.getLastMonthLoanBalance()
                        .add(a.getCurrMonthAllowAddAmt())
                        .subtract(totalCurrMonthAllowLoanBalance);
                resultMap.put("currMonthAllowLoanBalance",currMonthAllowLoanBalance);
                //月末贷款余额测算 scope.row.lastMonthLoanBalance + scope.row.currMonthAllowAddAmt
                BigDecimal curMonthLoanBalancePlan = a.getLastMonthLoanBalance()
                        .add(a.getCurrMonthAllowAddAmt());
                resultMap.put("curMonthLoanBalancePlan",curMonthLoanBalancePlan);

                manaBusiLmtVoList.add(resultMap);
            });
        }
        PageHelper.clearPage();
        return manaBusiLmtVoList;
    }

    /**
     * 异步导出业务条线额度管控数据
     * @return 导出进度信息
     */
    public ProgressDto asyncExportManaBusiLmt(QueryModel model) {
        model.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            List<ManaBusiLmtVo> manaBusiLmtVoList = new ArrayList<>();

            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            queryModeTemp.setSort(" busiType desc, orgAreaType asc ");
            List<ManaBusiLmt> manaBusiLmts = selectByModel(queryModeTemp);
            if (CollectionUtils.nonEmpty(manaBusiLmts)) {
                List<ManaBusiLmtVo> busiList =new ArrayList<>();//存放条线汇总信息
                ManaBusiLmtVo BusiLmtVo = new ManaBusiLmtVo();
                BigDecimal curMonthLoanBalancePlanTotal = BigDecimal.ZERO;
                BigDecimal lastMonthLoanBalanceTotal = BigDecimal.ZERO;
                BigDecimal currMonthAllowAddAmtTotal = BigDecimal.ZERO;
                BigDecimal lastDayLoanBalanceTotal = BigDecimal.ZERO;

                manaBusiLmts.stream().forEach(a -> {
                    ManaBusiLmtVo manaBusiLmtVo = new ManaBusiLmtVo();
                    BeanUtils.copyProperties(a, manaBusiLmtVo);
//                    //当月剩余可投放对公贷款余额
//                    BigDecimal currMonthAllowLoanBalance = a.getLastMonthLoanBalance()
//                            .add(a.getCurrMonthAllowAddAmt())
//                            .subtract(a.getLastDayLoanBalance());
//                    manaBusiLmtVo.setCurrMonthAllowLoanBalance(currMonthAllowLoanBalance);

                    //月末贷款余额测算 scope.row.lastMonthLoanBalance + scope.row.currMonthAllowAddAmt
                    BigDecimal curMonthLoanBalancePlan = a.getLastMonthLoanBalance()
                            .add(a.getCurrMonthAllowAddAmt());
                    manaBusiLmtVo.setCurMonthLoanBalancePlan(curMonthLoanBalancePlan);
                    manaBusiLmtVoList.add(manaBusiLmtVo);

                    if (CmisBizConstants.STD_ORG_AREA_TYPE_02.equals(manaBusiLmtVo.getOrgAreaType())) {
                        /**处理条线汇总*/
                        List<Map<String,Object>> busiTypeList = this.queryByBusiType(manaBusiLmtVo.getBusiType());
                        for(Map<String,Object> map : busiTypeList){
                            ManaBusiLmtVo busiTypeLmtVoTotal = new ManaBusiLmtVo();
                            BigDecimal lastMonthLoanBalance = (BigDecimal) map.get("lastMonthLoanBalance");
                            BigDecimal currMonthAllowAddAmt = (BigDecimal) map.get("currMonthAllowAddAmt");
                            BigDecimal lastDayLoanBalance = (BigDecimal) map.get("lastDayLoanBalance");
                            BigDecimal curMonthLoanBalancePlanBusiType =lastMonthLoanBalance.add(currMonthAllowAddAmt);
                            String belgText = this.getBelgText((String) map.get("busiType"));
                            busiTypeLmtVoTotal.setBusiType("");
                            busiTypeLmtVoTotal.setOrgAreaType(belgText);
                            busiTypeLmtVoTotal.setLastMonthLoanBalance(lastMonthLoanBalance);
                            busiTypeLmtVoTotal.setCurrMonthAllowAddAmt(currMonthAllowAddAmt);
                            busiTypeLmtVoTotal.setLastDayLoanBalance(lastDayLoanBalance);
                            busiTypeLmtVoTotal.setCurMonthLoanBalancePlan(curMonthLoanBalancePlanBusiType);
                            manaBusiLmtVoList.add(busiTypeLmtVoTotal);
                        }
                    }
                });

                /**增加本地机构汇总、异地机构汇总 add by zhangjw 20210901*/
                List<Map<String,Object>> orgList = this.queryByOrgType();
                if (CollectionUtils.nonEmpty(manaBusiLmts)) {
                    ManaBusiLmtVo orgLmtVoTotal = new ManaBusiLmtVo();
                    for(Map<String,Object> map : orgList){
                        //月末贷款余额测算
                        BigDecimal lastMonthLoanBalance = (BigDecimal) map.get("lastMonthLoanBalance");
                        BigDecimal currMonthAllowAddAmt = (BigDecimal) map.get("currMonthAllowAddAmt");
                        BigDecimal lastDayLoanBalance = (BigDecimal) map.get("lastDayLoanBalance");

                        BigDecimal curMonthLoanBalancePlan =lastMonthLoanBalance.add(currMonthAllowAddAmt);
                        //月末贷款余额测算 汇总
                        curMonthLoanBalancePlanTotal = curMonthLoanBalancePlanTotal.add(curMonthLoanBalancePlan);
                        //上月末贷款余额 汇总
                        lastMonthLoanBalanceTotal = lastMonthLoanBalanceTotal.add(lastMonthLoanBalance);
                        //当月可净新增贷款投放金额 汇总
                        currMonthAllowAddAmtTotal = currMonthAllowAddAmtTotal.add(currMonthAllowAddAmt);
                        //上一日贷款余额 汇总
                        lastDayLoanBalanceTotal = lastDayLoanBalanceTotal.add(lastDayLoanBalance);

                        ManaBusiLmtVo orgLmtVo = new ManaBusiLmtVo();
                        orgLmtVo.setBusiType("");
                        if (CmisBizConstants.STD_ORG_AREA_TYPE_01.equals(map.get("orgAreaType"))) {
                            orgLmtVo.setOrgAreaType("本地机构合计");
                        }else if (CmisBizConstants.STD_ORG_AREA_TYPE_02.equals(map.get("orgAreaType"))){
                            orgLmtVo.setOrgAreaType("异地机构合计");
                        }
                        //月末贷款余额测算 scope.row.lastMonthLoanBalance + scope.row.currMonthAllowAddAmt
                        orgLmtVo.setLastMonthLoanBalance(lastMonthLoanBalance);
                        orgLmtVo.setCurrMonthAllowAddAmt(currMonthAllowAddAmt);
                        orgLmtVo.setLastDayLoanBalance(lastDayLoanBalance);
                        orgLmtVo.setCurMonthLoanBalancePlan(curMonthLoanBalancePlan);
                        manaBusiLmtVoList.add(orgLmtVo);
                    }
                    orgLmtVoTotal.setBusiType("");
                    orgLmtVoTotal.setOrgAreaType("合计");
                    orgLmtVoTotal.setLastMonthLoanBalance(lastMonthLoanBalanceTotal);
                    orgLmtVoTotal.setCurrMonthAllowAddAmt(currMonthAllowAddAmtTotal);
                    orgLmtVoTotal.setCurMonthLoanBalancePlan(curMonthLoanBalancePlanTotal);
                    orgLmtVoTotal.setLastDayLoanBalance(lastDayLoanBalanceTotal);
                    manaBusiLmtVoList.add(orgLmtVoTotal);
                }
            }
            return manaBusiLmtVoList;
        };
        ExportContext exportContext = ExportContext.of(ManaBusiLmtVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 额度适用业务条线
     * 03	对公业务条线
     * 01	小微金融条线
     * 02	零售条线
     * 05	网络金融条线
     * 06	资金业务条线
     */
    private String getBelgText(String belgLine) {
        String belgText = "";

        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_01.equals(belgLine)) {
            belgText = "小微金融条线合计";
        }
        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_02.equals(belgLine)) {
            belgText = "零售条线合计";
        }
        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_03.equals(belgLine)) {
            belgText = "对公业务条线合计";
        }
        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_04.equals(belgLine)) {
            belgText = "";
        }
        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_05.equals(belgLine)) {
            belgText = "网络金融条线合计";
        }
        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_06.equals(belgLine)) {
            belgText = "资金业务条线合计";
        }
        return belgText;
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ManaBusiLmt record) {
        return manaBusiLmtMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ManaBusiLmt record) {
        return manaBusiLmtMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ManaBusiLmt record) {
        return manaBusiLmtMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ManaBusiLmt record) {
        return manaBusiLmtMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String busiType, String orgAreaType) {
        return manaBusiLmtMapper.deleteByPrimaryKey(busiType, orgAreaType);
    }

    /**
     * 根据机构编号获取分支机构额度管控信息
     * @param belgLine
     * @param orgAreaType
     * @return
     */
    public ManaBusiLmt selectByBelgLineAndOrgAreaType(String belgLine, String orgAreaType) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("busiType", belgLine);
        queryModel.addCondition("orgAreaType", orgAreaType);
        List<ManaBusiLmt> manaBusiLmts = selectByModel(queryModel);
        return CollectionUtils.nonEmpty(manaBusiLmts) ? manaBusiLmts.get(0) : null;
    }
}
