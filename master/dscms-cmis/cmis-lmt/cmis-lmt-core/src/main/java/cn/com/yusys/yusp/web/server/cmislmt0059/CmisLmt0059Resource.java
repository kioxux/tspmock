package cn.com.yusys.yusp.web.server.cmislmt0059;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0059.req.CmisLmt0059ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0059.resp.CmisLmt0059RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0059.CmisLmt0059Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）
 *
 * @author lizx 20210909
 * @version 1.0
 */
@Api(tags = "cmislmt0059:获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0059Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0059Resource.class);

    @Autowired
    private CmisLmt0059Service cmisLmt0059Service;

    /**
     * 交易码：CmisLmt0059
     * 交易描述：转贴现业务额度恢复
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）")
    @PostMapping("/cmislmt0059")
    protected @ResponseBody
    ResultDto<CmisLmt0059RespDto> CmisLmt0059(@Validated @RequestBody CmisLmt0059ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0059.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0059.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0059RespDto> CmisLmt0059RespDtoResultDto = new ResultDto<>();
        CmisLmt0059RespDto CmisLmt0059RespDto = new CmisLmt0059RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0059RespDto = cmisLmt0059Service.execute(reqDto);
            CmisLmt0059RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0059RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0059.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0059.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0059.value, e.getMessage());
            // 封装CmisLmt0059RespDtoResultDto中异常返回码和返回信息
            CmisLmt0059RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0059RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0059RespDto到CmisLmt0059RespDtoResultDto中
        CmisLmt0059RespDtoResultDto.setData(CmisLmt0059RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0059.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0059.value, JSON.toJSONString(CmisLmt0059RespDtoResultDto));
        return CmisLmt0059RespDtoResultDto;
    }
}