    package cn.com.yusys.yusp.service.server.cmislmt0011;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.ApprLmtChgFiledListDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtContRelMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0011Service
 * @类描述: #对内服务类
 * @功能描述: 额度占用接口
 * @创建人: lizx
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0011Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0011Service.class);

    @Autowired
    private LmtContRelService lmtContRelService ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private LmtContRelMapper lmtContRelMapper ;

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService ;

    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService ;

    @Autowired
    private LmtExptListInfoService lmtExptListInfoService;

    @Transactional
    public CmisLmt0011RespDto execute(CmisLmt0011ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.value);
        CmisLmt0011RespDto resqDto = new CmisLmt0011RespDto() ;

        try {
            List<ApprLmtChgFiledListDto> apprLmtChgFiledListDtos = new ArrayList<>() ;
            String dealBizNo = reqDto.getDealBizNo() ;
            //系统编号
            String sysId = reqDto.getSysId() ;
            //所属条线
            String belgLine = reqDto.getBelgLine() ;
            //交易业务类型
            String dealBizType = reqDto.getDealBizType() ;
            //交易属性
            String bizAttr = reqDto.getBizAttr() ;
            if(StringUtils.isBlank(dealBizType)){
                throw new YuspException(EclEnum.ECL070133.key, "交易业务类型" + EclEnum.ECL070133.value);
            }
            if(StringUtils.isBlank(bizAttr)){
                throw new YuspException(EclEnum.ECL070133.key, "交易属性" + EclEnum.ECL070133.value);
            }

            Map<String, String> paramtMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
            Map<String, String> resultMap = new HashMap<>() ;
            resultMap.putAll(paramtMap);
            resultMap.put("serno", dealBizNo) ;
            resultMap.put("serviceCode", CmisLmtConstants.CMIS_LMT0011_SERVICE_CODE) ;

            //查看客户是否额度调整白名单客户 根据业务编号，查看该客户是否白名单客户
            LmtExptListInfo lmtExptListInfo = lmtExptListInfoService.selectLmtExptListInfoByBussNo(dealBizNo) ;
            //白名单类外类型 01-忽略冻结状态	02-允许突破额度	03-允许突破限额	04-不占用额度（不占额，不校验）
            String exptType = "";
            //如果额度调整白名单非空，则获取例外类型
            if(lmtExptListInfo!=null) exptType = lmtExptListInfo.getExptType() ;
            //如果额度例外类型为空，则赋值为空字符，防止空指针
            if (exptType == null) exptType = "";
            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key+": 例外类型为："+exptType);

            //配置交易流水号
            String cfgDealBizNo = comm4Service.getSysParameterByName(CmisLmtConstants.DEAL_BIZ_NO) ;

            //04-不占用额度（不占额，不校验） 直接返回成功；额外类型存储方式为 【01，02，03】，所以此处更改为包含方式
            if(exptType.contains(CmisLmtConstants.EXPT_TYPE_04)){
                logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key+": 例外类型中包含【04-不占用额度】，额度校验通过。");
                resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
                resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
                return resqDto;
            }

            //保证金比例
            BigDecimal securityRate = BigDecimalUtil.replaceNull(reqDto.getDealBizBailPreRate()) ;
            //保证金金额（人民币）
            BigDecimal securityAmt = BigDecimalUtil.replaceNull(reqDto.getDealBizBailPreAmt()) ;
            //合同状态
            String bizStatus = reqDto.getDealBizStatus() ;
            //是否低风险
            String isLriskBiz = reqDto.getIsLriskBiz() ;

            /****************本次占用相关金额字段*********************/
            //占用总额（折人民币）
            BigDecimal bizTotalAmtCny;
            //占用敞口（折人民币）
            BigDecimal bizSpacAmtCny;
            //占用敞口余额（折人民币）
            BigDecimal bizSpacBalanceAmtCny;

            //是否无缝衔接
            String isFollowBiz = reqDto.getIsFollowBiz();
            //是否无缝衔接
            String isBizRev = reqDto.getIsBizRev();
            //原交易业务编号
            String origiDealBizNo = reqDto.getOrigiDealBizNo() ;
            //原交易业务恢复类型
            String origiRecoverType = reqDto.getOrigiRecoverType() ;

            String bussStageType = reqDto.getBussStageType() ;

            //无缝衔接，原交易编号不为空
            if(CmisLmtConstants.YES_NO_Y.equals(isFollowBiz) && StringUtils.isNotEmpty(origiDealBizNo)){
                logger.info("【{}】额度占用无缝衔接，原交易原交易流水号【{}】",DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key ,origiDealBizNo);
                //恢复合同金额 ，结清恢复，不需要处理分项已用总额和已用敞口总额
                //List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByDealBizNo(origiDealBizNo) ;
                List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(origiDealBizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
                logger.info("【{}】额度占用无缝衔接，合同信息{}",DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key ,JSON.toJSONString(lmtContRelList));
                if(CollectionUtils.isNotEmpty(lmtContRelList)){
                    for (LmtContRel lmtContRel : lmtContRelList) {
                        //额度分项编号
                        String limitSubNo = lmtContRel.getLimitSubNo();
                        //本次撤销占用非额度恢复中撤销占用
                        if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(origiRecoverType)){
                            //撤销占用，
                            logger.info("撤销占用【"+origiDealBizNo+"】，删除该占用信息");
                            comm4Service.updateSubInfoAmt(limitSubNo, lmtContRel, resultMap) ;
                            lmtContRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
                            lmtContRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
                            lmtContRel.setOprType(CmisLmtConstants.OPR_TYPE_DELETE);
                            lmtContRelService.updateByPKeyInApprLmtChgDetails(lmtContRel, resultMap);
                        }else{
                            throw new YuspException(EclEnum.ECL070087.key, EclEnum.ECL070087.value);
                        }
                    }
                }else{
                    logger.info("【{}】额度占用无缝衔接，未查询你到信息，此处跳过不处理",DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key);
                }
            }

            //合同重签处理
            //udpate by lizx 2021-11-18 描述：对公合同续签，需失效原合同信息，但是对公在合同申请提交时推送该占用，但是考虑到申请否决等场景
            //导致新老数据都失效，所以合同重签操作，从0011 接口更改到0012 接口操作
            /*if(CmisLmtConstants.YES_NO_Y.equals(isBizRev)){
                logger.info("额度合同重签，原交易原交易流水号【{}】" ,origiDealBizNo);
                //恢复合同金额 ，结清恢复，不需要处理分项已用总额和已用敞口总额
                comm4Service.bizRevExecute(origiDealBizNo, resultMap);
                logger.info("额度合同重签处理结束");
            }*/

            //工厂模式创建对象
            Supplier<LmtContRel> lmtContRelSupplier = LmtContRel::new ;
            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtos = reqDto.getCmisLmt0011OccRelListDtoList() ;
            //校验占用列表不允许为空，为空进行错误提示
            if(CollectionUtils.isEmpty(cmisLmt0011OccRelListDtos)){
                throw new YuspException(EclEnum.ECL070133.key, "占用额度列表" + EclEnum.ECL070133.value);
            }
            LmtContRel lmtContRel ;
            ApprLmtSubBasicInfo apprLmtSubBasicInfo = new ApprLmtSubBasicInfo();
            for(CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto:cmisLmt0011OccRelListDtos) {
                //额度类型
                String lmtType = cmisLmt0011OccRelListDto.getLmtType() ;
                //分项编号
                String lmtSubno = cmisLmt0011OccRelListDto.getLmtSubNo() ;
                //add by lizx 针对新零售双占业务，合作方额度占用退回后，更改了占用合作，导致根据额度分项编号，系统类型，交易编号查询，
                // 交易未成功，导致即占用了上一个合作方额度又占用了本次合作方额度
                /**
                 * 【bussStageType】业务阶段类型业务申请阶段，01-业务申请阶段 02-合同签订阶段
                 * 描述零售业务，例如个人按揭类贷款，占用第三方额度，在业务申请阶段，占用个人额度在合同鉴定阶段；
                 * 01-业务申请阶段 占用合作方额度打回，更换合作方，导致根据分项编号+合同编号+系统编号判断 交易是否成功时，找不到原占用额度，所以
                 * 针对此种情况，判断是否成功，根据合同编号+系统编号
                 * 02-合同签订阶段： 合同签订阶段，占用个人额度，因为占用个人额度合同编号和合作方合同编号一致，根据合同编号+系统编号 查询会查出
                 * 对应合作放的合同占用信息，针对此种情况需要根据分项编号+合同编号+系统编号 进行判断该交易是否成功
                 */
                String detailsSubNo = lmtSubno ;
                logger.info("业务条线{}，业务阶段类型{}", belgLine, bussStageType);
                if(CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_04.equals(belgLine) && CmisLmtConstants.STD_BUSS_STAGE_TYPE_01.equals(bussStageType)){
                    detailsSubNo = "" ;
                }
                //判断该交易是否已经成功
                if(comm4Service.judgeLmtDealIsSuss(detailsSubNo, dealBizNo, sysId)){
                    //判断该交易是否已经成功，如果已经成功，进行粗呢撤销操作
                    LmtContRel contRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(dealBizNo, detailsSubNo, sysId) ;
                    if(Objects.nonNull(contRel)){
                        //零售条线，交易已经成功不回复合作方额度 belgLine:04 零售条线  lmtType:03 合作方额度
                        logger.info("该交易已经成功，进行撤销占用你操作，撤销合同信息{}", contRel);
                        //撤销占用
                        detailsSubNo = contRel.getLimitSubNo() ;
                        logger.info("撤销占用【"+detailsSubNo+"】，删除该占用信息");
                        comm4Service.updateSubInfoAmt(detailsSubNo, contRel, resultMap) ;
                        contRel.setBizTotalBalanceAmtCny(BigDecimal.ZERO);
                        contRel.setBizSpacBalanceAmtCny(BigDecimal.ZERO);
                        contRel.setOprType(CmisLmtConstants.OPR_TYPE_DELETE);
                        lmtContRelMapper.updateByPrimaryKey(contRel);
                    }
                }

                if(StringUtils.isEmpty(lmtSubno)){
                    new YuspException(EclEnum.ECL070126.key, EclEnum.ECL070126.value) ;
                }
                lmtContRel = lmtContRelSupplier.get() ;
                //占用总额（折人民币）
                bizTotalAmtCny = BigDecimalUtil.replaceNull(cmisLmt0011OccRelListDto.getBizTotalAmtCny()) ;
                //占用敞口（折人民币）
                bizSpacAmtCny = BigDecimalUtil.replaceNull(cmisLmt0011OccRelListDto.getBizSpacAmtCny()) ;
                bizSpacBalanceAmtCny = BigDecimalUtil.replaceNull(cmisLmt0011OccRelListDto.getBizSpacAmtCny()) ;

                //如果是低风险占用，占用敞口金额不允许为大于0
                if(CmisLmtConstants.YES_NO_Y.equals(isLriskBiz)){
                    if(bizSpacAmtCny.compareTo(BigDecimal.ZERO)>0){
                        new YuspException(EclEnum.ECL070120.key,"【"+lmtSubno+"】"+EclEnum.ECL070120.value) ;
                    }
                }
                //单一客户授信
                if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_07.equals(lmtType)){
                    //获取合同占用分项信息
                    apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(lmtSubno) ;
                    if(Objects.isNull(apprLmtSubBasicInfo)){
                        new YuspException(EclEnum.ECL070007.key,"【"+lmtSubno+"】"+EclEnum.ECL070007.value) ;
                    }
                    logger.info("【{}】 额度占用分项信息【{}】", DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key ,JSON.toJSONString(apprLmtSubBasicInfo));

                    // add by 2021-09-07 无缝衔接不校验  20210917 配置流水相同不校验
                    //update by lizx 2021-11-18 允许突破限额，不校验额度
                    if(!(CmisLmtConstants.YES_NO_Y.equals(reqDto.getIsFollowBiz()) || CmisLmtConstants.EXPT_TYPE_02.contains(exptType) || CmisLmtConstants.YES_NO_Y.equals(reqDto.getIsBizRev()))){
                        if((CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType) && CmisLmtConstants.YES_NO_N.equals(isLriskBiz))
                                || !cfgDealBizNo.equals(dealBizNo)){
                            //校验分项信息
                            comm4Service.checkSpacLimit(apprLmtSubBasicInfo, bizSpacAmtCny, this.follwBizParam(reqDto)) ;
                            //校验分项总信息 add by 20211006 客户迁移数据，存在很多负数，拦截业务较大，此处暂时注释掉
                            //comm4Service.checkLimitTotal(lmtSubno, bizTotalAmtCny, this.follwBizParam(reqDto)) ;
                        }
                    }

                    //分项中 授信品种类型属性
                    String subLmtBizTypeProp = apprLmtSubBasicInfo.getLmtBizTypeProp() ;
                    //请求报文中
                    String reqLmtBizTypeProp = cmisLmt0011OccRelListDto.getPrdTypeProp() ;
                    if(!CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                        if(!comm4Service.IsEqualsTwoStr(subLmtBizTypeProp, reqLmtBizTypeProp)){
                            logger.info("【{}】检验授信品种类型属性不相等，分项中属性值【{}】，接口中属性值【{}】", DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key,
                                    subLmtBizTypeProp, reqLmtBizTypeProp);
                            throw new YuspException(EclEnum.ECL070086.key, EclEnum.ECL070086.value);
                        }
                    }

                    /**********************处理占用分项信息*****************************/
                    /*******
                     * 1、处理已用总额 ：已用总额 = 业务总额+已用总额
                     * 2、敞口已用额度：敞口已用金额 = 业务占用敞口金额（表内总额表外敞口）+敞口已用金额
                     * 3、若业务属性为台账，则用信余额 = 用信余额 + 本次业务总额；若业务属性未合同，用信余额不变
                     * 4、用信敞口余额：若业务属性为台账，则用信敞口余额 = 用信敞口余额 + 本次业务敞口金额（接口中占用敞口金额字段）
                     * 5、授信总额累加：若占用授信总额>授信总额可用，则   授信总额累加 = 授信总额累加 +  占用授信总额 - 授信总额可用
                     * 6、已出帐金额：合同占用不影响已出帐金额（业务属性非台账）；若业务属性为台账，已出帐金额 = 已出帐金额 + 业务占用敞口金额
                     * （台账校验：已出帐金额不能超过非低风险额度分项的授信敞口金额，不能超过低风险额度分项的授信总额）
                     * 7、可出账金额：敞口分项可出账金额 = 授信敞口 - 已出帐金额；低风险风险可出账金额 = 授信总额 - 已出帐金额*********/

                    //获取已用总额
                    BigDecimal outstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt()) ;
                    //获取敞口已用金额
                    BigDecimal spacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt()) ;
                    //用信余额
                    BigDecimal loanBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance()) ;
                    //已出账金额
                    BigDecimal pvpOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getPvpOutstndAmt()) ;
                    //可出账金额
                    BigDecimal avlOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlOutstndAmt()) ;
                    //用信敞口余额
                    BigDecimal loanSpacBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanSpacBalance()) ;
                    //占用后已用总金额
                    outstndAmt = outstndAmt.add(bizTotalAmtCny) ;
                    //占用后敞口已用金额
                    spacOutstndAmt = spacOutstndAmt.add(bizSpacAmtCny) ;
                    //是否低风险
                    String isLriskLmt = apprLmtSubBasicInfo.getIsLriskLmt() ;

                    //用信余额 判断该占用事台账占用还是合同占用，如果是合同占用，余额无变化，如果事台账占用，用信余额=用信余额 + 本次业务总额
                    if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                        if(CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(bizStatus)){
                            loanBalance = loanBalance.add(bizTotalAmtCny) ;
                            //用信敞口余额
                            loanSpacBalance = loanSpacBalance.add(bizTotalAmtCny) ;
                        }
                        if(CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)){
                            //已出账金额
                            pvpOutstndAmt = pvpOutstndAmt.add(bizTotalAmtCny) ;
                            //可出账金额
                            avlOutstndAmt = avlOutstndAmt.subtract(bizTotalAmtCny) ;
                        }else{
                            //已出账金额
                            pvpOutstndAmt = pvpOutstndAmt.add(bizSpacAmtCny) ;
                            //可出账金额
                            avlOutstndAmt = avlOutstndAmt.subtract(bizSpacAmtCny) ;
                        }

                        logger.info("【BizAttr】等于【2-台账】\"+apprLmtSubBasicInfoOne.getApprSubSerno()+\"处理后的金额--->用信余额:"+loanBalance+"， 用信敞口余额:"+loanSpacBalance+"， " +
                                "已出账金额:"+pvpOutstndAmt+"， 可出账金额:"+avlOutstndAmt);
                    }

                    //总额已用额度
                    apprLmtSubBasicInfo.setOutstndAmt(outstndAmt);
                    //敞口已用额度
                    apprLmtSubBasicInfo.setSpacOutstndAmt(spacOutstndAmt);
                    //用信余额
                    apprLmtSubBasicInfo.setLoanBalance(loanBalance);
                    //敞口用信余额
                    apprLmtSubBasicInfo.setLoanSpacBalance(loanSpacBalance);
                    //已出账金额
                    apprLmtSubBasicInfo.setPvpOutstndAmt(pvpOutstndAmt);
                    //可出账金额
                    apprLmtSubBasicInfo.setAvlOutstndAmt(avlOutstndAmt);
                    //累加金额
                    apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;
                    //累加金额处理
                    //comm4LmtCalFormulaUtils.calLmtAmtAddExecute(apprLmtSubBasicInfo.getApprSubSerno());
                    //上级编号
                    String parentId = apprLmtSubBasicInfo.getParentId() ;
                    if(StringUtils.isNotEmpty(parentId)){
                        apprLmtChgFiledListDtos.clear();
                        ApprLmtSubBasicInfo apprLmtSubBasicInfoOne = apprLmtSubBasicInfoService.selectByApprSubSerno(parentId);
                        if(apprLmtSubBasicInfoOne != null){
                            logger.info("一级分项【"+parentId+"】处理----------->start");
                            //已用用信总额
                            BigDecimal outstndAmtP = BigDecimalUtil.replaceNull(apprLmtSubBasicInfoOne.getOutstndAmt()) ;
                            apprLmtSubBasicInfoOne.setOutstndAmt(bizTotalAmtCny.add(outstndAmtP));

                            //已用敞口总额
                            BigDecimal spacOutstndAmtP = BigDecimalUtil.replaceNull(apprLmtSubBasicInfoOne.getSpacOutstndAmt()) ;
                            apprLmtSubBasicInfoOne.setSpacOutstndAmt(bizSpacAmtCny.add(spacOutstndAmtP));

                            //用信余额 判断该占用事台账占用还是合同占用，如果是合同占用，余额无变化，如果事台账占用，用信余额=用信余额 + 本次业务总额
                            if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                                //已出账金额
                                pvpOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfoOne.getPvpOutstndAmt()) ;
                                //可出账金额
                                avlOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfoOne.getAvlOutstndAmt()) ;
                                if(CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)){
                                    //已出账金额
                                    pvpOutstndAmt = pvpOutstndAmt.add(bizTotalAmtCny) ;
                                    //可出账金额
                                    avlOutstndAmt = avlOutstndAmt.subtract(bizTotalAmtCny) ;
                                }else{
                                    //已出账金额
                                    pvpOutstndAmt = pvpOutstndAmt.add(bizSpacAmtCny) ;
                                    //可出账金额
                                    avlOutstndAmt = avlOutstndAmt.subtract(bizSpacAmtCny) ;
                                }

                                //已出账金额
                                apprLmtSubBasicInfoOne.setPvpOutstndAmt(pvpOutstndAmt);
                                //可出账金额
                                apprLmtSubBasicInfoOne.setAvlOutstndAmt(avlOutstndAmt);
                                if(CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(bizStatus)){
                                    //用信余额
                                    loanBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfoOne.getLoanBalance()) ;
                                    //用信敞口余额
                                    loanSpacBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfoOne.getLoanSpacBalance()) ;
                                    //用信总额
                                    loanBalance = loanBalance.add(bizTotalAmtCny) ;
                                    //用信敞口余额
                                    loanSpacBalance = loanSpacBalance.add(bizTotalAmtCny) ;
                                    //用信余额
                                    apprLmtSubBasicInfoOne.setLoanBalance(loanBalance);
                                    //敞口用信余额
                                    apprLmtSubBasicInfoOne.setLoanSpacBalance(loanSpacBalance);
                                }
                                logger.info("【BizAttr】等于【2-台账】"+apprLmtSubBasicInfoOne.getApprSubSerno()+"处理后的金额--->用信余额:"+loanBalance+"， 用信敞口余额:"+loanSpacBalance+"， " +
                                        "已出账金额:"+pvpOutstndAmt+"， 可出账金额:"+avlOutstndAmt);
                            }
                            //TODO:此处是否需要考虑下，已出账金额等等
                            apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfoOne, resultMap) ;
                            //累加金额处理
                            //comm4LmtCalFormulaUtils.calLmtAmtAddExecute(apprLmtSubBasicInfoOne.getApprSubSerno());
                            logger.info("一级分项【"+parentId+"】处理----------->end");
                        }
                    }
                 }else if(CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType)){
                    logger.info("cmislmt0011 合作方额度占用开始.........");
                    //是否专业担保公司 1-是 0-否
                    String isProGuarCom = cmisLmt0011OccRelListDto.getIsProGuarCom() ;
                    //专业担保公司客户
                    String guarCusid = cmisLmt0011OccRelListDto.getGuarCusid() ;
                    //用信品种类型属性
                    String prdTypeProp =  cmisLmt0011OccRelListDto.getPrdTypeProp() ;
                    //合作方额度占用 查询合作方分项信息
                    ApprCoopSubInfo apprCoopSubInfo = null ;
                    if(CmisLmtConstants.YES_NO_Y.equals(isProGuarCom)){
                        //add by 20211006 防止担保公司重复占用，eg:推送占用列表中已经存在该担保公司的记录，又推送了记录时担保公司的信息，重复，此处只占用一次
                        if(comm4Service.check11ProGuarComRev(reqDto.getCmisLmt0011OccRelListDtoList(), guarCusid)){
                            continue;
                        }
                        logger.info("【{}】 专业担保公司额度获取分项信息【{}】", DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key ,isProGuarCom);
                        apprCoopSubInfo = apprCoopSubInfoService.selectSubInfoByCusIdAndPrdId(prdTypeProp, guarCusid) ;
                    }else{
                        apprCoopSubInfo = apprCoopSubInfoService.selectAcsiInfoBySubSerno(lmtSubno) ;
                    }
                    logger.info("【{}】 合作方占用信息：【{}】", DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key ,JSON.toJSONString(apprCoopSubInfo));
                    //检验 分项批复信息不允许未空
                    Optional.ofNullable(apprCoopSubInfo).orElseThrow(()->new YuspException(EclEnum.ECL070011.key, EclEnum.ECL070011.value)) ;

                    //校验合作方分项状态
                    if(!CmisLmtConstants.STD_ZB_APPR_ST_01.equals(apprCoopSubInfo.getStatus())){
                        throw new YuspException(EclEnum.ECL070003.key, EclEnum.ECL070003.value);
                    }
                    //增减已用额度
                    BigDecimal outstndAmt = BigDecimalUtil.replaceNull(cmisLmt0011OccRelListDto.getBizTotalAmtCny()) ;
                    //修改前
                    BigDecimal bfOutStndAmt = BigDecimalUtil.replaceNull(apprCoopSubInfo.getOutstndAmt()) ;
                    //修改后
                    BigDecimal afOutStndAmt = outstndAmt.add(bfOutStndAmt) ;
                    logger.info("合作方额度【"+apprCoopSubInfo.getApprSubSerno()+"】" +
                            "已用额度："+bfOutStndAmt+"，占用后已用额度总额："+ afOutStndAmt);
                    apprCoopSubInfo.setOutstndAmt(afOutStndAmt);
                    //更新合作方分项占用
                    apprCoopSubInfoService.updateByPKeyInApprLmtChgDetails(apprCoopSubInfo, resultMap);

                    logger.info("cmislmt0011 合作方额度占用结束.........");
                }else if(CmisLmtConstants.STD_ZB_LMT_TYPE_06.equals(lmtType) && CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                    //白名单额度 获取白名单额度
                    //查询白名单额度信息
                    LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfoService.selectLmtWhiteInfoBySubNo(lmtSubno) ;
                    Optional.ofNullable(lmtWhiteInfo).orElseThrow(()->new YuspException(EclEnum.ECL070009.key,EclEnum.ECL070009.value));
                    //已用限额   修改前
                    BigDecimal bfSigUseAmt = BigDecimalUtil.replaceNull(lmtWhiteInfo.getSigUseAmt()) ;
                    //修改后
                    BigDecimal afSigUseAmt = bizSpacAmtCny.add(bfSigUseAmt) ;
                    logger.info("白名单【"+lmtWhiteInfo.getSubAccNo()+"】已用限额更改："+bfSigUseAmt+" --> "+afSigUseAmt) ;
                    lmtWhiteInfo.setSigUseAmt(afSigUseAmt);
                    lmtWhiteInfoService.updateByPKeyInApprLmtChgDetails(lmtWhiteInfo, resultMap) ;
                }

                /*****合同占用分项数据落表处理*****/
                //数据拷贝
                BeanUtils.copyProperties(reqDto, lmtContRel);
                //字段不一样数据处理
                //主键
                lmtContRel.setPkId(comm4Service.generatePkId());
                //保证金比例
                lmtContRel.setSecurityRate(securityRate);
                //保证金金额
                lmtContRel.setSecurityAmt(securityAmt);
                //合同状态
                lmtContRel.setBizStatus(bizStatus);
                //分项编号
                lmtContRel.setLimitSubNo(cmisLmt0011OccRelListDto.getLmtSubNo());
                //额度类型
                lmtContRel.setLmtType(cmisLmt0011OccRelListDto.getLmtType());
                //合同金额
                lmtContRel.setBizAmt(reqDto.getDealBizAmt());
                //占用总金额（折人民币）
                lmtContRel.setBizTotalAmtCny(cmisLmt0011OccRelListDto.getBizTotalAmtCny());
                //占用敞口金额（折人民币）
                lmtContRel.setBizSpacAmtCny(bizSpacAmtCny);
                //占用总余额（折人民币）
                lmtContRel.setBizTotalBalanceAmtCny(cmisLmt0011OccRelListDto.getBizTotalAmtCny());
                //占用敞口余额（折人民币）
                lmtContRel.setBizSpacBalanceAmtCny(bizSpacBalanceAmtCny);
                //操作类型
                lmtContRel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                //更新人
                lmtContRel.setUpdId(reqDto.getInputId());
                //更新机构
                lmtContRel.setUpdBrId(reqDto.getInputBrId());
                //更新日期
                lmtContRel.setUpdDate(reqDto.getInputDate());
                //创建时间
                lmtContRel.setCreateTime(new Date());
                //更新时间
                lmtContRel.setUpdateTime(new Date());
                //更新时间
                lmtContRel.setPrdTypeProp(cmisLmt0011OccRelListDto.getPrdTypeProp());
                //输入落库
                lmtContRelMapper.insert(lmtContRel) ;
                //分项编号
                String apprSubSerno = cmisLmt0011OccRelListDto.getLmtSubNo() ;
                comm4Service.lmtAddAmtExecute(apprSubSerno, lmtType, dealBizType);
                /*if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType) || CmisLmtConstants.STD_ZB_LMT_TYPE_07.equals(lmtType)){
                    if(CmisLmtConstants.DEAL_BIZ_TYPE_3.equals(dealBizType)){
                        comm4LmtCalFormulaUtils.calParentIdLmtAmtAddExecute(cmisLmt0011OccRelListDto.getLmtSubNo());
                    }else{
                        comm4LmtCalFormulaUtils.calLmtAmtAddExecute(cmisLmt0011OccRelListDto.getLmtSubNo());
                        apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(cmisLmt0011OccRelListDto.getLmtSubNo());
                        if(Objects.nonNull(apprLmtSubBasicInfoService)) {
                            String parentId = apprLmtSubBasicInfo.getParentId() ;
                            comm4LmtCalFormulaUtils.calParentIdLmtAmtAddExecute(parentId);
                        }
                    }
                }*/
            }
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("合同校验接口报错：", e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.value);
        return resqDto;
    }

    /**
     * @方法名称: follwBizParam
     * @方法描述: 获取无缝衔接参数信息
     * @参数与返回说明: 返回无缝衔接参数信息
     * @算法描述: 无
     */
    public Map<String, String> follwBizParam(CmisLmt0011ReqDto reqDto){
        Map<String, String> paramMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
        return paramMap ;
    }
}