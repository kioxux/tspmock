package cn.com.yusys.yusp.web.server.cmislmt0055;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0055.req.CmisLmt0055ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0055.resp.CmisLmt0055RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0055.CmisLmt0055Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:获取客户有效的特定目的载体投资额度列表
 *
 * @author zhangjw 20210909
 * @version 1.0
 */
@Api(tags = "cmislmt0055:获取客户有效的特定目的载体投资额度列表")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0055Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0055Resource.class);

    @Autowired
    private CmisLmt0055Service cmisLmt0055Service;

    /**
     * 交易码：CmisLmt0055
     * 交易描述：获取客户有效的特定目的载体投资额度列表
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("获取客户有效的特定目的载体投资额度列表")
    @PostMapping("/cmislmt0055")
    protected @ResponseBody
    ResultDto<CmisLmt0055RespDto> CmisLmt0055(@Validated @RequestBody CmisLmt0055ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0055.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0055.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0055RespDto> CmisLmt0055RespDtoResultDto = new ResultDto<>();
        CmisLmt0055RespDto CmisLmt0055RespDto = new CmisLmt0055RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0055RespDto = cmisLmt0055Service.execute(reqDto);
            CmisLmt0055RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0055RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0055.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0055.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0055.value, e.getMessage());
            // 封装CmisLmt0055RespDtoResultDto中异常返回码和返回信息
            CmisLmt0055RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0055RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0055RespDto到CmisLmt0055RespDtoResultDto中
        CmisLmt0055RespDtoResultDto.setData(CmisLmt0055RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0055.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0055.value, JSON.toJSONString(CmisLmt0055RespDtoResultDto));
        return CmisLmt0055RespDtoResultDto;
    }
}