package cn.com.yusys.yusp.web.server.cmislmt0006;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.resp.CmisLmt0006RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0006.CmisLmt0006Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:额度冻结
 *
 * @author jiangyl 20210506
 * @version 1.0
 */
@Api(tags = "cmislmt0006:额度冻结")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0006Resource.class);

    @Autowired
    private CmisLmt0006Service cmisLmt0006Service;
    /**
     * 交易码：cmislmt0006
     * 交易描述：额度冻结
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("额度冻结")
    @PostMapping("/cmislmt0006")
    protected @ResponseBody
    ResultDto<CmisLmt0006RespDto> cmisLmt0006(@Validated @RequestBody CmisLmt0006ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0006.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0006.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0006RespDto> cmisLmt0006RespDtoResultDto = new ResultDto<>();
        CmisLmt0006RespDto cmisLmt0006RespDto = new CmisLmt0006RespDto();
        // 调用对应的service层
        try {
            cmisLmt0006RespDto = cmisLmt0006Service.frozen(reqDto);
            cmisLmt0006RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0006RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0006.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0006.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0006.value, e.getMessage());
            // 封装xddb0001DataResultDto中异常返回码和返回信息
            cmisLmt0006RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0006RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        cmisLmt0006RespDtoResultDto.setData(cmisLmt0006RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0006.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0006.value, JSON.toJSONString(cmisLmt0006RespDtoResultDto));
        return cmisLmt0006RespDtoResultDto;
    }
}
