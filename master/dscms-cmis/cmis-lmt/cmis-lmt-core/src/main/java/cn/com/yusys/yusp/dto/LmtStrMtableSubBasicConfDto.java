package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

public class LmtStrMtableSubBasicConfDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** PKID **/
    private String pkId;

    /** 额度结构编号 **/
    private String limitStrNo;

    /** 额度结构名称 **/
    private String limitStrName;

    /** 版本号 **/
    private String version;

    /** 版本控制方式 **/
    private String verCtrlMod;

    /** 适用机构类型 **/
    private String suitOrgType;

    /** 适用业务条线 **/
    private String suitApplicableLine;

    /** 是否公开授信 **/
    private String isOpenLmt;

    /** 是否唯有有效 **/
    private String isVld;

    /** 是否覆盖类额度 **/
    private String isCoverAmt;

    /** 是否启用 **/
    private String isBegin;

    /** 额度管控类型 **/
    private String limitControlType;

    /** 额度测算规则编号 **/
    private String limitMeasureRuleNo;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 更新人 **/
    private String updId;

    /** 更新日期 **/
    private String updDate;

    /** 操作类型 **/
    private String oprType;

    /** 父节点 **/
    private String parentId;

    /** 子节点 **/
    private String childId;

    /** 更新机构 **/
    private String updBrId;

    /** FK_PKID **/
    private String fkPkid;

    /** 额度分项基础信息-版本号 **/
    private String lmtSubBasicVersion;

    /** 额度分项编号 **/
    private String limitSubNo;

    /** 额度分项名称 **/
    private String limitSubName;

    /** 是否低风险授信 **/
    private String isLriskLmt;

    /** 授信分项类型 **/
    private String lmtSubType;

    /** 是否循环 **/
    private String isRevolv;

    /** 额度类型 **/
    private String limitType;

    /** 额度分项基础信息-管控类型 **/
    private String lmtSubBasicLimitControlType;

    /** 适用担保方式 **/
    private String suitGuarWay;

    /** 是否引入担保信息 **/
    private String isAddGuarInfo;

    /** 额度分项基础信息-适用业务条线 **/
    private String lmtSubBasicSuitApplicableLine;

    /** 风险系数 **/
    private java.math.BigDecimal riskCoef;

    /** 额度管控方式 **/
    private String limitControlMode;

    /** 额度切分方式 **/
    private String limitCutTyp;

    /** 共享/独占方式 **/
    private String shareTyp;

    /** 批复有效期（天） **/
    private Short apprIdate;

    /** 批复有效期操作符 **/
    private String apprIdateType;

    /** 批复有效期（日期） **/
    private String apprDate;

    /** 额度有效期（天） **/
    private Short lmtIdate;

    /** 额度有效期（日期） **/
    private String lmtDate;

    /** 额度有效期操作符 **/
    private String lmtIdateType;

    /** 业务有效期（天） **/
    private Short busiIdate;

    /** 业务有效期（日期） **/
    private String busiDare;

    /** 业务有效期操作符 **/
    private String busiIdateType;

    /** 额度宽限期（天） **/
    private Short lmtGraper;

    /** 业务期限管控方式 **/
    private String busiTermControlType;

    /** 审批系统 **/
    private String apprSys;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getLimitStrNo() {
        return limitStrNo;
    }

    public void setLimitStrNo(String limitStrNo) {
        this.limitStrNo = limitStrNo;
    }

    public String getLimitStrName() {
        return limitStrName;
    }

    public void setLimitStrName(String limitStrName) {
        this.limitStrName = limitStrName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVerCtrlMod() {
        return verCtrlMod;
    }

    public void setVerCtrlMod(String verCtrlMod) {
        this.verCtrlMod = verCtrlMod;
    }

    public String getSuitOrgType() {
        return suitOrgType;
    }

    public void setSuitOrgType(String suitOrgType) {
        this.suitOrgType = suitOrgType;
    }

    public String getSuitApplicableLine() {
        return suitApplicableLine;
    }

    public void setSuitApplicableLine(String suitApplicableLine) {
        this.suitApplicableLine = suitApplicableLine;
    }

    public String getIsOpenLmt() {
        return isOpenLmt;
    }

    public void setIsOpenLmt(String isOpenLmt) {
        this.isOpenLmt = isOpenLmt;
    }

    public String getIsVld() {
        return isVld;
    }

    public void setIsVld(String isVld) {
        this.isVld = isVld;
    }

    public String getIsCoverAmt() {
        return isCoverAmt;
    }

    public void setIsCoverAmt(String isCoverAmt) {
        this.isCoverAmt = isCoverAmt;
    }

    public String getIsBegin() {
        return isBegin;
    }

    public void setIsBegin(String isBegin) {
        this.isBegin = isBegin;
    }

    public String getLimitControlType() {
        return limitControlType;
    }

    public void setLimitControlType(String limitControlType) {
        this.limitControlType = limitControlType;
    }

    public String getLimitMeasureRuleNo() {
        return limitMeasureRuleNo;
    }

    public void setLimitMeasureRuleNo(String limitMeasureRuleNo) {
        this.limitMeasureRuleNo = limitMeasureRuleNo;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getFkPkid() {
        return fkPkid;
    }

    public void setFkPkid(String fkPkid) {
        this.fkPkid = fkPkid;
    }

    public String getLmtSubBasicVersion() {
        return lmtSubBasicVersion;
    }

    public void setLmtSubBasicVersion(String lmtSubBasicVersion) {
        this.lmtSubBasicVersion = lmtSubBasicVersion;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public String getIsLriskLmt() {
        return isLriskLmt;
    }

    public void setIsLriskLmt(String isLriskLmt) {
        this.isLriskLmt = isLriskLmt;
    }

    public String getLmtSubType() {
        return lmtSubType;
    }

    public void setLmtSubType(String lmtSubType) {
        this.lmtSubType = lmtSubType;
    }

    public String getIsRevolv() {
        return isRevolv;
    }

    public void setIsRevolv(String isRevolv) {
        this.isRevolv = isRevolv;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    public String getLmtSubBasicLimitControlType() {
        return lmtSubBasicLimitControlType;
    }

    public void setLmtSubBasicLimitControlType(String lmtSubBasicLimitControlType) {
        this.lmtSubBasicLimitControlType = lmtSubBasicLimitControlType;
    }

    public String getSuitGuarWay() {
        return suitGuarWay;
    }

    public void setSuitGuarWay(String suitGuarWay) {
        this.suitGuarWay = suitGuarWay;
    }

    public String getIsAddGuarInfo() {
        return isAddGuarInfo;
    }

    public void setIsAddGuarInfo(String isAddGuarInfo) {
        this.isAddGuarInfo = isAddGuarInfo;
    }

    public String getLmtSubBasicSuitApplicableLine() {
        return lmtSubBasicSuitApplicableLine;
    }

    public void setLmtSubBasicSuitApplicableLine(String lmtSubBasicSuitApplicableLine) {
        this.lmtSubBasicSuitApplicableLine = lmtSubBasicSuitApplicableLine;
    }

    public BigDecimal getRiskCoef() {
        return riskCoef;
    }

    public void setRiskCoef(BigDecimal riskCoef) {
        this.riskCoef = riskCoef;
    }

    public String getLimitControlMode() {
        return limitControlMode;
    }

    public void setLimitControlMode(String limitControlMode) {
        this.limitControlMode = limitControlMode;
    }

    public String getLimitCutTyp() {
        return limitCutTyp;
    }

    public void setLimitCutTyp(String limitCutTyp) {
        this.limitCutTyp = limitCutTyp;
    }

    public String getShareTyp() {
        return shareTyp;
    }

    public void setShareTyp(String shareTyp) {
        this.shareTyp = shareTyp;
    }

    public Short getApprIdate() {
        return apprIdate;
    }

    public void setApprIdate(Short apprIdate) {
        this.apprIdate = apprIdate;
    }

    public String getApprIdateType() {
        return apprIdateType;
    }

    public void setApprIdateType(String apprIdateType) {
        this.apprIdateType = apprIdateType;
    }

    public String getApprDate() {
        return apprDate;
    }

    public void setApprDate(String apprDate) {
        this.apprDate = apprDate;
    }

    public Short getLmtIdate() {
        return lmtIdate;
    }

    public void setLmtIdate(Short lmtIdate) {
        this.lmtIdate = lmtIdate;
    }

    public String getLmtDate() {
        return lmtDate;
    }

    public void setLmtDate(String lmtDate) {
        this.lmtDate = lmtDate;
    }

    public String getLmtIdateType() {
        return lmtIdateType;
    }

    public void setLmtIdateType(String lmtIdateType) {
        this.lmtIdateType = lmtIdateType;
    }

    public Short getBusiIdate() {
        return busiIdate;
    }

    public void setBusiIdate(Short busiIdate) {
        this.busiIdate = busiIdate;
    }

    public String getBusiDare() {
        return busiDare;
    }

    public void setBusiDare(String busiDare) {
        this.busiDare = busiDare;
    }

    public String getBusiIdateType() {
        return busiIdateType;
    }

    public void setBusiIdateType(String busiIdateType) {
        this.busiIdateType = busiIdateType;
    }

    public Short getLmtGraper() {
        return lmtGraper;
    }

    public void setLmtGraper(Short lmtGraper) {
        this.lmtGraper = lmtGraper;
    }

    public String getBusiTermControlType() {
        return busiTermControlType;
    }

    public void setBusiTermControlType(String busiTermControlType) {
        this.busiTermControlType = busiTermControlType;
    }

    public String getApprSys() {
        return apprSys;
    }

    public void setApprSys(String apprSys) {
        this.apprSys = apprSys;
    }
}
