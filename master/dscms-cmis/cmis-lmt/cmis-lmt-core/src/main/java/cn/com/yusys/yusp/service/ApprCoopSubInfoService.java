/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ApprCoopSubInfo;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import cn.com.yusys.yusp.service.server.cmislmt0009.CmisLmt0009Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopSubInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: lenovo
 * @创建时间: 2021-04-20 21:28:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ApprCoopSubInfoService {

    private static final Logger logger = LoggerFactory.getLogger(ApprCoopSubInfoService.class);

    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ApprCoopSubInfo selectByPrimaryKey(String pkId) {
        return apprCoopSubInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ApprCoopSubInfo> selectAll(QueryModel model) {
        List<ApprCoopSubInfo> records = (List<ApprCoopSubInfo>) apprCoopSubInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<ApprCoopSubInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ApprCoopSubInfo> list = apprCoopSubInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ApprCoopSubInfo record) {
        return apprCoopSubInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ApprCoopSubInfo record) {
        return apprCoopSubInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ApprCoopSubInfo record) {
        return apprCoopSubInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ApprCoopSubInfo record) {
        return apprCoopSubInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return apprCoopSubInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return apprCoopSubInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<ApprCoopSubInfo> selectAcsiInfoBySerno(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<ApprCoopSubInfo> mapList = apprCoopSubInfoMapper.selectAcsiInfoBySerno(queryModel);
        PageHelper.clearPage();
        return mapList;
    }

    /**
     * @方法名称: selectApprCoopSubInfoAndBasicBySerno
     * @方法描述: 关联合作方额度台账表，查询合作方额度分项信息
     * @参数与返回说明:
     * @算法描述: 无
     * 2021-04-25
     */
    public List<Map<String, Object>> selectApprCoopSubInfoAndBasicBySerno(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = apprCoopSubInfoMapper.selectApprCoopSubInfoAndBasicBySerno(queryModel);
        PageHelper.clearPage();
        return mapList;
    }

    /**
     * @方法名称: queryListByCusIdAndCopType
     * @方法描述: 合作方额度视图详情-分项明细
     * @参数与返回说明:
     * @算法描述: 无
     * 2021-05-06
     */
    public List<Map<String, Object>> queryListByCusIdAndCopType(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = apprCoopSubInfoMapper.queryListByCusIdAndCopType(queryModel);
        for (Map<String, Object> stringObjectMap : mapList) {
            String lmtBizTypeProp = (String)stringObjectMap.get("lmtBizTypeProp") ;
            if(StringUtils.isNotEmpty(lmtBizTypeProp)){
                String lmtBizTypePropName = DictTranslatorUtils.findValueByDictKey("STD_PRD_TYPE_PROP", lmtBizTypeProp);
                stringObjectMap.put("limitSubName",lmtBizTypePropName) ;
            }
        }
        PageHelper.clearPage();
        return mapList;
    }


    /**
     * @方法名称: selectAcsiInfoBySubSerno
     * @方法描述: 根据分项编号，获取分项信息，分项信息操作状态为01
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprCoopSubInfo selectAcsiInfoBySubSerno(String subSerno) {
        QueryModel queryModel = new QueryModel() ;
        //分项编号
        queryModel.addCondition("apprSubSerno", subSerno);
        //操作状态为新增-01
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //查询分项
        List<ApprCoopSubInfo> apprCoopSubInfos = apprCoopSubInfoMapper.selectByModel(queryModel);
        if(CollectionUtils.isNotEmpty(apprCoopSubInfos)){
            return apprCoopSubInfos.get(0) ;
        }
        return null ;
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据分项编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprCoopSubInfo selectBySubSerno(String subserno) {
        return apprCoopSubInfoMapper.selectBySubSerno(subserno);
    }

    /**
     * @方法名称: selectAppLmtSubBasicInfoByLmtSubNo
     * @方法描述: 根据分项编号，获取分项批复信息，不考虑状态
     * @参数与返回说明: 存在：返回分项批复信息，不存在返回空
     * @算法描述: 无
     */
    public ApprCoopSubInfo selectApprCoopSubInfoByLmtSubNo(String lmtSubNo) {
        //根据父级编号，查询分项信息
        QueryModel queryModel = new QueryModel();
        //分项编号
        queryModel.addCondition("apprSubSerno", lmtSubNo);
        //批复信息
        List<ApprCoopSubInfo> apprCoopSubInfoList = apprCoopSubInfoMapper.selectByModel(queryModel);
        if(CollectionUtils.isNotEmpty(apprCoopSubInfoList)){
            return apprCoopSubInfoList.get(0) ;
        }
        return null ;
    }

    /**
     * 根据批复台账编号查询对应的授信总额之和和已用总额之和
     * @param apprSerno
     * @return
     */
    public Map<String, BigDecimal> selectTotalAmtByApprSerno(String apprSerno){
        return apprCoopSubInfoMapper.selectTotalAmtByApprSerno(apprSerno);
    }


    /**
     * @作者:lizx
     * @方法名称: selectSubInfoByCusIdAndPrdId
     * @方法描述: 根据担保合同客户号，和担保合同产品号，获取担保合同对应的分项额度信息
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/5 13:58
     * @param lmtBizTypeProp: 额度产品产品编号属性
     * @param cusId: 担保公司客户号
     * @return: cn.com.yusys.yusp.domain.ApprCoopSubInfo
     * @算法描述: 无
    */
    public ApprCoopSubInfo selectSubInfoByCusIdAndPrdId(String lmtBizTypeProp, String cusId) {
        logger.info("专业担保公司{}，根据客户产品属性{} 查询分项额度信息------start", cusId, lmtBizTypeProp);
        //根据父级编号，查询分项信息
        QueryModel queryModel = new QueryModel();
        //分项额度产品编号
        queryModel.addCondition("lmtBizTypeProp", lmtBizTypeProp);
        //担保公司额度编号
        queryModel.addCondition("cusId", cusId);
        //批复信息
        ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoMapper.selectSubInfoByCusIdAndPrdId(queryModel);
        logger.info("专业担保公司,查询分项额度信息{}------end", JSON.toJSONString(apprCoopSubInfo));
        return apprCoopSubInfo;
    }

    /**
     * @方法名称: selectAccSubNoByApprSerno
     * @方法描述: 根据批复编号查询批复向下分项编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String selectAccSubNoByApprSerno(String apprSerno){
        return apprCoopSubInfoMapper.selectAccSubNoByApprSerno(apprSerno) ;
    }

    /**
     * @方法名称: selectAccSubNoByApprSerno
     * @方法描述: 根据批复编号查询批复向下分项编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateBySubSernoInApprLmtChgDetails(ApprCoopSubInfo newAppr, Map<String, String> paramMap){
        return apprCoopSubInfoMapper.updateByApprSubSernoSelective(newAppr);
    }


    /**
     * @方法名称: selectAccSubNoByApprSerno
     * @方法描述: 根据批复编号查询批复向下分项编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateByPKeyInApprLmtChgDetails(ApprCoopSubInfo newAppr, Map<String, String> paramMap){
        return apprCoopSubInfoMapper.updateByPrimaryKey(newAppr);
    }

    /**
     * @方法名称: selectCusIdByApprSubSerno
     * @方法描述: 根据分项编号，查询合作方客户号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String selectCusIdByApprSubSerno(String apprSerno){
        return apprCoopSubInfoMapper.selectCusIdByApprSubSerno(apprSerno) ;
    }

}
