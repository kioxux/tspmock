/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.ReflectionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.DeRiskIndexCfgHis;
import cn.com.yusys.yusp.domain.DmRisKhfxJgbxJk;
import cn.com.yusys.yusp.repository.mapper.DmRisKhfxJgbxJkMapper;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import cn.com.yusys.yusp.vo.RiskExpose01ExportVo;
import cn.com.yusys.yusp.vo.RiskExpose02ExportVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: DmRisKhfxJgbxJkService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-25 15:50:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DmRisKhfxJgbxJkService {

    private static final Logger logger = LoggerFactory.getLogger(DmRisKhfxJgbxJkService.class);

    @Autowired
    private DmRisKhfxJgbxJkMapper dmRisKhfxJgbxJkMapper;

    /**
     * DE_RISK_INDEX_CFG_HIS
     */
    @Autowired
    private DeRiskIndexCfgHisService deRiskIndexCfgHisService;


    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 通用当前时间（yyyy-MM-dd） form openDay
     * @return
     */
    public String getCurrrentDateStr(){
        return stringRedisTemplate.opsForValue().get("openDay");
    }
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public DmRisKhfxJgbxJk selectByPrimaryKey(String dataDt, String custTypeId, String custId) {
        return dmRisKhfxJgbxJkMapper.selectByPrimaryKey(dataDt, custTypeId, custId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<DmRisKhfxJgbxJk> selectAll(QueryModel model) {
        List<DmRisKhfxJgbxJk> records = (List<DmRisKhfxJgbxJk>) dmRisKhfxJgbxJkMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<DmRisKhfxJgbxJk> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DmRisKhfxJgbxJk> list = dmRisKhfxJgbxJkMapper.selectByModel(model);

        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(DmRisKhfxJgbxJk record) {
        return dmRisKhfxJgbxJkMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(DmRisKhfxJgbxJk record) {
        return dmRisKhfxJgbxJkMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(DmRisKhfxJgbxJk record) {
        return dmRisKhfxJgbxJkMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(DmRisKhfxJgbxJk record) {
        return dmRisKhfxJgbxJkMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dataDt, String custTypeId, String custId) {
        return dmRisKhfxJgbxJkMapper.deleteByPrimaryKey(dataDt, custTypeId, custId);
    }

    /**
     * 单一客户风险暴露查询
     * @param queryModel
     * @return
     */
    public List<Map> selectSingleCusList(QueryModel queryModel) {
        QueryModel model = new QueryModel();
        model.setCondition(JSON.toJSONString(queryModel.getCondition()));
        //1.查询条件【日期】如果为空，则默认获取上一日的数据；若【日期】不为空，则依照录入日期查询数据；
        String dataDt = (String) model.getCondition().get("dataDt");
        if (StringUtils.isBlank(dataDt)){
            Date curOpenDay = DateUtils.parseDate(getCurrrentDateStr(), DateFormatEnum.DEFAULT.getValue());
            dataDt = DateUtils.formatDate(DateUtils.addDay(curOpenDay,-1),DateFormatEnum.DEFAULT.getValue());
        }
        String custTypeId = (String) model.getCondition().get("custTypeId");
        String cusId = (String) model.getCondition().get("cusId");
        //2.根据【日期】，获取指标当日 cmis_lmt.DE_RISK_INDEX_CFG_HIS大额风险暴露-风险暴露指标配置历史表
        List<DeRiskIndexCfgHis> deRiskIndexCfgHisList = deRiskIndexCfgHisService.selectByDataDt(dataDt);
        if (CollectionUtils.isEmpty(deRiskIndexCfgHisList)){
            logger.error("指标限额要求获取失败！日期为："+dataDt);
            return new ArrayList<>();
        }
        //大额风险暴露-客户维度风险暴露汇总
        List<DmRisKhfxJgbxJk> dmRisKhfxJgbxJks = selectAll(model);
        //获取对应指标值
        List<Map> pageList = new ArrayList<>();
        //大额风险暴露指标类型(行标题)  === 指标为左表，获取  指标值 = 对应客户类型的 指标值   汇总
        List<String> deRiskTypeList = getDeRiskTypeList(custTypeId);
        for (String deRiskType : deRiskTypeList) {
            Map obj = new HashMap();
            //指标名称
            obj.put("deRiskType",deRiskType);

            DeRiskIndexCfgHis deRiskIndexCfgHis = deRiskIndexCfgHisList.stream().filter(a -> deRiskType.equals(a.getRiskType())).findFirst().orElse(null);
            if (deRiskIndexCfgHis == null){
                throw BizException.error(null,"999999","日期为【"+dataDt+"】的大额风险配置获取失败！");
            }
            //指标限额要求（%）
            BigDecimal riskIndexReq = deRiskIndexCfgHis.getRiskIndexReq();
            obj.put("riskIndexReq",riskIndexReq);
            //指标值
            BigDecimal zbLmt = getZbLmt(dmRisKhfxJgbxJks,cusId,deRiskType);
            obj.put("zbLmt",zbLmt);
            //授信总额
            BigDecimal sumSxLmt = getSumLmt(dmRisKhfxJgbxJks,deRiskType,"sx");
            obj.put("sumSxLmt",sumSxLmt);
            //用信余额
            BigDecimal sumYxLmt = getSumLmt(dmRisKhfxJgbxJks,deRiskType,"yx");
            obj.put("sumYxLmt",sumYxLmt);
            //指标日期
            obj.put("zbDate",dataDt);

            String color = "black";
            //资本净额(历史配置表)
            BigDecimal sumZbjeAmt = deRiskIndexCfgHis.getZbjeAmt();
            //一级资本净额(历史配置表)
            BigDecimal sumYjzbjeAmt = deRiskIndexCfgHis.getYjzbjeAmt();
            //非同业单一客户贷款余额占比   资本净额计算*(黄区阈值或红色阈值)
            if (CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_01.riskType.equals(deRiskType)){
                if (zbLmt.compareTo(sumZbjeAmt.multiply(deRiskIndexCfgHis.getRiskYellowReq()))>0){
                    color = "yellow";
                }
                if (zbLmt.compareTo(sumZbjeAmt.multiply(deRiskIndexCfgHis.getRiskRedReq()))>0){
                    color = "red";
                }
            }else{
                //其他是用一级资本净额计算*(黄区阈值或红色阈值)
                if (zbLmt.compareTo(sumYjzbjeAmt.multiply(deRiskIndexCfgHis.getRiskYellowReq()))>0){
                    color = "yellow";
                }
                if (zbLmt.compareTo(sumYjzbjeAmt.multiply(deRiskIndexCfgHis.getRiskRedReq()))>0){
                    color = "red";
                }
            }
            obj.put("color",color);

            pageList.add(obj);
        }
        return pageList;
    }


    /**
     * 单一指标风险暴露情况
     * @param queryModel
     * @return
     */
    public ResultDto selectSingleZbList(QueryModel queryModel){
        //1.查询条件【日期】如果为空，则默认获取上一日的数据；若【日期】不为空，则依照录入日期查询数据；
        String dataDt = (String) queryModel.getCondition().get("dataDt");
        if (StringUtils.isBlank(dataDt)){
            Date curOpenDay = DateUtils.parseDate(getCurrrentDateStr(), DateFormatEnum.DEFAULT.getValue());
            dataDt = DateUtils.formatDate(DateUtils.addDay(curOpenDay,-1),DateFormatEnum.DEFAULT.getValue());
            queryModel.addCondition("dataDt",dataDt);
        }
        List<Map> pageList = new ArrayList<>();
        //2.若查询条件【指标名称】为空，则默认展示空列表；当【指标名称】有查询条件时，才做查询
        String riskType = (String) queryModel.getCondition().get("riskType");
        String custId = (String) queryModel.getCondition().get("custId");
        if (StringUtils.isBlank(riskType)){
            ResultDto resultDto = new ResultDto();
            resultDto.setData(pageList);
            resultDto.setTotal(0);
            return resultDto;
        }
        //获取指标值区间
        BigDecimal minZbLmt = BigDecimalUtil.replaceNull(queryModel.getCondition().get("minZbLmt"));
        BigDecimal maxZbLmt = BigDecimalUtil.replaceNull(queryModel.getCondition().get("maxZbLmt"));
        if (minZbLmt.compareTo(BigDecimal.ZERO)<0 || maxZbLmt.compareTo(BigDecimal.ZERO) <0){
            throw BizException.error(null,"9999","指标值搜索条件不能为负数");
        }
        if (minZbLmt.compareTo(maxZbLmt) >= 0 && maxZbLmt.compareTo(BigDecimal.ZERO) >0) {
            throw BizException.error(null,"9999","指标值区间最大值必须大于最小值！");
        }
        //3.根据【日期】，获取指标当日 cmis_lmt.DE_RISK_INDEX_CFG_HIS大额风险暴露-风险暴露指标配置历史表
        DeRiskIndexCfgHis deRiskIndexCfgHis = deRiskIndexCfgHisService.selectByPrimaryKey(dataDt,riskType);
//        if (CollectionUtils.isEmpty(deRiskIndexCfgHisList)){
//            return new ArrayList<>();
//        }
//        DeRiskIndexCfgHis deRiskIndexCfgHis = deRiskIndexCfgHisList.stream().filter(a -> riskType.equals(a.getRiskType())).findFirst().orElse(null);
        if (deRiskIndexCfgHis == null){
//            throw BizException.error(null,"999999","日期为【"+dataDt+"】的大额风险配置获取失败！");
            ResultDto resultDto = new ResultDto();
            resultDto.setData(pageList);
            resultDto.setTotal(0);
            resultDto.setCode(ResultDto.success().getCode());
            resultDto.setMessage("日期为【"+dataDt+"】的大额风险配置获取失败！");
            return resultDto;
        }
        //客户类型
        String cusTypeId = CmisLmtConstants.STD_DE_RISK_TYPE.matchCusTypeId(riskType);
        queryModel.addCondition("custTypeId",cusTypeId);
        queryModel.addCondition("custId",custId);
        queryModel.addCondition("minZbLmt",minZbLmt);
        queryModel.addCondition("maxZbLmt",maxZbLmt);
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<DmRisKhfxJgbxJk> dmRisKhfxJgbxJks = dmRisKhfxJgbxJkMapper.selectByModel(queryModel);
        PageHelper.clearPage();
        Long getTotal = NumberUtils.toLong(ReflectionUtils.getMethodValue(dmRisKhfxJgbxJks, "getTotal"), 0L);
        System.err.println("total===>"+getTotal);
        for (DmRisKhfxJgbxJk dmRisKhfxJgbxJk : dmRisKhfxJgbxJks) {
            Map map = new HashMap();
            map.put("custTypeId",dmRisKhfxJgbxJk.getCustTypeId());
            //客户编号
            String custId1 = dmRisKhfxJgbxJk.getCustId();
            map.put("custId",custId1);
            //客户名称
            String custName = dmRisKhfxJgbxJk.getCustName();
            map.put("custName",custName);
            //指标值（非同业单一客户贷款余额(CONT_LON_BAL) + 非同业单一客户风险暴露(FTYDY_KHMX_EXPO)
            //          +  非同业关联客户风险暴露(FTYGL_KHMX_EXPO) +同业单一客户风险暴露(TYDY_KHMX_EXPO)
            //          + 同业关联客户风险暴露(TYGL_KHMX_EXPO) + 匿名客户风险暴露(MMKH_KHMX_EXPO)）
            BigDecimal zbLmt = dmRisKhfxJgbxJk.getContLonBal().add(dmRisKhfxJgbxJk.getFtydyKhmxExpo())
                    .add(dmRisKhfxJgbxJk.getFtyglKhmxExpo()).add(dmRisKhfxJgbxJk.getTydyKhmxExpo())
                    .add(dmRisKhfxJgbxJk.getTyglKhmxExpo()).add(dmRisKhfxJgbxJk.getMmkhKhmxExpo());
            map.put("zbLmt",zbLmt);
            //授信总额
            BigDecimal sumSxLmt = dmRisKhfxJgbxJk.getAcctBalzSx();
            map.put("sumSxLmt",sumSxLmt);
            //用信余额
            BigDecimal sumYxLmt = dmRisKhfxJgbxJk.getAcctBalzYx();
            map.put("sumYxLmt",sumYxLmt);

            //资本净额(历史配置表)
            BigDecimal sumZbjeAmt = deRiskIndexCfgHis.getZbjeAmt();
            map.put("sumZbjeAmt",sumZbjeAmt);
            //一级资本净额(历史配置表)
            BigDecimal sumYjzbjeAmt = deRiskIndexCfgHis.getYjzbjeAmt();
            map.put("sumYjzbjeAmt",sumYjzbjeAmt);
            //黄色阈值
            map.put("riskYellowReqAmt",sumZbjeAmt.multiply(deRiskIndexCfgHis.getRiskYellowReq()));
            //红色阈值
            map.put("riskRedReqAmt",sumZbjeAmt.multiply(deRiskIndexCfgHis.getRiskRedReq()));
            //指标阈值
            map.put("riskIndexReq",deRiskIndexCfgHis.getRiskIndexReq());

            map.put("zbDate",deRiskIndexCfgHis.getDataDt());

            //颜色
            String color = "black";
            //非同业单一客户贷款余额占比   资本净额计算*(黄区阈值或红色阈值)
            if (CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_01.riskType.equals(riskType)){
                if (zbLmt.compareTo(sumZbjeAmt.multiply(deRiskIndexCfgHis.getRiskYellowReq()))>0){
                    color = "yellow";
                }
                if (zbLmt.compareTo(sumZbjeAmt.multiply(deRiskIndexCfgHis.getRiskRedReq()))>0){
                    color = "red";
                }
            }else{
                //其他是用一级资本净额计算*(黄区阈值或红色阈值)
                if (zbLmt.compareTo(sumYjzbjeAmt.multiply(deRiskIndexCfgHis.getRiskYellowReq()))>0){
                    color = "yellow";
                }
                if (zbLmt.compareTo(sumYjzbjeAmt.multiply(deRiskIndexCfgHis.getRiskRedReq()))>0){
                    color = "red";
                }
            }
            map.put("color",color);

            //指标值区间过滤
//            boolean isAdd = checkZbLmtFilter(zbLmt,minZbLmt,maxZbLmt);
//            if (isAdd){
//                pageList.add(map);
//            }
            pageList.add(map);
        }
        ResultDto resultDto = new ResultDto();
        resultDto.setData(pageList);
        resultDto.setTotal(getTotal);
        return resultDto;
    }

    public boolean checkZbLmtFilter(BigDecimal zbLmt, BigDecimal minZbLmt, BigDecimal maxZbLmt) {
        boolean isAdd = false;
        if (minZbLmt.compareTo(BigDecimal.ZERO) == 0 && maxZbLmt.compareTo(BigDecimal.ZERO) == 0) {
            isAdd = true;
        }
        if (maxZbLmt.compareTo(BigDecimal.ZERO) == 0 && zbLmt.compareTo(minZbLmt) >= 0) {
            isAdd = true;
        }
        if (minZbLmt.compareTo(BigDecimal.ZERO) == 0 && zbLmt.compareTo(maxZbLmt) <= 0) {
            isAdd = true;
        }
        if (zbLmt.compareTo(minZbLmt) >= 0 && zbLmt.compareTo(maxZbLmt) <= 0) {
            isAdd = true;
        }
        return isAdd;
    }

    /**
     * 单一客户风险暴露查询xls导出
     * @param model
     * @return
     */
    public ProgressDto asyncExportRiskExpose01(QueryModel model) {
        QueryModel queryModel = new QueryModel();
        queryModel.setCondition(JSONObject.toJSON(model.getCondition()).toString());
        queryModel.setSize(1);
        queryModel.setPage(1);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            if (page != 1) {
                return new ArrayList<>();
            }
            List<RiskExpose01ExportVo> list = new ArrayList<>();
            QueryModel queryModeTemp = (QueryModel)object;
            List<Map> maps = selectSingleCusList(queryModeTemp);
            if (CollectionUtils.nonEmpty(maps)){
                for (Map map : maps) {
                    RiskExpose01ExportVo riskExpose01ExportVo = new RiskExpose01ExportVo();
                    BeanUtils.copyProperties(map,riskExpose01ExportVo);
                    riskExpose01ExportVo.setTypeName(map.get("deRiskType").toString());
                    riskExpose01ExportVo.setRiskIndexReq(((BigDecimal) map.get("riskIndexReq")).multiply(BigDecimal.valueOf(100)));
                    riskExpose01ExportVo.setSumSxLmt(((BigDecimal) map.get("sumSxLmt")).divide(BigDecimal.valueOf(10000)));
                    riskExpose01ExportVo.setSumYxLmt(((BigDecimal) map.get("sumYxLmt")).divide(BigDecimal.valueOf(10000)));
                    riskExpose01ExportVo.setZbLmt(((BigDecimal) map.get("zbLmt")).divide(BigDecimal.valueOf(10000)));
                    riskExpose01ExportVo.setZbDate(map.get("zbDate").toString());
                    list.add(riskExpose01ExportVo);
                }
            }
            return list;
        };
        ExportContext exportContext = ExportContext.of(RiskExpose01ExportVo.class)
                .exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, queryModel);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 单一指标风险暴露查询xls导出
     * @param model
     * @return
     */
    public ProgressDto asyncExportRiskExpose02(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            List<RiskExpose02ExportVo> list = new ArrayList<>();
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            ResultDto resultDto = selectSingleZbList(queryModeTemp);
            List<Map> maps = (List<Map>) resultDto.getData();
            if (CollectionUtils.nonEmpty(maps)){
                for (Map map : maps) {
                    RiskExpose02ExportVo riskExpose02ExportVo = new RiskExpose02ExportVo();
                    riskExpose02ExportVo.setCustName(map.get("custName").toString());
                    riskExpose02ExportVo.setCustId(map.get("custId").toString());
                    BigDecimal sumSxLmt = (BigDecimal) map.get("sumSxLmt");
                    riskExpose02ExportVo.setSumLmt(sumSxLmt.divide(BigDecimal.valueOf(10000)));
                    BigDecimal sumYxLmt = (BigDecimal) map.get("sumYxLmt");
                    riskExpose02ExportVo.setSumUseLmt(sumYxLmt.divide(BigDecimal.valueOf(10000)));
                    BigDecimal zbLmt = (BigDecimal) map.get("zbLmt");
                    riskExpose02ExportVo.setZbLmt(zbLmt.divide(BigDecimal.valueOf(10000)));
                    riskExpose02ExportVo.setZbDate(map.get("zbDate").toString());
                    list.add(riskExpose02ExportVo);
                }
            }
            return list;
        };
        ExportContext exportContext = ExportContext.of(RiskExpose02ExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }


    private List<String> getDeRiskTypeList(String custTypeId) {
        List<String> deRiskTypeList = new ArrayList<>();
        //01-非同业单一客户贷款余额占比       ----------->    01-非同业单一客户
        //02-非同业单一客户风险暴露占比       ----------->    01-非同业单一客户
        if (StringUtils.isBlank(custTypeId) || CmisLmtConstants.STD_DE_CUS_TYPE_01.equals(custTypeId)){
            deRiskTypeList.add(CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_01.riskType);
            deRiskTypeList.add(CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_02.riskType);
        }

        //03-非同业关联客户风险暴露占比       ----------->    02-非同业关联客户
        if (StringUtils.isBlank(custTypeId) || CmisLmtConstants.STD_DE_CUS_TYPE_02.equals(custTypeId)){
            deRiskTypeList.add(CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_03.riskType);
        }

        //04-同业单一客户风险暴露占比         ----------->    03-同业单一客户
        if (StringUtils.isBlank(custTypeId) || CmisLmtConstants.STD_DE_CUS_TYPE_03.equals(custTypeId)){
            deRiskTypeList.add(CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_04.riskType);
        }

        //05-同业关联客户风险暴露占比         ----------->    04-同业关联客户
        if (StringUtils.isBlank(custTypeId) || CmisLmtConstants.STD_DE_CUS_TYPE_04.equals(custTypeId)){
            deRiskTypeList.add(CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_05.riskType);
        }

        //06-匿名客户风险暴露占比             ----------->    05-匿名客户
        if (StringUtils.isBlank(custTypeId) || CmisLmtConstants.STD_DE_CUS_TYPE_05.equals(custTypeId)){
            deRiskTypeList.add(CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_06.riskType);
        }

        //07-合格中央交易对手非清算风险暴露占比 ----------->  07-中央交易对手
        if (StringUtils.isBlank(custTypeId) || CmisLmtConstants.STD_DE_CUS_TYPE_07.equals(custTypeId)){
            deRiskTypeList.add(CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_07.riskType);
        }

        //08-场外衍生交易名义本金对总资产占比 ----------->    默认为0，不做计算
        if (StringUtils.isBlank(custTypeId)){
            deRiskTypeList.add(CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_08.riskType);
        }

        return deRiskTypeList;
    }


    public BigDecimal getSumLmt(List<DmRisKhfxJgbxJk> dmRisKhfxJgbxJks, String deRiskType, String type) {
        BigDecimal sumLmt = BigDecimal.ZERO;
        String cusTypeId = CmisLmtConstants.STD_DE_RISK_TYPE.matchCusTypeId(deRiskType);
        if (CollectionUtils.isEmpty(dmRisKhfxJgbxJks) || CmisLmtConstants.STD_DE_CUS_TYPE_0.equals(cusTypeId)) {
            return sumLmt;
        }
        //授信总额
        if ("sx".equals(type)) {
            sumLmt = dmRisKhfxJgbxJks.stream().filter(a -> cusTypeId.equals(a.getCustTypeId())).map(DmRisKhfxJgbxJk::getAcctBalzSx).reduce(sumLmt, BigDecimal::add);
        }
        //用信余额
        if ("yx".equals(type)) {
            sumLmt = dmRisKhfxJgbxJks.stream().filter(a -> cusTypeId.equals(a.getCustTypeId())).map(DmRisKhfxJgbxJk::getAcctBalzYx).reduce(sumLmt, BigDecimal::add);
        }
        //资本净额
        if ("zbje".equals(type)){
            sumLmt = dmRisKhfxJgbxJks.stream().filter(a -> cusTypeId.equals(a.getCustTypeId())).map(DmRisKhfxJgbxJk::getZbjeAmt).reduce(sumLmt, BigDecimal::add);
        }
        //一级资本净额
        if ("yjzbje".equals(type)){
            sumLmt = dmRisKhfxJgbxJks.stream().filter(a -> cusTypeId.equals(a.getCustTypeId())).map(DmRisKhfxJgbxJk::getYjzbjeAmt).reduce(sumLmt, BigDecimal::add);
        }
        return sumLmt;
    }

    /**
     * 根据大额风险暴露指标类型 获取指标值
     * @param dmRisKhfxJgbxJks
     * @param cusId
     * @param deRiskType
     * @return
     */
    public BigDecimal getZbLmt(List<DmRisKhfxJgbxJk> dmRisKhfxJgbxJks, String cusId, String deRiskType) {
        BigDecimal zbLmt = BigDecimal.ZERO;
        if (CollectionUtils.isEmpty(dmRisKhfxJgbxJks)) {
            return zbLmt;
        }
        for (DmRisKhfxJgbxJk dmRisKhfxJgbxJk : dmRisKhfxJgbxJks) {
            //01-非同业单一客户贷款余额占比 -- cont_lon_bal 非同业单一客户贷款余额
            if (CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_01.riskType.equals(deRiskType)) {
                zbLmt = zbLmt.add(dmRisKhfxJgbxJk.getContLonBal());
            }
            //02-非同业单一客户风险暴露占比 --- ftydy_khmx_expo 非同业单一客户风险暴露
            if (CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_02.riskType.equals(deRiskType)) {
                zbLmt = zbLmt.add(dmRisKhfxJgbxJk.getFtydyKhmxExpo());
            }
            //03-非同业关联客户风险暴露占比 --- ftygl_khmx_expo 非同业关联客户风险暴露
            if (CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_03.riskType.equals(deRiskType)) {
                zbLmt = zbLmt.add(dmRisKhfxJgbxJk.getFtyglKhmxExpo());
            }
            //04-同业单一客户风险暴露占比 -- tydy_khmx_expo 同业单一客户风险暴露
            if (CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_04.riskType.equals(deRiskType)) {
                zbLmt = zbLmt.add(dmRisKhfxJgbxJk.getTydyKhmxExpo());
            }
            //05-同业关联客户风险暴露占比 -- tygl_khmx_expo 同业关联客户风险暴露
            if (CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_05.riskType.equals(deRiskType)) {
                zbLmt = zbLmt.add(dmRisKhfxJgbxJk.getTyglKhmxExpo());
            }
            //06-匿名客户风险暴露占比 -- mmkh_khmx_expo 匿名客户风险暴露
            if (CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_06.riskType.equals(deRiskType)) {
                zbLmt = zbLmt.add(dmRisKhfxJgbxJk.getMmkhKhmxExpo());
            }
            //07-合格中央交易对手非清算风险暴露占比 -- 0
            //08-场外衍生交易名义本金对总资产占比 -- 0
        }
        return zbLmt;
    }

}
