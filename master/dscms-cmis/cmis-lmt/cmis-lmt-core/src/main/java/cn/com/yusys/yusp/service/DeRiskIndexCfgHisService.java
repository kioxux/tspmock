/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.DeRiskIndexCfgHis;
import cn.com.yusys.yusp.repository.mapper.DeRiskIndexCfgHisMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: DeRiskIndexCfgHisService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-25 15:50:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DeRiskIndexCfgHisService {

    @Autowired
    private DeRiskIndexCfgHisMapper deRiskIndexCfgHisMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public DeRiskIndexCfgHis selectByPrimaryKey(String dataDt, String riskType) {
        return deRiskIndexCfgHisMapper.selectByPrimaryKey(dataDt, riskType);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<DeRiskIndexCfgHis> selectAll(QueryModel model) {
        List<DeRiskIndexCfgHis> records = (List<DeRiskIndexCfgHis>) deRiskIndexCfgHisMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<DeRiskIndexCfgHis> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DeRiskIndexCfgHis> list = deRiskIndexCfgHisMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(DeRiskIndexCfgHis record) {
        return deRiskIndexCfgHisMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(DeRiskIndexCfgHis record) {
        return deRiskIndexCfgHisMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(DeRiskIndexCfgHis record) {
        return deRiskIndexCfgHisMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(DeRiskIndexCfgHis record) {
        return deRiskIndexCfgHisMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dataDt, String riskType) {
        return deRiskIndexCfgHisMapper.deleteByPrimaryKey(dataDt, riskType);
    }

    public List<DeRiskIndexCfgHis> selectByDataDt(String dataDt) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("dataDt",dataDt);
        return selectAll(queryModel);
    }
}
