package cn.com.yusys.yusp.service.server.cmislmt0050;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ManaBusiLmt;
import cn.com.yusys.yusp.dto.server.cmislmt0050.req.CmisLmt0050ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0050.resp.CmisLmt0050RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ManaBusiLmtService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CmisLmt0050Service {


    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0050Service.class);

    @Autowired
    private ManaBusiLmtService manaBusiLmtService;

    /**
     * 根据机构编号获取分支机构额度管控信息
     * @param reqDto
     * @return
     * @throws Exception
     */
    @Transactional
    public CmisLmt0050RespDto execute(CmisLmt0050ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0050.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0050.value);
        CmisLmt0050RespDto resqDto = new CmisLmt0050RespDto() ;

        try {
            String belgLine = reqDto.getBelgLine();
            if(StringUtils.isBlank(belgLine)){
                throw new YuspException(EclEnum.ECL070133.key, "条线belgLine" + EclEnum.ECL070133.value);
            }
            String orgAreaType = reqDto.getOrgAreaType();
            if(StringUtils.isBlank(orgAreaType)){
                throw new YuspException(EclEnum.ECL070133.key, "区域orgAreaType" + EclEnum.ECL070133.value);
            }

            ManaBusiLmt manaBusiLmt = manaBusiLmtService.selectByBelgLineAndOrgAreaType(belgLine, orgAreaType);
            if (manaBusiLmt != null) {
                BeanUtils.copyProperties(manaBusiLmt, resqDto);
                //将列表放入返回中
                resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
                resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            }else{
                resqDto.setErrorCode(EpbEnum.EPB090004.key);
                resqDto.setErrorMsg(EpbEnum.EPB090004.value);
            }
        } catch (YuspException e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0050.value, e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0050.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0050.value);
        return resqDto;
    }
    
}
