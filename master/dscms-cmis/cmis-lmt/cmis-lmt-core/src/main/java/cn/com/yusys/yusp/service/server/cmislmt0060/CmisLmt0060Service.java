package cn.com.yusys.yusp.service.server.cmislmt0060;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0059.req.CmisLmt0059ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0059.resp.CmisLmt0059RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.req.CmisLmt0060ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.resp.CmisLmt0060RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ContAccRelService;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0060Service
 * @类描述: #对内服务类
 * @功能描述: 获取合同总已用及敞口已用金额
 * @创建时间: 2021-09-30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0060Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0060Service.class);

    @Autowired
    private ContAccRelService contAccRelService ;

    /**
     * 获取合同总已用及敞口已用金额
     * add by zhjw 2021-09-30
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0060RespDto execute(CmisLmt0060ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0060.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0060.value);
        CmisLmt0060RespDto respDto = new CmisLmt0060RespDto();
        try {

            //合同号
            String dealBizNo = reqDto.getDealBizNo();
            if(StringUtils.isBlank(dealBizNo)){
                respDto.setErrorCode("9999");
                respDto.setErrorMsg("合同号不允许为空");
                return respDto;
            }

            String tranAccNo = Optional.ofNullable(reqDto.getTranAccNo()).orElse("") ;
            Map  resultMap = contAccRelService.selectContUseAmtByDealBizNo(dealBizNo, tranAccNo);
            BigDecimal totalCny = BigDecimal.ZERO ;
            BigDecimal spacCny = BigDecimal.ZERO ;
            BigDecimal totalBalanceCny =  BigDecimal.ZERO ;
            BigDecimal spacBalanceCny =  BigDecimal.ZERO ;

            if(resultMap!=null && !"".equals((String)resultMap.get("dealBizNo"))){
                totalCny = resultMap.get("totalCny")==null || resultMap.get("totalCny").equals("") ? BigDecimal.ZERO : (BigDecimal) resultMap.get("totalCny");
                spacCny = resultMap.get("spacCny")==null || resultMap.get("spacCny").equals("") ? BigDecimal.ZERO : (BigDecimal) resultMap.get("spacCny");
                totalBalanceCny = resultMap.get("totalBalanceCny")==null || resultMap.get("totalBalanceCny").equals("") ? BigDecimal.ZERO : (BigDecimal) resultMap.get("totalBalanceCny");
                spacBalanceCny = resultMap.get("spacBalanceCny")==null || resultMap.get("spacBalanceCny").equals("") ? BigDecimal.ZERO : (BigDecimal) resultMap.get("spacBalanceCny");
            }
            respDto.setSpacCny(spacCny);
            respDto.setTotalCny(totalCny);
            respDto.setTotalBalanceCny(totalBalanceCny);
            respDto.setSpacBalanceCny(spacBalanceCny);

            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0060】获取客户或分项明细用信综合总额和用信敞口余额：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0060.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0060.value);
        return respDto;
    }
}