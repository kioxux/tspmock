package cn.com.yusys.yusp.service.server.cmislmt0051;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtDiscOrg;
import cn.com.yusys.yusp.dto.server.cmislmt0051.req.CmisLmt0051ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0051.resp.CmisLmt0051RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0051.resp.LmtDiscOrgDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtDiscOrgMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CmisLmt0051Service {


    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0051Service.class);

    //LMT_DISC_ORG
    @Autowired
    private LmtDiscOrgMapper lmtDiscOrgMapper;

    /**
     * 根据机构编号获取分支机构额度管控信息
     * @param reqDto
     * @return
     * @throws Exception
     */
    @Transactional
    public CmisLmt0051RespDto execute(CmisLmt0051ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0051.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0051.value);
        CmisLmt0051RespDto resqDto = new CmisLmt0051RespDto() ;

        try {
            String mmyy = reqDto.getMmyy();
            if(StringUtils.isBlank(mmyy)){
                throw new YuspException(EclEnum.ECL070133.key, "mmyy" + EclEnum.ECL070133.value);
            }
            String openDay = reqDto.getOpenDay();
            if(StringUtils.isBlank(openDay)){
                throw new YuspException(EclEnum.ECL070133.key, "openDay" + EclEnum.ECL070133.value);
            }
            String organno = reqDto.getOrganno();
            if(StringUtils.isBlank(organno)){
                throw new YuspException(EclEnum.ECL070133.key, "organno" + EclEnum.ECL070133.value);
            }
            List<LmtDiscOrg> lmtDiscOrgList = lmtDiscOrgMapper.selectByCmisLmt0051(mmyy,openDay,organno);
            List<LmtDiscOrgDto> lmtDiscOrgDtoList = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(lmtDiscOrgList)){
                lmtDiscOrgList.forEach(a->{
                    LmtDiscOrgDto lmtDiscOrgDto = new LmtDiscOrgDto();
                    BeanUtils.copyProperties(a,lmtDiscOrgDto);
                    lmtDiscOrgDtoList.add(lmtDiscOrgDto);
                });
            }
            resqDto.setLmtDiscOrgDtoList(lmtDiscOrgDtoList);
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0051.value, e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0051.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0051.value);
        return resqDto;
    }
    
}
