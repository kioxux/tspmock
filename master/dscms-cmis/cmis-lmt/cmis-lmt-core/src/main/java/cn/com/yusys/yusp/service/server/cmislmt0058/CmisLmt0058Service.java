package cn.com.yusys.yusp.service.server.cmislmt0058;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.domain.ContAccRel;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.server.cmislmt0058.req.CmisLmt0058ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0058.req.RecoverListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0058.resp.CmisLmt0058RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections4.CollectionUtils;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0058Service
 * @类描述: #对内服务类
 * @功能描述: 转贴现业务额度恢复
 * @创建时间: 2021-09-09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0058Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0058Service.class);

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private LmtContRelService lmtContRelService ;

    /**
     * 转贴现业务额度恢复
     * 描述：转贴现业务额度恢复；转贴现买入调用cmislmt0034 合同金额送空，调用cmislmt0011 进行额度占用生成额度占用关系，即lmt_cont_rel 就是
     * 台账信息。贴现业务恢复，就是恢复lmt_cont_rel 台账信息，按批次恢复
     * add by lizx 2021-09-09
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0058RespDto execute(CmisLmt0058ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.value);
        CmisLmt0058RespDto respDto = new CmisLmt0058RespDto();
        try {
            Map<String, String> paramMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
            Map<String, String> resultMap = new HashMap<>() ;
            resultMap.putAll(paramMap);
            resultMap.put("serviceCode", DscmsLmtEnum.TRADE_CODE_CMISLMT0058.key) ;

            //恢复额度明细
            List<RecoverListDto> recoverList = reqDto.getRecoverListDtoList() ;
            if(CollectionUtils.isEmpty(recoverList)){
                throw new YuspException(EclEnum.ECL070133.key, "恢复额度明细" + EclEnum.ECL070133.value) ;
            }

            String recoverType = Optional.ofNullable(reqDto.getRecoverType()).orElse(CmisLmtConstants.STD_RECOVER_TYPE_03) ;
            //遍历恢复列表信息
            for (RecoverListDto recoverDto : recoverList) {
                //大额行号
                String cusId = recoverDto.getCusId() ;
                //如果是票据的交易，cusId传的是行号，需要转换为客户号
                if(CmisBizConstants.SYS_NO_PJP.equals(reqDto.getSysNo())){
                    logger.info("票据系统交易，将同业客户大额行号【"+cusId+"】转换为客户号开始----->start");
                    cusId = comm4Service.queryCusIdByBankNo(cusId);
                    logger.info("票据系统交易，将同业客户大额行号转换为客户号【"+cusId+"】结束----->end");
                }

                if(StringUtils.isBlank(cusId)){
                    throw new YuspException(EclEnum.ECL070133.key, "客户号" + EclEnum.ECL070133.value) ;
                }
                //恢复金额
                BigDecimal recoverAmtCny = BigDecimalUtil.replaceNull(recoverDto.getRecoverAmtCny()) ;
                List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = apprLmtSubBasicInfoService.select3003ByCusId(cusId) ;
                ApprLmtSubBasicInfo apprLmtSubBasicInfo = new ApprLmtSubBasicInfo() ;
                if(CollectionUtils.isNotEmpty(apprLmtSubBasicInfoList)){
                    apprLmtSubBasicInfo = apprLmtSubBasicInfoList.get(0) ;
                }else{
                    throw new YuspException(EclEnum.ECL070141.key, "客户号" + EclEnum.ECL070141.value) ;
                }

                String apprSubSerno = apprLmtSubBasicInfo.getApprSubSerno() ;

                //根据客户号获取票据占用台账信息
                List<LmtContRel> lmtContRelList = new ArrayList<>();
                if(CmisLmtConstants.STD_RECOVER_TYPE_03.equals(recoverType)){
                    lmtContRelList = lmtContRelService.selectLmtContRelByLimitSubNo(apprSubSerno, CmisLmtConstants.STD_ZB_CONT_STATUS_200) ;
                }else{
                    lmtContRelList = lmtContRelService.selectLmtContRelByLimitSubNo(apprSubSerno, CmisLmtConstants.STD_ZB_CONT_STATUS_200, reqDto.getSerno()) ;
                }

                if(CollectionUtils.isEmpty(lmtContRelList)){
                    throw new YuspException(EclEnum.ECL070141.key, EclEnum.ECL070141.value) ;
                }

                //遍历改客户向下台账信息，按批次处理，从最早的开始恢复
                for (LmtContRel contRel : lmtContRelList) {
                    String bizStatus = "" ;
                    if(recoverAmtCny.compareTo(BigDecimal.ZERO)>0){
                        Map<String, BigDecimal> amtMap = new HashMap<>();
                        BigDecimal bizTotalBalanceAmtCny = BigDecimalUtil.replaceNull(contRel.getBizTotalBalanceAmtCny()) ;
                        if(recoverAmtCny.compareTo(bizTotalBalanceAmtCny)>0){
                            recoverAmtCny = recoverAmtCny.subtract(bizTotalBalanceAmtCny) ;
                            //恢复敞口金额(人民币)
                            amtMap.put("recoverSpacAmtCny", bizTotalBalanceAmtCny) ;
                            //恢复总额(人民币)
                            amtMap.put("recoverAmtCny", bizTotalBalanceAmtCny) ;
                            bizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
                        }else{
                            //恢复敞口金额(人民币)
                            amtMap.put("recoverSpacAmtCny", recoverAmtCny) ;
                            //恢复总额(人民币)
                            amtMap.put("recoverAmtCny", recoverAmtCny) ;
                            recoverAmtCny = BigDecimal.ZERO ;
                        }
                        comm4Service.recoverApprLmtSubBasicInfo(null, contRel, amtMap, resultMap) ;

                        if(CmisLmtConstants.STD_ZB_BIZ_STATUS_300.equals(bizStatus)){
                            contRel.setBizStatus(bizStatus);
                            lmtContRelService.update(contRel) ;
                        }
                    }else{
                        break ;
                    }
                }
            }
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0058】转贴现业务额度恢复：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.value);
        return respDto;
    }
}