/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.constants.CmisLmtConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ApprStrOrgInfo;
import cn.com.yusys.yusp.repository.mapper.ApprStrOrgInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprStrOrgInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-20 10:46:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ApprStrOrgInfoService {

    @Autowired
    private ApprStrOrgInfoMapper apprStrOrgInfoMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public ApprStrOrgInfo selectByPrimaryKey(String pkId) {
        return apprStrOrgInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional(readOnly=true)
    public List<ApprStrOrgInfo> selectAll(QueryModel model) {
        List<ApprStrOrgInfo> records = (List<ApprStrOrgInfo>) apprStrOrgInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ApprStrOrgInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ApprStrOrgInfo> list = apprStrOrgInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int insert(ApprStrOrgInfo record) {
        return apprStrOrgInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int insertSelective(ApprStrOrgInfo record) {
        return apprStrOrgInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int update(ApprStrOrgInfo record) {
        return apprStrOrgInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int updateSelective(ApprStrOrgInfo record) {
        return apprStrOrgInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return apprStrOrgInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return apprStrOrgInfoMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: selectByAccNoAndOrg
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<ApprStrOrgInfo> selectByAccNoAndOrg(String bchCde, String apprSerno) {
        QueryModel queryModel = new QueryModel() ;
        queryModel.addCondition("apprSerno", apprSerno);
        queryModel.addCondition("bchCde", bchCde);
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        return apprStrOrgInfoMapper.selectByAccNoAndOrg(queryModel);
    }
}
