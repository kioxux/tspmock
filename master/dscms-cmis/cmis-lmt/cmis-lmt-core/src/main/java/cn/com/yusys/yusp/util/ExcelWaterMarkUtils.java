package cn.com.yusys.yusp.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;

import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.OrderDownloadDto;


public class ExcelWaterMarkUtils {
    
	
	public static void setWaterMarkToExcel2(XSSFWorkbook workbook, BufferedImage bfi) throws Exception {
		ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
		ImageIO.write(bfi, "png", byteArrayOut);
		int pictureIdx = workbook.addPicture(byteArrayOut.toByteArray(), Workbook.PICTURE_TYPE_PNG);
		//add relation from sheet to the picture data
		POIXMLDocumentPart poixmlDocumentPart = workbook.getAllPictures().get(pictureIdx);
		for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
			XSSFSheet xssfSheet = workbook.getSheetAt(i);
			PackagePartName ppn = poixmlDocumentPart.getPackagePart().getPartName();
			String relType = XSSFRelation.IMAGES.getRelation();
			PackageRelationship pr = xssfSheet.getPackagePart().addRelationship(ppn, TargetMode.INTERNAL, relType, null);
			// xssfSheet.getCTWorksheet().addNewPicture().setId(pr.getId());
		}
	}
	
	public static BufferedImage createWatermarkImage(OrderDownloadDto orderDownload) {
//        if (watermark == null) {
//            watermark = new WaterMarkContent();
//            watermark.setEnable(true);
////            watermark.setText("userName");
//            watermark.setText("内部资料");
//            watermark.setColor("#C5CBCF");
//            watermark.setDateFormat("yyyy-MM-dd HH:mm");
//        }
		String contentInfo = "";
		String contentType = orderDownload.getWaterContent();
		if(contentType.indexOf("1")>-1){
			contentInfo = contentInfo + orderDownload.getInputId()+",";
		}
		if(contentType.indexOf("2")>-1){
			contentInfo = contentInfo + orderDownload.getInputBrId()+",";
		}
		if(contentType.indexOf("3")>-1){
			contentInfo = contentInfo + orderDownload.getWaterWords()+",";
		}
        String[] textArray = contentInfo.split(",");
        int size = 20;
        if(orderDownload.getWaterFontSize()!=null) {
        	size = orderDownload.getWaterFontSize();
        }
        Font font = new Font("宋体", Font.PLAIN, size);
        Integer width = 300;
        Integer height = 100;

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // 背景透明 开始
        Graphics2D g = image.createGraphics();
        image = g.getDeviceConfiguration().createCompatibleImage(width, height, Transparency.TRANSLUCENT);
        g.dispose();
        // 背景透明 结束
        g = image.createGraphics();
        g.setColor(new Color(Integer.parseInt("#C5CBCF".substring(1), 16)));// 设定画笔颜色
        g.setFont(font);// 设置画笔字体
        g.shear(0.1, -0.26);// 设定倾斜度
//        设置字体平滑
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        int y = 50;
        for (int i = 0; i < textArray.length; i++) {
            g.drawString(textArray[i], 0, y);// 画出字符串
            y = y + font.getSize();
        }
        g.drawString(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), 0, y);// 画出字符串

        g.dispose();// 释放画笔
        return image;

    }
}
