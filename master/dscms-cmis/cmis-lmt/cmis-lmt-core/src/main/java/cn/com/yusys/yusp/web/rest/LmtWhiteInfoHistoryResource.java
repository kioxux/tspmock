/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtWhiteInfoHistory;
import cn.com.yusys.yusp.service.LmtWhiteInfoHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtWhiteInfoHistoryResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: dumingdi
 * @创建时间: 2021-05-22 11:33:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtwhiteinfohistory")
public class LmtWhiteInfoHistoryResource {
    private static final Logger log = LoggerFactory.getLogger(LmtWhiteInfoHistoryResource.class);
    @Autowired
    private LmtWhiteInfoHistoryService lmtWhiteInfoHistoryService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtWhiteInfoHistory>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtWhiteInfoHistory> list = lmtWhiteInfoHistoryService.selectAll(queryModel);
        return new ResultDto<List<LmtWhiteInfoHistory>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/selectByModel")
    protected ResultDto<List<LmtWhiteInfoHistory>> index(QueryModel queryModel) {
        List<LmtWhiteInfoHistory> list = lmtWhiteInfoHistoryService.selectByModel(queryModel);
        return new ResultDto<List<LmtWhiteInfoHistory>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/querymodel")
    protected ResultDto<List<LmtWhiteInfoHistory>> selectByModel(@RequestBody QueryModel queryModel) {
        List<LmtWhiteInfoHistory> list = lmtWhiteInfoHistoryService.selectByModel(queryModel);
        return new ResultDto<List<LmtWhiteInfoHistory>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtWhiteInfoHistory> show(@PathVariable("pkId") String pkId) {
        LmtWhiteInfoHistory lmtWhiteInfoHistory = lmtWhiteInfoHistoryService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtWhiteInfoHistory>(lmtWhiteInfoHistory);
    }

}
