/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtDiscOrg
 * @类描述: lmt_disc_org数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 15:38:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_disc_org")
public class LmtDiscOrg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 申请年月 **/
	@Column(name = "APP_YM", unique = false, nullable = false, length = 10)
	private String appYm;
	
	/** 机构号 **/
	@Column(name = "ORGANNO", unique = false, nullable = false, length = 20)
	private String organno;
	
	/** 机构名称 **/
	@Column(name = "ORGANNAME", unique = false, nullable = false, length = 80)
	private String organname;
	
	/** 申请额度 **/
	@Column(name = "APPLY_AMOUNT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal applyAmount;
	
	/** 核准额度 **/
	@Column(name = "APPROVE_AMOUNT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal approveAmount;
	
	/** 已使用额度 **/
	@Column(name = "USE_AMT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal useAmt;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = false, length = 5)
	private String status;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param appYm
	 */
	public void setAppYm(String appYm) {
		this.appYm = appYm;
	}
	
    /**
     * @return appYm
     */
	public String getAppYm() {
		return this.appYm;
	}
	
	/**
	 * @param organno
	 */
	public void setOrganno(String organno) {
		this.organno = organno;
	}
	
    /**
     * @return organno
     */
	public String getOrganno() {
		return this.organno;
	}
	
	/**
	 * @param organname
	 */
	public void setOrganname(String organname) {
		this.organname = organname;
	}
	
    /**
     * @return organname
     */
	public String getOrganname() {
		return this.organname;
	}
	
	/**
	 * @param applyAmount
	 */
	public void setApplyAmount(java.math.BigDecimal applyAmount) {
		this.applyAmount = applyAmount;
	}
	
    /**
     * @return applyAmount
     */
	public java.math.BigDecimal getApplyAmount() {
		return this.applyAmount;
	}
	
	/**
	 * @param approveAmount
	 */
	public void setApproveAmount(java.math.BigDecimal approveAmount) {
		this.approveAmount = approveAmount;
	}
	
    /**
     * @return approveAmount
     */
	public java.math.BigDecimal getApproveAmount() {
		return this.approveAmount;
	}
	
	/**
	 * @param useAmt
	 */
	public void setUseAmt(java.math.BigDecimal useAmt) {
		this.useAmt = useAmt;
	}
	
    /**
     * @return useAmt
     */
	public java.math.BigDecimal getUseAmt() {
		return this.useAmt;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}