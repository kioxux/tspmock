package cn.com.yusys.yusp.web.server.cmislmt0048;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0048.req.CmisLmt0048ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0048.resp.CmisLmt0048RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0048.CmisLmt0048Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据分项编号，判断是否可以做最高额协议或一般最高额合同
 *
 * @author zhangjw 2021/7/14
 * @version 1.0
 */
@Api(tags = "cmislmt0048:根据分项编号，判断是否可以做最高额协议或一般最高额合同")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0048Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0048Resource.class);

    @Autowired
    private CmisLmt0048Service cmisLmt0048Service;
    /**
     * 交易码：cmislmt0048
     * 交易描述：根据分项编号，判断是否可以做最高额协议或一般最高额合同
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据分项编号，判断是否可以做最高额协议或一般最高额合同")
    @PostMapping("/cmislmt0048")
    protected @ResponseBody
    ResultDto<CmisLmt0048RespDto> cmisLmt0048(@Validated @RequestBody CmisLmt0048ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0048.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0048.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0048RespDto> cmisLmt0048RespDtoResultDto = new ResultDto<>();
        CmisLmt0048RespDto cmisLmt0048RespDto = new CmisLmt0048RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0048RespDto = cmisLmt0048Service.execute(reqDto);
            cmisLmt0048RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0048RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);

        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0048.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0048.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0048.value, e.getMessage());
            // 封装xddb0048DataResultDto中异常返回码和返回信息
            cmisLmt0048RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0048RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0016RespDto到cmisLmt0016RespDtoResultDto中
        cmisLmt0048RespDtoResultDto.setData(cmisLmt0048RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0048.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0048.value, JSON.toJSONString(cmisLmt0048RespDtoResultDto));
        return cmisLmt0048RespDtoResultDto;
    }
}
