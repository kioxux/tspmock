package cn.com.yusys.yusp.service.server.cmisLmt0038;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtSubPrdMappConf;
import cn.com.yusys.yusp.dto.server.cmislmt0038.req.CmisLmt0038ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038PrdListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.LmtSubPrdMappConfService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0038Service
 * @类描述: #对内服务类
 * @功能描述: 根据额度品种编号查找适用产品编号
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0038Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0038Service.class);

    @Autowired
    private LmtSubPrdMappConfService lmtSubPrdMappConfService;

    @Transactional
    public CmisLmt0038RespDto execute(CmisLmt0038ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0038.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0038.value);
        CmisLmt0038RespDto resqDto = new CmisLmt0038RespDto() ;
        //适用产品列表
        List<CmisLmt0038PrdListRespDto> cmisLmt0038PrdListRespDtoList = new ArrayList<>();

        try {
            String limitSubNo = reqDto.getLimitSubNo();
            if(StringUtils.isNotBlank(limitSubNo)){
                List<LmtSubPrdMappConf> lmtSubPrdMappConfList  = lmtSubPrdMappConfService.selectPrdIdByLimitSubNo(limitSubNo);
                if(lmtSubPrdMappConfList!=null && lmtSubPrdMappConfList.size()>0){
                    for(int i=0;i<lmtSubPrdMappConfList.size();i++){
                        LmtSubPrdMappConf lmtSubPrdMappConf = lmtSubPrdMappConfList.get(i);

                        CmisLmt0038PrdListRespDto respDto = new CmisLmt0038PrdListRespDto();
                        respDto.setPrdId(lmtSubPrdMappConf.getPrdId());
                        respDto.setPrdName(lmtSubPrdMappConf.getPrdName());

                        cmisLmt0038PrdListRespDtoList.add(respDto);
                    }
                }
            }
            //将列表放入返回中
            resqDto.setCmisLmt0038PrdListRespDtoList(cmisLmt0038PrdListRespDtoList);
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("根据额度品种编号查找适用产品编号接口报错：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0038.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0038.value);
        return resqDto;
    }
}