package cn.com.yusys.yusp.web.server.cmislmt0039;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0038.req.CmisLmt0038ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.req.CmisLmt0039ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.resp.CmisLmt0039RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmisLmt0038.CmisLmt0038Service;
import cn.com.yusys.yusp.service.server.cmislmt0039.CmisLmt0039Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据额度品种编号查找适用产品编号
 *
 * @author zhangjw 2021/7/6
 * @version 1.0
 */
@Api(tags = "cmislmt0039:根据额度品种编号查找适用产品编号")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0039Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0039Resource.class);

    @Autowired
    private CmisLmt0039Service cmisLmt0039Service;
    /**
     * 交易码：cmislmt0039
     * 交易描述：获取合作方额度台账项下总用信敞口余额
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("获取合作方额度台账项下总用信敞口余额")
    @PostMapping("/cmislmt0039")
    protected @ResponseBody
    ResultDto<CmisLmt0039RespDto> cmisLmt0039(@Validated @RequestBody CmisLmt0039ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0039.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0039.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0039RespDto> cmisLmt0039RespDtoResultDto = new ResultDto<>();
        CmisLmt0039RespDto cmisLmt0039RespDto = new CmisLmt0039RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0039RespDto = cmisLmt0039Service.execute(reqDto);
            cmisLmt0039RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0039RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0039.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0039.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0039.value, e.getMessage());
            // 封装xddb0039DataResultDto中异常返回码和返回信息
            cmisLmt0039RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0039RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0039RespDto到cmisLmt0039RespDtoResultDto中
        cmisLmt0039RespDtoResultDto.setData(cmisLmt0039RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0039.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0039.value, JSON.toJSONString(cmisLmt0039RespDtoResultDto));
        return cmisLmt0039RespDtoResultDto;
    }
}
