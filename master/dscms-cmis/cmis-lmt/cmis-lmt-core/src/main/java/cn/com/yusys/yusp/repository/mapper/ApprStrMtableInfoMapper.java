/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import cn.com.yusys.yusp.dto.server.cmislmt0019.req.CmisLmt0019ReqDto;
import cn.com.yusys.yusp.commons.data.authority.annotation.IgnoredDataAuthority;
import cn.com.yusys.yusp.dto.ApprStrMtableInfoDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.resp.CmisLmt0021RespDto;
import org.apache.ibatis.annotations.Param;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprStrMtableInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 86188
 * @创建时间: 2021-05-11 10:36:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface ApprStrMtableInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    ApprStrMtableInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(ApprStrMtableInfo record);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ApprStrMtableInfo> selectByModel(QueryModel model);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(ApprStrMtableInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(ApprStrMtableInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(ApprStrMtableInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

	List<ApprStrMtableInfo> getApprByCusAndPrdId(@Param("cusId") String cusId, @Param("prdId") String prdId);

	List<ApprStrMtableInfo> getApprByCusAndStrNoList(@Param("cusId") String cusId, @Param("list") List<String> limitStrNoList);
    /**
     * @param queryModel
     *         cusType：客户类型（Y）；instuCde 金融机构代码（Y）；cusId 客户号；cusName 客户名称
     *         notInstuCde 金融机构代码不为传入参数；
     * @date 2021/5/4
     * @version 1.0.0
     * @desc    根据金融机构号查询单一客户（法人、个人）额度视图列表，支持客户号精确查询、客户名称模糊查询
     * @修改历史: zhangjw credate
     */
    @IgnoredDataAuthority
    List<Map<String, Object>> selectStrInfoByList(QueryModel queryModel);

    /**
     * @param queryModel
     *         cusType：客户类型（Y）；instuCde 金融机构代码（Y）；cusId 客户号；cusName 客户名称
     *         notInstuCde 金融机构代码不为传入参数；
     * @date 2021/5/4
     * @version 1.0.0
     * @desc    根据金融机构号查询单一客户（法人、个人）额度视图列表，支持客户号精确查询、客户名称模糊查询
     * @修改历史: zhangjw credate
     */
    List<Map<String, Object>> selectStrInfoByBbList(QueryModel queryModel);

    /**
     * @param cusIdList
     *         instuCde 金融机构代码（Y）；cusId 客户号；cusName 客户名称
     *         notInstuCde 金融机构代码不为传入参数；
     * @date 2021/5/4
     * @version 1.0.0
     * @desc    根据金融机构号查询同业客户额度视图列表，支持客户号精确查询、客户名称模糊查询
     * @修改历史: zhangjw credate
     */
    @IgnoredDataAuthority
    List<Map<String, Object>> selectSigStrInfoByList(@Param("cusIdList") List<String> cusIdList);
    /**
     * @param grpNos
     *         instuCde 金融机构代码（Y）；cusId 子成员客户号；cusName 子成员客户名称;grpNo集团客户号；grpName集团客户名称
     *         notInstuCde 金融机构代码不为传入参数；
     * @date 2021/5/4
     * @version 1.0.0
     * @desc    根据金融机构号查询集团客户额度视图列表，支持客户号精确查询、客户名称模糊查询
     * @修改历史: zhangjw credate
     */
    @IgnoredDataAuthority
    List<Map<String, Object>> selectGrpStrInfoByGrpNos(@Param("grpNos") List<String> grpNos);

    List<Map<String, Object>> selectGrpNosByQueryModel(QueryModel queryModel);

    List<Map<String, Object>> queryStrChgAppBySerno(QueryModel queryModel);

    /**
     * @param queryModel  cusId  instuCde
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @date 2021/4/26 15:25
     * @version 1.0.0
     * @desc    根据当前金融机构号和客户号查询客户并表额度汇总列表
     * @修改历史: zhangjw credate
     */
    List<Map<String, Object>> selectStuCdeCusLmtByCusId(QueryModel queryModel);

    /**
     * 更新批复主信息最近更新人、修改时间
     * @param
     * @return
     */

    int updateApprStrMtableInfoByParams(Map params);

    /**
     * 根据客户编号和批复台账编号修改状态
     * @param
     * @return
     */
    int updateBySernoAndCusId(ApprStrMtableInfo record);

    /**
     * @方法名称: selectByAppSerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    ApprStrMtableInfo selectByAppSerno(@Param("apprSerno") String apprSerno);

    /**
     * 根据客户编号修改状态
     * @param
     * @return
     */
    int updateByApprSerno(QueryModel model);

    /**
     * 根据客户编号获得集团批复流水号
     * @param
     * @return
     */
    String getGrpSernoByCusId(@Param("cusId") String cusId);

    /**
     * 根据集团批复流水号获得该集团下所有单一客户的状态
     * @param
     * @return
     */
    List<String> getStatusByGrpSerno(@Param("grpApprSerno") String grpApprSerno);

    /**
     * 根据批复流水号获得客户批复信息
     * @param
     * @return
     */
    ApprStrMtableInfo getAllByCusId(@Param("cusId") String cusId);


    /**
     * 根据批复流水号修改状态(失效已结清/失效未结清)
     * @param
     * @return
     */
    int updateStatusByApprSerno(Map params);

    /**
     * 根据批复流水号修改批复台账状态
     * @param
     * @return
     */
    int updateApprStrMtableInfoByApprSerno(HashMap<String,Object> hashMap);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByApprSernoSelective(ApprStrMtableInfo record);


    /**
     * 对公授信信息查询
     * @param cusId
     * @return
     */
    CmisLmt0021RespDto selectCusComLmtDetailsByCusId(@Param("cusId") String cusId);

    /**
     * 查询客户列表
     * @param queryModel
     * @return
     */
    List<Map<String,String>> selectCusInfoListByQueryModel(QueryModel queryModel);

    /**
     * 根据客户列表查询法人及个人客户额度台账信息
     * @param cusIdList
     * @return
     */
    List<Map<String,Object>> selectLawAndPersonLmtAccListByCusIdList(@Param("cusIdList") List<String> cusIdList);

    /**
     * 根据queryModel查询集团客户的批复台账编号
     * @param queryModel
     * @return
     */
    List<Map<String,String>> selectGrpApprSernoInfoByQueryModel(QueryModel queryModel);

    /**
     * 返回该客户是否集团客户
     * @param queryModel
     * @return
     */
    Map<String,String> selectCusIsGrpMemCus(QueryModel queryModel);

    /**
     * 根据查询条件，查询有效的集团授信额度台账
     * @param queryModel
     * @return
     */
    List<Map<String,Object>> queryGrpLmtMemberListStatus(QueryModel queryModel);

    /**
     * @date 2021/5/15
     * @version 1.0.0
     * @desc    根据客户号和法人机构号获取客户额度视图信息
     * @修改历史: zhangjw credate
     */
    List<Map<String, Object>> queryLmtInfoByCusId(QueryModel queryModel);

    /**
     * 查询同业客户的客户编号列表
     * @param queryModel
     * @return
     */
    List<Map<String,String>> selectSigCusIdListByQueryModel(QueryModel queryModel);

    /**
     * 根据queryModel查询批复主表记录数
     * @param
     * @return
     */
    int countApprStrMtableInfoByQueryModel(QueryModel queryModel);

    /**
     * 查询客户名下有效标准化资产授信（未到期）金额之和
     * @param cusId
     * @return
     */
    BigDecimal selectTotalAssetLmtAmtByCusId(@Param("cusId") String cusId);

    /**
     * 查询客户名下标准化资产余额（已到期部分）
     * @param cusId
     * @return
     */
    BigDecimal selectTotalAssetBalanceAmtByCusId(@Param("cusId") String cusId);

    /**
     * 查询客户名下有效非标资产授信（未到期）金额之和
     * @param cusId
     * @return
     */
    BigDecimal selectTotalAssetLmtAmtNonStandardByCusId(@Param("cusId") String cusId);

    /**
     * 查询客户名下非标资产余额（已到期部分）
     * @param cusId
     * @return
     */
    BigDecimal selectTotalAssetBalanceAmtNonStandardByCusId(@Param("cusId") String cusId);

    /**
     * 查询客户（综合授信+主体授信）未到期的授信金额
     * @param model
     * @return
     */
    BigDecimal selectTotalLmtAmtByCusId(QueryModel model);

    /**
     * 查询客户（综合授信+主体授信）已到期的业务余额
     * @param model
     * @return
     */
    BigDecimal selectTotalBalanceAmtByCusId(QueryModel model);

    /**
     * 查询客户 委托贷款 未到期的授信金额
     * @param model
     * @return
     */
    BigDecimal selectTotalWtLmtAmtByCusId(QueryModel model);

    /**
     * 查询客户 委托贷款 已到期的用信余额
     * @param model
     * @return
     */
    BigDecimal selectTotalWtBalanceAmtByCusId(QueryModel model);


    /**
     * 根据批复台账编号列表更新集团批复台账编号字段
     * @param queryModel
     * @return
     */
    int updateGrpApprSernoByApprSernos(QueryModel queryModel);

    /**
     * 查询客户的低风险授信总额
     * @param cusIdList
     * @return
     */
    List<Map<String,Object>> selectLriskLmtByCusIdList(@Param("cusIdList") List<String> cusIdList);

    /**
     * 根据批复分项编号，查询该批复向下分项信息
     * @param apprSerno 批复分项编号
     * @return
     */
    List<String> selectApprSubSernoListByApprSerno(@Param("apprSerno") String apprSerno);

    /**
     * 根据批复台账编号获取授信模式
     * @param apprSerno
     * @return
     */
    String selectLmtModeByApprSerno(@Param("apprSerno") String apprSerno);

    /**
     * 获取客户当前有效的综合授信额度
     * @param queryModel 批复分项编号
     * @return
     */
    List<ApprStrMtableInfo> queryApprStrZHLmt(QueryModel queryModel);

    /**
     * @param queryModel
     * @date 2021/8/23
     * @version 1.0.0
     * @desc    根据金融机构号查询单一客户（法人、个人）额度视图列表，支持过滤客户编号、客户名称、冻结解冻提前终止操作状态
     * @修改历史: zhangjw credate
     */
    List<Map<String, Object>> queryLmtCusIdByModel(QueryModel queryModel);

    /**
     * @param
     * @date 2021/5/4
     * @version 1.0.0
     * @desc    根据客户号查找额度视图列表
     * @修改历史: zhangjw credate
     */
    List<Map<String, Object>> queryLmtListByCusId(QueryModel queryModel);
    /**
     * @方法名称: selectForPsp
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    List<ApprStrMtableInfoDto> selectForPsp(QueryModel model);

    /**
     * @param queryModel
     *         cusType：客户类型（Y）；instuCde 金融机构代码（Y）；cusId 客户号；cusName 客户名称
     *         notInstuCde 金融机构代码不为传入参数；
     * @date 2021/9/3
     * @version 1.0.0
     * @desc    法人、个人客户额度视图列表，获取客户号
     * @修改历史: zhangjw credate
     */
    List<String> queryListForCus(QueryModel queryModel);


    /**
     * 查询客户（综合授信+主体授信）敞口用信余额，不包含穿透化额度、承销额度
     * @param model
     * @return
     * add by zhangjw 20210905
     */
    public BigDecimal selectLoanSpacBalanceAmtByCusId(QueryModel model);
    /**
     * 查询客户（综合授信+主体授信）敞口用信余额，不包含穿透化额度、承销额度
     * @param model
     * @return
     * add by zhangjw 20210905
     */
    public Map<String,Object> calLmtAntLoanSpacBalanceAmt(QueryModel model);

    /**
     * 根据客户列表查询法人及个人客户额度台账信息
     * @param queryModel
     * @return
     */
    Map<String,BigDecimal> selectAvlAmtByCusIdParam(QueryModel queryModel);

    /**
     * 根据客户号和额度品种编号查找额度分项
     * @param model
     * @return
     */
    public List<Map<String,Object>> selectApprSernoByCusIdAndLimitSubNo(QueryModel model);


    List<String> selectCusIdInfoByList(QueryModel queryModel);

    /**
     * 根据客户号和客户类型，额度类型，金融机构代码查询批复主表信息，客户号必输
     * @param queryModel
     * @return
     */
    ApprStrMtableInfo selectByParamCusId(QueryModel queryModel);

    Integer selectPageCountByParam(QueryModel queryModel) ;

    /**
     * 根据客户号更新主表集团编号
     * @param params
     * @return
     */
    int updateGrpNoByCusId(Map params) ;


    /**
     * 根据客户号更新主表集团编号
     * @param queryModel
     * @return
     */
    public List<Map<String,Object>> selectByApprApp(QueryModel queryModel) ;
}