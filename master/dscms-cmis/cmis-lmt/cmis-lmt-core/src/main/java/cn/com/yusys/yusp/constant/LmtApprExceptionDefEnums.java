package cn.com.yusys.yusp.constant;

public enum LmtApprExceptionDefEnums {
    //定义默认的成功以及异常返回信息
    LMT_APPR_DEF_SUCCESS("ECL070000","成功"),
    LMT_APPR_DEF_EXCEPTION("ECL079999","异常"),
    E_LMT_APPR_NOT_EXISTS_FAILED("ECL0700001","额度批复不存在！"),
    E_TH_LMT_APPR_PARAMS_FAILED("ECL070002","请求入参数据不存在！"),
    AFTEREND_APPRLMTCHGAPP_EXCEPTION("ECL010001","单一客户额度调整失败，批复主信息不存在！"),
    AFTEREND_APPRLMTINFO_EXCEPTION("ECL010002","单一客户额度调整失败，额度调整申请主信息不存在！"),
    AFTEREND_APPRLMTSUBBASICINFOCHG_EXCEPTION("ECL010003","单一客户额度调整失败，批复分项信息不存在！"),
    AFTEREND_APPRLMTLMTCONTRELCHG_EXCEPTION("ECL010003","单一客户额度调整失败，占用合同信息不存在！"),
    E_LMT_COOP_UPDATE_STATUS_FAILED("ECL010004","合作方台账状态修改失败"),
    E_LMT_COOP_SUB_UPDATE_STATUS_FAILED("ECL010005","合作方授信分项状态修改失败"),
    E_LMT_STR_MTABLE_UPDATE_STATUS_FAILED("ECL010006","单一客户台账状态修改失败"),
    E_LMT_SUB_BASIC_UPDATE_STATUS_FAILED("ECL010007","单一客户授信分项状态修改失败")
    ;


    /** 异常编码 **/
    private String exceptionCode;

    /** 异常描述 **/
    private String exceptionDesc;

    LmtApprExceptionDefEnums(String exceptionCode,String exceptionDesc){
        this.exceptionCode = exceptionCode;
        this.exceptionDesc = exceptionDesc;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public String getExceptionDesc() {
        return exceptionDesc;
    }
}
