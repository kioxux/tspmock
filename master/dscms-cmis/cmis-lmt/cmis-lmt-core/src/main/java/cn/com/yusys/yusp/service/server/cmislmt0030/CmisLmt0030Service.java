package cn.com.yusys.yusp.service.server.cmislmt0030;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0030.req.CmisLmt0030ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0030.resp.CmisLmt0030RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusGrpMemberRelMapper;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service
public class CmisLmt0030Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0030Service.class);

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private CusGrpMemberRelMapper cusGrpMemberRelMapper ;
    /**
     * 查询客户标准资产授信余额
     * add by dumd 20210616
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0030RespDto execute(CmisLmt0030ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0030.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0030.value);
        CmisLmt0030RespDto respDto = new CmisLmt0030RespDto();
        try {
            //是否查询集团向下 1是 0 否
            // 查询集团向下，根据当前客户查询该集团向下所有子成员，汇总集团向下所有子成员的标准资产授信余额
            String isQuryGrp = reqDto.getIsQuryGrp() ;
            //客户号
            String cusId = reqDto.getCusId();
            BigDecimal lmtBalanceAmt = getLmtBalanceAmt(cusId);

            //如果查询集团向下
            if(CmisLmtConstants.YES_NO_Y.equals(isQuryGrp)){
                //根据改客户号查询该客户集团向下的客户信息
                List<Map<String, String>> cusMemList = cusGrpMemberRelMapper.selectByCusIdQueryGrpNoCuss(cusId)  ;
                //空处理
                if(CollectionUtils.nonEmpty(cusMemList)){
                    //遍历查询
                    for (Map<String, String> cusMap : cusMemList) {
                        String memCusId = cusMap.get("cusId") ;
                        //集团下可以等于当前客户跳过此循环
                        if(cusId.equals(memCusId)) continue;
                        //获取集团下客户信息是否存在授信
                        lmtBalanceAmt=lmtBalanceAmt.add(getLmtBalanceAmt(cusId));
                    }
                }
            }
            respDto.setLmtBalanceAmt(lmtBalanceAmt);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("查询客户标准资产授信余额" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0030.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0030.value);
        return respDto;
    }

    /**
     * @方法名称: getLmtBalanceAmt
     * @方法描述: 获取客户号标准化资产授信未到期和标准化资产余额已到期
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/8/12 21:02
     * @param cusId:
     * @return: java.math.BigDecimal
     * @算法描述: 无
    */
    private BigDecimal getLmtBalanceAmt(String cusId) {
        //客户有效标准化资产授信（未到期）金额之和
        BigDecimal totalAssetLmtAmt = comm4Service.getTotalAssetLmtAmt(cusId);
        //客户名下标准化资产余额（已到期部分）
        BigDecimal totalAssetBalanceAmt = comm4Service.getTotalAssetBalanceAmt(cusId);
        //客户标准资产授信余额 = 客户有效标准化资产授信（未到期）金额之和 + 客户名下标准化资产余额（已到期部分）
        BigDecimal lmtBalanceAmt = totalAssetLmtAmt.add(totalAssetBalanceAmt);
        return lmtBalanceAmt;
    }
}