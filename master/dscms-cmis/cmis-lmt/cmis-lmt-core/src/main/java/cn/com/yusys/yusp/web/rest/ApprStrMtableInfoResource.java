/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.dto.ApprStrMtableInfoDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprStrMtableInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 86188
 * @创建时间: 2021-05-11 10:36:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "ApprStrMtableInfoResource:批复主表信息处理")
@RestController
@RequestMapping("/api/apprstrmtableinfo")
public class ApprStrMtableInfoResource {
    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;
	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ApprStrMtableInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<ApprStrMtableInfo> list = apprStrMtableInfoService.selectAll(queryModel);
        return new ResultDto<List<ApprStrMtableInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ApprStrMtableInfo>> index(QueryModel queryModel) {
        List<ApprStrMtableInfo> list = apprStrMtableInfoService.selectByModel(queryModel);
        return new ResultDto<List<ApprStrMtableInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ApprStrMtableInfo> show(@PathVariable("pkId") String pkId) {
        ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<ApprStrMtableInfo>(apprStrMtableInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ApprStrMtableInfo> create(@RequestBody ApprStrMtableInfo apprStrMtableInfo) {
        apprStrMtableInfoService.insert(apprStrMtableInfo);
        return new ResultDto<ApprStrMtableInfo>(apprStrMtableInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ApprStrMtableInfo apprStrMtableInfo) {
        int result = apprStrMtableInfoService.update(apprStrMtableInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = apprStrMtableInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = apprStrMtableInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel
     *      cusType 客户类型（Y）；instuCde 金融机构号 （Y）；支持cusId客户号、cusName客户名称查询
     *      notInstuCde 金融机构代码不为传入参数；
     * @return List<ApprStrMtableInfo>
     * @date 2021/4/19 15:14
     * @version 1.0.0
     * @desc    单一客户额度视图列表（法人、个人）
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectStrInfoByList")
    protected ResultDto selectStrInfoByList(@RequestBody QueryModel queryModel) {
        ResultDto dto = new ResultDto();
        String cusId = (String) queryModel.getCondition().get("cusId");
        Map<String, String> map = new HashMap<>();
        if (StringUtils.isNotBlank(cusId)) {
            map = apprStrMtableInfoService.selectCusIsGrpMemCus(cusId);
        }
        if (EclEnum.ECL070091.key.equals(map.get("rtnCode"))) {
            dto.setData(map.get("rtnMsg"));
            return dto;
        }
        return apprStrMtableInfoService.selectStrInfoByList(queryModel);
    }

    /**
     * @param queryModel
     *      cusType 客户类型（Y）；instuCde 金融机构号 （Y）；支持cusId客户号、cusName客户名称查询
     *      notInstuCde 金融机构代码不为传入参数；
     * @return List<ApprStrMtableInfo>
     * @date 2021/4/19 15:14
     * @version 1.0.0
     * @desc    单一客户额度视图列表（法人、个人）
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectStrInfoByCusId")
    protected ResultDto selectStrInfoByCusId(@RequestBody QueryModel queryModel) {
        return apprStrMtableInfoService.selectStrInfoByCusId(queryModel);
    }

    /**
     * @param queryModel
     *      instuCde 金融机构号（Y）;支持cusId客户号、cusName客户名称查询
     *      notInstuCde 金融机构代码不为传入参数；
     * @return List<ApprStrMtableInfo>
     * @date 2021/4/19 15:14
     * @version 1.0.0
     * @desc    同业客户额度视图列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectSigStrInfoByList")
    protected ResultDto<List<Map<String,Object>>> selectSigStrInfoByList(@RequestBody QueryModel queryModel) {
//        List<Map<String,Object>> list = apprStrMtableInfoService.selectSigStrInfoByList(queryModel);
//        return new ResultDto<>(list);
        return apprStrMtableInfoService.selectSigStrInfoByList(queryModel);
    }


    /**
     * @param queryModel
     *          instuCde 金融机构号（Y）；支持cusId客户号、cusName客户名称查询;grpNo集团客户号；grpName集团客户名称
     *          notInstuCde 金融机构代码不为传入参数；
     * @return List<ApprStrMtableInfo>
     * @date 2021/5/4
     * @version 1.0.0
     * @desc    集团客户额度视图列表
     * @修改历史: zhangjw
     */
    @PostMapping("/selectGrpStrInfoByList")
    protected ResultDto<List<Map<String,Object>>> selectGrpStrInfoByList(@RequestBody QueryModel queryModel) {
        return apprStrMtableInfoService.selectGrpStrInfoByList(queryModel);
    }

    /**
     * @param queryModel
     *          instuCde 金融机构号（Y）；支持cusId客户号、cusName客户名称查询;grpNo集团客户号；grpName集团客户名称
     *          notInstuCde 金融机构代码不为传入参数；
     * @return List<ApprStrMtableInfo>
     * @date 2021/10/29
     * @version 1.0.0
     * @desc    集团客户额度视图详情
     * @修改历史:
     */
    @PostMapping("/selectGrpStrInfoByCusId")
    protected ResultDto<List<Map<String,Object>>> selectGrpStrInfoByCusId(@RequestBody QueryModel queryModel) {
        return apprStrMtableInfoService.selectGrpStrInfoByList(queryModel);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * 分页查询类
     * @算法描述:
     */
    @PostMapping("/queryStrChgAppBySerno")
    protected ResultDto<List<Map<String,Object>>> queryStrChgAppBySerno(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprStrMtableInfoService.queryStrChgAppBySerno(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel
     * @return
     * @date 2021/4/23 9:46
     * @version 1.0.0
     * @desc     单一客户额度视图-关联客户授信列表（关联个人/关联法人）
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectRelCusApprLmtByCusId")
    protected ResultDto<List<Map<String,Object>>> selectRelCusApprLmtByCusId(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprStrMtableInfoService.selectRelCusApprLmtByCusId(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<java.util.Map<java.lang.String,java.lang.Object>>>
     * @date 2021/4/26 9:46
     * @version 1.0.0
     * @desc     单一客户额度视图-并表额度
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectStuCdeCusLmtByCusId")
    protected ResultDto<List<Map<String,Object>>> selectStuCdeCusLmtByCusId(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprStrMtableInfoService.selectStuCdeCusLmtByCusId(queryModel);
        return new ResultDto<>(list);
    }


    /**
     * @param queryModel
     * @return
     * @date 2021/4/23 9:46
     * @version 1.0.0
     * @desc    根据客户号查询客户额度视图详情汇总信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectAssSumByCusId")
    protected ResultDto<List<Map<String,Object>>> selectAssSumByCusId(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprStrMtableInfoService.selectAssSumByCusId(queryModel);
        return new ResultDto<>(list);
    }


    /**
     * @param cusId
     * @return
     * @date 2021/5/15 9:46
     * @version 1.0.0
     * @desc    根据传入客户好，判断改客户客户是否集团下客户信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("是否集团成员判断")
    @GetMapping("/selectCusIsGrpMemCus/{cusId}")
    protected ResultDto<Map<String,String>> selectCusIsGrpMemCus(@PathVariable("cusId") String cusId) {
        Map<String,String> map = apprStrMtableInfoService.selectCusIsGrpMemCus(cusId);
        return new ResultDto<>(map);
    }

    /**
     * @param queryModel
     *      grpNo:集团编号   instuCde:金融机构号
     * @return List<Map<String,Object>>
     * @date 2021/4/19 15:14
     * @version 1.0.0
     * @desc    集团成员列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryGrpMemberLmtInfoList")
    protected ResultDto<List<Map<String,Object>>> queryGrpMemberLmtInfoList(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprStrMtableInfoService.queryGrpMemberLmtInfoList(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * 根据客户号查询该客户是否存在授信
     * @param cusId
     * @return
     */
    @PostMapping("/selectByCusId")
    protected ResultDto<Map> selectByCusId(@RequestBody String cusId) {
        Map result = apprStrMtableInfoService.selectByCusId(cusId);
        return new ResultDto<Map>(result);
    }

    /**
     * @param queryModel
     *      cusType 客户类型（Y）；instuCde 金融机构号 （Y）；支持cusId客户号、cusName客户名称查询
     *      notInstuCde 金融机构代码不为传入参数；
     * @return List<ApprStrMtableInfo>
     * @date 2021/4/19 15:14
     * @version 1.0.0
     * @desc    单一客户额度视图列表（法人、个人）
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryLmtListBymodel")
    protected ResultDto queryLmtListBymodel(@RequestBody QueryModel queryModel) {
        ResultDto dto = new ResultDto();
        String cusId = (String) queryModel.getCondition().get("cusId");
        Map<String, String> map = new HashMap<>();
        if (StringUtils.isNotBlank(cusId)) {
            map = apprStrMtableInfoService.selectCusIsGrpMemCus(cusId);
        }
        if (EclEnum.ECL070091.key.equals(map.get("rtnCode"))) {
            dto.setData(map.get("rtnMsg"));
            return dto;
        }
        ResultDto list = apprStrMtableInfoService.queryLmtListBymodel(queryModel);
        return list;
    }

    /**
     * 投后业务查询（贷后）
     * @param queryModel
     * @return
     * @创建人：周茂伟
     */
    @PostMapping("/selectForPsp")
    protected ResultDto<List<ApprStrMtableInfoDto>> selectForPsp(@RequestBody QueryModel queryModel){
        List<ApprStrMtableInfoDto> list = apprStrMtableInfoService.selectForPsp(queryModel);
        return new ResultDto<List<ApprStrMtableInfoDto>>(list);
    }


    /**
     * @param queryModel
     * @return List<ApprStrMtableInfo>
     * @date 2021/4/19 15:14
     * @version 1.0.0
     * @desc    单一客户额度视图列表（法人、个人）
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryListForComIndivLmtList")
    protected ResultDto queryListForComIndivLmtList(@RequestBody QueryModel queryModel) {
        ResultDto dto = new ResultDto();
        String cusId = (String) queryModel.getCondition().get("cusId");
        Map<String, String> map = new HashMap<>();
        if (StringUtils.isNotBlank(cusId)) {
            map = apprStrMtableInfoService.selectCusIsGrpMemCus(cusId);
        }
        if (EclEnum.ECL070091.key.equals(map.get("rtnCode"))) {
            dto.setData(map.get("rtnMsg"));
            return dto;
        }
        ResultDto resultDto = apprStrMtableInfoService.queryListForComIndivLmtList(queryModel);
        return resultDto;
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * 分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByApprApp")
    protected ResultDto<List<Map<String,Object>>> selectByApprApp(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>>  list = apprStrMtableInfoService.selectByApprApp(queryModel);
        return new ResultDto<List<Map<String,Object>>>(list);
    }

}


