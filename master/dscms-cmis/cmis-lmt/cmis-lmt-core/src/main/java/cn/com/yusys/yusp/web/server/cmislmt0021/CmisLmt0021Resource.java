package cn.com.yusys.yusp.web.server.cmislmt0021;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.req.CmisLmt0021ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.resp.CmisLmt0021RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0021.CmisLmt0021Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:对公授信信息查询
 *
 * @author xll 20210515
 * @version 1.0
 */
@Api(tags = "cmislmt0021:对公授信信息查询")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0021Resource.class);

    @Autowired
    private CmisLmt0021Service cmisLmt0021Service;

    /**
     * 交易码：cmislmt0021
     * 交易描述：查询对公客户授信信息
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("查询对公客户授信信息")
    @PostMapping("/cmislmt0021")
    protected @ResponseBody
    ResultDto<CmisLmt0021RespDto> cmisLmt0021(@Validated @RequestBody CmisLmt0021ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0021.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0021RespDto> cmisLmt0021RespDtoResultDto = new ResultDto<>();
        CmisLmt0021RespDto cmisLmt0021RespDto = new CmisLmt0021RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0021RespDto = cmisLmt0021Service.execute(reqDto);
            cmisLmt0021RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0021RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0021.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0021.value, e.getMessage());
            // 封装xddb0001DataResultDto中异常返回码和返回信息
            cmisLmt0021RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0021RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0021RespDto到cmisLmt0021RespDtoResultDto中
        cmisLmt0021RespDtoResultDto.setData(cmisLmt0021RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0021.value, JSON.toJSONString(cmisLmt0021RespDtoResultDto));
        return cmisLmt0021RespDtoResultDto;
    }

}
