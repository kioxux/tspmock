package cn.com.yusys.yusp.service.server.cmislmt0064;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0064.req.CmisLmt0064ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0064.resp.CmisLmt0064RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0064Service
 * @类描述: #对内服务类
 * @功能描述: 根据客户号获取客户综合授信批复编号，到期日，起始日，期限等
 * @创建时间: 2021-09-30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0064Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0064Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    /**
     * 关联交易计算个人客户余额
     * 1、如果授信为循环类且为生效或冻结状态则取授信敞口
     * 2、如果授信为循环类且为到期未结清状态则取用信余额
     * 3、如果授信为非循环类则取用信余额
     * add by zhangjw 20211021
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0064RespDto execute(CmisLmt0064ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0064.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0064.value);
        CmisLmt0064RespDto respDto = new CmisLmt0064RespDto();
        try {
            //客户号
            String cusId = reqDto.getCusId();
            String instuCde = reqDto.getInstuCde();

            BigDecimal lmtAmt =  apprLmtSubBasicInfoService.selectLmtAmtGeRenGljy(cusId,instuCde);

            respDto.setLmtAmt(lmtAmt);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0064】关联交易计算个人客户余额：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0064.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0064.value);
        return respDto;
    }
}