package cn.com.yusys.yusp.service.server.cmislmt0007;

import cn.com.yusys.yusp.dto.server.cmislmt0007.req.CmisLmt0007ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.req.CmisLmt0007ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.req.CmisLmt0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.resp.CmisLmt0007RespDto;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprCoopInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.service.server.cmislmt0006.CmisLmt0006Service;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0007Service
 * @类描述: #对内服务类
 * @功能描述: 额度解冻
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0007Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0007Service.class);

    @Autowired
    private ApprCoopInfoMapper apprCoopInfoMapper;

    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper;

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper;

    @Autowired
    private CmisLmt0006Service cmisLmt0006Service;

    @Transactional
    public CmisLmt0007RespDto thaw(CmisLmt0007ReqDto reqDto){
        logger.info(this.getClass().getName()+":额度解冻接口接收数据开始............. ");
        CmisLmt0007RespDto respDto = new CmisLmt0007RespDto();
        try{
            //最新更新人
            String inputId = reqDto.getInputId();
            //最新更新机构
            String inputBrId = reqDto.getInputBrId();
            //最新更新日期
            String inputDate = reqDto.getInputDate();

            //合作方额度解冻
            if (reqDto.getLmtType().equals(CmisLmtConstants.STD_ZB_LMT_TYPE_03)){
                for (CmisLmt0007ApprListReqDto apprListRrqDto:reqDto.getApprList()) {
                    //合作方额度全部解冻
                    if (apprListRrqDto.getOptType().equals(CmisLmtConstants.STD_ZB_APPR_INFO_02)){
                        //合作方额度额度解冻
                        cmisLmt0006Service.updateAoopStatus(apprListRrqDto.getApprSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_01);
                        //合作方全部分项额度解冻
                        cmisLmt0006Service.updateAllAoopSubStatus(apprListRrqDto.getApprSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_01);
                    }
                }
                //合作方部分授信额度解冻
                List<CmisLmt0007ApprSubListReqDto> cmisLmt0007ApprSubListReqDtos = reqDto.getApprSubList() ;
                if (CollectionUtils.isNotEmpty(cmisLmt0007ApprSubListReqDtos)){
                    for (CmisLmt0007ApprSubListReqDto apprSubListReqDto : cmisLmt0007ApprSubListReqDtos) {
                        cmisLmt0006Service.updatePartAoopSubStatus(apprSubListReqDto.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_01);
                    }
                }
            }else {
                for (CmisLmt0007ApprListReqDto apprListRrqDto:reqDto.getApprList()) {
                    if (apprListRrqDto.getOptType().equals(CmisLmtConstants.STD_ZB_APPR_INFO_02)){
                        //单一客户全部解冻
                        cmisLmt0006Service.updateStrMtableStatus(apprListRrqDto.getCusId(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_01);
                        //单一用户分项全部解冻
                        cmisLmt0006Service.updateAllLmtSubBasicStatus(apprListRrqDto.getCusId(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_01);
                        //解冻该单一客户所属的集团客户
                        cmisLmt0006Service.judgeUpdateGrp(apprListRrqDto.getCusId(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_01);
                    }
                }
                //单一用户分项部分解冻
                List<CmisLmt0007ApprSubListReqDto> cmisLmt0007ApprSubListReqDtos = reqDto.getApprSubList() ;
                if (CollectionUtils.isNotEmpty(cmisLmt0007ApprSubListReqDtos)){
                    for (CmisLmt0007ApprSubListReqDto subListReqDto : cmisLmt0007ApprSubListReqDtos) {
                        cmisLmt0006Service.updatePartLmtSubBasicStatus(subListReqDto.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_01);
                    }
                }
            }
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        }catch (YuspException e) {
            logger.error("额度解冻接口报错：", e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(this.getClass().getName()+":额度解冻接口接收数据结束............. ");
        return respDto;
    }
}
