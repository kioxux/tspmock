package cn.com.yusys.yusp.web.server.cmislmt0003;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0003.req.CmisLmt0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0003.resp.CmisLmt0003RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0003.CmisLmt0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:同业额度同步
 *
 * @author dmd 20210507
 * @version 1.0
 */
@Api(tags = "cmislmt0003:同业额度同步")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0003Resource.class);

    @Autowired
    private CmisLmt0003Service cmisLmt0003Service;
    /**
     * 交易码：cmislmt0003
     * 交易描述：同业额度同步
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("同业额度同步")
    @PostMapping("/cmislmt0003")
    protected @ResponseBody
    ResultDto<CmisLmt0003RespDto> CmisLmt0003(@Validated @RequestBody CmisLmt0003ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0003.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0003.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0003RespDto> cmisLmt0003RespDtoResultDto = new ResultDto<>();
        CmisLmt0003RespDto cmisLmt0003RespDto = new CmisLmt0003RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0003RespDto = cmisLmt0003Service.execute(reqDto);
            cmisLmt0003RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0003RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (YuspException e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0003.value, e);
            cmisLmt0003RespDto.setErrorCode(e.getCode());
            cmisLmt0003RespDto.setErrorMsg(e.getMsg());
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0003.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0003.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0003.value, e.getMessage());
            // 封装CmisLmt0003RespDtoResultDto中异常返回码和返回信息
            cmisLmt0003RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0003RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }

        // 封装CmisLmt0003RespDto到CmisLmt0003RespDtoResultDto中
        cmisLmt0003RespDtoResultDto.setData(cmisLmt0003RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0003.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0003.value, JSON.toJSONString(cmisLmt0003RespDtoResultDto));
        return cmisLmt0003RespDtoResultDto;
    }
}
