/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprStrMtableChgAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 86188
 * @创建时间: 2021-05-11 10:36:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ApprStrMtableChgAppService {
    private static final Logger log = LoggerFactory.getLogger(ApprStrMtableChgAppService.class);
    @Autowired
    private ApprStrMtableChgAppMapper apprStrMtableChgAppMapper;
    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private  ApprStrMtableInfoService apprStrMtableInfoService ;
    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;
    @Autowired
    private ApprLmtSubChgAppService apprLmtSubChgAppService ;

    @Autowired
    private ApprLmtSubChgAppMapper apprLmtSubChgAppMapper ;

    @Autowired
    private LmtContRelChgAppService lmtContRelChgAppService ;

    @Autowired
    private LmtContRelChgAppMapper lmtContRelChgAppMapper ;

    @Autowired
    private ContAccRelChgAppService contAccRelChgAppService ;

    @Autowired
    private ContAccRelChgAppMapper contAccRelChgAppMapper ;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ApprStrMtableChgApp selectByPrimaryKey(String pkId) {
        return apprStrMtableChgAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ApprStrMtableChgApp> selectAll(QueryModel model) {
        List<ApprStrMtableChgApp> records = (List<ApprStrMtableChgApp>) apprStrMtableChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ApprStrMtableChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ApprStrMtableChgApp> list = apprStrMtableChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ApprStrMtableChgApp record) {
        return apprStrMtableChgAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ApprStrMtableChgApp record) {
        return apprStrMtableChgAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ApprStrMtableChgApp record) {
        return apprStrMtableChgAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ApprStrMtableChgApp record) {
        return apprStrMtableChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return apprStrMtableChgAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return apprStrMtableChgAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(String pkId) {
        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("pkId", pkId);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType", CmisLmtConstants.OPR_TYPE_DELETE);
        ApprStrMtableChgApp record = apprStrMtableChgAppMapper.selectByPrimaryKey(pkId);
        delMap.put("appSerno", record.getAppSerno());
        QueryModel queryModel = new QueryModel();
        //批复编号
        queryModel.addCondition("appSerno", record.getAppSerno());
        //查询批复信息
        List<ApprLmtSubChgApp> apprLmtSubChgAppList = apprLmtSubChgAppMapper.selectByModel(queryModel);
        List<LmtContRelChgApp> lmtContRelChgAppList = lmtContRelChgAppMapper.selectByModel(queryModel);
        List<ContAccRelChgApp> contAccRelChgAppList = contAccRelChgAppMapper.selectByModel(queryModel);
        //批复是否为空
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(apprLmtSubChgAppList)) {
            apprLmtSubChgAppMapper.updateByAppSerno(delMap);
        }
        //批复是否为空
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(lmtContRelChgAppList)) {
            lmtContRelChgAppMapper.updateByAppSerno(delMap);
        }
        //批复是否为空
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(contAccRelChgAppList)) {
            contAccRelChgAppMapper.updateByAppSerno(delMap);
        }
        return apprStrMtableChgAppMapper.updateByParams(delMap);
    }

    /**
     * @方法名称: valiLmtChgAppInWay
     * @方法描述: 根据批复编号，查询批复是否存在在途申请
     * @参数与返回说明: 如果返回记录大于1 则存在在途记录，否则不存在
     * @算法描述: 无
     */

    public int valiLmtChgAppInWay(String serno) {
        ApprStrMtableChgApp apprStrMtableChgApp = new ApprStrMtableChgApp() ;
        //设置批复编号
        apprStrMtableChgApp.setApprSerno(serno);
        return apprStrMtableChgAppMapper.valiLmtChgAppInWay(apprStrMtableChgApp);
    }


    /**
     * @方法名称: insertApprStrMChgApp
     * @方法描述: 获取批复数据，根据批复台账主表信息，生成批复调整申请信息
     * @参数与返回说明:返回影响数据记录，等于0 操作失败
     * @算法描述: 无
     */

    public ApprStrMtableChgApp createApprStrMChgApp(ApprStrMtableChgApp apprStrMtableChgApp) {
        //获取批复编号
        String apprSerno = apprStrMtableChgApp.getApprSerno() ;
        //根据额度编号
        ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfoService.getAppStrMTableInfoBySerno(apprSerno) ;
        Optional.ofNullable(apprStrMtableInfo).orElseThrow(()->new YuspException(EclEnum.ECL070016.key,EclEnum.ECL070016.value));
        //生成主键
        HashMap<String, String> param = new HashMap<>();
        String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashMap<>());
        //生成流水号
        String appSerno = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.LMT_SERNO, new HashMap<>());

        //将批复台账中的数据拷贝到批复调整申请中
        BeanUtils.copyProperties(apprStrMtableInfo, apprStrMtableChgApp);
        //设置主键
        apprStrMtableChgApp.setPkId(pkValue);
        //设置新的申请流水号
        apprStrMtableChgApp.setAppSerno(appSerno);
        //设置批复编号
        apprStrMtableChgApp.setApprSerno(apprSerno);

        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        // 申请人
        apprStrMtableChgApp.setInputId(userInfo.getLoginCode());
        // 申请机构
        apprStrMtableChgApp.setInputBrId(userInfo.getOrg().getCode());
        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期
        // 申请时间
        apprStrMtableChgApp.setInputDate(openDay);
        // 更新人
        apprStrMtableChgApp.setUpdId(userInfo.getLoginCode());
        // 更新机构
        apprStrMtableChgApp.setUpdBrId(userInfo.getOrg().getCode());
        //更新日期
        apprStrMtableChgApp.setUpdDate(openDay);
        //修改时间
        apprStrMtableChgApp.setUpdateTime(new Date());
        // 创建时间
        apprStrMtableChgApp.setCreateTime(new Date());
        // 审批状态
        apprStrMtableChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        return apprStrMtableChgApp;
    }

    /**
     * @方法名称: initApprStrMTableChgAppInfo
     * @方法描述: 初始化额度调账变更信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map initApprStrMTableChgAppInfo(ApprStrMtableChgApp apprStrMtableChgApp){
        HashMap<String,String> resultMap = new HashMap() ;
        //获取批复变更信息
        apprStrMtableChgApp = createApprStrMChgApp(apprStrMtableChgApp) ;
        //数据插入操作
        apprStrMtableChgAppMapper.insert(apprStrMtableChgApp) ;

        String apprSerno = apprStrMtableChgApp.getApprSerno() ;
        String appSerno = apprStrMtableChgApp.getAppSerno() ;
        //初始化批复批复变更分项信息
        List<ApprLmtSubChgApp> apprLmtSubChgApps = apprLmtSubChgAppService.createAppLmtSubChgApp(apprSerno) ;
        //遍历处理
        for(ApprLmtSubChgApp apprLmtSubChgApp:apprLmtSubChgApps){
            //初始申请信息
            apprLmtSubChgApp.setSubAppSerno(appSerno);
            //数据落表处理
            apprLmtSubChgAppMapper.insert(apprLmtSubChgApp) ;
            //获取分项编号
            String apprSubSerno = apprLmtSubChgApp.getApprSubSerno() ;

            if(!StringUtils.isEmpty(apprSubSerno)){
                //根据分项编号处理分项占用信息  以及  合同占用关系
                lmtContRelChgAppService.createLmtcontRelChgApp(apprSubSerno,appSerno);
            }
        }
        resultMap.put("appSerno", appSerno);
        return resultMap ;
    }

    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 流程申请状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateApproveStatus(ApprStrMtableChgApp apprStrMtableChgApp) {
        return apprStrMtableChgAppMapper.updateApproveStatus(apprStrMtableChgApp);
    }

    /**
     * @方法名称: selectByAppSerno
     * @方法描述: 根据申请流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprStrMtableChgApp selectByAppSerno(String appSerno) {
        return apprStrMtableChgAppMapper.selectByAppSerno(appSerno);
    }

    /**
     * 单一客户额度调整申请  审批通过后处理
     * 1、更新额度主信息不变 最近更新人  最近更新机构  最近更新时间  修改时间
     * 2、更新额度分项信息已用金额等金额 最近更新人  最近更新机构  最近更新时间  修改时间
     * 3、更新分项占用关系数据
     * 4、更新台账占用关系数据
     * @param appSerno
     * credit by zhangjw 2021-04-26
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessDataAfterEnd(String appSerno) {
        try {

            log.info("审批通过更新单一客户额度主信息：获取单一客户额度调整申请" + appSerno + "主表信息");
            ApprStrMtableChgApp apprStrMtableChgApp = apprStrMtableChgAppMapper.selectByAppSerno(appSerno);
            if (apprStrMtableChgApp == null) {
                throw new YuspException(EclEnum.ECL070042.key, EclEnum.ECL070042.value);
            }
            String serno = apprStrMtableChgApp.getApprSerno();//批复台账编号
            Map params = new HashMap();
            params.put("updId", apprStrMtableChgApp.getUpdId());
            params.put("updBrId", apprStrMtableChgApp.getUpdBrId());
            params.put("updDate", apprStrMtableChgApp.getUpdDate());
            params.put("updBrId", apprStrMtableChgApp.getUpdBrId());
            params.put("updateTime", DateUtils.getCurrTimestamp());

            log.info("审批通过更新单一客户额度主信息，批复台账编号【" + serno +"】");
            int apprStrMtableInfo = apprStrMtableInfoMapper.updateApprStrMtableInfoByParams(params);
            if (apprStrMtableInfo < 0) {
                throw new YuspException(EclEnum.ECL070043.key,EclEnum.ECL070043.value);
            }
            log.info("审批通过更新单一客户额度分项信息_获取额度分项调整信息" + appSerno);
            Map rtnData = new HashMap();
            rtnData.put("subAppSerno", appSerno);

            //根据主申请流水号获取批复额度分项调整申请信息
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("subAppSerno",appSerno);
            List<ApprLmtSubChgApp> apprLmtSubChgAppList= apprLmtSubChgAppService.selectAll(queryModel);

            /*分项调整申请信息不为空时，循环更新批复额度分项基础信息*/
            if (CollectionUtils.isNotEmpty(apprLmtSubChgAppList)) {
                for (ApprLmtSubChgApp apprLmtSubChgApp : apprLmtSubChgAppList){
                    String apprSubSerno = apprLmtSubChgApp.getApprSubSerno();//获取分项调整申请的批复分项编号

                    //根据分项调整申请的批复分项编号 获取 批复额度分项基础信息   更新批复额度分项基础信息
                    ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectBySerno(apprSubSerno);

                    if (apprLmtSubBasicInfo==null || "".equals(apprLmtSubBasicInfo.getPkId())) {
                        throw new YuspException(EclEnum.ECL070044.key, EclEnum.ECL070044.value);
                    }

                    apprLmtSubBasicInfo.setLmtAmtAdd(apprLmtSubChgApp.getLmtAmtAdd());
                    apprLmtSubBasicInfo.setOutstndAmt(apprLmtSubChgApp.getOutstndAmt());
                    apprLmtSubBasicInfo.setSpacOutstndAmt(apprLmtSubChgApp.getSpacOutstndAmt());
                    apprLmtSubBasicInfo.setPvpOutstndAmt(apprLmtSubChgApp.getPvpOutstndAmt());
                    apprLmtSubBasicInfo.setAvlOutstndAmt(apprLmtSubChgApp.getAvlOutstndAmt());
                    apprLmtSubBasicInfo.setLoanBalance(apprLmtSubChgApp.getLoanBalance());
                    apprLmtSubBasicInfo.setSpacOutstndAmt(apprLmtSubChgApp.getSpacOutstndAmt());
                    apprLmtSubBasicInfo.setLoanSpacBalance(apprLmtSubChgApp.getLoanSpacBalance());
                    apprLmtSubBasicInfoService.update(apprLmtSubBasicInfo);
                }
            }

            /*处理分项占用关系数据*/
            lmtContRelChgAppService.handleBusinessDataAfterEnd(appSerno);

            /*处理合同占用关系数据*/
            contAccRelChgAppService.handleBusinessDataAfterEnd(appSerno);

            log.info("审批通过更新单一客户额度调整申请" + appSerno + "审批状态为【997】-审批通过");
            apprStrMtableChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            apprStrMtableChgAppMapper.updateApproveStatus(apprStrMtableChgApp);
        } catch(YuspException e){
            throw e;
        } catch (Exception e) {
            log.error("流程审批通过业务处理发生异常！", e);
        }
    }

    /**
     *@函数名称:queryStrChgAppBySerno
     *@函数描述:根据调整申请流水号和主批复台账编号加工展示主调整申请主信息
     * @参数与返回说明:
     * @param
     *
     * @算法描述:
     */
    public List<Map<String, Object>> queryStrChgAppBySerno(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = apprStrMtableChgAppMapper.queryStrChgAppBySerno(model);
        PageHelper.clearPage();
        return list;
    }
}


