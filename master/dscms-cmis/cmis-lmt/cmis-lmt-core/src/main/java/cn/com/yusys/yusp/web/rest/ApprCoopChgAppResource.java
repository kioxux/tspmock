/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ApprCoopChgApp;
import cn.com.yusys.yusp.domain.ApprCoopInfo;
import cn.com.yusys.yusp.service.ApprCoopChgAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopChgAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: yuanz
 * @创建时间: 2021-04-20 22:35:33
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/apprcoopchgapp")
public class ApprCoopChgAppResource {
    @Autowired
    private ApprCoopChgAppService apprCoopChgAppService;

    private static final Logger log = LoggerFactory.getLogger(ApprCoopChgAppResource.class);

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ApprCoopChgApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<ApprCoopChgApp> list = apprCoopChgAppService.selectAll(queryModel);
        return new ResultDto<List<ApprCoopChgApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ApprCoopChgApp>> index(QueryModel queryModel) {
        List<ApprCoopChgApp> list = apprCoopChgAppService.selectByModel(queryModel);
        return new ResultDto<List<ApprCoopChgApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ApprCoopChgApp> show(@PathVariable("pkId") String pkId) {
        ApprCoopChgApp apprCoopChgApp = apprCoopChgAppService.selectByPrimaryKey(pkId);
        return new ResultDto<ApprCoopChgApp>(apprCoopChgApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ApprCoopChgApp> create(@RequestBody ApprCoopChgApp apprCoopChgApp) {
        apprCoopChgAppService.insert(apprCoopChgApp);
        return new ResultDto<ApprCoopChgApp>(apprCoopChgApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ApprCoopChgApp apprCoopChgApp) {
        int result = apprCoopChgAppService.update(apprCoopChgApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = apprCoopChgAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = apprCoopChgAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete/{pkId}")
    protected ResultDto<Integer> logicalDelete(@PathVariable("pkId") String pkId) {
        int result = apprCoopChgAppService.logicDelete(pkId);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:saveapprCoopChgApp
     * @函数描述:额度项下业务申请修改页面保存操作
     * @参数与返回说明:
     * @算法描述:
     */
//    @PostMapping("/saveapprCoopChgApp")
//    public ResultDto<Map> saveapprCoopChgApp(@RequestBody Map params) throws URISyntaxException {
//        log.info("额度项下合作方授信台账调整申请修改保存【{}】", params.toString());
//        Map result = apprCoopChgAppService.updateApprCoopChgApp(params);
//        return new ResultDto<Map>(result);
//    }

    /**
     * @函数名称:initApprCoopChgAppInfo
     * @函数描述:额度项下业务，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/initApprCoopChgAppInfo")
    protected ResultDto<Map> initApprCoopChgAppInfo(@Validated @RequestBody ApprCoopInfo apprCoopInfo) {
        Map result = apprCoopChgAppService.initApprCoopChgAppInfo(apprCoopInfo);
        return new ResultDto<Map>(result);
    }

    /**
     * @函数名称:valiLmtChgAppInWay
     * @函数描述:校验该批复是否存在在途的申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/valilmtchgappinway/{serno}")
    protected ResultDto<Integer> valiLmtChgAppInWay(@Validated @PathVariable String serno) {
        int result = apprCoopChgAppService.valiLmtChgAppInWay(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:querySubChgAppBySerno
     * @函数描述:根据调整申请流水号和合作方批复台账编号加工展示合作方调整申请主信息
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryApprCoopChgAppInfoBySerno")
    protected ResultDto<List<Map<String,Object>>> querySubChgAppBySerno(@RequestBody QueryModel queryModel) {
        List<Map<String,Object>> list = apprCoopChgAppService.querySubChgAppBySerno(queryModel);
        return new ResultDto<>(list);
    }


}
