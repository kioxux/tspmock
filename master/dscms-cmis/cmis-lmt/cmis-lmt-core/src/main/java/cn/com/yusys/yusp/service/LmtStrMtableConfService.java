/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ApprCoopChgApp;
import cn.com.yusys.yusp.domain.LmtStrMtableConf;
import cn.com.yusys.yusp.domain.LmtSubBasicConf;
import cn.com.yusys.yusp.dto.LmtStrMtableSubBasicConfDto;
import cn.com.yusys.yusp.dto.server.cmislmt0018.req.CmisLmt0018ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0018.resp.CmisLmt0018StrListRespDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.LmtStrMtableConfMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSubBasicConfMapper;
import cn.com.yusys.yusp.repository.provider.GenericSqlProvider;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtStrMtableConfService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 86188
 * @创建时间: 2021-05-11 10:36:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtStrMtableConfService {
    private static final Logger log = LoggerFactory.getLogger(LmtStrMtableConfService.class);
    @Autowired
    private LmtStrMtableConfMapper lmtStrMtableConfMapper;

    @Autowired
    private LmtSubBasicConfMapper lmtSubBasicConfMapper;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtStrMtableConf selectByPrimaryKey(String lmtCfgSerno) {
        return lmtStrMtableConfMapper.selectByPrimaryKey(lmtCfgSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtStrMtableConf> selectAll(QueryModel model) {
        List<LmtStrMtableConf> records = (List<LmtStrMtableConf>) lmtStrMtableConfMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtStrMtableConf> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtStrMtableConf> list = lmtStrMtableConfMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtStrMtableConf record) {
        return lmtStrMtableConfMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtStrMtableConf record) {
        return lmtStrMtableConfMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtStrMtableConf record) {
        return lmtStrMtableConfMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtStrMtableConf record) {
        return lmtStrMtableConfMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String lmtCfgSerno) {
        return lmtStrMtableConfMapper.deleteByPrimaryKey(lmtCfgSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtStrMtableConfMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(String lmtSerno) {
        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("lmtSerno",lmtSerno);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType",  CmisLmtConstants.OPR_TYPE_DELETE);
        return lmtStrMtableConfMapper.updateByParams(delMap);
    }

    /**
     * 根据FK_PKID获取额度结构主表信息
     * @param id
     * @return
     */
    public LmtStrMtableConf getLmtStrByPkId(String id) {
        LmtStrMtableConf lmtStrMtableConf = null;
        QueryModel model = new QueryModel();
        model.addCondition("pkId", id);
        List<LmtStrMtableConf> records = (List<LmtStrMtableConf>) lmtStrMtableConfMapper.selectByModel(model);
        if(records!=null&&records.size()>0) {
            lmtStrMtableConf = records.get(0);
        }
        return lmtStrMtableConf;
    }

    /**
     * 保存基本信息
     * @param lmtStrMtableConf
     * @return
     */
    public int save(LmtStrMtableConf lmtStrMtableConf) {
        //根据FK_PKID获取额度结构主表信息
        LmtStrMtableConf lmtStrByPkId = getLmtStrByPkId(lmtStrMtableConf.getLmtCfgSerno());
        int strCount = 0;
        if(lmtStrByPkId == null){
            strCount = lmtStrMtableConfMapper.insertSelective(lmtStrMtableConf);
        }else{
            strCount = lmtStrMtableConfMapper.updateByPrimaryKeySelective(lmtStrMtableConf);
        }

        //额度主表更细为1的时候才允许更新额度分项基础
        if(strCount != 1){
            throw new YuspException(EclEnum.LMT_UPDATE_EXCEPTION.key,"额度结构主表" + EclEnum.LMT_UPDATE_EXCEPTION.value+ "；更新条数：" + strCount);
        }
        return strCount;
    }

    /**
     * 获取额度结构数据
     * @param pkId
     * @return
     */
    public LmtStrMtableSubBasicConfDto query(String pkId) {
        LmtStrMtableSubBasicConfDto lmtStrMtableSubBasicConfDto = lmtStrMtableConfMapper.query(pkId);
        return lmtStrMtableSubBasicConfDto;
    }

	private LmtStrMtableConf queryTopConf(LmtStrMtableConf lmtStrMtableConf) {
		String parentId = ""; //lmtStrMtableConf.getParentId();
		QueryModel model = new QueryModel();
		model.addCondition("childId", parentId);
		model.addCondition("isBegin", "Y");
		model.addCondition("oprType", "01");
		List<LmtStrMtableConf> lmtStrMtableConfList = lmtStrMtableConfMapper.selectByModel(model);
		if(lmtStrMtableConfList!=null&&lmtStrMtableConfList.size()>0) {
			lmtStrMtableConf = queryTopConf(lmtStrMtableConfList.get(0));
		}
		return lmtStrMtableConf;
	}

    public List<CmisLmt0018StrListRespDto> selectLmtStrConfByStrType(CmisLmt0018ReqDto reqDto) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("instuCde",reqDto.getInstuCde());
        queryModel.addCondition("strType", reqDto.getStrType());
        queryModel.addCondition("suitApplicableLine", reqDto.getSuitApplicableLine());
        return lmtStrMtableConfMapper.selectLmtStrConfByStrType(queryModel);
    }

    /**
     * @方法名称: valiLmtChgAppInWay
     * @方法描述: 根据额度结构编号，查询是否存在额度结构编号
     * @参数与返回说明: 如果返回记录大于等于1 则存在在途记录，否则不存在
     * @算法描述: 无
     */

    public  Map<String, String> valiLimitStrNoInWay(String limitStrNo) {
        HashMap<String, String> resultMap = new HashMap();
        String rtnCode = EclEnum.LMT_CTR_DEF_SUCCESS.key;
        String rtnMsg = EclEnum.LMT_CTR_DEF_SUCCESS.value;
//        try {
            QueryModel queryModel = new QueryModel() ;
            queryModel.addCondition("limitStrNo", limitStrNo);
            int rtnData = lmtStrMtableConfMapper.valiLimitStrNoInWay(queryModel);
            if (rtnData != 0) {
                rtnCode = EclEnum.ECL070092.key;
                rtnMsg = EclEnum.ECL070092.value;
//                return resultMap;
            }
//        } catch (YuspException e) {
//            rtnCode = e.getCode();
//            rtnMsg = e.getMessage();
//        } finally {
            resultMap.put("rtnCode", rtnCode);
            resultMap.put("rtnMsg", rtnMsg);
//        }
        return resultMap ;
    }

    /**
     * @param lmtStrMtableConf
     * @author zhangjw
     * @date 2021/5/7
     * @version 1.0.0
     * @desc 新增额度结构配置，同时保存对应根节点
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int addLmtStrMtableConf(LmtStrMtableConf lmtStrMtableConf) {
        //保存额度结构配置主表
        lmtStrMtableConf.setLmtCfgSerno(sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashMap<>()));
        int result = lmtStrMtableConfMapper.insert(lmtStrMtableConf);

        //创建额度结构配置根目录
        LmtSubBasicConf lmtSubBasicConf = new LmtSubBasicConf();
        lmtSubBasicConf.setPkId(sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashMap<>()));
        lmtSubBasicConf.setLimitStrNo(lmtStrMtableConf.getLimitStrNo());
        lmtSubBasicConf.setLimitSubNo(lmtStrMtableConf.getLimitStrNo());
        lmtSubBasicConf.setLimitSubName(lmtStrMtableConf.getLimitStrName());
        lmtSubBasicConf.setParentId("");
        lmtSubBasicConf.setIsLriskLmt(CmisLmtConstants.YES_NO_N);
        lmtSubBasicConf.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_01);
        lmtSubBasicConf.setIsRevolv(CmisLmtConstants.YES_NO_Y);
        lmtSubBasicConf.setLimitType(CmisLmtConstants.STD_ZB_LIMIT_TYPE_01);
        lmtSubBasicConf.setIsExpertLmt(CmisLmtConstants.YES_NO_N);
        lmtSubBasicConf.setOprType(CmisLmtConstants.OPR_TYPE_ADD);

        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期

        lmtSubBasicConf.setInputId(userInfo.getLoginCode());
        lmtSubBasicConf.setInputBrId(userInfo.getOrg().getCode());
        lmtSubBasicConf.setInputDate(openDay);
        lmtSubBasicConf.setUpdId(userInfo.getLoginCode());
        lmtSubBasicConf.setUpdBrId(userInfo.getOrg().getCode());
        lmtSubBasicConf.setUpdDate(openDay);

        Date currTime = null ;
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTime = DateUtils.getCurrentDate(DateFormatEnum.DATETIME);
        try{
            currTime = simpleDateFormat.parse(nowTime) ;
        } catch (ParseException e) {
            log.error(e.getMessage(),e);
        }

        lmtSubBasicConf.setCreateTime(currTime);
        lmtSubBasicConf.setUpdateTime(currTime);
        result = lmtSubBasicConfMapper.insert(lmtSubBasicConf);

        return result;
    }

    /**
     * @param limitStrNo
     * @author zhangjw
     * @date 2021/5/7
     * @version 1.0.0
     * @desc 根据额度结构编号删除额度结构（包含额度结构子节点）
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int deleteConfByLimitStrNo(String limitStrNo) {
        Map<String,String> model = new HashMap<String,String>();
        model.put("oprType",CmisLmtConstants.OPR_TYPE_DELETE);
        model.put("limitStrNo",limitStrNo);

        int result = 0;
        result = lmtStrMtableConfMapper.updateByParams(model);

        //逻辑删除额度结构项下子节点
        result = lmtSubBasicConfMapper.updateConfInfoByLimitSubNo(limitStrNo);
        return result;
    }
}
