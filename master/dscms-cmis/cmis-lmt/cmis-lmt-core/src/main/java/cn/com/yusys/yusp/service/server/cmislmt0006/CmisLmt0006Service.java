package cn.com.yusys.yusp.service.server.cmislmt0006;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ApprCoopSubInfo;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.resp.CmisLmt0006RespDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprCoopInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0006Service
 * @类描述: #对内服务类
 * @功能描述: 额度冻结
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0006Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0006Service.class);

    @Autowired
    private ApprCoopInfoMapper apprCoopInfoMapper;

    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper;

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Transactional
    public CmisLmt0006RespDto frozen(CmisLmt0006ReqDto reqDto){
        logger.info(this.getClass().getName()+":额度冻结接口接收数据开始............. ");
        CmisLmt0006RespDto respDto = new CmisLmt0006RespDto();
        try {
            //最新更新人
            String inputId = reqDto.getInputId();
            //最新更新机构
            String inputBrId = reqDto.getInputBrId();
            //最新更新日期
            String inputDate = reqDto.getInputDate();

            //合作方额度冻结
            if (reqDto.getLmtType().equals(CmisLmtConstants.STD_ZB_LMT_TYPE_03)){
                logger.info(this.getClass().getName()+":合作方额度冻结处理start............. ");
                for (CmisLmt0006ApprListReqDto apprListRrqDto:reqDto.getApprList()) {
                    //合作方额度全部冻结
                    if (apprListRrqDto.getOptType().equals(CmisLmtConstants.STD_ZB_APPR_INFO_02)){
                        String apprSerno = apprListRrqDto.getApprSerno() ;
                        if(StringUtils.isBlank(apprSerno)){
                            respDto.setErrorCode(EclEnum.ECL070115.key);
                            respDto.setErrorMsg(EclEnum.ECL070115.value);
                            return respDto;
                        }

                        //将生效的合作方额度分项进行冻结
                        updateAllAoopSubStatus(apprSerno,inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_11);
                        //合作方额度分项全部为冻结，才将合作方台账修改为冻结
                        List<ApprCoopSubInfo> statusList = apprCoopSubInfoMapper.getStatusByApprSerno(apprSerno);
                        boolean flag = true;
                        for (ApprCoopSubInfo apprCoopSubInfo:statusList) {
                            if (!apprCoopSubInfo.getStatus().equals(CmisLmtConstants.STD_ZB_APPR_ST_11)){
                                flag = false;
                                break;
                            }
                        }
                        if (flag){
                            updateAoopStatus(apprSerno,inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_11);
                        }
                    }
                }
                //合作方部分授信额度冻结
                List<CmisLmt0006ApprSubListReqDto> cmisLmt0006ApprSubListReqDtos = reqDto.getApprSubList() ;
                if (CollectionUtils.isNotEmpty(cmisLmt0006ApprSubListReqDtos)){
                    for (CmisLmt0006ApprSubListReqDto apprSubListReqDto : cmisLmt0006ApprSubListReqDtos) {
                        updatePartAoopSubStatus(apprSubListReqDto.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_11);
                    }
                }
                logger.info(this.getClass().getName()+":合作方额度冻结处理end............. ");
            }else{
                logger.info(this.getClass().getName()+":非合作方额度冻结处理start............. ");
                //非合作方额度冻结
                for (CmisLmt0006ApprListReqDto apprListRrqDto:reqDto.getApprList()) {
                    if (apprListRrqDto.getOptType().equals(CmisLmtConstants.STD_ZB_APPR_INFO_02)){
                        String apprSerno = apprListRrqDto.getApprSerno() ;
                        if(StringUtils.isBlank(apprSerno)){
                            respDto.setErrorCode(EclEnum.ECL070115.key);
                            respDto.setErrorMsg(EclEnum.ECL070115.value);
                            return respDto;
                        }
                        //生效状态的单一用户分项全部冻结
                        updateAllLmtSubBasicStatus(apprSerno,inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_11);
                        //如果分项全部冻结则冻结额度台账
                        List<ApprLmtSubBasicInfo> statusList = apprLmtSubBasicInfoMapper.getStatusByFkPkid(apprSerno);
                        boolean flag = true;
                        if(CollectionUtils.isNotEmpty(statusList)){
                            for (ApprLmtSubBasicInfo apprLmtSubBasicInfo:statusList) {
                                String status = apprLmtSubBasicInfo.getStatus() ;
                                if(apprLmtSubBasicInfo==null || CmisLmtConstants.STD_ZB_APPR_ST_90.equals(status)) continue ;
                                if (!CmisLmtConstants.STD_ZB_APPR_ST_11.equals(status)){
                                    flag = false;
                                }
                            }
                        }
                        if (flag){
                            //单一用户额度冻结
                            updateStrMtableStatus(apprSerno,inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_11);
                        }

                        //如果该单一客户所属的集团客户名下所有单一客户都被冻结，则冻结该集团客户
                        judgeUpdateGrp(apprSerno,inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_11);
                    }
                }
                //单一用户分项部分冻结
                List<CmisLmt0006ApprSubListReqDto> cmisLmt0006ApprSubListReqDtos = reqDto.getApprSubList() ;
                if (CollectionUtils.isNotEmpty(cmisLmt0006ApprSubListReqDtos)){
                    for (CmisLmt0006ApprSubListReqDto subListReqDto : cmisLmt0006ApprSubListReqDtos) {
                        updatePartLmtSubBasicStatus(subListReqDto.getApprSubSerno(),inputId,inputBrId,inputDate,CmisLmtConstants.STD_ZB_APPR_ST_11);
                    }
                }
                logger.info(this.getClass().getName()+":非合作方额度冻结处理end............. ");
            }
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        }catch (YuspException e) {
            logger.error("额度冻结接口报错：", e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(this.getClass().getName()+":额度冻结接口接收数据结束............. ");
        return respDto;
    }

    //根据单一客户的客户编号判断是否要冻结该单一客户所属的集团客户，若是，则冻结
    public void judgeUpdateGrp(String appSerno,String inputId,String inputBrId,String inputDate,String status){
        String grpSerno = apprStrMtableInfoMapper.getGrpSernoByCusId(appSerno);
        if (grpSerno != null && grpSerno != ""){
            List<String> grpStatusList = apprStrMtableInfoMapper.getStatusByGrpSerno(grpSerno);
            boolean flag = true;
            for (String grpStatus:grpStatusList) {
                if (!grpStatus.equals(status)){
                    flag = false;
                    break;
                }
            }
            if (flag){
                QueryModel model = new QueryModel();
                model.addCondition("updId", inputId);
                model.addCondition("updDate", inputDate);
                model.addCondition("updBrId", inputBrId);
                model.addCondition("apprStatus", status);
                model.addCondition("grpSerno", grpSerno);
                model.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
                model.addCondition("updateTime", DateUtils.getCurrTimestamp());
                int c = apprStrMtableInfoMapper.updateByApprSerno(model);
            }
        }
    }

    //合作方额度状态修改
    public void updateAoopStatus(String apprSerno,String inputId,String inputBrId,String inputDate,String status){
        logger.info(this.getClass().getName()+":合作方台账{}更新状态---------->start ", apprSerno);
        Map params = new HashMap();
        params.put("updId", inputId);
        params.put("updBrId", inputBrId);
        params.put("updDate", inputDate);
        params.put("apprStatus", status);
        params.put("apprSerno", apprSerno);
        params.put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        params.put("updateTime", DateUtils.getCurrTimestamp());
        int i = apprCoopInfoMapper.updateApprCoopInfoBySerNo(params);
        logger.info(this.getClass().getName()+":合作方台账{}更新状态，更新结果{}---------->end ", apprSerno, i);
    }

    //合作方部分分项额度状态修改
    public void updatePartAoopSubStatus(String apprSubSerno,String inputId,String inputBrId,String inputDate,String status){
        logger.info(this.getClass().getName()+":合作方更新分项{}更新分项状态---------->start ", apprSubSerno);
        Map params = new HashMap();
        params.put("updId", inputId);
        params.put("updBrId", inputBrId);
        params.put("updDate", inputDate);
        params.put("status", status);
        params.put("apprSubSerno", apprSubSerno);
        params.put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        params.put("updateTime", DateUtils.getCurrTimestamp());
        int k = apprCoopSubInfoMapper.updateByApprSubSerno(params);
        logger.info(this.getClass().getName()+":合作方更新分项{}更新分项状态，更新结果{}---------->end ", apprSubSerno, k);
    }

    //合作方全部分项额度状态修改
    public void updateAllAoopSubStatus(String apprSerno,String inputId,String inputBrId,String inputDate,String status){
        logger.info(this.getClass().getName()+":合作方根据批复台账编号{}更新分项状态---------->start ", apprSerno);
        Map params = new HashMap();
        params.put("updId", inputId);
        params.put("updBrId", inputBrId);
        params.put("updDate", inputDate);
        params.put("status", status);
        params.put("apprSerno", apprSerno);
        params.put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        params.put("updateTime", DateUtils.getCurrTimestamp());
        //返回值为1则执行成功，0则执行失败
        int k = apprCoopSubInfoMapper.updateByApprSerno(params);
        logger.info(this.getClass().getName()+":合作方根据批复台账编号{}更新分项状态，更新结果{}---------->end ", apprSerno, k);
    }

    //单一客户额度状态修改
    public void updateStrMtableStatus(String appSerno,String inputId,String inputBrId,String inputDate,String status){
        logger.info(this.getClass().getName()+":更新根据批复台账{}状态---------->start ", appSerno);
        QueryModel model = new QueryModel();
        model.addCondition("updId", inputId);
        model.addCondition("updBrId", inputBrId);
        model.addCondition("updDate", inputDate);
        model.addCondition("apprStatus", status);
        model.addCondition("apprSerno", appSerno);
        model.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        model.addCondition("updateTime", DateUtils.getCurrTimestamp());
        int i = apprStrMtableInfoMapper.updateByApprSerno(model);
        logger.info(this.getClass().getName()+":更新根据批复台账{}状态，更新结果{}---------->end ", appSerno, i);
    }

    //单一客户全部分项状态修改
    public void updateAllLmtSubBasicStatus(String apprSerno,String inputId,String inputBrId,String inputDate,String status){
        logger.info(this.getClass().getName()+":更新根据批复台账{}更新分项信息---------->start ", apprSerno);
        Map params = new HashMap();
        params.put("updId", inputId);
        params.put("updBrId", inputBrId);
        params.put("updDate", inputDate);
        params.put("status", status);
        params.put("fkPkid", apprSerno);
        params.put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        params.put("updateTime", DateUtils.getCurrTimestamp());
        //返回值为1则执行成功，0则执行失败
        int k = apprLmtSubBasicInfoMapper.updateByFkPkid(params);
        logger.info(this.getClass().getName()+":更新根据批复台账{}更新分项信息，更新结果{}---------->end ", apprSerno, k);
    }

    //单一客户部分分项状态修改
    public void updatePartLmtSubBasicStatus(String apprSubSerno,String inputId,String inputBrId,String inputDate,String status){
        logger.info(this.getClass().getName()+":更新分项信息{}---------->开始 ", apprSubSerno);
        Map params = new HashMap();
        params.put("updId", inputId);
        params.put("updBrId", inputBrId);
        params.put("updDate", inputDate);
        params.put("status", status);
        params.put("apprSubSerno", apprSubSerno);
        params.put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        params.put("updateTime", DateUtils.getCurrTimestamp());
        int k = apprLmtSubBasicInfoMapper.updateBySerno(params);
        logger.info(this.getClass().getName()+":更新分项信息{}---------->W结束，更新结果{} ", apprSubSerno, k);

        //更新处理分项明细信息
        ApprLmtSubBasicInfo apprLmtSubBasicInfo =  apprLmtSubBasicInfoMapper.selectByApprSubSerno(apprSubSerno) ;
        if(apprLmtSubBasicInfo !=null){
            String parentId = apprLmtSubBasicInfo.getParentId() ;
            String cusId = apprLmtSubBasicInfo.getCusId() ;
            if(StringUtils.nonBlank(parentId)){
                //冻结额度分项，做以下操作
                if(CmisLmtConstants.STD_ZB_APPR_ST_11.equals(status)){
                    Boolean flag = true ;
                    //判断父节点向下是否全部冻结，如果是，则更新该分项为冻结状态
                    if(StringUtils.nonBlank(parentId)){
                        List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = apprLmtSubBasicInfoService.selectSbuListByParentId(cusId, parentId) ;
                        if(CollectionUtils.isNotEmpty(apprLmtSubBasicInfoList)){
                            for (ApprLmtSubBasicInfo subBasicInfo : apprLmtSubBasicInfoList) {
                                String subStatus = subBasicInfo.getStatus() ;
                                if(subBasicInfo == null || CmisLmtConstants.STD_ZB_APPR_ST_90.equals(subStatus)) continue ;
                                if (!CmisLmtConstants.STD_ZB_APPR_ST_11.equals(subStatus)){
                                    flag = false;
                                }
                            }
                        }
                    }
                    if(flag){
                        updatePartLmtSubBasicStatus(parentId, inputId, inputBrId, inputDate, status) ;
                    }
                }else{
                    //解冻额度分项，若上级额度分项是冻结，则无条件更新上级额度分项状态为生效
                    Map<String,String> updateFMap = new HashMap<>();
                    updateFMap.put("status",CmisLmtConstants.STD_ZB_APPR_ST_01);
                    updateFMap.put("origiStatus",CmisLmtConstants.STD_ZB_APPR_ST_11);
                    updateFMap.put("oprType", CmisBizConstants.OPR_TYPE_01);
                    updateFMap.put("updId", inputId);
                    updateFMap.put("updBrId", inputBrId);
                    updateFMap.put("upddate", inputDate);
                    updateFMap.put("apprSubSerno",parentId);
                    int l = apprLmtSubBasicInfoMapper.updateApprSubStatus(updateFMap);
                    logger.info(this.getClass().getName()+":更新父级分项信息{}---------->W结束，更新结果{} ", apprSubSerno, l);
                }

            }
        }
    }
}
