/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ApprCoopInfo;
import cn.com.yusys.yusp.repository.mapper.ApprCoopInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprCoopSubInfoMapper;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-04-20 22:36:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ApprCoopInfoService {

    @Autowired
    private ApprCoopInfoMapper apprCoopInfoMapper;

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService;

    @Autowired
    private ApprCoopSubInfoMapper apprCoopSubInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ApprCoopInfo selectByPrimaryKey(String pkId) {
        return apprCoopInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ApprCoopInfo> selectAll(QueryModel model) {
        List<ApprCoopInfo> records = (List<ApprCoopInfo>) apprCoopInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ApprCoopInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ApprCoopInfo> list = apprCoopInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ApprCoopInfo record) {
        return apprCoopInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ApprCoopInfo record) {
        return apprCoopInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ApprCoopInfo record) {
        return apprCoopInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ApprCoopInfo record) {
        return apprCoopInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return apprCoopInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return apprCoopInfoMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据Serno查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprCoopInfo selectBySerno(String serno) {
        return apprCoopInfoMapper.selectBySerno(serno);
    }

    /**
     * @param queryModel
     * @return
     * @date 2021/4/22 15:09
     * @version 1.0.0
     * @desc    根据客户号和状态查询合作方案信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<Map<String,Object>> selectByCusIdAndStatus(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String,Object>> apprCoopInfoList = apprCoopInfoMapper.selectByCusIdAndStatus(queryModel);
        PageHelper.clearPage();
        return apprCoopInfoList;
    }
    /**
     * @param queryModel
     * @desc 合作方额度视图列表（支持查并表额度）：
     *      instuCde金融机构号;notInstuCde不为金融机构号；cusId客户号；cusName客户名称；copType合作方类型
     * @date 2021/5/6
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    public List<Map<String,Object>> queryListByInstuCde(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String,Object>> apprCoopInfoList = apprCoopInfoMapper.queryListByInstuCde(queryModel);
        PageHelper.clearPage();
        return apprCoopInfoList;
    }

    /**
     * @param queryModel
     * @return
     * @date 2021/5/15 15:09
     * @version 1.0.0
     * @desc    根据客户号和状态查询合作方案信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<Map<String,Object>> queryApprCoopBySerno(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String,Object>> apprCoopInfoList = apprCoopInfoMapper.queryApprCoopBySerno(queryModel);
        PageHelper.clearPage();
        return apprCoopInfoList;
    }

    /**
     * @方法名称: selectACIBySerno
     * @方法描述: 根据批复编号获取合作方批复信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprCoopInfo selectACIBySerno(String serno) {
        QueryModel queryModel = new QueryModel() ;
        //批复编号
        queryModel.addCondition("apprSerno", serno);
        //操作类型 01- 新增
        queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //查询批复信息
        List<ApprCoopInfo> apprCoopInfoList = apprCoopInfoMapper.selectByModel(queryModel) ;
        //如果不为空，返回批复信息
        if(CollectionUtils.isNotEmpty(apprCoopInfoList)){
            return apprCoopInfoList.get(0) ;
        }
        return null ;
    }

    /**
     * @方法名称: getSubOutTotal
     * @方法描述: 根据根据合作方批复编号，获取合作方向下分项已用金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map<String, BigDecimal> getSubOutTotal(String apprSerno) {
        //根据父级编号，查询分项信息
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("apprSerno", apprSerno);
        Map<String, BigDecimal> apprlmt = apprCoopInfoMapper.getSubOutTotal(queryModel);
        return apprlmt ;
    }


    /**
     * @方法名称: valiApprIsExistsBySerno
     * @方法描述: 判断是否存在批复信息
     * @参数与返回说明: 传入批复编号，存在返回true 不存在返回false
     * @算法描述: 无
     */

    public boolean valiApprIsExistsBySerno(String serno) {
        QueryModel queryModel = new QueryModel() ;
        //批复编号
        queryModel.addCondition("apprSerno", serno);
        //查询批复信息
        List<ApprCoopInfo> apprCoopInfoList =  apprCoopInfoMapper.selectByModel(queryModel) ;
        //批复是否为空
        if(CollectionUtils.isNotEmpty(apprCoopInfoList)){
            return true ;
        }else{
            return false ;
        }
    }

    /**
     * @方法名称: updateApprCoopInfoByCusIdAndSerNo
     * @方法描述: 更新合作方状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateApprCoopInfoByCusIdAndSerNo(Map params) {
        return apprCoopInfoMapper.updateApprCoopInfoBySerNo(params);
    }

    /**
     * @param queryModel
     * @desc 根据合作方客户号查找客户合作方额度项下关联合同列表
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    public List<Map<String,Object>> selectCoopLmtContRelByCusId(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String,Object>> apprCoopInfoList = apprCoopInfoMapper.selectCoopLmtContRelByCusId(queryModel);
        PageHelper.clearPage();
        return apprCoopInfoList;
    }

    /**
     * @param queryModel
     * @desc 根据合作方客户号查找客户合作方额度项下关联合同列表
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    public Map<String,Object> selectTotalCoopLmtContRelByCusId(QueryModel queryModel) {
        Map<String,Object> resultMap = new HashMap<String, Object>() ;
        //汇总信息
        List<Map<String,Object>> totalSumList = apprCoopInfoMapper.selectTotalCoopLmtContRelByCusId(queryModel);
        Map<String,Object> map = new HashMap<>();
        if(totalSumList!=null && totalSumList.size()>0){
            map = totalSumList.get(0);
            resultMap.putAll(map);
        }else{
            map.put("totalContAmt",0);
            map.put("totalSpacAmt",0);
            map.put("totalSpacBalanceAmt",0);
        }
        return resultMap;
    }
    /**
     * @param queryModel
     * @desc 根据合作方额度分项编号查找客户合作方额度项下关联合同列表(未结清)
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    public List<Map<String,Object>> selectCoopLmtContRelByApprSubSerno(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String,Object>> apprCoopInfoList = apprCoopInfoMapper.selectCoopLmtContRelByApprSubSerno(queryModel);
        PageHelper.clearPage();
        return apprCoopInfoList;
    }

    /**
     * @param queryModel
     * @desc 根据合作方额度分项编号查找客户合作方额度项下关联合同列表(未结清)
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    public Map<String,Object> selectTotalCoopLmtContRelByApprSubSerno(QueryModel queryModel) {
        Map<String,Object> resultMap = new HashMap<String, Object>() ;

        //汇总信息
        List<Map<String,Object>> totalSumList = apprCoopInfoMapper.selectTotalCoopLmtContRelByApprSubSerno(queryModel);
        Map<String,Object> map = new HashMap<>();
        if(totalSumList!=null && totalSumList.size()>0){
            map = totalSumList.get(0);
            resultMap.putAll(map);
        }else{
            map.put("totalContAmt",0);
            map.put("totalSpacAmt",0);
            map.put("totalSpacBalanceAmt",0);
        }
        return resultMap;
    }

    /**
     * @param queryModel
     * @desc 根据合作方客户号查找客户合作方额度项下关联合同下关联的台账列表
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    public List<Map<String,Object>> selectCoopAccContRelByCusId(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String,Object>> apprCoopInfoList = apprCoopInfoMapper.selectCoopAccContRelByCusId(queryModel);
        PageHelper.clearPage();
        return apprCoopInfoList;
    }

    /**
     * @param queryModel
     * @desc 根据合作方客户号查找客户合作方额度项下关联合同下关联的台账汇总
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    public Map<String,Object> selectTotalCoopAccContRelByCusId(QueryModel queryModel) {
        Map<String,Object> resultMap = new HashMap<String, Object>() ;
        //汇总信息
        List<Map<String,Object>> totalSumList = apprCoopInfoMapper.selectTotalCoopAccContRelByCusId(queryModel);
        Map<String,Object> map = new HashMap<>();
        if(totalSumList!=null && totalSumList.size()>0){
            map = totalSumList.get(0);
            resultMap.putAll(map);
        }else{
            map.put("totalContAmt",0);
            map.put("totalSpacAmt",0);
            map.put("totalSpacBalanceAmt",0);
        }
        return resultMap;
    }

    /**
     * @param queryModel
     * @desc 根据合作方额度分项编号查找客户合作方额度项下关联合同列表(未结清)
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    public List<Map<String,Object>> selectCoopAccContRelByApprSubSerno(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String,Object>> apprCoopInfoList = apprCoopInfoMapper.selectCoopAccContRelByApprSubSerno(queryModel);
        PageHelper.clearPage();
        return apprCoopInfoList;
    }

    /**
     * @param queryModel
     * @desc 根据合作方额度分项编号查找客户合作方额度项下关联合同列表(未结清)
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    public Map<String,Object> selectTotalCoopAccContRelByApprSubSerno(QueryModel queryModel) {
        Map<String,Object> resultMap = new HashMap<String, Object>() ;
        //汇总信息
        List<Map<String,Object>> totalSumList = apprCoopInfoMapper.selectTotalCoopAccContRelByApprSubSerno(queryModel);
        Map<String,Object> map = new HashMap<>();
        if(totalSumList!=null && totalSumList.size()>0){
            map = totalSumList.get(0);
            resultMap.putAll(map);
        }else{
            map.put("totalContAmt",0);
            map.put("totalSpacAmt",0);
            map.put("totalSpacBalanceAmt",0);
        }
        return resultMap;
    }

    /**
     * @param queryModel
     * @desc 合作方额度列表，以批复为维度（冻结解冻使用）：
     *      instuCde金融机构号;notInstuCde不为金融机构号；cusId客户号；cusName客户名称；copType合作方类型
     * @date 2021/9/29
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    public List<Map<String,Object>> queryListByInstuCdeForDj(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String,Object>> apprCoopInfoList = apprCoopInfoMapper.queryListByInstuCdeForDj(queryModel);
        PageHelper.clearPage();
        return apprCoopInfoList;
    }

}
