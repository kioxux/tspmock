package cn.com.yusys.yusp.service.server.cmislmt0002;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.req.CmisLmt0002LmtListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.req.CmisLmt0002LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.req.CmisLmt0002ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0002.resp.CmisLmt0002RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import cn.com.yusys.yusp.service.server.cmislmt0001.CmisLmt0001Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0002Service
 * @类描述: #对内服务类
 * @功能描述: 集团额度同步
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0002Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0002Service.class);

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;
//
//    @Autowired
//    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper ;

    @Autowired
    private CmisLmt0001Service cmisLmt0001Service;

    public CmisLmt0002RespDto execute(CmisLmt0002ReqDto reqDto) throws YuspException,Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0002.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0002.value);

        CmisLmt0002RespDto respDto = new CmisLmt0002RespDto();
        //批复编号
        String accNo = reqDto.getGrpAccNo() ;
        //判断是否生成新批复编号
        String isCreateAcc = reqDto.getIsCreateAcc() ;

        logger.info(this.getClass().getName()+":集团额度同步接口，批复编号【"+accNo+"】............. ");
        //初始化批复主表对象信息
        ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfoService.getAppStrMTableInfoBySerno(accNo) ;
        boolean chgFlag = false ;
        //判断是否变更操作
        if(apprStrMtableInfo != null){
            chgFlag = true ;
        }else{
            apprStrMtableInfo = new ApprStrMtableInfo() ;
        }
        //生成主键
        Map paramMap= new HashMap<>() ;
        String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
        //拷贝数据
        BeanUtils.copyProperties(reqDto, apprStrMtableInfo);
        //集团客户编号
        apprStrMtableInfo.setCusId(reqDto.getGrpNo());
        //集团客户名称
        apprStrMtableInfo.setCusName(reqDto.getGrpName());
        //客户类型 默认 4--集团客户
        apprStrMtableInfo.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG4);
        //授信模式 默认 01--综合授信
        apprStrMtableInfo.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_01);
        //批复编号转换
        apprStrMtableInfo.setApprSerno(reqDto.getGrpAccNo());
        //操作类型 默认 01- 新增
        apprStrMtableInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //额度类型 02-集团客户授信
        apprStrMtableInfo.setLmtType(CmisLmtConstants.LMT_TYPE_02);
        //最近更新时间
        apprStrMtableInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //最近更新处理
        apprStrMtableInfo.setUpdId(reqDto.getInputId());
        apprStrMtableInfo.setUpdBrId(reqDto.getInputBrId());
        apprStrMtableInfo.setUpdDate(reqDto.getInputDate());

        /**
         * 如果是 以新批复覆盖老批复：如果是则原批复编号必须存在，批复状态跟更改为99 失效已结清，如果存在未结清业务，则原业务挂到新批复下
         * 如果是 否 ：根据批复台账判断该批复是否已经存在，如果存在，进行变更操作，如果不存在则新增批复
         */

        if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
            logger.info("CmisLmt0002 批复台账编号【"+accNo+"】，是新批复覆盖老批复");
            //原批复编号
            String origiAccNo = reqDto.getOrigiGrpAccNo() ;
            //原批复信息
            ApprStrMtableInfo oldApprStrMtableInfo = apprStrMtableInfoService.selectByAppSerno(origiAccNo) ;
            if(oldApprStrMtableInfo == null){
                respDto.setErrorCode(EclEnum.ECL070072.key);
                respDto.setErrorMsg(EclEnum.ECL070072.value);
                return respDto ;
            }
            //原批复状态更改为失效已结清
            oldApprStrMtableInfo.setApprStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
            logger.info("CmisLmt0002 批复台账编号【"+accNo+"】，将原批复台账编号【"+origiAccNo+"】对应的批复状态改为99--失效已结清");
            apprStrMtableInfoMapper.updateByPrimaryKey(oldApprStrMtableInfo) ;
        }

        if(chgFlag){
            logger.info("集团变更变更后集团额度信息：", JSON.toJSONString(apprStrMtableInfo));
            //存在，即变更
            apprStrMtableInfoMapper.updateByPrimaryKey(apprStrMtableInfo) ;
        }else{
            //添加主键
            apprStrMtableInfo.setPkId(pkValue);
            //创建时间
            apprStrMtableInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            //登记日期
            apprStrMtableInfo.setInputDate(reqDto.getInputDate());
            //批复状态 默认01--生效
            apprStrMtableInfo.setApprStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            //不存在，即新增 插入批复主表信息
            logger.info("CmisLmt0002 根据批复台账编号【"+accNo+"】，查询不到记录，新增批复台账信息【{}】", JSON.toJSONString(apprStrMtableInfo));
            apprStrMtableInfoMapper.insert(apprStrMtableInfo) ;
        }

        logger.info("将集团中的成员信息转换为单一客户信息，以方便调用单一客户的方法");

        List<String> apprSernos = new ArrayList<>();

        List<CmisLmt0002LmtSubListReqDto> lmtSubList = reqDto.getLmSubList();

        //key:授信分项编号accSubNo value:父节点是key的授信分项列表List<CmisLmt0002LmtSubListReqDto>
        Map<String,List<CmisLmt0002LmtSubListReqDto>> accNoMap = new HashMap<>();

        for (CmisLmt0002LmtSubListReqDto cmisLmt0002LmtSubListReqDto : lmtSubList) {
            //授信分项编号
            String parentId = cmisLmt0002LmtSubListReqDto.getParentId();

            if (accNoMap.containsKey(parentId)){
                accNoMap.get(parentId).add(cmisLmt0002LmtSubListReqDto);
            }else{
                List<CmisLmt0002LmtSubListReqDto> lmtSubListReqDtos = new ArrayList<>();
                lmtSubListReqDtos.add(cmisLmt0002LmtSubListReqDto);
                accNoMap.put(cmisLmt0002LmtSubListReqDto.getAccSubNo(),lmtSubListReqDtos);
            }
        }

        for (CmisLmt0002LmtListReqDto lmt0002LmtListReqDto : reqDto.getLmtList()) {
            apprSernos.add(lmt0002LmtListReqDto.getAccNo());
            CmisLmt0001ReqDto cmisLmt0001ReqDto = new CmisLmt0001ReqDto();
            //拷贝数据
            BeanUtils.copyProperties(lmt0002LmtListReqDto, cmisLmt0001ReqDto);
            cmisLmt0001ReqDto.setSysId(reqDto.getSysId());
            cmisLmt0001ReqDto.setInstuCde(reqDto.getInstuCde());
            ArrayList<CmisLmt0001LmtSubListReqDto> lmt0001SubList = new ArrayList<>();

            for (CmisLmt0002LmtSubListReqDto lmt0002LmtSubListReqDto : reqDto.getLmSubList()) {
                if (lmt0002LmtSubListReqDto.getParentId().equals(lmt0002LmtListReqDto.getAccNo())){
                    //从accNoMap里获取该授信分项编号对应的授信分项列表
                    List<CmisLmt0002LmtSubListReqDto> lmtSubListReqDtos = accNoMap.get(lmt0002LmtSubListReqDto.getAccSubNo());

                    for (CmisLmt0002LmtSubListReqDto lmtSubListReqDto : lmtSubListReqDtos) {
                        CmisLmt0001LmtSubListReqDto lmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
                        BeanUtils.copyProperties(lmtSubListReqDto, lmt0001LmtSubListReqDto);
                        String lmtGraper= lmtSubListReqDto.getLmtGraper();
                        if(StringUtils.isEmpty(lmtGraper)){
                            lmtGraper = "0" ;
                        }
                        lmt0001LmtSubListReqDto.setLmtGraper(Integer.valueOf(lmtGraper));
                        lmt0001SubList.add(lmt0001LmtSubListReqDto);
                    }
                }
            }
            cmisLmt0001ReqDto.setLmtSubList(lmt0001SubList);
            logger.info("CmisLmt0002 调用 CmisLmt0001 接口，接口报文【{}】", JSON.toJSONString(cmisLmt0001ReqDto));
            CmisLmt0001RespDto cmisLmt0001RespDto = cmisLmt0001Service.execute(cmisLmt0001ReqDto);

            if (!SuccessEnum.SUCCESS.key.equals(cmisLmt0001RespDto.getErrorCode())){
                logger.info("CmisLmt0002 批复台账编号【"+accNo+"】，集团子成员【"+cmisLmt0001ReqDto.getCusId()+"】调cmislmt0001接口报错，报错信息:"+cmisLmt0001RespDto.getErrorMsg());
                throw new YuspException(EclEnum.ECL070105.key, EclEnum.ECL070105.value);
            }
        }

        logger.info("CmisLmt0002 批复台账编号【"+accNo+"】，将集团客户的集团子成员的批复台账信息里的集团批复台账编号更新为该集团客户的批复台账编号");
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("grpApprSerno",reqDto.getGrpAccNo());
        queryModel.addCondition("apprSernos",apprSernos);
        apprStrMtableInfoService.updateGrpApprSernoByApprSernos(queryModel);

        respDto.setErrorCode(SuccessEnum.SUCCESS.key);
        respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0002.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0002.value);
        return respDto;
    }
}