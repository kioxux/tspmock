package cn.com.yusys.yusp.web.server.cmislmt0047;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0047.CmisLmt0047Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据分项编号，查询分项向下未结清的业务
 *
 * @author zhangjw 2021/7/14
 * @version 1.0
 */
@Api(tags = "cmislmt0047:根据分项编号，查询分项向下未结清的业务")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0047Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0047Resource.class);

    @Autowired
    private CmisLmt0047Service cmisLmt0047Service;
    /**
     * 交易码：cmislmt0047
     * 交易描述：根据分项编号，查询分项向下未结清的业务
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("根据分项编号，查询分项向下未结清的业务")
    @PostMapping("/cmislmt0047")
    protected @ResponseBody
    ResultDto<CmisLmt0047RespDto> cmisLmt0047(@Validated @RequestBody CmisLmt0047ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0047.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0047.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0047RespDto> cmisLmt0047RespDtoResultDto = new ResultDto<>();
        CmisLmt0047RespDto cmisLmt0047RespDto = new CmisLmt0047RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0047RespDto = cmisLmt0047Service.execute(reqDto);
            cmisLmt0047RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0047RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);

        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0047.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0047.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0047.value, e.getMessage());
            // 封装xddb0047DataResultDto中异常返回码和返回信息
            cmisLmt0047RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0047RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0016RespDto到cmisLmt0016RespDtoResultDto中
        cmisLmt0047RespDtoResultDto.setData(cmisLmt0047RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0047.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0047.value, JSON.toJSONString(cmisLmt0047RespDtoResultDto));
        return cmisLmt0047RespDtoResultDto;
    }
}
