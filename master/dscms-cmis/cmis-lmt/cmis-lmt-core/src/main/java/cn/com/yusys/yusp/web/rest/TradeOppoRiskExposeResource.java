/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.LmtDiscOrgTempExportVo;
import cn.com.yusys.yusp.vo.TradeOppoRiskExposeTempExportVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.TradeOppoRiskExpose;
import cn.com.yusys.yusp.service.TradeOppoRiskExposeService;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TradeOppoRiskExposeResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 15:37:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/tradeopporiskexpose")
public class TradeOppoRiskExposeResource {
    private static final Logger log = LoggerFactory.getLogger(TradeOppoRiskExposeResource.class);
    @Autowired
    private TradeOppoRiskExposeService tradeOppoRiskExposeService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<TradeOppoRiskExpose>> query() {
        QueryModel queryModel = new QueryModel();
        List<TradeOppoRiskExpose> list = tradeOppoRiskExposeService.selectAll(queryModel);
        return new ResultDto<List<TradeOppoRiskExpose>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<TradeOppoRiskExpose>> index(QueryModel queryModel) {
        List<TradeOppoRiskExpose> list = tradeOppoRiskExposeService.selectByModel(queryModel);
        return new ResultDto<List<TradeOppoRiskExpose>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<TradeOppoRiskExpose>> selectByModel(@RequestBody QueryModel queryModel) {
        List<TradeOppoRiskExpose> list = tradeOppoRiskExposeService.selectByModel(queryModel);
        return new ResultDto<List<TradeOppoRiskExpose>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<TradeOppoRiskExpose> create(@RequestBody TradeOppoRiskExpose tradeOppoRiskExpose) throws URISyntaxException {
        tradeOppoRiskExposeService.insert(tradeOppoRiskExpose);
        return new ResultDto<TradeOppoRiskExpose>(tradeOppoRiskExpose);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody TradeOppoRiskExpose tradeOppoRiskExpose) throws URISyntaxException {
        int result = tradeOppoRiskExposeService.update(tradeOppoRiskExpose);
        return new ResultDto<Integer>(result);
    }

    /**
     * 异步下载交易对手风险暴露模板
     */
    @PostMapping("/exporttradeopporiskexposetemp")
    public ResultDto<ProgressDto> asyncExportTradeOppoRiskExposeTemp() {
        ProgressDto progressDto = tradeOppoRiskExposeService.asyncExportTradeOppoRiskExposeTemp();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importtradeopporiskexpose")
    public ResultDto<ProgressDto> asyncImportTradeopporiskexpose(@RequestParam("fileId") String fileId) {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);

            log.info("开始执行异步导入，导入fileId为[{}];", fileId);

            //异步改为同步
            ExcelUtils.syncImport(TradeOppoRiskExposeTempExportVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return tradeOppoRiskExposeService.importTradeopporiskexpose(dataList);
            }), true);

        } catch (IOException e) {
            log.error(EclEnum.ECL070094.value,e);
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        }
        return ResultDto.success().message("导入成功！");
    }

    /**
     * 异步下载交易对手风险暴露数据
     */
    @PostMapping("/exporttradeopporiskexpose")
    public ResultDto<ProgressDto> asyncExportTradeopporiskexpose(@RequestBody QueryModel model) {
        ProgressDto progressDto = tradeOppoRiskExposeService.asyncExportTradeopporiskexpose(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String cusId, String prdId) {
        int result = tradeOppoRiskExposeService.deleteByPrimaryKey(cusId, prdId);
        return new ResultDto<Integer>(result);
    }

}
