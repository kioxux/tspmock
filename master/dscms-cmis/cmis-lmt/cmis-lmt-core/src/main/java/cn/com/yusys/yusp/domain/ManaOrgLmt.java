/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ManaOrgLmt
 * @类描述: mana_org_lmt数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 15:32:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "mana_org_lmt")
public class ManaOrgLmt extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 机构编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ORG_ID")
	private String orgId;
	
	/** 上月末对公贷款余额 **/
	@Column(name = "LAST_MONTH_COM_LOAN_BALANCE", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal lastMonthComLoanBalance;
	
	/** 当月可净新增对公贷款投放金额 **/
	@Column(name = "CURR_MONTH_ALLOW_COM_ADD_AMT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal currMonthAllowComAddAmt;
	
	/** 上一日对公贷款余额 **/
	@Column(name = "LAST_DAY_COM_LOAN_BALANCE", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal lastDayComLoanBalance;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param orgId
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
    /**
     * @return orgId
     */
	public String getOrgId() {
		return this.orgId;
	}
	
	/**
	 * @param lastMonthComLoanBalance
	 */
	public void setLastMonthComLoanBalance(java.math.BigDecimal lastMonthComLoanBalance) {
		this.lastMonthComLoanBalance = lastMonthComLoanBalance;
	}
	
    /**
     * @return lastMonthComLoanBalance
     */
	public java.math.BigDecimal getLastMonthComLoanBalance() {
		return this.lastMonthComLoanBalance;
	}
	
	/**
	 * @param currMonthAllowComAddAmt
	 */
	public void setCurrMonthAllowComAddAmt(java.math.BigDecimal currMonthAllowComAddAmt) {
		this.currMonthAllowComAddAmt = currMonthAllowComAddAmt;
	}
	
    /**
     * @return currMonthAllowComAddAmt
     */
	public java.math.BigDecimal getCurrMonthAllowComAddAmt() {
		return this.currMonthAllowComAddAmt;
	}
	
	/**
	 * @param lastDayComLoanBalance
	 */
	public void setLastDayComLoanBalance(java.math.BigDecimal lastDayComLoanBalance) {
		this.lastDayComLoanBalance = lastDayComLoanBalance;
	}
	
    /**
     * @return lastDayComLoanBalance
     */
	public java.math.BigDecimal getLastDayComLoanBalance() {
		return this.lastDayComLoanBalance;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}