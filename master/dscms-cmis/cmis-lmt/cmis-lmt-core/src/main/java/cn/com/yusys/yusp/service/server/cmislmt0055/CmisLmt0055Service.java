package cn.com.yusys.yusp.service.server.cmislmt0055;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0055.req.CmisLmt0055ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0055.resp.CmisLmt0055RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0055.resp.LmtDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0055Service
 * @类描述: #对内服务类
 * @功能描述: 获取客户有效的特定目的载体投资额度列表
 * @创建时间: 2021-09-09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0055Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0055Service.class);

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService ;
    /**
     * 获取客户有效的特定目的载体投资额度列表
     * add by zhangjw 2021-09-09
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0055RespDto execute(CmisLmt0055ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0055.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0055.value);
        CmisLmt0055RespDto respDto = new CmisLmt0055RespDto();

        try {
            //客户号
            String cusId = reqDto.getCusId() ;
            List<LmtDto> lmtDtoList = new ArrayList<>();

            QueryModel queryModel = new QueryModel() ;
            queryModel.addCondition("cusId", cusId);
            queryModel.addCondition("limitSubNo", CmisLmtConstants.LARGE_LIMIT_TYPE_TDMDZTTZ);
            if(!StringUtils.isBlank(cusId)){
                List<Map<String,Object>> list = apprStrMtableInfoService.selectApprSernoByCusIdAndLimitSubNo(queryModel) ;
                if(list!=null && list.size()>0){
                    for(Map apprSernoMap : list){
                        LmtDto lmtDto = new LmtDto();
                        lmtDto.setApprSerno((String) apprSernoMap.get("apprSerno"));
                    }
                }
            }

            respDto.setLmtDtoList(lmtDtoList);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0055】获取客户有效的特定目的载体投资额度列表：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0055.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0055.value);
        return respDto;
    }
}