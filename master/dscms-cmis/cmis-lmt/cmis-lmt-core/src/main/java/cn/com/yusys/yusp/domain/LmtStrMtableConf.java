/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtStrMtableConf
 * @类描述: lmt_str_mtable_conf数据实体类
 * @功能描述: 
 * @创建人: jiangyl
 * @创建时间: 2021-05-11 10:36:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因:字段变动
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_str_mtable_conf")
public class LmtStrMtableConf extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 额度配置流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "LMT_CFG_SERNO")
	private String lmtCfgSerno;
	
	/** 额度结构编号 **/
	@Column(name = "LIMIT_STR_NO", unique = false, nullable = true, length = 20)
	private String limitStrNo;
	
	/** 额度结构名称 **/
	@Column(name = "LIMIT_STR_NAME", unique = false, nullable = true, length = 80)
	private String limitStrName;
	
	/** 是否启用STD_ZB_YES_NO **/
	@Column(name = "IS_BEGIN", unique = false, nullable = true, length = 5)
	private String isBegin;
	
	/** 结构主体类型STD_ZB_MBODY_TYPE **/
	@Column(name = "STR_MBODY_TYPE", unique = false, nullable = true, length = 5)
	private String strMbodyType;
	
	/** 操作类型STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param lmtCfgSerno
	 */
	public void setLmtCfgSerno(String lmtCfgSerno) {
		this.lmtCfgSerno = lmtCfgSerno;
	}
	
    /**
     * @return lmtCfgSerno
     */
	public String getLmtCfgSerno() {
		return this.lmtCfgSerno;
	}
	
	/**
	 * @param limitStrNo
	 */
	public void setLimitStrNo(String limitStrNo) {
		this.limitStrNo = limitStrNo;
	}
	
    /**
     * @return limitStrNo
     */
	public String getLimitStrNo() {
		return this.limitStrNo;
	}
	
	/**
	 * @param limitStrName
	 */
	public void setLimitStrName(String limitStrName) {
		this.limitStrName = limitStrName;
	}
	
    /**
     * @return limitStrName
     */
	public String getLimitStrName() {
		return this.limitStrName;
	}
	
	/**
	 * @param isBegin
	 */
	public void setIsBegin(String isBegin) {
		this.isBegin = isBegin;
	}
	
    /**
     * @return isBegin
     */
	public String getIsBegin() {
		return this.isBegin;
	}
	
	/**
	 * @param strMbodyType
	 */
	public void setStrMbodyType(String strMbodyType) {
		this.strMbodyType = strMbodyType;
	}
	
    /**
     * @return strMbodyType
     */
	public String getStrMbodyType() {
		return this.strMbodyType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}