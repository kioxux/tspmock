package cn.com.yusys.yusp.repository.provider;

import cn.com.yusys.yusp.commons.mapper.exception.YuMapperException;
import com.google.common.base.CaseFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

/**
 * @author reny
 * create at 2021/3/30 13:59
 **/
public class GenericSqlProvider {
    private static final Logger log = LoggerFactory.getLogger(GenericSqlProvider.class);
    /**
     * 根据需要插入实体数据
     * @param t 实体类
     * @return
     */
    public <T> String insertSelective(T t) {
        StringBuilder sqlStr = new StringBuilder("insert into ")
                .append(getDomainTableName(t))
                .append("(");
        StringBuilder fieldStr = new StringBuilder();
        StringBuilder valStr = new StringBuilder();
        Field[] declaredFields = t.getClass().getDeclaredFields();
        try {
            for (Field field : declaredFields) {
                Column column = field.getAnnotation(Column.class);
                if (column != null) {
                    field.setAccessible(true);
                    Object o = field.get(t);
                    if (o != null) {
                        fieldStr.append(column.name()).append(",");
                        valStr.append("#{").append(field.getName()).append("},");
                    }
                }
            }
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(),e);
        }
        if (fieldStr.length() > 0) {
            fieldStr.deleteCharAt(fieldStr.length() - 1);
            valStr.deleteCharAt(valStr.length() - 1);
        } else {
            throw new YuMapperException("没有可插入的数据！");
        }
        return sqlStr.append(fieldStr).append(") values(").append(valStr).append(")").toString();
    }

    public <T> String updateByPrimaryKeySelective(T t) {
        StringBuilder sqlStr = new StringBuilder("update ")
                .append(getDomainTableName(t))
                .append(" set ");
        StringBuilder updateStr = new StringBuilder();
        Field[] declaredFields = t.getClass().getDeclaredFields();
        String pk = null,pkFieldName = null;
        try {
            for (Field field : declaredFields) {
                field.setAccessible(true);
                Object o = field.get(t);
                Id id = field.getAnnotation(Id.class);
                String fieldDbName = getFieldDbName(field);
                if (id != null) {
                    if (o != null) {
                        pk = fieldDbName;
                        pkFieldName = field.getName();
                    } else {
                        throw new YuMapperException("主键值为空！");
                    }
                }
                if (fieldDbName != null && o != null) {
                    updateStr.append(fieldDbName)
                            .append("=")
                            .append("#{").append(field.getName()).append("},");
                }
            }
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(),e);
        }
        if (pk == null) {
            throw new YuMapperException("没有指定主键！");
        }
        if (updateStr.length() > 0) {
            updateStr.deleteCharAt(updateStr.length() - 1);
        } else {
            throw new YuMapperException("无可更新字段");
        }
        return sqlStr.append(updateStr).append(" where ").append(pk).append("= #{").append(pkFieldName).append("}").toString();
    }

    public <T> String updateBySpecialColumn(T t,String column,Object value) {
        Map<String,Object> map = new HashMap<>();
        map.put(column,value);
        return updateByColumnMap(t,map);
    }

    public <T> String updateByColumnMap(T t, Map<String,Object> columnMap) {
        if (columnMap.isEmpty()) {
            throw new YuMapperException("必须指定条件");
        }
        StringBuilder sqlStr = new StringBuilder("update ")
                .append(getDomainTableName(t))
                .append(" set ");
        StringBuilder updateStr = new StringBuilder();
        Field[] declaredFields = t.getClass().getDeclaredFields();
        try {
            for (Field field : declaredFields) {
                field.setAccessible(true);
                Object o = field.get(t);
                String fieldDbName = getFieldDbName(field);
                if (fieldDbName != null && o != null) {
                    updateStr.append(fieldDbName)
                            .append("=")
                            .append("#{").append(field.getName()).append("},");
                }
            }
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(),e);
        }
        if (updateStr.length() > 0) {
            updateStr.deleteCharAt(updateStr.length() - 1);
        } else {
            throw new YuMapperException("无可更新字段");
        }
        StringBuilder whereStr = new StringBuilder(" where ");
        columnMap.forEach((column,value) -> {
            if (value == null) {
                whereStr.append(column).append(" is null");
            } else {
                whereStr.append(column).append(" = #{").append(column).append("} and");
            }
        });
        whereStr.delete(whereStr.length()-4,whereStr.length());
        return sqlStr.append(updateStr).append(whereStr).toString();
    }

    /**
     * 获取实体类数据库表名
     * @param t 实体类
     * @param <T> 实体类泛型
     * @return
     */
    private <T> String getDomainTableName(T t) {
        Table table = t.getClass().getAnnotation(Table.class);
        if (table != null) {
            return table.name();
        } else {
            return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE,t.getClass().getSimpleName());
        }
    }

    /**
     * 获取实体类属性数据库字段名
     * @param f 实体类属性
     * @return
     */
    private String getFieldDbName(Field f) {
        if (!Modifier.isStatic(f.getModifiers())) {
            Column column = f.getAnnotation(Column.class);
            if (column != null) {
                return column.name();
            } else {
                return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE,f.getName());
            }
        }
        return null;
    }

    public static void main(String[] args) {
//        ApprStrConf conf = new ApprStrConf();
//        conf.setPkid("1234");
//        conf.setApprStrName("aa");
//        GenericSqlProvider provider = new GenericSqlProvider();
//        System.out.println("插入语句：" + provider.insertSelective(conf));
//        Map<String,Object> map = new HashMap<>();
//        map.put("code","123415");
//        System.out.println("更新语句：" + provider.updateByColumnMap(conf,map));
    }

}
