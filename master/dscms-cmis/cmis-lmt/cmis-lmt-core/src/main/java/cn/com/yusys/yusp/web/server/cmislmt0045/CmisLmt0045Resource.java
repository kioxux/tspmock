package cn.com.yusys.yusp.web.server.cmislmt0045;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0045.req.CmisLmt0045ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0045.resp.CmisLmt0045RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0045.CmisLmt0045Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:校验客户合同项下是否存在未结清业务
 *
 * @author zhangjw 2021/8/26
 * @version 1.0
 */
@Api(tags = "cmislmt0045:校验客户合同项下是否存在未结清业务")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0045Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0045Resource.class);

    @Autowired
    private CmisLmt0045Service cmisLmt0045Service;
    /**
     * 交易码：cmislmt0045
     * 交易描述：校验客户合同项下是否存在未结清业务
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("校验客户合同项下是否存在未结清业务")
    @PostMapping("/cmislmt0045")
    protected @ResponseBody
    ResultDto<CmisLmt0045RespDto> cmisLmt0045(@Validated @RequestBody CmisLmt0045ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0045.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0045.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0045RespDto> cmisLmt0045RespDtoResultDto = new ResultDto<>();
        CmisLmt0045RespDto cmisLmt0045RespDto = new CmisLmt0045RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0045RespDto = cmisLmt0045Service.execute(reqDto);
            cmisLmt0045RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0045RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);

        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0045.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0045.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0045.value, e.getMessage());
            // 封装xddb0045DataResultDto中异常返回码和返回信息
            cmisLmt0045RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0045RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0016RespDto到cmisLmt0016RespDtoResultDto中
        cmisLmt0045RespDtoResultDto.setData(cmisLmt0045RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0045.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0045.value, JSON.toJSONString(cmisLmt0045RespDtoResultDto));
        return cmisLmt0045RespDtoResultDto;
    }
}
