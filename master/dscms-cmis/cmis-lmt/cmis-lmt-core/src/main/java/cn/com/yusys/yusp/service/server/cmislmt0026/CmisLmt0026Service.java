package cn.com.yusys.yusp.service.server.cmislmt0026;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprCoopInfo;
import cn.com.yusys.yusp.domain.ApprCoopSubInfo;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprCoopInfoService;
import cn.com.yusys.yusp.service.ApprCoopSubInfoService;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0026Service
 * @类描述: #对内服务类
 * @功能描述: 额度分项信息查询
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0026Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0026Service.class);

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService;

    @Autowired
    private ApprCoopInfoService apprCoopInfoService;

    /**
     * 额度分项信息查询
     *
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0026RespDto execute(CmisLmt0026ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.value);
        CmisLmt0026RespDto respDto = new CmisLmt0026RespDto();

        //金融机构代码
        String instuCde = reqDto.getInstuCde();
        //分项编号
        String subSerno = reqDto.getSubSerno();
        //分项类型
        String queryType = reqDto.getQueryType();
        try {
            //非空校验
            if(StringUtils.isBlank(subSerno)){
                respDto.setErrorCode(EclEnum.ECL070085.key);
                respDto.setErrorMsg(EclEnum.ECL070085.value);
                return respDto ;
            }

            if(StringUtils.isBlank(instuCde)){
                respDto.setErrorCode(EclEnum.ECL070130.key);
                respDto.setErrorMsg(EclEnum.ECL070130.value);
                return respDto ;
            }

            if(StringUtils.isBlank(queryType)){
                respDto.setErrorCode(EclEnum.ECL070131.key);
                respDto.setErrorMsg(EclEnum.ECL070131.value);
                return respDto ;
            }


            if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(queryType)){
                //单一客户授信
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("instuCde",instuCde);
                queryModel.addCondition("apprSubSerno",subSerno);
                queryModel.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);

                List<ApprLmtSubBasicInfo> apprLmtSubBasicInfos = apprLmtSubBasicInfoService.selectByModel(queryModel);
                if (CollectionUtils.isEmpty(apprLmtSubBasicInfos)){
                    throw new YuspException(EclEnum.ECL070113.key,EclEnum.ECL070113.value);
                }

                ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfos.get(0);

                //将查询到的批复分项信息放入respDto里
                copyInfo4ApprLmt(respDto,apprLmtSubBasicInfo);
                //授信总额
                BigDecimal avlAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlAmt());
                //授信总额已用
                BigDecimal outstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt());
                //授信总额可用 = 授信总额 - 授信总额已用
                BigDecimal avlAvailAmt = avlAmt.subtract(outstndAmt);

                logger.info("cmislmt0026 单一客户授信,分项编号【"+subSerno+"】,授信总额："+avlAmt+";授信总额已用："+outstndAmt+";授信总额可用："+avlAvailAmt);

                //敞口金额
                BigDecimal spacAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacAmt());
                //敞口已用额度
                BigDecimal spacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt());
                //敞口可用额度 = 敞口金额 - 敞口已用额度
                BigDecimal spacAvailAmt = spacAmt.subtract(spacOutstndAmt);

                logger.info("cmislmt0026 单一客户授信,分项编号【"+subSerno+"】,敞口金额："+spacAmt+";敞口已用额度："+spacOutstndAmt+";敞口可用额度："+spacAvailAmt);

                //父节点
                String parentId = apprLmtSubBasicInfo.getParentId();
                //批复台账编号
                String fkPkid = apprLmtSubBasicInfo.getFkPkid();

                if (StringUtils.isEmpty(parentId)){
                    logger.info("cmislmt0026 单一客户授信,分项编号【"+subSerno+"】,父节点为空，为额度分项");
                    //parent_id为空，则为额度分项,查询批复台账对应的可用金额和敞口可用金额
                    Map<String, BigDecimal> minAvailAmtMap = getMinAvailAmtAndMinAvialSpacAmt(fkPkid, avlAvailAmt, spacAvailAmt,subSerno);
                    avlAvailAmt = minAvailAmtMap.get("avlAvailAmt");
                    spacAvailAmt = minAvailAmtMap.get("spacAvailAmt");

                    //查询对应产品分项的最小可用金额和敞口可用金额
                    logger.info("cmislmt0026 单一客户授信,查询分项编号【"+subSerno+"】的二级分项的最小可用金额与最小敞口可用金额");
                    Map<String, BigDecimal> minAmtMap = apprLmtSubBasicInfoService.selectMinAvailAndSpacAmt(subSerno);

                    if (minAmtMap!=null && minAmtMap.size()>0){
                        //最小可用金额
                        BigDecimal minAvailAmt = minAmtMap.get("minAvailAmt");
                        //最小敞口可用金额
                        BigDecimal minAvailSpacAmt = minAmtMap.get("minAvailSpacAmt");

                        logger.info("cmislmt0026 单一客户授信,查询分项编号【"+subSerno+"】的二级分项的最小可用金额为："+minAvailAmt+";最小敞口可用金额为："+minAvailSpacAmt);

                        if (avlAvailAmt.compareTo(minAvailAmt)>0){
                            logger.info("cmislmt0026 单一客户授信,分项编号【"+subSerno+"】的授信可用金额【"+avlAvailAmt+"】大于其二级分项的最小可用金额【"+minAvailAmt+"】");
                            //如果可用金额大于产品分项最小可用金额，则取产品分项最小可用金额
                            avlAvailAmt = minAvailAmt;
                        }

                        if (spacAvailAmt.compareTo(minAvailSpacAmt)>0){
                            logger.info("cmislmt0026 单一客户授信,分项编号【"+subSerno+"】的敞口可用金额【"+spacAvailAmt+"】大于其二级分项的最小敞口可用金额【"+minAvailSpacAmt+"】");
                            spacAvailAmt = minAvailSpacAmt;
                        }
                    }
                }else{
                    logger.info("cmislmt0026 单一客户授信,分项编号【"+subSerno+"】,父节点不为空，为产品分项");
                    //产品分项，先查询对应的额度分项的可用金额和敞口可用金额
                    queryModel.addCondition("apprSubSerno",parentId);

                    List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = apprLmtSubBasicInfoService.selectByModel(queryModel);

                    if (CollectionUtils.isEmpty(apprLmtSubBasicInfoList)){
                        throw new YuspException(EclEnum.ECL070114.key,EclEnum.ECL070114.value);
                    }

                    apprLmtSubBasicInfo = apprLmtSubBasicInfoList.get(0);

                    //父节点授信总额
                    BigDecimal parentAvlAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlAmt());
                    //父节点已用金额
                    BigDecimal parentOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt());
                    //父节点可用金额
                    BigDecimal parentAvlAvailAmt = parentAvlAmt.subtract(parentOutstndAmt);

                    logger.info("cmislmt0026 单一客户授信,分项编号【"+subSerno+"】,额度分项授信总额："+parentAvlAmt+";额度分项授信总额已用："+parentOutstndAmt+";额度分项授信总额可用："+parentAvlAvailAmt);

                    if (avlAvailAmt.compareTo(parentAvlAvailAmt)>0){
                        logger.info("cmislmt0026 单一客户授信,产品分项编号【"+subSerno+"】的授信可用金额【"+avlAvailAmt+"】大于其额度分项的最小可用金额【"+parentAvlAvailAmt+"】");
                        //如果可用金额大于父节点可用金额，取父节点可用金额
                        avlAvailAmt = parentAvlAvailAmt;
                    }

                    //父节点敞口金额
                    BigDecimal parentSpacAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacAmt());
                    //父节点敞口已用金额
                    BigDecimal parentSpacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt());
                    //父节点敞口可用金额 = 父节点敞口金额 - 父节点敞口已用金额
                    BigDecimal parentSpacAvailAmt = parentSpacAmt.subtract(parentSpacOutstndAmt);

                    if (spacAvailAmt.compareTo(parentSpacAvailAmt)>0){
                        logger.info("cmislmt0026 单一客户授信,产品分项编号【"+subSerno+"】的敞口可用金额【"+spacAvailAmt+"】大于其额度分项的最小敞口可用金额【"+parentSpacAvailAmt+"】");
                        //如果敞口可用金额大于父节点敞口可用金额，取父节点敞口可用金额
                        spacAvailAmt = parentSpacAvailAmt;
                    }

                    //查询对应批复台账编号的可用金额和敞口可用金额
                    Map<String, BigDecimal> minAvailAmtMap = getMinAvailAmtAndMinAvialSpacAmt(fkPkid, avlAvailAmt, spacAvailAmt,subSerno);
                    avlAvailAmt = minAvailAmtMap.get("avlAvailAmt");
                    spacAvailAmt = minAvailAmtMap.get("spacAvailAmt");
                }

                //授信总额可用
                respDto.setAvlAvailAmt(avlAvailAmt);
                //授信敞口可用
                respDto.setSpacAvailAmt(spacAvailAmt);
            }else if(CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(queryType)){
                //合作方授信
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("instuCde",instuCde);
                queryModel.addCondition("apprSubSerno",subSerno);
                queryModel.addCondition("oprType",CmisLmtConstants.OPR_TYPE_ADD);

                List<ApprCoopSubInfo> apprCoopSubInfos = apprCoopSubInfoService.selectByModel(queryModel);

                if (CollectionUtils.isEmpty(apprCoopSubInfos)){
                    throw new YuspException(EclEnum.ECL070011.key,EclEnum.ECL070011.value);
                }

                ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfos.get(0);
                //批复台账流水号
                String apprSerno = apprCoopSubInfo.getApprSerno();
                queryModel.addCondition("apprSerno",apprSerno);

                List<ApprCoopInfo> apprCoopInfos = apprCoopInfoService.selectByModel(queryModel);

                if (CollectionUtils.isEmpty(apprCoopInfos)){
                    throw new YuspException(EclEnum.ECL070012.key,EclEnum.ECL070012.value);
                }

                ApprCoopInfo apprCoopInfo = apprCoopInfos.get(0);
                //将合作方信息拷贝到respDto
                copyInfo4ApprCoop(respDto,apprCoopInfo,apprCoopSubInfo);

                //授信总额
                BigDecimal avlAmt = BigDecimalUtil.replaceNull(apprCoopSubInfo.getAvlAmt());
                //授信总额已用
                BigDecimal outstndAmt = BigDecimalUtil.replaceNull(apprCoopSubInfo.getOutstndAmt());
                //授信总额可用 = 授信总额 - 授信总额已用
                BigDecimal avlAvailAmt = avlAmt.subtract(outstndAmt);

                logger.info("cmislmt0026 合作方授信,分项编号【"+subSerno+"】,授信总额："+avlAmt+";授信总额已用："+outstndAmt+";授信总额可用："+avlAvailAmt);

                //根据批复台账编号查询对应的分项的授信总额之和和已用总额之和
                Map<String, BigDecimal> amtMap = apprCoopSubInfoService.selectTotalAmtByApprSerno(apprSerno);
                //合作方授信台账可用总额
                BigDecimal parentAvlAvailAmt = amtMap.get("avlAmtTotal").subtract(amtMap.get("outstndAmtTotal"));

                if (avlAvailAmt.compareTo(parentAvlAvailAmt)>0){
                    logger.info("cmislmt0026 合作方授信,分项编号【"+subSerno+"】的授信可用金额【"+avlAvailAmt+"】大于其合作方授信台账可用总额【"+parentAvlAvailAmt+"】");
                    //如果分项可用金额大于台账可用金额，则取台账可用金额
                    avlAvailAmt = parentAvlAvailAmt;
                }

                //授信可用金额
                respDto.setAvlAvailAmt(avlAvailAmt);
                //授信敞口可用取授信可用金额
                respDto.setSpacAvailAmt(avlAvailAmt);
            }

            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("额度分项信息查询" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.value);
        return respDto;
    }

    /**
     * 获取最小可用金额和最小敞口可用金额
     * @param apprSerno
     * @param avlAvailAmt
     * @param spacAvailAmt
     * @return
     */
    public Map<String,BigDecimal> getMinAvailAmtAndMinAvialSpacAmt(String apprSerno,BigDecimal avlAvailAmt,BigDecimal spacAvailAmt,String subSerno){
        //获取汇总金额
        logger.info("cmislmt0026 单一客户授信,查询批复台账编号【"+apprSerno+"】名下的批复分项授信总额之和等");
        Map<String, BigDecimal> amtMap = apprLmtSubBasicInfoService.selectLmtSubTotal(apprSerno);

        //授信已用金额之和
        BigDecimal outstndAmtTotal = amtMap.get("outstndAmt");
        //授信总额之和
        BigDecimal avlAmtTotal = amtMap.get("avlAmt");
        //授信可用金额之和 = 授信总额之和 - 授信已用金额之和
        BigDecimal avlAvailAmtTotal = avlAmtTotal.subtract(outstndAmtTotal);

        logger.info("cmislmt0026 单一客户授信,批复台账编号【"+apprSerno+"】名下的批复分项授信总额之和为："+avlAmtTotal+";已用额度之和："+outstndAmtTotal+";授信总额可用之和："+avlAvailAmtTotal);

        if (avlAvailAmt.compareTo(avlAvailAmtTotal)>0){
            logger.info("cmislmt0026 单一客户授信,批复分项编号【"+subSerno+"】的授信总额可用："+avlAvailAmt+"大于批复台账编号【"+apprSerno+"】名下的批复分项授信总额可用之和："+avlAvailAmtTotal);
            //如果额度分项的可用金额大于批复台账可用金额，则取批复台账可用金额
            avlAvailAmt = avlAvailAmtTotal;
        }

        //敞口金额之和
        BigDecimal spacAmtTotal = amtMap.get("spacAmt");
        //敞口已用金额之和
        BigDecimal spacOutstndAmtTotal = amtMap.get("spacOutstndAmt");
        //敞口可用金额之和
        BigDecimal spacAvailAmtTotal = spacAmtTotal.subtract(spacOutstndAmtTotal);

        logger.info("cmislmt0026 单一客户授信,批复台账编号【"+apprSerno+"】名下的批复分项敞口金额之和为："+spacAmtTotal+";敞口已用额度之和："+spacOutstndAmtTotal+";敞口可用额度之和："+spacAvailAmtTotal);

        if (spacAvailAmt.compareTo(spacAvailAmtTotal)>0){
            //如果额度分项的敞口可用金额大于批复台账敞口可用金额，则取批复台账敞口可用金额
            spacAvailAmt = spacAvailAmtTotal;
        }
        //最小可用金额
        amtMap.put("avlAvailAmt",avlAvailAmt);
        //最小敞口可用金额
        amtMap.put("spacAvailAmt",spacAvailAmt);
        return amtMap;
    }

    /**
     * 将查询到的批复分项信息放入respDto里
     * @param respDto
     * @param apprLmtSubBasicInfo
     */
    public void copyInfo4ApprLmt(CmisLmt0026RespDto respDto,ApprLmtSubBasicInfo apprLmtSubBasicInfo){
        //客户编号
        respDto.setCusId(apprLmtSubBasicInfo.getCusId());
        //批复编号
        respDto.setAccNo(apprLmtSubBasicInfo.getFkPkid());
        //分项编号
        respDto.setSubSerno(apprLmtSubBasicInfo.getApprSubSerno());
        //授信品种编号
        respDto.setLimitSubNo(apprLmtSubBasicInfo.getLimitSubNo());
        //授信品种名称
        respDto.setLimitSubName(apprLmtSubBasicInfo.getLimitSubName());
        //担保方式
        respDto.setSuitGuarWay(apprLmtSubBasicInfo.getSuitGuarWay());
        //是否低风险授信
        respDto.setIsLriskLmt(apprLmtSubBasicInfo.getIsLriskLmt());
        //币种
        respDto.setCny(apprLmtSubBasicInfo.getCurType());
        //是否循环
        respDto.setIsRevolv(apprLmtSubBasicInfo.getIsRevolv());
        //是否预授信
        respDto.setIsPreCrd(apprLmtSubBasicInfo.getIsPreCrd());
        //授信总额
        respDto.setAvlAmt(apprLmtSubBasicInfo.getAvlAmt());
        //授信总额已用
        respDto.setAvlOutstndAmt(apprLmtSubBasicInfo.getOutstndAmt());
        //授信敞口
        respDto.setSpacAmt(apprLmtSubBasicInfo.getSpacAmt());
        //授信敞口已用
        respDto.setSpacOutstndAmt(apprLmtSubBasicInfo.getSpacOutstndAmt());
        //额度起始日
        respDto.setStartDate(apprLmtSubBasicInfo.getStartDate());
        //额度到期日
        respDto.setEndDate(apprLmtSubBasicInfo.getLmtDate());
        //期限
        respDto.setTerm((Integer) apprLmtSubBasicInfo.getTerm());
        //额度状态
        respDto.setStatus(apprLmtSubBasicInfo.getStatus());
        //年利率
        respDto.setRateYear(apprLmtSubBasicInfo.getRateYear());
        //预留保证金比例
        respDto.setBailPreRate(apprLmtSubBasicInfo.getBailPreRate());
        //可出账金额
        respDto.setValPvpAmt(apprLmtSubBasicInfo.getAvlOutstndAmt());
        //产品类型属性
        respDto.setLmtBizTypeProp(apprLmtSubBasicInfo.getLmtBizTypeProp());
    }

    /**
     * 将合作方信息拷贝到respDto
     * @param respDto
     * @param apprCoopInfo
     * @param apprCoopSubInfo
     */
    public void copyInfo4ApprCoop(CmisLmt0026RespDto respDto,ApprCoopInfo apprCoopInfo,ApprCoopSubInfo apprCoopSubInfo){
        //客户编号
        respDto.setCusId(apprCoopInfo.getCusId());
        //批复编号
        respDto.setAccNo(apprCoopSubInfo.getApprSerno());
        //分项编号
        respDto.setSubSerno(apprCoopSubInfo.getApprSubSerno());
        //授信品种编号
        respDto.setLimitSubNo(apprCoopSubInfo.getLimitSubNo());
        //授信品种名称
        respDto.setLimitSubName(apprCoopSubInfo.getLimitSubName());
        //担保方式
        respDto.setSuitGuarWay(null);
        //是否低风险授信 默认非低风险
        respDto.setIsLriskLmt(CmisLmtConstants.STD_ZB_YES_NO_N);
        //币种
        respDto.setCny(apprCoopSubInfo.getCurType());
        //是否循环
        respDto.setIsRevolv(apprCoopSubInfo.getIsRevolv());
        //是否预授信
        respDto.setIsPreCrd(null);
        //授信总额
        respDto.setAvlAmt(apprCoopSubInfo.getAvlAmt());
        //授信总额已用
        respDto.setAvlOutstndAmt(apprCoopSubInfo.getOutstndAmt());
        //授信敞口取授信总额
        respDto.setSpacAmt(apprCoopSubInfo.getAvlAmt());
        //授信敞口已用取授信总额已用
        respDto.setSpacOutstndAmt(apprCoopSubInfo.getOutstndAmt());
        //额度起始日
        respDto.setStartDate(apprCoopSubInfo.getStartDate());
        //额度到期日
        respDto.setEndDate(apprCoopSubInfo.getEndDate());
        //期限
        respDto.setTerm((Integer) apprCoopSubInfo.getTerm());
        //额度状态
        respDto.setStatus(apprCoopSubInfo.getStatus());
        //年利率
        respDto.setRateYear(null);
        //预留保证金比例
        respDto.setBailPreRate(BigDecimal.ZERO);
        //可出账金额
        respDto.setValPvpAmt(null);
        //产品类型属性
        respDto.setLmtBizTypeProp(apprCoopSubInfo.getLmtBizTypeProp());
    }
}