package cn.com.yusys.yusp.web.server.cmislmt0013;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0013.CmisLmt0013Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:单一客户额度同步
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0013:合同下出账交易")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0013Resource.class);

    @Autowired
    private CmisLmt0013Service cmisLmt0013Service;
    /**
     * 交易码：cmislmt0013
     * 交易描述：合同下出账交易(台账占用)
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("合同下出账交易")
    @PostMapping("/cmislmt0013")
    protected @ResponseBody
    ResultDto<CmisLmt0013RespDto> cmisLmt0013(@Validated @RequestBody CmisLmt0013ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0013RespDto> cmisLmt0013RespDtoResultDto = new ResultDto<>();
        CmisLmt0013RespDto cmisLmt0013RespDto = new CmisLmt0013RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0013RespDto = cmisLmt0013Service.execute(reqDto);
            cmisLmt0013RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0013RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (YuspException yuspe) {
            logger.error("台账占用接口报错：", yuspe);
            cmisLmt0013RespDto.setErrorCode(yuspe.getCode());
            cmisLmt0013RespDto.setErrorMsg(yuspe.getMsg());
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.value, e.getMessage());
            // 封装xddb0013DataResultDto中异常返回码和返回信息
            cmisLmt0013RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0013RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }

        // 封装cmisLmt0013RespDto到cmisLmt0013RespDtoResultDto中
        cmisLmt0013RespDtoResultDto.setData(cmisLmt0013RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.value, JSON.toJSONString(cmisLmt0013RespDtoResultDto));
        return cmisLmt0013RespDtoResultDto;
    }
}
