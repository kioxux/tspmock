/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.LmtWhiteInfo;
import cn.com.yusys.yusp.domain.LmtWhiteInfoHistory;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Com001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Data;
import cn.com.yusys.yusp.dto.client.esb.com001.resp.Com001RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.req.CmisCus0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CmisCus0010RespDto;
import cn.com.yusys.yusp.repository.mapper.LmtWhiteInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.client.bsp.comstar.com001.Com001Service;
import cn.com.yusys.yusp.service.server.cmislmt0003.CmisLmt0003Service;
import cn.com.yusys.yusp.vo.WhiteInfo01ExportVo;
import cn.com.yusys.yusp.vo.WhiteInfo01ImportVo;
import cn.com.yusys.yusp.vo.WhiteInfo02ExportVo;
import cn.com.yusys.yusp.vo.WhiteInfo02ImportVo;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtWhiteInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-04-17 11:08:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtWhiteInfoService {

    @Autowired
    private LmtWhiteInfoMapper lmtWhiteInfoMapper;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private Com001Service com001Service;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private LmtWhiteInfoHistoryService lmtWhiteInfoHistoryService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0003Service.class);
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtWhiteInfo selectByPrimaryKey(String pkId) {
        return lmtWhiteInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtWhiteInfo> selectAll(QueryModel model) {
        List<LmtWhiteInfo> records = (List<LmtWhiteInfo>) lmtWhiteInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtWhiteInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtWhiteInfo> list = lmtWhiteInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtWhiteInfo record) {
        //生成流水号
        String subAccNo = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.LMT_SERNO, new HashMap<>());
        //设置新的申请流水号
        record.setSubAccNo(subAccNo);
        int count = lmtWhiteInfoMapper.insert(record);
        List<LmtWhiteInfo> lmtWhiteInfoList = new ArrayList<>();
        lmtWhiteInfoList.add(record);
        //白名单额度推送ComStar
        sendComStar(lmtWhiteInfoList);

        //新增白名单额度信息历史记录
        LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(record);
        lmtWhiteInfoHistoryService.insert(lmtWhiteInfoHistory);
        return count;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtWhiteInfo record) {
        int count = lmtWhiteInfoMapper.insertSelective(record);
        List<LmtWhiteInfo> lmtWhiteInfoList = new ArrayList<>();
        lmtWhiteInfoList.add(record);
        sendComStar(lmtWhiteInfoList);
        //新增白名单额度信息历史记录
        LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(record);
        lmtWhiteInfoHistoryService.insert(lmtWhiteInfoHistory);
        return count;
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtWhiteInfo record) {
        int count = lmtWhiteInfoMapper.updateByPrimaryKey(record);
        List<LmtWhiteInfo> lmtWhiteInfoList = new ArrayList<>();
        lmtWhiteInfoList.add(record);
        sendComStar(lmtWhiteInfoList);
        //新增白名单额度信息历史记录
        LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(record);
        lmtWhiteInfoHistoryService.insert(lmtWhiteInfoHistory);
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtWhiteInfo record) {
        int count = lmtWhiteInfoMapper.updateByPrimaryKeySelective(record);
        List<LmtWhiteInfo> lmtWhiteInfoList = new ArrayList<>();
        lmtWhiteInfoList.add(record);
        sendComStar(lmtWhiteInfoList);
        //新增白名单额度信息历史记录
        LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(record);
        lmtWhiteInfoHistoryService.insert(lmtWhiteInfoHistory);
        return count;
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfoMapper.selectByPrimaryKey(pkId);
        int count = lmtWhiteInfoMapper.deleteByPrimaryKey(pkId);
        List<LmtWhiteInfo> lmtWhiteInfoList = new ArrayList<>();
        //授信金额设为0
        lmtWhiteInfo.setSigAmt(BigDecimal.ZERO);
        lmtWhiteInfoList.add(lmtWhiteInfo);
        sendComStar(lmtWhiteInfoList);
        //新增白名单额度信息历史记录
        LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(lmtWhiteInfo);
        lmtWhiteInfoHistoryService.insert(lmtWhiteInfoHistory);
        return count;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        List<LmtWhiteInfo> lmtWhiteInfoList = lmtWhiteInfoMapper.selectByIds(ids);
        int count = lmtWhiteInfoMapper.deleteByIds(ids);

        for (LmtWhiteInfo lmtWhiteInfo:lmtWhiteInfoList) {
            //授信金额设为0
            lmtWhiteInfo.setSigAmt(BigDecimal.ZERO);
            //新增白名单额度信息历史记录
            LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(lmtWhiteInfo);
            lmtWhiteInfoHistoryService.insert(lmtWhiteInfoHistory);
        }
        sendComStar(lmtWhiteInfoList);
        return count;
    }


    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(String pkId) {
        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("pkId",pkId);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType",  CmisLmtConstants.OPR_TYPE_DELETE);

        List<LmtWhiteInfo> lmtWhiteInfoList = lmtWhiteInfoMapper.selectByIds(pkId);
        for (LmtWhiteInfo lmtWhiteInfo:lmtWhiteInfoList) {
            //授信金额设为0
            lmtWhiteInfo.setSigAmt(BigDecimal.ZERO);
            //新增白名单额度信息历史记录
            LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(lmtWhiteInfo);
            lmtWhiteInfoHistoryService.insert(lmtWhiteInfoHistory);
        }
        sendComStar(lmtWhiteInfoList);
        return lmtWhiteInfoMapper.updateByParams(delMap);
    }

    /**
     * 批量插入.
     * @param lmtWhiteInfoList 白名单管理
     * @return 结果
     */
    public int batchInsert(List<LmtWhiteInfo> lmtWhiteInfoList) {
        int count = lmtWhiteInfoList.size() != 0 ? lmtWhiteInfoMapper.batchInsert(lmtWhiteInfoList) : 0;
        sendComStar(lmtWhiteInfoList);
        //往白名单额度信息历史表里批量插入数据
        List<LmtWhiteInfoHistory> lmtWhiteInfoHistoryList = new ArrayList<>();

        for (LmtWhiteInfo lmtWhiteInfo : lmtWhiteInfoList) {
            //将白名单额度信息里的数据复制到历史里
            LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(lmtWhiteInfo);
            lmtWhiteInfoHistoryList.add(lmtWhiteInfoHistory);
        }

        lmtWhiteInfoHistoryService.batchInsert(lmtWhiteInfoHistoryList);
        return count;
    }


    /**
     * @方法名称: selectLmtWhiteInfoBySubNo
     * @方法描述: 根据分项编号，获取白名单额度信息
     * @参数与返回说明: 返回白名单信息
     * @算法描述:
     */

    public LmtWhiteInfo selectLmtWhiteInfoBySubNo(String subAccNo) {
        QueryModel query = new QueryModel() ;
        //分享编号
        query.addCondition("subAccNo", subAccNo);
        //操作类型为新增
        query.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<LmtWhiteInfo> lmtWhiteInfos = lmtWhiteInfoMapper.selectByModel(query) ;
        if(CollectionUtils.isNotEmpty(lmtWhiteInfos)){
            return lmtWhiteInfos.get(0) ;
        }
        return null ;
    }

    /**
     * 产品户买入返售（信用债）发送comstar
     * @param lmtWhiteInfoList
     */
    public void sendComStar(List<LmtWhiteInfo> lmtWhiteInfoList){
        //发送comstar
        Com001ReqDto com001ReqDto = new Com001ReqDto();
        Data com001ReqDtoData = new Data ();
        java.util.List<cn.com.yusys.yusp.dto.client.esb.com001.req.List> com001ReqLists = new ArrayList<>();
        int count = 0;

        String instuCde = null;

        for (LmtWhiteInfo lmtWhiteInfo:lmtWhiteInfoList) {
            cn.com.yusys.yusp.dto.client.esb.com001.req.List com001ReqDtoList = new cn.com.yusys.yusp.dto.client.esb.com001.req.List();

            if (CmisLmtConstants.STD_ZB_LMT_WHITE_TYPE_02.equals(lmtWhiteInfo.getLmtWhiteType())){
                count++;
                instuCde = lmtWhiteInfo.getInstuCde();
                //序号
                com001ReqDtoList.setNumber(String.valueOf(count));
                //授信批复编号取额度分项编号
                com001ReqDtoList.setOrigiAccNo(lmtWhiteInfo.getSubAccNo());
                //授信分项ID取额度分项编号
                com001ReqDtoList.setLmtSubNo(lmtWhiteInfo.getSubAccNo());
                //Ecif客户号
                com001ReqDtoList.setCusId(lmtWhiteInfo.getCusId());
                //TODO 授信主体证件类型	 certType
                //com001ReqDtoList.setCertType();
                //TODO 授信主体证件号码	certCode
                //com001ReqDtoList.setCertCode();
                //授信主体客户名称
                com001ReqDtoList.setCusName(lmtWhiteInfo.getCusName());
                //额度到期日
                String endDate = lmtWhiteInfo.getEndDate() ;
                if(StringUtils.isNotEmpty(endDate)){
                    endDate = endDate.replace("-","") ;
                }
                com001ReqDtoList.setEndDate(endDate);
                //额度品种编号 默认 300701-产品户买入返售（信用债）
                com001ReqDtoList.setLimitSubNo(CmisLmtConstants.LIMIT_SUB_NO_300701);
                //额度品种名称 默认 产品户买入返售（信用债）
                com001ReqDtoList.setLimitSubName(CmisLmtConstants.LIMIT_SUB_NO_300701_NAME);
                //操作类型 默认 01-调整
                com001ReqDtoList.setOptType(CmisLmtConstants.OPR_TYPE_ADD);
                //授信金额 默认取机构买入返售限额
                com001ReqDtoList.setLmtAmt(lmtWhiteInfo.getSigAmt());
                //单个产品户买入返售限额
                com001ReqDtoList.setSingleeResaleQuota(lmtWhiteInfo.getSingleResaleQuota());
                //额度类型 默认 01--同业综合授信额度
                com001ReqDtoList.setLmtType(CmisLmtConstants.COMSTAR_LMT_TYPE_01);
                com001ReqLists.add(com001ReqDtoList);
            }
        }

        //同业额度信息发给comstar
        if (com001ReqLists.size()>0){
            com001ReqDtoData.setList(com001ReqLists);
            com001ReqDto.setData(com001ReqDtoData);
            //交易流水号	serno
            Map seqMap = new HashMap();
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, seqMap);
            com001ReqDto.setSerno(serno);
            //金融机构代码
            com001ReqDto.setInstuCde(instuCde);
            try{
                logger.info(this.getClass().getName()+",调用Com001接口，同步额度信息到Comstart:"+ JSON.toJSONString(com001ReqDto));
                Com001RespDto com001RespDto = com001Service.Com001(com001ReqDto);
                logger.info(this.getClass().getName()+",调用Com001接口，同步额度信息到Comstar结束------>:"+ JSON.toJSONString(com001RespDto));
            }catch (Exception e){
                throw new BizException(null, "9999", null, "ComStar额度同步失败，请确认是否在ComStar系统维护客户信息");
            }
        }
    }

    /**
     * 异步下载承兑行白名单模板
     * @return 导出进度信息
     */
    public ProgressDto asyncExportWhiteInfo01() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {

                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(WhiteInfo01ImportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 异步下载产品户买入返售白名单模板
     * @return 导出进度信息
     */
    public ProgressDto asyncExportWhiteInfo02() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(WhiteInfo02ImportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 异步导出承兑行白名单数据
     * @return 导出进度信息
     */
    public ProgressDto asyncExportLmtWhiteInfo01(QueryModel model) {
        model.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        model.addCondition("lmtWhiteType", CmisLmtConstants.STD_ZB_LMT_WHITE_TYPE_01);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            return selectNameByModel01(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(WhiteInfo01ExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 异步导出产品户买入返售白名单数据
     * @return 导出进度信息
     */
    public ProgressDto asyncExportLmtWhiteInfo02(QueryModel model) {
        model.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        model.addCondition("lmtWhiteType", CmisLmtConstants.STD_ZB_LMT_WHITE_TYPE_02);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            return selectNameByModel02(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(WhiteInfo02ExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 导出白名单额度 责任人/责任机构转换
     * @param model
     * @return
     */
    public List<WhiteInfo01ExportVo> selectNameByModel01 (QueryModel model) {
        List<WhiteInfo01ExportVo> whiteInfo01ExportVoList = new ArrayList<>() ;
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtWhiteInfo> list = lmtWhiteInfoMapper.selectByModel(model);
        PageHelper.clearPage();

        for (LmtWhiteInfo lmtWhiteInfo:list){
            WhiteInfo01ExportVo whiteInfo01ExportVo = new WhiteInfo01ExportVo();
            String orgName = OcaTranslatorUtils.getOrgName(lmtWhiteInfo.getInputBrId());//责任机构
            String userName = OcaTranslatorUtils.getUserName(lmtWhiteInfo.getInputId());   //责任人
            org.springframework.beans.BeanUtils.copyProperties(lmtWhiteInfo, whiteInfo01ExportVo);
            whiteInfo01ExportVo.setInputBrIdName(orgName);
            whiteInfo01ExportVo.setInputIdName(userName);
            whiteInfo01ExportVoList.add(whiteInfo01ExportVo);
        }
        return whiteInfo01ExportVoList;
    }

    /**
     * 导出白名单额度 责任人/责任机构转换
     * @param model
     * @return
     */
    public List<WhiteInfo02ExportVo> selectNameByModel02 (QueryModel model) {
        List<WhiteInfo02ExportVo> whiteInfo02ExportVoList = new ArrayList<>() ;
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtWhiteInfo> list = lmtWhiteInfoMapper.selectByModel(model);
        PageHelper.clearPage();

        for (LmtWhiteInfo lmtWhiteInfo:list){
            WhiteInfo02ExportVo whiteInfo02ExportVo = new WhiteInfo02ExportVo();
            String orgName = OcaTranslatorUtils.getOrgName(lmtWhiteInfo.getInputBrId());//责任机构
            String userName = OcaTranslatorUtils.getUserName(lmtWhiteInfo.getInputId());   //责任人
            org.springframework.beans.BeanUtils.copyProperties(lmtWhiteInfo, whiteInfo02ExportVo);
            whiteInfo02ExportVo.setInputBrIdName(orgName);
            whiteInfo02ExportVo.setInputIdName(userName);

            BigDecimal sigAmt = lmtWhiteInfo.getSigAmt();
            if (sigAmt != null){
                whiteInfo02ExportVo.setSigAmt(sigAmt.divide(BigDecimal.valueOf(10000L)));
            }
            if (lmtWhiteInfo.getCreateTime() == null){
                whiteInfo02ExportVo.setCreateTime(lmtWhiteInfo.getUpdateTime());
            }
            whiteInfo02ExportVoList.add(whiteInfo02ExportVo);
        }
        return whiteInfo02ExportVoList;
    }


    /**
     * 批量插入承兑行白名单
     *
     * @param perWhiteInfoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertWhiteInfo01(List<Object> perWhiteInfoList) {
        List<LmtWhiteInfo> whiteInfoList = (List<LmtWhiteInfo>) BeanUtils.beansCopy(perWhiteInfoList, LmtWhiteInfo.class);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            LmtWhiteInfoMapper lmtWhiteInfoMapper = sqlSession.getMapper(LmtWhiteInfoMapper.class);
            for (LmtWhiteInfo whiteInfo : whiteInfoList) {
                QueryModel query = new QueryModel() ;
                query.addCondition("cusId", whiteInfo.getCusId());
                query.addCondition("lmtWhiteType", CmisLmtConstants.STD_ZB_LMT_WHITE_TYPE_01);
                query.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
                List<LmtWhiteInfo> lmtWhiteInfos = lmtWhiteInfoMapper.selectByModel(query) ;
                //数据库中已存在，更新信息
                if (CollectionUtils.isNotEmpty(lmtWhiteInfos)){
                    LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfos.get(0);
                    //限额
                    lmtWhiteInfo.setSigAmt(whiteInfo.getSigAmt().multiply(new BigDecimal(10000)));

                    //到期日转换  by zhangjw 20210813
                    String endDate = whiteInfo.getEndDate();
                    try{
                        endDate = LocalDate.parse(endDate,dateTimeFormatter).toString();
                    }catch (Exception e){
                        throw new BizException(null, "9999", null, "到期日格式录入错误，请使用YYYY-MM-DD格式");
                    }
                    //到期日期
                    lmtWhiteInfo.setEndDate(endDate);
                    //最近修改人
                    lmtWhiteInfo.setUpdId(userInfo.getLoginCode());
                    //最近修改机构
                    lmtWhiteInfo.setUpdBrId(userInfo.getOrg().getCode());
                    //最近修改日期
                    lmtWhiteInfo.setUpdDate(sf.format(new Date()));
                    //修改时间
                    lmtWhiteInfo.setUpdateTime(new Date());
                    lmtWhiteInfoMapper.updateByPrimaryKeySelective(lmtWhiteInfo);

                    //新增白名单额度信息历史记录
                    LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(lmtWhiteInfo);
                    lmtWhiteInfoHistoryService.insert(lmtWhiteInfoHistory);
                }//数据库中不存在，新增信息
                else{
                    //生成主键
                    Map paramMap= new HashMap<>() ;
                    String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
                    whiteInfo.setPkId(pkValue);
                    //金融机构编号
                    whiteInfo.setInstuCde(userInfo.getInstuOrg().getCode());

                    //增加查询同业客户信息
                    CmisCus0010ReqDto cmisCus0010ReqDto = new CmisCus0010ReqDto();
                    cmisCus0010ReqDto.setCusId(whiteInfo.getCusId());
                    ResultDto<CmisCus0010RespDto> cmisCus0010RespDtoResultDto = cmisCusClientService.cmiscus0010(cmisCus0010ReqDto);
                    if(cmisCus0010RespDtoResultDto == null){
                        throw new BizException(null, "99999",null,  "获取同业客户信息失败！");
                    }
                    CmisCus0010RespDto cus0010RespDto = cmisCus0010RespDtoResultDto.getData();

                    String aorgNo = whiteInfo.getAorgNo();
                    String cusName = whiteInfo.getCusName();
                    String cusType = whiteInfo.getCusType();

                    if (cus0010RespDto!=null){
                        aorgNo = cus0010RespDto.getLargeBankNo();
                        cusName = cus0010RespDto.getCusName();
                        cusType = cus0010RespDto.getIntbankType();
                    }else{
                        throw new BizException(null, "9999", null, "根据客户号未获取到客户信息！");
                    }

                    whiteInfo.setAorgNo(aorgNo);
                    whiteInfo.setCusName(cusName);
                    //客户类型
                    whiteInfo.setCusType(cusType);

                    //限额（万元）转换
                    whiteInfo.setSigAmt(whiteInfo.getSigAmt().multiply(new BigDecimal(10000)));
                    //白名单额度类型
                    whiteInfo.setLmtWhiteType(CmisLmtConstants.STD_ZB_LMT_WHITE_TYPE_01);
                    //操作类型
                    whiteInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                    //额度分项编号
                    whiteInfo.setSubAccNo(pkValue);
                    //开始日期
                    whiteInfo.setStartDate(sf.format(new Date()));
                    //到期日转换  by zhangjw 20210813
                    String endDate = whiteInfo.getEndDate();
                    try{
                        endDate = LocalDate.parse(endDate,dateTimeFormatter).toString();
                    }catch (Exception e){
                        throw new BizException(null, "9999", null, "到期日格式录入错误，请使用YYYY-MM-DD格式");
                    }
                    //到期日期
                    whiteInfo.setEndDate(endDate);
                    //已用限额
                    whiteInfo.setSigUseAmt(new BigDecimal(0));
                    //登记人
                    whiteInfo.setInputId(userInfo.getLoginCode());
                    //登记机构
                    whiteInfo.setInputBrId(userInfo.getOrg().getCode());
                    String openday = stringRedisTemplate.opsForValue().get("openDay");//当前日期
                    //登记日期
                    whiteInfo.setInputDate(openday);
                    //创建时间
                    whiteInfo.setCreateTime(new Date());
                    lmtWhiteInfoMapper.insertSelective(whiteInfo);

                    //新增白名单额度信息历史记录
                    LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(whiteInfo);
                    lmtWhiteInfoHistoryService.insert(lmtWhiteInfoHistory);
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perWhiteInfoList.size();
    }

    /**
     * 批量插入产品户买入返售白名单
     *
     * @param perWhiteInfoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertWhiteInfo02(List<Object> perWhiteInfoList) {
        List<LmtWhiteInfo> whiteInfoList = (List<LmtWhiteInfo>) BeanUtils.beansCopy(perWhiteInfoList, LmtWhiteInfo.class);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        User userInfo = SessionUtils.getUserInformation();
        List<LmtWhiteInfo> lmtWhiteInfoList = new ArrayList<>() ;
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            LmtWhiteInfoMapper lmtWhiteInfoMapper = sqlSession.getMapper(LmtWhiteInfoMapper.class);

            for (LmtWhiteInfo whiteInfo : whiteInfoList) {
                QueryModel query = new QueryModel() ;
                query.addCondition("cusId", whiteInfo.getCusId());
                query.addCondition("lmtWhiteType", CmisLmtConstants.STD_ZB_LMT_WHITE_TYPE_02);
                query.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
                List<LmtWhiteInfo> lmtWhiteInfos = lmtWhiteInfoMapper.selectByModel(query) ;
                //数据库中已存在，更新信息
                if (CollectionUtils.isNotEmpty(lmtWhiteInfos)){
                    LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfos.get(0);
                    //新增白名单额度信息历史记录
                    LmtWhiteInfoHistory lmtWhiteInfoHistory = copyPropertiesFromLmtWhiteInfo(lmtWhiteInfo);
                    lmtWhiteInfoHistoryService.insert(lmtWhiteInfoHistory);

                    //限额
                    lmtWhiteInfo.setSigAmt(whiteInfo.getSigAmt().multiply(new BigDecimal(10000)));
                    //单个产品户买入返售限额
                    lmtWhiteInfo.setSingleResaleQuota(whiteInfo.getSingleResaleQuota().multiply(new BigDecimal(10000)));
                    //到期日转换  by zhangjw 20210813
                    String endDate = whiteInfo.getEndDate();
                    try{
                        endDate = LocalDate.parse(endDate,dateTimeFormatter).toString();
                    }catch (Exception e){
                        throw new BizException(null, "9999", null, "到期日格式录入错误，请使用YYYY-MM-DD格式");
                    }
                    //到期日期
                    lmtWhiteInfo.setEndDate(endDate);
                    //最近修改人
                    lmtWhiteInfo.setUpdId(userInfo.getLoginCode());
                    //最近修改机构
                    lmtWhiteInfo.setUpdBrId(userInfo.getOrg().getCode());
                    //最近修改日期
                    lmtWhiteInfo.setUpdDate(sf.format(new Date()));
                    //修改时间
                    lmtWhiteInfo.setUpdateTime(new Date());
                    lmtWhiteInfoList.add(lmtWhiteInfo);
                    lmtWhiteInfoMapper.updateByPrimaryKeySelective(lmtWhiteInfo);
                }//数据库中不存在，新增信息
                else{
                    //生成主键
                    Map paramMap= new HashMap<>() ;
                    String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
                    whiteInfo.setPkId(pkValue);
                    //金融机构编号
                    whiteInfo.setInstuCde(userInfo.getInstuOrg().getCode());

                    //增加查询同业客户信息
                    CmisCus0010ReqDto cmisCus0010ReqDto = new CmisCus0010ReqDto();
                    cmisCus0010ReqDto.setCusId(whiteInfo.getCusId());
                    ResultDto<CmisCus0010RespDto> cmisCus0010RespDtoResultDto = cmisCusClientService.cmiscus0010(cmisCus0010ReqDto);
                    if(cmisCus0010RespDtoResultDto == null){
                        throw new BizException(null, "99999",null,  "获取同业客户信息失败！");
                    }
                    CmisCus0010RespDto cus0010RespDto = cmisCus0010RespDtoResultDto.getData();

                    String aorgNo = whiteInfo.getAorgNo();
                    String cusName = whiteInfo.getCusName();
                    String cusType = whiteInfo.getCusType();

                    if (cus0010RespDto!=null){
                        aorgNo = cus0010RespDto.getLargeBankNo();
                        cusName = cus0010RespDto.getCusName();
                        cusType = cus0010RespDto.getIntbankType();
                    }else{
                        throw new BizException(null, "9999", null, "根据客户号未获取到客户信息！");
                    }

                    whiteInfo.setAorgNo(aorgNo);
                    whiteInfo.setCusName(cusName);
                    //客户类型
                    whiteInfo.setCusType(cusType);
                    //限额（万元）转换
                    whiteInfo.setSigAmt(whiteInfo.getSigAmt().multiply(new BigDecimal(10000)));
                    //单个产品户买入返售限额
                    whiteInfo.setSingleResaleQuota(whiteInfo.getSingleResaleQuota().multiply(new BigDecimal(10000)));
                    //到期日转换  by zhangjw 20210813
                    String endDate = whiteInfo.getEndDate();
                    try{
                        endDate = LocalDate.parse(endDate,dateTimeFormatter).toString();
                    }catch (Exception e){
                        throw new BizException(null, "9999", null, "到期日格式录入错误，请使用YYYY-MM-DD格式");
                    }
                    //到期日期
                    whiteInfo.setEndDate(endDate);
                    //白名单额度类型
                    whiteInfo.setLmtWhiteType(CmisLmtConstants.STD_ZB_LMT_WHITE_TYPE_02);
                    //操作类型
                    whiteInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                    //额度分项编号
                    whiteInfo.setSubAccNo(pkValue);
                    //开始日期
                    whiteInfo.setStartDate(whiteInfo.getStartDate());
                    //已用限额
                    whiteInfo.setSigUseAmt(new BigDecimal(0));
                    //登记人
                    whiteInfo.setInputId(userInfo.getLoginCode());
                    //登记机构
                    whiteInfo.setInputBrId(userInfo.getOrg().getCode());
                    String openday = stringRedisTemplate.opsForValue().get("openDay");//当前日期
                    //登记日期
                    whiteInfo.setInputDate(openday);
                    //创建时间
                    whiteInfo.setCreateTime(new Date());
                    lmtWhiteInfoList.add(whiteInfo);
                    lmtWhiteInfoMapper.insertSelective(whiteInfo);
                }
            }
            //白名单额度推送ComStar
            sendComStar(lmtWhiteInfoList);
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perWhiteInfoList.size();
    }

    /**
     * @方法名称: selectLmtWhiteInfoBySubNo
     * @方法描述: 根据客户编号获取白名单额度信息
     * @参数与返回说明: 返回白名单信息
     * @算法描述:
     */

    public Map<String,BigDecimal> selectTotalLmtAmtByCusId(String cusId) {
        return lmtWhiteInfoMapper.selectTotalLmtAmtByCusId(cusId);
    }

    /**
     * 检查该客户号是否存在于白名单表里
     * @param cusId
     * @return
     */
    public int selectRecordsByCusId(String cusId){
        return lmtWhiteInfoMapper.selectRecordsByCusId(cusId);
    }

    /**
     * 检查该客户号是否存在于白名单表里
     * @param cusId
     * @return
     */
    public int selectResaleRecordsByCusId(String cusId){
        return lmtWhiteInfoMapper.selectResaleRecordsByCusId(cusId);
    }

    /**
     * 将白名单额度信息里的数据复制到历史表里
     * @param lmtWhiteInfo
     * @return
     */
    private LmtWhiteInfoHistory copyPropertiesFromLmtWhiteInfo(LmtWhiteInfo lmtWhiteInfo){
        LmtWhiteInfoHistory lmtWhiteInfoHistory = new LmtWhiteInfoHistory();
        //拷贝数据
        org.springframework.beans.BeanUtils.copyProperties(lmtWhiteInfo, lmtWhiteInfoHistory);
        //登记机构
        if (StringUtils.isEmpty(lmtWhiteInfo.getUpdBrId())){
            lmtWhiteInfoHistory.setInputBrId(lmtWhiteInfo.getInputBrId());
        }else {
            lmtWhiteInfoHistory.setInputBrId(lmtWhiteInfo.getUpdBrId());
        }
        //登记人
        if (StringUtils.isEmpty(lmtWhiteInfo.getUpdId())){
            lmtWhiteInfoHistory.setInputId(lmtWhiteInfo.getInputId());
        }else{
            lmtWhiteInfoHistory.setInputId(lmtWhiteInfo.getUpdId());
        }
        //登记日期
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前系统日期
        lmtWhiteInfoHistory.setInputDate(openDay);
        //创建时间
        lmtWhiteInfoHistory.setCreateTime(new Date());
        //生成主键
        Map paramMap= new HashMap<>();
        String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
        lmtWhiteInfoHistory.setPkId(pkValue);
        lmtWhiteInfoHistory.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        return lmtWhiteInfoHistory;
    }

    /**
     * 根据QueryModel查询白名单额度信息
     * @param queryModel
     * @return
     */
    public List<Map<String,Object>> selectInfoByQueryModel(QueryModel queryModel){
        return lmtWhiteInfoMapper.selectInfoByQueryModel(queryModel);
    }

    /**
     * 根据客户号更新已用限额
     * @param queryModel
     * @return
     */
    public int updateSigUseAmtByCusId(QueryModel queryModel){
        return lmtWhiteInfoMapper.updateSigUseAmtByCusId(queryModel);
    }

    /**
     * 根据额度分项编号更新已用限额
     * @param queryModel
     * @return
     */
    public int updateSigUseAmtByLimitSubNo(QueryModel queryModel){
        return lmtWhiteInfoMapper.updateSigUseAmtByLimitSubNo(queryModel);
    }

    /**
     * 根据客户号查询额度分项编号
     * @param cusId
     * @return
     */
    public String selectSubAccNoByCusId(String cusId){
        return lmtWhiteInfoMapper.selectSubAccNoByCusId(cusId);
    }


    /**
     * 根据根据承兑行行号查询白名单信息
     * @param aorgNo
     * @return
     */
    public String selectLmtWhiteInfoByAorgNo(String aorgNo){
        return lmtWhiteInfoMapper.selectLmtWhiteInfoByAorgNo(aorgNo);
    }

    /**
     * 根据根据承兑行客户号查询白名单信息
     * @param cusId
     * @return
     */
    public String selectLmtWhiteInfoByCusId(String cusId){
        return lmtWhiteInfoMapper.selectLmtWhiteInfoByCusId(cusId);
    }

    /**
     * @作者:lizx
     * @方法名称: updateByPKeyInApprLmtChgDetails
     * @方法描述:  传入原始数据，最新数据，一级相关接口信息，跟数据的同时，调用留存记录，留存数据信息appr_lmt_chg_details
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/28 19:44
     * @param lmtWhiteInfo:
     * @param paramMap:
     * @return: int
     * @算法描述: 无
     */
    public int updateByPKeyInApprLmtChgDetails(LmtWhiteInfo lmtWhiteInfo, Map<String, String> paramMap){
        return lmtWhiteInfoMapper.updateByPrimaryKey(lmtWhiteInfo);
    }

}


