package cn.com.yusys.yusp.web.server.cmislmt0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0023.req.CmisLmt0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.resp.CmisLmt0023RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0023.CmisLmt0023Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:个人额度查询（新微贷）
 *
 * @author dumd 20210526
 * @version 1.0
 */
@Api(tags = "cmislmt0023:个人额度查询（新微贷）")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0023Resource.class);

    @Autowired
    private CmisLmt0023Service cmisLmt0023Service;

    /**
     * 交易码：cmislmt0023
     * 交易描述：个人额度查询（新微贷）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("个人额度查询（新微贷）")
    @PostMapping("/cmislmt0023")
    protected @ResponseBody
    ResultDto<CmisLmt0023RespDto> cmisLmt0023(@Validated @RequestBody CmisLmt0023ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0023.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0023RespDto> cmisLmt0023RespDtoResultDto = new ResultDto<>();
        CmisLmt0023RespDto cmisLmt0023RespDto = new CmisLmt0023RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0023RespDto = cmisLmt0023Service.execute(reqDto);
            cmisLmt0023RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0023RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0023.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0023.value, e.getMessage());
            // 封装cmisLmt0023RespDtoResultDto中异常返回码和返回信息
            cmisLmt0023RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0023RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0023RespDto到cmisLmt0023RespDtoResultDto中
        cmisLmt0023RespDtoResultDto.setData(cmisLmt0023RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.value, JSON.toJSONString(cmisLmt0023RespDtoResultDto));
        return cmisLmt0023RespDtoResultDto;
    }
}