/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtSubPrdMappConf;
import cn.com.yusys.yusp.dto.CfgPrdCatalogDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfListDto;
import cn.com.yusys.yusp.service.LmtSubPrdMappConfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.management.Query;
import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtSubPrdMappConfResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-06 09:45:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsubprdmappconf")
public class LmtSubPrdMappConfResource {
    @Autowired
    private LmtSubPrdMappConfService lmtSubPrdMappConfService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSubPrdMappConf>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSubPrdMappConf> list = lmtSubPrdMappConfService.selectAll(queryModel);
        return new ResultDto<List<LmtSubPrdMappConf>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSubPrdMappConf>> index(QueryModel queryModel) {
        List<LmtSubPrdMappConf> list = lmtSubPrdMappConfService.selectByModel(queryModel);
        return new ResultDto<List<LmtSubPrdMappConf>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSubPrdMappConf> show(@PathVariable("pkId") String pkId) {
        LmtSubPrdMappConf lmtSubPrdMappConf = lmtSubPrdMappConfService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSubPrdMappConf>(lmtSubPrdMappConf);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSubPrdMappConf> create(@RequestBody LmtSubPrdMappConf lmtSubPrdMappConf) {
        lmtSubPrdMappConfService.insert(lmtSubPrdMappConf);
        return new ResultDto<LmtSubPrdMappConf>(lmtSubPrdMappConf);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSubPrdMappConf lmtSubPrdMappConf) {
        int result = lmtSubPrdMappConfService.update(lmtSubPrdMappConf);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSubPrdMappConfService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSubPrdMappConfService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 额度分项产品映射信息
     * @param cfgPrdCatalogDtos
     * @return
     */
    @PostMapping("/save/{fkPkid}")
    protected ResultDto<Integer> save(@PathVariable("fkPkid") String fkPkid,@RequestBody List<CfgPrdCatalogDto> cfgPrdCatalogDtos) {
        int result = lmtSubPrdMappConfService.save(fkPkid,cfgPrdCatalogDtos);
        return new ResultDto<Integer>(result);
    }

    /**
     *  保存额度结构树勾选的适用产品
     *  add by zhangjw 20210507
     * @param lmtSubPrdMappConfDto
     * @return
     */
    @PostMapping("/saveLmtSubPrdMappConfList")
    protected ResultDto<Integer> saveLmtSubPrdMappConfList(@RequestBody LmtSubPrdMappConfDto lmtSubPrdMappConfDto) {
        int result = lmtSubPrdMappConfService.saveLmtSubPrdMappConfList(lmtSubPrdMappConfDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据额度品种编号查询适用产品编号
     *  add by zhangjw 20210706
     * @param limitSubNo
     * @return
     */
    @PostMapping("/selectPrdIdByLimitSubNo")
    protected ResultDto<List<LmtSubPrdMappConf>> selectPrdIdByLimitSubNo(@RequestBody String limitSubNo) {
        List<LmtSubPrdMappConf>  list = lmtSubPrdMappConfService.selectPrdIdByLimitSubNo(limitSubNo);
        return new ResultDto<List<LmtSubPrdMappConf>>(list);
    }

    /**
     * 根据产品编号查询适用额度品种编号
     *  add by lihh 20210823
     * @param prdId
     * @return
     */
    @PostMapping("/selectLimitSubNoByPrdId")
    protected ResultDto<List<LmtSubPrdMappConfDto>> selectLimitSubNoByPrdId(@RequestBody String prdId) {
        List<LmtSubPrdMappConfDto>  list = lmtSubPrdMappConfService.selectLimitSubNoByPrdId(prdId);
        return new ResultDto<List<LmtSubPrdMappConfDto>>(list);
    }

}
