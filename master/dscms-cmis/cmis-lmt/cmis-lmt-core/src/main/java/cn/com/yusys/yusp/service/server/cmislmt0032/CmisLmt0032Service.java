package cn.com.yusys.yusp.service.server.cmislmt0032;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.req.CmisLmt0032ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0032.resp.CmisLmt0032RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusGrpMemberRelMapper;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0032Service
 * @类描述: #对内服务类
 * @功能描述: 查询客户授信余额(不包含穿透化额度)
 * @创建时间: 2021-06-18
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0032Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0032Service.class);

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private CusGrpMemberRelMapper cusGrpMemberRelMapper ;

    /**
     * 查询客户授信余额（不包含穿透化额度、承销额度、低风险额度）
     * add by dumd 20210617
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0032RespDto execute(CmisLmt0032ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0032.value);
        CmisLmt0032RespDto respDto = new CmisLmt0032RespDto();
        //客户号
        String cusId = reqDto.getCusId();

        try {
            /*
                是否查询集团向下 1-是 0-否
                1：根据当前客户查找是所属集团向下的所有子公司，如果有有自动存在相关授信，则返回是 否则返回否
                0：不查询集团
             */
            String isQuryGrp = reqDto.getIsQuryGrp() ;
            Map<String,BigDecimal> result = new HashMap<>();
            BigDecimal totalLmtAmt = BigDecimal.ZERO ;
            BigDecimal lmtBalanceAmt = BigDecimal.ZERO ;
            //如果查询集团向下
            if(CmisLmtConstants.YES_NO_Y.equals(isQuryGrp)) {
                //根据改客户号查询该客户集团向下的客户信息
                List<Map<String, String>> cusMemList = cusGrpMemberRelMapper.selectByCusIdQueryGrpNoCuss(cusId);
                //空处理
                if (CollectionUtils.nonEmpty(cusMemList)) {
                    //遍历查询
                    for (Map<String, String> cusMap : cusMemList) {
                        String memCusId = cusMap.get("cusId");
                        //集团下可以等于当前客户跳过此循环
                        logger.info("{}:查询集团项下不包含穿透化额度、承销额度、低风险额度{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key, memCusId);
                        result = this.calLmtBalanceAmt(memCusId,reqDto);
                        logger.info("{}:查询集团项下不包含穿透化额度、承销额度、低风险额度结果{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key, result);
                        totalLmtAmt = totalLmtAmt.add(result.get("totalLmtAmt"));
                        lmtBalanceAmt = lmtBalanceAmt.add(result.get("lmtBalanceAmt"));

                    }
                }else{
                    logger.info("{}:查询客户项下不包含穿透化额度、承销额度、低风险额度{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key, cusId);
                    result = this.calLmtBalanceAmt(cusId,reqDto);
                    logger.info("{}:查询客户项下不包含穿透化额度、承销额度、低风险额度结果{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key, result);
                    totalLmtAmt = result.get("totalLmtAmt");
                    lmtBalanceAmt = result.get("lmtBalanceAmt");
                }
            }else{
                logger.info("{}:查询客户项下不包含穿透化额度、承销额度、低风险额度{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key, cusId);
                result = this.calLmtBalanceAmt(cusId,reqDto);
                logger.info("{}:查询客户项下不包含穿透化额度、承销额度、低风险额度结果{}", DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key, result);
                totalLmtAmt = result.get("totalLmtAmt");
                lmtBalanceAmt = result.get("lmtBalanceAmt");
            }

            respDto.setLmtAmt(totalLmtAmt);
            respDto.setLmtBalanceAmt(lmtBalanceAmt);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("查询客户授信余额（不包含穿透化额度）" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0032.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0032.value);
        return respDto;
    }

    private Map<String,BigDecimal> calLmtBalanceAmt(String cusId,CmisLmt0032ReqDto reqDto) {
        String instuCde = reqDto.getInstuCde();

        String isQueryCth = reqDto.getIsQueryCth() ;//是否包含穿透化额度
        if(StringUtils.isBlank(isQueryCth)){
            isQueryCth = CmisLmtConstants.YES_NO_N;
        }

        String isQueryDfx = reqDto.getIsQueryDfx();//是否包含低风险额度
        if(StringUtils.isBlank(isQueryDfx)){
            isQueryDfx = CmisLmtConstants.YES_NO_N;
        }

        String isQueryWt = reqDto.getIsQueryWt();//是否包含委托贷款额度
        if(StringUtils.isBlank(isQueryWt)){
            isQueryWt = CmisLmtConstants.YES_NO_Y;
        }

        //查询客户（综合授信+主体授信）未到期的授信金额
        logger.info("查询客户（综合授信+主体授信）未到期的授信金额 ---------------------> START ：cusId ["+cusId+"]  instuCde["+instuCde+"]  isQueryCth["+isQueryCth+"]  isQueryDfx["+isQueryDfx+"] ");
        BigDecimal totalLmtAmt = comm4Service.selectTotalLmtAmtByCusId(cusId,instuCde,isQueryCth,isQueryDfx);
        logger.info("查询客户（综合授信+主体授信）未到期的授信金额 ---------------------> END ：totalLmtAmt ["+totalLmtAmt+"]");

        //查询客户（综合授信+主体授信）已到期的业务余额
        logger.info("查询客户（综合授信+主体授信）已到期的业务余额  ---------------------> START ：cusId ["+cusId+"]  instuCde["+instuCde+"]  isQueryCth["+isQueryCth+"]  isQueryDfx["+isQueryDfx+"] ");
        BigDecimal totalBalanceAmt = comm4Service.selectTotalBalanceAmtByCusId(cusId,instuCde,isQueryCth,isQueryDfx);
        logger.info("查询客户（综合授信+主体授信）已到期的业务余额 ---------------------> END ：totalBalanceAmt ["+totalBalanceAmt+"]");

        //如果不查询委托贷款，则将剔除  未到期的非低风险委托贷款额度 和 已到期的非低风险委托贷款余额
        BigDecimal wtTotalLmtAmt = BigDecimal.ZERO;
        BigDecimal wtTotalBalanceAmt = BigDecimal.ZERO;
        if(CmisLmtConstants.YES_NO_N.equals(isQueryWt)){
            //查询客户委托贷款 未到期的授信金额
            logger.info("查询客户 委托贷款 未到期的授信金额   ---------------------> START ：cusId ["+cusId+"]  instuCde["+instuCde+"]  isQueryDfx["+isQueryDfx+"] ");
            wtTotalLmtAmt = comm4Service.selectTotalWtLmtAmtByCusId(cusId,instuCde,isQueryDfx);
            logger.info("查询客户 委托贷款 未到期的授信金额 ---------------------> END ：wtTotalLmtAmt ["+wtTotalLmtAmt+"]");

            //查询客户委托贷款 已到期的用信余额
            logger.info("查询客户 委托贷款 已到期的用信余额   ---------------------> START ：cusId ["+cusId+"]  instuCde["+instuCde+"]  isQueryDfx["+isQueryDfx+"] ");
            wtTotalBalanceAmt = comm4Service.selectTotalWtBalanceAmtByCusId(cusId,instuCde,isQueryDfx);
            logger.info("查询客户 委托贷款 已到期的用信余额 ---------------------> END ：wtTotalBalanceAmt ["+wtTotalBalanceAmt+"]");
        }

        //客户授信余额(不包含穿透化额度) = 客户（综合授信+主体授信）未到期的授信金额 + 客户（综合授信+主体授信）已到期的业务余额
        totalLmtAmt = totalLmtAmt.subtract(wtTotalLmtAmt);
        BigDecimal lmtBalanceAmt = totalLmtAmt.add(totalBalanceAmt).subtract(wtTotalBalanceAmt).subtract(wtTotalLmtAmt);

        Map<String,BigDecimal> map = new HashMap<>();
        map.put("totalLmtAmt",totalLmtAmt);
        map.put("lmtBalanceAmt",lmtBalanceAmt);
        return map;
    }

}