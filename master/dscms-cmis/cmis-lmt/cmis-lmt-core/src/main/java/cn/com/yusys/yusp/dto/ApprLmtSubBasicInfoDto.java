package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprLmtSubBasicInfo
 * @类描述: appr_lmt_sub_basic_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 21:40:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class ApprLmtSubBasicInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;

	/** FK_PKID **/
	private String fkPkid;

	/** 批复分项编号 **/
	private String apprSubSerno;

	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 集团编号 **/
	private String grpNo;
	
	/** 客户类型 **/
	private String cusType;
	
	/** 额度分项编号 **/
	private String limitSubNo;
	
	/** 额度分项名称 **/
	private String limitSubName;
	
	/** 授信品种类型属性 **/
	private String lmtBizTypeProp;
	
	/** 父节点 **/
	private String parentId;
	
	/** 是否低风险授信 **/
	private String isLriskLmt;
	
	/** 授信分项类型 **/
	private String lmtSubType;
	
	/** 是否循环 **/
	private String isRevolv;
	
	/** 额度类型 **/
	private String limitType;
	
	/** 适用担保方式 **/
	private String suitGuarWay;
	
	/** 额度有效期 **/
	private String lmtDate;
	
	/** 额度宽限期（月) **/
	private Integer lmtGraper;
	
	/** 授信总额 **/
	private java.math.BigDecimal avlAmt;
	
	/** 已用总额 **/
	private java.math.BigDecimal outstndAmt;
	
	/** 敞口金额 **/
	private java.math.BigDecimal spacAmt;
	
	/** 敞口已用额度 **/
	private java.math.BigDecimal spacOutstndAmt;
	
	/** 用信余额 **/
	private java.math.BigDecimal loanBalance;
	
	/** 状态 **/
	private String status;
	
	/** 项目编号 **/
	private String proNo;
	
	/** 项目名称 **/
	private String proName;
	
	/** 资产编号 **/
	private String assetNo;
	
	/** 币种 **/
	private String curType;
	
	/** 保证金预留比例 **/
	private java.math.BigDecimal bailPreRate;
	
	/** 年利率 **/
	private java.math.BigDecimal rateYear;
	
	/** 授信总额累加 **/
	private java.math.BigDecimal lmtAmtAdd;
	
	/** 已出帐金额 **/
	private java.math.BigDecimal pvpOutstndAmt;
	
	/** 可出账金额 **/
	private java.math.BigDecimal avlOutstndAmt;
	
	/** 是否预授信标识 **/
	private String isPreCrd;
	
	/** 是否涉及货币基金 **/
	private String isIvlMf;
	
	/** 单只货币基金授信额度 **/
	private java.math.BigDecimal lmtSingleMfAmt;
	
	/** 起始日期 **/
	private String startDate;
	
	/** 期限 **/
	private Integer term;
	
	/** 用信敞口余额 **/
	private java.math.BigDecimal loanSpacBalance;
	
	/** 投资类型 **/
	private String investType;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param fkPkid
	 */
	public void setFkPkid(String fkPkid) {
		this.fkPkid = fkPkid == null ? null : fkPkid.trim();
	}
	
    /**
     * @return FkPkid
     */	
	public String getFkPkid() {
		return this.fkPkid;
	}
	
	/**
	 * @param apprSubSerno
	 */
	public void setApprSubSerno(String apprSubSerno) {
		this.apprSubSerno = apprSubSerno == null ? null : apprSubSerno.trim();
	}
	
    /**
     * @return ApprSubSerno
     */	
	public String getApprSubSerno() {
		return this.apprSubSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param grpNo
	 */
	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo == null ? null : grpNo.trim();
	}
	
    /**
     * @return GrpNo
     */	
	public String getGrpNo() {
		return this.grpNo;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType == null ? null : cusType.trim();
	}
	
    /**
     * @return CusType
     */	
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param limitSubNo
	 */
	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo == null ? null : limitSubNo.trim();
	}
	
    /**
     * @return LimitSubNo
     */	
	public String getLimitSubNo() {
		return this.limitSubNo;
	}
	
	/**
	 * @param limitSubName
	 */
	public void setLimitSubName(String limitSubName) {
		this.limitSubName = limitSubName == null ? null : limitSubName.trim();
	}
	
    /**
     * @return LimitSubName
     */	
	public String getLimitSubName() {
		return this.limitSubName;
	}
	
	/**
	 * @param lmtBizTypeProp
	 */
	public void setLmtBizTypeProp(String lmtBizTypeProp) {
		this.lmtBizTypeProp = lmtBizTypeProp == null ? null : lmtBizTypeProp.trim();
	}
	
    /**
     * @return LmtBizTypeProp
     */	
	public String getLmtBizTypeProp() {
		return this.lmtBizTypeProp;
	}
	
	/**
	 * @param parentId
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId == null ? null : parentId.trim();
	}
	
    /**
     * @return ParentId
     */	
	public String getParentId() {
		return this.parentId;
	}
	
	/**
	 * @param isLriskLmt
	 */
	public void setIsLriskLmt(String isLriskLmt) {
		this.isLriskLmt = isLriskLmt == null ? null : isLriskLmt.trim();
	}
	
    /**
     * @return IsLriskLmt
     */	
	public String getIsLriskLmt() {
		return this.isLriskLmt;
	}
	
	/**
	 * @param lmtSubType
	 */
	public void setLmtSubType(String lmtSubType) {
		this.lmtSubType = lmtSubType == null ? null : lmtSubType.trim();
	}
	
    /**
     * @return LmtSubType
     */	
	public String getLmtSubType() {
		return this.lmtSubType;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv == null ? null : isRevolv.trim();
	}
	
    /**
     * @return IsRevolv
     */	
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param limitType
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType == null ? null : limitType.trim();
	}
	
    /**
     * @return LimitType
     */	
	public String getLimitType() {
		return this.limitType;
	}
	
	/**
	 * @param suitGuarWay
	 */
	public void setSuitGuarWay(String suitGuarWay) {
		this.suitGuarWay = suitGuarWay == null ? null : suitGuarWay.trim();
	}
	
    /**
     * @return SuitGuarWay
     */	
	public String getSuitGuarWay() {
		return this.suitGuarWay;
	}
	
	/**
	 * @param lmtDate
	 */
	public void setLmtDate(String lmtDate) {
		this.lmtDate = lmtDate == null ? null : lmtDate.trim();
	}
	
    /**
     * @return LmtDate
     */	
	public String getLmtDate() {
		return this.lmtDate;
	}
	
	/**
	 * @param lmtGraper
	 */
	public void setLmtGraper(Integer lmtGraper) {
		this.lmtGraper = lmtGraper;
	}
	
    /**
     * @return LmtGraper
     */	
	public Integer getLmtGraper() {
		return this.lmtGraper;
	}
	
	/**
	 * @param avlAmt
	 */
	public void setAvlAmt(java.math.BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}
	
    /**
     * @return AvlAmt
     */	
	public java.math.BigDecimal getAvlAmt() {
		return this.avlAmt;
	}
	
	/**
	 * @param outstndAmt
	 */
	public void setOutstndAmt(java.math.BigDecimal outstndAmt) {
		this.outstndAmt = outstndAmt;
	}
	
    /**
     * @return OutstndAmt
     */	
	public java.math.BigDecimal getOutstndAmt() {
		return this.outstndAmt;
	}
	
	/**
	 * @param spacAmt
	 */
	public void setSpacAmt(java.math.BigDecimal spacAmt) {
		this.spacAmt = spacAmt;
	}
	
    /**
     * @return SpacAmt
     */	
	public java.math.BigDecimal getSpacAmt() {
		return this.spacAmt;
	}
	
	/**
	 * @param spacOutstndAmt
	 */
	public void setSpacOutstndAmt(java.math.BigDecimal spacOutstndAmt) {
		this.spacOutstndAmt = spacOutstndAmt;
	}
	
    /**
     * @return SpacOutstndAmt
     */	
	public java.math.BigDecimal getSpacOutstndAmt() {
		return this.spacOutstndAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return LoanBalance
     */	
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}
	
    /**
     * @return ProNo
     */	
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName == null ? null : proName.trim();
	}
	
    /**
     * @return ProName
     */	
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param assetNo
	 */
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo == null ? null : assetNo.trim();
	}
	
    /**
     * @return AssetNo
     */	
	public String getAssetNo() {
		return this.assetNo;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param bailPreRate
	 */
	public void setBailPreRate(java.math.BigDecimal bailPreRate) {
		this.bailPreRate = bailPreRate;
	}
	
    /**
     * @return BailPreRate
     */	
	public java.math.BigDecimal getBailPreRate() {
		return this.bailPreRate;
	}
	
	/**
	 * @param rateYear
	 */
	public void setRateYear(java.math.BigDecimal rateYear) {
		this.rateYear = rateYear;
	}
	
    /**
     * @return RateYear
     */	
	public java.math.BigDecimal getRateYear() {
		return this.rateYear;
	}
	
	/**
	 * @param lmtAmtAdd
	 */
	public void setLmtAmtAdd(java.math.BigDecimal lmtAmtAdd) {
		this.lmtAmtAdd = lmtAmtAdd;
	}
	
    /**
     * @return LmtAmtAdd
     */	
	public java.math.BigDecimal getLmtAmtAdd() {
		return this.lmtAmtAdd;
	}
	
	/**
	 * @param pvpOutstndAmt
	 */
	public void setPvpOutstndAmt(java.math.BigDecimal pvpOutstndAmt) {
		this.pvpOutstndAmt = pvpOutstndAmt;
	}
	
    /**
     * @return PvpOutstndAmt
     */	
	public java.math.BigDecimal getPvpOutstndAmt() {
		return this.pvpOutstndAmt;
	}
	
	/**
	 * @param avlOutstndAmt
	 */
	public void setAvlOutstndAmt(java.math.BigDecimal avlOutstndAmt) {
		this.avlOutstndAmt = avlOutstndAmt;
	}
	
    /**
     * @return AvlOutstndAmt
     */	
	public java.math.BigDecimal getAvlOutstndAmt() {
		return this.avlOutstndAmt;
	}
	
	/**
	 * @param isPreCrd
	 */
	public void setIsPreCrd(String isPreCrd) {
		this.isPreCrd = isPreCrd == null ? null : isPreCrd.trim();
	}
	
    /**
     * @return IsPreCrd
     */	
	public String getIsPreCrd() {
		return this.isPreCrd;
	}
	
	/**
	 * @param isIvlMf
	 */
	public void setIsIvlMf(String isIvlMf) {
		this.isIvlMf = isIvlMf == null ? null : isIvlMf.trim();
	}
	
    /**
     * @return IsIvlMf
     */	
	public String getIsIvlMf() {
		return this.isIvlMf;
	}
	
	/**
	 * @param lmtSingleMfAmt
	 */
	public void setLmtSingleMfAmt(java.math.BigDecimal lmtSingleMfAmt) {
		this.lmtSingleMfAmt = lmtSingleMfAmt;
	}
	
    /**
     * @return LmtSingleMfAmt
     */	
	public java.math.BigDecimal getLmtSingleMfAmt() {
		return this.lmtSingleMfAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(Integer term) {
		this.term = term;
	}
	
    /**
     * @return Term
     */	
	public Integer getTerm() {
		return this.term;
	}
	
	/**
	 * @param loanSpacBalance
	 */
	public void setLoanSpacBalance(java.math.BigDecimal loanSpacBalance) {
		this.loanSpacBalance = loanSpacBalance;
	}
	
    /**
     * @return LoanSpacBalance
     */	
	public java.math.BigDecimal getLoanSpacBalance() {
		return this.loanSpacBalance;
	}
	
	/**
	 * @param investType
	 */
	public void setInvestType(String investType) {
		this.investType = investType == null ? null : investType.trim();
	}
	
    /**
     * @return InvestType
     */	
	public String getInvestType() {
		return this.investType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}