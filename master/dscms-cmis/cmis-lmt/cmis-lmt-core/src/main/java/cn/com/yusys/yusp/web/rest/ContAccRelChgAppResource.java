/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ContAccRelChgApp;
import cn.com.yusys.yusp.service.ContAccRelChgAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ContAccRelChgAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-20 20:39:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/contaccrelchgapp")
public class ContAccRelChgAppResource {
    @Autowired
    private ContAccRelChgAppService contAccRelChgAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ContAccRelChgApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<ContAccRelChgApp> list = contAccRelChgAppService.selectAll(queryModel);
        return new ResultDto<List<ContAccRelChgApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ContAccRelChgApp>> index(QueryModel queryModel) {
        List<ContAccRelChgApp> list = contAccRelChgAppService.selectByModel(queryModel);
        return new ResultDto<List<ContAccRelChgApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<ContAccRelChgApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<ContAccRelChgApp> list = contAccRelChgAppService.selectByModel(queryModel);
        return new ResultDto<List<ContAccRelChgApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ContAccRelChgApp> show(@PathVariable("pkId") String pkId) {
        ContAccRelChgApp contAccRelChgApp = contAccRelChgAppService.selectByPrimaryKey(pkId);
        return new ResultDto<ContAccRelChgApp>(contAccRelChgApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ContAccRelChgApp> create(@RequestBody ContAccRelChgApp contAccRelChgApp) {
        contAccRelChgAppService.insert(contAccRelChgApp);
        return new ResultDto<ContAccRelChgApp>(contAccRelChgApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ContAccRelChgApp contAccRelChgApp) {
        int result = contAccRelChgAppService.update(contAccRelChgApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = contAccRelChgAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = contAccRelChgAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete/{pkId}")
    protected ResultDto<Integer> logicalDelete(@PathVariable("pkId") String pkId ) {
        int result = contAccRelChgAppService.logicDelete(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:
     * @函数描述:检查合同编号在系统中是否存在（额度调整校验）
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkSameDealBizNoIsExistAll")
    protected ResultDto<Integer> checkSameDealBizNoIsExistAll(@RequestBody QueryModel queryModel) {
        String dealBizNo = (String) queryModel.getCondition().get("dealBizNo");
        int result = contAccRelChgAppService.checkSameDealBizNoIsExistAll(dealBizNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:
     * @函数描述:检查台账编号在系统中是否存在（额度调整校验）
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkSameTranAccNoIsExistAll")
    protected ResultDto<Integer> checkSameTranAccNoIsExistAll(@RequestBody QueryModel queryModel) {
        String tranAccNo = (String) queryModel.getCondition().get("tranAccNo");
        int result = contAccRelChgAppService.checkSameTranAccNoIsExistAll(tranAccNo);
        return new ResultDto<Integer>(result);
    }
}
