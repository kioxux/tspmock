/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.ApprLmtSubChgApp;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubChgAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprLmtSubChgAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-20 20:39:45
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ApprLmtSubChgAppService {

    @Autowired
    private ApprLmtSubChgAppMapper apprLmtSubChgAppMapper;
    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ApprLmtSubChgApp selectByPrimaryKey(String pkId) {
        return apprLmtSubChgAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<ApprLmtSubChgApp> selectAll(QueryModel model) {
        List<ApprLmtSubChgApp> records = (List<ApprLmtSubChgApp>) apprLmtSubChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<ApprLmtSubChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ApprLmtSubChgApp> list = apprLmtSubChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(ApprLmtSubChgApp record) {
        return apprLmtSubChgAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(ApprLmtSubChgApp record) {
        return apprLmtSubChgAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(ApprLmtSubChgApp record) {
        return apprLmtSubChgAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(ApprLmtSubChgApp record) {
        return apprLmtSubChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return apprLmtSubChgAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return apprLmtSubChgAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: createAppLmtSubChaApp
     * @方法描述: 创建额度调整分项申请数据
     * @参数与返回说明:【apprSerno】批复台账编号
     * @算法描述: 无
     */
    public List<ApprLmtSubChgApp> createAppLmtSubChgApp(String apprSerno) {
        List<ApprLmtSubChgApp> apprLmtSubChgApps = new ArrayList<>();
        //根据批复编号和操作类型查找批复编号下所有的批复信息
        QueryModel model = new QueryModel();
        model.addCondition("fkPkid", apprSerno);
        model.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        List<ApprLmtSubBasicInfo> apprLmtSubBasicInfoList = apprLmtSubBasicInfoService.selectAll(model);
        //生成主键，获取流水号
        HashMap<String, String> param = new HashMap<>();
        Supplier<ApprLmtSubChgApp> supplier = ApprLmtSubChgApp::new;
        //判断获取的分项记录不为空，遍历处理
        if (apprLmtSubBasicInfoList.size() > 0) {
            for (ApprLmtSubBasicInfo apprLmtSubBasicInfo : apprLmtSubBasicInfoList) {
                //获取初始化对象
                ApprLmtSubChgApp apprLmtSubChgApp = supplier.get();
                BeanUtils.copyProperties(apprLmtSubBasicInfo, apprLmtSubChgApp);
                //生成主键，获取流水号
                String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, param);
                //生成流水号
                String appSerno = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.LMT_SERNO, param);
                //分项申请编号
                apprLmtSubChgApp.setSubAppSerno(appSerno);
                //主键
                apprLmtSubChgApp.setPkId(pkValue);
                //TODO 待补充 FK_PKID zhangjw

                apprLmtSubChgApps.add(apprLmtSubChgApp);
            }
        }
        return apprLmtSubChgApps;
    }

    /**
     * @param
     * @函数名称:querySubChgAppBySerno
     * @函数描述:根据调整申请流水号查询一级分项列表
     * @参数与返回说明:
     * @算法描述:
     */
    public List<Map<String, Object>> querySubChgAppBySerno(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = apprLmtSubChgAppMapper.querySubChgAppBySerno(model);
        if (list.size() > 0) {
            for (Map<String, Object> map : list) {
                // 支持树状结构
                map.put("hasChildren", true);
            }
        }
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param
     * @函数名称:querySubDetailsChgAppBySerno
     * @函数描述:根据一级批复分项编号和调整申请流水号查询二级分项列表
     * @参数与返回说明:
     * @算法描述:
     */
    public List<Map<String, Object>> querySubDetailsChgAppBySerno(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = apprLmtSubChgAppMapper.querySubDetailsChgAppBySerno(model);
        PageHelper.clearPage();
        return list;
    }

}
