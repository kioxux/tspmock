package cn.com.yusys.yusp.service.server.cmislmt0020;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtWhiteInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0020.req.CmisLmt0020ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0020.resp.CmisLmt0020LmtListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0020.resp.CmisLmt0020RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtWhiteInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0020Service
 * @类描述: #对内服务类
 * @功能描述: 承兑行白名单额度查询
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0020Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0020Service.class);

    @Autowired
    private LmtWhiteInfoMapper lmtWhiteInfoMapper;

    @Transactional
    public CmisLmt0020RespDto execute(CmisLmt0020ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0020.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0020.value);
        CmisLmt0020RespDto resqDto = new CmisLmt0020RespDto();
        List<CmisLmt0020LmtListRespDto> lmtList = new ArrayList<>();
        try {

            //金融机构号
            String instuCde = reqDto.getInstuCde();
            //客户编号
            String cusId = reqDto.getCusId();

            QueryModel model = new QueryModel();
            model.addCondition("instuCde",instuCde);
            model.addCondition("cusId",cusId);

            List<LmtWhiteInfo> lmtWhiteInfos = lmtWhiteInfoMapper.selectByModel(model);

            for(int i=0;i<lmtWhiteInfos.size();i++){
                CmisLmt0020LmtListRespDto cmisLmt0020LmtListRespDto = new CmisLmt0020LmtListRespDto();

                LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfos.get(i);

                cmisLmt0020LmtListRespDto.setSubAccNo(lmtWhiteInfo.getSubAccNo());
                cmisLmt0020LmtListRespDto.setCusId(lmtWhiteInfo.getCusId());
                cmisLmt0020LmtListRespDto.setCusName(lmtWhiteInfo.getCusName());
                cmisLmt0020LmtListRespDto.setSigAmt(lmtWhiteInfo.getSigAmt());
                cmisLmt0020LmtListRespDto.setSigUseAmt(lmtWhiteInfo.getSigUseAmt());
                cmisLmt0020LmtListRespDto.setSigValAmt(lmtWhiteInfo.getSigAmt().subtract(lmtWhiteInfo.getSigUseAmt()));
                cmisLmt0020LmtListRespDto.setStartDate(lmtWhiteInfo.getStartDate());
                cmisLmt0020LmtListRespDto.setEndDate(lmtWhiteInfo.getEndDate());

                lmtList.add(cmisLmt0020LmtListRespDto);
            }

            resqDto.setLmtList(lmtList);
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("承兑行白名单额度查询：", e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0020.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0020.value);
        return resqDto;
    }

}