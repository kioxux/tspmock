package cn.com.yusys.yusp.service.server.cmislmt0009;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0005.req.CmisCus0005ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0005.resp.CmisCus0005RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0009Service
 * @类描述: #对内服务类
 * @功能描述: 额度占用前校验，
 * @创建人: 李召星
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0009Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0009Service.class);

    @Autowired
    private LmtExptListInfoService lmtExptListInfoService ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService ;

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private CmisCusClientService cmisCusClientService ;

    @Autowired
    private LmtContRelService lmtContRelService ;

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService ;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Transactional
    public CmisLmt0009RespDto execute(CmisLmt0009ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value);
        // 处理业务逻辑
        CmisLmt0009RespDto resqDto = new CmisLmt0009RespDto() ;
        try {
            //查看客户是否额度调整白名单客户 根据业务编号，查看该客户是否白名单客户
            LmtExptListInfo lmtExptListInfo = lmtExptListInfoService.selectLmtExptListInfoByBussNo(reqDto.getDealBizNo()) ;
            //白名单类外类型 01-忽略冻结状态	02-允许突破额度	03-允许突破限额	04-不占用额度（不占额，不校验）
            String exptType = "";
            //如果额度调整白名单非空，则获取例外类型
            if(lmtExptListInfo!=null) exptType = lmtExptListInfo.getExptType() ;
            //如果额度例外类型为空，则赋值为空字符，防止空指针
            if (exptType == null) exptType = "";
            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value+" 例外类型为："+exptType);

            ifNotFiledCheck(reqDto) ;
            //交易流水号
            String dealBizNo = reqDto.getDealBizNo() ;
            //配置交易流水号
            String cfgDealBizNo = comm4Service.getSysParameterByName(CmisLmtConstants.DEAL_BIZ_NO) ;
            //04-不占用额度（不占额，不校验） 直接返回成功；额外类型存储方式为 【01，02，03】，所以此处更改为包含方式 TODO:额度占用是否考虑下
            if(exptType.contains(CmisLmtConstants.EXPT_TYPE_04) || dealBizNo.equals(cfgDealBizNo)){//CmisLmtConstants.EXPT_TYPE_04.equals(exptType)
                logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key+
                        " 例外类型中包含{}【04-不占用额度】 或配置额度流水【{}】与改交易相同额度, 校验通过", exptType, cfgDealBizNo);
                resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
                resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
                return resqDto;
            }

            //校验占用列表不允许为空
            List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtos = reqDto.getCmisLmt0009OccRelListReqDtoList() ;
            //到期日
            String endDate = reqDto.getEndDate() ;
            //遍历分项集合，校验额度占用分项是否通过
            for(CmisLmt0009OccRelListReqDto occSubRel : cmisLmt0009OccRelListReqDtos){
                //额度类型
                String lmtType = occSubRel.getLmtType();
                //额度分项编号
                String lmtSubNo = occSubRel.getLmtSubNo();
                if(StringUtils.isEmpty(lmtSubNo) && CmisLmtConstants.YES_NO_N.equals(reqDto.getIsLriskBiz())){
                    resqDto.setErrorCode(EclEnum.ECL070126.key);
                    resqDto.setErrorMsg("占用列表中，"+EclEnum.ECL070126.value);
                    return resqDto;
                }
                //授信分项类型 如果是白名单客户额度，查询白名单列表中的到期日以及额度信息
                if(CmisLmtConstants.STD_ZB_LMT_TYPE_06.equals(lmtType)){
                    //白名单额度校验
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key + ":根据额度分项编号【{}】进行白名单额度校验开始", lmtSubNo);
                    checkLmtWhiteInfo(occSubRel, endDate) ;
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key + ":根据额度分项编号【"+lmtSubNo+"】进行白名单额度校验结束");
                }else if(CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType)){
                    //合作方校验
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key + ":根据额度分项编号【"+lmtSubNo+"】进行合作方额度校验开始");
                    comm4Service.validCoopLmtType(occSubRel, exptType, reqDto) ;
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key + ":根据额度分项编号【"+lmtSubNo+"】进行合作方额度校验结束");
                }else if(CmisLmtConstants.STD_ZB_LMT_TYPE_07.equals(lmtType)) {
                    //同业客户额度校验客户合同到期日是否在额度到期日前，额度是足够，状态是否正确
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key + ":校验同业额度分项基础信息额度-------->start");
                    checkApprLmtSigInfo(occSubRel, reqDto, exptType) ;
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key + ":校验同业额度分项基础信息额度-------->end");
                }else{
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key + ":根据额度分项编号【"+lmtSubNo+"】进行单一额度校验开始");
                    checkApprLmtSubBasicInfo(occSubRel, reqDto, exptType);
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key + ":根据额度分项编号【"+lmtSubNo+"】进行单一额度校验结束");
                }
            };
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("CmisLmt0009:额度校验接口报错：", e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value);
        return resqDto;
    }

    /**
     * @方法名称: validLmtSubStatus
     * @方法描述: 校验额度分项状态，是否为生效状态 （额度调整白名单01-冻结忽略例外）
     * @参数与返回说明: 校验不通过，报错到上级处理
     * @算法描述: 无
     */
    public void validLmtSubStatus(String status, String finalExptType){
        /******************校验额度分项必须是生效状态，白名单例外类型分情况处理***start***********************/
        //如果额度不为生效
        logger.info("cmislmt0009 校验额度分项状态开始");
        if(!CmisLmtConstants.STD_ZB_APPR_ST_01.equals(status)){
            //如果分项为冻结、额外类型为 01-忽略冻结状态
            if(!(finalExptType.contains(CmisLmtConstants.EXPT_TYPE_01)&&CmisLmtConstants.STD_ZB_APPR_ST_11.equals(status))){//CmisLmtConstants.EXPT_TYPE_01.equals(finalExptType)
                throw new YuspException(EclEnum.ECL070003.key, EclEnum.ECL070003.value);
            }
        }
        logger.info("cmislmt0009 校验额度分项状态结束");
    }

    /**
     * 白名单额度校验
     * 1.主要额度占用对应的白名单额度是否存在
     * 2.本次占用额度+已用限额是否大于白名单限额
     * 3.到日期是否大于白名单到期日
     * @param occSubRel 额度加盐分项信息
     * @param endDate   到日期
     */
    public void checkLmtWhiteInfo(CmisLmt0009OccRelListReqDto occSubRel, String endDate){
        logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value+":" +
                " 校验白名单额度开始。");
        String lmtSubNo = occSubRel.getLmtSubNo() ;
        //占用敞口
        BigDecimal bizSpacAmt = occSubRel.getBizSpacAmt();
        //查询白名单额度信息
        LmtWhiteInfo lmtWhiteInfo = lmtWhiteInfoService.selectLmtWhiteInfoBySubNo(lmtSubNo) ;
        Optional.ofNullable(lmtWhiteInfo).orElseThrow(()->new YuspException(EclEnum.ECL070009.key,EclEnum.ECL070009.value));
        //获取限额信息
        BigDecimal sigAmt = BigDecimalUtil.replaceNull(lmtWhiteInfo.getSigAmt()) ;
        //已用限额
        BigDecimal sigUseAmt = BigDecimalUtil.replaceNull(lmtWhiteInfo.getSigUseAmt()) ;
        //校验本次是否超过限额
        if((bizSpacAmt.add(sigUseAmt)).compareTo(sigAmt)>0){
            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key+":"+DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value+":" +
                    "占用敞口金额【bizSpacAmt】："+bizSpacAmt+"+已用限额【sigUseAmt】"+sigUseAmt+"超过限额金额【sigAmt】："+sigAmt);
            throw new YuspException(EclEnum.ECL070010.key, EclEnum.ECL070010.value);
        }
        //校验到期日
        if (StringUtils.isEmpty(lmtWhiteInfo.getEndDate())){
            logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】的到期日为空");
            throw new YuspException(EclEnum.ECL070104.key, EclEnum.ECL070104.value);
        }

        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        logger.info("签订日期{}, 白名单到期日{}", openDay, lmtWhiteInfo.getEndDate());
        boolean dateFlag = comm4Service.checkLmtEndDate(openDay, lmtWhiteInfo.getEndDate()) ;
        if(dateFlag){
            throw new YuspException(EclEnum.ECL070145.key, EclEnum.ECL070145.value);
        }
    }

    /**
     * @作者:lizx
     * @方法名称: checkApprLmtSigInfo
     * @方法描述:  同业客户额度校验
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/8/24 9:58
     * @param occSubRel:
     * @param reqDto:
     * @param exptType:
     * @return: void
     * @算法描述: 无
    */
    public void checkApprLmtSigInfo(CmisLmt0009OccRelListReqDto occSubRel, CmisLmt0009ReqDto reqDto, String exptType){
        logger.info("同业客户校验开始--------start");
        //交易属性 1-合同 2-台账
        String bizAttr = reqDto.getBizAttr() ;
        //分项编号
        String apprSubSerno = occSubRel.getLmtSubNo() ;
        //占用总额
        BigDecimal bizTotalAmt = BigDecimalUtil.replaceNull(occSubRel.getBizTotalAmt());
        //占用敞口
        BigDecimal bizSpacAmt = BigDecimalUtil.replaceNull(occSubRel.getBizSpacAmt());
        //根据分项编号，获取分项信息
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(apprSubSerno) ;
        //检验 分项批复信息不允许未空
        Optional.ofNullable(apprLmtSubBasicInfo).orElseThrow(()->new YuspException(EclEnum.ECL070007.key, EclEnum.ECL070007.value)) ;
        logger.info("打印分项额度信息:{}", JSON.toJSONString(apprLmtSubBasicInfo));
        //额度状态
        String status = apprLmtSubBasicInfo.getStatus() ;
        //到期日期
        String endDate = apprLmtSubBasicInfo.getLmtDate() ;
        //可出账金额
        BigDecimal avlOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlOutstndAmt()) ;

        //STD_ZB_SUIT_BIZ_LINE_04 业务条线
        String belgLine = reqDto.getBelgLine();
        //校验额度分项必须是生效状态，白名单例外类型分情况处理
        validLmtSubStatus(status, exptType) ;
        String prdId= reqDto.getPrdId() ;
        String cfgPrdId = comm4Service.getSysParameterByName(CmisLmtConstants.PRD_ID_CONT_UNCHECK) ;
        //最高额合同和白名单额度不校验到期日问题 update by 20210714 最高额协议不校验到期日
        if(!(CmisLmtConstants.STD_ZB_CONT_TYPE_2.equals(reqDto.getDealBizType())
                || CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(reqDto.getDealBizType())
                || CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_04.equals(belgLine)
                || comm4Service.checkStrInSourceStr(cfgPrdId, prdId)
        )){
            //一般合同的到期日不能超过额度分项到期日+宽限期 获取分项到期日以及宽限起
            if(CmisLmtConstants.STD_ZB_CONT_TYPE_1.equals(reqDto.getDealBizType()) && StringUtils.isNotBlank(reqDto.getEndDate())){
                //不控制最高额合同的到期日，控制最高额合同项下台账的到期日≤额度到期日+宽限期
                //校验到期日
                if (StringUtils.isEmpty(endDate)){
                    logger.info("cmislmt0009 额度分项编号【"+apprSubSerno+"】的到期日为空");
                    throw new YuspException(EclEnum.ECL070104.key, EclEnum.ECL070104.value);
                }

                //非 null 处理
                Integer lmtGraper = apprLmtSubBasicInfo.getLmtGraper() ;
                if(lmtGraper == null) lmtGraper = 0 ;
                //获取到期日+ 宽限期 后的日期
                endDate = comm4Service.getEndDateAddGaraperDate(endDate, lmtGraper.intValue());
            }
            //校验到期日  add by zhangjw 20210713 只有到期日不为空时，校验到期日
            if(StringUtils.isNotBlank(reqDto.getEndDate())){
                boolean dateFlag = comm4Service.checkLmtEndDate(reqDto.getEndDate(), endDate) ;
                if(dateFlag){
                    logger.info("cmislmt0009 合同到期日（即请求里的enDate）"+reqDto.getEndDate()+"超过了额度分项编号【"+apprSubSerno+"】 的到期日加宽限期："+endDate);
                    throw new YuspException(EclEnum.ECL070004.key, EclEnum.ECL070004.value);
                }
            }
        }

        //分项中 授信品种类型属性
        String subLmtBizTypeProp = apprLmtSubBasicInfo.getLmtBizTypeProp() ;
        //请求报文中
        String reqLmtBizTypeProp = occSubRel.getPrdTypeProp() ;
        if(!comm4Service.IsEqualsTwoStr(subLmtBizTypeProp, reqLmtBizTypeProp)){
            logger.info("【{}】检验授信品种类型属性不相等，分项中属性值【{}】，接口中属性值【{}】", DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key,
                    subLmtBizTypeProp, reqLmtBizTypeProp);
            throw new YuspException(EclEnum.ECL070086.key, EclEnum.ECL070086.value);
        }

        //额度为预授信额度时不允许占用
        if(CmisLmtConstants.YES_NO_Y.equals(apprLmtSubBasicInfo.getIsPreCrd())){
            logger.info("cmislmt0009 额度分项编号【"+apprSubSerno+"】是预授信额度");
            throw new YuspException(EclEnum.ECL070005.key, EclEnum.ECL070005.value);
        }

        if(StringUtils.isNotEmpty(exptType) && exptType.contains(CmisLmtConstants.EXPT_TYPE_02)){
            logger.info("cmislmt0009 分项允许超出限额，此处不进行额度校验操作");
            return;
        }

        /*******************额度校验***********************/
        //校验分项信息
        logger.info("cmislmt0009 额度分项编号【"+apprSubSerno+"】，校验分项信息开始");
        //分项已用敞口金额
        BigDecimal spacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt());
        BigDecimal spacAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacAmt()) ;
        //是否无缝衔接
        String isFollowBiz = reqDto.getIsFollowBiz() ;
        //原交易业务编号
        String origiDealBizNo = reqDto.getOrigiDealBizNo() ;
        //交易编号
        String dealBizNo = reqDto.getDealBizNo() ;
        //系统编号
        String sysId = reqDto.getSysId() ;

        //判断该交易是否已经成功
        if(comm4Service.judgeLmtDealIsSuss(apprSubSerno, dealBizNo, sysId)){
            //判断该交易是否已经成功，如果已经成功，进行粗呢撤销操作
            //获取原交易合同信息
            LmtContRel lmtContRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(dealBizNo, apprSubSerno, sysId) ;
            if(lmtContRel !=null){
                //分项敞口已用余额 = 原交易敞口余额+分项敞口可用余额
                spacOutstndAmt = spacOutstndAmt.subtract(lmtContRel.getBizSpacBalanceAmtCny()) ;
                logger.info("额度分项编号【"+apprSubSerno+"】,无缝衔接，撤销恢复金额:"+lmtContRel.getBizSpacBalanceAmtCny()+";分项敞口已用金额【spacOutstndAmt】:"+spacOutstndAmt);
                if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                    avlOutstndAmt = avlOutstndAmt.add(BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny())) ;
                }
            }
        }

        //是无缝衔接，判断原交易业务恢复类型，撤销恢复，恢复分项金额；如果是结清恢复判断分项是否可循环，恢复原交易合同余额。不可循环不回复分项。
        if(CmisLmtConstants.YES_NO_Y.equals(isFollowBiz) && CmisLmtConstants.YES_NO_Y.equals(apprLmtSubBasicInfo.getIsRevolv())){
            //获取原交易合同信息
            LmtContRel lmtContRel = lmtContRelService.selectLCRelByDBizNoAngSubNo(origiDealBizNo, apprSubSerno, sysId) ;
            if(lmtContRel !=null){
                //分项敞口已用余额 = 原交易敞口余额+分项敞口可用余额  TODO:这里是不是应该减啊？
                spacOutstndAmt = spacOutstndAmt.subtract(lmtContRel.getBizSpacBalanceAmtCny()) ;
                logger.info("额度分项编号【"+apprSubSerno+"】,无缝衔接，撤销恢复金额:"+lmtContRel.getBizSpacBalanceAmtCny()+";分项敞口已用金额【spacOutstndAmt】:"+spacOutstndAmt);
                if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
                    avlOutstndAmt = avlOutstndAmt.add(BigDecimalUtil.replaceNull(lmtContRel.getBizTotalBalanceAmtCny())) ;
                }
            }
        }

        /**交易属性 1-合同 2-台账
         * 此接口调用一般是 合同占用分项信息，但是针对代开信用证和代开保函的业务，是只会存在台账占用分项信息，缺少合同环节，所以此处送台账时
         * 需考虑合同占用时的相关信息
         * 如果是台账占用，校验本次占用金额<= 可出账金额
         */
        if(CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(bizAttr)){
            if(avlOutstndAmt.compareTo(bizTotalAmt)<0){
                logger.info("cmislmt0009 额度分项编号【"+apprSubSerno+"】，可出账金额【avlOutstndAmt】:"+avlOutstndAmt+"小于本次占用金额【bizTotalAmt】:"+bizTotalAmt);
                throw new YuspException(EclEnum.ECL070071.key, EclEnum.ECL070071.value);
            }
        }

        if ((spacOutstndAmt.add(bizSpacAmt).compareTo(spacAmt) > 0)) {
            logger.info("额度分项编号【"+apprSubSerno+"】,分项敞口已用金额【spacOutstndAmt】:"+spacOutstndAmt+"+;本次占用敞口金额【bizSpacAmtCny】:"+bizSpacAmt+"大于敞口金额【spacAmt】"+spacAmt);
            throw new YuspException(EclEnum.ECL070006.key, EclEnum.ECL070006.value);
        }
        logger.info("cmislmt0009 额度分项编号【"+apprSubSerno+"】，校验分项信息结束");
    }

    /**
     * 单一法人额度校验
     * @param occSubRel
     * @param reqDto
     * @param exptType
     */
    public void checkApprLmtSubBasicInfo(CmisLmt0009OccRelListReqDto occSubRel, CmisLmt0009ReqDto reqDto, String exptType){
        logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key+":校验批复额度分项基础信息额度-------->start");
        //额度类型
        String lmtType = occSubRel.getLmtType();
        //占用总额
        BigDecimal bizTotalAmt = BigDecimalUtil.replaceNull(occSubRel.getBizTotalAmt());
        //占用敞口
        BigDecimal bizSpacAmt = BigDecimalUtil.replaceNull(occSubRel.getBizSpacAmt());
        //分项编号
        String lmtSubNo = occSubRel.getLmtSubNo() ;

        /**
         * add by 2021-08-30 针对反向生成的低风险，如果是低风险业务，占用额度列表中占用额度类型为：01-单一客户额度，
         * 允许额度分项编号为空，为空的情况下，不校验额度分项是否足额，只判断客户总额是否超N倍，且客户存在有效的单一客户授信
         */
        String isLriskBiz = reqDto.getIsLriskBiz() ;
        if(CmisLmtConstants.YES_NO_Y.equals(isLriskBiz) && StringUtils.isBlank(lmtSubNo)){
            String cusId = reqDto.getCusId() ;
            String instuCde = reqDto.getInstuCde() ;
            //校验是否存在有有效的单一综合授信
            QueryModel queryModel = new QueryModel() ;
            queryModel.addCondition("cusId", cusId);
            queryModel.addCondition("instuCde", instuCde);
            queryModel.addCondition("optType", CmisLmtConstants.OPR_TYPE_ADD);
            queryModel.addCondition("apprStatus", CmisLmtConstants.STD_ZB_APPR_ST_01);
            queryModel.addCondition("lmtType", CmisLmtConstants.STD_ZB_LMT_TYPE_01);
            List<ApprStrMtableInfo> apprStrMtableInfoList = apprStrMtableInfoService.selectByModel(queryModel) ;
            if(CollectionUtils.isEmpty(apprStrMtableInfoList)){
                throw new YuspException(EclEnum.ECL070136.key, EclEnum.ECL070136.value);
            }

            /**
             * 法人客户进行校验
             * 规则：客户批复累加授信总额+授信总额部分，不允许超过客户上年度销售总额和当年销售总额较大值（纳税申报表）的N倍，
             * 此处【补充保证金释放金额倍数】作为额度系统参数设置，初始值为【2】  客户销售总额查询  FY_BAIL_REL_RATE
             */
            String checkTotalSales = comm4Service.getSysParameterByName(CmisLmtConstants.CHECK_TOTAL_SALES) ;
            //是否校验授信总额参数配置  1-校验 0-不校验
            if(CmisLmtConstants.YES_NO_Y.equals(checkTotalSales)){
                if(CmisLmtConstants.STD_ZB_CUS_TYPE_2.equals(apprStrMtableInfoList.get(0).getCusType())){
                    BigDecimal bailRate = BigDecimalUtil.replaceNull(comm4Service.getSysParameterByName(CmisLmtConstants.FY_BAIL_REL_RATE)) ;
                    BigDecimal statYearAmtMax = comm4Service.getStatYearAmtMax(reqDto.getCusId()) ;

                    Map<String, BigDecimal> amtMap = apprLmtSubBasicInfoService.selectLmtTotalByCusId(reqDto.getCusId(), reqDto.getInstuCde()) ;
                    //授信累加
                    BigDecimal avlAmt = amtMap.get("avlAmt") ;
                    //授信总额
                    BigDecimal lmtAmtAdd = amtMap.get("lmtAmtAdd") ;

                    BigDecimal lmtTotal = avlAmt.add(lmtAmtAdd).add(bizTotalAmt) ;

                    if(lmtTotal.compareTo((statYearAmtMax.multiply(bailRate)))>0){
                        logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】，授信总额【avlAmt】:"+avlAmt+"+授信累加【lmtAmtAdd】:"+lmtAmtAdd+
                                "大于客户上年度销售总额和当年销售总额较大值【statYearAmtMax】"+statYearAmtMax+"*补充保证金释放金额倍数【bailRate】："+bailRate);
                        throw new YuspException(EclEnum.ECL070078.key, EclEnum.ECL070078.value+"【"+bailRate+"】倍");
                    }
                }
            }
            return ;
        }
        //根据分项编号，获取分项信息
        ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(lmtSubNo) ;
        //额度分项
        //检验 分项批复信息不允许未空
        Optional.ofNullable(apprLmtSubBasicInfo).orElseThrow(()->new YuspException(EclEnum.ECL070007.key, EclEnum.ECL070007.value)) ;
        logger.info(JSON.toJSONString(apprLmtSubBasicInfo));
        //额度状态
        String status = apprLmtSubBasicInfo.getStatus() ;
        //到期日期
        String endDate = apprLmtSubBasicInfo.getLmtDate() ;
        //授信分项类型
        String lmtSubType = apprLmtSubBasicInfo.getLmtSubType() ;

        //STD_ZB_SUIT_BIZ_LINE_04 业务条线
        String belgLine = reqDto.getBelgLine();

        //校验额度分项必须是生效状态，白名单例外类型分情况处理
        validLmtSubStatus(status, exptType) ;
        String prdId= reqDto.getPrdId() ;
        String cfgPrdId = comm4Service.getSysParameterByName(CmisLmtConstants.PRD_ID_LMT_UNCHECK) ;
        //最高额合同和白名单额度不校验到期日问题 update by 20210714 最高额协议不校验到期日
        if(!(CmisLmtConstants.STD_ZB_LMT_TYPE_06.equals(lmtType)
                || CmisLmtConstants.STD_ZB_CONT_TYPE_2.equals(reqDto.getDealBizType())
                || CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(reqDto.getDealBizType())
                || CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_04.equals(belgLine)
                || comm4Service.checkStrInSourceStr(cfgPrdId, prdId)
        )){
            //一般合同的到期日不能超过额度分项到期日+宽限期 获取分项到期日以及宽限起
            if(CmisLmtConstants.STD_ZB_CONT_TYPE_1.equals(reqDto.getDealBizType()) && StringUtils.isNotBlank(reqDto.getEndDate())){
                //不控制最高额合同的到期日，控制最高额合同项下台账的到期日≤额度到期日+宽限期
                //校验到期日
                if (StringUtils.isEmpty(endDate)){
                    logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】的到期日为空");
                    throw new YuspException(EclEnum.ECL070104.key, EclEnum.ECL070104.value);
                }

                //非 null 处理
                Integer lmtGraper = apprLmtSubBasicInfo.getLmtGraper() ;
                if(lmtGraper == null) lmtGraper = 0 ;
                //获取到期日+ 宽限期 后的日期
                endDate = comm4Service.getEndDateAddGaraperDate(endDate, lmtGraper.intValue());



            }
            //校验到期日  add by zhangjw 20210713 只有到期日不为空时，校验到期日
            if(StringUtils.isNotBlank(reqDto.getEndDate())){
                boolean dateFlag = comm4Service.checkLmtEndDate(reqDto.getEndDate(), endDate) ;
                if(dateFlag){
                    logger.info("cmislmt0009 合同到期日（即请求里的enDate）"+reqDto.getEndDate()+"超过了额度分项编号【"+lmtSubNo+"】 的到期日加宽限期："+endDate);
                    throw new YuspException(EclEnum.ECL070004.key, EclEnum.ECL070004.value);
                }
            }
        }

        //非最高额授信协议，不允许占用到法人客户额度额度分项层级，只允许占用至产品分项层级
        if(CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(reqDto.getDealBizType())){
            if(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02.equals(lmtSubType)){
                logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】是最高额协议，对应的额度类型是产品分项");
                throw new YuspException(EclEnum.ECL070073.key, EclEnum.ECL070073.value);
            }
        }else{
            if(!CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02.equals(lmtSubType)){
                logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】是非最高额协议，对应的额度类型是额度分项");
                throw new YuspException(EclEnum.ECL070008.key, EclEnum.ECL070008.value);
            }
        }

        //分项中 授信品种类型属性
        String subLmtBizTypeProp = apprLmtSubBasicInfo.getLmtBizTypeProp() ;
        //请求报文中
        String reqLmtBizTypeProp = occSubRel.getPrdTypeProp() ;
        if(!CmisLmtConstants.STD_ZB_BIZ_ATTR_2.equals(reqDto.getBizAttr())){
            if(!comm4Service.IsEqualsTwoStr(subLmtBizTypeProp, reqLmtBizTypeProp)){
                logger.info("【{}】检验授信品种类型属性不相等，分项中属性值【{}】，接口中属性值【{}】", DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key,
                        subLmtBizTypeProp, reqLmtBizTypeProp);
                throw new YuspException(EclEnum.ECL070086.key, EclEnum.ECL070086.value);
            }
        }

        //额度为预授信额度时不允许占用
        if(CmisLmtConstants.YES_NO_Y.equals(apprLmtSubBasicInfo.getIsPreCrd())){
            logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】是预授信额度");
            throw new YuspException(EclEnum.ECL070005.key, EclEnum.ECL070005.value);
        }

        if(StringUtils.isNotEmpty(exptType) && exptType.contains(CmisLmtConstants.EXPT_TYPE_02)){
            logger.info("cmislmt0009 允许突破额度，不校验额度信息！");
            return;
        }
        /**
         * 法人客户进行校验
         * 规则：客户批复累加授信总额+授信总额部分，不允许超过客户上年度销售总额和当年销售总额较大值（纳税申报表）的N倍，
         * 此处【补充保证金释放金额倍数】作为额度系统参数设置，初始值为【2】  客户销售总额查询  FY_BAIL_REL_RATE
         */
        //是否校验授信总额参数配置  1-校验 0-不校验
        String checkTotalSales = comm4Service.getSysParameterByName(CmisLmtConstants.CHECK_TOTAL_SALES) ;
        if(CmisLmtConstants.YES_NO_Y.equals(checkTotalSales)){
            if(CmisLmtConstants.STD_ZB_CUS_TYPE_2.equals(apprLmtSubBasicInfo.getCusType())){
                BigDecimal bailRate = BigDecimalUtil.replaceNull(comm4Service.getSysParameterByName(CmisLmtConstants.FY_BAIL_REL_RATE)) ;
                BigDecimal statYearAmtMax = comm4Service.getStatYearAmtMax(reqDto.getCusId()) ;
                //授信累加
                BigDecimal avlAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlAmt()) ;
                //授信总额
                BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLmtAmtAdd()) ;

                if((avlAmt.add(lmtAmtAdd)).compareTo((statYearAmtMax.multiply(bailRate)))>0){
                    logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】，授信总额【avlAmt】:"+avlAmt+"+授信累加【lmtAmtAdd】:"+lmtAmtAdd+
                            "大于客户上年度销售总额和当年销售总额较大值【statYearAmtMax】"+statYearAmtMax+"*补充保证金释放金额倍数【bailRate】："+bailRate);
                    throw new YuspException(EclEnum.ECL070078.key, EclEnum.ECL070078.value+"【"+bailRate+"】倍");
                }
            }
        }

        /*******************额度校验***********************/
        //非低风险授信，合同占用敞口金额+额度分项已占用敞口是否满足小于等于授信敞口金额
        //低风险单一客户不检验额度是否足额, 白名单不校验，无缝衔接不校验（无缝衔接不校验是因为合同申请，借新还旧无法没明确具体信息，导致校验不过）
        if(!(CmisLmtConstants.YES_NO_Y.equals(reqDto.getIsLriskBiz())
                || CmisLmtConstants.STD_ZB_LMT_TYPE_06.equals(lmtType)
                || CmisLmtConstants.YES_NO_Y.equals(reqDto.getIsFollowBiz())
        ) ){
            Map<String, String> paramMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
            //校验分项信息
            logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】，校验分项信息开始");
            comm4Service.checkSpacLimit(apprLmtSubBasicInfo, bizSpacAmt, paramMap) ;
            logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】，校验分项信息结束");
            //校验分项总信息 add by 20211006 客户迁移数据，存在很多负数，拦截业务较大，此处暂时注释掉
          /*  logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】，校验分项总信息开始");
            comm4Service.checkLimitTotal(lmtSubNo, bizTotalAmt, paramMap) ;
            logger.info("cmislmt0009 额度分项编号【"+lmtSubNo+"】，校验分项总信息结束");*/
        }
        logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key+":校验批复额度分项基础信息额度-------->end");
    }

    /**
     * @作者:lizx
     * @方法名称: ifNotFiledCheck
     * @方法描述:  必输项目校验
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/30 0:13
     * @param reqDto:
     * @return: void
     * @算法描述: 无
     */
    public void ifNotFiledCheck(CmisLmt0009ReqDto reqDto){
        //系统编号不允许为空
        if(StringUtils.isEmpty(reqDto.getSysId())) throw new YuspException(EclEnum.ECL070133.key, "系统编号" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getInstuCde())) throw new YuspException(EclEnum.ECL070115.key, "金融机构代码不能为空" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getCusId())) throw new YuspException(EclEnum.ECL070133.key, "客户号" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getDealBizType())) throw new YuspException(EclEnum.ECL070133.key, "交易业务类型" + EclEnum.ECL070133.value) ;
        //if(StringUtils.isEmpty(reqDto.getPrdId())) throw new YuspException(EclEnum.ECL070133.key, "产品编号" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getIsLriskBiz())) throw new YuspException(EclEnum.ECL070133.key, "是否低风险业务" + EclEnum.ECL070133.value) ;
        //校验占用列表不允许为空
        List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtos = reqDto.getCmisLmt0009OccRelListReqDtoList() ;
        if(CollectionUtils.isEmpty(cmisLmt0009OccRelListReqDtos)){
            throw new YuspException(EclEnum.ECL070132.key, EclEnum.ECL070132.value) ;
        }
        //占用额度列表非空校验
        for(CmisLmt0009OccRelListReqDto occRelListReqDto : cmisLmt0009OccRelListReqDtos){
            //反向生成无分项编号，校验不了
            //if(StringUtils.isEmpty(occRelListReqDto.getLmtSubNo())) throw new YuspException(EclEnum.ECL070133.key, "额度分项编号" + EclEnum.ECL070133.value) ;
            if(StringUtils.isEmpty(occRelListReqDto.getLmtType())) throw new YuspException(EclEnum.ECL070133.key, "额度类型" + EclEnum.ECL070133.value) ;
        }
    }
}
