package cn.com.yusys.yusp.web.server.cmislmt0052;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0052.req.CmisLmt0052ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0052.resp.CmisLmt0052RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0052.CmisLmt0052Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户（综合授信+主体授信）敞口用信余额，不包含穿透化额度、承销额度
 *
 * @author dumd 20210905
 * @version 1.0
 */
@Api(tags = "cmislmt0052:查询客户（综合授信+主体授信）敞口用信余额，不包含穿透化额度、承销额度")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0052Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0052Resource.class);

    @Autowired
    private CmisLmt0052Service cmisLmt0052Service;

    /**
     * 交易码：CmisLmt0052
     * 交易描述：查询客户授信余额（不包含穿透化额度）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("查询客户授信余额（不包含穿透化额度）")
    @PostMapping("/cmislmt0052")
    protected @ResponseBody
    ResultDto<CmisLmt0052RespDto> CmisLmt0052(@Validated @RequestBody CmisLmt0052ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0052.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0052RespDto> CmisLmt0052RespDtoResultDto = new ResultDto<>();
        CmisLmt0052RespDto CmisLmt0052RespDto = new CmisLmt0052RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0052RespDto = cmisLmt0052Service.execute(reqDto);
            CmisLmt0052RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0052RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0052.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0052.value, e.getMessage());
            // 封装CmisLmt0052RespDtoResultDto中异常返回码和返回信息
            CmisLmt0052RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0052RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0052RespDto到CmisLmt0052RespDtoResultDto中
        CmisLmt0052RespDtoResultDto.setData(CmisLmt0052RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0052.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0052.value, JSON.toJSONString(CmisLmt0052RespDtoResultDto));
        return CmisLmt0052RespDtoResultDto;
    }
}