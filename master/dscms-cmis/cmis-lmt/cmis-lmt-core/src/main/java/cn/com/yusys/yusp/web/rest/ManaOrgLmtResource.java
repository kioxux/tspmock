/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ManaOrgLmt;
import cn.com.yusys.yusp.service.ManaOrgLmtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ManaOrgLmtResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 15:32:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/manaorglmt")
public class ManaOrgLmtResource {
    @Autowired
    private ManaOrgLmtService manaOrgLmtService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ManaOrgLmt>> query() {
        QueryModel queryModel = new QueryModel();
        List<ManaOrgLmt> list = manaOrgLmtService.selectAll(queryModel);
        return new ResultDto<List<ManaOrgLmt>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ManaOrgLmt>> index(QueryModel queryModel) {
        List<ManaOrgLmt> list = manaOrgLmtService.selectByModel(queryModel);
        return new ResultDto<List<ManaOrgLmt>>(list);
    }

    /**
     * @函数名称:selectbymodel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<ManaOrgLmt>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<ManaOrgLmt> list = manaOrgLmtService.selectByModel(queryModel);
        return new ResultDto<List<ManaOrgLmt>>(list);
    }

    @PostMapping("/selectbymodel2")
    protected ResultDto<List<Map>> selectbymodel2(@RequestBody QueryModel queryModel) {
        List<Map> list = manaOrgLmtService.selectByModel2(queryModel);
        return new ResultDto<List<Map>>(list);
    }

    /**
     * 异步下载分支机构额度管控数据
     */
    @PostMapping("/exportmanaorglmt")
    public ResultDto<ProgressDto> asyncExportManaOrgLmt(@RequestBody QueryModel model) {
        ProgressDto progressDto = manaOrgLmtService.asyncExportManaOrgLmt(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{orgId}")
    protected ResultDto<ManaOrgLmt> show(@PathVariable("orgId") String orgId) {
        ManaOrgLmt manaOrgLmt = manaOrgLmtService.selectByPrimaryKey(orgId);
        return new ResultDto<ManaOrgLmt>(manaOrgLmt);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ManaOrgLmt> create(@RequestBody ManaOrgLmt manaOrgLmt) throws URISyntaxException {
        manaOrgLmtService.insert(manaOrgLmt);
        return new ResultDto<ManaOrgLmt>(manaOrgLmt);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ManaOrgLmt manaOrgLmt) throws URISyntaxException {
        int result = manaOrgLmtService.update(manaOrgLmt);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{orgId}")
    protected ResultDto<Integer> delete(@PathVariable("orgId") String orgId) {
        int result = manaOrgLmtService.deleteByPrimaryKey(orgId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = manaOrgLmtService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectbymodel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryOrgList")
    protected ResultDto<List<Map<String,String>>> queryOrgList(@RequestBody QueryModel queryModel) {
        List<Map<String,String>> list = manaOrgLmtService.queryOrgList(queryModel);
        return new ResultDto<List<Map<String,String>>>(list);
    }

    /**
     * @函数名称:selectOrgList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     */
    @PostMapping("/selectOrgList")
    protected ResultDto<List<String>> selectOrgList(@RequestBody QueryModel queryModel) {
        List<String> list = manaOrgLmtService.selectOrgList(queryModel);
        return new ResultDto<List<String>>(list);
    }
}
