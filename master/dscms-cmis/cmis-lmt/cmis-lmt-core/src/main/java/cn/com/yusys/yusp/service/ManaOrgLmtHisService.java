/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.vo.ManaOrgLmtHisVo;
import cn.com.yusys.yusp.vo.ManaOrgLmtVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ManaOrgLmtHis;
import cn.com.yusys.yusp.repository.mapper.ManaOrgLmtHisMapper;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ManaOrgLmtHisService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 15:32:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ManaOrgLmtHisService {

    @Autowired
    private ManaOrgLmtHisMapper manaOrgLmtHisMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ManaOrgLmtHis selectByPrimaryKey(String pkId, String orgId) {
        return manaOrgLmtHisMapper.selectByPrimaryKey(pkId, orgId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ManaOrgLmtHis> selectAll(QueryModel model) {
        List<ManaOrgLmtHis> records = (List<ManaOrgLmtHis>) manaOrgLmtHisMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ManaOrgLmtHis> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ManaOrgLmtHis> list = manaOrgLmtHisMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 异步导出分支机构额度管控历史数据
     * @return 导出进度信息
     */
    public ProgressDto asyncExportManaOrgLmtHis(QueryModel model) {
        model.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            List<ManaOrgLmtHis> manaOrgLmtHis = selectByModel(queryModeTemp);
            List<ManaOrgLmtHisVo> manaOrgLmtHisVoList = new ArrayList<>();
            if (CollectionUtils.nonEmpty(manaOrgLmtHis)) {
                manaOrgLmtHis.stream().forEach(a -> {
                    ManaOrgLmtHisVo manaOrgLmtHisVo = new ManaOrgLmtHisVo();
                    BeanUtils.copyProperties(a, manaOrgLmtHisVo);
                    //获取机构名称
                    manaOrgLmtHisVo.setOrgName(OcaTranslatorUtils.getOrgName(a.getOrgId()));
                    //月末对公贷款余额测算
                    BigDecimal curMonthComLoanBalancePlan = a.getLastMonthComLoanBalance()
                            .add(a.getCurrMonthAllowComAddAmt());
                    manaOrgLmtHisVo.setCurMonthComLoanBalancePlan(curMonthComLoanBalancePlan);
                    manaOrgLmtHisVoList.add(manaOrgLmtHisVo);
                });
            }
            return manaOrgLmtHisVoList;
        };
        ExportContext exportContext = ExportContext.of(ManaOrgLmtHisVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ManaOrgLmtHis record) {
        return manaOrgLmtHisMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ManaOrgLmtHis record) {
        return manaOrgLmtHisMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ManaOrgLmtHis record) {
        return manaOrgLmtHisMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ManaOrgLmtHis record) {
        return manaOrgLmtHisMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String orgId) {
        return manaOrgLmtHisMapper.deleteByPrimaryKey(pkId, orgId);
    }

    /**
     * @方法名称: selectOrgList
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<String> selectOrgList(QueryModel model) {
        List<String> list = manaOrgLmtHisMapper.queryOrgList(model);
        return list;
    }


}
