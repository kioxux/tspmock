/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TranAbnFlowInfo
 * @类描述: tran_abn_flow_info数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-03 10:17:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tran_abn_flow_info")
public class TranAbnFlowInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 60)
	private String cusName;
	
	/** 客户主体类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 2)
	private String cusType;
	
	/** 证件类型STD_ZB_CERT_TYP **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 交易业务编号 **/
	@Column(name = "DEAL_BIZ_NO", unique = false, nullable = true, length = 32)
	private String dealBizNo;
	
	/** 交易类型STD_ZB_DEAL_TYP **/
	@Column(name = "DEAL_TYP", unique = false, nullable = true, length = 3)
	private String dealTyp;
	
	/** 额度结构编号 **/
	@Column(name = "LIMIT_STR_NO", unique = false, nullable = true, length = 32)
	private String limitStrNo;
	
	/** 额度结构名称 **/
	@Column(name = "LIMIT_STR_NAME", unique = false, nullable = true, length = 500)
	private String limitStrName;
	
	/** 版本号 **/
	@Column(name = "VERSION", unique = false, nullable = true, length = 32)
	private String version;
	
	/** 额度分项编号 **/
	@Column(name = "LIMIT_SUB_NO", unique = false, nullable = true, length = 32)
	private String limitSubNo;
	
	/** 额度分项名称 **/
	@Column(name = "LIMIT_SUB_NAME", unique = false, nullable = true, length = 500)
	private String limitSubName;
	
	/** 交易币种STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 3)
	private String curType;
	
	/** 交易汇率 **/
	@Column(name = "TRAN_EXCHANGE_RATE", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal tranExchangeRate;
	
	/** 交易描述 **/
	@Column(name = "TRAN_DEC", unique = false, nullable = true, length = 500)
	private String tranDec;
	
	/** 交易时间 **/
	@Column(name = "TRAN_TIME", unique = false, nullable = true, length = 32)
	private String tranTime;
	
	/** 交易金额 **/
	@Column(name = "TRAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal tranAmt;
	
	/** 系统编号STD_ZB_SYS_ID **/
	@Column(name = "SYS_ID", unique = false, nullable = true, length = 2)
	private String sysId;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 2)
	private String status;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param dealBizNo
	 */
	public void setDealBizNo(String dealBizNo) {
		this.dealBizNo = dealBizNo;
	}
	
    /**
     * @return dealBizNo
     */
	public String getDealBizNo() {
		return this.dealBizNo;
	}
	
	/**
	 * @param dealTyp
	 */
	public void setDealTyp(String dealTyp) {
		this.dealTyp = dealTyp;
	}
	
    /**
     * @return dealTyp
     */
	public String getDealTyp() {
		return this.dealTyp;
	}
	
	/**
	 * @param limitStrNo
	 */
	public void setLimitStrNo(String limitStrNo) {
		this.limitStrNo = limitStrNo;
	}
	
    /**
     * @return limitStrNo
     */
	public String getLimitStrNo() {
		return this.limitStrNo;
	}
	
	/**
	 * @param limitStrName
	 */
	public void setLimitStrName(String limitStrName) {
		this.limitStrName = limitStrName;
	}
	
    /**
     * @return limitStrName
     */
	public String getLimitStrName() {
		return this.limitStrName;
	}
	
	/**
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	
    /**
     * @return version
     */
	public String getVersion() {
		return this.version;
	}
	
	/**
	 * @param limitSubNo
	 */
	public void setLimitSubNo(String limitSubNo) {
		this.limitSubNo = limitSubNo;
	}
	
    /**
     * @return limitSubNo
     */
	public String getLimitSubNo() {
		return this.limitSubNo;
	}
	
	/**
	 * @param limitSubName
	 */
	public void setLimitSubName(String limitSubName) {
		this.limitSubName = limitSubName;
	}
	
    /**
     * @return limitSubName
     */
	public String getLimitSubName() {
		return this.limitSubName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param tranExchangeRate
	 */
	public void setTranExchangeRate(java.math.BigDecimal tranExchangeRate) {
		this.tranExchangeRate = tranExchangeRate;
	}
	
    /**
     * @return tranExchangeRate
     */
	public java.math.BigDecimal getTranExchangeRate() {
		return this.tranExchangeRate;
	}
	
	/**
	 * @param tranDec
	 */
	public void setTranDec(String tranDec) {
		this.tranDec = tranDec;
	}
	
    /**
     * @return tranDec
     */
	public String getTranDec() {
		return this.tranDec;
	}
	
	/**
	 * @param tranTime
	 */
	public void setTranTime(String tranTime) {
		this.tranTime = tranTime;
	}
	
    /**
     * @return tranTime
     */
	public String getTranTime() {
		return this.tranTime;
	}
	
	/**
	 * @param tranAmt
	 */
	public void setTranAmt(java.math.BigDecimal tranAmt) {
		this.tranAmt = tranAmt;
	}
	
    /**
     * @return tranAmt
     */
	public java.math.BigDecimal getTranAmt() {
		return this.tranAmt;
	}
	
	/**
	 * @param sysId
	 */
	public void setSysId(String sysId) {
		this.sysId = sysId;
	}
	
    /**
     * @return sysId
     */
	public String getSysId() {
		return this.sysId;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}


}