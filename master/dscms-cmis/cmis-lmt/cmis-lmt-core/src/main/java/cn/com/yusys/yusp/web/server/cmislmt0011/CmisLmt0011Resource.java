package cn.com.yusys.yusp.web.server.cmislmt0011;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0011.CmisLmt0011Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:占用分项交易
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "cmislmt0011:占用分项交易")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0011Resource.class);

    @Autowired
    private CmisLmt0011Service cmisLmt0011Service;
    /**
     * 交易码：cmislmt0011
     * 交易描述：占用分项交易
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("占用分项交易")
    @PostMapping("/cmislmt0011")
    protected @ResponseBody
    ResultDto<CmisLmt0011RespDto> cmisLmt0011(@Validated @RequestBody CmisLmt0011ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0011RespDto> cmisLmt0011RespDtoResultDto = new ResultDto<>();
        CmisLmt0011RespDto cmisLmt0011RespDto = new CmisLmt0011RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0011RespDto = cmisLmt0011Service.execute(reqDto);
            cmisLmt0011RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0011RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0011.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.value, e.getMessage());
            // 封装xddb0011DataResultDto中异常返回码和返回信息
            cmisLmt0011RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0011RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0011RespDto到cmisLmt0011RespDtoResultDto中
        cmisLmt0011RespDtoResultDto.setData(cmisLmt0011RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.value, JSON.toJSONString(cmisLmt0011RespDtoResultDto));
        return cmisLmt0011RespDtoResultDto;
    }
}
