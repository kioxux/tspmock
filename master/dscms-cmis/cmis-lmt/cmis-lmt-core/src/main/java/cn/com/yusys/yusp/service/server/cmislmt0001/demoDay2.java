package cn.com.yusys.yusp.service.server.cmislmt0001;

public class demoDay2 {
    public static void main(String[] args) {
        final Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("线程1：t1");
            }
        });

        final Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                /*try {
                    //t1.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                //Thread.yield(); ;
                System.out.println("线程2：t2");
            }
        }) ;

        final Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                /*try {
                    t2.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                System.out.println("线程3：t3");
            }
        }) ;

        t3.start();
        t2.start();
        t1.start();
    }
}
