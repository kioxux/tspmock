package cn.com.yusys.yusp.service.server.cmislmt0062;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.DeRiskIndexCfgHis;
import cn.com.yusys.yusp.domain.DmRisKhfxJgbxJk;
import cn.com.yusys.yusp.dto.server.cmislmt0062.req.CmisLmt0062ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0062.resp.CmisLmt0062RespDto;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DeRiskIndexCfgHisService;
import cn.com.yusys.yusp.service.DmRisKhfxJgbxJkService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0061Service
 * @类描述: #对内服务类
 * @功能描述: 根据客户编号获取最新大额风险值
 * @创建时间: 2021-09-30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0062Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0062Service.class);

    @Autowired
    private DmRisKhfxJgbxJkService dmRisKhfxJgbxJkService;

    /**
     * DE_RISK_INDEX_CFG_HIS
     */
    @Autowired
    private DeRiskIndexCfgHisService deRiskIndexCfgHisService;

    public CmisLmt0062RespDto execute(CmisLmt0062ReqDto reqDto) {
        CmisLmt0062RespDto cmisLmt0062RespDto = new CmisLmt0062RespDto();
        try {
            String custId = reqDto.getCustId();
            if (StringUtils.isBlank(custId)){
                cmisLmt0062RespDto.setErrorCode(EpbEnum.EPB090004.key);
                cmisLmt0062RespDto.setErrorMsg("custId不能为空！");
                return cmisLmt0062RespDto;
            }
            //是否属于授信
            String belongSx = reqDto.getBelongSx();
            if (StringUtils.isBlank(belongSx)){
                cmisLmt0062RespDto.setErrorCode(EpbEnum.EPB090004.key);
                cmisLmt0062RespDto.setErrorMsg("belongSx不能为空！");
                return cmisLmt0062RespDto;
            }
            //是否属于同业授信
            String belongTysx = reqDto.getBelongTysx();
            if (StringUtils.isBlank(belongTysx)){
                cmisLmt0062RespDto.setErrorCode(EpbEnum.EPB090004.key);
                cmisLmt0062RespDto.setErrorMsg("belongTysx不能为空！");
                return cmisLmt0062RespDto;
            }
            //风险类型
            String riskType = "";
            //大额风险配置
            DeRiskIndexCfgHis deRiskIndexCfgHis = null;
            //净额
            BigDecimal netAmt = BigDecimal.ZERO;

            //1.同业授信，则默认   04-同业单一客户风险暴露占比     ；
            if ("1".equals(belongTysx)) {
                logger.info("CmisLmt0062 ====>2");
                riskType = CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_04.riskType;
            } else {
                // 2.非同业授信，则默认为02-非同业单一客户风险暴露占比
                logger.info("CmisLmt0062 ====>3");
                riskType = CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_02.riskType;
            }
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("custId", custId);
            queryModel.addCondition("custTypeId",CmisLmtConstants.STD_DE_RISK_TYPE.matchCusTypeId(riskType));
            //取最新数据
            queryModel.setSort(" dataDt desc");
            List<DmRisKhfxJgbxJk> dmRisKhfxJgbxJks = dmRisKhfxJgbxJkService.selectByModel(queryModel);
            //若客户最新一天的大额风险暴露数据不存在
            if (CollectionUtils.isEmpty(dmRisKhfxJgbxJks)) {
                logger.info("CmisLmt0062 ====>1");
                queryModel = new QueryModel();
                queryModel.addCondition("riskType", riskType);
                queryModel.setSort(" dataDt desc ");
                List<DeRiskIndexCfgHis> deRiskIndexCfgHisList = deRiskIndexCfgHisService.selectAll(queryModel);
                logger.info("deRiskIndexCfgHisList==========>【{}】",deRiskIndexCfgHisList.size());
                deRiskIndexCfgHis = CollectionUtils.isNotEmpty(deRiskIndexCfgHisList) ? deRiskIndexCfgHisList.get(0) : null;
                cmisLmt0062RespDto.setDeRiskExpoAmt(BigDecimal.ZERO);
                cmisLmt0062RespDto.setRiskRedReqAmt(BigDecimal.ZERO);
                cmisLmt0062RespDto.setRiskYellowReqAmt(BigDecimal.ZERO);
                cmisLmt0062RespDto.setRiskIndexReqAmt(BigDecimal.ZERO);
            } else {
                logger.info("CmisLmt0062 ====>5");
                DmRisKhfxJgbxJk dmRisKhfxJgbxJk = dmRisKhfxJgbxJks.get(0);
                String custTypeId = dmRisKhfxJgbxJk.getCustTypeId();
                logger.info("CmisLmt0062 ====>6===custTypeId【{}】", custTypeId);
                //大额风险暴露值
                BigDecimal deRiskExpoAmt = dmRisKhfxJgbxJk.getContLonBal().add(dmRisKhfxJgbxJk.getFtydyKhmxExpo())
                        .add(dmRisKhfxJgbxJk.getFtyglKhmxExpo()).add(dmRisKhfxJgbxJk.getTydyKhmxExpo())
                        .add(dmRisKhfxJgbxJk.getTyglKhmxExpo()).add(dmRisKhfxJgbxJk.getMmkhKhmxExpo());
                cmisLmt0062RespDto.setDeRiskExpoAmt(deRiskExpoAmt);
                logger.info("CmisLmt0062 ====>7===deRiskExpoAmt【{}】", deRiskExpoAmt);
                //对应指标类型的阈值设置
                //大额风险暴露值（根据客户类型代码获取对应的风险暴露） === > 用信申请，取第一个；授信申请取第二个
                if ("01".equals(custTypeId)) {
                    riskType = "1".equals(belongSx) ? CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_02.getRiskType() : CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_02.getRiskType();
                } else {
                    riskType = CmisLmtConstants.STD_DE_RISK_TYPE.matchRiskTypeByCustTypeId(custTypeId);
                }
                queryModel = new QueryModel();
                queryModel.addCondition("riskType", riskType);
                queryModel.setSort(" dataDt desc ");
                List<DeRiskIndexCfgHis> deRiskIndexCfgHisList = deRiskIndexCfgHisService.selectAll(queryModel);
                logger.info("deRiskIndexCfgHisList==========>【{}】",deRiskIndexCfgHisList.size());
                deRiskIndexCfgHis = CollectionUtils.isNotEmpty(deRiskIndexCfgHisList) ? deRiskIndexCfgHisList.get(0) : null;


                logger.info("CmisLmt0062 ====>8===riskType【{}】 belongSx【{}】 netAmt【{}】", riskType, belongSx, netAmt);
            }
            //阈值设置 获取失败
            if (deRiskIndexCfgHis == null) {
                logger.info("CmisLmt0062 ====>9");
                cmisLmt0062RespDto.setErrorCode(EpbEnum.EPB090004.key);
                cmisLmt0062RespDto.setErrorMsg("客户【"+custId+"】最新风险拦截配置【" + riskType + "】获取失败！");
            } else {
                //净额
                //非同业单一客户贷款余额占比   资本净额
                if (CmisLmtConstants.STD_DE_RISK_TYPE.TYPE_01.riskType.equals(riskType)){
                    netAmt = deRiskIndexCfgHis.getZbjeAmt();
                }else{
                    //其他是用一级资本净额计算
                    netAmt = deRiskIndexCfgHis.getYjzbjeAmt();
                }

                //红色阈值
                BigDecimal riskRedReqAmt =netAmt.multiply(deRiskIndexCfgHis.getRiskRedReq());
                cmisLmt0062RespDto.setRiskRedReqAmt(riskRedReqAmt);
                //黄区阈值
                BigDecimal riskYellowReqAmt =netAmt.multiply(deRiskIndexCfgHis.getRiskYellowReq());
                cmisLmt0062RespDto.setRiskYellowReqAmt(riskYellowReqAmt);
                //监管限额
                BigDecimal riskIndexReqAmt =netAmt.multiply(deRiskIndexCfgHis.getRiskIndexReq());
                cmisLmt0062RespDto.setRiskIndexReqAmt(riskIndexReqAmt);
                logger.info("CmisLmt0062 ====>10 ===> RiskRedReq 【{}】   RiskYellowReq【{}】 RiskIndexReq【{}】",
                        deRiskIndexCfgHis.getRiskRedReq(), deRiskIndexCfgHis.getRiskYellowReq(), deRiskIndexCfgHis.getRiskIndexReq());
                logger.info("CmisLmt0062 ====>11 ===> riskRedReqAmt 【{}】   riskYellowReqAmt【{}】 riskIndexReqAmt【{}】",
                        riskRedReqAmt, riskYellowReqAmt, riskIndexReqAmt);
                cmisLmt0062RespDto.setErrorCode(SuccessEnum.SUCCESS.key);
                cmisLmt0062RespDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            }
        } catch (Exception e) {
            logger.info("CmisLmt0062 ====>11 ===》", e);
            cmisLmt0062RespDto.setErrorCode(EpbEnum.EPB090004.key);
            cmisLmt0062RespDto.setErrorMsg(e.getMessage());
        }
        return cmisLmt0062RespDto;
    }
}
