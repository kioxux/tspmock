package cn.com.yusys.yusp.service.server.cmislmt0056;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0056.req.CmisLmt0056ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.resp.CmisLmt0056RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0056Service
 * @类描述: #对内服务类
 * @功能描述: 获取客户有效的低风险额度分项明细编号
 * @创建时间: 2021-09-09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0056Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0056Service.class);

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;
    /**
     * 获取客户有效的低风险额度分项明细编号
     * add by lizx 2021-09-09
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0056RespDto execute(CmisLmt0056ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0056.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0056.value);
        CmisLmt0056RespDto respDto = new CmisLmt0056RespDto();
        try {
            //客户号
            String cusId = reqDto.getCusId() ;
            //用信产品编号
            String prdId = reqDto.getPrdId() ;
            //金融组织代码
            String instuCde = reqDto.getInstuCde() ;
            //根据用信产品编号，获取额度产品编号
            String limitSubNo = comm4Service.selectLimitSubNoListByParam(prdId, CmisLmtConstants.STD_ZB_LMT_STR_MTABLE_CONF_01, CmisLmtConstants.YES_NO_Y) ;
            if(StringUtils.isBlank(limitSubNo)) limitSubNo = CmisLmtConstants.LIMIT_SUB_NO_12010103;
            QueryModel queryModel = new QueryModel() ;
            queryModel.addCondition("cusId", cusId);
            queryModel.addCondition("limitSubNo", limitSubNo);
            queryModel.addCondition("instuCde", instuCde);
            //如果额度产品编号不为空，则根据额度产品编号，客户号，金融组织代码获取额度明细分项编号信息
            List<Map<String, String>> subList = apprLmtSubBasicInfoService.selectApprSubSerNoByParam(queryModel) ;
            if(CollectionUtils.isNotEmpty(subList)){
                Map<String, String> resultMap = subList.get(0) ;
                respDto.setApprSubSerno(resultMap.get("apprSubSerno"));
                respDto.setLmtBizTypeProp(resultMap.get("lmtBizTypeProp"));
                respDto.setSuitGuarWay(resultMap.get("suitGuarWay"));
                respDto.setSubSerno(resultMap.get("parentId"));
            }
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0056】获取客户有效的特定目的载体投资额度列表：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0056.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0056.value);
        return respDto;
    }
}