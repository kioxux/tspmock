/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ApprCoopInfo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-04-20 22:36:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface ApprCoopInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    ApprCoopInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<ApprCoopInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(ApprCoopInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(ApprCoopInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(ApprCoopInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(ApprCoopInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据Serno查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    ApprCoopInfo selectBySerno(@Param("serno") String serno);

    List<Map<String,Object>> selectByCusIdAndStatus(QueryModel queryModel);
    /**
     * @param queryModel
     * @desc 合作方额度视图列表（支持查并表额度）：
     *      instuCde金融机构号;notInstuCde不为金融机构号；cusId客户号；cusName客户名称；copType合作方类型
     * @date 2021/5/6
     */
    List<Map<String,Object>>  queryListByInstuCde(QueryModel queryModel);

    List<Map<String,Object>>  queryApprCoopBySerno(QueryModel queryModel);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    Map<String, BigDecimal> getSubOutTotal(QueryModel queryModel);

    /**
     * 更新合作方主信息最近更新人、修改时间
     * @param
     * @return
     */

    int updateApprCoopInfoByParams(Map params);

    /**
     * 根据授信编号修改合作方授信额度状态(冻结/解冻)
     * @param
     * @return
     */
    int updateApprCoopInfoBySerNo(Map params);

    /**
     * 根据批复流水号查询合作方批复信息
     * @param
     * @return
     */
    ApprCoopInfo getAllByApprSerno(String apprSerno);

    /**
     * 根据授信编号修改合作方授信额度状态（失效已结清/失效未结清）
     * @param
     * @return
     */
    int updateByApprSerNo(Map params);


    /**
     * 根据授信编号修改合作方授信额度状态（失效已结清/失效未结清）
     * @param
     * @return
     */
    int updateApprCoopInfoByApprSerNo(HashMap<String,Object> hashMap);


    /**
     * 根据授信编号修改合作方授信信息
     * @param
     * @return
     */
    int updateByApprSerNoSelective(ApprCoopInfo record);

    /**
     * @param queryModel
     * @desc 根据合作方客户号查找客户合作方额度项下关联合同列表
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    List<Map<String,Object>> selectCoopLmtContRelByCusId(QueryModel queryModel);

    /**
     * @param queryModel
     * @desc 根据合作方客户号查找客户合作方额度项下关联合同汇总
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    List<Map<String,Object>> selectTotalCoopLmtContRelByCusId(QueryModel queryModel);

    /**
     * @param queryModel
     * @desc 根据合作方额度分项编号查找客户合作方额度项下关联合同列表
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    List<Map<String,Object>> selectCoopLmtContRelByApprSubSerno(QueryModel queryModel);
    /**
     * @param queryModel
     * @desc 根据合作方额度分项编号查找客户合作方额度项下关联合同汇总
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    List<Map<String,Object>> selectTotalCoopLmtContRelByApprSubSerno(QueryModel queryModel);

    /**
     * @param queryModel
     * @desc 根据合作方客户号查找客户合作方额度项下关联合同下关联的台账列表
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    List<Map<String,Object>> selectCoopAccContRelByCusId(QueryModel queryModel);

    /**
     * @param queryModel
     * @desc 根据合作方客户号查找客户合作方额度项下关联合同下关联的台账汇总
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    List<Map<String,Object>> selectTotalCoopAccContRelByCusId(QueryModel queryModel);

    /**
     * @param queryModel
     * @desc 根据合作方额度分项编号查找客户合作方额度项下关联合同下台账列表(未结清)
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    List<Map<String,Object>> selectCoopAccContRelByApprSubSerno(QueryModel queryModel);
    /**
     * @param queryModel
     * @desc 根据合作方额度分项编号查找客户合作方额度项下关联合同下台账汇总
     * @date 2021/8/17
     * @version 1.0.0
     * @修改历史: zhangjw
     */
    List<Map<String,Object>> selectTotalCoopAccContRelByApprSubSerno(QueryModel queryModel);

    /**
     * @param queryModel
     * @desc 合作方额度列表，以批复为维度（冻结解冻使用）：
     *      instuCde金融机构号;notInstuCde不为金融机构号；cusId客户号；cusName客户名称；copType合作方类型
     * @date 2021/9/29
     */
    List<Map<String,Object>>  queryListByInstuCdeForDj(QueryModel queryModel);

}