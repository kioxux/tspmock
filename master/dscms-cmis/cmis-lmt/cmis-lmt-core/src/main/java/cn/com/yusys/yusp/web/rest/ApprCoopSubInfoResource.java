/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.ApprCoopSubInfo;
import cn.com.yusys.yusp.service.ApprCoopSubInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopSubInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-03 16:56:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/apprcoopsubinfo")
public class ApprCoopSubInfoResource {
    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<ApprCoopSubInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<ApprCoopSubInfo> list = apprCoopSubInfoService.selectAll(queryModel);
        return new ResultDto<List<ApprCoopSubInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<ApprCoopSubInfo>> index(QueryModel queryModel) {
        List<ApprCoopSubInfo> list = apprCoopSubInfoService.selectByModel(queryModel);
        return new ResultDto<List<ApprCoopSubInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<ApprCoopSubInfo> show(@PathVariable("pkId") String pkId) {
        ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<ApprCoopSubInfo>(apprCoopSubInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<ApprCoopSubInfo> create(@RequestBody ApprCoopSubInfo apprCoopSubInfo) {
        apprCoopSubInfoService.insert(apprCoopSubInfo);
        return new ResultDto<ApprCoopSubInfo>(apprCoopSubInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody ApprCoopSubInfo apprCoopSubInfo) {
        int result = apprCoopSubInfoService.update(apprCoopSubInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = apprCoopSubInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = apprCoopSubInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectAcsiInfoBySerno
     * @函数描述:查询选择对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectAcsiInfoBySerno")
    protected ResultDto<List<ApprCoopSubInfo>> selectAcsiInfoBySerno(@RequestBody QueryModel queryModel) {
        List<ApprCoopSubInfo> list = apprCoopSubInfoService.selectAcsiInfoBySerno(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:selectApprCoopSubInfoAndBasicBySerno
     * @函数描述:关联合作方额度台账表，查询合作方额度分项信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectApprCoopSubInfoAndBasicBySerno")
    protected ResultDto<List<Map<String, Object>>> selectApprCoopSubInfoAndBasicBySerno(@RequestBody QueryModel queryModel) {
        List<Map<String, Object>> list = apprCoopSubInfoService.selectApprCoopSubInfoAndBasicBySerno(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:queryListByCusIdAndCopType
     * @函数描述:合作方额度视图详情-分项明细
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryListByCusIdAndCopType")
    protected ResultDto<List<Map<String, Object>>> queryListByCusIdAndCopType(@RequestBody QueryModel queryModel) {
        List<Map<String, Object>> list = apprCoopSubInfoService.queryListByCusIdAndCopType(queryModel);
        return new ResultDto<>(list);
    }
}
