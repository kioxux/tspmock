package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class LmtCreateConfDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** FK_PKID **/
    private String fkPkid;

    /** 额度分项编号 **/
    private String limitSubNo;

    /** 额度分项名称 **/
    private String lmtItmeName;

    /** 版本号 **/
    private String version;

    /** 额度创建方式 **/
    private String limitCreateMode;

    /** 是否支持维护押品或保证人 **/
    private String isSupportGuar;

    /** 是否支持担保合同 **/
    private String isSupportGrt;

    /** 变更方式 **/
    private String limitChangeMode;

    /** 额度生效方式 **/
    private String limitInureMode;

    public String getFkPkid() {
        return fkPkid;
    }

    public void setFkPkid(String fkPkid) {
        this.fkPkid = fkPkid;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLmtItmeName() {
        return lmtItmeName;
    }

    public void setLmtItmeName(String lmtItmeName) {
        this.lmtItmeName = lmtItmeName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLimitCreateMode() {
        return limitCreateMode;
    }

    public void setLimitCreateMode(String limitCreateMode) {
        this.limitCreateMode = limitCreateMode;
    }

    public String getIsSupportGuar() {
        return isSupportGuar;
    }

    public void setIsSupportGuar(String isSupportGuar) {
        this.isSupportGuar = isSupportGuar;
    }

    public String getIsSupportGrt() {
        return isSupportGrt;
    }

    public void setIsSupportGrt(String isSupportGrt) {
        this.isSupportGrt = isSupportGrt;
    }

    public String getLimitChangeMode() {
        return limitChangeMode;
    }

    public void setLimitChangeMode(String limitChangeMode) {
        this.limitChangeMode = limitChangeMode;
    }

    public String getLimitInureMode() {
        return limitInureMode;
    }

    public void setLimitInureMode(String limitInureMode) {
        this.limitInureMode = limitInureMode;
    }
}
