package cn.com.yusys.yusp.service.server.cmislmt0027;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.req.CmisLmt0027ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0027.resp.CmisLmt0027RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprCoopInfoService;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0027Service
 * @类描述: #对内服务类
 * @功能描述: 合作方客户额度查询
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0027Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0027Service.class);
    @Autowired
    private ApprCoopInfoService apprCoopInfoService;

    /**
     * 合作方客户额度查询
     * add by zhangjw 20210612
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0027RespDto execute(CmisLmt0027ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0027.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0027.value);
        CmisLmt0027RespDto respDto = new CmisLmt0027RespDto();

        //金融机构代码
        String instuCde = reqDto.getInstuCde();
        //客户编号
        String cusId = reqDto.getCusId();

        try {
            QueryModel model = new QueryModel();
            model.addCondition("instuCde",instuCde);
            model.addCondition("cusId",cusId);
            List<Map<String,Object>> list = apprCoopInfoService.queryListByInstuCde(model);
            logger.info("查询结果为{}", JSON.toJSONString(list) );
            if(list!=null && list.size()>0){
                Map map = list.get(0);
                respDto.setLmtAmt(BigDecimalUtil.replaceNull(map.get("totalAmt")));
                respDto.setOutstandAmt(BigDecimalUtil.replaceNull(map.get("totalUseAmt")));
                respDto.setValAmt(BigDecimalUtil.replaceNull(map.get("totalValAmt")));
            }else{
                respDto.setLmtAmt(BigDecimal.ZERO);
                respDto.setOutstandAmt(BigDecimalUtil.replaceNull(BigDecimal.ZERO));
                respDto.setValAmt(BigDecimalUtil.replaceNull(BigDecimal.ZERO));
            }

            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("合作方客户额度查询" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0027.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0027.value);
        return respDto;
    }
}