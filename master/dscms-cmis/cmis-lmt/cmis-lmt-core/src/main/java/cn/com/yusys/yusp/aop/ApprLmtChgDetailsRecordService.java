package cn.com.yusys.yusp.aop;

import cn.com.yusys.yusp.aop.UserAndOrgNameAspect;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.ApprLmtChgFiledListDto;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: aopDemo
 * @类描述: #对内服务类
 * @功能描述: TODO
 * @创建时间: 2021/7/27 9:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Aspect
@Component
public class ApprLmtChgDetailsRecordService {

    private static final Logger log = LoggerFactory.getLogger(UserAndOrgNameAspect.class);
    private static final HashSet<String> AMT_SET = new HashSet<>();
    @Autowired
    private Comm4Service comm4Service ;
    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;
    @Autowired
    private LmtContRelService lmtContRelService ;
    @Autowired
    private ContAccRelService contAccRelService ;
    @Autowired
    private LmtWhiteInfoService lmtWhiteInfoService ;
    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService ;
    @Autowired
    private LmtDiscOrgService lmtDiscOrgService ;

    @Autowired
    public ApprLmtChgDetailsRecordService() {
        init();
    }

    public void init() {
        //公用字段
        AMT_SET.add("avlAmt");
        //单一法人金额字段
        AMT_SET.add("outstndAmt");
        AMT_SET.add("spacAmt");
        AMT_SET.add("spacOutstndAmt");
        AMT_SET.add("loanBalance");
        AMT_SET.add("lmtAmtAdd");
        AMT_SET.add("pvpOutstndAmt");
        AMT_SET.add("avlOutstndAmt");
        AMT_SET.add("loanSpacBalance");
        //合作方分项金额
        AMT_SET.add("sigAmt");
        AMT_SET.add("sigBussAmt");
        //合同金额
        AMT_SET.add("bizTotalBalanceAmtCny");
        AMT_SET.add("bizSpacBalanceAmtCny");
        //台账金额
        AMT_SET.add("accTotalBalanceAmtCny");
        AMT_SET.add("accSpacBalanceAmtCny");
        //白名单金额
        AMT_SET.add("sigAmt");
        AMT_SET.add("sigUseAmt");
        //贴现限额管控
        AMT_SET.add("useAmt");
    }

    @Before("execution(* cn.com.yusys.yusp.service..*.update*InApprLmtChgDetails(..))")
    public void beforeMethod(JoinPoint jp) {
        try {
            String methodName = jp.getSignature().getName();
            if(jp.getArgs().length<2) return ;
            Object newObjBean = jp.getArgs()[0];
            Object paramMap = jp.getArgs()[1];
            List<ApprLmtChgFiledListDto> apprLmtChgFiledListDtos = new ArrayList<>() ;
            ApprLmtChgDetail apprLmtChgDetail = comm4Service.initApprLmtChgDetail((Map<String,String>) paramMap) ;
            Field[] fields = newObjBean.getClass().getDeclaredFields();
            String getPkMethod = getPkMethod(newObjBean) ;
            String pkId = Optional.ofNullable(newObjBean.getClass().getDeclaredMethod(getPkMethod).invoke(newObjBean))
                    .orElse("0")
                    .toString();

            Object serviceInst = getServiceClass(newObjBean) ;
            Method method = serviceInst.getClass().getDeclaredMethod("selectByPrimaryKey",String.class) ;
            method.setAccessible(true);
            Object origiService = method.invoke(serviceInst, pkId) ;

            log.info("原始数据为【{}】", JSON.toJSONString(origiService));
            log.info("最新数据为【{}】", JSON.toJSONString(newObjBean));

            for (Field field : fields) {
                String fieldName = field.getName();
                if (AMT_SET.contains(fieldName)) {
                    String getMethod = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                    String oldfileValue = Optional.ofNullable(origiService.getClass().getDeclaredMethod(getMethod).invoke(origiService))
                            .orElse("0")
                            .toString();

                    String newfileValue = Optional.ofNullable(newObjBean.getClass().getDeclaredMethod(getMethod).invoke(newObjBean))
                            .orElse("0")
                            .toString();
                    if(new BigDecimal(oldfileValue).compareTo(new BigDecimal(newfileValue)) != 0){
                        comm4Service.initLmtChgFiled(fieldName , new BigDecimal(oldfileValue) ,new BigDecimal(newfileValue) ,apprLmtChgFiledListDtos);
                    }
                }
            }

            if(apprLmtChgFiledListDtos.size()>0){
                String classPath = newObjBean.getClass().getName() ;
                String tableName = classPath.substring(classPath.lastIndexOf(".")+1) ;
                log.info("【表名】:" + newObjBean.getClass().getName());
                apprLmtChgDetail.setUpdTableName(tableName);
                apprLmtChgDetail.setPkIdRel(pkId);
                comm4Service.createApprLmtChgFileRecord7(apprLmtChgFiledListDtos ,apprLmtChgDetail);
            }
        } catch (Exception e) {
            log.error("获取字段值报错", e);
        }
    }

    public Object getServiceClass(Object obj){
        if(obj instanceof ApprLmtSubBasicInfo){
            return apprLmtSubBasicInfoService ;
        }else if(obj instanceof LmtContRel){
            return lmtContRelService ;
        }else if(obj instanceof LmtWhiteInfo){
            return lmtWhiteInfoService ;
        }else if(obj instanceof LmtContRel){
            return lmtContRelService ;
        }else if(obj instanceof ContAccRel){
            return contAccRelService ;
        }else if(obj instanceof ApprCoopSubInfo){
            return apprCoopSubInfoService ;
        }else if(obj instanceof LmtDiscOrg){
            return lmtDiscOrgService ;
        }
        return null ;
    }

    public String getPkMethod(Object obj){
        if(obj instanceof LmtDiscOrg){
            return "getSerno" ;
        }else{
            return "getPkId" ;
        }
    }
}
