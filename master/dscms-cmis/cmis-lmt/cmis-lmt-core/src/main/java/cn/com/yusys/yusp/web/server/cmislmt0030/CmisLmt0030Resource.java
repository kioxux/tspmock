package cn.com.yusys.yusp.web.server.cmislmt0030;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0030.req.CmisLmt0030ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0030.resp.CmisLmt0030RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0030.CmisLmt0030Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户标准资产授信余额
 *
 * @author dumd 20210616
 * @version 1.0
 */
@Api(tags = "cmislmt0030:查询客户标准资产授信余额")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0030Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0030Resource.class);

    @Autowired
    private CmisLmt0030Service cmisLmt0030Service;

    /**
     * 交易码：CmisLmt0030
     * 交易描述：查询客户标准资产授信余额
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("查询客户标准资产授信余额")
    @PostMapping("/cmislmt0030")
    protected @ResponseBody
    ResultDto<CmisLmt0030RespDto> CmisLmt0030(@Validated @RequestBody CmisLmt0030ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0030.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0030.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0030RespDto> CmisLmt0030RespDtoResultDto = new ResultDto<>();
        CmisLmt0030RespDto CmisLmt0030RespDto = new CmisLmt0030RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0030RespDto = cmisLmt0030Service.execute(reqDto);
            CmisLmt0030RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0030RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0030.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0030.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0030.value, e.getMessage());
            // 封装CmisLmt0030RespDtoResultDto中异常返回码和返回信息
            CmisLmt0030RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0030RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0030RespDto到CmisLmt0030RespDtoResultDto中
        CmisLmt0030RespDtoResultDto.setData(CmisLmt0030RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0030.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0030.value, JSON.toJSONString(CmisLmt0030RespDtoResultDto));
        return CmisLmt0030RespDtoResultDto;
    }
}