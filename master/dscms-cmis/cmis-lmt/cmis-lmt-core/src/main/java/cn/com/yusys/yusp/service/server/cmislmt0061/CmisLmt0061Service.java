package cn.com.yusys.yusp.service.server.cmislmt0061;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0061.req.CmisLmt0061ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0061.resp.CmisLmt0061RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0061Service
 * @类描述: #对内服务类
 * @功能描述: 根据客户号获取客户综合授信批复编号，到期日，起始日，期限等
 * @创建时间: 2021-09-30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0061Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0061Service.class);

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService ;

    /**
     * 获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）
     * add by lizx 2021-09-09
     * @param reqDto
     * @return
     */
    @Transactional
    public CmisLmt0061RespDto execute(CmisLmt0061ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0061.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0061.value);
        CmisLmt0061RespDto respDto = new CmisLmt0061RespDto();
        try {
            //客户号
            String cusId = reqDto.getCusId();
            //查询类型
            String queryType = reqDto.getQueryType();
            //金融组织代码
            String instuCde = reqDto.getInstuCde();
            //客户类型 1-对私客户  2-对公客户  3-同业  4-集团
            String cusType = reqDto.getCusType();

            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId", cusId);
            queryModel.addCondition("lmtType", queryType);
            queryModel.addCondition("instuCde", instuCde);
            queryModel.addCondition("cusType", cusType);
            ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfoService.selectByParamCusId(queryModel) ;
            if(Objects.nonNull(apprStrMtableInfo)){
                respDto.setAccNo(apprStrMtableInfo.getApprSerno());
                respDto.setStartDate(apprStrMtableInfo.getStartDate());
                respDto.setEndDate(apprStrMtableInfo.getEndDate());
                Integer term = apprStrMtableInfo.getTerm() ;
                if(term==null) term = 0 ;
                respDto.setTerm(term.toString());
            }
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("【cmislmt0061】获取客户或分项明细用信综合总额和用信敞口余额：" , e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0061.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0061.value);
        return respDto;
    }
}