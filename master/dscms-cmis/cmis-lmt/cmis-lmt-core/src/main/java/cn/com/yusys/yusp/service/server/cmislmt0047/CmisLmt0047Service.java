package cn.com.yusys.yusp.service.server.cmislmt0047;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047LmtSubDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.req.CmisLmt0047ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047ContRelDtoList;
import cn.com.yusys.yusp.dto.server.cmislmt0047.resp.CmisLmt0047RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.LmtSubPrdMappConfService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0047Service
 * @类描述: #对内服务类
 * @功能描述: 根据分项编号和用信产品编号，查询分项向下是否存在适用改产品的明
 * @修改备注: 2021/8/26     lizx   新增接口
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0047Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0047Service.class);
    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;
    @Autowired
    private LmtContRelService lmtContRelService ;

    @Transactional
    public CmisLmt0047RespDto execute(CmisLmt0047ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0047.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0047.value);
        CmisLmt0047RespDto resqDto = new CmisLmt0047RespDto() ;
        List<CmisLmt0047ContRelDtoList> contRelDtoList = new ArrayList<>();
        try {
            Supplier<CmisLmt0047ContRelDtoList> supplier = CmisLmt0047ContRelDtoList :: new  ;
            String queryType = reqDto.getQueryType() ;
            if(StringUtils.isBlank(queryType)){
                throw new YuspException(EclEnum.ECL070133.key, "查询类型" + EclEnum.ECL070133.value);
            }
            List<CmisLmt0047LmtSubDtoList> lmtSubDtoLists = reqDto.getLmtSubDtoList() ;
            if(CollectionUtils.isEmpty(lmtSubDtoLists)){
                throw new YuspException(EclEnum.ECL070133.key, "编号列表" + EclEnum.ECL070133.value);
            }
            //如果是额度分项
            if(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_01.equals(queryType)){
                for (CmisLmt0047LmtSubDtoList lmtSubDto : lmtSubDtoLists) {
                    String accSubNo = lmtSubDto.getAccSubNo() ;
                    //查询额度分项向下业务
                    List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByLimitSubNo(accSubNo,
                            CmisLmtConstants.STD_ZB_BIZ_STATUS_STR) ;
                    if(CollectionUtils.isNotEmpty(lmtContRelList)){
                        for (LmtContRel contRel : lmtContRelList) {
                            CmisLmt0047ContRelDtoList cmisLmt0047ContRelDto = supplier.get() ;
                            cmisLmt0047ContRelDto.setAccSubNo(accSubNo);
                            cmisLmt0047ContRelDto.setDealBizNo(contRel.getDealBizNo());
                            cmisLmt0047ContRelDto.setPrdName(contRel.getPrdName());
                            cmisLmt0047ContRelDto.setSubType(queryType);
                            contRelDtoList.add(cmisLmt0047ContRelDto);
                        }
                    }

                    //获取该分项向下的分项明细编号
                    String apprSubNos = apprLmtSubBasicInfoService.selectApprSubSernoArrayByParentId(accSubNo) ;

                    if(StringUtils.isNotEmpty(apprSubNos)){
                        lmtContRelList = lmtContRelService.selectLmtContRelByLimitSubNo(apprSubNos,
                                CmisLmtConstants.STD_ZB_BIZ_STATUS_STR) ;
                        if(CollectionUtils.isNotEmpty(lmtContRelList)){
                            for (LmtContRel contRel : lmtContRelList) {
                                CmisLmt0047ContRelDtoList cmisLmt0047ContRelDto = supplier.get() ;
                                cmisLmt0047ContRelDto.setAccSubNo(accSubNo);
                                cmisLmt0047ContRelDto.setDealBizNo(contRel.getDealBizNo());
                                cmisLmt0047ContRelDto.setPrdName(contRel.getPrdName());
                                cmisLmt0047ContRelDto.setSubType(queryType);
                                contRelDtoList.add(cmisLmt0047ContRelDto);
                            }
                        }
                    }
                }
            }else{
                //查询额度分项向下业务
                for (CmisLmt0047LmtSubDtoList lmtSubDto : lmtSubDtoLists) {
                    String accSubNo = lmtSubDto.getAccSubNo() ;
                    //查询额度分项向下业务
                    List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByLimitSubNo(accSubNo,
                            CmisLmtConstants.STD_ZB_BIZ_STATUS_STR) ;
                    if(CollectionUtils.isNotEmpty(lmtContRelList)){
                        for (LmtContRel contRel : lmtContRelList) {
                            CmisLmt0047ContRelDtoList cmisLmt0047ContRelDto = supplier.get() ;
                            cmisLmt0047ContRelDto.setAccSubNo(accSubNo);
                            cmisLmt0047ContRelDto.setDealBizNo(contRel.getDealBizNo());
                            cmisLmt0047ContRelDto.setPrdName(contRel.getPrdName());
                            cmisLmt0047ContRelDto.setSubType(queryType);
                            contRelDtoList.add(cmisLmt0047ContRelDto);
                        }
                    }
                }
            }
            //将列表放入返回中
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            resqDto.setContRelDtoList(contRelDtoList);
        } catch (YuspException e) {
            logger.error("判断是否存在未结清业务：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0047.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0047.value);
        return resqDto;
    }

}