/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.ManaBusiLmt;
import cn.com.yusys.yusp.vo.ManaBusiLmtHisVo;
import cn.com.yusys.yusp.vo.ManaBusiLmtVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ManaBusiLmtHis;
import cn.com.yusys.yusp.repository.mapper.ManaBusiLmtHisMapper;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ManaBusiLmtHisService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 15:32:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ManaBusiLmtHisService {

    @Autowired
    private ManaBusiLmtHisMapper manaBusiLmtHisMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ManaBusiLmtHis selectByPrimaryKey(String pkId, String busiType) {
        return manaBusiLmtHisMapper.selectByPrimaryKey(pkId, busiType);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ManaBusiLmtHis> selectAll(QueryModel model) {
        List<ManaBusiLmtHis> records = (List<ManaBusiLmtHis>) manaBusiLmtHisMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ManaBusiLmtHis> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ManaBusiLmtHis> list = manaBusiLmtHisMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectListByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<ManaBusiLmtHisVo> selectListByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ManaBusiLmtHis> manaBusiLmtHis = manaBusiLmtHisMapper.selectByModel(model);
        List<ManaBusiLmtHisVo> manaBusiLmtHisVoList = new ArrayList<>();
        if (CollectionUtils.nonEmpty(manaBusiLmtHis)){
            manaBusiLmtHis.stream().forEach(a->{
                ManaBusiLmtHisVo manaBusiLmtHisVo = new ManaBusiLmtHisVo();
                BeanUtils.copyProperties(a,manaBusiLmtHisVo);

                //月末贷款余额测算
                BigDecimal curMonthLoanBalancePlan = a.getLastMonthLoanBalance().add(a.getCurrMonthAllowAddAmt());
                manaBusiLmtHisVo.setCurMonthLoanBalancePlan(curMonthLoanBalancePlan);

                manaBusiLmtHisVoList.add(manaBusiLmtHisVo);
            });
        }
        PageHelper.clearPage();
        return manaBusiLmtHisVoList;
    }

    /**
     * 异步导出业务条线额度管控历史数据
     * @return 导出进度信息
     */
    public ProgressDto asyncExportManaBusiLmtHis(QueryModel model) {
        model.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
        DataAcquisition dataAcquisition = (page, size, object) -> {
            List<ManaBusiLmtHisVo> manaBusiLmtHisVoList = new ArrayList<>();
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            List<ManaBusiLmtHis> manaBusiLmtHis = selectByModel(queryModeTemp);
            if (CollectionUtils.nonEmpty(manaBusiLmtHis)){
                manaBusiLmtHis.stream().forEach(a->{
                    ManaBusiLmtHisVo manaBusiLmtHisVo = new ManaBusiLmtHisVo();
                    BeanUtils.copyProperties(a,manaBusiLmtHisVo);

                    //月末贷款余额测算 scope.row.lastMonthLoanBalance + scope.row.currMonthAllowAddAmt
                    BigDecimal curMonthLoanBalancePlan = a.getLastMonthLoanBalance().add(a.getCurrMonthAllowAddAmt());
                    manaBusiLmtHisVo.setCurMonthLoanBalancePlan(curMonthLoanBalancePlan);

                    manaBusiLmtHisVoList.add(manaBusiLmtHisVo);
                });
            }
            return manaBusiLmtHisVoList;
        };
        ExportContext exportContext = ExportContext.of(ManaBusiLmtHisVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ManaBusiLmtHis record) {
        return manaBusiLmtHisMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ManaBusiLmtHis record) {
        return manaBusiLmtHisMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ManaBusiLmtHis record) {
        return manaBusiLmtHisMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ManaBusiLmtHis record) {
        return manaBusiLmtHisMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String busiType) {
        return manaBusiLmtHisMapper.deleteByPrimaryKey(pkId, busiType);
    }

}
