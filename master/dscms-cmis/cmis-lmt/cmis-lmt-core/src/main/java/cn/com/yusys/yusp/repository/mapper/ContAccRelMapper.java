/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.LmtContRel;
import feign.QueryMap;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.ContAccRel;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

import javax.management.Query;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ContAccRelMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-05-03 10:07:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface ContAccRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    ContAccRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByTranAccNo
     * @方法描述: 根据台账编号查询数据信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    ContAccRel selectByTranAccNo(@Param("tranAccNo") String tranAccNo);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<ContAccRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(ContAccRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(ContAccRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(ContAccRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(ContAccRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectOutstandBal
     * @方法描述: 根据分项编号，判断该分项是否存在未结清的业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal selectOutstandBal(@Param("limitSubNo") String limitSubNo) ;

    /**
     * @方法名称: judgeIsExistOutstandingLoan
     * @方法描述: 根据台账交易编号，判断该分项是否存在未结清的业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    Map<String, BigDecimal> getContAccRelBal(QueryModel queryModel) ;

    /**
     * 额度视图列表，根据合同号查询合同号下台账列表
     * @author zhangjw 2021-05-05
     * @return
     */
    List<ContAccRel> queryListByDealBizNo(QueryModel model);


    /**
     * 根据合同编号，查询合同向下是否存在未结清的业务
     * @param dealBizNo
     * @return
     */
    List<ContAccRel> queryUnsettleListByDealBizNo(@Param("dealBizNo") String dealBizNo);

    /**
     * 检查合同编号是否存在
     * @param dealBizNo
     * @return
     */
    int checkSameDealBizNoIsExist(@Param("dealBizNo") String dealBizNo);

    /**
     * 根据合同编号查询项下台账总余额
     * @param dealBizNo
     * @return
     */
    BigDecimal getTotalAccTotalBalanceAmtCnyByDealBizNo(@Param("dealBizNo") String dealBizNo);


    /**
     * @方法名称: selectAccTotalBalByLimitSubNo
     * @方法描述: 根据分项编号，判断该分项是否存在未结清的业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal selectAccTotalBalByLimitSubNo(@Param("limitSubNos") String limitSubNos) ;

    /**
     * @方法名称: selectByTranAccNoAndDealNo
     * @方法描述: 根据根据加以编号和合同编号查询该交易是否已经成功
     * @参数与返回说明:
     * @算法描述: 无
     */
    ContAccRel selectByTranAccNoAndDealNo(QueryModel queryModel) ;

    /**
     * @方法名称: selectByTranAccNoAndDealNo
     * @方法描述: 判断合同编号项下是否存在未结清的业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    String checkDealBizNoHasBussBalanceFlag(String dealBizNo) ;

    /**
     * @方法名称: selectAccTotalByLimitSubNos
     * @方法描述: 根据分项编号们，获取向下贷款总额，不包含结清
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal selectAccTotalByLimitSubNos(@Param("limitSubNos") String limitSubNos) ;

    BigDecimal selectSpacAccBalByLimitSubNos(@Param("limitSubNos") String limitSubNos) ;

    /**
     * @方法名称: selectContUseAmtByDealBizNo
     * @方法描述: 根据交易业务编号，获取合同项下台账总占用
     * @参数与返回说明:
     * @算法描述: 无
     */
    Map selectContUseAmtByDealBizNo(QueryModel paramQuery) ;

    /**
     * @方法名称: selectRevolvSpceBalByLimitSubNo
     * @方法描述: 根据分项编号们，获取向下敞口贷款余额（循环分项）
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal selectRevolvSpceBalByLimitSubNo(QueryModel queryModel) ;

    /**
     * @方法名称: selectUnRevolvSpceBalByLimitSubNo
     * @方法描述: 根据分项编号们，获取向下敞口贷款余额（不可循环分项）
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal selectUnRevolvSpceBalByLimitSubNo(QueryModel queryModel) ;
}