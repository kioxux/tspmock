package cn.com.yusys.yusp.web.server.cmislmt0057;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0057.req.CmisLmt0057ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0057.resp.CmisLmt0057RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0057.CmisLmt0057Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:线上产品自动生成合同台账
 *
 * @author lizx 20210909
 * @version 1.0
 */
@Api(tags = "cmislmt0057:线上产品自动生成合同台账")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0057Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0057Resource.class);

    @Autowired
    private CmisLmt0057Service cmisLmt0057Service;

    /**
     * 交易码：CmisLmt0057
     * 交易描述：获取客户有效的低风险额度分项明细编号
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("线上产品自动生成合同台账")
    @PostMapping("/cmislmt0057")
    protected @ResponseBody
    ResultDto<CmisLmt0057RespDto> CmisLmt0057(@Validated @RequestBody CmisLmt0057ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0057.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0057RespDto> CmisLmt0057RespDtoResultDto = new ResultDto<>();
        CmisLmt0057RespDto CmisLmt0057RespDto = new CmisLmt0057RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0057RespDto = cmisLmt0057Service.execute(reqDto);
            CmisLmt0057RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0057RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (YuspException yuspe) {
            logger.error("线上产品自动生成合同台账：",yuspe);
            CmisLmt0057RespDto.setErrorCode(yuspe.getCode());
            CmisLmt0057RespDto.setErrorMsg(yuspe.getMsg());
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0057.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0057.value, e.getMessage());
            // 封装CmisLmt0057RespDtoResultDto中异常返回码和返回信息
            CmisLmt0057RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0057RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0057RespDto到CmisLmt0057RespDtoResultDto中
        CmisLmt0057RespDtoResultDto.setData(CmisLmt0057RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.value, JSON.toJSONString(CmisLmt0057RespDtoResultDto));
        return CmisLmt0057RespDtoResultDto;
    }
}