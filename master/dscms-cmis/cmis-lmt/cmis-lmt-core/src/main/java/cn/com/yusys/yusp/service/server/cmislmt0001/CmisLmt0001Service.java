package cn.com.yusys.yusp.service.server.cmislmt0001;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.domain.ApprStrMtableInfo;
import cn.com.yusys.yusp.domain.LmtContRel;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import cn.com.yusys.yusp.service.ContAccRelService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.server.comm.Comm4LmtCalFormulaUtils;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0001Service
 * @类描述: #对内服务类
 * @功能描述: 法人额度同步
 * @创建时间: 2021-05-07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0001Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0001Service.class);

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService;

    @Autowired
    private ContAccRelService contAccRelService ;

    @Autowired
    private LmtContRelService lmtContRelService ;

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper ;

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper ;

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private Comm4LmtCalFormulaUtils comm4LmtCalFormulaUtils ;

    @Transactional
    public CmisLmt0001RespDto execute(CmisLmt0001ReqDto reqDto) throws YuspException,Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value);
        CmisLmt0001RespDto resqDto = new CmisLmt0001RespDto() ;
        //批复主信息整理
        // 批复编号
        String accNo = reqDto.getAccNo() ;
        Map<String, String> paramtMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
        Map<String, String> resultMap = new HashMap<>() ;
        resultMap.putAll(paramtMap);
        resultMap.put("serno", accNo) ;
        resultMap.put("serviceCode", CmisLmtConstants.CMIS_LMT0001_SERVICE_CODE) ;

        //存放一级分项编号
        List<String> subList = new ArrayList<>();
        //存放二级分项产品编号
        List<String> subDetailList = new ArrayList<>();
        /**** 非空检验 ****/
        ifNotFiledCheck(reqDto);

        /** 校验是否允许进行额度建立 **/
        checkOutIsAllowCreate(reqDto) ;

        // 打印日志，记录流水号
        logger.info("CmisLmt0001额度建立接口，建立主表信息数据，批复编号："+accNo) ;
        // 授信模式  01-综合授信 02-单笔单批
        //String lmtMode = reqDto.getLmtMode() ;
        // 是否生成新批复编号
        String isCreateAcc = reqDto.getIsCreateAcc() ;
        //原批复编号
        String origiAccNo = reqDto.getOrigiAccNo() ;
        //获取集团客户编号
        String grpNo = comm4Service.selectGrpNoByCusId(reqDto.getCusId());

        /**************处理主表信息********************/
        // 是否生成新批复，主表数据处理，主表数据直接更新未失效，分项表查看是否有未结清业务如果有则挂到新分项下，此处放在分项处处理
        if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
            apprStrMtableInfoService.createNewAccExtracted(origiAccNo, accNo);
        }
        //初始化批复主表对象信息
        ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfoService.getAppStrMTableInfoBySerno(accNo) ;
        //判断是否变更操作
        if(apprStrMtableInfo != null){
            chgApprBasicInfo(reqDto, apprStrMtableInfo, accNo) ;
        }else{
            insertApprBasicInfo(reqDto, accNo) ;
        }

        /**************处理分项信息********************/
        //利用工厂接口创建实例
        Supplier<ApprLmtSubBasicInfo> supplier = ApprLmtSubBasicInfo::new ;
        for(CmisLmt0001LmtSubListReqDto lmtSub : reqDto.getLmtSubList()){
            //新分项编号
            String accSubNo = lmtSub.getAccSubNo() ;
            //原分项编号
            String origiAccSubNo = lmtSub.getOrigiAccSubNo() ;
            if(lmtSub.getOprType()==null) lmtSub.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
            //如果是删除分项，必须无未结清业务
            if(CmisLmtConstants.OPR_TYPE_DELETE.equals(lmtSub.getOprType())){
                //如果存在未结清业务，则不允许删除操作
                if(comm4Service.isExistsUnSettleTwo(accSubNo, lmtSub.getLmtSubType())){
                    throw new YuspException(EclEnum.ECL070023.key, "【"+accSubNo+"】，"+EclEnum.ECL070023.value);
                }
            }

            //获取授信总额，敞口金额
            //可用总额
            BigDecimal avlAmt = BigDecimal.ZERO ;
            BigDecimal spacAmt = BigDecimal.ZERO ;
            //可出账金额
            BigDecimal avlOutstndAmt = BigDecimal.ZERO ;
            //敞口可用额度
            if(CmisLmtConstants.YES_NO_Y.equals(lmtSub.getIsLriskLmt())){
                spacAmt = BigDecimal.ZERO ;
                avlAmt = lmtSub.getAvlamt() ;
                //可出账金额
                avlOutstndAmt = avlAmt ;
            }else{
                spacAmt = lmtSub.getAvlamt();
                avlAmt = getAvlAmt(reqDto, lmtSub) ;
                BigDecimal preRate = BigDecimalUtil.replaceNull(lmtSub.getBailPreRate()) ;
                if(preRate.compareTo(BigDecimal.ONE)==0){
                    throw new YuspException(EclEnum.ECL070119.key, EclEnum.ECL070119.value);
                }
                //可出账金额
                avlOutstndAmt = spacAmt ;
            }
            logger.info("【"+lmtSub.getAccSubNo()+"】分项：分项可用总额【avlAmt】："+avlAmt+",敞口可用总额【spacAmt】："+spacAmt);

            //获取分项批复实例对象
            ApprLmtSubBasicInfo apprLmtSubBasicInfo = supplier.get() ;
            //拷贝数据
            BeanUtils.copyProperties(lmtSub, apprLmtSubBasicInfo);
            apprLmtSubBasicInfo.setAvlAmt(lmtSub.getAvlamt());
            apprLmtSubBasicInfo.setIsLriskLmt(lmtSub.getIsLriskLmt());
            apprLmtSubBasicInfo.setIsRevolv(lmtSub.getIsRevolv());
            apprLmtSubBasicInfo.setIsPreCrd(lmtSub.getIsPreCrd());
            ApprLmtSubBasicInfo origiApprLmtSub =  apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(accSubNo) ;

            //如果是以新批复覆盖老批复isCreateAcc && 原分项编号不为空，则判断原分项编号是否存在未结清业务，存在则将未结清业务挂到新批复下，
            // 不存则直接更改状态未 99-失效已结清
            Map<String, BigDecimal> subAmtMap = new HashedMap() ;
            if(CmisLmtConstants.YES_NO_Y.equals(isCreateAcc)){
                if(StringUtils.isNotEmpty(origiAccSubNo)){
                    logger.info("创建新批复，处理老批复信息，原批复分项编号【{}】分项：", origiAccSubNo);
                    //获取原分项对象
                    ApprLmtSubBasicInfo apprLmtSubBasicInfoOld = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(origiAccSubNo) ;
                    Optional.ofNullable(apprLmtSubBasicInfoOld).orElseThrow(()->new YuspException(EclEnum.ECL070007.key,EclEnum.ECL070007.value));
                    //将原分项下业务挂靠到新分项下
                    comm4Service.updateLmtContRelRelSubNo(origiAccSubNo, accSubNo);
                    subAmtMap = createAccAmt(apprLmtSubBasicInfoOld) ;
                    //更改原批复状态未 99-失效已结清
                    apprLmtSubBasicInfoOld.setStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                    apprLmtSubBasicInfoService.update(apprLmtSubBasicInfoOld) ;
                }
            }else{
                logger.info("额度变更分项信息：【{}】分项：", JSON.toJSONString(origiApprLmtSub));
                //如果是变更授信 变更分项
                if(origiApprLmtSub!=null){
                    subAmtMap = createAccAmt(origiApprLmtSub) ;
                    String pkId = origiApprLmtSub.getPkId()  ;
                    apprLmtSubBasicInfo.setPkId(pkId);
                    apprLmtSubBasicInfo.setInputBrId(origiApprLmtSub.getInputBrId());
                    apprLmtSubBasicInfo.setInputDate(comm4Service.formatDateStr(origiApprLmtSub.getInputDate()));
                    apprLmtSubBasicInfo.setInputId(origiApprLmtSub.getInputId());
                    apprLmtSubBasicInfo.setCreateTime(origiApprLmtSub.getCreateTime());
                }
            }

            //用信余额
            BigDecimal loanBalance = BigDecimalUtil.replaceNull(subAmtMap.get("loanBalance")) ;
            //已用总额
            BigDecimal outstndAmt = BigDecimalUtil.replaceNull(subAmtMap.get("outstndAmt")) ;
            //敞口已用
            BigDecimal spacOutstndAmt = BigDecimalUtil.replaceNull(subAmtMap.get("spacOutstndAmt")) ;
            //用信敞口余额
            BigDecimal loanSpacBalance = BigDecimalUtil.replaceNull(subAmtMap.get("loanSpacBalance")) ;
            //已出帐金额
            BigDecimal pvpOutstndAmt = BigDecimalUtil.replaceNull(subAmtMap.get("pvpOutstndAmt")) ;
            //可出账金额
            avlOutstndAmt = avlOutstndAmt.subtract(pvpOutstndAmt) ;

            //zhangjw20210525 增加处理：如果分项的父节点 == 批复台账编号，则将 分项父节点置空
            String parentId = lmtSub.getParentId();
            if(parentId.equals(accNo)){
                apprLmtSubBasicInfo.setParentId("");
            }

            initLmtSubBasicInfo(reqDto, lmtSub, apprLmtSubBasicInfo);
            //分项可用总额
            apprLmtSubBasicInfo.setGrpNo(grpNo);
            //分项可用总额
            apprLmtSubBasicInfo.setAvlAmt(avlAmt);
            //分项已用总额
            apprLmtSubBasicInfo.setOutstndAmt(outstndAmt);
            //敞口可用
            apprLmtSubBasicInfo.setSpacAmt(spacAmt);
            //敞口已用
            apprLmtSubBasicInfo.setSpacOutstndAmt(spacOutstndAmt);
            //用信余额
            apprLmtSubBasicInfo.setLoanBalance(loanBalance);
            //用信敞口余额
            apprLmtSubBasicInfo.setLoanSpacBalance(loanSpacBalance);
            //已出帐金额
            apprLmtSubBasicInfo.setPvpOutstndAmt(pvpOutstndAmt);
            //可出账金额
            apprLmtSubBasicInfo.setAvlOutstndAmt(avlOutstndAmt);
            //累加金额
            //apprLmtSubBasicInfo.setLmtAmtAdd(lmtAmtAdd);
            //授信品种类型属性
            apprLmtSubBasicInfo.setLmtBizTypeProp(lmtSub.getLmtBizTypeProp());
            //宽限期如果是空，则默认未宽限起为0
            if(apprLmtSubBasicInfo.getLmtGraper()==null) apprLmtSubBasicInfo.setLmtGraper(0);

            //判断分项编号，是一级分项还是二级产品分项，计算累加金额时适用
            if(StringUtils.isBlank(apprLmtSubBasicInfo.getParentId())){
                subList.add(accSubNo);
            }else{
                subDetailList.add(accSubNo) ;
            }

            //判断分项是否存在，如果存在，则变更，更新，不存在，新增操作
            if(origiApprLmtSub!=null){
                apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;
                //累加金额处理
                //comm4LmtCalFormulaUtils.calLmtAmtAddExecute(accSubNo);
            }else{
                apprLmtSubBasicInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));//创建时间
                //可出账金额取授信总额
                apprLmtSubBasicInfo.setAvlOutstndAmt(avlOutstndAmt);
                //主键
                apprLmtSubBasicInfo.setPkId(comm4Service.generatePkId());
                //操作类型 默认 01--新增
                apprLmtSubBasicInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                //登记人
                apprLmtSubBasicInfo.setInputId(reqDto.getInputId());
                //登记机构
                apprLmtSubBasicInfo.setInputBrId(reqDto.getInputBrId());
                //登记日期
                apprLmtSubBasicInfo.setInputDate(comm4Service.formatDateStr(reqDto.getInputDate()));
                //创建日期
                apprLmtSubBasicInfo.setUpdateTime(comm4Service.getCurrrentDate());
                apprLmtSubBasicInfoMapper.insert(apprLmtSubBasicInfo) ;
                //comm4LmtCalFormulaUtils.calLmtAmtAddExecute(accSubNo);
            }
        };

        //update by 2021-11-22 授信变更时，考虑到授信金额可能变小，低风险回出现累加金额，所以此处仅从累加金额处理
        logger.info("处理累加金额----------------------start");
        if(CollectionUtils.isNotEmpty(subDetailList)){
            for (String subNo : subDetailList) {
                comm4LmtCalFormulaUtils.calLmtAmtAddExecute(subNo);
            }
        }

        if(CollectionUtils.isNotEmpty(subList)){
            for (String subNo : subList) {
                comm4LmtCalFormulaUtils.calLmtAmtAddExecute(subNo);
                comm4LmtCalFormulaUtils.calParentIdLmtAmtAddExecute(subNo);
            }
        }
        logger.info("处理累加金额----------------------end");

        resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
        resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0001.value);
        return resqDto;
    }

    /**
     * @作者:lizx
     * @方法名称: initLmtSubBasicInfo
     * @方法描述: 组装是初始化批复分项公用字段信息
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/6 9:51
     * @param reqDto: 
     * @param lmtSub: 
     * @param apprLmtSubBasicInfo: 
     * @return: void
     * @算法描述: 无
    */
    private void initLmtSubBasicInfo(CmisLmt0001ReqDto reqDto, CmisLmt0001LmtSubListReqDto lmtSub, ApprLmtSubBasicInfo apprLmtSubBasicInfo) {
        //客户号
        apprLmtSubBasicInfo.setCusId(reqDto.getCusId());
        //客户名称
        apprLmtSubBasicInfo.setCusName(reqDto.getCusName());
        //客户类型
        apprLmtSubBasicInfo.setCusType(reqDto.getCusType());
        //分项批复编号
        apprLmtSubBasicInfo.setApprSubSerno(lmtSub.getAccSubNo());
        //到日期
        apprLmtSubBasicInfo.setLmtDate(lmtSub.getEndDate());
        //台账状态
        apprLmtSubBasicInfo.setStatus(lmtSub.getAccSubStatus());
        //授信模式
        apprLmtSubBasicInfo.setLimitType(reqDto.getLmtMode());
        //外健- 关联到批复台账编号
        apprLmtSubBasicInfo.setFkPkid(reqDto.getAccNo());
        //更新人
        apprLmtSubBasicInfo.setUpdId(reqDto.getInputId());
        //更新机构
        apprLmtSubBasicInfo.setUpdBrId(reqDto.getInputBrId());
        apprLmtSubBasicInfo.setUpdateTime(comm4Service.getCurrrentDate());
        apprLmtSubBasicInfo.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
    }

    /**
     * @方法名称: sortByBailPreRateDesc
     * @方法描述: 根据保证金比例，对集合进行排序
     * @参数与返回说明:
     * @算法描述: 无
     */
    private static void sortByBailPreRateDesc(List<CmisLmt0001LmtSubListReqDto> apprLmtSubBasicInfoList) {
        apprLmtSubBasicInfoList.sort(
            Comparator.comparing(CmisLmt0001LmtSubListReqDto::getBailPreRate, (d1, d2) -> d2.compareTo(d1))
        );
    }

    /**
     * @方法名称: getAvlAmt
     * @方法描述: 获取授信总额，敞口金额
     * @参数与返回说明: 返回授信总额
     * @算法描述: 如果是额度分项
     */
    public BigDecimal getAvlAmt(CmisLmt0001ReqDto reqDto, CmisLmt0001LmtSubListReqDto sublmt){
        Map<String, BigDecimal> amtMap = new HashMap<>() ;
        //授信总额
        BigDecimal avlAmt = BigDecimal.ZERO ;
        //授信总额
        BigDecimal lmtAmt = BigDecimalUtil.replaceNull(sublmt.getAvlamt()) ;
        //分项编号
        String accSubNo = sublmt.getAccSubNo() ;
        //计算可用总额
        List<CmisLmt0001LmtSubListReqDto> lmt0001LmtSubListReqDtos = new ArrayList<>();
        //遍历列表信息，获取到父级目录是当前【subSerno】的二级分项数据
        for(CmisLmt0001LmtSubListReqDto lmtSubappr: reqDto.getLmtSubList()) {
            //遍历分项信息，排除当前风险信息，查看该分项下是挂有子分项信息
            if(StringUtils.isNotEmpty(accSubNo)){
                if(accSubNo.equals(lmtSubappr.getParentId())){
                    //如果二级分项保证金比例为空，赋值为0
                    if(lmtSubappr.getBailPreRate()==null){
                        lmtSubappr.setBailPreRate(BigDecimal.ZERO);
                    }
                    lmt0001LmtSubListReqDtos.add(lmtSubappr) ;
                }
            }else{
                continue ;
            }
        }

        if(lmt0001LmtSubListReqDtos.size()>0){
            List<Map<String, Object>> subList = new ArrayList<>();
            for (CmisLmt0001LmtSubListReqDto lmt0001Sub : lmt0001LmtSubListReqDtos) {
                Map<String, Object> map = new HashMap<>();
                map.put("bailPreRate", lmt0001Sub.getBailPreRate()) ;
                map.put("AvlAmt", lmt0001Sub.getAvlamt()) ;
                map.put("apprSubSerno", lmt0001Sub.getAccSubNo()) ;
                subList.add(map);
            }
            avlAmt = comm4LmtCalFormulaUtils.calAvlLmtTotal(subList, lmtAmt) ;
        }else{
            Map<String, Object> map = new HashMap<>();
            map.put("bailPreRate", sublmt.getBailPreRate()) ;
            map.put("AvlAmt", sublmt.getAvlamt()) ;
            map.put("apprSubSerno", sublmt.getAccSubNo()) ;
            avlAmt = comm4LmtCalFormulaUtils.calDetailAvlLmtTotal(map) ;
        }
        return avlAmt ;
    }


    /**
     * @方法名称: getNewOutstndAmt
     * @方法描述: 额度同步，分项发生变更或者覆盖，如果分项是否可循环发生改变；从不可循环改变到可循环，则需要重新计算已用总额
     * @参数与返回说明:返回计算后的已用总额
     * @算法描述: 计算公式：新已用额度 = ∑（额度下合同状态为生效的最高额合同金额）+
     *                                  ∑（额度下合同状态为生效的一般合同余额+可出账金额(合同可出账金额)）+∑（额度下已到期合同余额）
     * 最高额合同只会挂到一级分项额度上
     * 合同可出账金额=一般合同总额-合同向下台账总额汇总
     * 一般合同已用余额：合同向下台账占用余额
     * 额度已到期合同余额：已到期未结清合同向下业务余额
     */
    public BigDecimal getNewOutstndAmt(ApprLmtSubBasicInfo apprLmtSubBasicInfo) {
        //分项总金额
        BigDecimal lmtAmt = apprLmtSubBasicInfo.getAvlAmt() ;
        //获取合同状态为生效的最高额合同金额
        Map<String, BigDecimal> highContAmtMap =
                lmtContRelService.getNorStatusLmtContCnyBal(apprLmtSubBasicInfo.getApprSubSerno(),
                        CmisLmtConstants.STD_ZB_CONT_TYPE_2,
                CmisLmtConstants.STD_ZB_CONT_STATUS_200, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
        //获取已用
        BigDecimal highContAmt = highContAmtMap.get("norCnyBal") ;
        //额度下合同状态为生效的一般合同余额+可出账金额(合同可出账金额)
        //根据分项编号，查询向下生效的一般合同，遍历借款合同，获取合同下的台账占用，根据调账占用获取
        //获取分项下有效的一般担保合同
        List<LmtContRel> lmtContRelList =
                lmtContRelService.selectLmtContRelByLimitSubNo(apprLmtSubBasicInfo.getApprSubSerno(),
                        CmisLmtConstants.STD_ZB_CONT_STATUS_200) ;
        BigDecimal accTotal = BigDecimal.ZERO ;
        BigDecimal accAlance = BigDecimal.ZERO ;

        //遍历集合，计算合同余额+可出账金额
        if(CollectionUtils.isNotEmpty(lmtContRelList)){
            for(LmtContRel lmtContRel:lmtContRelList){
                //根据合同编号，获取合同向下的台账占用总额汇总以及余额汇总
                String dealBizNo = lmtContRel.getDealBizNo() ;
                Map<String, BigDecimal> accRelMap = contAccRelService.getContAccRelBal(dealBizNo, CmisLmtConstants.STD_ZB_CONT_STATUS_200) ;
                accTotal = accTotal.add(accRelMap.get("acctotal")) ;
                accAlance = accAlance.add(accRelMap.get("accalance")) ;

            }
        }
        BigDecimal lmtSubAmt = lmtAmt.subtract(accTotal).add(accAlance) ;

        //获取合同状态为生效的最高额合同金额
        Map<String, BigDecimal> invalidContAmtMap =
                lmtContRelService.getNorStatusLmtContCnyBal(apprLmtSubBasicInfo.getApprSubSerno(),null,
                CmisLmtConstants.STD_ZB_CONT_STATUS_400, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;

        BigDecimal invalidContAmt = invalidContAmtMap.get("norCnyBal") ;
        return highContAmt.add(lmtSubAmt).add(invalidContAmt) ;
    }

    /**
     * @方法名称: getLmtAmtAdd
     * @方法描述: 针对额度变更或者额度覆盖，额度累加进行计算后处理
     * @参数与返回说明: 返回计算后的累加金额
     * @算法描述: 计算公式：额度变更后，若同步的授信总额与原授信总额有金额差，则  授信总额累加 =
     *                      授信总额累加 + （原授信总额-新授信总额）;小于0 ，默认为0.
     */
    /*public BigDecimal getLmtAmtAdd(ApprLmtSubBasicInfo apprLmtSubBasicInfo, ApprLmtSubBasicInfo oldApprLmtSubBasicInfo) {
        //原分项授信总额
        BigDecimal oldAvlAmt = BigDecimalUtil.replaceNull(oldApprLmtSubBasicInfo.get()) ;
        //新分项授信总额
        BigDecimal newAvlAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getAvlAmt()) ;
        //原授信分项累加金额
        BigDecimal lmtAmtAdd = BigDecimalUtil.replaceNull(oldApprLmtSubBasicInfo.getLmtAmtAdd()) ;

        if (oldAvlAmt.compareTo(newAvlAmt)==0){
            return lmtAmtAdd;
        }else{
            //根据公式，计算最终累加金额
            BigDecimal newLmtAmtAdd = BigDecimalUtil.replaceNull(lmtAmtAdd.add(oldAvlAmt.subtract(newAvlAmt))) ;
            if(newLmtAmtAdd.compareTo(BigDecimal.ZERO)>0){
                return newLmtAmtAdd ;
            }else{
                return BigDecimal.ZERO ;
            }
        }
    }*/

    /**
     * @方法名称: getLmtAmtAdd
     * @方法描述: 针对额度变更或者额度覆盖，额度累加进行计算后处理
     * @参数与返回说明: 返回计算后的累加金额
     * @算法描述: 计算公式：额度已用总额-最新批复总额  大于0 返回其值；返回0
     * @param:outstndAmt 额度已用总额
     * @param:avlAmt 最新批复金额
     */
    public BigDecimal getLmtAmtAdd(BigDecimal outstndAmt, BigDecimal avlAmt) {
        if(outstndAmt.compareTo(avlAmt)>0){
            return outstndAmt.subtract(avlAmt) ;
        }
        return BigDecimal.ZERO ;
    }

    /**
     * @方法名称: getOutstndAmt
     * @方法描述: 获取总已用接你
     * @参数与返回说明: 返回计算后的累加金额
     * @算法描述: 计算公式：额度变更后，若同步的授信总额与原授信总额有金额差，则  授信总额累加 =
     *                      授信总额累加 + （原授信总额-新授信总额）;小于0 ，默认为0.
     */
    public BigDecimal getOutstndAmt(ApprLmtSubBasicInfo oldapprLSBasicInfo, ApprLmtSubBasicInfo lmtSub){
        BigDecimal outstndAmt = BigDecimal.ZERO ;
        //获取原分项是否可循环信息
        String origiIsRevolv = oldapprLSBasicInfo.getIsRevolv() ;
        //换取最新的分项是否可循环
        String isREvolv = lmtSub.getIsRevolv() ;
        if(CmisLmtConstants.YES_NO_N.equals(origiIsRevolv) && CmisLmtConstants.YES_NO_Y.equals(isREvolv)){
            //如果不可循环变成可循环
            //计算公式：新已用额度 = ∑（额度下合同状态为生效的最高额合同金额）+
            // ∑（额度下合同状态为生效的一般合同余额+可出账金额）+∑（额度下已到期合同余额）
            outstndAmt = getNewOutstndAmt(oldapprLSBasicInfo) ;
        }else{
            //是否可循环未发生改变 || 如果可循环变成不可以循环
            outstndAmt = oldapprLSBasicInfo.getOutstndAmt() ;
        }
        if(outstndAmt==null) outstndAmt = BigDecimal.ZERO ;
        return outstndAmt ;
    }

    /**
     * @方法名称: getSpacOutstndAmt
     * @方法描述: 重新计算敞口已用金额
     * @参数与返回说明: 返回计算后的累加金额
     * @算法描述: 计算公式：额度变更后，若同步的授信总额与原授信总额有金额差，则  授信总额累加 =
     *                      授信总额累加 + （原授信总额-新授信总额）;小于0 ，默认为0.
     */
    public BigDecimal getSpacOutstndAmt(ApprLmtSubBasicInfo oldapprLSBasicInfo, ApprLmtSubBasicInfo lmtSub){
        BigDecimal spacOutstndAmt = BigDecimal.ZERO ;
        //获取原分项是否可循环信息
        String origiIsRevolv = oldapprLSBasicInfo.getIsRevolv() ;
        //换取最新的分项是否可循环
        String isREvolv = lmtSub.getIsRevolv() ;

        if(CmisLmtConstants.YES_NO_N.equals(origiIsRevolv) && CmisLmtConstants.YES_NO_Y.equals(isREvolv)){
            //如果不可循环变成可循环
            //计算公式：新已用额度 = ∑（额度下合同状态为生效的最高额合同金额）+
            // ∑（额度下合同状态为生效的一般合同余额+可出账金额）+∑（额度下已到期合同余额）
            spacOutstndAmt = getNewOutstndAmt(oldapprLSBasicInfo) ;
        }else{
            //是否可循环未发生改变  || 如果可循环变成不可以循环
            spacOutstndAmt = oldapprLSBasicInfo.getOutstndAmt() ;
        }
        if(spacOutstndAmt==null) spacOutstndAmt = BigDecimal.ZERO ;
        return spacOutstndAmt ;
    }


    /**
     * @标注：2021-06-28 分项变更不允许更改是否可循环字段，此处方法暂时不使用
     * @方法名称: getNewSpacOutstndAmt
     * @方法描述: 额度同步，敞口已用金额计算，分项发生变更或者覆盖，如果分项是否可循环发生改变；从不可循环改变到可循环，则需要重新计算已用总额
     * @参数与返回说明:返回计算后的已用总额
     * @算法描述: 计算公式：新已用额度 = ∑（额度下合同状态为生效的最高额合同金额）+
     *                                  ∑（额度下合同状态为生效的一般合同余额+可出账金额(合同可出账金额)）+∑（额度下已到期合同余额）
     * 最高额合同只会挂到一级分项额度上
     * 合同可出账金额=一般合同总额-合同向下台账总额汇总
     * 一般合同已用余额：合同向下台账占用余额
     * 额度已到期合同余额：已到期未结清合同向下业务余额
     */
    public BigDecimal getNewSpacOutstndAmt(ApprLmtSubBasicInfo apprLmtSubBasicInfo) {
        //分项总金额
        BigDecimal spacAmt = apprLmtSubBasicInfo.getSpacAmt() ;
        if(spacAmt==null) spacAmt = BigDecimal.ZERO ;
        //获取合同状态为生效的最高额合同金额
        Map<String, BigDecimal> highContAmtMap =
                lmtContRelService.getNorStatusLmtContCnyBal(apprLmtSubBasicInfo.getApprSubSerno(),
                        CmisLmtConstants.STD_ZB_CONT_TYPE_2,
                CmisLmtConstants.STD_ZB_CONT_STATUS_200, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
        //获取已用
        BigDecimal highContAmt = highContAmtMap.get("spacCnyBal") ;
        //额度下合同状态为生效的一般合同余额+可出账金额(合同可出账金额)
        //根据分项编号，查询向下生效的一般合同，遍历借款合同，获取合同下的台账占用，根据调账占用获取
        //获取分项下有效的一般担保合同
        List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByLimitSubNo(apprLmtSubBasicInfo.getApprSubSerno(), CmisLmtConstants.STD_ZB_CONT_STATUS_200) ;
        BigDecimal accSpacTotal = BigDecimal.ZERO ;
        BigDecimal accSpacAlance = BigDecimal.ZERO ;

        //遍历集合，计算合同余额+可出账金额
        if(CollectionUtils.isNotEmpty(lmtContRelList)){
            for(LmtContRel lmtContRel:lmtContRelList){
                //根据合同编号，获取合同向下的台账占用总额汇总以及余额汇总
                String dealBizNo = lmtContRel.getDealBizNo() ;
                Map<String, BigDecimal> accRelMap = contAccRelService.getContAccRelBal(dealBizNo, CmisLmtConstants.STD_ZB_CONT_STATUS_200) ;
                accSpacTotal = accSpacTotal.add(accRelMap.get("accspactotal")) ;
                accSpacAlance = accSpacAlance.add(accRelMap.get("accspacalance")) ;

            }
        }
        BigDecimal lmtSubAmt = spacAmt.subtract(accSpacTotal).add(accSpacAlance) ;

        //获取合同状态为生效的最高额合同金额
        Map<String, BigDecimal> invalidContAmtMap = lmtContRelService.getNorStatusLmtContCnyBal(apprLmtSubBasicInfo.getApprSubSerno(),null,
                CmisLmtConstants.STD_ZB_CONT_STATUS_400, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;

        BigDecimal invalidContAmt = invalidContAmtMap.get("spacCnyBal") ;
        return highContAmt.add(lmtSubAmt).add(invalidContAmt) ;
    }


    /**
     * @方法名称: insertApprBasicInfo
     * @方法描述: 额度新增操作
     * @参数与返回说明:
     * @算法描述:
     */
    public void insertApprBasicInfo(CmisLmt0001ReqDto reqDto, String accNo){
        //初始化批复主表对象信息
        ApprStrMtableInfo apprStrMtableInfo =  new ApprStrMtableInfo() ;
        //拷贝数据
        BeanUtils.copyProperties(reqDto, apprStrMtableInfo);
        apprStrMtableInfo.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
        //批复编号转换
        apprStrMtableInfo.setApprSerno(accNo);
        //批复状态
        apprStrMtableInfo.setApprStatus(reqDto.getAccStatus());
        //操作类型 默认 01- 新增
        apprStrMtableInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //额度类型 01-单一客户授信
        apprStrMtableInfo.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);
        //最近更新时间
        apprStrMtableInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //最近更新处理
        apprStrMtableInfo.setUpdId(reqDto.getInputId());
        //最近更新机构
        apprStrMtableInfo.setUpdBrId(reqDto.getInputBrId());
        //最近更新机构
        apprStrMtableInfo.setInputDate(comm4Service.formatDateStr(reqDto.getInputDate()));
        //最近更新时间
        apprStrMtableInfo.setUpdDate(comm4Service.formatDateStr(reqDto.getInputDate()));
        //创建时间
        apprStrMtableInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //添加主键
        apprStrMtableInfo.setPkId(comm4Service.generatePkId());
        //数据插入
        apprStrMtableInfoMapper.insert(apprStrMtableInfo) ;
    }

    public void chgApprBasicInfo(CmisLmt0001ReqDto reqDto, ApprStrMtableInfo apprStrMtableInfo, String accNo){
        //拷贝数据
        BeanUtils.copyProperties(reqDto, apprStrMtableInfo);
        //集团客户编号
        apprStrMtableInfo.setGrpNo(comm4Service.selectGrpNoByCusId(reqDto.getCusId()));
        //批复编号转换
        apprStrMtableInfo.setApprSerno(accNo);
        //批复状态
        apprStrMtableInfo.setApprStatus(reqDto.getAccStatus());
        //操作类型 默认 01- 新增
        apprStrMtableInfo.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //额度类型 01-单一客户授信
        apprStrMtableInfo.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);
        //最近更新时间
        apprStrMtableInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //最近更新处理
        apprStrMtableInfo.setUpdId(reqDto.getInputId());
        //最近更新人
        apprStrMtableInfo.setUpdBrId(reqDto.getInputBrId());
        //最近更新日期
        apprStrMtableInfo.setUpdDate(comm4Service.formatDateStr(reqDto.getInputDate()));
        //变更处理
        apprStrMtableInfoMapper.updateByPrimaryKey(apprStrMtableInfo) ;
    }

    /**
     * @作者:lizx
     * @方法名称: createAccAmt
     * @方法描述:  用信余额，已用总额，敞口已用，用信敞口余额
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/6 14:19
     * @param apprLmtSubBasicInfo: 
     * @return: java.util.Map<java.lang.String,java.math.BigDecimal>
     * @算法描述: 无
    */
    public Map<String, BigDecimal> createAccAmt(ApprLmtSubBasicInfo apprLmtSubBasicInfo){
        Map<String, BigDecimal> createAccAmtMap = new HashedMap() ;
        //用信余额
        BigDecimal loanBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance()) ;
        //已用总额
        BigDecimal outstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt()) ;
        //敞口已用
        BigDecimal spacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt()) ;
        //用信敞口余额
        BigDecimal loanSpacBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanSpacBalance()) ;
        BigDecimal pvpOutstndAmt = BigDecimal.ZERO ;
        //是否低风险
        String isLriskLmt = apprLmtSubBasicInfo.getIsLriskLmt() ;
        if(CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)){
            //已出帐金额
            pvpOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance()) ;
        }else{
            pvpOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanSpacBalance()) ;
        }
        createAccAmtMap.put("loanBalance", loanBalance) ;
        createAccAmtMap.put("outstndAmt", outstndAmt) ;
        createAccAmtMap.put("spacOutstndAmt", spacOutstndAmt) ;
        createAccAmtMap.put("loanSpacBalance", loanSpacBalance) ;
        createAccAmtMap.put("pvpOutstndAmt", pvpOutstndAmt) ;
        logger.info("【"+apprLmtSubBasicInfo.getApprSubSerno()+"】生成信批复：用信余额【loanBalance】："+loanBalance+",已用总额【outstndAmt】："+outstndAmt+
                ",敞口已用【spacOutstndAmt】："+spacOutstndAmt+",用信敞口余额【loanSpacBalance】："+loanSpacBalance+
                ",已出帐金额【pvpOutstndAmt】："+pvpOutstndAmt);

        return createAccAmtMap ;
    }


    /**
     * @作者:lizx
     * @方法名称: createAccAmt
     * @方法描述:  用信余额，已用总额，敞口已用，用信敞口余额-- 变更
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/6 14:19
     * @param apprLmtSubBasicInfo:
     * @return: java.util.Map<java.lang.String,java.math.BigDecimal>
     * @算法描述: 无
     */
    public Map<String, BigDecimal> changeAccAmt(ApprLmtSubBasicInfo apprLmtSubBasicInfo, ApprLmtSubBasicInfo origiApprLmtSub){
        Map<String, BigDecimal> createAccAmtMap = new HashedMap() ;
        //用信余额
        BigDecimal loanBalance = BigDecimalUtil.replaceNull(origiApprLmtSub.getLoanBalance()) ;
        //已用总额
        BigDecimal outstndAmt = BigDecimalUtil.replaceNull(getOutstndAmt(origiApprLmtSub, apprLmtSubBasicInfo)) ;
        //敞口已用
        BigDecimal spacOutstndAmt = BigDecimalUtil.replaceNull(getSpacOutstndAmt(origiApprLmtSub, apprLmtSubBasicInfo)) ;
        //用信敞口余额
        BigDecimal loanSpacBalance = BigDecimalUtil.replaceNull(origiApprLmtSub.getLoanSpacBalance()) ;
        //已出帐金额
        BigDecimal pvpOutstndAmt = BigDecimal.ZERO;
        //是否低风险
        String isLriskLmt = origiApprLmtSub.getIsLriskLmt() ;
        if(CmisLmtConstants.YES_NO_Y.equals(isLriskLmt)){
            //已出帐金额
            pvpOutstndAmt = BigDecimalUtil.replaceNull(origiApprLmtSub.getLoanBalance()) ;
        }else{
            pvpOutstndAmt = BigDecimalUtil.replaceNull(origiApprLmtSub.getLoanSpacBalance()) ;
        }
        createAccAmtMap.put("loanBalance", loanBalance) ;
        createAccAmtMap.put("outstndAmt", outstndAmt) ;
        createAccAmtMap.put("spacOutstndAmt", spacOutstndAmt) ;
        createAccAmtMap.put("loanSpacBalance", loanSpacBalance) ;
        createAccAmtMap.put("pvpOutstndAmt", pvpOutstndAmt) ;

        logger.info("【"+apprLmtSubBasicInfo.getApprSubSerno()+"】变更处理，参数信息：{}", JSON.toJSONString(createAccAmtMap));
        return createAccAmtMap ;
    }

    /**
     * @作者:lizx
     * @方法名称: ifNotFiledCheck
     * @方法描述:  必输项目校验
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/9/30 0:13
     * @param reqDto:
     * @return: void
     * @算法描述: 无
    */
    public void ifNotFiledCheck(CmisLmt0001ReqDto reqDto){
        //系统编号不允许为空
        if(StringUtils.isEmpty(reqDto.getSysId())) throw new YuspException(EclEnum.ECL070133.key, "系统编号" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getAccNo())) throw new YuspException(EclEnum.ECL070115.key, EclEnum.ECL070115.value) ;
        if(StringUtils.isEmpty(reqDto.getCusId())) throw new YuspException(EclEnum.ECL070133.key, "客户号" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getCusName())) throw new YuspException(EclEnum.ECL070133.key, "客户名称" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getCusType())) throw new YuspException(EclEnum.ECL070133.key, "客户类型" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getAccStatus())) throw new YuspException(EclEnum.ECL070133.key, "批复台账状态" + EclEnum.ECL070133.value) ;
        if(StringUtils.isEmpty(reqDto.getLmtMode())) throw new YuspException(EclEnum.ECL070133.key, "授信模式" + EclEnum.ECL070133.value) ;

        for(CmisLmt0001LmtSubListReqDto lmtSub : reqDto.getLmtSubList()){
            if(StringUtils.isEmpty(lmtSub.getAccSubNo())) throw new YuspException(EclEnum.ECL070133.key, "分项编号" + EclEnum.ECL070133.value) ;
            //if(StringUtils.isEmpty(lmtSub.getLimitSubNo())) throw new YuspException(EclEnum.ECL070133.key, "授信品种编号" + EclEnum.ECL070133.value) ;
            //if(StringUtils.isEmpty(lmtSub.getLimitSubName())) throw new YuspException(EclEnum.ECL070133.key, "授信品种名称" + EclEnum.ECL070133.value) ;
            if(StringUtils.isEmpty(lmtSub.getIsRevolv())) throw new YuspException(EclEnum.ECL070133.key, "是否可循环" + EclEnum.ECL070133.value) ;
            if(StringUtils.isEmpty(lmtSub.getIsLriskLmt())) throw new YuspException(EclEnum.ECL070133.key, "是否低风险" + EclEnum.ECL070133.value) ;
            if(StringUtils.isEmpty(lmtSub.getAccSubStatus())) throw new YuspException(EclEnum.ECL070133.key, "批复分项状态" + EclEnum.ECL070133.value) ;
        }
    }

    /**
     * @作者:lizx
     * @方法名称: checkOutIsAllowCreate
     * @方法描述:  校验是否允许进行额度建立
     *          校验规则：1、综合授信额度向下只允许存在一个有效额度额度信息
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/18 23:20
     * @return: void
     * @算法描述: 无
    */
    public void checkOutIsAllowCreate(CmisLmt0001ReqDto reqDto){
        logger.info("校验是否允许建立额度-----------start");
        //批复编号
        String accNo = reqDto.getAccNo() ;
        //是否覆盖原批复
        String isCreateAcc =  Optional.ofNullable(reqDto.getIsCreateAcc()).orElse("0") ;
        //客户编号
        String cusId = reqDto.getCusId() ;
        //授信模式
        String lmtMode = reqDto.getLmtMode() ;
        /** 场景一：变更场景，根据批复编号查询到原批复信息，有数据，则证明是变更操作，不拦截 **/
        ApprStrMtableInfo apprStrMtableInfo = apprStrMtableInfoService.selectByAppSerno(accNo) ;
        if(Objects.nonNull(apprStrMtableInfo)) return ;

        /** 场景二：不覆盖原批复，授信模式为 【01-综合授信】综合授信， 但是客户号存在不等于当前批复编号的有效批复报错处理**/
        if(CmisLmtConstants.YES_NO_N.equals(isCreateAcc) && CmisLmtConstants.STD_ZB_LMT_MODE_01.equals(lmtMode)){
            //根据客户号查询向下有效的综合批复信息
            List<ApprStrMtableInfo> apprList = apprStrMtableInfoService.queryApprStrZHLmt(cusId) ;
            if(CollectionUtils.isNotEmpty(apprList)){
                if(apprList.size()>1) {
                    String appStr = (String) apprList.stream().filter(apprSMInfo->!apprSMInfo.equals(accNo)).map(apprInfo->{return apprInfo.getApprSerno();}).collect(Collectors.joining(","));
                    throw new YuspException(EclEnum.ECL070142.key,  "【" + cusId + "】"+ EclEnum.ECL070142.value + "【" +appStr+ "】") ;
                }
                if(apprList.size()==1){
                    String apprSerno = apprList.get(0).getApprSerno() ;
                    if(!apprSerno.equals(accNo)) throw new YuspException(EclEnum.ECL070142.key,  "【" + cusId + "】"+ EclEnum.ECL070142.value + "【" +apprSerno+ "】") ;
                }
            }
        }
        logger.info("校验是否允许建立额度-----------end");
    }
}


