/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ApprCoopChgApp;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprCoopChgAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-04-20 22:35:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface ApprCoopChgAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    ApprCoopChgApp selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<ApprCoopChgApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(ApprCoopChgApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(ApprCoopChgApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(ApprCoopChgApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(ApprCoopChgApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: updateByParams
     * @方法描述: 根据主键逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByParams(Map delMap);

    /**
     * @方法名称: valiLmtChgAppInWay
     * @方法描述: 根据实体类中的非空书，查询批复是否存在子在途申请
     * @参数与返回说明:
     * @算法描述: 无
     */

    int valiLmtChgAppInWay(ApprCoopChgApp apprCoopChgApp);

    /**
     * @方法名称: selectByAppSerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    ApprCoopChgApp selectByAppSerno(@Param("appSerno") String appSerno);

    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 流程审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateApproveStatus(ApprCoopChgApp apprCoopChgApp);
    /**
     * @函数名称:querySubChgAppBySerno
     * @函数描述:根根据调整申请流水号和合作方批复台账编号加工展示合作方调整申请主信息
     * @参数与返回说明:
     * @param model
     *            分页查询类
     * @算法描述:
     */
    List<Map<String, Object>> querySubChgAppBySerno(QueryModel model);
}