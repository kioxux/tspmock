package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class LmtCorreChnlOrSysConfDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    private String sysPkid;

    /** FK_PKID **/
    private String fkPkid;

    /** 流水号 **/
    private String serno;

    /** 额度分项编号 **/
    private String limitSubNo;

    /** 额度分项名称 **/
    private String limitSubName;

    /** 版本号 **/
    private String version;

    /** 关联系统编号 **/
    private String sysId;

    /** 关联渠道编号 **/
    private String chnlId;

    /** 操作标识 **/
    private String oprFlag;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getSysPkid() {
        return sysPkid;
    }

    public void setSysPkid(String sysPkid) {
        this.sysPkid = sysPkid;
    }

    public String getFkPkid() {
        return fkPkid;
    }

    public void setFkPkid(String fkPkid) {
        this.fkPkid = fkPkid;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getChnlId() {
        return chnlId;
    }

    public void setChnlId(String chnlId) {
        this.chnlId = chnlId;
    }

    public String getOprFlag() {
        return oprFlag;
    }

    public void setOprFlag(String oprFlag) {
        this.oprFlag = oprFlag;
    }
}
