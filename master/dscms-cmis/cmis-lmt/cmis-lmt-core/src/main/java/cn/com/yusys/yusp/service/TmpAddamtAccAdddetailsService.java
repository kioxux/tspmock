/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.TmpAddamtAccAdddetails;
import cn.com.yusys.yusp.repository.mapper.TmpAddamtAccAdddetailsMapper;

/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: TmpAddamtAccAdddetailsService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-11-06 20:32:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class TmpAddamtAccAdddetailsService {

    @Autowired
    private TmpAddamtAccAdddetailsMapper tmpAddamtAccAdddetailsMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public TmpAddamtAccAdddetails selectByPrimaryKey(String apprSubSerno, String contNo) {
        return tmpAddamtAccAdddetailsMapper.selectByPrimaryKey(apprSubSerno, contNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<TmpAddamtAccAdddetails> selectAll(QueryModel model) {
        List<TmpAddamtAccAdddetails> records = (List<TmpAddamtAccAdddetails>) tmpAddamtAccAdddetailsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<TmpAddamtAccAdddetails> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<TmpAddamtAccAdddetails> list = tmpAddamtAccAdddetailsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(TmpAddamtAccAdddetails record) {
        return tmpAddamtAccAdddetailsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(TmpAddamtAccAdddetails record) {
        return tmpAddamtAccAdddetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(TmpAddamtAccAdddetails record) {
        return tmpAddamtAccAdddetailsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(TmpAddamtAccAdddetails record) {
        return tmpAddamtAccAdddetailsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String apprSubSerno, String contNo) {
        return tmpAddamtAccAdddetailsMapper.deleteByPrimaryKey(apprSubSerno, contNo);
    }

    /**
     * @方法名称: truncateTalbeDate
     * @方法描述: 清空中间表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public void truncateTalbeData() {
        tmpAddamtAccAdddetailsMapper.truncateTalbeData();
    }


    /**
     * @方法名称: truncateTalbeDate
     * @方法描述: 清空中间表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSceneTwo(String apprSubSerno) {
        return tmpAddamtAccAdddetailsMapper.insertSceneTwo(apprSubSerno);
    }

}
