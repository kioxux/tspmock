package cn.com.yusys.yusp.web.server.cmislmt0033;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.resp.CmisLmt0033RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0033.CmisLmt0033Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:国结票据出账额度校验
 *
 * @author dumd 20210618
 * @version 1.0
 */
@Api(tags = "cmislmt0033:国结票据出账额度校验")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0033Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0033Resource.class);

    @Autowired
    private CmisLmt0033Service cmisLmt0033Service;

    /**
     * 交易码：CmisLmt0033
     * 交易描述：国结票据出账额度校验
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("国结票据出账额度校验")
    @PostMapping("/cmislmt0033")
    protected @ResponseBody
    ResultDto<CmisLmt0033RespDto> CmisLmt0033(@Validated @RequestBody CmisLmt0033ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0033.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0033RespDto> CmisLmt0033RespDtoResultDto = new ResultDto<>();
        CmisLmt0033RespDto CmisLmt0033RespDto = new CmisLmt0033RespDto() ;
        // 调用对应的service层
        try {
            CmisLmt0033RespDto = cmisLmt0033Service.execute(reqDto);
            CmisLmt0033RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            CmisLmt0033RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0033.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key,
                    DscmsLmtEnum.TRADE_CODE_CMISLMT0033.value, e.getMessage());
            // 封装CmisLmt0033RespDtoResultDto中异常返回码和返回信息
            CmisLmt0033RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            CmisLmt0033RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装CmisLmt0033RespDto到CmisLmt0033RespDtoResultDto中
        CmisLmt0033RespDtoResultDto.setData(CmisLmt0033RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.value, JSON.toJSONString(CmisLmt0033RespDtoResultDto));
        return CmisLmt0033RespDtoResultDto;
    }
}