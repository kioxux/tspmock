package cn.com.yusys.yusp.web.server.cmislmt0051;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0051.req.CmisLmt0051ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0051.resp.CmisLmt0051RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmislmt0051.CmisLmt0051Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据条线部门和区域获取业务条线额度管控信息
 *
 * @author zhangjw 2021/7/14
 * @version 1.0
 */
@Api(tags = "cmislmt0051:获取机构贴现限额管控信息")
@RestController
@RequestMapping("/api/lmt4inner")
public class CmisLmt0051Resource {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0051Resource.class);

    @Autowired
    private CmisLmt0051Service cmisLmt0051Service;
    /**
     * 交易码：cmislmt0051
     * 交易描述：获取机构贴现限额管控信息
     *
     * @param reqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("获取机构贴现限额管控信息")
    @PostMapping("/cmislmt0051")
    protected @ResponseBody
    ResultDto<CmisLmt0051RespDto> cmisLmt0051(@Validated @RequestBody CmisLmt0051ReqDto reqDto){
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0051.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0051.value, JSON.toJSONString(reqDto));
        ResultDto<CmisLmt0051RespDto> cmisLmt0051RespDtoResultDto = new ResultDto<>();
        CmisLmt0051RespDto cmisLmt0051RespDto = new CmisLmt0051RespDto() ;
        // 调用对应的service层
        try {
            cmisLmt0051RespDto = cmisLmt0051Service.execute(reqDto);
            cmisLmt0051RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisLmt0051RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);

        }catch (Exception e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0051.value, e);
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0051.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0051.value, e.getMessage());
            // 封装xddb0051DataResultDto中异常返回码和返回信息
            cmisLmt0051RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisLmt0051RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisLmt0016RespDto到cmisLmt0016RespDtoResultDto中
        cmisLmt0051RespDtoResultDto.setData(cmisLmt0051RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0051.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0051.value, JSON.toJSONString(cmisLmt0051RespDtoResultDto));
        return cmisLmt0051RespDtoResultDto;
    }

}
