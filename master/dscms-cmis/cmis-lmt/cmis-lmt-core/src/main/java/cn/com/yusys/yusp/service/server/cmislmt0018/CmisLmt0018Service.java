package cn.com.yusys.yusp.service.server.cmislmt0018;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0018.req.CmisLmt0018ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0018.resp.CmisLmt0018RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0018.resp.CmisLmt0018StrListRespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprStrMtableInfoService;
import cn.com.yusys.yusp.service.LmtStrMtableConfService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.Query;
import java.util.List;
/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0018Service
 * @类描述: #对内服务类
 * @功能描述: cmisLmt0018
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0018Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0018Service.class);

    @Autowired
    private ApprStrMtableInfoService apprStrMtableInfoService;

    @Autowired
    private LmtStrMtableConfService lmtStrMtableConfService;

    @Transactional
    public CmisLmt0018RespDto execute(CmisLmt0018ReqDto reqDto){
        logger.info(this.getClass().getName()+":额度结构查询开始............. ");
        CmisLmt0018RespDto resqDto = new CmisLmt0018RespDto() ;
        try {

            List<CmisLmt0018StrListRespDto> cmisLmt0018StrList=lmtStrMtableConfService.selectLmtStrConfByStrType(reqDto);

            resqDto.setCmisLmt0018StrList(cmisLmt0018StrList);
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error("额度结构查询接口报错：" ,e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        return resqDto;
    }

}


