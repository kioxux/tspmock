package cn.com.yusys.yusp.service.server.cmislmt0019;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0019.req.CmisLmt0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ApprLmtSubBasicInfoMapper;
import cn.com.yusys.yusp.repository.mapper.ApprStrMtableInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CusGrpMemberRelMapper;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.server.cmislmt0007.CmisLmt0007Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0019Service
 * @类描述: #对内服务类
 * @功能描述: 校验客户是否存在有效综合授信额度
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0019Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0019Service.class);

    @Autowired
    private ApprStrMtableInfoMapper apprStrMtableInfoMapper;

    @Autowired
    private ApprLmtSubBasicInfoMapper apprLmtSubBasicInfoMapper ;

    @Autowired
    private CusGrpMemberRelMapper cusGrpMemberRelMapper ;

    public CmisLmt0019RespDto checkLmtMode(CmisLmt0019ReqDto reqDto){
        logger.info(this.getClass().getName()+":综合授信额度校验接口接收数据开始............. ");
        CmisLmt0019RespDto respDto = new CmisLmt0019RespDto();
        try {
            /*
                是否查询集团向下 1-是 0-否
                1：根据当前客户查找是所属集团向下的所有子公司，如果有有自动存在相关授信，则返回是 否则返回否
                0：不查询集团
             */
            String isQuryGrp = reqDto.getIsQuryGrp() ;
            /* 查询类型 01 贷款  02 贷款和非标
               01 贷款：即查询客户综合授信贷款
               02 非标：查询类型为：
               4009-债务融资工具（投资）
               4010-理财直融工具（投资）
               4012-其他非标债权投资
               4007-资产证券化产品（非标）
               4011-结构化融资  投资授信
             */
            String queryType = reqDto.getQueryType() ;
            //金融机构代码
            String instuCde = reqDto.getInstuCde() ;
            //客户号
            String cusId = reqDto.getCusId() ;
            //查询该客户是否有贷款类授信或非标类授信
            int count = getCount(instuCde, cusId, queryType);
            //如果存在，响应报文中返回
            if(count>0){
                return yesResp() ;
            }

            //如果查询集团向下
            if(CmisLmtConstants.YES_NO_Y.equals(isQuryGrp)){
                //根据改客户号查询该客户集团向下的客户信息
                List<Map<String, String>> cusMemList = cusGrpMemberRelMapper.selectByCusIdQueryGrpNoCuss(cusId)  ;
                //空处理
                if(CollectionUtils.nonEmpty(cusMemList)){
                    //遍历查询
                    for (Map<String, String> cusMap : cusMemList) {
                        String memCusId = cusMap.get("cusId") ;
                        //集团下可以等于当前客户跳过此循环
                        if(cusId.equals(memCusId)) continue;
                        //获取集团下客户信息是否存在授信
                        count = getCount(instuCde, memCusId, queryType);
                        logger.info("集团向下客户【{}】,是否存在授信额度{}", memCusId, count) ;
                        if(count>0){
                            return yesResp() ;
                        }
                    }
                }
            }

            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            respDto.setResult(CmisLmtConstants.YES_NO_N);
        }catch (YuspException e) {
            logger.error("综合授信额度校验接口报错：", e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(this.getClass().getName()+":综合授信额度接口接收数据结束............. ");
        return respDto;
    }

    /**
     * @作者:lizx
     * @方法名称: getCount
     * @方法描述:  01  查询是是否有贷款类授信（综合授信） 02 贷款类授信和非标类授信
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/8/12 20:19
     * @param instuCde: 金融机构代码
     * @param cusId:  客户编号
     * @param queryType:  查询类型  01 贷款 02 贷款和非标
     * @return: int
     * @算法描述: 无
    */
    private int getCount(String instuCde, String cusId, String queryType) {
        logger.info("查询客户【{}】是否存在信贷类额度，查询类型【{}】--【01 贷款 02 贷款和非标】-------->start", cusId, queryType) ;
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("instuCde", instuCde);
        queryModel.addCondition("cusId", cusId);
        int count = apprStrMtableInfoMapper.countApprStrMtableInfoByQueryModel(queryModel);
        logger.info("查询客户【{}】是否存在信贷类额度，查询结果【{}】", cusId, count) ;
        if(count>0){
            return count ;
        }
        // 查询类型 02 非标
        if(CmisLmtConstants.CMIS_QUERY_TYPE_02.equals(queryType)){
           int countF = apprLmtSubBasicInfoMapper.countApprLmtSubBasicInfoByQueryModel(queryModel) ;
            logger.info("查询客户【{}】是否存在非标类额度，查询结果【{}】", cusId, count) ;
            count = count+ countF ;
        }
        logger.info("查询客户【{}】是否存在额度，查询结果【{}】-------->end", cusId, count) ;
        return count;
    }

    /**
     * @作者:lizx
     * @方法名称: yesResp
     * @方法描述: 组装返回报文，返回有业务的请求
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/8/12 20:19
     * @return: cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto
     * @算法描述: 无
    */
    private CmisLmt0019RespDto yesResp(){
        CmisLmt0019RespDto respDto = new CmisLmt0019RespDto();
        respDto.setErrorCode(SuccessEnum.SUCCESS.key);
        respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        respDto.setResult(CmisLmtConstants.YES_NO_Y);
        return respDto ;
    }

}
