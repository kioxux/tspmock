package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprStrMtableChgApp
 * @类描述: appr_str_mtable_chg_app数据实体类
 * @功能描述: 
 * @创建人: jiangyl
 * @创建时间: 2021-05-11 10:36:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因:字段变动
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class ApprStrMtableChgAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;

	/** 主申请流水号 **/
	private String appSerno;
	
	/** 批复台账流水号 **/
	private String apprSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 集团编号 **/
	private String grpNo;
	
	/** 客户类型 **/
	private String cusType;
	
	/** 批复台账状态 **/
	private String apprStatus;
	
	/** 金融机构代码 **/
	private String instuCde;
	
	/** 集团批复台账编号 **/
	private String grpApprSerno;
	
	/** 期限(月) **/
	private Integer term;
	
	/** 起始日期 **/
	private String startDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 审批状态STD_ZB_APPR_STATUS **/
	private String approveStatus;
	
	/** 授信类型STD_ZB_LMT_TYPE **/
	private String lmtType;
	
	/** 授信模式STD_ZB_LMT_MODE **/
	private String lmtMode;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param appSerno
	 */
	public void setAppSerno(String appSerno) {
		this.appSerno = appSerno == null ? null : appSerno.trim();
	}
	
    /**
     * @return AppSerno
     */	
	public String getAppSerno() {
		return this.appSerno;
	}
	
	/**
	 * @param apprSerno
	 */
	public void setApprSerno(String apprSerno) {
		this.apprSerno = apprSerno == null ? null : apprSerno.trim();
	}
	
    /**
     * @return ApprSerno
     */	
	public String getApprSerno() {
		return this.apprSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param grpNo
	 */
	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo == null ? null : grpNo.trim();
	}
	
    /**
     * @return GrpNo
     */	
	public String getGrpNo() {
		return this.grpNo;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType == null ? null : cusType.trim();
	}
	
    /**
     * @return CusType
     */	
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus == null ? null : apprStatus.trim();
	}
	
    /**
     * @return ApprStatus
     */	
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param instuCde
	 */
	public void setInstuCde(String instuCde) {
		this.instuCde = instuCde == null ? null : instuCde.trim();
	}
	
    /**
     * @return InstuCde
     */	
	public String getInstuCde() {
		return this.instuCde;
	}
	
	/**
	 * @param grpApprSerno
	 */
	public void setGrpApprSerno(String grpApprSerno) {
		this.grpApprSerno = grpApprSerno == null ? null : grpApprSerno.trim();
	}
	
    /**
     * @return GrpApprSerno
     */	
	public String getGrpApprSerno() {
		return this.grpApprSerno;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(Integer term) {
		this.term = term;
	}
	
    /**
     * @return Term
     */	
	public Integer getTerm() {
		return this.term;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType == null ? null : lmtType.trim();
	}
	
    /**
     * @return LmtType
     */	
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param lmtMode
	 */
	public void setLmtMode(String lmtMode) {
		this.lmtMode = lmtMode == null ? null : lmtMode.trim();
	}
	
    /**
     * @return LmtMode
     */	
	public String getLmtMode() {
		return this.lmtMode;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}