/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.ApprStrMtableChgApp;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprStrMtableChgAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: jiangyl
 * @创建时间: 2021-05-11 10:36:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因:字段变动
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface ApprStrMtableChgAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    ApprStrMtableChgApp selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<ApprStrMtableChgApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(ApprStrMtableChgApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(ApprStrMtableChgApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(ApprStrMtableChgApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(ApprStrMtableChgApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: updateByParams
     * @方法描述: 根据主键逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByParams(Map delMap);


    /**
     * @方法名称: valiLmtChgAppInWay
     * @方法描述: 根据实体类中的非空书，查询批复是否存在子在途申请
     * @参数与返回说明:
     * @算法描述: 无
     */

    int valiLmtChgAppInWay(ApprStrMtableChgApp record);


    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 流程审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateApproveStatus(ApprStrMtableChgApp apprStrMtableChgApp);
    /**
     * @方法名称: selectByAppSerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    ApprStrMtableChgApp selectByAppSerno(@Param("appSerno") String appSerno);

    /**
     * @函数名称:queryStrChgAppBySerno
     * @函数描述:根据调整申请流水号和主批复台账编号加工展示主调整申请主信息
     * @参数与返回说明:
     * @param model
     *            分页查询类
     * @算法描述:
     */
    List<Map<String, Object>> queryStrChgAppBySerno(QueryModel model);

}