package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "产品户买入返售白名单导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class WhiteInfo02ImportVo {

    /*
    额度分项编号
     */
    @ExcelField(title = "ECIF编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "同业名称", viewLength = 20)
    private String cusName;

    /*
    总行行号
     */
    @ExcelField(title = "行号", viewLength = 20)
    private String aorgNo;

    /*
    客户类型
     */
    @ExcelField(title = "同业机构类型", dictCode = "STD_ZB_INTBANK_TYPE",  viewLength = 20)
    private String cusType;

    /*
    限额
     */
    @ExcelField(title = "机构买入返售限额（万元）", viewLength = 30)
    private java.math.BigDecimal sigAmt;

    /*
    单个产品户买入返售限额
     */
    @ExcelField(title = "单个产品户买入返售限额（万元）", viewLength = 30)
    private java.math.BigDecimal singleResaleQuota;

    /*
    到期日期
     */
    @ExcelField(title = "到期日", viewLength = 20)
    private String endDate;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public BigDecimal getSigAmt() {
        return sigAmt;
    }

    public void setSigAmt(BigDecimal sigAmt) {
        this.sigAmt = sigAmt;
    }

    public BigDecimal getSingleResaleQuota() {
        return singleResaleQuota;
    }

    public void setSingleResaleQuota(BigDecimal singleResaleQuota) {
        this.singleResaleQuota = singleResaleQuota;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }


}
