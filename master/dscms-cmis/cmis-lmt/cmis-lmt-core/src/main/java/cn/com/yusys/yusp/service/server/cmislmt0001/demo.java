package cn.com.yusys.yusp.service.server.cmislmt0001;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class demo {

    public static void main(String[] args) {
        //创建线程1 方式
        Mythread myThread = new Mythread() ;
        myThread.start();

        //常见线程2 方式
        Mythread2 mythread = new Mythread2() ;
        Thread thread = new Thread(mythread) ;
        thread.start();

        //常见线程方式3
        int taskSize = 10 ;
        ExecutorService pool = Executors.newFixedThreadPool(taskSize) ;
        List<Future> list = new ArrayList<>() ;
        for(int i=0;i<taskSize;i++){
            int finalI = i;
            Callable c =new Callable() {
                @Override
                public Object call() throws Exception {
                    return finalI;
                }
            };

            Future f = pool.submit(c) ;
            list.add(f) ;
        }
        pool.shutdown();
        for (Future future : list) {
            System.out.println(future.toString());
        }


        //线程池的方式
        ExecutorService pool2 = Executors.newFixedThreadPool(10) ;
        while (true){
            pool2.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName()+"is Running..");
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}

class Mythread extends Thread{
    public void run(){
        System.out.println("创建常用线程------：extends Thread!");
    }
}

class Mythread2 implements  Runnable{

    @Override
    public void run() {
        System.out.println("创建线程方式2： 实现Runnable接口");
    }
}
