package cn.com.yusys.yusp.service.server.cmislmt0048;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ApprLmtSubBasicInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0048.req.CmisLmt0048ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0048.resp.CmisLmt0048RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.ApprLmtSubBasicInfoService;
import cn.com.yusys.yusp.service.LmtContRelService;
import cn.com.yusys.yusp.service.LmtSubPrdMappConfService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.OpenOption;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0048Service
 * @类描述: #对内服务类
 * @功能描述: 根据分项编号，判断是否可以做最高额协议或一般最高额合同
 * @修改备注: 2021/8/26     lizx   新增接口
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0048Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0048Service.class);
    @Autowired
    private LmtContRelService lmtContRelService ;
    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;
    @Transactional
    public CmisLmt0048RespDto execute(CmisLmt0048ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0048.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0048.value);
        CmisLmt0048RespDto resqDto = new CmisLmt0048RespDto() ;

        try {
            String accSubNo = reqDto.getAccSubNo() ;
            String isHighLmt = reqDto.getIsHighLmt() ;
            if(StringUtils.isBlank(accSubNo)){
                throw new YuspException(EclEnum.ECL070133.key, "分项编号" + EclEnum.ECL070133.value);
            }
            if(StringUtils.isBlank(isHighLmt)){
                throw new YuspException(EclEnum.ECL070133.key, "是否最高额授信协议" + EclEnum.ECL070133.value);
            }

            /**
             * 如果是最高额度授信协议，则判断分项编号项下二级分项是否存在一般合同或者最高额合同；
             * 如果非最高额授信协议，则判断分项编号对应对应父节点分项，是否存在签订了最高额授信协议，若是，则返回是 selectByParamIsExistsBusinss
             */
            if(CmisLmtConstants.YES_NO_Y.equals(isHighLmt)){
                //获取父类向下所有的分项编号  str1,str2,str3
                String apprSubSernos = apprLmtSubBasicInfoService.selectApprSubSernoArrayByParentId(accSubNo) ;
                //判断处理，如果时空的话，则进行
                if(StringUtils.isBlank(apprSubSernos)) {
                    throw new YuspException(EclEnum.ECL070143.key, accSubNo + ":"+ EclEnum.ECL070143.value);
                }

                logger.info("最高额协议，分项向下分项明细编号{}", apprSubSernos);
                //根据分项编号等信息查询是否一般合同，最高额合同
                int count = lmtContRelService.selectByParamIsExistsBusinss(apprSubSernos, CmisLmtConstants.STD_ZB_BIZ_STATUS_STR,
                        CmisLmtConstants.DEAL_BIZ_TYPE_STR) ;
                if(count>0){
                    resqDto.setHasOtherBuss(CmisLmtConstants.YES_NO_Y);
                }else{
                    resqDto.setHasOtherBuss(CmisLmtConstants.YES_NO_N);
                }
            }else{
                ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectByApprSubSerno(accSubNo) ;
                //判断处理，如果时空的话，则进行
                if(Objects.isNull(apprLmtSubBasicInfo)) {
                    throw new YuspException(EclEnum.ECL070144.key, accSubNo + ":"+ EclEnum.ECL070144.value);
                }

                String parentId = apprLmtSubBasicInfo.getParentId() ;
                int count = lmtContRelService.selectByParamIsExistsBusinss(parentId, CmisLmtConstants.STD_ZB_BIZ_STATUS_STR,
                        CmisLmtConstants.DEAL_BIZ_TYPE_3) ;
                if(count>0){
                    resqDto.setHasOtherBuss(CmisLmtConstants.YES_NO_Y);
                }else{
                    resqDto.setHasOtherBuss(CmisLmtConstants.YES_NO_N);
                }
            }
            //将列表放入返回中
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsLmtEnum.TRADE_CODE_CMISLMT0048.value, e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0048.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0048.value);
        return resqDto;
    }
}