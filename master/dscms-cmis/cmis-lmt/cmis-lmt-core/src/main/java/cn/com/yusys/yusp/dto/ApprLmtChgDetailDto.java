package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: ApprLmtChgDetail
 * @类描述: appr_lmt_chg_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-27 21:41:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class ApprLmtChgDetailDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 流水号 **/
	private String serno;
	
	/** 接口服务码 **/
	private String serviceCode;
	
	/** 修改表名主键 **/
	private String pkIdRel;
	
	/** 修改表名 **/
	private String updTableName;
	
	/** 字段名1 **/
	private String filed1;
	
	/** 修改前字段值1 **/
	private java.math.BigDecimal updbfFiled1;
	
	/** 修改后字段值1 **/
	private java.math.BigDecimal updafFiled1;
	
	/** 字段名2 **/
	private String filed2;
	
	/** 修改前字段值2 **/
	private java.math.BigDecimal updbfFiled2;
	
	/** 修改后字段值2 **/
	private java.math.BigDecimal updafFiled2;
	
	/** 字段名3 **/
	private String filed3;
	
	/** 修改前字段值3 **/
	private java.math.BigDecimal updbfFiled3;
	
	/** 修改后字段值3 **/
	private java.math.BigDecimal updafFiled3;
	
	/** 字段名4 **/
	private String filed4;
	
	/** 修改前字段值4 **/
	private java.math.BigDecimal updbfFiled4;
	
	/** 修改后字段值4 **/
	private java.math.BigDecimal updafFiled4;
	
	/** 字段名5 **/
	private String filed5;
	
	/** 修改前字段值5 **/
	private java.math.BigDecimal updbfFiled5;
	
	/** 修改后字段值5 **/
	private java.math.BigDecimal updafFiled5;
	
	/** 字段名6 **/
	private String filed6;
	
	/** 修改前字段值6 **/
	private java.math.BigDecimal updbfFiled6;
	
	/** 修改后字段值6 **/
	private java.math.BigDecimal updafFiled6;
	
	/** 字段名7 **/
	private String filed7;
	
	/** 修改前字段值7 **/
	private java.math.BigDecimal updbfFiled7;
	
	/** 修改后字段值7 **/
	private java.math.BigDecimal updafFiled7;
	
	/** 字段名8 **/
	private String filed8;
	
	/** 修改前字段值8 **/
	private java.math.BigDecimal updbfFiled8;
	
	/** 修改后字段值8 **/
	private java.math.BigDecimal updafFiled8;
	
	/** 字段名9 **/
	private String filed9;
	
	/** 修改前字段值9 **/
	private java.math.BigDecimal updbfFiled9;
	
	/** 修改后字段值9 **/
	private java.math.BigDecimal updafFiled9;
	
	/** 字段名10 **/
	private String filed10;
	
	/** 修改前字段值10 **/
	private java.math.BigDecimal updbfFiled10;
	
	/** 修改后字段值10 **/
	private java.math.BigDecimal updafFiled10;
	
	/** 字段名11 **/
	private String filed11;
	
	/** 修改前字段值11 **/
	private java.math.BigDecimal updbfFiled11;
	
	/** 修改后字段值11 **/
	private java.math.BigDecimal updafFiled11;
	
	/** 字段名12 **/
	private String filed12;
	
	/** 修改前字段值12 **/
	private java.math.BigDecimal updbfFiled12;
	
	/** 修改后字段值12 **/
	private java.math.BigDecimal updafFiled12;
	
	/** 字段名13 **/
	private String filed13;
	
	/** 修改前字段值13 **/
	private java.math.BigDecimal updbfFiled13;
	
	/** 修改后字段值13 **/
	private java.math.BigDecimal updafFiled13;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param serviceCode
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode == null ? null : serviceCode.trim();
	}
	
    /**
     * @return ServiceCode
     */	
	public String getServiceCode() {
		return this.serviceCode;
	}
	
	/**
	 * @param pkIdRel
	 */
	public void setPkIdRel(String pkIdRel) {
		this.pkIdRel = pkIdRel == null ? null : pkIdRel.trim();
	}
	
    /**
     * @return PkIdRel
     */	
	public String getPkIdRel() {
		return this.pkIdRel;
	}
	
	/**
	 * @param updTableName
	 */
	public void setUpdTableName(String updTableName) {
		this.updTableName = updTableName == null ? null : updTableName.trim();
	}
	
    /**
     * @return UpdTableName
     */	
	public String getUpdTableName() {
		return this.updTableName;
	}
	
	/**
	 * @param filed1
	 */
	public void setFiled1(String filed1) {
		this.filed1 = filed1 == null ? null : filed1.trim();
	}
	
    /**
     * @return Filed1
     */	
	public String getFiled1() {
		return this.filed1;
	}
	
	/**
	 * @param updbfFiled1
	 */
	public void setUpdbfFiled1(java.math.BigDecimal updbfFiled1) {
		this.updbfFiled1 = updbfFiled1;
	}
	
    /**
     * @return UpdbfFiled1
     */	
	public java.math.BigDecimal getUpdbfFiled1() {
		return this.updbfFiled1;
	}
	
	/**
	 * @param updafFiled1
	 */
	public void setUpdafFiled1(java.math.BigDecimal updafFiled1) {
		this.updafFiled1 = updafFiled1;
	}
	
    /**
     * @return UpdafFiled1
     */	
	public java.math.BigDecimal getUpdafFiled1() {
		return this.updafFiled1;
	}
	
	/**
	 * @param filed2
	 */
	public void setFiled2(String filed2) {
		this.filed2 = filed2 == null ? null : filed2.trim();
	}
	
    /**
     * @return Filed2
     */	
	public String getFiled2() {
		return this.filed2;
	}
	
	/**
	 * @param updbfFiled2
	 */
	public void setUpdbfFiled2(java.math.BigDecimal updbfFiled2) {
		this.updbfFiled2 = updbfFiled2;
	}
	
    /**
     * @return UpdbfFiled2
     */	
	public java.math.BigDecimal getUpdbfFiled2() {
		return this.updbfFiled2;
	}
	
	/**
	 * @param updafFiled2
	 */
	public void setUpdafFiled2(java.math.BigDecimal updafFiled2) {
		this.updafFiled2 = updafFiled2;
	}
	
    /**
     * @return UpdafFiled2
     */	
	public java.math.BigDecimal getUpdafFiled2() {
		return this.updafFiled2;
	}
	
	/**
	 * @param filed3
	 */
	public void setFiled3(String filed3) {
		this.filed3 = filed3 == null ? null : filed3.trim();
	}
	
    /**
     * @return Filed3
     */	
	public String getFiled3() {
		return this.filed3;
	}
	
	/**
	 * @param updbfFiled3
	 */
	public void setUpdbfFiled3(java.math.BigDecimal updbfFiled3) {
		this.updbfFiled3 = updbfFiled3;
	}
	
    /**
     * @return UpdbfFiled3
     */	
	public java.math.BigDecimal getUpdbfFiled3() {
		return this.updbfFiled3;
	}
	
	/**
	 * @param updafFiled3
	 */
	public void setUpdafFiled3(java.math.BigDecimal updafFiled3) {
		this.updafFiled3 = updafFiled3;
	}
	
    /**
     * @return UpdafFiled3
     */	
	public java.math.BigDecimal getUpdafFiled3() {
		return this.updafFiled3;
	}
	
	/**
	 * @param filed4
	 */
	public void setFiled4(String filed4) {
		this.filed4 = filed4 == null ? null : filed4.trim();
	}
	
    /**
     * @return Filed4
     */	
	public String getFiled4() {
		return this.filed4;
	}
	
	/**
	 * @param updbfFiled4
	 */
	public void setUpdbfFiled4(java.math.BigDecimal updbfFiled4) {
		this.updbfFiled4 = updbfFiled4;
	}
	
    /**
     * @return UpdbfFiled4
     */	
	public java.math.BigDecimal getUpdbfFiled4() {
		return this.updbfFiled4;
	}
	
	/**
	 * @param updafFiled4
	 */
	public void setUpdafFiled4(java.math.BigDecimal updafFiled4) {
		this.updafFiled4 = updafFiled4;
	}
	
    /**
     * @return UpdafFiled4
     */	
	public java.math.BigDecimal getUpdafFiled4() {
		return this.updafFiled4;
	}
	
	/**
	 * @param filed5
	 */
	public void setFiled5(String filed5) {
		this.filed5 = filed5 == null ? null : filed5.trim();
	}
	
    /**
     * @return Filed5
     */	
	public String getFiled5() {
		return this.filed5;
	}
	
	/**
	 * @param updbfFiled5
	 */
	public void setUpdbfFiled5(java.math.BigDecimal updbfFiled5) {
		this.updbfFiled5 = updbfFiled5;
	}
	
    /**
     * @return UpdbfFiled5
     */	
	public java.math.BigDecimal getUpdbfFiled5() {
		return this.updbfFiled5;
	}
	
	/**
	 * @param updafFiled5
	 */
	public void setUpdafFiled5(java.math.BigDecimal updafFiled5) {
		this.updafFiled5 = updafFiled5;
	}
	
    /**
     * @return UpdafFiled5
     */	
	public java.math.BigDecimal getUpdafFiled5() {
		return this.updafFiled5;
	}
	
	/**
	 * @param filed6
	 */
	public void setFiled6(String filed6) {
		this.filed6 = filed6 == null ? null : filed6.trim();
	}
	
    /**
     * @return Filed6
     */	
	public String getFiled6() {
		return this.filed6;
	}
	
	/**
	 * @param updbfFiled6
	 */
	public void setUpdbfFiled6(java.math.BigDecimal updbfFiled6) {
		this.updbfFiled6 = updbfFiled6;
	}
	
    /**
     * @return UpdbfFiled6
     */	
	public java.math.BigDecimal getUpdbfFiled6() {
		return this.updbfFiled6;
	}
	
	/**
	 * @param updafFiled6
	 */
	public void setUpdafFiled6(java.math.BigDecimal updafFiled6) {
		this.updafFiled6 = updafFiled6;
	}
	
    /**
     * @return UpdafFiled6
     */	
	public java.math.BigDecimal getUpdafFiled6() {
		return this.updafFiled6;
	}
	
	/**
	 * @param filed7
	 */
	public void setFiled7(String filed7) {
		this.filed7 = filed7 == null ? null : filed7.trim();
	}
	
    /**
     * @return Filed7
     */	
	public String getFiled7() {
		return this.filed7;
	}
	
	/**
	 * @param updbfFiled7
	 */
	public void setUpdbfFiled7(java.math.BigDecimal updbfFiled7) {
		this.updbfFiled7 = updbfFiled7;
	}
	
    /**
     * @return UpdbfFiled7
     */	
	public java.math.BigDecimal getUpdbfFiled7() {
		return this.updbfFiled7;
	}
	
	/**
	 * @param updafFiled7
	 */
	public void setUpdafFiled7(java.math.BigDecimal updafFiled7) {
		this.updafFiled7 = updafFiled7;
	}
	
    /**
     * @return UpdafFiled7
     */	
	public java.math.BigDecimal getUpdafFiled7() {
		return this.updafFiled7;
	}
	
	/**
	 * @param filed8
	 */
	public void setFiled8(String filed8) {
		this.filed8 = filed8 == null ? null : filed8.trim();
	}
	
    /**
     * @return Filed8
     */	
	public String getFiled8() {
		return this.filed8;
	}
	
	/**
	 * @param updbfFiled8
	 */
	public void setUpdbfFiled8(java.math.BigDecimal updbfFiled8) {
		this.updbfFiled8 = updbfFiled8;
	}
	
    /**
     * @return UpdbfFiled8
     */	
	public java.math.BigDecimal getUpdbfFiled8() {
		return this.updbfFiled8;
	}
	
	/**
	 * @param updafFiled8
	 */
	public void setUpdafFiled8(java.math.BigDecimal updafFiled8) {
		this.updafFiled8 = updafFiled8;
	}
	
    /**
     * @return UpdafFiled8
     */	
	public java.math.BigDecimal getUpdafFiled8() {
		return this.updafFiled8;
	}
	
	/**
	 * @param filed9
	 */
	public void setFiled9(String filed9) {
		this.filed9 = filed9 == null ? null : filed9.trim();
	}
	
    /**
     * @return Filed9
     */	
	public String getFiled9() {
		return this.filed9;
	}
	
	/**
	 * @param updbfFiled9
	 */
	public void setUpdbfFiled9(java.math.BigDecimal updbfFiled9) {
		this.updbfFiled9 = updbfFiled9;
	}
	
    /**
     * @return UpdbfFiled9
     */	
	public java.math.BigDecimal getUpdbfFiled9() {
		return this.updbfFiled9;
	}
	
	/**
	 * @param updafFiled9
	 */
	public void setUpdafFiled9(java.math.BigDecimal updafFiled9) {
		this.updafFiled9 = updafFiled9;
	}
	
    /**
     * @return UpdafFiled9
     */	
	public java.math.BigDecimal getUpdafFiled9() {
		return this.updafFiled9;
	}
	
	/**
	 * @param filed10
	 */
	public void setFiled10(String filed10) {
		this.filed10 = filed10 == null ? null : filed10.trim();
	}
	
    /**
     * @return Filed10
     */	
	public String getFiled10() {
		return this.filed10;
	}
	
	/**
	 * @param updbfFiled10
	 */
	public void setUpdbfFiled10(java.math.BigDecimal updbfFiled10) {
		this.updbfFiled10 = updbfFiled10;
	}
	
    /**
     * @return UpdbfFiled10
     */	
	public java.math.BigDecimal getUpdbfFiled10() {
		return this.updbfFiled10;
	}
	
	/**
	 * @param updafFiled10
	 */
	public void setUpdafFiled10(java.math.BigDecimal updafFiled10) {
		this.updafFiled10 = updafFiled10;
	}
	
    /**
     * @return UpdafFiled10
     */	
	public java.math.BigDecimal getUpdafFiled10() {
		return this.updafFiled10;
	}
	
	/**
	 * @param filed11
	 */
	public void setFiled11(String filed11) {
		this.filed11 = filed11 == null ? null : filed11.trim();
	}
	
    /**
     * @return Filed11
     */	
	public String getFiled11() {
		return this.filed11;
	}
	
	/**
	 * @param updbfFiled11
	 */
	public void setUpdbfFiled11(java.math.BigDecimal updbfFiled11) {
		this.updbfFiled11 = updbfFiled11;
	}
	
    /**
     * @return UpdbfFiled11
     */	
	public java.math.BigDecimal getUpdbfFiled11() {
		return this.updbfFiled11;
	}
	
	/**
	 * @param updafFiled11
	 */
	public void setUpdafFiled11(java.math.BigDecimal updafFiled11) {
		this.updafFiled11 = updafFiled11;
	}
	
    /**
     * @return UpdafFiled11
     */	
	public java.math.BigDecimal getUpdafFiled11() {
		return this.updafFiled11;
	}
	
	/**
	 * @param filed12
	 */
	public void setFiled12(String filed12) {
		this.filed12 = filed12 == null ? null : filed12.trim();
	}
	
    /**
     * @return Filed12
     */	
	public String getFiled12() {
		return this.filed12;
	}
	
	/**
	 * @param updbfFiled12
	 */
	public void setUpdbfFiled12(java.math.BigDecimal updbfFiled12) {
		this.updbfFiled12 = updbfFiled12;
	}
	
    /**
     * @return UpdbfFiled12
     */	
	public java.math.BigDecimal getUpdbfFiled12() {
		return this.updbfFiled12;
	}
	
	/**
	 * @param updafFiled12
	 */
	public void setUpdafFiled12(java.math.BigDecimal updafFiled12) {
		this.updafFiled12 = updafFiled12;
	}
	
    /**
     * @return UpdafFiled12
     */	
	public java.math.BigDecimal getUpdafFiled12() {
		return this.updafFiled12;
	}
	
	/**
	 * @param filed13
	 */
	public void setFiled13(String filed13) {
		this.filed13 = filed13 == null ? null : filed13.trim();
	}
	
    /**
     * @return Filed13
     */	
	public String getFiled13() {
		return this.filed13;
	}
	
	/**
	 * @param updbfFiled13
	 */
	public void setUpdbfFiled13(java.math.BigDecimal updbfFiled13) {
		this.updbfFiled13 = updbfFiled13;
	}
	
    /**
     * @return UpdbfFiled13
     */	
	public java.math.BigDecimal getUpdbfFiled13() {
		return this.updbfFiled13;
	}
	
	/**
	 * @param updafFiled13
	 */
	public void setUpdafFiled13(java.math.BigDecimal updafFiled13) {
		this.updafFiled13 = updafFiled13;
	}
	
    /**
     * @return UpdafFiled13
     */	
	public java.math.BigDecimal getUpdafFiled13() {
		return this.updafFiled13;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}