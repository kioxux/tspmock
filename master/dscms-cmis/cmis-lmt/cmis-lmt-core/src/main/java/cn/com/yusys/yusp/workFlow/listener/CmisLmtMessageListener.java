package cn.com.yusys.yusp.workFlow.listener;


import cn.com.yusys.yusp.flow.client.ClientBizFactory;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @className BizMessageListener
 * @Description 流程mq监听处理
 * @Date 2020/12/21 : 10:43
 */
@Service
public class CmisLmtMessageListener {
    private final Logger log = LoggerFactory.getLogger(CmisLmtMessageListener.class);

    @Autowired
    private ClientBizFactory clientBizFactory;

    @RabbitListener(queuesToDeclare = {
//            @Queue("yusp-flow.LMT_APPR_CHG_APP_FLOW_test"),// 队列名称为【yusp-flow.流程申请类型】,可以添加多个
//            @Queue("yusp-flow.LMT_COOP_CHG_APP_FLOW"),
            @Queue("yusp-flow.EDGL01"),//额度管控白名单申请流程
            @Queue("yusp-flow.HZXM05"),//合作方额度冻结解冻终止审批流程-分支机构
            @Queue("yusp-flow.HZXM06"),//合作方额度冻结解冻终止审批流程-总行部门
            @Queue("yusp-flow.EDGL02"),//额度调整审批流程
    })//
    @RabbitHandler
    public void receiveQueue(ResultInstanceDto message) {
        log.info("客户端(额度服务)业务监听:"+message);
        clientBizFactory.processBiz(message);

    }
}
