package cn.com.yusys.yusp.service.server.cmislmt0013;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ContAccRelMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.server.comm.Comm4LmtCalFormulaUtils;
import cn.com.yusys.yusp.service.server.comm.Comm4Service;
import cn.com.yusys.yusp.util.BigDecimalUtil;
import com.alibaba.excel.util.StringUtils;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0013Service
 * @类描述: #对内服务类
 * @功能描述: 台账占用
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0013Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0013Service.class);

    @Autowired
    private LmtContRelService lmtContRelService;

    @Autowired
    private ApprLmtSubBasicInfoService apprLmtSubBasicInfoService ;

    @Autowired
    private ContAccRelMapper contAccRelMapper ;

    @Autowired
    private ContAccRelService contAccRelService ;

    @Autowired
    private Comm4Service comm4Service ;

    @Autowired
    private ApprCoopSubInfoService apprCoopSubInfoService ;

    @Autowired
    private Comm4LmtCalFormulaUtils comm4LmtCalFormulaUtils ;

    @Transactional
    public CmisLmt0013RespDto execute(CmisLmt0013ReqDto reqDto) throws YuspException, Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.value);
        CmisLmt0013RespDto resqDto = new CmisLmt0013RespDto() ;
        //交易流水号
        String serno = reqDto.getSerno() ;
        //合同编号
        String bizNo = reqDto.getBizNo() ;
        //非空校验
        if(org.apache.commons.lang.StringUtils.isBlank(bizNo)){
            throw new YuspException(EclEnum.ECL070133.key, "合同编号" + EclEnum.ECL070133.value);
        }
        logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key+":【{}】台账占用开始", serno) ;

        Map<String, String> paramMap = (Map<String, String>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(reqDto);
        Map<String, String> resultMap = new HashMap<>() ;
        resultMap.putAll(paramMap);
        resultMap.put("serviceCode", CmisLmtConstants.CMIS_LMT0013_SERVICE_CODE) ;

        //遍历分项信息
        for(CmisLmt0013ReqDealBizListDto dealBizInfo:reqDto.getCmisLmt0013ReqDealBizListDtos()){
            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key+":处理台账占用数据【{}】", JSON.toJSONString(dealBizInfo)) ;

            String dealBizNo = dealBizInfo.getDealBizNo() ;
            if(StringUtils.isEmpty(dealBizNo)){
                throw new YuspException(EclEnum.ECL070133.key, "台账编号" + EclEnum.ECL070133.value);
            }

            String origiDealBizNo = dealBizInfo.getOrigiDealBizNo() ;
            String sysId = reqDto.getSysId();

            //根据交易流水查选是否已经占用成功
            if(comm4Service.judgeContDealIsSuss(dealBizNo, null,sysId) && !Objects.equals(dealBizNo, origiDealBizNo)){
                ContAccRel contAccRel = contAccRelService.selectByTranAccNoAndDealNo(dealBizNo, null,sysId) ;
                if (contAccRel!=null){
                    //根据合同查询合同信息
                    //通过合同编号，获取合同信息
                    //List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByDealBizNo(bizNo);
                    String oriBizNo = contAccRel.getDealBizNo() ;
                    List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(oriBizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
                    if(CollectionUtils.isNotEmpty(lmtContRelList)) {
                        //添加技术标识，同一个合同查出两条记录，占用的时候，只需要挂一条记录
                        for (LmtContRel lmtContRel : lmtContRelList) {
                            comm4Service.cancelContAccRel(contAccRel, lmtContRel, resultMap);
                        }
                    }
                    //撤销占用，删除台账信息，恢复分项可出账金额，已出账金额，用信余额等
                    logger.info("交易已经成功,撤销处理，删除台账信息：{}", JSON.toJSONString(contAccRel)) ;
                    contAccRelMapper.deleteByPrimaryKey(contAccRel.getPkId()) ;
                }
            }
            //台账状态
            String dealBizStatus = dealBizInfo.getDealBizStatus() ;
            //是否无缝衔接
            String isFollowBiz = dealBizInfo.getIsFollowBiz() ;
            //一个合同编号查出两个合同或多个合同，处理第二条合同时，台账占用已经生成，不需要处理。eg:如果原台账编号和本次台账编号一致，会导致数据先插入后删除
            if(CmisLmtConstants.YES_NO_Y.equals(isFollowBiz) && !StringUtils.isEmpty(origiDealBizNo)){
                followBiz(resultMap, bizNo, dealBizInfo);
            }

            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key+":推送给额度状态{}---(状态不为200 不进行占用额度操作)------------>start", dealBizStatus) ;
            //推送过来数据，只有生效状态才可以进行占用分项数据信息
            if(CmisLmtConstants.STD_ZB_BIZ_STATUS_200.equals(dealBizStatus)){
                bizStatus200extracted(resultMap, bizNo, dealBizInfo, reqDto);
            }else{
                //台账状态100处理逻辑
                bizStatus100extracted(resultMap, bizNo, dealBizInfo, reqDto);
            }
            insertLmtContRelextracted(reqDto, dealBizInfo, dealBizStatus);
        }
        resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
        resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.value);
        return resqDto;
    }

    /**
     * @作者:lizx
     * @方法名称: insertLmtContRelextracted
     * @方法描述:  台账占用，插入记录
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/8/13 11:08
     * @param reqDto: 请求报文
     * @param dealBizInfo:占用明细
     * @param dealBizStatus:状态
     * @return: void
     * @算法描述: 无
    */
    private void insertLmtContRelextracted(CmisLmt0013ReqDto reqDto, CmisLmt0013ReqDealBizListDto dealBizInfo, String dealBizStatus) {
        logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key+":插入【LmtContRel】数据------------>start") ;
        //创建对象
        ContAccRel contAccRel = new ContAccRel() ;
        //拷贝数据
        BeanUtils.copyProperties(dealBizInfo, contAccRel);
        //台账编号
        String dealBizNo = dealBizInfo.getDealBizNo() ;
        //主键
        contAccRel.setPkId(comm4Service.generatePkId());
        //系统编号
        contAccRel.setSysId(reqDto.getSysId()) ;
        //台账编号
        contAccRel.setTranAccNo(dealBizNo);
        //业务交易流水号
        contAccRel.setDealBizNo(reqDto.getBizNo());
        //合同金额
        contAccRel.setBizAmt(dealBizInfo.getDealBizAmtCny());
        //占用总金额（折人民币）
        contAccRel.setAccTotalAmtCny(dealBizInfo.getDealBizAmtCny());
        //占用敞口金额（折人民币）
        contAccRel.setAccSpacAmtCny(dealBizInfo.getDealBizSpacAmtCny());
        //占用总余额（折人民币）
        contAccRel.setAccTotalBalanceAmtCny(dealBizInfo.getDealBizAmtCny());
        //占用敞口余额（折人民币）
        contAccRel.setAccSpacBalanceAmtCny(dealBizInfo.getDealBizSpacAmtCny());
        //保证金比例
        contAccRel.setSecurityRate(dealBizInfo.getDealBizBailPreRate());
        //保证金金额
        contAccRel.setSecurityAmt(dealBizInfo.getDealBizBailPreAmt());
        //台账状态
        contAccRel.setStatus(dealBizStatus);
        //操作类型 默认： 01 新增
        contAccRel.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        //登记人
        contAccRel.setInputId(reqDto.getInputId());
        //登记机构
        contAccRel.setInputBrId(reqDto.getInputBrId());
        //登记日期
        contAccRel.setInputDate(comm4Service.formatDateStr(reqDto.getInputDate()));
        //更新人
        contAccRel.setUpdId(reqDto.getInputId());
        //更新机构
        contAccRel.setUpdBrId(reqDto.getInputBrId());
        //更新日期
        contAccRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        //更新时间
        contAccRel.setUpdateTime(comm4Service.getCurrrentDate());
        //创建时间
        contAccRel.setCreateTime(comm4Service.getCurrrentDate());
        //产品类型属性
        contAccRel.setPrdTypeProp(dealBizInfo.getPrdTypeProp());
        logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key+":插入【LmtContRel】数据,数据信息{}------------>end", JSON.toJSONString(contAccRel)) ;
        contAccRelMapper.insert(contAccRel) ;
    }

    private void bizStatus200extracted(Map<String, String> resultMap, String bizNo, CmisLmt0013ReqDealBizListDto dealBizInfo, CmisLmt0013ReqDto reqDto) {
        //用信产品编号
        String prdId = dealBizInfo.getPrdId() ;
        //机构信息
        String inputBrId = resultMap.get("inputBrId") ;
        //贴现限额累加处理
        comm4Service.lmtLastDiscOrgAddUseAmt(resultMap, BigDecimalUtil.replaceNull(dealBizInfo.getDealBizAmtCny()), prdId, inputBrId);
        //通过合同编号，获取合同信息
        //List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByDealBizNo(bizNo);
        List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(bizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
        if(CollectionUtils.isNotEmpty(lmtContRelList)){
            //添加技术标识，同一个合同查出两条记录，占用的时候，只需要挂一条记录
            for (LmtContRel lmtContRel : lmtContRelList) {
                //额度类型 01 单一法人  03 合作方 06 白名单
                String lmtType = lmtContRel.getLmtType() ;
                //分项编号
                String limitSubNo = lmtContRel.getLimitSubNo() ;
                // 交易业务类型 1 一般合同 2 最高额合同  3 最高额协议
                String dealBizType = lmtContRel.getDealBizType() ;
                //合同占用总额
                BigDecimal bizTotalAmtCny = BigDecimalUtil.replaceNull(lmtContRel.getBizTotalAmtCny());
                BigDecimal dealBizSpacAmtCny = BigDecimalUtil.replaceNull(dealBizInfo.getDealBizSpacAmtCny()) ;
                BigDecimal dealBizAmtCny = BigDecimalUtil.replaceNull(dealBizInfo.getDealBizAmtCny()) ;

                //产品类型属性
                String prdTypeProp = dealBizInfo.getPrdTypeProp() ;

                if(CmisLmtConstants.STD_ZB_LMT_TYPE_03.equals(lmtType)){
                    ApprCoopSubInfo apprCoopSubInfo = apprCoopSubInfoService.selectApprCoopSubInfoByLmtSubNo(limitSubNo) ;
                    logger.info("合作方额度台账占用start--------------->{}", apprCoopSubInfo) ;
                    ApprCoopSubInfo oldApprCoopSubInfo = new ApprCoopSubInfo() ;//合作方分项信息
                    if(apprCoopSubInfo == null || StringUtils.isEmpty(apprCoopSubInfo.getApprSerno())){
                        new YuspException(EclEnum.ECL070007.key,"【"+ limitSubNo +"】"+EclEnum.ECL070007.value);
                    }
                    BeanUtils.copyProperties(apprCoopSubInfo, oldApprCoopSubInfo);
                    //修改前
                    BigDecimal bfLoanBalance = BigDecimalUtil.replaceNull(apprCoopSubInfo.getLoanBalance()) ;
                    //修改后
                    BigDecimal afLoanBalance = bfLoanBalance.add(dealBizAmtCny) ;
                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key+": 合作方【"+apprCoopSubInfo.getApprSubSerno()+"】," +
                            "用信总余额更改为：" + bfLoanBalance + " --> "+ afLoanBalance);
                    apprCoopSubInfo.setLoanBalance(afLoanBalance);
                    apprCoopSubInfoService.updateByPKeyInApprLmtChgDetails(apprCoopSubInfo, resultMap) ;

                    logger.info("合作方额度台账占用--------------->end") ;
                }else{
                    //如果是法人额度占用，更改分项中的已经出账金额，可出账金额，用信余额，用信敞口余额
                    logger.info("单一法人额度台账占用-------1-------->start") ;
                    //根据分项编号获取分项信息
                    ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(limitSubNo) ;
                    //分项判空处理
                    if(null == apprLmtSubBasicInfo || StringUtils.isEmpty(apprLmtSubBasicInfo.getApprSubSerno())){
                        new YuspException(EclEnum.ECL070007.key,"【"+ limitSubNo +"】"+EclEnum.ECL070007.value) ;
                    }

                    //如果是最高额协议，则台账占用，额度占用到额度分项，台账占用产品分项
                    if(CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(dealBizType)){
                        if(StringUtils.isEmpty(prdTypeProp)) prdTypeProp = "" ;
                        String cusType = apprLmtSubBasicInfo.getCusType() ;
                        String limitStrNo = comm4Service.getLimitStrNo(cusType) ;
                        //根据分项信息和用信产品获取到产品分项信息
                        apprLmtSubBasicInfo = comm4Service.selectSubBasicInfoByParentId(limitSubNo, prdId, limitStrNo, prdTypeProp) ;
                        Optional.ofNullable(apprLmtSubBasicInfo).orElseThrow(()->new YuspException(EclEnum.ECL070081.key,EclEnum.ECL070081.value));
                        //修改前
                        BigDecimal bfOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt()) ;
                        //更改后
                        BigDecimal afOutstndAmt = BigDecimalUtil.replaceNull(bfOutstndAmt.add(dealBizAmtCny)) ;

                        //修改前
                        BigDecimal bfSpacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt()) ;
                        //更改后
                        BigDecimal afSpacOutstndAmt = BigDecimalUtil.replaceNull(bfSpacOutstndAmt.add(dealBizSpacAmtCny)) ;
                        apprLmtSubBasicInfo.setOutstndAmt(afOutstndAmt);
                        apprLmtSubBasicInfo.setSpacOutstndAmt(afSpacOutstndAmt);

                        //插入占用关系
                        LmtContRel lmtContRel1 = new LmtContRel() ;
                        //合同编号
                        lmtContRel1.setDealBizNo(dealBizInfo.getDealBizNo());
                        //主键
                        lmtContRel1.setPkId(comm4Service.generatePkId());
                        //客户号
                        lmtContRel1.setCusId(dealBizInfo.getCusId());
                        //客户名称
                        lmtContRel1.setCusName(dealBizInfo.getCusName());
                        //系统编号
                        lmtContRel1.setSysId(lmtContRel.getSysId());
                        //合同类型
                        lmtContRel1.setDealBizType(CmisLmtConstants.DEAL_BIZ_TYPE_1);
                        //合同属性
                        lmtContRel1.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_2);
                        //产品编号
                        lmtContRel1.setPrdId(dealBizInfo.getPrdId());
                        //产品名称
                        lmtContRel1.setPrdName(dealBizInfo.getPrdName());
                        //产品属性
                        lmtContRel1.setPrdTypeProp(dealBizInfo.getPrdTypeProp());
                        //开始日期
                        lmtContRel1.setStartDate(dealBizInfo.getStartDate());
                        //结束日期
                        lmtContRel1.setEndDate(dealBizInfo.getEndDate());
                        //保证金比例
                        lmtContRel1.setSecurityRate(BigDecimal.ZERO);
                        //保证金金额
                        lmtContRel1.setSecurityAmt(BigDecimal.ZERO);
                        //合同状态
                        lmtContRel1.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_200);
                        //分项编号
                        lmtContRel1.setLimitSubNo(apprLmtSubBasicInfo.getApprSubSerno());
                        //额度类型
                        lmtContRel1.setLmtType(lmtContRel.getLmtType());
                        //业务交易金额（折人民币）
                        lmtContRel1.setBizAmt(dealBizAmtCny);
                        //占用总金额（折人民币）
                        lmtContRel1.setBizTotalAmtCny(dealBizAmtCny);
                        //占用敞口金额（折人民币）
                        lmtContRel1.setBizSpacAmtCny(dealBizSpacAmtCny);
                        //占用总余额（折人民币）
                        lmtContRel1.setBizTotalBalanceAmtCny(dealBizAmtCny);
                        //占用敞口余额（折人民币）
                        lmtContRel1.setBizSpacBalanceAmtCny(dealBizSpacAmtCny);
                        //操作类型
                        lmtContRel1.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                        //更新人
                        lmtContRel1.setUpdId(reqDto.getInputId());
                        //更新机构
                        lmtContRel1.setUpdBrId(reqDto.getInputBrId());
                        //更新日期
                        lmtContRel1.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                        //创建时间
                        lmtContRel1.setCreateTime(comm4Service.getCurrrentDate());
                        //更新时间
                        lmtContRel1.setUpdateTime(comm4Service.getCurrrentDate());
                        //登记人
                        lmtContRel1.setInputId(reqDto.getInputId());
                        //登记机构
                        lmtContRel1.setInputBrId(reqDto.getInputBrId());
                        //登记日期
                        lmtContRel1.setInputDate(comm4Service.formatDateStr(reqDto.getInputDate()));
                        //更新时间
                        lmtContRel1.setPrdTypeProp(lmtContRel.getPrdTypeProp());
                        //输入落库
                        lmtContRelService.insert(lmtContRel1) ;
                    }

                    comm4Service.occAvlAndPvpOutstndAmt(apprLmtSubBasicInfo, dealBizAmtCny, dealBizSpacAmtCny) ;

                    //用信余额
                    //修改前
                    BigDecimal bfLoanBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanBalance());
                    //修改后
                    BigDecimal afLoanBalance = bfLoanBalance.add(dealBizAmtCny);
                    apprLmtSubBasicInfo.setLoanBalance(afLoanBalance);

                    //用信敞口余额
                    //修改前
                    BigDecimal bfLoanSpacBalance = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getLoanSpacBalance());
                    //修改后
                    BigDecimal afLoanSpacBalance = bfLoanSpacBalance.add(dealBizSpacAmtCny) ;
                    apprLmtSubBasicInfo.setLoanSpacBalance(afLoanSpacBalance);

                    logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key+":分项【"+apprLmtSubBasicInfo.getApprSubSerno()+"】,"
                            +"用信敞口余额更改为："+bfLoanSpacBalance+" --> "+ afLoanSpacBalance
                            +"用信余额更改为："+bfLoanBalance+" --> "+ afLoanBalance);
                    apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap) ;
                    //累加金额处理
                    comm4LmtCalFormulaUtils.calLmtAmtAddExecute(apprLmtSubBasicInfo.getApprSubSerno());

                    //上级目录处理
                    String parentId = apprLmtSubBasicInfo.getParentId() ;
                    //根据上级目录，查询一级分项信息
                    if(!StringUtils.isEmpty(parentId)){
                        ApprLmtSubBasicInfo apprLmtSubBasicInfoOne = apprLmtSubBasicInfoService.selectByApprSubSerno(parentId) ;
                        //初始化最新数据
                        if(Objects.nonNull(apprLmtSubBasicInfoOne)){
                            comm4Service.occAvlAndPvpOutstndAmt(apprLmtSubBasicInfoOne, dealBizAmtCny, dealBizSpacAmtCny) ;
                            apprLmtSubBasicInfoOne.setLoanBalance(dealBizAmtCny.add(apprLmtSubBasicInfoOne.getLoanBalance()));
                            apprLmtSubBasicInfoOne.setLoanSpacBalance(dealBizSpacAmtCny.add(apprLmtSubBasicInfoOne.getLoanSpacBalance()));
                            //调用更新代码，并且对比原始数据，流程金额更改记录
                            apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfoOne, resultMap) ;
                            //累加金额处理
                            comm4LmtCalFormulaUtils.calParentIdLmtAmtAddExecute(apprLmtSubBasicInfoOne.getApprSubSerno());
                        }
                    }
                    logger.info("单一法人额度台账占用--------------->end") ;
                }
            }
        }
    }

    private void bizStatus100extracted(Map<String, String> resultMap, String bizNo, CmisLmt0013ReqDealBizListDto dealBizInfo, CmisLmt0013ReqDto reqDto) {
        //根据合同查询合同信息
        //通过合同编号，获取合同信息
        //List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByDealBizNo(bizNo);
        List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(bizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
        if(CollectionUtils.isNotEmpty(lmtContRelList)){
            for (LmtContRel lmtContRel : lmtContRelList) {
                BigDecimal dealBizAmtCny = BigDecimalUtil.replaceNull(dealBizInfo.getDealBizAmtCny()) ;
                BigDecimal dealBizSpacAmtCny  =BigDecimalUtil.replaceNull(dealBizInfo.getDealBizSpacAmtCny()) ;
                //用信产品编号
                String prdId = dealBizInfo.getPrdId() ;
                //获取授信类型
                String lmtType = lmtContRel.getLmtType() ;
                //获取分项编号
                String limitSubNo = lmtContRel.getLimitSubNo() ;
                //交易业务类型    1-一般合同  2-最高额合同  3-最高额授信协议
                String dealBizType = lmtContRel.getDealBizType() ;
                //产品类型属性
                String prdTypeProp = Optional.ofNullable(dealBizInfo.getPrdTypeProp()).orElse("") ;

                //如果是法人额度占用，更改分项中的已经出账金额，可出账金额，用信余额，用信敞口余额
                if(CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType)) {
                    logger.info("单一法人额度台账占用------2--------->start");
                    //根据分项编号获取分项信息
                    ApprLmtSubBasicInfo apprLmtSubBasicInfo = apprLmtSubBasicInfoService.selectAppLmtSubBasicInfoByLmtSubNo(limitSubNo);
                    ApprLmtSubBasicInfo oldApprLmtSubBasicInfo = new ApprLmtSubBasicInfo();//合作方分项信息
                    if (CmisLmtConstants.STD_ZB_LMT_TYPE_01.equals(lmtType)) {
                        if (null == apprLmtSubBasicInfo || StringUtils.isEmpty(apprLmtSubBasicInfo.getApprSubSerno())) {
                            new YuspException(EclEnum.ECL070007.key, "【" + limitSubNo + "】" + EclEnum.ECL070007.value);
                        }
                        BeanUtils.copyProperties(apprLmtSubBasicInfo, oldApprLmtSubBasicInfo);
                    }

                    //如果是最高额协议，则台账占用，额度占用到额度分项，台账占用产品分项
                    if (CmisLmtConstants.STD_ZB_CONT_TYPE_3.equals(dealBizType)) {
                        String cusType = apprLmtSubBasicInfo.getCusType() ;
                        String limitStrNo = comm4Service.getLimitStrNo(cusType) ;
                        //根据分项信息和用信产品获取到产品分项信息
                        apprLmtSubBasicInfo = comm4Service.selectSubBasicInfoByParentId(limitSubNo, prdId, limitStrNo, prdTypeProp) ;
                        Optional.ofNullable(apprLmtSubBasicInfo).orElseThrow(()->new YuspException(EclEnum.ECL070081.key,EclEnum.ECL070081.value));
                        //修改前
                        BigDecimal bfOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getOutstndAmt()) ;
                        //更改后
                        BigDecimal afOutstndAmt = BigDecimalUtil.replaceNull(bfOutstndAmt.add(dealBizAmtCny)) ;

                        //修改前
                        BigDecimal bfSpacOutstndAmt = BigDecimalUtil.replaceNull(apprLmtSubBasicInfo.getSpacOutstndAmt()) ;
                        //更改后
                        BigDecimal afSpacOutstndAmt = BigDecimalUtil.replaceNull(bfSpacOutstndAmt.add(dealBizSpacAmtCny)) ;
                        apprLmtSubBasicInfo.setOutstndAmt(afOutstndAmt);
                        apprLmtSubBasicInfo.setSpacOutstndAmt(afSpacOutstndAmt);

                        //插入占用关系
                        LmtContRel lmtContRel1 = new LmtContRel() ;
                        //合同编号
                        lmtContRel1.setDealBizNo(dealBizInfo.getDealBizNo());
                        //主键
                        lmtContRel1.setPkId(comm4Service.generatePkId());
                        //客户号
                        lmtContRel1.setCusId(dealBizInfo.getCusId());
                        //客户名称
                        lmtContRel1.setCusName(dealBizInfo.getCusName());
                        //系统编号
                        lmtContRel1.setSysId(lmtContRel.getSysId());
                        //合同类型
                        lmtContRel1.setDealBizType(CmisLmtConstants.DEAL_BIZ_TYPE_1);
                        //合同属性
                        lmtContRel1.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_2);
                        //产品编号
                        lmtContRel1.setPrdId(dealBizInfo.getPrdId());
                        //产品名称
                        lmtContRel1.setPrdName(dealBizInfo.getPrdName());
                        //产品属性
                        lmtContRel1.setPrdTypeProp(dealBizInfo.getPrdTypeProp());
                        //开始日期
                        lmtContRel1.setStartDate(dealBizInfo.getStartDate());
                        //结束日期
                        lmtContRel1.setEndDate(dealBizInfo.getEndDate());
                        //保证金比例
                        lmtContRel1.setSecurityRate(BigDecimal.ZERO);
                        //保证金金额
                        lmtContRel1.setSecurityAmt(BigDecimal.ZERO);
                        //合同状态
                        lmtContRel1.setBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_100);
                        //分项编号
                        lmtContRel1.setLimitSubNo(apprLmtSubBasicInfo.getApprSubSerno());
                        //额度类型
                        lmtContRel1.setLmtType(lmtContRel.getLmtType());
                        //业务交易金额（折人民币）
                        lmtContRel1.setBizAmt(dealBizAmtCny);
                        //占用总金额（折人民币）
                        lmtContRel1.setBizTotalAmtCny(dealBizAmtCny);
                        //占用敞口金额（折人民币）
                        lmtContRel1.setBizSpacAmtCny(dealBizSpacAmtCny);
                        //占用总余额（折人民币）
                        lmtContRel1.setBizTotalBalanceAmtCny(dealBizAmtCny);
                        //占用敞口余额（折人民币）
                        lmtContRel1.setBizSpacBalanceAmtCny(dealBizSpacAmtCny);
                        //操作类型
                        lmtContRel1.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                        //更新人
                        lmtContRel1.setUpdId(reqDto.getInputId());
                        //更新机构
                        lmtContRel1.setUpdBrId(reqDto.getInputBrId());
                        //更新日期
                        lmtContRel1.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                        //更新日期
                        lmtContRel1.setInputDate(comm4Service.formatDateStr(reqDto.getInputDate()));
                        //更新日期
                        lmtContRel1.setInputBrId(reqDto.getInputBrId());
                        //更新日期
                        lmtContRel1.setInputId(reqDto.getInputId());
                        //创建时间
                        lmtContRel1.setCreateTime(comm4Service.getCurrrentDate());
                        //更新时间
                        lmtContRel1.setUpdateTime(comm4Service.getCurrrentDate());
                        //更新时间
                        lmtContRel1.setPrdTypeProp(lmtContRel.getPrdTypeProp());
                        //输入落库
                        lmtContRelService.insert(lmtContRel1) ;
                    }

                    //可出账金额 = 已出账金额 = 本次出账金额  && 已出账金额 = 已出账金额 + 本次出账金额
                    comm4Service.occAvlAndPvpOutstndAmt(apprLmtSubBasicInfo, dealBizAmtCny, dealBizSpacAmtCny) ;
                    apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfo, resultMap);
                    //累加金额处理
                    comm4LmtCalFormulaUtils.calLmtAmtAddExecute(apprLmtSubBasicInfo.getApprSubSerno());

                    //上级目录处理
                    String parentId = apprLmtSubBasicInfo.getParentId();
                    //根据上级目录，查询一级分项信息
                    if (!StringUtils.isEmpty(parentId)) {
                        ApprLmtSubBasicInfo apprLmtSubBasicInfoOne = apprLmtSubBasicInfoService.selectByApprSubSerno(parentId);
                        if (Objects.nonNull(apprLmtSubBasicInfoOne)) {
                            //可出账金额 = 已出账金额 = 本次出账金额  && 已出账金额 = 已出账金额 + 本次出账金额
                            comm4Service.occAvlAndPvpOutstndAmt(apprLmtSubBasicInfoOne, dealBizAmtCny, dealBizSpacAmtCny) ;
                            //调用更新代码，并且对比原始数据，流程金额更改记录
                            apprLmtSubBasicInfoService.updateByPKeyInApprLmtChgDetails(apprLmtSubBasicInfoOne, resultMap);
                            //累加金额处理
                            comm4LmtCalFormulaUtils.calParentIdLmtAmtAddExecute(apprLmtSubBasicInfoOne.getApprSubSerno());
                        }
                    }
                    logger.info("单一法人额度台账占用--------------->end");
                }
            }
        }
    }

    /**
     * @作者:lizx
     * @方法名称: followBiz
     * @方法描述:  无缝衔接处理
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/10/24 17:27
     * @param resultMap:
     * @param bizNo:
     * @param dealBizInfo:
     * @return: void
     * @算法描述: 无
    */
    private void followBiz(Map<String, String> resultMap, String bizNo, CmisLmt0013ReqDealBizListDto dealBizInfo) {
        //无缝衔接
        //原交易业务编号
        String origiDealBizNo = dealBizInfo.getOrigiDealBizNo() ;
        //原加以业务状态
        String origiDealBizStatus = dealBizInfo.getOrigiDealBizStatus() ;
        //原交易恢复类型
        String origiRecoverType = dealBizInfo.getOrigiRecoverType() ;
        //原交易属性
        String origiBizAttr = dealBizInfo.getOrigiBizAttr() ;
        logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key+":撤销占用，原业务编号为【{}】,原恢复类型为【{}】", origiDealBizNo, origiRecoverType) ;

        if(CmisLmtConstants.STD_RECOVER_TYPE_06.equals(origiRecoverType)){
            //撤销恢复
            //根据原交易业务编号，获取台账信息
            ContAccRel contAccRel = contAccRelService.selectContAccRelByTranAccNo(origiDealBizNo) ;
            logger.info(DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key+":撤销占用，原台账信息【{}】", JSON.toJSONString(contAccRel)) ;
            //撤销占用，删除台账信息，恢复分项可出账金额，已出账金额，用信余额等
            if (contAccRel!=null){
                //根据合同查询合同信息
                //通过合同编号，获取合同信息
                //List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelByDealBizNo(bizNo);
                List<LmtContRel> lmtContRelList = lmtContRelService.selectLmtContRelListByDealBizNo(bizNo, CmisLmtConstants.STD_ZB_BIZ_ATTR_1) ;
                if(CollectionUtils.isNotEmpty(lmtContRelList)) {
                    //添加技术标识，同一个合同查出两条记录，占用的时候，只需要挂一条记录
                    for (LmtContRel lmtContRel : lmtContRelList) {
                        comm4Service.cancelContAccRel(contAccRel, lmtContRel, resultMap);
                    }
                }
                //撤销占用，删除台账信息，恢复分项可出账金额，已出账金额，用信余额等
                logger.info("撤销占用删除台账信息：{}", JSON.toJSONString(contAccRel)) ;
                contAccRelMapper.deleteByPrimaryKey(contAccRel.getPkId()) ;
            }
        }else{
            throw new YuspException(EclEnum.ECL070087.key,EclEnum.ECL070087.value) ;
        }
    }
}
