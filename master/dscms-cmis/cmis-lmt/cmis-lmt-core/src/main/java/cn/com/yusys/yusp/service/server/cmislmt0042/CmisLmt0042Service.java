package cn.com.yusys.yusp.service.server.cmislmt0042;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0042.req.CmisLmt0042ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0042.resp.CmisLmt0042ListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0042.resp.CmisLmt0042RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.cmiscfg0002.resp.CmisCfg0002ListRespDto;
import cn.com.yusys.yusp.service.LmtSubBasicConfService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-lmt模块
 * @类名称: CmisLmt0042Service
 * @类描述: #对内服务类
 * @功能描述: 根据额度品种编号获取产品扩展属性
 * @修改备注: 2021/7/12     zhangjw   新增接口
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CmisLmt0042Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0042Service.class);

    @Autowired
    private LmtSubBasicConfService lmtSubBasicConfService ;

    @Transactional
    public CmisLmt0042RespDto execute(CmisLmt0042ReqDto reqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0042.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0042.value);
        CmisLmt0042RespDto resqDto = new CmisLmt0042RespDto() ;

        try {
            logger.info("根据额度品种编号获取产品扩展属性",DscmsLmtEnum.TRADE_CODE_CMISLMT0042.key);
            String limitSubNo = reqDto.getLimitSubNo();
            List<CmisCfg0002ListRespDto> list = lmtSubBasicConfService.queryDetailBylimitSubNo(limitSubNo);
            List<CmisLmt0042ListRespDto> cmisLmt0042ListRespDtoList = new ArrayList<CmisLmt0042ListRespDto>();

            if(list!=null && list.size()>0){
                for(CmisCfg0002ListRespDto cfg :list){
                    CmisLmt0042ListRespDto cmisLmt0042ListRespDto = new CmisLmt0042ListRespDto();
                    BeanUtils.copyProperties(cfg,cmisLmt0042ListRespDto);
                    cmisLmt0042ListRespDtoList.add(cmisLmt0042ListRespDto);
                }
            }
            //将列表放入返回中
            resqDto.setErrorCode(SuccessEnum.SUCCESS.key);
            resqDto.setErrorMsg(SuccessEnum.SUCCESS.value);
            resqDto.setCfgPrdTypePropertiesList(cmisLmt0042ListRespDtoList);

        } catch (YuspException e) {
            logger.error("根据额度品种编号获取产品扩展属性报错：",e);
            resqDto.setErrorCode(e.getCode());
            resqDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0042.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0042.value);
        return resqDto;
    }

}